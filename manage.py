# encoding: utf-8
import os

import click

from app import create_app, db, mongo, redis
from app.sys.init import execute_update
from app.sys.utils import drop_all_collections

app = create_app(os.getenv('FLASK_ENV') or 'default')


# export FLASK_APP=manage.py && export FLASK_DEBUG=1 && flask shell
@app.shell_context_processor
def make_shell_context():
    return dict(app=app, db=db, mongo=mongo, redis=redis)


@app.cli.command()
def test():
    """ Run the unit test. """
    import unittest
    tests = unittest.TestLoader().discover('tests')
    unittest.TextTestRunner(verbosity=2).run(tests)


@app.cli.command()
def daily():
    """ Run daily update """
    execute_update('daily')


@app.cli.command()
def init():
    """ Run init update """
    execute_update('init')


@app.cli.command()
def update_all():
    """ Run all update """
    r = drop_all_collections()
    app.logger.warning(f'delete collections: {r}')

    execute_update('update_all')


@app.cli.command()
@click.argument('month', required=False)
@click.argument('cover', required=False)
def init_cache(month, cover):
    if not month:
        month = 1
    month = int(month) * -1
    from app.data.workshop_health_index.cache.cache import global_cache
    global_cache.init_data(months_ago=month, cover=bool(cover))


@app.cli.command()
def profile(length=25, profile_dir=None):
    """ 代码分析器监视下启动 app """
    # 使用`flask profile`启动, 终端会显示每条请求的分析数据
    # 包含最慢运行的25个函数, --length 选项可修改, 指定--profile_dir可用指定保存目录
    # 官方文档(https://docs.python.org/3/library/profile.html)
    from werkzeug.contrib.profiler import ProfilerMiddleware
    app.wsgi_app = ProfilerMiddleware(app.wsgi_app, restrictions=[length],
                                      profile_dir=profile_dir)
    app.run()


if __name__ == '__main__':
    app.run()
