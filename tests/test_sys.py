import unittest

from app import create_app
from app.sys.utils import calc_month_delta


class SysBlueprintTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        # drop_all_collections()
        self.app_context.pop()

    def test_month_delta(self):
        delta = calc_month_delta()
        self.assertIsInstance(delta, int)
