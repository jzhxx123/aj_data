import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # current_app.logger.debug(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()


class NewBigThreeMajorApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # current_app.logger.debug(self.result)
        pass

    # @unittest.skip
    def test_can_get_correct_gd_get_safe_status_data(self):
        '''测试安全情况接口可以获取到正确数据'''
        url = "/new_big/gongdian/gd_get_safe_status"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['C', 'D', 'count', 'safety_pro']
            self.assertEqual(set(keys), set(self.result['data'].keys()))
        else:
            current_app.logger.debug("供电专业安全情况接口出错")

    # @unittest.skip
    def test_can_get_correct_gd_get_yesterday_safety_info_data(self):
        '''测试昨日安全生产信息接口可以获取到正确数据'''
        url = "/new_big/gongdian/gd_get_yesterday_safety_info"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("供电专业昨日安全生产信息接口出错")

    # @unittest.skip
    def test_can_get_correct_gd_get_yesterday_gongdian_shigu_data(self):
        ''' 测试昨日外局事故信息接口可以获取到正确的数据 '''
        url = "/new_big/gongdian/gd_get_yesterday_gongdian_shigu"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', "tags"]
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("供电专业供电专业昨日局外事故接口出错")

    # @unittest.skip
    def test_can_get_correct_gd_get_safety_risk_warning_data(self):
        ''' 测试安全风险预警接口可以获取到正确的数据 '''
        url = "/new_big/gongdian/gd_get_safety_risk_warning"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', "tags", "date"]
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("供电专业安全风险预警接口出错")

    # @unittest.skip
    def test_can_get_corrent_gd_get_lwtj_status(self):
        ''' 测试职工两违接口可以获取到正确的数据 '''
        url = "/new_big/gongdian/gd_get_lwtj_status"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        keys = ['title', 'data']
        if self.result['data']:
            for item in self.result['data']:
                self.assertEquals(set(keys), set(item.keys()))
                out_keys = ['data', 'pivot']
                self.assertEquals(set(out_keys), set(item['data'].keys()))
                pie_keys = ['amount', 'name', 'rank', 'value']
                for in_item in item['data']['data']:
                    self.assertEquals(set(pie_keys), in_item.keys())
        else:
            current_app.logger.debug("供电职工两违无数据")

    def test_can_get_corrent_get_ganbu_luzhi_koufen_wb(self):
        ''' 测试供电干部履职扣分可以获取到正确的数据 '''
        url = "/new_big/gongdian/gd_get_ganbu_luzhi1"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        keys = ['title', 'data']
        for item in self.result['data']:
            self.assertEquals(set(keys), set(item.keys()))
            for out_item in item['data']:
                self.assertEquals(set(keys), set(out_item.keys()))
                out_keys = ['data', 'pivot']
                self.assertEquals(set(out_keys), set(out_item['data'].keys()))
                pie_keys = ['amount', 'name', 'rank', 'value']
                for in_item in out_item['data']['data']:
                    self.assertEquals(set(pie_keys), in_item.keys())

    @unittest.skip
    def test_can_get_equipment_status(self):
        ''' 测试gd_get_equipment_status接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']
        for major in majors:
            url = "/new_big/gongdian/gd_get_equipment_status?duan={}".format(
                major)
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data'] is not None:
                data = self.result["data"]
                keys_li = ["content", "contentIsHtml"]
                for key in keys_li:
                    self.assertIn(key, list(data.keys()))
            else:
                current_app.logger.debug(
                    "供电专业无gd_get_equipment_status?duan={}数据".format(major))

    def test_can_get_safe_produce_tendency(self):
        ''' 测试gd_safety_produce_tendency接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']
        quanxi_li = [
            "供电系统", "成都供电段", "重庆供电段", "贵阳供电段", "西昌供电段", "达州供电段", "贵阳北供电段",
            "重庆工电段"
        ]
        url_all = "/new_big/gongdian/gd_safety_produce_tendency?duan=all"
        response_all = self.client.get(url_all, )
        self.result_all = json.loads(response_all.data.decode())
        if self.result_all["data"] is not None:
            for data in self.result_all["data"]:
                keys_li1 = ["data", "title"]
                for key in keys_li1:
                    self.assertIn(key, list(data.keys()))
                self.assertIn(data["title"], quanxi_li)
                data1 = data["data"]
                keys_li2 = ["date", "事故（责任）", "其他", "故障", "合计"]
                for key in keys_li2:
                    self.assertIn(key, list(data1.keys()))

        for major in majors[1:]:
            url = f"/new_big/gongdian/gd_safety_produce_tendency?duan={major}"
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data'] is not None:
                keys_li3 = ["date", "事故（责任）", "其他", "故障", "合计"]
                data2 = self.result["data"]
                for key in keys_li3:
                    self.assertIn(key, list(data2.keys()))
        else:
            current_app.logger.debug("供电专业无gd_get_equipment_status无数据")

    def test_can_get_serious_safety_risk_tendency(self):
        ''' 测试gd_serious_safety_risk_tendency接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']
        keys_li = [
            "date", "专业管理缺陷", "从业人员伤害", "冒进、挤岔", "断杆断线",
            "行车设备事故故障", "超速、冲突、相撞"
        ]

        for major in majors:
            url = f"/new_big/gongdian/ \
                gd_serious_safety_risk_tendency?duan={major}"
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data']:
                data1 = self.result["data"]
                for key in keys_li:
                    self.assertIn(key, list(data1.keys()))
        else:
            current_app.logger.debug(
                "供电专业无gd_serious_safety_risk_tendency无数据")

    def test_can_get_safety_comprehensive_index(self):
        ''' 测试gd_safety_comprehensive_index接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']

        url_all = "/new_big/gongdian/gd_safety_comprehensive_index?duan=all"
        response_all = self.client.get(url_all, )
        self.result_all = json.loads(response_all.data.decode())
        if self.result_all["data"] is not None:
            self.assertEquals(3, len(self.result_all["data"]))
            keys_li = ["date", "tooltip"]
            for key in keys_li:
                self.assertIn(key, list(self.result_all["data"].keys()))

        for major in majors[1:]:
            url = f"/new_big/gongdian/\
                gd_safety_comprehensive_index?duan={major}"
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data'] is not None:
                for data_in in self.result["data"]:
                    keys_li3 = ["data", "title"]
                    for key in keys_li3:
                        self.assertIn(key, data_in)
        else:
            current_app.logger.debug(
                "供电专业无gd_safety_comprehensive_index无数据")

    def test_can_get_problem_classified_statistic(self):
        ''' 测试gd_get_problem_classified_statistic接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']

        quanxi_li = [
            "供电系统", "成都供电段", "重庆供电段", "贵阳供电段", "西昌供电段",
            "达州供电段", "贵阳北供电段", "重庆工电段"
        ]
        url_all = "/new_big/gongdian/ \
            gd_get_problem_classified_statistic?duan=all"
        key_li1 = ["data", "title"]
        key_li2 = ["data", "pivot"]
        key_li3 = ["amount", "name", "rank", "value"]
        response_all = self.client.get(url_all, )
        self.result_all = json.loads(response_all.data.decode())
        if self.result_all["data"] is not None:
            for data in self.result_all["data"]:
                for key in key_li1:
                    self.assertIn(key, list(data.keys()))
                self.assertIn(data["title"], quanxi_li)
                data1 = data["data"]
                for key in key_li2:
                    self.assertIn(key, list(data1.keys()))
                data2 = data1["data"]
                for data3 in data2:
                    for key in key_li3:
                        self.assertIn(key, list(data3.keys()))

        for major in majors[1:]:
            url = f"/new_big/gongdian/\
                gd_get_problem_classified_statistic?duan={major}"
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data'] is not None:

                data_duan = self.result["data"]
                for data_in in data_duan:
                    for key in key_li1:
                        self.assertIn(key, list(data_in.keys()))
                    data4 = data_in["data"]
                    for key in key_li2:
                        self.assertIn(key, list(data4.keys()))
                    data5 = data4["data"]
                    for data_in in data5:
                        for key in key_li3:
                            self.assertIn(key, list(data_in.keys()))

        else:
            current_app.logger.debug(
                "供电专业无gd_get_problem_classified_statistic无数据")

    def test_can_get_ganbu_luzhi_koufen1(self):
        ''' 测试gd_get_ganbu_luzhi2接口可以获取到正确的数据 '''
        majors = ['all', 'cdd', 'cqd', 'gyd', 'gyb', 'dzd', 'xcd', 'cqg']
        name_li1 = ["月", "季", "年"]
        key_li1 = ["data", "title"]
        key_li2 = ["data", "pivot"]
        key_li3 = ["amount", "name", "rank", "value"]
        for major in majors:
            url = "/new_big/gongdian/gd_get_ganbu_luzhi2?duan={}".format(major)
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if self.result['data'] is not None:
                for data in self.result["data"]:
                    for key in key_li1:
                        self.assertIn(key, list(data.keys()))
                    self.assertIn(data["title"], name_li1)
                    data7 = data["data"]
                    for data8 in data7:
                        for key in key_li1:
                            self.assertIn(key, list(data8.keys()))
                        data9 = data8["data"]
                        for key in key_li2:
                            self.assertIn(key, list(data9.keys()))
                        data10 = data9["data"]
                        for data11 in data10:
                            for key in key_li3:
                                self.assertIn(key, list(data11.keys()))
            else:
                current_app.logger.debug("供电专业无gd_get_ganbu_luzhi2无数据")


if __name__ == '__main__':
    unittest.main()
