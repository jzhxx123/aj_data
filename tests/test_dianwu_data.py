import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # current_app.logger.debug(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()


class NewBigThreeMajorApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # current_app.logger.debug(self.result)
        pass

    def test_can_get_correct_abc_duty_dw_data(self):
        '''测试abc_duty_dw接口可以获取到正确数据'''
        url = "/new_big/dianwu/abc_duty_dw"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['A', 'B', 'C']
            self.assertEqual(keys, list(self.result['data'].keys()))
        else:
            current_app.logger.debug("电务专业无abc_duty_dw接口出错，问题在mongo库")

    def test_can_get_correct_safe_produce_notice_data(self):
        ''' 测试安全警告接口可以获取到正确的数据 '''
        url = "/new_big/dianwu/safe_produce_notice_dw"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("电务专业无安全警告数据")

    def test_can_get_correct_auto_notice_data(self):
        ''' 测试自动提示接口可以获取到正确的数据 '''
        url = "/new_big/dianwu/auto_notice_dw"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("电务专业无自动提示数据")

    def test_can_get_correct_safe_notice_data(self):
        ''' 测试安全提示接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/safe_notice_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("电务专业无安全提示数据")

    def test_can_get_correct_accident_error_info_data(self):
        ''' 测试事故故障信息接口可以获取到正确的数据 '''
        url = "/new_big/dianwu/accident_error_info_dw"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
        else:
            current_app.logger.debug("电务专业无事故故障信息数据")

    def test_can_get_correct_evaluate_table_data(self):
        ''' 测试履职累计积分统计接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/dianwu/evaluate_table_dw", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result)
        self.assertEqual(7, len(self.result['data']['data']))
        total = 0
        for item in self.result['data']['data']:
            self.assertEqual(9, len(item))
            total += item[-1]
        self.assertEqual(total, self.result['data']['data'][6][-1] * 2)

    def test_can_get_corrent_evaluate_issue_data(self):
        ''' 测试履职问题分类统计接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/dianwu/evaluate_issue_dw")
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            # self.assertEqual(7, len(self.result['data']))　#数据不全面，长度不定
            types = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            in_keys = ['amount', 'name', 'rank', 'value']
            for item in self.result['data']:
                self.assertIn(item['title'], types)
                data = item['data']['data']
                for in_item in data:
                    for inkey in in_keys:
                        self.assertIn(inkey, list(in_item.keys()))
                    self.assertEqual(len(in_keys), len(list(in_item.keys())))
                self.assertIn('pivot', list(item['data'].keys()))
        else:
            current_app.logger.debug("履职问题分类接口无数据")

    def test_can_get_correct_important_work_analysis_data(self):
        ''' 测试important_work_analysis接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/dianwu/important_work_dw")
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result)
        if self.result['data']:
            out_keys = ['date', 'table', '人均细化指标计划', '人均细化指标完成', '人均完成质量分']
            for out_key in out_keys:
                self.assertIn(out_key, list(self.result['data'].keys()))
            self.assertEqual(
                len(out_keys), len(list(self.result['data'].keys())))
            lenth = len(self.result['data']['date'])
            for key, item in self.result['data'].items():
                if key != 'table':
                    self.assertEqual(lenth, len(item))
                else:
                    in_keys = ["人均细化指标计划", "人均细化指标完成", "人均完成质量分", "工作复查率"]
                    for key, in_item in item.items():
                        if key == "data":
                            self.assertIn(in_item[0][0], in_keys)
                            self.assertEqual(4, len(in_item))
                        else:
                            self.assertEqual(lenth, len(in_item))
                    # self.assertEqual(6,l)
        else:
            current_app.logger.debug(
                "当月无重点工作履职情况数据，检查T_SAFETY_ASSESS_MONTH_QUANTIFY_DETAIL")

    def test_can_get_problem_point_statistics_dianwu(self):
        ''' 测试问题项点接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/problem_sta_count_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            duan_name = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            keys_li = ["count", "上级检查问题个数", "自查问题个数"]
            for item in self.result['data']:
                self.assertIn(item["title"], duan_name)
                data = item["data"]
                for i, j in data.items():
                    self.assertIn(i, keys_li)

        else:
            current_app.logger.debug("电务专业无问题项点统计数据")

    def test_can_get_safety_produce_trendency_dianwu(self):
        ''' 测试safety_produce_trendency_dw接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/safety_produce_trendency_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ["date", "事故（责任）、故障件数"]   # "故障率$_2"
            duan_name = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段',
                '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            for item in self.result['data'][0]:
                self.assertIn(item["title"], duan_name)
                data_duan = item["data"]
                for key in keys:
                    self.assertIn(key, list(data_duan.keys()))
        else:
            current_app.logger.debug("电务专业无safety_produce_trendency_dw数据")

    @unittest.skip
    def test_can_get_height_class_safety_risk_case_dianwu(self):
        ''' 测试h_safety_risk_case_dw接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/h_safety_risk_case_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result["data"] is not None:
            keys = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            keys_li = ["title", "data"]
            key_li = ["date", "table", "发现问题数", "检查次数"]
            key_li1 = ["检查次数", "发现问题数", "问题发现率"]
            for item in self.result['data']:
                for key in keys_li:
                    self.assertIn(key, list(item.keys()))
                self.assertIn(item["title"], keys)
                data = item["data"]
                for key in key_li:
                    self.assertIn(key, list(data.keys()))
                table_data = data["table"]
                for key in keys_li:
                    self.assertIn(key, list(table_data.keys()))
                self.assertEqual(3, len(table_data["data"]))
                for item_data in table_data["data"]:
                    self.assertIn(item_data[0], key_li1)
        else:
            current_app.logger.debug("电务专业无h_safety_risk_case_dw数据")

    @unittest.skip
    def test_can_get_height_class_safety_risk_trendency_dianwu(self):
        ''' 测试h_safety_risk_trendency_dw接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/h_safety_risk_trendency_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            keys_li = ["title", "data"]
            key_li = ["date", "table", "发现问题数", "检查次数"]
            key_li1 = ["检查次数", "发现问题个数", "问题发现率"]
            for item in self.result['data']:
                for key in keys_li:
                    self.assertIn(key, list(item.keys()))
                self.assertIn(item["title"], keys)
                data = item["data"]

                for key in key_li:
                    self.assertIn(key, list(data.keys()))
                table_data = data["table"]
                for key in keys_li:
                    self.assertIn(key, list(table_data.keys()))
                self.assertEqual(3, len(table_data["data"]))
                for item_data in table_data["data"]:
                    self.assertEqual(len(data["date"]) + 1, len(item_data))
                    self.assertIn(item_data[0], key_li1)
        else:
            current_app.logger.debug("电务专业h_safety_risk_trendency_dw无数据")

    def test_can_get_height_class_safety_risk_serious_category_dianwu(self):
        ''' 测试h_safety_risk_serious_dw接口可以获取到正确的数据 '''
        api = "/new_big/dianwu/h_safety_risk_serious_dw"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())

        if self.result['data'] is not None:
            keys = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            keys_li = ["title", "data"]
            key_li = ["data", "pivot"]
            key_li1 = ["amount", "name", "rank", "value"]
            for item in self.result['data']:
                for key in keys_li:
                    self.assertIn(key, list(item.keys()))
                self.assertIn(item["title"], keys)
                data = item["data"]
                for key in key_li:
                    self.assertIn(key, list(data.keys()))
                table_data = data["data"]
                for i in table_data:
                    for key in key_li1:
                        self.assertIn(key, list(i.keys()))
        else:
            current_app.logger.debug("电务专业无h_safety_risk_serious_dw数据")

    def test_can_get_corrent_violation_situation_data(self):
        ''' 测试violation_situation_dw接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/dianwu/violation_situation_dw")
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            self.assertEqual(3, len(self.result['data']))
            types = ['月', '季度', '年']
            keys = ['count', '上级检查问题个数', '自查问题个数']
            for item in self.result['data']:
                self.assertIn(item['title'], types)
                data = item['data']
                self.assertEqual(set(keys), set(data.keys()))
                lenth = len(data['count'])
                self.assertEqual(lenth, len(data['上级检查问题个数']))
                self.assertEqual(lenth, len(data['自查问题个数']))
        else:
            current_app.logger.debug("电务两违接口无数据")

    def test_can_get_corrent_important_check_item_data(self):
        ''' 测试important_check_item接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/dianwu/important_check_item_dw")
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            self.assertEqual(7, len(self.result['data']))
            types = [
                '电务系统', '成都电务段', '重庆电务段', '贵阳电务段',
                '贵阳北电务段', '达州电务段', '成都通信段'
            ]
            in_keys = ['amount', 'name', 'rank', 'value']
            for item in self.result['data']:
                self.assertIn(item['title'], types)
                data = item['data']['data']
                for in_item in data:
                    for inkey in in_keys:
                        self.assertIn(inkey, list(in_item.keys()))
                    self.assertEqual(len(in_keys), len(list(in_item.keys())))
                self.assertIn('pivot', list(item['data'].keys()))


if __name__ == '__main__':
    unittest.main()
