import unittest
import warnings

from flask import Response

from app import create_app, utctl


class SysAPITestCase(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', category=ImportWarning)  # https://stackoverflow.com/a/52463661/4757521
        self.app = create_app('testing')
        self.utctl = utctl
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client()

    def tearDown(self):
        self.app_context.pop()

    def test_init(self):
        r: Response = self.client.post(
            '/sys/init/',
            data={
                'start_date': '20190225',
                'end_date': '20190324',
                # 'today': '20190326',
                'map_index': '10',
                'part': 'default',
                'func_index': '4',
                # 'risk':
            }
        )
        self.assertEqual(r.status_code, 200)
