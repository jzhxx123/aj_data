import unittest
import warnings

from app import create_app, utctl
from app.utils.data_update_models import InitSequenceHandler, UpdateMonth


class InitCommandTestCase(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', category=ImportWarning)  # https://stackoverflow.com/a/52463661/4757521
        self.app = create_app('testing')
        self.utctl = utctl
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def set_config(self, kwargs):
        self.app.config.update(kwargs)
        self.utctl.init_app(self.app)

    def test_update_all_sequences(self):
        self.set_config({
            'INIT_START_MONTH': '2018-10',
            'FAKE_TODAY': '2019-03',
        })
        past_months = self.utctl.get_init_month_list()
        past_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in past_months]
        a = InitSequenceHandler(past_month_objs)
        self.assertEqual(a.m_list, a.get_index_month_list('indexes'))
