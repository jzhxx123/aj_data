import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()


class NewBigThreeMajorApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        pass

    def test_can_get_correct_safe_produce_cl_1_data(self):
        '''测试safe_produce_cl_1接口可以获取到正确数据'''
        url = "/new_big/cheliang/safe_produce_cl_1"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'contentIsHtml']
            for item in self.result['data']:
                self.assertEqual(keys, list(item.keys()))
                in_keys = [
                    '安全生产天数：', '历史上的今天：', '本日安报信息：', '事故信息：',
                    '设备故障']
                for key in in_keys:
                    self.assertIn(key, item['content'])
        else:
            current_app.logger.debug("车辆专业safe_produce_cl_1接口出错")

    def test_can_get_correct_safe_notice_cl_1_data(self):
        ''' 测试safe_notice_cl_1接口可以获取到正确的数据 '''
        url = "/new_big/cheliang/safe_notice_cl_1"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', "tags", 'contentIsHtml']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
            # in_keys = ['集团公司预警：','专业预警']
            # for key in in_keys:
            #     self.assertIn(key ,self.result['data']['content'])
        else:
            current_app.logger.debug("车辆专业safe_notice_cl_1接口出错")

    # @unittest.skip
    def test_can_get_correct_important_work_cl_1_data(self):
        ''' 测试important_work_cl_1接口可以获取到正确的数据 '''
        url = "/new_big/cheliang/important_work_cl_1"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'contentIsHtml', 'date', 'tags']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("车辆专业important_work_cl_1接口出错")

    # @unittest.skip
    def test_can_get_correct_larger_danger_cl_1_data(self):
        ''' 测试larger_danger_cl_1接口可以获取到正确的数据 '''
        api = "/new_big/cheliang/larger_danger_cl_1"
        response = self.client.get(api, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content']
            for item in self.result['data']:
                self.assertEqual(keys, list(item.keys()))
        else:
            current_app.logger.debug("车辆专业无larger_danger_cl_1数据")

    def test_can_get_correct_high_quality_issue_cl_1_data(self):
        ''' 测试high_quality_issue_cl_1接口可以获取到正确的数据 '''
        urls = [
            "high_quality_issue_cl_1", "high_quality_issue_cl_2",
            "high_quality_issue_cl_3"
        ]
        for in_url in urls:
            url = "/new_big/cheliang/" + in_url
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if len(self.result['data']) != 0:
                len_trendency = len(self.result['data'][0]['date'])

                for level in self.result['data'][0].keys():
                    self.assertEqual(len_trendency,
                                     len(self.result['data'][0][level]))
                keys = ["amount", "name", "rank", "value"]
                for item in self.result['data'][1]['data']:
                    self.assertEqual(set(keys), set(item.keys()))
            else:
                current_app.logger.debug("车辆高质量问题分析无今年数据")

    # @unittest.skip
    def test_can_get_correct_cadre_evluate_cl_1_data(self):
        ''' 测试cadre_evluate_cl_1接口可以获取到正确的数据 '''
        urls = [
            "cadre_evluate_cl_1", "cadre_evluate_cl_2", "cadre_evluate_cl_3"
        ]
        for in_url in urls:
            url = "/new_big/cheliang/" + in_url
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            self.assertIsNotNone(self.result['data'])
            if in_url == 'cadre_evluate_cl_1':
                lenth = len(self.result['data']['date'])
                levels = ['全年', '当月']
                for level in levels:
                    self.assertEqual(lenth, len(self.result['data'][level]))
            else:
                lenth_0 = len(self.result['data'][1]['date'])
                levels = ['全年', '当月']
                for level in levels:
                    self.assertEqual(lenth_0,
                                     len(self.result['data'][1][level]))
                lenth_1 = len(self.result['data'][1]['date'])
                for key, value in self.result['data'][0].items():
                    self.assertEqual(lenth_1, len(value))

    # @unittest.skip
    def test_can_get_corrent_evaluate_issue_data(self):
        ''' 测试预警联动接口可以获取到正确的数据 '''
        urls = ["notice_link_cl_2", "notice_link_cl_3"]
        for in_url in urls:
            url = "/new_big/cheliang/" + in_url
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if len(self.result['data']) != 0:
                if in_url == 'notice_link_cl_2':
                    keys = ['rank', 'table']
                    self.assertEquals(
                        set(keys), set(self.result['data'][0].keys()))
                    rank_keys = ['amount', 'name', 'rank']
                    for item in self.result['data'][0]['rank']:
                        self.assertEquals(set(rank_keys), set(item.keys()))
                    self.assertEquals(3, len(self.result['data'][0]['table']))
                    for item in self.result['data'][0]['table']:
                        self.assertEquals(3, len(item))
                    pie_keys = ['amount', 'name', 'rank', 'value']
                    for item in self.result['data'][1]['data']:
                        self.assertEquals(set(pie_keys), item.keys())
            else:
                current_app.logger.debug("车辆预警联动接口无数据")

    # @unittest.skip
    def test_can_get_corrent_risk_tendency_data(self):
        ''' 测试主要风险走势图接口可以获取到正确的数据 '''
        urls = ["risk_tendency_cl_2", "risk_tendency_cl_3"]
        for in_url in urls:
            url = "/new_big/cheliang/" + in_url
            response = self.client.get(url, )
            self.result = json.loads(response.data.decode())
            if in_url == 'risk_tendency_cl_2':
                keys = ['动车', '客车']
                for item in self.result['data']:
                    self.assertIn(item['title'], keys)
                    self.assertEqual(2, len(item))
                    lenth = len(item['data'][0]['date'])
                    for key, value in item['data'][0].items():
                        self.assertEqual(lenth, len(value))
                    pie_keys = ['amount', 'name', 'rank', 'value']
                    for in_item in item['data'][1]['data']:
                        self.assertEquals(set(pie_keys), in_item.keys())

    def test_can_get_safe_risk_info_tendency(self):
        ''' 测试 safe_risk_info_cl_1接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/cheliang/safe_risk_info_cl_1", )
        self.result = json.loads(response.data.decode())
        if self.result["data"] is not None:
            data = self.result["data"]
            keys_li = ["动车", "客车", "货车", "劳安"]
            keys_li1 = ["data", "title", "data_tags", "table"]
            key_li4 = ["date", "检查次数", "检查问题"]
            keys_li2 = [
                "date", "成都动车段", "贵阳车辆段", "重庆车辆段", "成都车辆段", "重庆西车辆段", "成都北车辆段",
                "贵阳南车辆段"
            ]
            self.assertEquals(4, len(data))
            for data1 in data[:3]:
                for key in keys_li1:
                    self.assertIn(key, list(data1.keys()))
                self.assertIn(data1["title"], keys_li)
                data2 = data1["data"]
                self.assertEquals(3, len(data2))
                for data_in in data2:
                    for key in list(data_in.keys()):
                        self.assertIn(key, keys_li2)
            data4 = data[3]
            self.assertIn(data4["title"], keys_li)
            for key in key_li4:
                self.assertIn(key, list(data4["data"][0].keys()))
        else:
            current_app.logger.debug("车辆专业无safe_risk_info_cl_1数据")

    def test_can_get_safe_risk_problem_tendency(self):
        ''' 测试safe_risk_problem_cl_1接口可以获取到正确的数据 '''
        response = self.client.get(
            "/new_big/cheliang/safe_risk_problem_cl_1", )
        self.result = json.loads(response.data.decode())
        if self.result["data"] is not None:
            data = self.result["data"]
            keys_li = ["动车", "客车", "货车", "劳安"]
            keys_li1 = ["data", "title", "data_tags", "table"]
            key_li4 = ["data", "pivot"]
            keys_li5 = ["rank", "name", "value", "amount"]
            keys_li2 = [
                "date", "成都动车段", "贵阳车辆段", "重庆车辆段", "成都车辆段", "重庆西车辆段", "成都北车辆段",
                "贵阳南车辆段"
            ]
            self.assertEquals(4, len(data))
            for data1 in data[:3]:
                for key in keys_li1:
                    self.assertIn(key, list(data1.keys()))
                self.assertIn(data1["title"], keys_li)
                data2 = data1["data"]
                self.assertEquals(3, len(data2))
                for data_in in data2:
                    for key in list(data_in.keys()):
                        self.assertIn(key, keys_li2)
            data4 = data[3]
            self.assertIn(data4["title"], keys_li)
            for key in key_li4:
                self.assertIn(key, list(data4["data"][0].keys()))
            data5 = data4["data"][0]["data"]
            for li in data5:
                for key in keys_li5:
                    self.assertIn(key, list(li.keys()))
        else:
            current_app.logger.debug("车辆专业无safe_risk_problem_cl_1数据")


if __name__ == '__main__':
    unittest.main()
