import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # print(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()


class NewBigGongWuApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # print(self.result)
        pass

    def test_can_get_correct_get_safe_days_data(self):
        '''测试安全天数接口可以获取到正确数据'''
        url = "/new_big/gongwu/gw_safe_days"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['B']
            self.assertEqual(set(keys), set(self.result['data'].keys()))
        else:
            current_app.logger.debug("工务专业安全天数接口出错")

    def test_can_get_correct_gw_safe_warning_data(self):
        '''测试安全预警接口可以获取到正确数据'''
        url = "/new_big/gongwu/gw_safe_warning"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("工务专业安全预警接口出错")

    def test_can_get_correct_gw_safe_notice_data(self):
        ''' 测试安全提示接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_safe_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("工务专业安全提示接口出错")

    def test_can_get_correct_gw_get_safe_warning_notice_data(self):
        ''' 测试安全警示接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_get_safe_warning_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("工务专业安全警示接口出错")

    def test_can_get_correct_gw_safe_info_data(self):
        ''' 测试安全信息接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_safe_info"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("工务专业安全信息接口出错")

    def test_can_get_correct_gw_problem_point(self):
        ''' 测试问题项点接口可以获取到正确的数据 '''
        url_all = "/new_big/gongwu/gw_problem_point"
        response_all = self.client.get(url_all, )
        self.result_all = json.loads(response_all.data.decode())
        if len(self.result_all["data"]) != 0:
            for item in self.result_all["data"]['data']:
                keys = ["amount", "name", "rank", "value"]
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("工务专业问题项点无数据")

    def test_can_get_correct_important_work_data(self):
        ''' 测试重点施工接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_important_work"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            lenth = len(self.result['data']['date'])
            for key, value in self.result['data'].items():
                self.assertEqual(lenth, len(value))
        else:
            current_app.logger.debug("工务专业无重点施工无数据")

    def test_can_get_correct_gw_important_work_data(self):
        ''' 测试安全信息趋势图接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_safe_produce_tendency"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            lenth = len(self.result['data']['date'])
            for key, value in self.result['data'].items():
                self.assertEqual(lenth, len(value))
        else:
            current_app.logger.debug("工务专业安全生产信息无数据")

    def test_can_get_correct_gw_serious_injury_equipment_data(self):
        ''' 测试重伤设备统计接口可以获取到正确的数据 '''
        url = "/new_big/gongwu/gw_serious_injury_equipment"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            lenth = len(self.result['data']['date'])
            for key, value in self.result['data'].items():
                self.assertEqual(lenth, len(value))
        else:
            current_app.logger.debug("工务专业重伤设备统计无数据")


if __name__ == '__main__':
    unittest.main()
