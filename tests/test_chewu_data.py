import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()

        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # current_app.logger.debug(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()
        # self.db.close()
        pass


class NewBigThreeMajorApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # current_app.logger.debug(self.result)
        pass

    def test_can_get_correct_safe_produce_notice_data(self):
        ''' 测试safe_produce_notice接口可以获取到正确的数据 '''
        url = "/new_big/chewu/safe_produce_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("车辆专业无安全预警数据")

    def test_can_get_correct_auto_notice_data(self):
        ''' 测试auto_notice接口可以获取到正确的数据 '''
        url = "/new_big/chewu/auto_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("车辆专业无自动提示数据")

    def test_can_get_correct_safe_notice_data(self):
        ''' 测试safe_notice接口可以获取到正确的数据 '''
        url = "/new_big/chewu/safe_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content', 'date', 'tags']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
                self.assertEqual(len(keys), len(list(item.keys())))
        else:
            current_app.logger.debug("车辆专业无安全提示数据")

    def test_can_get_correct_accident_error_info_data(self):
        ''' 测试accident_error_info接口可以获取到正确的数据 '''
        url = "/new_big/chewu/accident_error_info"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
        else:
            current_app.logger.debug("车辆专业无安全事故、故障数据")

    def test_can_get_correct_stress_issue_data(self):
        ''' 测试stress_issue接口可以获取到正确的数据 '''
        url = "/new_big/chewu/stress_issue"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['content']
            for item in self.result['data']:
                for key in keys:
                    self.assertIn(key, list(item.keys()))
        else:
            current_app.logger.debug("车辆专业无突出问题数据")

    def test_can_get_correct_safe_top_title_data(self):
        ''' 测试safe_top_title接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/chewu/safe_top_title", )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            for item in self.result['data']:
                self.assertIn('department', item.keys())
            self.assertEqual(3, len(self.result['data']))
        else:
            current_app.logger.debug("车辆专业顶部横栏接口出错")

    # 安全健康指数排名类型接口测试
    # @unittest.skip
    def test_can_get_correct_safe_health_index_data(self):
        ''' 测试safe_health_index接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/chewu/safe_health_index", )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['amount', 'name', 'rank']
            for item in self.result['data']:
                for in_item in item:
                    for key in keys:
                        self.assertIn(key, list(in_item.keys()))
                    self.assertEqual(len(keys), len(list(in_item.keys())))
        else:
            current_app.logger.debug("车辆专业无站段综合健康指数")

    def test_can_get_correct_high_quality_analysis_data(self):
        ''' 测试high_quality_analysis接口可以获取到正确的数据 '''
        response = self.client.get(
            "/new_big/chewu/high_quality_analysis", )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            lenth = 5
            table = self.result['data'][0]
            pie = self.result['data'][1]
            if table:
                for item in table:
                    for in_item in item['data']:
                        self.assertEqual(lenth, len(in_item))
            keys = ['name', 'amount', 'value', 'rank']
            if pie:
                for item in pie['data']:
                    self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("车务高质量问题分析无数据")

    def test_can_get_correct_stress_risk_monitor_data(self):
        ''' 测试stress_risk_monitor接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/chewu/stress_risk_monitor", )
        self.result = json.loads(response.data.decode())
        lenth = len(self.result['data']['date'])
        for key, value in self.result['data'].items():
            self.assertEqual(lenth, len(value))

    def test_can_get_correct_evaluate_issue_pie_data(self):
        '''测试evaluate_issue_pie接口可以获取到正确数据'''
        response = self.client.get("/new_big/chewu/evaluate_issue_pie", )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            self.assertEqual(2, len(self.result['data']))
            types = ['按问题统计', '按扣分统计']
            in_keys = ['amount', 'name', 'rank', 'value']
            for item in self.result['data']:
                self.assertIn(item['title'], types)
                data = item['data']['data']
                for in_item in data:
                    for inkey in in_keys:
                        self.assertIn(inkey, list(in_item.keys()))
                    self.assertEqual(len(in_keys), len(list(in_item.keys())))
                self.assertIn('pivot', list(item['data'].keys()))
        else:
            current_app.logger.debug("车务履职问题饼图无数据")

    def test_can_get_corrent_safe_days_data(self):
        '''测试可以得到站段安全天数接口'''
        response = self.client.get("/new_big/chewu/safe_days", )
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            self.assertEqual(type(self.result['data']['B']), type(1))
        else:
            current_app.logger.debug("车务站段安全天数接口出错")

    def test_can_get_corrent_analysis_check_data(self):
        '''测试可以得到站段安全天数接口'''
        response = self.client.get("/new_big/chewu/analysis_check", )
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            self.assertIn('content', list(self.result['data'][0].keys()))
        else:
            current_app.logger.debug("车务站段监督检查统计接口出错")


if __name__ == '__main__':
    unittest.main()
