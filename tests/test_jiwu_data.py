import json
import os
import sys
import unittest

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        self.app_context = app.app_context()
        self.app_context.push()
        # delta = app.config.get('MONTH_DELTA')
        # concurrent_update(delta)
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # current_app.logger.debug(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        self.app_context.pop()


class NewBigJiWuApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # current_app.logger.debug(self.result)
        pass

    def test_can_get_correct_get_jw_safe_days_data(self):
        '''测试安全天数接口可以获取到正确数据'''
        url = "/new_big/jiwu/jw_safe_days"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['A']
            stations = [
                '机务系统', '成都机务段',
                '西昌机务段', '重庆机务段', '贵阳机务段'
            ]
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item['data'].keys()))
                self.assertIn(item['title'], stations)
        else:
            current_app.logger.debug("机务专业安全天数接口出错")

    def test_can_get_correct_jw_notification_data(self):
        '''测试安全预警接口可以获取到正确数据'''
        url = "/new_big/jiwu/jw_notification"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("机务专业通知接口出错")

    def test_can_get_correct_jw_risk_notice_data(self):
        ''' 测试风险预警接口可以获取到正确的数据 '''
        url = "/new_big/jiwu/jw_risk_notice"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['content', 'tags', 'date']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("机务专业风险预警接口出错")

    def test_can_get_correct_jw_risk_check_data(self):
        ''' 测试风险防控监督检查接口可以获取到正确的数据 '''
        url = "/new_big/jiwu/jw_risk_check"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            for data in self.result['data']:
                lenth = len(data['data']['line']['date'])
                for key, value in data['data']['line'].items():
                    self.assertEqual(lenth, len(value))
                self.assertEqual(lenth, len(data['data']['tooltip']))
        else:
            current_app.logger.debug("机务专业监督检查接口出错")

    def test_can_get_correct_gw_important_work_data(self):
        ''' 测试未再次发生问题项点接口可以获取到正确的数据 '''
        url = "/new_big/jiwu/jw_not_again_problem"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['name', 'amount', 'rank']
            for item in self.result['data']:
                for key in item['data']:
                    self.assertEqual(set(keys), set(key.keys()))
        else:
            current_app.logger.debug("机务专业未再次发生项点问题无数据")

    def test_can_get_correct_jw_cadre_evaluate_data(self):
        ''' 测试干部履职积分统计接口可以获取到正确的数据 '''
        url = "/new_big/jiwu/jw_cadre_evaluate"
        response = self.client.get(url, )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ["amount", "name", "rank", "value"]
            for item in self.result["data"]:
                for key in item['data']['data']:
                    self.assertEqual(set(keys), set(key.keys()))
        else:
            current_app.logger.debug("机务专业干部积分统计无数据")


if __name__ == '__main__':
    unittest.main()
