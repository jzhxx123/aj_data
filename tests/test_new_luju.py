import json
import os
import sys
import unittest
from datetime import datetime as dt

from flask import current_app

from app import create_app

curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)


class AppTestCase(unittest.TestCase):
    @classmethod
    def setUpClass(self):
        app = create_app("testing")
        # app.testing = True
        self.client = app.test_client()
        # load_dotenv(find_dotenv())
        # mongo_uri = os.environ.get('MONGO_URI')
        # print(mongo_uri)
        # self.db = init_db(uri=mongo_uri)
        # make_data_monthly_detail_safety_produce_info(self.db)

    @classmethod
    def tearDownClass(self):
        # self.db.close()
        pass


class NewBigThreeAllApiTest(AppTestCase):
    def setUp(self):
        self.result = ""

    def tearDown(self):
        # print(self.result)
        pass

    def test_can_get_correct_safe_produce_notice_data(self):
        ''' 测试接口可以获取到正确的数据 '''
        response = self.client.get(
            "/new_big/safe_produce_notice",
        )
        self.result = json.loads(response.data.decode())
        keys = ['content', 'date', 'tags']
        if self.result['data']:
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
        else:
            current_app.logger.debug("路局安全生产信息接口无数据")

    def test_can_get_correct_a_duty_data(self):
        ''' 测试a_duty接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/a_duty", )
        self.result = json.loads(response.data.decode())
        days = self.result['data']['safe_produce_duration']
        self.assertNotEqual(0, days)

    def test_can_get_correct_safe_produce_data(self):
        ''' 测试safe_produce接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_produce", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['date', '劳安', '行车', '路外', '故障']
        self.assertEqual(set(keys), set(self.result['data'].keys()))
        length = dt.today().month
        for key in keys:
            self.assertEqual(length, len(self.result['data'][key]))

    def test_can_get_correct_safe_duty_produce_data(self):
        ''' 测试safe_duty_produce接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_duty_produce", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['CL', 'CW', 'DW', 'GD', 'GW', 'jw', 'title']
        self.assertEqual(keys, list(self.result['data'].keys()))
        for key in keys:
            self.assertIsNotNone(self.result['data'][key])

    def test_can_get_correct_risk_distribute_data(self):
        ''' 测试risk_distribute接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/risk_distribute", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        lenth = 10
        self.assertEqual(lenth, len(self.result['data']))
        for rs in self.result['data']:
            self.assertIsNotNone(rs['name'])
            self.assertIsNotNone(rs['amount'])
            self.assertIsNotNone(rs['rank'])

    def test_can_get_correct_typical_accident_data(self):
        ''' 测试typical_accident接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/typical_accident", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['content', 'reason']
        self.assertEquals(keys, list(self.result['data'][0].keys()))

    def test_can_get_correct_safe_produce_reason_data(self):
        ''' 测试safe_produce_reason接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_produce_reason", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['amount', 'name', 'rank', 'value']
        self.assertEqual(10, len(self.result['data']['data']))
        for item in self.result['data']['data']:
            self.assertIsNotNone(item)
            self.assertEquals(keys, list(item.keys()))

    def test_can_get_correct_inspect_check_data(self):
        ''' 测试inspect_check接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/inspect_check", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['date', '发现问题数', '检查人次', '责任事故', '问题质量']
        self.assertEquals(keys, list(self.result['data'].keys()))
        length = dt.today().month
        for key in keys:
            self.assertEqual(length, len(self.result['data'][key]))

    def test_can_get_correct_evaluate_table_data(self):
        ''' 测试evaluate_table接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/evaluate_table", )
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            self.assertEqual(7, len(self.result['data']['data']))
            total = 0
            for item in self.result['data']['data']:
                self.assertEqual(9, len(item))
                total += item[-1]
            self.assertEqual(total, self.result['data']['data'][6][-1] * 2)
        else:
            current_app.logger.debug("路局履职表格无数据")

    def test_can_get_correct_stress_issue_data(self):
        ''' 测试stress_issue接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/stress_issue", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['content', 'date', 'tags']
        for item in self.result['data']:
            self.assertEqual(keys, list(item.keys()))

    def test_can_get_correct_evaluate_produce_data(self):
        ''' 测试evaluate_produce接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/evaluate_produce", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        lenth = dt.today().month
        self.assertEqual(lenth, len(self.result['data']['date']))
        keys = ['SCORE', 'TIMES']
        for key in keys:
            for item in self.result['data'][key]:
                self.assertEqual(lenth, len(list(item.values())[0]))

    def test_can_get_correct_evaluate_issue_data(self):
        ''' 测试evaluate_issue接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/evaluate_issue", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        self.assertEqual(11, len(self.result['data']['data']))
        keys = ['amount', 'name', 'rank', 'value']
        for item in self.result['data']['data']:
            self.assertEqual(keys, list(item.keys()))

    def test_can_get_correct_check_poor_data(self):
        ''' 测试check_poor接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/check_poor", )
        self.result = json.loads(response.data.decode())
        if len(self.result['data']) != 0:
            keys = ['amount', 'name', 'rank']
            lenth = 10
            self.assertEqual(lenth, len(self.result['data']))
            for item in self.result['data']:
                self.assertEqual(keys, list(item.keys()))
        else:
            current_app.logger.debug("检查薄弱接口无数据")

    def test_can_get_correct_safe_manage_poor_data(self):
        ''' 测试safe_manage_poor接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_manage_poor", )
        self.result = json.loads(response.data.decode())
        if self.result['data'] is not None:
            keys = ['供电', '工务', '机务', '电务', '车务', '车辆']
            self.assertEqual(len(keys), len(self.result['data']))
            for item in self.result['data']:
                self.assertIn(list(item.keys())[0], keys)
                in_keys = ['NAME', 'SCORE']
                for in_item in item.get(list(item.keys())[0]):
                    self.assertEqual(set(in_keys), set(in_item.keys()))
        else:
            current_app.logger.debug("管理薄弱接口无数据")

    def test_can_get_correct_risk_possible_data(self):
        ''' 测试risk_possible接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/risk_possible", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = list(self.result['data'].keys())
        lenth = dt.today().month
        self.assertEqual(lenth, len(self.result['data']['date']))
        keys.remove('date')
        for key in keys:
            items = self.result['data'][key]
            for item in items:
                self.assertEqual(lenth, len(list(item.values())[0]))

    def test_can_get_correct_serious_risk_data(self):
        ''' 测试serious_risk接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/serious_risk", )
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            keys = ['data', 'title']
            for item in self.result['data']:
                self.assertEqual(set(keys), set(item.keys()))
                lenth = len(item['data']['date'])
                for key, value in item['data'].items():
                    self.assertEqual(lenth, len(value))

    def test_can_get_correct_safe_warnning_data(self):
        ''' 测试safe_warnning接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_warnning", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['content', 'date', 'tags']
        for item in self.result['data']:
            for key in list(item.keys()):
                self.assertIn(key, keys)

    def test_can_get_correct_safe_notice_data(self):
        ''' 测试safe_notice接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/safe_notice", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['content', 'date', 'tags']
        for item in self.result['data']:
            self.assertEqual(keys, list(item.keys()))

    def test_can_get_correct_auto_notice_data(self):
        ''' 测试auto_notice接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/auto_notice", )
        self.result = json.loads(response.data.decode())
        self.assertIsNotNone(self.result['data'])
        keys = ['content', 'date', 'tags']
        for item in self.result['data']:
            self.assertEqual(keys, list(item.keys()))

    def test_can_get_correct_duty_accident_error_data(self):
        ''' 测试duty_accident_error接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/duty_accident_error", )
        self.result = json.loads(response.data.decode())
        keys = list(self.result['data'].keys())
        lenth = dt.today().month
        self.assertEqual(lenth, len(list(self.result['data']['date'])))
        keys.remove('date')
        for key in keys:
            values = list(self.result['data'][key].values())
            for value in values:
                self.assertEqual(lenth, len(value))

    def test_can_get_correct_key_people_data(self):
        ''' 测试重点人员统计接口可以获取到正确的数据 '''
        response = self.client.get("/new_big/key_people", )
        self.result = json.loads(response.data.decode())
        if self.result['data']:
            self.assertEqual(7, len(self.result['data']['data']))
            total = 0
            for item in self.result['data']['data']:
                self.assertEqual(6, len(item))
                total += item[-1]
            self.assertEqual(total, self.result['data']['data'][6][-1] * 2)
        else:
            current_app.logger.debug("路局重点人员表格无数据")


if __name__ == '__main__':
    unittest.main()
