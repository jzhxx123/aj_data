import itertools
import unittest
import warnings
from datetime import date, datetime

from dateutil import parser, relativedelta

from app import create_app, utctl
from app.data.safety_produce_info import execute as spi_e
from app.utils.data_update_models import InitSequenceHandler, UpdateMonth, UpdateTimeController


class UpdateTimerControllerTestCase(unittest.TestCase):
    def setUp(self):
        warnings.simplefilter('ignore', category=ImportWarning)  # https://stackoverflow.com/a/52463661/4757521
        self.app = create_app('testing')
        self.utctl = utctl
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def set_config(self, kwargs):
        self.app.config.update(kwargs)
        return UpdateTimeController(app=self.app)

    def test_attr_fake_today(self):
        fake_today = '2019-03-04'
        self.app.config.update({
            'FAKE_TODAY': fake_today
        })
        _utctl = UpdateTimeController(app=self.app)
        self.assertEqual(datetime.strptime(fake_today, '%Y-%m-%d').date(), _utctl.get_today())

    def test_attr_init_start_month(self):
        init_start_month = '2018-10'
        _utctl = self.set_config({
            'INIT_START_MONTH': init_start_month
        })
        # 应返回上月的更新日
        self.assertEqual(_utctl.init_start_date, parser.parse(init_start_month).date().replace(
            day=self.utctl.update_day) + relativedelta.relativedelta(months=-1))

    def test_is_update_last_month(self):
        for day in itertools.chain(range(25, 32), range(1, 2)):
            self.utctl.today = date.today().replace(day=day)
            self.assertTrue(self.utctl.is_update_last_month())
        for day in (2, 13, 24):
            self.utctl.today = date.today().replace(day=day)
            self.assertFalse(self.utctl.is_update_last_month())

    def test_get_daily_update_month_list(self):
        _utctl = self.set_config({
            'FAKE_TODAY': '2019-03-25'
        })
        self.assertEqual(len(_utctl.get_daily_update_month_list()), 2)
        _utctl = self.set_config({
            'FAKE_TODAY': '2019-03-28'
        })
        self.assertEqual(len(_utctl.get_daily_update_month_list()), 2)
        _utctl = self.set_config({
            'FAKE_TODAY': '2019-04-01'
        })
        self.assertEqual(len(_utctl.get_daily_update_month_list()), 2)
        _utctl = self.set_config({
            'FAKE_TODAY': '2019-03-24'
        })
        self.assertEqual(len(_utctl.get_daily_update_month_list()), 1)
        _utctl = self.set_config({
            'FAKE_TODAY': '2019-04-02'
        })
        self.assertEqual(len(_utctl.get_daily_update_month_list()), 1)

    def test_get_init_month_list(self):
        _utctl = self.set_config({
            'INIT_START_MONTH': '2018-10',
            'FAKE_TODAY': '2019-03',
        })
        month_num = relativedelta.relativedelta(_utctl.update_date, _utctl.init_start_date).months + 1
        self.assertEqual(len(list(_utctl.get_init_month_list())), month_num)


class UpdateMonthTestCase(unittest.TestCase):

    def setUp(self):
        warnings.simplefilter('ignore', category=ImportWarning)  # https://stackoverflow.com/a/52463661/4757521
        self.app = create_app('testing')
        self.utctl = utctl
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    @staticmethod
    def parse_date(*args: str):
        return [parser.parse(s).date() for s in args]

    def test_get_month_delta_in_daily(self):
        str_dates = (
            '20190225',
            '20190319',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        delta = um.get_month_delta()
        self.assertEqual(delta, 0)

    def test_get_month_delta_in_monthly(self):
        str_dates = (
            '20190125',
            '20190224',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        delta = um.get_month_delta()
        self.assertIn(delta, range(1, 13))

    def test_get_month_delta_in_year(self):
        str_dates = (
            '20180125',
            '20180224',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        delta = um.get_month_delta()
        self.assertGreater(delta, 12)

    def test_get_col_prefix(self):
        str_dates = (
            '20190225',
            '20190319',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertEqual(um.get_col_prefix(), 'daily_')
        str_dates = (
            '20180225',
            '20180319',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertEqual(um.get_col_prefix(), 'monthly_')
        str_dates = (
            '20180125',
            '20180224',
            '20190319',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertEqual(um.get_col_prefix(), 'history_')

    def test_execute(self):
        str_dates = (
            '20190225',
            '20190324',
            '20190326',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertEqual(spi_e(um), 'OK')

    def test_is_update_day(self):
        str_dates = (
            '20181125',
            '20181224',
            '20190321',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertFalse(um.is_update_day())
        str_dates = (
            '20190225',
            '20190324',
            '20190325',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertTrue(um.is_update_day())
        str_dates = (
            '20190225',
            '20190324',
            '20190401',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertTrue(um.is_update_day())
        str_dates = (
            '20190325',
            '20190401',
            '20190401',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertFalse(um.is_update_day())
        str_dates = (
            '20190225',
            '20190324',
            '20190327',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertTrue(um.is_update_day())
        str_dates = (
            '20190225',
            '20190324',
            '20190402',
        )
        um = UpdateMonth(*self.parse_date(*str_dates))
        self.assertFalse(um.is_update_day())


class InitSequenceHandleTestCase(unittest.TestCase):

    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()

    def tearDown(self):
        self.app_context.pop()

    def set_config(self, kwargs):
        self.app.config.update(kwargs)
        return UpdateTimeController(app=self.app)

    def create_handler(self):
        _utctl = self.set_config({
            'INIT_START_MONTH': '2018-10',
            'FAKE_TODAY': '2019-03',
        })
        past_months = utctl.get_init_month_list()
        past_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in past_months]
        return InitSequenceHandler(past_month_objs)

    def test_generate_seq(self):
        h = self.create_handler()
        seq = h.generate_seq()
        self.assertIsInstance(seq, list)
        for x in seq:
            self.assertIn('index', x)
            self.assertIn('months', x)

    def test_sort(self):
        handler = self.create_handler()
        self.assertEqual(handler.sort()[-1].get('index'), 999)
