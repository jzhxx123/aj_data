.PHONY: .ONESHELL venv start

.ONESHELL:

SHELL=/bin/bash
.DEFAULT_GOAL := start

### git
gl:
	git stash
	git pull -r
	git stash pop
# 查看最新tag
gtl:
	git tag -l --sort=-v:refname

# for dev
d%: export COMPOSE_PROJECT_NAME = dv-be-api_dev
d%: export COMPOSE_FILE := docker-compose.dev.yml
i%: export COMPOSE_FILE = docker-compose.yml:docker-compose.init.yml
di%: export COMPOSE_FILE = docker-compose.dev.yml:docker-compose.init.dev.yml

### pipenv

venv-all: venv pid

venv:
	pip3 install pipenv --user -i https://pypi.douban.com/simple
	pipenv shell
venv-clean:
	pipenv --rm
	pip uninstall pipenv -y

pip:
	pip install -r requirements.txt
pipd:
	pip install -r requirements-dev.txt

plr:
	pipenv lock -r > requirements.txt
plrd:
	pipenv lock -rd > requirements-dev.txt

pi:
	pipenv install
pid:
	pipenv install -d
pu:
	pipenv update
pud:
	pipenv update -d

### common

clean-image:
	docker-compose down --rmi local

prune:
	docker image prune -f --filter until=240h

### deploy

clean-volume:
	docker-compose down -v

CMD=-h

test:
	docker-compose ${CMD}
stop:
	docker-compose down
build:
	docker-compose build
up:
	docker-compose up -d
up-%:
	docker-compose up -d --no-dep $*
start: build stop up

iup-%: up-$@
istart: build stop clean-volume up
istop: stop

### dev

dit: test
dbuild: build
dup: up
dstart: build stop up
dstop: distop

distart: build distop clean-volume up
distop: stop
diup-%:
	docker-compose up -d --no-dep $*


tbuild:
	docker build -t dv-be-api:test .
ttag:
	docker tag dv-be-api:test localhost:5000/dv-be-api:test
tpush:
	docker push localhost:5000/dv-be-api:test
tstart: tbuild ttag tpush
