#@IgnoreInspection BashAddShebang
### Common
TZ=Asia/Shanghai

### honeycomb OAuth2

APPID=
APPSECRET=

### flask

FLASK_APP=manage.py
FLASK_ENV=production
#FLASK_ENV=testing
#FLASK_ENV=development
FLASK_RUN_HOST=0.0.0.0
FLASK_RUN_PORT=5000
# 初始化月份数量
MONTH_DELTA=
# 设定当前日期
FAKE_TODAY=
# 指数计算(true)
IS_INIT_INDEX=

# 初始化开始月份
INIT_START_MONTH=
# 初始化并发进程数量
INIT_MAX_WORKERS=

### DB URI

# SQLALCHEMY_DATABASE_URI='{dialect}+{driver}://{user}:{pwd}@{host}:{port}/{db}?charset=utf8mb4'
# MONGO_URI='mongodb://{user}:{pwd}@{host}:{port}/{db}'
# REDIS_URL='redis://{host}:{port}/{db}'
# CACHE_REDIS_URL='redis://{host}:{port}/{db}'
SQLALCHEMY_DATABASE_URI=mysql+pymysql://root:heheda@mysql/new_system?charset=utf8mb4
SQLALCHEMY_DATABASE_URI_MID=mysql+pymysql://root:heheda@mysql/new_system?charset=utf8mb4
MONGO_URI=mongodb://admin:123@mongo/railway_map
REDIS_URL=redis://redis/0
CACHE_REDIS_URL=redis://redis/0

### Mongo

MONGO_INITDB_ROOT_USERNAME=root
MONGO_INITDB_ROOT_PASSWORD=123
MONGO_INITDB_DATABASE=railway_map
MONGO_DATA_DIR=/data/db
MONGO_LOG_DIR=/data/db/log

### gunicorn config

GUNICORN_BIND=0.0.0.0:9000
GUNICORN_WORKER_CLASS=gevent
#GUNICORN_WORKERS=$(($(nproc) * 2 + 1))
#GUNICORN_THREADS=$(($(nproc) * 2 + 1))
GUNICORN_TIMEOUT=3000
GUNICORN_GRACEFUL_TIMEOUT=100
GUNICORN_LOGLEVEL=info
GUNICORN_ERRORLOG=-
GUNICORN_ACCESSLOG=-
