#!/bin/sh

printenv >> /etc/environment

if [ "$1" = 'sh' ]; then
    exec "$@"
fi

# 是否初始化
if [ "$INIT" = 'true' ]; then
    # 跳过指数初始化
    if [ "$IS_INIT_INDEX" = 'false' ]; then
        echo
        echo 'Init without index'
        echo
        flask init --init-index "$IS_INIT_INDEX"
    else
        echo
        echo 'Init all'
        echo
        flask init
    fi
    exit 0
fi

export GUNICORN_WORKERS=$(($(nproc) * 2 + 1))

exec "$@"
