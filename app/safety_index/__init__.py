#! /usr/bin/env python3
# -*- encoding:utf-8 -*-

from flask import Blueprint
'''指数独立小屏相关接口, 包括各指数站段排行，站段指数详情、指数中间过程、指数报告word导出，
    指数的权重配置调节（预览）
    health_index: 综合指数相关
    major_index: 专业重点风险指数相关
    author: Qiangsheng
    date: 2019/01/11
'''

safety_index_bp = Blueprint('safety_index', __name__)
from . import route
