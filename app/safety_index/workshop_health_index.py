#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/13
Description: 
"""

from importlib import import_module
from copy import deepcopy

import pandas as pd
import xlwt
from docx import Document
from docx.shared import Inches
from flask import current_app

from app.data.util import get_mongodb_prefix, plot_radar, pd_query
from app.story.util import get_hierarchy_dpid
from app import mongo
from app.utils.common_func import choose_collection_prefix, get_history_months
from app.utils.safety_index_common_func import (is_exist_dpid)
from app.data.workshop_health_index.const import INDEX_TYPE, MainType


def get_department_name_by_dpid(dpid):
    """根据部门ID取部门名称

    Arguments:
        dpid {str} -- 部门ID

    Returns:
        str -- 部门名称
    """
    record = mongo.db.base_department.find_one({
        "DEPARTMENT_ID": dpid
    }, {
        "ALL_NAME": 1,
        "_id": 0
    })
    if record:
        return record['ALL_NAME']
    else:
        raise ValueError('DPID不存在')


# 获取前端显示分类的专业站段数据
def get_zhanduan_deparment(sql):
    data = pd_query(sql)
    # 将所属专业属性为客运的归纳为新的专业（客运）
    data['MAJOR'][data['BELONG_PROFESSION_NAME'] == '客运'] = '客运'
    # 工电段会被分配到电务、供电、工务三个专业中
    copy_rows_dianwu = data[data['NAME'].str.contains('工电段')].copy()
    copy_rows_gongdian = data[data['NAME'].str.contains('工电段')].copy()
    copy_rows_gongwu = data[data['NAME'].str.contains('工电段')].copy()
    data.drop(data.loc[data['NAME'].str.contains('工电段')].index, inplace=True)
    copy_rows_dianwu['MAJOR'] = '电务'
    copy_rows_gongdian['MAJOR'] = '供电'
    copy_rows_gongwu['MAJOR'] = '工务'
    data = pd.concat([data, copy_rows_dianwu, copy_rows_gongdian, copy_rows_gongwu])
    # data['MAJOR'][data['NAME'].str.contains('工电段')] = '供电'
    data.drop(['BELONG_PROFESSION_NAME'], inplace=True, axis=1)
    return data


def get_major_dpid(dpid, is_station=False):
    # 通过department_id获取专业名称
    if not is_station:
        condition = {"DEPARTMENT_ID": dpid}
    else:
        condition = {"TYPE3": dpid}
    record = mongo.db['monthly_workshop_health_index'].find_one(condition, {
        "MAJOR": 1,
        "_id": 0
    })
    if record:
        return record['MAJOR']
    else:
        return None


def _get_sorted_dp(data, last_mon_data):
    data = sorted(data, key=lambda x: x['SCORE'], reverse=True)
    return [{
        'dpid': item['DEPARTMENT_ID'],
        'name': item['DEPARTMENT_NAME'],
        'rank': idx + 1,  # 前端显示时排序用
        'value': item['SCORE'],
        'last_value': last_mon_data.get(item['DEPARTMENT_ID'], 0),
        'cls_rank': f"{item.get('CLS_RANK', '')}/{item.get('CLS_COUNT', '')}"
    } for idx, item in enumerate(data)]


def _sort_score_by_mon(data):
    desc_dict = {}
    for idx, row in data.iterrows():
        # desc_dict[row['MON']] = f"{row['SCORE']:.2f}/{int(row['RANK'])}"
        desc_dict[row['MON']] = f"{row['SCORE']:.2f}/{row['CLS_RANK']}"
    rst = []
    global MON
    for x in MON:
        rst.append(desc_dict.get(x, '0/0'))
    return rst


def _get_mon(mon):
    mon.sort()
    rst = ["DATE"]
    global MON
    MON = mon
    mon = ['{}-{:0>2}'.format(item // 100, item % 100) for item in mon]
    rst.extend(mon)
    return rst


def get_frontend_display_index_name(main_type):
    _map = {
        1: 'check_intensity',
        2: 'evaluate_intensity',
        4: 'check_balance',
        3: 'rate_intensity',
        5: 'issue_exposure',
        6: 'issue_fix',
        7: 'self_control_ability'
    }
    if main_type in _map:
        return _map.get(main_type, '-')


def get_rank_data(param_dict):
    """按专业（综合）展示各站段的指数得分排名和6个分指数得分排名
        如果没有传MAJOR，只传了TYPE3字段，则不限专业，找出此站段下所有计算过车间指数的车间
    Arguments:
        param_dict {dict} -- MONTH：月份， MAJOR：专业
    """
    mon = int(param_dict['MONTH'])
    type3 = param_dict.get('TYPE3')
    classify = param_dict.get('CLASSIFY')
    if mon < 201711:
        return 'MONTH should be > 201810'
    # 获取上个月年月YYYYmm
    if mon % 100 == 1:
        last_mon = mon - 100 + 11
    else:
        last_mon = mon - 1
    condition = {"MON": mon, "HIERARCHY": 4, "DETAIL_TYPE": 0, "MAIN_TYPE": {'$ne': MainType.key_control}}
    major = param_dict.get('MAJOR')
    if type3:
        condition['TYPE3'] = type3
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    elif not type3:
        return 'MAJOR - %s INVAILD' % major
    if classify:
        condition['CLASSIFY'] = classify
    prefix = choose_collection_prefix(mon)
    last_prefix = choose_collection_prefix(last_mon)
    documents = list(mongo.db[f'{prefix}detail_workshop_health_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
            "CLS_RANK": 1,
            "CLS_COUNT": 1
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    rst = []
    condition_last = {
            "MON": last_mon,
            "HIERARCHY": 4,
            "DETAIL_TYPE": 0,
        }
    if major:
        condition_last['MAJOR'] = major
    if type3:
        condition_last['TYPE3'] = type3
    last_mon_data = list(mongo.db[f'{last_prefix}detail_workshop_health_index'].find(
        condition_last, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "SCORE": 1
        }))
    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }
    data = pd.DataFrame(documents)
    groups = data.groupby(['MAIN_TYPE'])
    for k, v in groups:
        vdata = _get_sorted_dp(v.to_dict('records'), last_mon_data_dict)
        rst.append({'title': get_index_title(k), 'major': 1, 'data': vdata})

    # 增加整体排名
    condition_total = {
            'MON': mon,
            'HIERARCHY': 4,
        }
    if major:
        condition_total['MAJOR'] = major
    if type3:
        condition_total['TYPE3'] = type3
    if classify:
        condition_total['CLASSIFY'] = classify
    documents = list(mongo.db[f'{prefix}workshop_health_index'].find(
        condition_total, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
            "CLS_RANK": 1,
            "CLS_COUNT": 1
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    condition_total_last = {
            "MON": last_mon,
            "HIERARCHY": 4
        }
    if major:
        condition_total_last['MAJOR'] = major
    if type3:
        condition_total_last['TYPE3'] = type3
    last_mon_data = list(mongo.db[f'{last_prefix}workshop_health_index'].find(
        condition_total_last, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "SCORE": 1
        }))
    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }
    data = sorted(documents, key=lambda x: x['SCORE'], reverse=True)
    rst.append({
        'title':
            '总指数排行',
        'major':
            0,
        'data': [{
            'dpid':
                item['DEPARTMENT_ID'],
            'name':
                item['DEPARTMENT_NAME'],
            'rank':
                idx + 1,
            'value':
                item['SCORE'],
            'last_value':
                last_mon_data_dict.get(item['DEPARTMENT_ID'], 0),
            'cls_rank': f"{item.get('CLS_RANK', '')}/{item.get('CLS_COUNT', '')}"
        } for idx, item in enumerate(data)]
    })
    return rst


def get_department_detail_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {"DEPARTMENT_ID": param_dict['DPID'], "HIERARCHY": 4}
    rst_all = []
    rst_score = []
    mon = list(mongo.db['monthly_workshop_health_index'].distinct('MON'))
    rst_score.append(_get_mon(mon))
    # 子指数折线图加入安全综合指数（总体）的数据
    data_health_index = mongo.db['monthly_workshop_health_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0
        })
    data = pd.DataFrame(list(data_health_index))
    if len(data) == 0:
        return "NO DATA"
    data = {item['MON']: f"{item['SCORE']:.2f}/{int(item['RANK'])}" for item in data.to_dict('records')}
    score_total_health_index = ['all']
    global MON
    for x in MON:
        score_total_health_index.append(data.get(x, '0/0'))
    rst_score.append(score_total_health_index)

    # 各子指数历史12个月趋势图
    data_detail_health_index = mongo.db['monthly_detail_workshop_health_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_detail_health_index))
    if len(data) == 0:
        return "NO DATA"
    groups = data[data['DETAIL_TYPE'] == 0].groupby(['MAIN_TYPE'])
    # 按格式输出
    for k, v in groups:
        rst_score.append([
            get_frontend_display_index_name(k), *(_sort_score_by_mon(v))
        ])
    rst_all.append(rst_score)

    # 各子指数的分指数历史12个月趋势图
    for main_type in range(1, 8):
        rst_score = []
        rst_score.append(_get_mon(mon))
        groups = data[data['MAIN_TYPE'] == main_type].groupby(['DETAIL_TYPE'])
        for k, v in groups:
            _title = get_child_index_name(k, main_type)
            if _title is None:
                continue
            rst_score.append([_title, *(_sort_score_by_mon(v))])
        rst_all.append(rst_score)
    return rst_all


def get_calc_tree_data(param_dict):
    if 'MONTH' not in param_dict:
        mon = get_history_months(-1)[0]
    else:
        mon = int(param_dict['MONTH'])
    index_type = int(param_dict['TYPE'])
    dpid = param_dict['DPID']
    if not is_exist_dpid(dpid):
        return "DPID不存在"
    if index_type not in [0, 1, 2, 3, 4, 5, 6, 7]:
        return "该指数还在开发中"
    rtn_html = {}
    if index_type == 0:
        tree_child = []
        for child_index_type in [1, 2, 3, 4, 5, 6, 7]:
            child_data = get_health_index_calc_tree(dpid, mon,
                                                    child_index_type)
            if child_data is not None:
                tree_child.append(child_data)
        rtn_html.update({'name': '安全综合指数', 'children': tree_child})
    else:
        rtn_html = get_health_index_calc_tree(dpid, mon, index_type)

    return rtn_html


def get_health_index_calc_tree(DPID, mon, index_type, index_flag=INDEX_TYPE):
    """获取某个部门某个月的安全综合指数计算过程

    Arguments:
        DPID {str} -- 部门ID
        mon {int} -- 月份， format： YYYYmm
        index_type {int} -- 指数分类（0代表安全综合指数，1-6分别代表各个子指数）

    Returns:
        list -- 指数计算过程
    """
    coll_name_prefix = get_mongodb_prefix(index_flag)
    prefix = choose_collection_prefix(mon)
    data = pd.DataFrame(
        list(mongo.db[f'{prefix}{coll_name_prefix}_index_basic_data'].find({
            'DEPARTMENT_ID':
            DPID,
            'MON':
            mon,
            'MAIN_TYPE':
            index_type,
        })))
    if data.empty:
        return None
    child_html = []
    for idx, row in data.iterrows():
        title = get_child_index_name(row['DETAIL_TYPE'], index_type)
        formula_str = get_child_calc_formula(row['DETAIL_TYPE'], index_type, row['TYPE'])
        cnt = row.get('CONTENT', '')
        if isinstance(row['SCORE'], (int, float)):
            title = '{0}\n{1}red|({2}){3}'.format(title, '{', row['SCORE'], '}')
        if row['TYPE'] == 1:
            avg_quotient = row['AVG_QUOTIENT']
            avg_score = row['AVG_SCORE']
            formula_html = {
                'name': title,
                'value': {
                    'type':
                    'html',
                    'content':
                    formula_str.format(row['SCORE'], int(
                        row.get('CLS_RANK')), avg_quotient, row['QUOTIENT'],
                                       row['NUMERATOR'], row['DENOMINATOR'],
                                       avg_score),
                }
            }
        else:
            if pd.isnull(cnt):
                cnt = '暂无数据'
            formula_html = {
                'name': title,
                'value': {
                    'type': 'html',
                    'content': formula_str.format(cnt),
                }
            }
        child_html.append(formula_html)
    rtn_html = {'name': get_index_title(index_type), 'children': child_html}
    return rtn_html


def get_child_calc_formula(detail_type, main_type, content_type):
    """
    获取指数前端展示描述html
    :param detail_type:
    :param main_type:
    :param content_type: 1: 两数相除类型 2：读取content类型，直接返回<p>{0}</p>
    :return:
    """
    if content_type == 2:
        return '<p>{0}</p>'
    """指数中间过程计算公式的html模板

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 计算公式html模板
    """
    _check_intensity_map = {
        1:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算单位量化率({3}) = '
        + '量化人员数({4})/ 正式职工总数({5})*100%</p>',
        2:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>人均检查频次({3}) = '
        + '现场检查车间次数(人次)({4})/ 工作量({5})*100%</p>',
        3:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算查处问题率({3}) = '
        + '车间问题数({4})/ 工作量({5})*100%</p>',
        4:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>查处问题考核率({3}) = '
        + '考核问题数({4})/ 车间问题数({5})*100%</p>',
        5:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>问题平均质量分({3}) = '
        + '问题质量分累计({4})/ 问题总数({5})</p>',
        6:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算人均质量分({3}) = '
        + '问题质量分累计({4})/ 工作量({5})</p>',
        7:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>夜查率({3}) = '
        + '现场夜查次数(人次)({4})/ 现场检查次数(人次)({5})*100%</p>',
        8:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>较大及以上风险问题占比({3}) = '
        + '较大及以上风险问题数({4})/ 工作量({5})*100%</p>',
        9:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>覆盖率({3}) = '
        + '检查地点数({4})/ 地点总数({5})*100%</p>',
        10:
        '<p>{0}</p>',
        11:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>一般及以上风险问题占比({3}) = '
        + '一般及以上风险问题数({4})/ 工作量({5})*100%</p>',
    }
    _evaluate_intensity_map = {
        1:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>主动评价记分占比({3}) = '
        + '主动评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        2:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>干部人均主动评价记分条数({3}) = '
        + '主动评价记分条数({4})/ 干部总人数({5})</p>',
        3:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>干部人均主动评价记分分数({3}) = '
        + '主动评价记分分数({4})/ 干部总人数({5})</p>',
        4:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>评价职务占比({3}) = '
        + '（主动）科职干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        5:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>段机关干部占比({3}) = '
        + '（主动）段机关干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        6:
        '<p>{0}</p>',
        +
        7:
        '<p>{0}</p>',
    }
    _assess_intensity_map = {
        1:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算人均考核问题数({3}) = '
        + '考核问题数({4})/总人数 ({5})</p>',
        2:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算人均考核金额({3}) = '
        + '月度职工考核总金额({4})/ 总人数({5})</p>',
        3:
        '<p>扣分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>返奖率({3}) = '
        + '月度返奖金额({4})/ 月度考核金额({5})</p>',
        4:
        '<p>扣分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>考核率({3}) = '
        + '考核问题数({4})/ 问题总数（非路外）({5})</p>',
    }
    _check_evenness_map = {
        1:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>问题查处均衡度({3}) = '
        + '查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>',
        2:
        '<p>{0}</p>',
        3:
        '<p>{0}</p>',
        4:
        '<p>{0}</p>',
        5:
        '<p>{0}</p>',
    }

    _problem_exposure_map = {
        1: '<p>{0}</p>',
        2: '<p>{0}</p>',
        3: '<p>{0}</p>',
        4: '<p>{0}</p>',
        5: '<p>{0}</p>',
    }

    _problem_rectification_map = {
        1:
        '<p>{0}</p>',
        2:
        '<p>{0}</p>',
        3:
        '<p>{0}</p>',
        4:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>整改复查({3}) = '
        + '库内问题复查数({4})/ 工作量({5})</p>',
        5:
        '<p>{0}</p>',
        6:
        '<p>{0}</p>',
    }

    _self_control_ability_map = {
        1:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算车间自查频次({3}) = '
        + '现场检查车间次数(人次)({4})/ 工作量({5})*100%</p>',
        2:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>车间人均质量分（({3}) = '
        + '车间自查问题质量分累计({4})/ 工作量({5})</p>',
        3:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>较严重及以上风险问题（({3}) = '
        + '车间自查较严重及以上风险问题({4})/ 工作量({5})</p>',
        4: '<p>{0}</p>',
        5:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>被评价力度指数得分（({3}) = '
        + '100-被评价记分总和/({4})/ 职工人数({5}) *100*4（干部乘以职务系数，非干部不乘系数</p>',
        6:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算车间考核问题数（({3}) = '
        + '车间自查考核问题数({4})/ 职工总数（正式职工）({5})</p>',
        7:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>夜查率({3}) = '
        + '现场夜查次数(人次)({4})/ 现场检查次数(人次)({5})*100%</p>',
        8:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>覆盖率({3}) = '
        + '检查地点数({4})/ 地点总数({5})*100%</p>',
        9:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>中高质量问题占比（({3}) = '
        + '车间自查中高质量问题占比({4})/ 总问题数({5})</p>',
        10:
        '<p>{0}</p>',
        11:
        '<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p><p>换算单位考核金额({3}) = '
        + '月度职工考核总金额({4})/ 职工总数({5})</p>',
    }

    map_dict = {
        MainType.check_intensity: _check_intensity_map,
        MainType.evaluate_intensity: _evaluate_intensity_map,
        MainType.assess_intensity: _assess_intensity_map,
        MainType.check_evenness: _check_evenness_map,
        MainType.problem_exposure: _problem_exposure_map,
        MainType.problem_rectification: _problem_rectification_map,
        MainType.self_control_ability: _self_control_ability_map
    }
    _map = map_dict[main_type]
    if detail_type in _map:
        return _map.get(detail_type, None)


def get_index_title(MAIN_TYPE):
    """指数大类名称获取

    Arguments:
        MAIN_TYPE {int} -- 指数大类

    Returns:
        str -- 指数名称
    """
    _map = {
        1: '检查力度指数',
        2: '评价力度指数',
        4: '检查均衡度指数',
        3: '考核力度指数',
        5: '问题暴露指数',
        6: '问题整改效果指数',
        7: '自控能力指数',
    }
    return _map.get(MAIN_TYPE, '-')


def get_child_index_name(DETAIL_TYPE, MAIN_TYPE):
    """获取子指数的名称

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 指数名称
    """
    _check_intensity_map = {
        # 1: '换算单位量化率',
        2: '换算单位检查频次',
        3: '换算查处问题率',
        4: '查处问题考核率',
        5: '问题平均质量分',
        6: '换算人均质量分',
        7: '夜查率',
        8: '换算较严重及以上风险问题',
        9: '覆盖率',
        10: '监控调阅力度',
        11: '换算一般及以上风险问题'
    }
    _evaluate_intensity_map = {
        1: '被评价得分',
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '返奖率',
        4: '考核率',
    }
    _check_evenness_map = {
        1: '问题查处均衡度',
        2: '检查日期均衡度',
        3: '检查地点均衡度',
        4: '检查时段均衡度',
        5: '问题日期均衡度',
    }

    _problem_exposure_map = {
        1: '普遍性暴露度',
        2: '较严重隐患暴露',
        3: '事故隐患问题暴露度',
        4: '班组问题暴露度',
        5: '他查问题扣分',
    }

    _problem_rectification_map = {
        1: '整改时效',
        2: '整改履责',
        3: '问题控制',
        4: '整改复查',
        # 5: '隐患复查',
        5: '隐患整治',
        6: '整改成效',
    }

    _self_control_ability_map = {
        1: '换算车间自查频次',
        2: '车间人均质量分',
        3: '较严重及以上风险问题',
        4: '他查问题扣分',
        5: '被评价力度指数',
        6: '换算车间考核问题数',
        7: '夜查率',
        8: '覆盖率',
        9: '中高质量问题占比',
        10: '检查时段均衡度',
        11: '换算单位考核金额'
    }

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _self_control_ability_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


def get_index_table_data(param_dict):
    """按专业（综合）展示各站段的指数得分和6个分指数得分
    Arguments:
        param_dict {dict} -- MONTH：月份， MAJOR：专业
    """
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 4, "DETAIL_TYPE": 0}
    major = param_dict['MAJOR']
    if major in ["综合", '供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    data_rst = []
    coll_prefix = choose_collection_prefix(mon)
    major_list = [major]
    if major == '综合':
        major_list = ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']
    # 获取子指数得分
    for major in major_list:
        condition.update({'MAJOR': major})
        major_data = get_index_table_data_by_major(coll_prefix,
                                                   condition.copy(),
                                                   index_type=INDEX_TYPE)
        if major_data != []:
            data_rst.append({'major_name': major, 'major_data': major_data})
    return data_rst


# 获取雷达图信息
def get_department_detail_radar_data(mon, dpid, major):
    prefix = choose_collection_prefix(mon)
    condition = {"DETAIL_TYPE": 0, "MON": mon, "DEPARTMENT_ID": dpid,
                 "MAIN_TYPE": {'$ne': MainType.key_control}}
    radar_labels = list(mongo.db[f"{prefix}detail_workshop_health_index"].find(condition,
                                                                               {'_id': 0, "MAIN_TYPE": 1, "SCORE": 1}))
    rst_data = {}
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE']) for label in radar_labels]
        data = []
        subtags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        if not major:
            major = get_major_dpid(dpid)
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            data_tmp = list(mongo.db[f'{prefix}detail_workshop_health_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major},
                {"_id": 0, "SCORE": 1, "RANK": 1}))
            most_data[main_type] = [i['SCORE'] for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE'] for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        rst_data['labels'] = labels
        rst_data['data'] = data
        rst_data['subtags'] = subtags
        return rst_data
    return ''


def export_health_index_word(department_name, param_dict, file_path, index_flag=INDEX_TYPE):
    coll_name_prefix = get_mongodb_prefix(index_flag)
    mon = int(param_dict['MONTH'])
    if mon < 201901:
        return '2019年以前指数不生成报告'
    dpid = param_dict['DPID']
    major = get_major_dpid(dpid)
    document = Document()
    document.add_heading(f'{department_name}车间级安全综合指数报告（{mon//100}年{mon%100}月）',
                         0)
    # todo 添加雷达图
    prefix = choose_collection_prefix(mon)
    condition = {"DETAIL_TYPE": 0,
                 "MON": mon,
                 "DEPARTMENT_ID": dpid,
                 "MAJOR": major,
                 "MAIN_TYPE": {'$ne': MainType.key_control}}
    radar_labels = list(mongo.db[f"{prefix}detail_{coll_name_prefix}_index"].find(condition,
                                                                                  {'_id': 0, "MAIN_TYPE": 1,
                                                                                   "SCORE": 1}))
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE']) for label in radar_labels]
        data = []
        subtags = ['本车间各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            data_tmp = list(mongo.db[f'{prefix}detail_{coll_name_prefix}_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major},
                {"_id": 0, "SCORE": 1, "RANK": 1}))

            most_data[main_type] = [i['SCORE'] for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE'] for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        pic_path = plot_radar([get_index_title(label['MAIN_TYPE'])[:-2] for label in radar_labels],
                              data, f'{department_name}车间级安全管理综合', subtags,
                              current_app.config.get('DOWNLOAD_FILE_WORKSHOP_HEALTH'))
        document.add_picture(pic_path, width=Inches(6.0))
        # 添加描述表格
        table = document.add_table(rows=4, cols=7, style='Table Grid')
        # 第一行所有的方格
        for r in range(4):
            hdr_cells = table.rows[r].cells
            if r == 0:
                for p in range(1, 7):
                    hdr_cells[p].add_paragraph(labels[p-1])
            else:
                for p in range(7):
                    if p == 0:
                        hdr_cells[p].add_paragraph(subtags[r-1])
                    else:
                        hdr_cells[p].add_paragraph(str(round(data[r-1][p-1], 2)))

    # 往文档中添加段落
    for main_type in [1, 2, 3, 4, 5, 6, 7]:
        main_type_weight = mongo.db['base_index_weight'].find(
            {
                'INDEX_TYPE': INDEX_TYPE,
                'MAIN_TYPE': main_type,
                'MAJOR': major
            }, {
                '_id': 0,
                'WEIGHT': 1
            })
        if main_type_weight.count() == 0:
            continue
        main_type_weight = main_type_weight[0]['WEIGHT']
        index_title = '{0}. {1}({2}%)'.format(main_type,
                                              get_index_title(main_type),
                                              main_type_weight * 100)
        document.add_heading(index_title, level=1)

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'INDEX_TYPE': INDEX_TYPE,
                'MAIN_TYPE': main_type,
                'MON': mon,
                'MAJOR': major
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'INDEX_TYPE': INDEX_TYPE,
                    'MAIN_TYPE': main_type,
                    'MAJOR': major
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))
        # weight_data = list(mongo.db['base_detail_index_weight'].find(
        #     {
        #         'INDEX_TYPE': 0,
        #         'MAIN_TYPE': main_type
        #     }, {
        #         '_id': 0,
        #         'WEIGHT': 1,
        #         'DETAIL_TYPE': 1
        #     }))
        weight_table = document.add_table(rows=1, cols=2)
        heading_cells = weight_table.rows[0].cells
        heading_cells[0].text = '指数名称'
        heading_cells[1].text = '占比%'
        for item in weight_data:
            cells = weight_table.add_row().cells
            cells[0].text = get_child_index_name(item['DETAIL_TYPE'],
                                                 main_type)
            cells[1].text = str(int(item['WEIGHT'] * 100)).replace(
                '-100', '扣分')
        prefix = choose_collection_prefix(mon)
        basic_data = list(mongo.db[f'{prefix}{coll_name_prefix}_index_basic_data'].find({
            'DEPARTMENT_ID':
            dpid,
            'MON':
            mon,
            'MAIN_TYPE':
            main_type,
        }))
        if len(basic_data) < 1:
            continue
        basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
        for idx, item in enumerate(weight_data):
            idx_name = get_child_index_name(item["DETAIL_TYPE"], main_type)
            document.add_heading(f'{main_type}.{idx+1} {idx_name}')
            if item['DETAIL_TYPE'] not in basic_data_dict:
                continue
            row = basic_data_dict.get(item['DETAIL_TYPE'])
            formula_str = get_child_calc_formula(row['DETAIL_TYPE'], main_type, row['TYPE'])
            if row['TYPE'] == 1:
                # 处理2月份之前版本指数，没有专业平均分
                if 'AVG_QUOTIENT' not in row:
                    avg_quotient = row['AVG_SCORE']
                    avg_score = '暂无'
                else:
                    avg_quotient = row['AVG_QUOTIENT']
                    avg_score = row['AVG_SCORE']
                index_paragraph = formula_str.format(
                    row['SCORE'], int(
                        row['CLS_RANK']), avg_quotient, row['QUOTIENT'],
                    row['NUMERATOR'], row['DENOMINATOR'], avg_score)
            else:
                cnt = row['CONTENT']
                if pd.isnull(cnt):
                    cnt = '暂无数据'
                index_paragraph = formula_str.format(cnt)
            document.add_paragraph(
                index_paragraph.replace('<p>', '').replace('</p>',
                                                           '\n').replace(
                                                               '<br/>', '\n'))
    # 保存文档
    document.save(file_path)
    return True


def get_index_table_data_by_major(coll_prefix, condition, index_type=1):
    coll_name_prefix = get_mongodb_prefix(index_type)
    total_condition = condition.copy()
    # 取出各个站段的子指数的分数
    child_docs = list(mongo.db[f'{coll_prefix}detail_{coll_name_prefix}_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(child_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in detail_{coll_name_prefix}_index(MONTH-' +
            f'{condition})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的分指数放入一个list
    child_data = {}
    for each in child_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((each['MAIN_TYPE'], each['SCORE']))
        child_data.update({dep_name: dep_data})
    # 取各个站段的综合指数分数
    del total_condition['DETAIL_TYPE']
    total_docs = list(mongo.db[f'{coll_prefix}{coll_name_prefix}_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in {coll_name_prefix}_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((0, each['SCORE']))
        child_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    child_score = []
    for dep_name in child_data:
        dep_score = {
            get_index_title(main_type).replace('-', '综合指数'): score
            for main_type, score in child_data[dep_name]
        }
        dep_score.update({'name': dep_name})
        child_score.append(dep_score)
    return child_score


def get_grandson_index_data_by_major(coll_prefix, condition, index_flag=INDEX_TYPE):
    coll_name_prefix = get_mongodb_prefix(index_flag)
    total_condition = condition.copy()
    grandson_docs = list(mongo.db[f'{coll_prefix}detail_{coll_name_prefix}_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(grandson_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in detail_{coll_name_prefix}_index(MONTH-' +
            f'{condition})')
        return []
    df_grandson = pd.DataFrame(grandson_docs)
    grandson_data = {}
    full_grandson_index = {
        1: (0, 2, 5, 6, 7, 8, 9, 10),
        2: (0, 1, 2, 3, 4, 5, 6, 7),
        3: (0, 1, 2, 3, 4),
        4: (0, 1, 2, 3, 4, 5),
        5: (0, 1, 2, 3, 4, 5),
        6: (0, 1, 2, 3, 4, 5, 6),
        7: (0, 1, 2, 3, 4, 5, 6)
    }
    for idx, val in df_grandson.groupby(['DEPARTMENT_NAME', 'MAIN_TYPE']):
        dep_name = idx[0]
        main_type = idx[1]
        grandson_score = []
        for detail_type in full_grandson_index[main_type]:
            if detail_type in val['DETAIL_TYPE'].values:
                grandson_score.append([
                    detail_type,
                    val[val['DETAIL_TYPE'] == detail_type]['SCORE'].values[0]
                ])
            else:
                grandson_score.append([detail_type, 0])
        dep_data = grandson_data.get(dep_name, {})
        dep_data.update({main_type: grandson_score})
        grandson_data.update({dep_name: dep_data})
    # 取各个站段的综合指数分数
    total_docs = list(mongo.db[f'{coll_prefix}{coll_name_prefix}_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in {coll_name_prefix}_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in grandson_data:
            dep_data = grandson_data[dep_name]
        dep_data.update({0: each['SCORE']})
        grandson_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    fmt_index_score = {}
    for dep_name in grandson_data:
        fmt_dep_data = []
        dep_data = grandson_data[dep_name]
        for i in range(1, 7):
            if i not in dep_data:
                continue
            fmt_dep_data.append(dep_data[i])
        fmt_dep_data.append(['综合指数', dep_data[0]])

        fmt_index_score.update({dep_name: fmt_dep_data})
    return fmt_index_score


def export_score_table_excel(param_dict, file_path, index_flag=INDEX_TYPE):
    wb = xlwt.Workbook(encoding='ascill')
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 4}
    major = param_dict['MAJOR']
    if major in ["综合", '供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    coll_prefix = choose_collection_prefix(mon)
    major_list = [major]
    if major == '综合':
        major_list = ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']
    full_grandson_index = {
        1: (0, 2, 5, 6, 7, 8, 9, 10),
        2: (0, 1, 2, 3, 4, 5, 6, 7),
        3: (0, 1, 2, 3, 4),
        4: (0, 1, 2, 3, 4, 5),
        5: (0, 1, 2, 3, 4, 5),
        6: (0, 1, 2, 3, 4, 5, 6),
        7: (0, 1, 2, 3, 4, 5, 6)
    }
    # 获取子指数得分
    for major in major_list:
        condition.update({'MAJOR': major})
        major_data = get_grandson_index_data_by_major(coll_prefix,
                                                      condition.copy(),
                                                      index_flag=index_flag)
        if major_data == []:
            continue
        # 为每个专业生成一个work_sheet
        wt = wb.add_sheet(major)
        row = 0
        # 第一行填入各个指数名称
        if row == 0:
            wt.write(0, 0, '序号')
            wt.write(0, 1, '站段')
            col_title = 2
            for main_type in full_grandson_index:
                for detail_type in full_grandson_index[main_type]:
                    if detail_type == 0:
                        wt.write(0, col_title, get_index_title(main_type))
                    else:
                        wt.write(0, col_title,
                                 get_child_index_name(detail_type, main_type))
                    col_title += 1
            wt.write(0, col_title, '综合指数')
        for dep_name in major_data:
            col_idx = 2
            wt.write(row + 1, 0, row + 1)
            wt.write(row + 1, 1, dep_name)
            for child_index_scores in major_data[dep_name][:-1]:
                for child_score in child_index_scores:
                    wt.write(row + 1, col_idx, child_score[1])
                    col_idx += 1
            wt.write(row + 1, col_idx, major_data[dep_name][-1][1])
            row += 1
    wb.save(file_path)
    return True


def _combine_weight_table(major, main_type, weight_data,
                          score_data, mon):
    grandson_indexes = []
    score_dict = {item['DETAIL_TYPE']: item['SCORE'] for item in score_data}
    for iweight in weight_data:
        detail_type = iweight['DETAIL_TYPE']
        # 获取孙子指数名称
        index_name = get_child_index_name(detail_type, main_type)
        if index_name is None:
            continue
        grandson_indexes.append({
            'name': index_name,
            'weight': iweight['WEIGHT'],
            'score': score_dict.get(detail_type, 0),
            'detail_type': detail_type
        })

    rst_data = {
        'name':
            get_index_title(main_type),
        'main_type':
            main_type,
        'weight':
            mongo.db['base_index_weight'].find({
                'INDEX_TYPE': INDEX_TYPE,
                'MAJOR': major,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            })[0]['WEIGHT'],
        'score':
            score_dict.get(0, 0),
        'grandson_indexes':
            grandson_indexes
    }
    return rst_data


def get_weight_data(param_dict):
    """车间级安全综合指数【站段】各层级指数权重展示&调整配置

    Arguments:
        param_dict {dict} -- 参数
    """
    mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']
    prefix = choose_collection_prefix(mon)
    rst_child_data = []
    for main_type in [1, 2, 3, 4, 5, 6, 7]:

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': INDEX_TYPE,
                'MAIN_TYPE': main_type,
                "MON": mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'MAJOR': major,
                    'INDEX_TYPE': INDEX_TYPE,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))

        # 判断该main_type指数是否存在
        if len(weight_data) < 1:
            continue
        score_data = list(mongo.db[f'{prefix}detail_workshop_health_index'].find(
            {
                'DEPARTMENT_ID': param_dict['DPID'],
                'MON': mon,
                'MAIN_TYPE': main_type,
                'MAJOR': major,
            }, {
                '_id': 0,
                'SCORE': 1,
                'DETAIL_TYPE': 1
            }))
        rst_weight_table = _combine_weight_table(major, main_type,
                                                 weight_data, score_data, mon)
        if rst_weight_table is not None:
            rst_child_data.append(rst_weight_table)
    rst_data = {
        'safety_index':
            mongo.db[f'{prefix}workshop_health_index'].find(
                {
                    'DEPARTMENT_ID': param_dict['DPID'],
                    'MON': mon,
                    'MAJOR': major
                }, {
                    '_id': 0,
                    'SCORE': 1
                })[0]['SCORE'],
        'child_indexes':
            rst_child_data
    }
    return rst_data


def _validate_weight_keys(data, required_keys):
    """判断参数字典里是否缺失必选参数

    Arguments:
        data {dict} -- 参数
        required_keys {list} -- 必选参数

    Returns:
        [bool] --
    """
    for item in required_keys:
        if item not in data:
            raise KeyError(f'lack of {item}')
    return False


def get_weight_preview(param_dict):
    data = deepcopy(param_dict['DATA'])
    first_keys = ['safety_index', 'child_indexes']
    _validate_weight_keys(data, first_keys)
    second_keys = ['name', 'main_type', 'weight', 'score', 'grandson_indexes']
    third_keys = ['name', 'weight', 'score', 'detail_type']
    child_score = []
    for child_index in data['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        grandson_score = []
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_score.append(
                grandson_index['score'] * grandson_index['weight'])
        child_index['score'] = max(0, round(sum(grandson_score) * 100) / 100)
        child_score.append(child_index['score'] * child_index['weight'])
    data['safety_index'] = sum(child_score)
    return data


def _extract_weight(param):
    """提取指数权重

    Arguments:
        param {dict} -- 请求传入的参数
    """
    first_keys = ['child_indexes']
    _validate_weight_keys(param, first_keys)
    second_keys = ['main_type', 'weight', 'grandson_indexes']
    third_keys = ['weight', 'detail_type']
    main_weight = {}
    detail_weight = {}
    for child_index in param['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        main_weight.update({child_index['main_type']: child_index['weight']})
        grandson_weight = {}
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_weight.update({
                grandson_index['detail_type']:
                    grandson_index['weight']
            })
        detail_weight.update({child_index['main_type']: grandson_weight})
    return main_weight, detail_weight


def _calc_child_score(data, detail_weight):
    """重新计算子指数
    """
    old_child = []
    grandson_score = []
    for item in data:
        detail_type = item[2]
        if detail_type == 0:
            old_child = item
            continue
        elif detail_type in detail_weight:
            grandson_score.append(
                round(item[5], 2) * round(detail_weight[detail_type], 2))
    return {
        'DEPARTMENT_ID': old_child[0],
        'DEPARTMENT_NAME': old_child[1],
        'MAIN_TYPE': old_child[3],
        'SCORE': old_child[5],
        'SCORE_NEW': max(0, round(sum(grandson_score) * 100) / 100)
    }


def _sort_zhanduan_rank(data):
    data = sorted(data, key=lambda x: x[4], reverse=True)
    return [{
        'dpid': item[0],
        'name': item[1],
        'value': item[3],
        'rank_after': idx + 1,
        'value_after': round(item[4], 2),
    } for idx, item in enumerate(data)]


def _calc_index_score(child_score, index_data, main_weight):
    new_child_score = {}
    for item in child_score:
        dpid = item['DEPARTMENT_ID']
        new_score = round(main_weight[item['MAIN_TYPE']],
                          2) * item['SCORE_NEW']
        new_child_score.update({
            dpid: new_child_score.get(dpid, 0) + new_score
        })
    new_data = []
    for item in index_data:
        item.update({'SCORE_NEW': new_child_score.get(item['DEPARTMENT_ID'])})
        new_data.append(item)
    data = sorted(new_data, key=lambda x: x['SCORE_NEW'], reverse=True)
    rst_data = {
        'title':
            '总指数排行',
        'major':
            0,
        'data': [{
            'dpid': item['DEPARTMENT_ID'],
            'name': item['DEPARTMENT_NAME'],
            'value': item['SCORE'],
            'rank_after': idx + 1,
            'value_after': round(item['SCORE_NEW'] * 100) / 100,
        } for idx, item in enumerate(data)]
    }
    return rst_data


def get_rank_preview(param_dict):
    weight_param = deepcopy(param_dict['DATA'])
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    major = param_dict['MAJOR']
    condition = {
        "MON": mon,
        "HIERARCHY": 4,
        'MAJOR': param_dict['MAJOR']
    }
    main_weight, detail_weight = _extract_weight(weight_param)
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运']:
        condition.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(mon)
    documents = list(mongo.db[f'{prefix}detail_workshop_health_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}detail_workshop_health_index], \
            MONTH {param_dict["MONTH"]} invalid'

    rst_data = []
    data = pd.DataFrame(documents)
    new_detail_data = []
    for k, v in data.groupby(['DEPARTMENT_ID', 'MAIN_TYPE']):
        detail_score = v.values.tolist()
        new_detail_data.append(
            _calc_child_score(detail_score, detail_weight[k[1]]))
    for k, v in pd.DataFrame(new_detail_data).groupby('MAIN_TYPE'):
        # vdata = _sort_zhanduan_rank(v.values.tolist())
        # rst_data.append({
        #     'title': get_index_title(k),
        #     'major': 1,
        #     'data': vdata
        # })
        vdata = _get_sorted_dp(v.to_dict('records'), {})
        rst_data.append({'title': get_index_title(k), 'major': 1, 'data': vdata})
    # 增加整体排名
    documents = list(mongo.db[f'{prefix}workshop_health_index'].find(
        condition, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}workshop_health_index], \
            MONTH {param_dict["MONTH"]} invalid'

    new_data = _calc_index_score(new_detail_data, documents, main_weight)
    rst_data.append(new_data)
    return rst_data


def get_department_classify_desc():
    result = []
    major_map = {
        '供电': (0, 'gongdian'),
        '车辆': (1, 'cheliang'),
        '机务': (2, 'jiwu'),
        '车务': (3, 'chewu'),
        '工务': (4, 'gongwu'),
        '电务': (5, 'dianwu'),
        '客运': (6, 'keyun'),
        '工电': (7, 'gongdian'),
    }
    for major, value in major_map.items():
        module = import_module('app.data.workshop_health_index.{}.common'.format(value[1]))
        result.extend([{'key': k, 'name': v, 'major': value[0]} for k, v in module.department_classify_desc.items()])
    return result
