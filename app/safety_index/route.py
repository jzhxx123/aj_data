import os

from flask import (current_app, jsonify, make_response, request,
                   send_from_directory)
from app.safety_index import (health_index, major_index, safety_index_bp, autohealth_index, workshop_health_index,
                              control_intensity_index)
from app.utils.common_func import (get_department_name_by_dpid,
                                   is_lack_required_args, wrapper_rtn_msg)
from app.utils.safety_index_common_func import (
    export_health_index_word, export_score_table_excel,
    export_major_index_word, get_risk_type_name, export_major_score_table_excel,
    export_major_index_cardinal_number_word, export_health_index_cardinal_number_word)
from app.data.index.common import get_zhanduan_deparment
from app.data.health_index.common_sql import ZHANDUAN_DPID_SQL

# --------------------指数分数排名展示类------------------------- #


@safety_index_bp.route('/health_index/rank', methods=['GET'])
def health_index_rank():
    """综合指数站段排行（专业级别）
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = health_index.get_rank_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/health_index/score_table', methods=['GET'])
def health_index_score_table():
    """综合指数站段分数（专业级别）-表格形式
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = health_index.get_index_table_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/score_table', methods=['GET'])
def major_index_score_table():
    """重点指数站段分数（专业级别）-表格形式
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = major_index.get_index_table_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/rank', methods=['GET'])
def major_index_rank():
    """专业指数站段排行
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = major_index.get_rank_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/health_index', methods=['GET'])
def department_detail_health_data():
    param_dict = request.args
    necessary_param = ['DPID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = health_index.get_department_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index', methods=['GET'])
def department_detail_major_data():
    param_dict = request.args
    necessary_param = ['DPID', 'TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = major_index.get_department_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/health_index/tree', methods=['GET'])
def health_index_calc_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = health_index.get_calc_tree_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/tree', methods=['GET'])
def major_index_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = major_index.get_calc_tree_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/rank', methods=['GET'])
def autohealth_index_rank():
    """综合指数站段排行（专业级别）
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = autohealth_index.get_rank_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/score_table', methods=['GET'])
def autohealth_index_score_table():
    """综合指数站段分数（专业级别）-表格形式
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = autohealth_index.get_index_table_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index', methods=['GET'])
def department_detail_autohealth_data():
    param_dict = request.args
    necessary_param = ['DPID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = autohealth_index.get_department_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/tree', methods=['GET'])
def autohealth_index_calc_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = autohealth_index.get_calc_tree_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)

# --------------------指数权重配置类------------------------- #


@safety_index_bp.route('/health_index/weight', methods=['GET'])
def health_index_weigth():
    """安全综合指数的权重配置页面
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = health_index.get_weight_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/weight', methods=['GET'])
def major_index_weigth():
    """专业重点指数的权重配置页面
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = major_index.get_weight_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/health_index/weight_preview', methods=['POST'])
def health_index_weigth_preview():
    """调整完参数后的预览
    """
    param_dict = request.json
    data = health_index.get_weight_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/weight_preview', methods=['POST'])
def major_index_weigth_preview():
    """调整完参数后的预览
    """
    param_dict = request.json
    data = major_index.get_weight_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/health_index/rank_preview', methods=['POST'])
def health_index_rank_preview():
    """调整完参数后的站段排名预览
    """
    param_dict = request.json
    data = health_index.get_rank_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/rank_preview', methods=['POST'])
def major_index_rank_preview():
    """调整完参数后的站段排名预览
    """
    param_dict = request.json
    data = major_index.get_rank_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/weight', methods=['GET'])
def autohealth_index_weigth():
    """安全综合指数的权重配置页面
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = autohealth_index.get_weight_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/weight_preview', methods=['POST'])
def autohealth_index_weigth_preview():
    """调整完参数后的预览
    """
    param_dict = request.json
    data = autohealth_index.get_weight_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/rank_preview', methods=['POST'])
def autohealth_index_rank_preview():
    """调整完参数后的站段排名预览
    """
    param_dict = request.json
    data = autohealth_index.get_rank_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)

# --------------------指数报告类------------------------- #


@safety_index_bp.route('/download/health_index/clear', methods=['GET'])
def download_clear():
    """删除下载文件目录下制定目录下的所有文件，如果没有指定则是health_index下
    """
    folder_path = 'download/health_index'
    if request:
        param = request.args
        if 'folder_name' in param:
            folder_path = param['folder_path']
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    folder_path = os.path.join(root_path, folder_path)

    if os.path.exists(folder_path):
        for remove_path in os.listdir(folder_path):
            remove_path = os.path.join(folder_path, remove_path)
            if os.path.isfile(remove_path):
                os.remove(remove_path)
        return jsonify(wrapper_rtn_msg("删除成功", 0, ""))
    else:
        return jsonify(wrapper_rtn_msg("", 1, "目录不存在"))


@safety_index_bp.route('/health_index/export_file', methods=['GET'])
def health_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    filename = f'安全管理综合指数_{department_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_health_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route(
    '/health_index/export_score_table_excel', methods=['GET'])
def health_index_export_score_table_excel():
    """按专业（综合）展示各站段的指数得分和6个子指数及子子指数得分,以excel文件导出
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    filename = f'安全管理综合指数_{param_dict["MONTH"]}.xls'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_score_table_excel(param_dict, file_path, index_flag=1)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/major_index/export_file', methods=['GET'])
def major_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    risk_name = get_risk_type_name(param_dict['MAJOR'],
                                   param_dict['RISK_TYPE'])
    risk_name = f'{param_dict["MAJOR"]}_{risk_name}'
    filename = f'{department_name}_{risk_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_major_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route(
    '/major_index/export_score_table_excel', methods=['GET'])
def major_index_export_score_table_excel():
    """按专业展示各站段的指数得分和6个子指数及子子指数得分,以excel文件导出
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    department_name = param_dict['MAJOR']

    filename = f'{department_name}_重点风险指数_{param_dict["MONTH"]}.xls'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_major_score_table_excel(param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/autohealth_index/export_file', methods=['GET'])
def autohealth_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    filename = f'安全管理综合指数(自动基数版)_{department_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_health_index_word(department_name, param_dict, file_path, index_flag=-1)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route(
    '/autohealth_index/export_score_table_excel', methods=['GET'])
def autohealth_index_export_score_table_excel():
    """按专业（综合）展示各站段的指数得分和6个子指数及子子指数得分,以excel文件导出
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    filename = f'安全管理综合指数(自动基数版)_{param_dict["MONTH"]}.xls'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_score_table_excel(param_dict, file_path, index_flag=-1)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))

# --------------------自动预警类------------------------- #


# ---------------------指数雷达图------------------------ #
@safety_index_bp.route('/health_index/radar', methods=['GET'])
def department_detail_health_data_byradar():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = health_index.get_department_detail_radar_data(mon, dpid)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/major_index/radar', methods=['GET'])
def department_detail_major_data_byradar():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH', 'RISK_TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    risk_type = int(param_dict['RISK_TYPE'])
    major = param_dict['MAJOR']
    data = major_index.get_department_detail_radar_data(mon, dpid, risk_type, major)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@safety_index_bp.route('/autohealth_index/radar', methods=['GET'])
def department_detail_autohealth_data_byradar():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = autohealth_index.get_department_detail_radar_data(mon, dpid)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)

# ---------------------站段级用户显示指数内容------------------------ #
@safety_index_bp.route('/zhanduan_index/info', methods=['GET'])
def get_zhuanduan_index_by_dpid():
    """
    根据部门id，获取对应的指数类型
    :return:
    """
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = major_index.get_zhanduan_index_by_dpid(dpid, mon)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@safety_index_bp.route('/major_index/department', methods=['GET'])
def department_detail_major_data_bymonth():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH', 'RISK_TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    risk_type = int(param_dict['RISK_TYPE'])
    major = param_dict['MAJOR']
    data = major_index.get_department_detail_major_data(mon, dpid, risk_type, major)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@safety_index_bp.route('/health_index/department', methods=['GET'])
def department_detail_health_data_bymonth():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = health_index.get_department_detail_major_data(mon, dpid)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


# --------------------- 基数分析报告下载接口------------------------ #
@safety_index_bp.route('/major_index/export_cardinal_number_file', methods=['GET'])
def major_index_cardinal_number_file():
    """
    重点指数基数分析报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    risk_name = get_risk_type_name(param_dict['MAJOR'],
                                   param_dict['RISK_TYPE'])
    risk_name = f'{param_dict["MAJOR"]}_{risk_name}'
    filename = f'{department_name}_{risk_name}_{param_dict["MONTH"]}_基数分析报告.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        rst_response = make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
        rst_response.headers['Access-Control-Expose-Headers'] = 'Content-Disposition'
        rst_response.headers['Access-Control-Allow-Origin'] = '*'
        return rst_response
    else:
        try:
            rtn = export_major_index_cardinal_number_word(department_name, 
                                                    param_dict, file_path)
        except Exception as e:
            current_app.logger.debug(
                f'├── └── export major cardinal number file failed! \n {e}')
            rtn = False
        if rtn is True:
            rst_response = make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
            rst_response.headers['Access-Control-Expose-Headers'] = 'Content-Disposition'
            rst_response.headers['Access-Control-Allow-Origin'] = '*'
            return rst_response
        else:
            # 请求资源失败
            return jsonify(wrapper_rtn_msg("", 1, rtn)), 404


@safety_index_bp.route('/health_index/export_cardinal_number_file', methods=['GET'])
def health_index_index_cardinal_number_file():
    """
    重点指数基数分析报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    filename = f'安全管理综合指数_{department_name}_{param_dict["MONTH"]}_基数分析报告.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_health_index_cardinal_number_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


# ---------------------车间级综合指数接口------------------------ #
@safety_index_bp.route('/workshop_health_index/get_station_data', methods=['GET'])
def get_station_data():
    """为车间级指数提供站段列表
    它用于【车间级指数】顶部筛选的选项中。它返回的数据，主要包含label,major,order三个字段。
    其对应意义：
        label: 指数在页面上显示的选项的名称
        major: 实际上是序号。在前端的代码里面，硬编码了['供电', '车辆', '机务', '车务', '工务', '电务', '客运', '工电']数组。它
               的【下标】对应了major的值。从0开始。
        order: 它实际上是risk_type，将被前端作为RISK_TYPE在调用数据的时候返回。同时也用于前端进行菜单项排序。
    Returns:
        [JSON] -- [指数的清单] 格式为：
        {
            "data": [{
                "label": "专业管理风险",
                "major": 7,
                "order": 3
            },..],
            "error_message": "",
            "status": 0
        }
    """
    zhanduan_data = workshop_health_index.get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    # 格式为： "<风险名称>":"<专业名称>-<RISK_TYPE>"
    data = []
    major_map = {
        '供电': 0,
        '车辆': 1,
        '机务': 2,
        '车务': 3,
        '工务': 4,
        '电务': 5,
        '客运': 6,
        '工电': 7,
    }
    for _, row in zhanduan_data.iterrows():
        data.append({'label': row['NAME'],
                     'major': major_map.get(row['MAJOR']),
                     'dpid': row['DEPARTMENT_ID']})
    rs = {'data': data, 'status': 0, 'error_message': ''}
    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/rank', methods=['GET'])
def workshop_health_index_rank():

    """综合指数站段排行（专业级别）
    """
    param_dict = request.args
    necessary_param = {'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = workshop_health_index.get_rank_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/score_table', methods=['GET'])
def workshop_health_index_score_table():
    """车间级综合指数分数（专业级别）-表格形式
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = workshop_health_index.get_index_table_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index', methods=['GET'])
def department_detail_workshop_health_data():
    param_dict = request.args
    necessary_param = ['DPID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = workshop_health_index.get_department_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/tree', methods=['GET'])
def workshop_health_index_calc_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = workshop_health_index.get_calc_tree_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/export_file', methods=['GET'])
def workshop_health_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_WORKSHOP_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = workshop_health_index.get_department_name_by_dpid(param_dict['DPID'])
    filename = f'车间级安全管理综合指数_{department_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = workshop_health_index.export_health_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/workshop_health_index/radar', methods=['GET'])
def department_detail_workshop_health_data_byradar():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']
    data = workshop_health_index.get_department_detail_radar_data(mon, dpid, major)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/export_score_table_excel', methods=['GET'])
def workshop_health_index_export_score_table_excel():
    """按专业（综合）展示各站段的指数得分和6个子指数及子子指数得分,以excel文件导出
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())

    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    filename = f'车间级安全管理综合指数_{param_dict["MONTH"]}.xls'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = workshop_health_index.export_score_table_excel(param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/workshop_health_index/weight', methods=['GET'])
def workshop_health_index_weight():
    """车间级安全综合指数的权重配置页面
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = workshop_health_index.get_weight_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/weight_preview', methods=['POST'])
def workshop_health_index_weight_preview():
    """调整完参数后的预览
    """
    param_dict = request.json
    data = workshop_health_index.get_weight_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/rank_preview', methods=['POST'])
def workshop_health_index_rank_preview():
    """调整完参数后的站段排名预览
    """
    param_dict = request.json
    data = workshop_health_index.get_rank_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/workshop_health_index/department_classify', methods=['GET'])
def workshop_health_index_department_classify():
    """各专业车间分类
    """
    param_dict = request.json
    data = workshop_health_index.get_department_classify_desc()
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


# ---------------------卡控力度指数接口------------------------ #
@safety_index_bp.route('/control_intensity_index/rank', methods=['GET'])
def control_intensity_index_rank():

    """卡控力度指数站段排行
    """
    param_dict = request.args
    necessary_param = {'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = control_intensity_index.get_rank_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index', methods=['GET'])
def department_detail_control_intensity_data():
    param_dict = request.args
    necessary_param = ['DPID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = control_intensity_index.get_department_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/tree', methods=['GET'])
def control_intensity_index_calc_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = control_intensity_index.get_calc_tree_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/radar', methods=['GET'])
def department_detail_control_intensity_data_byradar():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = control_intensity_index.get_department_detail_radar_data(mon, dpid)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/export_file', methods=['GET'])
def control_intensity_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_CONTROL_INTENSITY'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    filename = f'卡控力度指数_{department_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = control_intensity_index.export_health_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/control_intensity_index/score_table', methods=['GET'])
def control_intensity_index_score_table():
    """卡控力度指数指数分数（专业级别）-表格形式
    """
    param_dict = request.args
    necessary_param = {'MAJOR', 'MONTH'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = control_intensity_index.get_index_table_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/export_score_table_excel', methods=['GET'])
def control_intensity_index_export_score_table_excel():
    """按专业（综合）展示各站段的指数得分和6个子指数及子子指数得分,以excel文件导出
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())

    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_CONTROL_INTENSITY'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    filename = f'卡控力度指数_{param_dict["MONTH"]}.xls'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = control_intensity_index.export_score_table_excel(param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@safety_index_bp.route('/control_intensity_index/weight', methods=['GET'])
def control_intensity_index_weight():
    """卡控力度指数的权重配置页面
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = control_intensity_index.get_weight_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/weight_preview', methods=['POST'])
def control_intensity_index_weight_preview():
    """调整完参数后的预览
    """
    param_dict = request.json
    data = control_intensity_index.get_weight_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/rank_preview', methods=['POST'])
def control_intensity_index_rank_preview():
    """调整完参数后的站段排名预览
    """
    param_dict = request.json
    data = control_intensity_index.get_rank_preview(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@safety_index_bp.route('/control_intensity_index/department', methods=['GET'])
def department_detail_control_intensity_data_bymonth():
    param_dict = request.args
    necessary_param = ['DPID', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    dpid = param_dict['DPID']
    mon = int(param_dict['MONTH'])
    data = control_intensity_index.get_department_detail_major_data(mon, dpid)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)
