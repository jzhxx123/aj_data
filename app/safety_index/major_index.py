#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import operator
from copy import deepcopy

import pandas as pd
import pymongo

from app import mongo
from app.data.util import filter_dianwu_laoan
from app.story.util import get_hierarchy_dpid
from app.utils.common_func import (choose_collection_prefix,
                                   get_history_months, get_major_dpid)
from app.utils.safety_index_common_func import (
    get_frontend_display_index_name, get_index_title,
    get_risk_child_index_name, get_risk_index_calc_tree,
    get_major_index_table_data_by_major, get_risk_type_name)

_MAJOR_RISK_TYPE_MAP = {
    # 供电
    "供电-1": "设备质量",
    "供电-3": "专业管理",
    "供电-6": "劳安",
    "供电-7": "施工安全",
    "供电-9": "自轮设备运用",
    '供电-10': "施工配合监管",
    # 电务
    "电务-5": "劳动安全",
    "电务-6": "施工安全",
    "电务-7": "道岔设备",
    "电务-8": "点外修",
    "电务-9": "轨道电路",
    "电务-10": "信号机",
    # 车务
    "车务-1": "调车",
    "车务-2": "劳动安全",
    "车务-6": "接发列车",
    "车务-7": "高动客接发列车",
    '车务-8': '货装安全',
    '车务-10': '调乘一体化',
    # 机务
    "机务-1": "间断瞭望",
    "机务-2": "调车风险",
    "机务-4": "错误操纵风险",
    "机务-5": "劳安风险",
    "机务-6": "机车质量风险",
    "机务-7": "库内牵车",
    # 车辆
    "车辆-1": "火灾爆炸",
    "车辆-2": "劳动安全",
    "车辆-5": "高压牵引",
    "车辆-8": "配件脱落(货车)",
    "车辆-9": "配件脱落(动车)",
    "车辆-10": "配件脱落(客车)",
    # 工务
    "工务-1": "防断风险",
    "工务-2": "劳动安全风险",
    "工务-3": "点外修作业风险",
    "工务-4": "防洪风险",
    "工务-5": "自轮设备风险",
    "工务-6": "施工安全风险",
    "工务-7": "施工风险",
    "工务-8": "天窗修风险",
    "工务-9": "施工监管风险",
    "工务-10": "站专线风险",
    "工务-11": "天窗施工单项",
    # 工电
    "工电-2": "施工安全风险",
    "工电-1": "劳动安全风险",
    "工电-3": "点外修作业风险",
    "工电-4": "天窗修作业风险",
    "工电-5": "施工作业风险",
    "工电-6": "施工安全风险(工务版)",
    "工电-7": "劳动安全风险(工务版)",
    "工电-8": "劳动安全风险(电务版)",
    "工电-9": "施工安全风险(电务版)",
    "工电-12": "天窗施工单项",
    # 客运
    "客运-1": '劳动安全风险',
    "客运-2": "客运组织及消防(车务站)",
    "客运-3": "客运组织及消防(客运段)",
    "客运-4": "客运组织及消防(客站)",
}

_MAJOR_MAP = {
    0: '供电',
    1: '车辆',
    2: '机务',
    3: '车务',
    4: '工务',
    5: '电务',
    6: '客运',
    7: '工电',
}


def _get_sorted_dp(data, last_mon_data):
    data = sorted(data, key=lambda x: x[4], reverse=True)
    return [{
        'dpid': item[0],
        'name': item[1],
        'rank': idx + 1,
        'value': item[4],
        'last_value': last_mon_data.get(item[0], 0),
    } for idx, item in enumerate(data)]


def _isspecial_risk_type(major, risk_type):
    """[判断是否为特殊指数类型,返回多个查询条件]

    Arguments:
        major {[str]} -- [description]
        risk_type {[int]} -- [description]

    Returns:
        [tuple] -- [提供判断的bool值以及条件]
    """
    static_special_risk_type_info = {

    }
    key = major + '-' + str(risk_type)
    init_condition = {"MAJOR": major,
                      "TYPE": risk_type}
    final_condition = list()
    if key in static_special_risk_type_info.keys():
        value = static_special_risk_type_info.get(key)
        final_condition = [{
            "MAJOR": item.split('-')[0],
            "TYPE": int(item.split('-')[1])
        } for item in value]
        final_condition.append(init_condition)
        return final_condition
    return [init_condition]


def _get_documents_by_conditions(condition_tpl, conditions, collection_name, required_keys):
    """[根据多个条件获取集合内容]

    Arguments:
        conditions {[list]} -- [条件列表]
        collection_name {[str]} -- [集合名称]
    """
    documents = list()
    for condition in conditions:
        real_condition = {**condition_tpl, **condition}
        tmp_documents = list(mongo.db[collection_name].find(
            real_condition, required_keys))
        documents += tmp_documents
    return documents


def get_rank_data(param_dict):
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    # 获取上个月年月YYYYmm
    if mon % 100 == 1:
        last_mon = mon - 100 + 11
    else:
        last_mon = mon - 1
    # 针对工务、电务、供电对工电段的特殊需要
    condition_tpl = {
        "MON": mon,
        "HIERARCHY": 3,
        "DETAIL_TYPE": 0,
        "TYPE": int(param_dict['RISK_TYPE']),
        "MAJOR": param_dict['MAJOR']
    }
    major = param_dict['MAJOR']
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition_tpl.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(mon)
    last_prefix = choose_collection_prefix(last_mon)
    conditions = _isspecial_risk_type(major, int(param_dict['RISK_TYPE']))
    major_detail_coll_name = f'{prefix}detail_major_index'
    required_keys = {
        "_id": 0,
        "MAIN_TYPE": 1,
        "DEPARTMENT_ID": 1,
        "DEPARTMENT_NAME": 1,
        "RANK": 1,
        "SCORE": 1,
    }
    documents = _get_documents_by_conditions(condition_tpl, conditions,
                                             major_detail_coll_name,
                                             required_keys)
    # documents = list(mongo.db[f'{prefix}detail_major_index'].find(
    #     condition, {
    #         "_id": 0,
    #         "MAIN_TYPE": 1,
    #         "DEPARTMENT_ID": 1,
    #         "DEPARTMENT_NAME": 1,
    #         "RANK": 1,
    #         "SCORE": 1,
    #     }))

    if len(documents) == 0:
        return f'NO DATA[{prefix}detail_major_index], \
            MONTH {param_dict["MONTH"]} invalid'

    rst = []
    last_major_detail_coll_name = f'{last_prefix}detail_major_index'
    last_required_keys = {
        "_id": 0,
        "DEPARTMENT_ID": 1,
        "SCORE": 1
    }

    last_mon_data = _get_documents_by_conditions(
        condition_tpl,
        [{**item, **{"MON": last_mon}} for item in conditions],
        last_major_detail_coll_name,
        last_required_keys)
    # last_mon_data = list(mongo.db[f'{last_prefix}detail_major_index'].find(
    #     {
    #         "MON": last_mon,
    #         "HIERARCHY": 3,
    #         "DETAIL_TYPE": 0,
    #         "TYPE": int(param_dict['RISK_TYPE']),
    #         "MAJOR": major,
    #     }, {
    #         "_id": 0,
    #         "DEPARTMENT_ID": 1,
    #         "SCORE": 1
    #     }))

    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }
    data = pd.DataFrame(documents)
    groups = data.groupby(['MAIN_TYPE'])

    for k, v in groups:
        vdata = _get_sorted_dp(v.values.tolist(), last_mon_data_dict)
        rst.append({'title': get_index_title(k), 'major': 1, 'data': vdata})

    del condition_tpl['DETAIL_TYPE']
    major_index_coll_name = f'{prefix}major_index'
    major_required_keys = {
        "_id": 0,
        "DEPARTMENT_ID": 1,
        "DEPARTMENT_NAME": 1,
        "RANK": 1,
        "SCORE": 1,
    }
    documents = _get_documents_by_conditions(condition_tpl,
                                             conditions,
                                             major_index_coll_name,
                                             major_required_keys)
    # documents = list(mongo.db[f'{prefix}major_index'].find(
    #     condition_tpl, {
    #         "_id": 0,
    #         "DEPARTMENT_ID": 1,
    #         "DEPARTMENT_NAME": 1,
    #         "RANK": 1,
    #         "SCORE": 1,
    #     }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}major_index], \
            MONTH {param_dict["MONTH"]} invalid'
    last_major_index_coll_name = f'{last_prefix}major_index'
    last_mon_data = _get_documents_by_conditions(
        condition_tpl,
        [{**item, **{"MON": last_mon}} for item in conditions],
        last_major_index_coll_name,
        last_required_keys
    )
    # last_mon_data = list(mongo.db[f'{last_prefix}major_index'].find(
    #     {
    #         "MON": last_mon,
    #         "HIERARCHY": 3,
    #         "MAJOR": major,
    #         "TYPE": int(param_dict['RISK_TYPE'])
    #     }, {
    #         "_id": 0,
    #         "DEPARTMENT_ID": 1,
    #         "SCORE": 1
    #     }))

    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }

    data = sorted(documents, key=lambda x: x['SCORE'], reverse=True)
    rst.append({
        'title':
            '总指数排行',
        'major':
            0,
        'data': [{
            'dpid':
                item['DEPARTMENT_ID'],
            'name':
                item['DEPARTMENT_NAME'],
            'rank':
                idx + 1,
            'value':
                item['SCORE'],
            'last_value':
                last_mon_data_dict.get(item['DEPARTMENT_ID'], 0),
        } for idx, item in enumerate(data)]
    })
    return rst


def _sort_score_by_mon(data):
    data = {item[2]: f'{item[4]:.2f}/{int(item[3])}' for item in data}
    rst = []
    global MON
    for x in MON:
        rst.append(data.get(x, '0/0'))
    return rst


def _get_mon(mon):
    mon.sort()
    rst = ["DATE"]
    global MON
    MON = mon
    mon = ['{}-{:0>2}'.format(item // 100, item % 100) for item in mon]
    rst.extend(mon)
    return rst


def get_department_detail_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {
        'MAJOR': param_dict['MAJOR'],
        "DEPARTMENT_ID": param_dict['DPID'],
        "TYPE": int(param_dict['TYPE']),
    }

    #
    rst_all = []
    rst_score = []
    mon = list(mongo.db['monthly_major_index'].distinct('MON'))
    rst_score.append(_get_mon(mon))
    # 子指数折线图加入安全综合指数（总体）的数据
    data_major_index = mongo.db['monthly_major_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_major_index))
    if len(data) == 0:
        return "NO DATA"
    data = {item[0]: f'{item[2]:.2f}/{int(item[1])}' for item in data.values}
    score_total_major_index = ['all']
    global MON
    for x in MON:
        score_total_major_index.append(data.get(x, '0/0'))
    rst_score.append(score_total_major_index)

    data_detail_health_index = mongo.db['monthly_detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "TYPE": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    # mon = list(mongo.db['monthly_detail_major_index'].distinct('MON'))
    data = pd.DataFrame(list(data_detail_health_index))
    # 将电务劳安的detail修改
    data = filter_dianwu_laoan(
        data, int(param_dict['TYPE']), get_major_dpid(param_dict['DPID']))

    if len(data) == 0:
        return "NO DATA"
    # rst_all = []
    groups = data[data['DETAIL_TYPE'] == 0].groupby(['MAIN_TYPE'])
    # rst_tmp = []
    # rst_score.append(_get_mon(mon))
    # 因前端显示问题修改main_type_set为{1,2,3,4,5,6}
    main_type_set = set([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14])
    for k, v in groups:
        # main_type_set.add(k)  同上
        rst_score.append([
            get_frontend_display_index_name(k), *(_sort_score_by_mon(v.values))
        ])
    rst_all.append(rst_score)
    main_type_set = sorted(main_type_set)
    for main_type in main_type_set:
        rst_score = []
        main_type = int(main_type)
        rst_score.append(_get_mon(mon))
        groups = data[data['MAIN_TYPE'] == main_type].groupby(['DETAIL_TYPE'])
        for k, v in groups:
            _title = get_risk_child_index_name(k, main_type)
            if _title is None:
                continue
            rst_score.append([_title, *(_sort_score_by_mon(v.values))])
        rst_all.append(rst_score)
    return rst_all


def get_calc_tree_data(param_dict):
    # mon = int(param_dict['MON'])
    if 'MONTH' not in param_dict:
        mon = get_history_months(-1)[0]
    else:
        mon = int(param_dict['MONTH'])
    index_type = int(param_dict['TYPE'])
    major = param_dict['MAJOR']
    risk_type = int(param_dict['RISK_TYPE'])
    dpid = param_dict['DPID']
    hierarchy = get_hierarchy_dpid(dpid)
    if hierarchy is None:
        return "DPID不存在"
    # 这里判断这个二级子指数是否在开发，应该判断是不是在数据里存了，
    # 取数展示，并不要知道是不是开发
    if index_type not in [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]:
        return "该指数还在开发中"
    rtn_html = {}
    if index_type == 0:
        tree_child = []
        for child_index_type in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]:
            child_rst = get_risk_index_calc_tree(dpid, mon, major, risk_type,
                                                 child_index_type)
            if child_rst is not None:
                tree_child.append(child_rst)
        rtn_html.update({'name': '重点风险指数', 'children': tree_child})
    else:
        rtn_html = get_risk_index_calc_tree(dpid, mon, major, risk_type,
                                            index_type)
    return rtn_html


def _combine_weight_table(major, risk_type, main_type, weight_data,
                          score_data, mon):
    grandson_indexes = []
    score_dict = {item['DETAIL_TYPE']: item['SCORE'] for item in score_data}
    for iweight in weight_data:
        detail_type = iweight['DETAIL_TYPE']
        # 获取孙子指数名称
        index_name = get_risk_child_index_name(detail_type, main_type)
        if index_name is None:
            continue
        grandson_indexes.append({
            'name': index_name,
            'weight': iweight['WEIGHT'],
            'score': score_dict.get(detail_type, 0),
            'detail_type': detail_type
        })

    rst_data = {
        'name':
            get_index_title(main_type),
        'main_type':
            main_type,
        'weight':
            mongo.db['base_index_weight'].find({
                'INDEX_TYPE': risk_type,
                'MAJOR': major,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            })[0]['WEIGHT'],
        'score':
            score_dict.get(0, 0),
        'grandson_indexes':
            grandson_indexes
    }
    return rst_data


def get_weight_data(param_dict):
    """安全综合指数【站段】各层级指数权重展示&调整配置

    Arguments:
        param_dict {dict} -- 参数
    """
    mon = int(param_dict['MONTH'])
    risk_type = int(param_dict['RISK_TYPE'])
    major = param_dict['MAJOR']
    prefix = choose_collection_prefix(mon)
    rst_child_data = []
    for main_type in [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14]:

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': risk_type,
                'MAIN_TYPE': main_type,
                "MON": mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'MAJOR': major,
                    'INDEX_TYPE': risk_type,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))

        # weight_data = list(mongo.db['base_detail_index_weight'].find(
        #     {
        #         'MAJOR': major,
        #         'INDEX_TYPE': risk_type,
        #         'MAIN_TYPE': main_type
        #     }, {
        #         '_id': 0,
        #         'WEIGHT': 1,
        #         'DETAIL_TYPE': 1
        #     }))
        # 判断该main_type指数是否存在
        if len(weight_data) < 1:
            continue
        score_data = list(mongo.db[f'{prefix}detail_major_index'].find(
            {
                'DEPARTMENT_ID': param_dict['DPID'],
                'MON': mon,
                'MAIN_TYPE': main_type,
                'TYPE': risk_type,
            }, {
                '_id': 0,
                'SCORE': 1,
                'DETAIL_TYPE': 1
            }))
        # 历史权重时判断有无数据
        if not score_data:
            continue
        rst_weight_table = _combine_weight_table(major, risk_type, main_type,
                                                 weight_data, score_data, mon)
        if rst_weight_table is not None:
            rst_child_data.append(rst_weight_table)
    rst_data = {
        'safety_index':
            mongo.db[f'{prefix}major_index'].find(
                {
                    'DEPARTMENT_ID': param_dict['DPID'],
                    'MON': mon,
                    'MAJOR': major,
                    'TYPE': risk_type,
                }, {
                    '_id': 0,
                    'SCORE': 1
                })[0]['SCORE'],
        'child_indexes':
            rst_child_data
    }
    return rst_data


def _validate_weight_keys(data, required_keys):
    """判断参数字典里是否缺失必选参数

    Arguments:
        data {dict} -- 参数
        required_keys {list} -- 必选参数

    Returns:
        [bool] --
    """
    for item in required_keys:
        if item not in data:
            raise KeyError(f'lack of {item}')
    return False


def get_weight_preview(param_dict):
    data = deepcopy(param_dict['DATA'])
    first_keys = ['safety_index', 'child_indexes']
    _validate_weight_keys(data, first_keys)
    second_keys = ['name', 'main_type', 'weight', 'score', 'grandson_indexes']
    third_keys = ['name', 'weight', 'score', 'detail_type']
    child_score = []
    for child_index in data['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        grandson_score = []
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_score.append(
                grandson_index['score'] * grandson_index['weight'])
        child_index['score'] = max(0, round(sum(grandson_score) * 100) / 100)
        child_score.append(child_index['score'] * child_index['weight'])
    data['safety_index'] = sum(child_score)
    return data


def _extract_weight(param):
    """提取指数权重

    Arguments:
        param {dict} -- 请求传入的参数
    """
    first_keys = ['child_indexes']
    _validate_weight_keys(param, first_keys)
    second_keys = ['main_type', 'weight', 'grandson_indexes']
    third_keys = ['weight', 'detail_type']
    main_weight = {}
    detail_weight = {}
    for child_index in param['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        main_weight.update({child_index['main_type']: child_index['weight']})
        grandson_weight = {}
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_weight.update({
                grandson_index['detail_type']:
                    grandson_index['weight']
            })
        detail_weight.update({child_index['main_type']: grandson_weight})
    return main_weight, detail_weight


def _calc_child_score(data, detail_weight):
    """重新计算子指数
    """
    old_child = []
    grandson_score = []
    for item in data:
        detail_type = item[2]
        if detail_type == 0:
            old_child = item
            continue
        elif detail_type in detail_weight:
            grandson_score.append(
                round(item[5], 2) * round(detail_weight[detail_type], 2))
    return {
        'DEPARTMENT_ID': old_child[0],
        'DEPARTMENT_NAME': old_child[1],
        'MAIN_TYPE': old_child[3],
        'SCORE': old_child[5],
        'SCORE_NEW': max(0, round(sum(grandson_score) * 100) / 100)
    }


def _sort_zhanduan_rank(data):
    data = sorted(data, key=lambda x: x[4], reverse=True)
    return [{
        'dpid': item[0],
        'name': item[1],
        'value': item[3],
        'rank_after': idx + 1,
        'value_after': round(item[4], 2),
    } for idx, item in enumerate(data)]


def get_rank_preview(param_dict):
    weight_param = deepcopy(param_dict['DATA'])
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    major = param_dict['MAJOR']
    condition = {
        "MON": mon,
        "HIERARCHY": 3,
        'MAJOR': param_dict['MAJOR'],
        "TYPE": int(param_dict['RISK_TYPE'])
    }
    main_weight, detail_weight = _extract_weight(weight_param)
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运']:
        condition.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(mon)
    documents = list(mongo.db[f'{prefix}detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}detail_major_index], \
            MONTH {param_dict["MONTH"]} invalid'

    rst_data = []
    data = pd.DataFrame(documents)
    new_detail_data = []
    for k, v in data.groupby(['DEPARTMENT_ID', 'MAIN_TYPE']):
        detail_score = v.values.tolist()
        new_detail_data.append(
            _calc_child_score(detail_score, detail_weight[k[1]]))
    for k, v in pd.DataFrame(new_detail_data).groupby('MAIN_TYPE'):
        vdata = _sort_zhanduan_rank(v.values.tolist())
        rst_data.append({
            'title': get_index_title(k),
            'major': 1,
            'data': vdata
        })
    # 增加整体排名
    documents = list(mongo.db[f'{prefix}major_index'].find(
        condition, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}major_index], \
            MONTH {param_dict["MONTH"]} invalid'

    new_data = _calc_index_score(new_detail_data, documents, main_weight)
    rst_data.append(new_data)
    return rst_data


def _calc_index_score(child_score, index_data, main_weight):
    new_child_score = {}
    for item in child_score:
        dpid = item['DEPARTMENT_ID']
        new_score = round(main_weight[item['MAIN_TYPE']],
                          2) * item['SCORE_NEW']
        new_child_score.update({
            dpid: new_child_score.get(dpid, 0) + new_score
        })
    new_data = []
    for item in index_data:
        item.update({'SCORE_NEW': new_child_score.get(item['DEPARTMENT_ID'])})
        new_data.append(item)
    data = sorted(new_data, key=lambda x: x['SCORE_NEW'], reverse=True)
    rst_data = {
        'title':
            '总指数排行',
        'major':
            0,
        'data': [{
            'dpid': item['DEPARTMENT_ID'],
            'name': item['DEPARTMENT_NAME'],
            'value': item['SCORE'],
            'rank_after': idx + 1,
            'value_after': round(item['SCORE_NEW'] * 100) / 100,
        } for idx, item in enumerate(data)]
    }
    return rst_data


def _get_major_all_risk_type(coll_prefix, condition):
    """

    :param coll_prefix:
    :param condition:
    :return:
    """
    del condition['DETAIL_TYPE']
    risk_type_list = list(
        mongo.db[f'{coll_prefix}major_index'].distinct("TYPE", condition))
    return risk_type_list


def get_index_table_data(param_dict):
    """
    按专业展示各站段的不同风险指数得分和6个分指数得分
    Arguments:
        param_dict {dict} -- MONTH：月份， MAJOR：专业
    :param param_dict:
    :return:
    """

    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 3, "DETAIL_TYPE": 0}
    major = param_dict['MAJOR']
    if major in ["综合", '供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    data_rst = []
    coll_prefix = choose_collection_prefix(mon)
    risk_type_list = _get_major_all_risk_type(
        coll_prefix,
        condition.copy()
    )
    if risk_type_list:
        for risk_type in risk_type_list:
            # 获取该专业的各个风险指数信息
            condition.update({'TYPE': risk_type})
            risk_name = get_risk_type_name(param_dict['MAJOR'], risk_type)
            risk_data = get_major_index_table_data_by_major(
                coll_prefix, condition.copy())
            if risk_data:
                data_rst.append(
                    {"risk_name": risk_name, "risk_data": risk_data})
    return data_rst


def _translate_special_risktype_to_normal(mon, dpid, major, risk_type):
    """[将特殊的重点指数类型转换成正常]
    例如车务劳安指数对应贵阳车站来说，应该是属于客运专业的劳安，
    前端风险筛选器根据部门专业筛选
    Arguments:
        major {[str]} -- [description]
        risk_type {[int]} -- [description]
    """
    prefix = choose_collection_prefix(mon)
    # 相应指数映射表
    static_special_risk_type_info = {
        "车务-2": "客运-1",
        "工务-2": "工电-1",
        "工务-3": "工电-3",
        "工务-6": "工电-2",
        "工务-7": "工电-5",
        "工务-8": "工电-4",
    }

    if major in ['工务', '车务']:
        vitual_major_map = {
            "工务": "工电",
            "车务": "客运"
        }

        key = major + '-' + str(risk_type)
        real_risk_type = int(static_special_risk_type_info.get(key, key)[3:5])
        vitual_major_ids = list(mongo.db[f'{prefix}major_index'].find({"MON": mon,
                                                                       "MAJOR": vitual_major_map.get(major),
                                                                       "TYPE": real_risk_type},
                                                                      {'_id': 0, "DEPARTMENT_ID": 1}))
        print(vitual_major_ids)
        ispecial = True if dpid in [item['DEPARTMENT_ID']
                                    for item in vitual_major_ids] else False
        if ispecial:
            real_major = static_special_risk_type_info.get(key, key)
            major, risk_type = real_major.split(
                '-')[0], int(real_major.split('-')[1])
    return major, risk_type


# 获取雷达图信息
def get_department_detail_radar_data(mon, dpid, risk_type, major):
    from app.utils.common_func import get_major_dpid
    prefix = choose_collection_prefix(mon)
    # major = get_major_dpid(dpid)
    # 针对特殊站段，需要判断是否为工电或者客运，避免相同risk_type数据查询有误
    # major, risk_type = _translate_special_risktype_to_normal(
    #     mon, dpid, major, risk_type)
    radar_labels = list(mongo.db[f"{prefix}detail_major_index"].find({"DETAIL_TYPE": 0, "MON": mon,
                                                                      "DEPARTMENT_ID": dpid,
                                                                      "MAJOR": major,
                                                                      "TYPE": risk_type},
                                                                     {'_id': 0, "MAIN_TYPE": 1, "SCORE": 1}))
    rst_data = {}
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE'])
                  for label in radar_labels]
        data = []
        subtags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            data_tmp = list(mongo.db[f'{prefix}detail_major_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major,
                 "TYPE": risk_type},
                {"_id": 0, "SCORE": 1, "RANK": 1}))
            most_data[main_type] = [i['SCORE']
                                    for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE']
                                       for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        rst_data['labels'] = labels
        rst_data['data'] = data
        rst_data['subtags'] = subtags
        return rst_data
    return ''


# 根据月份获取部门详细重点指数信息
def get_department_detail_major_data(mon, dpid, risk_type, major):
    from app.utils.common_func import get_major_dpid
    prefix = choose_collection_prefix(mon)
    rst_data = {}
    # major = get_major_dpid(dpid)
    # 总指数标签
    # 针对特殊站段，需要判断是否为工电或者客运，避免相同risk_type数据查询有误
    # major, risk_type = _translate_special_risktype_to_normal(
    #     mon, dpid, major, risk_type)
    main_labels = list(mongo.db[f"{prefix}detail_major_index"].find({"DETAIL_TYPE": 0, "MON": mon,
                                                                     "DEPARTMENT_ID": dpid,
                                                                     "MAJOR": major,
                                                                     "TYPE": risk_type},
                                                                    {'_id': 0, "MAIN_TYPE": 1, "SCORE": 1}))
    if not main_labels:
        return {"data": [
            [[], []],
            [[], []],
            [[], []],
            [[], []],
            [[], []],
            [[], []],
            [[], []]],
            "subtags": [
                "检查力度指数",
                "评价力度指数",
                "考核力度指数",
                "检查均衡度指数",
                "问题暴露指数",
                "问题整改效果指数"
            ]}
    total = list(mongo.db[f"{prefix}major_index"].find({"MON": mon, "DEPARTMENT_ID": dpid,
                                                        "MAJOR": major,
                                                        "TYPE": risk_type}))[0][
        'SCORE']
    # 按main_type重新排序
    main_labels.sort(key=operator.itemgetter('MAIN_TYPE'))
    if main_labels:
        m_labels_lest = [get_index_title(label['MAIN_TYPE'])
                         for label in main_labels]
        m_labels = ['总指数'] + m_labels_lest
        m_data = [total]
        m_data.extend([score['SCORE'] for score in main_labels])
        data = []
        data.append([m_data, m_labels])
        for main_type in [label['MAIN_TYPE'] for label in main_labels]:
            d_labels = []
            d_data = []
            detail_labels = list(mongo.db[f'{prefix}major_index_basic_data'].find(
                {"MON": mon, "DEPARTMENT_ID": dpid, "INDEX_TYPE": risk_type,
                 "MAJOR": major,
                 "MAIN_TYPE": main_type}))
            detail_score = list(mongo.db[f'{prefix}detail_major_index'].find({"MON": mon, "DEPARTMENT_ID": dpid,
                                                                              "MAIN_TYPE": main_type,
                                                                              "MAJOR": major,
                                                                              "TYPE": risk_type},
                                                                             {"_id": 0, "SCORE": 1, "MAIN_TYPE": 1,
                                                                              "DETAIL_TYPE": 1}))
            for detail in detail_labels:
                d_labels.append(get_risk_child_index_name(
                    detail['DETAIL_TYPE'], main_type))
                score = 0
                for item in detail_score:
                    if int(item['DETAIL_TYPE']) == int(detail['DETAIL_TYPE']):
                        score = item['SCORE']
                        break
                    else:
                        pass
                if detail['TYPE'] == 1:
                    # 处理2月份之前版本指数，没有专业平均分
                    if 'AVG_QUOTIENT' not in detail.keys():
                        avg_quotient = detail['AVG_SCORE']
                        avg_score = '暂无'
                    else:
                        avg_quotient = detail['AVG_QUOTIENT']
                else:
                    avg_quotient = '暂无'
                d_data.append(str(score) + '/' + str(avg_quotient))
            data.append([d_data, d_labels])
        rst_data['subtags'] = m_labels_lest
        rst_data['data'] = data
        return rst_data
    return ''


def get_zhanduan_index_by_dpid(dpid, mon):
    """
    根据月份，部门获取指数类型
    :param dpid:
    :param mon:
    :return:{label: "点外修作业风险(工务)", major: 3}
    """
    prefix = choose_collection_prefix(mon)
    zhanduan_index_list = list(
        mongo.db[f'{prefix}major_index'].find(
            {
                "MON": mon,
                "DEPARTMENT_ID": dpid
            }
            , {
                "_id": 0,
                "MAJOR": 1,
                "TYPE": 1
            }
        ).sort([('MAJOR', pymongo.ASCENDING), ('TYPE', pymongo.ASCENDING)])
    )
    major_kinds = set([item['MAJOR'] for item in zhanduan_index_list])
    # zhanduan_index_set = set([item['MAJOR'] + '-' + str(item['TYPE'])
    #                           for item in zhanduan_index_list])
    major_index_list = [
        item['MAJOR'] + '-' + str(item['TYPE']) for item in zhanduan_index_list]
    rst_data = list()
    for item in major_index_list:
        major = item.split('-')[0]
        risk_type = item.split('-')[1]
        label = _MAJOR_RISK_TYPE_MAP.get(item, None)
        if not label:
            continue
        if len(major_kinds) > 1:
            label += f'({major})'
        tmp_data = {
            'label': label,
            'major': major,
            'risk_type': risk_type
        }
        rst_data.append(tmp_data)
    return rst_data
