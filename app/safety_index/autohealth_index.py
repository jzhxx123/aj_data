#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from copy import deepcopy

import pandas as pd
from app.story.util import get_hierarchy_dpid
from app import mongo
from app.utils.common_func import choose_collection_prefix, get_history_months
from app.utils.safety_index_common_func import (
    get_child_index_name, get_health_index_calc_tree,
    get_index_table_data_by_major, get_index_title, is_exist_dpid,
    get_frontend_display_index_name)


def _get_sorted_dp(data, last_mon_data):
    data = sorted(data, key=lambda x: x[4], reverse=True)
    return [{
        'dpid': item[0],
        'name': item[1],
        'rank': idx + 1,
        'value': item[4],
        'last_value': last_mon_data.get(item[0], 0),
    } for idx, item in enumerate(data)]


def get_rank_data(param_dict):
    """按专业（综合）展示各站段的指数得分排名和6个分指数得分排名
    Arguments:
        param_dict {dict} -- MONTH：月份， MAJOR：专业
    """
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    # 获取上个月年月YYYYmm
    if mon % 100 == 1:
        last_mon = mon - 100 + 11
    else:
        last_mon = mon - 1
    condition = {"MON": mon, "HIERARCHY": 3, "DETAIL_TYPE": 0}
    major = param_dict['MAJOR']
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(mon)
    last_prefix = choose_collection_prefix(last_mon)
    documents = list(mongo.db[f'{prefix}detail_autohealth_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_autohealth_index], MONTH \
            {param_dict["MONTH"]} invalid'

    rst = []
    last_mon_data = list(mongo.db[f'{last_prefix}detail_autohealth_index'].find(
        {
            "MON": last_mon,
            "HIERARCHY": 3,
            "DETAIL_TYPE": 0,
            "MAJOR": major,
        }, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "SCORE": 1
        }))
    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }
    data = pd.DataFrame(documents)
    groups = data.groupby(['MAIN_TYPE'])
    for k, v in groups:
        vdata = _get_sorted_dp(v.values.tolist(), last_mon_data_dict)
        rst.append({'title': get_index_title(k), 'major': 1, 'data': vdata})

    # 增加整体排名
    documents = list(mongo.db[f'{prefix}autohealth_index'].find(
        {
            'MAJOR': major,
            'MON': mon,
            'HIERARCHY': 3,
        }, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_autohealth_index], MONTH \
            {param_dict["MONTH"]} invalid'

    last_mon_data = list(mongo.db[f'{last_prefix}autohealth_index'].find(
        {
            "MON": last_mon,
            "HIERARCHY": 3,
            "MAJOR": major,
        }, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "SCORE": 1
        }))
    last_mon_data_dict = {
        item["DEPARTMENT_ID"]: item["SCORE"]
        for item in last_mon_data
    }
    data = sorted(documents, key=lambda x: x['SCORE'], reverse=True)
    rst.append({
        'title':
        '总指数排行',
        'major':
        0,
        'data': [{
            'dpid':
            item['DEPARTMENT_ID'],
            'name':
            item['DEPARTMENT_NAME'],
            'rank':
            idx + 1,
            'value':
            item['SCORE'],
            'last_value':
            last_mon_data_dict.get(item['DEPARTMENT_ID'], 0),
        } for idx, item in enumerate(data)]
    })
    return rst


def _sort_score_by_mon(data):
    data = {item[2]: f'{item[4]:.2f}/{int(item[3])}' for item in data}
    rst = []
    global MON
    for x in MON:
        rst.append(data.get(x, '0/0'))
    return rst


def _get_mon(mon):
    mon.sort()
    rst = ["DATE"]
    global MON
    MON = mon
    mon = ['{}-{:0>2}'.format(item // 100, item % 100) for item in mon]
    rst.extend(mon)
    return rst


def get_department_detail_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {"DEPARTMENT_ID": param_dict['DPID'], "HIERARCHY": 3}
    rst_all = []
    rst_score = []
    mon = list(mongo.db['monthly_autohealth_index'].distinct('MON'))
    rst_score.append(_get_mon(mon))
    # 子指数折线图加入安全综合指数（总体）的数据
    data_health_index = mongo.db['monthly_autohealth_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_health_index))
    if len(data) == 0:
        return "NO DATA"
    data = {item[0]: f'{item[2]:.2f}/{int(item[1])}' for item in data.values}
    score_total_health_index = ['all']
    global MON
    for x in MON:
        score_total_health_index.append(data.get(x, '0/0'))
    rst_score.append(score_total_health_index)
    # 各子指数历史12个月趋势图
    data_detail_health_index = mongo.db['monthly_detail_autohealth_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_detail_health_index))
    if len(data) == 0:
        return "NO DATA"
    groups = data[data['DETAIL_TYPE'] == 0].groupby(['MAIN_TYPE'])
    # 按格式输出
    for k, v in groups:
        rst_score.append([
            get_frontend_display_index_name(k), *(_sort_score_by_mon(v.values))
        ])
    rst_all.append(rst_score)

    # 各子指数的分指数历史12个月趋势图
    for main_type in range(1, 7):
        rst_score = []
        rst_score.append(_get_mon(mon))
        groups = data[data['MAIN_TYPE'] == main_type].groupby(['DETAIL_TYPE'])
        for k, v in groups:
            _title = get_child_index_name(k, main_type)
            if _title is None:
                continue
            rst_score.append([_title, *(_sort_score_by_mon(v.values))])
        rst_all.append(rst_score)
    return rst_all


def get_calc_tree_data(param_dict):
    if 'MONTH' not in param_dict:
        mon = get_history_months(-1)[0]
    else:
        mon = int(param_dict['MONTH'])
    index_type = int(param_dict['TYPE'])
    dpid = param_dict['DPID']
    if not is_exist_dpid(dpid):
        return "DPID不存在"
    if index_type not in [0, 1, 2, 3, 4, 5, 6]:
        return "该指数还在开发中"
    rtn_html = {}
    if index_type == 0:
        tree_child = []
        for child_index_type in [1, 2, 3, 4, 5, 6]:
            child_data = get_health_index_calc_tree(dpid, mon,
                                                    child_index_type, index_flag=-1)
            if child_data is not None:
                tree_child.append(child_data)
        rtn_html.update({'name': '安全综合指数', 'children': tree_child})
    else:
        rtn_html = get_health_index_calc_tree(dpid, mon, index_type, index_flag=-1)

    return rtn_html


def _combine_weight_table(main_type, weight_data, score_data, mon):
    grandson_indexes = []
    score_dict = {item['DETAIL_TYPE']: item['SCORE'] for item in score_data}
    for iweight in weight_data:
        detail_type = iweight['DETAIL_TYPE']
        # 获取孙子指数名称
        index_name = get_child_index_name(detail_type, main_type)
        if index_name is None:
            continue
        grandson_indexes.append({
            'name': index_name,
            'weight': iweight['WEIGHT'],
            'score': score_dict.get(detail_type, 0),
            'detail_type': detail_type
        })
    weight = mongo.db['base_index_weight'].find({
            'INDEX_TYPE': -1,
            'MAIN_TYPE': main_type,
        }, {
            '_id': 0,
            'WEIGHT': 1
        })[0]['WEIGHT']
    if not weight:
        weight = mongo.db['base_index_weight'].find({
            'INDEX_TYPE': -1,
            'MAIN_TYPE': main_type
        }, {
            '_id': 0,
            'WEIGHT': 1
        })[0]['WEIGHT']

    rst_data = {
        'name':
        get_index_title(main_type),
        'main_type':
        main_type,
        'weight': weight,
        # mongo.db['base_index_weight'].find({
        #     'INDEX_TYPE': 0,
        #     'MAIN_TYPE': main_type
        # }, {
        #     '_id': 0,
        #     'WEIGHT': 1
        # })[0]['WEIGHT'],
        'score':
        score_dict.get(0, 0),
        'grandson_indexes':
        grandson_indexes
    }
    return rst_data


def get_index_table_data(param_dict):
    """按专业（综合）展示各站段的指数得分和6个分指数得分
    Arguments:
        param_dict {dict} -- MONTH：月份， MAJOR：专业
    """
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 3, "DETAIL_TYPE": 0}
    major = param_dict['MAJOR']
    if major in ["综合", '供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    data_rst = []
    coll_prefix = choose_collection_prefix(mon)
    major_list = [major]
    if major == '综合':
        major_list = ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']
    # 获取子指数得分
    for major in major_list:
        condition.update({'MAJOR': major})
        major_data = get_index_table_data_by_major(coll_prefix,
                                                   condition.copy(), index_type=-1)
        if major_data != []:
            data_rst.append({'major_name': major, 'major_data': major_data})
    return data_rst


def get_weight_data(param_dict):
    """安全综合指数【站段】各层级指数权重展示&调整配置

    Arguments:
        param_dict {dict} -- 参数
    """
    mon = int(param_dict['MONTH'])
    prefix = choose_collection_prefix(mon)
    rst_child_data = []
    for main_type in [1, 2, 3, 4, 5, 6]:
        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'INDEX_TYPE': -1,
                'MAIN_TYPE': main_type,
                'MON': mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'INDEX_TYPE': -1,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))
        # weight_data = list(mongo.db['base_detail_index_weight'].find(
        #     {
        #         'INDEX_TYPE': 0,
        #         'MAIN_TYPE': main_type
        #     }, {
        #         '_id': 0,
        #         'WEIGHT': 1,
        #         'DETAIL_TYPE': 1
        #     }))
        score_data = list(mongo.db[f'{prefix}detail_autohealth_index'].find(
            {
                'DEPARTMENT_ID': param_dict['DPID'],
                'MON': mon,
                'MAIN_TYPE': main_type,
            }, {
                '_id': 0,
                'SCORE': 1,
                'DETAIL_TYPE': 1
            }))
        rst_weight_table = _combine_weight_table(main_type, weight_data,
                                                 score_data, mon)
        if rst_weight_table is not None:
            rst_child_data.append(rst_weight_table)
    rst_data = {
        'safety_index':
        mongo.db[f'{prefix}autohealth_index'].find(
            {
                'DEPARTMENT_ID': param_dict['DPID'],
                'MON': mon
            }, {
                '_id': 0,
                'SCORE': 1
            })[0]['SCORE'],
        'child_indexes':
        rst_child_data
    }
    return rst_data


def _validate_weight_keys(data, required_keys):
    """判断参数字典里是否缺失必选参数

    Arguments:
        data {dict} -- 参数
        required_keys {list} -- 必选参数

    Returns:
        [bool] --
    """
    for item in required_keys:
        if item not in data:
            raise KeyError(f'lack of {item}')
    return False


def get_weight_preview(param_dict):
    data = deepcopy(param_dict['DATA'])
    first_keys = ['safety_index', 'child_indexes']
    _validate_weight_keys(data, first_keys)
    second_keys = ['name', 'main_type', 'weight', 'score', 'grandson_indexes']
    third_keys = ['name', 'weight', 'score', 'detail_type']
    child_score = []
    for child_index in data['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        grandson_score = []
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_score.append(
                grandson_index['score'] * grandson_index['weight'])
        child_index['score'] = max(0, round(sum(grandson_score) * 100) / 100)
        child_score.append(child_index['score'] * child_index['weight'])
    data['safety_index'] = sum(child_score)
    return data


def _extract_weight(param):
    """提取指数权重

    Arguments:
        param {dict} -- 请求传入的参数
    """
    first_keys = ['child_indexes']
    _validate_weight_keys(param, first_keys)
    second_keys = ['main_type', 'weight', 'grandson_indexes']
    third_keys = ['weight', 'detail_type']
    main_weight = {}
    detail_weight = {}
    for child_index in param['child_indexes']:
        _validate_weight_keys(child_index, second_keys)
        main_weight.update({child_index['main_type']: child_index['weight']})
        grandson_weight = {}
        for grandson_index in child_index['grandson_indexes']:
            _validate_weight_keys(grandson_index, third_keys)
            grandson_weight.update({
                grandson_index['detail_type']:
                grandson_index['weight']
            })
        detail_weight.update({child_index['main_type']: grandson_weight})
    return main_weight, detail_weight


def _calc_child_score(data, detail_weight):
    """重新计算子指数
    """
    old_child = []
    grandson_score = []
    for item in data:
        detail_type = item[2]
        if detail_type == 0:
            old_child = item
            continue
        elif detail_type in detail_weight:
            grandson_score.append(
                round(item[5], 2) * round(detail_weight[detail_type], 2))
    return {
        'DEPARTMENT_ID': old_child[0],
        'DEPARTMENT_NAME': old_child[1],
        'MAIN_TYPE': old_child[3],
        'SCORE': old_child[5],
        'SCORE_NEW': max(0,
                         round(sum(grandson_score) * 100) / 100)
    }


def _sort_zhanduan_rank(data):
    data = sorted(data, key=lambda x: x[4], reverse=True)
    return [{
        'dpid': item[0],
        'name': item[1],
        'value': item[3],
        'rank_after': idx + 1,
        'value_after': round(item[4], 2),
    } for idx, item in enumerate(data)]


def get_rank_preview(param_dict):
    weight_param = deepcopy(param_dict['DATA'])
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    major = param_dict['MAJOR']
    condition = {"MON": mon, "HIERARCHY": 3}
    main_weight, detail_weight = _extract_weight(weight_param)
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(mon)
    documents = list(mongo.db[f'{prefix}detail_health_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    rst_data = []
    data = pd.DataFrame(documents)
    new_detail_data = []
    for k, v in data.groupby(['DEPARTMENT_ID', 'MAIN_TYPE']):
        detail_score = v.values.tolist()
        new_detail_data.append(
            _calc_child_score(detail_score, detail_weight[k[1]]))
    for k, v in pd.DataFrame(new_detail_data).groupby('MAIN_TYPE'):
        vdata = _sort_zhanduan_rank(v.values.tolist())
        rst_data.append({
            'title': get_index_title(k),
            'major': 1,
            'data': vdata
        })
    # 增加整体排名
    documents = list(mongo.db[f'{prefix}health_index'].find(
        {
            'MAJOR': major,
            'MON': mon,
            'HIERARCHY': 3,
        }, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    new_data = _calc_index_score(new_detail_data, documents, main_weight)
    rst_data.append(new_data)
    return rst_data


def _calc_index_score(child_score, index_data, main_weight):
    new_child_score = {}
    for item in child_score:
        dpid = item['DEPARTMENT_ID']
        new_score = round(main_weight[item['MAIN_TYPE']],
                          2) * item['SCORE_NEW']
        new_child_score.update({
            dpid: new_child_score.get(dpid, 0) + new_score
        })
    new_data = []
    for item in index_data:
        item.update({'SCORE_NEW': new_child_score.get(item['DEPARTMENT_ID'])})
        new_data.append(item)
    data = sorted(new_data, key=lambda x: x['SCORE_NEW'], reverse=True)
    rst_data = {
        'title':
        '总指数排行',
        'major':
        0,
        'data': [{
            'dpid': item['DEPARTMENT_ID'],
            'name': item['DEPARTMENT_NAME'],
            'value': item['SCORE'],
            'rank_after': idx + 1,
            'value_after': round(item['SCORE_NEW'] * 100) / 100,
        } for idx, item in enumerate(data)]
    }
    return rst_data


# 获取雷达图信息
def get_department_detail_radar_data(mon, dpid):
    from app.utils.common_func import get_major_dpid
    prefix = choose_collection_prefix(mon)
    radar_labels = list(mongo.db[f"{prefix}detail_autohealth_index"].find({"DETAIL_TYPE": 0, "MON": mon,
                                                                       "DEPARTMENT_ID": dpid},
                                                                      {'_id': 0, "MAIN_TYPE": 1, "SCORE": 1}))
    rst_data = {}
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE']) for label in radar_labels]
        data = []
        subtags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            major = get_major_dpid(dpid)
            data_tmp = list(mongo.db[f'{prefix}detail_autohealth_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major},
                {"_id": 0, "SCORE": 1, "RANK": 1}))
            most_data[main_type] = [i['SCORE'] for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE'] for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        rst_data['labels'] = labels
        rst_data['data'] = data
        rst_data['subtags'] = subtags
        return rst_data
    return ''
