import sys
import os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)
from app.new_three.mongo_util import init_db
from dotenv import find_dotenv, load_dotenv


def monthly_data_handler(mongo_result):
    _date = []
    _value = []
    for result in mongo_result:
        _date.append(result["_id"])
        _value.append(result["amount"])

    return _date, _value


def get_the_daily_data_from_mongo(detail_type, dpid=None, start=None,
                                  end=None):
    load_dotenv(find_dotenv())
    mongo_uri = os.environ.get('MONGO_URI')
    db = init_db(uri=mongo_uri)
    # print(222222222222, dpid, start, end)
    if dpid and start and end:
        start = int(start)
        end = int(end)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$lte": end, "$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid and start:
        start = int(start)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid and end:
        end = int(end)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$lte": end}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif start and end:
        start = int(start)
        end = int(end)
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$lte": end, "$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid:
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif start:
        start = int(start)
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif end:
        end = int(end)
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$lte": end}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    else:
        data1 = db.group(
            "daily_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    result = monthly_data_handler(data1)
    db.close()
    return result


def get_the_monthly_data_from_mongo(detail_type, dpid=None, start=None,
                                    end=None):
    load_dotenv(find_dotenv())
    mongo_uri = os.environ.get('MONGO_URI')
    db = init_db(uri=mongo_uri)
    # print(222222222222, dpid, start, end)
    if dpid and start and end:
        start = int(start)
        end = int(end)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$lte": end, "$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid and start:
        start = int(start)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid and end:
        end = int(end)
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
                "MON": {"$lte": end}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif start and end:
        start = int(start)
        end = int(end)
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$lte": end, "$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif dpid:
        hierarchy = get_the_hierarchy_of_DPID(dpid)[0]["HIERARCHY"]
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                f"TYPE{hierarchy}": dpid,
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif start:
        start = int(start)
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$gte": start}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    elif end:
        end = int(end)
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
                "MON": {"$lte": end}
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    else:
        data1 = db.group(
            "monthly_detail_safety_produce_info",
            {
                "DETAIL_TYPE": detail_type,
            },
            {
                "_id": "$MON",
                "amount": {"$sum": 1}
            },
            {"_id": 1},
        )
    result = monthly_data_handler(data1)
    db.close()
    return result


def get_the_correct_date_and_the_correct_value(dpid=None, start=None,
                                               end=None):
    drive_date = get_the_monthly_data_from_mongo(1, dpid, start, end)[0]
    work_date = get_the_monthly_data_from_mongo(2, dpid, start, end)[0]
    road_date = get_the_monthly_data_from_mongo(3, dpid, start, end)[0]
    drive_value = get_the_monthly_data_from_mongo(1, dpid, start, end)[1]
    work_value = get_the_monthly_data_from_mongo(2, dpid, start, end)[1]
    road_value = get_the_monthly_data_from_mongo(3, dpid, start, end)[1]

    # 先补日期和值位
    work_date, work_value = \
        supplement(drive_date, work_date, work_value)
    road_date, road_value = \
        supplement(drive_date, road_date, road_value)
    drive_date, drive_value = \
        supplement(work_date, drive_date, drive_value)
    road_date, road_value = \
        supplement(work_date, road_date, road_value)
    drive_date, drive_value = \
        supplement(road_date, drive_date, drive_value)
    work_date, work_value = \
        supplement(road_date, work_date, work_value)

    # 再排序
    drive_date, drive_value = sort_ab(drive_date, drive_value)
    work_date, work_value = sort_ab(work_date, work_value)
    road_date, road_value = sort_ab(road_date, road_value)

    return drive_date, drive_value, work_value, road_value


def get_the_hierarchy_of_DPID(dpid):
    load_dotenv(find_dotenv())
    mongo_uri = os.environ.get('MONGO_URI')
    db = init_db(uri=mongo_uri)
    data1 = db.find(
        "base_department",
        {
            "DEPARTMENT_ID": dpid
        },
        {
            "_id": 0,
            "HIERARCHY": 1
        },
    )
    db.close()
    return data1


def get_the_correct_date_and_the_correct_value_daily(dpid=None, start=None,
                                                     end=None):
    drive_date = get_the_daily_data_from_mongo(1, dpid, start, end)[0]
    work_date = get_the_daily_data_from_mongo(2, dpid, start, end)[0]
    road_date = get_the_daily_data_from_mongo(3, dpid, start, end)[0]
    drive_value = get_the_daily_data_from_mongo(1, dpid, start, end)[1]
    work_value = get_the_daily_data_from_mongo(2, dpid, start, end)[1]
    road_value = get_the_daily_data_from_mongo(3, dpid, start, end)[1]

    # 先补日期和值位
    work_date, work_value = \
        supplement(drive_date, work_date, work_value)
    road_date, road_value = \
        supplement(drive_date, road_date, road_value)
    drive_date, drive_value = \
        supplement(work_date, drive_date, drive_value)
    road_date, road_value = \
        supplement(work_date, road_date, road_value)
    drive_date, drive_value = \
        supplement(road_date, drive_date, drive_value)
    work_date, work_value = \
        supplement(road_date, work_date, work_value)

    # 再排序
    drive_date, drive_value = sort_ab(drive_date, drive_value)
    work_date, work_value = sort_ab(work_date, work_value)
    road_date, road_value = sort_ab(road_date, road_value)

    return drive_date, drive_value, work_value, road_value


# 两个列表a, b。a排序后，b按照a的排序状态排序
def sort_ab(li1, li2):
    li3 = sorted(li1)
    li4 = []
    for i in range(len(li1)):
        loc = li3.index(li1[i])
        li4.insert(loc, li2[i])
    return li3, li4


# 参照drive_date补work_date的日期和值位
def supplement(drive_date, work_date, work_value):
    for i in range(len(drive_date)):
        if drive_date[i] not in work_date:
            work_date.append(drive_date[i])
            work_value.append(0)
    return work_date, work_value


# if __name__ == "__main__":
#     print(get_the_monthly_data_from_mongo(
#         3, "19B8C3534E275665E0539106C00A58FD")[0])
#     print(get_the_monthly_data_from_mongo(
#         3, "19B8C3534E275665E0539106C00A58FD")[1])
#     print(get_the_correct_date_and_the_correct_value_daily())
