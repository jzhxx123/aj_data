from pymongo import MongoClient
from random import randrange as rr, choice


class MongoDB:

    def createMongoConnection(self, db_name="", ip_addr="localhost", port=27017,
                              username="", password="", uri=""):
        ''' 创建Mongo数据库连接 '''
        self.db_name = db_name
        self.ip_addr = ip_addr
        self.port = port
        if not uri:
            # print("连接%s:%d   数据库:%s" % (self.ip_addr, self.port, db_name))
            self.conn = MongoClient(ip_addr, port)
            self.db = self.conn[self.db_name]
        else:
            self.conn = MongoClient(uri)
            self.db = self.conn.get_default_database()
        if username and password:
            self.db.authenticate(username, password)

    def clear(self, set_name):
        ''' 清除集合数据 '''
        # print("%s.%s.remove()" % (self.db_name, set_name))
        account = self.db.get_collection(set_name)
        account.remove()

    def insert(self, set_name, args):
        ''' 插入集合数据 '''
        # print("%s.%s.insert(%s)" % (self.db_name, set_name, args))
        account = self.db.get_collection(set_name)
        account.insert(args)

    def find(self, set_name, dic1={}, dic2={}, distinc=None):
        ''' 查询集合数据 '''
        account = self.db.get_collection(set_name)
        if distinc and dic1 and dic2:
            # print("%s.%s.find(%s, %s).distinct('%s')"
            #       % (self.db_name, set_name, dic1, dic2, distinc))
            result = account.find(dic1, dic2).distinct(distinc)
            return list(result)
        elif distinc and dic1:
            # print("%s.%s.find(%s).distinct('%s')"
            #       % (self.db_name, set_name, dic1, distinc))
            result = account.find(dic1).distinct(distinc)
            return list(result)
        elif distinc:
            # print("%s.%s.distinct=('%s')"
            #       % (self.db_name, set_name, distinc))
            result = account.distinct(distinc)
            return list(result)
        elif dic1 and dic2:
            # print("%s.%s.find(%s, %s)"
            #       % (self.db_name, set_name, dic1, dic2))
            result = account.find(dic1, dic2)
            return list(result)
        elif dic1:
            # print("%s.%s.find(%s)"
            #       % (self.db_name, set_name, dic1))
            result = account.find(dic1)
            return list(result)
        else:
            cusor = account.find({})
            try:
                result = []
                for c in cusor:
                    result.append(c)
            except:
                pass
            return result

    def find_one(self, set_name, dic1={}, dic2={}, distinc=None):
        ''' 查询第一个满足条件的集合数据 '''
        account = self.db.get_collection(set_name)
        if distinc and dic1 and dic2:
            # print("%s.%s.find_one(%s, %s).distinct('%s')"
            #       % (self.db_name, set_name, dic1, dic2, distinc))
            result = account.find_one(dic1, dic2).distinct(distinc)
            return result
        elif distinc and dic1:
            # print("%s.%s.find_one(%s).distinct('%s')"
            #       % (self.db_name, set_name, dic1, distinc))
            result = account.find_one(dic1).distinct(distinc)
            return result
        elif distinc:
            # print("%s.%s.distinct=('%s')"
            #       % (self.db_name, set_name, distinc))
            result = account.distinct(distinc)
            return result
        elif dic1 and dic2:
            # print("%s.%s.find_one(%s, %s)"
            #       % (self.db_name, set_name, dic1, dic2))
            result = account.find_one(dic1, dic2)
            return result
        elif dic1:
            # print("%s.%s.find_one(%s)"
            #       % (self.db_name, set_name, dic1))
            result = account.find_one(dic1)
            return result
        else:
            return account.find_one({})

    def update(self, set_name, arg, args):
        ''' 更新集合数据 '''
        # print("%s.%s.update(%s, %s)" % (self.db_name, set_name, arg, args))
        account = self.db.get_collection(set_name)
        account.update(arg, args)

    def group(self, set_name, match, group, _sort=None, project=None):
        ''' 分组查询，返回总数 '''
        # print("%s.%s.aggregate([{$match:%s}, {$group:%s}])" %
        #       (self.db_name, set_name, match, group))
        account = self.db.get_collection(set_name)
        if project and not _sort:
            result = account.aggregate(
                [{"$match": match}, {"$group": group}, {"$project": project}]
            )
        elif _sort:
            result = account.aggregate([{"$match": match}, {"$group": group},
                                        {"$sort": _sort}])
        else:
            result = account.aggregate([{"$match": match}, {"$group": group}])
        return result

    def close(self):
        ''' 关闭数据库连接 '''
        # print("关闭数据库连接%s:%s   数据库:%s" % (self.ip_addr, self.port,
        #                                  self.db_name))
        self.conn.close()


def init_db(db_name="", ip_addr="localhost", port=27017,
            username="", password="", uri=""):
    mongo = MongoDB()
    if uri:
        mongo.createMongoConnection(uri=uri)
    else:
        mongo.createMongoConnection(db_name, ip_addr=ip_addr, port=port,
                                    username=username, password=password)
    return mongo


def make_data_monthly_detail_safety_produce_info(db):
    db.clear("monthly_detail_safety_produce_info")
    dpid = [
        "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "19B8C3534DDC5665E0539106C00A58FD",
        "99990002001400031495AAAA",
        "19B8C3534E4E5665E0539106C00A58FD",
        "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "19B8C3534E275665E0539106C00A58FD",
    ]
    for i in range(1000):
        date = 20180400 + rr(100, 800, 100)
        db.insert("monthly_detail_safety_produce_info", {
            "_id": i + 1,
            "PK_ID": 2222,
            "MAIN_TYPE": 2,
            "DETAIL_TYPE": rr(1, 4),
            "CATEGORY": 0.0,
            "OVERVIEW": "4 月1日15时42分",
            "REASON": 0,
            "RESPONSIBILITY_UNIT": 0,
            "RANK": 0,
            "TYPE3": choice(dpid),
            "TYPE4": 0,
            "TYPE5": 0,
            "DATE": date,
            "MON": int(date / 100),
            "CLOCK": "14: 42",
            "RESPONSIBILITY_NAME": 2,
            "MAJOR": "工务"
        })
    for i in range(1000, 2000):
        date = 20180400 + rr(100, 800, 100)
        db.insert("monthly_detail_safety_produce_info", {
            "_id": i + 1,
            "PK_ID": 2222,
            "MAIN_TYPE": 2,
            "DETAIL_TYPE": rr(1, 4),
            "CATEGORY": 0.0,
            "OVERVIEW": "4 月1日15时42分",
            "REASON": 0,
            "RESPONSIBILITY_UNIT": 0,
            "RANK": 0,
            "TYPE3": 0,
            "TYPE4": choice(dpid),
            "TYPE5": 0,
            "DATE": date,
            "MON": int(date / 100),
            "CLOCK": "14: 42",
            "RESPONSIBILITY_NAME": 2,
            "MAJOR": "工务"
        })
    for i in range(2000, 3000):
        date = 20180400 + rr(100, 800, 100)
        db.insert("monthly_detail_safety_produce_info", {
            "_id": i + 1,
            "PK_ID": 2222,
            "MAIN_TYPE": 2,
            "DETAIL_TYPE": rr(1, 4),
            "CATEGORY": 0.0,
            "OVERVIEW": "4 月1日15时42分",
            "REASON": 0,
            "RESPONSIBILITY_UNIT": 0,
            "RANK": 0,
            "TYPE3": 0,
            "TYPE4": 0,
            "TYPE5": choice(dpid),
            "DATE": date,
            "MON": int(date / 100),
            "CLOCK": "14: 42",
            "RESPONSIBILITY_NAME": 2,
            "MAJOR": "工务"
        })
    db.insert("monthly_detail_safety_produce_info", {
        "_id": 3001,
        "PK_ID": 2222,
        "MAIN_TYPE": 1,
        "DETAIL_TYPE": rr(1, 4),
        "CATEGORY": 0.0,
        "OVERVIEW": "4 月1日15时42分",
        "REASON": 0,
        "RESPONSIBILITY_UNIT": 0,
        "RANK": 0,
        "TYPE3": 0,
        "TYPE4": 0,
        "TYPE5": 0,
        "DATE": 20181223,
        "MON": 201812,
        "CLOCK": "14: 42",
        "RESPONSIBILITY_NAME": 2,
        "MAJOR": "工务"
    })


if __name__ == "__main__":
    # help(MongoClient)
    mongo = MongoDB()
    mongo.createMongoConnection("railway_map", "127.0.0.1", 27017)
    print()
    mongo.find("test_set")
    print()
    mongo.clear("test_set")
    print()
    mongo.find("test_set")
    print()
    mongo.insert("test_set", {"name": "zhaojy"})
    print()
    mongo.update("test_set", {"name": "zhaojy"}, {"$set": {"age": 18}})
    print()
    mongo.close()
    mongo = init_db("railway_map", "47.104.145.108", 27017,
                    "xiaoyu", "heheda")
    data = mongo.find(
        "daily_detail_safety_produce_info",
    )
    print(data)
