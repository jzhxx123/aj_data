import sys
import os
curPath = os.path.abspath(os.path.dirname(__file__))
rootPath = os.path.split(os.path.split(curPath)[0])[0]
sys.path.append(rootPath)
from app.new_three import new_three_bp
from flask import jsonify, request

from app.new_three.broken_line_util import (
    get_the_correct_date_and_the_correct_value,
    get_the_correct_date_and_the_correct_value_daily
)


@new_three_bp.route('/accident_broken_line', methods=['GET', 'POST'])
def get_accident_broken_line():
    try:
        if request.method == "POST":
            params = request.form.to_dict()
            if params.get("DPID", "") and params.get("START_MONTH", "") and \
                    params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    params.get("START_MONTH", ""),
                    params.get("END_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    start=params.get("START_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("START_MONTH", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    start=params.get("START_MONTH", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("DPID", ""):
                result = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                )
            elif params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    start=params.get("START_MONTH", ""),
                )
            elif params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value(
                    end=params.get("END_MONTH", ""),
                )
        else:
            result = get_the_correct_date_and_the_correct_value()
        data = {}
        data["DATE"] = result[0]
        data["VALUES"] = {}
        # 获取行车数据
        data["VALUES"]["drive"] = result[1]
        # 获取劳安数据
        data["VALUES"]["work"] = result[2]
        # 获取路外数据
        data["VALUES"]["road"] = result[3]
    except Exception as e:
        return jsonify({"data": "", "error_message": e, "status": 404})
    return jsonify({"data": data, "error_message": "", "status": 200})


@new_three_bp.route('/accident_broken_line/daily', methods=['GET', 'POST'])
def get_accident_broken_line_daily():
    try:
        if request.method == "POST":
            params = request.form.to_dict()
            if params.get("DPID", "") and params.get("START_MONTH", "") and \
                    params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    params.get("START_MONTH", ""),
                    params.get("END_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    start=params.get("START_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("START_MONTH", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    start=params.get("START_MONTH", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("DPID", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                )
            elif params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    start=params.get("START_MONTH", ""),
                )
            elif params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    end=params.get("END_MONTH", ""),
                )
        else:
            result = get_the_correct_date_and_the_correct_value_daily()
        data = {}
        data["DATE"] = result[0]
        data["VALUES"] = {}
        # 获取行车数据
        data["VALUES"]["drive"] = result[1]
        # 获取劳安数据
        data["VALUES"]["work"] = result[2]
        # 获取路外数据
        data["VALUES"]["road"] = result[3]
    except Exception as e:
        return jsonify({"data": "", "error_message": e, "status": 404})
    return jsonify({"data": data, "error_message": "", "status": 200})


@new_three_bp.route('/accident_broken_line/all', methods=['GET', 'POST'])
def get_accident_broken_line_all():
    try:
        if request.method == "POST":
            params = request.form.to_dict()
            if params.get("DPID", "") and params.get("START_MONTH", "") and \
                    params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    params.get("START_MONTH", ""),
                    params.get("END_MONTH", "")
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    params.get("START_MONTH", ""),
                    params.get("END_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    start=params.get("START_MONTH", "")
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    start=params.get("START_MONTH", "")
                )
            elif params.get("DPID", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                    end=params.get("END_MONTH", "")
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("START_MONTH", "") and params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    start=params.get("START_MONTH", ""),
                    end=params.get("END_MONTH", "")
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    start=params.get("START_MONTH", ""),
                    end=params.get("END_MONTH", "")
                )
            elif params.get("DPID", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    params.get("DPID", ""),
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                )
            elif params.get("START_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    start=params.get("START_MONTH", ""),
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    params.get("DPID", ""),
                    start=params.get("START_MONTH", "")
                )
            elif params.get("END_MONTH", ""):
                result = get_the_correct_date_and_the_correct_value_daily(
                    end=params.get("END_MONTH", ""),
                )
                result2 = get_the_correct_date_and_the_correct_value(
                    end=params.get("END_MONTH", ""),
                )
        else:
            result = get_the_correct_date_and_the_correct_value_daily()
            result2 = get_the_correct_date_and_the_correct_value()

        result3 = result2[:]
        # print(result2[0], result[0])
        # print(result2[1], result[1])
        # print(result2[2], result[2])
        # print(result2[3], result[3])
        try:
            k = 1
            if len(result3[0]) and len(result[0]):
                for i in range(len(result3[0])):
                    if result3[0][i] == result[0][0]:
                        result3[1][i] += result[1][0]
                        result3[2][i] += result[2][0]
                        result3[3][i] += result[3][0]
                        k = 0

            if k and len(result[0]):
                result3[0].append(result[0][0])
                result3[1].append(result[1][0])
                result3[2].append(result[2][0])
                result3[3].append(result[3][0])
        except Exception:
            raise
        # print(result3[0])
        # print(result3[1])
        # print(result3[2])
        # print(result3[3])

        data = {}
        data["DATE"] = result3[0]
        data["VALUES"] = {}
        # 获取行车数据
        data["VALUES"]["drive"] = result3[1]
        # 获取劳安数据
        data["VALUES"]["work"] = result3[2]
        # 获取路外数据
        data["VALUES"]["road"] = result3[3]
    except Exception as e:
        return jsonify({"data": "", "error_message": e, "status": 404})
    return jsonify({"data": data, "error_message": "", "status": 200})
