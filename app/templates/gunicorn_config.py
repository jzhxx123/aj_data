import multiprocessing

bind = 'localhost:5000'
workers = multiprocessing.cpu_count() * 2 + 1
worker_class = 'gevent'
# timeout = 300
# graceful_timeout = 100
syslog = True
