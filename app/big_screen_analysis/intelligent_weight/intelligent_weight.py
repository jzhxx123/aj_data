#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/16
Description:   智能权重
"""
from random import random, randint

import pandas as pd
import numpy as np
import pymongo

from app import mongo
from app.big_screen_analysis.algorithm.emphasisindex import Weights, Forecast_any
from app.big_screen_analysis.common.common_sql import department_safety_sql
from app.big_screen_analysis.index_warning.index_warning import get_arrow_type
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.big_screen_analysis.util import get_range_history_months
from app.data.util import pd_query
from app.utils.common_func import get_history_months
from app.utils.safety_index_common_func import get_index_title


def apply_to_get_errors(row, safety_info_df):
    return safety_info_df[safety_info_df['MON'] == row['MON']].index.size


def get_main_index_weight(index_type, major, main_type_list):
    if index_type == 0:
        condition = {'INDEX_TYPE': index_type}
    else:
        condition = {'INDEX_TYPE': index_type, 'MAJOR': major}
    documents = mongo.db['base_index_weight'].find(condition, {'_id': 0})

    df = pd.DataFrame(list(documents))
    df = df[df['MAIN_TYPE'].isin(main_type_list)]
    lack_main_type = set(main_type_list) - set(df['MAIN_TYPE'].tolist())
    if lack_main_type:
        temp = pd.DataFrame({'MAIN_TYPE': list(lack_main_type)})
        df = df.append(temp, sort=False)
        df['WEIGHT'] = df['WEIGHT'].fillna(0)
        df.sort_values(['MAIN_TYPE'], ascending=True, inplace=True)
    return df


def get_index_data_from_mongo(major, index_type, month):
    index_score_list = []
    detail_score_list = []

    condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'MON': {'$lte': month}
    }
    detail_condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'DETAIL_TYPE': 0,
        'MON': {'$lte': month}
    }

    if index_type == 0:
        index_coll_prefix = 'health'
    else:
        index_coll_prefix = 'major'
        condition['TYPE'] = index_type
        detail_condition['TYPE'] = index_type

    for prefix in ['history_', 'monthly_']:
        index_score = list(mongo.db[f'{prefix}{index_coll_prefix}_index'].find(
            condition, {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING), ('MON', pymongo.ASCENDING)]))

        index_detail_score = list(mongo.db[f'{prefix}detail_{index_coll_prefix}_index'].find(
            detail_condition,
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAIN_TYPE': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING),
                     ('MON', pymongo.ASCENDING),
                     ('MAIN_TYPE', pymongo.ASCENDING)]))

        index_score_list.append(pd.DataFrame(index_score))
        detail_score_list.append(pd.DataFrame(index_detail_score))

    index_score = pd.concat(index_score_list, ignore_index=True)
    index_detail_score = pd.concat(detail_score_list, ignore_index=True)

    return index_score, index_detail_score


def get_analysis_data(risk_type, department_id, month, months_ago=None):
    major, index_type = risk_type.split('-')
    index_type = int(index_type)
    if months_ago:
        month = get_history_months(months_ago)[0]

    index_score, index_detail_score = get_index_data_from_mongo(major, index_type, month)
    if index_score.empty or index_detail_score.empty:
        return None

    edge_mon = get_history_months(months_ago - 2)[0]
    range_start_mon = get_history_months(months_ago - 12)[0]

    index_score = index_score[index_score['MON'].between(range_start_mon, month)]
    index_detail_score = index_detail_score[index_detail_score['MON'].between(range_start_mon, month)]

    department_major_index_score = index_score[index_score['DEPARTMENT_ID'] == department_id]
    department_index_detail_score = index_detail_score[
        index_detail_score['DEPARTMENT_ID'] == department_id].copy()

    department_index_detail_score.drop_duplicates(subset=['MAIN_TYPE', 'MON'], inplace=True)

    score_list = []
    safe_info_list = []
    months_list = set(department_index_detail_score.groupby('MON').groups)
    if len(months_list) <= 3:
        min_month = min(months_list)
        for _mon in get_range_history_months(min_month, 6 - len(months_list)):
            months_list.add(_mon)
    months_list = set(sorted(months_list))
    safe_info = pd_query(department_safety_sql.format(department_id))
    main_type_index = []
    df_list = []

    total_lack_months = months_list - set(department_major_index_score.groupby('MON').groups)
    if total_lack_months:
        temp = pd.DataFrame({'MON': list(total_lack_months)})
        department_major_index_score = department_major_index_score.append(temp, sort=False)
        department_major_index_score.sort_values(['MON'], ascending=True, inplace=True)
        department_major_index_score.fillna(method='ffill', inplace=True)
        department_major_index_score.fillna(method='bfill', inplace=True)

    for main_type, df in department_index_detail_score.groupby('MAIN_TYPE'):
        df = df.copy()
        main_type_index.append(main_type)
        lack_months = months_list - set(df.groupby('MON').groups)
        if lack_months:
            temp = pd.DataFrame({'MON': list(lack_months)})
            df = df.append(temp, sort=False)
            df.sort_values(['MON'], ascending=True, inplace=True)
            df.fillna(method='ffill', inplace=True)
            df.fillna(method='bfill', inplace=True)
        score_list.append(df['SCORE'].values)
        df['MON_DIFF'] = df['SCORE'].diff()
        df['YEAR_DIFF'] = df['SCORE'].diff(12)
        df['RANK_DIFF'] = df['RANK'].diff()
        df_list.append(df)

    detail_df = pd.concat(df_list, ignore_index=True)
    detail_df['MON_DIFF_RATIO'] = round(detail_df['MON_DIFF'] / (detail_df['SCORE'] - detail_df['MON_DIFF']), 4)
    detail_df['YEAR_DIFF_RATIO'] = round(detail_df['YEAR_DIFF'] / (detail_df['SCORE'] - detail_df['YEAR_DIFF']), 4)
    detail_df.replace([np.inf, -np.inf], np.nan, inplace=True)

    months_list = sorted(months_list)
    for _mon in months_list:
        safe_info_list.append(safe_info[safe_info['MON'] == _mon].index.size)

    score_array = np.array(score_list)
    safe_info_array = np.array(safe_info_list)
    weight = get_main_index_weight(index_type, major, main_type_index)
    #
    # try:
    #     forecast_input = np.array([department_major_index_score['SCORE'].values,
    #                                safe_info_array])
    #     forecast_result = Forecast_any(forecast_input)
    # except Exception as e:
    #     forecast_result = None
    #     if not forecast_result:
    #         forecast_result = (80 + randint(-5, 5), 1,
    #                            np.array([np.array([randint(-2, 2) for x in range(len(months_list) - 1)]),
    #                                      [randint(-2, 2) for x in range(len(months_list) - 1)]]), 0.1)
    forecast_input = np.array([department_major_index_score['SCORE'].values,
                               safe_info_array])
    forecast_result = Forecast_any(forecast_input)
    """
    forecast_result 预测结果说明
    类型：元组
    元素1：下月预测指数得分
    元素2：下月预测事故故障数量
    元素3：相关性计算结果， numpy二维数组，第一行为得分相关性，第二行为事故故障相关性，列数为输入的月份的间隔数
    元素4：相关系数
    """

    relevant_result, weight_result = Weights(score_array, safe_info_array, weight['WEIGHT'].values)
    """
    relevant_result 子指数相关性计算结果说明
    类型：numpy二维数组
    行：按顺序输入的各个子指数
    列：第一列：相关系数
        第二列：暂无用到
    """

    """
    weight_result 权重计算结果说明
    类型：numpy数组
    数据：按顺序输入的各个子指数新权重
    """

    forecast_result = {'forecast_score': forecast_result[0], 'forecast_error': forecast_result[1],
                       'relevant_data': forecast_result[2],
                       'relevant_ratio': round(forecast_result[3], 2)}

    score_and_error_forecast = pd.DataFrame(np.array([list(months_list),
                                                      department_major_index_score['SCORE'].values,
                                                      safe_info_array]))
    score_and_error_forecast = pd.concat([score_and_error_forecast,
                                          pd.DataFrame([get_history_months(months_ago + 1)[0],
                                                        round(forecast_result['forecast_score'], 2),
                                                        forecast_result['forecast_error']])],
                                         axis=1, ignore_index=True)

    relevant_data = {'data': pd.DataFrame(forecast_result['relevant_data']),
                     'relevant_ratio': forecast_result['relevant_ratio'],
                     'main_type_relevant_ratio': pd.DataFrame(relevant_result, index=main_type_index)}

    new_weight = pd.DataFrame(weight_result, index=main_type_index)
    weight.set_index('MAIN_TYPE', drop=True, inplace=True)

    print('ok')
    return detail_df, score_and_error_forecast, relevant_data, weight, new_weight, department_major_index_score


def apply_to_add_desc_data(row):
    res = [
        get_arrow_type(row['MON_DIFF']),
        '{}%'.format(abs(int(row['MON_DIFF_RATIO'] * 100))) if not pd.isna(row['MON_DIFF_RATIO']) else '-',
        get_arrow_type(row['YEAR_DIFF']),
        '{}%'.format(abs(int(row['YEAR_DIFF_RATIO'] * 100))) if not pd.isna(row['YEAR_DIFF_RATIO']) else '-',
        '{}-{:0>2}'.format(row['MON'] // 100, row['MON'] % 100)
    ]

    return pd.Series(res,
                     index=['MON_DIFF_IS_RISE', 'MON_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE', 'YEAR_DIFF_PERCENT',
                            'MON_DESC'])


def get_relevant_desc(ratio):
    ratio = ratio * 100
    if ratio > 0:
        content = '整体正相关，指数得分越高，事故发生数越少。相关系数：{}，相对性{}。'
    else:
        content = '整体负相关，指数得分越高，事故发生数越多。相关系数：{}，相对性{}。'
    if abs(ratio) <= 10:
        strength = '较弱'
    elif 10 < abs(ratio) < 30:
        strength = '较强'
    else:
        strength = '极强'
    return content.format(round(ratio, 2), strength)


def get_data(param_dict, index_type=None):
    result = {}
    major = param_dict['MAJOR']
    if not index_type:
        index_type = int(param_dict.get('RISK_TYPE', 0))
    risk_type = '{}-{}'.format(major, index_type)
    department_id = param_dict['DEPARTMENT_ID']
    months_ago = -1
    mon = get_history_months(months_ago)[0]

    analysis_data = get_analysis_data(risk_type, department_id, mon, months_ago)
    if analysis_data is None:
        return 'empty data'

    index_data = analysis_data[0]
    score_and_error_forecast = analysis_data[1]
    relevant_data = analysis_data[2]
    current_weight = analysis_data[3]
    new_weight = analysis_data[4]
    total_score_data = analysis_data[5]

    result['major'] = major
    result['department_name'] = index_data.iloc[0]['DEPARTMENT_NAME']
    if index_type == 0:
        result['index_name'] = '安全综合指数'
    else:
        result['index_name'] = major_risk_map.get(risk_type, {}).get('name', '重点指数')

    charts_data = []
    main_type_groups = {'检查指数趋势': [1, 4], '问题指数趋势': [5, 6], '评价指数趋势': [2, 3],
                        '运用指数趋势': [8], '隐患指数趋势': [9, 10], '计划与盯控指数趋势': [11, 12]}
    index_data.replace([np.inf, -np.inf], np.nan, inplace=True)
    index_data = index_data.join(index_data.apply(lambda r: apply_to_add_desc_data(r), axis=1))
    main_type_relevant_data = relevant_data['main_type_relevant_ratio']
    for cate, types in main_type_groups.items():
        _data = {'title': cate, 'data': []}
        diff_months = set(index_data[index_data['MAIN_TYPE'].isin(types)].drop_duplicates(subset=['MON'], keep=False)[
                              'MON'].values)
        for main_type in types:
            _df = index_data.loc[index_data['MAIN_TYPE'] == main_type,
                                 ['MON', 'MON_DESC', 'SCORE',
                                  'YEAR_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE',
                                  'MON_DIFF_PERCENT', 'MON_DIFF_IS_RISE']].copy()
            # 有些指数某个月无数据，造成和同组的指数月份不一致
            if diff_months:
                lack_months = diff_months - set(_df['MON'].values)
                if lack_months:
                    _len = len(lack_months)
                    temp = pd.DataFrame({'MON': list(lack_months),
                                         'MON_DESC': ['{}-{:0>2}'.format(x // 100, x % 100).format(x % 100) for x in lack_months],
                                         'SCORE': ['缺失'] * _len,
                                         'YEAR_DIFF_PERCENT': ['-'] * _len,
                                         'YEAR_DIFF_IS_RISE': ['-'] * _len,
                                         'MON_DIFF_PERCENT': ['-'] * _len,
                                         'MON_DIFF_IS_RISE': ['-'] * _len})
                    _df = _df.append(temp)
                    _df.sort_values(['MON'], ascending=True, inplace=True)
            _df.drop(['MON'], axis=1, inplace=True)
            if _df.empty:
                continue
            index_title = get_index_title(main_type)
            if main_type in main_type_relevant_data.index:
                main_type_relevant_row = main_type_relevant_data.loc[main_type]
                index_title = index_title + '(相关系数 : {})'.format(int(main_type_relevant_row[0] * 100))
            _data['data'].append({'title': index_title,
                                  'data': _df.values.tolist()})
        if len(_data['data']) > 0:
            charts_data.append(_data)
    result['charts'] = charts_data

    result['predict'] = {'months': ['{}月'.format(int(x) % 100) for x in score_and_error_forecast.loc[0].values],
                         'scores': score_and_error_forecast.loc[1].tolist(),
                         'errors': score_and_error_forecast.loc[2].tolist()}

    result['relative'] = {'errors': [round(float(x), 2) for x in relevant_data['data'].loc[1].values],
                          'months': relevant_data['data'].columns.tolist(),
                          'scores': [round(float(x), 2) for x in relevant_data['data'].loc[0].values],
                          'desc': get_relevant_desc(relevant_data['relevant_ratio'])}

    weights_data = {'smartWeights': {'data': []}, 'experiWeights': {'data': []}, 'data': {}}
    new_weight = new_weight.to_dict(orient='dict')[0]
    current_weight = current_weight[['WEIGHT']].to_dict(orient='dict')['WEIGHT']
    new_score_list = []
    old_score_list = total_score_data['SCORE'].tolist()
    for _mon, mon_df in index_data.groupby('MON'):
        new_score = 0
        old_score = 0
        for _, row in mon_df.iterrows():
            new_score += new_weight[row['MAIN_TYPE']] * row['SCORE']
            old_score += current_weight[row['MAIN_TYPE']] * row['SCORE']
            if _mon == mon:
                weights_data['smartWeights']['data'].append([row['SCORE'], round(new_weight[row['MAIN_TYPE']], 2)])
                weights_data['experiWeights']['data'].append([row['SCORE'], round(current_weight[row['MAIN_TYPE']], 2)])
        new_score = round(new_score, 2)
        old_score = round(old_score, 2)
        new_score_list.append(new_score)
        if _mon == mon:
            weights_data['smartWeights']['total'] = new_score
            weights_data['experiWeights']['total'] = old_score

    weights_data['data']['old'] = old_score_list
    weights_data['data']['new'] = new_score_list
    weights_data['data']['months'] = ['{}月'.format(int(x) % 100) for x in total_score_data['MON'].values]
    result['weights'] = weights_data
    return result


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        pass

