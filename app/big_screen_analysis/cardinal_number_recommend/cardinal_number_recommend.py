#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/16
Description: 基数推荐
"""
import pandas as pd
import numpy as np
import pymongo

from app import mongo
from app.big_screen_analysis.algorithm.emphasisindex import Cardinal_number
from app.big_screen_analysis.common.common_sql import department_safety_sql
from app.big_screen_analysis.index_warning.index_warning import get_arrow_type
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.big_screen_analysis.util import get_range_history_months
from app.data.util import pd_query
from app.utils.common_func import get_history_months
from app.utils.safety_index_common_func import get_index_title


def get_index_data_from_mongo(major, index_type, month):
    index_score_list = []
    detail_score_list = []

    condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'MON': {'$lte': month}
    }
    detail_condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'DETAIL_TYPE': 0,
        'MON': {'$lte': month}
    }

    if index_type == 0:
        index_coll_prefix = 'health'
    else:
        index_coll_prefix = 'major'
        condition['TYPE'] = index_type
        detail_condition['TYPE'] = index_type

    for prefix in ['history_', 'monthly_']:
        index_score = list(mongo.db[f'{prefix}{index_coll_prefix}_index'].find(
            condition, {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING), ('MON', pymongo.ASCENDING)]))

        index_detail_score = list(mongo.db[f'{prefix}detail_{index_coll_prefix}_index'].find(
            detail_condition,
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAIN_TYPE': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING),
                     ('MON', pymongo.ASCENDING),
                     ('MAIN_TYPE', pymongo.ASCENDING)]))

        index_score_list.append(pd.DataFrame(index_score))
        detail_score_list.append(pd.DataFrame(index_detail_score))

    index_score = pd.concat(index_score_list, ignore_index=True)
    index_detail_score = pd.concat(detail_score_list, ignore_index=True)

    return index_score, index_detail_score


def get_analysis_data(risk_type, month, months_ago=None):
    major, index_type = risk_type.split('-')
    index_type = int(index_type)
    if months_ago:
        month = get_history_months(months_ago)[0]

    index_score, index_detail_score = get_index_data_from_mongo(major, index_type, month)
    if index_score.empty or index_detail_score.empty:
        return None

    edge_mon = get_history_months(months_ago - 2)[0]
    range_start_mon = get_history_months(months_ago - 12)[0]

    index_detail_score = index_detail_score[index_detail_score['MON'].between(range_start_mon, month)]
    # index_detail_score = index_detail_score[~index_detail_score['DEPARTMENT_NAME'].str.contains('工电段')]

    safe_info_cache = {}
    result = []

    for main_type, df in index_detail_score.groupby('MAIN_TYPE'):
        if df.empty:
            continue
        df = df.copy()
        if main_type in (6,):
            continue
        cur_month_df = df[df['MON'] == month].copy()
        cur_month_df.sort_values(['RANK'], ascending=True, inplace=True)
        cur_month_df = cur_month_df.iloc[:6]
        departments = cur_month_df['DEPARTMENT_ID'].values

        df = df[df['DEPARTMENT_ID'].isin(departments)]
        if df.empty:
            continue
        # df.sort_values(['DEPARTMENT_ID'], ascending=True, inplace=True)

        months_list = set(df.groupby('MON').groups)
        if len(months_list) <= 5:
            min_month = min(months_list)
            for _mon in get_range_history_months(min_month, 7 - len(months_list)):
                months_list.add(_mon)
        months_list = set(sorted(months_list))
        score_list = []
        safe_info_list = []
        department_index = []
        department_names = {}
        charts_data = []
        for department, department_df in df.groupby('DEPARTMENT_ID'):
            if department_df.empty:
                continue
            department_df = department_df.copy()
            department_index.append(department)
            department_names[department] = department_df.iloc[0]['DEPARTMENT_NAME']
            lack_months = months_list - set(department_df.groupby('MON').groups)
            if lack_months:
                temp = pd.DataFrame({'MON': list(lack_months)})
                department_df = department_df.append(temp, sort=False)
                department_df.sort_values(['MON'], ascending=True, inplace=True)
                department_df.fillna(method='ffill', inplace=True)
                department_df.fillna(method='bfill', inplace=True)
            department_df.sort_values(['MON'], ascending=True, inplace=True)
            department_df['MON_DIFF'] = department_df['SCORE'].diff()
            department_df['YEAR_DIFF'] = department_df['SCORE'].diff(12)
            department_df['RANK_DIFF'] = department_df['RANK'].diff()
            department_df['MON_DIFF_RATIO'] = round(
                department_df['MON_DIFF'] / (department_df['SCORE'] - department_df['MON_DIFF']), 4)
            department_df['YEAR_DIFF_RATIO'] = round(
                department_df['YEAR_DIFF'] / (department_df['SCORE'] - department_df['YEAR_DIFF']), 4)
            charts_data.append(department_df)

            score_list.append(department_df['SCORE'].values[:13])

            safe_info = safe_info_cache.get(department, None)
            if safe_info is None:
                safe_info = pd_query(department_safety_sql.format(department))
                safe_info_cache[department] = safe_info
            safe_info = safe_info[safe_info['MON'].between(edge_mon, month)]
            safe_info_list.append(np.array([safe_info.index.size]))

        score_array = np.array(score_list)
        safe_info_array = np.array(safe_info_list)

        if score_array.shape[1] <= 4:
            continue
        calc_result = Cardinal_number(score_array, safe_info_array)
        """
        calc_result 算法计算结果说明：
        类型：numpy二维数组
        返回列: 0, 1, 2, 3, 4,
        列含义: 0: 是否能作为基数部门，1：可以，0：不可以
                  其它列暂时未用到
        """

        result.append({'index_name': get_index_title(main_type),
                       'charts_data': pd.concat(charts_data, ignore_index=True),
                       'department_names': department_names,
                       'calc_result': pd.DataFrame(calc_result, index=department_index)})

        print('ok')
    return result


def apply_to_add_desc_data(row):
    res = [
        get_arrow_type(row['MON_DIFF']),
        '{}%'.format(abs(int(row['MON_DIFF_RATIO'] * 100))) if not pd.isna(row['MON_DIFF_RATIO']) else '-',
        get_arrow_type(row['YEAR_DIFF']),
        '{}%'.format(abs(int(row['YEAR_DIFF_RATIO'] * 100))) if not pd.isna(row['YEAR_DIFF_RATIO']) else '-',
        '{}-{:0>2}'.format(row['MON'] // 100, row['MON'] % 100)
    ]

    return pd.Series(res,
                     index=['MON_DIFF_IS_RISE', 'MON_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE', 'YEAR_DIFF_PERCENT',
                            'MON_DESC'])


def get_data(param_dict, index_type=None):
    """
    获取接口数据
    :param param_dict:
    :param index_type: 0：综合指数，其它表示为重点指数
    :return:
    """
    result = {}
    major = param_dict['MAJOR']
    if not index_type:
        index_type = int(param_dict.get('RISK_TYPE', 0))
    risk_type = '{}-{}'.format(major, index_type)
    months_ago = -1
    mon = get_history_months(months_ago)[0]

    if index_type == 0:
        index_name = '安全综合指数'
    else:
        index_name = major_risk_map.get(risk_type, {}).get('name', '重点指数')

    analysis_data = get_analysis_data(risk_type, mon, months_ago)
    if analysis_data is None:
        return 'empty data'
    index_data = analysis_data

    result['data'] = []
    result['index_name'] = index_name

    for data in index_data:
        charts = []
        department_names = data['department_names']
        calc_result = data['calc_result']
        for department, df in data['charts_data'].groupby('DEPARTMENT_ID'):
            df = df.copy()
            df.replace([np.inf, -np.inf], np.nan, inplace=True)
            df = df.join(df.apply(lambda r: apply_to_add_desc_data(r), axis=1))
            df.fillna('-')
            _d = {'name': department_names[department], 'data': df[['MON_DESC', 'SCORE',
                                                                    'YEAR_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE',
                                                                    'MON_DIFF_PERCENT',
                                                                    'MON_DIFF_IS_RISE']].values.tolist()}
            charts.append(_d)

        report = [department_names[x] for x in calc_result[calc_result[0] == 1].index.tolist()]
        report = '、'.join(report) or '暂无符合条件的站段'
        report_desc = '下月推荐基数来源站段：'
        result['data'].append({'name': data['index_name'],
                               'charts': charts,
                               'report': [report_desc, report]})

    return result


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        get_data({})