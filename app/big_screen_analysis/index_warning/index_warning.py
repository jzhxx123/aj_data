#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/13
Description: 智能预警标准
"""
import pandas as pd
import numpy as np
import pymongo

from app import mongo
from app.big_screen_analysis.algorithm.emphasisindex import Emphasis_Index
from app.big_screen_analysis.common.common_sql import department_safety_sql
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.big_screen_analysis.util import get_range_history_months
from app.data.util import pd_query
from app.utils.common_func import get_history_months
from app.utils.safety_index_common_func import get_index_title


relevant_desc = {
    -1: '{}与事故故障发生呈负相关，说明该指数不可以较为准确的反馈事故故障信息',
    0: '{}与事故故障发生无明显相关性，说明该指数不可以较为准确的反馈事故故障信息',
    1: '{}与事故故障发生呈正相关，说明该指数可以较为准确的反馈事故故障信息'
}


def get_arrow_type(value):
    if value > 0:
        return 1
    elif value < 0:
        return 0
    else:
        return 2


def _apply_to_add_extra_data(row, detail_df, columns):
    res = []
    df = detail_df.loc[(detail_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])
                       & (detail_df['MON'] == row['MON'])].copy()
    df.drop_duplicates(subset=['MAIN_TYPE'], inplace=True)
    columns = list(df.groupby('MAIN_TYPE').groups)
    df = df.stack().unstack(0)
    res = df.loc['SCORE'].values
    return pd.Series(res, index=[f't{x}' for x in columns])


def get_index_data_from_mongo(major, index_type, month):
    index_score_list = []
    detail_score_list = []

    condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'MON': {'$lte': month}
    }
    detail_condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'DETAIL_TYPE': 0,
        'MON': {'$lte': month}
    }

    if index_type == 0:
        index_coll_prefix = 'health'
    else:
        index_coll_prefix = 'major'
        condition['TYPE'] = index_type
        detail_condition['TYPE'] = index_type

    for prefix in ['history_', 'monthly_']:
        index_score = list(mongo.db[f'{prefix}{index_coll_prefix}_index'].find(
            condition, {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING), ('MON', pymongo.ASCENDING)]))

        index_detail_score = list(mongo.db[f'{prefix}detail_{index_coll_prefix}_index'].find(
            detail_condition,
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAIN_TYPE': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING),
                     ('MON', pymongo.ASCENDING),
                     ('MAIN_TYPE', pymongo.ASCENDING)]))

        index_score_list.append(pd.DataFrame(index_score))
        detail_score_list.append(pd.DataFrame(index_detail_score))

    index_score = pd.concat(index_score_list, ignore_index=True)
    index_detail_score = pd.concat(detail_score_list, ignore_index=True)

    return index_score, index_detail_score


def get_analysis_data(risk_type, department_id, month, months_ago=None):
    major, index_type = risk_type.split('-')
    index_type = int(index_type)
    if months_ago:
        month = get_history_months(months_ago)[0]

    index_score, index_detail_score = get_index_data_from_mongo(major, index_type, month)
    if index_score.empty or index_detail_score.empty:
        return None

    department_index_detail_score = index_detail_score[
        index_detail_score['DEPARTMENT_ID'] == department_id].copy()
    department_index_detail_score.drop_duplicates(subset=['MAIN_TYPE', 'MON'], inplace=True)

    df_list = []
    months_list = set(department_index_detail_score.groupby('MON').groups)
    if len(months_list) <= 2:
        min_month = min(months_list)
        for _mon in get_range_history_months(min_month, 6 - len(months_list)):
            months_list.add(_mon)
    months_list = set(sorted(months_list))
    for main_type, df in department_index_detail_score.groupby('MAIN_TYPE'):
        df = df.copy()
        lack_months = months_list - set(df.groupby('MON').groups)
        if lack_months:
            temp = pd.DataFrame({'MON': list(lack_months)})
            df = df.append(temp, sort=False)
            df.sort_values(['MON'], ascending=True, inplace=True)
            df.fillna(method='ffill', inplace=True)
            df.fillna(method='bfill', inplace=True)

        df['MON_DIFF'] = df['SCORE'].diff()
        df['YEAR_DIFF'] = df['SCORE'].diff(12)
        df['RANK_DIFF'] = df['RANK'].diff()
        df_list.append(df)

    detail_df = pd.concat(df_list, ignore_index=True)

    detail_df['MON_DIFF_RATIO'] = round(detail_df['MON_DIFF'] / (detail_df['SCORE'] - detail_df['MON_DIFF']), 4)
    detail_df['YEAR_DIFF_RATIO'] = round(detail_df['YEAR_DIFF'] / (detail_df['SCORE'] - detail_df['YEAR_DIFF']), 4)

    detail_df.replace([np.inf, -np.inf], np.nan, inplace=True)

    # 调用分析模块
    safety_df = pd_query(department_safety_sql.format(department_id))
    safety_info_count = safety_df[safety_df['MONTH'] == '{}-{:0>2}'.format(month // 100, month % 100)].index.size
    detail_df['ERROR_COUNT'] = -1
    detail_df.loc[detail_df['MON'] == month, ['ERROR_COUNT']] = safety_info_count
    score_list = []

    current_main_types = []
    for main_type, df in index_detail_score[index_detail_score['MON'] == month].groupby('MAIN_TYPE'):
        score_list.append(df['SCORE'].values)
        current_main_types.append(main_type)

    detail_df = detail_df[detail_df['MAIN_TYPE'].isin(current_main_types)]

    start_month = get_history_months(-3)[0]
    if months_ago:
        start_month = get_history_months(months_ago - 2)[0]
    months_list = sorted(months_list)
    input_data = detail_df.loc[detail_df['MON'].isin(months_list[-3:]),
                               ['MAIN_TYPE', 'MON', 'SCORE', 'ERROR_COUNT']]
    input_data.rename(columns={'MAIN_TYPE': 'type', 'SCORE': 'score', 'ERROR_COUNT': 'error'}, inplace=True)
    input_data.reset_index(drop=True, inplace=True)

    result = pd.DataFrame(Emphasis_Index(input_data, np.array(score_list)))
    result.rename(columns={0: 'MAIN_TYPE',  # 子指数类型
                           1: 'IS_UNUSUAL',  # 是否异常
                           2: 'TREND_UNUSUAL',  # 趋势是否异常
                           3: 'COMPARE_UNUSUAL',  # 与专业内其它的单位比较是否异常
                           4: 'RELEVANT_TYPE',  # 相关性
                           5: 'SCORE_TREND',  # 分数趋势，上升/下降百分比
                           6: 'SCORE_TREND_VALUE',  # 分数趋势变化值
                           7: 'THRESHOLD',  # 单指数预警阀门值
                           8: 'COMBINE_WEIGHT',  # 组合预警中的系数1
                           9: 'COMBINE_THRESHOLD',  # 组合预警阈值
                           10: 'TREND_THRESHOLD',  # 趋势变化阈值
                           11: 'COMBINE_RATIO'  # 组合预警中的系数2
                           }, inplace=True)
    return detail_df, result


def execute():
    print('ok')


def apply_to_add_desc_data(row):
    res = [
        get_arrow_type(row['MON_DIFF']),
        '{}%'.format(abs(int(row['MON_DIFF_RATIO'] * 100))) if not pd.isna(row['MON_DIFF_RATIO']) else '-',
        get_arrow_type(row['YEAR_DIFF']),
        '{}%'.format(abs(int(row['YEAR_DIFF_RATIO'] * 100))) if not pd.isna(row['YEAR_DIFF_RATIO']) else '-',
        '{}月'.format(row['MON'] % 100)
    ]

    return pd.Series(res,
                     index=['MON_DIFF_IS_RISE', 'MON_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE', 'YEAR_DIFF_PERCENT',
                            'MON_DESC'])


def get_combine_warning_desc(df):
    index_list = []
    for _, row in df.iterrows():
        index_list.append('{}*{}*{}'.format(get_index_title(row['MAIN_TYPE']),
                                            round(row['COMBINE_WEIGHT'], 2),
                                            round(row['COMBINE_RATIO'], 2)))
    content = ' + '.join(index_list)
    content += ' ≤ {}'.format(round(df.iloc[0]['COMBINE_THRESHOLD'], 2))
    return content


def get_report_data(report_list, analysis_data):
    level_dict = {8: (5, 3), 7: (4, 2), 6: (4, 2), 5: (3, 2), 4: (2, 1), 3: (2, 1), 2: (1, 1)}
    index_count = len(report_list) + 1
    index_score_threshold = int(analysis_data.iloc[0]['COMBINE_THRESHOLD'])
    content = ''
    content += '指数阈值：<br/>'
    content += '指数总得分：{}分以下<br/>'.format(index_score_threshold)
    content += '<br/>'.join(report_list)
    content += '<br/>'
    content += 'I级预警：以上{}项都满足，且指数总得分在{}分以下<br/>'.format(index_count,
                                                        max(index_score_threshold, 10) - 5)
    content += 'II级预警：以上任意{}项满足<br/>'.format(level_dict.get(index_count, (1,))[0])
    content += 'III级预警：以上任意{}项满足<br/>'.format(level_dict.get(index_count, (1, 1))[1])
    return content


def get_data(param_dict, index_type=None):
    major = param_dict['MAJOR']
    if not index_type:
        index_type = int(param_dict.get('RISK_TYPE', 0))
    department_id = param_dict['DEPARTMENT_ID']
    # department_id = '19B8C3534E0D5665E0539106C00A58FD'  # ''19B8C3534E255665E0539106C00A58FD'
    risk_type = '{}-{}'.format(major, index_type)
    # risk_type = '工务-6'
    months_ago = -1
    mon = get_history_months(months_ago)[0]
    analysis_data = get_analysis_data(risk_type, department_id, mon, months_ago)
    if analysis_data is None:
        return 'empty data'
    index_data, analysis_data = analysis_data
    result = dict()

    result['major'] = major
    result['department_name'] = index_data.iloc[-1]['DEPARTMENT_NAME']
    if index_type == 0:
        result['index_name'] = '安全综合指数'
    else:
        result['index_name'] = major_risk_map.get(risk_type, {}).get('name', '重点指数')

    start_mon = get_history_months(months_ago - 11)[0]
    index_data = index_data[index_data['MON'].between(start_mon, mon)]
    index_data = index_data.join(index_data.apply(lambda r: apply_to_add_desc_data(r), axis=1))

    combine_warning_desc = get_combine_warning_desc(analysis_data)

    score_data = []
    charts_data = []
    notice_data = []
    report_data = []

    analysis_data.replace([np.inf, -np.inf], np.nan, inplace=True)
    for main_type, df in index_data.groupby('MAIN_TYPE'):
        if df[df['MON'] == mon].empty:
            continue
        _row = df[df['MON'] == mon].iloc[0]
        analysis_row = analysis_data[analysis_data['MAIN_TYPE'] == main_type]
        analysis_row = analysis_row.iloc[0]
        score_unusual = True if analysis_row['IS_UNUSUAL'] == 1 else False
        score_trend = int(analysis_row['SCORE_TREND'] * 100) if not pd.isna(analysis_row['SCORE_TREND']) else 0
        main_index_name = get_index_title(main_type)
        # 得分数据
        _score_data = {
            "name": main_index_name,
            "score": _row['SCORE'],
            "isRise": score_unusual,
            "data": {
                "PM_number": f"第{int(_row['RANK'])}",  # 排名
                "PM_isRise": get_arrow_type(-_row['RANK_DIFF']),  # 排名是否上升
                "PM_diff": abs(_row['RANK_DIFF']),
                "PM_TB": _row['YEAR_DIFF_PERCENT'],  # 同比变化率
                "PM_TB_diff": round(_row['YEAR_DIFF'], 1) if not pd.isna(_row['YEAR_DIFF']) else '-',
                "PM_TB_isRise": int(_row['YEAR_DIFF_IS_RISE']),  # 同比是否上升
                "BH_number": '{}%'.format(score_trend),  # 变化趋势
                "BH_diff": round(analysis_row['SCORE_TREND_VALUE'], 1),
                "BH_isRise": get_arrow_type(analysis_row['SCORE_TREND_VALUE']),  # 变化趋势是否上升
                "BH_HB": _row['MON_DIFF_PERCENT'],  # 环比变化率
                "BH_HB_diff": round(_row['MON_DIFF'], 1) if not pd.isna(_row['MON_DIFF']) else '-',
                "BH_HB_isRise": int(_row['MON_DIFF_IS_RISE']),  # 环比是否上升
            }
        }
        score_data.append(_score_data)
        # 异动通知
        if not analysis_row.empty:
            if analysis_row['IS_UNUSUAL'] == 1:
                content = '{}异常'
                content = content.format(main_index_name)
                if _row['MON_DIFF'] < 0:
                    content += '，环比下降{}，下降值{}'.format(_row['MON_DIFF_PERCENT'].replace('-', ''),
                                                      abs(round(_row['MON_DIFF'], 2)))
                if _row['YEAR_DIFF'] < 0:
                    content += '，同比下降{}，下降值{}'.format(_row['YEAR_DIFF_PERCENT'].replace('-', ''),
                                                      abs(round(_row['YEAR_DIFF'], 2)))
                if analysis_row['TREND_UNUSUAL'] == 1:
                    content += '，连续三个月下降趋势较大，下降值{}'.format(abs(round(analysis_row['SCORE_TREND_VALUE'], 2)))
                elif analysis_row['SCORE_TREND_VALUE'] < 0:
                    content += '，下降趋势为{}，下降值{}'.format('{}%'.format(score_trend),
                                                       abs(round(analysis_row['SCORE_TREND_VALUE'], 2)))
                if analysis_row['COMPARE_UNUSUAL'] == 1:
                    content += '，得分与专业内其他单位对比较低'
                notice_data.append(content)

        report_list = []
        report_list.append('{}：{}分以下'.format(main_index_name, int(analysis_row['THRESHOLD'])))
        # report_list.append('组合预警条件：{}'.format(combine_warning_desc))
        # report_list.append('指数下降趋势预警阈值：{}'.format(round(analysis_row['TREND_THRESHOLD'], 2)))
        report_data.extend(report_list)

    main_type_groups = {'检查指数趋势': [1, 4], '问题指数趋势': [5, 6], '评价指数趋势': [2, 3],
                        '运用指数趋势': [8], '隐患指数趋势': [9, 10], '计划与盯控指数趋势': [11, 12]}
    for cate, types in main_type_groups.items():
        _data = {'title': cate, 'data': []}
        diff_months = set(index_data[index_data['MAIN_TYPE'].isin(types)].drop_duplicates(subset=['MON'], keep=False)[
            'MON'].values)
        for main_type in types:
            _df = index_data.loc[index_data['MAIN_TYPE'] == main_type,
                                 ['MON', 'MON_DESC', 'SCORE',
                                  'YEAR_DIFF_PERCENT', 'YEAR_DIFF_IS_RISE',
                                  'MON_DIFF_PERCENT', 'MON_DIFF_IS_RISE']].copy()
            # 有些指数某个月无数据，造成和同组的指数月份不一致
            if diff_months:
                lack_months = diff_months - set(_df['MON'].values)
                if lack_months:
                    _len = len(lack_months)
                    temp = pd.DataFrame({'MON': list(lack_months),
                                         'MON_DESC': ['{}月'.format(x % 100) for x in lack_months],
                                         'SCORE': ['缺失'] * _len,
                                         'YEAR_DIFF_PERCENT': ['-'] * _len,
                                         'YEAR_DIFF_IS_RISE': ['-'] * _len,
                                         'MON_DIFF_PERCENT': ['-'] * _len,
                                         'MON_DIFF_IS_RISE': ['-'] * _len},)
                    _df = _df.append(temp)
                    _df.sort_values(['MON'], ascending=True, inplace=True)
            _df.drop(['MON'], axis=1, inplace=True)
            if _df.empty:
                continue
            _data['data'].append({'title': get_index_title(main_type),
                                  'data': _df.values.tolist()})
        if len(_data['data']) > 0:
            charts_data.append(_data)

    if not notice_data:
        notice_data.append('暂无指数异动信息')

    result['score'] = score_data
    result['charts'] = charts_data
    result['notice'] = notice_data
    result['report'] = get_report_data(report_data, analysis_data).replace('<br/>', '\n')

    return result


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        execute()

