#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/12
Description: 
"""


def get_range_history_months(start_mon, length):
    """
    :param start_mon: etc. 201902
    :param length:  etc. 5
    :return: etc. [201902, 201901, 201812, 201811, 201810]
    """
    year = start_mon // 100
    month = start_mon % 100
    month_range = [int('{}{:0>2}'.format(year + mon // 12, mon % 12 + 1))
                   for mon in range(month - 1, month - length - 1, -1)]
    return month_range

