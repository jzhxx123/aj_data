#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:  执行事故故障与综合指数相关性计算
"""
import pandas as pd
import numpy as np
import pymongo

from app import mongo
from app.big_screen_analysis.risk_control_forecast_model import cache_client
from app.big_screen_analysis.safety_info.safety_info_data import get_keyun_stations, get_gongdian_stations
from app.data.util import get_coll_prefix, get_history_months, write_bulk_mongo
from app.big_screen_analysis.algorithm.safety_info_with_index import safety_supervision

main_index_map = {'t1': 1,  # check_intensity
                  't2': 2,  # evaluate_intensity
                  't3': 3,  # assess_intensity
                  't4': 4,  # check_evenness
                  't5': 5,  # problem_exposure
                  't6': 6}  # problem_rectification


SAFETY_INFO_ACCIDENT = 1
SAFETY_INFO_TROUBLE = 2


def _apply_to_add_major(row):
    major = row['PROFESSION']
    if row['TYPE3'] in keyun_stations:
        major = '客运'
    elif row['TYPE3'] in gongdian_stations:
        major = '工电'
    return major


def _apply_to_add_extra_data(row, detail_df, safety_info_df, safety_info_type):
    res = []
    for col, main_index in main_index_map.items():
        score = detail_df.loc[(detail_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])
                              & (detail_df['MAIN_TYPE'] == main_index)
                              & (detail_df['MON'] == row['MON'])]
        if not score.empty:
            score = score.iloc[0]['SCORE']
        else:
            score = 0
        res.append(score)

    mon = str(row['MON'])
    mon = '{}-{}'.format(mon[:4], mon[4:])
    safety_count = safety_info_df.loc[(safety_info_df['TYPE3'] == row['DEPARTMENT_ID'])
                                      & (safety_info_df['MONTH'] == mon)
                                      & (safety_info_df['MAIN_TYPE'] == safety_info_type)].index.size
    res.append(safety_count)
    return pd.Series(res, index=[x for x in main_index_map] + ['error'])


def add_department_info(df):
    df.reset_index(drop=True, inplace=True)
    month_length = len(df.groupby('MON').groups.keys())
    part_rank_df = pd.DataFrame(np.array(list(([x] * month_length for x in [1, 2, 3, -1, -2, -3]))).flatten(),
                                columns=['PART_RANK'])
    df = pd.merge(df, part_rank_df, left_index=True, right_index=True)
    for col in ['t1', 't2', 't3', 't4', 't5', 't6', 't0', 'MON', 'rank', 'error']:
        df[col] = df[col].astype(int)
    return df


def run(months_ago):
    safety_df = cache_client.get_or_set_global('safety_info_analysis')

    global keyun_stations, gongdian_stations
    keyun_stations = get_keyun_stations()
    gongdian_stations = get_gongdian_stations()

    safety_df['MAJOR'] = safety_df.apply(lambda _row: _apply_to_add_major(_row), axis=1)
    major_list = ['车务', '客运', '机务', '供电', '工电', '工务', '车辆', '电务']
    safety_df = safety_df[safety_df['MAJOR'].isin(major_list)]

    prefix = get_coll_prefix(months_ago)
    mon = get_history_months(months_ago)[0]
    next_mon = get_history_months(months_ago + 1)[0]
    health_index_score = list(mongo.db[f'{prefix}health_index'].find(
        {
            'HIERARCHY': 3,
        }, {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAJOR': 1
        }).sort('RANK', pymongo.ASCENDING))

    health_index_detail_score = list(mongo.db[f'{prefix}detail_health_index'].find(
        {'HIERARCHY': 3,
         'DETAIL_TYPE': 0},
        {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAIN_TYPE': 1,
            'MAJOR': 1
        }))

    health_index_score = pd.DataFrame(health_index_score)
    health_index_detail_score = pd.DataFrame(health_index_detail_score)

    for major in major_list:
        for safety_main_type in [SAFETY_INFO_ACCIDENT, SAFETY_INFO_TROUBLE]:
            major_health_index_score = health_index_score[health_index_score['MAJOR'] == major]
            drop_mon = []
            i = 0
            months_list = list(major_health_index_score.groupby('MON').groups)
            months_list.sort(reverse=True)
            for _mon in months_list:
                if _mon != get_history_months(months_ago - i)[0]:
                    drop_mon.append(_mon)
                i += 1
            major_health_index_score = major_health_index_score[~major_health_index_score['MON'].isin(drop_mon)]
            major_health_index_detail_score = health_index_detail_score[health_index_detail_score['MAJOR'] == major]
            input_data = pd.DataFrame(major_health_index_score,
                                      columns=['DEPARTMENT_ID', 'MON', 'MAJOR', 'SCORE', 'RANK'])
            extra_df = input_data.apply(
                lambda row: _apply_to_add_extra_data(row, major_health_index_detail_score, safety_df, safety_main_type),
                axis=1,
                result_type='expand')
            input_data = input_data.join(extra_df)
            # input_data = input_data[input_data['DEPARTMENT_ID'] == '19B8C3534E0D5665E0539106C00A58FD']
            input_data.rename(columns={'DEPARTMENT_ID': 'did', 'SCORE': 't0', 'RANK': 'rank', 'MAJOR': 'major'},
                              inplace=True)
            input_data.sort_values(['did', 'MON'], ascending=True, inplace=True)
            input_data.reset_index(drop=True, inplace=True)
            length = len(major_health_index_score.groupby('MON').groups)
            result = safety_supervision(input_data, mon, length)
            # 指数得分与事故故障数量变化
            index_and_safety_score = result[0]
            # 指数得分与事故故障相关性变化
            index_and_safety_relation = result[1]
            # 子指数与事故故障相关性
            sub_index_and_safety_relation = result[2]
            # 专业指数得分与事故故障趋势
            major_index_and_safety_score = result[3]
            # 指数得分与事故故障
            index_and_safety_relation_detail = result[4]

            department_df = pd.DataFrame(major_health_index_score,
                                         columns=['DEPARTMENT_ID', 'DEPARTMENT_NAME']).drop_duplicates()

            index_and_safety_score = pd.merge(
                index_and_safety_score, department_df,
                left_on='did', right_on='DEPARTMENT_ID', how='left')
            index_and_safety_relation = pd.merge(
                index_and_safety_relation, department_df,
                left_on='did', right_on='DEPARTMENT_ID', how='left')

            index_and_safety_score = add_department_info(index_and_safety_score)
            index_and_safety_relation = add_department_info(index_and_safety_relation)

            department_with_part_rank = pd.DataFrame(index_and_safety_score,
                                                     columns=['DEPARTMENT_ID', 'DEPARTMENT_NAME', 'PART_RANK'])
            department_with_part_rank.drop_duplicates(inplace=True)
            department_with_part_rank.reset_index(drop=True, inplace=True)

            sub_index_and_safety_relation = pd.DataFrame(sub_index_and_safety_relation,
                                                         columns=['t1', 't2', 't3', 't4', 't5', 't6'])
            sub_index_and_safety_relation = sub_index_and_safety_relation.astype(int)
            sub_index_and_safety_relation = pd.merge(sub_index_and_safety_relation, department_with_part_rank,
                                                     left_index=True, right_index=True)

            index_and_safety_relation_detail = pd.DataFrame(index_and_safety_relation_detail,
                                                            columns=['CORRELATION', 'COEFFICIENT', 'INTENSITY'])
            index_and_safety_relation_detail = pd.merge(index_and_safety_relation_detail, department_with_part_rank,
                                                        left_index=True, right_index=True)
            index_and_safety_relation_detail['CORRELATION'] = index_and_safety_relation_detail['CORRELATION'].astype(
                int)
            index_and_safety_relation_detail['INTENSITY'] = index_and_safety_relation_detail['INTENSITY'].astype(int)
            index_and_safety_relation_detail['SCORE'] = round(index_and_safety_relation_detail['COEFFICIENT'], 2)

            months_df = pd.DataFrame(list(index_and_safety_score.groupby('MON').groups.keys()) + [next_mon],
                                     columns=['MON'])
            major_index_and_safety_score = pd.DataFrame(major_index_and_safety_score,
                                                        columns=['SCORE', 'SAFETY_COUNT'])
            major_index_and_safety_score = pd.merge(major_index_and_safety_score, months_df,
                                                    left_index=True, right_index=True)
            major_index_and_safety_score['SAFETY_COUNT'] = major_index_and_safety_score['SAFETY_COUNT'].astype(int)
            major_index_and_safety_score['MON'] = major_index_and_safety_score['MON'].astype(int)
            major_index_and_safety_score['SCORE'] = round(major_index_and_safety_score['SCORE'], 2)

            coll_name = 'health_index_and_safety_analysis'
            mongo.db[coll_name].remove({'MAJOR': major, 'EXECUTE_MON': mon, 'SAFETY_INFO_MAIN_TYPE': safety_main_type})
            mongo_record = {'MAJOR': major,
                            'EXECUTE_MON': mon,
                            'SAFETY_INFO_MAIN_TYPE': safety_main_type,
                            'INDEX_AND_SAFETY_SCORE': index_and_safety_score.to_dict(orient='records'),
                            'INDEX_AND_SAFETY_RELATION': index_and_safety_relation.to_dict(orient='records'),
                            'SUB_INDEX_AND_SAFETY_RELATION': sub_index_and_safety_relation.to_dict(orient='records'),
                            'INDEX_AND_SAFETY_DETAIL': index_and_safety_relation_detail.to_dict(orient='records'),
                            'MAJOR_INDEX_AND_SAFETY_SCORE': major_index_and_safety_score.to_dict(orient='records')}
            write_bulk_mongo(coll_name, [mongo_record])


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        run(-1)
