#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/23
Description: 
"""
import pandas as pd

from app import mongo

from app.big_screen_analysis.risk_control_forecast_model.execute_forecast import main_index_map
from app.utils.safety_index_common_func import get_index_title, get_child_index_name


safety_info_type_map = {
    1: '事故',
    2: '故障'
}


def get_sub_index_relation_desc(department_name, safety_type, active_index):
    """

    :param department_name: 部门名称
    :param safety_type:  安全生产信息类型
    :param active_index:  相关的指数名称列表
    :return:
    """
    content = '{0}{1}发生数与{2}有较强的相关性。说明以上指数得分的高低可以比较准确反应{1}发生的多少'
    return content.format(department_name, safety_info_type_map.get(safety_type), '、'.join(active_index))


def get_total_relation_desc(correlation, coefficient, intensity, safety_type):
    """
    :param correlation: 相关性 1: 正 0: 负
    :param coefficient: 相关系数
    :param intensity: 强度 0：弱 1：强
    :return:
    """
    correlation_map = {0: ['负', '小'], 1: ['正', '大']}
    intensity_map = {0: '弱', 1: '强'}
    content = '整体{0}相关，综合安全指数得分越高，{1}发生数越{2}，相关系数{3}，相对性较{4}'
    content = content.format(correlation_map.get(correlation)[0], safety_info_type_map.get(safety_type),
                             correlation_map.get(correlation)[1], round(coefficient, 2), intensity_map.get(intensity))
    return content


def get_data(params_dict):
    major = params_dict['MAJOR']
    safety_info_type = int(params_dict['SAFETY_INFO_TYPE'])
    mon = int(params_dict['MONTH'])
    result = {}

    document = mongo.db['health_index_and_safety_analysis'].find_one(
        {'MAJOR': major, 'SAFETY_INFO_MAIN_TYPE': safety_info_type, 'EXECUTE_MON': mon})

    if not document:
        return result

    index_and_safety_score = pd.DataFrame(document['INDEX_AND_SAFETY_SCORE'])

    index_and_safety_score_top = index_and_safety_score[index_and_safety_score['PART_RANK'] > 0]
    index_and_safety_score_last = index_and_safety_score[index_and_safety_score['PART_RANK'] < 0]

    top_three_score = []
    for DEPARTMENT_NAME, group in index_and_safety_score_top.groupby('DEPARTMENT_NAME'):
        top_three_score.append({'label': DEPARTMENT_NAME,
                                'months': group['MON'].tolist(),
                                'scores': group['t0'].tolist(),
                                'errors': group['error'].tolist()})
    result['top_three_score'] = top_three_score

    last_three_score = []
    for DEPARTMENT_NAME, group in index_and_safety_score_last.groupby('DEPARTMENT_NAME'):
        last_three_score.append({'label': DEPARTMENT_NAME,
                                 'months': group['MON'].tolist(),
                                 'scores': group['t0'].tolist(),
                                 'errors': group['error'].tolist()})
    result['last_three_score'] = last_three_score

    index_and_safety_relation = pd.DataFrame(document['INDEX_AND_SAFETY_RELATION'])
    index_and_safety_relation_detail = pd.DataFrame(document['INDEX_AND_SAFETY_DETAIL'])
    index_and_safety_relation_top = index_and_safety_relation[index_and_safety_relation['PART_RANK'] > 0]
    index_and_safety_relation_last = index_and_safety_relation[index_and_safety_relation['PART_RANK'] < 0]
    top_three_relation = []
    for DEPARTMENT_NAME, group in index_and_safety_relation_top.groupby('DEPARTMENT_NAME'):
        relation_detail = index_and_safety_relation_detail[(index_and_safety_relation_detail['PART_RANK'] > 0)
                                                           & (index_and_safety_relation_detail[
                                                                  'DEPARTMENT_NAME'] == DEPARTMENT_NAME)]
        if not relation_detail.empty:
            relation_detail = relation_detail.iloc[0]
            desc = get_total_relation_desc(relation_detail['CORRELATION'], relation_detail['COEFFICIENT'],
                                           relation_detail['INTENSITY'], safety_info_type)
        else:
            desc = '暂无相关性描述'
        top_three_relation.append({'label': DEPARTMENT_NAME,
                                   'scores': group['t0'].tolist(),
                                   'errors': group['error'].tolist(),
                                   'months': group['MON'].tolist(),
                                   'desc': desc})
    result['top_three_relation'] = top_three_relation

    last_three_relation = []
    for DEPARTMENT_NAME, group in index_and_safety_relation_last.groupby('DEPARTMENT_NAME'):
        relation_detail = index_and_safety_relation_detail[(index_and_safety_relation_detail['PART_RANK'] < 0)
                                                           & (index_and_safety_relation_detail[
                                                                  'DEPARTMENT_NAME'] == DEPARTMENT_NAME)]
        if not relation_detail.empty:
            relation_detail = relation_detail.iloc[0]
            desc = get_total_relation_desc(relation_detail['CORRELATION'], relation_detail['COEFFICIENT'],
                                           relation_detail['INTENSITY'], safety_info_type)
        else:
            desc = '暂无相关性描述'
        last_three_relation.append({'label': DEPARTMENT_NAME,
                                    'scores': group['t0'].tolist(),
                                    'errors': group['error'].tolist(),
                                    'months': group['MON'].tolist(),
                                    'desc': desc})
    result['last_three_relation'] = last_three_relation

    major_index_and_safety_score = pd.DataFrame(document['MAJOR_INDEX_AND_SAFETY_SCORE'])
    major_score_forecast = {
        'months': major_index_and_safety_score['MON'].tolist(),
        'scores': major_index_and_safety_score['SCORE'].tolist(),
        'errors': major_index_and_safety_score['SAFETY_COUNT'].tolist()}
    result['major_score_forecast'] = major_score_forecast

    sub_index_and_safety_relation = pd.DataFrame(document['SUB_INDEX_AND_SAFETY_RELATION'])
    sub_index_and_safety_relation_top = sub_index_and_safety_relation[sub_index_and_safety_relation['PART_RANK'] > 0]
    sub_index_and_safety_relation_last = sub_index_and_safety_relation[sub_index_and_safety_relation['PART_RANK'] < 0]

    top_three_sub_index_relation = []
    for _, row in sub_index_and_safety_relation_top.iterrows():
        index_result = []
        for key, value in main_index_map.items():
            index_result.append({'label': get_index_title(value), 'active': row[key]})
        active_index = [x['label'] for x in index_result if x['active'] == 1]
        desc = get_sub_index_relation_desc(row['DEPARTMENT_NAME'], safety_info_type, active_index)
        top_three_sub_index_relation.append({'label': row['DEPARTMENT_NAME'],
                                             'index': index_result,
                                             'desc': desc})
    result['top_three_sub_index_relation'] = top_three_sub_index_relation

    last_three_sub_index_relation = []
    for _, row in sub_index_and_safety_relation_last.iterrows():
        index_result = []
        for key, value in main_index_map.items():
            index_result.append({'label': get_index_title(value), 'active': row[key]})
        active_index = [x['label'] for x in index_result if x['active'] == 1]
        desc = get_sub_index_relation_desc(row['DEPARTMENT_NAME'], safety_info_type, active_index)
        last_three_sub_index_relation.append({'label': row['DEPARTMENT_NAME'],
                                              'index': index_result,
                                              'desc': desc})
    result['last_three_sub_index_relation'] = last_three_sub_index_relation

    return result
