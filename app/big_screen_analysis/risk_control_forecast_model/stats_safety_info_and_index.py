#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/11
Description: 
"""

import pandas as pd
import numpy as np
import pymongo
from dateutil.relativedelta import relativedelta
from flask import current_app

from app import mongo
from app.big_screen_analysis.common.common_sql import check_item_sql, problem_base_risk_sql, \
    resp_unit_risk_sql, department_safety_sql
from app.data.health_index.common_sql import ZHANDUAN_DPID_SQL
from app.data.index.common import get_zhanduan_deparment
from app.data.util import pd_query
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.big_screen_analysis.util import get_range_history_months


resp_identify_map = {1: '全部责任',
                     2: '主要责任',
                     3: '同等主要责任',
                     4: '重要责任',
                     5: '同等重要责任'}

safety_info_main_type = {1: '事故', 2: '故障'}

safety_info_rank = {1: '严重', 2: '较严重', 3: '一般', 4: '低'}


def add_major_index_data(row, major_risk_data, major):
    responsibility_unit_id = row['PK_ID']
    occurrence_time = row['OCCURRENCE_TIME']

    check_item = pd_query(check_item_sql.format(responsibility_unit_id))
    problem_base_risk = pd_query(problem_base_risk_sql.format(responsibility_unit_id))
    resp_unit_risk = pd_query(resp_unit_risk_sql.format(responsibility_unit_id))

    related_check_items = set(check_item['FK_CHECK_ITEM_ID'].tolist())
    related_risks = set(problem_base_risk['FK_RISK_ID'].tolist() + resp_unit_risk['FK_RISK_ID'].tolist())

    related_index_list = set()
    related_index_name_list = set()

    for major_index in major_risk_data:
        major_index_risk_type = major_index['MAJOR_INDEX_RISK_TYPE']
        if major not in major_index_risk_type:
            continue
        if related_check_items.intersection(major_index['CHECK_ITEM_IDS']):
            related_index_list.add(major_index_risk_type)
            related_index_name_list.add(major_index['INDEX_NAME'])
        if related_risks.intersection(major_index['CHECK_RISK_IDS']):
            related_index_list.add(major_index_risk_type)
            related_index_name_list.add(major_index['INDEX_NAME'])

    delta = 0
    if occurrence_time.day >= current_app.config.get('UPDATE_DAY'):
        delta = 1
    occurrence_time = occurrence_time + relativedelta(months=delta)
    index_mon = int('{}{:0>2}'.format(occurrence_time.year, occurrence_time.month))
    analysis_months = get_range_history_months(index_mon, 3)

    append_data = [list(related_index_list),
                   ','.join(related_index_name_list),
                   safety_info_main_type.get(row['MAIN_TYPE']),
                   resp_identify_map.get(row['RESPONSIBILITY_IDENTIFIED']),
                   safety_info_rank.get(row['RANK']),
                   ','.join(list(map(str, analysis_months)))]
    return pd.Series(append_data,
                     index=['MAJOR_INDEX', 'MAJOR_INDEX_NAME', 'MAIN_TYPE_DESC', 'RESP_ID_DESC', 'RANK_DESC', 'MONTHS'])


def _apply_to_add_extra_data(row, detail_df, columns):
    res = []
    for main_type in columns:
        score = detail_df.loc[(detail_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])
                              & (detail_df['MAIN_TYPE'] == main_type)
                              & (detail_df['MON'] == row['MON'])]
        if not score.empty:
            score = score.iloc[0]['SCORE']
        else:
            score = np.nan
        res.append(score)

    return pd.Series(res, index=[f't{x}' for x in columns])


def get_major_index_data(major_index_type, months=None):
    prefix = 'monthly_'
    major, index_type = major_index_type.split('-')
    index_type = int(index_type)
    major_index_score = list(mongo.db[f'{prefix}major_index'].find(
        {
            'HIERARCHY': 3,
            'MAJOR': major,
            'TYPE': index_type
        }, {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAJOR': 1
        }).sort('DEPARTMENT_ID', pymongo.ASCENDING))

    major_index_detail_score = list(mongo.db[f'{prefix}detail_major_index'].find(
        {'HIERARCHY': 3,
         'DETAIL_TYPE': 0,
         'MAJOR': major,
         'TYPE': index_type
         },
        {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAIN_TYPE': 1,
            'MAJOR': 1
        }))

    major_index_score = pd.DataFrame(major_index_score)
    major_index_detail_score = pd.DataFrame(major_index_detail_score)

    input_data = pd.DataFrame(major_index_score,
                              columns=['DEPARTMENT_ID', 'MON', 'MAJOR', 'SCORE', 'RANK'])
    main_type_columns = list(major_index_detail_score.groupby('MAIN_TYPE').groups)
    extra_df = input_data.apply(
        lambda row: _apply_to_add_extra_data(row, major_index_detail_score, main_type_columns),
        axis=1,
        result_type='expand')
    input_data = input_data.join(extra_df)
    return input_data


def get_health_index_data(major):
    prefix = 'monthly_'
    health_index_score = list(mongo.db[f'{prefix}health_index'].find(
        {
            'HIERARCHY': 3,
            'MAJOR': major
        }, {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAJOR': 1
        }).sort('DEPARTMENT_ID', pymongo.ASCENDING))

    health_index_detail_score = list(mongo.db[f'{prefix}detail_health_index'].find(
        {'HIERARCHY': 3,
         'MAJOR': major,
         'DETAIL_TYPE': 0},
        {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1,
            'MAIN_TYPE': 1,
            'MAJOR': 1
        }))

    health_index_score = pd.DataFrame(health_index_score)
    health_index_detail_score = pd.DataFrame(health_index_detail_score)

    input_data = pd.DataFrame(health_index_score,
                              columns=['DEPARTMENT_ID', 'MON', 'MAJOR', 'SCORE', 'RANK'])
    main_type_columns = list(health_index_detail_score.groupby('MAIN_TYPE').groups)
    extra_df = input_data.apply(
        lambda row: _apply_to_add_extra_data(row, health_index_detail_score, main_type_columns),
        axis=1,
        result_type='expand')
    input_data = input_data.join(extra_df)
    return input_data


def run():
    department_id = '19B8C3534E1D5665E0539106C00A58FD'
    safety_data = pd_query(department_safety_sql.format(department_id))
    station_department_data = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)

    department_data = station_department_data[
        station_department_data['DEPARTMENT_ID'] == department_id].iloc[0]
    major = department_data['MAJOR']

    major_risk_data = list(mongo.db['major_index_item_and_risk_data'].find({}, {'_id': 0}))
    append_df = safety_data.apply(lambda r: add_major_index_data(r, major_risk_data, major), axis=1)
    safety_data = safety_data.join(append_df)

    writer = pd.ExcelWriter('data2.xls')
    safety_data.to_excel(writer, sheet_name='事故故障', index=False,
                         columns=['OCCURRENCE_TIME', 'DEPARTMENT_NAME', 'FK_DEPARTMENT_ID',
                                  'MAIN_TYPE_DESC', 'RANK_DESC', 'RESP_ID_DESC', 'MONTHS', 'MAJOR_INDEX_NAME'],
                         header=['发生时间', '部门名称', '部门ID', '大类', '等级', '责任认定', '指数分析月份',
                                 '涉及的重点风险指数'])

    health_index_data = get_health_index_data(major)
    health_index_data.to_excel(writer, sheet_name='安全综合指数', index=False)

    major_index_list = []
    for i in safety_data['MAJOR_INDEX'].tolist():
        major_index_list.extend(i)
    major_index_list = set(major_index_list)

    for major_index in major_index_list:
        index_data = get_major_index_data(major_index)
        index_data.to_excel(writer, sheet_name=major_risk_map.get(major_index)['name'], index=False)

    writer.save()
    print('ok')


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        run()
