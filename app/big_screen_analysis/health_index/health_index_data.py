#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/9/29
Description: 
"""

import pandas as pd
import pymongo

from app import mongo
from app.utils.common_func import choose_collection_prefix
from app.utils.safety_index_common_func import get_index_title, get_child_index_name

from app.big_screen_analysis.health_index.common import (get_past_months, get_main_index_weight,
                                                         settle_months_prefix, get_detail_index_weight,
                                                         main_index_name, get_department_location,
                                                         get_detail_score_data, get_index_weight, get_total_score_data,
                                                         get_format_total_score, get_format_main_index_score,
                                                         get_format_main_index_score_several)
from app.big_screen_analysis.health_index import jiwu, gongwu, gongdian, gw_gongdian, cheliang


def get_data(param_dict):
    request_mon = int(param_dict['MONTH'])

    major = param_dict['MAJOR']
    if request_mon < 201711:
        return 'MONTH should be > 201810'

    if major not in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        return 'MAJOR - %s INVALID' % major

    if major == '机务':
        return jiwu.get_data(param_dict)
    elif major == '工务':
        return gongwu.get_data(param_dict)
    elif major == '供电':
        return gongdian.get_data(param_dict)
    elif major == '工电':
        return gw_gongdian.get_data(param_dict)
    elif major == '车辆':
        return cheliang.get_data(param_dict)
    if major in ('客运', '车务'):
        return get_data_single_month(param_dict)
    return get_data_several_month(param_dict)


def get_data_single_month(param_dict):
    request_mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']

    result = {}
    location_dict = {}

    documents = get_total_score_data(request_mon, major)
    months_list = get_past_months(request_mon, 0)
    detail_data = get_detail_score_data(months_list, major)
    main_index_weight, detail_index_weight = get_index_weight(months_list)

    result['total'] = get_format_total_score(request_mon, documents, detail_data, main_index_weight, location_dict)

    for key, main_index in main_index_name.items():
        result[main_index] = get_format_main_index_score(request_mon, documents, detail_data, key, detail_index_weight,
                                                         location_dict)

    return result


def get_data_several_month(param_dict):
    request_mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']

    result = {}
    location_dict = {}
    if major in ['供电', '电务', '工电', '工务']:
        past_months_count = 2
    else:
        past_months_count = 5
    documents = get_total_score_data(request_mon, major)
    months_list = get_past_months(request_mon, past_months_count)
    detail_data = get_detail_score_data(months_list, major)
    main_index_weight, detail_index_weight = get_index_weight(months_list)

    result['total'] = get_format_total_score(request_mon, documents, detail_data, main_index_weight, location_dict)

    for key, main_index in main_index_name.items():
        result[main_index] = get_format_main_index_score_several(months_list, documents, detail_data, key,
                                                                 detail_index_weight, location_dict)

    return result
