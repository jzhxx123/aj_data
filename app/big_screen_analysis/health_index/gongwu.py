#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/15
Description: 
"""

from app.big_screen_analysis.health_index.common import (get_past_months, main_index_name,
                                                         get_format_detail_index_score_avg, get_total_score_data,
                                                         get_detail_score_data, get_index_weight,
                                                         get_format_total_score, get_format_main_index_score_avg)


def get_data(param_dict):
    request_mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']

    result = {}

    location_dict = {}
    if major in ['供电', '电务', '工电', '工务']:
        past_months_count = 2
    else:
        past_months_count = 5
    documents = get_total_score_data(request_mon, major)
    months_list = get_past_months(request_mon, past_months_count)
    detail_data = get_detail_score_data(months_list, major)
    main_index_weight, detail_index_weight = get_index_weight(months_list)

    result['total'] = get_format_total_score(request_mon, documents, detail_data, main_index_weight, location_dict)

    for key, main_index in main_index_name.items():
        if key in (2, 5):
            result[main_index] = get_format_main_index_score_avg(months_list, detail_data, key, detail_index_weight)
        else:
            result[main_index] = get_format_detail_index_score_avg(months_list, detail_data, key, detail_index_weight)

    return result
