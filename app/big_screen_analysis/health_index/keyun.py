#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/9
Description: 
"""

import pandas as pd

import pymongo

from app import mongo
from app.utils.common_func import choose_collection_prefix
from app.utils.safety_index_common_func import get_index_title, get_child_index_name

main_index_name = {1: 'check_intensity',
                   2: 'evaluate_intensity',
                   3: 'assess_intensity',
                   4: 'check_evenness',
                   5: 'problem_exposure',
                   6: 'problem_rectification'}


def get_past_months(start_month, number=2):
    res = [start_month]
    current = start_month
    for i in range(1, number + 1):
        if current % 100 == 1:
            last_mon = current - 100 + 11
        else:
            last_mon = current - 1
        current = last_mon
        res.append(last_mon)
    return res


def settle_months_prefix(months_list):
    res = dict()
    for mon in months_list:
        prefix = choose_collection_prefix(mon)
        months = res.setdefault(prefix, [])
        months.append(mon)
    return res


def get_main_index_weight(main_index_weight):
    res = {}
    for _, row in main_index_weight.iterrows():
        res[row['MAIN_TYPE']] = row['WEIGHT']
    return res


def get_department_location(department_id, mon):
    """
    获取站段的经纬度字典
    :return:
    """
    record = mongo.db['monthly_map_data'].find_one({'DPID': department_id, 'MON': mon})
    if record:
        return [record['LONGITUDE'], record['LATITUDE']]
    record = mongo.db['monthly_map_data'].find_one({'DPID': department_id}, sort=[('MON', pymongo.DESCENDING)])
    if record:
        return [record['LONGITUDE'], record['LATITUDE']]
    return []


def get_detail_index_weight(main_type, detail_type, mon, weight_df):
    df = weight_df[(weight_df['MAIN_TYPE'] == main_type)
                   & (weight_df['DETAIL_TYPE'] == detail_type)
                   & (weight_df['MON'] == mon)]
    if df.empty:
        df = weight_df[(weight_df['MAIN_TYPE'] == main_type)
                       & (weight_df['DETAIL_TYPE'] == detail_type)]
    if not df.empty:
        return df.iloc[0]['WEIGHT']
    return 0


def get_data_single_month(param_dict):
    mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {'MON': mon, 'HIERARCHY': 3, 'DETAIL_TYPE': 0}

    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({'MAJOR': major})
    else:
        return 'MAJOR - %s INVALID' % major

    result = {}
    detail_data = []
    location_dict = {}
    prefix = choose_collection_prefix(mon)
    months_list = get_past_months(mon)

    main_index_weight = mongo.db['base_index_weight'].find({'INDEX_TYPE': 0})
    main_index_weight = pd.DataFrame(list(main_index_weight))
    main_index_weight = get_main_index_weight(main_index_weight)
    detail_index_weight = mongo.db['monthly_base_detail_index_weight'].find(
        {'INDEX_TYPE': 0, 'MON': {'$in': months_list}})
    detail_index_weight = pd.DataFrame(list(detail_index_weight))

    for mon_prefix, months in settle_months_prefix(months_list).items():
        _detail_data = mongo.db[f'{mon_prefix}detail_health_index'].find(
            {'HIERARCHY': 3,
             'MAJOR': major,
             'MON': {'$in': months}},
            {
             '_id': 0,
             'DEPARTMENT_ID': 1,
             'DEPARTMENT_NAME': 1,
             'RANK': 1,
             'SCORE': 1,
             'MON': 1,
             'MAIN_TYPE': 1,
             'DETAIL_TYPE': 1
            })
        detail_data.extend(list(_detail_data))

    detail_data = pd.DataFrame(detail_data)

    documents = list(mongo.db[f'{prefix}health_index'].find(
        {
            'MAJOR': major,
            'MON': mon,
            'HIERARCHY': 3,
        }, {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1
        }).sort('RANK', pymongo.ASCENDING))

    data_total = []

    for department_data in documents:
        sub_index = []
        department_id = department_data['DEPARTMENT_ID']
        _data = detail_data[(detail_data['DEPARTMENT_ID'] == department_id)
                            & (detail_data['MON'] == mon)
                            & (detail_data['DETAIL_TYPE'] == 0)]

        for _, row in _data.iterrows():
            main_type = row['MAIN_TYPE']
            weight = main_index_weight.get(main_type, 0)
            sub_index.append(
                {'label': get_index_title(main_type), 'score': row['SCORE'], 'percentage': f'{int(weight * 100)}%',
                 'computeScore': row['SCORE'] * weight})
        location = location_dict.setdefault(department_id, get_department_location(department_id, mon))
        data_total.append({'name': department_data['DEPARTMENT_NAME'], 'rank': department_data['RANK'],
                           'totalScore': department_data['SCORE'], 'problemInfo': '', 'coord': location,
                           'subindex': sub_index})

    result['total'] = data_total

    for key, main_index in main_index_name.items():
        main_index_data = []
        _detail_index_weight = {}
        for department_data in documents:
            _data = detail_data[(detail_data['DEPARTMENT_ID'] == department_data['DEPARTMENT_ID'])
                                & (detail_data['MON'] == mon)
                                & (detail_data['MAIN_TYPE'] == key)]
            main_index_score = _data[_data['DETAIL_TYPE'] == 0].iloc[0]['SCORE']
            child_index_data = []
            for _, row in _data.iterrows():
                if row['DETAIL_TYPE'] == 0:
                    continue
                weight = _detail_index_weight.setdefault(
                    row['DETAIL_TYPE'],
                    get_detail_index_weight(key, row['DETAIL_TYPE'], mon, detail_index_weight))
                child_index_data.append(
                    {'label': get_child_index_name(row['DETAIL_TYPE'], row['MAIN_TYPE']), 'score': row['SCORE'],
                     'percentage': f'{int(weight * 100)}%', 'computeScore': row['SCORE'] * weight})
            main_index_data.append(
                {'name': department_data['DEPARTMENT_NAME'], 'totalScore': main_index_score,
                 'coord': location_dict.get(department_data['DEPARTMENT_ID'], []), 'index': child_index_data})
        result[main_index] = main_index_data

    return result
