#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/16
Description: 
"""


check_info_and_person = """SELECT
        b.FK_DEPARTMENT_ID,
        a.PK_ID, a.CHECK_WAY, a.CHECK_TYPE, a.PROBLEM_REVIEW_NUMBER,
        a.SUBMIT_TIME, a.START_CHECK_TIME, a.END_CHECK_TIME, c.TYPE3
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_department AS c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE 1
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.TYPE3 IN {2}
"""