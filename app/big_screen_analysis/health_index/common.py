#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/14
Description: 
"""

import pandas as pd
import pymongo

from app import mongo
from app.big_screen_analysis.health_index import cache_client
from app.utils.common_func import choose_collection_prefix, get_history_months
from app.utils.safety_index_common_func import get_index_title, get_child_index_name


main_index_name = {1: 'check_intensity',
                   2: 'evaluate_intensity',
                   3: 'assess_intensity',
                   4: 'check_evenness',
                   5: 'problem_exposure',
                   6: 'problem_rectification'}


def get_past_months(start_month, number=5):
    """
    获取过去月份列表
    :param start_month:
    :param number:
    :return:
    """
    res = [start_month]
    current = start_month
    for i in range(1, number + 1):
        if current % 100 == 1:
            last_mon = current - 100 + 11
        else:
            last_mon = current - 1
        current = last_mon
        res.append(last_mon)
    return res


def settle_months_prefix(months_list):
    """
    归类表前缀
    :param months_list:
    :return:
    """
    res = dict()
    for mon in months_list:
        prefix = choose_collection_prefix(mon)
        months = res.setdefault(prefix, [])
        months.append(mon)
    return res


def get_department_location(department_id, mon=None):
    """
    获取站段的经纬度字典
    :return:
    """
    record = None
    if mon:
        record = mongo.db['monthly_map_data'].find_one({'DPID': department_id, 'MON': mon},
                                                       {'_id': 0, 'LONGITUDE': 1, 'LATITUDE': 1})
    if record:
        return [record['LONGITUDE'], record['LATITUDE']]
    record = mongo.db['monthly_map_data'].find_one({'DPID': department_id},
                                                   {'_id': 0, 'LONGITUDE': 1, 'LATITUDE': 1},
                                                   sort=[('MON', pymongo.DESCENDING)])
    if record:
        return [record['LONGITUDE'], record['LATITUDE']]
    return []


def get_main_index_weight(main_index_weight):
    """
    获取指数权重字典
    :param main_index_weight:
    :return:
    """
    res = {}
    for _, row in main_index_weight.iterrows():
        res[row['MAIN_TYPE']] = row['WEIGHT']
    return res


def get_detail_index_weight(main_type, detail_type, mon, weight_df):
    """
    获取指数权重
    :param main_type:
    :param detail_type:
    :param mon:
    :param weight_df:
    :return:
    """
    df = weight_df[(weight_df['MAIN_TYPE'] == main_type)
                   & (weight_df['DETAIL_TYPE'] == detail_type)
                   & (weight_df['MON'] == mon)]
    if df.empty:
        df = weight_df[(weight_df['MAIN_TYPE'] == main_type)
                       & (weight_df['DETAIL_TYPE'] == detail_type)]
    if not df.empty:
        return df.iloc[0]['WEIGHT']
    return 0


def get_detail_score_data(months_list, major):
    """
    从mongodb获得子指数得分数据
    :param months_list:
    :param major:
    :return:
    """
    detail_data = []
    for mon_prefix, months in settle_months_prefix(months_list).items():
        _detail_data = mongo.db[f'{mon_prefix}detail_health_index'].find(
            {'HIERARCHY': 3,
             'MAJOR': major,
             'MON': {'$in': months}},
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAIN_TYPE': 1,
                'DETAIL_TYPE': 1
            })
        detail_data.extend(list(_detail_data))
    return pd.DataFrame(detail_data)


def get_total_score_data(request_mon, major):
    """
    从mongodb获取综合指数总得分数据
    :param request_mon:
    :param major:
    :return:
    """
    prefix = choose_collection_prefix(request_mon)
    return list(mongo.db[f'{prefix}health_index'].find(
        {
            'MAJOR': major,
            'MON': request_mon,
            'HIERARCHY': 3,
        }, {
            '_id': 0,
            'DEPARTMENT_ID': 1,
            'DEPARTMENT_NAME': 1,
            'RANK': 1,
            'SCORE': 1,
            'MON': 1
        }).sort('RANK', pymongo.ASCENDING))


def get_total_score_data_several(months_list, major):
    total_index_data = []
    for mon_prefix, months in settle_months_prefix(months_list).items():
        _total_index_data = mongo.db[f'{mon_prefix}health_index'].find(
            {'HIERARCHY': 3,
             'MAJOR': major,
             'MON': {'$in': months}},
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1
            })
        total_index_data.extend(list(_total_index_data))

    total_index_data = pd.DataFrame(total_index_data)
    return total_index_data


def get_index_weight(months_list):
    """
    获取指数与子指数权重数据集
    :param months_list:
    :return:
    """
    main_index_weight = mongo.db['base_index_weight'].find({'INDEX_TYPE': 0}, {'_id': 0})
    main_index_weight = pd.DataFrame(list(main_index_weight))
    main_index_weight = get_main_index_weight(main_index_weight)
    detail_index_weight = mongo.db['monthly_base_detail_index_weight'].find(
        {'INDEX_TYPE': 0, 'MON': {'$in': months_list}}, {'_id': 0})
    detail_index_weight = pd.DataFrame(list(detail_index_weight))
    return main_index_weight, detail_index_weight


def get_format_total_score(request_mon, documents, detail_data, main_index_weight, location_dict):
    """
    获取规定格式的指数总得分数据
    :param request_mon:
    :param documents:
    :param detail_data:
    :param main_index_weight:
    :param location_dict:
    :return:
    """
    data_total = []
    for department_data in documents:
        sub_index = []
        department_id = department_data['DEPARTMENT_ID']
        _data = detail_data[(detail_data['DEPARTMENT_ID'] == department_id)
                            & (detail_data['MON'] == request_mon)
                            & (detail_data['DETAIL_TYPE'] == 0)]

        for _, row in _data.iterrows():
            main_type = row['MAIN_TYPE']
            weight = main_index_weight.get(main_type, 0)
            sub_index.append(
                {'label': get_index_title(main_type), 'score': row['SCORE'], 'percentage': f'{int(weight * 100)}%',
                 'computeScore': row['SCORE'] * weight})
        location = location_dict.setdefault(department_id, get_department_location(department_id, request_mon))
        data_total.append({'name': department_data['DEPARTMENT_NAME'], 'rank': department_data['RANK'],
                           'totalScore': department_data['SCORE'], 'problemInfo': '', 'coord': location,
                           'subindex': sub_index})
    return data_total


def get_format_total_score_several(documents, months_list, total_index_data):
    """
    获取指数总得分数据，多月
    :param documents:
    :param months_list:
    :param total_index_data:
    :return:
    """
    total_index_list = []
    for department_data in documents:
        mon_data_list = []
        for mon in months_list:
            score = total_index_data[(total_index_data['DEPARTMENT_ID'] == department_data['DEPARTMENT_ID'])
                                     & (total_index_data['MON'] == mon)].iloc[0]['SCORE']
            mon_data_list.append({'name': department_data['DEPARTMENT_NAME'], 'month': mon,
                                  'rank': department_data['RANK'], 'totalScore': score, 'problemInfo': ''})
        total_index_list.append(mon_data_list)
    return total_index_list


def get_format_main_index_score(request_mon, documents, detail_data, main_index_key, detail_index_weight,
                                location_dict):
    """
    获取规定格式的子指数得分数据，单月
    :param request_mon:
    :param documents:
    :param detail_data:
    :param main_index_key:
    :param detail_index_weight:
    :param location_dict:
    :return:
    """
    main_index_data = []
    _detail_index_weight = {}
    for department_data in documents:
        _data = detail_data[(detail_data['DEPARTMENT_ID'] == department_data['DEPARTMENT_ID'])
                            & (detail_data['MON'] == request_mon)
                            & (detail_data['MAIN_TYPE'] == main_index_key)]
        main_index_score = _data[_data['DETAIL_TYPE'] == 0].iloc[0]['SCORE']
        child_index_data = []
        for _, row in _data.iterrows():
            if row['DETAIL_TYPE'] == 0:
                continue
            weight = _detail_index_weight.setdefault(
                row['DETAIL_TYPE'],
                get_detail_index_weight(main_index_key, row['DETAIL_TYPE'], request_mon, detail_index_weight))
            child_index_data.append(
                {'label': get_child_index_name(row['DETAIL_TYPE'], row['MAIN_TYPE']), 'score': row['SCORE'],
                 'percentage': f'{int(weight * 100)}%', 'computeScore': row['SCORE'] * weight})
        main_index_data.append(
            {'name': department_data['DEPARTMENT_NAME'], 'totalScore': main_index_score,
             'rank': int(_data[_data['DETAIL_TYPE'] == 0].iloc[0]['RANK']),
             'coord': location_dict.get(department_data['DEPARTMENT_ID'], []),
             'index': child_index_data})
    main_index_data = sorted(main_index_data, key=lambda x: x['rank'])
    return main_index_data


def get_format_main_index_score_several(months_list, documents, detail_data, main_index_key, detail_index_weight,
                                        location_dict):
    """
    获取规定格式的子指数得分数据，多月
    :param months_list:
    :param documents:
    :param detail_data:
    :param main_index_key:
    :param detail_index_weight:
    :param location_dict:
    :return:
    """
    main_index_data = []
    _detail_index_weight = {}
    for department_data in documents:
        several_mon_data = []
        for mon in months_list:
            _data = detail_data[(detail_data['DEPARTMENT_ID'] == department_data['DEPARTMENT_ID'])
                                & (detail_data['MON'] == mon)
                                & (detail_data['MAIN_TYPE'] == main_index_key)]
            main_index_score = _data[_data['DETAIL_TYPE'] == 0].iloc[0]['SCORE']
            child_index_data = []
            for _, row in _data.iterrows():
                if row['DETAIL_TYPE'] == 0:
                    continue
                weight = _detail_index_weight.setdefault(
                    row['DETAIL_TYPE'],
                    get_detail_index_weight(main_index_key, row['DETAIL_TYPE'], mon, detail_index_weight))
                child_index_data.append(
                    {'label': get_child_index_name(row['DETAIL_TYPE'], row['MAIN_TYPE']), 'score': row['SCORE'],
                     'percentage': f'{int(weight * 100)}%', 'computeScore': row['SCORE'] * weight})
            several_mon_data.append(
                {'name': department_data['DEPARTMENT_NAME'], 'totalScore': main_index_score, 'month': mon,
                 'coord': location_dict.get(department_data['DEPARTMENT_ID'], []), 'index': child_index_data})
        main_index_data.append(several_mon_data)
    return main_index_data


def iter_to_set_time_section(date):
    hour = date.hour
    if 6 <= hour < 14:
        return 'morning'
    elif 14 <= hour < 22:
        return 'noon'
    else:
        return 'night'


def get_check_time_section_data(documents, months_list):
    """
    获取按时间段统计的检查次数数据
    :param documents:
    :param months_list:
    :return:
    """
    check_time_section_data = []
    time_section = ('morning', 'noon', 'night')
    department_list = tuple([x['DEPARTMENT_ID'] for x in documents])
    mon_check_info = {}
    for department_data in documents:
        department_time_section_data = []
        for mon in months_list:
            mon_section_data = []
            mon_delta = mon - get_history_months(0)[0]
            check_info = mon_check_info.get(mon, None)
            if check_info is None:
                check_info = cache_client.get_or_set_global('check_info_and_person', months_ago=mon_delta)
                check_info = check_info[check_info['TYPE3'].isin(department_list)].copy()
                check_info['TIME_SECTION'] = check_info['END_CHECK_TIME'].apply(lambda v: iter_to_set_time_section(v))
                check_info['COUNT'] = 1
                check_info = check_info.groupby(['TYPE3', 'TIME_SECTION'], as_index=False)['COUNT'].sum()
                mon_check_info[mon] = check_info

            for section in time_section:
                record = check_info[(check_info['TYPE3'] == department_data['DEPARTMENT_ID'])
                                    & (check_info['TIME_SECTION'] == section)]
                if not record.empty:
                    check_count = int(record.iloc[0]['COUNT'])
                else:
                    check_count = 0
                mon_section_data.append({'time_section': section, 'count': check_count})
            department_time_section_data.append({'name': department_data['DEPARTMENT_NAME'],
                                                 'month': mon, 'section_data': mon_section_data})
        check_time_section_data.append(department_time_section_data)
    return check_time_section_data


def get_format_detail_index_score_avg(months_list, detail_data, main_index_key, detail_index_weight):
    """
    获取专业指数子指数平均分数据
    :param months_list:
    :param detail_data:
    :param main_index_key:
    :param detail_index_weight:
    :return:
    """
    main_index_data = []
    _detail_index_weight = {}
    several_mon_data = []
    for mon in months_list:
        _data = detail_data[(detail_data['MON'] == mon)
                            & (detail_data['MAIN_TYPE'] == main_index_key)
                            & ~(detail_data['DETAIL_TYPE'] == 0)]
        child_index_data = []
        for detail_type, group_df in _data.groupby('DETAIL_TYPE'):
            detail_index_avg_score = group_df['SCORE'].mean()
            weight = _detail_index_weight.setdefault(detail_type,
                                                     get_detail_index_weight(main_index_key, detail_type, mon,
                                                                             detail_index_weight))
            child_index_data.append(
                {'label': get_child_index_name(detail_type, main_index_key), 'score': detail_index_avg_score,
                 'percentage': f'{int(weight * 100)}%'})
        several_mon_data.append(
            {'month': mon, 'index': child_index_data})
    main_index_data.append(several_mon_data)
    return main_index_data


def get_format_main_index_score_avg(months_list, detail_data, main_index_key, detail_index_weight):
    """
    获取专业指数平均分数据
    :param months_list:
    :param detail_data:
    :param main_index_key:
    :param detail_index_weight:
    :return:
    """
    main_index_data = []
    _detail_index_weight = {}
    several_mon_data = []
    for mon in months_list:
        _data = detail_data[(detail_data['MON'] == mon)
                            & (detail_data['MAIN_TYPE'] == main_index_key)
                            & (detail_data['DETAIL_TYPE'] == 0)]
        avg_score = round(_data['SCORE'].mean(), 2)
        # child_index_data = []
        # for detail_type, group_df in _data.groupby('DETAIL_TYPE'):
        #     detail_index_avg_score = group_df['SCORE'].mean()
        #     weight = _detail_index_weight.setdefault(detail_type,
        #                                              get_detail_index_weight(main_index_key, detail_type, mon,
        #                                                                      detail_index_weight))
        #     child_index_data.append(
        #         {'label': get_child_index_name(detail_type, main_index_key), 'score': detail_index_avg_score,
        #          'percentage': f'{int(weight * 100)}%'})
        several_mon_data.append(
            {'month': mon, 'score': avg_score})
    main_index_data.append(several_mon_data)
    return main_index_data


def get_check_time_weekday_data(request_mon, documents):
    """
    获取按星期统计的检查次数数据
    :param request_mon:
    :param documents:
    :return:
    """
    check_info = cache_client.get_or_set_global('check_info_and_person',
                                                months_ago=request_mon - get_history_months(0)[0])
    department_list = [x['DEPARTMENT_ID'] for x in documents]
    check_info = check_info[check_info['TYPE3'].isin(department_list)].copy()
    check_info['WEEKDAY'] = check_info['SUBMIT_TIME'].dt.weekday
    check_info['COUNT'] = 1
    check_info = check_info.groupby(['TYPE3', 'WEEKDAY'], as_index=False)['COUNT'].sum()

    weekday_data = []
    for department_data in documents:
        department_weekday_day_data = []
        for weekday in range(0, 7):
            record = check_info[(check_info['TYPE3'] == department_data['DEPARTMENT_ID'])
                                & (check_info['WEEKDAY'] == weekday)]
            if not record.empty:
                check_count = int(record.iloc[0]['COUNT'])
            else:
                check_count = 0
            department_weekday_day_data.append({'weekday': weekday, 'count': check_count})
        weekday_data.append({'name': department_data['DEPARTMENT_NAME'], 'check_data': department_weekday_day_data})
    return weekday_data
