#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/16
Description: 
"""

from app.big_screen_analysis.health_index.common import (get_past_months, main_index_name,
                                                         get_format_main_index_score_several, get_total_score_data,
                                                         get_detail_score_data, get_index_weight,
                                                         get_format_total_score, get_check_time_section_data)


def get_data(param_dict):
    request_mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']

    result = {}
    location_dict = {}
    if major in ['供电', '电务', '工电', '工务']:
        past_months_count = 2
    else:
        past_months_count = 5
    months_list = get_past_months(request_mon, past_months_count)
    main_index_weight, detail_index_weight = get_index_weight(months_list)
    detail_data = get_detail_score_data(months_list, major)
    documents = get_total_score_data(request_mon, major)

    result['total'] = get_format_total_score(request_mon, documents, detail_data, main_index_weight, location_dict)

    for key, main_index in main_index_name.items():
        result[main_index] = get_format_main_index_score_several(months_list, documents, detail_data, key,
                                                                 detail_index_weight, location_dict)

    result['check_period'] = get_check_time_section_data(documents, months_list)

    return result
