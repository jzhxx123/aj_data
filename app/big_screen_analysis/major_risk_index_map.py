#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/11
Description:  重点指数整理
"""

from app import mongo
from app.data.index.util import get_query_condition_by_risktype
from app.data.util import write_bulk_mongo

major_risk_map = {'供电-1': {'name': '设备质量风险', 'risk_name': 9, 'index_code': 'GD-1'},
                  '供电-3': {'name': '专业管理风险', 'risk_name': 5, 'index_code': 'GD-3'},
                  '供电-6': {'name': '劳安风险', 'risk_name': 35, 'index_code': 'GD-6'},
                  '供电-7': {'name': '施工安全', 'risk_name': 34, 'index_code': 'GD-7'},
                  '供电-9': {'name': '自轮设备运用风险', 'risk_name': 55, 'index_code': 'GD-9'},
                  '供电-10': {'name': '施工配合监管风险', 'risk_name': 54, 'index_code': 'GD-10'},
                  # '电务-1': {'name': '电务普铁信号设备质量风险分析', 'risk_name': 23},
                  # '电务-2': {'name': '电务普铁通信设备质量风险分析', 'risk_name': 24},
                  # '电务-3': {'name': '电务高铁通信设备质量风险分析', 'risk_name': 26},
                  # '电务-4': {'name': '电务高铁信号设备质量风险分析', 'risk_name': 25},
                  '电务-5': {'name': '电务劳动安全风险分析', 'risk_name': 30, 'index_code': 'DW-5'},
                  '电务-6': {'name': '电务施工安全风险分析', 'risk_name': 91, 'index_code': 'DW-6'},
                  '电务-7': {'name': '电务道岔设备', 'risk_name': 93, 'index_code': 'DW-7'},
                  '电务-8': {'name': '点外修', 'risk_name': 147, 'index_code': 'DW-8'},
                  '电务-9': {'name': '电务轨道电路', 'risk_name': 148, 'index_code': 'DW-9'},
                  '电务-10': {'name': '电务信号机', 'risk_name': 149, 'index_code': 'DW-10'},
                  '车务-1': {'name': '调车', 'risk_name': 2, 'index_code': 'CW-1'},
                  '车务-2': {'name': '劳动安全', 'risk_name': 18, 'index_code': 'CW-2'},
                  # '车务-3': {'name': '车门管理', 'risk_name': 31},
                  # '车务-4': {'name': '客运组织', 'risk_name': 32},
                  # '车务-5': {'name': '多方向接发列车', 'risk_name': 16},
                  '车务-6': {'name': '接发列车', 'risk_name': 15, 'index_code': 'CW-6'},
                  '车务-7': {'name': '高动客接发列车', 'risk_name': 17, 'index_code': 'CW-7'},
                  '车务-8': {'name': '货装安全风险', 'risk_name': 48, 'index_code': 'CW-8'},
                  # '车务-9': {'name': '接发列车错办风险', 'risk_name': 16},
                  '车务-10': {'name': '调乘一体化', 'risk_name': 114, 'index_code': 'CW-10'},
                  '机务-1': {'name': '间断瞭望', 'risk_name': 13, 'index_code': 'JW-1'},
                  '机务-2': {'name': '调车风险', 'risk_name': 14, 'index_code': 'JW-2'},
                  # '机务-3': {'name': '漏检漏修、简化修风险', 'risk_name': 22},
                  '机务-4': {'name': '错误操纵风险', 'risk_name': 20, 'index_code': 'JW-4'},
                  '机务-5': {'name': '劳安风险', 'risk_name': 19, 'index_code': 'JW-5'},
                  '机务-6': {'name': '机车质量风险', 'risk_name': 22, 'index_code': 'JW-6'},
                  '机务-7': {'name': '库内牵车', 'risk_name': 151, 'index_code': 'JW-7'},
                  '车辆-1': {'name': '火灾爆炸', 'risk_name': 8, 'index_code': 'CL-1'},
                  '车辆-2': {'name': '劳动安全', 'risk_name': 40, 'index_code': 'CL-2'},
                  # '车辆-3': {'name': '客车制动供风', 'risk_name': 43},
                  # '车辆-4': {'name': '货车制动抱闸', 'risk_name': 44},
                  '车辆-5': {'name': '高压牵引', 'risk_name': 39, 'index_code': 'CL-5'},
                  # '车辆-6': {'name': '车辆脱轨', 'risk_name': 45},
                  # '车辆-7': {'name': '列车分离', 'risk_name': 46},
                  '车辆-8': {'name': '配件脱落(货车)', 'risk_name': 37, 'index_code': 'CL-8'},
                  '车辆-9': {'name': '配件脱落(动车)', 'risk_name': 38, 'index_code': 'CL-9'},
                  '车辆-10': {'name': '配件脱落(客车)', 'risk_name': 42, 'index_code': 'CL-10'},
                  '车辆-11': {'name': '调车防溜', 'risk_name': 41, 'index_code': 'CL-11'},
                  '工务-1': {'name': '防断风险', 'risk_name': 12, 'index_code': 'GW-1'},
                  '工务-2': {'name': '劳动安全风险', 'risk_name': 66, 'index_code': 'GW-2'},
                  '工务-3': {'name': '点外修作业风险', 'risk_name': 90, 'index_code': 'GW-3'},
                  '工务-4': {'name': '防洪风险', 'risk_name': 29, 'index_code': 'GW-4'},
                  '工务-5': {'name': '自轮设备风险', 'risk_name': 28, 'index_code': 'GW-5'},
                  '工务-6': {'name': '施工安全风险', 'risk_name': 87, 'index_code': 'GW-6'},
                  # '工务-7': {'name': '施工风险', 'risk_name': 88},
                  # '工务-8': {'name': '天窗修风险', 'risk_name': 89},
                  '工务-9': {'name': '施工监管风险', 'risk_name': 62, 'index_code': 'GW-9'},
                  '工务-10': {'name': '站专线风险', 'risk_name': 92, 'index_code': 'GW-10'},
                  '工务-11': {'name': '天窗施工单项', 'risk_name': [89, 88], 'index_code': 'GW-11'},
                  '工电-1': {'name': '劳动安全风险', 'risk_name': [66, 30, 35], 'index_code': 'GW_GD-1'},
                  '工电-2': {'name': '施工安全风险', 'risk_name': [87, 34, 91], 'index_code': 'GW_GD-2'},
                  '工电-3': {'name': '点外修作业风险', 'risk_name': [90, 147], 'index_code': 'GW_GD-3'},
                  # '工电-4': {'name': '天窗修风险', 'risk_name': 89},
                  # '工电-5': {'name': '施工风险', 'risk_name': 88},
                  '工电-6': {'name': '施工安全风险(工务版)', 'risk_name': 87, 'index_code': 'GW_GD-6'},
                  '工电-7': {'name': '劳动安全风险(工务版)', 'risk_name': 66, 'index_code': 'GW_GD-7'},
                  '工电-8': {'name': '劳动安全风险(电务版)', 'risk_name': 30, 'index_code': 'GW_GD-8'},
                  '工电-9': {'name': '施工安全风险(电务版)', 'risk_name': 91, 'index_code': 'GW_GD-9'},
                  '工电-10': {'name': '劳动安全风险(供电版)', 'risk_name': 35, 'index_code': 'GW_GD-10'},
                  '工电-11': {'name': '施工安全风险(供电版)', 'risk_name': 34, 'index_code': 'GW_GD-11'},
                  '工电-12': {'name': '天窗施工单项', 'risk_name': [89, 88], 'index_code': 'GW_GD-12'},
                  # '工电-13': {'name': '专业管理风险(供电版)', 'risk_name': 5},
                  # '工电-14': {'name': '自轮设备风险(供电版)', 'risk_name': 55},
                  # '工电-15': {'name': '施工配合监管风险(供电版)', 'risk_name': 54},
                  '客运-1': {'name': '劳动安全风险', 'risk_name': 18, 'index_code': 'CW_KY-1'},
                  '客运-2': {'name': '客运组织及消防(车务站)', 'risk_name': 32, 'index_code': 'CW_KY-2'},
                  '客运-3': {'name': '客运组织及消防(客运段)', 'risk_name': 32, 'index_code': 'CW_KY-3'},
                  '客运-4': {'name': '客运组织及消防(客站)', 'risk_name': 32, 'index_code': 'CW_KY-4'}}


def save_major_risk_data():
    records = []
    for k, v in major_risk_map.items():
        risktype_data = get_query_condition_by_risktype(v['risk_name'])
        check_item_ids = risktype_data[0]
        check_risk_ids = risktype_data[1]
        record = {'MAJOR_INDEX_RISK_TYPE': k,
                  'INDEX_NAME': v['name'],
                  'CHECK_ITEM_IDS': list(map(int, check_item_ids.split(','))),
                  'CHECK_RISK_IDS': list(map(int, check_risk_ids.split(',')))}
        records.append(record)

    coll_name = 'major_index_item_and_risk_data'
    mongo.db[coll_name].remove()
    write_bulk_mongo(coll_name, records)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        save_major_risk_data()
