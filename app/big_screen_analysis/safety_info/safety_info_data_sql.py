#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/8
Description: 
"""


SAFETY_INFO_SQL = """SELECT 
    a.PK_ID,
    a.RANK,
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m') AS MONTH,
    c.NAME AS DEPARTMENT_NAME,
    c.TYPE,
    c.TYPE3,
    c.BELONG_JURISDICTION_NAME,
    f.NAME AS RISK_NAME,
    g.NAME AS PROFESSION,
    1 AS COUNT
FROM
    t_safety_produce_info a
        LEFT JOIN
    t_safety_produce_info_responsibility_unit b ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
        LEFT JOIN
    t_department c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
        LEFT JOIN
    t_safety_produce_info_responsibility_unit_and_risk d ON d.FK_RESPONSIBILITY_UNIT_ID = b.PK_ID
        LEFT JOIN
    t_risk e ON e.PK_ID = d.FK_RISK_ID
        LEFT JOIN
    t_risk f ON e.PARENT_ID = f.PK_ID
        LEFT JOIN
    t_department AS g ON g.DEPARTMENT_ID = c.TYPE2
WHERE
    b.STATUS = 2
"""


GET_STATIONS_BY_DICTIONARY_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """


GET_STATIONS_BY_PROFESSION_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
    """