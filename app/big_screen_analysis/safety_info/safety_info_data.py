#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/8
Description: 
"""
from app.data.util import pd_query
from app.big_screen_analysis.safety_info.safety_info_data_sql import (SAFETY_INFO_SQL, GET_STATIONS_BY_DICTIONARY_SQL,
                                                                      GET_STATIONS_BY_PROFESSION_SQL)
from app.big_screen_analysis.health_index.common import get_department_location
from app.big_screen_analysis.health_index import cache_client


def get_gongdian_stations():
    """
    获取工电专业站段
    :return:
    """
    profession_dictionary_id = 2140
    major_ids = pd_query(GET_STATIONS_BY_DICTIONARY_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def get_keyun_stations():
    """
    获取客运专业站段
    :return:
    """
    profession_dictionary_id = 898
    major_ids = pd_query(GET_STATIONS_BY_PROFESSION_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _apply_to_add_data(row):
    major = row['PROFESSION']
    if row['TYPE3'] in keyun_stations:
        major = '客运'
    elif row['TYPE3'] in gongdian_stations:
        major = '工电'
    return major


def get_data(param_dict):
    major = param_dict.get('MAJOR')
    if major:
        return get_data_with_major(param_dict)

    result = {}
    safety_df = pd_query(SAFETY_INFO_SQL)

    global keyun_stations, gongdian_stations
    keyun_stations = get_keyun_stations()
    gongdian_stations = get_gongdian_stations()

    safety_df['MAJOR'] = safety_df.apply(lambda row: _apply_to_add_data(row), axis=1)
    major_list = ['车务', '客运', '机务', '供电', '工电', '工务', '车辆', '电务']
    safety_df = safety_df[safety_df['MAJOR'].isin(major_list)]

    # 专业分布
    major_distribution_data = []
    # 专业级别分布
    major_rank_distribution_data = []

    for major, major_df in safety_df.groupby('MAJOR'):
        major_sum = major_df.index.size
        major_distribution_data.append({'label': major, 'value': major_sum})

        major_df = major_df.groupby('RANK').size().to_frame('COUNT')
        major_df['PROPORTION'] = major_df['COUNT'] / major_sum
        major_df['PROPORTION'] = major_df['PROPORTION'].round(2)

        labels = major_df.index.tolist()
        values = major_df['PROPORTION'].tolist()
        major_rank_distribution_data.append({'label': major, 'value': {'labels': labels, 'values': values}})

    result['major_distribution'] = major_distribution_data
    result['major_rank_distribution'] = major_rank_distribution_data

    # 时间分布
    time_distribution_data = []
    # 时间级别分布
    time_rank_distribution_data = []
    for month, group in safety_df.groupby('MONTH'):
        time_distribution_data.append({'label': month, 'value': group.index.size})

        rank_df = group.groupby('RANK', as_index=False)['COUNT'].sum()
        labels = rank_df['RANK'].tolist()
        values = rank_df['COUNT'].tolist()

        time_rank_distribution_data.append({'label': month, 'value': {'labels': labels, 'values': values}})
    result['time_distribution'] = time_distribution_data
    result['time_rank_distribution'] = time_rank_distribution_data

    # 涉及部门时间分布
    department_time_group = safety_df.groupby(['MONTH', 'DEPARTMENT_NAME']).size().unstack()
    department_time_group = department_time_group.fillna(0)
    department_time_group = department_time_group.astype(int)
    department_time_group_data = {'unit': department_time_group.columns.tolist()}
    year_data = {}
    for index in department_time_group.index.tolist():
        year = index[:4]
        mon = index[-2:]
        year_values = year_data.setdefault(year, [])
        year_values.append({'month': mon, 'value': department_time_group.loc[index].tolist()})

    department_time_group_data['data'] = [{'year': x, 'value': year_data[x]} for x in year_data]
    result['unit_time_distribution'] = department_time_group_data

    return result


def station_data_classify(risk_station_data, major):
    result = risk_station_data
    if major == '车务':
        classify = {'车务段': [], '车站': []}
        for record in risk_station_data:
            if '车务段' in record['label']:
                classify['车务段'].append(record)
            else:
                classify['车站'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '工务':
        classify = {'高铁工务段': [], '工务段': []}
        for record in risk_station_data:
            if '高铁工务段' in record['label']:
                classify['高铁工务段'].append(record)
            else:
                classify['工务段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '车辆':
        classify = {'动车段': [], '车辆段': []}
        for record in risk_station_data:
            if '动车段' in record['label']:
                classify['动车段'].append(record)
            else:
                classify['车辆段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '电务':
        classify = {'电务维修段': [], '电务段': []}
        for record in risk_station_data:
            if '电务维修段' in record['label']:
                classify['电务维修段'].append(record)
            else:
                classify['电务段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '客运':
        classify = {'客运段': [], '车站': []}
        for record in risk_station_data:
            if '客运段' in record['label']:
                classify['客运段'].append(record)
            else:
                classify['车站'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '机务':
        classify = {'机务段': []}
        for record in risk_station_data:
            classify['机务段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '工电':
        classify = {'工电段': []}
        for record in risk_station_data:
            classify['工电段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    elif major == '供电':
        classify = {'供电段': []}
        for record in risk_station_data:
            classify['供电段'].append(record)
        result = [{'label': k, 'value': v} for k, v in classify.items()]
    return result


def get_data_with_major(param_dict):

    result = {}
    major = param_dict.get('MAJOR')
    safety_df = cache_client.get_or_set_global('safety_info_analysis')

    global keyun_stations, gongdian_stations
    keyun_stations = get_keyun_stations()
    gongdian_stations = get_gongdian_stations()

    safety_df = safety_df[(safety_df['MAIN_TYPE'].between(1, 2))]

    safety_df['MAJOR'] = safety_df.apply(lambda _row: _apply_to_add_data(_row), axis=1)
    major_list = [major]
    safety_df = safety_df[safety_df['MAJOR'].isin(major_list)]

    # 风险分布
    risk_group = safety_df.groupby('RISK_NAME')
    risk_group_data = []
    for risk, group in risk_group:
        risk_group_data.append({'label': risk, 'value': group.index.size})
    result['risk_distribution'] = risk_group_data
    # 风险时间分布
    month_risk_group = safety_df.groupby(['MONTH', 'RISK_NAME']).size().unstack()
    month_risk_group = month_risk_group.fillna(0)
    month_risk_group = month_risk_group.astype(int)
    risk_time_group_data = {'months': month_risk_group.index.tolist(),
                            'data': [{'label': col, 'value': month_risk_group[col].values.tolist()} for col in
                                     month_risk_group.columns.values]}
    result['risk_time_distribution'] = risk_time_group_data

    # 风险站段分布
    risk_station_data = []
    station_safety_df = safety_df[safety_df['TYPE'] == 4]
    for (department_id, department_name), group in station_safety_df.groupby(['TYPE3', 'DEPARTMENT_NAME']):
        position = get_department_location(department_id)
        if position:
            risk_station_data.append({'label': department_name, 'value': group.index.size,
                                      'position': get_department_location(department_id)})
    risk_station_data = station_data_classify(risk_station_data, major)
    result['risk_station_distribution'] = risk_station_data

    # 区域风险细分分布
    area_risk_data = []
    for area, group in safety_df.groupby('BELONG_JURISDICTION_NAME'):
        risk_data = []
        risk_group = group.groupby('RISK_NAME', as_index=False)['COUNT'].sum()
        total_count = risk_group['COUNT'].sum()
        risk_group['PERCENTAGE'] = risk_group['COUNT'] / total_count
        for _, row in risk_group.iterrows():
            risk_data.append({'label': row['RISK_NAME'], 'value': row['COUNT'],
                              'percentage': '{0}%'.format(round(row['PERCENTAGE'] * 100, 1))})
        area_risk_data.append({'label': area, 'risk': risk_data})
    result['area_risk_distribution'] = area_risk_data

    # 涉及部门时间分布
    department_time_group = safety_df.groupby(['MONTH', 'DEPARTMENT_NAME']).size().unstack()
    department_time_group = department_time_group.fillna(0)
    department_time_group = department_time_group.astype(int)
    department_time_group_data = {'unit': department_time_group.columns.tolist()}
    year_data = {}
    for index in department_time_group.index.tolist():
        year = index[:4]
        mon = index[-2:]
        year_values = year_data.setdefault(year, [])
        year_values.append({'month': mon, 'value': department_time_group.loc[index].tolist()})

    department_time_group_data['data'] = [{'year': x, 'value': year_data[x]} for x in year_data]
    result['unit_time_distribution'] = department_time_group_data

    return result

