#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/6
Description: 
"""

import pandas as pd
import numpy as np
import pymongo
from flask import current_app

from app import mongo
from app.big_screen_analysis.common.common_sql import department_safety_sql
from app.big_screen_analysis.util import get_range_history_months
from app.data.util import pd_query, write_bulk_mongo
from app.utils.common_func import get_history_months
from app.big_screen_analysis.algorithm.security_warning import start_final, dataTr
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.big_screen_analysis.const import training_data_coll


def get_index_data_from_mongo(major, index_type):
    index_score_list = []
    detail_score_list = []

    condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
    }
    detail_condition = {
        'HIERARCHY': 3,
        'MAJOR': major,
        'DETAIL_TYPE': 0
    }

    if index_type == 0:
        index_coll_prefix = 'health'
    else:
        index_coll_prefix = 'major'
        condition['TYPE'] = index_type
        detail_condition['TYPE'] = index_type

    for prefix in ['history_', 'monthly_']:
        index_score = list(mongo.db[f'{prefix}{index_coll_prefix}_index'].find(
            condition, {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING), ('MON', pymongo.ASCENDING)]))

        index_detail_score = list(mongo.db[f'{prefix}detail_{index_coll_prefix}_index'].find(
            detail_condition,
            {
                '_id': 0,
                'DEPARTMENT_ID': 1,
                'DEPARTMENT_NAME': 1,
                'RANK': 1,
                'SCORE': 1,
                'MON': 1,
                'MAIN_TYPE': 1,
                'MAJOR': 1
            }).sort([('DEPARTMENT_ID', pymongo.ASCENDING),
                     ('MON', pymongo.ASCENDING),
                     ('MAIN_TYPE', pymongo.ASCENDING)]))

        index_score_list.append(pd.DataFrame(index_score))
        detail_score_list.append(pd.DataFrame(index_detail_score))

    index_score = pd.concat(index_score_list, ignore_index=True)
    index_detail_score = pd.concat(detail_score_list, ignore_index=True)

    return index_score, index_detail_score


def _get_init_train_input(calc_df, columns):
    """
    获取训练集输入数据
    :param calc_df:
    :param columns:
    :return:
    """
    score_df = pd.DataFrame(columns=columns)
    error_df = pd.DataFrame(columns=columns)

    for department, department_df in calc_df.groupby('DEPARTMENT_ID'):
        temp = department_df.drop_duplicates(['MON']).set_index(['MON'])[['SCORE']].stack().unstack(0)
        score_df = score_df.append(temp, ignore_index=True)

        safe_info = pd_query(department_safety_sql.format(department))
        safe_info_list = []
        for _mon in columns:
            safe_info_list.append(safe_info[safe_info['MON'] == _mon].index.size)
        temp = pd.DataFrame([safe_info_list], columns=columns)
        error_df = error_df.append(temp, ignore_index=True)

    score_df.fillna(0, inplace=True)

    return score_df.values, error_df.values


def get_training_data_record(major, index_type, main_type):
    record = mongo.db[training_data_coll].find_one({'MAJOR': major, 'INDEX_TYPE': index_type, 'MAIN_TYPE': main_type})
    return record


def index_data_training(major, index_type, index_name):
    records = []
    months_ago = -1
    index_score, index_detail_score = get_index_data_from_mongo(major, index_type)
    range_start_mon = get_history_months(months_ago - 30)[0]

    month = get_history_months(months_ago)[0]

    index_score = index_score[index_score['MON'].between(range_start_mon, month)]
    index_detail_score = index_detail_score[index_detail_score['MON'].between(range_start_mon, month)]

    columns = sorted(list(index_score.groupby(['MON']).groups))

    if len(columns) <= 2:
        # 小于三个月不能训练
        print(major, index_type)
        print('month range too short')
        return

    # 总分数训练集
    training_record = get_training_data_record(major, index_type, 0)
    if not training_record:
        score_array, error_array = _get_init_train_input(index_score, columns)
        result = start_final(score_array, error_array)
        record = {'MAJOR': major, 'INDEX_TYPE': index_type, 'MAIN_TYPE': 0,
                  'INDEX_NAME': index_name, 'MONTHS': columns,
                  'T1': result[0].tolist(),
                  'T11': result[1].tolist(),
                  'T2': result[2].tolist(),
                  'T22': result[3].tolist(),
                  'T3': result[4].tolist(),
                  'T33': result[5].tolist()}
        records.append(record)
    else:
        if month not in training_record['MONTHS'] and month in columns:
            columns_new = columns[-3:]  # 取最新三个月
            index_score = index_score[index_score['MON'].isin(columns_new)]
            score_array, error_array = _get_init_train_input(index_score, columns_new)
            result_new = dataTr(score_array, error_array, month, training_record['MONTHS'],
                                np.array(training_record['T1']),
                                np.array(training_record['T11']),
                                np.array(training_record['T2']),
                                np.array(training_record['T22']),
                                np.array(training_record['T3']),
                                np.array(training_record['T33']))
            mongo.db[training_data_coll].update({'_id': training_record['_id']},
                                                {"$set": {'MONTHS': columns,
                                                          'T1': result_new[0].tolist(),
                                                          'T11': result_new[1].tolist(),
                                                          'T2': result_new[2].tolist(),
                                                          'T22': result_new[3].tolist(),
                                                          'T3': result_new[4].tolist(),
                                                          'T33': result_new[5].tolist()}})
            current_app.logger.debug('update training data with {}'.format(index_name))
        else:
            current_app.logger.debug('nothing todo with {}'.format(index_name))

    # 分指数训练集
    for main_type, df in index_detail_score.groupby(['MAIN_TYPE']):
        calc_df = df
        training_record = get_training_data_record(major, index_type, main_type)
        if not training_record:
            score_array, error_array = _get_init_train_input(calc_df, columns)
            result = start_final(score_array, error_array)
            record = {'MAJOR': major, 'INDEX_TYPE': index_type, 'MAIN_TYPE': main_type,
                      'INDEX_NAME': index_name, 'MONTHS': columns,
                      'T1': result[0].tolist(),
                      'T11': result[1].tolist(),
                      'T2': result[2].tolist(),
                      'T22': result[3].tolist(),
                      'T3': result[4].tolist(),
                      'T33': result[5].tolist()}
            records.append(record)
        else:
            if month not in training_record['MONTHS'] and month in columns:
                columns_new = columns[-3:]  # 取最新三个月
                calc_df = calc_df[calc_df['MON'].isin(columns_new)]
                score_array, error_array = _get_init_train_input(calc_df, columns_new)
                result_new = dataTr(score_array, error_array, month, training_record['MONTHS'],
                                    np.array(training_record['T1']),
                                    np.array(training_record['T11']),
                                    np.array(training_record['T2']),
                                    np.array(training_record['T22']),
                                    np.array(training_record['T3']),
                                    np.array(training_record['T33']))
                mongo.db[training_data_coll].update({'_id': training_record['_id']},
                                                    {"$set": {'MONTHS': columns,
                                                              'T1': result_new[0].tolist(),
                                                              'T11': result_new[1].tolist(),
                                                              'T2': result_new[2].tolist(),
                                                              'T22': result_new[3].tolist(),
                                                              'T3': result_new[4].tolist(),
                                                              'T33': result_new[5].tolist()}})

    if records:
        write_bulk_mongo(training_data_coll, records)
        current_app.logger.debug('init training data with {}'.format(index_name))


def execute_index_training():
    """
    进行指数分数数据训练
    :return:
    """
    # 综合指数
    for major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        index_data_training(major, 0, '{}安全综合指数'.format(major))

    # 重点指数
    for k, v in major_risk_map.items():
        major, index_type = k.split('-')
        index_type = int(index_type)
        index_data_training(major, index_type, v['name'])
    print('ok')


if __name__ == '__main__':
    from manage import app

    with app.app_context():
        execute_index_training()


