#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/9
Description: 
"""

from flask import jsonify, request

from app.big_screen_analysis import big_screen_analysis_bp
from app.big_screen_analysis.risk_control_forecast_model import risk_forecast
from app.big_screen_analysis.safety_info import safety_info_data
from app.big_screen_analysis.health_index import health_index_data
from app.big_screen_analysis.index_warning import index_warning
from app.big_screen_analysis.cardinal_number_recommend import cardinal_number_recommend
from app.big_screen_analysis.intelligent_weight import intelligent_weight
from app.big_screen_analysis.common.common import get_station_data
from app.utils.common_func import wrapper_rtn_msg


@big_screen_analysis_bp.route('/health_index')
def api_new_health_index_big():
    """
    综合指数大屏数据展示
    按专业区别
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = health_index_data.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/safety_info')
def api_safety_info_big():
    """
    安全生产信息分布数据
    :return:
    """
    param_dict = request.args
    necessary_param = []
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = safety_info_data.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/risk_forecast')
def api_risk_forecast():
    """
    综合指数与安全生产信息相关性数据
    按专业与事安全生产信息类型区分
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'SAFETY_INFO_TYPE', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = risk_forecast.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/major_index_warning')
def api_major_index_warning():
    """
    智能预警标准接口
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'RISK_TYPE', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = index_warning.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/major_index_cardinal_recommend')
def api_major_index_cardinal_recommend():
    """
    重点指数基数推荐
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'RISK_TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = cardinal_number_recommend.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/major_index_weight')
def api_major_index_weight():
    """
    重点指数智能权重
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'RISK_TYPE', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = intelligent_weight.get_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/health_index_warning')
def api_health_index_warning():
    """
    综合指数智能预警标准接口
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = index_warning.get_data(param_dict, 0)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/health_index_cardinal_recommend')
def api_health_index_cardinal_recommend():
    """
    综合指数基数推荐
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = cardinal_number_recommend.get_data(param_dict, 0)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/health_index_weight')
def api_health_index_weight():
    """
    重点指数智能权重
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = intelligent_weight.get_data(param_dict, 0)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@big_screen_analysis_bp.route('/get_station_data')
def api_get_station_data():
    param_dict = request.args
    necessary_param = []
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = get_station_data(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)
