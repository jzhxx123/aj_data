import numpy as np
import pandas as pd
import random
import seaborn as sns
import matplotlib.pyplot as plt
import scipy
from scipy.stats import pearsonr
from statsmodels.tsa.arima_model import ARIMA


# 模块摘要
# 异常检测：1 专业内对比 2 三个月的趋势对比 3 与去年的同比
# 相关性判断： 分数异常 发生故障正相关 分数正常 未发生故障 负相关
# 相关趋势判断： 计算相关系数，通过比例分配
# 基数选择：选择无异常 相关性正常 最好的站段 若没有则选择相关趋势最好的站段
# 权重修改：选择子指数相关趋势分配 正负相关检验 经验修改
# 安全预警：统计分数变化规律 与分数分布规律 考虑该站段历史的相关性控制比例 预警条件

# 异常检测

# 专业内对比 输入data专业内所有站段分数数据 cot检测的站段分数数据 返回0（1）表示分数正常（异常）
def mid(data, cot):
    dmax = max(data)
    dmin = min(data)
    while dmax - dmin > 1:
        dhalf = (dmax + dmin) / 2
        cmax = 0
        cmin = 0
        for i in range(len(data)):
            cmax = cmax + abs(data[i] - dmax)
            cmin = cmin + abs(data[i] - dmin)
        if cmax >= cmin:
            dmax = dhalf
        else:
            dmin = dhalf
    dhalf = (dmax + dmin) / 2  # 站段数据中心
    if cot < dhalf - 20:
        return 1
    else:
        return 0


# 三月内趋势对比 data检测站段近三个月的数据没有分数使用0代替 返回0（1）表示趋势正常（异常）
def trend(data):
    diff1 = data[1] - data[2]
    diff2 = data[0] - data[2]
    if data[0] == 0 and data[1] == 0:
        return 0;
    elif data[0] == 0:
        if diff1 > 15 and diff1 / data[1] > 0.3:
            return 1;
        else:
            return 0;
    elif data[1] == 0:
        if diff2 > 20 and diff2 / data[0] > 0.3:
            return 1;
        else:
            return 0;
    else:
        if diff1 > 15 and diff1 / data[1] > 0.3:
            return 1;
        elif diff2 > 20 and diff2 / data[0] > 0.3:
            return 1;
        else:
            return 0;


# 与去年同期对比 data站段得分 lastdata 去年站段得分没有按0算 lasterror去年的故障事故数 返回0（1）表示同比正常（异常）
def lastcom(data, lastdata, lasterror):
    if lastdata != 0 and lasterror > 0:
        if data < lastdata + lasterror * 0.3:
            return;
        else:
            return 0;
    else:
        return 0;


# 汇总：data 站段三个月的得分 line 专业内所有站段得分 last去年得分 error 去年的事故故障数
# 返回answer取值0（1）表示正常（异常）顺序：总体 横向 趋势 同比
def ErrorObtain(data, line, last, error):
    ans1 = mid(line, data[2])
    ans2 = trend(data)
    ans3 = lastcom(data[2], last, error)

    answer = np.zeros(4)

    answer[0] = min(1, ans1 + ans2 + ans3)
    answer[1] = min(1, ans1)
    answer[2] = min(1, ans2)
    answer[3] = min(1, ans3)
    return answer;


# 相关性判断 answer异常判断结果 error 事故故障数 1 0 -1 表示正相关 无关 负相关
def Compare(answer, error):
    if error > 0:
        if answer[0] > 0:
            return 1
        else:
            return -1
    else:
        return 0


# 相关趋势判断 返回第一项为相关系数 第二项为P值
def CompareTrend(data, error):
    if sum(data) != 0 and sum(error) != 0:
        x = scipy.array(data)
        y = scipy.array(error)
        return pearsonr(x, y)
    else:
        return 0, 1;


# 基数选择 data专业内所有站段三个月的数据 colm所有站段三个月的故障事故数 last去年所有站段分数 error去年所有站段故障
def Cardinal_number(data, colm, last, error):
    lenth = len(data[:, 0])  # 站段个数
    answer = np.zeros(lenth)
    high = len(data[0, :])
    copy = 0

    for i in range(lenth):
        ans1 = ErrorObtain(data[i, high - 3:], data[:, high - 1], last[i], error[i])
        if ans1[0] == 0 and colm[i] == 0:
            answer[i] = data[i, high - 1]
            copy = 1
        else:
            answer[i] = 0

    if copy == 0:
        maxnum = max(data[:, high])
        for i in range(lenth):
            if data[i, high - 1] == maxnum:
                answer[i] = data[i, high - 1]
                break;

    return answer;


# 权重自动调配

def Weights(data, exam, clom):
    lenth = len(data[:, 0])  # 指数个数
    answer = np.zeros(lenth)
    percent = np.zeros([lenth, 2])

    for i in range(lenth):
        a, b = CompareTrend(data[i, :], exam)
        percent[i, 0] = a
        percent[i, 1] = b

    mindata = min(abs(percent[:, 0])) / 3

    for i in range(lenth):
        if percent[i, 0] < 0:
            percent[i, 0] = mindata

    ker = sum(percent[:, 0])

    for i in range(lenth):
        answer[i] = (clom[i] + percent[i, 0] / ker) / 2

    return percent, answer;


# 安全预警

# 新数据训练
# data专业内所有站段三个月的数据 error三个月的事故故障数 date 月份 table1 单月差异表 table2 两月差异表
# kertable1 单月记录表 kertable2 两月记录表 datetable月份记录表 dtable 故障数记录 dkertable故障记录
def dataTr(data, error, date, datetable, table1, table2, kertable1, kertable2, dtable, dkertable):
    lenth = len(datetable)  # 数据月数
    note = 0
    high = len(data[:, 0])  # 站段
    for i in range(lenth):
        if date == datetable[i]:
            note = 1
            break;

    if note == 0:
        newdatetable = np.zeros(lenth + 1)
        for i in range(lenth):
            newdatetable[i] = datetable[i]

        newdatetable[lenth] = date

        for i in range(high):
            if data[i, 2] != 0:
                if data[i, 1] != 0:
                    diff = data[i, 1] - data[i, 2]
                    diffe = error[i, 2] - error[i, 1]
                    if diff * diffe > 0:
                        if diff > 0:
                            cot = min(int(diff / 5) + 20, 40)
                        else:
                            cot = max(int(diff / 5) + 19, 0)
                        usenum = min(int(data[i, 2] / 5), 19)
                        table1[usenum, cot] = table1[usenum, cot] + abs(diffe)
                        kertable1[usenum, cot] = kertable1[usenum, cot] + 1

                if data[i, 0] != 0:
                    diff = data[i, 0] - data[i, 2]
                    diffe = error[i, 2] - error[i, 0]
                    if diff * diffe > 0:
                        if diff > 0:
                            cot = min(int(diff / 5) + 20, 39)
                        else:
                            cot = max(int(diff / 5) + 19, 0)
                        usenum = min(int(data[i, 2] / 5), 19)
                        table2[usenum, cot] = table2[usenum, cot] + abs(diffe)
                        kertable2[usenum, cot] = kertable2[usenum, cot] + 1

                if error[i, 2] > 0:
                    if data[i, 0] != 0 and data[i, 1] != 0:
                        mindata = min(data[i, :])
                    elif data[i, 0] != 0:
                        mindata = min(data[i, 1], data[i, 2])
                    elif data[i, 1] != 0:
                        mindata = min(data[i, 0], data[i, 2])
                    else:
                        mindata = data[i, 2]

                    dtable[error[i, 2] - 1] = dtable[error[i, 2] - 1] + mindata
                    dkertable[error[i, 2] - 1] = dkertable[error[i, 2] - 1] + 1

        return table1, table2, kertable1, kertable2, dtable, dkertable, newdatetable;

    else:
        return table1, table2, kertable1, kertable2, dtable, dkertable, datetable;


# 预警条件 table 子指数的故障数记录表 kertable子指数的故障记录
def Emphasis_Indexmin(table, kertable):
    lenth = len(table)
    answer = np.zeros(lenth)
    for i in range(lenth):
        if kertable[i] != 0:
            answer[i] = table[i] / kertable[i]
        else:
            answer[i] = 0
    ans = max(answer)

    return ans;


def Emphasis_Index(data, array, last, lerror, table, kertable, t3, t33):
    # 指数个数
    lenth = int(len(data.score) / 3)

    answer = np.zeros([lenth, 10])
    cnum = 0
    for i in range(lenth):
        answer[i, 0] = data.at[3 * i, 'type']

        datathree = np.zeros(3)
        for j in range(3):
            datathree[j] = data.at[3 * i + j, 'score']

        aans = ErrorObtain(datathree, array[i, :], last[i], lerror[i])
        answer[i, 1] = aans[0]
        answer[i, 2] = aans[1]
        answer[i, 3] = aans[2]
        answer[i, 4] = aans[3]

        answer[i, 5] = Compare(aans, data.at[3 * i + 2, 'error'])

        answer[i, 6] = (data.at[3 * i + 2, 'score'] - data.at[3 * i, 'score']) / data.at[3 * i, 'score']

        answer[i, 7] = data.at[3 * i + 2, 'score'] - data.at[3 * i, 'score']

        answer[i, 8] = Emphasis_Indexmin(table[i, :], kertable[i, :])

        answer[i, 9] = Emphasis_Indexmin(t3, t33)

    return answer;


# 安全预警 data 预警站段12个月的数据没有用0代替 error 两个月的故障数
def Forecast_predict(data, error, table1, table2, kertable1, kertable2):
    if data[len(data) - 1] != 0:
        for i in range(len(data) - 1):
            if data[len(data) - 2 - i] == 0:
                data[len(data) - 2 - i] = data[len(data) - 1 - i]

        data = pd.Series(data)
        model = ARIMA(data, order=(1, 0, 0))
        result_ARIMA = model.fit(disp=-1)

        Data = np.array(result_ARIMA.fittedvalues)

        x = 0
        y = 0
        xy = 0
        x2 = 0

        for i in range(len(Data)):
            x = x + i
            y = y + Data[i]
            xy = i * Data[i] + xy
            x2 = x2 + i * i

        k = (x * y / len(Data) - xy) / (x * x / len(Data) - x2)

        b = (y - k * x) / len(Data)

        score = len(Data) * k + b
        if data[len(data) - 2] != 0:
            diff = data[len(data) - 1] - score
            diffd = data[len(data) - 2] - score

            if diff > 0:
                ax = min(int(diff / 5) + 20, 39)
                answer1 = error[1] + table1[int(data[len(data) - 1] / 5), ax] / kertable1[
                    int(data[len(data) - 1] / 5), ax]
            else:
                ax = max(int(diff / 5) + 19, 0)
                answer1 = error[1] - table1[int(data[len(data) - 1] / 5), ax] / kertable1[
                    int(data[len(data) - 1] / 5), ax]
                answer1 = max(0, answer1)

            if diffd > 0:
                ay = min(int(diffd / 5) + 20, 39)
                answer2 = error[0] + table2[int(data[len(data) - 2] / 5), ay] / kertable2[
                    int(data[len(data) - 2] / 5), ay]
            else:
                ay = max(int(diffd / 5) + 19, 0)
                answer2 = error[0] - table2[int(data[len(data) - 2] / 5), ay] / kertable2[
                    int(data[len(data) - 2] / 5), ay]
                answer2 = max(0, answer2)
            answer = (answer1 + answer2) / 2
        else:
            diff = data[len(data) - 1] - score

            if diff > 0:
                ax = min(int(diff / 5) + 20, 39)
                answer = error[1] + table1[int(data[len(data) - 1] / 5), ax] / kertable1[
                    int(data[len(data) - 1] / 5), ax]
            else:
                ax = max(int(diff / 5) + 19, 0)
                answer = error[1] - table1[int(data[len(data) - 1] / 5), ax] / kertable1[
                    int(data[len(data) - 1] / 5), ax]
                answer = max(0, answer)
    else:
        answer = 0

    return answer;


# 初始训练
def start_final(data, error):
    t1 = np.zeros([20, 40])
    t11 = np.zeros([20, 40])
    t2 = np.zeros([20, 40])
    t22 = np.zeros([20, 40])
    t3 = np.zeros(30)
    t33 = np.zeros(30)

    lenth = len(data[0, :])  # 月数
    high = len(data[:, 0])  # 站段数

    for i in range(high):
        for j in range(lenth - 1):
            if data[i, j] != 0 and data[i, j + 1] != 0:
                diff = data[i, j] - data[i, j + 1]
                eiff = error[i, j + 1] - error[i, j]

                if diff * eiff > 0:
                    if diff > 0:
                        cot = min(int(diff / 5) + 20, 40)
                    else:
                        cot = max(int(diff / 5) + 19, 0)
                    usenum = min(int(data[i, 2] / 5),19)
                    t1[usenum, cot] = t1[usenum, cot] + abs(eiff)
                    t11[usenum, cot] = t11[usenum, cot] + 1

    for i in range(high):
        for j in range(lenth - 2):
            if data[i, j] != 0 and data[i, j + 2] != 0:
                diff = data[i, j] - data[i, j + 2]
                eiff = error[i, j + 2] - error[i, j]

                if diff * eiff > 0:
                    if diff > 0:
                        cot = min(int(diff / 5) + 20, 40)
                    else:
                        cot = max(int(diff / 5) + 19, 0)
                    usenum = min(int(data[i, 2] / 5), 19)
                    t2[usenum, cot] = t2[usenum, cot] + abs(eiff)
                    t22[usenum, cot] = t22[usenum, cot] + 1

    for i in range(high):
        for j in range(lenth):
            if error[i, j] != 0:
                if j > 1:
                    if data[i, j] != 0 and data[i, j - 1] != 0 and data[i, j - 2] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + min(data[i, j], data[i, j - 1], data[i, j - 2])
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                    if data[i, j] != 0 and data[i, j - 1] != 0 and data[i, j - 2] == 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + min(data[i, j], data[i, j - 1])
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                    if data[i, j] != 0 and data[i, j - 1] == 0 and data[i, j - 2] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + min(data[i, j], data[i, j - 2])
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                    if data[i, j] == 0 and data[i, j - 1] != 0 and data[i, j - 2] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + min(data[i, j - 1], data[i, j - 2])
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                elif j == 1:
                    if data[i, j] != 0 and data[i, j - 1] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + min(data[i, j], data[i, j - 1])
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                    if data[i, j] != 0 and data[i, j - 1] == 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + data[i, j]
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                    if data[i, j] == 0 and data[i, j - 1] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + data[i, j - 1]
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1
                else:
                    if data[i, j] != 0:
                        t3[error[i, j] - 1] = t3[error[i, j] - 1] + data[i, j]
                        t33[error[i, j] - 1] = t33[error[i, j] - 1] + 1

    return t1, t11, t2, t22, t3, t33;
