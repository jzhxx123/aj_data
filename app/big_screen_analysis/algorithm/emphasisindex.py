import numpy as np
import pandas as pd
import random
import scipy
from scipy.stats import pearsonr
from statsmodels.tsa.arima_model import ARIMA


# 简易数据中心
def mid(data):
    dmax = max(data)
    dmin = min(data)
    while dmax - dmin > 1:
        dhalf = (dmax + dmin) / 2
        cmax = 0
        cmin = 0
        for i in range(len(data)):
            cmax = cmax + abs(data[i] - dmax)
            cmin = cmin + abs(data[i] - dmin)
        if cmax >= cmin:
            dmax = dhalf
        else:
            dmin = dhalf
    dhalf = (dmax + dmin) / 2
    return dhalf


# 指数横向对比
def compare(cot, score, array):
    data = array[cot, :]
    dmid = mid(data)
    end = 0
    if dmid - score > 20:
        end = 1
    return end;


# 指数趋势分析
def trend(data):
    end = 0
    count = (data[0] - data[2]) + (data[1] - data[2]) * 2
    if count > 90:
        end = 1
    return end;


def Emphasis_Index(data, array):
    # 指数个数
    lenth = int(len(data.score) / 3)

    answer = np.zeros([lenth, 12])
    cnum = 0
    for i in range(lenth):
        answer[i, 0] = data.at[3 * i, 'type']

        datathree = np.zeros(3)
        for j in range(3):
            datathree[j] = data.at[3 * i + j, 'score']
        answer[i, 2] = trend(datathree)

        answer[i, 3] = compare(i, data.at[3 * i + 2, 'score'], array)

        if answer[i, 2] + answer[i, 3] > 0:
            answer[i, 1] = 1

        if answer[i, 1] > 0:
            if data.at[3 * i + 2, 'error'] > 0:
                answer[i, 4] = 1
        elif data.at[3 * i + 2, 'error'] > 0:
            answer[i, 4] = -1

        # per1 = (data.at[3*i + 1, 'score'] - data.at[3*i , 'score'])/data.at[3*i, 'score']
        # per2 = (data.at[3*i + 2, 'score'] - data.at[3*i + 1, 'score'])/data.at[3*i + 2, 'score']
        #
        # answer[i,5]  = (per2 - per1) * 100

        answer[i, 5] = (data.at[3 * i + 2, 'score'] - data.at[3 * i, 'score']) / data.at[3 * i, 'score']

        answer[i, 6] = data.at[3 * i + 2, 'score'] - data.at[3 * i, 'score']

        cudata = array[i, :]
        answer[i, 7] = max(mid(cudata) - 20, 20)

        answer[i, 8] = 1 + answer[i, 2] + answer[i, 3]

        cnum = cnum + answer[i, 8]

        answer[i, 10] = (20 + answer[i, 6]) / 2

        answer[i, 11] = (answer[i, 4] + 1) * 2 / 3

    for i in range(lenth):
        answer[i, 8] = answer[i, 8] / cnum

        answer[lenth - 1, 9] = answer[i, 7] * answer[i, 8] + answer[lenth - 1, 9]

    for i in range(lenth):
        answer[i, 9] = answer[lenth - 1, 9]

    return answer;


def Cardinal_number(data, colm):
    lenth = len(data[0, :])
    high = len(data[:, 0])

    answer = np.zeros([high, 5])

    testone = np.zeros(lenth)

    for i in range(high):
        testone[i] = data[i, 0]

    for i in range(high):

        if mid(testone) - data[i, 0] > 20:
            answer[i, 1] = 1

        testtwo = np.zeros(3)

        for j in range(3):
            testtwo[j] = data[i, j]

        answer[i, 2] = trend(testtwo)

        if lenth > 12:
            if data[i, 12] - data[i, 0] > 30:
                answer[i, 3] = 1

        if colm[i] > 0:
            answer[i, 4] = 1

        if answer[i, 1] + answer[i, 2] + answer[i, 3] + answer[i, 4] > 0:
            answer[i, 0] = 1

    return answer;


def Weights(data, exam, clom):
    lenth = len(data[:, 0])

    high = len(data[0, :])

    gread = np.zeros([lenth, 2])

    dexam = np.zeros(high - 1)

    level = 0

    for i in range(high - 1):
        dexam[i] = exam[i] - exam[i + 1]
        if dexam[i] != 0:
            level = 1

    # print(dexam)
    if level == 1:
        for i in range(lenth):
            duse = np.zeros(high - 1)
            control = 0
            for j in range(high - 1):
                duse[j] = data[i, j + 1] - data[i, j]
                if duse[j] != 0:
                    control = 1

            if control == 1:
                x = scipy.array(duse)

                y = scipy.array(dexam)

                gread[i, 0], gread[i, 1] = pearsonr(x, y)

        lenthgd = np.zeros(lenth)

        matw = 0

        for i in range(lenth):
            lenthgd[i] = max(gread[i, 0], min(abs(gread[:, 0])) / 3)

            matw = matw + lenthgd[i]

        newclom = np.zeros(lenth)
        if matw != 0:
            for i in range(lenth):
                lenthgd[i] = lenthgd[i] / matw

            for i in range(lenth):
                newclom[i] = (clom[i] + lenthgd[i]) / 2

        else:
            for i in range(lenth):
                newclom[i] = clom[i]

    else:
        newclom = np.zeros(lenth)
        for i in range(lenth):
            gread[i, 0] = 0
            gread[i, 1] = 0
            newclom[i] = clom[i]

    return gread, newclom;


def Forecast_predict(data):
    # 单数据源分析预测
    data = pd.Series(data)
    model = ARIMA(data, order=(1, 0, 0))
    result_ARIMA = model.fit(disp=-1)

    Data = np.array(result_ARIMA.fittedvalues)

    x = 0
    y = 0
    xy = 0
    x2 = 0

    for i in range(len(Data)):
        x = x + i
        y = y + Data[i]
        xy = i * Data[i] + xy
        x2 = x2 + i * i

    k = (x * y / len(Data) - xy) / (x * x / len(Data) - x2)

    b = (y - k * x) / len(Data)

    answer = len(Data) * k + b

    return answer;


def Forecast_predict1(data):
    x = 0
    y = 0
    xy = 0
    x2 = 0

    for i in range(len(data)):
        x = x + i
        y = y + data[i]
        xy = i * data[i] + xy
        x2 = x2 + i * i

    k = (x * y / len(data) - xy) / (x * x / len(data) - x2)

    b = (y - k * x) / len(data)

    answer = len(data) * k + b

    return answer;


def Forecast_any(data):
    answer1 = Forecast_predict(data[0, :])

    answer2 = Forecast_predict1(data[1, :])

    if answer2 != int(answer2):
        answer2 = int(answer2) + 1

    answer = np.zeros([2, len(data[0, :]) - 1])

    for i in range(len(data[0, :]) - 1):
        answer[0, i] = data[0, i] - data[0, i + 1]

        answer[1, i] = data[1, i + 1] - data[1, i]

    right = 0
    for i in range(len(data[1, :])):
        if data[1, i] != 0:
            right = 1
            break;

    if right == 1:
        x = scipy.array(answer[0, :])

        y = scipy.array(answer[1, :])

        r, p = pearsonr(x, y)

    else:
        r = 0

    return answer1, answer2, answer, r;
