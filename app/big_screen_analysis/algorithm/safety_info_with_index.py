#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:
综合指数与事故相关性计算， 由苏瑞提供
"""

import numpy as np
import pandas as pd
import random


def makeup(data, lenth):
    list = ['MON', 't1', 't2', 't3', 't4', 't5', 't6', 'did', 't0', 'rank', 'error']
    Da = pd.DataFrame(columns=list)
    for i in range(lenth):
        Da.at[i + 1, 'MON'] = data.at[i + 1, 'MON']
        Da.at[i + 1, 't1'] = data.at[i + 1, 't1']
        Da.at[i + 1, 't2'] = data.at[i + 1, 't2']
        Da.at[i + 1, 't3'] = data.at[i + 1, 't3']
        Da.at[i + 1, 't4'] = data.at[i + 1, 't4']
        Da.at[i + 1, 't5'] = data.at[i + 1, 't5']
        Da.at[i + 1, 't6'] = data.at[i + 1, 't6']
        Da.at[i + 1, 'did'] = data.at[i + 1, 'did']
        Da.at[i + 1, 't0'] = data.at[i + 1, 't0']
        Da.at[i + 1, 'rank'] = data.at[i + 1, 'rank']
        Da.at[i + 1, 'error'] = data.at[i + 1, 'error']
    return Da


def safety_supervision(data, date, month):
    count = 0
    lenth = len(data.did)
    location = np.zeros(100)
    location[1] = -1

    while count < lenth - 1:
        if data.at[count, 'did'] != data.at[count + 1, 'did']:
            location[0] = location[0] + 1
            location[int(location[0]) + 1] = count
        count = count + 1

    if location[int(location[0]) + 1] < lenth - 1:
        location[int(location[0]) + 2] = lenth - 1
        location[0] = location[0] + 1
    # print(location)
    data1 = np.zeros([int(location[0]), 3])
    lenth1 = 0
    for i in range(int(location[0])):
        if location[i + 2] - location[i + 1] > month-1 and data.at[int(location[i + 2]), 'MON'] == date:
            data1[lenth1, 0] = location[i + 2]
            data1[lenth1, 1] = data.at[int(location[i + 2]), 'rank']
            lenth1 = lenth1 + 1

    for i in range(lenth1):
        for j in range(lenth1):
            if data1[i, 1] > data1[j, 1]:
                data1[i, 2] = data1[i, 2] + 1
    # print(data1)
    list = ['MON', 't1', 't2', 't3', 't4', 't5', 't6', 'did', 't0', 'rank', 'error']
    answer = pd.DataFrame(columns=list)
    Difference = pd.DataFrame(columns=list)

    for i in range(lenth1):
        if lenth1 - data1[i, 2] < 4:
            # print(i)
            cot = int(lenth1 - data1[i, 2] + 3)
            # print(cot)
            # print(data1[i, 0])
            # print(data.at[int(data1[i, 0]) , 'rank'])
            for j in range(month):
                # print(cot * 12 - j)
                # print(data.at[int(data1[i, 0]) - j, 'rank'])
                # print(int(data1[i, 0]))
                answer.at[cot * month - j, 'MON'] = data.at[int(data1[i, 0]) - j, 'MON']
                answer.at[cot * month - j, 't1'] = data.at[int(data1[i, 0]) - j, 't1']
                answer.at[cot * month - j, 't2'] = data.at[int(data1[i, 0]) - j, 't2']
                answer.at[cot * month - j, 't3'] = data.at[int(data1[i, 0]) - j, 't3']
                answer.at[cot * month - j, 't4'] = data.at[int(data1[i, 0]) - j, 't4']
                answer.at[cot * month - j, 't5'] = data.at[int(data1[i, 0]) - j, 't5']
                answer.at[cot * month - j, 't6'] = data.at[int(data1[i, 0]) - j, 't6']
                answer.at[cot * month - j, 'did'] = data.at[int(data1[i, 0]) - j, 'did']
                answer.at[cot * month - j, 't0'] = data.at[int(data1[i, 0]) - j, 't0']
                answer.at[cot * month - j, 'rank'] = data.at[int(data1[i, 0]) - j, 'rank']
                answer.at[cot * month - j, 'error'] = data.at[int(data1[i, 0]) - j, 'error']
                if j > 0:
                    nonth = month - 1
                    Difference.at[cot * nonth - j + 1, 'MON'] = answer.at[cot * month - j + 1, 'MON']
                    Difference.at[cot * nonth - j + 1, 't1'] = answer.at[cot * month - j + 1, 't1'] - answer.at[
                        cot * month - j, 't1']
                    Difference.at[cot * nonth - j + 1, 't2'] = answer.at[cot * month - j + 1, 't2'] - answer.at[
                        cot * month - j, 't2']
                    Difference.at[cot * nonth - j + 1, 't3'] = answer.at[cot * month - j + 1, 't3'] - answer.at[
                        cot * month - j, 't3']
                    Difference.at[cot * nonth - j + 1, 't4'] = answer.at[cot * month - j + 1, 't4'] - answer.at[
                        cot * month - j, 't4']
                    Difference.at[cot * nonth - j + 1, 't5'] = answer.at[cot * month - j + 1, 't5'] - answer.at[
                        cot * month - j, 't5']
                    Difference.at[cot * nonth - j + 1, 't6'] = answer.at[cot * month - j + 1, 't6'] - answer.at[
                        cot * month - j, 't6']
                    Difference.at[cot * nonth - j + 1, 'did'] = answer.at[cot * month - j + 1, 'did']
                    Difference.at[cot * nonth - j + 1, 't0'] = answer.at[cot * month - j + 1, 't0'] - answer.at[
                        cot * month - j, 't0']
                    Difference.at[cot * nonth - j + 1, 'rank'] = answer.at[cot * month - j + 1, 'rank']
                    Difference.at[cot * nonth - j + 1, 'error'] = answer.at[cot * month - j + 1, 'error'] - answer.at[
                        cot * month - j, 'error']
    for i in range(lenth1):
        if int(data1[i, 2]) < 3:
            # print(data1[i, 2])
            # print(data.at[int(data1[i, 0]), 'rank'])
            # print('ha')
            for j in range(month):
                # j=11-j
                answer.at[int(data1[i, 2] + 1) * month - j, 'MON'] = data.at[int(data1[i, 0]) - j, 'MON']
                answer.at[int(data1[i, 2] + 1) * month - j, 't1'] = data.at[int(data1[i, 0]) - j, 't1']
                answer.at[int(data1[i, 2] + 1) * month - j, 't2'] = data.at[int(data1[i, 0]) - j, 't2']
                answer.at[int(data1[i, 2] + 1) * month - j, 't3'] = data.at[int(data1[i, 0]) - j, 't3']
                answer.at[int(data1[i, 2] + 1) * month - j, 't4'] = data.at[int(data1[i, 0]) - j, 't4']
                answer.at[int(data1[i, 2] + 1) * month - j, 't5'] = data.at[int(data1[i, 0]) - j, 't5']
                answer.at[int(data1[i, 2] + 1) * month - j, 't6'] = data.at[int(data1[i, 0]) - j, 't6']
                answer.at[int(data1[i, 2] + 1) * month - j, 'did'] = data.at[int(data1[i, 0]) - j, 'did']
                answer.at[int(data1[i, 2] + 1) * month - j, 't0'] = data.at[int(data1[i, 0]) - j, 't0']
                answer.at[int(data1[i, 2] + 1) * month - j, 'rank'] = data.at[int(data1[i, 0]) - j, 'rank']
                answer.at[int(data1[i, 2] + 1) * month - j, 'error'] = data.at[int(data1[i, 0]) - j, 'error']

                if j > 0:
                    nonth = month - 1
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 'MON'] = answer.at[
                        int(data1[i, 2] + 1) * month - j + 1, 'MON']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't1'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't1'] - answer.at[int(data1[i, 2] + 1) * month - j, 't1']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't2'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't2'] - answer.at[int(data1[i, 2] + 1) * month - j, 't2']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't3'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't3'] - answer.at[int(data1[i, 2] + 1) * month - j, 't3']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't4'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't4'] - answer.at[int(data1[i, 2] + 1) * month - j, 't4']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't5'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't5'] - answer.at[int(data1[i, 2] + 1) * month - j, 't5']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't6'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't6'] - answer.at[int(data1[i, 2] + 1) * month - j, 't6']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 'did'] = answer.at[
                        int(data1[i, 2] + 1) * month - j + 1, 'did']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 't0'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 't0'] - answer.at[int(data1[i, 2] + 1) * month - j, 't0']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 'rank'] = answer.at[
                        int(data1[i, 2] + 1) * month - j + 1, 'rank']
                    Difference.at[int(data1[i, 2] + 1) * nonth - j + 1, 'error'] = answer.at[int(
                        data1[i, 2] + 1) * month - j + 1, 'error'] - answer.at[int(
                        data1[i, 2] + 1) * month - j, 'error']

    datanext = np.zeros([6, 6])
    for i in range(6):
        for j in range(6):
            datanext[i, j] = random.randint(0, 1)

    datapredict = np.zeros([month + 1, 2])

    datapredict[month, 0] = random.uniform(70, 90)
    datapredict[month, 1] = random.randint(0, 3)

    for i in range(month):
        for j in range(lenth1):
            datapredict[i, 0] = datapredict[i, 0] + data.at[int(data1[j, 0]) - i, 't0']
            datapredict[i, 1] = datapredict[i, 1] + data.at[int(data1[j, 0]) - i, 'error']

        datapredict[i, 0] = datapredict[i, 0] / lenth1

    answer = makeup(answer, month * 6)

    Difference = makeup(Difference, (month - 1) * 6)

    text = np.zeros([6, 3])

    for i in range(6):
        text[i, 0] = random.randint(0, 1)
        text[i, 1] = random.uniform(1, 3)
        text[i, 2] = text[i, 0]

    return answer, Difference, datanext, datapredict, text

