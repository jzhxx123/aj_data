#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/12
Description: 
"""


# 安全信息查询
department_safety_sql = """SELECT 
    a.OCCURRENCE_TIME,
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m') AS MONTH,
    CONVERT(DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y%%m'), SIGNED) AS MON,
    a.MAIN_TYPE,
    a.RANK,
    b.PK_ID,
    b.FK_SAFETY_PRODUCE_INFO_ID,
    b.TYPE,
    b.FK_DEPARTMENT_ID,
    b.DEPARTMENT_NAME,
    b.RESPONSIBILITY_IDENTIFIED,
    b.STATUS
FROM
    t_safety_produce_info a
        LEFT JOIN
    t_safety_produce_info_responsibility_unit b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
WHERE
    a.FK_RESPONSIBILITY_DIVISION_ID = 667
        AND b.RESPONSIBILITY_IDENTIFIED IN (1 , 2, 3, 4, 5)
        AND a.MAIN_TYPE IN (1 , 2)
        AND b.FK_DEPARTMENT_ID = '{0}'
        AND b.STATUS not in (1)
ORDER BY b.FK_SAFETY_PRODUCE_INFO_ID;"""

# 获取责任单位检查项目
check_item_sql = """SELECT 
    FK_CHECK_ITEM_ID
FROM
    t_problem_base
WHERE
    PK_ID IN (SELECT 
            FK_PROBLEM_BASE_ID
        FROM
            t_safety_produce_info_problem_base
        WHERE
            FK_RESPONSIBILITY_UNIT_ID = {0});"""

# 获取责任单位风险类型
problem_base_risk_sql = """SELECT 
    FK_RISK_ID
FROM
    t_problem_base_risk
WHERE
    FK_PROBLEM_BASE_ID IN (SELECT 
            FK_PROBLEM_BASE_ID
        FROM
            t_safety_produce_info_problem_base
        WHERE
            FK_RESPONSIBILITY_UNIT_ID = {0});"""

# 获取责任单位风险类型
resp_unit_risk_sql = """ SELECT 
    FK_RISK_ID
FROM
    t_safety_produce_info_responsibility_unit_and_risk
WHERE
    FK_RESPONSIBILITY_UNIT_ID = {0}"""