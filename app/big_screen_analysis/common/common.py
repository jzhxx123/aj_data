#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/18
Description: 
"""
import pandas as pd

from app import mongo
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.data.index.common import get_zhanduan_deparment
from app.data.util import get_history_months
from app.data.health_index.common_sql import ZHANDUAN_DPID_SQL
from app.big_screen_analysis import major_risk_index_map


def get_station_by_sql(major):
    zhanduan_data = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    df = zhanduan_data[zhanduan_data['MAJOR'] == major]
    df = df.rename(columns={'DEPARTMENT_ID': 'dpid', 'NAME': 'label', 'MAJOR': 'major'})
    return df


def _get_station_by_risk_type(major, risk_type, mon):
    """
    通过重点指数风险类型获取实际参加计算的站段
    :param major:
    :param risk_type:
    :param mon:
    :return:
    """
    risk_type = int(risk_type)
    documents = list(mongo.db['monthly_major_index'].find({'MAJOR': major, 'TYPE': risk_type, 'MON': mon},
                                                          {'_id': 0, 'DEPARTMENT_ID': 1,
                                                           'DEPARTMENT_NAME': 1, 'MAJOR': 1, 'TYPE': 1}))

    if len(documents) == 0:
        df = get_station_by_sql(major)
        df['risk_type'] = risk_type
    else:
        df = pd.DataFrame(documents)
        df['risk_name'] = major_risk_map.get('{}-{}'.format(major, risk_type), {}).get('name', '重点指数')
        df.rename(columns={'DEPARTMENT_ID': 'dpid', 'DEPARTMENT_NAME': 'label', 'MAJOR': 'major', 'TYPE': 'risk_type'},
                  inplace=True)
    return df.to_dict(orient='records')


def get_station_data(param_dict):
    index_type = param_dict.get('INDEX_TYPE')
    major = param_dict.get('MAJOR')
    risk_type = param_dict.get('RISK_TYPE')

    if index_type and int(index_type) == 0:  # 获取综合指数站段
        df = get_station_by_sql(major)
        return df.to_dict(orient='records')
    else:  # 获取重点指数站段，根据最新一个月的数据
        months_ago = -1
        mon = get_history_months(months_ago)[0]
        if risk_type:
            return _get_station_by_risk_type(major, risk_type, mon)
        else:  # 没有指定risk_type，获取该专业下所有重点指数现在站段，分组
            res = []
            for k, v in major_risk_index_map.major_risk_map.items():
                risk_type = k.split('-')[1]
                if major in k:
                    res.append(_get_station_by_risk_type(major, risk_type, mon))
            return res
