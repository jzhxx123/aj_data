import functools
import inspect
import logging
import textwrap
import time
import warnings
import datetime
import calendar
import sqlalchemy
from flask import current_app
from app import db


def retry(f):
    """ 数据库超时重试 """

    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        for i in range(5):
            try:
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore')
                    r = f(*args, **kwargs)
                return r
            except (ConnectionResetError, sqlalchemy.exc.OperationalError) as e:
                my_frame = inspect.currentframe()
                while True:
                    (filename, line_number, function_name, lines,
                     index) = inspect.getframeinfo(my_frame)
                    print('EEROR: ',filename, line_number, function_name, lines, index)
                    if function_name in ('execute_daily', 'main', 'execute'):
                        break
                    else:
                        my_frame = my_frame.f_back
                tw = textwrap.TextWrapper(width=1000)
                sql = tw.fill(args[0])
                func_name = inspect.getframeinfo(my_frame)
                current_app.logger.warning(
                    f'**** Failed, func: {func_name}, time {i + 1}, args: \n{sql},error_message: {e}')
                time.sleep(10)
            finally:
                db.engine.dispose()

    return wrapped


def init_wrapper(f):
    """ 数据库超时重试 """

    @functools.wraps(f)
    def wrapped(*args, **kwargs):
        try:
            return f(*args, **kwargs)
        except Exception:
            logging.exception('Init Error')

    return wrapped


def record_func_runtime(func):
    """[记录函数运行时间]
 暂行数据锁死方案，六大指数（检查力度指数，考核力度，评价力度，
    检查均衡度，问题暴露度，问题整改成效）
    暂时办法是：考核力度指数在每月25到下个月1号，保持每天更新
                其余指数只更新25，26两天
Arguments:
    func {[functions]} -- [description]

Returns:
    [functions] -- [description]
    """
    from functools import wraps

    @wraps(func)
    def wrapper(*args, **kwargs):
        import os
        nowdate = datetime.date.today()
        # days = calendar.monthrange(nowdate.year, nowdate.month)[1]
        startdate = nowdate.replace(day=25)
        # end = nowdate + datetime.timedelta(days=days)
        enddate = nowdate.replace(day=28)
        flag = True
        start = time.time()
        # 获取函数模块最后一个文件名
        module_name_fragments = str(func.__module__).split('.')
        if module_name_fragments[-1] not in [
                                "assess_intensity",
                                "evaluate_intensity",
                                "problem_rectification"]:
            if startdate <= nowdate < enddate:
                flag = True
            else:
                flag = False
        # TODO 查询环境变量，是否启动临时更新模式（所有指数都更新）
        # 这里存在一个问题，可能以后面临不同指数更新方式不一样，这里暂无其他方案，
        # 临时使用这个环境变量过滤一下
        # print("TMP_UPDATE_MODE: ", os.getenv("TMP_UPDATE_MODE"))
        if not os.getenv("TMP_UPDATE_MODE"):
            if flag :
                flag = True
            else:
                current_app.logger.debug(f"├── └── {func.__module__} can't execute in illegal date( {nowdate} )!")
        else:
            flag = True
        if flag:
            func(*args, **kwargs)
            cost = round(time.time() - start, 3)
            current_app.logger.debug(f'├── └── {func.__module__} had costed time --- {cost}.')
    return wrapper
