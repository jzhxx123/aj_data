import itertools
from datetime import date
from typing import Dict, List, Optional, Tuple

from dateutil import parser, relativedelta, rrule
from flask import current_app


# from app.utils.init_func_maps import UPDATE_FUNC_MAP


class UpdateTimeController:
    update_day: int
    today: date
    update_date: date
    init_start_date: date
    env_init_start_month: date
    env_fake_today: Optional[date]

    optional_env_list = (
        'FAKE_TODAY',
        'INIT_START_MONTH',
    )

    def __init__(self, app=None):
        """"""
        self.env_fake_today = None
        self.today = None
        self.update_date = None
        self.init_start_date = None
        self.app = app
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        for env in self.optional_env_list:
            value = app.config.get(env)
            if value:
                setattr(self, 'env_' + env.lower(), parser.parse(value).date())

        self.today = self.get_today()
        self.update_day = app.config.get('UPDATE_DAY')
        if self.today.day >= self.update_day:
            # 25-31, 本月25
            self.update_date = self.today.replace(day=self.update_day)
        else:
            # 1-24, 上月25
            self.update_date = self.today + relativedelta.relativedelta(months=-1)
            self.update_date = self.update_date.replace(day=self.update_day)
        self.init_start_date = self.env_init_start_month + relativedelta.relativedelta(
            months=-1, day=self.update_day)

    @staticmethod
    def return_yesterday(dt_date: date):
        return dt_date + relativedelta.relativedelta(days=-1)

    def is_update_last_month(self):
        """ 是否更新上个月 """
        # 条件: 更新日至1号
        return self.today.day in itertools.chain(range(25, 32), range(1, 2))

    def get_today(self):
        """ 获取系统当前日期 """
        if self.env_fake_today:
            return self.env_fake_today
        else:
            return date.today()

    def get_current_period(self):
        """ 返回本月更新时间间隔 """
        return self.update_date, self.today

    def get_past_period(self):
        """ 返回过去更新时间间隔 """
        # 以开始时间, 按月间隔生成时间序列
        s_dates = [x.date() for x in
                   rrule.rrule(freq=rrule.MONTHLY, dtstart=self.init_start_date, until=self.update_date)]
        e_dates = list(map(self.return_yesterday, s_dates))
        return list(zip(s_dates[:-1], e_dates[1:]))

    def get_daily_update_month_list(self):
        """ 返回日更新列表"""
        if self.is_update_last_month():
            return [self.get_past_period()[-1], self.get_current_period()]
        else:
            return [self.get_current_period(), ]

    def get_init_month_list(self):
        """ 返回初始化月份列表"""
        return self.get_past_period() + [self.get_current_period(), ]


class UpdateMonth:
    def __init__(self, start_date: date, end_date: date, today: date):
        """ 月份更新对象 """
        self.start_date = start_date
        self.end_date = end_date
        self.year_month = self.get_year_month()
        self.today = today
        self.update_day = self.start_date.day
        self.prefix = self.get_col_prefix()
        self.risk = None  # 专业重点风险指数

    def __repr__(self):
        return str(self.year_month)

    def is_update_day(self):
        """ 对象是否是日更新的上个月 """
        # 日更新时, 需要在更新日至1日, 更新上个月
        if self.today.day in itertools.chain(range(25, 32), range(1, 2)):
            # 确认是否为上个月
            if self.today.day == 1:
                return True
            else:
                return (self.today + relativedelta.relativedelta(days=-1)).replace(day=self.update_day - 1) == self.end_date
        else:
            return False

    def is_this_month(self):
        return self.end_date == self.today

    def get_year_month(self):
        if self.start_date.month == self.end_date.month:
            d = self.end_date + relativedelta.relativedelta(months=1)
            return d.year * 100 + d.month
        else:
            return self.end_date.year * 100 + self.end_date.month

    def get_month_delta(self):
        """ 返回月份差值 """
        r = relativedelta.relativedelta(self.today, self.start_date)
        return r.years * 12 + r.months

    def get_col_prefix(self):
        """ 返回数据库月份前缀 """
        delta = self.get_month_delta()
        if delta == 0:
            return 'daily_'
        elif delta in range(1, 13):
            return 'monthly_'
        elif delta >= 13:
            return 'history_'
        else:
            raise ValueError(f'months delta is invalid: {delta}')


class InitSequenceHandler:
    maps: Dict

    def __init__(self, month_obj_list, index_month_list=None):
        """

        :param month_obj_list:
        :param index_month_list: 如果存在, 指数月份使用提供内容
        """
        self.index_month_list = index_month_list
        self.m_list = month_obj_list
        self.maps = current_app.maps
        self.seq = self.generate_seq()
        self.sorted_seq = self.sort()

    def get_index_month_list(self, key: str):
        """ 指数类型 月份返回特殊列表 """
        if key == 'indexes' and self.index_month_list is not None:
            return self.index_month_list
        else:
            return self.m_list

    def generate_seq(self):
        """ 生成函数+月份序列 """
        a = []
        for key, value in self.maps.items():
            for d in value:
                _list = self.get_index_month_list(key)
                d['months'] = _list
                a.append(d)
        return a

    def sort(self):
        """ 根据index值排序 """
        return sorted(self.seq, key=lambda x: x.get('index'))
