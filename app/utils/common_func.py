from datetime import datetime as dt

from dateutil.relativedelta import relativedelta
from flask import current_app, g

from app import mongo

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day

# 各个专业对应的TYPE2的部门ID
ALL_MAJOR_DEPARTMENT_ID_DICT = {
    "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
    "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
    "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
    "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
    "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
    "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC"
}


def get_major_dpid(dpid):
    # 通过department_id获取专业名称
    record = mongo.db.base_department.find_one({
        "DEPARTMENT_ID": dpid
    }, {
        "MAJOR": 1,
        "_id": 0
    })
    if record:
        return record['MAJOR']
    else:
        return None


def get_department_name_by_dpid(dpid):
    """根据部门ID取部门名称

    Arguments:
        dpid {str} -- 部门ID

    Returns:
        str -- 部门名称
    """
    record = mongo.db.base_department.find_one({
        "DEPARTMENT_ID": dpid
    }, {
        "NAME": 1,
        "_id": 0
    })
    if record:
        return record['NAME']
    else:
        raise ValueError('DPID不能存在')


def get_last_month(MONTHS=None):
    '''
    :start_date: yyyy-mm.
    :return: yyyy-mm
    '''
    today = get_today()
    last_mon = today + relativedelta(months=-MONTHS)
    last_mon = '{}{:0>2}'.format(last_mon.year, last_mon.month)
    now_mon = '{}{:0>2}'.format(today.year, today.month)
    return [int(last_mon), int(now_mon)]


def get_history_months(mondelta):
    today = get_today()
    delta = 0
    if today.day >= current_app.config.get('UPDATE_DAY'):
        delta = 1
    start_mon = today + relativedelta(months=mondelta + delta)
    start_mon = '{}{:0>2}'.format(start_mon.year, start_mon.month)
    if int(start_mon) < 201710:
        start_mon = '201710'
    end_mon = today + relativedelta(months=delta)
    end_mon = '{}{:0>2}'.format(end_mon.year, end_mon.month)
    return (int(start_mon), int(end_mon))


def get_last_year():
    mons = get_history_months(-12)
    return ['{}-{}'.format(str(mon)[:4], str(mon)[4:]) for mon in mons]


def choose_collection(param_dict):
    '''判断当前选择的开始、结束年月份是否需要跨daily_、monthly_查询
        :param_dict: dict
    '''
    if (param_dict['END_MONTH'] == param_dict['START_MONTH']) and (
            param_dict['START_MONTH'] != ""):
        today = get_today()
        now_mon = f'{today.year}{today.month:0>2}'
        if int(param_dict['START_MONTH']) > int(now_mon):
            return ['daily_']
        elif int(param_dict['START_MONTH']) == int(now_mon):
            if today.day >= current_app.config.get('UPDATE_DAY'):
                return ['monthly_']
            else:
                return ['daily_']
        else:
            return ['monthly_']
    else:
        return ['daily_', 'monthly_']


def choose_collection_prefix(mon):
    today = get_today()
    months = ((mon % 100) - today.month) + ((mon // 100) - today.year) * 12
    if today.day >= current_app.config.get('UPDATE_DAY'):
        months -= 1

    if months == 0:
        return 'daily_'
    elif months <= -13:
        return 'history_'
    elif months < 0 and months >= -12:
        return 'monthly_'
    else:
        return ValueError('month: %s invalid.' % mon)


def get_token_major(TOKEN):
    if TOKEN is None or TOKEN == '':
        return None
    document = mongo.db.user.find_one({
        'TOKEN': {
            '$elemMatch': {
                '$in': [TOKEN]
            }
        }
    }, {
        '_id': 0,
        'NAME': 1
    })
    if document is not None:
        return document['NAME']
    return None


def get_today():
    """获取系统当前日期

    Returns:
        today -- now date or fake now date
    """
    fake_today = current_app.config.get('FAKE_TODAY')
    if fake_today:
        today = dt.strptime(fake_today, '%Y-%m-%d')
    else:
        today = dt.today()
    return today


def get_date(today):
    """计算当前日期的年份月份

    Returns:
        mon -- year and month
    """
    day = today.day
    month = today.month
    year = today.year
    if day > current_app.config.get('UPDATE_DAY') and month < 12:
        mon = month + 1
    elif day > current_app.config.get('UPDATE_DAY') and month == 12:
        mon = 1
        year = year + 1
    else:
        mon = month
    return int(f'{year}{mon:0>2}')


def request_record(data):
    mongo.db['sys_request_log'].insert_one(data)


def wrapper_rtn_msg(data, status, msg):
    return {'data': data, 'status': status, 'error_message': msg}


def is_lack_required_args(required_args, request_args):
    """判断是否缺失必选参数

    Arguments:
        required_args {set} -- 必选参数
        request_args {set} -- 接口请求传入的参数

    Returns:
        bool --
    """
    rst = required_args.difference(request_args)
    if rst:
        return True
    return False

def get_type3_name_by_idcard(id_card):
    """
    根据用户身份证获取站段名称
    :param id_card:
    :return:
    """
    from app.data.util import pd_query
    get_type3_name_sql = """
    SELECT 
    c.PERSON_NAME, d.ALL_NAME, b.NAME,b.DEPARTMENT_ID
FROM
    t_person AS c
        INNER JOIN
  t_department as d on c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
    inner join
    t_department AS b ON d.TYPE3 = b.DEPARTMENT_ID
WHERE
    b.TYPE = 4 AND c.IS_DELETE = 0
        AND b.SHORT_NAME != ''
        AND d.IS_DELETE = 0
        and c.ID_CARD = '{0}'
    """
    info = pd_query(get_type3_name_sql.format(id_card))
    if info.empty:
        return "", ""
    return info.loc[0]['DEPARTMENT_ID'], info.loc[0]['NAME']


# niubi_cards = [
#     '510106197908081417', '510125197409290010', '510802198209070513',
#     '51012519840611581X', '510125198112131216', '511303198403160010',
#     '513401197008231616', '513001197803090857', '530111198203024454',
#     '520111198312165414', '510129198801173548', '511026197108194713',
#     '51100219740501153X', '51102719761025487X', '622826198610041178',
#     '510802198108253513', '510622197305220315', '510106198310261418',
#     '511137197610211413', '510283198008120399', '620202198912260223',
#     '51100219760618121X', '510781198801271816', '420105197302122036',
#     '362502198110296851', '510781197503118739', '510106198611111819',
#     '510625197612110019', '510125198403242311', '511025198407258818',
#     '511024198611225216', '510681198312120317', '513825198711020615',
#     '511024198011173133', '511026197303046417', '513401196310271614',
#     '510106198409031014', '51102619740921022X', '513401197605041618',
#     '320107197502013423', '510106197607211417'
# ]

niubi_cards = [
    "513401196905131610", "510106197503052132",
    "432524198203044537", "510106198409031014",
    "51102619740921022X", "320107197502013423",
    "510106197607211417", "513401196310271614",
    "511026197303046417", "510625197612110019",
]


def is_permission(person):
    major = ["车务", '供电', "机务", "车辆", "工务", "电务", "客运", "工电"]
    zhanduan_id, zhuanduan_name = get_type3_name_by_idcard(person['ID_CARD'])
    if person['ID_CARD'] in niubi_cards or person['MAJOR'] in["安监", "局领导"] :
        permission_type = 1
    elif person['MAJOR'] in major and zhanduan_id and zhuanduan_name:
        permission_type = 2
    else:
        permission_type = 3
    return permission_type, zhanduan_id, zhuanduan_name


def get_person_major(userid):
    person = mongo.db['base_person'].find_one({"ID_CARD": userid}, {"_id": 0})
    major = person['MAJOR']
    return major if major else '路局'


def get_person_info(userid):
    '''根据身份证ID获取相应身份信息'''
    person = mongo.db['base_person'].find_one({"ID_CARD": userid}, {"_id": 0})
    g.major = person['MAJOR']
    idcard = person['ID_CARD']
    # 职务代码
    job = str(person['PERSON_JOB_TYPE']) if 'PERSON_JOB_TYPE' in person.keys() else ''
    business_classfiy = str(person['BUSINESS_CLASSIFY']) if 'BUSINESS_CLASSIFY' in person.keys() else ''
    if idcard in niubi_cards or person['NAME'] == '局领导' or '安监室' in person['ALL_NAME'] or '党委正职' in job or \
            '处室正职' in job or '行政正职' in job or "领导" in business_classfiy:
        job_id = 0  # 领导
    else:
        job_id = 1  # 职员
    # 部门代码
    if person['TYPE'] in [1]:  # 路局
        dep_id = 0
        station = ''
        all_name = ''
    elif person['TYPE'] in [2, 3]:  # 专业
        dep_id = 1
        station = ''
        all_name = g.major
    elif person['TYPE'] in [4, 5]:  # 站段
        dep_id = 2
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''
    elif person['TYPE'] in [6, 7, 8]:  # 车间
        dep_id = 3
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''
    else:
        dep_id = 4  # 班组
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''

    permission_type, zhanduan_id, zhanduan_name = is_permission(person)
    if permission_type == 2:
        if zhanduan_id in ('99990002001499A10013',
                           '99990002001499A10010',
                           '99990002001499A10012',
                           '99990002001499A10015'):
            g.major = '工电'
        elif zhanduan_id in ('19B8C3534E1E5665E0539106C00A58FD',
                             '19B8C3534E205665E0539106C00A58FD',
                             '19B8C3534E125665E0539106C00A58FD',
                             '19B8C3534E3B5665E0539106C00A58FD',
                             '19B8C3534E055665E0539106C00A58FD',
                             '19B8C3534E045665E0539106C00A58FD'):
            g.major = '客运'
    content = f'{idcard}#{job_id}#{dep_id}#{station}#{all_name}#{zhanduan_id}#{zhanduan_name}#{permission_type}'
    # content = [idcard, job_id, dep_id]
    return content


def get_special_major_for_station(station_id):
    """
    若干的站段被归并到‘工电’，‘客运’两个专业中。目前主要通过该站段的ID来判断
    :param station_id: 站段的ID
    :return: None, 如果不是特殊站段；'工电'|'客运', 如果是在特殊的站段列表中，则返回其对应的专业名称
    """
    if station_id in ('99990002001499A10013',
                      '99990002001499A10010',
                      '99990002001499A10012',
                      '99990002001499A10015'):
        return '工电'
    elif station_id in ('19B8C3534E1E5665E0539106C00A58FD',
                        '19B8C3534E205665E0539106C00A58FD',
                        '19B8C3534E125665E0539106C00A58FD',
                        '19B8C3534E3B5665E0539106C00A58FD',
                        '19B8C3534E055665E0539106C00A58FD',
                        '19B8C3534E045665E0539106C00A58FD'):
        return '客运'
    return None


if __name__ == '__main__':
    print(get_last_month('2018-01'))
