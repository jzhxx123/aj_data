#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
"""
    This module is the collection of those functions or methods used in
    safety_index modules.
    date: 2018/02/28
"""
import calendar
import datetime
import os

import pandas as pd
import xlwt
from docx import Document
from docx.shared import Inches
from flask import current_app, jsonify, make_response, send_from_directory

from app import mongo
from app.data.util import get_mongodb_prefix
from app.data.util import plot_radar, filter_dianwu_laoan
from app.utils.common_func import choose_collection_prefix, wrapper_rtn_msg, get_major_dpid


def is_exist_dpid(dpid):
    """判断该部门单位是否存在

    Arguments:
        dpid {str} -- 部门单位ID

    Returns:
        bool --
    """
    record = mongo.db.base_department.find_one({"DEPARTMENT_ID": dpid})
    if record:
        return True
    else:
        return False


def return_report_export(file_path, file_name, func_report_export):
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, file_name, as_attachment=True))
    else:
        rtn = func_report_export()
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


def get_frontend_display_index_name(MAIN_TYPE):
    _map = {
        1: 'check_intensity',
        2: 'evaluate_intensity',
        4: 'check_balance',
        3: 'rate_intensity',
        5: 'issue_exposure',
        6: 'issue_fix',
        7: 'respon_produce_deduct',
        8: 'application',
        9: 'hidden_investigation',
        10: 'hidden_rectification',
        11: 'plan_management',
        12: 'stare_intensity',
        13: 'check_quality',
        14: 'check_control_intensity',
    }
    if MAIN_TYPE in _map:
        return _map.get(MAIN_TYPE, '-')


def get_risk_type_name(major, risk_type):
    risk_name = {
        "供电-1": "设备质量风险",
        "供电-2": "规章制度风险",
        "供电-3": "专业管理风险",
        "供电-4": "接触网专业管理风险",
        "供电-5": "职工素质",
        "供电-6": "劳安风险",
        "供电-7": "施工安全",
        "供电-8": "自轮设备安全风险",
        "供电-9": "自轮设备运用风险",
        '供电-10': '施工配合监管风险',
        "电务-1": "电务普铁信号设备质量风险分析",
        "电务-2": "电务普铁通信设备质量风险分析",
        "电务-3": "电务高铁通信设备质量风险分析",
        "电务-4": "电务高铁信号设备质量风险分析",
        "电务-5": "电务劳动安全风险分析",
        "电务-6": "电务施工安全风险分析",
        "电务-7": "电务道岔设备",
        "电务-8": "点外修",
        "电务-9": "电务轨道电路",
        "电务-10": "电务信号机",
        "车务-1": "调车",
        "车务-2": "劳动安全",
        "车务-3": "车门管理",
        "车务-4": "客运组织",
        "车务-5": "多方向接发列车",
        "车务-6": "接发列车",
        "车务-7": "高动客接发列车",
        "车务-8": "货装安全风险",
        "车务-9": "接发列车错办风险",
        "车务-10": "调乘一体化",
        "机务-1": "间断瞭望",
        "机务-2": "调车风险",
        "机务-3": "漏检漏修、简化修风险",
        "机务-4": "错误操纵风险",
        "机务-5": "劳安风险",
        "机务-6": "机车质量风险",
        '机务-7': '库内牵车',
        "车辆-1": "火灾爆炸",
        "车辆-2": "劳动安全",
        "车辆-3": "客车制动供风",
        "车辆-4": "货车制动抱闸",
        "车辆-5": "高压牵引",
        "车辆-6": "车辆脱轨",
        "车辆-7": "列车分离",
        "车辆-8": "货车配件脱落",
        "车辆-9": "动车配件脱落",
        "车辆-10": "客车配件脱落",
        "车辆-11": "调车防溜",
        "工务-1": "防断风险",
        "工务-2": "劳动安全风险",
        "工务-3": "点外修作业风险",
        "工务-4": "防洪风险",
        "工务-5": "自轮设备风险",
        "工务-6": "施工安全风险",
        "工务-7": "施工风险",
        "工务-8": "天窗修风险",
        "工务-9": "施工监管风险",
        "工务-10": "站专线风险",
        "工务-11": "天窗施工单项",
        "工电-1": "劳动安全风险",
        "工电-2": "施工安全风险",
        "工电-3": "点外修作业风险",
        "工电-4": "天窗修风险",
        "工电-5": "施工风险",
        "工电-6": "施工安全风险(工务版)",
        "工电-7": "劳动安全风险(工务版)",
        "工电-8": "劳动安全风险(电务版)",
        "工电-9": "施工安全风险(电务版)",
        "工电-10": "劳动安全风险(供电版)",
        "工电-11": "施工安全风险(供电版)",
        "工电-12": "天窗施工单项",
        '工电-13': '专业管理风险(供电版)',
        '工电-14': '自轮设备风险(供电版)',
        '工电-15': '施工配合监管风险(供电版)',
        "客运-1": "劳动安全风险",
        "客运-2": "客运组织及消防(车务站)",
        "客运-3": "客运组织及消防(客运段)",
        "客运-4": "客运组织及消防(客站)",

    }
    return risk_name.get(f'{major}-{risk_type}')


def get_index_title(MAIN_TYPE):
    """指数大类名称获取

    Arguments:
        MAIN_TYPE {int} -- 指数大类

    Returns:
        str -- 指数名称
    """
    _map = {
        1: '检查力度指数',
        2: '评价力度指数',
        4: '检查均衡度指数',
        3: '考核力度指数',
        5: '问题暴露指数',
        6: '问题整改效果指数',
        7: '安全效果(扣分)',
        8: '运用指数',
        9: '隐患排查力度指数',
        10: '隐患整改力度指数',
        11: '计划管理指数',
        12: '盯控力度指数',
        13: '检查质量指数',
        14: '卡控力度指数',
    }
    return _map.get(MAIN_TYPE, '-')


def get_child_calc_formula(DETAIL_TYPE, MAIN_TYPE):
    """指数中间过程计算公式的html模板

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 计算公式html模板
    """
    _check_intensity_map = {
        1:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位量化率({3}) = '
            + '量化人员数({4})/ 正式职工总数({5})*100%</p>',
        2:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
            + '现场检查次数(人次)({4})/ 单位总人数({5})*100%</p>',
        # 3: '换算查处问题率',
        5:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题平均质量分({3}) = '
            + '问题质量分累计({4})/ 问题总数({5})</p>',
        6:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位人均质量分({3}) = '
            + '问题质量分累计({4})/ 单位总人数({5})</p>',
        7:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '
            + '夜查次数(人次)({4})/ 现场检查次数(人次)({5})*100%</p>',
        8:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>较大及以上风险问题占比({3}) = '
            + '较大及以上风险问题数({4})/ 总人数({5})*100%</p>',
        9:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
            + '检查地点数({4})/ 地点总数({5})*100%</p>',
        10:
            '<p>{0}</p>'
    }
    _evaluate_intensity_map = {
        1:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>主动评价记分占比({3}) = '
            + '主动评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        2:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分条数({3}) = '
            + '主动评价记分条数({4})/ 干部总人数({5})</p>',
        3:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分分数({3}) = '
            + '主动评价记分分数({4})/ 干部总人数({5})</p>',
        4:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价职务占比({3}) = '
            + '（主动）科职干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        5:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>段机关干部占比({3}) = '
            + '（主动）段机关干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        6:
            '<p>{0}</p>',
        +
        7:
            '<p>{0}</p>',
    }
    _assess_intensity_map = {
        1:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = '
            + '考核问题数({4})/职工总人数 ({5})</p>',
        2:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) = '
            + '月度职工考核总金额({4})/ 职工总人数({5})</p>',
        3:
            '<p>{0}</p>',
        4:
            '<p>扣分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业比例: {2}</p><p>考核率({3}) = '
            + '考核问题数({4})/ 问题总数（非路外）({5})</p>',
        5:
            '<p>{0}</p>',
    }
    _check_evenness_map = {
        1:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = '
            + '查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>',
        2:
            '<p>{0}</p>',
        3:
            '<p>{0}</p>',
        4:
            '<p>{0}</p>',
        5:
            '<p>{0}</p>',
    }

    _problem_exposure_map = {
        1: '<p>{0}</p>',
        2: '<p>{0}</p>',
        3: '<p>{0}</p>',
        4: '<p>{0}</p>',
        5: '<p>{0}</p>',
    }

    _problem_rectification_map = {
        1:
            '<p>{0}</p>',
        2:
            '<p>{0}</p>',
        3:
            '<p>{0}</p>',
        4:
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>整改复查({3}) = '
            + '库内问题复查数({4})/ 总人数({5})</p>',
        5:
            '<p>{0}</p>',
        6:
            '<p>{0}</p>',
    }

    _respon_produce_map = {1: '责任生产信息扣分'}

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


# 安全综合指数
def get_child_index_name(DETAIL_TYPE, MAIN_TYPE):
    """获取子指数的名称

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 指数名称
    """
    _check_intensity_map = {
        # 1: '换算单位量化率',
        2: '换算单位检查频次',
        # 3: '换算查处问题率',
        5: '问题平均质量分',
        6: '换算人均质量分',
        7: '夜查率',
        8: '换算较严重及以上风险问题',
        9: '覆盖率',
        10: '监控调阅力度'
    }
    _evaluate_intensity_map = {
        1: '主动评价记分占比',
        2: '干部人均主动评价记分条数',
        3: '干部人均主动评价记分分数',
        4: '评价职务占比',
        5: '段机关干部占比',
        6: '分析中心得分',
        7: '评价集中度',
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '返奖率',
        4: '考核率',
        5: '扣分分值',
    }
    _check_evenness_map = {
        1: '问题查处均衡度',
        2: '检查日期均衡度',
        3: '检查地点均衡度',
        4: '检查时段均衡度',
        5: '问题日期均衡度',
    }

    _problem_exposure_map = {
        1: '普遍性暴露度',
        2: '较严重隐患暴露',
        3: '事故隐患问题暴露度',
        4: '班组问题暴露度',
        5: '他查问题扣分',
    }

    _problem_rectification_map = {
        1: '整改时效',
        2: '整改履责',
        3: '问题控制',
        4: '整改复查',
        # 5: '隐患复查',
        5: '隐患整治',
        6: '整改成效',
    }

    _respon_produce_map = {1: '责任生产信息扣分'}

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


# excl表格填充
def get_major_child_index_name(DETAIL_TYPE, MAIN_TYPE):
    """获取子指数的名称

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 指数名称
    """
    _check_intensity_map = {
        # 1: '换算单位量化率',
        2: '换算单位检查频次',
        3: '换算查处问题率',
        4: '查处问题考核率',
        5: '换算问题质量分',
        6: '较大风险问题质量均分',
        7: '夜查率',
        8: '一般及以上风险问题占比',
        9: '覆盖率',
        10: '监控调阅力度',
        11: '问题平均质量分',
        12: '换算单位跟班率',
        13: '夜查率',
    }
    _evaluate_intensity_map = {
        1: '主动评价记分占比',
        2: '干部人均主动评价记分条数',
        3: '人均评价记分',
        4: '评价职务占比',
        5: '段机关干部占比',
        6: '分析中心得分',
        7: '评价集中度',
        8: '履职评价占比',
        9: '站段路局评价次数差异',
        10: '站段路局评价分数差异',
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '返奖率',
        4: '考核率',
        5: '扣分分值',
    }
    _check_evenness_map = {
        1: '检查问题均衡度',
        2: '检查时间均衡度',
        3: '检查地点均衡度',
        4: '检查班组覆盖率',
        5: '检查时段均衡度',
        6: '关键时段检查均衡度',
    }

    _problem_exposure_map = {
        1: '总体暴露度',
        2: '较严重隐患暴露',
        3: '事故隐患问题暴露度',
        4: '班组问题暴露度',
        5: '他查问题扣分',
        7: '分析中心查处问题暴露',
        8: '劳安信息问题暴度',  # 电务劳安
        9: '超期设备管控力度',  # 供电设备质量
        10: '设备跳闸管控力度',  # 供电设备质量
        11: '问题均衡度',  # 电务点外修
        12: '问题查处率',  # 电务点外修
        13: '问题考核率',  # 电务点外修
    }

    _problem_rectification_map = {
        1: '整改时效',
        2: '整改履责',
        # 3: '整改反复',
        3: '问题控制',
        4: '整改复查',
        # 5: '隐患复查',
        5: '隐患整治',
        6: '整改成效',
        7: '中高质量问题占比',

    }

    _respon_produce_map = {
        1: '责任生产信息扣分',
        2: '机车故障扣分',
    }

    _application_map = {
        1: '报警率指数',
        2: '行车影响指数'
    }

    _hidden_investigation_map = {
        1: '问题暴露度指数',
        2: '检查均衡度指数',
        3: '报警分析指数',
        4: '问题考核率指数'
    }

    _hidden_rectification_map = {
        1: '报警销号指数',
        2: '报警控制指数',
        3: '报警处置及时率指数',
        4: '整改时效',
    }

    _plan_management_map = {
        1: '作业计划兑现率',
        2: '隐患处置计划管理指数',
    }

    _stare_intensity_map = {
        1: '检查力度指数',
        2: '检查均衡度指数',
        3: '盯控及时率指数',
        4: '盯控完整性指数',
    }
    _check_quality_map = {
        1: '换算单位质量分',
        2: '高质量问题质量分',
        3: '问题均衡度',
        4: '他查问题暴露',
        5: '关键问题查处力度'
    }

    _check_control_intensity_map = {
        1: '卡控质量指数',
    }

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
        _application_map,
        _hidden_investigation_map,
        _hidden_rectification_map,
        _plan_management_map,
        _stare_intensity_map,
        _check_quality_map,
        _check_control_intensity_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


def get_health_index_calc_tree(DPID, mon, index_type, index_flag=1):
    """获取某个部门某个月的安全综合指数计算过程

    Arguments:
        DPID {str} -- 部门ID
        mon {int} -- 月份， format： YYYYmm
        index_type {int} -- 指数分类（0代表安全综合指数，1-6分别代表各个子指数）

    Returns:
        list -- 指数计算过程
    """
    coll_name_prefix = get_mongodb_prefix(index_flag)
    prefix = choose_collection_prefix(mon)
    data = pd.DataFrame(
        list(mongo.db[f'{prefix}{coll_name_prefix}_index_basic_data'].find({
            'DEPARTMENT_ID':
                DPID,
            'MON':
                mon,
            'MAIN_TYPE':
                index_type,
        })))
    if data.empty:
        return None
    child_html = []
    for _, row in data.iterrows():
        title = get_child_index_name(row['DETAIL_TYPE'], index_type)
        formula_str = get_child_calc_formula(row['DETAIL_TYPE'], index_type)
        if row['TYPE'] == 1:
            # 处理2月份之前版本指数，没有专业平均分
            if 'AVG_QUOTIENT' not in row:
                avg_quotient = row['AVG_SCORE']
                avg_score = '暂无'
            else:
                avg_quotient = row['AVG_QUOTIENT']
                avg_score = row['AVG_SCORE']
            formula_html = {
                'name':
                    '{0}\n{1}red|({2}){3}'.format(title, '{', row['SCORE'], '}'),
                'value': {
                    'type':
                        'html',
                    'content':
                        formula_str.format(row['SCORE'], int(
                            row['RANK']), avg_quotient, row['QUOTIENT'],
                                           row['NUMERATOR'], row['DENOMINATOR'],
                                           avg_score),
                }
            }
        else:
            cnt = row['CONTENT']
            if pd.isnull(cnt):
                cnt = '暂无数据'
            formula_html = {
                'name': title,
                'value': {
                    'type': 'html',
                    'content': formula_str.format(cnt),
                }
            }
        child_html.append(formula_html)
    rtn_html = {'name': get_index_title(index_type), 'children': child_html}
    return rtn_html


def get_risk_child_calc_formula(DETAIL_TYPE, CONTENT_TYPE, MAIN_TYPE):
    """指数中间过程计算公式的html模板

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 计算公式html模板
    """
    _check_intensity_map = {
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
            + '现场检查作业次数({4})/ 工作量({5})</p>',
        '2-2':
            '<p>{0}</p>',
        '2-3':
            '<p>{0}</p>',
        '3-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = '
            + '问题数({4})/ 工作量({5})</p>',
        '3-2':
            '<p>{0}</p>',
        '3-3':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题考核率({3}) = '
            + '考核问题数({4})/ 发现问题数({5})</p>',
        '4-2':
            '<p>{0}</p>',
        '4-3':
            '<p>{0}</p>',
        '5-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = '
            + '问题质量分累计({4})/ 工作量({5})</p>',
        '5-2':
            '<p>{0}</p>',
        '5-3':
            '<p>{0}</p>',
        '6-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>较大风险问题质量均分({3}) = '
            + '较大和重大安全风险问题质量分累计({4})/ 工作量({5})</p>',
        '6-2':
            '<p>{0}</p>',
        '7-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '
            + '夜查次数({4})/ 现场检查次数({5})*100%</p>',
        '7-2':
            '<p>{0}</p>',
        '7-3':
            '<p>{0}</p>',
        '8-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>一般及以上风险问题占比({3}) = '
            + '一般及以上风险问题数({4})/ 问题总数({5})*100%</p>',
        '8-2':
            '<p>{0}</p>',
        '9-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
            + '检查班组数({4})/ 作业班组数({5})*100%</p>',
        '9-2':
            '<p>{0}</p>',
        '9-3':
            '<p>{0}</p>',
        '10-2':
            '<p>{0}</p>',
        '11-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题平均质量分({3}) = '
            + '问题质量分累计({4})/ 问题总数({5})</p>',
        '12-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>跟班率({3}) = '
            + '跟班工作票数({4})/ 工作票总数({5})*100%</p>',
        '13-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '
            + '夜查跟班工作票数({4})/ 夜查工作票数({5})</p>',
        '14-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>添乘检查频次({3}) = '
            + '检查次数({4})/出车趟次({5})</p>',
        '15-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>一般风险问题质量均分({3}) = '
            + '一般风险及以上问题质量分累计({4})/工作量({5})</p>',
        '15-2':
            '<p>{0}</p>',
        '16-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
            + '现场检查或添乘检查地点数({4})/地点总数({5})</p>',
        '17-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>质量均分({3}) = '
            + '问题质量分累计({4})/ 工作量({5})</p>',
        '17-2':
            '<p>{0}</p>',
        '18-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>视频查处问题率({3}) = '
            + '视频检查货装问题数({4})/ 装卸车工作量({5})</p>',
    }
    _evaluate_intensity_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>主动评价记分占比({3}) = '
            + '主动评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        '1-2':
            '<p>{0}</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分条数({3}) = '
            + '主动评价记分条数({4})/ 干部总人数({5})</p>',
        '2-2':
            '<p>{0}</p>',
        '3-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分分数({3}) = '
            + '主动评价记分分数({4})/ 干部总人数({5})</p>',
        '3-2':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价职务占比({3}) = '
            + '（主动）科职干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        '4-2':
            '<p>{0}</p>',
        '5-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>段机关干部占比({3}) = '
            + '（主动）段机关干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>',
        '5-2':
            '<p>{0}</p>',
        '6-2':
            '<p>{0}</p>',
        '7-2':
            '<p>{0}</p>',
        '8-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价力度({3}) = '
            + '履职评价分数（指定风险）({4})/ 履职评价总分({5})*100%</p>',
        '9-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>履职评价条数差（站段与路局）({3}) = '
            + '履职评价条数（站段）({4})/ 履职评价条数（路局）({5})*100%</p>',
        '9-2':
            '<p>{0}</p>',
        '10-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>履职评价分数差（站段与路局）({3}) = '
            + '履职评价分数（站段）({4})/ 履职评价分数（路局）({5})*100%</p>',
        '10-2':
            '<p>{0}</p>',
        '11-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价得分比率({3}) = '
            + '三个月被评价人次数量({4})/ 三个月所有评价人次总数({5})</p>',
        '11-2':
            '<p>{0}</p>',
    }
    _assess_intensity_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = '
            + '考核问题数({4})/职工总人数 ({5})</p>',
        '1-2':
            '<p>{0}</p>',
        '1-3':
            '<p>{0}</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) = '
            + '月度考核总金额({4})/ 总人数({5})</p>',
        '2-2':
            '<p>{0}</p>',
        '2-3':
            '<p>{0}</p>',
        '3-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>返奖率({3}) = '
            + '月度返奖金额({4})/ 月度考核金额({5})</p>',
        '3-2':
            '<p>{0}</p>',
        '3-3':
            '<p>{0}</p>',
        '4-1':
            '<p>得分(扣分)：{0}</p><p>专业平均得分(扣分)：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>考核率({3}) = '
            + '考核问题数({4})/ 问题总数({5})</p>',
        '4-3':
            '<p>{0}</p>',
        '5-2':
            '<p>{0}</p>',
        '5-3':
            '<p>{0}</p>',
    }
    _check_evenness_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = '
            + '查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>',
        '1-2':
            '<p>{0}</p>',
        "1-3":
            '<p>{0}</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) = '
            + '月度考核总金额({4})/ 总人数({5})</p>',
        '2-2':
            '<p>{0}</p>',
        '3-2':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
            + '检查地点数({4})/ 地点总数({5})*100%</p>',
        '4-2':
            '<p>{0}</p>',
        '5-2':
            '<p>{0}</p>',
        '6-2':
            '<p>{0}</p>',
    }

    _problem_exposure_map = {
        '1-2':
            '<p>{0}</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>较大隐患问题暴露度({3}) = '
            + '自查较大隐患问题({4})/ 所有劳安问题({5})</p>',
        '2-2':
            '<p>{0}</p>',
        '3-2':
            '<p>{0}</p>',
        '4-2':
            '<p>{0}</p>',
        '5-2':
            '<p>{0}</p>',
        '6-2':
            '<p>{0}</p>',
        '7-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>分析中心查处问题暴露({3}) = '
            + '分析中心查处的间断瞭望问题数({4})/ 乘务工作量({5})*100%</p>',
        '8-2':
            '<p>{0}</p>',
        '9-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>超期设备管控力度({3}) = '
            + '未超期设备({4})/ 本单位换算设备数量({5})*100%</p>',
        '10-2':
            '<p>{0}</p>',
        '11-2':
            '<p>{0}</p>',
        '12-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处率({3}) = '
            + '关键问题数({4})/ 关键问题总数({5})*100%</p>',
        '13-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题考核率({3}) = '
            + '点外修考核问题数({4})/ 点外修考核问题数({5})*100%</p>',
        '14-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题数({3}) = '
            + '问题数({4})/ 基本工作量({5})*100%</p>',
        '15-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>质量分占比({3}) = '
            + '问题质量分({4})/ 基本工作量({5})*100%</p>',
        '16-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>中高质量问题数占比({3}) = '
            + '中高质量问题数({4})/ 基本工作量({5})*100%</p>',
        '17-2':
            '<p>{0}</p>',
        '18-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>高质量问题数占比({3}) = '
            + '高质量问题数({4})/ 基本工作量({5})*100%</p>',
        '19-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>高质量问题质量分占比({3}) = '
            + '高质量问题质量分({4})/ 基本工作量({5})*100%</p>',
    }

    _problem_rectification_map = {
        '1-2':
            '<p>{0}</p>',
        '2-2':
            '<p>{0}</p>',
        '3-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题控制({3}) = '
            + '问题数({4})/ 基本工作量({5})</p>',
        '3-2':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>整改复查({3}) = '
            + '库内问题复查数({4})/ 总人数({5})</p>',
        '5-2':
            '<p>{0}</p>',
        '6-2':
            '<p>{0}</p>',
        '7-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>中高质量问题占比({3}) = '
            + '点外修中高质量问题查数({4})/ 点外修总问题数({5})</p>',
    }

    _respon_produce_map = {
        '1-2':
            '<p>{0}</p>',
        '2-2':
            '<p>{0}</p>',
    }

    _application_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>报警率指数({3}) = '
            + '报警总条数({4})/ 道岔牵引点总数({5})*100%</p>',
        '1-2':
            '<p>{0}</p>',
        '2-2':
            '<p>{0}</p>',
    }
    _hidden_investigation_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题暴露度指数({3}) = '
            + ' 微机监测问题数量({4})/干部检查问题总数({5})*100%</p>',
        '1-2':
            '<p>{0}</p>',
        '2-2':
            '<p>{0}</p>',
        '3-2':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>考核率({3}) = '
            + ' 考核问题数({4})/总问题数({5})</p>',
    }

    _hidden_rectification_map = {
        '1-2':
            '<p>{0}</p>',
        '2-2':
            '<p>{0}</p>',
        '3-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>报警处置及时率指数({3}) = '
            + '告警及时处置条数({4})/告警总条数({5})*100%</p>',
        '4-2':
            '<p>{0}</p>',
    }

    _plan_management_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>作业计划兑现率({3}) = '
            + '点外修完成数量({4})/点外修总数量({5})*100%</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>隐患处置计划管理指数({3}) = '
            + '状态修条数({4})/1、2级报警总数({5})*100%</p>',
    }

    _stare_intensity_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>检查力度({3}) = '
            + '三级目录检查次数({4})/点外修作业条数({5})*100%</p>',
        '2-2':
            '<p>{0}</p>',
        '3-2':
            '<p>{0}</p>',
        '4-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>盯控完整性({3}) = '
            '完整盯控作业条数({4})/总作业条数({5})*100%</p>',
    }

    _check_quality_map = {
        '1-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位质量分({3}) = '
            + '问题质量分累计({4})/ 工作量({5})</p>',
        '2-1':
            '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>高质量问题质量分({3}) = '
            + '高质量问题质量分累计({4})/ 工作量({5})</p>',
        '3-2':
            '<p>{0}</p>',
        '4-2':
            '<p>{0}</p>',
        '5-2':
            '<p>{0}</p>',
    }
    _check_control_intensity_map = {
        '1-2':
            '<p>{0}</p>',
    }

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
        _application_map,
        _hidden_investigation_map,
        _hidden_rectification_map,
        _plan_management_map,
        _stare_intensity_map,
        _check_quality_map,
        _check_control_intensity_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    DETAIL_TYPE = f'{DETAIL_TYPE}-{CONTENT_TYPE}'
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


# 文档,重点指数
def get_risk_child_index_name(DETAIL_TYPE, MAIN_TYPE):
    """获取子指数的名称

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 指数名称
    """
    _check_intensity_map = {
        # 1: '换算单位量化率',
        2: '换算单位检查频次',
        3: '查处问题率',
        4: '查处问题考核率',
        5: '换算人均问题质量分',
        # 5: '质量均分',
        6: '较大风险问题质量均分',
        7: '夜查率',
        # 8: '较大风险问题及以上占比',
        8: '一般及以上风险问题占比',
        9: '覆盖率',
        10: '监控调阅力度',
        11: '问题平均质量分',
        12: '跟班率',
        13: '夜查率',
        14: '添乘检查频次',
        15: '一般风险问题质量均分',
        16: '覆盖率',
        17: '质量均分',  # 电务劳安
        18: '视频查处问题率'
    }
    _evaluate_intensity_map = {
        1: '主动评价记分占比',
        2: '干部人均主动评价记分条数',
        3: '干部人均主动评价记分分数',
        4: '评价职务占比',
        5: '段机关干部占比',
        6: '分析中心得分',
        7: '评价集中度',
        8: '评价占比',
        9: '履职评价条数差（站段与路局）',
        10: '履职评价分数差（站段与路局）',
        11: '评价得分'
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '返奖率',
        4: '考核率',
        5: '扣分分值'
    }
    _check_evenness_map = {
        1: '问题查处均衡度',
        2: '检查时间均衡度',
        3: '检查地点均衡度',
        4: '覆盖率',
        5: "检查时段均衡度",
        6: "关键时段检查均衡度",
    }

    _problem_exposure_map = {
        1: '普遍性暴露度',
        2: '较严重隐患暴露',
        3: '事故隐患问题暴露度',
        4: '班组问题暴露度',
        5: '他查问题扣分',
        6: '问题暴露度',
        7: '分析中心查处问题暴露',
        8: '劳安信息问题暴度',  # 电务劳安
        9: '超期设备管控力度',  # 供电设备质量
        10: '设备跳闸管控力度',  # 供电设备质量
        11: '问题均衡度',  # 电务点外修
        12: '问题查处率',  # 电务点外修
        13: '问题考核率',  # 电务点外修
        14: '问题数',  # 机务
        15: '质量分占比',  # 机务
        16: '中高质量问题数占比',  # 机务
        17: '中高质量问题质量分占比',  # 机务
        18: '高质量问题数占比',  # 机务
        19: '高质量问题质量分占比',  # 机务
    }

    _problem_rectification_map = {
        1: '整改时效',
        2: '整改履责',
        # 3: '整改成效',
        3: '问题控制',
        4: '整改复查',
        # 5: '隐患复查',
        5: '隐患整治',
        6: '整改成效',
        7: '中高质量问题占比',
    }

    _respon_produce_map = {
        1: '责任生产信息扣分',
        2: '机车故障扣分'
    }

    _application_map = {
        1: '报警率指数',
        2: '行车影响指数'
    }

    _hidden_investigation_map = {
        1: '问题暴露度指数',
        2: '检查均衡度指数',
        3: '报警分析指数',
        4: '问题考核率指数'
    }

    _hidden_rectification_map = {
        1: '报警销号指数',
        2: '报警控制指数',
        3: '报警处置及时率指数',
        4: '整改时效',
    }

    _plan_management_map = {
        1: '作业计划兑现率',
        2: '隐患处置计划管理指数',
    }

    _stare_intensity_map = {
        1: '检查力度指数',
        2: '检查均衡度指数',
        3: '盯控及时率指数',
        4: '盯控完整性指数',
    }
    _check_quality_map = {
        1: '换算单位质量分',
        2: '高质量问题质量分',
        3: '问题均衡度',
        4: '他查问题暴露',
        5: '关键问题查处力度',
    }

    _check_control_intensity_map = {
        1: '卡控质量指数',
    }

    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
        _application_map,
        _hidden_investigation_map,
        _hidden_rectification_map,
        _plan_management_map,
        _stare_intensity_map,
        _check_quality_map,
        _check_control_intensity_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


def _get_default_title_for_risk_index(MAIN_TYPE, DETAIL_TYPE):
    """获取子指数的名称

    Arguments:
        DETAIL_TYPE {int} -- 指数小类1-N
        MAIN_TYPE {int} -- 指数大类1-N

    Returns:
        str -- 指数名称
    """
    _check_intensity_map = {10: '暂无数据'}
    _evaluate_intensity_map = {
        6: '未查询到分析中心得分',
        7: '暂无相关数据',
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '暂无返奖数据',
        4: '暂无相关数据',
        5: '暂无相关数据',
    }
    _check_evenness_map = {1: '问题查处均衡度', 2: '检查时间均衡度', 3: '该站段没有相关作业班组'}

    _problem_exposure_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
        3: '暂无相关数据',
        4: '不存在未暴露班组',
        5: '没有发现他查问题',
        6: '暂无相关数据'
    }

    _problem_rectification_map = {
        1: '没有超期问题',
        2: '没有整改履责评价',
        3: '前3个月没有发生超过阀门值的统一问题项点',
        4: '暂无相关数据',
        5: '暂无相关数据',
        6: '暂无相关数据',
    }
    _respon_produce_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
    }
    _application_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
    }
    _hidden_investigation_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
        3: '暂无相关数据',
        4: '暂无相关数据',
    }
    _hidden_rectification_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
        3: '暂无相关数据',
        4: '暂无相关数据',
    }
    _plan_management_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
    }
    _stare_intensity_map = {
        1: '暂无相关数据',
        2: '暂无相关数据',
        3: '暂无相关数据',
        4: '暂无相关数据',
    }

    _check_quality_map = {
        3: '暂无相关数据',
        4: '暂无相关数据',
    }

    _check_control_intensity_map = {
        1: '暂无相关数据',
    }
    map_dict = [
        _check_intensity_map,
        _evaluate_intensity_map,
        _assess_intensity_map,
        _check_evenness_map,
        _problem_exposure_map,
        _problem_rectification_map,
        _respon_produce_map,
        _application_map,
        _hidden_investigation_map,
        _hidden_rectification_map,
        _plan_management_map,
        _stare_intensity_map,
        _check_quality_map,
        _check_control_intensity_map,
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, '-')


def get_risk_index_calc_tree(DPID, mon, major, risk_type, index_type):
    """获取某个部门某个月的重点分析指数计算过程

    Arguments:
        DPID {str} -- 部门ID
        mon {int} -- 月份， format： YYYYmm
        major{str} -- 专业，ex: [车机工电、车辆、供电]
        index_type {int} -- 指数分类（0代表安全综合指数，1-6分别代表各个子指数）

    Returns:
        list -- 指数计算过程
    """
    prefix = choose_collection_prefix(mon)
    data = pd.DataFrame(
        list(mongo.db[f'{prefix}major_index_basic_data'].find({
            'DEPARTMENT_ID':
                DPID,
            'MON':
                mon,
            'MAIN_TYPE':
                index_type,
            'MAJOR':
                major,
            'INDEX_TYPE':
                risk_type,
        })))
    # 处理电务劳安
    data = filter_dianwu_laoan(data, risk_type, major)

    if len(data) < 1:
        current_app.logger.info(
            f'Error: MAIN_TYPE-{index_type}, MONTH-{mon} can not find ' +
            'major_index_basic_data')
        return None
    child_html = []
    for _, row in data.iterrows():
        title = get_risk_child_index_name(row['DETAIL_TYPE'], index_type)
        formula_str = get_risk_child_calc_formula(row['DETAIL_TYPE'],
                                                  row['TYPE'], index_type)
        if row['TYPE'] == 1 or row['TYPE'] == 3:
            # 类型1，3一套模版，不过3可以传值自定义内容
            # 处理2月份之前版本指数，没有专业平均分
            if 'AVG_QUOTIENT' not in row:
                avg_quotient = row['AVG_SCORE']
                avg_score = '暂无'
            else:
                avg_quotient = row['AVG_QUOTIENT']
                avg_score = row['AVG_SCORE']

            if row['TYPE'] == 3:
                formula_str = row['CONTENT']
            formula_html = {
                'name':
                    '{0}\n{1}red|({2}){3}'.format(title, '{', row['SCORE'], '}'),
                'value': {
                    'type':
                        'html',
                    'content':
                        formula_str.format(row['SCORE'], int(
                            row['RANK']), avg_quotient, row['QUOTIENT'],
                                           row['NUMERATOR'], row['DENOMINATOR'],
                                           avg_score),
                }
            }
        else:
            # TODO 类型2 不会显示中间过程分数
            cnt = row['CONTENT']
            if pd.isnull(cnt):
                cnt = _get_default_title_for_risk_index(
                    index_type, row['DETAIL_TYPE'])
            formula_html = {
                'name': title,
                'value': {
                    'type': 'html',
                    'content': formula_str.format(cnt),
                }
            }
        child_html.append(formula_html)
    rtn_html = {'name': get_index_title(index_type), 'children': child_html}
    return rtn_html


def export_health_index_word(department_name, param_dict, file_path, index_flag=1):
    coll_name_prefix = get_mongodb_prefix(index_flag)
    mon = int(param_dict['MONTH'])
    if mon < 201901:
        return '2019年以前指数不生成报告'
    dpid = param_dict['DPID']
    document = Document()
    document.add_heading(f'{department_name}安全管理综合指数报告（{mon // 100}年{mon % 100}月）',
                         0)
    # todo 添加雷达图
    prefix = choose_collection_prefix(mon)
    radar_labels = list(mongo.db[f"{prefix}detail_{coll_name_prefix}_index"].find({"DETAIL_TYPE": 0, "MON": mon,
                                                                                   "DEPARTMENT_ID": dpid,
                                                                                   "DEPARTMENT_NAME": department_name},
                                                                                  {'_id': 0, "MAIN_TYPE": 1,
                                                                                   "SCORE": 1}))
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE']) for label in radar_labels]
        data = []
        subtags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            major = get_major_dpid(dpid)
            data_tmp = list(mongo.db[f'{prefix}detail_{coll_name_prefix}_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major},
                {"_id": 0, "SCORE": 1, "RANK": 1}))

            most_data[main_type] = [i['SCORE'] for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE'] for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        pic_path = plot_radar([get_index_title(label['MAIN_TYPE'])[:-2] for label in radar_labels],
                              data, f'{department_name}安全管理综合', subtags)
        document.add_picture(pic_path, width=Inches(6.0))
        # 添加描述表格
        table = document.add_table(rows=4, cols=7, style='Table Grid')
        # 第一行所有的方格
        for r in range(4):
            hdr_cells = table.rows[r].cells
            if r == 0:
                for p in range(1, 7):
                    hdr_cells[p].add_paragraph(labels[p - 1])
            else:
                for p in range(7):
                    if p == 0:
                        hdr_cells[p].add_paragraph(subtags[r - 1])
                    else:
                        hdr_cells[p].add_paragraph(str(round(data[r - 1][p - 1], 2)))

    # 往文档中添加段落
    for main_type in [1, 2, 3, 4, 5, 6]:
        main_type_weight = mongo.db['base_index_weight'].find(
            {
                'INDEX_TYPE': 0,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            })[0]['WEIGHT']
        index_title = '{0}. {1}({2}%)'.format(main_type,
                                              get_index_title(main_type),
                                              main_type_weight * 100)
        document.add_heading(index_title, level=1)

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'INDEX_TYPE': 0,
                'MAIN_TYPE': main_type,
                'MON': mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'INDEX_TYPE': 0,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))
        # weight_data = list(mongo.db['base_detail_index_weight'].find(
        #     {
        #         'INDEX_TYPE': 0,
        #         'MAIN_TYPE': main_type
        #     }, {
        #         '_id': 0,
        #         'WEIGHT': 1,
        #         'DETAIL_TYPE': 1
        #     }))
        weight_table = document.add_table(rows=1, cols=2)
        heading_cells = weight_table.rows[0].cells
        heading_cells[0].text = '指数名称'
        heading_cells[1].text = '占比%'
        for item in weight_data:
            cells = weight_table.add_row().cells
            cells[0].text = get_child_index_name(item['DETAIL_TYPE'],
                                                 main_type)
            cells[1].text = str(int(item['WEIGHT'] * 100)).replace(
                '-100', '扣分')
        prefix = choose_collection_prefix(mon)
        basic_data = list(mongo.db[f'{prefix}{coll_name_prefix}_index_basic_data'].find({
            'DEPARTMENT_ID':
                dpid,
            'MON':
                mon,
            'MAIN_TYPE':
                main_type,
        }))
        if len(basic_data) < 1:
            return "NO DATA"
        basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
        for idx, item in enumerate(weight_data):
            idx_name = get_child_index_name(item["DETAIL_TYPE"], main_type)
            document.add_heading(f'{main_type}.{idx + 1} {idx_name}')
            if item['DETAIL_TYPE'] not in basic_data_dict:
                continue
            row = basic_data_dict.get(item['DETAIL_TYPE'])
            formula_str = get_child_calc_formula(row['DETAIL_TYPE'], main_type)
            if row['TYPE'] == 1:
                # 处理2月份之前版本指数，没有专业平均分
                if 'AVG_QUOTIENT' not in row:
                    avg_quotient = row['AVG_SCORE']
                    avg_score = '暂无'
                else:
                    avg_quotient = row['AVG_QUOTIENT']
                    avg_score = row['AVG_SCORE']
                index_paragraph = formula_str.format(
                    row['SCORE'], int(
                        row['RANK']), avg_quotient, row['QUOTIENT'],
                    row['NUMERATOR'], row['DENOMINATOR'], avg_score)
            else:
                cnt = row['CONTENT']
                if pd.isnull(cnt):
                    cnt = '暂无数据'
                index_paragraph = formula_str.format(cnt)
            document.add_paragraph(
                index_paragraph.replace('<p>', '').replace('</p>',
                                                           '\n').replace(
                    '<br/>', '\n'))
    # 保存文档
    document.save(file_path)
    return True


def get_grandson_index_data_by_major(coll_prefix, condition, index_flag=1):
    coll_name_prefix = get_mongodb_prefix(index_flag)
    total_condition = condition.copy()
    grandson_docs = list(mongo.db[f'{coll_prefix}detail_{coll_name_prefix}_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(grandson_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in detail_{coll_name_prefix}_index(MONTH-' +
            f'{condition})')
        return []
    df_grandson = pd.DataFrame(grandson_docs)
    grandson_data = {}
    full_grandson_index = {
        1: (0, 2, 5, 6, 7, 8, 9, 10),
        2: (0, 1, 2, 3, 4, 5, 6, 7),
        3: (0, 1, 2, 3, 4),
        4: (0, 1, 2, 3, 4, 5),
        5: (0, 1, 2, 3, 4, 5),
        6: (0, 1, 2, 3, 4, 5, 6),
    }
    for idx, val in df_grandson.groupby(['DEPARTMENT_NAME', 'MAIN_TYPE']):
        dep_name = idx[0]
        main_type = idx[1]
        grandson_score = []
        for detail_type in full_grandson_index[main_type]:
            if detail_type in val['DETAIL_TYPE'].values:
                grandson_score.append([
                    detail_type,
                    val[val['DETAIL_TYPE'] == detail_type]['SCORE'].values[0]
                ])
            else:
                grandson_score.append([detail_type, 0])
        dep_data = grandson_data.get(dep_name, {})
        dep_data.update({main_type: grandson_score})
        grandson_data.update({dep_name: dep_data})
    # 取各个站段的综合指数分数
    total_docs = list(mongo.db[f'{coll_prefix}{coll_name_prefix}_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in {coll_name_prefix}_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in grandson_data:
            dep_data = grandson_data[dep_name]
        dep_data.update({0: each['SCORE']})
        grandson_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    fmt_index_score = {}
    for dep_name in grandson_data:
        fmt_dep_data = []
        dep_data = grandson_data[dep_name]
        for i in range(1, 7):
            if i not in dep_data:
                continue
            fmt_dep_data.append(dep_data[i])
        fmt_dep_data.append(['综合指数', dep_data[0]])

        fmt_index_score.update({dep_name: fmt_dep_data})
    return fmt_index_score


def get_major_grandson_index_data_by_major(coll_prefix, condition, full_grandson_index):
    total_condition = condition.copy()
    grandson_docs = list(mongo.db[f'{coll_prefix}detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(grandson_docs) == 0:
        current_app.logger.info(
            'Error: can not find data in detail_major_index(MONTH-' +
            f'{condition})')
        return []
    df_grandson = pd.DataFrame(grandson_docs)
    grandson_data = {}
    full_grandson_index = full_grandson_index
    for idx, val in df_grandson.groupby(['DEPARTMENT_NAME', 'MAIN_TYPE']):
        dep_name = idx[0]
        main_type = idx[1]
        grandson_score = []
        for detail_type in full_grandson_index[main_type]:
            if detail_type in val['DETAIL_TYPE'].values:
                grandson_score.append([
                    detail_type,
                    val[val['DETAIL_TYPE'] == detail_type]['SCORE'].values[0]
                ])
            else:
                grandson_score.append([detail_type, 0])
        dep_data = grandson_data.get(dep_name, {})
        dep_data.update({main_type: grandson_score})
        grandson_data.update({dep_name: dep_data})
    # 取各个站段的重点指数分数
    total_docs = list(mongo.db[f'{coll_prefix}major_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in major_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in grandson_data:
            dep_data = grandson_data[dep_name]
        dep_data.update({0: each['SCORE']})
        grandson_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    fmt_index_score = {}
    for dep_name in grandson_data:
        fmt_dep_data = []
        dep_data = grandson_data[dep_name]
        for i in full_grandson_index.keys():
            if i not in dep_data:
                continue
            fmt_dep_data.append(dep_data[i])
        # 存在一直情况，因为指数计算舍去一些站段，但是数据库还保留着，综合指数分默认为0
        fmt_dep_data.append(['综合指数', dep_data.get(0, 0)])
        fmt_index_score.update({dep_name: fmt_dep_data})
    return fmt_index_score


def get_index_table_data_by_major(coll_prefix, condition, index_type=1):
    coll_name_prefix = get_mongodb_prefix(index_type)
    total_condition = condition.copy()
    # 取出各个站段的子指数的分数
    child_docs = list(mongo.db[f'{coll_prefix}detail_{coll_name_prefix}_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(child_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in detail_{coll_name_prefix}_index(MONTH-' +
            f'{condition})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的分指数放入一个list
    child_data = {}
    for each in child_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((each['MAIN_TYPE'], each['SCORE']))
        child_data.update({dep_name: dep_data})
    # 取各个站段的综合指数分数
    del total_condition['DETAIL_TYPE']
    total_docs = list(mongo.db[f'{coll_prefix}{coll_name_prefix}_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in {coll_name_prefix}_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((0, each['SCORE']))
        child_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    child_score = []
    for dep_name in child_data:
        dep_score = {
            get_index_title(main_type).replace('-', '综合指数'): score
            for main_type, score in child_data[dep_name]
        }
        dep_score.update({'name': dep_name})
        child_score.append(dep_score)
    return child_score


def get_major_index_table_data_by_major(coll_prefix, condition):
    """
    :param coll_prefix:
    :param condition:
    :return:
    """
    total_condition = condition.copy()
    # 取出各个站段的子指数的分数
    child_docs = list(mongo.db[f'{coll_prefix}detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(child_docs) == 0:
        current_app.logger.info(
            'Error: can not find data in detail_major_index(MONTH-' +
            f'{condition})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的分指数放入一个list
    child_data = {}
    for each in child_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((each['MAIN_TYPE'], each['SCORE']))
        child_data.update({dep_name: dep_data})
    # 取各个站段的综合指数分数
    del total_condition['DETAIL_TYPE']
    total_docs = list(mongo.db[f'{coll_prefix}major_index'].find(
        total_condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
        }))
    if len(total_docs) == 0:
        current_app.logger.info(
            f'Error: can not find data in major_index(MONTH-' +
            '{total_condition["MON"]})')
        return []
    # # 工电段需要额外加入供电、电务、工务中
    # 按站段将每个站段的综合指数分插入之前的child_data中
    for each in total_docs:
        dep_name = each['DEPARTMENT_NAME']
        dep_data = []
        if dep_name in child_data:
            dep_data = child_data[dep_name]
        dep_data.append((0, each['SCORE']))
        # 按main_type的大小排序(升序)
        dep_data = sorted(dep_data, key=lambda x: x[0])
        child_data.update({dep_name: dep_data})

    # 排序、格式化child_data
    child_score = []
    for dep_name in child_data:
        # dep_score = {
        #     get_index_title(main_type).replace('-', '总指数'): score
        #     for main_type, score in child_data[dep_name]
        # }
        dep_score = {
            "name": ['name'],
            "value": [dep_name]
        }
        for main_type, score in child_data[dep_name]:
            dep_score['name'].append(get_index_title(main_type).replace('-', '总指数')),
            dep_score['value'].append(score)
        # dep_score.update({'name': dep_name})
        child_score.append(dep_score)
    return child_score


def export_score_table_excel(param_dict, file_path, index_flag=-1):
    wb = xlwt.Workbook(encoding='ascill')
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 3}
    major = param_dict['MAJOR']
    if major in ["综合", '供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    coll_prefix = choose_collection_prefix(mon)
    major_list = [major]
    if major == '综合':
        major_list = ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']
    full_grandson_index = {
        1: (0, 2, 5, 6, 7, 8, 9, 10),
        2: (0, 1, 2, 3, 4, 5, 6, 7),
        3: (0, 1, 2, 3, 4),
        4: (0, 1, 2, 3, 4, 5),
        5: (0, 1, 2, 3, 4, 5),
        6: (0, 1, 2, 3, 4, 5, 6),
    }
    # 获取子指数得分
    for major in major_list:
        condition.update({'MAJOR': major})
        major_data = get_grandson_index_data_by_major(coll_prefix,
                                                      condition.copy(),
                                                      index_flag=index_flag)
        if major_data == []:
            continue
        # 为每个专业生成一个work_sheet
        wt = wb.add_sheet(major)
        row = 0
        # 第一行填入各个指数名称
        if row == 0:
            wt.write(0, 0, '序号')
            wt.write(0, 1, '站段')
            col_title = 2
            for main_type in full_grandson_index:
                for detail_type in full_grandson_index[main_type]:
                    if detail_type == 0:
                        wt.write(0, col_title, get_index_title(main_type))
                    else:
                        wt.write(0, col_title,
                                 get_child_index_name(detail_type, main_type))
                    col_title += 1
            wt.write(0, col_title, '综合指数')
        for dep_name in major_data:
            col_idx = 2
            wt.write(row + 1, 0, row + 1)
            wt.write(row + 1, 1, dep_name)
            for child_index_scores in major_data[dep_name][:-1]:
                for child_score in child_index_scores:
                    wt.write(row + 1, col_idx, child_score[1])
                    col_idx += 1
            wt.write(row + 1, col_idx, major_data[dep_name][-1][1])
            row += 1
    wb.save(file_path)
    return True


def export_major_score_table_excel(param_dict, file_path):
    wb = xlwt.Workbook(encoding='ascill')
    mon = int(param_dict['MONTH'])
    if mon < 201711:
        return 'MONTH should be > 201810'
    condition = {"MON": mon, "HIERARCHY": 3}
    major = param_dict['MAJOR']
    if major in ['供电', '车辆', '工务', '车务', '机务', '电务', '客运', '工电']:
        condition.update({"MAJOR": major})
    else:
        return f'MAJOR - {major} INVAILD'

    coll_prefix = choose_collection_prefix(mon)

    # 重点风险指数列表
    risk_type_list = list(mongo.db[f'{coll_prefix}major_index'].distinct("TYPE", condition))

    # 获取子指数得分
    if risk_type_list:
        for risk_type in risk_type_list:
            condition_risktype = condition.copy()
            condition_risktype.update({'TYPE': risk_type})
            risk_name = get_risk_type_name(major, risk_type=risk_type)
            # 指数大类列表
            main_type_list = list(
                mongo.db[f'{coll_prefix}detail_major_index'].distinct("MAIN_TYPE", condition_risktype))
            full_grandson_index = dict()
            if main_type_list:
                for main_type in main_type_list:
                    condition_detailtype = condition_risktype.copy()
                    condition_detailtype.update({"MAIN_TYPE": main_type})
                    full_grandson_index[main_type] = tuple(mongo.db[f'{coll_prefix}detail_major_index'].distinct(
                        "DETAIL_TYPE", condition_detailtype))
            risk_data = get_major_grandson_index_data_by_major(coll_prefix,
                                                               condition_risktype,
                                                               full_grandson_index=full_grandson_index)

            if risk_data == []:
                continue
            # 为每个指数风险生成一个work_sheet
            wt = wb.add_sheet(risk_name)
            row = 0
            # 第一行填入各个指数名称
            if row == 0:
                wt.write(0, 0, '序号')
                wt.write(0, 1, '站段')
                col_title = 2
                for main_type in full_grandson_index:
                    for detail_type in full_grandson_index[main_type]:
                        if detail_type == 0:
                            wt.write(0, col_title, get_index_title(main_type))
                        else:
                            wt.write(0, col_title,
                                     get_major_child_index_name(detail_type, main_type))
                        col_title += 1
                wt.write(0, col_title, '总指数')
            for dep_name in risk_data:
                col_idx = 2
                wt.write(row + 1, 0, row + 1)
                wt.write(row + 1, 1, dep_name)
                for child_index_scores in risk_data[dep_name][:-1]:
                    for child_score in child_index_scores:
                        wt.write(row + 1, col_idx, child_score[1])
                        col_idx += 1
                wt.write(row + 1, col_idx, risk_data[dep_name][-1][1])
                row += 1
        wb.save(file_path)
        return True
    return False


def export_major_index_word(department_name, param_dict, file_path):
    mon = int(param_dict['MONTH'])
    major = param_dict['MAJOR']
    risk_type = int(param_dict['RISK_TYPE'])
    if mon < 201901:
        return '2019年以前指数不生成报告'
    dpid = param_dict['DPID']
    document = Document()
    document.add_heading(f'{department_name}－指数报告（{mon // 100}年{mon % 100}月）', 0)
    # 往文档中添加段落
    coll_prefix = choose_collection_prefix(mon)
    radar_labels = list(mongo.db[f"{coll_prefix}detail_major_index"].find({"DETAIL_TYPE": 0, "MON": mon,
                                                                           "DEPARTMENT_ID": dpid,
                                                                           "TYPE": risk_type,
                                                                           "MAJOR": major,
                                                                           "DEPARTMENT_NAME": department_name},
                                                                          {'_id': 0, "MAIN_TYPE": 1, "SCORE": 1}))
    if radar_labels:
        # 添加雷达图
        labels = [get_index_title(label['MAIN_TYPE']) for label in radar_labels]
        data = []
        subtags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
        data.append([score['SCORE'] for score in radar_labels])
        avg_data = {}
        most_data = {}
        for main_type in [label['MAIN_TYPE'] for label in radar_labels]:
            data_tmp = list(mongo.db[f'{coll_prefix}detail_major_index'].find(
                {"MON": mon, "MAIN_TYPE": main_type, "DETAIL_TYPE": 0, "MAJOR": major, "TYPE": risk_type},
                {"_id": 0, "SCORE": 1, "RANK": 1}))

            most_data[main_type] = [i['SCORE'] for i in data_tmp if i['RANK'] == 1][0]
            avg_data[main_type] = sum([i['SCORE'] for i in data_tmp]) / len(data_tmp)
        data.append([most_data[label['MAIN_TYPE']] for label in radar_labels])
        data.append([avg_data[label['MAIN_TYPE']] for label in radar_labels])
        risk_name = get_risk_type_name(major, risk_type)
        pic_path = plot_radar([get_index_title(label['MAIN_TYPE'])[:-2] for label in radar_labels],
                              data, f'{department_name}{risk_name}', subtags)
        document.add_picture(pic_path, width=Inches(6.0))
        # 添加描述表格
        conlunm_lenth = len(labels)
        table = document.add_table(rows=4, cols=conlunm_lenth + 1, style='Table Grid')
        # 第一行所有的方格
        for r in range(4):
            hdr_cells = table.rows[r].cells
            if r == 0:
                for p in range(1, conlunm_lenth + 1):
                    hdr_cells[p].add_paragraph(labels[p - 1])
            else:
                for p in range(conlunm_lenth + 1):
                    if p == 0:
                        hdr_cells[p].add_paragraph(subtags[r - 1])
                    else:
                        hdr_cells[p].add_paragraph(str(round(data[r - 1][p - 1], 2)))

    main_type_list = list(mongo.db[f'{coll_prefix}detail_major_index'].distinct("MAIN_TYPE",
                                                                                {"MON": mon, "MAJOR": major,
                                                                                 "TYPE": risk_type}))
    main_type_list.sort()
    for main_type_idx, main_type in enumerate(main_type_list):
        main_type_weight = list(mongo.db['base_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': risk_type,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            }))

        # 判断该main_type子指数是否存在
        if len(main_type_weight) < 1:
            continue
        main_type_weight = main_type_weight[0]['WEIGHT']
        index_title = '{0}. {1}({2}%)'.format(main_type_idx + 1,
                                              get_index_title(main_type),
                                              main_type_weight * 100)
        document.add_heading(index_title, level=1)

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': risk_type,
                'MAIN_TYPE': main_type,
                "MON": mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'MAJOR': major,
                    'INDEX_TYPE': risk_type,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))

        weight_table = document.add_table(rows=1, cols=2)
        heading_cells = weight_table.rows[0].cells
        heading_cells[0].text = '指数名称'
        heading_cells[1].text = '占比%'
        for item in weight_data:
            cells = weight_table.add_row().cells
            cells[0].text = get_risk_child_index_name(item['DETAIL_TYPE'],
                                                      main_type)
            cells[1].text = str(int((item['WEIGHT'] * 100))).replace(
                '-100', '扣分')
        prefix = choose_collection_prefix(mon)
        basic_data = list(mongo.db[f'{prefix}major_index_basic_data'].find({
            'DEPARTMENT_ID':
                dpid,
            'MON':
                mon,
            'MAIN_TYPE':
                main_type,
            'MAJOR':
                major,
            'INDEX_TYPE':
                risk_type,
        }))
        # print("basic_data: ", basic_data)
        if len(basic_data) < 1:
            current_app.logger.info(
                f'Error: MAIN_TYPE-{main_type}, MONTH-{mon}\
                can not find major_index_basic_data')
            continue

        basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
        for idx, item in enumerate(weight_data):
            idx_name = get_risk_child_index_name(item["DETAIL_TYPE"],
                                                 main_type)
            document.add_heading(f'{main_type_idx + 1}.{idx + 1} {idx_name}')
            if item['DETAIL_TYPE'] not in basic_data_dict:
                continue
            row = basic_data_dict.get(item['DETAIL_TYPE'])
            formula_str = get_risk_child_calc_formula(row['DETAIL_TYPE'],
                                                      row['TYPE'], main_type)
            if row['TYPE'] == 1 or row['TYPE'] == 3:
                # 处理2月份之前版本指数，没有专业平均分
                if 'AVG_QUOTIENT' not in row:
                    avg_quotient = row['AVG_SCORE']
                    avg_score = '暂无'
                else:
                    avg_quotient = row['AVG_QUOTIENT']
                    avg_score = row['AVG_SCORE']
                if row['TYPE'] == 3:
                    formula_str = row['CONTENT']
                index_paragraph = formula_str.format(
                    row['SCORE'], int(
                        row['RANK']), avg_quotient, row['QUOTIENT'],
                    row['NUMERATOR'], row['DENOMINATOR'], avg_score)
            else:
                cnt = row['CONTENT']
                if pd.isnull(cnt):
                    cnt = _get_default_title_for_risk_index(
                        main_type, row['DETAIL_TYPE'])
                try:
                    index_paragraph = formula_str.format(cnt)
                except Exception as e:
                    index_paragraph = "数据异常"
            document.add_paragraph(
                index_paragraph.replace('<p>', '').replace('</p>',
                                                           '\n').replace(
                    '<br/>', '\n'))
    # 保存文档
    document.save(file_path)
    return True


def export_major_index_cardinal_number_word(department_name, param_dict, file_path):
    """[summary]
    下载生成重点指数基数分析报告
    Arguments:
        department_name {[type]} -- [description]
        param_dict {[type]} -- [description]
        file_path {[type]} -- [description]
    
    Returns:
        [type] -- [description]
    """
    from app.data.major_risk_index.common.cardinal_number_common import (
        calc_fitting_cardinal_number_by_formula)
    mon = int(param_dict['MONTH'])
    history_three_months = [mon]
    for i in range(3):
        # 取最后一个月，计算它的前一个月，并插入到队尾
        date_string = str(history_three_months[-1])
        d_time = datetime.datetime.strptime(
            date_string + '25', "%Y%m%d")
        days_num = calendar.monthrange(d_time.year, d_time.month)[1]  # 获取一个月有多少天
        # print(days_num)
        d_time = d_time - datetime.timedelta(days=days_num)
        history_three_months.append(int(str(d_time).replace('-', '')[:6]))
    major = param_dict['MAJOR']
    risk_type = int(param_dict['RISK_TYPE'])
    # 针对特殊指数，基数报告替换成目标指数基数
    # 例如工电劳安指数工务基数 ->工务劳安基数
    static_major_index = {

    }
    major, risk_type = static_major_index.get(major + '-' + str(risk_type), (major, risk_type))
    if mon < 201909:
        return '2019年10月以前指数不生成报告'
    dpid = param_dict['DPID']
    document = Document()
    document.add_heading(f'{department_name}－基数分析报告（{mon // 100}年{mon % 100}月）', 0)
    # 往文档中添加段落
    coll_prefix = choose_collection_prefix(mon)
    # 各月基数选择的部门及故障率信息
    for history_mon in history_three_months[1:]:
        prefix = choose_collection_prefix(history_mon)
        cardinal_number_dp_info = list(
            mongo.db[f'{prefix}major_index_cardinal_number_basic_data'].find(
                {"MON": history_mon,
                 'MAJOR': major, 'INDEX_TYPE': risk_type,
                 "ACCIDENT_COUNT": {'$exists': 'true'}
                 }, {'_id': 0}
            ))
        document.add_paragraph(
            f"基数选择月份: {cardinal_number_dp_info[0]['MON']} \n" + \
            f"事故数: {cardinal_number_dp_info[0]['ACCIDENT_COUNT']} \n" + \
            f"故障数: {cardinal_number_dp_info[0]['TROUBLE_COUNT']} \n" + \
            f"涉及部门: {cardinal_number_dp_info[0]['BASE_UNIT_NAME']}")
    main_type_list = list(mongo.db[f'{coll_prefix}detail_major_index'].distinct("MAIN_TYPE",
                                                                                {"MON": mon, "MAJOR": major,
                                                                                 "TYPE": risk_type}))
    main_type_list.sort()
    for main_type in main_type_list:
        main_type_weight = list(mongo.db['base_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': risk_type,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            }))

        # 判断该main_type子指数是否存在
        if len(main_type_weight) < 1:
            continue
        main_type_weight = main_type_weight[0]['WEIGHT']
        index_title = '{0}. {1}({2}%)'.format(main_type,
                                              get_index_title(main_type),
                                              main_type_weight * 100)
        document.add_heading(index_title, level=1)

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'MAJOR': major,
                'INDEX_TYPE': risk_type,
                'MAIN_TYPE': main_type,
                "MON": mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'MAJOR': major,
                    'INDEX_TYPE': risk_type,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))

        weight_table = document.add_table(rows=1, cols=2)
        heading_cells = weight_table.rows[0].cells
        heading_cells[0].text = '指数名称'
        heading_cells[1].text = '占比%'
        for item in weight_data:
            cells = weight_table.add_row().cells
            cells[0].text = get_risk_child_index_name(item['DETAIL_TYPE'],
                                                      main_type)
            cells[1].text = str(int((item['WEIGHT'] * 100))).replace(
                '-100', '扣分')
        basic_data = list(mongo.db[f'{coll_prefix}major_index_basic_data'].find({
            'DEPARTMENT_ID':
                dpid,
            'MON':
                mon,
            'MAIN_TYPE':
                main_type,
            'MAJOR':
                major,
            'INDEX_TYPE':
                risk_type,
        }))
        # print("basic_data: ", basic_data)
        if len(basic_data) < 1:
            current_app.logger.info(
                f'Error: MAIN_TYPE-{main_type}, MONTH-{mon}\
                can not find major_index_basic_data')
            continue

        basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
        for idx, item in enumerate(weight_data):
            idx_name = get_risk_child_index_name(item["DETAIL_TYPE"],
                                                 main_type)
            document.add_heading(f'{main_type}.{idx + 1} {idx_name}')
            if item['DETAIL_TYPE'] not in basic_data_dict:
                continue
            cardinal_number = []
            for i, history_mon in enumerate(history_three_months):
                # 因为历史月份的prefix并不是不变的。使用$in一次取多个月份有局限
                # "CARDINAL_NUMBER": {"$exists": 'true'}
                prefix = choose_collection_prefix(history_mon)
                item_cardinal = list(
                    mongo.db[f'{prefix}major_index_cardinal_number_basic_data'].find(
                        {"MON": history_mon, 'MAIN_TYPE': main_type,
                         'MAJOR': major, 'INDEX_TYPE': risk_type,
                         "DETAIL_TYPE": item['DETAIL_TYPE'],
                         }, {'_id': 0, "CARDINAL_NUMBER": 1, "CONTENT": 1, "MON": 1,
                             "DENOMINATOR_NAME": 1, "DENOMINATOR_VERSION": 1,
                             "NUMERATOR_NAME": 1, "NUMERATOR_VERSION": 1}
                    ))
                if i > 0:
                    cardinal_number.extend(
                        [item for item in item_cardinal if item.get('CARDINAL_NUMBER', None)])
                else:
                    # 获取本月数据
                    this_cardinal = item_cardinal
            keys = []
            # 过滤 cardinal_number
            cardinal_number = _filter_cardinal_number(cardinal_number, this_cardinal)
            for item in cardinal_number:
                _key = (item['DENOMINATOR_NAME'],
                        item['DENOMINATOR_VERSION'], item['NUMERATOR_NAME'],
                        item['NUMERATOR_VERSION'])
                if _key not in keys:
                    keys.append(_key)
            for key in keys:
                tmp_cardinal_number = [item for item in cardinal_number if ((item['DENOMINATOR_NAME'],
                                                                             item['DENOMINATOR_VERSION'],
                                                                             item['NUMERATOR_NAME'],
                                                                             item['NUMERATOR_VERSION']) == key)]
                tmp_cardinal_number = calc_fitting_cardinal_number_by_formula(
                    tmp_cardinal_number)
                document.add_paragraph(
                    f"基数选择月份: {tmp_cardinal_number['MON']} \n" + \
                    f"基数值: {tmp_cardinal_number['CARDINAL_NUMBER']} \n" + \
                    f"过程: {tmp_cardinal_number['CONTENT']}")
    # 保存文档
    document.save(file_path)
    return True


def export_health_index_cardinal_number_word(department_name, param_dict, file_path):
    from app.data.major_risk_index.common.cardinal_number_common import (
        calc_fitting_cardinal_number_by_formula)
    mon = int(param_dict['MONTH'])
    history_three_months = [mon]
    for _ in range(3):
        # 取最后一个月，计算它的前一个月，并插入到队尾
        date_string = str(history_three_months[-1])
        d_time = datetime.datetime.strptime(
            date_string + '25', "%Y%m%d")
        days_num = calendar.monthrange(d_time.year, d_time.month)[1]  # 获取一个月有多少天
        d_time = d_time - datetime.timedelta(days=days_num)
        history_three_months.append(int(str(d_time).replace('-', '')[:6]))
    major = param_dict['MAJOR']
    if mon < 201909:
        return '2019年10月以前指数不生成报告'
    dpid = param_dict['DPID']
    document = Document()
    document.add_heading(f'{department_name}－基数分析报告（{mon // 100}年{mon % 100}月）', 0)
    # 往文档中添加段落
    coll_prefix = choose_collection_prefix(mon)
    # 各月基数选择的部门及故障率信息
    for history_mon in history_three_months[1:]:
        prefix = choose_collection_prefix(history_mon)
        cardinal_number_dp_info = list(
            mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
                {"MON": history_mon,
                 'MAJOR': major, 'INDEX_TYPE': 0,
                 "ACCIDENT_COUNT": {'$exists': 'true'}
                 }, {'_id': 0}
            ))
        document.add_paragraph(
            f"基数选择月份: {cardinal_number_dp_info[0]['MON']} \n" + \
            f"事故数: {cardinal_number_dp_info[0]['ACCIDENT_COUNT']} \n" + \
            f"故障数: {cardinal_number_dp_info[0]['TROUBLE_COUNT']} \n" + \
            f"涉及部门: {cardinal_number_dp_info[0]['BASE_UNIT_NAME']}")
    main_type_list = list(mongo.db[f'{coll_prefix}detail_health_index'].distinct("MAIN_TYPE",
                                                                                 {"MON": mon, "MAJOR": major}))
    main_type_list.sort()
    for main_type in main_type_list:
        main_type_weight = list(mongo.db['base_index_weight'].find(
            {
                'INDEX_TYPE': 0,
                'MAIN_TYPE': main_type
            }, {
                '_id': 0,
                'WEIGHT': 1
            }))

        # 判断该main_type子指数是否存在
        if len(main_type_weight) < 1:
            continue
        main_type_weight = main_type_weight[0]['WEIGHT']
        index_title = '{0}. {1}({2}%)'.format(main_type,
                                              get_index_title(main_type),
                                              main_type_weight * 100)
        document.add_heading(index_title, level=1)

        weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
            {
                'INDEX_TYPE': 0,
                'MAIN_TYPE': main_type,
                "MON": mon
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
        if len(weight_data) == 0:
            weight_data = list(mongo.db['base_detail_index_weight'].find(
                {
                    'INDEX_TYPE': 0,
                    'MAIN_TYPE': main_type
                }, {
                    '_id': 0,
                    'WEIGHT': 1,
                    'DETAIL_TYPE': 1
                }))

        weight_table = document.add_table(rows=1, cols=2)
        heading_cells = weight_table.rows[0].cells
        heading_cells[0].text = '指数名称'
        heading_cells[1].text = '占比%'
        for item in weight_data:
            cells = weight_table.add_row().cells
            cells[0].text = get_child_index_name(item['DETAIL_TYPE'],
                                                 main_type)
            cells[1].text = str(int((item['WEIGHT'] * 100))).replace(
                '-100', '扣分')
        basic_data = list(mongo.db[f'{coll_prefix}health_index_basic_data'].find({
            'DEPARTMENT_ID':
                dpid,
            'MON':
                mon,
            'MAIN_TYPE':
                main_type,
            'MAJOR':
                major,
            'INDEX_TYPE':
                0,
        }))
        # print("basic_data: ", basic_data)
        if len(basic_data) < 1:
            current_app.logger.info(
                f'Error: MAIN_TYPE-{main_type}, MONTH-{mon}\
                can not find major_index_basic_data')
            continue

        basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
        for idx, item in enumerate(weight_data):
            idx_name = get_child_index_name(item["DETAIL_TYPE"],
                                            main_type)
            document.add_heading(f'{main_type}.{idx + 1} {idx_name}')
            if item['DETAIL_TYPE'] not in basic_data_dict:
                continue
            cardinal_number = []
            for i, history_mon in enumerate(history_three_months):
                # 因为历史月份的prefix并不是不变的。使用$in一次取多个月份有局限
                # 获取当月的基数计算内容，以它为蓝本过滤过去三个月不合适的基数版本
                # "CARDINAL_NUMBER": {"$exists": 'true'}
                prefix = choose_collection_prefix(history_mon)
                item_cardinal = list(
                        mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
                            {"MON": history_mon, 'MAIN_TYPE': main_type,
                            'MAJOR': major, 'INDEX_TYPE': 0,
                            "DETAIL_TYPE": item['DETAIL_TYPE'],
                            }, {'_id': 0, "CARDINAL_NUMBER": 1, "CONTENT": 1, "MON": 1,
                                "DENOMINATOR_NAME": 1, "DENOMINATOR_VERSION": 1,
                                "NUMERATOR_NAME": 1, "NUMERATOR_VERSION": 1}
                        ))
                if i > 0:
                    # 筛选有CARDINAL_NUMBER的属性字典
                    cardinal_number.extend(
                        [item for item in item_cardinal if item.get('CARDINAL_NUMBER', None)])
                else:
                    # 获取本月数据
                    this_cardinal = item_cardinal
            keys = []
            # 过滤 cardinal_number
            cardinal_number = _filter_cardinal_number(cardinal_number, this_cardinal)
            for item in cardinal_number:
                _key = (item['DENOMINATOR_NAME'],
                        item['DENOMINATOR_VERSION'], item['NUMERATOR_NAME'],
                        item['NUMERATOR_VERSION'])
                if _key not in keys:
                    keys.append(_key)
            for key in keys:
                tmp_cardinal_number = [item for item in cardinal_number if ((item['DENOMINATOR_NAME'],
                                                                             item['DENOMINATOR_VERSION'],
                                                                             item['NUMERATOR_NAME'],
                                                                             item['NUMERATOR_VERSION']) == key)]
                tmp_cardinal_number = calc_fitting_cardinal_number_by_formula(
                    tmp_cardinal_number)
                document.add_paragraph(
                    f"基数选择月份: {tmp_cardinal_number['MON']} \n" + \
                    f"基数值: {tmp_cardinal_number['CARDINAL_NUMBER']} \n" + \
                    f"过程: {tmp_cardinal_number['CONTENT']}")
    # 保存文档
    document.save(file_path)
    return True


def _filter_cardinal_number(cardinal_number_prv, this_cardinal_number):
    """[summary]
    根据这个月的基数模版过滤过去三个月不合适的基数
    以数据项名称，版本为依据
    Arguments:
        cardinal_number_prv {[type]} -- [description]
        this_cardinal_number {[type]} -- [description]
    """
    rst = []
    for prv in cardinal_number_prv:
        prv_key = (prv['DENOMINATOR_NAME'],
                    prv['DENOMINATOR_VERSION'], 
                    prv['NUMERATOR_NAME'],
                    prv['NUMERATOR_VERSION'])
        for this in this_cardinal_number:
            this_key = (this['DENOMINATOR_NAME'],
                    this['DENOMINATOR_VERSION'], 
                    this['NUMERATOR_NAME'],
                    this['NUMERATOR_VERSION'])
            if prv_key == this_key:
                rst.append(prv)
    return rst
