from app.data import (check_info, check_problem, data_migration, department,
                      diaoche_index, evaluate, forgery_data, health_index,
                      major_risk_index, railway_map, safety_produce_info,
                      ai_health_index)
from app.data import workshop_health_index
from app.data.big_screen_data import (
    analysis_center_assess, base_problem, check_address, key_info_tracking,
    key_person, person, quantify_assess_real, quantization_refinement,
    safety_days, safety_work_supervise, system_notify, typical_accident,
    warn_notification)
from app.data.report import (analysis_report, evaluate_report_new,
                             safety_daily)
from app.new_big.all_data import count_inspect_check_data
from app.data import control_intensity_index

risks = [
    "9-供电-1",  # 设备质量风险
    "11-供电-2",  # 规章制度风险
    "5-供电-3",  # 专业管理风险
    "10-供电-4",  # 接触网专业管理风险
    "36-供电-5",  # 职工素质
    "7-供电-8",  # 自轮设备安全风险
    "6-供电-9",  # 自轮设备运用风险
    "23-电务-1",  # 电务普铁信号设备质量风险分析
    "26-电务-3",  # 电务高铁通信设备质量风险分析
    "25-电务-4",  # 电务高铁信号设备质量风险分析
    "31-车务-3",  # 车门管理
    "32-车务-4",  # 客运组织
    "16-车务-5",  # 多方向接发列车
    "17-车务-7",  # 高动客接发列车
    "13-机务-1",  # 间断瞭望
    "21-机务-3",  # 漏检漏修、简化修风险
    "20-机务-4",  # 错误操纵风险
    "22-机务-6",  # 机车质量风险
    "43-车辆-3",  # 客车制动抱闸
    "44-车辆-4",  # 货车制动抱闸
    "39-车辆-5",  # 高压牵引
    "45-车辆-6",  # 车辆脱轨
    "46-车辆-7",  # 列车分离
    "37-车辆-8",  # 货车配件脱落
    "38-车辆-9",  # 动车配件脱落
    "42-车辆-10",  # 客车配件脱落
    "41-车辆-11",  # 调车防溜
    "27-工务-3",  # 点外修作业风险
    "29-工务-4",  # 防洪风险
    "28-工务-5",  # 自轮设备风险
]

funcs1 = {
    'default': (
        check_info.execute,
        check_problem.execute,
        safety_daily.execute,
        evaluate.execute,
        safety_produce_info.execute,
        typical_accident.execute,
        warn_notification.execute,
        key_person.execute,
        safety_work_supervise.execute,
        system_notify.execute,
        analysis_center_assess.execute,
        quantization_refinement.execute,
        quantify_assess_real.execute,
        key_info_tracking.execute,
    ),
    'daily': (
        department.execute,
        forgery_data.execute,
        base_problem.execute,
        check_address.execute,
        safety_days.execute,
        person.execute,
    ),
    'index': 10,
}

funcs2 = {
    'update_day': (
        health_index.check_intensity.execute,
        health_index.evaluate_intensity.execute,
        health_index.check_evenness.execute,
        health_index.assess_intensity.execute,
        health_index.problem_exposure.execute,
        diaoche_index.check_intensity.execute,
        diaoche_index.evaluate_intensity.execute,
        diaoche_index.check_evenness.execute,
        diaoche_index.assess_intensity.execute,
        diaoche_index.problem_exposure.execute,
        ai_health_index.check_intensity.execute,
        ai_health_index.evaluate_intensity.execute,
        ai_health_index.check_evenness.execute,
        ai_health_index.assess_intensity.execute,
        ai_health_index.problem_exposure.execute,
    ),
    'index': 20,
}

funcs_problem_rectification = {
    # 确保问题整改能获取问题暴露数据
    'update_day': (
        health_index.problem_rectification.execute,
        ai_health_index.problem_rectification.execute,
        diaoche_index.problem_rectification.execute,
    ),
    'index': 25,
}

funcs3 = {
    'update_day': (
        health_index.health_index.execute,
        diaoche_index.diaoche_index.execute,
        ai_health_index.health_index.execute,
    ),
    'index': 30,
}
funcs4 = {
    'default': (railway_map.execute,),
    'index': 40,
}
index_funcs = {
    'update_day': (major_risk_index.main.execute,),
    'params': risks,
    'index': 50,
}
funcs6 = {
    'update_day': (evaluate_report_new.execute,),
    'index': 60,
}
funcs7 = {
    'default': (analysis_report.execute,),
    'index': 70,
}

inspect_check_funcs = {
    # 更新监督检查信息
    'daily': (count_inspect_check_data,),
    'index': 75
}

funcs8 = {
    'update_day': (
        # 车务
        major_risk_index.chewu_laoan.combine_child_index.execute,
        major_risk_index.chewu_jiefalc.combine_child_index.execute,
        major_risk_index.chewu_gaodongkejflc.combine_child_index.execute,
        major_risk_index.chewu_huozhuangaq.combine_child_index.execute,
        major_risk_index.chewu_diaochengyth.combine_child_index.execute,

        # 供电
        major_risk_index.gongdian_laoan.combine_child_index.execute,
        major_risk_index.gongdian_shigongaq.combine_child_index.execute,
        major_risk_index.gongdian_shebeizl.combine_child_index.execute,
        major_risk_index.gongdian_zhuanyegl.combine_child_index.execute,
        major_risk_index.gongdian_zlsb.combine_child_index.execute,
        major_risk_index.gongdian_shigongjg.combine_child_index.execute,
        major_risk_index.gd_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.gd_gongdian_shigongaq_single.combine_child_index.execute,

        # 电务
        major_risk_index.dianwu_laoan.combine_child_index.execute,
        major_risk_index.dianwu_shigongaq.combine_child_index.execute,
        major_risk_index.dw_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.dw_gongdian_shigongaq_single.combine_child_index.execute,
        major_risk_index.dianwu_daochasb.combine_child_index.execute,
        major_risk_index.dianwu_dianwaixiu.combine_child_index.execute,
        major_risk_index.dianwu_guidaodl.combine_child_index.execute,
        major_risk_index.dianwu_xinhaoji.combine_child_index.execute,

        # 工务
        major_risk_index.gongwu_laoan.combine_child_index.execute,
        major_risk_index.gongwu_fangduanfx.combine_child_index.execute,
        major_risk_index.gongwu_dianwaixiu.combine_child_index.execute,
        major_risk_index.gongwu_fanghong.combine_child_index.execute,
        major_risk_index.gongwu_zlsb.combine_child_index.execute,
        major_risk_index.gongwu_shigongaq.combine_child_index.execute,
        major_risk_index.gongwu_zhanzhuanxianfx.combine_child_index.execute,
        major_risk_index.gongwu_shigongjg.combine_child_index.execute,
        major_risk_index.gongwu_shigongtianchuangxiu.combine_child_index.execute,

        # 机务
        major_risk_index.jiwu_laoan.combine_child_index.execute,
        major_risk_index.jiwu_cuowucz.combine_child_index.execute,
        major_risk_index.jiwu_jichezl.combine_child_index.execute,
        major_risk_index.jiwu_jianduanlw.combine_child_index.execute,
        major_risk_index.jiwu_diaoche.combine_child_index.execute,

        # 车辆
        major_risk_index.cheliang_laoan.combine_child_index.execute,
        major_risk_index.cheliang_huozaibz.combine_child_index.execute,
        major_risk_index.cheliang_dongchegyqy.combine_child_index.execute,
        major_risk_index.cheliang_dongchepjtl.combine_child_index.execute,
        major_risk_index.cheliang_huochepjtl.combine_child_index.execute,
        major_risk_index.cheliang_kechepjtl.combine_child_index.execute,

        # 工电
        major_risk_index.gw_gongdian_laoan.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongaq.combine_child_index.execute,
        major_risk_index.gw_gongdian_dianwaixiu.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongaq_single.combine_child_index.execute,
        major_risk_index.gw_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongtianchuangxiu.combine_child_index.execute,

        # 客运
        major_risk_index.cw_keyun_laoan.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_cwz.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_cz.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_kyd.combine_child_index.execute,
    ),
    'index': 80,
}

chewu_funcs = {
    'update_day': (
        # 车务
        major_risk_index.chewu_laoan.combine_child_index.execute,
        major_risk_index.chewu_jiefalc.combine_child_index.execute,
        major_risk_index.chewu_gaodongkejflc.combine_child_index.execute,
        major_risk_index.chewu_huozhuangaq.combine_child_index.execute,
        major_risk_index.chewu_diaochengyth.combine_child_index.execute,
    ),
    'index': 90,
}

gongdian_funcs = {
    'update_day': (
        # 供电
        major_risk_index.gongdian_laoan.combine_child_index.execute,
        major_risk_index.gongdian_shigongaq.combine_child_index.execute,
        major_risk_index.gongdian_shebeizl.combine_child_index.execute,
        major_risk_index.gongdian_zhuanyegl.combine_child_index.execute,
        major_risk_index.gongdian_zlsb.combine_child_index.execute,
        major_risk_index.gongdian_shigongjg.combine_child_index.execute,
        major_risk_index.gd_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.gd_gongdian_shigongaq_single.combine_child_index.execute,
    ),
    'index': 100,
}

dianwu_funcs = {
    'update_day': (
        # 电务
        major_risk_index.dianwu_laoan.combine_child_index.execute,
        major_risk_index.dianwu_shigongaq.combine_child_index.execute,
        major_risk_index.dw_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.dw_gongdian_shigongaq_single.combine_child_index.execute,
        major_risk_index.dianwu_daochasb.combine_child_index.execute,
        major_risk_index.dianwu_dianwaixiu.combine_child_index.execute,
        major_risk_index.dianwu_guidaodl.combine_child_index.execute,
        major_risk_index.dianwu_xinhaoji.combine_child_index.execute,
    ),
    'index': 110,
}

gongwu_funcs = {
    'update_day': (
        # 工务
        major_risk_index.gongwu_laoan.combine_child_index.execute,
        major_risk_index.gongwu_fangduanfx.combine_child_index.execute,
        major_risk_index.gongwu_dianwaixiu.combine_child_index.execute,
        major_risk_index.gongwu_fanghong.combine_child_index.execute,
        major_risk_index.gongwu_zlsb.combine_child_index.execute,
        major_risk_index.gongwu_shigongaq.combine_child_index.execute,
        major_risk_index.gongwu_zhanzhuanxianfx.combine_child_index.execute,
        major_risk_index.gongwu_shigongjg.combine_child_index.execute,
        major_risk_index.gongwu_shigongtianchuangxiu.combine_child_index.execute,
    ),
    'index': 120,
}

jiwu_funcs = {
    'update_day': (
        # 机务
        major_risk_index.jiwu_laoan.combine_child_index.execute,
        major_risk_index.jiwu_cuowucz.combine_child_index.execute,
        major_risk_index.jiwu_jichezl.combine_child_index.execute,
        major_risk_index.jiwu_jianduanlw.combine_child_index.execute,
        major_risk_index.jiwu_diaoche.combine_child_index.execute,
        major_risk_index.jiwu_kuneiqc.combine_child_index.execute,
    ),
    'index': 130,
}

cheliang_funcs = {
    'update_day': (
        # 车辆
        major_risk_index.cheliang_laoan.combine_child_index.execute,
        major_risk_index.cheliang_huozaibz.combine_child_index.execute,
        major_risk_index.cheliang_dongchegyqy.combine_child_index.execute,
        major_risk_index.cheliang_dongchepjtl.combine_child_index.execute,
        major_risk_index.cheliang_huochepjtl.combine_child_index.execute,
        major_risk_index.cheliang_kechepjtl.combine_child_index.execute,
        major_risk_index.cheliang_diaochefl.combine_child_index.execute,

    ),
    'index': 140,
}

gw_gongdian_funcs = {
    'update_day': (
        # 工电
        major_risk_index.gw_gongdian_laoan.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongaq.combine_child_index.execute,
        major_risk_index.gw_gongdian_dianwaixiu.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongaq_single.combine_child_index.execute,
        major_risk_index.gw_gongdian_laoan_single.combine_child_index.execute,
        major_risk_index.gw_gongdian_shigongtianchuangxiu.combine_child_index.execute,
    ),
    'index': 150,
}

keyun_funcs = {
    'update_day': (
        # 客运
        major_risk_index.cw_keyun_laoan.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_cwz.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_cz.combine_child_index.execute,
        major_risk_index.keyun_zuzhixf_kyd.combine_child_index.execute,
    ),
    'index': 160,
}

migrate_funcs = {
    'daily': (data_migration.execute,),
    'index': 999
}

cache_funcs = {
    'update_day': (workshop_health_index.cache.cache.run_cache_timed_task,),
    'index': 10
}

workshop_index_funcs = {
    'update_day': (
        workshop_health_index.jiwu.health_index.execute,
        workshop_health_index.cheliang.health_index.execute,
        workshop_health_index.gongdian.health_index.execute,
        workshop_health_index.dianwu.health_index.execute,
        workshop_health_index.keyun.health_index.execute,
        workshop_health_index.chewu.health_index.execute,
        workshop_health_index.gongwu.health_index.execute),
    'index': 200,
}

control_intensity_funcs = {
    'update_day': (
        control_intensity_index.item_check_cycle.execute,
        control_intensity_index.control_frequency.execute,
        control_intensity_index.total_control_quality.execute,
        control_intensity_index.key_control_quality.execute,
        control_intensity_index.key_problem_control.execute
    ),
    'index': 1000,
}

control_intensity_combine_funcs = {
    'update_day': (
        control_intensity_index.combine_child_index.execute,),
    'index': 1010,
}

UPDATE_FUNC_MAP = {
    'normal': (funcs1, funcs6, funcs7, cache_funcs),
    'indexes': (
        funcs2, funcs_problem_rectification, funcs3, funcs4, migrate_funcs,
        chewu_funcs, gongdian_funcs, dianwu_funcs, gongwu_funcs,
        jiwu_funcs, cheliang_funcs, gw_gongdian_funcs, keyun_funcs,
        inspect_check_funcs, workshop_index_funcs,
        control_intensity_funcs, control_intensity_combine_funcs),
    # 'indexes': (
    # gongdian_funcs,
    # cheliang_funcs,
    # jiwu_funcs,
    # chewu_funcs,
    # gongwu_funcs,
    # dianwu_funcs,
    # keyun_funcs,
    # gw_gongdian_funcs,
    # ),
    # 'indexes': (funcs2, funcs_problem_rectification, funcs3, funcs4, migrate_funcs, funcs8, inspect_check_funcs),
}
