#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    This module defines some logger handler.
    date: 2018/02/28
'''

import logging
import logging.config
import os
from logging.handlers import RotatingFileHandler

import config


# def create_logger():
#     '''Create a logger according to the given settings
#     '''
#     try:
#         # _file_path = os.getcwd()
#         # if _file_path.split('/')[-1] == 'util':
#         #     _file_path = _file_path[:-5]
#         _file_path = os.path.join(BASEDIR, 'conf/global_logging.conf')
#         logging.config.fileConfig(_file_path)
#         ch = logging.getLogger('general')
#         return ch
#     except Exception as e:
#         print('init logger configuration failed')
#         print(str(e))
#
#
# # custom logger for application
# mylogger = create_logger()


def create_low_query_logger():
    log_file = os.path.join(config.LOG_DIR, 'slow_query.log')
    if not os.path.exists(config.LOG_DIR):
        os.makedirs(config.LOG_DIR)
    formatter = logging.Formatter("[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s")
    handler = RotatingFileHandler(log_file, maxBytes=1000000, backupCount=5)
    handler.setFormatter(formatter)
    handler.setLevel(logging.WARN)
    return handler
