#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import current_app, jsonify, request

from app import db, utctl
from app.data.util import pd_query
from app.sys import sys_bp
from app.sys.utils import drop_all_collections, parse_date
from app.utils.data_update_models import UpdateMonth


@sys_bp.route('/test/')
def test_db():
    # import time
    # time.sleep(2)
    try:
        # db_rs = db.get_engine(bind='db_mid').execute('SELECT 1').fetchall()
        db_rs = pd_query('SELECT * from t_department limit 10')
    except Exception as e:
        db_rs = e
    return jsonify(dict(
        mysql_uri=current_app.config.get('SQLALCHEMY_DATABASE_URI'),
        mongo_uri=current_app.config.get('MONGO_URI'),
        mysql=str(db_rs),
    ))


@sys_bp.route('/drop/', methods=['GET'])
def drop_collections():
    """
    初始化数据库之前删除历史表
    :return:
    """
    r = drop_all_collections()
    result_json = {'status': 0, 'data': r}
    return jsonify(result_json)


@sys_bp.route('/init/', methods=['POST'])
def init_db():
    form = request.form

    um = UpdateMonth(*parse_date(
        form.get('start_date'),
        form.get('end_date'),
        form.get('today', str(utctl.today)),
    ))
    f = None
    for key, value in current_app.maps.items():
        for d in value:
            if d['index'] == int(form.get('map_index')):
                f = d.get(form.get('part'))[int(form.get('func_index'))]
    if f:
        r = f(um)
    else:
        r = 'No Function'
    result_json = {'data': r}
    return jsonify(result_json)
