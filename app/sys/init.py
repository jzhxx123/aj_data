import copy
import logging
import random
from concurrent import futures
from multiprocessing import cpu_count
from typing import Dict, List
from flask import Flask, current_app

from app import utctl
from app.sys.utils import done
from app.utils.data_update_models import InitSequenceHandler, UpdateMonth

current_app: Flask


def init_compute(executor, obj: Dict):
    """ 计算控制逻辑 """
    todo = []
    tasks = []
    index = 0

    default_funcs = obj.get('default') or []
    this_funcs = obj.get('daily') or []
    update_day_funcs = obj.get('update_day') or []

    # TODO: 暂时为特殊指数设立, params取消后移除
    if 'params' in obj:
        for risk in obj['params']:
            for m in obj['months']:
                new_m = copy.deepcopy(m)
                new_m.risk = risk
                todo += ((i, new_m) for i in default_funcs)
                if new_m.is_this_month():
                    todo += ((i, new_m) for i in this_funcs)
                if new_m.is_update_day() or (not new_m.is_this_month()):
                    todo += ((i, new_m) for i in update_day_funcs)
    # 正常流程
    else:
        for m in obj['months']:
            todo += ((i, m) for i in default_funcs)
            if m.is_this_month():
                todo += ((i, m) for i in this_funcs)
            # 当月更新日或者历史月份
            if m.is_update_day() or (not m.is_this_month()):
                todo += ((i, m) for i in update_day_funcs)

    # 打乱顺序
    random.shuffle(todo)

    for t in todo:
        index += 1
        future = executor.submit(t[0], t[1])
        future.index = index
        future.arg = t[1]
        # noinspection PyProtectedMember
        future.app = current_app._get_current_object()
        future.date = t[1].year_month
        future.f = t[0]
        # 修复指数初始化函数名称只有main
        if t[1].risk:
            future.func_name = '.'.join((t[0].__module__.split('.')[-2], t[1].risk))
        else:
            future.func_name = '.'.join(t[0].__module__.split('.')[-2:])
        future.add_done_callback(done)
        tasks.append(future)
        msg = f'=== Schedule: {index:<4} Month: {future.date:<8} module: {future.func_name}'
        current_app.logger.info(msg)
    # futures.as_completed(tasks)
    # 4h
    futures.wait(tasks, timeout=14400)


def parallel_init_seqs(seq: List[Dict]):
    workers = current_app.config.get('INIT_MAX_WORKERS') or cpu_count()
    with futures.ProcessPoolExecutor(workers) as executor:
        for i in seq:
            index = i.get('index')
            init_compute(executor, i)
            current_app.logger.info(f'***  Indexes:{index:^20} done')


def execute_update(level='daily'):
    with current_app.app_context():
        if current_app.debug is True:
            current_app.logger.setLevel(logging.DEBUG)
        else:
            current_app.logger.setLevel(logging.INFO)
        if level == 'daily':
            this_months = utctl.get_daily_update_month_list()
            this_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in this_months]
            a = InitSequenceHandler(this_month_objs)
        elif level == 'init':
            this_months = utctl.get_daily_update_month_list()
            this_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in this_months]
            past_months = utctl.get_init_month_list()
            past_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in past_months]
            a = InitSequenceHandler(past_month_objs, this_month_objs)
        elif level == 'update_all':
            past_months = utctl.get_init_month_list()
            past_month_objs = [UpdateMonth(x[0], x[1], utctl.today) for x in past_months]
            a = InitSequenceHandler(past_month_objs)
        else:
            raise ValueError('LEVEL is invalid')

        current_app.logger.info('=== normal months list')
        current_app.logger.info(a.m_list)
        current_app.logger.info('=== indexes months list')
        current_app.logger.info(a.get_index_month_list('indexes'))

        parallel_init_seqs(a.sorted_seq)

        # 合并工电与工务，电务的数据
        from app.data.util import copy_special_index_to_another
        from app.safety_index.route import download_clear
        # copy_special_index_to_another(-1, '工电-5', '工务-7')
        # copy_special_index_to_another(-1, "工电-3", "工务-3")
        # copy_special_index_to_another(-1, "工电-4", "工务-8")
        # copy_special_index_to_another(-1, "工电-6", "工务-6")
        # copy_special_index_to_another(-1, "工电-7", "工务-2")
        # copy_special_index_to_another(-1, "工电-8", "电务-5")
        # copy_special_index_to_another(-1, "工电-9", "电务-6")
        # copy_special_index_to_another(-1, "工电-10", "供电-6")
        # copy_special_index_to_another(-1, "工电-11", "供电-7")
        # copy_special_index_to_another(-1, "工电-13", "供电-3")
        # copy_special_index_to_another(-1, "工电-14", "供电-9")
        # copy_special_index_to_another(-1, "工电-15", "供电-10")
        download_clear()

        current_app.logger.info('=== FINISHED ===')


def test():
    pass
