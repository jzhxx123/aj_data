#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import traceback

from flask import jsonify, request, current_app
import os
from app.data import diaoche_index, health_index, ai_health_index, major_risk_index
from app.data.major_risk_index import (cheliang_laoan, chewu_laoan,
                                       gongdian_laoan, gongdian_shebeizl,
                                       gongdian_shigongaq, gongdian_zhuanyegl,
                                       jiwu_laoan, dianwu_laoan, gongwu_laoan, gongwu_shigongaq,
                                       gongwu_dianwaixiu, gw_gongdian_laoan, cw_keyun_laoan, gw_gongdian_shigongaq,
                                       chewu_chemengl, chewu_jiefalc, chewu_duofangxiangjflc, chewu_gaodongkejflc,
                                       chewu_keyunzz, gongwu_tianchuangxiu, gongwu_shigong,
                                       dianwu_putietxzl, dianwu_putiexhzl, dianwu_gaotietxzl, dianwu_gaotiexhzl,
                                       gongwu_fangduanfx, gongwu_fanghong, gongwu_zlsb, jiwu_cuowucz, jiwu_diaoche,
                                       jiwu_jichezl, jiwu_jianduanlw, jiwu_loujianlx, cheliang_huozaibz,
                                       cheliang_cheliangtg, cheliang_diaochefd, cheliang_dongchegyqy,
                                       cheliang_dongchepjtl, cheliang_huochepjtl, cheliang_kechepjtl,
                                       cheliang_huochezdbz, cheliang_liechefenli, cheliang_kechezdbz,
                                       dianwu_shigongaq, gongdian_zlsb,
                                       gw_gongdian_dianwaixiu, gw_gongdian_tianchuangxiu, gw_gongdian_shigong,
                                       gw_gongdian_shigongaq_single, gw_gongdian_laoan_single,
                                       dw_gongdian_laoan_single, dw_gongdian_shigongaq_single, chewu_huozhuangaq,
                                       keyun_zuzhixf_cwz, keyun_zuzhixf_kyd, keyun_zuzhixf_cz,
                                       chewu_jiefalccb, gongdian_shigongjg, gongwu_shigongjg, gongwu_zhanzhuanxianfx,
                                       gd_gongdian_laoan_single, gd_gongdian_shigongaq_single, dianwu_daochasb,
                                       gw_gongdian_shigongtianchuangxiu, gongwu_shigongtianchuangxiu,
                                       gd_gongdian_zhuanyegl_single, gd_gongdian_zlsb_single,
                                       gd_gongdian_shigongjg_single, dianwu_guidaodl, dianwu_xinhaoji,
                                       dianwu_dianwaixiu, chewu_diaochengyth, jiwu_kuneiqc,
                                       cheliang_diaochefl)
from app.data.health_index import (assess_intensity, check_evenness,
                                   problem_exposure, check_intensity,
                                   evaluate_intensity, problem_rectification)

from app.data import major_risk_index as major, check_info
from app.sys import sys_bp
from app.data import safety_produce_info
import datetime
from app import mongo
from dateutil.relativedelta import relativedelta

from app.sys.init import execute_update
from app.utils.data_update_models import UpdateMonth
from app.data.util import copy_special_index_to_another


@sys_bp.before_request
def before_request():
    """
    限制145本模块的运行
    :return:
    """
    host_ip = str(request.host)
    host_ip_list = host_ip.split(':')
    if request.endpoint in current_app.view_functions:
        endpoint = current_app.view_functions[request.endpoint]
        if hasattr(endpoint, 'exclude_from_analytics') and endpoint.exclude_from_analytics:
            return
    if host_ip_list[0] in [
        # '0.0.0.0',
        '10.192.34.145'
    ]:
        return jsonify({
            'data': 'failed', 'status': 0, 'error_message': 'no right permission !'
        }
        )


def exclude_from_analytics(func):
    func.exclude_from_analytics = True
    return func


@sys_bp.route('/data/update/test_major/<int:index_type>', methods=['GET'])
def test_major(index_type=0):
    # from local_test import test_doc_pic
    # test_doc_pic()
    # 综合指数
    # check_evenness.execute(1)
    # check_intensity.execute(1)
    # evaluate_intensity.execute(1)
    # assess_intensity.execute(1)
    # problem_exposure.execute(1)
    # 工电
    cw_keyun_laoan.check_evenness.execute(-1, 18, '客运-1')
    # copy_special_index_to_another(-1, '工电-5', '工务-7')
    # copy_special_index_to_another(-1,"工电-3","工务-3")
    # copy_special_index_to_another(-1,"工电-4","工务-8")
    # copy_special_index_to_another(-1, "工电-6", "工务-6")
    # copy_special_index_to_another(-1, "工电-7", "工务-2")
    # copy_special_index_to_another(-1, "工电-8", "电务-5")
    # copy_special_index_to_another(-1, "工电-9", "电务-6")
    # copy_special_index_to_another(-1, '工电-10', '供电-6')
    # copy_special_index_to_another(-1, '工电-11', '供电-7')
    # gw_gongdian_shigongaq_single.check_intensity.execute(-1, 87, '工电-6')
    # gw_gongdian_shigongaq.assess_intensity.execute(-1, [87, 34, 91], '工电-2')
    # gongwu_shigongaq.assess_intensity.execute(-1, 87, '工务-6')
    # gw_gongdian_shigongaq.check_evenness.execute(-1, [87, 34, 91], '工电-2')
    # 车辆
    # cheliang_huozaibz.combine_child_index.execute(1)
    # cheliang_laoan.combine_child_index.execute(1)
    # cheliang_laoan.check_intensity.execute(-1,40,'车辆-2')
    # 电务
    # dianwu_laoan.combine_child_index.execute(1)
    # 机务
    # jiwu_jianduanlw.check_evenness.execute(-1, 13, "机务-1")
    # jiwu_jianduanlw.combine_child_index.execute(1)
    # jiwu_laoan.check_intensity.execute(-1, 19, '机务-5')
    # jiwu_laoan.assess_intensity.execute(-1, 19, '机务-5')
    # jiwu_laoan.problem_exposure.execute(-1, 19, '机务-5')
    # jiwu_jichezl.combine_child_index.execute(1)
    # 工务
    result_json = {'data': 'success', 'status': 0, 'error_message': ''}
    return jsonify(result_json)


@sys_bp.route('/data/update/test', methods=['GET'])
def test():
    params = request.args
    index_type = int(params['INDEX_TYPE'])
    mons = [1]
    if int(params['MONTHS']) == 2:
        mons = [21]
    else:
        mons = [i for i in range(1, int(params['MONTHS']) + 1)]
        mons = sorted(mons, reverse=True)
    health_index_func = [
        health_index.check_intensity.execute,
        health_index.assess_intensity.execute,
        health_index.problem_exposure.execute,
        health_index.check_evenness.execute,
        health_index.evaluate_intensity.execute,
        health_index.problem_rectification.execute,
        health_index.health_index.execute
    ]
    diaoche_index_func = [
        diaoche_index.check_intensity.execute,
        diaoche_index.assess_intensity.execute,
        diaoche_index.problem_exposure.execute,
        diaoche_index.check_evenness.execute,
        diaoche_index.evaluate_intensity.execute,
        diaoche_index.problem_rectification.execute,
        diaoche_index.diaoche_index.execute
    ]
    need_execute_func = []
    if index_type == 1:
        need_execute_func.extend(health_index_func)
    elif index_type == 2:
        need_execute_func.extend(diaoche_index_func)
    elif index_type == 3:
        need_execute_func.extend(health_index_func)
        need_execute_func.extend(diaoche_index_func)
    for months_ago in mons:
        for func in need_execute_func:
            func(months_ago)
    result_json = {'data': 'success', 'status': 0, 'error_message': ''}
    return jsonify(result_json)


@sys_bp.route('/data/update/check', methods=['GET'])
def update_check():
    params = request.args
    check_type = int(params['type'])
    if check_type == 1:
        t2 = datetime.date.today()
        t1 = datetime.date.today() - relativedelta(months=+1)
        mon_param = UpdateMonth(t1, t2, t2)
        safety_produce_info.execute(mon_param)
    elif check_type == 2:
        gongdian_shigongaq.combine_child_index.execute(1)
        gongdian_laoan.combine_child_index.execute(1)
    elif check_type == 3:
        dianwu_shigongaq.combine_child_index.execute(1)
    elif check_type == 4:
        gw_gongdian_laoan.combine_child_index.execute(1)
        gw_gongdian_shigongaq.combine_child_index.execute(1)
    elif check_type == 5:
        # dianwu_putietxzl.combine_child_index.execute(1)
        dianwu_putietxzl.check_intensity.execute(-1, 24, '电务-2')
    elif check_type == 6:
        chewu_chemengl.evaluate_intensity.execute(-1, 31, '车务-3')
    return 'ok'


@sys_bp.route('/data/update/test_major_two/<int:major>/<int:months_ago>', methods=['GET'])
def test_major_two(major=10, months_ago=1):
    major_map = {
        0: '供电',
        1: '车辆',
        2: '机务',
        3: '车务',
        4: '工务',
        5: '电务',
        # '路局': 6,
        # '安监': 6,
        6: "客运",
        7: '工电',
        8: '综合'
    }
    if major > 200:
        # 电务
        dianwu_dianwaixiu.plan_management.execute(-1, 147, '电务-8')
        # dianwu_shigongaq.combine_child_index.execute(1)
        # dianwu_laoan.combine_child_index.execute(months_ago)
        # chewu_jiefalc.combine_child_index.execute(months_ago)
        # chewu_gaodongkejflc.combine_child_index.execute(months_ago)
        # gongdian_laoan.check_evenness.execute(-1, 35, '供电-6')
        # execute_update()
        # chewu_jiefalc.check_intensity.execute(-1, 15, '车务-6')
        # dianwu_laoan.problem_rectification.execute(-1, 30, '电务-5')
        # dianwu_laoan.combine_child_index.execute(1)

    if major == 99:
        pass
        # copy_special_index_to_another(-1, '工电-5', '工务-7')
        # copy_special_index_to_another(-1, "工电-3", "工务-3")
        # copy_special_index_to_another(-1, "工电-4", "工务-8")
        # copy_special_index_to_another(-1, "工电-6", "工务-6")
        # copy_special_index_to_another(-1, "工电-7", "工务-2")
        # copy_special_index_to_another(-1, "工电-8", "电务-5")
        # copy_special_index_to_another(-1, "工电-9", "电务-6")
        # copy_special_index_to_another(-1, '工电-10', '供电-6')
        # copy_special_index_to_another(-1, '工电-11', '供电-7')
        # copy_special_index_to_another(-1, '工电-13', '供电-3')
        # copy_special_index_to_another(-1, '工电-14', '供电-9')
        # copy_special_index_to_another(-1, '工电-15', '供电-10')
    if major < 10:
        major = major_map.get(major)
    if major == 81:
        health_index.check_intensity.execute(months_ago)
    elif major == 82:
        health_index.evaluate_intensity.execute(months_ago)
    elif major == 83:
        health_index.assess_intensity.execute(months_ago)
    elif major == 84:
        health_index.check_evenness.execute(months_ago)
    elif major == 85:
        health_index.problem_exposure.execute(months_ago)
    elif major == 86:
        health_index.problem_rectification.execute(months_ago)
    elif major == 87:
        health_index.health_index.execute(months_ago)
        ai_health_index.health_index.execute(months_ago)
    if major == '综合':
        health_index.check_intensity.execute(months_ago)
        health_index.evaluate_intensity.execute(months_ago)
        health_index.check_evenness.execute(months_ago)
        health_index.assess_intensity.execute(months_ago)
        health_index.problem_exposure.execute(months_ago)
        health_index.problem_rectification.execute(months_ago)
        ai_health_index.check_intensity.execute(months_ago)
        ai_health_index.evaluate_intensity.execute(months_ago)
        ai_health_index.check_evenness.execute(months_ago)
        ai_health_index.assess_intensity.execute(months_ago)
        ai_health_index.problem_exposure.execute(months_ago)
        ai_health_index.problem_rectification.execute(months_ago)

    elif major == '车务':
        diaoche_index.check_intensity.execute(months_ago)
        diaoche_index.evaluate_intensity.execute(months_ago)
        diaoche_index.check_evenness.execute(months_ago),
        diaoche_index.assess_intensity.execute(months_ago)
        diaoche_index.problem_exposure.execute(months_ago)
        diaoche_index.problem_rectification.execute(months_ago)
        diaoche_index.diaoche_index.execute(months_ago)
        chewu_laoan.combine_child_index.execute(months_ago),
        chewu_jiefalc.combine_child_index.execute(months_ago),
        chewu_gaodongkejflc.combine_child_index.execute(months_ago),
        chewu_huozhuangaq.combine_child_index.execute(months_ago),
        cheliang_diaochefl.combine_child_index.execute(months_ago)

    elif major == '供电':
        gongdian_laoan.combine_child_index.execute(months_ago),
        gongdian_shigongaq.combine_child_index.execute(months_ago),
        gongdian_shebeizl.combine_child_index.execute(months_ago),
        gongdian_zhuanyegl.combine_child_index.execute(months_ago)
        gongdian_zlsb.combine_child_index.execute(months_ago),
        gongdian_shigongjg.combine_child_index.execute(months_ago),
        gd_gongdian_laoan_single.combine_child_index.execute(months_ago),
        gd_gongdian_shigongaq_single.combine_child_index.execute(months_ago),
    # # 电务
    elif major == '电务':
        dianwu_laoan.combine_child_index.execute(months_ago),
        dianwu_shigongaq.combine_child_index.execute(months_ago),
        dw_gongdian_laoan_single.combine_child_index.execute(months_ago),
        dw_gongdian_shigongaq_single.combine_child_index.execute(months_ago),
        dianwu_daochasb.combine_child_index.execute(months_ago),
        dianwu_dianwaixiu.combine_child_index.execute(months_ago),
        dianwu_guidaodl.combine_child_index.execute(months_ago),
        dianwu_xinhaoji.combine_child_index.execute(months_ago),
    # 工务
    elif major == '工务':
        gongwu_laoan.combine_child_index.execute(months_ago),
        gongwu_fangduanfx.combine_child_index.execute(months_ago),
        gongwu_dianwaixiu.combine_child_index.execute(months_ago),
        gongwu_fanghong.combine_child_index.execute(months_ago),
        gongwu_zlsb.combine_child_index.execute(months_ago)(months_ago),
        gongwu_shigongaq.combine_child_index.execute(months_ago),
        gongwu_zhanzhuanxianfx.combine_child_index.execute(months_ago),
        gongwu_shigongjg.combine_child_index.execute(months_ago),
        gongwu_shigongtianchuangxiu.combine_child_index.execute(months_ago)
    elif major == '机务':
        jiwu_jianduanlw.combine_child_index.execute(months_ago),
        jiwu_diaoche.combine_child_index.execute(months_ago),
        jiwu_cuowucz.combine_child_index.execute(months_ago),
        jiwu_laoan.combine_child_index.execute(months_ago),
        jiwu_jichezl.combine_child_index.execute(months_ago),
        jiwu_kuneiqc.combine_child_index.execute(months_ago),
    elif major == '车辆':
        cheliang_huozaibz.combine_child_index.execute(months_ago),
        cheliang_laoan.combine_child_index.execute(months_ago),
        cheliang_dongchegyqy.combine_child_index.execute(months_ago),
        cheliang_huochepjtl.combine_child_index.execute(months_ago),
        cheliang_dongchepjtl.combine_child_index.execute(months_ago),
        cheliang_kechepjtl.combine_child_index.execute(months_ago),
        cheliang_diaochefl.combine_child_index.execute(months_ago),
    elif major == '工电':
        gw_gongdian_laoan.combine_child_index.execute(months_ago),
        gw_gongdian_shigongaq.combine_child_index.execute(months_ago),
        gw_gongdian_dianwaixiu.combine_child_index.execute(months_ago),
        gw_gongdian_shigongaq_single.combine_child_index.execute(months_ago),
        gw_gongdian_laoan_single.combine_child_index.execute(months_ago),
        gw_gongdian_shigongtianchuangxiu.combine_child_index.execute(months_ago),
    elif major == '客运':
        cw_keyun_laoan.combine_child_index.execute.execute(months_ago),
        keyun_zuzhixf_cwz.combine_child_index.execute.execute(months_ago),
        keyun_zuzhixf_cz.combine_child_index.execute.execute(months_ago),
        keyun_zuzhixf_kyd.combine_child_index.execute.execute(months_ago),
    current_app.logger.info(f'{major} update done !')
    result_json = {'data': 'success', 'status': 0, 'error_message': ''}
    return jsonify(result_json)


@sys_bp.route('/data/major/<int:major>/<int:index_type>/<int:month_ago>', methods=['GET'])
def calc_major_risk(major, month_ago, index_type=0):
    """
    计算mongo中某专业的某月的数据
    :param major: 专业代号
    :param month: 月份:201901
    :param index_type: 电务-5中的5
    """
    print(request.host)
    params = request.args
    mons = [int(month_ago)]
    if params['CALC_TYPE'] == 'list':
        mons = [i for i in range(1, int(month_ago) + 1)]
        mons = sorted(mons, reverse=True)
    elif params['CALC_TYPE'] == 'tmp_update_mode':
        # 目前这个变量只要存在就更激活临时更新（所有main_type指数更新）
        os.environ['TMP_UPDATE_MODE'] = 'ALL'
    chewu_diaoche = [
        diaoche_index.check_intensity,
        diaoche_index.assess_intensity,
        diaoche_index.problem_exposure,
        diaoche_index.check_evenness,
        diaoche_index.evaluate_intensity,
        diaoche_index.problem_rectification,
        diaoche_index.diaoche_index]
    health_index_func = [
        health_index.check_intensity,
        health_index.assess_intensity,
        health_index.problem_exposure,
        health_index.check_evenness,
        health_index.evaluate_intensity,
        health_index.problem_rectification,
        health_index.health_index,
    ]
    ai_health_index_func = [
        ai_health_index.check_intensity,
        ai_health_index.evaluate_intensity,
        ai_health_index.check_evenness,
        ai_health_index.assess_intensity,
        ai_health_index.problem_exposure,
        ai_health_index.problem_rectification,
        ai_health_index.health_index,
    ]

    major_map = {
        0: '供电',
        1: '车辆',
        2: '机务',
        3: '车务',
        4: '工务',
        5: '电务',
        6: "客运",
        7: '工电',
        8: '综合指数',
        9: '综合指数(自动版)',
    }

    risk_map = {
        "0-1": gongdian_shebeizl.combine_child_index,
        "0-3": gongdian_zhuanyegl.combine_child_index,
        "0-6": gongdian_laoan.combine_child_index,
        "0-7": gongdian_shigongaq.combine_child_index,
        "0-9": gongdian_zlsb.combine_child_index,
        "0-10": gongdian_shigongjg.combine_child_index,
        "1-1": cheliang_huozaibz.combine_child_index,
        "1-2": cheliang_laoan.combine_child_index,
        "1-5": cheliang_dongchegyqy.combine_child_index,
        "1-8": cheliang_huochepjtl.combine_child_index,
        "1-9": cheliang_dongchepjtl.combine_child_index,
        "1-10": cheliang_kechepjtl.combine_child_index,
        "1-11": cheliang_diaochefl.combine_child_index,
        "2-1": jiwu_jianduanlw.combine_child_index,
        "2-2": jiwu_diaoche.combine_child_index,
        # "2-3": jiwu_loujianlx.combine_child_index,
        "2-4": jiwu_cuowucz.combine_child_index,
        "2-5": jiwu_laoan.combine_child_index,
        "2-6": jiwu_jichezl.combine_child_index,
        "2-7": jiwu_kuneiqc.combine_child_index,
        "3-1": chewu_diaoche,
        "3-2": chewu_laoan.combine_child_index,
        "3-6": chewu_jiefalc.combine_child_index,
        "3-7": chewu_gaodongkejflc.combine_child_index,
        "3-8": chewu_huozhuangaq.combine_child_index,
        "3-10": chewu_diaochengyth.combine_child_index,
        "4-1": gongwu_fangduanfx.combine_child_index,
        "4-2": gongwu_laoan.combine_child_index,
        "4-3": gongwu_dianwaixiu.combine_child_index,
        "4-4": gongwu_fanghong.combine_child_index,
        "4-5": gongwu_zlsb.combine_child_index,
        "4-6": gongwu_shigongaq.combine_child_index,
        "4-9": gongwu_shigongjg.combine_child_index,
        "4-10": gongwu_zhanzhuanxianfx.combine_child_index,
        "4-11": gongwu_shigongtianchuangxiu.combine_child_index,
        "5-5": dianwu_laoan.combine_child_index,
        "5-6": dianwu_shigongaq.combine_child_index,
        "5-7": dianwu_daochasb.combine_child_index,
        "5-8": dianwu_dianwaixiu.combine_child_index,
        '5-9': dianwu_guidaodl.combine_child_index,
        '5-10': dianwu_xinhaoji.combine_child_index,
        '6-1': cw_keyun_laoan.combine_child_index,
        '6-2': keyun_zuzhixf_cwz.combine_child_index,
        '6-3': keyun_zuzhixf_kyd.combine_child_index,
        '6-4': keyun_zuzhixf_cz.combine_child_index,
        "7-1": gw_gongdian_laoan.combine_child_index,
        "7-2": gw_gongdian_shigongaq.combine_child_index,
        "7-3": gw_gongdian_dianwaixiu.combine_child_index,
        "7-6": gw_gongdian_shigongaq_single.combine_child_index,
        "7-7": gw_gongdian_laoan_single.combine_child_index,
        "7-8": dw_gongdian_laoan_single.combine_child_index,
        "7-9": dw_gongdian_shigongaq_single.combine_child_index,
        "7-10": gd_gongdian_laoan_single.combine_child_index,
        "7-11": gd_gongdian_shigongaq_single.combine_child_index,
        "7-12": gw_gongdian_shigongtianchuangxiu.combine_child_index,
        "7-13": gd_gongdian_zhuanyegl_single.combine_child_index,
        "7-14": gd_gongdian_zlsb_single.combine_child_index,
        "7-15": gd_gongdian_shigongjg_single.combine_child_index,
        '8-0': health_index_func,
        "8-1": health_index.check_intensity,
        "8-2": health_index.evaluate_intensity,
        "8-3": health_index.check_evenness,
        "8-4": health_index.assess_intensity,
        "8-5": health_index.problem_exposure,
        "8-6": health_index.problem_rectification,
        '8-7': health_index.health_index,
        '9-0': ai_health_index_func,
        '9-1': ai_health_index.check_intensity,
        "9-2": ai_health_index.evaluate_intensity,
        "9-3": ai_health_index.check_evenness,
        "9-4": ai_health_index.assess_intensity,
        "9-5": ai_health_index.problem_exposure,
        "9-6": ai_health_index.problem_rectification,
        '9-7': ai_health_index.health_index,
    }
    risk_type = str(major) + '-' + str(index_type)
    func = risk_map.get(risk_type)
    major_name = major_map.get(major)
    if not isinstance(func, list):
        func = [func]
    for i in func:
        for month_ago in mons:
            i.execute(month_ago)
    result_json = {'data': f'{major_name} {risk_type} success', 'status': 0, 'error_message': ''}
    # 删除临时环境变量
    if os.getenv('TMP_UPDATE_MODE'):
        del os.environ['TMP_UPDATE_MODE']
    return jsonify(result_json)


@sys_bp.route('/data/early_warning/<int:months_ago>', methods=['GET'])
def run_early_warning(months_ago):
    from app.data.early_warning.scheme import scheme
    return scheme.run_task(months_ago, ignore_day_limit=True)


@sys_bp.route('/data/init_index/<int:months_ago>/<int:major>/<int:index_type>', methods=['GET'])
def init_monthly_index_map(months_ago, major, index_type):
    from app.data.util import init_monthly_index_map
    major_map = {
        0: '供电',
        1: '车辆',
        2: '机务',
        3: '车务',
        4: '工务',
        5: '电务',
        6: "客运",
        7: '工电',
    }
    mon = int(months_ago)
    months_ago = mon if mon < 0 else 0 - mon
    risk_type = major_map.get(major) + '-' + str(index_type)
    info = init_monthly_index_map(months_ago, risk_type)
    result_json = {'data': f'{info}', 'status': 0, 'error_message': ''}
    return jsonify(result_json)


@sys_bp.route('/data/update/chejian_health/<int:major>/<int:months_ago>', methods=['GET'])
@exclude_from_analytics
def test_chejian_health(major=1, months_ago=1):
    from app.data import workshop_health_index
    if major == 1:
        workshop_health_index.jiwu.health_index.execute(months_ago)
    elif major == 2:
        workshop_health_index.cheliang.health_index.execute(months_ago)
    elif major == 3:
        workshop_health_index.gongdian.health_index.execute(months_ago)
    elif major == 4:
        workshop_health_index.keyun.health_index.execute(months_ago)
    elif major == 5:
        workshop_health_index.dianwu.health_index.execute(months_ago)
    elif major == 6:
        workshop_health_index.chewu.health_index.execute(months_ago)
    elif major == 7:
        workshop_health_index.gongwu.health_index.execute(months_ago)
    return 'ok'


@sys_bp.route('/data/update/control_intensity/<int:main_type>/<int:months_ago>', methods=['GET'])
@exclude_from_analytics
def test_control_intensity(main_type=0, months_ago=1):
    from app.data.control_intensity_index import combine_child_index, item_check_cycle, control_frequency, \
        total_control_quality, key_control_quality, key_problem_control, common_data
    if main_type == 0:
        combine_child_index.execute(months_ago, False)
    elif main_type == 1:
        common_data.init_common_data()
        item_check_cycle.execute(-months_ago)
    elif main_type == 2:
        common_data.init_common_data()
        control_frequency.execute(-months_ago)
    elif main_type == 3:
        common_data.init_common_data()
        total_control_quality.execute(-months_ago)
    elif main_type == 4:
        common_data.init_common_data()
        key_control_quality.execute(-months_ago)
    elif main_type == 5:
        common_data.init_common_data()
        key_problem_control.execute(-months_ago)
    elif main_type == 9:
        combine_child_index.execute(months_ago)
    return 'ok'


@sys_bp.errorhandler(Exception)
def error(e):
    result_json = {"status": 1, "error_message": str(e)}
    return jsonify(result_json)
