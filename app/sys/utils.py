from datetime import date

from dateutil import parser
from dateutil.relativedelta import relativedelta
from flask import current_app
from flask_pymongo import PyMongo

from app.utils.common_func import get_today


def drop_all_collections():
    _mongo = PyMongo(current_app)
    names = _mongo.db.list_collection_names()
    # names.remove('')
    r = list(map(_mongo.db.drop_collection, names))
    return r


def calc_month_delta():
    """ 计算月份差值 """

    # today = datetime.datetime.today()
    today = get_today()
    begin = date(2017, 10, 1)
    month_delta = (today.year - begin.year) * 12 + today.month - begin.month
    if today.day >= 25:
        month_delta += 1
    return month_delta


def get_date_by_month_delta(delta):
    today = get_today()
    date = today - relativedelta(months=delta)
    return date.strftime('%Y-%m')


def done(fn):
    """ 多进程并发调用返回 """
    if fn.cancelled():
        fn.app.logger.warning(
            f'Index: {fn.index:<8} Month: {fn.date:<12} name: {fn.func_name:<28} canceled'
        )
    elif fn.done():
        error = fn.exception()
        if error:
            fn.app.logger.error(
                f'!!! Index: {fn.index:<8} Month: {fn.date:<12} name: {fn.func_name:<40}'
                f'error returned: {error} !!!')
        else:
            result = fn.result()
            fn.app.logger.info(
                f'==    Index: {fn.index:<4} Month: {fn.date:<10} name: {fn.func_name:<40}'
                f'Return: {result} ===')


def parse_date(*args: str):
    return [parser.parse(s).date() for s in args]
