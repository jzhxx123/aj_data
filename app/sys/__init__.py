#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Blueprint

sys_bp = Blueprint('sys', __name__)

from . import data
from . import views
