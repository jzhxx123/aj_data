#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# @Date     : 2018-03-14 18:15
# @Author   : Bluethon (j5088794@gmail.com)
# @Link     : http://github.com/bluethon
from flask import Blueprint

admin_bp = Blueprint('admins', __name__)
