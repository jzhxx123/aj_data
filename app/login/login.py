#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    用户登录
'''

from app import mongo
import hashlib
import time


def get_hash_pwd(salt, pwd):
    word = salt + pwd
    pwd = hashlib.sha1(word.encode('utf8'))
    return pwd.hexdigest()


def login_check(user, pwd):
    salt = 'p2han0tomt1iger8'
    data = mongo.db.user.find_one({
        'USER': user,
        'PASSWORD': get_hash_pwd(salt, pwd)
    })
    if data is None:
        return None
    else:
        # 生成TOKEN
        token = get_hash_pwd(salt, str(time.time()))
        tokens = mongo.db.user.find_one({'USER': user}, {'_id': 0, 'TOKEN': 1})
        if tokens is None or tokens == {}:
            tokens = [token]
        else:
            tokens = tokens['TOKEN']
            tokens.append(token)
            if len(tokens) > 20:
                tokens = tokens[1:]
        mongo.db.user.update_one({'USER': user}, {'$set': {'TOKEN': tokens}})
        return token


def logout(TOKEN):
    document = mongo.db.user.find_one({
        'TOKEN': {
            '$elemMatch': {
                '$in': [TOKEN]
            }
        }
    }, {
        '_id': 0,
        'USER': 1
    })
    user = document['USER']
    if document is not None:
        tokens = mongo.db.user.find_one({'USER': user}, {'_id': 0, 'TOKEN': 1})
        tokens = tokens['TOKEN']
        tokens.remove(TOKEN)
        mongo.db.user.update_one({'USER': user}, {'$set': {'TOKEN': tokens}})
        return True
    else:
        return False
