#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from app import mongo
from app.utils.common_func import choose_collection_prefix


def _get_index_name(MAIN_TYPE):
    _map = {
        1: '检查力度指数',
        2: '评价力度指数',
        4: '检查均衡度指数',
        3: '考核力度指数',
        5: '问题暴露指数',
        6: '问题整改效果指数',
        7: '安全效果（扣分）',
    }
    if MAIN_TYPE in _map:
        return _map.get(MAIN_TYPE, '-')


def _get_sorted_dp(data, gongdianduan_data=None):
    if gongdianduan_data is not None:
        data.extend(
            [[i['DEPARTMENT_NAME'], i['MAIN_TYPE'], i['RANK'], i['SCORE']]
             for i in gongdianduan_data])
    data = sorted(data, key=lambda x: x[3], reverse=True)
    return [{
        'name': item[0],
        'value': item[3],
        'rank': idx + 1,
    } for idx, item in enumerate(data)]


def _extra_handle_for_gongdianduan():
    """为工电段进行额外处理，工电段的综合指数分别纳入 工务 电务 供电专业排序
    """
    pass


def get_data(param_dict):
    MONTH = int(param_dict['MONTH'])
    if MONTH < 201710:
        return 'MONTH should be > 201809'
    condition = {"MON": MONTH, "HIERARCHY": 3, "DETAIL_TYPE": 0}
    major = param_dict['MAJOR']
    if major in ['供电', '车辆', '工务', '车务', '客运', '机务', '电务', '工电']:
        condition.update({"MAJOR": major})
    else:
        return 'MAJOR - %s INVAILD' % major
    prefix = choose_collection_prefix(MONTH)
    documents = list(mongo.db[f'{prefix}detail_health_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    rst = []
    data = pd.DataFrame(documents)

    groups = data.groupby(['MAIN_TYPE'])
    for k, v in groups:
        # 将工电独立出来，不再加入['供电', '电务', '工务']
        # if major in ['供电', '电务', '工务']:
        #     condition = {
        #         "MON": MONTH,
        #         "HIERARCHY": 3,
        #         "MAIN_TYPE": k,
        #         "DETAIL_TYPE": 0,
        #         "DEPARTMENT_NAME": {
        #             "$regex": ".*工电段"
        #         }
        #     }
        #     gongdianduan_data = list(
        #         mongo.db[f'{prefix}detail_health_index'].find(
        #             condition, {
        #                 "_id": 0,
        #                 "DEPARTMENT_NAME": 1,
        #                 "MAIN_TYPE": 1,
        #                 "RANK": 1,
        #                 "SCORE": 1,
        #             }))
        #     vdata = _get_sorted_dp(v.values.tolist(), gongdianduan_data)
        # else:
        vdata = _get_sorted_dp(v.values.tolist())
        rst.append({'title': _get_index_name(k), 'major': 1, 'data': vdata})
    # 增加整体排名

    documents = list(mongo.db[f'{prefix}health_index'].find(
        {
            'MAJOR': major,
            'MON': MONTH,
            'HIERARCHY': 3,
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[detail_health_index], MONTH \
            {param_dict["MONTH"]} invalid'

    else:
        # if major in ['供电', '电务', '工务']:
        #     condition = {
        #         "MON": MONTH,
        #         "HIERARCHY": 3,
        #         "DEPARTMENT_NAME": {
        #             "$regex": ".*工电段"
        #         }
        #     }
        #     documents.extend(
        #         list(mongo.db[f'{prefix}health_index'].find(
        #             condition, {
        #                 "_id": 0,
        #                 "DEPARTMENT_NAME": 1,
        #                 "RANK": 1,
        #                 "SCORE": 1,
        #             })))
        data = sorted(documents, key=lambda x: x['SCORE'], reverse=True)
        rst.append({
            'title':
            '总指数排行',
            'major':
            0,
            'data': [{
                'name': item['DEPARTMENT_NAME'],
                'value': item['SCORE'],
                'rank': idx + 1
            } for idx, item in enumerate(data)]
        })
    return rst


if __name__ == '__main__':
    pass
