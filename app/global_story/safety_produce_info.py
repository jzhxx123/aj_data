#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

from app import mongo
from app.utils.common_func import choose_collection, get_last_year


def get_line_data(data):
    data.drop_duplicates('PK_ID', keep='first', inplace=True)
    data = data.groupby(['MON']).size()
    data.sort_index(inplace=True)

    return {
        "COUNT": [int(x) for x in data.values],
        "DATE": ['{}-{}'.format(str(x)[:4],
                                str(x)[4:]) for x in data.index]
    }


def get_accumulate_ponderance(df):
    return df['PONDERANCE_NUMBER'].sum()


def get_check_item_data(data):
    data_sum = data.groupby(by=['PROBLEM_POINT'])['PONDERANCE_NUMBER'].sum()
    data_size = data.groupby(by=['PROBLEM_POINT']).size()
    xdata = pd.concat([data_sum, data_size], axis=1, join='outer', sort=False)
    xdata.sort_index(inplace=True)
    result = []
    for index in xdata.index:
        result.append({
            'TITLE': index,
            'COUNT': int(xdata.loc[index].values[1]),
            'PONDERANCE': float(xdata.loc[index].values[0])
        })
    return result


def get_problem_classify_data(data):
    data_sum = data.groupby(
        by=['PROBLEM_CLASSIFY_NAME'])['PONDERANCE_NUMBER'].sum()
    data_size = data.groupby(by=['PROBLEM_CLASSIFY_NAME']).size()
    xdata = pd.concat([data_sum, data_size], axis=1, join='outer', sort=False)
    xdata.sort_index(inplace=True)
    result = []
    for index in xdata.index:
        result.append({
            'TITLE': index,
            'COUNT': int(xdata.loc[index].values[1]),
            'PONDERANCE': float(xdata.loc[index].values[0])
        })
    return result


def get_data(param_dict, major):
    condition = {
        "MON": {
            "$lte": int(param_dict['END_MONTH']),
            "$gte": int(param_dict['START_MONTH'])
        }
    }
    if major not in ['路局', '安监']:
        condition.update({"MAJOR": major})

    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}global_safety_produce_info'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(condition, {
                "_id": 0,
                "DATE": 0
            })))
    if len(documents) == 0:
        return {
            'range_month': get_last_year(),
            'line_data': {
                "COUNT": [],
                "DATE": []
            },
            'problem_classify_data': [],
            'check_item_data': []
        }
    data = pd.DataFrame(documents)

    # 获取折线图数据
    line_data = get_line_data(data.copy())

    # 从问题项点进行分析，统计各问题项点的个数、严重性值
    check_item_data = get_check_item_data(data.copy())

    # 从问题项点所属问题类型进行统计，统计各问题类型的问题个数、累计严重性值
    problem_classify_data = get_problem_classify_data(data.copy())

    return {
        'range_month': get_last_year(),
        'line_data': line_data,
        'problem_classify_data': problem_classify_data,
        'check_item_data': check_item_data
    }


if __name__ == '__main__':
    pass
