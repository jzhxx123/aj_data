#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from app import mongo
from app.utils.common_func import choose_collection_prefix


def _get_index_name(MAIN_TYPE):
    _map = {
        1: '检查力度指数',
        2: '评价力度指数',
        4: '检查均衡度指数',
        3: '考核力度指数',
        5: '问题暴露指数',
        6: '问题整改效果指数',
    }
    if MAIN_TYPE in _map:
        return _map.get(MAIN_TYPE, '-')


def _get_sorted_dp(data):
    data = sorted(data, key=lambda x: x[3], reverse=True)
    return [{
        'name': item[0],
        'value': item[3],
        'rank': item[2]
    } for item in data]


def get_data(param_dict):
    MONTH = int(param_dict['MONTH'])
    if MONTH < 201710:
        return 'MONTH should be bigger than 201809'
    condition = {
        "MON": MONTH,
        "HIERARCHY": 3,
        "DETAIL_TYPE": 0,
        "TYPE": int(param_dict['TYPE']),
        "MAJOR": param_dict['MAJOR']
    }
    prefix = choose_collection_prefix(MONTH)
    documents = list(mongo.db[f'{prefix}detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAIN_TYPE": 1,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}detail_major_index], \
            MONTH {param_dict["MONTH"]} invalid'
    rst = []
    data = pd.DataFrame(documents)
    groups = data.groupby(['MAIN_TYPE'])
    for k, v in groups:
        rst.append({
            'title': _get_index_name(k),
            'major': 1,
            'data': _get_sorted_dp(v.values)
        })

    # 增加整体排名
    del condition['DETAIL_TYPE']
    documents = list(mongo.db[f'{prefix}major_index'].find(
        condition, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "RANK": 1,
            "SCORE": 1,
        }))
    if len(documents) == 0:
        return f'NO DATA[{prefix}major_index], \
            MONTH {param_dict["MONTH"]} invalid'
    else:
        data = sorted(documents, key=lambda x: x['RANK'])
        rst.append({
            'title':
            '总指数排行',
            'major':
            0,
            'data': [{
                'name': item['DEPARTMENT_NAME'],
                'value': item['SCORE'],
                'rank': item['RANK']
            } for item in data]
        })
    return rst


if __name__ == '__main__':
    pass
