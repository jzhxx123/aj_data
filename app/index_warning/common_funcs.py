#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/3
Description: 
"""
import pymongo

from app import mongo
from app.data.early_warning.enums import IndexType
from app.utils.safety_index_common_func import get_index_title
from app.big_screen_analysis.major_risk_index_map import major_risk_map

major_map = {
    '供电': 0,
    '车辆': 1,
    '机务': 2,
    '车务': 3,
    '工务': 4,
    '电务': 5,
    '客运': 6,
    '工电': 7,
}


def get_current_sub_index(major, index_type, risk_type=None):
    """
    从权重配置统计某个指数当前的子指数
    :param major:
    :param index_type:
    :param risk_type:
    :return:
    """
    result = [{'main_type': '0', 'label': '总得分'}]
    coll_name = 'base_index_weight'
    if index_type == IndexType.health_index.value:
        documents = mongo.db[coll_name].find({'INDEX_TYPE': 0}, {'_id': 0}).sort([('MAIN_TYPE', pymongo.ASCENDING)])
    else:
        documents = mongo.db[coll_name].find({'INDEX_TYPE': int(risk_type), 'MAJOR': major},
                                             {'_id': 0}).sort([('MAIN_TYPE', pymongo.ASCENDING)])
    for row in documents:
        main_type = int(row['MAIN_TYPE'])
        result.append({'main_type': str(main_type), 'label': get_index_title(main_type)})
    return result


def get_index_list():
    """
    获取指数列表(综合指数和重点指数)
    :return:
    """
    result = []

    for k, v in major_map.items():
        result.append({'major': k, 'risk_type': 0, 'index_type': IndexType.health_index.value, 'label': '安全综合指数'})

    for k, v in major_risk_map.items():
        major, risk_type = k.split('-')
        result.append({'major': major, 'risk_type': int(risk_type), 'index_type': IndexType.major_index.value,
                       'label': v['name']})

    return result


def auth_verify(token):
    """
    身份认证
    :param token:
    :return:
    """
    if token:
        id_card = token.split('#')[0]
        if id_card in ('510625197612110019',  # gxb
                       '513401196310271614',
                       '511026197303046417',
                       '510625197612110019',
                       '51100219740501153X',
                       '520111198312165414',
                       '420105197302122036',
                       '510622197305220315',
                       '511137197610211413',
                       '510125198112131216',
                       '510125197409290010',
                       '513001197803090857'):
            return True
    return False
