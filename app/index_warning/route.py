#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/3
Description: 
"""
import functools

from flask import request, jsonify, Response

from app.data.early_warning.inspect.service import result_preview, get_inspect_records, get_config_record
from app.utils.common_func import wrapper_rtn_msg
from app.index_warning import index_warning_bp
from app.data.early_warning.condition.model import IndexWarningConfig
from app.index_warning import common_funcs


def auth_require(func):
    """
    身份认证，使用cookie中TOKKEN里保存的身份证
    :param func:
    :return:
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        if common_funcs.auth_verify(request.cookies.get('TOKEN')):
            return func(*args, **kwargs)
        # rs = {'data': '', 'status': 1, 'error_message': '暂无权限'}
        return Response('', status=401)
    return wrapper


@index_warning_bp.route('index_list', methods=['GET'])
@auth_require
def get_index_list():
    """
    获取指数列表(综合指数和重点指数)
    :return:
    """
    param_dict = request.args
    necessary_param = []
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = common_funcs.get_index_list()
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('sub_index', methods=['GET'])
@auth_require
def get_sub_index():
    """
    获取某个指数下的子指数
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'INDEX_TYPE', 'RISK_TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = common_funcs.get_current_sub_index(param_dict['MAJOR'], int(param_dict['INDEX_TYPE']),
                                                    param_dict['RISK_TYPE'])
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('condition_config', methods=['POST'])
@auth_require
def condition_config_add():
    """添加自定义配置
    """
    param_dict = request.json
    try:
        result = IndexWarningConfig.insert(param_dict, request)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}

    return jsonify(rs)


@index_warning_bp.route('condition_config_detail', methods=['GET'])
@auth_require
def get_condition_config_detail():
    """
    获取某条预警配置详情（用于前端配置查询页面）
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'INDEX_TYPE', 'RISK_TYPE', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = IndexWarningConfig.get_detail(param_dict['MAJOR'], param_dict['INDEX_TYPE'], param_dict['RISK_TYPE'],
                                               param_dict['DEPARTMENT_ID'])
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('condition_config', methods=['GET'])
@auth_require
def get_condition_config_record():
    """
    获取某条预警配置详情（用于前端配置修改页面）
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'INDEX_TYPE', 'RISK_TYPE', 'DEPARTMENT_ID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = get_config_record(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('inspect_result_preview', methods=['POST'])
@auth_require
def inspect_result_preview():
    """
    根据配置数据计算预警结果
    :return:
    """
    param_dict = request.json
    try:
        result = result_preview(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('inspect_execute', methods=['GET'])
@auth_require
def inspect_execute():
    """
    执行检查（稍后移除）
    :return:
    """
    param_dict = request.json
    try:
        from app.data.early_warning.inspect.service import execute_inspection
        result = execute_inspection()
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('inspect_records', methods=['GET'])
@auth_require
def inspect_records():
    """
    获取预警检查结果数据
    :return:
    """
    param_dict = request.args
    necessary_param = ['MAJOR', 'INDEX_TYPE', 'RISK_TYPE', 'MONTH']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    try:
        result = get_inspect_records(param_dict)
        rs = {'data': result, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@index_warning_bp.route('auth_verify', methods=['GET'])
def get_auth_verify_result():
    """
    确认身份状态
    :return:
    """
    try:
        result = common_funcs.auth_verify(request.cookies.get('TOKEN'))
        rs = {'data': {'authentication': result}, 'status': 0, 'error_message': ''}
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)
