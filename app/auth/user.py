"""
系统用户管理模块
"""
from app import mongo
from app.auth import utils
import base64
import hmac
import time

token_key = 'VbwxWi%v7Ixw*G*N%b4WvBd$'


def validate_access_token(token):
    """
    验证token是否合法的，也就是验证token是否被篡改。
    token, 分为用户信息以及信息摘要。对比消息摘要部分，是否被篡改。

    本方法的token解密过程，需要与class User的make_access_token的加密过程对应
    :param token:
    :return:
    """
    token_str = base64.urlsafe_b64decode(token).decode('utf-8')
    token_str_list = token_str.split(':')
    if len(token_str_list) != 2:
        return False

    digest_str = hmac.new(token_key.encode('utf-8'), token_str_list[0].encode('utf-8'), 'sha1').hexdigest()
    if digest_str != token_str_list[1]:
        return False
    else:
        return True


def make_access_token(user):
    """
    创建用于安全访问认证的token。
    该token是一个密文，主要用户访问系统api的时候进行安全认证的。
    :return:
    """
    if user.get_login_status():
        # 使用token_key对用户的信息进行哈希，得到消息摘要
        user_info = f'{user.get_id_card()}#{user.get_role_code()}#{time.time()}'
        digest_str = hmac.new(token_key.encode('utf-8'), user_info.encode('utf-8'), 'sha1').hexdigest()
        raw_token = user_info + ':' + digest_str
        b64_token = base64.urlsafe_b64encode(raw_token.encode('utf-8'))
        return b64_token.decode("utf-8")
    else:
        return None


def extract_user_info_from_token(token):
    """
    从Access Token里面提取用户的信息
    :param token: 加密后的安全访问用的token
    :return: dict类型，用户的信息。
        user_info = {
            id_card: 'xxxx',
            role_code: '1'
        ]
    """
    token_str = base64.urlsafe_b64decode(token).decode('utf-8')
    user_str = token_str.split(':')[0]
    user_info = {
        'id_card': user_str.split('#')[0],
        'role_code': user_str.split('#')[1]
    }
    return user_info


class User:
    """系统用户的类
    它包含了系统用户及相关权限信息
    """

    def __init__(self, id_card):
        # __person 对应 base_person表的记录
        self._person = None
        self._id_card = id_card
        self._login_status = False
        self._cookie_token = 'no_user'
        self._role_code = 0

    def get_id_card(self):
        return self._id_card

    def get_login_status(self):
        return self._login_status

    def login(self):
        """用户进行登陆
        登陆过程中，用户将加载用户的权限等信息

        Returns:
            [Bool] -- 如果登陆成功，则返回True，否则反馈False
        """

        '''根据身份证ID获取相应身份信息'''
        person = mongo.db['base_person'].find_one({"ID_CARD": self._id_card}, {"_id": 0})
        if not person:
            return False
        self._person = person
        major = person['MAJOR']

        # 职务代码
        job = str(person['PERSON_JOB_TYPE']) if 'PERSON_JOB_TYPE' in person.keys() else ''
        business_classfiy = str(person['BUSINESS_CLASSIFY']) if 'BUSINESS_CLASSIFY' in person.keys() else ''
        if person['NAME'] == '局领导' \
                or '安监室' in person['ALL_NAME'] \
                or '党委正职' in job \
                or '处室正职' in job \
                or '行政正职' in job \
                or "领导" in business_classfiy:
            job_type = 0  # 领导
        else:
            job_type = 1  # 职员
        # 部门的类型(TYPE字段为人员所属部门的TYPE)
        if person['TYPE'] in [1]:  # 路局
            dept_type = 0
            upper_dept = ''
            all_name = ''
        elif person['TYPE'] in [2, 3]:  # 专业
            dept_type = 1
            upper_dept = ''
            all_name = major
        elif person['TYPE'] in [4, 5]:  # 站段
            dept_type = 2
            upper_dept = person['ALL_NAME'].split('-')[0]
            all_name = f'''{major}-{person['ALL_NAME']}'''
        elif person['TYPE'] in [6, 7, 8]:  # 车间
            dept_type = 3
            upper_dept = person['ALL_NAME'].split('-')[0]
            all_name = f'''{major}-{person['ALL_NAME']}'''
        else:
            dept_type = 4  # 班组
            upper_dept = person['ALL_NAME'].split('-')[0]
            all_name = f'''{major}-{person['ALL_NAME']}'''

        permission_type = utils.get_permission_type(person)
        '''
        if permission_type == 2:
            # 判断是否'工电','客运'专业
            sp_major = common_func.get_special_major_for_station(person['STATION_ID'])
            if not sp_major:
                major = sp_major
        '''
        self.__validate_permission()
        self._cookie_token = f'{self._id_card}#{job_type}#{dept_type}#{upper_dept}#{all_name}#{person["STATION_ID"]}#' \
                             f'{person["STATION_NAME"]}#{permission_type}'
        self._login_status = True
        return True

    def make_cookie_token(self):
        """获取当前用户的token字符串。该token为明文，存储了用户的主要信息
        它用于兼容原系统的token信息的接口。该token被存储与cookie。
        Returns:
            [string] -- 当前用户实例的token字符串
        """
        return self._cookie_token

    def get_person_info(self):
        """
        获取当前系统用户所管理的人员的信息

        :return:
            [Dict] --  返回的用户为base_person表的dict
        """
        return self._person

    def get_role_code(self):
        """
        获取当前用于的角色的代码
        :return:
        """
        return self._role_code

    def __validate_permission(self):
        """
        用户校验，校验完成后，当前用户的role信息将被加载
        self.__role = x
        目前的需求(Jul2019)，用户只有一个角色。
        当前逻辑，主要基于mongodb中base_person表中的信息筛选实现，具体逻辑哦如下：
        1. 管理员角色
        2. 局领导角色, MAJOR = '局领导'
        3. 安监角色, STATION_NAME = '安全监察大队'
        4. 人事处，按指定的身份证号码
        5. 业务处，专业属于六大专业，type3(第三级的部门/站段)为XX处的
        6. 站段，专业属于六大专业，type3为某站段
        :return:
        """
        try:
            if self._person is None:
                return
            if self._id_card == '510625197612110019':
                # 官小波管理员
                self._role_code = '1'
            elif self._person['MAJOR'] == '局领导':
                self._role_code = '2'
            elif self._person['STATION_NAME'] in ['安监室', '安全监察大队']:
                # 安监室以及安全监察大队
                self._role_code = '3'
            elif self._id_card in utils.hr_leader_idcards:
                # 人事处特定领导
                self._role_code = '4'
            elif self._person['MAJOR'] in utils.majors \
                    and self._person['STATION_NAME'] in utils.group_business_sections \
                    and (self._person['JOB'].endswith('长')
                         or self._person['JOB'].endswith('主任')):
                # 归属业务处的，职位位XX长。XX主任的人员
                self._role_code = '5'
            elif self._person['MAJOR'] in utils.majors \
                    and self._person['STATION_NAME'].endswith('段') \
                    and (self._person['JOB'].endswith('长')
                         or self._person['JOB'].endswith('主任')):
                # 归属某个站[段]的，职位位XX长。XX主任的人员
                self._role_code = '6'
        except Exception as ex:
            # 避免__person中的字段为空
            pass
