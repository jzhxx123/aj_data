#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Blueprint

auth_bp = Blueprint('auth', __name__)
# CORS(api)

from . import login
