'''
鉴权功能服务(auth)中提供的通用工具方法模块
'''
import json
import requests
from flask import current_app, g, request

'''
成都铁路局用于解析用户token获取用户信息的URL
'''
EXT_AUTH_URL_DEFAULT = 'http://10.192.4.169/SSOauth/openapi/client/getUrlInfo'


def get_user_idcard(uid):
    """
    获取用户身份证号。
    通过调用路局的中心认证服务器，获取用户的ID Card信息。
    params:
            uid: person uuid
    return 用户身份层级
    """
    base_url = current_app.config.get('EXT_AUTH_URL', EXT_AUTH_URL_DEFAULT)
    params = {"accessToken": g.accessToken, "urlEncode": uid}
    auth_resp = requests.post(base_url, data=params)
    rst = auth_resp.json().get('data')
    if rst:
        resp_data = json.loads(rst)
        user_id = resp_data.get('UserPID')
    else:
        token = request.cookies.get('TOKEN')
        if token:
            user_id = token.split('#')[0]
        else:
            user_id = 'no_user'
    return user_id


niubi_cards = [
    "513401196905131610", "510106197503052132",
    "432524198203044537", "510106198409031014",
    "51102619740921022X", "320107197502013423",
    "510106197607211417", "513401196310271614",
    "511026197303046417", "510625197612110019"
]

'''
人事部若干位领导的IDCard
'''
hr_leader_idcards = [
    "432524198203044537",
    "510106197503052132",
    "510106198409031014",
    "510106197607211417",
    "51102619740921022X",
    "320107197502013423",
    "513401196905131610"
]

'''
专业列表
'''
majors = ["车务", '供电', "机务", "车辆", "工务", "电务", "客运", "工电"]

'''
专业的业务处列表，它在部门的第三层级(与站段同一层级)各个专业的直属管理部门。
'''
group_business_sections = ["货运处", "运输处", "客运处", "机务处", "工务处", "电务处", "车辆处", "供电处"]


def get_permission_type(person):
    """
    本方法是参照原系统的权限判断逻辑构建，主要用于兼容原系统的功能而开发
    参考：common_func.is_permission
    :param person: Dict类型，person的对象从mongodb的base_person加载
    :return: 返回permit类型
    """
    if person['ID_CARD'] in niubi_cards \
            or person['MAJOR'] in ["安监", "局领导"]:
        permission_type = 1
    elif person['MAJOR'] in majors:
        permission_type = 2
    else:
        permission_type = 3
    return permission_type
