import json

from flask import Response, request

from app.auth import auth_bp
from app.login import login


@auth_bp.route('/login', methods=['GET', 'POST'])
def login_check():
    param_dict = request.get_json()
    if param_dict and 'username' in param_dict and 'pwd' in param_dict:
        username = param_dict['username']
        pwd = param_dict['pwd']
        TOKEN = login.login_check(username, pwd)
        if TOKEN is None:
            rs = {'data': '', 'status': 1, 'error_message': '用户名或密码错误'}
        else:
            rs = {'data': '登录成功', 'status': 0, 'error_message': ''}
    else:
        rs = {'data': '', 'status': 1, 'error_message': 'LACK_PARAM ERROR'}
    res = Response(json.dumps(rs), mimetype='application/json')
    res.delete_cookie('TOKEN')
    if TOKEN is not None:
        expire = 32472115200  # 2999年过期
        # res.set_cookie(key='TOKEN', value=TOKEN, max_age=None,
        # expires=expire, domain=request.remote_addr)
        res.set_cookie(key='TOKEN', value=TOKEN, max_age=None, expires=expire)
    return res


@auth_bp.route('/logout', methods=['GET', 'POST'])
def logout():
    TOKEN = request.cookies.get('TOKEN')
    is_update = login.logout(TOKEN)
    if is_update is True:
        rs = {'data': '注销成功', 'status': 0, 'error_message': ''}
    else:
        rs = {'data': '注销失败', 'status': 2, 'error_message': 'TOKEN无效'}
    res = Response(json.dumps(rs), mimetype='application/json')
    if is_update is True:
        res.delete_cookie('TOKEN')

    return res
