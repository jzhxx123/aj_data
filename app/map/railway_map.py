#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    小屏地图数据加载
'''

from datetime import datetime as dt
import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app, g

from app import mongo
from app.map.util import get_alldepartment_underid
from app.utils.common_func import (choose_collection, choose_collection_prefix,
                                   get_major_dpid, get_today)


def get_range_months(s_month, e_month):
    result = []
    for i in range(s_month, e_month + 1):
        if (i % 100) < 13 and (i % 100) > 0:
            result.append(i)
    return result


def value(records, level):
    month_check = []
    for record in records:
        tmp_fields = [
            record['DPID'], record['NAME'], record['PARENT'],
            [record['LONGITUDE'], record['LATITUDE']], [
                record['EVALUATE'], record['CHECK_PROBLEM'],
                record['SAFETY_PRODUCE_INFO'], record['CHECK_INFO']
            ]
        ]
        if level == 1:
            if 'RANK' in record and 'SCORE' in record:
                if record['RANK'] > 0:
                    tmp_fields.append([int(record['SCORE']), record['RANK']])
                else:
                    tmp_fields.append([])
            else:
                tmp_fields.append([])
        month_check.append(tmp_fields)

    return month_check  # 每个月的检查的list

def  formatdata_byuserdepartmentid(major, param_dict, zhanduan_info):
    """
    查询地图信息按照用户部门id，以及查询到下属部门
    Arguments:
        major {[str]} -- [description]
        param_dict {[dict]} -- [description]
    """
    mons = get_range_months(
        int(param_dict['START_MONTH']), int(param_dict['END_MONTH']))
    all_data = []
    dpid = f"'{zhanduan_info['zhanduan_id']}'"
    all_department_id = get_alldepartment_underid(dpid)
    all_department_id = [eval(item) for item in all_department_id]
    value_list = [{} for i in range(3)]
    for mon in mons:
        for _prefix in choose_collection(param_dict):
            coll_name = f'{_prefix}map_data'
            raw_data = list(mongo.db[coll_name].find({'MON': mon, 'MAJOR': major}))
            if not raw_data:
                continue
            all_data.extend(raw_data)
        for item in value_list:
            item[mon] = []
    raw_df = pd.DataFrame(all_data)
    raw_df = raw_df[raw_df['DPID'].isin(all_department_id)]
    rst_data = dict()
    rst_data['name'] = major
    for index, row in raw_df.iterrows():
        cur = int(row['LEVEL']) - 1
        mon = row['MON']
        info = [
            row['DPID'],
            row['NAME'],
            row['PARENT'],
            [row['LONGITUDE'], row['LATITUDE']], [
                row['EVALUATE'], row['CHECK_PROBLEM'],
                row['SAFETY_PRODUCE_INFO'], row['CHECK_INFO']
            ]
        ]
        value_list[cur][mon].append(info)
    rst_data['value'] = value_list
    return rst_data

def format(Major, param_dict, zhanduan_info={}):
    mons = get_range_months(
        int(param_dict['START_MONTH']), int(param_dict['END_MONTH']))
    major_check = {}
    level_3 = []
    every_month = {}
    if Major != '综合':
        for level in [1, 2, 3]:
            for mon in mons:
                records = []
                for _prefix in choose_collection(param_dict):
                    coll_name = '{}map_data'.format(_prefix)
                    records.extend(
                        list(mongo.db[coll_name].find({
                            'MON': mon,
                            'MAJOR': Major,
                            'LEVEL': level
                        })))
                if len(records) == 0:
                    continue
                every_month[str(mon)] = value(records, level)
            level_3.append(every_month)
            every_month = {}
        major_check['value'] = level_3
        major_check['name'] = Major
    else:
        for level in [1, 2, 3]:
            for mon in mons:
                records = []
                for _prefix in choose_collection(param_dict):
                    coll_name = '{}map_data'.format(_prefix)
                    records.extend(
                        list(mongo.db[coll_name].find({
                            'MON': mon,
                            'LEVEL': level
                        })))
                if len(records) == 0:
                    continue
                every_month[str(mon)] = value(records, level)
            level_3.append(every_month)
            every_month = {}
        major_check['value'] = level_3
        major_check['name'] = '综合'
    return major_check  # dictionary


def get_notification_check_info(today, mon, _prefix, major):
    '''获取今日/当月检查的新增数
    '''
    coll_name = '{}detail_check_info'.format(_prefix)
    cdn_mon = {
        "MON": {
            "$lte": int(mon),
            "$gte": int(mon)
        }
    }
    cdn_today = {"DATE": today}
    if major is not None:
        cdn_mon.update(major)
        cdn_today.update(major)
    mon_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_mon))
    today_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_today))
    return [mon_num, today_num]


def get_notification_check_problem(today, mon, _prefix, major):
    '''获取今日/当月检查问题的新增数
    '''
    coll_name = '{}detail_check_problem'.format(_prefix)
    cdn_mon = {
        "MON": mon
    }
    cdn_today = {'DATE': today}
    if major is not None:
        cdn_mon.update(major)
        cdn_today.update(major)
    mon_total_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_mon))
    today_total_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_today))
    mon_redline_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "IS_RED_LINE": 1
        }
    }))
    today_redline_num = len(mongo.db[coll_name].distinct(
        "PK_ID", {
            **cdn_today,
            **{
                "IS_RED_LINE": 1
            }
        }))
    mon_risk_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "RISK_LEVEL": {
                "$lte": 2
            }
        }
    }))
    today_risk_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_today,
        **{
            "RISK_LEVEL": {
                "$lte": 2
            }
        }
    }))
    return {
        'total': [mon_total_num, today_total_num],
        'redline': [mon_redline_num, today_redline_num],
        'risk': [mon_risk_num, today_risk_num]
    }


def get_notification_spi(today, mon, _prefix, major):
    '''获取今日/当月安全生产信息的新增数
    '''
    coll_name = '{}detail_safety_produce_info'.format(_prefix)
    cdn_mon = {"MON": mon}
    cdn_today = {'DATE': today}
    if major is not None:
        cdn_mon.update(major)
        cdn_today.update(major)
    today_total_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_today))
    mon_total_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_mon))
    today_guzhang_num = len(mongo.db[coll_name].distinct(
        "PK_ID", {
            **cdn_today,
            **{
                "MAIN_TYPE": 2
            }
        }))
    mon_guzhang_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "MAIN_TYPE": 2
        }
    }))
    today_zonghexinxi_num = len(mongo.db[coll_name].distinct(
        "PK_ID", {
            **cdn_today,
            **{
                "MAIN_TYPE": 3
            }
        }))
    mon_zonghexinxi_num = len(mongo.db[coll_name].distinct(
        "PK_ID", {
            **cdn_mon,
            **{
                "MAIN_TYPE": 3
            }
        }))
    today_shigu_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_today,
        **{
            "MAIN_TYPE": 1
        }
    }))
    mon_shigu_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "MAIN_TYPE": 1
        }
    }))
    # 事故分类
    today_xingche_num = len(mongo.db[coll_name].distinct(
        "PK_ID", {
            **cdn_today,
            **{
                "MAIN_TYPE": 1,
                "DETAIL_TYPE": 1
            }
        }))
    mon_xingche_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1
        }
    }))
    today_luwai_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_today,
        **{
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 3
        }
    }))
    mon_luwai_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 3
        }
    }))
    today_laoan_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_today,
        **{
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 2
        }
    }))
    mon_laoan_num = len(mongo.db[coll_name].distinct("PK_ID", {
        **cdn_mon,
        **{
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 2
        }
    }))

    return {
        'total': [mon_total_num, today_total_num],
        'shiju': [[mon_shigu_num,
                   today_shigu_num], [mon_xingche_num, today_xingche_num],
                  [mon_luwai_num,
                   today_luwai_num], [mon_laoan_num, today_laoan_num]],
        'guzhang': [mon_guzhang_num, today_guzhang_num],
        'zonghexinxi': [mon_zonghexinxi_num, today_zonghexinxi_num]
    }


def get_today_notification(ltime, major):
    _prefix = 'daily_'
    now = dt.fromtimestamp(ltime)
    now = get_today()
    mon = int(f'{now.year}{now.month:0>2}')
    if now.day > current_app.config.get('UPDATE_DAY'):
        mon = now + relativedelta(months=1)
        mon = int(f'{mon.year}{mon.month:0>2}')
    if now.day == current_app.config.get('UPDATE_DAY'):
        _prefix = 'monthly_'
    now = now - relativedelta(days=1)
    today = int('{}{:0>2}{:0>2}'.format(now.year, now.month, now.day))
    if major in ['路局', '安监']:
        major = None
    else:
        major = {'MAJOR': major}
    check_info = get_notification_check_info(today, mon, _prefix, major)
    check_problem = get_notification_check_problem(today, mon, _prefix, major)
    safety_produce_info = get_notification_spi(today, mon, _prefix, major)
    return {
        'safety_produce_info': safety_produce_info,
        'check_info': check_info,
        'check_problem': check_problem,
    }


def get_global_data(ltime, major):

    health_index = {'total': 91, 'major': [92, 86, 90, 93, 88, 95]}
    radar = [{
        'name': '异常站段',
        'value': [3, 0, 2, 1, 0, 2],
        'range': [0, 3]
    }, {
        'name': '安全综合指数',
        'value': [92, 86, 90, 93, 88, 95],
        'range': [0, 100]
    }]
    result = {
        'health_index': health_index,
        'radar': radar,
        'today': get_today_notification(ltime, major),
    }
    return result


def get_cache_data(data):
    result = [None for x in range(7)]
    major = ['供电', '车辆', '机务', '车务', '工务', '电务', '综合']
    for document in data:
        _idx = major.index(document['MAJOR'])
        result[_idx] = document['DATA']
    return result


def get_map_data(param_dict, TOKEN, zhanduan_info={}):
    result = []
    if zhanduan_info['zhanduan_id']:
        major_data = formatdata_byuserdepartmentid(g.major, param_dict, zhanduan_info)
        result.append(major_data)
    else:
        for major in ['供电', '车辆', '机务', '车务', '工务', '电务', '综合']:
            if TOKEN in ['路局', '安监']:
                pass
            else:
                if major != TOKEN:
                    continue
            major_data = format(major, param_dict)
            if major_data is None:
                continue
            result.append(major_data)
    return result


def get_dpid_rank(DPID, TYPE, MONTH):
    MAJOR = get_major_dpid(DPID)
    if MAJOR not in ['供电', '车辆', '机务', '车务', '工务', '电务', '综合']:
        return 'DPID-%s invalid' % DPID
    coll_name = '{}health_index'.format(choose_collection_prefix(MONTH))
    documents = list(mongo.db[coll_name].find({
        "MON": MONTH,
        "MAJOR": MAJOR
    }, {
        "_id": 0,
        "DEPARTMENT_NAME": 1,
        "RANK": 1,
        "SCORE": 1
    }))
    documents = sorted(documents, key=lambda x: x['RANK'])
    return documents


def main():
    pass


if __name__ == '__main__':
    main()
