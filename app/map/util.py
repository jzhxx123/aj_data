# -*- coding: utf-8 -*-

# 获取某个部门的下级部门id
def get_alldepartment_underid(dpid):
    """[summary]
    
    Arguments:
        dpid {[str]} -- [description]
    """
    from app.data.util import pd_query
    load_child_check_item_sql = """SELECT DEPARTMENT_ID FROM t_department WHERE FK_PARENT_ID IN ({0});"""
    ret = dpid
    parent_ids = dpid
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['DEPARTMENT_ID'].tolist()
            str_list = [f"'{val}'" for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    return list(total_set)