#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
from app import mongo
from app.story.util import get_hierarchy_dpid, get_range_months
from app.utils.common_func import get_last_year, choose_collection
import pandas as pd


def code_map(code_name):
    code = {
        'LH': 'LH',
        'JL': 'JC',
        'ZL': 'JD',
        'KH': 'KH',
        'ZG': 'WT',
        'ZD': 'ZD',
        'YY': 'YSP',
        'LZ': 'LZ',
        'SZ': 'SG',
        'ZJ': 'NX',
        'TX': 'AQ',
    }
    return code.get(code_name, None)


def code_map_revese(code_name):
    code = {
        'LH': 'LH',
        'JC': 'JL',
        'JD': 'ZL',
        'KH': 'KH',
        'WT': 'ZG',
        'ZD': 'ZD',
        'YSP': 'YY',
        'LZ': 'LZ',
        'SG': 'SZ',
        'NX': 'ZJ',
        'AQ': 'TX',
    }
    return code.get(code_name, None)


def code_name_map(code_name):
    code = {
        'LH': '量化指标完成',
        'JC': '检查信息录入',
        'JD': '监督检查质量',
        'KH': '考核责任落实',
        'WT': '问题闭环管理',
        'ZD': '重点工作落实',
        'YSP': '音视频运用管理',
        'LZ': '履职评价管理',
        'SG': '事故故障追溯',
        'NX': '弄虚作假',
        'AQ': '安全谈心',
    }
    return code.get(code_name, None)


def merge_item(item_dict, items):
    for each in items:
        if each[0] in item_dict:
            item_dict.update({each[0]: item_dict.get(each[0]) + each[1]})
        else:
            item_dict.update({each[0]: each[1]})
    return item_dict


def get_code_count_history(DPID, months):
    # build grid
    code_grid = [[0 for col in range(4)] for row in range(11)]
    for each in mongo.db.evaluate_code.find({
            "DPID": DPID,
            "MONTH": {
                "$lt": months[1],
                "$gte": months[0]
            }
    }, {
            "_id": 0,
            'CODE': 1
    }):
        code = each['CODE']
        for each in code:
            index = code_map(each['NAME'])
            for idx, val in enumerate(each['VALUE']):
                if val['SIZE'] == 0:
                    continue
                code_grid[index][idx] += val['SIZE']

    return code_grid


def get_count_check_type(param_dict):
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_evaluate_check_type'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({
                "DPID": param_dict["DPID"],
                "MON": {
                    "$lte": int(param_dict["END_MONTH"]),
                    "$gte": int(param_dict["START_MONTH"])
                }
            }, {
                "_id": 0,
                "CHECK_TYPE": 1,
                "COUNT": 1,
                "CODE": 1
            })))
    codes = ['LH', 'JC', 'JD', 'KH', 'WT', 'ZD', 'YSP', 'LZ', 'SG', 'NX', 'AQ']
    data_dict = {key: 0 for key in codes}
    # 3个字典分别用来存[路局、站段、全部]的统计
    code_grid = [copy.deepcopy(data_dict) for col in range(3)]
    for idx, title in enumerate(['路局评价', '站段评价', '全部']):
        code_grid[idx].update({'header': title})
    for document in documents:
        code = code_map(document['CODE'])
        if code is None:
            continue
        code_grid[2].update({code: document['COUNT'] + code_grid[2].get(code)})
        if document['CHECK_TYPE'] == 1:
            code_grid[0].update({
                code:
                document['COUNT'] + code_grid[0].get(code)
            })
        elif document['CHECK_TYPE'] == 2:
            code_grid[1].update({
                code:
                document['COUNT'] + code_grid[1].get(code)
            })
    return code_grid


def get_count_single_score(param_dict):
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_evaluate_single_score'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({
                "DPID": param_dict["DPID"],
                "MON": {
                    "$lte": int(param_dict["END_MONTH"]),
                    "$gte": int(param_dict["START_MONTH"])
                }
            }, {
                "_id": 0,
                "LEVEL": 1,
                "COUNT": 1,
                "CODE": 1
            })))
    codes = ['LH', 'JC', 'JD', 'KH', 'WT', 'ZD', 'YSP', 'LZ', 'SG', 'NX', 'AQ']
    data_dict = {key: 0 for key in codes}
    # 8个字典分别用来存单个问题分数段：0-1分、1-2分、2-4分、4-6分、
    # 6-8分、8-10分、10分以上, 全部（统计问题个数）
    code_grid = [copy.deepcopy(data_dict) for col in range(8)]
    for idx, title in enumerate(
        ['0-1分', '1-2分', '2-4分', '4-6分', '6-8分', '8-10分', '10分以上', '全部']):
        code_grid[idx].update({'header': title})
    for document in documents:
        code = code_map(document['CODE'])
        if code is None:
            continue
        code_grid[7].update({code: document['COUNT'] + code_grid[7].get(code)})
        LEVEL = document['LEVEL']

        code_grid[LEVEL - 1].update({
            code:
            document['COUNT'] + code_grid[LEVEL - 1].get(code)
        })
    return code_grid


def get_count_grada(param_dict):
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_evaluate_grada'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({
                "DPID": param_dict["DPID"],
                "MON": {
                    "$lte": int(param_dict["END_MONTH"]),
                    "$gte": int(param_dict["START_MONTH"])
                }
            }, {
                "_id": 0,
                "GRADA_ID": 1,
                "COUNT": 1,
                "CODE": 1
            })))
    codes = ['LH', 'JC', 'JD', 'KH', 'WT', 'ZD', 'YSP', 'LZ', 'SG', 'NX', 'AQ']
    data_dict = {key: 0 for key in codes}
    # 4个字典分别用来存[局管，正科，副科及以下,全部]（统计问题个数）
    code_grid = [copy.deepcopy(data_dict) for col in range(4)]
    for idx, title in enumerate(['局管', '正科', '副科及以下', '全部']):
        code_grid[idx].update({'header': title})
    for document in documents:
        code = code_map(document['CODE'])
        if code is None:
            continue
        code_grid[3].update({code: document['COUNT'] + code_grid[3].get(code)})
        GRADA_ID = document['GRADA_ID']

        code_grid[GRADA_ID - 1].update({
            code:
            document['COUNT'] + code_grid[GRADA_ID - 1].get(code)
        })
    return code_grid


def get_code_count(param_dict):
    count_check_type = get_count_check_type(param_dict)
    count_single_score = get_count_single_score(param_dict)
    count_grada = get_count_grada(param_dict)
    return {
        'check_type': count_check_type,
        'single_score': count_single_score,
        'grada': count_grada
    }


def get_code_history(DPID):
    code_grid = [[[] for row in range(4)] for col in range(11)]
    for record in mongo.db.evaluate_code_history.find({
            "DPID": DPID
    }, {"_id": 0}):
        index = code_map(record['CODE'])
        for each in record['VALUE']:
            date = [x[0] for x in record['VALUE'][each]]
            value = [x[1] for x in record['VALUE'][each]]
            code_grid[index][int(each)] = [date, value]
    return code_grid


def get_pie_data(data):
    data = data[-1]
    del data['header']
    privot = sum(list(data.values())) / len(data)
    big_half = {k: v for k, v in data.items() if v >= privot}
    small_half = {k: v for k, v in data.items() if v < privot}
    big_half = sorted(big_half.items(), key=lambda x: x[1])
    small_half = sorted(small_half.items(), key=lambda x: x[1])
    result = []
    for idx, val in enumerate(small_half):
        result.append({
            'name': code_name_map(val[0]),
            'value': val[1],
            'rank': idx
        })
    for idx, val in enumerate(big_half):
        result.append({
            'name': code_name_map(val[0]),
            'value': val[1],
            'rank': len(big_half) - idx - 1
        })
    return {'privot': '{:.2f}'.format(privot), 'data': result}


def code_calculate_ratio(now, last):
    code_grid = [[0 for row in range(4)] for col in range(11)]
    for col in range(11):
        for row in range(4):
            old = last[col][row]
            new = now[col][row]
            if (new - old) == 0:
                code_grid[col][row] = '-'
            elif old == 0 or new == 0:
                code_grid[col][row] = '{} -> {}'.format(old, new)
            else:
                code_grid[col][row] = '{:.1f}%'.format((new - old) / old * 100)
    return code_grid


# def convert_month(months):
#     _month_type = {
#         'month': 1,
#         'quarter': 3,
#         'halfyear': 6,
#         'year': 12,
#     }
#     now = get_last_month(_month_type.get(months))
#     last = [get_last_month(2 * _month_type.get(months))[0], now[0]]
#     return now, last


def merge_person(person_dict, persons):
    for each in persons:
        if each not in person_dict:
            person_dict.update({each: '{:.2f}'.format(float(persons[each]))})
        else:
            if persons[each] > person_dict[each]:
                person_dict.update({
                    each: '{:.2f}'.format(float(persons[each]))
                })
    return person_dict


# def get_score_count_history(param_dict):
#     # build grid
#     level_grid = [[0 for row in range(4)] for col in range(7)]
#     for each in mongo.db.evaluate_score.find({
#             "DPID": DPID,
#             "MONTH": {
#                 "$lt": months[1],
#                 "$gte": months[0]
#             }
#     }, {
#             "_id": 0,
#             'LEVEL': 1
#     }):
#         level = each['LEVEL']
#         for each in level:
#             index = each['level'] - 1
#             value = each['VALUE']
#             for idx, grada in enumerate(value):
#                 if grada != None:
#                     level_grid[index][idx] = grada['COUNT']
#     return level_grid


def score_calculate_ratio(now, last):
    score_grid = [[0 for row in range(4)] for col in range(7)]
    for col in range(7):
        for row in range(4):
            old = last[col][row]
            new = now[col][row]
            if (new - old) == 0:
                score_grid[col][row] = '-'
            elif new > old:
                score_grid[col][row] = '+{}'.format(new - old)
            else:
                score_grid[col][row] = '{}'.format(new - old)
    return score_grid


def get_score_count1(param_dict):

    documents = []
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_evaluate_record'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(
                {
                    "TYPE{}".format(hierarchy): param_dict['DPID'],
                    "MON": {
                        "$gte": int(param_dict['START_MONTH']),
                        "$lte": int(param_dict['END_MONTH'])
                    }
                }, {
                    "_id": 0,
                    "PERSON_ID": 1,
                    "PERSON_NAME": 1,
                    "GRADA_ID": 1,
                    "SCORE": 1
                })))
    # build grid
    level_grid = [[0 for col in range(4)] for row in range(7)]
    level_person = [[{} for col in range(4)] for row in range(7)]
    data = pd.DataFrame(documents)
    if data.empty:
        return level_grid, level_person
    score_rst = data.groupby(['PERSON_ID'])['SCORE'].sum()
    for idx in score_rst.index:
        person_id = idx
        score = score_rst.loc[idx]
        row = data[data['PERSON_ID'] == person_id]
        grada_id = int(row['GRADA_ID'].values[0])
        person_name = row['PERSON_NAME'].values[0]
        if score < 2:
            level = 1
        elif score < 4:
            level = 2
        elif score < 6:
            level = 3
        elif score < 8:
            level = 4
        elif score < 10:
            level = 5
        elif score < 12:
            level = 6
        else:
            level = 7

        level_grid[level - 1][grada_id] += 1
        level_grid[level - 1][0] += 1
        level_person[level - 1][grada_id] = merge_person(
            level_person[level - 1][grada_id], {person_name: score})
        level_person[level - 1][0] = merge_person(level_person[level - 1][0],
                                                  {person_name: score})
    for col in range(7):
        for row in range(4):
            if level_person[col][row] == {}:
                level_person[col][row] = []
            else:
                value = level_person[col][row]
                value = sorted(value.items(), key=lambda x: x[1], reverse=True)
                value = value[:10]
                level_person[col][row] = value
    return level_grid, level_person


def get_accumulative_score(param_dict):
    # 履值分数统计
    score_data, score_person_data = get_score_count1(param_dict)
    codes = [
        '1',
        '2',
        '3',
        '4',
        '5',
        '6',
        '7',
    ]
    score_dict = {key: 0 for key in codes}
    person_dict = {key: [] for key in codes}
    # 4个字典分别用来存[局管，正科，副科及以下,全部]（统计问题个数）
    score_grid = [copy.deepcopy(score_dict) for col in range(4)]
    person_grid = [copy.deepcopy(person_dict) for col in range(4)]
    for idx, title in enumerate(['局管', '正科', '副科及以下', '全部']):
        score_grid[idx].update({'header': title})
        person_grid[idx].update({'header': title})
    for col in range(7):
        for row in range(4):
            idx = (row - 1 + 4) % 4
            score_grid[idx].update({str(col + 1): score_data[col][row]})
            person_grid[idx].update({
                str(col + 1): score_person_data[col][row]
            })

    return {'score': score_grid, 'person': person_grid}


def get_data(param_dict):
    code_count = get_code_count(param_dict)
    pie_data = get_pie_data(copy.deepcopy(code_count['check_type']))
    score = get_accumulative_score(param_dict)

    return {
        'range_month': get_last_year(),
        'CODE': code_count,
        'PIE': pie_data,
        'SCORE': score
    }


def get_query_condition(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {
        "TYPE{}".format(hierarchy): param_dict['DPID'],
        "MON": {
            "$lte": int(param_dict["END_MONTH"]),
            "$gte": int(param_dict["START_MONTH"])
        }
    }

    # 确定哪个检查类型
    code = code_map_revese(param_dict['CODE'])
    if code is None:
        return "CODE值无效"
    else:
        condition.update({"CODE": code})

    total_id = {'CHECK_TYPE': 3, 'GRADA_ID': 4, 'LEVEL': 8}
    field_type = {
        'check_type': 'CHECK_TYPE',
        'grada': 'GRADA_ID',
        'single_score': 'LEVEL'
    }
    k = field_type.get(param_dict['TYPE'])
    v = int(param_dict['LEVEL'])
    if v == total_id.get(k):
        return condition
    condition.update({k: v})

    return condition


def statistics_code_addition(documents):
    CODE_ADDITION = {}
    for document in documents:
        CODE_ADDITION.update({
            document['SITUATION']:
            CODE_ADDITION.get(document['SITUATION'], 0) + 1
        })
    CODE_ADDITION = sorted(
        CODE_ADDITION.items(), key=lambda x: x[1], reverse=True)
    result = []
    for item in CODE_ADDITION:
        result.append({'item': item[0].replace('。', ''), 'value': item[1]})
    return result


def statistics_code_history(documents):
    result = {}
    for document in documents:
        date = '{}{}'.format(
            str(document['MON'])[:4],
            str(document['MON'])[4:])
        result.update({date: result.get(date, 0) + 1})
    x = []
    y = []
    for month in get_range_months():
        x.append('{}-{}'.format(str(month)[:4], str(month)[4:]))
        y.append(result.get(str(month), 0))
    return {
        'DATE': x,
        'COUNT': y,
    }


def get_code_detail_data(param_dict):
    condition = get_query_condition(param_dict)
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_evaluate_record'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(condition, {
                "_id": 0,
                "SITUATION": 1,
            })))
    CODE_ADDITION = statistics_code_addition(documents)

    del condition["MON"]
    documents = []
    for _prefix in ['daily_', 'monthly_']:
        coll_name = '{}detail_evaluate_record'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(condition, {
                "_id": 0,
                "MON": 1,
            })))
    HISTORY_DATA = statistics_code_history(documents)

    return {
        'detail': CODE_ADDITION,
        'history': HISTORY_DATA,
    }


if __name__ == '__main__':
    pass
