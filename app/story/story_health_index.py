#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd

from app import mongo
from app.story.util import get_hierarchy_dpid
from app.utils.safety_index_common_func import (
    get_child_calc_formula, get_child_index_name, get_index_title,
    get_health_index_calc_tree)
from app.utils.common_func import get_history_months


def _get_index_code(MAIN_TYPE):
    _map = {
        0: 'all',
        1: 'check_intensity',
        2: 'evaluate_intensity',
        4: 'check_balance',
        3: 'rate_intensity',
        5: 'issue_exposure',
        6: 'issue_fix',
        # 7: 'respon_produce'
    }
    if MAIN_TYPE in _map:
        return _map.get(MAIN_TYPE, '-')


def _get_sorted_dp(data):
    data = {item[2]: f'{item[4]:.2f}/{int(item[3])}' for item in data}
    rst = []
    global MON
    for x in MON:
        rst.append(data.get(x, '0/0'))
    return rst


def _get_mon(mon):
    mon.sort()
    rst = ["DATE"]
    global MON
    MON = mon
    mon = ['{}-{:0>2}'.format(item // 100, item % 100) for item in mon]
    rst.extend(mon)
    return rst


def get_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {"DEPARTMENT_ID": param_dict['DPID'], "HIERARCHY": 3}
    rst_all = []
    rst_score = []
    mon = list(mongo.db['monthly_health_index'].distinct('MON'))
    rst_score.append(_get_mon(mon))
    # 子指数折线图加入安全综合指数（总体）的数据
    data_health_index = mongo.db['monthly_health_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_health_index))
    if len(data) == 0:
        return "NO DATA"
    data = {item[0]: f'{item[2]:.2f}/{int(item[1])}' for item in data.values}
    score_total_health_index = ['all']
    global MON
    for x in MON:
        score_total_health_index.append(data.get(x, '0/0'))
    rst_score.append(score_total_health_index)
    # 各子指数历史12个月趋势图
    data_detail_health_index = mongo.db['monthly_detail_health_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    data = pd.DataFrame(list(data_detail_health_index))
    if len(data) == 0:
        return "NO DATA"
    groups = data[data['DETAIL_TYPE'] == 0].groupby(['MAIN_TYPE'])
    # 按格式输出
    for k, v in groups:
        rst_score.append([_get_index_code(k), *(_get_sorted_dp(v.values))])
    rst_all.append(rst_score)

    # 各子指数的分指数历史12个月趋势图
    for main_type in range(1, 7):
        rst_score = []
        rst_score.append(_get_mon(mon))
        groups = data[data['MAIN_TYPE'] == main_type].groupby(['DETAIL_TYPE'])
        for k, v in groups:
            _title = get_child_index_name(k, main_type)
            if _title is None:
                continue
            rst_score.append([_title, *(_get_sorted_dp(v.values))])
        rst_all.append(rst_score)
    return rst_all


def calc_tree(param_dict):
    # mon = int(param_dict['MON'])
    if 'MONTH' not in param_dict:
        mon = get_history_months(-1)[0]
    else:
        mon = int(param_dict['MONTH'])
    index_type = int(param_dict['TYPE'])
    dpid = param_dict['DPID']
    hierarchy = get_hierarchy_dpid(dpid)
    if hierarchy is None:
        return "DPID不存在"
    if index_type not in [0, 1, 2, 3, 4, 5, 6]:
        return "该指数还在开发中"
    rtn_html = {}
    if index_type == 0:
        tree_child = []
        for child_index_type in [1, 2, 3, 4, 5, 6]:
            tree_child.append(
                get_health_index_calc_tree(dpid, mon, child_index_type))
        rtn_html.update({'name': '安全综合指数', 'children': tree_child})
    else:
        rtn_html = get_health_index_calc_tree(dpid, mon, index_type)

    return rtn_html


if __name__ == '__main__':
    pass
