#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import datetime as dt

from app import mongo
from app.story.util import get_hierarchy_dpid, get_major_dpid, get_range_months
from app.utils.common_func import choose_collection, get_last_year


def merge_person(
        person_dict,
        person,
):
    person_dict.update({person: person_dict.get(person, 0) + 1})
    return person_dict


def merge_persons(person_dict_list):
    result = {}
    for person_dict in person_dict_list:
        for person in person_dict:
            result.update({
                person:
                result.get(person, 0) + person_dict.get(person)
            })
    return result


def get_problem_condition(DPID):
    # 从check_problem_item加载筛选条件
    major = get_major_dpid(DPID)
    if major is None:
        return ValueError('DPID-%s invalid' % DPID)
    documents = list(
        mongo.db.base_check_problem_item.find({
            'MAJOR': {
                "$in": ['global', major]
            }
        }))
    titles = [
        'risk_level', 'problem_classify', 'risk_consequence', 'problem_level'
    ]
    result = [None for x in range(4)]
    for document in documents:
        if document['TYPE'] not in titles:
            continue
        idx = titles.index(document['TYPE'])
        result[idx] = [[value['ID'], value['NAME']]
                       for value in document['VALUE']]
    return result


def data_format(hour):
    hour_section = [[0, 6], [6, 8], [8, 12], [12, 14], [14, 18], [18, 20],
                    [20, 24]]
    for idx, val in enumerate(hour_section):
        if hour >= val[0] and hour < val[1]:
            return idx + 1


def get_week_hours(start_time, end_time):
    start_time = dt.datetime.strptime(start_time, '%Y%m%d%H')
    end_time = dt.datetime.strptime(end_time, '%Y%m%d%H')
    res = set()
    res.add('{}-{}'.format(start_time.weekday() + 1,
                           data_format(start_time.hour)))
    res.add('{}-{}'.format(end_time.weekday() + 1, data_format(end_time.hour)))
    hour = 1
    while hour > 0:
        deltahour = dt.timedelta(hours=hour)
        tmp_time = start_time + deltahour
        if tmp_time > end_time:
            break
        res.add('{}-{}'.format(tmp_time.weekday() + 1,
                               data_format(tmp_time.hour)))
        hour += 1

    return [[int(item[0]), int(item[-1])] for item in sorted(res)]


def check_way_classify(item_list):
    return [item_list.count(1), item_list.count(2), item_list.count(3)]


def get_info(hierarchy, param_dict):
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_check_info'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(
                {
                    "TYPE{}".format(hierarchy): param_dict['DPID'],
                    "MON": {
                        "$lte": int(param_dict['END_MONTH']),
                        "$gte": int(param_dict['START_MONTH'])
                    }
                }, {
                    "_id": 0,
                    "PERSON_NAME": 1,
                    "CHECK_WAY": 1,
                    "START_CHECK_HOUR": 1,
                    "END_CHECK_HOUR": 1,
                })))
    count_grid = [[0 for row in range(8)] for col in range(8)]
    item_grid = [[[] for row in range(8)] for col in range(8)]
    person_grid = [[{} for row in range(8)] for col in range(8)]
    person_grid_xc = [[{} for row in range(8)] for col in range(8)]
    person_grid_tc = [[{} for row in range(8)] for col in range(8)]
    person_grid_jd = [[{} for row in range(8)] for col in range(8)]
    for record in documents:
        # 1-7表示周一到周日， 1-7表示分时段
        # 横跨几天的分别计数
        week_hours = get_week_hours(
            str(record['START_CHECK_HOUR']), str(record['END_CHECK_HOUR']))
        for item in week_hours:
            weekday = item[0] - 1
            hour = item[1] - 1
            count_grid[weekday][hour] += 1
            tmp_item = item_grid[weekday][hour]
            tmp_item.append(record['CHECK_WAY'])
            item_grid[weekday][hour] = tmp_item
            person_grid[weekday][hour] = merge_person(
                person_grid[weekday][hour], record['PERSON_NAME'])
            if record['CHECK_WAY'] == 1:
                person_grid_xc[weekday][hour] = merge_person(
                    person_grid_xc[weekday][hour], record['PERSON_NAME'])
            elif record['CHECK_WAY'] == 2:
                person_grid_tc[weekday][hour] = merge_person(
                    person_grid_tc[weekday][hour], record['PERSON_NAME'])
            elif record['CHECK_WAY'] == 3:
                person_grid_jd[weekday][hour] = merge_person(
                    person_grid_jd[weekday][hour], record['PERSON_NAME'])
            else:
                pass
    # 按weekday、hour累计计算
    for x in range(7):
        count_grid[x][7] = sum(count_grid[x][:7])
        item_grid[x][7] = [i for j in item_grid[x][:7] for i in j]
        person_grid[x][7] = merge_persons(person_grid[x][:7])
        person_grid_xc[x][7] = merge_persons(person_grid_xc[x][:7])
        person_grid_tc[x][7] = merge_persons(person_grid_tc[x][:7])
        person_grid_jd[x][7] = merge_persons(person_grid_jd[x][:7])
    for y in range(7):
        count_grid[7][y] = sum([count_grid[num][y] for num in range(7)])
        item_grid[7][y] = [i for j in range(7) for i in item_grid[j][y]]
        person_grid[7][y] = merge_persons(
            [person_grid[k][y] for k in range(7)])
        person_grid_xc[7][y] = merge_persons(
            [person_grid_xc[k][y] for k in range(7)])
        person_grid_tc[7][y] = merge_persons(
            [person_grid_tc[k][y] for k in range(7)])
        person_grid_jd[7][y] = merge_persons(
            [person_grid_jd[k][y] for k in range(7)])

    # total
    count_grid[7][7] = sum(count_grid[7][:7])
    item_grid[7][7] = [i for j in item_grid[7][:7] for i in j]
    person_grid[7][7] = merge_persons(person_grid[7][:7])
    person_grid_xc[7][7] = merge_persons(person_grid_xc[7][:7])
    person_grid_tc[7][7] = merge_persons(person_grid_tc[7][:7])
    person_grid_jd[7][7] = merge_persons(person_grid_jd[7][:7])

    # sorted
    for col in range(8):
        for row in range(8):
            item_grid[col][row] = check_way_classify(item_grid[col][row])
            if person_grid[col][row] == {}:
                person_grid[col][row] = []
            else:
                value = person_grid[col][row]
                value = sorted(value.items(), key=lambda x: x[1], reverse=True)
                value = value[:20]
                person_grid[col][row] = value
            if person_grid_xc[col][row] == {}:
                person_grid_xc[col][row] = []
            else:
                value = person_grid_xc[col][row]
                value = sorted(value.items(), key=lambda x: x[1], reverse=True)
                value = value[:20]
                person_grid_xc[col][row] = value
            if person_grid_tc[col][row] == {}:
                person_grid_tc[col][row] = []
            else:
                value = person_grid_tc[col][row]
                value = sorted(value.items(), key=lambda x: x[1], reverse=True)
                value = value[:20]
                person_grid_tc[col][row] = value
            if person_grid_jd[col][row] == {}:
                person_grid_jd[col][row] = []
            else:
                value = person_grid_jd[col][row]
                value = sorted(value.items(), key=lambda x: x[1], reverse=True)
                value = value[:20]
                person_grid_jd[col][row] = value

    return {
        'count_grid': count_grid,
        'item_grid': item_grid,
        'person_grid': person_grid,
        'person_grid_child': [person_grid_xc, person_grid_tc, person_grid_jd]
    }


def _calc_problems_count(prob_list):
    """计算去重后的问题个数

    Arguments:
        prob_list {list[list]} -- 问题列表，每个问题都是一个属性列表

    Returns:
        int -- 问题个数
    """
    count = 0
    prob_id = set()
    for prob in prob_list:
        if prob[-1] not in prob_id:
            count += 1
            prob_id.add(prob[-1])
    return count


def get_problem(hierarchy, param_dict):
    documents = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_check_problem'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(
                {
                    "TYPE{}".format(hierarchy): param_dict['DPID'],
                    "MON": {
                        "$lte": int(param_dict['END_MONTH']),
                        "$gte": int(param_dict['START_MONTH'])
                    }
                }, {
                    "_id": 0,
                    "PK_ID": 1,
                    "RISK_LEVEL": 1,
                    "LEVEL": 1,
                    "PROBLEM_CLASSIFY": 1,
                    "RISK_TYPE": 1,
                    "RISK_CONSEQUENCE": 1,
                    "hour_section": 1,
                    "weekday": 1,
                })))
    item_grid = [[[] for row in range(8)] for col in range(8)]
    for record in documents:
        # 1-7表示周一到周日， 1-7表示分时段
        weekday = record['weekday'] - 1
        hour = record['hour_section'] - 1
        tmp_item = item_grid[weekday][hour]
        tmp_item.append([
            record['RISK_LEVEL'],
            record['PROBLEM_CLASSIFY'],
            record['RISK_CONSEQUENCE'],
            record['LEVEL'],
            record['PK_ID'],
        ])
        item_grid[weekday][hour] = tmp_item
        # 按weekday、hour累计计算
    for x in range(7):
        item_grid[x][7] = [i for j in item_grid[x][:7] for i in j]
    for y in range(7):
        item_grid[7][y] = [i for j in range(7) for i in item_grid[j][y]]

    # total
    item_grid[7][7] = [i for j in item_grid[7][:7] for i in j]
    count_grid = [[
        _calc_problems_count(item_grid[col][row]) for row in range(8)
    ] for col in range(8)]

    return {
        'condition': get_problem_condition(param_dict['DPID']),
        'count_grid': count_grid,
        'item_grid': item_grid,
    }


def json_format(x, y):
    # color = ['#5793f3', '#d14a61']
    if y:
        ymax = round(max(y) * 1.2)
    else:
        ymax = 0
    option = {
        'color': ['#5793f3', '#d14a61'],
        'tooltip': {
            'trigger': 'axis',
            'axisPointer': {
                'type': 'cross'
            }
        },
        'grid': {
            'right': '15%',
            'left': '5%',
            'top': '10%',
            'bottom': '10%',
        },
        'xAxis': [{
            'type': 'category',
            'axisTick': {
                'alignWithLabel': True
            },
            'data': x
        }],
        'yAxis': [
            {
                'type': 'value',
                'name': '风险值累计值',
                'min': 0,
                'max': ymax,
                'position': 'right',
                'axisLine': {
                    'lineStyle': {
                        'color': '#5793f3'
                    }
                },
                'axisLabel': {
                    'formatter': '{value}'
                }
            },
            {
                'type': 'value',
                'min': 0,
                'max': ymax,
                'position': 'left',
                'offset': 80,
                'axisLine': {
                    'lineStyle': {
                        'color': '#d14a61'
                    }
                },
                'axisLabel': {
                    'formatter': '{value}'
                }
            },
        ],
        'series': [
            {
                'name': '风险值累计值',
                'type': 'bar',
                'data': y
            },
            {
                'name': '风险值累计值',
                'type': 'line',
                'yAxisIndex': 1,
                'data': y
            },
        ]
    }
    return option


def get_accumulate(hierarchy, param_dict):
    documents = []
    for _prefix in ['daily_', 'monthly_']:
        coll_name = '{}detail_check_problem'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find(
                {
                    "TYPE{}".format(hierarchy): param_dict['DPID']
                }, {
                    "_id": 0,
                    "SERIOUS_VALUE": 1,
                    "DATE": 1
                })))
    result = {}
    for record in documents:
        date = record['DATE']
        if record['SERIOUS_VALUE'] is not None:
            result.update({
                date: result.get(date, 0) + record['SERIOUS_VALUE']
            })
    result = sorted(result.items(), key=lambda x: x[0])
    if len(result) == 0:
        return []
    x = [result[0][0]]
    y = [result[0][1]]
    for idx, val in enumerate(result[1:]):
        x.append(val[0])
        y.append(val[1] + y[-1])

    return json_format(x, y)


def get_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"

    check_info = get_info(hierarchy, param_dict)

    accumulate = get_accumulate(hierarchy, param_dict)

    check_problem = get_problem(hierarchy, param_dict)

    result = {
        'range_month': get_last_year(),
        'check_info': check_info,
        'accumulate': accumulate,
        'check_problem': check_problem,
    }

    return result


def get_query_condition(param_dict, hierarchy):
    condition = {
        "TYPE{}".format(hierarchy): param_dict['DPID'],
        "MON": {
            "$lte": int(param_dict['END_MONTH']),
            "$gte": int(param_dict['START_MONTH'])
        }
    }
    if param_dict['WEEKDAY'] and int(param_dict['WEEKDAY']) in range(1, 8):
        condition.update({"weekday": int(param_dict['WEEKDAY'])})
    if param_dict['HOUR'] and int(param_dict['HOUR']) in range(1, 8):
        condition.update({"hour_section": int(param_dict['HOUR'])})
    for x in [
            'PROBLEM_LEVEL', 'RISK_TYPE', 'RISK_CONSEQUENCE', 'RISK_LEVEL',
            'PROBLEM_CLASSIFY'
    ]:
        if x in param_dict:
            value = param_dict[x]
            if value != "":
                if value.count(',') > 0:  # 如果是多选
                    if x == 'PROBLEM_LEVEL':
                        val = value.split(',')
                        condition.update({'LEVEL': {"$in": val}})
                    else:
                        val = [int(k) for k in value.split(',')]
                        condition.update({x: {"$in": val}})
                else:
                    if x == 'PROBLEM_LEVEL':
                        condition.update({'LEVEL': value})
                    else:
                        condition.update({x: int(value)})
    return condition


def get_detail_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"

    condition = get_query_condition(param_dict, hierarchy)
    documents = []
    pipeline = [{
        "$match": condition,
    }, {
        "$group": {
            "_id": {
                "PK_ID": "$PK_ID",
                "DATE": "$DATE",
                "DESCRIPTION": "$DESCRIPTION"
            }
        }
    }, {
        "$project": {
            "_id": 0,
            "DATE": "$_id.DATE",
            "DESCRIPTION": "$_id.DESCRIPTION"
        },
    }]
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_check_problem'.format(_prefix)
        documents.extend(list(mongo.db[coll_name].aggregate(pipeline)))
    del condition["MON"]
    data = []
    pipeline = [{
        "$match": condition,
    }, {
        "$group": {
            "_id": {
                "PK_ID": "$PK_ID",
                "MON": "$MON",
            }
        }
    }, {
        "$project": {
            "_id": 0,
            "MON": "$_id.MON",
        },
    }]
    for _prefix in ['daily_', 'monthly_']:
        coll_name = '{}detail_check_problem'.format(_prefix)
        data.extend(list(mongo.db[coll_name].aggregate(pipeline)))
    result = {}
    for each in data:
        mon = each['MON']
        result.update({mon: result.get(mon, 0) + 1})
    x = []
    y = []
    for month in get_range_months():
        x.append(f'{str(month)[:4]}-{str(month)[4:6]}')
        y.append(result.get(month, 0))

    # type2 = get_type_dpid(param_dict['DPID'], 2)
    # condition.update({'TYPE2': type2})
    # del condition["TYPE{}".format(hierarchy)]
    # df = pd.DataFrame(list(mongo.db.check_problem.find(condition,
    # {"_id": 0, "DATE": 1, "DPID": 1})))
    # df['MON'] = df['DATE'].apply(lambda x: str(x)[:6])
    # del df['DATE']
    # df = df.groupby(['MON', 'DPID']).size()
    # df = df.reset_index()
    # current_app.logger.debug(df[:20])

    # return None

    return {
        'detail': documents,
        'bar': [x, y],
    }


if __name__ == '__main__':
    pass
