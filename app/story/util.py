#! /usr/bin/env python3
# -*- encoding:utf-8 -*-


from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from flask import current_app
from app import mongo
from app.utils.common_func import get_today


def get_hierarchy_dpid(dpid):
    record = mongo.db.base_department.find_one({
        "DEPARTMENT_ID": dpid
    }, {
        "HIERARCHY": 1,
        "_id": 0
    })
    if record:
        return record['HIERARCHY']
    else:
        return None


def get_major_dpid(dpid):
    # 通过department_id获取专业名称
    record = mongo.db.base_department.find_one({
        "DEPARTMENT_ID": dpid
    }, {
        "MAJOR": 1,
        "_id": 0
    })
    if record:
        return record['MAJOR']
    else:
        return None


def get_type_dpid(dpid, field_type):
    record = mongo.db.check_problem.find_one({
        "DPID": dpid
    }, {
        "TYPE{}".format(field_type): 1,
        "_id": 0
    })
    if record:
        return record["TYPE{}".format(field_type)]
    else:
        return None


def get_range_months(s_month=None, e_month=None):
    if s_month is None:
        s_month = 201710
    today = get_today()
    if e_month is None:
        e_month = today.year * 100 + today.month
    result = []
    for i in range(s_month, e_month + 1):
        if (i % 100) < 13 and (i % 100) > 0:
            result.append(i)
    return result


def get_range_days(MONTH):
    today = get_today()
    year = int(str(MONTH)[:4])
    mon = int(str(MONTH)[4:6])
    e_mon = dt(year=year, month=mon, day=1)
    s_mon = e_mon - relativedelta(months=1)
    update_day = current_app.config.get('UPDATE_DAY')
    if e_mon.month > today.month:
        return [f'{s_mon.month}/{_}' for _ in range(update_day, today.day + 1)]
    else:
        pre_days = []
        if s_mon.month == 2:
            if (s_mon.year % 4) == 0 and (s_mon.year % 100) != 0 or (
                    s_mon.year % 400) == 0:
                pre_days = [f'{s_mon.month}/{_}' for _ in range(update_day, 30)]
            else:
                pre_days = [f'{s_mon.month}/{_}' for _ in range(update_day, 29)]
        elif s_mon.month in [1, 3, 5, 7, 8, 10, 12]:
            pre_days = [f'{s_mon.month}/{_}' for _ in range(update_day, 32)]
        else:
            pre_days = [f'{s_mon.month}/{_}' for _ in range(update_day, 31)]
        post_days = [f'{e_mon.month}/{_}' for _ in range(1, update_day)]
        if e_mon.month == today.month:
            post_days = [f'{e_mon.month}/{_}' for _ in range(1, today.day + 1)] 
        pre_days.extend(post_days)
        return pre_days


if __name__ == '__main__':
    pass
