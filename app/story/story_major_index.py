#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from app import mongo
from app.story.util import get_hierarchy_dpid, get_range_months
from app.utils.common_func import get_major_dpid, get_history_months
from app.utils.safety_index_common_func import (
    get_child_calc_formula, get_child_index_name, get_index_title,
    get_risk_index_calc_tree)


def _get_index_tendency(DPID, TYPE):
    documents = list(mongo.db['monthly_detail_diaoche_index'].find(
        {
            'DPID': DPID,
            'TYPE': TYPE,
        }, {
            "_id": 0,
            "MON": 1,
            "SCORE": 1
        }))
    result = {}
    for document in documents:
        date = document['MON']
        result.update({date: document['SCORE']})
    x = []
    y = []
    for month in get_range_months():
        x.append('{}-{}'.format(str(month)[:4], str(month)[4:]))
        y.append(result.get(month, 0))
    return {
        'DATE': x,
        'COUNT': y,
    }


def _get_index_name(MAIN_TYPE):
    _map = {
        1: 'check_intensity',
        2: 'evaluate_intensity',
        4: 'check_balance',
        3: 'rate_intensity',
        5: 'issue_exposure',
        6: 'issue_fix',
    }
    if MAIN_TYPE in _map:
        return _map.get(MAIN_TYPE, '-')


def _get_child_index_name(DETAIL_TYPE, MAIN_TYPE):
    _check_intensity_map = {
        2: '换算单位检查频次',
        3: '查处问题率',
        4: '查处问题考核率',
        5: '换算人均问题质量分',
        6: '较大风险问题质量均分',
        7: '夜查率',
        8: '一般风险问题及以上占比',
        9: '覆盖率',
        10: '监控调阅力度',
        11: '问题平均质量分',
        12: '跟班率',
    }
    _evaluate_intensity_map = {
        1: '主动评价记分占比',
        2: '干部人均主动评价记分条数',
        3: '干部人均主动评价记分分数',
        4: '评价职务占比',
        5: '段机关干部占比',
        6: '分析中心得分',
        7: '评价集中度',
        8: '评价占比',
        9: '履职评价条数差（站段与路局）'
    }
    _assess_intensity_map = {
        1: '换算人均考核问题数',
        2: '换算人均考核金额',
        3: '返奖率',
    }
    _check_evenness_map = {
        1: '问题均衡度',
        2: '检查时间均衡度',
        3: '检查地点均衡度',
    }

    _problem_exposure_map = {
        1: '普遍性暴露度',
        2: '较严重隐患暴露',
        3: '事故隐患问题暴露度',
        4: '班组问题暴露度',
        5: '他查问题扣分',
        7: '分析中心查处问题暴露',
    }

    _problem_rectification_map = {
        1: '整改时效',
        2: '整改履职',
        3: '整改成效',
        4: '整改复查',
        5: '隐患复查',
        6: '整改成效',
    }

    map_dict = [
        _check_intensity_map, _evaluate_intensity_map, _assess_intensity_map,
        _check_evenness_map, _problem_exposure_map, _problem_rectification_map
    ]
    _map = map_dict[MAIN_TYPE - 1]
    if DETAIL_TYPE in _map:
        return _map.get(DETAIL_TYPE, None)


def _get_sorted_dp(data):
    data = {item[2]: f'{item[4]:.2f}/{int(item[3])}' for item in data}
    rst = []
    global MON
    for x in MON:
        rst.append(data.get(x, '0/0'))
    return rst


def _get_mon(mon):
    mon.sort()
    rst = ["DATE"]
    global MON
    MON = mon
    mon = ['{}-{:0>2}'.format(item // 100, item % 100) for item in mon]
    rst.extend(mon)
    return rst


def get_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {
        "DEPARTMENT_ID": param_dict['DPID'],
        "MAJOR": get_major_dpid(param_dict['DPID']),
        "TYPE": int(param_dict['TYPE']),
    }
    documents = mongo.db['monthly_detail_major_index'].find(
        condition, {
            "_id": 0,
            "MAJOR": 0,
            "TYPE": 0,
            "DEPARTMENT_NAME": 0,
            "DEPARTMENT_ID": 0,
            "HIERARCHY": 0,
        })
    mon = list(mongo.db['monthly_detail_major_index'].distinct('MON'))
    data = pd.DataFrame(list(documents))
    if len(data) == 0:
        return "NO DATA"
    rst_all = []
    groups = data[data['DETAIL_TYPE'] == 0].groupby(['MAIN_TYPE'])
    rst_tmp = []
    rst_tmp.append(_get_mon(mon))
    for k, v in groups:
        rst_tmp.append([_get_index_name(k), *(_get_sorted_dp(v.values))])
    rst_all.append(rst_tmp)
    for main_type in range(1, 7):
        rst_tmp = []
        rst_tmp.append(_get_mon(mon))
        groups = data[data['MAIN_TYPE'] == main_type].groupby(['DETAIL_TYPE'])
        for k, v in groups:
            _title = _get_child_index_name(k, main_type)
            if _title is None:
                continue
            rst_tmp.append([_title, *(_get_sorted_dp(v.values))])
        rst_all.append(rst_tmp)
    return rst_all


def calc_tree(param_dict):
    # mon = int(param_dict['MON'])
    if 'MONTH' not in param_dict:
        mon = get_history_months(-1)[0]
    else:
        mon = int(param_dict['MONTH'])
    index_type = int(param_dict['TYPE'])
    major = param_dict['MAJOR']
    risk_type = int(param_dict['RISK_TYPE'])
    dpid = param_dict['DPID']
    hierarchy = get_hierarchy_dpid(dpid)
    if hierarchy is None:
        return "DPID不存在"
    if index_type not in [0, 1, 2, 3, 4, 5, 6]:
        return "该指数还在开发中"
    rtn_html = {}
    if index_type == 0:
        tree_child = []
        for child_index_type in [1, 2, 3, 4, 5, 6]:
            child_rst = get_risk_index_calc_tree(dpid, mon, major, risk_type,
                                                 child_index_type)
            if child_rst is not None:
                tree_child.append(child_rst)
        rtn_html.update({'name': '重点风险指数', 'children': tree_child})
    else:
        rtn_html = get_risk_index_calc_tree(dpid, mon, major, risk_type,
                                            index_type)

    return rtn_html


if __name__ == '__main__':
    pass
