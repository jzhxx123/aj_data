#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from app import mongo
from dateutil.relativedelta import relativedelta
from datetime import datetime as dt
from app.story.util import get_hierarchy_dpid, get_range_days, get_range_months
from app.utils.common_func import (
    choose_collection,
    choose_collection_prefix,
    get_last_year,
)


def statistics_count(data, type, flag, param_dict):
    res_count = {}
    res_detail = {}
    pk_id_list = []
    for each in data:
        if each['PK_ID'] in pk_id_list:
            continue
        pk_id_list.append(each['PK_ID'])
        if each['MAIN_TYPE'] == type or type == 0:
            if flag:
                mon = each['MON']
            else:
                mon = f'{each["DATE"] // 100 % 100}/{each["DATE"] % 100}'
            if mon in res_count:
                res_count.update({mon: res_count.get(mon) + 1})
            else:
                res_count.update({mon: 1})
            tmp = []
            if mon in res_detail:
                tmp = res_detail.get(mon)
            if type == 1:
                tmp.append([
                    each['DETAIL_TYPE'],
                    each['RESPONSIBILITY_NAME'],
                    1 if each['CATEGORY'] <= 105 else 2  # B类事故以上或B类一下
                ])
            elif type == 0:
                tmp.append([each['RESPONSIBILITY_NAME']])
            else:
                tmp.append([each['RESPONSIBILITY_NAME'], each['RANK']])

            res_detail.update({mon: tmp})
    res_date = []
    res_size = []
    res_item = []
    if flag:
        for mon in get_range_months(
                int(param_dict['START_MONTH']), int(param_dict['END_MONTH'])):
            res_date.append('{}-{}'.format(str(mon)[:4], str(mon)[4:]))
            if mon in res_count:

                res_size.append(res_count.get(mon))
                res_item.append(res_detail.get(mon))
            else:
                res_size.append(0)
                res_item.append([])
    else:
        for day in get_range_days(int(param_dict['START_MONTH'])):
            res_date.append(day)
            if day in res_count:
                res_size.append(res_count.get(day))
                res_item.append(res_detail.get(day))
            else:
                res_size.append(0)
                res_item.append([])

    return res_date, res_size, res_item


def get_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    # 判断是按月还是按日统计
    flag = True
    if (param_dict['END_MONTH'] == param_dict['START_MONTH']) and (
            param_dict['START_MONTH'] != ""):
        flag = False
    records = []
    for _prefix in choose_collection(param_dict):
        coll_name = '{}detail_safety_produce_info'.format(_prefix)
        records.extend(
            list(mongo.db[coll_name].find(
                {
                    "TYPE{}".format(hierarchy): param_dict['DPID'],
                    "MON": {
                        "$lte": int(param_dict['END_MONTH']),
                        "$gte": int(param_dict['START_MONTH'])
                    }
                }, {
                    "_id": 0,
                    "PK_ID": 1,
                    "RANK": 1,
                    "MAIN_TYPE": 1,
                    "DETAIL_TYPE": 1,
                    "RESPONSIBILITY_NAME": 1,
                    "CATEGORY": 1,
                    "MON": 1,
                    "DATE": 1,
                })))
    item = [['行车', 1], ['路外', 2], ['劳安', 3], ['责任', 1], ['非责任', 2], ['严重', 1],
            ['较严重', 2], ['一般严重', 3], ['低', 4], ['B类及以上', 1], ['B类以下', 2]]
    result = {
        'range_month': get_last_year(),
        'item': item,
        'total': list(statistics_count(records, 0, flag, param_dict)),
        'shigu': list(statistics_count(records, 1, flag, param_dict)),
        'guzhang': list(statistics_count(records, 2, flag, param_dict)),
        'zonghexinxi': list(statistics_count(records, 3, flag, param_dict)),
    }

    return result


def get_detail_data(param_dict):
    hierarchy = get_hierarchy_dpid(param_dict['DPID'])
    if hierarchy is None:
        return "DPID不存在"
    condition = {
        "TYPE{}".format(hierarchy): param_dict['DPID'],
    }
    _date = param_dict['MON'].replace('-', '')
    # 判断当月还是历史月份
    coll_prefix = choose_collection_prefix(int(_date[:6]))

    if len(_date) > 6:  # 按日统计
        seq_loc = _date.find('/')
        if seq_loc >= 0:
            _date = f'{_date[:4]}{_date[6:8]}{_date[seq_loc+1:]:0>2}'
        if int(_date[-2:]) >= 25:
            tmp_date = dt(
                year=int(_date[:4]),
                month=int(_date[4:6]),
                day=int(_date[-2:]))
            tmp_date += relativedelta(months=1)
            coll_prefix = choose_collection_prefix(tmp_date.year * 100 +
                                                   tmp_date.month)
        condition.update({"DATE": int(_date)})
    else:
        condition.update({"MON": int(_date)})
    for param in ['MAIN_TYPE', 'RANK', 'RESPONSIBILITY_NAME', 'DETAIL_TYPE']:
        if param in param_dict:
            if param == 'RANK' and param_dict.get('MAIN_TYPE', 0) == 1:
                if param_dict['RANK'] == 1:
                    condition.update({'CATEGORY': {"$lte": 105}})
                else:
                    condition.update({'CATEGORY': {"$gt": 105}})
            else:
                condition.update({param: int(param_dict[param])})

    records = list(
        mongo.db['{}detail_safety_produce_info'.format(coll_prefix)].find(
            condition, {
                "_id": 0,
                "OVERVIEW": 1,
                "REASON": 1,
                "RESPONSIBILITY_UNIT": 1,
                "PK_ID": 1,
                "DATE": 1,
                "RANK": 1,
                "RESPONSIBILITY_NAME": 1,
                "MAIN_TYPE": 1,
                "DETAIL_TYPE": 1,
                "CATEGORY": 1,
            }))
    result = []
    pk_id_list = []
    for record in records:
        if record['PK_ID'] in pk_id_list:
            continue
        pk_id_list.append(record['PK_ID'])
        record['RESPONSIBILITY_NAME'] = '-' if record[
            'RESPONSIBILITY_NAME'] == 0 else record['RESPONSIBILITY_NAME']
        record['RESPONSIBILITY_UNIT'] = '-'
        # record['RESPONSIBILITY_UNIT'] = '-' if record['RESPONSIBILITY_UNIT']
        # == 0 else record['RESPONSIBILITY_UNIT']
        record['REASON'] = '-' if record['REASON'] == 0 else record['REASON']
        record['OVERVIEW'] = '-' if record['OVERVIEW'] == 0 else record[
            'OVERVIEW']
        record['DATE'] = '{}/{}/{}'.format(
            str(record['DATE'])[:4],
            str(record['DATE'])[4:6],
            str(record['DATE'])[6:])
        record['DETAIL_TYPE'] = {
            1: '行车',
            2: '路外',
            3: '劳安'
        }.get(record['DETAIL_TYPE'], '-')
        record['MAIN_TYPE'] = {
            1: '事故',
            2: '故障',
            3: '综合信息'
        }.get(record['MAIN_TYPE'], '-')
        record['RESPONSIBILITY_NAME'] = {
            1: '责任',
            2: '非责任'
        }.get(record['RESPONSIBILITY_NAME'], '-')
        if 'MAIN_TYPE' in param_dict and param_dict['MAIN_TYPE'] == 1:
            record['RANK'] = {
                101: '特别重大',
                102: '重大',
                103: '较大',
                104: 'A类事故',
                105: 'B类事故',
                106: 'C类事故',
                107: 'D类事故',
            }.get(record['CATEGORY'], '-')
        else:
            record['RANK'] = {
                1: '严重',
                2: '较严重',
                3: '一般严重',
                4: '低'
            }.get(record['RANK'], '-')
        result.append(record)
    return result


if __name__ == '__main__':
    pass
