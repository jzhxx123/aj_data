#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    电务大屏API数据
    Author: WeiBo
    Date: 2018/10/17
    Desc: Dianwu big_screen all API
    Method: Find the API function from get_data on the bottom
'''
import json
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app import mongo
from app.new_big.util import (
    get_check_info_data, get_check_pro_data, get_data_from_mongo_by_find,
    get_major, get_pie_data, get_twelve_month, singe_score_section)
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day


def get_station(short_name):
    stations = {
        "all": "all",
        "gybd": "贵阳北电务段",
        "dzd": "达州电务段",
        "gyd": "贵阳电务段",
        "cqd": "重庆电务段",
        "cdd": "成都电务段",
        "cdtx": "成都通信段"
    }
    return stations.get(short_name)


def trans_to_dianwuname(type3):
    # 获取电务段的名称，注之后通过连表查找mongo库中的base_department获取专业下设段的相关数据
    name_dict = {
        "19B8C3534E075665E0539106C00A58FD": "成都电务段",
        "19B8C3534E175665E0539106C00A58FD": "重庆电务段",
        "19B9D8D920DC589FE0539106C00A1189": "达州电务段",
        "19B8C3534E3A5665E0539106C00A58FD": "贵阳电务段",
        "19B8C3534E3A566512345106C00A58FD": "贵阳北电务段",
        "19B8C3534DE85665E0539106C00A58FD": "成都通信段"
    }
    dw_name = name_dict[type3]
    return dw_name


duan_type3_li = [
    "19B8C3534E075665E0539106C00A58FD", "19B8C3534E175665E0539106C00A58FD",
    "19B9D8D920DC589FE0539106C00A1189", "19B8C3534E3A5665E0539106C00A58FD",
    "19B8C3534E3A566512345106C00A58FD", "19B8C3534DE85665E0539106C00A58FD"
]


def trans_to_type3(duan_name):
    type3_dict = {
        "贵阳北电务段": "19B8C3534E3A566512345106C00A58FD",
        "达州电务段": "19B9D8D920DC589FE0539106C00A1189",
        "贵阳电务段": "19B8C3534E3A5665E0539106C00A58FD",
        "重庆电务段": "19B8C3534E175665E0539106C00A58FD",
        "成都电务段": "19B8C3534E075665E0539106C00A58FD",
        "成都通信段": "19B8C3534DE85665E0539106C00A58FD"
    }
    return type3_dict[duan_name]


def _get_days_without_accident_dianwu(major):
    '''无责任计算天数'''
    major = major[0]
    days_pipeline = {  # 无B类责任事故天数
        "match": {
            "MAJOR": major,
            "HIERARCHY": 1
        },
        "project": {
            "_id": 0,
            "VALUE": 1,
            "TYPE": 1
        }
    }
    days_prefix = 'without_accident_data'
    days_documents = get_data_from_mongo_by_find("", days_pipeline,
                                                 days_prefix)
    a_data = mongo.db.base_data.find_one({'TYPE': 'days_without_accident'})
    if len(days_documents) == 0:
        current_app.logger.debug("无责任计算ABC天数获取数据失败")
        return
    else:
        data = pd.DataFrame(days_documents)
        if dt.now().hour == 18:
            a = a_data['VALUE'] + 1
            b = list(data[data['TYPE'] == 'B']['VALUE'])[0] + 1
            c = list(data[data['TYPE'] == 'C']['VALUE'])[0] + 1
        else:
            a = a_data['VALUE']
            b = list(data[data['TYPE'] == 'B']['VALUE'])[0]
            c = list(data[data['TYPE'] == 'C']['VALUE'])[0]
        return {"A": a, "B": b, "C": c}


def _get_safety_produce_notification(major):
    '''安全生产信息通知接口'''
    major = major[0]
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    # yesterday = 20180801  # 开发使用日期，实际应用上面的日期
    # _prefix = get_coll_prefix(yesterday)
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            'DATE': yesterday,
            'MAJOR': major
        },
        "project": {
            '_id': 0,
            'OVERVIEW': 1,
            'LINE_NAME': 1,
            'MAIN_CLASS': 1,
            'DETAIL_CLASS': 1,
            'BELONG': 1,
            'PK_ID': 1,
            'CLOCK': 1,
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        records = []
        tmp_id_set = set()
        for record in documents:
            if record['PK_ID'] in tmp_id_set:
                continue
            else:
                tmp_id_set.add(record['PK_ID'])
            tags = []
            if record['DETAIL_CLASS'] != 0:
                tags = [
                    record['DETAIL_CLASS'], record['LINE_NAME'],
                    record['BELONG']
                ]
            else:
                tags = [
                    record['MAIN_CLASS'], record['LINE_NAME'], record['BELONG']
                ]
            record['content'] = record['OVERVIEW']
            record['tags'] = tags
            record['date'] = '{}/{:0>2}/{:0>2} {}'.format(
                str(yesterday)[:4],
                str(yesterday)[4:6],
                str(yesterday)[6:], record['CLOCK'])
            del record['LINE_NAME'], record['MAIN_CLASS'], record[
                'DETAIL_CLASS'], record['BELONG']
            del record['CLOCK']
            del record['OVERVIEW']
            del record['PK_ID']
            records.append(record)
        return records
    else:
        return


def _get_auto_notice_data(major):
    # 长期未出现安全隐患暂无
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    # today = 20180701
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 3,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID']).reset_index()
        documents = json.loads(data.to_json(orient='records'))
        global_rst = []
        for item in documents:
            global_rst.append({
                "content":
                item['CONTENT'],
                "tags": [item['NAME']],
                "date":
                str(item['START_DATE'])[:4] + '/' + str(
                    item['START_DATE'])[4:6] + '/' + str(
                        item['START_DATE'])[6:]
            })
        return global_rst
    else:
        return


def _get_safe_notice_data(major):
    '''获取安全警告通知数据'''
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 1,
            "DUTY_NAME": {
                "$regex": major
            }
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "DUTY_NAME": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("电务专业今日无安全警告信息")
        global_rst = []
    else:
        data = pd.DataFrame(documents)
        global_rst = []
        for index in data.index:
            global_rst.append({
                "content":
                data.loc[index].at['CONTENT'],
                "tags": [data.loc[index].at['DUTY_NAME']],
                "date":
                str(data.loc[index].at['START_DATE'])[:4] + '/' + str(
                    data.loc[index].at['START_DATE'])[4:6] + '/' + str(
                        data.loc[index].at['START_DATE'])[6:]
            })
    return global_rst


def _get_accident_error_info(major):
    major = major[0]
    today = get_today()
    to_month = get_date(today)
    to_month = 201809
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            'MON': to_month,
            'MAJOR': major
        },
        "project": {
            '_id': 0,
            'RESPONSIBILITY_UNIT': 1,
            'PK_ID': 1,
            "MAIN_TYPE": 1,
            "MON": 1,
            "STATUS": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("电务事故故障无当月安全生产信息")
        return []
    else:
        result = []
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID', 'RESPONSIBILITY_UNIT'])
        data = data[data['STATUS'] != 8]
        data = data[(data['MAIN_TYPE'] == 1) | (data['MAIN_TYPE'] == 2)]
        data = data[data['RESPONSIBILITY_UNIT'] != 0]
        total = len(data)
        shi_total = len(data[data['MAIN_TYPE'] == 1])
        gu_total = len(data[data['MAIN_TYPE'] == 2])
        if shi_total == 0:
            first = ''
            second = ''
        else:
            first = f',其中事故{shi_total}件'
            if gu_total == 0:
                second = ''
            else:
                second = f',故障{gu_total}件'
        major_content = f'''电务系统：
                            本月，电务系统共发生设备故障{total}件
                            ''' + first + second + '。'
        major_content = major_content.replace("\n", "").replace(" ", "")
        result.append({"content": major_content})
        data = data.groupby('RESPONSIBILITY_UNIT')
        for name, group in data:
            total = len(group)
            shi_count = len(group[group['MAIN_TYPE'] == 1])
            gu_count = len(group[group['MAIN_TYPE'] == 2])
            if shi_count == 0:
                first = ''
                second = ''
            else:
                first = f',其中事故{shi_count}件'
                if gu_count == 0:
                    second = ''
                else:
                    second = f',故障{gu_count}件'
            content = f'{name}： 本月共发生设备故障{total}件' + first + second + '。'
            result.append({"content": content})
        return result


def handel_problem_point_statistics_dianwu_df(df_all, df_self):

    if df_all.empty:
        current_app.logger.debug("无电务专业问题项点数据")
        super_li = []
        self_li = []
        count_li = []
        i = 10
        while i > 0:  # 固定返回十条数据 注意排序返回是最小值在最前
            self_li.append({"label": " ", "amount": 0})
            super_li.append({"label": " ", "amount": 0})
            count_li.append(0)
            i -= 1
        data_dict = {
            '自查问题个数': self_li,
            '上级检查问题个数': super_li,
            "count": count_li
        }
        return data_dict

    df_all = df_all[(df_all["PROBLEM_POINT"].notnull())]
    all_num = df_all['PROBLEM_POINT'].value_counts()
    if df_self.empty:
        current_app.logger.debug("无自查问题项点数据")
        self_num = {}
    else:
        df_self = df_self[(df_self["PROBLEM_POINT"].notnull())]
        self_num = df_self["PROBLEM_POINT"].value_counts()

    df_num = pd.DataFrame({
        "all_num": dict(all_num),
        # "self_num": dict(all_num-self_num),
        "super_num": dict(self_num)
    })
    df_num.fillna(0, inplace=True)
    df_num['self_num'] = df_num['all_num'] - df_num['super_num']
    df_sort = df_num.sort_values(by='all_num', ascending=False)
    pro_point = list(df_sort.index)
    self_check = list(df_sort["self_num"].values)
    super_check = list(df_sort["super_num"].values)
    all_check = list(df_sort["all_num"].values)
    super_li = []
    self_li = []
    count_li = []
    i = 10
    while i > 0:  # 固定返回十条数据 注意排序返回是最小值在最前
        if i < len(pro_point):
            super_li.append({
                "label": pro_point[i],
                "amount": int(super_check[i])
            })
            self_li.append({
                "label": pro_point[i],
                "amount": int(self_check[i])
            })
            count_li.append(int(all_check[i]))
        else:
            self_li.append({"label": " ", "amount": 0})
            super_li.append({"label": " ", "amount": 0})
            count_li.append(0)
        i -= 1
    data_dict = {'自查问题个数': self_li, '上级检查问题个数': super_li, "count": count_li}
    return data_dict


def _get_problem_point_statistics_dianwu(major):
    # 问题项点统计
    data = []
    duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    keys = {
        "match": {
            'MON': month,
            "MAJOR": major
        },
        "project": {
            "PROBLEM_POINT": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "TYPE3": 1,
            "TYPE": 1
        }
    }
    coll = 'detail_check_problem'
    res_data = get_data_from_mongo_by_find(coll, keys)
    df = pd.DataFrame(list(res_data))
    if df.empty:
        current_app.logger.debug("daily_detail_check_problem无数据")
        return data
    df1 = df.copy()
    df1 = df1[(df1["DEPARTMENT_ALL_NAME"].notnull())]
    s_check_num = df1[(df1['TYPE'] == 1) | (df1['TYPE'] == 2)]  # 全局上级数量
    if duan == "all":
        result = handel_problem_point_statistics_dianwu_df(df1, s_check_num)
        data.append({"title": "电务系统", "data": result})
        for duan_type3 in duan_type3_li:
            df_all_duan = df1.loc[(df1.TYPE3 == duan_type3)]
            df_self_duan = s_check_num.loc[(s_check_num.TYPE3 == duan_type3)]
            result_duan = handel_problem_point_statistics_dianwu_df(
                df_all_duan, df_self_duan)
            data.append({
                "title": trans_to_dianwuname(duan_type3),
                "data": result_duan
            })
    else:
        df_all_duan = df1.loc[(df1.TYPE3 == trans_to_type3(duan))]
        df_self_duan = s_check_num.loc[(
            s_check_num.TYPE3 == trans_to_type3(duan))]
        result = handel_problem_point_statistics_dianwu_df(
            df_all_duan, df_self_duan)
        data.append({"title": duan, "data": result})
    return data


def handle_get_safety_produce_trendency_dianwu_data(df_all, equ_num):
    data = {}
    month_li = get_twelve_month()[1]
    month_li_int = [int(i.replace("/", "")) for i in month_li]
    huansuanzushu = equ_num  # 无道岔换算组数  故障率暂时无法计算
    if df_all.empty:
        return data
    df_zrsg_gz = df_all.loc[((df_all.MAIN_TYPE == 1) &
                             (df_all.RESPONSIBILITY_NAME == 1))
                            | (df_all.MAIN_TYPE == 2)]  # 责任事故，故障总
    if df_zrsg_gz.empty:
        return data
    shigu_dict = dict(df_zrsg_gz["MON"].value_counts())
    df_guzhang = df_all.loc[(df_all.MAIN_TYPE == 2)]
    if df_guzhang.empty:
        guzhang_dict = {}
    else:
        guzhang_dict = dict(df_guzhang["MON"].value_counts())
    for mon in month_li_int:
        if mon not in shigu_dict.keys():
            shigu_dict[mon] = 0
        if mon not in guzhang_dict.keys():
            guzhang_dict[mon] = 0
    df_res = pd.DataFrame({
        "shigu": shigu_dict,
        "guzhang": guzhang_dict
    })
    df_res.fillna(0, inplace=True)
    data["故障率(%)"] = [
        round(int(i) / huansuanzushu * 100, 1)
        for i in list(df_res["guzhang"].values)
    ]
    # data["局控目标"] = 暂时无数据
    data["事故（责任）、故障件数"] = [int(i) for i in list(df_res["shigu"].values)]
    return data


def handel_equipment_safety_risk_data():
    data = [{
        "data": {
            "date": ["成都电务段", "重庆电务段", "贵阳电务段", "达州电务段", "成都通信段", "贵阳北电务段"],
            "本月报警": [20, 30, 40, 50, 60, 70],
            "上月报警": [20, 30, 40, 50, 60, 70],
            "table": {
                "data": {
                    "本月报警": ["本月报警", 20, 30, 40, 50, 60, 70],
                    "上月报警": ["上月报警", 20, 30, 40, 50, 60, 70],
                    "问题发现率": ["问题发现率", 20, 30, 40, 50, 60, 70],
                },
                "title":
                ["成都电务段", "重庆电务段", "贵阳电务段", "达州电务段", "成都通信段", "贵阳北电务段"]
            }
        },
        "title": "所有设备"
    },
        {
        "data": {
            "date":
            ["成都电务段", "重庆电务段", "贵阳电务段", "达州电务段", "成都通信段", "贵阳北电务段"],
            "本月报警": [20, 30, 40, 50, 60, 70],
            "上月报警": [20, 30, 40, 50, 60, 70],
            "table": {
                "data": {
                    "本月报警": ["本月报警", 20, 30, 40, 50, 60, 70],
                    "上月报警": ["上月报警", 20, 30, 40, 50, 60, 70],
                    "问题发现率": ["问题发现率", 20, 30, 40, 50, 60, 70],
                },
                "title": [
                    "成都电务段", "重庆电务段", "贵阳电务段", "达州电务段", "成都通信段",
                    "贵阳北电务段"
                ]
            }
        },
        "title": "道岔"
    }]
    return data


def get_error_rate(station):
    error_rate = {
        "重庆电务段": 366.76,
        "贵阳电务段": 333.01,
        "成都电务段": 607.99,
        "贵阳北电务段": 372.03,
        "达州电务段": 127.59,
        "成都通信段": 853.37,
        "all": 2642.75
    }
    return error_rate[station]


def get_local_control_target(station):
    local_target = {
        "重庆电务段": [7, 5, 6, 7, 7, 7, 7, 7, 7, 7, 7, 6],
        "贵阳电务段": [5, 4, 5, 4, 6, 5, 5, 5, 5, 5, 5, 4],
        "成都电务段": [2, 4, 2, 4, 4, 5, 6, 5, 5, 4, 3, 3],
        "贵阳北电务段": [4, 7, 5, 4, 5, 5, 5, 5, 4, 4, 4, 3],
        "达州电务段": [2, 3, 2, 4, 4, 4, 4, 4, 4, 3, 3, 2],
        "成都通信段": [1, 0, 2, 0, 1, 1, 0, 1, 1, 1, 1, 0],
        "all": [35, 38, 34, 34, 40, 41, 42, 41, 45, 40, 34, 32]
    }
    return local_target[station]


def _get_safety_produce_trendency_dianwu(major):
    data = []
    duan = major[1]
    major = major[0]
    month_li = get_twelve_month()[1]
    result = list(
        mongo.db.monthly_detail_safety_produce_info.find(
            {
                "MAJOR": major
            }, {
                "MON": 1,
                "MAIN_TYPE": 1,
                "RESPONSIBILITY_NAME": 1,
                "RESPONSIBILITY_UNIT": 1,
                "TYPE3": 1
            }))
    df = pd.DataFrame(result)
    if df.empty:
        current_app.logger.debug("monthly_detail_safety_produce_info无事故故障数据")
        return data
    if duan == "all":
        equ_num = get_error_rate(duan)
        result = handle_get_safety_produce_trendency_dianwu_data(df, equ_num)
        result["date"] = month_li
        local_target = get_local_control_target(duan)
        result["局控目标"] = local_target[:len(month_li)]
        data.append({"title": "电务系统", "data": result})
        for duan_type3 in duan_type3_li:
            station = trans_to_dianwuname(duan_type3)
            df_duan = df.loc[(df.TYPE3 == duan_type3)]
            equ_num = get_error_rate(station)
            result_duan = handle_get_safety_produce_trendency_dianwu_data(
                df_duan, equ_num)
            result_duan["date"] = month_li
            local_target = get_local_control_target(station)
            result_duan["局控目标"] = local_target[:len(month_li)]
            data.append({"title": station, "data": result_duan})
    else:
        df_duan = df.loc[(df.TYPE3 == trans_to_type3(duan))]
        equ_num = get_error_rate(duan)
        result_duan = handle_get_safety_produce_trendency_dianwu_data(
            df_duan, equ_num)
        result_duan["date"] = month_li
        local_target = get_local_control_target(duan)
        result_duan["局控目标"] = local_target[:len(month_li)]
        data.append({"title": duan, "data": result_duan})
    result_shebei = handel_equipment_safety_risk_data()
    all_data = [data, result_shebei]
    return all_data


def _get_evaluate_table_data_dianwu(major):
    duan = major[1]
    major = major[0]
    today = get_today()
    if today >= dt(today.year, 12, 25):
        start_day = int(f'{today.year}1225')
        end_day = int(f'{today.year+1}1225')
    else:
        start_day = int(f'{today.year-1}1225')
        end_day = int(f'{today.year}1225')
    keys = {
        "match": {
            "DATE": {
                '$gte': start_day,
                '$lt': end_day
            },
            "MAJOR": major,
            "IDENTITY": "干部"
        },
        "project": {
            "_id": 0,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "PK_ID": 1,
            "SCORE": 1,
        }
    }
    coll = 'detail_evaluate_record'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("电务干部履职积分无今年数据")
    else:
        data = pd.DataFrame(documents)
        data = data.fillna("0-0")
        data = data.drop_duplicates('PK_ID')
        if duan == "all":
            data['NAME'] = data['ALL_NAME'].apply(
                lambda x: str(x).split('-')[0])
        else:
            data = data[data.ALL_NAME.str.contains(duan, regex=False)]
            data['NAME'] = data['ALL_NAME'].apply(
                lambda x: str(x).split('-')[1])
    data = data[data['NAME'] != '电务处']
    data = data.groupby(['PERSON_ID', 'NAME'])['SCORE'].sum().reset_index()
    data['LEVEL'] = data['SCORE'].apply(singe_score_section)
    data = data.groupby(['LEVEL', 'NAME']).size().reset_index()
    data['COUNT'] = data[0]
    del data[0]
    # 计算合计人次
    total_level = data.groupby(['LEVEL'])['COUNT'].sum().reset_index()
    LEVEL = [i for i in range(1, 8)]
    total_count = [
        int(total_level['COUNT'][total_level['LEVEL'] == i].values[0])
        if i in list(total_level['LEVEL']) else 0 for i in LEVEL
    ]
    total_count.append(sum(total_count))
    total_count.insert(0, "合计")
    count = data.groupby(['LEVEL']).sum(by='COUNT')
    total = list(count['COUNT'].apply(lambda x: int(x)))
    total.append(sum(total))
    # 计算单位人次
    data = data.groupby(['NAME'])
    global_rst = {}
    table_data = []
    TITLE = [
        "∑≤2", "2<∑≤4", "4<∑≤6", "6<∑≤8", "8<∑≤10", "10<∑≤12", "∑>12",
        "合计"
    ]
    for name, group in data:
        group = group.groupby('LEVEL').sum(by='COUNT').reset_index()
        tr_data = [
            list(group[group['LEVEL'] == i]['COUNT'])[0]
            if i in list(group['LEVEL']) else 0 for i in range(1, len(TITLE))
        ]
        tr_data.insert(0, name)
        tr_data.append(sum(list(group['COUNT'].apply(lambda x: int(x)))))
        table_data.append(tr_data)
    table_data.append(total_count)
    global_rst['title'] = TITLE
    global_rst['data'] = table_data
    return global_rst


def _get_evaluate_issue_data_dianwu(major):
    '''履职问题分类统计'''
    station = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "ITEM_NAME": 1,
            "ALL_NAME": 1,
            "LEVEL": 1
        }
    }
    coll = 'detail_evaluate_record'
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("履职问题分类统计无当月数据")
        return []
    data = pd.DataFrame(documents)
    if station != 'all':
        data = data[data.ALL_NAME.str.contains(station, regex=False)]
        datas = [data]
        names = [station]
    else:
        datas = [data]
        names = ['电务系统']
        data['NAME'] = data['ALL_NAME'].apply(lambda x: str(x).split("-")[0])
        # data = data[data['NAME'] != "电务处"]
        data = data.groupby('NAME')
        for name, group in data:
            datas.append(group)
            names.append(name)
    result = []
    for idx, data in enumerate(datas):
        title = names[idx]
        if len(data) != 0:
            count_data = data.groupby('ITEM_NAME').size()
            count_doc = [{
                idx: int(count_data[idx])
            } if idx in list(count_data.index) else {
                idx: 0
            } for idx in list(code.values())]
            count_data = pd.DataFrame({
                'name': [list(item.keys())[0] for item in count_doc],
                'count': [list(item.values())[0] for item in count_doc]
            },
                index=[i for i in range(1, 12)])
            columns = ['name', 'count', 'count']
            count_rst = get_pie_data(columns, count_data, columns)
        else:
            count_rst = {}
        result.append({"data": count_rst, "title": title})
    return result


def _get_height_class_safety_risk_case_dianwu(major):
    '''高等级安全风险排查趋势图（风险）'''
    # 从mongoDB daily_detail_check_info表中检索
    # 电务专业（MAJOR）风险类型名称中含有data_li中的风险项共有47个风险项点
    # data_li = ['防松防脱', '高柱设备', '机械室防火', '劳安',
    #            '联锁', '设备', '施工', '数据', '驼峰', '应急'
    #            ]  # 10大高等级安全风险类型
    duan = major[1]
    today = get_today()
    mon = get_date(today)
    info_data = get_check_info_data(major, mon)
    pro_data = get_check_pro_data(major, mon)
    if len(info_data) == 0:
        return []
    else:
        # 求风险大类前十
        risk_names = get_high_risk(info_data)
        # 计算接口返回结果
        result = []
        if duan == 'all':
            result.append(
                get_table_content_by_risk("电务系统", info_data, pro_data,
                                          risk_names))
            stations = list(set(info_data.station))
            for station in stations:
                station_info = info_data[info_data['station'] == station]
                station_pro = pro_data[pro_data['station'] == station]
                result.append(
                    get_table_content_by_risk(station, station_info,
                                              station_pro, risk_names))
        else:
            result.append(
                get_table_content_by_risk(duan, info_data, pro_data,
                                          risk_names))
    return result


def get_table_content_by_risk(title, info_data, pro_data, risk_names):
    '''params: info_data: 检查信息数据
               pro_data: 检查问题数据
               risk_names: 风险列表
       return: result: 表格内容'''
    info_rst = []
    pro_rst = []
    for risk in risk_names:
        info_count = len(info_data[info_data.RISK_NAME.str.contains(
            risk, regex=False)])
        pro_count = len(pro_data[pro_data.RISK_NAMES.str.contains(
            risk, regex=False)])
        info_rst.append(info_count)
        pro_rst.append(pro_count)
    check_rate = [
        f'{round(pro_rst[i]*100/info_rst[i],1)}%' if info_rst[i] != 0 else 0
        for i in range(len(info_rst))
    ]
    table_info = info_rst.copy()
    table_pro = pro_rst.copy()
    table_info.insert(0, "检查次数")
    table_pro.insert(0, "发现问题数")
    check_rate.insert(0, "发现问题率")
    result = {
        "data": {
            "date": risk_names,
            "table": {
                "data": {
                    "检查次数": table_info,
                    "发现问题数": table_pro,
                    "问题发现率": check_rate
                },
                "title": risk_names
            },
            "检查次数": info_rst,
            "发现问题数": pro_rst
        },
        "title": title
    }
    return result


def get_high_risk(info_data):
    '''
        获取前十风险类型
        params: info_data: 带有risk_name的dataframe数据
        return: risk_names: 前十风险列表'''
    risk_list = list(info_data['RISK_NAME'])
    risks = [
        risk.split('-')[1] for i in risk_list for risk in i.split(',')
        if len(risk.split('-')) > 1
    ]
    risk_data = pd.DataFrame({"RISK": risks})
    risk_data = risk_data.groupby('RISK').size().reset_index()
    risk_data = risk_data.sort_values(0, ascending=False)
    risk_data = risk_data[~risk_data.RISK.str.contains('其他', regex=False)]
    risk_data = risk_data[risk_data['RISK'] != '0-0-0']
    if len(list(risk_data.RISK)) > 10:
        risk_names = list(risk_data.RISK)[:10]
    else:
        risk_names = list(risk_data.RISK)
    return risk_names


def get_high_risk_point(data):
    '''
        获取前十风险项点
        params: data: 带有risk_name的dataframe数据
        return: risk_names: 前十风险列表'''
    risk_list = list(data['RISK_NAME'])
    risks = [risk.split('-')[-1] for i in risk_list for risk in i.split(',')]
    risk_data = pd.DataFrame({"RISK": risks})
    risk_data = risk_data.groupby('RISK').size().reset_index()
    risk_data = risk_data.sort_values(0, ascending=False)
    risk_data = risk_data[~risk_data.RISK.str.contains('其他', regex=False)]
    risk_data = risk_data[risk_data['RISK'] != '0-0-0']
    if len(list(risk_data.RISK)) > 10:
        risk_names = list(risk_data.RISK)[:10]
    else:
        risk_names = list(risk_data.RISK)
    return risk_names


def _get_height_class_safety_risk_trendency_dianwu(major):
    '''高等级安全风险排查趋势图（月份）'''
    data_li = [
        '2017/11', '2017/12', '2018/01', '2018/02', '2018/03', '2018/04',
        '2018/05', '2018/06', '2018/07', '2018/08', '2018/09', '2018/10'
    ]
    duan = major[1]
    # now_year = get_date(get_today())//100
    # now_year = 2018
    mon = {"$gt": int(f'{now_year-1}{now_month:0>2}')}
    info_data = get_check_info_data(major, mon)
    info_data = get_high_risk_data(info_data)
    info_data = info_data.drop_duplicates(['PK_ID', 'RISK_NAME'])
    pro_data = get_check_pro_data(major, mon)
    pro_data['RISK_NAME'] = pro_data['RISK_NAMES']
    del pro_data['RISK_NAMES']
    pro_data = get_high_risk_data(pro_data)
    pro_data = pro_data.drop_duplicates(['PK_ID', 'RISK_NAME'])
    if len(info_data) == 0:
        return []
    else:
        # 计算接口返回结果
        result = []
        if duan == 'all':
            result.append(
                get_table_content_by_mon("电务系统", info_data, pro_data, data_li))
            stations = ['成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段']
            for station in stations:
                station_info = info_data[info_data['station'] == station]
                station_pro = pro_data[pro_data['station'] == station]
                result.append(
                    get_table_content_by_mon(station, station_info,
                                             station_pro, data_li))
        else:
            result.append(
                get_table_content_by_mon(duan, info_data, pro_data, data_li))
    return result


def get_high_risk_data(data):
    '''
    params: data: 原始数据
    return： data: 十大高等级安全风险数据
    '''
    risk_names = get_high_risk(data)
    datas = []
    for risk in risk_names:
        data_i = data[data.RISK_NAME.str.contains(risk, regex=False)]
        datas.append(data_i)
    data = pd.concat(datas, axis=0, sort=False)
    return data


def get_table_content_by_mon(title, info_data, pro_data, data_li):
    '''params: info_data: 检查信息数据
               pro_data: 检查问题数据
               dates: 日期列表
       return: result: 表格内容'''
    date_li = [int(date.replace('/', '')) for date in data_li]
    info_rst = []
    pro_rst = []
    for mon in date_li:
        info_count = len(info_data[info_data['MON'] == mon])
        pro_count = len(pro_data[pro_data['MON'] == mon])
        info_rst.append(info_count)
        pro_rst.append(pro_count)
    check_rate = [
        f'{round(pro_rst[i]*100/info_rst[i],1)}%' if info_rst[i] != 0 else 0
        for i in range(len(info_rst))
    ]
    table_info = info_rst.copy()
    table_pro = pro_rst.copy()
    table_info.insert(0, "检查次数")
    table_pro.insert(0, "发现问题数")
    check_rate.insert(0, "发现问题率")
    result = {
        "data": {
            "date": data_li,
            "table": {
                "data": {
                    "检查次数": table_info,
                    "发现问题数": table_pro,
                    "问题发现率": check_rate
                },
                "title": data_li
            },
            "检查次数": info_rst,
            "发现问题数": pro_rst
        },
        "title": title
    }
    return result


def _get__height_class_safety_risk_serious_category_dianwu(major):
    '''高等级安全风险严重性分类统计'''
    duan = major[1]
    today = get_today()
    mon = get_date(today)
    pro_data = get_check_pro_data(major, mon)  # 获取check_problem数据
    pro_data = pro_data.drop_duplicates(['PK_ID', 'RISK_NAMES'])
    pro_data['RISK_NAME'] = pro_data['RISK_NAMES']
    del pro_data['RISK_NAMES']
    result = []
    if duan == "all":
        result.append(get_high_value_content("电务系统", pro_data))
        stations = set(pro_data['station'])
        for station in stations:
            data = pro_data[pro_data['station'] == station]
            result.append(get_high_value_content(station, data))
    else:
        data = pro_data[pro_data['station'] == duan]
        result.append(get_high_value_content(duan, data))
    return result


def get_high_value_content(title, data):
    '''获取饼图数据结果
    params:
        title: 轮播标题
        data: dataframe数据
    return：
        result: 轮播单元饼图内容
    '''
    pro_data = get_high_risk_data(data)
    risk_names = get_high_risk_point(pro_data)
    datas = []
    for risk in risk_names:
        data = pro_data[pro_data.RISK_NAME.str.contains(risk,
                                                        regex=False)].copy()
        count = len(data)
        data = filter(lambda x: x != '0-0-0', list(data['SERIOUS_VALUE']))
        total = sum(data)
        datas.append({"name": risk, "amount": count, "value": total})
    datas = pd.DataFrame(datas)
    names = ["name", "amount", "value"]
    pie = get_pie_data(names, datas, names)
    result = {"data": pie, "title": title}
    return result


def _important_work_analysis_data_dianwu(major):
    '''重点工作履职情况'''
    # duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    score_data = get_quantify_score(major, month)
    assess_data = get_quantify_assess(major, month)
    data = pd.merge(score_data, assess_data, how='left', on='station')
    data = data[data['station'] != '电务处']
    data = data.fillna(0)
    score_rst = []
    all_rst = []
    com_rst = []
    stations = []
    for index in data.index:
        stations.append(data.at[index, 'station'])
        score_rst.append(round(data.at[index, 'score'], 2))
        all_rst.append(round(data.at[index, 'all'], 2))
        com_rst.append(round(data.at[index, 'complete'], 2))
    table_score_rst = score_rst.copy()
    table_all_rst = all_rst.copy()
    table_com_rst = com_rst.copy()
    table_score_rst.insert(0, "人均完成质量分")
    table_all_rst.insert(0, "人均细化指标计划")
    table_com_rst.insert(0, "人均细化指标完成")
    result = {
        "date": stations,
        "人均完成质量分": score_rst,
        "人均细化指标计划": all_rst,
        "人均细化指标完成": com_rst,
        "table": {
            "data": {
                "人均完成质量分": table_score_rst,
                "人均细化指标计划": table_all_rst,
                "人均细化指标完成": table_com_rst
            },
            "title": stations
        }
    }
    return result


def get_quantify_score(major, month):
    '''获取最低质量分数据'''
    keys = {
        "match": {
            "MAJOR": major,
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "REALITY_MIN_QUALITY_GRADES": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'quantify_assess_real'
    score_doc = get_data_from_mongo_by_find(coll, keys)
    if len(score_doc) == 0:
        score_data = []
    else:
        score_data = pd.DataFrame(score_doc)
        score_data = score_data.drop_duplicates('PK_ID')
        score_data = score_data[score_data['REALITY_MIN_QUALITY_GRADES'] != 0]
        score_data['station'] = score_data['ALL_NAME'].apply(
            lambda x: x.split('-')[0])
        score_data = score_data.groupby(
            'station')['REALITY_MIN_QUALITY_GRADES'].mean().reset_index()
        score_data['score'] = score_data['REALITY_MIN_QUALITY_GRADES']
        del score_data['REALITY_MIN_QUALITY_GRADES']
    return score_data


def get_quantify_assess(major, month):
    '''获取量化考核实时数据'''
    keys = {
        "match": {
            "MAJOR": major,
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "ID_CARD": 1,
            "COMPLETE": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'quantization_refine_quota'
    doc = get_data_from_mongo_by_find(coll, keys)
    if len(doc) == 0:
        data = []
    else:
        data = pd.DataFrame(doc)
        data = data.drop_duplicates('PK_ID')
        del data['PK_ID']
        data = data.fillna(2)
        data = data[data['COMPLETE'] != 2]
        data['station'] = data['ALL_NAME'].apply(lambda x: x.split('-')[0])
        data_all = data.groupby(['station', 'ID_CARD']).size().reset_index()
        data_all = data_all.groupby('station')[0].mean().reset_index()
        data_com = data[data['COMPLETE'] == 1]
        data_com = data_com.groupby(['station',
                                     'ID_CARD']).size().reset_index()
        data_com = data_com.groupby('station')[0].mean().reset_index()
        data_com['complete'] = data_com[0]
        data_all['all'] = data_all[0]
        del data_all[0], data_com[0]
        data = pd.merge(data_all, data_com, how='left', on='station')
    return data


def _get_desobey_rule_situation_data_dianwu(major):
    '''"两违管理情况"'''
    today = get_today()
    last_day = today - relativedelta(years=1)
    ji_day = today - relativedelta(months=3)
    last_month = get_date(last_day)  # 去年月份
    mon = {"$gt": last_month}
    now_month = get_date(today)  # 当前月份
    ji_month = get_date(ji_day)  # 季度月份
    data = get_check_pro_data(major, mon)  # 年度数据
    now_data = data[data['MON'] == now_month]  # 当前月份数据
    ji_data = data[data['MON'] > ji_month]  # 季度月份数据
    datas = [now_data, ji_data, data]
    result = []
    titles = ["月", "季", "年"]
    for idx, data_i in enumerate(datas):
        title = titles[idx]
        rst = get_two_content(title, data_i)
        result.append(rst)
    return result


def get_two_content(title, data):
    '''两违管理情况结果
    params: title: 单元结果title
            data: 传入数据
    return: rst: 单元结果'''
    data['chejian'] = data['ALL_NAME'].apply(lambda x: x.split("-")[1])
    data['jian_name'] = data['DEPARTMENT_ALL_NAME'].apply(
        lambda x: x.split("-")[-1])
    data['type'] = data.apply(
        lambda x: 'zc' if x['chejian'] == x['jian_name'] else 'sc', axis=1)
    data_i = data[data.LEVEL.str.contains("A", regex=False)
                  | data.LEVEL.str.contains("B", regex=False)]
    data_i = data_i[data_i.chejian.str.contains("车间", regex=False)]
    count_data = data_i.groupby(['chejian']).size().reset_index().sort_values(
        0, ascending=False).copy()
    departs = list(count_data['chejian'])[:10]
    super_rst = []
    self_rst = []
    count = []
    for depart in departs:
        lit_da = data_i[data_i['chejian'] == depart]
        count.append(len(lit_da))
        super_rst.append({
            "label": depart,
            "amount": len(lit_da[lit_da['type'] == 'sc'])
        })
        self_rst.append({
            "label": depart,
            "amount": len(lit_da[lit_da['type'] == 'zc'])
        })
    rst = {
        "title": title,
        "data": {
            "count": count,
            "本单位自查问题责任扣分": self_rst,
            "上级检查问题责任扣分": super_rst
        }
    }
    return rst


def _get_important_check_item_data_dianwu(major):
    '''重点工作检查项目统计'''
    duan = major[1]
    now_year = get_date(get_today())//100
    mon = int(f'{now_year}{now_month:0>2}')
    data = get_check_pro_data(major, mon)
    if len(data) == 0:
        return []
    else:
        data = data[data.DESCRIPTION.str.contains("重点工作履职检查", regex=False)]
        data['station'] = data['ALL_NAME'].apply(lambda x: x.split("-")[0])
        result = []
        if duan == 'all':
            result.append(get_improtant_item_content("电务系统", data))
            stations = ['成都电务段', '重庆电务段', '贵阳电务段', '贵阳北电务段', '达州电务段', '成都通信段']
            for station in stations:
                major_data = data[data['station'] == station]
                result.append(get_improtant_item_content(station, major_data))
        else:
            result.append(get_improtant_item_content(duan, data))
        return result


def get_improtant_item_content(title, data):
    '''获取饼图数据结果
    params:
        title: 轮播标题
        data: dataframe数据
    return：
        result: 轮播单元饼图内容
    '''
    global_data = data.groupby('CHECK_ITEM_NAME').size().reset_index()
    global_data = global_data.sort_values(0, ascending=False)
    global_data = global_data[:10]
    global_data['amount'] = global_data[0]
    names = ['CHECK_ITEM_NAME', 'amount', 'amount']
    rst = get_pie_data(names, global_data, names)
    result = {"data": rst, "title": title}
    return result


def get_data(major, node, params):
    funcName = {
        # 第一屏
        'abc_duty_dw':
        _get_days_without_accident_dianwu,  # 电务专业——无责任A,B,C事故累计天数
        'safe_produce_notice_dw':
        _get_safety_produce_notification,  # 安全生产信息通知数据
        'safe_notice_dw':
        _get_safe_notice_data,  # 安全提示通知数据
        'auto_notice_dw':
        _get_auto_notice_data,  # 动提示通知数据
        'accident_error_info_dw':
        _get_accident_error_info,  # 事故，故障信息通知数据
        'problem_sta_count_dw':
        _get_problem_point_statistics_dianwu,  # 电务专业——问题项点统计
        'safety_produce_trendency_dw':
        _get_safety_produce_trendency_dianwu,  # 电务专业——安全生产趋势图
        'evaluate_table_dw':
        _get_evaluate_table_data_dianwu,  # 电务专业——干部累计记分情况
        'evaluate_issue_dw':
        _get_evaluate_issue_data_dianwu,  # 电务专业——干部当月履职问题分类统计

        # 第二屏
        "h_safety_risk_case_dw":
        _get_height_class_safety_risk_case_dianwu,
        # 电务专业02——高等安全风险排查情况
        "h_safety_risk_trendency_dw":
        _get_height_class_safety_risk_trendency_dianwu,
        # 电务专业02——高等级安全风险排查趋势图
        "h_safety_risk_serious_dw":
        _get__height_class_safety_risk_serious_category_dianwu,
        # 电务专业02——高等级安全风险严重性分类统计
        'important_work_analysis':
        _important_work_analysis_data_dianwu,  # 电务专业——电务重点工作履职情况分析
        'violation_situation_dw':
        _get_desobey_rule_situation_data_dianwu,  # 电务专业——“两违”管理情况
        'important_check_item_dw':
        _get_important_check_item_data_dianwu,  # 电务专业——重点工作检查项目统计
    }
    stations = ['all', 'gybd', 'dzd', 'gyd', 'cqd', 'cdd', 'cdtx']
    if params not in stations:
        return '站段参数错误'
    if node not in funcName:
        return 'API参数错误'
    major = get_major(major)
    station = get_station(params)
    return funcName[node]([major, station])


if __name__ == '__main__':
    pass
