#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    大屏API数据
'''
import json
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app.new_big.util import (get_data_from_mongo_by_find, get_detail_index,
                              get_major, get_pie_data)
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day


def get_staiton_name(params):
    stations = {
        'njcw': '内江车务段',
        'cdky': '成都客运段',
        'cqcz': '重庆车站',
        'zycw': '遵义车务段',
        'cqcw': '重庆车务段',
        'lpscw': '六盘水车务段',
        'sncw': '遂宁车务段',
        'gycz': '贵阳车站',
        'mycw': '绵阳车务段',
        'gycw': '贵阳车务段',
        'plcw': '涪陵车务段',
        'cqky': '重庆客运段',
        'klcw': '凯里车务段',
        'xlccz': '兴隆场车站',
        'cqbcw': '重庆北车务段',
        'cdbcz': '成都北车站',
        'cdcw': '成都车务段',
        'cdcz': '成都车站',
        'gycwd': '广元车务段',
        'gyncz': '贵阳南车站',
        'gyky': '贵阳客运段',
        'emcw': '峨眉车务段',
        'xccw': '西昌车务段',
        'dzcw': '达州车务段',
        'all': 'all'
    }
    return stations.get(params)


def _get_safe_days(major):
    station = major[1]
    major = major[0]
    keys = {  # 无B类责任事故天数
        "match": {
            "MAJOR": major,
            "STATION": station,
            "HIERARCHY": 3,
            "TYPE": "C"
        },
        "project": {
            "_id": 0,
            "TYPE": 1,
            "VALUE": 1
        }
    }
    days_prefix = 'without_accident_data'
    days_documents = get_data_from_mongo_by_find("", keys, days_prefix)
    result = {}
    for item in days_documents:
        if item['TYPE'] != 'D':
            result[item['TYPE']] = item['VALUE']
    return result


def _get_safety_produce_notification(major):
    '''安全生产信息通知接口'''
    station = major[1]
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "DUTY_NAME": {
                "$regex": major
            },
            "TYPE": 1
        },
        "project": {
            "_id": 1,
            "CONTENT": 1,
            "START_DATE": 1,
            "DUTY_NAME": 1,
            "CREATE_NAME": 1,
            "HIERARCHY": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("今日无安全警告信息")
        result = []
    else:
        data = pd.DataFrame(documents)
        if station != 'all':
            data = data[data.DUTY_NAME.str.contains(station, regex=False)]
        else:
            data = data[~data.CREATE_NAME.str.contains("车务段", regex=False)]
        if len(data) == 0:
            result = []
        else:
            result = [{
                "content":
                data.loc[index].at['CONTENT'],
                "tags": [data.loc[index].at['DUTY_NAME']],
                "date":
                '{}/{:0>2}/{:0>2}'.format(
                    str(data.loc[index].at['START_DATE'])[:4],
                    str(data.loc[index].at['START_DATE'])[4:6],
                    str(data.loc[index].at['START_DATE'])[6:])
            } for index in data.index]
    return result


def _get_auto_notice_data(major):
    # 长期未出现安全隐患暂无

    station = major[1]
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 3,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) != 0:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID', 'CONTENT']).reset_index()
        if station != 'all':
            data = data[data['NAME'] == station]
        if len(data) == 0:
            return []
        else:
            documents = json.loads(data.to_json(orient='records'))
            global_rst = []
            for item in documents:
                global_rst.append({
                    "content":
                    item['CONTENT'],
                    "tags": [item['NAME']],
                    "date":
                    str(item['START_DATE'])[:4] + '/' + str(
                        item['START_DATE'])[4:6] + '/' + str(
                            item['START_DATE'])[6:]
                })
            return global_rst
    else:
        current_app.logger.debug("车务无自动提示数据")
        return []


def _get_safe_notice_data(major):
    '''获取安全警告通知数据'''
    major = major[0]
    today = get_today()
    week_day = today - relativedelta(days=7)
    week_day = int('{}{:0>2}{:0>2}'.format(week_day.year, week_day.month,
                                           week_day.day))
    today = int('{}{:0>2}{:0>2}'.format(today.year, today.month, today.day))
    keys = {
        "match": {
            "TYPE": 2,
            "MAJOR": major,
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            }
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        data = pd.DataFrame(documents)
        documents = json.loads(data.to_json(orient='records'))
        global_rst = []
        for item in documents:
            global_rst.append({
                "content":
                item['CONTENT'],
                "tags": [],
                "date":
                str(item['START_DATE'])[:4] + '/' + str(
                    item['START_DATE'])[4:6] + '/' + str(
                        item['START_DATE'])[6:]
            })
    else:
        current_app.logger.debug("车务无今日安全提示信息")
        global_rst = []
    return global_rst


def _get_accident_error_info(major):
    station = major[1]
    major = major[0]
    yesterday = get_today() - relativedelta(days=7)
    month = get_date(yesterday)
    # yesterday = int(str(yesterday)[:10].replace("-", ''))
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            # 'DATE': {"$gte": yesterday},
            "MON": month,
            'MAJOR': major,
            'RESPONSIBILITY_NAME': 1
        },
        "project": {
            '_id': 0,
            'PK_ID': 1,
            'OVERVIEW': 1,
            'RESPONSIBILITY_UNIT': 1,
            'MAIN_CLASS': 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        records = []
        tmp_id_set = set()
        for record in documents:
            if record['PK_ID'] in tmp_id_set:
                continue
            else:
                tmp_id_set.add(record['PK_ID'])
            record['content'] = record['OVERVIEW']
            record['tags'] = [
                record['MAIN_CLASS'], record['RESPONSIBILITY_UNIT']
            ]
            del record['PK_ID'], record['RESPONSIBILITY_UNIT'], record[
                'OVERVIEW'], record['MAIN_CLASS']
            if (station == 'all'
                    or station == record['tags'][1]) and ("车务" in str(
                        record['tags'][1]) or "车站" in str(record['tags'][1])):
                records.append(record)
        return records
    else:
        current_app.logger.debug("车务无昨日事故故障信息")
        return


def _get_stress_issue_data(major):
    '''业务处突出问题'''
    station = major[1]
    major = major[0]
    documents = []
    coll = 'detail_check_problem'
    today = get_today()
    last_day = today - relativedelta(months=2)
    last_month = get_date(last_day)
    keys = {
        "match": {
            "MON": {
                "$gte": last_month
            },
            "IS_RED_LINE": 1,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "DESCRIPTION": 1,
            "ALL_NAME": 1,
            "PK_ID": 1,
            "DATE": 1,
            "PROBLEM_DIVIDE_NAMES": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) != 0:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID'])
        if station != 'all':
            data = data[data.ALL_NAME.str.contains(station, regex=False)]
        data = data.reset_index()
        result = []
        if len(data) != 0:
            for index in data.index:
                if data.loc[index].at['PROBLEM_DIVIDE_NAMES'] != '其他':
                    result.append({
                        "content":
                        data.loc[index].at['DESCRIPTION'],
                        "tags":
                        data.loc[index].at['PROBLEM_DIVIDE_NAMES'].split(","),
                        "date":
                        '{}/{}/{}'.format(
                            str(data.iloc[index].at['DATE'])[:4],
                            str(data.iloc[index].at['DATE'])[4:6],
                            str(data.iloc[index].at['DATE'])[6:])
                    })
    else:
        result = []
        current_app.logger.debug("车务无突出问题")
    return result


def _get_safe_top_title_info(major):
    major = major[0]
    owns = ['运输', '客运', '货运']
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    check_documents = []
    problem_documents = []
    check_coll = 'detail_check_info'
    problem_coll = 'detail_check_problem'
    result = []
    for own in owns:
        keys = {
            "match": {
                "DATE": yesterday,
                "OWN": {
                    "$regex": own
                },
                "MAJOR": major
            },
            "project": {
                "_id": 0,
                "PK_ID": 1
            }
        }
        days_pipeline = {  # 无B类责任事故天数
            "match": {
                "TYPE": "B",
                "chewu_own": {
                    "$regex": own
                },
                "MAJOR": major
            },
            "project": {
                "_id": 0,
                "VALUE": 1
            }
        }
        days_prefix = 'without_accident_data'
        days_documents = get_data_from_mongo_by_find("", days_pipeline,
                                                     days_prefix)
        check_documents = get_data_from_mongo_by_find(check_coll, keys)
        problem_documents = get_data_from_mongo_by_find(problem_coll, keys)
        if len(check_documents) == 0:
            check = 0
        else:
            data = pd.DataFrame(check_documents)
            data = data.drop_duplicates('PK_ID')
            check = len(data)
        if len(problem_documents) == 0:
            problem = 0
        else:
            data = pd.DataFrame(problem_documents)
            data = data.drop_duplicates('PK_ID')
            problem = len(data)
        result.append({
            "check": check,
            "issue": problem,
            "department": own,
            "days": days_documents[0]['VALUE']
        })
    return result


def _get_safe_health_index_info(major):
    '''获取站段综合安全健康指数排名'''
    duan = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(months=1)
    month = get_date(last_day)
    coll = 'health_index'
    documents = get_detail_index(major, month, coll)
    result = []
    if len(documents) != 0:
        data = pd.DataFrame(documents)
        if duan == 'all':
            data = data.sort_values('RANK', ascending=True).reset_index()
            result = []
            data['RANK'] = [x for x in range(1, len(data) + 1)]
            data = data.reset_index()
            result = [{
                "amount": int(data.loc[index].at['SCORE']),
                "name": data.loc[index].at['DEPARTMENT_NAME'],
                "rank": int(data.loc[index].at['RANK'])
            } for index in data.index]
        else:
            data = data[data['DEPARTMENT_NAME'] == duan].reset_index()
            result.append({
                "rank": data.at[0, 'DEPARTMENT_NAME'],
                "name": f'''综合得分{data.at[0,'SCORE']}''',
                'amount': f'''排名{data.at[0,'RANK']}'''
            })
            coll = 'detail_health_index'
            detail_data = pd.DataFrame(get_detail_index(major, month, coll))
            detail_data = detail_data[(detail_data['DEPARTMENT_NAME'] == duan)
                                      & (detail_data['DETAIL_TYPE'] == 0)]
            del detail_data['DETAIL_TYPE']
            label = [
                '检查力度指数', '评价力度指数', '考核力度指数', '检查均衡度指数', '问题暴露度指数', '问题整改指数',
                '安全效果（扣分）'
            ]
            detail_data['name'] = detail_data['MAIN_TYPE'].apply(
                lambda x: label[x - 1])
            detail_data = detail_data.reset_index()
            for index in detail_data.index:
                result.append({
                    "rank":
                    detail_data.at[index, 'name'],
                    "name":
                    f'''得分{detail_data.at[index,'SCORE']}''',
                    'amount':
                    f'''排名{detail_data.at[index,'RANK']}'''
                })
    else:
        current_app.logger.debug("站段综合安全健康指数无近上月数据")
    return result


def _get_safe_health_index_detail(major):
    '''获取站段综合安全健康指数排名'''
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(months=1)
    last_month = get_date(last_day)
    coll = 'detail_health_index'
    documents = get_detail_index(major, last_month, coll)
    result = []
    if len(documents) != 0:
        data = pd.DataFrame(documents)
        data = data[data['DETAIL_TYPE'] == 0]  # 详细指数
        label = ['检查力度', '评价力度', '考核力度', '检查均衡度', '问题暴露度', '问题整改']
        stations = set(list(data['DEPARTMENT_NAME']))
        for station in stations:
            # 指数计算
            data_sta = data[data['DEPARTMENT_NAME'] == station]
            data_sta = data_sta.sort_values(by=['MAIN_TYPE']).reset_index(
                drop=True)
            detail_index = list(data_sta['SCORE'])
            detail_rank = list(data_sta['RANK'])
            rst = {
                "label": label,
                "range": [0, 100],
                "value": detail_index,
                # "rank": detail_rank
            }
            result.append({"data": rst, "title": station})
    else:
        current_app.logger.debug("站段综合安全健康指数无近三月数据")
    return result


def get_risk_name(all_name):
    if all_name is not None:
        names = all_name.split("-")
        if "接发列车" in names[1]:
            return "接发列车"
        elif "调车" in names[1]:
            return "调车风险"
        elif "防溜" in names[1]:
            return "防溜风险"
        elif "设备" in names[1]:
            return "设备问题"
        elif "客运" in names[1]:
            return "客运"
        elif "货" in names[1]:
            return "货运"
        else:
            return names[1]
    else:
        return


def _get_high_quality_analysis_info(major):
    station = major[1]
    major = major[0]
    yesterday = get_today() - relativedelta(days=1)
    month = get_date(yesterday)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    xing_risks = [
        '接发列车', '调车', '防溜', '施工', '调乘一体化', '劳动安全', '路外安全', '行车安全管理', '行车专业管理',
        '行车自管设备'
    ]
    huo_risks = ['货装作业', '货装劳安', '货装消防']
    ke_risks = ['客运生产作业', '客运劳安', '客运消防']
    all_risks = [xing_risks, huo_risks, ke_risks]
    all_name = ['运输', '货运', '客运']
    keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RISK_NAME": 1,
            "ALL_NAME": 1,
            "SERIOUS_VALUE": 1,
            "CHECK_ITEM_NAMES": 1,
            "DATE": 1
        }
    }
    coll = "detail_check_problem"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        data = data.fillna({'SERIOUS_VALUE': 0, 'RISK_NAME': 'A'})
        if station != 'all':
            data = data[data.ALL_NAME.str.contains(station, regex=False)]
            ke_sta = ['成都客运段', '贵阳客运段', '重庆客运段']
            zhan_sta = ['成都车站', '重庆车站', '贵阳车站']
            if station in ke_sta:
                all_risks = [ke_risks]
                all_name = ['客运']
            elif station in zhan_sta:
                all_risks = [xing_risks, ke_risks]
                all_name = ['行车', '客运']
    if len(data) == 0:
        return []
    day_data = data[data['DATE'] == yesterday]
    result = []
    for idx, risks in enumerate(all_risks):
        name = all_name[idx]
        res = []
        for risk in risks:
            res.append([
                risk,
                len(day_data[day_data.RISK_NAME.str.
                             contains(risk, regex=False)
                             | day_data.CHECK_ITEM_NAMES.str.
                             contains(risk, regex=False)]),
                len(data[data.RISK_NAME.str.contains(risk, regex=False) | data.
                         CHECK_ITEM_NAMES.str.contains(risk, regex=False)]),
                round(
                    sum(
                        list(day_data[day_data.RISK_NAME.str.
                                      contains(risk, regex=False)
                                      | day_data.CHECK_ITEM_NAMES.str.
                                      contains(risk, regex=False)]
                             ['SERIOUS_VALUE'])), 1),
                round(
                    sum(
                        list(data[data.RISK_NAME.str.
                                  contains(risk, regex=False)
                                  | data.CHECK_ITEM_NAMES.str.contains(
                                      risk, regex=False)]['SERIOUS_VALUE'])),
                    1)
            ])
        result.append({"data": res, "title": name})
    return result


def _get_stress_risk_monitor_info(major):
    '''获取今年监督检查情况数据 '''
    station = major[1]
    if station == 'all':
        major = major[0]
    else:
        major = station
    global_rst = {}
    today = get_today()
    last_year = get_today() - relativedelta(months=11)
    date = []
    for i in range(0, 12):
        day = get_today() - relativedelta(months=i)
        date.insert(0, int(f'{day.year}{day.month:0>2}'))
    check_documents = []
    problem_documents = []
    accident_documents = []
    check_pipeline = {  # 检查信息pipeline
        "match": {
            "MON": {
                '$gte': get_date(last_year),
                "$lte": get_date(today)
            },
            "CHECK_WAY": {
                "$in": [1, 2, 3, 4]
            },
            "DEPARTMENT_ALL_NAME": {
                "$regex": major
            }
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MON": 1
        }
    }
    problem_pipeline = {  # 发现问题pipeline
        "match": {
            "MON": {
                '$gte': get_date(last_year),
                "$lte": get_date(today)
            },
            "DEPARTMENT_ALL_NAME": {
                "$regex": major
            },
            "CHECK_WAY": {
                "$in": [1, 2, 3, 4]
            }
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MON": 1,
            "PROBLEM_SCORE": 1
        }
    }
    accident_pipeline = {
        "match": {
            "MON": {
                '$gte': get_date(last_year),
                "$lte": get_date(today)
            },
            "MAIN_TYPE": 1,
            "RESPONSIBILITY_NAME": 1,
            "RESPONSIBILITY_IDENTIFIED": {
                "$in": [1, 2]
            },
            "DETAIL_TYPE": {
                "$ne": 3
            }
        },
        "project": {
            "_id": 0,
            "MON": 1,
            "PK_ID": 1
        }
    }
    if station == 'all':
        accident_pipeline['match']['PROFESSION'] = major
    else:
        accident_pipeline['match']['RESPONSIBILITY_UNIT'] = major
    check_coll = 'detail_check_info'
    check_documents = get_data_from_mongo_by_find(check_coll, check_pipeline)
    problem_coll = 'detail_check_problem'
    problem_documents = get_data_from_mongo_by_find(problem_coll,
                                                    problem_pipeline)
    accident_coll = 'detail_safety_produce_info'
    accident_documents = get_data_from_mongo_by_find(accident_coll,
                                                     accident_pipeline)
    check_data = pd.DataFrame(check_documents)
    problem_data = pd.DataFrame(problem_documents)
    accident_data = pd.DataFrame(accident_documents)
    if len(check_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年检查信息无数据")
        check_counts = [0 for mon in date]
    else:
        check_data = check_data.drop_duplicates('PK_ID')
        check_data['COUNT'] = 1
        check_data = check_data.groupby('MON')['COUNT'].sum().reset_index()
        check_counts = [
            int(check_data['COUNT'][check_data['MON'] == mon].values[0])
            if mon in list(check_data['MON']) else 0 for mon in date
        ]
    if len(problem_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年检查问题信息无数据")
        problem_counts = [0 for mon in date]
        problem_scores = [0 for mon in date]
    else:
        problem_data = problem_data.drop_duplicates('PK_ID')
        problem_data['COUNT'] = 1
        problem_data = problem_data.groupby(
            'MON')['COUNT', 'PROBLEM_SCORE'].sum().reset_index()
        problem_counts = [
            int(problem_data['COUNT'][problem_data['MON'] == mon].values[0])
            if mon in list(problem_data['MON']) else 0 for mon in date
        ]
        problem_scores = [
            int(problem_data['PROBLEM_SCORE'][problem_data['MON'] == mon].
                values[0]) if mon in list(problem_data['MON']) else 0
            for mon in date
        ]
    if len(accident_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年安全生产信息无数据")
        accdient_counts = [0 for mon in date]
    else:
        accident_data = accident_data.drop_duplicates('PK_ID')
        accident_data['COUNT'] = 1
        accident_data = accident_data.groupby('MON').sum().reset_index()
        accdient_counts = [
            int(accident_data['COUNT'][accident_data['MON'] == mon].values[0])
            if mon in list(accident_data['MON']) else 0 for mon in date
        ]
    global_rst.update({
        'date': ['{}/{}'.format(str(d)[0:4],
                                str(d)[4:]) for d in date],
        "检查人次":
        check_counts,
        "发现问题数":
        problem_counts,
        "问题质量":
        problem_scores,
        "责任事故$_2":
        accdient_counts
    })
    return global_rst


def singe_score_section(score):
    if score < 2:
        return 1
    elif score < 4:
        return 2
    elif score < 6:
        return 3
    elif score < 8:
        return 4
    elif score < 10:
        return 5
    elif score < 12:
        return 6
    else:
        return 7


def _get_evaluate_issue_pie_data(major):
    # return "数据格式不确定"
    '''履职问题分类统计'''
    station = major[1]
    major = major[0]
    today = get_today()
    # last_day = today - relativedelta(months=1)
    month = get_date(today)
    now_year = get_date(today) // 100
    # now_day = month * 100 + 24
    # last_month = get_date(last_day)
    # last_day = last_month * 100 + 25
    keys = {
        "match": {
            "MON": {
                "$gt": int('{}00'.format(now_year))
            },
            "MAJOR": major,
            "IDENTITY": "干部"
        },
        "project": {
            "_id": 0,
            "MON": 1,
            "ITEM_NAME": 1,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "SCORE": 1,
            "DATE": 1,
            "PK_ID": 1
        }
    }
    coll = 'detail_evaluate_record'
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    level = {
        "1": "0~2分 （不含2分）",
        "2": "2~4分 （不含4分）",
        "3": "4~6分 （不含6分）",
        "4": "6~8分 （不含8分）",
        "5": "8~10分 （不含10分）",
        "6": "10~12分 （不含12分）",
        "7": ">12分 （大于12分）",
    }
    codes = code.values()
    levels = level.values()
    columns = ['name', 'count', 'count']
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("履职问题分类统计无当月数据")
        return []
    data = pd.DataFrame(documents)
    data = data.drop_duplicates(['PK_ID'])
    all_data = data.copy()
    # data = data[data['DATE'] <= now_day]
    # data = data[data['DATE'] >= last_day]
    data = data[data['MON'] == month]
    if station != 'all':
        data = data[data.ALL_NAME.str.contains(station, regex=False)]
        all_data = all_data[all_data.ALL_NAME.str.contains(
            station, regex=False)]
    if len(data) != 0:
        # 按问题统计
        count_data = data.groupby('ITEM_NAME').size()
        count_doc = [{
            idx: int(count_data[idx])
        } if idx in list(count_data.index) else {
            idx: 0
        } for idx in list(code.values())]
        count_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in count_doc],
            'count': [list(item.values())[0] for item in count_doc]
        },
                                  index=[i for i in range(1, 12)])

        # 按口分段统计
        data = data.groupby('PERSON_ID')['SCORE'].sum().reset_index()
        data['LEVEL'] = data['SCORE'].apply(singe_score_section)
        level_data = data.groupby('LEVEL').size()
        level_doc = [{
            level[idx]: int(level_data[int(idx)])
        } if int(idx) in list(level_data.index) else {
            level[idx]: 0
        } for idx in list(level.keys())]
        level_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in level_doc],
            'count': [list(item.values())[0] for item in level_doc]
        },
                                  index=[i for i in range(1, 8)])
        count_rst = get_pie_data(columns, count_data, columns)
        level_rst = get_pie_data(columns, level_data, columns)
    else:
        count_data_rst = []
        level_data_rst = []
        for cod in codes:
            count_data_rst.append({
                "amount": 0,
                "name": cod,
                "rank": 0,
                "value": 0
            })
        for lev in levels:
            level_data_rst.append({
                "amount": 0,
                "name": lev,
                "rank": 0,
                "value": 0
            })
        count_rst = {"data": count_data_rst, "pivot": 0}
        level_rst = {"data": level_data_rst, "pivot": 0}
    if len(all_data) != 0:
        # 按问题统计
        count_all_data = all_data.groupby('ITEM_NAME').size()
        count_doc = [{
            idx: int(count_all_data[idx])
        } if idx in list(count_all_data.index) else {
            idx: 0
        } for idx in list(code.values())]
        count_all_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in count_doc],
            'count': [list(item.values())[0] for item in count_doc]
        },
                                      index=[i for i in range(1, 12)])

        # 全年按扣分段统计
        level_all_data = all_data.groupby(
            'PERSON_ID')['SCORE'].sum().reset_index()
        level_all_data['LEVEL'] = level_all_data['SCORE'].apply(
            singe_score_section)
        level_all_data = level_all_data.groupby('LEVEL').size()
        all_doc = [{
            level[idx]: int(level_all_data[int(idx)])
        } if int(idx) in list(level_all_data.index) else {
            level[idx]: 0
        } for idx in list(level.keys())]
        level_all_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in all_doc],
            'count': [list(item.values())[0] for item in all_doc]
        },
                                      index=[i for i in range(1, 8)])
        count_all_rst = get_pie_data(columns, count_all_data, columns)
        level_all_rst = get_pie_data(columns, level_all_data, columns)
    else:
        count_all_data_rst = []
        level_all_data_rst = []
        for cod in codes:
            count_all_data_rst.append({
                "amount": 0,
                "name": cod,
                "rank": 0,
                "value": 0
            })
        for lev in levels:
            level_all_data_rst.append({
                "amount": 0,
                "name": lev,
                "rank": 0,
                "value": 0
            })
        count_all_rst = {"data": count_all_data_rst, "pivot": 0}
        level_all_rst = {"data": level_all_data_rst, "pivot": 0}
    return [{
        "data": [{
            "title": "问题分类",
            "data": count_rst
        }, {
            "title": "扣分分类",
            "data": level_rst
        }],
        "title":
        "当月"
    },
            {
                "data": [{
                    "title": "年度记分分段",
                    "data": count_all_rst
                }, {
                    "title": "按分数统计",
                    "data": level_all_rst
                }],
                "title":
                "全年"
            }]


def _get_analysis_check_data(major):
    station = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    yesterday = today - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    info_coll = 'detail_check_info'
    pro_coll = 'detail_check_problem'
    info_keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "DATE": 1,
            "IS_YECHA": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "CHECK_WAY": 1,
            "PK_ID": 1
        }
    }
    pro_keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "DATE": 1,
            "PROBLEM_CLASSITY_NAME": 1,
            "ALL_NAME": 1,
            "PK_ID": 1,
            "LEVEL": 1,
            "RISK_LEVEL": 1,
            "IS_RED_LINE": 1,
        }
    }
    pro_documents = get_data_from_mongo_by_find(pro_coll, pro_keys)
    info_documents = get_data_from_mongo_by_find(info_coll, info_keys)
    if len(info_documents) == 0:
        current_app.logger.debug("车务当月无检查信息数据")
        info_total = 0
        day_info_total = 0
        xian_count = 0
        day_xian_count = 0
        tian_count = 0
        day_tian_count = 0
        jian_count = 0
        day_jian_count = 0
        ye_count = 0
        day_ye_count = 0
    else:
        data = pd.DataFrame(info_documents)
        data = data.fillna("aa")
        data = data.drop_duplicates('PK_ID')
        data = data[data.DEPARTMENT_ALL_NAME.str.contains(
            station, regex=False)]
        day_data = data[data['DATE'] == yesterday]
        info_total = len(data)
        day_info_total = len(day_data)
        xian_count = len(data[data['CHECK_WAY'] == 1])
        day_xian_count = len(day_data[day_data['CHECK_WAY'] == 1])
        tian_count = len(data[data['CHECK_WAY'] == 2])
        day_tian_count = len(day_data[day_data['CHECK_WAY'] == 2])
        jian_count = len(data[data['CHECK_WAY'] == 3])
        day_jian_count = len(day_data[day_data['CHECK_WAY'] == 3])
        ye_count = len(data[data['IS_YECHA'] == 1])
        day_ye_count = len(day_data[day_data['IS_YECHA'] == 1])
    if len(pro_documents) == 0:
        current_app.logger.debug("车务当月无检查问题数据")
        pro_total = 0
        day_pro_total = 0
        zuo_count = 0
        day_zuo_count = 0
        she_count = 0
        day_she_count = 0
        guan_count = 0
        day_guan_count = 0
        lu_count = 0
        day_lu_count = 0
        fan_count = 0
        day_fan_count = 0
        a_count = 0
        day_a_count = 0
        b_count = 0
        day_b_count = 0
        c_count = 0
        day_c_count = 0
        d_count = 0
        day_d_count = 0
        zhong_count = 0
        day_zhong_count = 0
        jiao_count = 0
        day_jiao_count = 0
        yi_count = 0
        day_yi_count = 0
        di_count = 0
        day_di_count = 0
        red_count = 0
        day_red_count = 0
    else:
        data = pd.DataFrame(pro_documents)
        data = data.drop_duplicates('PK_ID')
        data = data.fillna("aa")
        data = data[data.ALL_NAME.str.contains(station, regex=False)]
        day_data = data[data['DATE'] == yesterday]
        pro_total = len(data)
        day_pro_total = len(day_data)
        zuo_count = len(data[data.PROBLEM_CLASSITY_NAME.str.contains(
            '作业', regex=False)])
        day_zuo_count = len(
            day_data[day_data.PROBLEM_CLASSITY_NAME.str.contains(
                '作业', regex=False)])
        she_count = len(data[data.PROBLEM_CLASSITY_NAME.str.contains(
            '设备质量', regex=False)])
        day_she_count = len(
            day_data[day_data.PROBLEM_CLASSITY_NAME.str.contains(
                '设备质量', regex=False)])
        guan_count = len(data[data.PROBLEM_CLASSITY_NAME.str.contains(
            '管理', regex=False)])
        day_guan_count = len(
            day_data[day_data.PROBLEM_CLASSITY_NAME.str.contains(
                '管理', regex=False)])
        lu_count = len(data[data.PROBLEM_CLASSITY_NAME.str.contains(
            '路外', regex=False)])
        day_lu_count = len(
            day_data[day_data.PROBLEM_CLASSITY_NAME.str.contains(
                '路外', regex=False)])
        fan_count = len(data[data.PROBLEM_CLASSITY_NAME.str.contains(
            '反恐防暴', regex=False)])
        day_fan_count = len(
            day_data[day_data.PROBLEM_CLASSITY_NAME.str.contains(
                '反恐防暴', regex=False)])
        a_count = len(data[data.LEVEL.str.contains("A", regex=False)])
        day_a_count = len(day_data[day_data.LEVEL.str.contains(
            "A", regex=False)])
        b_count = len(data[data.LEVEL.str.contains("B", regex=False)])
        day_b_count = len(day_data[day_data.LEVEL.str.contains(
            "B", regex=False)])
        c_count = len(data[data.LEVEL.str.contains("C", regex=False)])
        day_c_count = len(day_data[day_data.LEVEL.str.contains(
            "C", regex=False)])
        d_count = len(data[data.LEVEL.str.contains("D", regex=False)])
        day_d_count = len(day_data[day_data.LEVEL.str.contains(
            "D", regex=False)])
        zhong_count = len(data[data['RISK_LEVEL'] == 1])
        day_zhong_count = len(day_data[day_data['RISK_LEVEL'] == 1])
        jiao_count = len(data[data['RISK_LEVEL'] == 2])
        day_jiao_count = len(day_data[day_data['RISK_LEVEL'] == 2])
        yi_count = len(data[data['RISK_LEVEL'] == 3])
        day_yi_count = len(day_data[day_data['RISK_LEVEL'] == 3])
        di_count = len(data[data['RISK_LEVEL'] == 4])
        day_di_count = len(day_data[day_data['RISK_LEVEL'] == 4])
        red_count = len(data[data['IS_RED_LINE'] == 1])
        day_red_count = len(day_data[day_data['IS_RED_LINE'] == 1])
    content = f'''昨日安全监督检查{day_info_total}人次，月累计{info_total}人次。
        现场检查：　昨日{day_xian_count}人次，月累计啊{xian_count}人次，
        添乘检查：　昨日{day_tian_count}人次，月累计啊{tian_count}人次，
        监控调阅：　昨日{day_jian_count}人次，月累计啊{jian_count}人次，
        夜查：　昨日{day_ye_count}人次，月累计啊{ye_count}人次。
        昨日发现{day_pro_total}个问题，月累计{pro_total}个。
        作业问题：　昨日{day_zuo_count}个，月累计{zuo_count}个，
        设备质量问题：　昨日{day_she_count}个，月累计{she_count}个，
        管理问题：　昨日{day_guan_count}个，月累计{guan_count}个，
        路外环境问题：　昨日{day_lu_count}个，月累计{lu_count}个，
        反恐防暴问题：　昨日{day_fan_count}个，月累计{fan_count}个，
        重大安全风险问题：　昨日{day_zhong_count}个，月累计{zhong_count}个，
        较大安全风险问题：　昨日{day_jiao_count}个，月累计{jiao_count}个，
        一般安全风险问题：　昨日{day_yi_count}个，月累计{yi_count}个，
        低安全风险问题：　昨日{day_di_count}个，月累计{di_count}个，
        Ａ类问题：　昨日{day_a_count}个，月累计{a_count}个，
        Ｂ类问题：　昨日{day_b_count}个，月累计{b_count}个，
        Ｃ类问题：　昨日{day_c_count}个，月累计{c_count}个，
        Ｄ类问题：　昨日{day_d_count}个，月累计{d_count}个，
        红线问题：　昨日{day_red_count}个，月累计{red_count}个。
    '''
    content = content.replace(" ", "")
    return [{"content": content}]


def get_data(major, node, params):
    funcName = {

        # common
        'safe_produce_notice': _get_safety_produce_notification,  # 安全生产信息
        'safe_notice': _get_safe_notice_data,  # 安全提示通知数据
        'auto_notice': _get_auto_notice_data,  # 自动提示通知数据
        'accident_error_info': _get_accident_error_info,  # 事故，故障信息通知数据
        'stress_issue': _get_stress_issue_data,  # 突出问题通知

        # 车务
        'safe_top_title': _get_safe_top_title_info,  # 车务顶部安全横栏数据
        'safe_health_index': _get_safe_health_index_info,  # 站段综合安全健康指数
        'high_quality_analysis':
        _get_high_quality_analysis_info,  # 高质量问题分析类型表格接口
        'stress_risk_monitor': _get_stress_risk_monitor_info,  # 专业重点风险监控
        'evaluate_issue_pie': _get_evaluate_issue_pie_data,  # 履职问题分类统计饼图
        'safe_days': _get_safe_days,  # 站段获取无责任安全生产天数
        'analysis_check': _get_analysis_check_data,  # 站段监督检查情况
        'health_index_detail': _get_safe_health_index_detail,  # 综合指数子子数
    }
    duan_names = [
        "njcw", "cdky", "cqcz", "zycw", "cqcw", "lpscw", "sncw", 'gycz', 'all',
        'mycw', 'gycw', 'plcw', 'cqky', 'klcw', 'xlccz', 'cqbcw', 'cdbcz',
        'cdcw', 'cdcz', 'gycwd', 'gyncz', 'gyky', 'emcw', 'xccw', 'dzcw'
    ]
    if node not in funcName:
        return 'API参数错误'
    if params not in duan_names:
        return '站段参数错误'
    major = get_major(major)
    station = get_staiton_name(params)
    return funcName[node]([major, station])


if __name__ == '__main__':
    pass
