from flask import jsonify, request

from app.new_big import (all_data, cheliang_data, chewu_data, dianwu_data,
                         gongdian_data, gongwu_data, jiwu_data, new_big_bp,
                         old_luju_data, xingzheng_data)


@new_big_bp.route('old/<string:node>', methods=['GET'])
def api_old_big(node):
    try:
        result = old_luju_data.get_data(node)
        if result != "参数错误":
            rs = {'data': result, 'status': 0, 'error_message': ''}
        else:
            rs = {'data': '', 'status': 1, 'error_message': result}
        return jsonify(rs)
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@new_big_bp.route('/<string:node>', methods=['GET'])
def api_new_big(node):
    try:
        result = all_data.get_data(node)
        if result != "参数错误":
            rs = {'data': result, 'status': 0, 'error_message': ''}
        else:
            rs = {'data': '', 'status': 1, 'error_message': result}
        return jsonify(rs)
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)


@new_big_bp.route('/<string:major>/<string:node>')
def api_new_major_big(major, node):
    try:
        if major == "cheliang":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = cheliang_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "chewu":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = chewu_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "dianwu":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = dianwu_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "gongwu":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = gongwu_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "gongdian":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = gongdian_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "jiwu":
            params = request.args.get("duan")
            if params is None:
                params = "all"
            result = jiwu_data.get_data(major, node, params)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        elif major == "xingzheng":
            result = xingzheng_data.get_data(node)
            rs = {'data': result, 'status': 0, 'error_message': ''}
        else:
            rs = {'data': "", 'status': 1, 'error_message': '专业参数错误'}
            return jsonify(rs)
        if result == 'API参数错误' or result == "站段参数错误":
            rs = {'data': "", 'status': 1, 'error_message': result}
            return jsonify(rs)
    except Exception as e:
        rs = {'data': '', 'status': 1, 'error_message': e.args[0]}
    return jsonify(rs)
