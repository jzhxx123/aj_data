#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    机务大屏API数据
    Author: WeiBo
    Date: 2018/10/17
    Desc: jiwu big_screen all API
    Method: Find the API function from get_data on the bottom
'''
import re
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app.new_big.util import (get_check_info_data, get_check_pro_data,
                              get_data_from_mongo_by_find, get_detail_index,
                              get_major, get_median, get_pie_data,
                              get_trendency_data, get_week_hours)
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day
stations = ['贵阳机务段', '西昌机务段', '成都机务段', '重庆机务段']


def get_station(station_id):
    station_names = {
        "all": "all",
        "gyj": "贵阳机务段",
        "xcj": "西昌机务段",
        "cdj": "成都机务段",
        "cqj": "重庆机务段"
    }
    return station_names.get(station_id)


def _get_safe_days(major):
    station = major[1]
    major = major[0]
    keys = {  # 无B类责任事故天数
        "match": {
            "TYPE": "B",
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "VALUE": 1,
            "STATION": 1
        }
    }
    days_prefix = 'without_accident_data'
    days_documents = get_data_from_mongo_by_find("", keys, days_prefix)
    data = pd.DataFrame(days_documents)
    data = data.fillna("机务系统")
    if station == 'all':
        days = list(data[data['STATION'] == '机务系统']['VALUE'])[0]  # 机务系统安全天数
        result = [{"data": {'A': days}, "title": "机务系统"}]
        # 各站段安全天数
        stations = ['成都机务段', '西昌机务段', '重庆机务段', '贵阳机务段']
        for station in stations:
            days = list(data[data['STATION'] == station]['VALUE'])[0]
            result.append({"data": {'A': days}, "title": station})
    else:
        days = list(data[data['STATION'] == station]['VALUE'])[0]  # 机务系统安全天数
        result = [{"data": {'A': days}, "title": station}]
    return result


# 督办事项
def _get_supervise_item(major):
    station = major[1]
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    result = []
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "MAJOR": major,
            "STATUS": {
                "$in": [2, 3]
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "ITEM": 1,
            "CLAIM": 1,
            "TARGET": 1,
            "LEVEL": 1,
            "RESPONSIBILITY_DEPARTMENT_NAMES": 1,
            "START_DATE": 1
        }
    }
    coll = "safety_work_supervise"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("机务接口无今日安全督办待整改信息（目前表里没有待整改数据）")
    else:
        data = pd.DataFrame(documents)
        data = data.fillna('0')
        data = data.drop_duplicates("PK_ID")
        data = data[data['RESPONSIBILITY_DEPARTMENT_NAMES'] != 0]
        data['level'] = data['LEVEL'].apply(lambda x: str(x))
        if station == "all":
            data = data[data.level.str.contains("1", regex=False)
                        | data.level.str.contains("2", regex=False)]
        else:
            big = data[data.level.str.contains("1", regex=False)
                       | data.level.str.contains("2", regex=False)]
            small = data[data['level'] == '3'].sort_values(
                'START_DATE', ascending=False)
            if len(small) > 5:
                small = small[:5]
            data = pd.concat([big, small], axis=0, sort=False)
        if len(data) == 0:
            return []
        for index in data.index:
            department = data.loc[index].at['RESPONSIBILITY_DEPARTMENT_NAMES']
            item = data.loc[index].at["ITEM"]
            requir = data.loc[index].at["CLAIM"]
            target = data.loc[index].at["TARGET"]
            content = f'''\
            <span style="color: #ff5c68">督办事项</span>: {item}\
            <span style="color: #ff5c68">督办要求</span>: {requir}\
            <span style="color: #ff5c68">责任部门</span>: {department}\n\
            <span style="color: #ff5c68">督办目标</span>: {target}'''
            content = content.replace("  ", '')
            result.append({
                "content":
                content,
                "tags": ["安全工作督办"],
                "date":
                "{}/{:0>2}/{:0>2}".format(
                    str(data.loc[index].at['START_DATE'])[:4],
                    str(data.loc[index].at['START_DATE'])[4:6],
                    str(data.loc[index].at['START_DATE'])[6:]),
                "contentIsHtml":
                True
            })
    return result


# 重点工作提示
def _get_important_work(major):
    station = major[1]
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    result = []
    today = get_today()
    last_day = today - relativedelta(days=15)
    last = int(f'{last_day.year}{last_day.month:0>2}{last_day.day:0>2}')
    today = int(str(today)[:10].replace('-', ''))
    keys = {
        "match": {
            "DATE": {
                "$lte": today,
                "$gte": last
            },
            "MAJOR": major,
            "STATUS": 2,
            "TYPE": 2
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "DEPARTMENT_NAMES": 1,
            "DATE": 1,
            "ALL_NAME": 1
        }
    }
    coll = "system_notify"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("机务接口无今日重点工作提醒信息（目前表里没有待整改数据）")
    else:
        data = pd.DataFrame(documents)
        data = data.fillna('0')
        data = data[data['CONTENT'] != ""]
        if station != "all":
            data = data[data.DEPARTMENT_NAMES.str.
                        contains(station, regex=False)
                        | data.ALL_NAME.str.contains(station, regex=False)]
        else:
            data = data[data.ALL_NAME.str.contains("安监室", regex=False)
                        | data.ALL_NAME.str.contains("宣传部处", regex=False)]
        if len(data) != 0:
            data = data.reset_index()
            for index in data.index:
                content = data.loc[index].at['CONTENT']
                content = content.replace('69, 69, 69', '255, 255, 255')
                content = content.replace('font-size: 16pt',
                                          'font-size: inherit')
                content = content.replace('仿宋_GB2312', 'inherit')
                result.append({
                    "content":
                    content,
                    "tags": [
                        "重点工作提示",
                        data.loc[index].at['DEPARTMENT_NAMES'].split(',')
                    ],
                    "date":
                    "{}/{:0>2}/{:0>2}".format(
                        str(data.loc[index].at['DATE'])[:4],
                        str(data.loc[index].at['DATE'])[4:6],
                        str(data.loc[index].at['DATE'])[6:]),
                    "contentIsHtml":
                    True
                })
    return result


# 异常信息
def _get_error_check(major):
    station = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(months=1)
    last_day = int(str(last_day)[:10].replace('-', ''))
    result = []
    keys = {
        "match": {
            "MAJOR": major,
            "DATE": {
                "$gte": last_day
            }
        },
        "project": {
            "_id": 0,
            "CHECKED_DPID": 1,
            "DEPARTMENT_ALL_NAME": 1
        }
    }
    department_keys = {
        "match": {
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "DEPARTMENT_ID": 1,
            "NAME": 1
        }
    }
    address_keys = {
        "match": {
            "MAJOR": major
        },
        "project": {
            "NAME": 1,
            "STATION": 1
        }
    }
    coll = 'detail_check_info'
    department_coll = 'base_department'
    documents = get_data_from_mongo_by_find(coll, keys)
    department = get_data_from_mongo_by_find(
        department_coll, department_keys, prefix="")
    base_documents = get_data_from_mongo_by_find(
        'check_address', address_keys, prefix="")
    address_data = pd.DataFrame(base_documents)
    if len(documents) != 0:
        data = pd.DataFrame(documents)
        depart_data = pd.DataFrame(department)
        if station != 'all':
            data = data[data.DEPARTMENT_ALL_NAME.str.contains(
                station, regex=False)]
            address_data = address_data[address_data['STATION'] == station]
        data = pd.merge(
            data,
            depart_data,
            how='left',
            left_on='CHECKED_DPID',
            right_on='DEPARTMENT_ID')
        # data = data.drop_duplicates(['CHECKED_ID', 'NAME'])
        address = set(data['NAME'])
        base_addresses = set(address_data['NAME'])
        address = list(base_addresses.difference(address))
        if len(address) > 0:
            data = pd.DataFrame({"NAME": address})
            rst_data = pd.merge(data, address_data, how='left', on='NAME')
            for index in rst_data.index:
                station = rst_data.loc[index].at['STATION']
                name = rst_data.loc[index].at['NAME']
                result.append({"content": f"{station}{name}超过30天未检查"})
    return result


def _get_risk_notice(major):
    station = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(days=70)
    last_day = int(str(last_day)[:10].replace('-', ''))
    today = int(str(today)[:10].replace('-', ''))
    notify_keys = {
        "match": {
            "DUTY_NAME": {
                "$regex": major
            },
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 1
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "DUTY_NAME": 1,
            "CREATE_NAME": 1,
            "START_DATE": 1
        }
    }
    risk_keys = {
        "match": {
            "MAJOR": major,
            "START_DATE": {
                "$lte": today,
                "$gte": last_day
            },
            # "END_DATE": {
            #     "$gte": today
            # },
            "TYPE": 2
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "START_DATE": 1
        }
    }
    coll = 'warn_notification'
    notify_documents = get_data_from_mongo_by_find(coll, notify_keys)
    risk_documents = get_data_from_mongo_by_find(coll, risk_keys)
    documents = notify_documents + risk_documents
    if len(documents) == 0:
        current_app.logger.debug("今日无安全风险预警通知")
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.fillna('')
        data = data.drop_duplicates('CONTENT')
        if station != 'all':
            data = data[data.NAME.str.contains(station, regex=False)]
        else:
            data = data[~data.CREATE_NAME.str.contains("机务段", regex=False)]
        if len(data) == 0:
            return []
        result = []
        for index in data.index:
            if data.loc[index].at['DUTY_NAME']:
                result.append({
                    "content":
                    data.loc[index].at['CONTENT'],
                    "tags": ["预警通知书", data.loc[index].at['DUTY_NAME']],
                    "date":
                    "{}/{:0>2}/{:0>2}".format(
                        str(data.loc[index].at['START_DATE'])[:4],
                        str(data.loc[index].at['START_DATE'])[4:6],
                        str(data.loc[index].at['START_DATE'])[6:])
                })
            else:
                result.append({
                    "content":
                    data.loc[index].at['CONTENT'],
                    "tags": ["风险预警"],
                    "date":
                    "{}/{:0>2}/{:0>2}".format(
                        str(data.loc[index].at['START_DATE'])[:4],
                        str(data.loc[index].at['START_DATE'])[4:6],
                        str(data.loc[index].at['START_DATE'])[6:])
                })
    return result


def _get_risk_check(major):
    # 机务风险防控检查趋势图
    duan = major[1]
    major = major[0]
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today())//100
    keys = {
        "match": {
            "DEPARTMENT_ALL_NAME": {
                "$regex": major
            },
            "MON": {
                "$gt": int(f'{now_year}00')
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "IS_GAOTIE": 1,
            "IS_DONGCHE": 1,
            "IS_KECHE": 1,
            "RISK_NAME": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "MON": 1
        }
    }
    coll = 'detail_check_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("机务无当年检查信息数据")
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID', 'RISK_NAME'])
        data = data.fillna('0-0-0')
        if duan != 'all':
            data = data[data.DEPARTMENT_ALL_NAME.str.contains(
                duan, regex=False)]
            data['station'] = data['DEPARTMENT_ALL_NAME'].apply(
                lambda x: str(x).split('-')[1])
            data['station'] = data['station'].apply(
                lambda x: str(x).split(',')[0])
        else:
            data['station'] = data['DEPARTMENT_ALL_NAME'].apply(
                lambda x: str(x).split('-')[0])
        if len(data) == 0:
            return []
        data['GAOTIE'] = data['IS_GAOTIE'].apply(lambda x: str(x))
        data['DONGCHE'] = data['IS_DONGCHE'].apply(lambda x: str(x))
        data['KECHE'] = data['IS_KECHE'].apply(lambda x: str(x))
        dkg_data = data[data.GAOTIE.str.contains("1", regex=False)
                        | data.DONGCHE.str.contains("1", regex=False)
                        | data.KECHE.str.contains("1", regex=False)]
        result = []
        if duan == 'all':
            all_data = [data, dkg_data]
            titles = ["机务系统", "动客高"]
        else:
            all_data = [data, dkg_data]
            titles = [duan, "动客高"]
        for dex, data_i in enumerate(all_data):
            # risk_list = list(data_i['RISK_NAME'])
            # risks = [risk.split('-')[-1]
            #          for i in risk_list for risk in i.split(',')]
            # risk_data = pd.DataFrame({"RISK": risks})
            # risk_data = risk_data.groupby('RISK').size().reset_index()
            # risk_data = risk_data.sort_values(0, ascending=False)
            # risk_data = risk_data[~ risk_data.RISK.str.contains(
            #     '其他', regex=False)]
            # risk_data = risk_data[risk_data['RISK'] != '0-0-0']
            # risk_names = list(risk_data.RISK)[:5]
            risk_names = ['间断瞭望', '漏检、漏修', '机械伤害', '走行部质量', '油库、油房火灾']
            datas = []  # 全年排名前五风险数据集合
            for risk in risk_names:
                risk_data = data_i.copy()
                risk_data = risk_data[risk_data.RISK_NAME.str.contains(
                    risk, regex=False)]
                risk_data['RISK'] = risk
                datas.append(risk_data)
            all_datas = pd.concat(datas, sort=False)
            title = [
                int(f'{now_year}{i:0>2}') for i in range(1, now_month + 1)
            ]
            line_rst = get_trendency_data(risk_names, datas, title)
            table_result = []  # 表格总结构
            for mon in title:
                table_rst = []  # 表格内风险结构定义
                mon_data = all_datas[all_datas['MON'] == mon]
                all_stas = list(set(mon_data['station']))
                totals = {}
                for sta in all_stas:
                    totals[sta] = len(mon_data[mon_data['station'] == sta])
                for idx, data in enumerate(datas):
                    data_mon = data[data['MON'] == mon]
                    if len(data_mon) == 0:
                        table_rst.append({
                            "data": [],
                            "title": risk_names[idx]
                        })
                    else:
                        station = list(data_mon['RISK'])[0]
                        data_mon = data_mon[data_mon['station'] != '安监室']
                        data_mon = data_mon.groupby('station').size()
                        rst = []
                        for index in data_mon.index:
                            if duan == 'all' and index != '机务处' \
                                    or "车间" in index:
                                rst.append({
                                    "amount":
                                    int(data_mon[index]),
                                    "name":
                                    index,
                                    "value":
                                    '{:.1f}%'.format(
                                        int(data_mon[index]) / totals[index] *
                                        100)
                                })
                        table_rst.append({"data": rst, "title": station})
                table_result.append(table_rst)
            result.append({
                "data": {
                    "line": line_rst,
                    "tooltip": table_result
                },
                "title": titles[dex]
            })
    return result


def _get_not_again_problem(major):
    duan = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(days=90)
    last_day = int(str(last_day)[:10].replace('-', ''))
    keys = {
        "match": {
            "DATE": {
                "$gte": last_day
            },
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "PROBLEM_POINT": 1,
            "DATE": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    base_keys = {
        "match": {
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "STATION": 1,
            "PROBLEM_POINT": 1,
            "RISK_LEVEL": 1
        }
    }
    base_coll = 'base_problem_lib'
    base_documents = get_data_from_mongo_by_find(
        base_coll, base_keys, prefix="")
    if len(documents) == 0:
        current_app.logger.debug("机务上月无检查问题信息")
    else:
        data = pd.DataFrame(documents)
        base_data = pd.DataFrame(base_documents)
        stations = ["贵阳机务段", "西昌机务段", "成都机务段", "重庆机务段"]
        points = list(set(base_data['PROBLEM_POINT']))
        data = data.fillna("a")
        datas = [data]
        if duan == "all":
            stations = ["贵阳机务段", "西昌机务段", "成都机务段", "重庆机务段"]
            for station in stations:
                datas.append(data[data.ALL_NAME.str.contains(
                    station, regex=False)])
            stations.insert(0, '机务系统')
        else:
            datas = []
            stations = [duan]
            datas = [data[data.ALL_NAME.str.contains(duan, regex=False)]]
        rst = []  # 最终结果
        for idx, data in enumerate(datas):
            result = []  # 单个结果
            now_points = list(data['PROBLEM_POINT'])
            now_points = list(
                set([point for i in now_points for point in i.split('；')]))
            result_points = [
                point for point in points if point not in now_points
            ]
            if len(result_points) > 0:
                base_data = base_data.drop_duplicates(
                    ['RISK_LEVEL', 'PROBLEM_POINT'])
                risks = [
                    list(base_data[base_data['PROBLEM_POINT'] == point]
                         ['RISK_LEVEL'])[0] for point in result_points
                ]
                point_data = pd.DataFrame({
                    "point": result_points,
                    "level": risks
                })
                point_data = point_data.sort_values('level').reset_index()
                if len(point_data) < 5:
                    for index in point_data.index:
                        result.append({
                            "name":
                            point_data.loc[index].at['point'],
                            "amount":
                            90,
                            "rank":
                            index + 1
                        })
                        point_datas = []
                    for point in points:
                        point_datas.append(
                            data[data['PROBLEM_POINT'] == point])
                    point_data = pd.concat(point_datas, axis=0, sort=False)
                    point_data = point_data.groupby('PROBLEM_POINT')
                    names = []
                    dates = []
                    for name, group in point_data:
                        names.append(name)
                        dates.append(max(list(group['DATE'])))
                    lenth = 5 - len(result)
                    data = pd.DataFrame({"name": names, "date": dates})
                    data = data.sort_values('date').reset_index()
                    data = data[:lenth]
                    for index in data.index:
                        date = str(data.loc[index].at['date'])
                        date = f'{date[:4]}-{date[4:6]}-{date[6:]}'
                        date = dt.strptime(date, '%Y-%m-%d')
                        days = today - date
                        result.append({
                            "name": data.loc[index].at['name'],
                            "amount": days.days,
                            "rank": 5 - lenth + index + 1
                        })
                else:
                    for index in point_data.index:
                        result.append({
                            "name":
                            point_data.loc[index].at['point'],
                            "amount":
                            90,
                            "rank":
                            index + 1
                        })
                    result = result[:5]
            else:
                for point in points:
                    point_datas.append(data[data['PROBLEM_POINT'] == point])
                point_data = pd.concat(point_datas, axis=0, sort=False)
                point_data = point_data.groupby('PROBLEM_POINT')
                names = []
                dates = []
                for name, group in point_data:
                    names.append(name)
                    dates.append(max(list(group['DATE'])))
                data = pd.DataFrame({"name": names, "date": dates})
                data = data.sort_values('date').reset_index()
                data = data[:5]
                for index in data.index:
                    date = data.loc[index].at['date']
                    date = f'{str(date)[:4]}-{str(date)[4:6]}-{str(date)[6:]}'
                    date = dt.strptime(date, '%Y-%m-%d')
                    days = today - date
                    result.append({
                        "name": data.loc[index].at['name'],
                        "amount": days.days,
                        "rank": 5 - lenth + index + 1
                    })
            rst.append({"data": result, "title": stations[idx]})
    return rst


def singe_score_section(score):
    if score < 2:
        return 1
    elif score < 4:
        return 2
    elif score < 6:
        return 3
    elif score < 8:
        return 4
    else:
        return 5


def _get_cadre_evaluate(major):
    # 机务干部履职问题分类饼图
    '''履职问题分类统计'''
    station = major[1]
    major = major[0]
    today = get_today()
    # last_day = today - relativedelta(months=1)
    month = get_date(today)
    now_year = get_date(today)//100
    # now_day = month * 100 + 24
    # last_month = get_date(last_day)
    # last_day = last_month * 100 + 25
    keys = {
        "match": {
            "MON": {
                "$gt": int('{}00'.format(now_year))
            },
            "MAJOR": major,
            "IDENTITY": "干部"
        },
        "project": {
            "_id": 0,
            "MON": 1,
            "ITEM_NAME": 1,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "SCORE": 1,
            "DATE": 1,
            "PK_ID": 1
        }
    }
    coll = 'detail_evaluate_record'
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    level = {
        "1": "0~2分 （不含2分）",
        "2": "2~4分 （不含4分）",
        "3": "4~6分 （不含6分）",
        "4": "6~8分 （不含8分）",
        "5": "8~12分",
    }
    codes = code.values()
    levels = level.values()
    columns = ['name', 'count', 'count']
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("履职问题分类统计无当月数据")
        return []
    data = pd.DataFrame(documents)
    data = data.drop_duplicates(['PK_ID'])
    all_data = data.copy()
    # data = data[data['DATE'] <= now_day]
    # data = data[data['DATE'] >= last_day]
    data = data[data['MON'] == month]
    if station != 'all':
        data = data[data.ALL_NAME.str.contains(station, regex=False)]
        all_data = all_data[all_data.ALL_NAME.str.contains(
            station, regex=False)]
    if len(data) != 0:
        # 按问题统计
        count_data = data.groupby('ITEM_NAME').size()
        count_doc = [{
            idx: int(count_data[idx])
        } if idx in list(count_data.index) else {
            idx: 0
        } for idx in list(code.values())]
        count_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in count_doc],
            'count': [list(item.values())[0] for item in count_doc]
        },
            index=[i for i in range(1, 12)])

        # 按口分段统计
        data = data.groupby('PERSON_ID')['SCORE'].sum().reset_index()
        data['LEVEL'] = data['SCORE'].apply(singe_score_section)
        level_data = data.groupby('LEVEL').size()
        level_doc = [{
            level[idx]: int(level_data[int(idx)])
        } if int(idx) in list(level_data.index) else {
            level[idx]: 0
        } for idx in list(level.keys())]
        level_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in level_doc],
            'count': [list(item.values())[0] for item in level_doc]
        },
            index=[i for i in range(1, 6)])
        count_rst = get_pie_data(columns, count_data, columns)
        level_rst = get_pie_data(columns, level_data, columns)
    else:
        count_data_rst = []
        level_data_rst = []
        for cod in codes:
            count_data_rst.append({
                "amount": 0,
                "name": cod,
                "rank": 0,
                "value": 0
            })
        for lev in levels:
            level_data_rst.append({
                "amount": 0,
                "name": lev,
                "rank": 0,
                "value": 0
            })
        count_rst = {"data": count_data_rst, "pivot": 0}
        level_rst = {"data": level_data_rst, "pivot": 0}
    if len(all_data) != 0:
        # 按问题统计
        count_all_data = all_data.groupby('ITEM_NAME').size()
        count_doc = [{
            idx: int(count_all_data[idx])
        } if idx in list(count_all_data.index) else {
            idx: 0
        } for idx in list(code.values())]
        count_all_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in count_doc],
            'count': [list(item.values())[0] for item in count_doc]
        },
            index=[i for i in range(1, 12)])

        # 全年按扣分段统计
        level_all_data = all_data.groupby(
            'PERSON_ID')['SCORE'].sum().reset_index()
        level_all_data['LEVEL'] = level_all_data['SCORE'].apply(
            singe_score_section)
        level_all_data = level_all_data.groupby('LEVEL').size()
        all_doc = [{
            level[idx]: int(level_all_data[int(idx)])
        } if int(idx) in list(level_all_data.index) else {
            level[idx]: 0
        } for idx in list(level.keys())]
        level_all_data = pd.DataFrame({
            'name': [list(item.keys())[0] for item in all_doc],
            'count': [list(item.values())[0] for item in all_doc]
        },
            index=[i for i in range(1, 6)])
        count_all_rst = get_pie_data(columns, count_all_data, columns)
        level_all_rst = get_pie_data(columns, level_all_data, columns)
    else:
        count_all_data_rst = []
        level_all_data_rst = []
        for cod in codes:
            count_all_data_rst.append({
                "amount": 0,
                "name": cod,
                "rank": 0,
                "value": 0
            })
        for lev in levels:
            level_all_data_rst.append({
                "amount": 0,
                "name": lev,
                "rank": 0,
                "value": 0
            })
        count_all_rst = {"data": count_all_data_rst, "pivot": 0}
        level_all_rst = {"data": level_all_data_rst, "pivot": 0}
    return [{
        "data": [{
            "title": "问题分类",
            "data": count_rst
        }, {
            "title": "扣分分类",
            "data": level_rst
        }],
        "title":
        "当月"
    },
        {
        "data": [{
            "title": "年度记分分段",
            "data": count_all_rst
        }, {
            "title": "按分数统计",
            "data": level_all_rst
        }],
        "title":
        "全年"
    }]


def _get_safe_produce_info(major):
    # 安全生产信息趋势图
    duan = major[1]
    major = major[0]
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today())//100
    keys = {
        "match": {
            "MON": {
                "$gte": int("{}00".format(now_year))
            },
            "MAJOR": major,
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "MAIN_TYPE": 1,
            "MON": 1,
            "RESPONSIBILITY_UNIT": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("今年无安全生产信息")
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        data['station'] = data['RESPONSIBILITY_UNIT'].apply(lambda x: str(x))
        if duan != 'all':
            data = data[data.station.str.contains(duan, regex=False)]
        else:
            duan = "机务专业"
        names = ['事故信息', '设备质量', '其他', '合计']
        date = [int(f'{now_year}{i:0>2}') for i in range(1, now_month + 1)]
        shi_data = data[data['MAIN_TYPE'] == 1]
        she_data = data[data['MAIN_TYPE'] == 2]
        qi_data = data[data['MAIN_TYPE'] == 3]
        all_data = pd.concat([shi_data, she_data, qi_data], axis=0, sort=False)
        datas = [shi_data, she_data, qi_data, all_data]
        result = [{
            "data": get_trendency_data(names, datas, date),
            "title": duan
        }]
        if duan == "机务专业":
            stations = ['成都机务段', '重庆机务段', '贵阳机务段', '西昌机务段']
            for station in stations:
                station_data = data[data['station'] == station]
                shi_data = station_data[station_data['MAIN_TYPE'] == 1]
                she_data = station_data[station_data['MAIN_TYPE'] == 2]
                qi_data = station_data[station_data['MAIN_TYPE'] == 3]
                all_data = pd.concat([shi_data, she_data, qi_data], axis=0, sort=False)
                datas = [shi_data, she_data, qi_data, all_data]
                result.append({
                    "data": get_trendency_data(names, datas, date),
                    "title": station
                })
    return result


def format_time_to_int(date):
    '''传入time类型数据，返回‘20180801’int型'''
    date_int = int(str(date)[:10].replace("-", ""))
    return date_int


def get_point_data(major):
    '''获取检查地点数据'''
    keys = {
        "match": {
            "CHECK_ALL_NAME": {
                "$regex": major
            },
            "IS_DELETE": 0,
            "TYPE": 2
        },
        "project": {
            "_id": 0,
            "POINT_ID": 1,
            "PARENT_NAME": 1,
            "CHECK_NAME": 1
        }
    }
    point_coll = 'check_address'
    point_doc = get_data_from_mongo_by_find(point_coll, keys, '')
    point_data = pd.DataFrame(point_doc)
    return point_data


def _get_check_map(major):
    '''重点检查点情况'''
    duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    points = [
        '整备场', '调车点', '油库', '始发', '外公寓', '折返', '救援列车', '检修库', '待乘', '派班',
        '热备机车', '锅炉房', '关键站', '动车停放', '调乘一体检查'
    ]
    point_data = get_point_data(major)  # 检查地点数据
    person_data = get_person_data(major)  # 检查人数据
    info_keys = {
        "match": {
            "DEPARTMENT_ALL_NAME": {
                "$regex": major
            },
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "ID_CARD": 1,
            "FK_CHECK_POINT_ID": 1
        }
    }
    info_coll = 'detail_check_info'
    check_info_doc = get_data_from_mongo_by_find(info_coll, info_keys)
    if len(check_info_doc) == 0:
        current_app.logger.debug("当月无检查信息数据")
        return []
    check_data = pd.DataFrame(check_info_doc)  # 检查信息数据
    check_data = check_data.fillna(0)
    check_data = check_data.drop_duplicates(['PK_ID', "FK_CHECK_POINT_ID"])
    check_data = check_data[check_data['FK_CHECK_POINT_ID'] != 0]
    data = pd.merge(
        check_data,
        point_data,
        how='left',
        left_on='FK_CHECK_POINT_ID',
        right_on='POINT_ID')
    data = data.fillna("aa")
    result = []
    for point in points:
        pointData = data[data.PARENT_NAME.str.contains(point, regex=False)]
        point_lists = list(set(pointData['PARENT_NAME']))
        if len(point_lists) > 0:
            point = point_lists[0]
        all_data = pointData.groupby(
            'CHECK_NAME').size().reset_index().sort_values(
                by=0, ascending=False).reset_index()
        all_data['amount'] = all_data[0]
        if duan == 'all':
            check_rst = [{
                "name": all_data.at[index, 'CHECK_NAME'],
                "rank": index + 1,
                "amount": int(all_data.at[index, 'amount'])
            } for index in all_data.index if index < 3]
            rst = [{"data": check_rst, "title": "受检集中点"}]
            if today.day > 10 and today.day <= 24:
                point_list = point_data[point_data['PARENT_NAME'] == point]
                point_list = list(set(point_list['CHECK_NAME']))
                check_list = list(set(all_data['CHECK_NAME']))
                no_list = [
                    check for check in point_list if check not in check_list
                ]
                no_list = [{
                    "name": name,
                    "rank": idx + 1,
                    "amount": 0
                } for idx, name in enumerate(no_list)]
                rst.append({"data": no_list, "title": "未检查点"})
        else:
            rst = []
            lead_data = pd.merge(
                pointData, person_data, how='left', on='ID_CARD')
            lead_data = lead_data[lead_data['IDENTITY'] == '干部']
            lead_data = lead_data.groupby('CHECK_NAME').size()
            for index in all_data.index:
                name = all_data.at[index, 'CHECK_NAME']
                if name in list(lead_data.index):
                    count = str(all_data.at[index, 'amount']) + '/' + str(
                        lead_data.at[name])
                else:
                    count = str(all_data.at[index, 'amount']) + '/0'
                rst.append({"name": name, "rank": index + 1, "amount": count})
            rst = [{"data": rst, "title": "受检集中点"}]
        result.append({"data": rst, "title": point})
    return result


def _get_safe_index(major):
    '''安全综合指数'''
    duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today - relativedelta(months=1))
    stations = ['贵阳机务段', '西昌机务段', '成都机务段', '重庆机务段']
    if duan != 'all':
        stations = [duan]
    detail_index_coll = 'detail_health_index'
    index_coll = 'health_index'
    index_doc = get_detail_index(major, month, index_coll)
    detail_index_doc = get_detail_index(major, month, detail_index_coll)
    result = []
    if len(index_doc) == 0:
        current_app.logger.debug("机务无站段上月安全综合指数信息")
        return result
    index_data = pd.DataFrame(index_doc)  # 综合指数
    index_data = index_data.set_index('DEPARTMENT_NAME')
    detail_index_data = pd.DataFrame(detail_index_doc)
    detail_index_data = detail_index_data[detail_index_data['DETAIL_TYPE'] ==
                                          0]  # 详细指数
    label = ['检查力度指数', '评价力度指数', '考核力度指数', '检查均衡度指数', '问题暴露度指数', '问题整改指数']
    for station in stations:
        # 指数计算
        index = int(index_data.at[station, 'SCORE'])
        detail_data = detail_index_data[detail_index_data['DEPARTMENT_NAME'] ==
                                        station]
        detail_data = detail_data.sort_values(by=['MAIN_TYPE']).reset_index(
            drop=True)
        detail_index = list(detail_data['SCORE'])
        rst = {"label": label, "range": [0, 100], "value": detail_index}
        # 安全检查计算
        keys = {
            "match": {
                "MON": month,
                "DPID": index_data.at[station, 'DEPARTMENT_ID']
            },
            "project": {
                "_id": 0,
                "COUNT": 1,
                "SCORE": 1
            }
        }
        safe_doc = get_data_from_mongo_by_find('map_safety_produce_info', keys)
        evaluate_doc = get_data_from_mongo_by_find('map_evaluate', keys)
        problem_doc = get_data_from_mongo_by_find('map_check_problem', keys)
        info_doc = get_data_from_mongo_by_find('map_check_info', keys)
        if len(safe_doc) > 0:
            safe_count = safe_doc[0]['COUNT']
        else:
            safe_count = 0
        if len(evaluate_doc) > 0:
            evaluate_count = evaluate_doc[0]['SCORE']
        else:
            evaluate_count = 0
        if len(problem_doc) > 0:
            problem_count = problem_doc[0]['COUNT']
        else:
            problem_count = 0
        if len(info_doc) > 0:
            info_count = info_doc[0]['COUNT']
        else:
            info_count = 0
        dim = [safe_count, evaluate_count, problem_count, info_count]
        result.append({
            "dim": dim,
            "data": rst,
            "title": station,
            "index": index
        })
    return result


def get_person_data(major):
    '''获取人员信息'''
    person_keys = {
        "match": {
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "ID_CARD": 1,
            "PERSON_NAME": 1,
            "IDENTITY": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'base_person'
    person_doc = get_data_from_mongo_by_find(coll, person_keys, '')
    person_data = pd.DataFrame(person_doc)
    return person_data


def get_info_map_content(data):
    '''获取热点图表格内容
    params:
        data: dataframe 数据类型
    return:
        scatter: 其余内容
        scatter_emphasis: 排名前十内容'''
    rst_grid = [[0 for col in range(7)] for row in range(7)]
    for idx in data.index:
        _stime = str(data.at[idx, 'START_CHECK_HOUR'])
        _etime = str(data.at[idx, 'END_CHECK_HOUR'])
        week_hours = get_week_hours(_stime, _etime)
        for item in week_hours:
            weekday = item[0] - 1
            hour = item[1] - 1
            rst_grid[hour][weekday] += 1
    # 结果数据集
    rst = [[hour, weekday, rst_grid[hour][weekday]] for hour in range(7)
           for weekday in range(7)]
    pivot = sorted(rst, key=lambda x: x[2], reverse=True)[10][-1]
    scatter = []
    scatter_emphasis = []
    for item in rst:
        if item[2] > pivot:
            scatter_emphasis.append(item)
        else:
            scatter.append(item)
    return scatter, scatter_emphasis


def get_high_problem_data(pro_data):
    '''获取高质量问题数量'''
    data = pro_data[pro_data.LEVEL.str.contains("A", regex=False)
                    | pro_data.LEVEL.str.contains("B", regex=False)
                    | pro_data.LEVEL.str.contains("F1", regex=False)
                    | pro_data.LEVEL.str.contains("F2", regex=False)
                    | pro_data.LEVEL.str.contains("E1", regex=False)
                    | pro_data.LEVEL.str.contains("E2", regex=False)]
    return data


def _get_check_heat_map(major):
    # 获取当月检查频次散点图
    duan = major[1]
    last_day = get_today() - relativedelta(months=1)
    last_month = get_date(last_day)  # 当前月份
    info_data = get_check_info_data(major, last_month)
    pro_data = get_check_pro_data(major, last_month)
    result = []
    stations = ['贵阳机务段', '西昌机务段', '成都机务段', '重庆机务段']
    if duan != 'all':
        stations = [duan]
    names = ['总体', '领导']
    for station in stations:
        sta_data = info_data[info_data.DEPARTMENT_ALL_NAME.str.contains(
            station, regex=False)]
        lead_info_data = sta_data[
            sta_data.DEPARTMENT_ALL_NAME.str.contains('党群领导', regex=False)
            | sta_data.DEPARTMENT_ALL_NAME.str.contains('行政领导', regex=False)]
        pro_sta = pro_data[pro_data['station'] == station]
        lead_pro_data = pro_sta[
            pro_sta.ALL_NAME.str.contains('党群领导', regex=False)
            | pro_sta.ALL_NAME.str.contains('行政领导', regex=False)]
        info_datas = [sta_data, lead_info_data]
        pro_datas = [pro_sta, lead_pro_data]
        for idx, data_i in enumerate(info_datas):
            title = station + names[idx]
            pro_sta = pro_datas[idx]
            total = len(pro_sta)
            high = len(get_high_problem_data(pro_sta))
            if total == 0:
                rate = 0
            else:
                rate = round(high * 100 / total, 1)
            content = f'发现问题数{total}个，高质量问题数{high}个，占比{rate}%'
            scatter, scatter_emphasis = get_info_map_content(data_i)
            result.append({
                "data": {
                    'scatter': scatter,
                    'scatter_emphasis': scatter_emphasis,
                    'content': content
                },
                "title": title
            })
    return result


def _get_check_situation(major):
    '''检查情况'''
    duan = major[1]
    mon = {"$gte": get_date(get_today()) - 1}
    # mon = {"$gte": int(f'{now_year}{now_month-1:0>2}')}
    info_data = get_check_info_data(major, mon)  # 获取检查信息数据
    pro_data = get_check_pro_data(major, mon)  # 获取检查问题数据
    # 获取动客车数据
    info_dong_data = info_data[(info_data['IS_DONGCHE'] == 1)
                               | (info_data['IS_KECHE'] == 1)]
    pro_dong_data = pro_data[(pro_data['IS_DONGCHE'] == 1)
                             | (pro_data['IS_KECHE'] == 1)]
    datas = [[info_data, pro_data], [info_dong_data, pro_dong_data]]
    # stations = []
    # datas = []
    # if duan == 'all':
    #     stations.append('机务专业')
    #     info_dong_data = info_data[(info_data['IS_DONGCHE'] == 1)
    #                                | (info_data['IS_KECHE'] == 1)]
    #     pro_dong_data = pro_data[(pro_data['IS_DONGCHE'] == 1)
    #                              | (pro_data['IS_KECHE'] == 1)]
    #   datas.append([[info_data, pro_data], [info_dong_data, pro_dong_data]])
    #     stations = ['贵阳机务段', '西昌机务段', '成都机务段', '重庆机务段']
    #     for station in stations:
    #         station.append(station)
    #         info_data = info_data[info_data['station'] == station]
    #         pro_data = pro_data[pro_data['station'] == station]
    #         info_dong_data = info_data[(info_data['IS_DONGCHE'] == 1)
    #                                    | (info_data['IS_KECHE'] == 1)]
    #         pro_dong_data = pro_data[(pro_data['IS_DONGCHE'] == 1)
    #                                  | (pro_data['IS_KECHE'] == 1)]
    #         datas.append([[info_data, pro_data],
    #                       [info_dong_data, pro_dong_data]])
    # 计算上周和上月日期
    today = get_today()
    # 计算周日期
    week = today.weekday()  # 周按照周四-周三计算
    weeks = [3, 2, 1, 0, 6, 5, 4]
    week_delta = [7, 6, 5, 4, 3, 2, 1]
    delta_day = week_delta[weeks.index(week)]
    last_week_start = format_time_to_int(today -
                                         relativedelta(days=(7 + delta_day)))
    last_week_end = format_time_to_int(today - relativedelta(days=7))
    now_week_start = format_time_to_int(today - relativedelta(days=delta_day))
    # 计算月日期
    now_mon = get_date(today)
    last_day = today - relativedelta(months=1)
    last_day = int(str(last_day)[:10].replace('-', ''))
    titles = ['', '动客车']
    result = []
    rst_week_i = []
    rst_mon_i = []
    # for index,major_datas in enumerate(datas):
    #     duan = stations[index]
    if duan == 'all':
        duan = '机务专业'
    for idx, data in enumerate(datas):
        title = duan + titles[idx]
        info_data = data[0]
        pro_data = data[1]
        # 获取上周和本周数据
        last_week_info = info_data[info_data['DATE'] > last_week_start]
        last_week_info = last_week_info[last_week_info['DATE'] < last_week_end]
        now_week_info = info_data[info_data['DATE'] > now_week_start]
        last_week_pro = pro_data[pro_data['DATE'] > last_week_start]
        last_week_pro = last_week_pro[last_week_pro['DATE'] < last_week_end]
        now_week_pro = pro_data[pro_data['DATE'] > now_week_start]
        # 计算周结果
        rst = get_check_situation_content(now_week_info, now_week_pro,
                                          last_week_info, last_week_pro)
        rst_week_i.append({"data": rst, "title": title})
        # 获取上月和本月数据
        now_info_data = info_data[info_data['MON'] == now_mon]
        last_info_data = info_data[info_data['DATE'] <= last_day]
        now_pro_data = pro_data[pro_data['MON'] == now_mon]
        last_pro_data = pro_data[pro_data['DATE'] <= last_day]
        # 计算月结果数值
        rst = get_check_situation_content(now_info_data, now_pro_data,
                                          last_info_data, last_pro_data)
        rst_mon_i.append({"data": rst, "title": title})
    result.append({"data": rst_week_i, "title": "周"})
    result.append({"data": rst_mon_i, "title": "月"})
    return result


def get_check_situation_content(now_info_data, now_pro_data, last_info_data,
                                last_pro_data):
    '''检查情况结果内容'''
    now_value = get_check_value(now_info_data, now_pro_data)
    last_value = get_check_value(last_info_data, last_pro_data)
    delta = [
        round(((now_value[i] - last_value[i]) / last_value[i] - 1) * 100, 1)
        if last_value[i] > 0 else 0 for i in range(len(now_value))
    ]
    big = (max(now_value) // 1000 + 1) * 1000
    label = [
        "现场检查次数",
        "添乘检查次数",
        "视频调阅次数",
        "数据分析次数",
        "发现问题总数",
        "作业项问题数",
        "管理项问题数",
        "设备项问题数",
        "质量分",
        "高质量问题数",
    ]
    data = {
        "range": [0, big],
        "value": now_value,
        "label": label,
        "delta": delta
    }
    return data


def get_check_value(info_data, pro_data):
    '''计算各项检查次数
    params: info_data: 检查信息数据
            pro_data: 检查问题数据
    return: 数组： 各项检查次数'''
    site_count = len(info_data[info_data['CHECK_WAY'] == 1])
    tian_count = len(info_data[info_data['CHECK_WAY'] == 2])
    view_count = len(info_data[info_data.RETRIVAL_TYPE_NAME.str.contains(
        "视频", regex=False)])
    data_count = len(info_data[info_data['CHECK_WAY'] == 3]) - view_count
    pro_total = len(pro_data)
    work_count = len(pro_data[pro_data.LEVEL.str.contains("A", regex=False)
                              | pro_data.LEVEL.str.contains("B", regex=False)
                              | pro_data.LEVEL.str.contains("C", regex=False)
                              | pro_data.LEVEL.str.contains("D", regex=False)])
    manage_count = len(pro_data[pro_data.LEVEL.str.contains("F", regex=False)])
    equirment_count = len(pro_data[pro_data.LEVEL.str.contains(
        "E", regex=False)])
    scores = filter(lambda x: x != '0-0-0', list(pro_data['PROBLEM_SCORE']))
    score_total = int(sum(scores))
    high_count = len(get_high_problem_data(pro_data))
    value = [
        site_count, tian_count, view_count, data_count, pro_total, work_count,
        manage_count, equirment_count, score_total, high_count
    ]
    return value


def _get_site_important_issus(major):
    '''现场重点作业问题'''
    duan = major[1]
    now_month = get_date(get_today())
    last_month = get_date(get_today() - relativedelta(months=1))
    mon = {"$gte": last_month}
    points = [
        '移动设备停留不采取防溜措施。', '当班期间睡觉。', '不执行“运输调度集中统一指挥、行车单一指挥”。',
        '当班期间违规使用手机等电子产品(在线路限界内使用)。', '汛期行车未执行“瞭、降、停”措施。', '未按规定进行回送设置。',
        '擅自关闭、违章操作导致LKJ(ATP)等行车安全装备失去控制。', '未执行LKJ数据转储上传。',
        '酒精检测不合格或拒绝进行酒精检测。', '挂头作业时，未执行两停一挂，未在脱轨器、车列10米前一度停车', '升弓未确认升弓状态。',
        '未执行108#调车安全措施。', '运行中间断瞭望，未造成后果。', '不执行“十必停”。',
        '车顶不明原因跳闸、降弓或弓网故障后，未查明车顶情况就盲目升弓、换弓或合闸的', '出、退勤未按规定径路行走。站接、站交未2人同行。',
        '遇有车站停车作业时，司机未执行CRH2型系列动车组列车以不超过60km/h速度进站、其他型号动车组以不超过80km/h速度进站规定。',
        '运行中间隙打盹。', '使用便携式记录仪等音视频设备不按规定录像、拍照。', '未按规定执行断电、降弓、拉蓄电池的作业步骤。',
        '未按规定勾划、标注运行揭示、调车计划、卡控表等。', '当班期间打牌、饮酒、脱岗。',
        '旅客列车、动车组列车发车时联控用语不清不全开车。', '旅客列车未执行询问式发车联控。',
        '单机作业集中联锁区向非集中联锁区作业时，未按规定进行问路联控。中间站调车未执行问路调车；编区站要求调车联控的作业未进行调车联控。',
        '乘务员站停、救援作业人员救援过程中违规使用手机', '旅客列车、动车组列车非正常停车后未第一时间报告车站(调度)。',
        '动车组司机办客站未得到列车长关门通知擅自集控关门，误听车次集控关门。', '机车走行部关键部件漏检、漏修、漏探，存在严重安全隐患。',
        '机车主变压器、高压电压互感器未进行油温、油爆保护性能试验，压力释放阀、油温表未按规定进行检验。',
        'LKJ、JK430、6A系统、ERM等车载安全数据未转储分析。', '未做制动机试验或更换制动机配件未进行制动机试验。',
        '修程和趟检作业中未按规定执行双管供风、直供电试验', '未按工艺、范围检修，造成严重安全质量隐患。',
        '检修机车未进行高压试验、制动机试验的。', '特种设备发生影响安全的故障未及时修复。',
        '关键重点设备发生影响生产和安全的故障未及时修复。', '设备检修作业（设备小修、巡检、鉴定、整治等）未按规定设置防护。',
        '锅炉、压力容器、金属切削设备运行期间操作人员不在岗。', '无人看护开启的电炉、热风机等取暖设备',
        '检修、整备机车作业前未挂安全防护牌。', '缓解列车制动时，机车未制动。', '接车后升弓状态离开机车无人看守。',
        '在脱轨器、防护信号前未停车确认。'
    ]
    pro_data = get_check_pro_data(major, mon)
    pro_data = pro_data[pro_data.PROBLEM_POINT.isin(points)]
    now_data = pro_data[pro_data['MON'] == now_month]
    last_data = pro_data[pro_data['MON'] == last_month]
    if duan != "all":
        result = [{
            "data": get_problem_point_content(now_data, last_data),
            "title": duan
        }]
        return result
    result = [{
        "data": get_problem_point_content(now_data, last_data),
        "title": "机务系统"
    }]
    for station in stations:
        now_sta = now_data[now_data['station'] == station]
        last_sta = last_data[last_data['station'] == station]
        rst = get_problem_point_content(now_sta, last_sta)
        result.append({"data": rst, "title": station})
    return result


def get_problem_point_content(now_data, last_data):
    '''获取重点现场问题站段结果'''
    rst = []
    now_sta = now_data.groupby('PROBLEM_POINT').size()
    last_sta = last_data.groupby('PROBLEM_POINT').size()
    last_points = list(set(last_sta.index))
    now_points = list(set(now_sta.index))
    for point in last_points:
        if point in now_points:
            rst.append({
                "name": point,
                "total": int(last_sta.at[point]),
                "value": int(now_sta.at[point])
            })
        else:
            rst.append({
                "name": point,
                "total": int(last_sta.at[point]),
                "value": 0
            })
    return rst


def get_complete_num(index, data):
    '''获取量化指标完成数量
    params: index: data索引
            data: 数据
    return: 数组 [完成量，完成率]'''
    condition = data.loc[index].at['ASSESS_CONDITION']
    conditions = condition.split('，')
    pattern = re.compile(r'\d+')
    index = 0
    complete = 0
    for con in conditions:
        if "指标" in con:
            index = float(pattern.findall(con)[0])
        elif "完成" in con:
            complete = float(pattern.findall(con)[0])
    if index != 0:
        comp_rate = round(complete * 100 / index, 2)
        return [complete, comp_rate]
    else:
        return [0, 0]


def _get_analysis_quality(major):
    '''分析中心工作质量'''
    duan = major[1]
    major = major[0]
    today = get_today()
    now_month = today.month
    if today.day > current_app.config.get('UPDATE_DAY'):
        mon = int(f'{now_year}{now_month:0>2}')
    else:
        mon = int(str(today - relativedelta(months=1)).replace('-', '')[:6])
    keys = {
        "match": {
            "MON": mon,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "ASSESS_POINT": 1,
            "ASSESS_CONDITION": 1,
            "GRADES_TYPE": 1,
            "ACTUAL_SCORE": 1,
            "DEPARTMENT_NAME": 1
        }
    }
    coll = 'analysis_center_assess'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("无当月安全分析中心考核数据")
        return []
    else:
        data = pd.DataFrame(documents)
        # 计算站段质量考评
        rst_data = data.copy()
        rst_data['SCORE'] = rst_data.apply(
            lambda row: row['ACTUAL_SCORE'] if row['GRADES_TYPE'] == 1 else (
                -1 * row['ACTUAL_SCORE']),
            axis=1)
        rst_data = rst_data.groupby('DEPARTMENT_NAME').sum(
            by='SCORE').sort_values(['SCORE'], ascending=False).reset_index()
        rst_data['SCORE'] = rst_data['SCORE'].apply(lambda x: min(100, x + 80))
        contents = {}
        # 机务处不参与排名,单独计算
        major_data = rst_data[rst_data['DEPARTMENT_NAME'] ==
                              '机务处'].reset_index()
        score = major_data.loc[0].at['SCORE']
        contents["机务处"] = f'{mon}机务处分析中心工作质量考评得分为{score}'
        # 计算其余站段
        rst_data = rst_data[rst_data['DEPARTMENT_NAME'] != '机务处'].reset_index()
        for index in rst_data.index:
            name = rst_data.loc[index].at['DEPARTMENT_NAME']
            score = rst_data.loc[index].at['SCORE']
            content = f'''
                <p>{mon}{name}分析中心工作质量考评得分为<span class="color-red">
                {score}</span>，排名<span class="color-red">
                第{index+1}</span></p >'''
            contents[name] = content
        if duan != 'all':
            stations = [duan]
        else:
            stations = ['机务处', '贵阳机务段', '西昌机务段', '成都机务段', '重庆机务段']
        result = []
        for station in stations:
            sta_data = data.copy()
            sta_data = sta_data[sta_data['DEPARTMENT_NAME'] ==
                                station].set_index('ASSESS_POINT')
            zhutiao = get_complete_num('逐条评价条数', sta_data)
            dingqi = get_complete_num('定期复查人数', sta_data)
            pingjia = get_complete_num('评价记分指标', sta_data)
            dy_wenti = get_complete_num('调阅发现问题数', sta_data)
            dy_zuoye = get_complete_num('调阅发现作业项问题数', sta_data)
            dy_gangwei = get_complete_num('调阅覆盖岗位数', sta_data)
            if station == '机务处':
                dy_chejian = [0, 0]
            else:
                dy_chejian = get_complete_num('调阅覆盖车间数', sta_data)
            guanjian = get_complete_num('关键时段调阅次数', sta_data)
            value = [
                zhutiao[0], dingqi[0], pingjia[0], dy_wenti[0], dy_zuoye[0],
                dy_gangwei[0], dy_chejian[0], guanjian[0]
            ]
            rate = [
                zhutiao[1], dingqi[1], pingjia[1], dy_wenti[1], dy_zuoye[1],
                dy_gangwei[1], dy_chejian[1], guanjian[1]
            ]
            big = ((max(value) // 100) + 1) * 100
            rst = {
                "range": [0, big],
                "value":
                value,
                "delta":
                rate,
                "label": [
                    "逐条评价",
                    "定期复查",
                    "评价记分",
                    # "设备监控调阅",
                    # "设备监控复查",
                    "发现问题",
                    "发现作业项问题",
                    "调阅覆盖岗位",
                    "调阅覆盖车间",
                    "关键时段调阅"
                ],
                "content":
                contents[station]
            }
            result.append({"data": rst, "title": station})
        return result


def _get_key_unit_quality(major):
    '''关键部件质量'''
    duan = major[1]
    now_month = get_date(get_today())
    last_day = get_today() - relativedelta(months=1)
    last_month = get_date(last_day)
    last_day = int(str(last_day)[:10].replace("-", ''))
    mon = {"$gte": last_month}
    points = [
        '机车车顶部件（受电弓、主断路器、高压互感器等高压电器）质量不良。', '车钩部件质量不良。', '轮对质量不良。',
        '制动系统部件质量不良。', '空气管路系统部件质量不良。', '机车存在火险隐患。', '单元制动器部件质量不良。',
        '走行部部件松、断、裂及过热，存在部件断裂、脱落等安全隐患。', '机砂不合标准。', '关键部件质量不良存在行车安全隐患'
    ]
    pro_data = get_check_pro_data(major, mon)
    pro_data = pro_data[pro_data.PROBLEM_POINT.isin(points)]
    now_data = pro_data[pro_data['MON'] == now_month]
    last_data = pro_data[pro_data['DATE'] <= last_day]
    now_ke = now_data[now_data['IS_KECHE'] == 1]
    last_ke = last_data[last_data['IS_KECHE'] == 1]
    if duan == 'all':
        duan = '机务系统'
    if len(now_data) != 0 and len(last_data) != 0:
        result = [{
            "data": get_key_unit_pie_content(now_data, last_data),
            "title": duan
        }]
    else:
        result = [{"data": [], "title": duan}]
    if len(now_ke) != 0 and len(last_ke) != 0:
        result.append({
            "data": get_key_unit_pie_content(now_ke, last_ke),
            "title": duan + '客车'
        })
    else:
        result.append({"data": [], "title": duan + '客车'})
    if duan != '机务系统':
        return result
    for station in stations:
        now_sta = now_data[now_data['station'] == station]
        last_sta = last_data[last_data['station'] == station]
        if len(now_sta) != 0:
            result.append({
                "data": get_key_unit_pie_content(now_sta, last_sta),
                "title": station
            })
        else:
            result.append({"data": [], "title": station})
        now_ke = now_sta[now_sta['IS_KECHE'] == 1]
        last_ke = last_sta[last_sta['IS_KECHE'] == 1]
        if len(now_ke) != 0 and len(last_ke) != 0:
            result.append({
                "data": get_key_unit_pie_content(now_ke, last_ke),
                "title": station + '客车'
            })
        else:
            result.append({"data": [], "title": station + '客车'})
    return result


def get_key_unit_pie_content(now_data, last_data):
    now_data = now_data.groupby('PROBLEM_POINT').size().reset_index()
    now_data['amount'] = now_data[0]
    last_points = list(set(last_data['PROBLEM_POINT']))
    last_data = last_data.groupby('PROBLEM_POINT').size()
    datas = now_data.sort_values('amount', ascending=False).reset_index()
    pivot = get_median(set(datas['amount']))
    big_half = datas[datas['amount'] > pivot].reset_index()
    small_half = datas[datas['amount'] <= pivot].reset_index()
    pie = []
    for index in big_half.index:
        if big_half.loc[index].at['PROBLEM_POINT'] in last_points:
            pie.append({
                "name":
                big_half.loc[index].at['PROBLEM_POINT'],
                "amount":
                int(big_half.loc[index].at['amount']),
                "delta":
                round((int(big_half.loc[index].at['amount']) / int(
                    last_data.at[big_half.loc[index].at['PROBLEM_POINT']]) - 1)
                    * 100, 1),
                "value":
                int(big_half.loc[index].at['amount']),
                "rank":
                index
            })
        else:
            pie.append({
                "name": big_half.loc[index].at['PROBLEM_POINT'],
                "amount": int(big_half.loc[index].at['amount']),
                "delta": 0,
                "value": int(big_half.loc[index].at['amount']),
                "rank": index
            })
    for index in small_half.index:
        if small_half.loc[index].at['PROBLEM_POINT'] in last_points:
            pie.append({
                "name":
                small_half.loc[index].at['PROBLEM_POINT'],
                "amount":
                int(small_half.loc[index].at['amount']),
                "delta":
                round((int(small_half.loc[index].at['amount']) / int(
                    last_data.at[small_half.loc[index].at['PROBLEM_POINT']]) -
                    1) * 100, 1),
                "value":
                int(small_half.loc[index].at['amount']),
                "rank":
                len(small_half) - index - 1
            })
        else:
            pie.append({
                "name": small_half.loc[index].at['PROBLEM_POINT'],
                "amount": int(small_half.loc[index].at['amount']),
                "delta": 0,
                "value": int(small_half.loc[index].at['amount']),
                "rank": len(small_half) - index - 1
            })
    return {"data": pie, "pivot": pivot}


def get_data(major, node, params):
    funcName = {
        # 第一屏
        "jw_safe_days": _get_safe_days,  # 安全天数
        "jw_supervise_item": _get_supervise_item,  # 督办事项
        "jw_important_work": _get_important_work,  # 重点工作
        "jw_error_check": _get_error_check,  # 异常信息
        "jw_risk_notice": _get_risk_notice,  # 安全风险预警
        "jw_risk_check": _get_risk_check,  # 机务风险防控检查趋势图
        # "jw_space_problem": _get_space_problem,  # 间隔时间最长问题项点
        "jw_not_again_problem": _get_not_again_problem,  # 未再次发生问题项点
        "jw_cadre_evaluate": _get_cadre_evaluate,  # 干部履职积分统计
        "jw_safe_produce_info": _get_safe_produce_info,  # 安全生产信息趋势图

        # 第二屏
        "jw_check_map": _get_check_map,  # 检查地图点
        "jw_safe_index": _get_safe_index,  # 安全综合指数
        "jw_check_heat_map": _get_check_heat_map,  # 检查频次热力图

        # 第三屏
        "jw_check_situation": _get_check_situation,  # 检查情况
        "jw_site_important_issus": _get_site_important_issus,  # 现场重点作业问题
        "jw_analysis_quality": _get_analysis_quality,  # 分析中心工作质量
        "jw_key_unit_quality": _get_key_unit_quality,  # 关键部件质量
    }
    duans_li = ['all', 'gyj', 'xcj', 'cdj', 'cqj']
    if node not in funcName:
        return 'API参数错误'
    if params not in duans_li:
        return "站段参数错误"
    major = get_major(major)
    station = get_station(params)
    return funcName[node]([major, station])


if __name__ == '__main__':
    pass
