#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    大屏API数据
'''
import json
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app
from app.utils.common_func import choose_collection_prefix
from app import mongo
from app.new_big.util import (devide_type, get_broken_line_group,
                              get_broken_month, get_coll_prefix,
                              get_data_from_mongo_by_aggregate,
                              get_data_from_mongo_by_find, get_pie_data,
                              get_trendency_data, singe_score_section)
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day


def _get_days_without_accident():
    ''' 无事故安全生产天数'''
    data = mongo.db.base_data.find_one({'TYPE': 'days_without_accident'})
    if dt.now().hour == 18:
        days = data['VALUE'] + 1
    else:
        days = data['VALUE']
    return {'safe_produce_duration': days}


# 安全生产信息通知接口
def _get_safety_produce_notification():
    '''安全生产信息通知接口'''
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    # yesterday = 20180802  #开发使用日期，实际应用上面的日期
    _prefix = get_coll_prefix(yesterday)
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            'DATE': yesterday
        },
        "project": {
            '_id': 0,
            'OVERVIEW': 1,
            'LINE_NAME': 1,
            'MAIN_CLASS': 1,
            'DETAIL_CLASS': 1,
            'BELONG': 1,
            'PK_ID': 1,
            'CLOCK': 1,
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys, _prefix)
    records = []
    tmp_id_set = set()
    for record in documents:
        if record['PK_ID'] in tmp_id_set:
            continue
        else:
            tmp_id_set.add(record['PK_ID'])
        tags = []
        if record['DETAIL_CLASS'] != 0:
            tags = [
                record['DETAIL_CLASS'], record['LINE_NAME'], record['BELONG']
            ]
        else:
            tags = [
                record['MAIN_CLASS'], record['LINE_NAME'], record['BELONG']
            ]
        record['content'] = record['OVERVIEW']
        record['tags'] = tags
        record['date'] = '{}/{}/{} {}'.format(
            str(yesterday)[:4],
            str(yesterday)[4:6],
            str(yesterday)[6:], record['CLOCK'])
        del record['LINE_NAME'], record['MAIN_CLASS'], record[
            'DETAIL_CLASS'], record['BELONG']
        del record['CLOCK']
        del record['OVERVIEW']
        del record['PK_ID']
        records.append(record)
    return records


def code_name_map(code_name):
    # 履值问题代码-文字描述
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    return code.get(code_name, None)


def _get_safety_produce_info_trendency():
    '''获取今年安全生产事故趋势图数据'''
    global_rst = {}
    DETAIL_CLASS = ['行车', '劳安', '路外']
    # now_month = int(str(get_date(get_today()))[4:])
    # now_year = get_date(get_today())//100
    month_list = get_broken_month(get_today(), 5, 5)
    start_mon = int(f'{month_list[0].year}{month_list[0].month:0>2}')
    end_mon = int(f'{month_list[-1].year}{month_list[-1].month:0>2}')
    mon = {'$gte': start_mon, '$lte': end_mon}
    date = [int(f'{yue.year}{yue.month:0>2}') for yue in month_list]
    for value in DETAIL_CLASS:
        keys = {
            "match": {
                # "MON": {
                #     '$gt': int('{}00'.format(now_year))
                # },
                "MON": mon,
                "DETAIL_CLASS": value
            },
            "project": {
                "_id": 0,
                "PK_ID": 1,
                "MON": 1,
                "DETAIL_CLASS": 1
            }
        }
        coll = 'detail_safety_produce_info'
        documents = get_data_from_mongo_by_find(coll, keys)
        if len(documents) == 0:
            global_rst[value] = [0 for x in range(1, now_month + 1)]
        else:
            data = pd.DataFrame(documents)
            data = data.drop_duplicates(['PK_ID'])
            global_data = data.groupby(['MON']).size()
            global_data = global_data.reset_index()
            data = pd.merge(
                pd.DataFrame({
                    'MON': date
                }), global_data, how='left', on='MON')
            data = data.fillna(0)
            global_rst[value] = [int(x) for x in data[0].values]
    # 获取故障数据
    keys = {
        "match": {
            # "MON": {
            #     '$gt': int('{}00'.format(now_year))
            # },
            "MON": mon,
            "MAIN_TYPE": 2
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "MON": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        global_rst['故障'] = [0 for x in date]
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID'])
        global_data = data.groupby(['MON']).size().reset_index()
        global_rst['故障'] = [
            int(global_data[0][global_data['MON'] == mon])
            if mon in list(global_data['MON']) else 0 for mon in date
        ]
    global_rst.update({
        'date': ['{}/{}'.format(str(d)[0:4],
                                str(d)[4:]) for d in date]
    })
    return global_rst


def _get_safe_duty_produce_info():
    '''获取今年安全生产信息和责任安全生产信息数据'''
    now_year = get_date(get_today()) // 100
    global_rst = {}
    TITLE = {
        "CW": "车务",
        "JW": "机务",
        "GW": "工务",
        "DW": "电务",
        "GD": "供电",
        "CL": "车辆"
    }
    global_rst = {'title': TITLE}
    majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
    major_keys = ['CW', 'JW', 'GW', 'DW', 'GD', 'CL']
    for index, value in enumerate(majors):
        key = major_keys[index]
        keys = {
            "match": {
                "MON": {
                    '$gt': int('{}00'.format(now_year))
                },
                "MAJOR": value
            },
            "project": {
                "_id": 0,
                "PK_ID": 1,
                "MAIN_CLASS": 1,
                "MAJOR": 1,
                "RESPONSIBILITY_NAME": 1
            }
        }
        coll = 'detail_safety_produce_info'
        documents = get_data_from_mongo_by_find(coll, keys)
        if len(documents) == 0:
            current_app.logger.debug("路局第一屏安全生产信息统计无数据")
            global_rst[key] = {
                "total": {"ACC": 0, "COM": 0, "ERR": 0},
                "duty": {"ACC": 0, "COM": 0, "ERR": 0}
            }
        else:
            data = pd.DataFrame(documents)
            data = data.drop_duplicates(['PK_ID'])
            total_data = data.copy()
            total_data = total_data.groupby(['MAIN_CLASS']).size()
            total_data = total_data.reindex(['事故', '故障', '综合'])
            total_data.index = ['ACC', 'ERR', 'COM']
            total_data = total_data.fillna(0)
            duty_data = data.copy()
            duty_data = duty_data[duty_data['RESPONSIBILITY_NAME'] == 1]
            duty_data = duty_data.groupby(['MAIN_CLASS']).size()
            duty_data = duty_data.reindex(['事故', '故障', '综合'])
            duty_data.index = ['ACC', 'ERR', 'COM']
            duty_data = duty_data.fillna(0)
            global_rst[key] = {
                "total": eval(total_data.to_json()),
                "duty": eval(duty_data.to_json())
            }
    return global_rst


def _get_risk_distribute_data():
    '''获取近三月的风险分布数据'''
    global_rst = []
    today = get_today()
    year = today.year
    documents = []
    keys = {
        "match": {
            "MON": {
                '$gte': year * 100
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RISK_NAME": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("风险分布无近期（三月）数据")
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID'])
        data['name'] = data['RISK_NAME'].apply(
            lambda x: x.split('-')[-1] if x != 0 else 0)
        del data['RISK_NAME']
        data = data.groupby(['name']).size().reset_index()
        data = data[data['name'] != 0]
        data['count'] = data[0]
        data = data.sort_values('count', ascending=0)
        data = data.reset_index()
        data['rank'] = data.index + 1
        for index in data.index:
            if int(index) >= 10:
                break
            else:
                global_rst.append({
                    "name": data.loc[index].at['name'],
                    "rank": int(data.loc[index].at['rank']),
                    "amount": int(data.loc[index].at['count'])
                })

    return global_rst


def _get_typical_accident_data():
    '''获取典型事故轮播信息'''
    today = get_today()
    month = get_date(today - relativedelta(months=2))
    global_rst = []
    document = []
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            "MON": {
                '$gt': month
            },
            "MAIN_TYPE": 1,
            "RESPONSIBILITY_NAME": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "OVERVIEW": 1,
            "DIRECT_REASON": 1
        }
    }
    document = get_data_from_mongo_by_find(coll, keys)
    if len(document) == 0:
        current_app.logger.debug("典型事故无数据信息")
        return global_rst
    else:
        data = pd.DataFrame(document)
        data = data.drop_duplicates('PK_ID')
        for index in data.index:
            if data.loc[index].at['DIRECT_REASON'] == 0:
                global_rst.append({
                    "content": data.loc[index].at['OVERVIEW'],
                    "reason": ""
                })
            else:
                global_rst.append({
                    "content": data.loc[index].at['OVERVIEW'],
                    "reason": data.loc[index].at['DIRECT_REASON']
                })
    return global_rst


def _get_safe_produce_reason_data():
    '''获取安全生產信息原因（責任）数据'''
    today = get_today()
    if today.month != 12 or today.day < 25:
        year = today.year - 1
    else:
        year = today.year
    documents = []
    keys = {
        "match": {
            "DATE": {
                "$gte": int(f'{year}1225')
            },
            "RESPONSIBILITY_IDENTIFIED": {
                "$in": [1, 2]
            },
            "RESPONSIBILITY_NAME": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "PONDERANCE_NUMBER": 1,
            "DIRECT_REASON": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    data = pd.DataFrame(documents)
    if data.empty:
        result = {}
    else:
        data = data.drop_duplicates(['PK_ID', 'DIRECT_REASON'])
        data['COUNT'] = 1
        data = data.groupby('DIRECT_REASON').sum().reset_index()
        data = data.sort_values('PONDERANCE_NUMBER', ascending=False)
        data = data[data['DIRECT_REASON'] != 0][:10]
        columns = ['DIRECT_REASON', 'COUNT', 'PONDERANCE_NUMBER']
        result = get_pie_data(columns, data, columns)
    return result


def _get_duty_accident_error_data():
    '''获取今年安全生产事故趋势图数据'''
    global_rst = {}
    # now_month = get_today().month
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today()) // 100
    date_now = [int(f'{now_year}{x:0>2}') for x in range(1, now_month + 1)]
    date_last = [int(f'{now_year - 1}{x:0>2}')
                 for x in range(1, now_month + 1)]
    date = [int('{:0>2}'.format(x)) for x in range(1, now_month + 1)]
    documents = []
    for _prefix in ['daily_', 'monthly_', 'history_']:
        coll_name = '{}detail_safety_produce_info'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({
                "MON": {
                    '$in': date_now
                },
                "RESPONSIBILITY_NAME": 1,
                "RESPONSIBILITY_IDENTIFIED": {
                    "$in": [1, 2]
                }
            }, {
                "_id": 0,
                "PK_ID": 1,
                "MON": 1,
                "MAIN_TYPE": 1,
                "CODE": 1
            })))
        documents.extend(
            list(mongo.db[coll_name].find({
                "MON": {
                    '$in': date_last
                },
                "RESPONSIBILITY_NAME": 1,
                "RESPONSIBILITY_IDENTIFIED": {
                    "$in": [1, 2]
                }
            }, {
                "_id": 0,
                "PK_ID": 1,
                "MAIN_TYPE": 1,
                "MON": 1,
                "CODE": 1
            })))
    if len(documents) == 0:
        current_app.logger.debug("责任事故故障去年比较图接口报错：今年安全生产信息无数据")
        global_rst = []
        return global_rst
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID'])
        data = data.fillna(111111)
        line = {}
        bar = {}
        error_data = data[data['MAIN_TYPE'] == 2].copy()
        error_data['year'] = error_data['MON'].apply(
            lambda x: int(str(x)[0:4]))
        error_last = error_data[error_data['year'] == now_year - 1]
        error_now = error_data[error_data['year'] == now_year]
        error_date = [
            int('{}{:0>2}'.format(now_year, i))
            for i in range(1, now_month + 1)
        ]
        error_last_date = [
            int(f'{now_year - 1}{i:0>2}') for i in range(1, now_month + 1)
        ]
        if len(error_last) > 0:
            error_last = error_last.groupby('MON').size().reset_index()
            line[f'{now_year - 1}年故障'] = [
                int(error_last[0][error_last['MON'] == mon])
                if mon in list(error_last['MON']) else 0
                for mon in error_last_date
            ]
        else:
            line[f'{now_year - 1}年故障'] = [0 for mon in error_last_date]
        if len(error_now) > 0:
            error_now = error_now.groupby('MON').size().reset_index()
            bar[f'{now_year}年故障'] = [
                int(error_now[0][error_now['MON'] == mon])
                if mon in list(error_now['MON']) else 0 for mon in error_date
            ]
        else:
            bar[f'{now_year}年故障'] = [0 for mon in error_date]
    data['type'] = data['CODE'].apply(devide_type)
    del data['CODE']
    data = data.groupby(['MON', 'type']).size()
    data = data.reset_index()
    n = ['A', 'B', 'C', 'D']
    data_case = pd.DataFrame(columns=['date', 'type'])
    for i in n:
        data_a = pd.DataFrame({"date": date_now, "type": i})
        data_case = pd.concat([data_case, data_a], ignore_index=True)
    data['MON'] = data['MON'].apply(str)
    data = pd.merge(
        data_case,
        data,
        how='left',
        left_on=['date', 'type'],
        right_on=['MON', 'type'])
    data = data.fillna(0)
    data['year'] = data['date'].apply(lambda x: str(x)[0:4])
    data = data.groupby(['type', "year"])
    for name, group in data:
        if name[1] == f'{now_year - 1}':
            line.update({
                '{}年{}类'.format(name[1], name[0]): [int(x) for x in group[0]]
            })
        if name[1] == f'{now_year}':
            bar.update({
                '{}年{}类'.format(name[1], name[0]): [int(x) for x in group[0]]
            })
    lines = [f'{now_year - 1}年A类', f'{now_year - 1}年B类',
             f'{now_year - 1}年C类', f'{now_year - 1}年D类']
    for name in lines:
        if name not in line.keys():
            line[name] = [0 for d in date]
    bars = [f'{now_year}年A类', f'{now_year}年B类',
            f'{now_year}年C类', f'{now_year}年D类']
    for name in bars:
        if name not in bar.keys():
            bar[name] = [0 for d in date]
    global_rst['date'] = ['{}月'.format(d) for d in date]
    global_rst['line'] = line
    global_rst['bar'] = bar
    return global_rst


def count_inspect_check_data(months):
    """[统计给定月份列表的监督检查数据]

    Arguments:
        months {[list]} -- [description]
    """
    month_list = get_broken_month(get_today(), 5, 5)
    start_mon = int(f'{month_list[0].year}{month_list[0].month:0>2}')
    end_mon = int(f'{month_list[-1].year}{month_list[-1].month:0>2}')
    if not isinstance(months, list):
        # 非list类型 说明每晚再更新
        # 自定义当前月为更新月
        del months
        months = [end_mon]
    mon = {'$gte': start_mon, '$lte': end_mon}
    check_documents = []
    problem_documents = []
    accident_documents = []
    check_pipeline = {  # 检查信息pipeline
        "match": {
            "MON": mon,
            "CHECK_WAY": {
                "$in": [1, 2, 3, 4]
            }
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MON": 1
        }
    }
    problem_pipeline = {  # 发现问题pipeline
        "match": {
            "MON": mon,
            "CHECK_WAY": {
                "$in": [1, 2, 3, 4]
            }
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MON": 1,
            "PROBLEM_SCORE": 1
        }
    }
    accident_pipeline = {
        "match": {
            "MON": mon,
            "MAIN_TYPE": 1,
            "RESPONSIBILITY_NAME": 1,
            "RESPONSIBILITY_IDENTIFIED": {
                "$in": [1, 2]
            },
            "DETAIL_TYPE": {
                "$ne": 3
            }
        },
        "project": {
            "_id": 0,
            "MON": 1,
            "PK_ID": 1
        }
    }
    check_coll = 'detail_check_info'
    check_documents = get_data_from_mongo_by_find(check_coll, check_pipeline)
    problem_coll = 'detail_check_problem'
    problem_documents = get_data_from_mongo_by_find(problem_coll,
                                                    problem_pipeline)
    accident_coll = 'detail_safety_produce_info'
    accident_documents = get_data_from_mongo_by_find(accident_coll,
                                                     accident_pipeline)
    check_data = pd.DataFrame(check_documents)
    problem_data = pd.DataFrame(problem_documents)
    accident_data = pd.DataFrame(accident_documents)
    missed_inspect_check_data = dict()
    for mon in months:
        # 初始化字典
        missed_inspect_check_data[mon] = dict()
    if len(check_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年检查信息无数据")
        # check_counts = [0 for mon in months]
        for mon in months:
            missed_inspect_check_data[mon]['check_counts'] = 0
    else:
        check_data = check_data.drop_duplicates('PK_ID')
        check_data['COUNT'] = 1
        check_data = check_data.groupby('MON')['COUNT'].sum().reset_index()
        # check_counts = [
        #     int(check_data['COUNT'][check_data['MON'] == mon].values[0])
        #     if mon in list(check_data['MON']) else 0 for mon in months
        # ]
        for mon in months:
            if mon in list(check_data['MON']):
                missed_inspect_check_data[mon]['check_counts'] = int(
                    check_data['COUNT'][check_data['MON'] == mon].values[0])
            else:
                missed_inspect_check_data[mon]['check_counts'] = 0

    if len(problem_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年检查问题信息无数据")
        # problem_counts = [0 for mon in months]
        # problem_scores = [0 for mon in months]
        for mon in months:
            missed_inspect_check_data[mon]['problem_counts'] = 0
            missed_inspect_check_data[mon]['problem_scores'] = 0
    else:
        problem_data = problem_data.drop_duplicates('PK_ID')
        problem_data['COUNT'] = 1
        problem_data = problem_data.groupby(
            'MON')['COUNT', 'PROBLEM_SCORE'].sum().reset_index()
        # problem_counts = [
        #     int(problem_data['COUNT'][problem_data['MON'] == mon].values[0])
        #     if mon in list(problem_data['MON']) else 0 for mon in months
        # ]
        # problem_scores = [
        #     int(problem_data['PROBLEM_SCORE'][problem_data['MON'] == mon].
        #         values[0]) if mon in list(problem_data['MON']) else 0
        #     for mon in months
        # ]
        for mon in months:
            if mon in list(problem_data['MON']):
                missed_inspect_check_data[mon]['problem_scores'] = int(problem_data[
                    'PROBLEM_SCORE'][problem_data['MON'] == mon].values[0])
                missed_inspect_check_data[mon]['problem_counts'] = int(problem_data[
                    'COUNT'][problem_data['MON'] == mon].values[0])
            else:
                missed_inspect_check_data[mon]['problem_scores'] = 0
                missed_inspect_check_data[mon]['problem_counts'] = 0
    if len(accident_data) == 0:
        current_app.logger.debug("路局第二屏监督检查情况接口：今年安全生产信息无数据")
        # accdient_counts = [0 for mon in months]
        for mon in months:
            missed_inspect_check_data[mon]['accdient_counts'] = 0
    else:
        accident_data = accident_data.drop_duplicates('PK_ID')
        accident_data['COUNT'] = 1
        accident_data = accident_data.groupby('MON').sum().reset_index()
        # accdient_counts = [
        #     int(accident_data['COUNT'][accident_data['MON'] == mon].values[0])
        #     if mon in list(accident_data['MON']) else 0 for mon in months
        # ]
        for mon in months:
            if mon in list(accident_data['MON']):
                missed_inspect_check_data[mon]['accdient_counts'] = int(accident_data[
                    'COUNT'][accident_data['MON'] == mon].values[0])
            else:
                missed_inspect_check_data[mon]['accdient_counts'] = 0
    # 更新到mongo库
    for mon, values in missed_inspect_check_data.items():
        # get_coll_prefix这个函数有点不明白，只要不是更新当天都是daily
        # 这里应该用指数那套prefix
        prefix = choose_collection_prefix(mon)
        # 不把当前月存到daily
        if prefix == 'daily_':
            prefix = 'monthly_'
        mongo.db[f'{prefix}inspect_check_data'].update(
            {"MON": mon},
            {"$set":
             {"MON": mon,
              "check_counts": values['check_counts'],
              "problem_counts": values['problem_counts'],
              "problem_scores": values['problem_scores'],
              "accdient_counts": values['accdient_counts'],
              }
             }, upsert=True)
    # global_rst.update({
    #     'date': ['{}/{}'.format(str(d)[0:4],
    #                             str(d)[4:]) for d in months],
    #     "检查人次":
    #         check_counts,
    #     "发现问题数":
    #         problem_counts,
    #     "问题质量":
    #         problem_scores,
    #     "责任事故$_2":
    #         accdient_counts
    # })
    current_app.logger.debug(
        'new_big.all_data.count_inspect_check_data [{}] init sucess.'.format(end_mon))
    return missed_inspect_check_data


# 要取去年一整年数据
def _get_inspect_check_data_first():
    '''获取今年监督检查情况数据(原函数) '''
    global_rst = {}
    # now_month = int(str(get_date(get_today()))[4:])
    # now_year = get_date(get_today())//100
    # date = [int(f'{now_year}{x:0>2}') for x in range(1, now_month + 1)]
    month_list = get_broken_month(get_today(), 5, 5)
    date = [int(f'{yue.year}{yue.month:0>2}') for yue in month_list]
    # 存在哪些月份无数据
    missed_months = []
    rst_data = dict()
    # 从monthly(prefix)_inspect_check_data直接读取数据
    for d in date:
        prefix = choose_collection_prefix(d)
        # 不把当前月存到daily
        if prefix == 'daily_':
            prefix = 'monthly_'
        month_inspect_check_data = list(mongo.db[f'{prefix}inspect_check_data'].find({
            'MON': d}, {'_id': 0, "MON": 0}))
        if len(month_inspect_check_data) == 0:
            # 记录无数据的月份
            missed_months.append(d)
            continue
        rst_data[d] = month_inspect_check_data[0]
    # 计算缺失月份的数据
    if missed_months:
        missed_inspect_check_data = count_inspect_check_data(
            missed_months)
        rst_data.update(missed_inspect_check_data)
    # 整理成前端获的标准格式
    global_rst = dict()
    global_rst['date'] = []
    global_rst['检查人次'] = []
    global_rst['发现问题数'] = []
    global_rst['问题质量'] = []
    global_rst['责任事故$_2'] = []
    for key, values in rst_data.items():
        global_rst['date'].append('{}/{}'.format(str(key)[0:4],
                                                 str(key)[4:]))
        global_rst['检查人次'].append(values['check_counts'])
        global_rst['发现问题数'].append(values['problem_counts'])
        global_rst['问题质量'].append(values['problem_scores'])
        global_rst['责任事故$_2'].append(values['accdient_counts'])
        # global_rst.update({
        #     'date': ['{}/{}'.format(str(d)[0:4],
        #                             str(d)[4:]) for d in date],
        #     "检查人次":
        #         check_counts,
        #     "发现问题数":
        #         problem_counts,
        #     "问题质量":
        #         problem_scores,
        #     "责任事故$_2":
        #         accdient_counts
        # })
    return global_rst


def _get_inspect_check_data():
    """获取今年监督检查情况数据(临时处理)"""
    global_rst = {}
    date = [201903, 201904, 201905, 201906, 201907, 201908]
    check_counts = [195968, 220443, 229704, 222669, 230960, 0]
    problem_counts = [85898, 88633, 89599, 89159, 92726, 0]
    problem_scores = [215112, 221667, 224577, 224545, 230714, 0]
    accdient_counts = [0, 6, 4, 4, 4, 0]
    global_rst.update({
        'date': ['{}/{}'.format(str(d)[0:4],
                                str(d)[4:]) for d in date],
        "检查人次":
            check_counts,
        "发现问题数":
            problem_counts,
        "问题质量":
            problem_scores,
        "责任事故$_2":
            accdient_counts
    })
    return global_rst


def _get_evaluate_table_data():
    '''获取干部累计记分情况数据'''
    now_year = get_date(get_today()) // 100
    keys = {
        "match": {
            "MON": {
                '$gt': int('{}00'.format(now_year))
            }
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MAJOR": 1,
            "SCORE": 1,
            "PERSON_ID": 1,
            "IDENTITY": 1
        }
    }
    coll = 'detail_evaluate_record'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("干部履职积分接口无数据")
        return
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        # data = data[data['IDENTITY'] == "干部"]
        data = data.groupby(['PERSON_ID',
                             'MAJOR'])['SCORE'].sum().reset_index()
        data['LEVEL'] = data['SCORE'].apply(singe_score_section)
        data = data.groupby(['LEVEL', 'MAJOR']).count().reset_index()
        data['COUNT'] = data['SCORE']
        del data['SCORE'], data['PERSON_ID']
        total_level = data
    LEVEL = [i for i in range(1, 8)]
    total_count = [
        sum(list(total_level['COUNT'][total_level['LEVEL'] == i]))
        if i in list(total_level['LEVEL']) else 0 for i in LEVEL
    ]
    total_count.append(sum(total_count))
    total_count.insert(0, "合计")
    MAJOR = ['车务', '机务', '工务', '电务', '车辆', '供电']
    data_model = [{
        "MAJOR": major,
        "LEVEL": level
    } for major in MAJOR for level in LEVEL]
    data = pd.merge(
        data, pd.DataFrame(data_model), how='right', on=['MAJOR', 'LEVEL'])
    data = data.fillna(0)

    count = data.groupby(['LEVEL']).sum(by='COUNT')
    total = list(count['COUNT'].apply(lambda x: int(x)))
    total.append(sum(total))
    data = data.groupby(['MAJOR'])
    global_rst = {}
    table_data = [0 for i in range(7)]
    for name, group in data:
        group = group.groupby('LEVEL').sum(by='COUNT')
        idx = MAJOR.index(name)
        tr_data = list(group['COUNT'].apply(lambda x: int(x)))
        tr_data.insert(0, name)
        tr_data.append(sum(list(group['COUNT'].apply(lambda x: int(x)))))
        table_data[idx] = tr_data
    table_data[6] = total_count
    TITLE = [
        "∑≤2", "2<∑≤4", "4<∑≤6", "6<∑≤8", "8<∑≤10", "10<∑≤12", "∑>12", "合计"
    ]
    global_rst['title'] = TITLE
    global_rst['data'] = table_data
    return global_rst


def _get_stress_issue_data():
    '''获取近期路局红线问题数据（近一周）'''
    documents = []
    today = get_today()
    last_day = today - relativedelta(days=7)
    last_day = int(str(last_day)[:10].replace('-', ''))
    coll = 'detail_check_problem'
    keys = {
        "match": {
            "DATE": {
                "$gte": last_day
            },
            "IS_RED_LINE": 1
        },
        "project": {
            "_id": 0,
            "DESCRIPTION": 1,
            "PK_ID": 1,
            "DATE": 1,
            "PROBLEM_DIVIDE_NAMES": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    data = pd.DataFrame(documents)
    if data.empty:
        current_app.logger.debug("近期无路局红线问题")
        return []
    data = data.drop_duplicates(['PK_ID']).reset_index()
    result = []
    for index in data.index:
        if data.loc[index].at['PROBLEM_DIVIDE_NAMES'] != '其他':
            result.append({
                "content":
                    data.loc[index].at['DESCRIPTION'],
                "tags":
                    data.loc[index].at['PROBLEM_DIVIDE_NAMES'].split(","),
                "date":
                    '{}/{}/{}'.format(
                        str(data.iloc[index].at['DATE'])[:4],
                        str(data.iloc[index].at['DATE'])[4:6],
                        str(data.iloc[index].at['DATE'])[6:])
            })
    return result


def _get_evaluate_produce_data():
    '''履职评价与安全生产信息趋势折线图相关数据'''
    # now_month = int(str(get_date(get_today()))[4:])
    # now_year = get_date(get_today())//100
    # date = [
    #     int('{}{:0>2}'.format(now_year, i)) for i in range(1, now_month + 1)
    # ]
    month_list = get_broken_month(get_today(), 5, 5)
    start_mon = int(f'{month_list[0].year}{month_list[0].month:0>2}')
    end_mon = int(f'{month_list[-1].year}{month_list[-1].month:0>2}')
    mon = {'$gte': start_mon, '$lte': end_mon}
    date = [int(f'{yue.year}{yue.month:0>2}') for yue in month_list]
    evaluate_pipeline = [
        {  #
            "$match": {
                "MON": mon
            }
        },
        {
            "$group": {
                "_id": "$MON",
                "AMOUNT": {
                    "$sum": 1
                },
                "TOTAL": {
                    "$sum": "$SCORE"
                }
            }
        },
        {
            "$sort": {
                "_id": 1
            }
        }
    ]
    pipeline = {
        "match": {
            "MON": mon,
            "RESPONSIBILITY_NAME": 1
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MON": 1,
            "PONDERANCE_NUMBER": 1
        }
    }
    evaluate_coll = 'detail_evaluate_record'
    produce_coll = 'detail_safety_produce_info'
    doc = get_data_from_mongo_by_find(produce_coll, pipeline)
    data = pd.DataFrame(doc)
    # date = [
    #     int('{}{:0>2}'.format(now_year, i)) for i in range(1, now_month + 1)
    # ]
    if data.empty:
        pro_times = [0 for x in date]
        pro_score = [0 for x in date]
    else:
        data = data.fillna(0)
        data = data.drop_duplicates('PK_ID').reset_index()
        data_total = data.groupby(
            'MON')['PONDERANCE_NUMBER'].sum().reset_index()
        data_count = data.groupby('MON').size().reset_index()

        produce_data = pd.merge(data_total, data_count, how='left', on='MON')
        produce_data['AMOUNT'] = produce_data[0]
        pro_times = [
            int(produce_data['AMOUNT'][produce_data['MON'] == mon].
                values[0]) if mon in list(produce_data['MON']) else 0
            for mon in date
        ]
        pro_score = [
            int(produce_data['PONDERANCE_NUMBER'][
                produce_data['MON'] == mon].values[0])
            if mon in list(produce_data['MON']) else 0
            for mon in date
        ]
    evaluate_documents = get_data_from_mongo_by_aggregate(
        evaluate_coll, evaluate_pipeline)
    evaluate_data = pd.DataFrame(evaluate_documents)
    if evaluate_data.empty:
        eva_times = [0 for x in date]
        eva_score = [0 for x in date]
    else:
        eva_times = [
            int(evaluate_data['AMOUNT'][evaluate_data['_id'] == mon].
                values[0]) if mon in list(evaluate_data['_id']) else 0
            for mon in date
        ]
        eva_score = [
            int(evaluate_data['TOTAL'][evaluate_data['_id'] == mon].
                values[0]) if mon in list(evaluate_data['_id']) else 0
            for mon in date
        ]
    return {
        "date": ['{}/{}'.format(str(d)[:4],
                                str(d)[4:]) for d in date],
        "TIMES":
            [{
                "履职评价条数": eva_times
            },
                {
                    "生产信息条数": pro_times
            }],
        "SCORE": [{
            "履职评价累计扣分": eva_score
        },
            {
                "生产信息严重性值": pro_score
        }]
    }


def _get_evaluate_issue_data():
    # 管道
    today = get_today()
    # if today.month != 12 or today.day < 25:
    # year = today.year - 1
    # else:
    # year = today.year
    year = get_date(today) // 100
    keys = {
        "match": {
            "DATE": {
                "$gte": int(f'{year}0000'),
                "$lte": int(f'{year + 1}0000')
            }
            # "CHECK_TYPE": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "CODE": 1,
            "GRADATION": 1
        }
    }
    coll = 'detail_evaluate_record'
    doc = get_data_from_mongo_by_find(coll, keys)
    data = pd.DataFrame(doc)
    if data.empty:
        return {}
    data = data.drop_duplicates('PK_ID')
    data['COUNT'] = 1
    data = data[data['GRADATION'] != '非管理和专业技术人员'][['CODE', 'COUNT']]
    data = data.groupby('CODE').sum().reset_index()
    # 补全为出现的履职问题
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    data['NAME'] = data['CODE'].apply(lambda x: code.get(x))
    del data['CODE']
    for name in list(code.values()):
        if name not in list(data['NAME']):
            data = pd.concat(
                [data,
                 pd.DataFrame([[0, name]], columns=['COUNT', 'NAME'])],
                ignore_index=True)
    total = sum(list(data['COUNT']))
    data = data[data['NAME'] != '检查信息录入']
    columns = ['NAME', 'COUNT', 'COUNT']
    result = get_pie_data(columns, data, columns)
    result['total'] = total
    return result


def _get_check_poor_data():
    '''得到检查力度不高单位'''
    # 计算上月日期
    # mon, year = get_date()
    # 获取检查力度指数
    documents = []
    today = get_today()
    month = get_date(today - relativedelta(months=1))
    coll_name = '{}detail_health_index'.format('monthly_')
    documents = list(mongo.db[coll_name].find({
        "MON": month,
        "MAIN_TYPE": 1,
        "DETAIL_TYPE": 0,
        "HIERARCHY": 3,
    }, {
        "_id": 0,
        "DEPARTMENT_NAME": 1,
        "SCORE": 1
    }).sort([('SCORE', 1)]).limit(10))
    data = pd.DataFrame(documents)
    data = data[data['SCORE'] < 75]
    documents = json.loads(data.to_json(orient='records'))
    result = []
    for idx, val in enumerate(documents):
        result.append({
            "name": val['DEPARTMENT_NAME'],
            "rank": idx + 1,
            "amount": round(val['SCORE'], 2)
        })
    return result


def _get_safe_manage_poor_data():
    '''得到管理力度薄弱单位'''
    # 计算上月日期
    # mon, year = get_date()
    # 获取检查力度指数
    documents = []
    today = get_today()
    month = get_date(today - relativedelta(months=1))
    coll_name = '{}health_index'.format('monthly_')
    documents = list(mongo.db[coll_name].find({
        "MON": month,
        "HIERARCHY": 3,
        "SCORE": {
            "$lt": 75
        }
    }, {
        "_id": 0,
        "DEPARTMENT_NAME": 1,
        "MAJOR": 1,
        "SCORE": 1
    }).sort([('SCORE', 1)]))
    if len(documents) == 0:
        current_app.logger.debug("管理力度薄弱无上月数据（按自然月计算）")
        return
    else:
        data = pd.DataFrame(documents)
        majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
        result = []
        for major in majors:
            group = data[data['MAJOR'] == major]
            group = group.reset_index()
            result.append({
                major: [{
                    "NAME": group.loc[index].at['DEPARTMENT_NAME'],
                    "SCORE": int(group.loc[index].at['SCORE'])
                } for index in group.index]
            })
    return result


def _get_risk_possible_data():
    # now_month = int(str(get_date(get_today()))[4:])
    # now_year = get_date(get_today())//100

    month_list = get_broken_month(get_today(), 5, 5)
    start_mon = int(f'{month_list[0].year}{month_list[0].month:0>2}')
    end_mon = int(f'{month_list[-1].year}{month_list[-1].month:0>2}')
    mon = {'$gte': start_mon, '$lte': end_mon}
    date = [int(f'{yue.year}{yue.month:0>2}') for yue in month_list]
    pipeline = [{
        "$match": {
            "MON": mon
        }
    },
        {
            "$group": {
                "_id": {
                    "RISK_CONSEQUENCE_NAME": "$RISK_CONSEQUENCE_NAME",
                    "MON": "$MON"
                },
                "COUNT": {
                    "$sum": 1
                }
            }
    }]
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_aggregate(coll, pipeline)
    data = [{
        "MON": item['_id']['MON'],
        "RISK": item['_id']['RISK_CONSEQUENCE_NAME'],
        "COUNT": item['COUNT']
    } for item in documents]

    data = pd.DataFrame(data)
    data = pd.merge(
        data.groupby('RISK')['COUNT'].mean().reset_index().rename(
            index=str, columns={"COUNT": "MEAN"}),
        data,
        how='left',
        on='RISK')
    data = data.sort_values(by=['COUNT'], ascending=False).reset_index()
    data = data[~data['RISK'].str.contains('（党）', regex=False)]
    data = data.groupby('RISK', sort=False)
    col = 'COUNT'
    result = get_broken_line_group(data, date, col)
    result.update({
        'date': ['{}/{}'.format(str(d)[0:4],
                                str(d)[4:]) for d in date]
    })
    return result


def _get_serious_risk_data():
    month_list = get_broken_month(get_today(), 5, 5)
    start_mon = int(f'{month_list[0].year}{month_list[0].month:0>2}')
    end_mon = int(f'{month_list[-1].year}{month_list[-1].month:0>2}')
    mon = {'$gte': start_mon, '$lte': end_mon}
    date = [int(f'{yue.year}{yue.month:0>2}') for yue in month_list]
    safe_keys = {
        "match": {
            "MON": mon,
            "RESPONSIBILITY_NAME": 1,
            "MAIN_TYPE": {
                "$in": [1, 2]
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RISK_NAME": 1,
            "MON": 1
        }
    }
    safe_coll = 'detail_safety_produce_info'
    problem_coll = 'detail_check_problem'
    problem_keys = {
        "match": {
            "MON": mon
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "MON": 1,
            "RISK_NAME": 1
        }
    }
    safe_documents = get_data_from_mongo_by_find(safe_coll, safe_keys)
    problem_documents = get_data_from_mongo_by_find(problem_coll, problem_keys)
    if safe_documents and problem_documents:
        # 安全生产信息数据
        safe_data = pd.DataFrame(safe_documents)
        safe_data = safe_data.drop_duplicates(['PK_ID', 'RISK_NAME'])
        # 发现问题数据
        problem_data = pd.DataFrame(problem_documents)
        problem_data = problem_data.drop_duplicates(['PK_ID', 'RISK_NAME'])
        risks = list(set(safe_data['RISK_NAME']))
        # title = [int(f'{now_year}{i:0>2}') for i in range(1, now_month + 1)]
        names = ['安全问题', '责任事故故障$_2']
        result = []
        for risk in risks:
            risk_data = safe_data[safe_data['RISK_NAME'] == risk]
            pro_data = problem_data[problem_data['RISK_NAME'] == risk]
            datas = [pro_data, risk_data]
            brokenLine = get_trendency_data(names, datas, date)
            a = 1
            b = 1
            error = '责任事故故障$_2'
            if sum(brokenLine['安全问题']) == 0:
                del brokenLine['安全问题']
                brokenLine['责任事故故障'] = brokenLine.pop('责任事故故障$_2')
                error = '责任事故故障'
                a = 0
            if sum(brokenLine[error]) == 0:
                del brokenLine[error]
                b = 0
            if a + b >= 1:
                result.append({"data": brokenLine, "title": risk})
        return result
    else:
        current_app.logger.debug('今年无责任事故发生')
        return []


def _get_safe_warnning_data():
    '''获取安全警告通知数据'''
    today = int(str(get_today())[:10].replace('-', ''))
    global_rst = []
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 1,
            "HIERARCHY": 1
        },
        "project": {
            "_id": 0,
            "MAJOR": 1,
            'DUTY_NAME': 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1,
            "RANK": 1,
            "WARN_TYPE": 1,
            "SHORT_DESCRIPTION": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("路局今日无安全警告信息")
        return global_rst
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID']).reset_index()
        data = data.fillna("无")
        for index in data.index:
            sta = data.loc[index].at['DUTY_NAME']
            typ = data.loc[index].at['WARN_TYPE']
            lev = data.loc[index].at['RANK']
            sho = data.loc[index].at['SHORT_DESCRIPTION']
            content = f'{sta} {typ} {lev}级 ({sho})'
            global_rst.append({
                "content":
                    content,
                "date":
                    str(data.loc[index].at['START_DATE'])[:4] + '/' + str(
                        data.loc[index].at['START_DATE'])[4:6] + '/' + str(
                        data.loc[index].at['START_DATE'])[6:]
            })
        return global_rst


def _get_safe_notice_data():
    '''获取安全提示通知数据'''
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    global_rst = []
    keys = {
        "match": {
            "START_DATE": {
                "$lte": yesterday
            },
            "END_DATE": {
                "$gte": yesterday
            },
            "TYPE": 2
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "MAJOR": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("路局今日无安全提示信息")
        return global_rst
    else:

        data = pd.DataFrame(documents)
        documents = json.loads(data.to_json(orient='records'))
        for item in documents:
            if item['CONTENT'] is None:
                continue
            if len(item['CONTENT']) < 10:
                continue
            global_rst.append({
                "content":
                    item['CONTENT'],
                "tags": [item['MAJOR']],
                "date":
                    str(item['START_DATE'])[:4] + '/' + str(
                        item['START_DATE'])[4:6] + '/' + str(
                        item['START_DATE'])[6:]
            })
        return global_rst


def _get_auto_notice_data():
    '''获取自动提示通知数据'''
    today = int(str(get_today())[:10].replace('-', ''))
    global_rst = []
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 3
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("路局今日无自动提示信息")
        return global_rst
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('CONTENT').reset_index()
        data = data[~data.CONTENT.str.contains("责任扣分", regex=False)]
        documents = json.loads(data.to_json(orient='records'))

        for item in documents:
            global_rst.append({
                "content":
                    item['CONTENT'],
                "tags": [item['NAME']],
                "date":
                    str(item['START_DATE'])[:4] + '/' + str(
                        item['START_DATE'])[4:6] + '/' + str(
                        item['START_DATE'])[6:]
            })
        return global_rst


def _get_key_people_data():
    # today = get_today()
    # month = get_mon_from_date(today)
    now_year = get_date(get_today()) // 100
    # now_year = 2018
    keys = {
        "match": {
            "MON": {
                "$gt": int(f'{now_year}00')
            }
            # "MON": month
        },
        "project": {
            "_id": 0,
            "DISPOSE_METHOD_NAME": 1,
            "PERSON_NAME": 1,
            "STATUS_NAME": 1,
            "MAJOR": 1,
            "MON": 1,
        }
    }
    coll = 'key_person'
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        data = pd.DataFrame(documents)
        data = data.fillna('a')
        data = data.drop_duplicates(['PERSON_NAME', 'DISPOSE_METHOD_NAME'])
        majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
        dai_total = 0
        pei_total = 0
        gang_total = 0
        bang_total = 0
        he_total = 0
        result = []
        for major in majors:
            tr_rst = [major]
            major_data = data[data['MAJOR'] == major]
            dai = len(major_data[major_data['STATUS_NAME'] == "待处置"]) + \
                len(major_data[major_data['STATUS_NAME'] == "待确认"])
            pei = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
                '培训', regex=False)])
            gang = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
                '岗', regex=False)])
            bang = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
                '帮促', regex=False)])
            he = len(major_data[major_data['STATUS_NAME'] == "待确认"]) + \
                len(major_data[~ major_data.DISPOSE_METHOD_NAME.str.contains(
                    "前期已处置", regex=False)]) - len(major_data[
                        major_data.DISPOSE_METHOD_NAME.str.contains(
                            "a", regex=False)])
            tr_rst.append(dai)
            tr_rst.append(pei)
            tr_rst.append(gang)
            tr_rst.append(bang)
            tr_rst.append(he)
            dai_total += dai
            pei_total += pei
            gang_total += gang
            bang_total += bang
            he_total += he
            result.append(tr_rst)
        result.append(
            ["合计", dai_total, pei_total, gang_total, bang_total, he_total])
        title = ["", '待处置', '培训', '待岗、离岗、转岗', '帮促', '合计']
    else:
        current_app.logger.debug("路局无重点人员数据（key_person出错）")
        result = []
        title = []
    return {"data": result, "title": title}


def _get_key_people_data_ex():
    now_year = get_date(get_today())
    keys = {
        "match": {
            "MON": int(f'{now_year}'),
            # "MON": month
        },
        "project": {
            "_id": 0,
            "DATA": 1,
            "TITLE": 1,
            "MON": 1,
        }
    }
    coll = 'key_person'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) != 0:
        result = documents[0]['DATA']
        title = documents[0]['TITLE']
    else:
        current_app.logger.debug("路局无重点人员数据（key_person出错）")
        result = []
        title = []
    return {"data": result, "title": title}


def get_data(node):
    funcName = {
        'a_duty': _get_days_without_accident,  # 第一屏无责任A类事故天数
        'safe_produce_notice': _get_safety_produce_notification,  # 第一屏安全生产信息通知
        'safe_produce': _get_safety_produce_info_trendency,  # 第一屏安全事故、故障趋势图
        'safe_duty_produce': _get_safe_duty_produce_info,  # 第一屏安全生产信息统计
        'risk_distribute': _get_risk_distribute_data,  # 第一屏风险分布
        'typical_accident': _get_typical_accident_data,  # 第一屏典型事故通知
        'safe_produce_reason': _get_safe_produce_reason_data,  # 第一屏安全生产原因饼图
        'duty_accident_error': _get_duty_accident_error_data,  # 第一屏事故故障信息折线图
        # 第二屏
        'inspect_check': _get_inspect_check_data_first,  # 第二屏监督检查信息统计折线图
        'inspect_check_test': _get_inspect_check_data_first,
        'evaluate_table': _get_evaluate_table_data,  # 第二屏干部积分统计表
        'stress_issue': _get_stress_issue_data,  # 第二屏突出问题通知
        'evaluate_produce':
            _get_evaluate_produce_data,  # 第二屏履职评价与安全生产信息趋势折线图相关数据
        'evaluate_issue': _get_evaluate_issue_data,  # 第二屏干部履职问题分布饼图
        'check_poor': _get_check_poor_data,  # 第二屏检查力度不高单位排名
        # 第三屏
        'safe_manage_poor': _get_safe_manage_poor_data,  # 第三屏安全管理薄弱单位数据
        'risk_possible': _get_risk_possible_data,  # 第三屏风险可能后果分析趋势折线图
        'serious_risk': _get_serious_risk_data,  # 第三屏近期重点风险相关趋势折线图
        'safe_warnning': _get_safe_warnning_data,  # 第三屏安全警告通知数据
        'safe_notice': _get_safe_notice_data,  # 第三屏安全提示通知数据
        'auto_notice': _get_auto_notice_data,  # 第三屏自动提示通知数据
        # 'key_people': _get_key_people_data  # 第三屏重点违章人员统计分析
        'key_people': _get_key_people_data_ex  # 新第三屏重点违章人员统计分析
    }

    if node not in funcName:
        return '参数错误'
    return funcName[node]()


if __name__ == '__main__':
    pass
