#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    路局大屏【老版】API数据[左屏]
'''

from dateutil.relativedelta import relativedelta
from flask import current_app

import pandas as pd

from app import mongo
from app.new_big.util import (get_coll_prefix, get_month, get_scce_data,
                              get_week_hours)
from app.utils.common_func import get_history_months, get_today


def _get_days_without_accident():
    # 无事故安全生产天数
    data = mongo.db.base_data.find_one({'TYPE': 'days_without_accident'})
    return {'safe_produce_duration': data['VALUE']}


def _get_safety_produce_notification():
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    prefix = get_coll_prefix()
    coll_name = '{}detail_safety_produce_info'.format(prefix)
    documents = list(mongo.db[coll_name].find(
        {
            'DATE': yesterday
        },
        {
            '_id': 0,
            'OVERVIEW': 1,
            'PK_ID': 1,
            # 'MAIN_TYPE': 1,
            # 'DETAIL_TYPE': 1,
            'CLOCK': 1,
        }))
    records = []
    tmp_id_set = set()
    for record in documents:
        if record['PK_ID'] in tmp_id_set:
            continue
        else:
            tmp_id_set.add(record['PK_ID'])
        record['content'] = record['OVERVIEW']
        record['date'] = '{}/{}/{} {}'.format(
            str(yesterday)[:4],
            str(yesterday)[4:6],
            str(yesterday)[6:], record['CLOCK'])
        del record['CLOCK']
        del record['OVERVIEW']
        records.append(record)
    return records


def code_name_map(code_name):
    # 履值问题代码-文字描述
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    return code.get(code_name, None)


def _get_evaluate_classify_stat_data():
    prefix = get_coll_prefix()
    mon = get_month()
    coll_name = '{}detail_evaluate_record'.format(prefix)
    # 管道
    pipeline = [{
        "$match": {
            "MON": mon,
            "CHECK_TYPE": 1
        }
    }, {
        "$group": {
            "_id": "$CODE",
            "amount": {
                "$sum": 1
            }
        }
    }]
    documents = list(mongo.db[coll_name].aggregate(pipeline))
    data = {doc['_id']: doc['amount'] for doc in documents}
    if len(data) == 0:
        return {'pivot': 0, 'data': []}
    privot = sum(list(data.values())) / len(data)
    # 补全为出现的履职问题
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    for each in code:
        if each not in data:
            data.update({each: 0})
    # pie图分2类颜色，所以数据按均值分2部分
    big_half = {k: v for k, v in data.items() if v >= privot}
    small_half = {k: v for k, v in data.items() if v < privot}
    big_half = sorted(big_half.items(), key=lambda x: x[1], reverse=True)
    small_half = sorted(small_half.items(), key=lambda x: x[1], reverse=True)
    result = []
    for idx, val in enumerate(big_half):
        result.append({
            'name': code_name_map(val[0]),
            'value': val[1],
            'rank': idx
        })
    for idx, val in enumerate(small_half):
        result.append({
            'name': code_name_map(val[0]),
            'value': val[1],
            'rank': len(small_half) - idx - 1
        })
    return {'pivot': round(privot, 2), 'data': result}


def _get_problem_classify_data():
    # 统计近3个月数据
    # prefix = get_coll_prefix()
    # mon = get_month()
    mon = get_history_months(mondelta=-2)
    prefix = ['daily_', 'monthly_']
    documents = []
    for _prefix in prefix:
        coll_name = '{}global_safety_produce_info'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({
                "MON": {
                    "$lte": mon[1],
                    "$gte": mon[0]
                }
            }, {
                "_id": 0,
                "DATE": 0
            })))
    data = pd.DataFrame(documents)
    if len(data) == 0:
        return {'pivot': 0, 'data': []}
    data_sum = data.groupby(by=['PROBLEM_POINT'])['PONDERANCE_NUMBER'].sum()
    data_size = data.groupby(by=['PROBLEM_POINT']).size()
    xdata = pd.concat([data_sum, data_size], axis=1, join='outer', sort=False)
    xdata.sort_values(by='PONDERANCE_NUMBER', ascending=False, inplace=True)
    # 取严重性值前10的问题项点
    xdata = xdata[:10]
    problem_count = {}
    ponderance = {}
    for index in xdata.index:
        problem_count.update({index: int(xdata.loc[index].values[1])})
        ponderance.update({index: float(xdata.loc[index].values[0])})
    if len(ponderance) == 0:
        return {'pivot': 0, 'data': []}
    # format, pie图分2类颜色，所以数据按均值分2部分
    pivot = sum(list(ponderance.values())) / len(ponderance)
    big_half = {k: v for k, v in ponderance.items() if v >= pivot}
    small_half = {k: v for k, v in ponderance.items() if v < pivot}
    big_half = sorted(big_half.items(), key=lambda x: x[1], reverse=True)
    small_half = sorted(small_half.items(), key=lambda x: x[1], reverse=True)
    result = []
    for idx, val in enumerate(big_half):
        result.append({
            'name': val[0],
            'value': val[1],
            'amount': problem_count.get(val[0]),
            'rank': idx
        })
    for idx, val in enumerate(small_half):
        result.append({
            'name': val[0],
            'value': val[1],
            'amount': problem_count.get(val[0]),
            'rank': len(small_half) - idx - 1
        })
    return {'pivot': round(pivot, 2), 'data': result}


def _get_safety_produce_info_trendency():
    documents = []
    for _prefix in ['daily_', 'monthly_']:
        coll_name = '{}global_safety_produce_info'.format(_prefix)
        documents.extend(
            list(mongo.db[coll_name].find({}, {
                "_id": 0,
                "PK_ID": 1,
                "MON": 1,
                "MAJOR": 1
            })))
    data = pd.DataFrame(documents)
    data.drop_duplicates('PK_ID', inplace=True)
    global_data = data.groupby(['MON']).size().sort_index()
    global_date = global_data.index
    global_rst = {
        'date': ['{}/{}'.format(str(x)[:4],
                                str(x)[4:]) for x in global_date],
        'total': [int(x) for x in global_data.values]
    }
    majors_data = data.groupby(['MAJOR', 'MON']).size().sort_index()
    majors_rst = {
        'date': ['{}/{}'.format(str(x)[:4],
                                str(x)[4:]) for x in global_date]
    }
    majors_dict = {}
    for index in majors_data.index:
        if index[0] in majors_dict:
            majors_dict[index[0]].update({index[1]: int(majors_data[index])})
        else:
            majors_dict.update({index[0]: {index[1]: int(majors_data[index])}})
    major_name_map = {
        '工务': 'gongwu',
        '电务': 'dianwu',
        '机务': 'jiwu',
        '车务': 'chewu',
        '供电': 'gongdian',
        '车辆': 'cheliang',
    }
    for major in majors_dict:
        _size = []
        for _date in global_date:
            _size.append(majors_dict[major].get(_date, 0))
        if major in major_name_map:
            majors_rst.update({major_name_map[major]: _size})

    return {
        'global': global_rst,
        'majors': majors_rst,
    }


def _get_abnormal_major_stat():
    majors = ['工务', '车务', '机务', '车辆', '电务', '供电']
    val = [0 for x in range(6)]
    total = [0 for x in range(6)]
    mon = get_history_months(mondelta=-1)[0]
    data = list(mongo.db['monthly_health_index'].find({
        'MON': mon,
    }, {
        '_id': 0,
        'MAJOR': 1,
        'SCORE': 1,
    }))
    for each in data:
        if each['MAJOR'] in majors:
            if each['SCORE'] < 70:
                val[majors.index(each['MAJOR'])] += 1
            total[majors.index(each['MAJOR'])] += 1
    return [{
        'name': v[0],
        'amount': v[1],
        'total': v[2]
    } for v in zip(majors, val, total)]


def _get_departments():
    # 获取所有的站段[经纬度、名字、ID]
    prefix = get_coll_prefix()

    condition = {'LEVEL': 1}
    if prefix != 'daily_':
        condition.update({'MON': get_history_months(-1)[0]})
    documents = list(mongo.db[f'{prefix}map_data'].find(
        condition, {
            "_id": 0,
            "DPID": 1,
            "NAME": 1,
            "LONGITUDE": 1,
            "LATITUDE": 1,
            "CHECK_PROBLEM": 1,
            "CHECK_INFO": 1,
            "EVALUATE": 1,
            "SAFETY_PRODUCE_INFO": 1,
        }))
    result = {}
    for document in documents:
        result.update({document['DPID']: document})
    return result


def _get_child_index_zhanduan():
    """获取站段的安全综合指数信息（上月）

    Returns:
        dict --
    """
    mon = get_history_months(-1)[0]
    query_condition = {'MON': mon, 'DETAIL_TYPE': 0}
    index_data = list(mongo.db['monthly_detail_health_index'].find(
        query_condition, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "SCORE": 1,
            "MAIN_TYPE": 1
        }))
    rst_child_index = {}
    df_index = pd.DataFrame(index_data)
    groups = df_index.groupby(['DEPARTMENT_ID'])
    for k, v in groups:
        index_value = [0 for _ in range(6)]  # 6个子指数
        for idx, val in v.iterrows():
            score = round(val['SCORE'], 2)
            main_type = int(val['MAIN_TYPE'])
            index_value[main_type - 1] = score
        rst_child_index.update({k: index_value})
    return rst_child_index


def _get_map_zhanduan_info():
    departments = _get_departments()
    dpid_child_index = _get_child_index_zhanduan()
    result = []
    for dpid in departments:
        val = departments[dpid]
        result.append({
            'value': [val['LONGITUDE'], val['LATITUDE']],
            'name':
            val['NAME'],
            'dim': [
                val['SAFETY_PRODUCE_INFO'], val['CHECK_PROBLEM'],
                val['EVALUATE'], val['CHECK_INFO']
            ],
            'radar': {
                'value': dpid_child_index.get(dpid, [0, 0, 0, 0, 0, 0]),
                'range': [0, 100]
            }
        })
    return result


def _get_check_problem_scatter():
    # 获取当月检查问题热力图
    prefix = get_coll_prefix()
    mon = get_month()
    coll_name = '{}detail_check_problem'.format(prefix)
    documents = list(mongo.db[coll_name].find({
        "MON": mon
    }, {
        "_id": 0,
        "PK_ID": 1,
        "hour_section": 1,
        "weekday": 1
    }))
    if len(documents) == 0:
        return {
            'scatter': [],
            'scatter_emphasis': [],
        }
    data = pd.DataFrame(documents)
    data.drop_duplicates('PK_ID', inplace=True)
    xdata = data.groupby(['hour_section', 'weekday']).size()
    # 结果数据集
    scatter = []
    scatter_emphasis = []
    pivot = xdata.sort_values(ascending=False)[10]
    for idx in xdata.index:
        x_idx = int(idx[0]) - 1
        y_idx = int(idx[1]) - 1
        val = int(xdata[idx])
        if val > pivot:
            scatter_emphasis.append([x_idx, y_idx, val])
        else:
            scatter.append([x_idx, y_idx, val])
    return {
        'scatter': scatter,
        'scatter_emphasis': scatter_emphasis,
    }


def _get_check_info_scatter():
    # 获取当月检查频次散点图
    prefix = get_coll_prefix()
    mon = get_month()
    coll_name = '{}detail_check_info'.format(prefix)
    documents = list(mongo.db[coll_name].find({
        "MON": mon
    }, {
        "_id": 0,
        "PK_ID": 1,
        "START_CHECK_HOUR": 1,
        "END_CHECK_HOUR": 1
    }))
    if len(documents) == 0:
        return {
            'scatter': [],
            'scatter_emphasis': [],
        }
    data = pd.DataFrame(documents)
    data.drop_duplicates('PK_ID', inplace=True)
    rst_grid = [[0 for col in range(7)] for row in range(7)]
    for idx in data.index:
        _stime = str(data.at[idx, 'START_CHECK_HOUR'])
        _etime = str(data.at[idx, 'END_CHECK_HOUR'])
        week_hours = get_week_hours(_stime, _etime)
        for item in week_hours:
            weekday = item[0] - 1
            hour = item[1] - 1
            rst_grid[hour][weekday] += 1
    # 结果数据集
    rst = [[hour, weekday, rst_grid[hour][weekday]] for hour in range(7)
           for weekday in range(7)]
    pivot = sorted(rst, key=lambda x: x[2], reverse=True)[10][-1]
    scatter = []
    scatter_emphasis = []
    for item in rst:
        if item[2] > pivot:
            scatter_emphasis.append(item)
        else:
            scatter.append(item)
    return {
        'scatter': scatter,
        'scatter_emphasis': scatter_emphasis,
    }


def _get_majors_check_stat_info():
    majors = ['工务', '车务', '机务', '车辆', '电务', '供电']
    result = []
    for major in majors:
        data = get_scce_data(major=major)
        amount = [item['amount'] for item in data]
        result.append({'name': major, 'amount': amount})
    return result


def _get_analysis_center_daily():
    """
    总计＝　sum（[处级干部，科级干部，一般干部] ）按专业分
    """
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    # today = 20180510  # test
    prefix = get_coll_prefix()
    # prefix = 'monthly_'  # test
    coll_name = '{}analysis_center_daily'.format(prefix)
    documents = list(mongo.db[coll_name].find({
        "DATE": yesterday
    }, {
        "_id": 0,
        "PK_ID": 0,
        "DATE": 0,
        "MONTHLY_PROBLEM_NUMBER": 0,
        "MONTHLY_COMPLETE": 0
    }))
    rst = {}
    for document in documents:
        FLAG = int(document['FLAG'])
        TYPE = int(document['TYPE'])
        # MAJOR = document['NAME']
        COMPLETE = document['DAILY_COMPLETE']
        if FLAG in [3, 4, 5, 6, 7]:
            if TYPE in [2, 3, 4]:
                rst[FLAG] = rst.get(FLAG, 0) + COMPLETE
        elif FLAG == 1:
            rst[2] = rst.get(2, 0) + COMPLETE
        elif FLAG == 2:
            if TYPE == 5:
                rst[2] = round((rst.get(2, 0) + COMPLETE), 1)
        else:
            pass
    rtn_text = """今日完成视频调阅复查{}小时，干部履职评价{}条，干部履职评价{}人次，
    复查履职评价{}条，干部履职复查{}人次，阶段评价{}人次。
    """.format(rst.get(2, 0), *[int(rst.get(k, 0)) for k in [3, 4, 5, 6, 7]])

    return {
        "works": {
            "check": rtn_text,
            "issue": "音视频调阅（复查）发现问题24个，干部履职评价（复查）发现13个问题。"
        },
        "global_check": {
            "check_amount": 18562,
            "issue_amount": {
                "zuoye": 590,
                "shebei": 238,
                "guanli": 252,
                "zhanduan": 2764,
                "luju": 2608,
                "total": 6452
            }
        }
    }


def _get_risk_tendency():
    prefix = get_coll_prefix()
    condition_check = {}
    condition_safety = {}
    if prefix == 'monthly_':
        mon = get_month()
        condition_check = {"MON": mon}
        condition_safety = {"MON": mon}
    documents = []
    coll_name = '{}global_safety_produce_info'.format(prefix)
    documents.extend(
        list(mongo.db[coll_name].aggregate([{
            "$match": condition_safety
        }, {
            "$project": {
                "SERIOUS_VALUE": "$PONDERANCE_NUMBER",
                "_id": 0,
                "DATE": 1,
                "TYPE": 1
            }
        }])))
    coll_name = '{}detail_check_problem'.format(prefix)
    documents.extend(
        list(mongo.db[coll_name].find(condition_check, {
            "_id": 0,
            "DATE": 1,
            "SERIOUS_VALUE": 1,
            "TYPE": 2
        })))
    if len(documents) == 0:
        result = {'amount': [], 'date': []}
    else:
        data = pd.DataFrame(documents)
        data.dropna(inplace=True)
        # 过滤无效数据
        today = get_today()
        today = today.year * 10000 + today.month * 1000 + today.day
        data = data[data['DATE'] <= today]
        xdata = data.groupby(by=['DATE'])['SERIOUS_VALUE'].sum()
        result = {
            'amount': [int(x) for x in xdata.values],
            'date': [f'{x % 10000 // 100}/{x % 100}' for x in xdata.index]
        }
    return {
        'daily': result,
        'monthly': {
            'amount': [],
            'date': [],
        },
        'weekly': {
            'amount': [],
            'date': [],
        }
    }


def format_data(days_without_accident, safety_produce_info, check_stat_info,
                evaluate_classify_stat, problem_classify_stat,
                safety_produce_info_trendency, abnormal_major_stat):
    data = {}
    data['calender'] = {'safe_produce_duration': days_without_accident}
    data['notification'] = safety_produce_info
    data['dim_amount'] = check_stat_info
    data['evaluate'] = evaluate_classify_stat
    data['issue_item'] = problem_classify_stat
    data['safe_produce'] = safety_produce_info_trendency
    data['abnormal_station'] = abnormal_major_stat
    return data


def get_data(node):
    funcName = {
        'calendar': _get_days_without_accident,
        'notification': _get_safety_produce_notification,
        'dim_amount': get_scce_data,
        'evaluate': _get_evaluate_classify_stat_data,
        'issue_item': _get_problem_classify_data,
        'safe_produce': _get_safety_produce_info_trendency,
        'abnormal_station': _get_abnormal_major_stat,
        'map': _get_map_zhanduan_info,
        'check_issue': _get_check_problem_scatter,
        'check_frequency': _get_check_info_scatter,
        'majors_information': _get_majors_check_stat_info,
        'daily_report': _get_analysis_center_daily,
        'global_safe': _get_risk_tendency,
    }

    if node not in funcName:
        return '参数错误'
    return funcName[node]()


if __name__ == '__main__':
    pass
