#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    大屏API数据
'''
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app.new_big.util import (get_data_from_mongo_by_find, get_department_name,
                              get_major, get_pie_data, get_trendency_data,
                              get_twelve_month, singe_score_section)
from app.utils.common_func import get_date, get_today
from app import mongo

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day
today = int('{}{:0>2}{:0>2}'.format(now_year, now_month, now_day))
strong_start = '<span class="strong-tag">'
strong_end = '</span>'


def get_station_id(short):
    station_short = {
        "njg": '19B8C3534E0D5665E0539106C00A58FD',
        "klg": '19B8C3534E395665E0539106C00A58FD',
        "dzg": '19B8C3534E0F5665E0539106C00A58FD',
        "cdgt": '19B9D8D920D8589FE0539106C00A1189',
        "cddx": '19B8C3534E255665E0539106C00A58FD',
        "gygt": '19B8C3534E505665E0539106C00A58FD',
        "sng": '19B8C3534E065665E0539106C00A58FD',
        'plg': '19B8C3534E085665E0539106C00A58FD',
        'cddj': '19B8C3534E145665E0539106C00A58FD',
        'myg': '19B8C3534E2D5665E0539106C00A58FD',
        'gyg': '19B8C3534E1D5665E0539106C00A58FD',
        'cdg': '19B8C3534E135665E0539106C00A58FD',
        "cqg": '19B8C3534E275665E0539106C00A58FD',
        'xcg': '19B8C3534E195665E0539106C00A58FD',
        'lpsg': '19B8C3534E2C5665E0539106C00A58FD',
        'cqgd': '99990002001499A10010',
        'ybg': '99990002001499A10012'
    }
    return station_short.get(short)


def _get_safe_days(major):
    station = major[1]
    major = major[0]
    keys = {
        "match": {
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "HIERARCHY": 1,
            "STATION": 1,
            "TYPE": 1,
            "VALUE": 1
        }
    }
    coll = "without_accident_data"
    prefix = ""
    documents = get_data_from_mongo_by_find(coll, keys, prefix)
    result = {}
    if len(documents) == 0:
        current_app.logger.debug("工务安全天数接口出错")
        return result
    else:
        data = pd.DataFrame(documents)
        data = data[data['TYPE'] != 'D']
        if station == 'all':
            data = data[data['HIERARCHY'] == 1]
        else:
            data = data[(data['HIERARCHY'] == 3)
                        & (data['STATION'] == station)]
        for idx in data.index:
            result[data.at[idx, 'TYPE']] = int(data.at[idx, 'VALUE'])
    return result


def _get_safe_warning(major):
    """
    获取重点信息追踪以及安全预警
    :param major:
    :return:
    """
    today = get_today()
    month = get_date(today)
    today = int(str(today)[:10].replace('-', ''))
    duan = major[1]
    major = major[0]
    keys = {
        "match": {
            '$or': [{
                "DUTY_NAME": {
                    "$regex": "工务"
                }
            }, {
                "DUTY_NAME": {
                    "$regex": "工电"
                }
            }],
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE":
            1
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "PK_ID": 1,
            "DUTY_NAME": 1,
            "START_DATE": 1
        }
    }
    coll = "warn_notification"
    documents = get_data_from_mongo_by_find(coll, keys)
    result = []
    # 安全预警内容
    if len(documents) == 0:
        current_app.logger.debug("工务无安全预警信息")
    else:
        data = pd.DataFrame(documents)
        data = data.fillna(0)
        data = data.drop_duplicates("PK_ID").reset_index()
        if duan != 'all':
            data['station'] = data['DUTY_NAME'].apply(
                lambda x: str(x).split('-')[0])
            data = data[data['station'] == duan]
            if len(data) == 0:
                current_app.logger.debug("{}今日无安全警告信息".format(duan))
        for index in data.index:
            result.append({
                "content": data.loc[index].at['CONTENT'],
                "tags": ["安全预警", data.loc[index].at['DUTY_NAME']],
                "date": int(data.loc[index].at['START_DATE'])
            })
    # 重点信息追踪内容
    track_keys = {
        "match": {
            "SURVEY_DEPARTMENT_NAMES": {
                "$regex": major
            },
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "TITLE": 1,
            "SURVEY_DEPARTMENT_NAMES": 1,
            "CONTENT": 1,
            "HIERARCHY": 1,
            "DATE": 1
        }
    }
    track_coll = 'key_info_track'
    track_doc = get_data_from_mongo_by_find(track_coll, track_keys)
    if len(track_doc) == 0:
        current_app.logger.debug('无当月重点信息追踪内容')
    else:
        track_data = pd.DataFrame(track_doc)
        if duan != 'all':
            track_data = track_data[(track_data['HIERARCHY'] == 4)
                                    & (track_data.SURVEY_DEPARTMENT_NAMES.str.
                                       contains(duan, regex=False))]
        else:
            track_data = track_data[(track_data['HIERARCHY'] != 1)]
        track_data = track_data.drop_duplicates('PK_ID')
        for index in track_data.index:
            result.append({
                "content":
                track_data.at[index, 'CONTENT'],
                "tags":
                ["重点信息追踪", track_data.at[index, 'SURVEY_DEPARTMENT_NAMES']],
                "date":
                track_data.at[index, 'DATE'].replace('-', '/')
            })
    return result


def _get_safe_notice(major):
    """
    获取安全提示信息
    :param major: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    today = get_today() - relativedelta(days=1)
    today = int(str(today)[:10].replace('-', ''))
    duan = major[1]
    major = major[0]
    keys = {
        "match": {
            "MAJOR": major,
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 2
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "MAJOR": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    coll = "warn_notification"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("工务无安全提示信息")
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.fillna(0)
        data = data.drop_duplicates(["CONTENT", "MAJOR"]).reset_index()

        result = [{
            "content":
            data.loc[index].at['CONTENT'],
            "tags":
            [data.loc[index].at['MAJOR']],
            "date":
            str(data.loc[index].at['START_DATE'])[:4] + '/' + str(
                data.loc[index].at['START_DATE'])[4:6] + '/' + str(
                    data.loc[index].at['START_DATE'])[6:]
        } for index in data.index]
    return result


def _get_safe_warning_notice(major):
    # 近两月责任事故，后期改成近两年本月责任事故
    duan = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(months=2)
    month = get_date(last_day)
    keys = {
        "match": {
            "MON": {
                "$gte": month
            },
            "MAJOR": major,
            # "RESPONSIBILITY_NAME": 1,
            "MAIN_TYPE": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RESPONSIBILITY_UNIT": 1,
            "OVERVIEW": 1,
            "LINE_NAME": 1,
            "DATE": 1,
            "CLOCK": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("工务无近两月责任事故发生")
        res = []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        if duan != 'all':
            data['station'] = data['RESPONSIBILITY_UNIT'].apply(
                lambda x: str(x))
            data = data[data['station'] == duan]
            if len(data) == 0:
                return []
        res = [{
            "content":
            data.loc[index].at['OVERVIEW'],
            "tags": [data.loc[index].at['LINE_NAME']],
            "date":
            '{}/{:0>2}/{:0>2} {}'.format(
                str(data.loc[index].at['DATE'])[:4],
                str(data.loc[index].at['DATE'])[4:6],
                str(data.loc[index].at['DATE'])[6:],
                data.loc[index].at['CLOCK'])
        } if data.loc[index].at['RESPONSIBILITY_UNIT'] == 0 else {
            "content":
            data.loc[index].at['OVERVIEW'],
            "tags": [
                data.loc[index].at['RESPONSIBILITY_UNIT'],
                data.loc[index].at['LINE_NAME']
            ],
            "date":
            '{}/{:0>2}/{:0>2} {}'.format(
                str(data.loc[index].at['DATE'])[:4],
                str(data.loc[index].at['DATE'])[4:6],
                str(data.loc[index].at['DATE'])[6:],
                data.loc[index].at['CLOCK'])
        } for index in data.index]
    return res


def _get_safe_info(major):
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    duan = major[1]
    major = major[0]
    keys = {
        "match": {
            "DATE": yesterday,
            "MAJOR": major,
            # "RESPONSIBILITY_NAME": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "OVERVIEW": 1,
            "LINE_NAME": 1,
            "RESPONSIBILITY_UNIT": 1,
            "DATE": 1,
            "CLOCK": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("工务昨日安全生产信息")
        res = []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        if duan != 'all':
            data['station'] = data['RESPONSIBILITY_UNIT'].apply(
                lambda x: str(x))
            data = data[data['station'] == duan]
            if len(data) == 0:
                return []
        res = [{
            "content":
            data.loc[index].at['OVERVIEW'],
            "tags": [data.loc[index].at['LINE_NAME']],
            "date":
            '{}/{:0>2}/{:0>2} {}'.format(
                str(data.loc[index].at['DATE'])[:4],
                str(data.loc[index].at['DATE'])[4:6],
                str(data.loc[index].at['DATE'])[6:],
                data.loc[index].at['CLOCK'])
        } for index in data.index]
    return res


def _get_problem_point(major):
    duan = major[1]
    major = major[0]
    now_year = get_date(get_today())//100
    keys = {
        "match": {
            "MON": {
                "$gt": int('{}00'.format(now_year))
            },
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "RISK_NAMES": 1,
            "ALL_NAME": 1,
            "PK_ID": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("工务问题项点无今年检查问题数据")
        return []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(['PK_ID', 'ALL_NAME'])
        data = data.fillna(str(0))
        data['station'] = data['ALL_NAME'].apply(
            lambda x: str(x).split('-')[0])
        if duan != 'all':
            data = data[data['station'] == duan]
            if len(data) == 0:
                current_app.logger.debug("{}今年无检查问题数据".format(duan))
                return []
        risks = [
            "断轨风险", "施工作业风险", "防洪及地质灾害风险", "路外及道口安全风险", "胀轨风险", "高速铁路设备风险",
            "普速铁路设备风险", "机械、特种设备风险", "人身伤害风险"
        ]
        amounts = []
        for risk in risks:
            risk_data = data[data.RISK_NAMES.str.contains(risk, regex=False)]
            if risk == '机械、特种设备风险':
                risk_data = data[
                    data.RISK_NAMES.str.contains('自轮设备运用风险', regex=False)
                    | data.RISK_NAMES.str.contains('机械设备质量风险', regex=False)
                    | data.RISK_NAMES.str.contains('特种设备风险', regex=False)]
            amounts.append(len(risk_data))
        data = pd.DataFrame({"name": risks, "amount": amounts})
        columns = ["name", "amount", "amount"]
        result = get_pie_data(risks, data, columns)
        return result


def _get_important_work(major):
    # 工务重点施工
    result = {
        "date": ["岔磨", "线磨", "岔捣", "线捣", "换轨", "换枕", "换岔", "清筛"],
        "事故（责任）": [70, 25, 53, 65, 72, 48, 104, 73]
    }
    return result


def _get_safe_produce_tendency(major):
    # 工务安全信息趋势图
    duan = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(years=1)
    last_date = get_date(last_day)
    keys = {
        "match": {
            "MON": {
                "$gte": last_date
            },
            "MAJOR": major,
            "RESPONSIBILITY_NAME": 1,
            # "RESPONSIBILITY_IDENTIFIED": {
            #    "$in": [1, 2, 3]
            # },
            "DETAIL_TYPE": {
                "$in": [0, 1, 2]
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "MAIN_CLASS": 1,
            "MON": 1,
            "RESPONSIBILITY_UNIT": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("工务过去十二月无安全生产信息")
        return {}
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        data['station'] = data['RESPONSIBILITY_UNIT'].apply(lambda x: str(x))
        if duan != 'all':
            data = data[data.station.str.contains(duan, regex=False)]
        names = ['事故', '故障']
        datas = []
        date = get_twelve_month()[0]
        title = [int(da.replace('/', '')) for da in date]
        for name in names:
            data_i = data[data['MAIN_CLASS'] == name]
            datas.append(data_i)
        result = get_trendency_data(names, datas, title)
    return result


def _get_serious_injury_equipment(major):
    # 重伤设备统计
    result = {
        "date": ["正线钢轨", "正线撤叉", "正线尖基轨", "战线钢轨", "战线撤叉", "战线尖基轨"],
        "累计": [70, 25, 53, 65, 48, 73],
        "当月": [36, 37, 38, 39, 40, 41]
    }
    return result


def _get_temporar_speed_limit(major):
    """临时限速慢行"""
    date = [
        "成都", "重庆", "贵阳", "遂宁", "西昌", "涪陵", "内江", "达州", "凯里", "六盘水", "成高",
        "重庆工电", "贵高"
    ]
    tootip = [["施工", 7], ["病害", 7], ["高速", 7], ["普速", 7], ["正线", 7], ["战线", 7]]
    data = {
        "date":
        date,
        "施工": [1, 8, 1, 9, 5, 7, 5, 6, 7, 4, 3, 2, 4],
        "病害": [6, 5, 4, 8, 9, 7, 3, 5, 2, 1, 8, 2, 4],
        "高速": [8, 3, 4, 6, 2, 5, 7, 9, 5, 1, 4, 7, 5],
        "普速": [7, 8, 9, 6, 5, 4, 8, 7, 5, 1, 2, 3, 7],
        "正线": [2, 5, 4, 9, 7, 6, 3, 7, 1, 2, 8, 5, 1],
        "战线": [5, 8, 7, 9, 6, 4, 1, 2, 8, 5, 4, 3, 1],
        "tooltip": [
            tootip, tootip, tootip, tootip, tootip, tootip, tootip, tootip,
            tootip, tootip, tootip, tootip, tootip
        ]
    }
    return data


def _get_overhaul_situation(major):
    '''大修情况'''
    date = ['清筛', '换岔', '换轨', '换枕']
    tootip = [["当日完成", 7], ["累计完成", 7], ["全年任务", 7]]
    data = {
        "date": date,
        "全年任务": [1256, 1358, 2651, 1789],
        "累计完成": [566, 985, 1544, 1658],
        "当日完成": [887, 1365, 254, 698],
        "tooltip": [tootip, tootip, tootip, tootip]
    }
    return data


def _get_profess_repair(major):
    '''专业修情况'''
    date = ['轨面修', '道岔立体修', '线捣', '岔捣', '现磨', '岔磨']
    # tootip = [["当日完成", 7], ["累计完成", 7], ["全年任务", 7]]
    data = {
        "date": date,
        "全年任务": [1256, 1358, 2651, 1789, 2354, 1245],
        "累计完成": [566, 985, 1544, 1658, 654, 875],
        "当日完成": [887, 1365, 254, 698, 789, 658]
    }
    return data


def _get_key_people_data(params):
    """
    重点违章人员统计分析
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    now_year = get_date(get_today())//100
    major = params[0]
    station = params[1]
    keys = {
        "match": {
            "MON": {
                "$gt": int(f'{now_year}00')
            },
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "DISPOSE_METHOD_NAME": 1,
            "PERSON_NAME": 1,
            "STATUS_NAME": 1,
            "MON": 1,
            "DEPARTMENT": 1
        }
    }
    coll = 'key_person'
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents:
        data = pd.DataFrame(documents)
        data = data.fillna('a')
        data = data.drop_duplicates(['PERSON_NAME', 'DISPOSE_METHOD_NAME'])
        depts = pd.DataFrame(data['DEPARTMENT'].str.split('-', expand=True, n=2))
        depts.fillna('-')
        depts.columns = ['STATION', 'SECTION', 'SHIFT']
        data = pd.merge(data, depts, how='left', left_index=True, right_index=True)
        dept_list = []

        if station == 'all':
            dept_list = depts['STATION'].unique().tolist()
        else:
            dept_list = (depts[depts['STATION'] == station])['SECTION'].unique().tolist()

        dai_total = 0
        pei_total = 0
        gang_total = 0
        bang_total = 0
        he_total = 0
        result = []
        # for major in majors:
        for org_item in dept_list:
            tr_rst = [org_item]

            if station == 'all':
                dept_data = data[data['STATION'] == org_item]
            else:
                dept_data = data[(data['STATION'] == station) & (data['SECTION'] == org_item)]

            dai = len(dept_data[dept_data['STATUS_NAME'] == "待处置"]) + \
                len(dept_data[dept_data['STATUS_NAME'] == "待确认"])
            pei = len(dept_data[dept_data.DISPOSE_METHOD_NAME.str.contains(
                '培训', regex=False)])
            gang = len(dept_data[dept_data.DISPOSE_METHOD_NAME.str.contains(
                '岗', regex=False)])
            bang = len(dept_data[dept_data.DISPOSE_METHOD_NAME.str.contains(
                '帮促', regex=False)])
            he = len(dept_data[dept_data['STATUS_NAME'] == "待确认"]) + \
                len(dept_data[~ dept_data.DISPOSE_METHOD_NAME.str.contains(
                    "前期已处置", regex=False)]) - len(dept_data[
                        dept_data.DISPOSE_METHOD_NAME.str.contains(
                            "a", regex=False)])
            tr_rst.append(dai)
            tr_rst.append(pei)
            tr_rst.append(gang)
            tr_rst.append(bang)
            tr_rst.append(he)
            dai_total += dai
            pei_total += pei
            gang_total += gang
            bang_total += bang
            he_total += he
            result.append(tr_rst)
        result.append(
            ["合计", dai_total, pei_total, gang_total, bang_total, he_total])
        title = ["", '待处置', '培训', '待岗、离岗、转岗', '帮促', '合计']
    else:
        current_app.logger.debug("路局无重点人员数据（key_person出错）")
        result = []
        title = []
    return {"data": result, "title": title}


def _get_safe_warming_data(params):
    """
    获取安全警告通知数据
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    today = get_today()
    month = get_date(today)
    today = int(str(today)[:10].replace('-', ''))
    major = params[0]
    station = params[1]
    keys = {
        "match": {
            '$or': [{
                "DUTY_NAME": {
                    "$regex": "工务"
                }
            }, {
                "DUTY_NAME": {
                    "$regex": "工电"
                }
            }],
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE":
            1
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "PK_ID": 1,
            "DUTY_NAME": 1,
            "START_DATE": 1
        }
    }
    coll = "warn_notification"
    documents = get_data_from_mongo_by_find(coll, keys)
    result = []
    # 安全预警内容
    if len(documents) == 0:
        current_app.logger.debug("工务无安全预警信息")
    else:
        data = pd.DataFrame(documents)
        data = data.fillna(0)
        data = data.drop_duplicates("PK_ID").reset_index()
        if station != 'all':
            data['station'] = data['DUTY_NAME'].apply(
                lambda x: str(x).split('-')[0])
            data = data[data['station'] == station]
            if len(data) == 0:
                current_app.logger.debug("{}今日无安全警告信息".format(station))
        for index in data.index:
            result.append({
                "content": data.loc[index].at['CONTENT'],
                "tags": ["安全预警", data.loc[index].at['DUTY_NAME']],
                "date": int(data.loc[index].at['START_DATE'])
            })
    return result


def _get_key_info_track(params):
    """
    获取重点追踪信息
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    today = get_today()
    month = get_date(today)
    today = int(str(today)[:10].replace('-', ''))
    major = params[0]
    station = params[1]

    '''
    if major == 'all':
        keys["match"]["DUTY_NAME"] = {
            "$regex": major
        }
    else:
        keys["match"]["DUTY_NAME"] ={
            "$regex": station
        }
    '''
    result = []
    # 重点信息追踪内容
    track_keys = {
        "match": {
            "SURVEY_DEPARTMENT_NAMES": {
                "$regex": major
            },
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "TITLE": 1,
            "SURVEY_DEPARTMENT_NAMES": 1,
            "CONTENT": 1,
            "HIERARCHY": 1,
            "DATE": 1
        }
    }
    track_coll = 'key_info_track'
    track_doc = get_data_from_mongo_by_find(track_coll, track_keys)
    if len(track_doc) == 0:
        current_app.logger.debug('无当月重点信息追踪内容')
    else:
        track_data = pd.DataFrame(track_doc)
        if station != 'all':
            track_data = track_data[(track_data['HIERARCHY'] == 4)
                                    & (track_data.SURVEY_DEPARTMENT_NAMES.str.
                                       contains(station, regex=False))]
        else:
            track_data = track_data[(track_data['HIERARCHY'] != 1)]
        track_data = track_data.drop_duplicates('PK_ID')
        for index in track_data.index:
            result.append({
                "content":
                track_data.at[index, 'CONTENT'],
                "tags":
                ["重点信息追踪", track_data.at[index, 'SURVEY_DEPARTMENT_NAMES']],
                "date":
                track_data.at[index, 'DATE'].replace('-', '/')
            })
    return result


def _get_auto_notice_data(params):
    """
    获取自动提示通知数据
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    today = get_today()
    month = get_date(today)
    today = int(str(today)[:10].replace('-', ''))
    major = params[0]
    station = params[1]
    result = []
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 3,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "PK_ID": 1,
            "START_DATE": 1,
            "END_DATE": 1
        }
    }
    if station != 'all':
        keys["match"]["NAME"] = {
            "$regex": station
        }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("路局今日无自动提示信息")
        return result
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('CONTENT').reset_index()
        data = data[~data.CONTENT.str.contains("责任扣分", regex=False)]

        for index in data.index:
            result.append({
                "content": data.loc[index].at['CONTENT'],
                "tags": [data.loc[index].at['NAME']],
                "date": '{}/{:0>2}/{:0>2}'.format(
                    str(data.loc[index].at['START_DATE'])[:4],
                    str(data.loc[index].at['START_DATE'])[4:6],
                    str(data.loc[index].at['START_DATE'])[6:],)
            })

        return result


def _get_safe_manage_poor_data(params):
    """
    得到管理力度薄弱单位
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    # 计算上月日期
    # mon, year = get_date()
    # 获取检查力度指数
    documents = []
    today = get_today()
    major = params[0]
    station = params[1]
    month = get_date(today - relativedelta(months=1))
    coll_name = '{}health_index'.format('monthly_')
    documents = list(mongo.db[coll_name].find({
        "MON": month,
        "HIERARCHY": 3,
        "SCORE": {
            "$lt": 75
        },
        '$or': [{
                  "MAJOR": {
                      "$regex": "工务"
                  }
              }, {
                  "MAJOR": {
                      "$regex": "工电"
                  }
              }],
    }, {
        "_id": 0,
        "DEPARTMENT_NAME": 1,
        "MAJOR": 1,
        "SCORE": 1
    }).sort([('SCORE', 1)]))
    if len(documents) == 0:
        current_app.logger.debug("管理力度薄弱无上月数据（按自然月计算）")
        return
    else:
        data = pd.DataFrame(documents)
        result = []
        group = data[data['MAJOR'] == major]
        group = group.reset_index()
        result.append({
            major: [{
                "NAME": group.loc[index].at['DEPARTMENT_NAME'],
                "SCORE": int(group.loc[index].at['SCORE'])
            } for index in group.index]
        })
    return result


def _get_evaluate_table_data(params):
    """
    获取干部累计记分情况数据
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    now_year = get_date(get_today())//100
    major = params[0]
    station = params[1]
    keys = {
        "match": {
            "MON": {
                '$gt': int('{}00'.format(now_year))
            },
            "MAJOR": major
        },
        "project": {
            "PK_ID": 1,
            "_id": 0,
            "MAJOR": 1,
            "SCORE": 1,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "IDENTITY": 1
        }
    }

    if station != 'all':
        keys["match"]["ALL_NAME"] = {
            "$regex": station
        }

    coll = 'detail_evaluate_record'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("干部履职积分接口无数据")
        return
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
        data.rename(columns={'ALL_NAME': 'DEPARTMENT'}, inplace=True)
        # 将部门全称进行切割
        depts = pd.DataFrame(data['DEPARTMENT'].str.split('-', expand=True, n=2))
        depts.fillna('-')
        depts.columns = ['STATION', 'SECTION', 'SHIFT']

        data = pd.merge(data, depts, how='left', left_index=True, right_index=True)
        dept_list = []

        if station == 'all':
            dept_list = depts['STATION'].unique().tolist()
            data['UNIT'] = data['STATION']
        else:
            dept_list = (depts[depts['STATION'] == station])['SECTION'].unique().tolist()
            data['UNIT'] = data['SECTION']

        # data = data[data['IDENTITY'] == "干部"]

        data = data.groupby(['PERSON_ID',
                             'UNIT'])['SCORE'].sum().reset_index()
        data['LEVEL'] = data['SCORE'].apply(singe_score_section)
        data = data.groupby(['LEVEL', 'UNIT']).count().reset_index()
        data['COUNT'] = data['SCORE']
        del data['SCORE'], data['PERSON_ID']
        total_level = data
    # 将计分情况划分为8个等级
    LEVEL = [i for i in range(1, 8)]
    total_count = [
        sum(list(total_level['COUNT'][total_level['LEVEL'] == i]))
        if i in list(total_level['LEVEL']) else 0 for i in LEVEL
    ]
    total_count.append(sum(total_count))
    total_count.insert(0, "合计")
    # MAJOR = ['车务', '机务', '工务', '电务', '车辆', '供电']
    data_model = [{
        "UNIT": dept,
        "LEVEL": level
    } for dept in dept_list for level in LEVEL]
    data = pd.merge(
        data, pd.DataFrame(data_model), how='right', on=['UNIT', 'LEVEL'])
    data = data.fillna(0)

    count = data.groupby(['LEVEL']).sum(by='COUNT')
    total = list(count['COUNT'].apply(lambda x: int(x)))
    total.append(sum(total))
    data = data.groupby(['UNIT'])
    global_rst = {}
    table_data = [0 for i in range(len(dept_list)+1)]
    for name, group in data:
        group = group.groupby('LEVEL').sum(by='COUNT')
        idx = dept_list.index(name)
        tr_data = list(group['COUNT'].apply(lambda x: int(x)))
        tr_data.insert(0, name)
        tr_data.append(sum(list(group['COUNT'].apply(lambda x: int(x)))))
        table_data[idx] = tr_data
    table_data[-1] = total_count
    TITLE = [
        "", "∑≤2", "2<∑≤4", "4<∑≤6", "6<∑≤8", "8<∑≤10", "10<∑≤12", "∑>12", "合计"
    ]
    global_rst['title'] = TITLE
    global_rst['data'] = table_data
    return global_rst


def _get_evaluate_issue_data(params):
    """
    干部履职问题分布数据
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    # 管道
    today = get_today()
    major = params[0]
    station = params[1]

    year = get_date(today)//100
    keys = {
        "match": {
            "DATE": {
                "$gte": int(f'{year}0000'),
                "$lte": int(f'{year+1}0000')
            },
            "MAJOR": major
            # "CHECK_TYPE": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "CODE": 1,
            "GRADATION": 1
        }
    }
    if station != 'all':
        keys["match"]["ALL_NAME"] = {
            "$regex": station
        }
    coll = 'detail_evaluate_record'
    doc = get_data_from_mongo_by_find(coll, keys)
    data = pd.DataFrame(doc)
    if data.empty:
        return {}
    data = data.drop_duplicates('PK_ID')
    data['COUNT'] = 1
    data = data[data['GRADATION'] != '非管理和专业技术人员'][['CODE', 'COUNT']]
    data = data.groupby('CODE').sum().reset_index()
    # 补全为出现的履职问题
    code = {
        'LH': '量化指标完成',
        'JL': '检查信息录入',
        'ZL': '监督检查质量',
        'KH': '考核责任落实',
        'ZG': '问题闭环管理',
        'ZD': '重点工作落实',
        'YY': '音视频运用管理',
        'LZ': '履职评价管理',
        'SZ': '事故故障追溯',
        'ZJ': '弄虚作假',
        'TX': '安全谈心',
    }
    data['NAME'] = data['CODE'].apply(lambda x: code.get(x))
    del data['CODE']
    for name in list(code.values()):
        if name not in list(data['NAME']):
            data = pd.concat(
                [data,
                 pd.DataFrame([[0, name]], columns=['COUNT', 'NAME'])],
                ignore_index=True)
    total = sum(list(data['COUNT']))
    data = data[data['NAME'] != '检查信息录入']
    columns = ['NAME', 'COUNT', 'COUNT']
    result = get_pie_data(columns, data, columns)
    result['total'] = total
    return result


def _get_check_poor_data(params):
    """
    得到检查力度不高单位
    :param params: 参数列表,是一个list类型。0：专业的中文名称, 1：站段的中文名称
    :return:
    """
    documents = []
    today = get_today()
    major = params[0]
    station = params[1]
    month = get_date(today - relativedelta(months=1))
    coll_name = '{}detail_health_index'.format('monthly_')
    documents = list(mongo.db[coll_name].find({
        "MON": month,
        "MAIN_TYPE": 1,
        "DETAIL_TYPE": 0,
        "HIERARCHY": 3,
        "MAJOR": major
    }, {
        "_id": 0,
        "DEPARTMENT_NAME": 1,
        "SCORE": 1
    }).sort([('SCORE', 1)]).limit(10))
    data = pd.DataFrame(documents)
    data = data[data['SCORE'] < 75]
    # documents = json.loads(data.to_json(orient='records'))

    result = []
    rank_idx = 0
    for index in data.index:
        rank_idx = rank_idx + 1
        result.append({
            "name": data.loc[index].at['DEPARTMENT_NAME'],
            "rank": rank_idx,
            "amount": data.loc[index].at['SCORE'].round(2)
        })
    return result


def get_data(major, node, params):
    funcName = {
        # 第一屏
        "gw_safe_days": _get_safe_days,  # 安全天数
        "gw_safe_warning": _get_safe_warning,  # 安全预警
        "gw_safe_notice": _get_safe_notice,  # 安全提示
        "gw_safe_info": _get_safe_info,  # 安全信息
        "gw_safe_warning_notice": _get_safe_warning_notice,  # 安全警示
        "gw_problem_point": _get_problem_point,  # 问题项点统计饼图
        "gw_important_work": _get_important_work,  # 工务系统重点施工
        "gw_safe_produce_tendency": _get_safe_produce_tendency,  # 安全信息趋势图
        "gw_serious_injury_equipment": _get_serious_injury_equipment,  # 重伤设备统计

        # 第二屏
        "gw_temporar_speed_limit": _get_temporar_speed_limit,  # 临时限速慢行
        "gw_overhaul_situation": _get_overhaul_situation,  # 大修情况
        "gw_profess_repair": _get_profess_repair,  # 专业修情况

        # 201907改版
        "gw_key_info_track": _get_key_info_track,  # 重点信息追踪
        "gw_key_people": _get_key_people_data,  # 重点违章人员统计
        "gw_safe_warn": _get_safe_warming_data,  # 安全预警
        "gw_auto_notice": _get_auto_notice_data,  # 自动提示


        'safe_manage_poor': _get_safe_manage_poor_data,  # 安全管理薄弱单位数据
        'evaluate_table': _get_evaluate_table_data,  # 干部积分统计表
        'evaluate_issue': _get_evaluate_issue_data,  # 干部履职问题分布饼图
        'check_poor': _get_check_poor_data,  # 第二屏检查力度不高单位排名

    }

    duans_li = [
        "all", "klg", "dzg", "cdgt", "cddx", "gygt", "gyg", "sng", "plg",
        "cddj", "myg", "cdg", "cqg", "xcg", "lpsg", "cqgd", "njg", "ybg"
    ]
    if node not in funcName:
        return 'API参数错误'
    if params not in duans_li:
        return "站段参数错误"
    major = get_major(major)
    station = get_department_name(get_station_id(params), 'TYPE3', major, 3, 4)
    if params == 'all':
        station = 'all'
    return funcName[node]([major, station])


if __name__ == '__main__':
    pass
