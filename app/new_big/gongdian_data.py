#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    供电大屏API数据
    Author: WeiBo
    Date: 2018/10/17
    Desc: GongDian big_screen all API
    Method: Find the API function from get_data on the bottom
'''
import json
import re
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app import mongo
from app.new_big.util import (get_coll_prefix, get_data_from_mongo_by_find,
                              get_major, get_median, get_pie_data,
                              get_trendency_data, get_twelve_month,
                              get_week_hours, singe_score_section)
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day

gd_duan_name_li = ["all", "cdd", "cqd", "gyd", "xcd", "dzd", "gyb", "cqg"]
gd_duan_li = [
    "供电系统", "成都供电段", "重庆供电段", "贵阳供电段", "西昌供电段",
    "达州供电段", "贵阳北供电段", "重庆工电段", "六盘水工电段", "宜宾工电段", "西昌工电段"
]
gd_duan_type3_li = [
    "19B8C3534E33512345539106C00A58FD", "19B8C3534E335665E0539106C00A58FD",
    "19B8C3534E355665E0539106C00A58FD", "19B8C3534E345665E0539106C00A58FD",
    "19B8C3534E1B5665E0539106C00A58FD", "19B9D8D920D5589FE0539106C00A1189",
    "99990002001499A10010", "99990002001499A10013", "99990002001499A10012",
    "99990002001499A10015"
]
last_accident_data = {
    'all': {
        "acc": [0, 0, 2, 1, 0, 0, 3, 0, 0, 0, 0, 0],
        "err": [0, 3, 2, 1, 1, 4, 0, 3, 0, 0, 2, 2]
    },
    '19B8C3534E355665E0539106C00A58FD': {
        "acc": [0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0],
        "err": [0, 3, 2, 0, 0, 1, 0, 1, 0, 0, 1, 1]
    },
    '19B9D8D920D5589FE0539106C00A1189': {
        "acc": [0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "err": [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    },
    '19B8C3534E335665E0539106C00A58FD': {
        "acc": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "err": [0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1]
    },
    '19B8C3534E1B5665E0539106C00A58FD': {
        "acc": [0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0],
        "err": [0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0]
    },
    '19B8C3534E33512345539106C00A58FD': {
        "acc": [0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 0, 0],
        "err": [0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0]
    },
    '19B8C3534E345665E0539106C00A58FD': {
        "acc": [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        "err": [0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0]
    }
}


def trans_to_name(duan_type3):
    gd_duan_dict = {
        "19B8C3534E33512345539106C00A58FD": "贵阳北供电段",
        "19B8C3534E335665E0539106C00A58FD": "贵阳供电段",
        "19B8C3534E355665E0539106C00A58FD": "成都供电段",
        "19B8C3534E345665E0539106C00A58FD": "达州供电段",
        "19B8C3534E1B5665E0539106C00A58FD": "西昌供电段",
        "19B9D8D920D5589FE0539106C00A1189": "重庆供电段",
        "99990002001499A10010": "重庆工电段",
        "99990002001499A10013": "六盘水工电段",
        "99990002001499A10012": "宜宾工电段",
        "99990002001499A10015": "西昌工电段"
    }
    return gd_duan_dict[duan_type3]


def trans_to_type3_gd(duan_quanpin):
    gd_duan_dict = {
        "gyb": "19B8C3534E33512345539106C00A58FD",
        "gyd": "19B8C3534E335665E0539106C00A58FD",
        "cdd": "19B8C3534E355665E0539106C00A58FD",
        "dzd": "19B8C3534E345665E0539106C00A58FD",
        "xcd": "19B8C3534E1B5665E0539106C00A58FD",
        "cqd": "19B9D8D920D5589FE0539106C00A1189",
        "cqg": "99990002001499A10010"
    }
    return gd_duan_dict[duan_quanpin]


def _get_safe_status(major):
    duan = major[1]
    major = major[0]
    days_doc = list(mongo.db['without_accident_data'].find({
        "MAJOR": major
    }, {
        "TYPE": 1,
        "_id": 0,
        "HIERARCHY": 1,
        "STATION": 1,
        "VALUE": 1
    }))
    days_data = pd.DataFrame(days_doc)
    if duan != 'all':
        duan = trans_to_name(trans_to_type3_gd(duan))
        day_data = days_data[days_data['STATION'] == duan]
    else:
        day_data = days_data[days_data['HIERARCHY'] == 1]
    days_data = {}
    for index in day_data.index:
        days_data[day_data.at[index, 'TYPE']] = int(
            day_data.at[index, 'VALUE'])
    error_doc = list(mongo.db['daily_detail_safety_produce_info'].find(
        {
            "MAIN_TYPE": 2,
            "MAJOR": major
        }, {
            "_id": 0,
            "PK_ID": 1,
            "RESPONSIBILITY_UNIT": 1
        }))
    if len(error_doc) == 0:
        error_count = 0
    else:
        data = pd.DataFrame(error_doc)
        if duan != 'all':
            data = data[data.RESPONSIBILITY_UNIT.str.contains(
                duan, regex=False)]
        if len(data) != 0:
            data.drop_duplicates('PK_ID', inplace=True)
            error_count = len(list(data.index))
        else:
            error_count = 0
    result = {
        "safety_pro": days_data['B'],
        "C": days_data['C'],
        "D": days_data['D'],
        "count": error_count
    }
    return result


def _get_yesterday_safety_info(major):
    duan = major[1]
    major = major[0]
    yesterday = get_today() - relativedelta(days=1)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    # yesterday = 20180801  #开发使用日期，实际应用上面的日期
    _prefix = get_coll_prefix(yesterday)
    coll = 'detail_safety_produce_info'
    keys = {
        "match": {
            'DATE': yesterday,
            "MAJOR": major
        },
        "project": {
            '_id': 0,
            'OVERVIEW': 1,
            'LINE_NAME': 1,
            'MAIN_CLASS': 1,
            'DETAIL_CLASS': 1,
            'BELONG': 1,
            'PK_ID': 1,
            'CLOCK': 1,
            "RESPONSIBILITY_UNIT": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys, _prefix)
    if len(documents) == 0:
        current_app.logger.debug("昨日供电专业无安全生产信息")
        records = []
    else:
        data = pd.DataFrame(documents)
        if duan != "all":
            station = trans_to_name(trans_to_type3_gd(duan))
            data['station'] = data['RESPONSIBILITY_UNIT'].apply(
                lambda x: str(x))
            data = data[data.station.str.contains(station, regex=False)]
            del data['RESPONSIBILITY_UNIT'], data['station']
        else:
            del data['RESPONSIBILITY_UNIT']
        documents = json.loads(data.to_json(orient='records'))
        records = []
        tmp_id_set = set()
        for record in documents:
            if record['PK_ID'] in tmp_id_set:
                continue
            else:
                tmp_id_set.add(record['PK_ID'])
            tags = []
            if record['DETAIL_CLASS'] != 0:
                tags = [
                    record['DETAIL_CLASS'], record['LINE_NAME'],
                    record['BELONG']
                ]
            else:
                tags = [
                    record['MAIN_CLASS'], record['LINE_NAME'], record['BELONG']
                ]
            record['content'] = record['OVERVIEW']
            record['tags'] = tags
            record['date'] = '{}/{}/{} {}'.format(
                str(yesterday)[:4],
                str(yesterday)[4:6],
                str(yesterday)[6:], record['CLOCK'])
            del record['LINE_NAME'], record['MAIN_CLASS'], record[
                'DETAIL_CLASS'], record['BELONG']
            del record['CLOCK']
            del record['OVERVIEW']
            del record['PK_ID']
            records.append(record)
    return records


def get_system_notify_content(major, day):
    keys = {
        "match": {
            "DEPARTMENT_NAMES": {
                "$regex": major
            },
            "DATE": {
                "$gte": day
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "TITLE": 1,
            "CONTENT": 1,
            "DEPARTMENT_NAMES": 1
        }
    }
    coll = 'system_notify'
    documents = get_data_from_mongo_by_find(coll, keys)
    return documents


def _get_yesterday_gongdian_shigu(major):
    duan = major[1]
    major = major[0]
    yesterday = get_today() - relativedelta(days=7)
    yesterday = int(str(yesterday)[:10].replace("-", ''))
    documents = get_system_notify_content(major, yesterday)
    if duan != 'all':
        duan = trans_to_name(trans_to_type3_gd(duan))
        all_doc = []
        for doc in documents:
            if duan in doc['DEPARTMENT_NAMES'] and doc not in all_doc:
                all_doc.append(doc)
    else:
        all_doc = documents
    if len(all_doc) == 0:
        current_app.logger.debug("今日无预警信息数据")
        return []
    result = []
    data = pd.DataFrame(all_doc)
    data = data[data.TITLE.str.contains("供电系统事故（故障）提示", regex=False)]
    data = data.drop_duplicates('PK_ID')
    for index in data.index:
        content = data.at[index, 'CONTENT'].replace('\r\n', '')
        content = content.replace('69, 69, 69', '255, 255, 255')
        content = content.replace('font-size:16.0pt', 'font-size: inherit')
        content = content.replace('font-size:21.3333px', 'font-size: inherit')
        content = content.replace('仿宋_GB2312', 'inherit')
        result.append({
            "content": content,
            "tags": [data.at[index, 'TITLE']],
            "contentIsHtml": True
        })
    return result


def _get_safety_risk_warning(major):
    duan = major[1]
    major = major[0]
    if duan != 'all':
        duan = trans_to_name(trans_to_type3_gd(duan))
    today = get_today()
    last_day = today - relativedelta(days=7)
    today = int(str(today)[:10].replace("-", ""))
    last_day = int(str(last_day)[:10].replace("-", ""))
    keys = {
        "match": {
            "MAJOR": major,
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "TYPE": 1
        },
        "project": {
            "_id": 0,
            "CONTENT": 1,
            "NAME": 1,
            "DUTY_NAME": 1,
            "TYPE": 1
        }
    }
    coll = 'warn_notification'
    documents = get_data_from_mongo_by_find(coll, keys)
    point_doc = get_system_notify_content(major, last_day)
    result = []
    if len(documents) != 0:
        for doc in documents:
            if duan == 'all' or doc['DUTY_NAME'] == duan:
                result.append({
                    "content": doc['CONTENT'].replace('\r\n', ''),
                    "date": " ",
                    "tags": ["安全警告", doc['DUTY_NAME']]
                })
    if len(point_doc) != 0:
        point_data = pd.DataFrame(point_doc)
        point_data = point_data.drop_duplicates('PK_ID')
        for index in point_data.index:
            content = point_data.at[index, 'CONTENT'].replace('\r\n', '')
            content = re.sub(r'color:\w+', 'color:white', content)
            content = content.replace('69, 69, 69', '255, 255, 255')
            content = content.replace('font-size:16.0pt', 'font-size: 1rem')
            content = content.replace('font-size:21.3333px', 'font-size: 1rem')
            content = content.replace('仿宋_GB2312', 'inherit')
            if duan == 'all' or duan in point_data.at[
                    index, 'DEPARTMENT_NAMES'] and "风险预警提示" in point_data.at[
                        index, "TITLE"]:
                result.append({
                    "content": content,
                    "tags": ["安全提示"],
                    "contentIsHtml": True
                })
            elif duan == 'all' or duan in point_data.at[
                    index, 'DEPARTMENT_NAMES'] and "日盯日控" in point_data.at[
                        index, "TITLE"]:
                result.append({
                    "content": content,
                    "tags": ["供电突出风险日盯日控"],
                    "contentIsHtml": True
                })
    return result


def _get_lwtj_status(major):
    # 问题分类统计
    major = major[0]
    today = get_today()
    year = get_date(today)//100
    keys = {
        "match": {
            "MAJOR": major,
            "MON": {
                "$gt": year * 100,
                "$lt": (year + 1) * 100
            }
        },
        "project": {
            "_id": 0,
            "DEDUCT_SCORE": 1,
            "DEPARTMENT": 1
        }
    }
    coll = 'key_person'
    documents = get_data_from_mongo_by_find(coll, keys)
    key = {
        "match": {
            "DEPARTMENT_ALL_NAME": {
                "$regex": "供电"
            },
            "DATE": {
                "$gt": year * 10000,
                '$lt': (year + 1) * 10000
            },
            "LEVEL": {
                "$in": ['A', 'B', 'C', 'D']
            },
            "RESPONSIBILITY_LEVEL": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RESPONSIBILITY_SCORE": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "RESPONSIBILITY_ID_CARD": 1
        }
    }
    coll_name = 'detail_check_problem'
    data = pd.DataFrame(get_data_from_mongo_by_find(coll_name, key))
    if data.empty:
        return []
    data = data.drop_duplicates('PK_ID')
    data = data.rename(
        columns={
            "DEPARTMENT_ALL_NAME": "DEPARTMENT",
            "RESPONSIBILITY_SCORE": "DEDUCT_SCORE",
            "RESPONSIBILITY_ID_CARD": "ID_CARD"
        })
    data['station'] = data['DEPARTMENT'].apply(lambda x: x.split('-')[0])
    data = data.groupby(['station', 'ID_CARD']).sum().reset_index()
    total = data.groupby('station').size().reset_index()
    sta_total = {}
    for index in total.index:
        sta_total[total.loc[index, 'station']] = total.loc[index, 0]
    first_data = data[(data['DEDUCT_SCORE'] >= 4) & (data['DEDUCT_SCORE'] < 8)]
    second_data = data[(data['DEDUCT_SCORE'] >= 8)
                       & (data['DEDUCT_SCORE'] < 12)]
    third_data = data[(data['DEDUCT_SCORE'] >= 12)
                      & (data['DEDUCT_SCORE'] < 16)]
    fourth_data = pd.DataFrame(documents)
    fourth_data['station'] = fourth_data['DEPARTMENT'].apply(
        lambda x: x.split('-')[0])
    datas = [first_data, second_data, third_data, fourth_data]
    columns = ['station', 'count', 'count']
    results = []
    all_titles = ['4<=Σ<8', '8<=Σ<12', '12<=Σ<16', '违章大王']
    for idx, data in enumerate(datas):
        title = all_titles[idx]
        if data.empty:
            results.append({"data": {}, "title": title})
            continue
        data = data[(data['station'] != '安监室') & (data['station'] != '供电处') &
                    (data['station'] != '调度所')]
        data['count'] = 1
        data = data.groupby('station').sum().reset_index()
        rst = get_pie_data(columns, data, columns)
        glo_rst = []
        for index, item in enumerate(rst['data']):
            percent = round(item['amount']/sta_total[item['name']]*100, 1)
            item['station_percent'] = str(percent) + '%'
            glo_rst.append(item)
        rst = {'data': glo_rst, 'pivot': rst['pivot']}
        results.append({
            "data": rst,
            "title": title
        })
    return results


def _get_ganbu_luzhi_koufen2(major):
    # 干部履职扣分
    duan = major[1]
    major = major[0]
    today = get_today()
    three_day = today - relativedelta(months=2)
    month = get_date(today)
    three_month = get_date(three_day)
    result = []
    now_year = get_date(today)//100
    keys = {
        "match": {
            "MAJOR": major,
            "MON": {
                "$gt": int("{}00".format(now_year))
            },
            "IDENTITY": "干部"
        },
        "project": {
            "_id": 0,
            "SCORE": 1,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "MON": 1
        }
    }
    coll = 'detail_evaluate_record'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("履职扣分无今年数据")
        return
    else:
        data = pd.DataFrame(documents)
        data['station'] = data['ALL_NAME'].apply(
            lambda x: str(x).split('-')[0])
        data_month = data[data['MON'] == month]
        data_three = data[data['MON'] >= three_month]
        data_year = data.copy()
        LEVEL_count = [i for i in range(1, 8)]
        result = []
        b = 0  # 月季年状态（1.2.3.）
        LEVEL_title = []
        for name in range(1, 8):
            a = (name - 1) * 2
            if a == 12:
                title = 'Σ>=12'
            else:
                title = '{}<=Σ<{}'.format(a, a + 2)
            LEVEL_title.append(title)
        columns = ['title', 'level', 'level']
        level_data = pd.DataFrame({"level": LEVEL_count, "title": LEVEL_title})
        for data_i in (data_month, data_three, data_year):
            pie_rst = []
            if len(data_i) == 0:
                current_app.logger.debug("干部履职扣分统计缺失部分数据")
            else:
                data_i = data_i.groupby(
                    ['PERSON_ID', 'MON', 'station',
                     'ALL_NAME'])['SCORE'].sum().reset_index()
                data_i['level'] = data_i['SCORE'].apply(singe_score_section)
                data_i = pd.merge(data_i, level_data, how='left', on='level')
                data_i = data_i[data_i['station'] != '供电处']
                if duan == 'all' or duan is None:
                    stations = [
                        '成都供电段', '重庆供电段', '贵阳供电段', '西昌供电段', '达州供电段', '贵阳北供电段',
                    ]
                    station_data = data_i.groupby('station')
                else:
                    station = trans_to_name(trans_to_type3_gd(duan))
                    data = data_i[data_i['station'] == station]
                    if len(data) == 0:
                        current_app.logger.debug("{}当月无违章情况".format(station))
                        return pie_rst
                    del data['station']
                    data['station'] = data['ALL_NAME'].apply(
                        lambda x: str(x).split('-')[1])
                    station_data = data.groupby('station')
                for name, group in station_data:
                    group = group.groupby('title').count().reset_index()
                    rst = get_pie_data(LEVEL_title, group, columns)
                    pie_rst.append({"title": name, "data": rst})
                pie_stas = [item['title'] for item in pie_rst]
                for sta in stations:
                    if sta not in pie_stas:
                        pie_rst.append({'title': sta, 'data': {}})
            if b == 0:
                result.append({"title": "月", "data": pie_rst})
            elif b == 1:
                result.append({"title": "季", "data": pie_rst})
            elif b == 2:
                result.append({"title": "年", "data": pie_rst})
            b += 1
    return result


def _get_equipment_status(major):
    content = '''接触网专业
        1.达成双线         达州          99.14
        2.达成双线          成都          98.93
        3.沪蓉线            成都         98.70
        4.沪蓉线            重庆         98.38
        5.黔桂线            贵阳         98.25
        6.高南线            达州          98.21
        7.内六线            成都         97.90
        8.内六线            贵阳北       97.18
        9.沪昆六沾段上下行     贵阳北     97.03
        10.渝怀线           重庆工电         97
        11.沪昆贵六段       贵阳北       96.79
        12.黄织毕织单线     贵阳北       96.51
        13.涪三线           重庆         96.49
        14.重庆北至蔡家     重庆工电      95.14
        15.达万线           达州         94.93
        16.巴达线           达州         94.83
        17.渝怀线           重庆         93.68
        18.达成单线         成都         93.27
        19.襄渝线           重庆         92.91
        20.襄渝线           重庆         92.54
        21.成渝线           重庆         92.31
        22.沪昆线           贵阳        91.83
        23.成昆线           西昌         91.52
        24.成渝线           成都         91.15
        25.宝成线           成都        90.77
        26.川黔线           重庆         86.60
        27.川黔线           贵阳北     82.87'''
    return [{"content": content}]


def get_safe_produce_info(year):
    keys = {
        "match": {
            "MON": {
                "$gte": int("{}00".format(year))
            },
            "TYPE3": {"$in": gd_duan_type3_li},
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "MAIN_TYPE": 1,
            "MON": 1,
            "RESPONSIBILITY_UNIT": 1
        }
    }

    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug(f"{year}年无安全生产信息")
        data = []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('PK_ID')
    return data


def get_delta_data(names, dates, compare_data):
    rst = []
    for idx, data in enumerate(compare_data):
        local_date = dates[idx]
        shi_data = data[data['MAIN_TYPE'] == 1]
        she_data = data[data['MAIN_TYPE'] == 2]
        qi_data = data[data['MAIN_TYPE'] == 3]
        all_data = pd.concat(
            [shi_data, she_data, qi_data], axis=0, sort=False)
        datas = [shi_data, she_data, qi_data, all_data]
        rst.append(get_trendency_data(names, datas, local_date))
    delta = {
        "事故（责任）": [rst[1]['事故（责任）'][idx] - rst[0]['事故（责任）'][idx] for idx in range(len(rst[1]['date']))],
        "故障": [rst[1]['故障'][idx] - rst[0]['故障'][idx] for idx in range(len(rst[1]['date']))]
    }
    return rst, delta


def _get_safe_produce_info(major):
    # 安全生产信息趋势图
    duan = major[1]
    major = major[0]
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today())//100
    data = get_safe_produce_info(now_year)
    last_data = get_safe_produce_info(now_year - 1)
    if data.empty:
        return []
    else:
        data['station'] = data['RESPONSIBILITY_UNIT'].apply(str)
        last_data['station'] = last_data['RESPONSIBILITY_UNIT'].apply(str)
        if duan != 'all':
            data = data[data.station.str.contains(duan, regex=False)]
            duan = trans_to_name(trans_to_type3_gd(duan))
        else:
            duan = "供电系统"
        names = ['事故（责任）', '故障', '其他', '合计']
        date = [int(f'{now_year}{i:0>2}') for i in range(1, now_month + 1)]
        last_date = [int(f'{now_year-1}{i:0>2}')
                     for i in range(1, now_month + 1)]
        dates = [last_date, date]
        compare_data = [last_data, data]
        rst, delta = get_delta_data(names, dates, compare_data)
        result = [{
            "data": rst[1],
            "delta": delta,
            "title": duan
        }]
        if duan == "供电系统":
            for station in gd_duan_li:
                if station == '供电系统':
                    continue
                station_data = data[data['station'] == station]
                last_station_data = last_data[last_data['station'] == station]
                station_datas = [station_data, last_station_data]
                station_rst, station_delta = get_delta_data(
                    names, dates, station_datas)
                result.append({
                    "data": station_rst[1],
                    "delta": station_delta,
                    "title": station
                })
    return result


def handel_serious_risk_data(data_df):
    data_li = []
    month_li1 = get_twelve_month()[1]
    month_li = [int(i.replace("/", "")) for i in month_li1]
    risk_li = [
        "从业人员伤害", "行车设备事故故障", "断杆断线", "超速", "冲突", "相撞", "冒进", "挤岔", "专业管理缺陷"
    ]
    data_df = data_df[(data_df["RISK_CONSEQUENCE_NAME"].notnull())]

    data1 = data_df[data_df.RISK_CONSEQUENCE_NAME.str.contains(
        risk_li[0], regex=False)]
    dict1 = dict(data1["MON"].value_counts())
    data_li.append(dict1)

    data2 = data_df[data_df.RISK_CONSEQUENCE_NAME.str.contains(
        risk_li[1], regex=False)]
    dict2 = dict(data2["MON"].value_counts())
    data_li.append(dict2)

    data3 = data_df[data_df.RISK_CONSEQUENCE_NAME.str.contains(
        risk_li[2], regex=False)]
    dict3 = dict(data3["MON"].value_counts())
    data_li.append(dict3)

    data4 = data_df[
        (data_df.RISK_CONSEQUENCE_NAME.str.contains(risk_li[3], regex=False))
        | (data_df.RISK_CONSEQUENCE_NAME.str.contains(risk_li[4], regex=False))
        |
        (data_df.RISK_CONSEQUENCE_NAME.str.contains(risk_li[5], regex=False))]
    dict4 = dict(data4["MON"].value_counts())
    data_li.append(dict4)

    data5 = data_df[
        (data_df.RISK_CONSEQUENCE_NAME.str.contains(risk_li[6], regex=False)) |
        (data_df.RISK_CONSEQUENCE_NAME.str.contains(risk_li[7], regex=False))]
    dict5 = dict(data5["MON"].value_counts())
    data_li.append(dict5)

    data6 = data_df[(data_df.RISK_CONSEQUENCE_NAME.str.contains(
        risk_li[8], regex=False))]
    dict6 = dict(data6["MON"].value_counts())
    data_li.append(dict6)
    data_df1 = pd.DataFrame(data_li, columns=month_li)
    data_df1.fillna(0, inplace=True)

    result = {
        "date": month_li1,
        "从业人员伤害": [int(i) for i in list(data_df1.ix[0])],
        "行车设备事故故障": [int(i) for i in list(data_df1.ix[1])],
        "断杆断线": [int(i) for i in list(data_df1.ix[2])],
        "超速、冲突、相撞": [int(i) for i in list(data_df1.ix[3])],
        "冒进、挤岔": [int(i) for i in list(data_df1.ix[4])],
        "专业管理缺陷": [int(i) for i in list(data_df1.ix[5])]
    }
    return result


def _get_serious_safety_risk_tendency(major):
    # 供电重大安全风险趋势
    duan = major[1]
    major = major[0]
    result = {}
    months_li = get_twelve_month()[1]
    # 获取要查找的数据是供电系统的还是确定工务段的
    data = mongo.db.monthly_detail_check_problem.find(
        {
            "MAJOR": major
        }, {
            "MON": 1,
            "TYPE3": 1,
            "RISK_CONSEQUENCE_NAME": 1
        })
    data_df = pd.DataFrame(list(data))
    if data_df.empty:
        current_app.logger.debug("monthly_detail_check_problem无查询数据")
        a = [0] * len(months_li)
        result = {
            "date": months_li,
            "从业人员伤害": a,
            "行车设备事故故障": a,
            "断杆断线": a,
            "超速、冲突、相撞": a,
            "冒进、挤岔": a,
            "专业管理缺陷": a
        }
        return result
    else:
        if duan == "all":
            data_df = data_df.copy()
            result = handel_serious_risk_data(data_df)

        else:
            duan_type3 = trans_to_type3_gd(duan)
            data_duan_df = data_df[data_df.TYPE3 == duan_type3]
            if data_duan_df.empty:
                a = [0] * len(months_li)
                result = {
                    "date": months_li,
                    "从业人员伤害": a,
                    "行车设备事故故障": a,
                    "断杆断线": a,
                    "超速、冲突、相撞": a,
                    "冒进、挤岔": a,
                    "专业管理缺陷": a
                }
            else:
                result = handel_serious_risk_data(data_duan_df)

    return result


names_li = ["成都供电段", "重庆供电段", "贵阳供电段", "西昌供电段", "达州供电段", "贵阳北供电段", "重庆工电段"]


def handel_index_detail(data_df):
    score_li = []
    for i in range(1, 7):
        data1 = data_df[(data_df.MAIN_TYPE == i) & (data_df.DETAIL_TYPE == 0)]
        if data1.empty:
            score_li.append(0)
        else:
            a = list(data1.SCORE)[0]
            score_li.append(int(a))
    return score_li


def handel_index(data_df):
    score_li = []
    for duan_name in names_li:
        data_duan = data_df[data_df.DEPARTMENT_NAME == duan_name]
        if data_duan.empty:
            score_li.append(0)
        else:
            a = list(data_duan.SCORE)[0]
            score_li.append(int(a))
    return score_li


def _get_safety_comprehensive_index(major):
    # 供电系统安全综合指数排序 获取当前时间上一个月的数据

    months_li = get_twelve_month()[1]

    pro_month1 = months_li[len(months_li) - 1]
    days = pro_month1.split('/')
    if int(days[1]) == 1:
        pro_month = int(f'{int(days[0])-1}12')
    else:
        pro_month = int(pro_month1.replace("/", ""))
    duan = major[1]
    major = major[0]
    index_li = ["检查力度指数", "评价力度指数", "考核力度指数", "检查均衡度指数", "问题暴露度指数", "问题整改效果指数"]

    names_jc_li = ["成都", "重庆", "贵阳", "西昌", "达州", "贵阳北", "重庆工"]
    result = {}
    result_detail = mongo.db.monthly_detail_health_index.find(
        {
            "MAJOR": major,
            "MON": pro_month,
            "HIERARCHY": 3,
        }, {
            "DEPARTMENT_NAME": 1,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "SCORE": 1,
            "MON": 1
        })
    detail_df = pd.DataFrame(list(result_detail))  # 指数详情数据

    if duan == "all":

        result["date"] = names_jc_li
        tooltip = []

        if detail_df.empty:
            # 处理无数据的情况， 查找当mongo无返回结果时，返回内容是什么注意补全
            current_app.logger.debug("monthly_detail_health_index无数据")
            tooltip = [[
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ],
                [
                ["检查力度指数", "0分"],
                ["评价力度指数", "0分"],
                ["考核力度指数", "0分"],
                ["检查均衡度指数", "0分"],
                ["问题暴露度指数", "0分"],
                ["问题整改效果指数", "0分"],
            ]]
        else:
            for duan_name in names_li:
                # 处理详情分数
                duan_detail_data = detail_df[detail_df.DEPARTMENT_NAME ==
                                             duan_name]
                result1 = handel_index_detail(duan_detail_data)
                duan_tooltip = [
                    ["检查力度指数", str(result1[0]) + "分"],
                    ["评价力度指数", str(result1[1]) + "分"],
                    ["考核力度指数", str(result1[2]) + "分"],
                    ["检查均衡度指数", str(result1[3]) + "分"],
                    ["问题暴露度指数", str(result1[4]) + "分"],
                    ["问题整改效果指数", str(result1[5]) + "分"],
                ]
                tooltip.append(duan_tooltip)
        result["tooltip"] = tooltip

        result_data = mongo.db.monthly_health_index.find({
            "MAJOR": major,
            "MON": pro_month
        }, {
            "SCORE": 1,
            "DEPARTMENT_NAME": 1
        })

        result_df = pd.DataFrame(list(result_data))
        if result_df.empty:
            # 处理综合指数
            current_app.logger.debug("monthly_health_inde表无查询数据")
            result[str(pro_month)] = [0, 0, 0, 0, 0, 0, 0]
        else:
            result_data = handel_index(result_df)
            result[str(pro_month)] = result_data
            result_df = pd.DataFrame(result)

            sort_res = result_df.sort_values(
                by=[str(pro_month)], ascending=False)
            result = [{
                "data": {
                    str(pro_month):
                    [int(i) for i in list(sort_res[str(pro_month)].values)],
                    "date":
                    list(sort_res['date'].values),
                },
                "tooltip": list(sort_res['tooltip'].values),
                "title": "供电系统"
            }]

    else:
        duan_type3 = trans_to_type3_gd(duan)
        duan_name = trans_to_name(duan_type3)
        result_all = []
        result["date"] = index_li
        if detail_df.empty:
            # 处理无数据的情况， 查找当mongo无返回结果时，返回内容是什么注意补全
            current_app.logger.debug("无数据")
            result[str(pro_month)] = [0, 0, 0, 0, 0, 0]
        else:
            detail_df_duan = detail_df[detail_df.DEPARTMENT_NAME == duan_name]
            result_detail = handel_index_detail(detail_df_duan)
            result[str(pro_month)] = result_detail
            result_df = pd.DataFrame(result)

            sort_res = result_df.sort_values(
                by=[str(pro_month)], ascending=False)
            result = {
                str(pro_month):
                [int(i) for i in list(sort_res[str(pro_month)].values)],
                "date":
                list(sort_res['date'].values)
            }
        result_all.append({"title": "全车间", "data": result})

        result = result_all

    return result


def handel_problem_data(data_df):
    result = {}
    data = []
    if data_df.empty:
        result["data"] = [{"amount": 0, "name": "", "rank": 0, "value": 0}]
        result["pivot"] = 0
        return result
    data_dict = data_df["CHECK_ITEM_NAME"].value_counts()[:6]
    i = 0
    index_li = list(data_dict.index)
    value_li = list(data_dict.values)
    for index in index_li:
        dict_pro = {}
        dict_pro["name"] = str(index).replace('供电-', '')
        dict_pro["amount"] = int(value_li[i])
        dict_pro["value"] = int(value_li[i])
        i += 1
        dict_pro["rank"] = i
        data.append(dict_pro)
    result["data"] = data
    result["pivot"] = get_median(value_li)
    return result


def _get_problem_classified_statistic(major):
    # 问题分类统计
    duan = major[1]
    major = major[0]
    result = []
    today = get_today()
    month = get_date(today)
    keys = {
        "match": {
            "MAJOR": major,
            "MON": month
        },
        "project": {
            "TYPE3": 1,
            "TYPE4": 1,
            "TYPE5": 1,
            "CHECK_ITEM_NAME": 1
        }
    }
    coll = 'detail_check_problem'
    result_data = get_data_from_mongo_by_find(coll, keys)
    data_df = pd.DataFrame(list(result_data))
    if duan == "all":
        if data_df.empty:
            current_app.logger.debug("daily_detail_check_problem无查询数据")
            result = [{
                "title": "成都供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }, {
                "title": "重庆供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }, {
                "title": "贵阳供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }, {
                "title": "西昌供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }, {
                "title": "达州供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }, {
                "title": "贵阳北供电段",
                "data": {
                    "data": [],
                    "pivot": 0
                }
            }]
        else:
            data_quanxi_df = data_df.copy()
            res_data = handel_problem_data(data_quanxi_df)
            result.append({"title": "供电系统", "data": res_data})
            for duan_type3 in gd_duan_type3_li:
                duan_data_df = data_quanxi_df[data_quanxi_df.TYPE3 ==
                                              duan_type3]
                res_duan = handel_problem_data(duan_data_df)
                if trans_to_name(duan_type3) != '重庆工电段':
                    result.append({
                        "title": trans_to_name(duan_type3),
                        "data": res_duan
                    })
    else:
        duan_type3 = trans_to_type3_gd(duan)
        # 查找车查找该段下所有的间
        chejian_li = mongo.db.base_department.find({
            "MAJOR": major,
            "TYPE3": duan_type3,
            "TYPE": 8,
            "HIERARCHY": 4
        }, {
            "TYPE4": 1,
            "NAME": 1,
            "DEPARTMENT_ID": 1,
            "ALL_NAME": 1
        })
        chejian_df = pd.DataFrame(list(chejian_li))

        if data_df.empty:
            result.append({
                "title": trans_to_name(duan_type3),
                "data": {
                    "data": [],
                    "pivot": 0
                }
            })
            return result
        else:
            data_duan = data_df.copy()
            data_duan_df = data_duan[data_duan["TYPE3"] == duan_type3]
            result_duan = handel_problem_data(data_duan_df)
            result.append({
                "title": trans_to_name(duan_type3),
                "data": result_duan
            })
            if chejian_df.empty:
                current_app.logger.debug("无车间数据")
                return result
            else:
                chejian_name = list(chejian_df["NAME"].values)
                chejian_type4 = list(chejian_df["TYPE4"].values)
                i = 0
                for chejian_type4 in chejian_type4:
                    data_chejian_df = data_duan_df[data_duan_df["TYPE4"] ==
                                                   chejian_type4]
                    chejian_result = handel_problem_data(data_chejian_df)
                    result.append({
                        "title": chejian_name[i],
                        "data": chejian_result
                    })
                    i += 1
    return result


def get_duan_chejian(major, duan_type3):
    # 根据专业及段获取车间
    chejian_li = mongo.db.base_department.find({
        "MAJOR": major,
        "TYPE3": duan_type3,
        "TYPE": 8,
        "HIERARCHY": 4
    }, {
        "TYPE4": 1,
        "NAME": 1,
        "DEPARTMENT_ID": 1,
        "ALL_NAME": 1
    })
    chejian_df = pd.DataFrame(list(chejian_li))
    if chejian_df.empty:
        return
    cejian_dict = {
        "name": list(chejian_df["NAME"].values),
        "all_name": list(chejian_df["ALL_NAME"].values),
        "type4": list(chejian_df["TYPE4"].values)
    }
    return cejian_dict


def handle_pro_cla(data_df):
    all_result = []
    if data_df.empty:
        result = {"data": [], "pivot": 0}
        return result
    data_ser = data_df["SITUATION"].value_counts()[:6]
    pro_name = list(data_ser.index)
    pro_count = list(data_ser.values)
    i = 0
    for name in pro_name:
        result = {}
        result["name"] = str(name)
        result["value"] = int(pro_count[i])
        result["amount"] = int(pro_count[i])
        i += 1
        result["rank"] = i
        all_result.append(result)
    pivot = get_median(pro_count)
    data = {"pivot": pivot, "data": all_result}
    return data


def _get_ganbu_luzhi_koufen1(major):
    # 干部履职问题统计
    duan = major[1]
    major = major[0]
    month_li = get_twelve_month()[1]
    result = []

    res_data_month = mongo.db.daily_detail_evaluate_record.find({
        "MAJOR": major
    })
    res_df_month = pd.DataFrame(list(res_data_month))

    if res_df_month.empty:
        current_app.logger.debug("daily_detail_evaluate_record无数据")
        return result
    res_data_year = mongo.db.monthly_detail_evaluate_record.find({
        "MAJOR": major
    })
    res_df_year = pd.DataFrame(list(res_data_year))

    if res_df_year.empty:
        current_app.logger.debug("monthly_detail_evaluate_record无数据")
        return result

    month1 = month_li[len(month_li) - 1]
    month2 = month_li[len(month_li) - 2]
    res_data_ji1 = res_df_year[
        (res_df_year["MON"] == int(month1.replace("/", "")))
        | (res_df_year["MON"] == int(month2.replace("/", "")))]

    res_df_ji = res_data_ji1.append(res_df_month)

    if res_df_ji.empty:
        current_app.logger.debug("monthly_detail_evaluate_record前两月无数据")
        return result
    yue_data = []
    ji_data = []
    year_data = []
    if duan == "all":
        # res_month = handle_pro_cla(res_df_month)
        # yue_data.append({"title": "供电系统", "data": res_month})
        # res_ji = handle_pro_cla(res_df_ji)
        # ji_data.append({"title": "供电系统", "data": res_ji})
        # res_year = handle_pro_cla(res_df_year)
        # year_data.append({"title": "供电系统", "data": res_year})

        for duan in gd_duan_type3_li:
            duan_df_month = res_df_month[res_df_month["TYPE3"] == duan]
            duan_month = handle_pro_cla(duan_df_month)
            yue_data.append({"title": trans_to_name(duan), "data": duan_month})
            duan_df_ji = res_df_ji[res_df_ji["TYPE3"] == duan]
            duan_ji = handle_pro_cla(duan_df_ji)
            ji_data.append({"title": trans_to_name(duan), "data": duan_ji})
            duan_df_year = res_df_year[res_df_year["TYPE3"] == duan]
            duan_year = handle_pro_cla(duan_df_year)
            year_data.append({"title": trans_to_name(duan), "data": duan_year})

    else:
        duan_type3 = trans_to_type3_gd(duan)
        duan_df_month = res_df_month[res_df_month["TYPE3"] == duan_type3]
        duan_month = handle_pro_cla(duan_df_month)
        yue_data.append({
            "title": trans_to_name(duan_type3),
            "data": duan_month
        })
        duan_df_ji = res_df_ji[res_df_ji["TYPE3"] == duan_type3]
        duan_ji = handle_pro_cla(duan_df_ji)
        ji_data.append({"title": trans_to_name(duan_type3), "data": duan_ji})
        duan_df_year = res_df_year[res_df_year["TYPE3"] == duan_type3]
        duan_year = handle_pro_cla(duan_df_year)
        year_data.append({
            "title": trans_to_name(duan_type3),
            "data": duan_year
        })

        chejian = get_duan_chejian(major, duan_type3)  # 如果该段无车间
        if chejian is None:
            result.append({"title": "月", "data": yue_data})
            result.append({"title": "季", "data": ji_data})
            result.append({"title": "年", "data": year_data})
            return result
        cj_name = chejian["name"]
        i = 0
        for cj_type4 in chejian["type4"]:
            cj_df_month = duan_df_month[duan_df_month["TYPE4"] == cj_type4]
            cj_month = handle_pro_cla(cj_df_month)
            yue_data.append({"title": cj_name[i], "data": cj_month})
            cj_df_ji = duan_df_ji[duan_df_ji["TYPE4"] == cj_type4]
            cj_ji = handle_pro_cla(cj_df_ji)
            ji_data.append({"title": cj_name[i], "data": cj_ji})
            cj_df_year = duan_df_year[duan_df_year["TYPE4"] == cj_type4]
            duan_year = handle_pro_cla(cj_df_year)
            year_data.append({"title": cj_name[i], "data": duan_year})
            i += 1
    result.append({"title": "月", "data": yue_data})
    result.append({"title": "季", "data": ji_data})
    result.append({"title": "年", "data": year_data})

    return result


# 第二屏
def _get_check_map(major):
    # duan = major[1]
    # major = major[0]
    # today = get_today()
    # month = int(str(today)[:7].replace("-", ''))
    # keys = {
    #     "match": {
    #         "mon": month,
    #         "major": major
    #     },
    #     "project": {
    #         "_id": 0,
    #         "name": 1,
    #         "latitude": 1,
    #         "longitude": 1,
    #         "check_info": 1,
    #         "parent": 1
    #     }
    # }
    # coll = "map_data"
    # documents = get_data_from_mongo_by_find(coll, keys)
    # department_doc = list(mongo.db['base_department'].find({
    #     "MAJOR": major
    # }, {
    #     "_id": 0,
    #     "NAME": 1,
    #     "DEPARTMENT_ID": 1,
    #     "TYPE3": 1
    # }))
    # if len(documents) == 0:
    #     current_app.logger.debug("当月无检查地点信息数据")
    #     return
    # else:
    #     data = pd.DataFrame(documents)
    #     department_data = pd.DataFrame(department_doc)
    #     data = pd.merge(
    #         data,
    #         department_data,
    #         how='left',
    #         left_on='parent',
    #         right_on='DEPARTMENT_ID')
    #     del data['DEPARTMENT_ID'], data['parent']
    #     data = pd.merge(
    #         data,
    #         department_data,
    #         how='left',
    #         left_on='TYPE3',
    #         right_on='DEPARTMENT_ID')
    #     data = data.rename(index=str, columns={'NAME_y': "STATION"})
    #     del data['TYPE3_x'], data['TYPE3_y'], data['DEPARTMENT_ID']
    #     chejian_data = data.copy()
    #     del data['latitude'], data['longitude']
    #     data = data.groupby([
    #         'STATION',
    #         'NAME_x',
    #     ]).sum().reset_index()
    #     data = pd.merge(
    #         data,
    #         chejian_data,
    #         how='left',
    #         left_on=['STATION', 'NAME_x'],
    #         right_on=['STATION', 'name'])
    #     # data = data['']
    #     # for name, data in data:
    #     data = data.fillna(0)
    #     data['times'] = data['check_info_x'] + data['check_info_y']
    #     del data['check_info_x'], data['check_info_y']
    #     #     print(name, data)
    #     stations = list(set(data['STATION']))
    #     for station in stations:
    #         sta_data = data[data.STATION == station]
    #         sta_data = sta_data[sta_data.name != 0]
    pass


def _get_check_heat_map(major):
    # 获取当月检查频次散点图
    duan = major[1]
    major = major[0]
    today = get_today()
    ji_day = today - relativedelta(months=2)
    year_day = today - relativedelta(years=1)
    now_month = get_date(today)  # 当前月份
    ji_month = get_date(ji_day)  # 季度月份
    year_month = get_date(year_day)  # 年度月份
    coll_name = 'detail_check_info'
    keys = {
        "match": {
            "MAJOR": major,
            "MON": {
                "$gt": year_month
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "START_CHECK_HOUR": 1,
            "END_CHECK_HOUR": 1,
            "CHECK_WAY": 1,
            "MON": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll_name, keys)
    result = []
    if len(documents) == 0:
        return result
    if duan == 'all':
        duan = '供电系统'
    data = pd.DataFrame(documents)
    data.drop_duplicates('PK_ID', inplace=True)
    now_data = data[data['MON'] == now_month]  # 当前月份数据
    ji_data = data[data['MON'] >= ji_month]  # 季度数据
    year_data = data.copy()  # 年度数据
    datas = [now_data, ji_data, year_data]
    titles = ['月', '季', '年']
    for idx, data_i in enumerate(datas):
        title = titles[idx]
        total = len(data_i)
        xian_count = len(data_i[data_i['CHECK_WAY'] == 1])
        tian_count = len(data_i[data_i['CHECK_WAY'] == 2])
        gen_count = 999  # 未计算（不明确计算公式）
        content = f'{duan}共计检查{total}次： 添乘检查{xian_count}次， \
            现场检查{tian_count}次， 其中跟班检查{gen_count}次。'

        rst_grid = [[0 for col in range(7)] for row in range(7)]
        for idx in data_i.index:
            _stime = str(data_i.at[idx, 'START_CHECK_HOUR'])
            _etime = str(data_i.at[idx, 'END_CHECK_HOUR'])
            week_hours = get_week_hours(_stime, _etime)
            for item in week_hours:
                weekday = item[0] - 1
                hour = item[1] - 1
                rst_grid[hour][weekday] += 1
        # 结果数据集
        rst = [[hour, weekday, rst_grid[hour][weekday]] for hour in range(7)
               for weekday in range(7)]
        pivot = sorted(rst, key=lambda x: x[2], reverse=True)[10][-1]
        scatter = []
        scatter_emphasis = []
        for item in rst:
            if item[2] > pivot:
                scatter_emphasis.append(item)
            else:
                scatter.append(item)
        result.append({
            "data": {
                'scatter': scatter,
                'scatter_emphasis': scatter_emphasis,
                'content': content
            },
            "title": title
        })
    return result


def _get_problem_heat_map(major):
    '''获取检查问题热力图'''
    duan = major[1]
    major = major[0]
    today = get_today()
    ji_day = today - relativedelta(months=2)
    year_day = today - relativedelta(years=1)
    now_month = get_date(today)  # 当前月份
    ji_month = get_date(ji_day)  # 季度月份
    year_month = get_date(year_day)  # 年度月份
    coll_name = 'detail_check_problem'
    keys = {
        "match": {
            "MAJOR": major,
            "MON": {
                "$gt": year_month
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "hour_section": 1,
            "weekday": 1,
            "LEVEL": 1,
            "MON": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll_name, keys)
    result = []
    if len(documents) == 0:
        return result
    if duan == 'all':
        duan = '供电系统'
    data = pd.DataFrame(documents)
    data.drop_duplicates('PK_ID', inplace=True)
    now_data = data[data['MON'] == now_month]  # 当前月份数据
    ji_data = data[data['MON'] >= ji_month]  # 季度数据
    year_data = data.copy()  # 年度数据
    datas = [now_data, ji_data, year_data]
    titles = ['月', '季', '年']
    for idx, data_i in enumerate(datas):
        title = titles[idx]
        level_datas = get_issue_group(data_i)
        counts = [len(level_data) for level_data in level_datas]
        count = len(data_i[data_i.LEVEL.str.contains("K", regex=False)])
        total = len(data_i)
        content = f'{duan}共计检查{total}个问题： 作业类{counts[0]}个， \
            管理类{counts[1]}个， 设备类{counts[2]}个，外部环境类{count}个。'

        xdata = data_i.groupby(['hour_section', 'weekday']).size()
        # 结果数据集
        scatter = []
        scatter_emphasis = []
        if len(xdata) >= 10:
            pivot = xdata.sort_values(ascending=False)[10]
        else:
            pivot = xdata.sort_values(ascending=False)[-1]
        for idx in xdata.index:
            x_idx = int(idx[0]) - 1
            y_idx = int(idx[1]) - 1
            val = int(xdata[idx])
            if val > pivot:
                scatter_emphasis.append([x_idx, y_idx, val])
            else:
                scatter.append([x_idx, y_idx, val])
        result.append({
            "data": {
                'scatter': scatter,
                'scatter_emphasis': scatter_emphasis,
                'content': content
            },
            "title": title
        })
    return result


def _get_problem_point(major):
    duan = major[1]
    major = major[0]
    today = get_today()
    last_day = today - relativedelta(months=1)
    month = get_date(last_day)
    # 获取检查问题数据
    keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "LEVEL": 1,
            "PROBLEM_POINT": 1,
            "ALL_NAME": 1,  # 责任部门
            "DEPARTMENT_ALL_NAME": 1,  # 检查部门
            "SERIOUS_VALUE": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    # 获取地点信息数据
    keys = {
        "match": {
            "MON": month,
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            "LATITUDE": 1,
            "LONGITUDE": 1,
        }
    }
    coll = "map_data"
    address_doc = get_data_from_mongo_by_find(coll, keys)

    stations = ["成都供电段", "重庆供电段", "贵阳供电段", "西昌供电段", "达州供电段"]
    issus = ['作业项', '管理项', '设备项']
    result = {}
    map_rst = []
    pie_rst = []
    if len(documents) == 0:
        current_app.logger.debug('当月无检查问题数据')
    else:
        data = pd.DataFrame(documents)
        add_data = pd.DataFrame(address_doc)
        if duan == 'all':
            # 获取安全综合指数
            query_condition = {'MON': month, 'DETAIL_TYPE': 0, "MAJOR": major}
            index_data = list(mongo.db['monthly_detail_health_index'].find(
                query_condition, {
                    "_id": 0,
                    "DEPARTMENT_NAME": 1,
                    "SCORE": 1,
                    "MAIN_TYPE": 1
                }))
            rst_child_index = {}
            df_index = pd.DataFrame(index_data)
            groups = df_index.groupby(['DEPARTMENT_NAME'])
            for k, v in groups:
                index_value = [0 for _ in range(7)]  # 6个子指数
                for idx, val in v.iterrows():
                    score = round(val['SCORE'], 2)
                    main_type = int(val['MAIN_TYPE'])
                    index_value[main_type - 1] = score
                rst_child_index.update({k: index_value})
            for station in stations:
                data['ze_sta'] = data['ALL_NAME'].apply(
                    lambda x: x.split('-')[0])  # 责任部门数据
                data['jian_sta'] = data['DEPARTMENT_ALL_NAME'].apply(
                    lambda x: x.split('-')[0])  # 检查部门数据
                add = add_data.copy()
                add = add[add['NAME'] == station].reset_index()
                ze_datas = get_issue_group(data[data['ze_sta'] == station])
                jian_datas = get_issue_group(data[data['jian_sta'] == station])
                names = ['PROBLEM_POINT', 'SERIOUS_VALUE', 'SERIOUS_VALUE']
                ze_rst = get_point_pie_content(names, issus, ze_datas)
                jian_rst = get_point_pie_content(names, issus, jian_datas)
                map_value = sum(
                    rst_child_index[station]) / len(rst_child_index)
                map_rst.append({
                    "name":
                    station,
                    "value": [
                        add.loc[0].at['LONGITUDE'], add.loc[0].at['LATITUDE'],
                        map_value
                    ]
                })
                pie_rst.append([{
                    "data": ze_rst,
                    "title": "责任部门"
                }, {
                    "data": jian_rst,
                    "title": "检查部门"
                }])
        else:
            station = trans_to_name(trans_to_type3_gd(duan))
            data = data[data['ALL_NAME'].str.contains(station, regex=False)]
            chejians = list(
                set(data['ALL_NAME'].apply(lambda x: x.split('-')[1])))
            for chejian in chejians:
                data['ze_sta'] = data['ALL_NAME'].apply(
                    lambda x: x.split('-')[1])  # 责任部门数据
                data['jian_sta'] = data['DEPARTMENT_ALL_NAME'].apply(
                    lambda x: x.split('-')[1])  # 检查部门数据
                add = add_data.copy()
                add = add[add['NAME'] == chejian].reset_index()
                ze_datas = get_issue_group(data[data['ze_sta'] == chejian])
                jian_datas = get_issue_group(data[data['jian_sta'] == chejian])
                names = ['PROBLEM_POINT', 'SERIOUS_VALUE', 'SERIOUS_VALUE']
                ze_rst = get_point_pie_content(names, issus, ze_datas)
                jian_rst = get_point_pie_content(names, issus, jian_datas)
                if len(add) != 0:
                    map_rst.append({
                        "name":
                        station + '>' + chejian,
                        "value": [
                            add.loc[0].at['LONGITUDE'],
                            add.loc[0].at['LATITUDE'], 0
                        ]
                    })
                pie_rst.append([{
                    "data": ze_rst,
                    "title": "责任部门"
                }, {
                    "data": jian_rst,
                    "title": "检查部门"
                }])
    result = {"map": map_rst, "pie": pie_rst}
    return result


def get_point_pie_content(names, issus, data):
    issus_rst = []
    for idx, data_i in enumerate(data):
        title = issus[idx]
        if len(data_i) != 0:
            data_i = data_i.groupby(
                ['PROBLEM_POINT', 'ze_sta']).sum().reset_index().sort_values(
                    'SERIOUS_VALUE', ascending=False)
            if len(data_i) >= 10:
                data_i = data_i[:10]
            issus_rst.append({
                "data": get_pie_data(names, data_i, names),
                "title": title
            })
        else:
            issus_rst.append({"data": [], "title": title})
    return issus_rst


def get_issue_group(data):
    '''根据问题分项获取相应数据
    作业项： ABCD
    设备项： F
    管理项： E
    return [作业项， 管理项，设备项]'''
    data['LEVEL'] = data['LEVEL'].fillna('').copy()
    work_data = data[data.LEVEL.str.contains("A", regex=False)
                     | data.LEVEL.str.contains("B", regex=False)
                     | data.LEVEL.str.contains("C", regex=False)
                     | data.LEVEL.str.contains("D", regex=False)]
    manage_data = data[data.LEVEL.str.contains("F", regex=False)]
    equirment_data = data[data.LEVEL.str.contains("E", regex=False)]
    return [work_data, manage_data, equirment_data]


def _get_safe_schedule(major):
    '''安全生产进度统计'''
    date = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    data = {
        "date": date,
        "总量": [
            100,
            100,
            100,
            100,
            100,
            100,
            100,
        ],
        "完成量": [
            95,
            80,
            30,
            21,
            13,
            20,
            15,
        ]
    }
    return data


def _get_produce_complete(major):
    '''生产任务完成情况'''
    stations = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    date = [
        "接触网",
        "电力",
        "变配电",
        "自轮运转",
    ]
    data = {
        "date": date,
        "总量": [
            100,
            100,
            100,
            100,
        ],
        "完成量": [
            21,
            13,
            20,
            15,
        ]
    }
    result = []
    for station in stations:
        result.append({"data": data, "title": station})
    return result


def _get_last_follow_problem(major):
    '''上周跟班问题统计'''
    date = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    data = {
        "date": date,
        "作业次数": [
            156,
            108,
            98,
            145,
            100,
            112,
            75,
        ],
        "跟班次数": [
            95,
            80,
            30,
            21,
            13,
            20,
            15,
        ]
    }
    return data


def _get_last_follow_check(major):
    '''上周跟班频次统计'''
    date = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    data = {
        "date": date,
        "问题数量": [
            156,
            108,
            98,
            145,
            100,
            112,
            75,
        ],
        "跟班次数": [
            95,
            80,
            30,
            21,
            13,
            20,
            15,
        ]
    }
    return data


def _get_follow_check(major):
    '''跟班频次统计'''
    stations = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    date = [
        "接触网",
        "电力",
        "变配电",
        "自轮运转",
    ]
    data = {
        "date": date,
        "作业次数": [
            100,
            100,
            100,
            100,
        ],
        "跟班次数": [
            21,
            13,
            20,
            15,
        ]
    }
    result = []
    for station in stations:
        result.append({"data": data, "title": station})
    return result


def _get_follow_problem(major):
    '''跟班问题统计'''
    stations = [
        "成都",
        "重庆",
        "贵阳",
        "西昌",
        "达州",
        "贵阳北",
        "重庆工电",
    ]
    date = [
        "接触网",
        "电力",
        "变配电",
        "自轮运转",
    ]
    data = {
        "date": date,
        "问题数量": [
            100,
            100,
            100,
            100,
        ],
        "跟班次数": [
            21,
            13,
            20,
            15,
        ]
    }
    result = []
    for station in stations:
        result.append({"data": data, "title": station})
    return result


def get_data(major, node, params):
    funcName = {
        # 第一屏
        "gd_get_safe_status": _get_safe_status,  # 安全情况
        "gd_get_yesterday_safety_info": _get_yesterday_safety_info,  # 昨天安全信息
        "gd_get_yesterday_gongdian_shigu":
        _get_yesterday_gongdian_shigu,  # 昨天外局供电系统故障
        "gd_get_safety_risk_warning": _get_safety_risk_warning,  # 安全风险预警
        "gd_get_equipment_status": _get_equipment_status,  # 设备运行状况
        "gd_safety_produce_tendency": _get_safe_produce_info,  # 安全信息趋势图
        "gd_serious_safety_risk_tendency":
        _get_serious_safety_risk_tendency,  # 重大安全风险趋势图
        "gd_safety_comprehensive_index":
        _get_safety_comprehensive_index,  # 安全综合指数排序
        "gd_get_problem_classified_statistic":
        _get_problem_classified_statistic,  # 问题分类统计
        "gd_get_lwtj_status": _get_lwtj_status,  # 两违情况
        "gd_get_ganbu_luzhi1": _get_ganbu_luzhi_koufen1,  # 干部履职扣分
        "gd_get_ganbu_luzhi2": _get_ganbu_luzhi_koufen2,  # 干部履职问题

        # 第二屏
        "gd_check_map": _get_check_map,  # 检查地点地图
        "gd_check_heat_map": _get_check_heat_map,  # 检查频次热力图
        "gd_problem_point": _get_problem_point,  # 问题项点统计
        "gd_problem_heat_map": _get_problem_heat_map,  # 检查问题热力图

        # 第三屏
        "gd_safe_schedule": _get_safe_schedule,  # 安全生产进度统计
        "gd_produce_complete": _get_produce_complete,  # 生产任务完成情况
        "gd_last_follow_problem": _get_last_follow_problem,  # 上周跟班问题统计
        "gd_last_follow_check": _get_last_follow_check,  # 上周跟班频次统计
        "gd_follow_check": _get_follow_check,  # 跟班频次统计
        "gd_follow_problem": _get_follow_problem,  # 跟班问题统计
    }
    duans_li = ['all', 'gyb', 'gyd', 'cdd', 'dzd', 'xcd', 'cqd', 'cqg']
    if node not in funcName:
        return 'API参数错误'
    if params not in duans_li:
        return "站段参数错误"
    major = get_major(major)
    return funcName[node]([major, params])


if __name__ == '__main__':
    pass
