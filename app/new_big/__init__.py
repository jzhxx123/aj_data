#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
from flask import Blueprint
from flask_cors import CORS

'''
    author: WeiBo
    date: 2018/08/02
    desc: 最新大屏数据API
'''

new_big_bp = Blueprint('new_big', __name__)
# CORS(api)

from . import route