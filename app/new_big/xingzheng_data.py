#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    大屏API数据
'''
from datetime import datetime as dt

import pandas as pd

from app import mongo
from app.new_big.all_data import (_get_evaluate_issue_data,
                                  _get_inspect_check_data,
                                  _get_safe_notice_data,
                                  _get_safe_warnning_data,
                                  _get_safety_produce_info_trendency,
                                  _get_serious_risk_data)
from app.new_big.util import get_data_from_mongo_by_find
from app.utils.common_func import get_date, get_today

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day


def _get_days_without_accident():
    ''' 无事故安全生产天数'''
    data = mongo.db.base_data.find_one({'TYPE': 'days_without_accident'})
    rst = []
    if dt.now().hour == 18:
        days = data['VALUE'] + 1
    else:
        days = data['VALUE']
    rst.append({'title': '集团公司', 'days': days, 'type': 'A'})
    documents = list(mongo.db.without_accident_data.find(
        {'HIERARCHY': 3, 'VALUE': {'$gt': 500}, 'TYPE': 'B'}))
    for doc in documents:
        if (doc['VALUE'] % 100) == 0:
            rst.append({'title': doc['STATION'],
                        'days': doc['VALUE'], 'type': doc['TYPE']})
    return rst


def _get_major_safety_produce_info():
    year = get_date(get_today())//100
    # year = 2018
    majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
    classes = ['事故', '故障', '综合']
    keys = {
        'match': {
            "MON": {'$gt': year*100, '$lt': (year+1)*100},
            "MAJOR": {'$in': majors}
        },
        'project': {
            "_id": 0,
            "PK_ID": 1,
            "MAIN_CLASS": 1,
            "MAJOR": 1,
            "RESPONSIBILITY_NAME": 1
        }
    }
    coll_name = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll_name, keys)
    data = pd.DataFrame(documents)
    data = data.drop_duplicates(['PK_ID', 'MAIN_CLASS', 'MAJOR'])
    res_data = data[data['RESPONSIBILITY_NAME'] == 1]  # 责任事故故障数据
    titles = ['全部', '责任']
    datas = [data, res_data]
    rst = []
    for idx, data in enumerate(datas):
        del data['PK_ID'], data['RESPONSIBILITY_NAME']
        res_rst = {'title': titles[idx]}
        data_rst = {'date': majors}
        for name in classes:
            group = data[data['MAIN_CLASS'] == name]
            if group.empty:
                data_rst[name] = [0 for i in range(0, 6)]
            group['count'] = 1
            group = group.groupby('MAJOR').sum().reset_index()
            group_dict = {}
            for index in group.index:
                group_dict[group.at[index, 'MAJOR']] = int(
                    group.at[index, 'count'])
            data_rst[name] = [group_dict.get(major, 0) for major in majors]
        res_rst['data'] = data_rst
        rst.append(res_rst)
    return rst


def get_data(node):
    funcName = {
        'a_duty': _get_days_without_accident,  # 集团安全生产天数
        'major_produce': _get_major_safety_produce_info,  # 2018年生产信息
        'safe_produce': _get_safety_produce_info_trendency,  # 安全事故、故障趋势
        'inspect_check': _get_inspect_check_data,  # 监督检查情况趋势
        'evaluate_issue': _get_evaluate_issue_data,  # 干部履职问题分类统计
        'serious_risk': _get_serious_risk_data,  # 责任、事故、故障风险分析
        'safe_warnning': _get_safe_warnning_data,  # 安全警告通知数据
        'safe_notice': _get_safe_notice_data,  # 安全提示通知数据
    }

    if node not in funcName:
        return '参数错误'
    return funcName[node]()


if __name__ == '__main__':
    pass
