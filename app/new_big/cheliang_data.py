#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    车辆大屏API数据
    Author: WeiBo
    Date: 2018/10/17
    Desc: cheliang big_screen all API
    Method: Find the API function from get_data on the bottom
'''
import json
from datetime import datetime as dt

import pandas as pd
from flask import current_app

from app import mongo
from app.new_big.util import (get_data_from_mongo_by_find, get_major,
                              get_median, get_pie_data, get_station,
                              get_trendency_data, singe_score_section)
from app.utils.common_func import get_date, get_today
from app.data.util import pd_query

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day
today = int('{}{:0>2}{:0>2}'.format(now_year, now_month, now_day))
strong_start = '<span class="strong-tag">'
strong_end = '</span>'
br = '<br/>'


def get_staiton_name(params):
    stations = {
        'gycl': '贵阳车辆段',
        'cqcl': '重庆车辆段',
        'cqxcl': '重庆西车辆段',
        'cddc': '成都动车段',
        'cdcl': '成都车辆段',
        'cdbcl': '成都北车辆段',
        'gyncl': '贵阳南车辆段',
        'all': 'all'
    }
    return stations.get(params)


def trans_to_duan(type3):
    """
    动客货车的以此为分类依据
    重庆西、成都北、贵阳南车辆段为纯货车；
    成都车辆段为纯客车；
    成都动车段为纯动车；
    重庆车辆段、贵阳车辆段为客、动混合段，内部细分以检查选项为依据
    """
    # 注之后更改为通过mongo.base_department表查找
    duan_dict = {
        "1B59E5DC88007585E0539106C00A3BAD": ["成都动车段", "cdd_info", "cdd_pro"],
        "19B8C3534E0A5665E0539106C00A58FD": ["成都车辆段", "cdc_info", "cdc_pro"],
        "19B8C3534E445665E0539106C00A58FD": ["成都北车辆段", "cdb_info", "cdb_pro"],
        "19B8C3534E1F5665E0539106C00A58FD": ["重庆车辆段", "cqc_info", "cqc_pro"],
        "19B9D8D920DB589FE0539106C00A1189": ["重庆西车辆段", "cqx_info", "cqx_pro"],
        "19B8C3534E185665E0539106C00A58FD": ["贵阳车辆段", "gyc_info", "gyc_pro"],
        "19B8C3534E165665E0539106C00A58FD": ["贵阳南车辆段", "gyn_info", "gyn_pro"]
    }
    duan_name = duan_dict[type3]
    return duan_name


def yujing_no_count(quan_li, count_li, duan_li):
    """
    作用计算警示图标，注意参数列表相同段的要在相同的索引位上
    :param quan_li: 权值列表
    :param count_li: 总数
    :param duan_li: 段名
    :return: 若多个段的计算结果相同，返回前边的段名，返回需要警示的段名
    """
    i = 0
    jingshizhi_li = []
    while i < len(duan_li):
        if quan_li[i] == 0:
            current_app.logger.debug("预警权值有误")
            continue
        jingshizhi_li.append(count_li[i] / quan_li[i])
        i += 1
    min_num = min(jingshizhi_li)
    min_index = jingshizhi_li.index(min_num)
    duan_name = duan_li[min_index]
    return duan_name


def _get_safe_produce_info(major, TYPE=None):
    # 无责任天数计算
    duan = major[1]
    major = major[0]
    now_year = get_date(get_today()) // 100
    days_doc = list(mongo.db['without_accident_data'].find({
        "MAJOR": major
    }, {
        "TYPE": 1,
        "_id": 0,
        "VALUE": 1,
        "HIERARCHY": 1,
        "STATION": 1
    }))
    days_data = {}
    data = pd.DataFrame(days_doc)
    if duan == 'all':
        data = data[data['HIERARCHY'] == 1]
    else:
        data = data[data['HIERARCHY'] == 3]
        data = data[data['STATION'] == duan]
    days_doc = json.loads(data.to_json(orient='records'))
    for item in days_doc:
        days_data[item['TYPE']] = item['VALUE']
    if days_data['C'] < days_data['B']:
        days_c = days_data['C']
    else:
        days_c = days_data['B']
    days_con = "##安全生产天数：### 无责任D类：{}天，无责任C类及以上：{}天<br/>".format(
        days_data['D'], days_c)

    # 历史上的今天信息
    history_date = [
        int('{}{:0>2}{:0>2}'.format(year, now_month, now_day))
        for year in range(2015, now_year)
    ]
    his_coll = "detail_safety_produce_info"
    his_docs = []
    for his in history_date:
        keys = {
            "match": {
                "DATE": his,
                "MAJOR": major,
                "RESPONSIBILITY_NAME": 1
            },
            "project": {
                "_id": 0,
                "OVERVIEW": 1
            }
        }
        his_doc = get_data_from_mongo_by_find(his_coll, keys)
        his_docs.extend(his_doc)
    if len(his_docs) == 0:
        his_con = "##历史上的今天：### 历史今日无责任事故发生<br/>"
    else:
        hiscon = ""
        for idx, item in enumerate(his_docs):
            hiscon = hiscon + str(idx + 1) + ". " + item['OVERVIEW'] + "<br/>"
        his_con = "##历史上的今天：### {}<br/>".format(hiscon)
    # 本日安报信息
    today = get_today()
    sql = """SELECT
        a.OCCURRENCE_TIME,
        a.OVERVIEW,
        b.ALL_NAME
    FROM
        t_safety_tracking_info AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        b.ALL_NAME LIKE "%%{}%%"
        AND a.OCCURRENCE_TIME >= '{}' """.format(major, str(today))
    anbao_data = pd_query(sql)
    if len(anbao_data) == 0:
        daily_con = "##本日安报信息：### 本日安报无问题信息<br/>"
    else:
        daily_in_con = ""
        for index in anbao_data.index:
            if len(anbao_data.at[index, 'OVERVIEW']) <= 10:
                continue
            else:
                daily_in_con = daily_in_con + anbao_data.at[
                    index, 'OVERVIEW'] + "<br/>"
        if len(daily_in_con) < 10:
            daily_con = "##本日安报信息：### 本日安报无问题信息<br/>"
        else:
            daily_con = "##本日安报信息：### " + daily_in_con

    # 计算事故信息
    month = get_date(today)
    acc_keys = {
        "match": {
            "MON": month,
            "MAJOR": major,
            "RESPONSIBILITY_NAME": 1
        },
        "project": {
            "_id": 0,
            "MAIN_CLASS": 1,
            "IS_MATERIALS": 1,
            "OVERVIEW": 1
        }
    }
    acc_coll = "detail_safety_produce_info"
    acc_doc = get_data_from_mongo_by_find(acc_coll, acc_keys)
    if len(acc_doc) == 0:
        acc_con = "##事故信息：### 当月无责任事故发生<br/>"
        error_con = "##设备故障：### 当月无责任设备故障<br/>"
    else:
        data = pd.DataFrame(acc_doc)
        acc_data = data[data['MAIN_CLASS'] == "事故"]
        if len(acc_data) == 0:
            acc_con = "##事故信息：### 当月无责任事故发生<br/>"
        else:
            acccon = ""
            for index in acc_data.index:
                acccon = acccon + str(
                    index + 1) + ". " + acc_data.loc[index].at['OVERVIEW']
            acc_con = "##事故信息：### " + acccon + '<br/>'
        error_data = data[data['MAIN_CLASS'] == "故障"]
        error_data = error_data[error_data['IS_MATERIALS'] == 1]
        if len(error_data) == 0:
            error_con = "##事故信息：### 当月无责任事故发生<br/>"
        else:
            errorcon = ""
            for index in error_data.index:
                errorcon = errorcon + str(
                    index + 1) + ". " + error_data.loc[index].at['OVERVIEW']
            error_con = "##设备故障：### " + errorcon + "<br/>"
    content = days_con + his_con + daily_con + acc_con + error_con

    content = content.replace("###", strong_end).replace("##", strong_start)
    return [{"content": content, "contentIsHtml": True}]


def _get_safe_notice_info(major, TYPE=None):
    duan = major[1]
    major = major[0]
    today = get_today()
    today = int(str(today)[:10].replace('-', ''))
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "DUTY_NAME": {"$regex": major},
            "TYPE": 1
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "HIERARCHY": 1,
            "CONTENT": 1,
            "DUTY_NAME": 1
        }
    }
    coll = "warn_notification"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("车辆专业今日无警告预警通知")
        result = []
    else:
        data = pd.DataFrame(documents)
        data = data.fillna(0)
        data['station'] = data['DUTY_NAME'].apply(
            lambda x: str(x).split("-")[0])
        if duan != "all":
            data = data[data['station'] == duan]
        if len(data) != 0:
            all_data = data[data['HIERARCHY'] == 1].reset_index()
            if len(all_data) == 0:
                all_rst = []
            else:
                all_rst = []
                for index in all_data.index:
                    content = "##集团预警内容：### <br/>" + all_data.loc[
                        index].at['CONTENT']
                    content = content.replace("###", strong_end).replace(
                        "##", strong_start)
                    all_rst.append({
                        "content": content,
                        "tags": [all_data.loc[index].at['station']],
                        "contentIsHtml": True
                    })
            profess_data = data[data['HIERARCHY'] == 2].reset_index()
            if len(profess_data) == 0:
                pro_rst = []
            else:
                pro_rst = []
                for index in profess_data.index:
                    content = "##专业公司预警：### <br/>" + profess_data.loc[
                        index].at['CONTENT']
                    content = content.replace("###", strong_end).replace(
                        "##", strong_start)
                    pro_rst.append({
                        "content":
                        content,
                        "tags": [profess_data.loc[index].at['station']],
                        "contentIsHtml":
                        True
                    })
        else:
            all_rst = []
            pro_rst = []
        result = all_rst + pro_rst
    return result


def _get_important_work_info(major, TYPE=None):
    duan = major[1]
    major = major[0]
    today = get_today()
    today = int(f'{today.year}{today.month:0>2}{today.day:0>2}')
    keys = {
        "match": {
            "START_DATE": {
                "$lte": today
            },
            "END_DATE": {
                "$gte": today
            },
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "ITEM": 1,
            "PK_ID": 1,
            "REAL_SITUATION": 1,
            "RESPONSIBILITY_DEPARTMENT_NAMES": 1
        }
    }
    coll = 'safety_work_supervise'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("今日无重点工督办内容")
        content = "##督办事项：### 今日无重点工作督办内容<br/>"
        pro_rst = []
    else:
        data = pd.DataFrame(documents)
        data['station'] = data['RESPONSIBILITY_DEPARTMENT_NAMES'].apply(
            get_station)
        if duan != 'all':
            data = data[data.station.str.contains(duan)]
        data['situation'] = data['REAL_SITUATION'].apply(
            lambda x: "无" if x == 0 else x)
        data = data.drop_duplicates(['PK_ID', 'station', 'ITEM'])
        pro_rst = []
        for index in data.index:
            content = '''##督办事项：### {}<br/>##\
                整改情况：### {}<br/>##责任部门：### {}<br/>'''.format(
                data.loc[index].at['ITEM'], data.loc[index].at['situation'],
                data.loc[index].at['station'])
            content = content.replace("###", strong_end).replace(
                "##", strong_start).replace(" ", "").replace("\n", "")
            pro_rst.append({
                "content": content,
                "tags": [data.loc[index].at['station']],
                "contentIsHtml": True,
                "date": " "
            })

    return pro_rst


huoche_li = [
    "19B8C3534E445665E0539106C00A58FD",  #: "成都北车辆段",
    "19B9D8D920DB589FE0539106C00A1189",  #: "重庆西车辆段",
    "19B8C3534E165665E0539106C00A58FD"
]  #: "贵阳南车辆段"

keche_li = [
    "19B8C3534E0A5665E0539106C00A58FD",  #: "成都车辆段",
    "19B8C3534E1F5665E0539106C00A58FD",  #: "重庆车辆段",
    "19B8C3534E185665E0539106C00A58FD"
]  #: "贵阳车辆段",

dongche_li = [
    "1B59E5DC88007585E0539106C00A3BAD",  # "成都动车段",
    "19B8C3534E1F5665E0539106C00A58FD",  # "重庆车辆段",
    "19B8C3534E185665E0539106C00A58FD"
]  # "贵阳车辆段",


def _get_safe_risk_info_tendency(major, TYPE=None):
    major = major[0]
    today = get_today()
    # 安全风险管控检查信息走势图
    dc_CHECK_ITEM_NAMES = [
        "动车运用作业", "动车组高级修", "动车组设备质量",
        "动车辅助作业", "安全防范信息化系统"
    ]
    kc_CHECK_ITEM_NAMES = [
        "客车运用作业", "客车检修作业", "客车设备质量",
        "客车辅助作业", "安全防范信息化系统"]
    hc_CHECK_ITEM_NAMES = [
        "货车运用作业",
        "货车检修作业",
        "货车设备质量",
        "货车辅助作业",
        "货车装卸作业",
        "安全防范信息化系统",
        "货车调车作业",
        "货车防溜",
    ]

    che_li = [
        # 顺序动车，客车，货车
        {
            "duan": dongche_li,
            "check_item_names": dc_CHECK_ITEM_NAMES,
            "risk_name1": ["火灾爆炸"],  # 火灾爆炸
            "quanzhi": [1, 0.456, 0.584],  # ["成都动车段", "重庆车辆段", "贵阳车辆段",]
            "risk_name2": ["钩缓", "走行部", "车体底架"],  # 配件脱落
            "risk_name3": ["动车组高压牵引"],  # 制动抱闸
        },
        {
            "duan": keche_li,
            "check_item_names": kc_CHECK_ITEM_NAMES,
            "risk_name1": ["火灾爆炸"],  # 火灾爆炸
            "risk_name2": ["钩缓", "走行部", "车体底架"],  # 配件脱落
            "risk_name3": ["制动供风"],  # 制动抱闸
            "quanzhi": [1, 0.558, 0.459]  # ["成都车辆段", "重庆车辆段", "贵阳车辆段",]
        },
        {
            "duan": huoche_li,
            "check_item_names": hc_CHECK_ITEM_NAMES,
            "risk_name1": ["钩缓", "走行部", "车体底架"],  # 配件脱落
            "risk_name2": ["走行部", "防溜风险", "调车风险"],  # 车辆脱轨
            "risk_name3": ["钩缓"],  # 列车分离
            "quanzhi": [1, 0.79, 0.78]  # ["成都北车辆段","重庆西车辆段","贵阳南车辆段"]
        }
    ]

    # month_li = get_twelve_month()[1]
    if today.month == 1:
        mon = {"$gte": int(f'{today.year-1}12')}
    else:
        mon = {"$gte": int(f'{today.year}00'),
               "$lte": int(f'{today.year+1}00')}
    keys = {
        "match": {
            'MAJOR': "车辆",
            "MON": mon
        },
        "project": {
            '_id': 0,
            'TYPE3': 1,
            "MON": 1,
            'IS_DONGCHE': 1,
            'IS_KECHE': 1,
            "RISK_NAME": 1,
            "CHECK_ITEM_NAMES": 1,
            "MAJOR": 1
        }
    }
    result = list(get_data_from_mongo_by_find('detail_check_info', keys))
    # result = list(
    #     mongo.db.monthly_detail_check_info.find({
    #         'MAJOR': "车辆"
    #     }, {
    #         '_id': 0,
    #         'TYPE3': 1,
    #         "MON": 1,
    #         'IS_DONGCHE': 1,
    #         'IS_KECHE': 1,
    #         "RISK_NAME": 1,
    #         "CHECK_ITEM_NAMES": 1,
    #         "MAJOR": 1
    #     }))  # 检查次数数据

    df_quanxi = pd.DataFrame(result)
    if df_quanxi.empty is not True:
        df_quanxi_m = df_quanxi[[
            'TYPE3', "MON", 'RISK_NAME', 'MAJOR', 'CHECK_ITEM_NAMES',
            'IS_DONGCHE', 'IS_KECHE'
        ]]

        data_dc_li = []
        # 获取数据库中的月份
        month_li1 = list(set(df_quanxi_m["MON"].values))
        month_li1 = sorted(month_li1)
        month_li = [
            str(month)[:4] + "/" + str(month)[4:] for month in month_li1
        ]
        huozai_dc = {"date": month_li}
        peijian_dc = {"date": month_li}
        zhidong_dc = {"date": month_li}

        for duan_no in che_li[0]["duan"]:  # 动车段
            duan_huozai_li = []
            duan_peijian_li = []
            duan_zhidong_li = []
            for cha_month in month_li:
                month_time = cha_month.replace("/", "")
                cdd_duan_type3 = duan_no  # 要查看得段
                df_month_duan_data = df_quanxi_m.loc[
                    (df_quanxi_m.MON == int(month_time))
                    & (df_quanxi_m.TYPE3 == cdd_duan_type3)
                    & (df_quanxi_m.IS_DONGCHE == 1)]
                # 动车——火灾爆炸
                df1_dc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[0]["risk_name1"][0], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[0]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][4], regex=False))]
                df1_dc_num = int(df1_dc.shape[0])
                duan_huozai_li.append(df1_dc_num)
                # print(duan_huozai_li)
                # 动车——配件脱落
                df2_dc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[0]["risk_name2"][0], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[0]["risk_name2"][1], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[0]["risk_name2"][2], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[0]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][4], regex=False))]
                df2_dc_num = int(df2_dc.shape[0])
                duan_peijian_li.append(df2_dc_num)
                # 动车——制动抱闸
                df3_dc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[0]["risk_name3"][0], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[0]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[0]["check_item_names"][4], regex=False))]
                df3_dc_num = int(df3_dc.shape[0])
                duan_zhidong_li.append(df3_dc_num)
            huozai_dc[trans_to_duan(duan_no)[0]] = duan_huozai_li
            peijian_dc[trans_to_duan(duan_no)[0]] = duan_peijian_li
            zhidong_dc[trans_to_duan(duan_no)[0]] = duan_zhidong_li
        data_dc_li.append(huozai_dc)
        data_dc_li.append(peijian_dc)
        data_dc_li.append(zhidong_dc)
        data_dc = {
            "title": "动车",
            "data": data_dc_li,
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"]
        }

        data_kc_li = []
        huozai_kc = {"date": month_li}
        peijian_kc = {"date": month_li}
        zhidong_kc = {"date": month_li}
        for duan_no in che_li[1]["duan"]:  # 客车
            duan_huozai_li = []
            duan_peijian_li = []
            duan_zhidong_li = []
            for cha_month in month_li:
                month_time = cha_month.replace("/", "")
                cdd_duan_type3 = duan_no  # 要查看得段

                df_month_duan_data = df_quanxi_m.loc[
                    (df_quanxi_m.MON == int(month_time))
                    & (df_quanxi_m.TYPE3 == cdd_duan_type3)
                    & (df_quanxi_m.IS_KECHE == 1)]
                # 客车——火灾爆炸
                df1_kc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[1]["risk_name1"][0], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[1]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][3], regex=False))]
                df1_kc_num = int(df1_kc.shape[0])
                duan_huozai_li.append(df1_kc_num)
                # 客车——配件脱落
                df2_kc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[1]["risk_name2"][0], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[1]["risk_name2"][1], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[1]["risk_name2"][2], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[1]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][4], regex=False))]
                df2_kc_num = int(df2_kc.shape[0])
                duan_peijian_li.append(df2_kc_num)
                # 客车——制动抱闸
                df3_kc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[1]["risk_name3"][0], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[1]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[1]["check_item_names"][4], regex=False))]
                df3_kc_num = int(df3_kc.shape[0])
                duan_zhidong_li.append(df3_kc_num)

            huozai_kc[trans_to_duan(duan_no)[0]] = duan_huozai_li
            peijian_kc[trans_to_duan(duan_no)[0]] = duan_peijian_li
            zhidong_kc[trans_to_duan(duan_no)[0]] = duan_zhidong_li

        data_kc_li.append(huozai_kc)
        data_kc_li.append(peijian_kc)
        data_kc_li.append(zhidong_kc)

        data_kc = {
            "title": "客车",
            "data": data_kc_li,
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"]
        }

        data_hc_li = []
        peijian_hc = {"date": month_li}
        fenli_hc = {"date": month_li}
        tuogui_hc = {"date": month_li}
        for duan_no in huoche_li:  # 货车
            duan_peijian_li = []
            duan_fenli_li = []
            duan_tuogui_li = []
            for cha_month in month_li:
                month_time = cha_month.replace("/", "")
                cdd_duan_type3 = duan_no  # 要查看得段
                df_month_duan_data = df_quanxi_m.loc[(df_quanxi_m.MON == int(
                    month_time)) & (df_quanxi_m.TYPE3 == cdd_duan_type3)]
                # 货车——配件脱落
                df1_hc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[2]["risk_name1"][0], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[2]["risk_name1"][1], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[2]["risk_name1"][2], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[2]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][4], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][5], regex=False))]
                df1_hc_num = int(df1_hc.shape[0])
                duan_peijian_li.append(df1_hc_num)

                # 货车——车辆脱轨
                df2_hc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[2]["risk_name2"][0], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[2]["risk_name2"][1], regex=False)
                     | df_month_duan_data.RISK_NAME.str.contains(
                         che_li[2]["risk_name2"][2], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[2]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][4], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][5], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][6], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][7], regex=False))]
                df2_hc_num = int(df2_hc.shape[0])
                duan_tuogui_li.append(df2_hc_num)
                # 货车——车辆分ß离
                df3_hc = df_month_duan_data[
                    (df_month_duan_data.RISK_NAME.str.contains(
                        che_li[2]["risk_name3"][0], regex=False))
                    & (df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                        che_li[2]["check_item_names"][0], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][1], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][2], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][3], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][4], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][5], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][6], regex=False)
                       | df_month_duan_data.CHECK_ITEM_NAMES.str.contains(
                           che_li[2]["check_item_names"][7], regex=False))]
                df3_hc_num = int(df3_hc.shape[0])
                duan_fenli_li.append(df3_hc_num)

            peijian_hc[trans_to_duan(duan_no)[0]] = duan_peijian_li
            tuogui_hc[trans_to_duan(duan_no)[0]] = duan_tuogui_li
            fenli_hc[trans_to_duan(duan_no)[0]] = duan_fenli_li

        data_hc_li.append(peijian_hc)
        data_hc_li.append(tuogui_hc)
        data_hc_li.append(fenli_hc)

        data_hc = {
            "title": "货车",
            "data": data_hc_li,
            "data_tags": ["配件脱落", "车辆脱轨", "列车分离"]
        }

        data_laoan = {"date": month_li}
        jiancha = []
        wenti = []
        df_quanxi_m1 = df_quanxi_m[(df_quanxi_m["RISK_NAME"].notnull())]
        df_laoan = df_quanxi_m1[(df_quanxi_m1.RISK_NAME.str.contains(
            "劳动安全", regex=False))]
        df_laoan_dict = dict(df_laoan['MON'].value_counts())
        pro_keys = {"match": {'MAJOR': "车辆",
                              "MON": mon}, "project": {"_id": 0}}
        ques_result = get_data_from_mongo_by_find(
            'detail_check_problem', pro_keys)  # 查找年问题详情数据

        if ques_result:
            df_ques_res = pd.DataFrame(list(ques_result))
            df_ques_res = df_ques_res[(df_ques_res.RISK_NAME.notnull())]
            df_ques_laoan = df_ques_res[(df_ques_res.RISK_NAME.str.contains(
                "劳动安全", regex=False))]
            df_ques_dict = dict(df_ques_laoan["MON"].value_counts())
            for check_month in month_li:
                month_time = check_month.replace("/", "")
                jiancha.append(int(df_laoan_dict[int(month_time)]))
                wenti.append(int(df_ques_dict[int(month_time)]))
            data_laoan["检查次数"] = jiancha
            data_laoan["检查问题"] = wenti
        else:
            data_laoan["检查次数"] = jiancha
            data_laoan["检查问题"] = wenti
            current_app.logger.debug("数据查找失败")

    else:
        data_dc = {
            "title": "动车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": ""
        }
        data_kc = {
            "title": "客车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": ""
        }
        data_hc = {
            "title": "货车",
            "data_tags": ["配件脱落", "车辆脱轨", "列车分离"],
            "data": ""
        }
        current_app.logger.debug("数据查找失败")
        return

    # result_month = mongo.db.daily_detail_check_info.find({"MAJOR": "车辆"})
    # result_month_li1 = pd.DataFrame(list(result_month))
    result_month_li1 = df_quanxi[df_quanxi['MON'] == get_date(today)]
    if not result_month_li1.empty:
        # 动车
        result_month_li = result_month_li1.copy()
        result_month_li = result_month_li.loc[
            (result_month_li.IS_DONGCHE == 1) & (
                (result_month_li.TYPE3 == dongche_li[0])
                | (result_month_li.TYPE3 == dongche_li[1])
                | (result_month_li.TYPE3 == dongche_li[2]))]

        result_month_df = result_month_li[
            (result_month_li["RISK_NAME"].notnull())
            & (result_month_li["CHECK_ITEM_NAMES"].notnull())]

        # 动车——火灾爆炸
        df1_huozai_dc = result_month_df[
            (result_month_df.RISK_NAME.str.contains(
                che_li[0]["risk_name1"][0], regex=False))
            & (result_month_df.CHECK_ITEM_NAMES.str.contains(
                che_li[0]["check_item_names"][0], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][1], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][2], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][3], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][4], regex=False))]

        huozai_dict_dc = dict(df1_huozai_dc["TYPE3"].value_counts())
        huozai_num_li_dc = [
            huozai_dict_dc[x] if x in huozai_dict_dc else 0 for x in dongche_li
        ]
        huozai_duan_no_dc = yujing_no_count(che_li[0]["quanzhi"],
                                            huozai_num_li_dc, dongche_li)
        # 火灾需要预警的单位
        huozai_duan_name_dc = trans_to_duan(huozai_duan_no_dc)

        df1_peijian_dc = result_month_df[
            (result_month_df.RISK_NAME.str.contains(
                che_li[0]["risk_name2"][0], regex=False) | result_month_df.
             RISK_NAME.str.contains(che_li[0]["risk_name2"][1], regex=False)
             | result_month_df.RISK_NAME.str.contains(
                 che_li[0]["risk_name2"][2], regex=False))
            & (result_month_df.CHECK_ITEM_NAMES.str.contains(
                che_li[0]["check_item_names"][0], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][1], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][2], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][3], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][4], regex=False))]

        peijian_dict_dc = dict(df1_peijian_dc["TYPE3"].value_counts())
        peijian_num_li_dc = [
            peijian_dict_dc[x] if x in peijian_dict_dc else 0
            for x in dongche_li
        ]
        peijian_duan_no_dc = yujing_no_count(che_li[0]["quanzhi"],
                                             peijian_num_li_dc, dongche_li)
        peijian_duan_name_dc = trans_to_duan(peijian_duan_no_dc)  # 动车配件需要预警的单位

        df1_zhidong_dc = result_month_df[
            (result_month_df.RISK_NAME.str.contains(
                che_li[0]["risk_name3"][0], regex=False))
            & (result_month_df.CHECK_ITEM_NAMES.str.contains(
                che_li[0]["check_item_names"][0], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][1], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][2], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][3], regex=False)
               | result_month_df.CHECK_ITEM_NAMES.str.contains(
                   che_li[0]["check_item_names"][4], regex=False))]

        zhidong_dict_dc = dict(df1_zhidong_dc["TYPE3"].value_counts())
        zhidong_num_li_dc = [
            zhidong_dict_dc[x] if x in zhidong_dict_dc else 0
            for x in dongche_li
        ]
        zhidong_duan_no_dc = yujing_no_count(che_li[0]["quanzhi"],
                                             zhidong_num_li_dc, dongche_li)
        # 动车配件需要预警的单位
        zhidong_duan_name_dc = trans_to_duan(zhidong_duan_no_dc)

        table_dc = [["火灾爆炸", huozai_duan_name_dc[1]],
                    ["配件脱落", peijian_duan_name_dc[1]],
                    ["制动抱闸", zhidong_duan_name_dc[1]]]

        # 客车
        result_month_li2 = result_month_li1.copy()
        result_month_li2 = result_month_li2.loc[
            (result_month_li2.IS_KECHE == 1) & (
                (result_month_li2.TYPE3 == keche_li[0])
                | (result_month_li2.TYPE3 == keche_li[1])
                | (result_month_li2.TYPE3 == keche_li[2]))]

        result_month_df_kc = result_month_li2[
            (result_month_li2["RISK_NAME"].notnull())
            & (result_month_li2["CHECK_ITEM_NAMES"].notnull())]

        # 客车——火灾爆炸
        df1_huozai_kc = result_month_df_kc[
            (result_month_df_kc.RISK_NAME.str.contains(
                che_li[1]["risk_name1"][0], regex=False))
            & (result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                che_li[1]["check_item_names"][0], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][1], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][2], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][3], regex=False))]

        huozai_dict_kc = dict(df1_huozai_kc["TYPE3"].value_counts())
        huozai_num_li_kc = [
            huozai_dict_kc[x] if x in huozai_dict_kc else 0 for x in keche_li
        ]
        huozai_duan_no_kc = yujing_no_count(che_li[1]["quanzhi"],
                                            huozai_num_li_kc, keche_li)
        huozai_duan_name_kc = trans_to_duan(huozai_duan_no_kc)  # 火灾需要预警的单位

        df1_peijian_kc = result_month_df_kc[
            (result_month_df_kc.RISK_NAME.str.contains(
                che_li[1]["risk_name2"][0], regex=False) | result_month_df_kc.
             RISK_NAME.str.contains(che_li[1]["risk_name2"][1], regex=False)
             | result_month_df_kc.RISK_NAME.str.contains(
                 che_li[1]["risk_name2"][2], regex=False))
            & (result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                che_li[1]["check_item_names"][0], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][1], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][2], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][3], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][4], regex=False))]

        peijian_dict_kc = dict(df1_peijian_kc["TYPE3"].value_counts())
        peijian_num_li_kc = [
            peijian_dict_kc[x] if x in peijian_dict_kc else 0 for x in keche_li
        ]
        peijian_duan_no_kc = yujing_no_count(che_li[1]["quanzhi"],
                                             peijian_num_li_kc, keche_li)
        peijian_duan_name_kc = trans_to_duan(peijian_duan_no_kc)  # 动车配件需要预警的单位

        df1_zhidong_kc = result_month_df_kc[
            (result_month_df_kc.RISK_NAME.str.contains(
                che_li[1]["risk_name3"][0], regex=False))
            & (result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                che_li[1]["check_item_names"][0], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][1], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][2], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][3], regex=False)
               | result_month_df_kc.CHECK_ITEM_NAMES.str.contains(
                   che_li[1]["check_item_names"][4], regex=False))]

        zhidong_dict_kc = dict(df1_zhidong_kc["TYPE3"].value_counts())
        zhidong_num_li_kc = [
            zhidong_dict_kc[x] if x in zhidong_dict_kc else 0 for x in keche_li
        ]
        zhidong_duan_no_kc = yujing_no_count(che_li[1]["quanzhi"],
                                             zhidong_num_li_kc, keche_li)
        zhidong_duan_name_kc = trans_to_duan(zhidong_duan_no_kc)  # 动车配件需要预警的单位

        table_kc = [["火灾爆炸", huozai_duan_name_kc[1]],
                    ["配件脱落", peijian_duan_name_kc[1]],
                    ["制动抱闸", zhidong_duan_name_kc[1]]]

        # 货车
        result_month_li3 = result_month_li1.copy()
        result_month_li3 = result_month_li3.loc[
            (result_month_li3.TYPE3 == keche_li[0])
            | (result_month_li3.TYPE3 == keche_li[1])
            | (result_month_li3.TYPE3 == keche_li[2])]

        result_month_df_hc = result_month_li3[
            (result_month_li3["RISK_NAME"].notnull())
            & (result_month_li3["CHECK_ITEM_NAMES"].notnull())]

        # 货车——配件脱落
        df1_peijian_hc = result_month_df_hc[
            (result_month_df_hc.RISK_NAME.str.contains(
                che_li[2]["risk_name1"][0], regex=False) | result_month_df_hc.
             RISK_NAME.str.contains(che_li[2]["risk_name1"][1], regex=False)
             | result_month_df_hc.RISK_NAME.str.contains(
                 che_li[2]["risk_name1"][2], regex=False))
            & (result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                che_li[2]["check_item_names"][0], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][1], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][2], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][3], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][4], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][5], regex=False))]

        peijian_dict_hc = dict(df1_peijian_hc["TYPE3"].value_counts())
        peijian_num_li_hc = [
            peijian_dict_hc[x] if x in peijian_dict_hc else 0
            for x in huoche_li
        ]
        peijian_duan_no_hc = yujing_no_count(che_li[2]["quanzhi"],
                                             peijian_num_li_hc, huoche_li)
        peijian_duan_name_hc = trans_to_duan(peijian_duan_no_hc)

        df1_tuogui_hc = result_month_df_hc[
            (result_month_df_hc.RISK_NAME.str.contains(
                che_li[2]["risk_name2"][0], regex=False) | result_month_df_hc.
             RISK_NAME.str.contains(che_li[2]["risk_name2"][1], regex=False)
             | result_month_df_hc.RISK_NAME.str.contains(
                 che_li[2]["risk_name2"][2], regex=False))
            & (result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                che_li[2]["check_item_names"][0], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][1], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][2], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][3], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][4], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][5], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][6], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][7], regex=False))]

        tuogui_dict_hc = dict(df1_tuogui_hc["TYPE3"].value_counts())
        tuogui_num_li_hc = [
            tuogui_dict_hc[x] if x in tuogui_dict_hc else 0 for x in huoche_li
        ]
        tuogui_duan_no_hc = yujing_no_count(che_li[2]["quanzhi"],
                                            tuogui_num_li_hc, huoche_li)
        tuogui_duan_name_hc = trans_to_duan(tuogui_duan_no_hc)  # 动车配件需要预警的单位

        df1_fenli_hc = result_month_df_hc[
            (result_month_df_hc.RISK_NAME.str.contains(
                che_li[2]["risk_name3"][0], regex=False))
            & (result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                che_li[2]["check_item_names"][0], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][1], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][2], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][3], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][4], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][5], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][6], regex=False)
               | result_month_df_hc.CHECK_ITEM_NAMES.str.contains(
                   che_li[2]["check_item_names"][7], regex=False))]

        fenli_dict_hc = dict(df1_fenli_hc["TYPE3"].value_counts())
        fenli_num_li_hc = [
            fenli_dict_hc[x] if x in fenli_dict_hc else 0 for x in huoche_li
        ]
        fenli_duan_no_hc = yujing_no_count(che_li[2]["quanzhi"],
                                           fenli_num_li_hc, huoche_li)
        fenli_duan_name_hc = trans_to_duan(fenli_duan_no_hc)  # 动车配件需要预警的单位

        table_hc = [["配件脱落", peijian_duan_name_hc[1]],
                    ["车辆脱轨",
                     tuogui_duan_name_hc[1]], ["列车分离", fenli_duan_name_hc[1]]]

    else:
        table_hc = [["配件脱落", ""], ["车辆脱轨", ""], ["列车分离", ""]]
        table_kc = [["火灾爆炸", ""], ["配件脱落", ""], ["制动抱闸", ""]]
        table_dc = [["火灾爆炸", ""], ["配件脱落", ""], ["制动抱闸", ""]]
    data_dc["table"] = table_dc
    data_kc["table"] = table_kc
    data_hc["table"] = table_hc

    data = []
    data.append(data_dc)
    data.append(data_kc)
    data.append(data_hc)
    data.append({"data": [data_laoan], "title": "劳安"})
    return data


def _get_safe_risk_problem_tendency(major, TYPE=None):

    major = major[0]
    # 安全风险管控检查问题走势图
    dc_dict = {
        "risk1": ["RISK_CONSEQUENCE", "火灾爆炸"],  # 火灾爆炸
        "risk2": ["RISK_CONSEQUENCE", "机辆部件脱落"],  # 配件脱落
        "risk3": ["RISK_NAME", "动车组高压牵引"],  # 制动抱闸
        "quanzhi": [1, 0.45, 0.584]
    }

    kc_dict = {
        "risk1": ["RISK_CONSEQUENCE", "火灾爆炸"],  # 火灾爆炸
        "risk2": ["RISK_CONSEQUENCE", "机辆部件脱落"],  # 配件脱落
        "risk3": ["RISK_NAME", "制动供风"],  # 制动抱闸
        "quanzhi": [1, 0.558, 0.459]
    }
    hc_dict = {
        "risk1": ["RISK_CONSEQUENCE", "机辆部件脱落"],  # 配件脱落
        "risk2": ["RISK_CONSEQUENCE", "脱轨", "溜逸"],  # 车辆脱轨
        "risk3": ["RISK_CONSEQUENCE", "冲突相撞", "分离"],  # 列车分离
        "quanzhi": [1, 0.79, 0.78]
    }
    result_all = []
    today = get_today()
    if today.month == 1:
        mon = {"$gte": int(f'{today.year-1}12')}
    else:
        mon = {"$gte": int(f'{today.year}00')}
    # month_li = get_twelve_month()[1]  # 获取本年度，非历史十二个月的，不算当前月
    keys = {
        "match": {
            "MON": mon,
            "MAJOR": "车辆"
        },
        "project": {
            "_id": 0
        }
    }
    coll_name = 'detail_check_problem'
    result = get_data_from_mongo_by_find(coll_name, keys)
    # result = list(
    #     mongo.db.monthly_detail_check_problem.find({
    #         'MAJOR': major
    #     }))  # 历史12个月问题详细数据
    df = pd.DataFrame(result)
    if df.empty is not True:
        month_li1 = list(set(df["MON"].values))
        month_li1 = sorted(month_li1)
        month_li = [
            str(month)[:4] + "/" + str(month)[4:] for month in month_li1
        ]
        all_data = [
        ]
        for duan_name in dongche_li:  # 动车数据处理过程
            df1 = df.copy()
            duan_df_data = df1.loc[(df1.TYPE3 == duan_name) & (
                df1.IS_DONGCHE == 1)]  # 获取该段所有数据

            duan_df_data = duan_df_data[duan_df_data["RISK_CONSEQUENCE_NAME"]
                                        .notnull()]
            duan_df_data = duan_df_data[duan_df_data["RISK_NAME"].notnull()]

            dfhuozai_dc = duan_df_data[
                duan_df_data.RISK_CONSEQUENCE_NAME.str.contains(
                    dc_dict["risk1"][1], regex=False)]

            df_huozai_dc_dict = dict(dfhuozai_dc['MON'].value_counts())
            all_data.append(df_huozai_dc_dict)

            dfpeijian_dc = duan_df_data[(
                duan_df_data.RISK_CONSEQUENCE_NAME.str.contains(
                    dc_dict["risk2"][1], regex=False))]
            df_peijian_dc_dict = dict(dfpeijian_dc['MON'].value_counts())
            all_data.append(df_peijian_dc_dict)

            dfzhidong_dc = duan_df_data[(duan_df_data.RISK_NAME.str.contains(
                dc_dict["risk3"][1], regex=False))]
            df_zhidong_dc_dict = dict(dfzhidong_dc['MON'].value_counts())

            all_data.append(df_zhidong_dc_dict)

        for duan_name_kc in keche_li:  # 客车数据
            df2 = df.copy()
            duan_df_data_kc = df2.loc[(df2.TYPE3 == duan_name_kc)
                                      & (df2.IS_KECHE == 1)]

            duan_df_data_kc = duan_df_data_kc[duan_df_data_kc[
                "RISK_CONSEQUENCE_NAME"].notnull()]
            duan_df_data_kc = duan_df_data_kc[duan_df_data_kc["RISK_NAME"]
                                              .notnull()]

            dfhuozai_kc = duan_df_data_kc[
                duan_df_data_kc.RISK_CONSEQUENCE_NAME.str.contains(
                    kc_dict["risk1"][1], regex=False)]

            df_huozai_kc_dict = dict(dfhuozai_kc['MON'].value_counts())
            all_data.append(df_huozai_kc_dict)

            dfpeijian_kc = duan_df_data_kc[(
                duan_df_data_kc.RISK_CONSEQUENCE_NAME.str.contains(
                    kc_dict["risk2"][1], regex=False))]
            df_peijian_kc_dict = dict(dfpeijian_kc['MON'].value_counts())
            all_data.append(df_peijian_kc_dict)

            dfzhidong_kc = duan_df_data_kc[(
                duan_df_data_kc.RISK_NAME.str.contains(
                    kc_dict["risk3"][1], regex=False))]
            df_zhidong_kc_dict = dict(dfzhidong_kc['MON'].value_counts())
            all_data.append(df_zhidong_kc_dict)

        for duan_name_hc in huoche_li:  # 货车数据

            df3 = df.copy()
            duan_df_data_hc = df3.loc[(df3.TYPE3 == duan_name_hc)]

            duan_df_data_hc = duan_df_data_hc[duan_df_data_hc[
                "RISK_CONSEQUENCE_NAME"].notnull()]

            dfpeijian_hc = duan_df_data_hc[(
                duan_df_data_hc.RISK_CONSEQUENCE_NAME.str.contains(
                    hc_dict["risk1"][1], regex=False))]
            df_peijian_hc_dict = dict(dfpeijian_hc['MON'].value_counts())
            all_data.append(df_peijian_hc_dict)

            dftuogui_hc = duan_df_data_hc[
                (duan_df_data_hc.RISK_CONSEQUENCE_NAME.str.contains(
                    hc_dict["risk2"][1], regex=False))
                | (duan_df_data_hc.RISK_CONSEQUENCE_NAME.str.contains(
                    hc_dict["risk2"][1], regex=False))]
            df_tuogui_hc_dict = dict(dftuogui_hc['MON'].value_counts())
            all_data.append(df_tuogui_hc_dict)

            dffenli_hc = duan_df_data_hc[
                (duan_df_data_hc.RISK_CONSEQUENCE_NAME.str.contains(
                    hc_dict["risk3"][1], regex=False))
                | (duan_df_data_hc.RISK_CONSEQUENCE_NAME.str.contains(
                    hc_dict["risk3"][2], regex=False))]

            df_fenli_hc_dict = dict(dffenli_hc['MON'].value_counts())
            all_data.append(df_fenli_hc_dict)

        df_all_data = pd.DataFrame(all_data)

        df_all_data.fillna(0, inplace=True)
        months = [int(i.replace("/", "")) for i in month_li]

        df_all = df_all_data[months]

        dongche_dict = {
            "title":
            "动车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": [{
                "date": month_li,
                "成都动车段": list(df_all.ix[0]),
                "重庆车辆段": list(df_all.ix[3]),
                "贵阳车辆段": list(df_all.ix[6])
            }, {
                "date": month_li,
                "成都动车段": list(df_all.ix[1]),
                "重庆车辆段": list(df_all.ix[4]),
                "贵阳车辆段": list(df_all.ix[7])
            }, {
                "date": month_li,
                "成都动车段": list(df_all.ix[2]),
                "重庆车辆段": list(df_all.ix[5]),
                "贵阳车辆段": list(df_all.ix[8])
            }]
        }

        keche_dict = {
            "title":
            "客车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": [{
                "date": month_li,
                "成都车辆段": list(df_all.ix[9]),
                "重庆车辆段": list(df_all.ix[12]),
                "贵阳车辆段": list(df_all.ix[15])
            }, {
                "date": month_li,
                "成都车辆段": list(df_all.ix[10]),
                "重庆车辆段": list(df_all.ix[13]),
                "贵阳车辆段": list(df_all.ix[16])
            }, {
                "date": month_li,
                "成都车辆段": list(df_all.ix[11]),
                "重庆车辆段": list(df_all.ix[14]),
                "贵阳车辆段": list(df_all.ix[17])
            }]
        }
        huoche_dict = {
            "title":
            "货车",
            "data_tags": ["配件脱落", "车辆脱轨", "列车分离"],
            "data": [{
                "date": month_li,
                "成都北车辆段": list(df_all.ix[18]),
                "重庆西车辆段": list(df_all.ix[21]),
                "贵阳南车辆段": list(df_all.ix[24])
            }, {
                "date": month_li,
                "成都北车辆段": list(df_all.ix[19]),
                "重庆西车辆段": list(df_all.ix[22]),
                "贵阳南车辆段": list(df_all.ix[25])
            }, {
                "date": month_li,
                "成都北车辆段": list(df_all.ix[20]),
                "重庆西车辆段": list(df_all.ix[23]),
                "贵阳车辆段": list(df_all.ix[26])
            }]
        }

    else:
        current_app.logger.debug("monthly_detail_check_proble无查询数据")
        dongche_dict = {
            "title":
            "动车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": [{
                "date": [],
                "成都动车段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }, {
                "date": [],
                "成都动车段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }, {
                "date": [],
                "成都动车段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }]
        }

        keche_dict = {
            "title":
            "客车",
            "data_tags": ["火灾爆炸", "配件脱落", "制动抱闸"],
            "data": [{
                "date": [],
                "成都车辆段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }, {
                "date": [],
                "成都车辆段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }, {
                "date": [],
                "成都车辆段": [],
                "重庆车辆段": [],
                "贵阳车辆段": []
            }]
        }
        huoche_dict = {
            "title":
            "货车",
            "data_tags": ["配件脱落", "车辆脱轨", "列车分离"],
            "data": [{
                "date": [],
                "成都北车辆段": [],
                "重庆西车辆段": [],
                "贵阳南车辆段": []
            }, {
                "date": [],
                "成都北车辆段": [],
                "重庆西车辆段": [],
                "贵阳南车辆段": []
            }, {
                "date": [],
                "成都北车辆段": [],
                "重庆西车辆段": [],
                "贵阳车辆段": []
            }]
        }

    # 查询月数据，处理预警和劳动安全饼图
    # result_month = mongo.db.daily_detail_check_problem.find({"MAJOR": major})
    # result_month_df = pd.DataFrame(list(result_month))
    result_month_df = df[df['MON'] == get_date(today)]
    laoan = {"title": "劳安"}
    if not result_month_df.empty:
        laoan_dict = {}
        # 处理饼图数据
        risk_name_li = ["交通安全", "车辆伤害", "机械伤害", "电击伤害", "高空坠落", "其他"]

        result_month_df1 = result_month_df.copy()
        result_month_df1 = result_month_df1[result_month_df1["RISK_NAME"]
                                            .notnull()]
        result_month_df1 = result_month_df1[(
            result_month_df1.RISK_NAME.str.contains("劳动安全", regex=False))]

        result_jiaotong = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[0], regex=False))].shape[0])
        laoan_dict[risk_name_li[0]] = result_jiaotong
        result_cheliang = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[1], regex=False))].shape[0])
        laoan_dict[risk_name_li[1]] = result_cheliang
        result_jixie = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[2], regex=False))].shape[0])
        laoan_dict[risk_name_li[2]] = result_jixie
        result_dianji = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[3], regex=False))].shape[0])
        laoan_dict[risk_name_li[3]] = result_dianji
        result_gaokong = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[4], regex=False))].shape[0])
        laoan_dict[risk_name_li[4]] = result_gaokong
        result_qita = int(
            result_month_df1[(result_month_df1.RISK_NAME.str.contains(
                risk_name_li[5], regex=False))].shape[0])
        laoan_dict[risk_name_li[5]] = result_qita

        zong_li = [x for x in laoan_dict.values()]
        # zong = sum(zong_li)
        pivot = get_median(zong_li)
        # if zong == 0:
        # 处理完全无数据的情况
        # zong = 1
        data_paixu = sorted(
            laoan_dict.items(), key=lambda x: x[1], reverse=True)
        i = 0
        data_laoan_li = []
        for pro_nu in data_paixu:
            data_laoan_dict = {}
            data_laoan_dict["name"] = pro_nu[0]
            data_laoan_dict["value"] = int(pro_nu[1])  # 该项总数
            data_laoan_dict["amount"] = int(
                pro_nu[1])  # round((pro_nu[1]/zong)*100,2)
            i += 1
            data_laoan_dict["rank"] = i
            data_laoan_li.append(data_laoan_dict)

        laoan["data"] = [{"data": data_laoan_li, "pivot": pivot}]

        # 处理预警情况
        # 动车
        result_month_df1 = result_month_df.copy()
        result_month_dc = result_month_df1.loc[
            (result_month_df1.IS_DONGCHE == 1) & (
                (result_month_df1.TYPE3 == dongche_li[0])
                | (result_month_df1.TYPE3 == dongche_li[1])
                | (result_month_df1.TYPE3 == dongche_li[2]))]

        result_month_dc_hzpj = result_month_dc[(
            result_month_dc["RISK_CONSEQUENCE_NAME"].notnull())]
        result_month_dc_zd = result_month_dc[(
            result_month_dc["RISK_NAME"].notnull())]
        # 动车——火灾爆炸
        df_huozai_dc = result_month_dc_hzpj[(
            result_month_dc_hzpj.RISK_CONSEQUENCE_NAME.str.contains(
                dc_dict["risk1"][1], regex=False))]

        huozai_dict_dc = dict(df_huozai_dc["TYPE3"].value_counts())
        huozai_num_li_dc = [
            int(huozai_dict_dc[x]) if x in huozai_dict_dc else 0
            for x in dongche_li
        ]
        huozai_duan_no_dc = yujing_no_count(dc_dict["quanzhi"],
                                            huozai_num_li_dc, dongche_li)
        # 火灾需要预警的单位
        huozai_duan_name_dc = trans_to_duan(huozai_duan_no_dc)

        df_peijian_dc = result_month_dc_hzpj[(
            result_month_dc_hzpj.RISK_CONSEQUENCE_NAME.str.contains(
                dc_dict["risk2"][1], regex=False))]

        peijian_dict_dc = dict(df_peijian_dc["TYPE3"].value_counts())
        peijian_num_li_dc = [
            int(peijian_dict_dc[x]) if x in peijian_dict_dc else 0
            for x in dongche_li
        ]
        peijian_duan_no_dc = yujing_no_count(dc_dict["quanzhi"],
                                             peijian_num_li_dc, dongche_li)
        peijian_duan_name_dc = trans_to_duan(peijian_duan_no_dc)  # 动车配件需要预警的单位

        df_zhidong_dc = result_month_dc_zd[(
            result_month_dc_zd.RISK_NAME.str.contains(
                dc_dict["risk3"][1], regex=False))]

        zhidong_dict_dc = dict(df_zhidong_dc["TYPE3"].value_counts())
        zhidong_num_li_dc = [
            int(zhidong_dict_dc[x]) if x in zhidong_dict_dc else 0
            for x in dongche_li
        ]
        zhidong_duan_no_dc = yujing_no_count(dc_dict["quanzhi"],
                                             zhidong_num_li_dc, dongche_li)
        # 动车配件需要预警的单位
        zhidong_duan_name_dc = trans_to_duan(zhidong_duan_no_dc)

        table_dc = [["火灾爆炸", huozai_duan_name_dc[2]],
                    ["配件脱落", peijian_duan_name_dc[2]],
                    ["制动抱闸", zhidong_duan_name_dc[2]]]

        # 客车
        result_month_df2 = result_month_df.copy()
        result_month_kc = result_month_df2.loc[
            (result_month_df2.IS_KECHE == 1) & (
                (result_month_df2.TYPE3 == keche_li[0])
                | (result_month_df2.TYPE3 == keche_li[1])
                | (result_month_df2.TYPE3 == keche_li[2]))]

        result_month_df_kc_hzpj = result_month_kc[(
            result_month_kc["RISK_CONSEQUENCE_NAME"].notnull())]

        result_month_df_kc_zd = result_month_kc[(
            result_month_kc["RISK_NAME"].notnull())]

        # 客车——火灾爆炸
        df_huozai_kc = result_month_df_kc_hzpj[(
            result_month_df_kc_hzpj.RISK_CONSEQUENCE_NAME.str.contains(
                kc_dict["risk1"][1], regex=False))]

        huozai_dict_kc = dict(df_huozai_kc["TYPE3"].value_counts())
        huozai_num_li_kc = [
            int(huozai_dict_kc[x]) if x in huozai_dict_kc else 0
            for x in keche_li
        ]
        huozai_duan_no_kc = yujing_no_count(kc_dict["quanzhi"],
                                            huozai_num_li_kc, keche_li)
        huozai_duan_name_kc = trans_to_duan(huozai_duan_no_kc)  # 火灾需要预警的单位

        df_peijian_kc = result_month_df_kc_hzpj[(
            result_month_df_kc_hzpj.RISK_CONSEQUENCE_NAME.str.contains(
                kc_dict["risk2"][1], regex=False))]

        peijian_dict_kc = dict(df_peijian_kc["TYPE3"].value_counts())
        peijian_num_li_kc = [
            int(peijian_dict_kc[x]) if x in peijian_dict_kc else 0
            for x in keche_li
        ]
        peijian_duan_no_kc = yujing_no_count(kc_dict["quanzhi"],
                                             peijian_num_li_kc, keche_li)
        peijian_duan_name_kc = trans_to_duan(peijian_duan_no_kc)  # 动车配件需要预警的单位

        df_zhidong_kc = result_month_df_kc_zd[(
            result_month_df_kc_zd.RISK_NAME.str.contains(
                kc_dict["risk3"][1], regex=False))]

        zhidong_dict_kc = dict(df_zhidong_kc["TYPE3"].value_counts())
        zhidong_num_li_kc = [
            int(zhidong_dict_kc[x]) if x in zhidong_dict_kc else 0
            for x in keche_li
        ]
        zhidong_duan_no_kc = yujing_no_count(kc_dict["quanzhi"],
                                             zhidong_num_li_kc, keche_li)
        zhidong_duan_name_kc = trans_to_duan(zhidong_duan_no_kc)  # 动车配件需要预警的单位

        table_kc = [["火灾爆炸", huozai_duan_name_kc[2]],
                    ["配件脱落", peijian_duan_name_kc[2]],
                    ["制动抱闸", zhidong_duan_name_kc[2]]]

        # 货车
        result_month_df3 = result_month_df.copy()
        result_month_df3 = result_month_df3.loc[
            (result_month_df3.TYPE3 == huoche_li[0])
            | (result_month_df3.TYPE3 == huoche_li[1])
            | (result_month_df3.TYPE3 == huoche_li[2])]

        result_month_df_hc = result_month_df3[(
            result_month_df3["RISK_CONSEQUENCE_NAME"].notnull())]

        # 货车——配件脱落
        df_peijian_hc = result_month_df_hc[(
            result_month_df_hc.RISK_CONSEQUENCE_NAME.str.contains(
                hc_dict["risk1"][1], regex=False))]

        peijian_dict_hc = dict(df_peijian_hc["TYPE3"].value_counts())
        peijian_num_li_hc = [
            int(peijian_dict_hc[x]) if x in peijian_dict_hc else 0
            for x in huoche_li
        ]
        peijian_duan_no_hc = yujing_no_count(hc_dict["quanzhi"],
                                             peijian_num_li_hc, huoche_li)
        peijian_duan_name_hc = trans_to_duan(peijian_duan_no_hc)

        df_tuogui_hc = result_month_df_hc[(
            result_month_df_hc.RISK_CONSEQUENCE_NAME.str.contains(
                hc_dict["risk2"][1], regex=False)
            | result_month_df_hc.RISK_CONSEQUENCE_NAME.str.contains(
                hc_dict["risk2"][2], regex=False))]

        tuogui_dict_hc = dict(df_tuogui_hc["TYPE3"].value_counts())
        tuogui_num_li_hc = [
            int(tuogui_dict_hc[x]) if x in tuogui_dict_hc else 0
            for x in huoche_li
        ]
        tuogui_duan_no_hc = yujing_no_count(hc_dict["quanzhi"],
                                            tuogui_num_li_hc, huoche_li)
        tuogui_duan_name_hc = trans_to_duan(tuogui_duan_no_hc)  # 动车配件需要预警的单位

        df_fenli_hc = result_month_df_hc[
            (result_month_df_hc.RISK_CONSEQUENCE_NAME.str.contains(
                hc_dict["risk3"][1], regex=False))
            | (result_month_df_hc.RISK_CONSEQUENCE_NAME.str.contains(
                hc_dict["risk3"][2], regex=False))]

        fenli_dict_hc = dict(df_fenli_hc["TYPE3"].value_counts())
        fenli_num_li_hc = [
            int(fenli_dict_hc[x]) if x in fenli_dict_hc else 0
            for x in huoche_li
        ]
        fenli_duan_no_hc = yujing_no_count(hc_dict["quanzhi"], fenli_num_li_hc,
                                           huoche_li)
        fenli_duan_name_hc = trans_to_duan(fenli_duan_no_hc)  # 动车配件需要预警的单位

        table_hc = [["配件脱落", peijian_duan_name_hc[2]],
                    ["车辆脱轨",
                     tuogui_duan_name_hc[2]], ["列车分离", fenli_duan_name_hc[2]]]

        dongche_dict["table"] = table_dc
        keche_dict["table"] = table_kc
        huoche_dict["table"] = table_hc
    else:
        table_hc = [["配件脱落", ""], ["车辆脱轨", ""], ["列车分离", ""]]
        table_kc = [["火灾爆炸", ""], ["配件脱落", ""], ["制动抱闸", ""]]
        table_dc = [["火灾爆炸", ""], ["配件脱落", ""], ["制动抱闸", ""]]
        dongche_dict["table"] = table_dc
        keche_dict["table"] = table_kc
        huoche_dict["table"] = table_hc
        laoan["data"] = [{"data": [], "pivot": ""}]
        current_app.logger.debug("问题详情表数据无数据")

    result_all.append(dongche_dict)
    result_all.append(keche_dict)
    result_all.append(huoche_dict)
    result_all.append(laoan)
    return result_all


def _get_larger_danger_info(major, TYPE=None):
    duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)
    keys = {
        "match": {
            "MON": month,
            "ALL_NAME": {"$regex": major},
            "LEVEL": {
                "$in": ['A', 'F1', 'E1']
            }
        },
        "project": {
            "_id": 0,
            "DESCRIPTION": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("较大安全隐患管控情况无今日数据")
        rst = []
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates('DESCRIPTION')
        data = data.fillna('0')
        data['station'] = data['ALL_NAME'].apply(lambda x: x.split('-')[0])
        if duan != 'all':
            data = data[data.station.str.contains(duan, regex=False)]
        rst = []
        for index in data.index:
            rst.append({
                "content": data.loc[index].at['DESCRIPTION'],
                "tags": [data.loc[index].at['station']]
            })
    return rst


def _get_high_quality_issue_tendency(major, TYPE=None):
    duan = major[1]
    major = major[0]
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today()) // 100
    result = []
    if TYPE == 'high_quality_issue_cl_1':
        types = ['A', 'B', 'F1', 'F2', 'E1', 'E2']
    elif TYPE == 'high_quality_issue_cl_2':
        types = ['A', 'B', 'F1', 'F2', 'E1', 'E2', 'K1', 'G1']
    elif TYPE == 'high_quality_issue_cl_3':
        types = ['A', 'B', 'F1', 'F2', 'E1', 'E2']
    keys = {
        "match": {
            "MON": {
                "$gt": int('{}00'.format(now_year))
            },
            "MAJOR": major,
            "LEVEL": {
                "$in": types
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "LEVEL": 1,
            "MON": 1,
            "RISK_NAME": 1,
            "ALL_NAME": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("高质量问题分析模块无今年检查问题信息")
    else:
        data = pd.DataFrame(documents)
        data.drop_duplicates('PK_ID', keep='first', inplace=True)
        data = data.fillna("0-0-0")
        data['station'] = data['ALL_NAME'].apply(
            lambda x: str(x).split("-")[0])
        if TYPE == 'high_quality_issue_cl_2':
            if duan == 'all':
                data = pd.concat([
                    data[data['station'].str.contains('成都车辆段', regex=False)],
                    data[data['station'].str.contains('成都动车段', regex=False)],
                    data[data['station'].str.contains('重庆车辆段', regex=False)],
                    data[data['station'].str.contains('贵阳车辆段', regex=False)]
                ], sort=False)
            else:
                data = data[data['station'].str.contains(duan, regex=False)]
        elif TYPE == 'high_quality_issue_cl_3':
            if duan != 'all':
                data = data[data['station'] == duan]
            else:
                data = pd.concat([
                    data[data['station'] == '重庆西车辆段'],
                    data[data['station'] == '成都北车辆段'],
                    data[data['station'] == '贵阳南车辆段']
                ], sort=False)
        data['risk'] = data['RISK_NAME'].apply(lambda x: x.split("-")[1])
        del data['RISK_NAME']
        # 计算高质量问题年度走势图
        trendency_rst = {}
        date = [
            int('{}{:0>2}'.format(now_year, i))
            for i in range(1, now_month + 1)
        ]
        trendency_date = [
            '{}/{:0>2}'.format(now_year, i) for i in range(1, now_month + 1)
        ]
        high_data = data.groupby('LEVEL')
        for name, group in high_data:
            level_data = group.groupby('MON').size().reset_index()
            trendency_rst[name] = [
                int(level_data[level_data['MON'] == mon][0])
                if mon in list(level_data['MON']) else 0 for mon in date
            ]
        trendency_rst['date'] = trendency_date
        # 计算月度风险公布图
        columns = ['risk', 'amount', 'amount']
        if TYPE == 'high_quality_issue_cl_1':
            pie_data = data[data['MON'] == int("{}{:0>2}".format(
                now_year, now_month))]
            if len(pie_data) == 0:
                current_app.logger.debug("高质量问题信息无饼图数据")
                all_pie_rst = []
            else:
                pie_data = pie_data.groupby('risk').size().reset_index()
                pie_data['amount'] = pie_data[0]
                all_pie_rst = get_pie_data(columns, pie_data, columns)
        else:
            pie_data = data
            pie_data = pie_data.groupby('risk').size().reset_index()
            pie_data['amount'] = pie_data[0]
            all_pie_rst = get_pie_data(columns, pie_data, columns)
        if len(all_pie_rst) == 0:
            result = [trendency_rst, {}]
        else:
            result = [trendency_rst, all_pie_rst]
    return result


def _get_cadre_evaluate_tendency(major, TYPE=None):
    duan = major[1]
    major = major[0]
    now_year = get_date(get_today()) // 100
    keys = {
        "match": {
            "MON": {
                "$gt": int('{}00'.format(now_year))
            },
            "MAJOR": major
        },
        "project": {
            "_id": 0,
            "LEVEL": 1,
            "MON": 1,
            "ALL_NAME": 1,
            "PERSON_ID": 1,
            "SCORE": 1
        }
    }
    coll = "detail_evaluate_record"
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("干部履职评价无全年信息")
        return
    else:
        data = pd.DataFrame(documents)
        data = data.fillna(0)
        data['station'] = data['ALL_NAME'].apply(
            lambda x: str(x).split('-')[0])
        title_station = [
            "成都动车段", "成都车辆段", "成都北车辆段", "重庆车辆段",
            "重庆西车辆段", "贵阳车辆段", "贵阳南车辆段"
        ]
        nowMonth = get_date(get_today())
        if TYPE == 'cadre_evluate_cl_2':
            data_station = data[data['MON'] == nowMonth]
            data_station = data_station.groupby('station').size().reset_index()
            station_rst = [
                int(data_station[data_station['station'] == station][0])
                if station in list(data_station['station']) else 0
                for station in title_station
            ]
            if duan != 'all':
                data = data[data['station'] == duan]
            else:
                data = pd.concat([
                    data[data['station'] == '成都车辆段'],
                    data[data['station'] == '成都动车段'],
                    data[data['station'] == '重庆车辆段'],
                    data[data['station'] == '贵阳车辆段']
                ], sort=False)
        elif TYPE == 'cadre_evluate_cl_3':
            data_station = data[data['MON'] == nowMonth]
            data_station = data_station.groupby('station').size().reset_index()
            station_rst = [
                int(data_station[data_station['station'] == station][0])
                if station in list(data_station['station']) else 0
                for station in title_station
            ]
            if duan != 'all':
                data = data[data['station'] == duan]
            else:
                data = pd.concat([
                    data[data['station'] == '重庆西车辆段'],
                    data[data['station'] == '成都北车辆段'],
                    data[data['station'] == '贵阳南车辆段']
                ], sort=False)
        # 计算当月数据
        month_data = data[data['MON'] == nowMonth]
        month_data = month_data.groupby('LEVEL').count().reset_index()
        month_rst = [
            int(month_data[month_data['LEVEL'] == level]['MON'])
            if level in list(month_data['LEVEL']) else 0
            for level in range(1, 8)
        ]
        # 计算当年数据
        year_data = data.groupby('PERSON_ID')['SCORE'].sum().reset_index()
        year_data['LEVEL'] = year_data['SCORE'].apply(singe_score_section)
        year_data = data.groupby('LEVEL').count().reset_index()
        year_rst = [
            int(year_data[year_data['LEVEL'] == level]['SCORE'])
            if level in list(year_data['LEVEL']) else 0
            for level in range(1, 8)
        ]
        title = ["0-2", "0-4", "4-6", "6-8", "8-10", "10-12", ">12"]
    if TYPE == 'cadre_evluate_cl_1':
        return {"date": title, "全年": year_rst, "当月": month_rst}
    else:
        return [{
            "date": title_station,
            '{}年{:0>2}月'.format(now_year, now_month): station_rst
        }, {
            "date": title,
            "全年": year_rst,
            "当月": month_rst
        }]


def get_drive_equipment(x, y, z):
    keys = ["动车组设备质量", "客车设备质量"]
    for key in keys:
        if key in x and "行车设备质量" in y and "行车设备质量" in z:
            return "行车设备质量"
    return "无"


def get_produce_equipment(x, y, z):
    keys = ["机械动力设备质量"]
    for key in keys:
        if key in x and "生产设备质量" in y and "生产设备质量" in z:
            return "生产设备质量"
    return "无"


def get_safe_manage(x, y, z):
    keys = ["安全管理"]
    for key in keys:
        if key in x and "安全管理" in y and "安全管理" in z:
            return "安全管理"
    return "无"


def get_profession_manage(x, y, z):
    keys = ["专业管理", "配件及委外管理"]
    for key in keys:
        if key in x and "专业管理" in y and "专业管理" in z:
            return "专业管理"
    return "无"


def get_work_safe(x, y, z):
    keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "客车运用作业",
        "客车检修作业", "客车设备质量", "客车辅助作业"
    ]
    for key in keys:
        if key in x and "劳动安全" in y and "专业管理" in z:
            return "劳动安全"
    return "无"


def get_fire_disaster(x, y, z):
    keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "客车运用作业",
        "客车检修作业", "客车设备质量", "客车辅助作业"
    ]
    for key in keys:
        if key in x and "火灾爆炸" in y and "火灾爆炸" in z:
            return "火灾爆炸"
    return "无"


def get_anti_terroism(x, y, z):
    keys = ["反恐防暴"]
    for key in keys:
        if key in x and "反恐怖防范" in y and "反恐怖防范" in z:
            return "反恐怖防范"
    return "无"


def get_emergency(x, y, z):
    keys = [
        "应急处置作业-动车应急处置作业", "应急处置作业-客车应急处置作业",
        "应急处置作业-其他应急处置作业"]
    for key in keys:
        if key in x and "应急处置" in y and "应急处置" in z:
            return "应急处置"
    return "无"


def get_stage(x, y, z):
    if "季节性（阶段性）风险" in y and "季节性（阶段性）风险" in z:
        return "季节性（阶段性）风险"
    return "无"


def get_anti_slip(x, y, z):
    keys = ["调车防溜"]
    for key in keys:
        if key in x and "防溜风险" in y and "防溜风险" in z:
            return "防溜风险"
    return "无"


def get_shunting(x, y, z):
    keys = ["调车防溜"]
    for key in keys:
        if key in x and "调车风险" in y and "调车风险" in z:
            return "调车风险"
    return "无"


def get_construction(x, y, z):
    keys = ["施工"]
    for key in keys:
        if key in x and "施工风险" in y and "施工风险" in z:
            return "施工风险"
    return "无"


def get_fire(x, y):
    keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "客车运用作业",
        "客车检修作业", "客车设备质量", "客车辅助作业"
    ]
    for key in keys:
        if key in x and "火灾爆炸" in y:
            return "火灾爆炸"
    return "无"


def get_ke_check_fire(x, y):
    keys = [
        "客车运用作业", "客车检修作业", "客车设备质量", "客车辅助作业"
    ]
    for key in keys:
        if key in x and "火灾爆炸" in y:
            return "火灾爆炸"
    return "无"


def get_dong_check_fire(x, y):
    keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业"
    ]
    for key in keys:
        if key in x and "火灾爆炸" in y:
            return "火灾爆炸"
    return "无"


def get_fitting(x, y):
    item_keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "客车运用作业",
        "客车检修作业", "客车设备质量", "客车辅助作业",
        "安全防范信息化系统"
    ]
    risk_keys = ["钩缓", "走行部", "车体底架"]
    for item_key in item_keys:
        for risk_key in risk_keys:
            if item_key in x and risk_key in y:
                return "配件脱落"
    return "无"


def get_ke_check_fitting(x, y):
    item_keys = [
        "客车运用作业", "客车检修作业", "客车设备质量", "客车辅助作业",
        "安全防范信息化系统"
    ]
    risk_keys = ["钩缓", "走行部", "车体底架"]
    for item_key in item_keys:
        for risk_key in risk_keys:
            if item_key in x and risk_key in y:
                return "配件脱落"
    return "无"


def get_dong_check_fitting(x, y):
    item_keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "安全防范信息化系统"
    ]
    risk_keys = ["钩缓", "走行部", "车体底架"]
    for item_key in item_keys:
        for risk_key in risk_keys:
            if item_key in x and risk_key in y:
                return "配件脱落"
    return "无"


def get_high_pressure(x, y):
    item_keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "动车组设备质量",
        "客车运用作业", "客车检修作业", "客车设备质量",
        "客车辅助作业", "安全防范信息化系统"
    ]
    for item_key in item_keys:
        if item_key in x and "动车组高压牵引" in y:
            return "高压牵引"
    return "无"


def get_dong_check_high_pressure(x, y):
    item_keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "动车组设备质量",
        "安全防范信息化系统"
    ]
    for item_key in item_keys:
        if item_key in x and "动车组高压牵引" in y:
            return "高压牵引"
    return "无"


# 动车火灾爆炸
def get_dong_fire_disaster(x, y, z):
    keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业", "动车组设备质量",
        "安全防范信息化系统"]
    for key in keys:
        if key in x and "火灾爆炸" in y and "火灾爆炸" in z:
            return "火灾爆炸"
    return "无"


# 动车配件脱落
def get_dong_fitting(x, y, z):
    item_keys = [
        "动车组运用作业", "动车组高级修", "动车辅助作业",
        "动车组设备质量", "安全防范信息化系统"]
    risk_keys = ["钩缓", "走行部", "车体底架"]
    for item_key in item_keys:
        for risk_key in risk_keys:
            if item_key in x and risk_key in y and "机辆部件脱落" in z:
                return "配件脱落"
    return "无"


# 动车调车防溜
def get_dong_shunting(x, y, z, h):
    item_keys = ["动车组调车作业", "动车组防溜作业", "特种设备设备质量"]
    risk_keys = ["调车风险", "防溜风险", "特种设备、自轮运转设备风险"]
    type_keys = ["防溜", "调车"]
    result_keys = ["冲突", "相撞", "脱轨", "溜逸", "挤岔"]
    for item_key in item_keys:
        if item_key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for type_key in type_keys:
                        if type_key in z:
                            for result_key in result_keys:
                                if result_key in h:
                                    return "调车防溜"
    return "无"


# 动车问题高压牵引
def get_dong_high_pressure(x, y, z):
    item_keys = [
        "动车运用作业", "动车组高级修", "动车组设备质量",
        "动车辅助作业", "安全防范信息化系统"]
    for item_key in item_keys:
        if item_key in x and "动车组高压牵引" in y and "动车组高压牵引" in z:
            return "高压牵引"
    return "无"


# 客车火灾爆炸
def get_ke_fire_disaster(x, y, z):
    keys = [
        "客车运用作业", "客车检修作业", "客车辅助作业", "客车设备质量",
        "安全防范信息化系统"]
    for key in keys:
        if key in x and "火灾爆炸" in y and "火灾爆炸" in z:
            return "火灾爆炸"
    return "无"


# 客车配件脱落
def get_ke_fitting(x, y, z):
    item_keys = ["客车运用作业", "客车检修作业", "客车辅助作业", "客车设备质量", "安全防范信息化系统"]
    risk_keys = ["钩缓", "走行部", "车体底架"]
    for item_key in item_keys:
        for risk_key in risk_keys:
            if item_key in x and risk_key in y and "机辆部件脱落" in z:
                return "配件脱落"
    return "无"


# 客车调车防溜
def get_ke_shunting(x, y, z, h):
    item_keys = ["客车调车作业", "客车防溜作业", "特种设备设备质量"]
    risk_keys = ["调车风险", "防溜风险", "特种设备、自轮运转设备风险"]
    type_keys = ["防溜", "调车"]
    result_keys = ["冲突", "相撞", "脱轨", "溜逸", "挤岔"]
    for item_key in item_keys:
        if item_key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for type_key in type_keys:
                        if type_key in z:
                            for result_key in result_keys:
                                if result_key in h:
                                    return "调车防溜"
    return "无"


# 客车制动供风
def get_ke_brake(x, y, z):
    item_keys = [
        "客车运用作业", "客车检修作业", "客车设备质量",
        "客车辅助作业", "安全防范信息化系统"]
    for item_key in item_keys:
        if item_key in x and "制动供风" in y and "制动供风" in z:
            return "制动供风"
    return "无"


def get_ke_check_brake(x, y):
    item_keys = [
        "客车运用作业", "客车检修作业", "客车设备质量",
        "客车辅助作业", "安全防范信息化系统"]
    for item_key in item_keys:
        if item_key in x and "制动供风" in y:
            return "制动供风"
    return "无"


# 货车火灾爆炸
def get_huo_fire_disaster(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "货车设备质量", "设备维修作业"]
    for key in keys:
        if key in x and "火灾爆炸" in y and "火灾爆炸" in z:
            return "火灾爆炸"
    return "无"


# 货车配件脱落
def get_huo_fitting(x, y, z):
    item_keys = [
        "货车运用作业", "货车检修作业", "货车设备质量",
        "辅助作业", "安全防范信息化系统"]
    risk_keys = ["防溜风险", "调车风险"]
    result_keys = ["冲突、相撞", "溜逸", "挤岔"]
    for item_key in item_keys:
        if item_key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for result_key in result_keys:
                        if result_key in z:
                            return "配件脱落"
    return "无"


def get_huo_check_fitting(x, y):
    item_keys = [
        "货车运用作业", "货车检修作业", "货车设备质量",
        "辅助作业", "安全防范信息化系统"]
    risk_keys = ["防溜风险", "调车风险"]
    for item_key in item_keys:
        if item_key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    return "配件脱落"
    return "无"


# 货车调车防溜
def get_huo_shunting(x, y, z):
    item_keys = ["货车运用作业", "货车检修作业", "货车设备质量", "辅助作业", "安全防范信息化系统"]
    risk_keys = ["防溜风险", "调车风险"]
    result_keys = ["冲突、相撞", "溜逸", "挤岔"]
    for item_key in item_keys:
        if item_key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for result_key in result_keys:
                        if result_key in y:
                            return "调车防溜"
    return "无"


# 货车车辆脱轨
def get_huo_tuogui(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "设备维修作业", "货车设备质量"]
    risk_keys = ["走形部", "路材路料"]
    result_keys = ["脱轨", "溜逸", "超限侵限"]
    for key in keys:
        if key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for result_key in result_keys:
                        if result_key in z:
                            return "车辆脱轨"
    return "无"


# 货车制动抱闸
def get_huo_brake(x, y, z):
    item_keys = ["货车运用作业", "货车检修作业", "货车设备质量"]
    for item_key in item_keys:
        if item_key in x and "制动供风" in y and "行车设备质量" in z:
            return "制动供风"
    return "无"


def get_huo_check_brake(x, y):
    item_keys = ["货车运用作业", "货车检修作业", "货车设备质量"]
    for item_key in item_keys:
        if item_key in x and "制动供风" in y:
            return "制动供风"
    return "无"


# 货车列车分离
def get_huo_fenli(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "货车设备质量", "调车防溜"]
    result_keys = ["分离", "溜逸"]
    for key in keys:
        if key in x and "钩缓" in y:
            for result_key in result_keys:
                if result_key in z:
                    return "列车分离"
    return "无"


def get_huo_check_fenli(x, y):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "货车设备质量", "调车防溜"]
    for key in keys:
        if key in x and "钩缓" in y:
            return "列车分离"
    return "无"


# 劳安车辆伤害
def get_lao_cheliang(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "货车设备质量", "调车防溜", "设备维修作业", "施工"]
    risk_keys = ["车辆伤害", "施工风险"]
    result_keys = ["从业人员伤害", "路外人员伤害"]
    for key in keys:
        if key in x:
            for risk_key in risk_keys:
                if risk_key in y:
                    for result_key in result_keys:
                        if result_key in z:
                            return "车辆伤害"
    return "无"


# 劳安机械伤害
def get_lao_jixie(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "设备维修作业", "机械动力设备质量", "安全防范信息化系统"]
    result_keys = ["从业人员伤害", "路外人员伤害"]
    for key in keys:
        if key in x and "机械伤害" in y:
            for result_key in result_keys:
                if result_key in z:
                    return "机械伤害"
    return "无"


#
def get_lao_jiaotong(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "设备维修作业", "施工"]
    result_keys = ["从业人员伤害", "路外人员伤害"]
    for key in keys:
        if key in x and "交通安全" in y:
            for result_key in result_keys:
                if result_key in z:
                    return "交通安全"
    return "无"


def get_lao_dianji(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "设备维修作业", "施工", "安全防范信息化系统"]
    result_keys = ["从业人员伤害", "路外人员伤害"]
    for key in keys:
        if key in x and "电击伤害" in y:
            for result_key in result_keys:
                if result_key in z:
                    return "电击伤害"
    return "无"


def get_lao_gaokong(x, y, z):
    keys = ["货车运用作业", "货车检修作业", "辅助作业", "设备维修作业"]
    result_keys = ["从业人员伤害", "路外人员伤害"]
    for key in keys:
        if key in x and "高空坠落" in y:
            for result_key in result_keys:
                if result_key in z:
                    return "高空坠落"
    return "无"


# 第二屏 动客车
def _get_notice_linkage_info_tendency(major, TYPE=None):
    duan = major[1]
    major = major[0]
    today = get_today()
    month = get_date(today)

    info_keys = {
        "match": {
            "MAJOR": major,
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "CHECK_ITEM_NAMES": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "RISK_NAME": 1,
        }
    }
    info_coll = 'detail_check_info'
    problem_keys = {
        "match": {
            "MAJOR": major,
            "MON": month
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "CHECK_ITEM_NAMES": 1,
            "CHECK_RISK": 1,
            "RISK_NAMES": 1,
            "DEPARTMENT_ALL_NAME": 1
        }
    }
    problem_coll = 'detail_check_problem'
    info_documents = get_data_from_mongo_by_find(info_coll, info_keys)
    problem_documents = get_data_from_mongo_by_find(problem_coll, problem_keys)
    if len(info_documents) == 0 or len(problem_documents) == 0:
        current_app.logger.debug("预警联动接口：检查信息或问题信息无当月数据")
        return
    else:
        a = 0  # 0：信息，1：问题
        dong_sta = ['成都动车段']
        ke_sta = ['成都车辆段', '重庆车辆段', '贵阳车辆段']
        if TYPE == 'notice_link_cl_2':
            if duan in dong_sta:
                fire_table = ["火灾爆炸"]
                fitting_table = ['配件脱落']
                high_table = ['高压牵引']
            elif duan in ke_sta:
                fire_table = ["火灾爆炸"]
                fitting_table = ['配件脱落']
                high_table = ['制动抱闸']
        else:
            fire_table = ["配件脱落"]
            fitting_table = ['列车分离']
            high_table = ['车辆抱闸']
        for documents in (info_documents, problem_documents):
            data = pd.DataFrame(documents)
            data = data.drop_duplicates('PK_ID')
            data = data.fillna("无")
            data['station'] = data['DEPARTMENT_ALL_NAME'].apply(
                lambda x: str(x).split("-")[0])
            if TYPE == 'notice_link_cl_2':
                data_1 = data[data['station'] == '成都车辆段']
                data_2 = data[data['station'] == '成都动车段']
                data_3 = data[data['station'] == '重庆车辆段']
                data_4 = data[data['station'] == '贵阳车辆段']
                if duan in dong_sta:
                    all_data = [data_2, data_3, data_4]
                    data = pd.concat([data_2, data_3, data_4], sort=False)
                    if a == 0:
                        yujing_names = ["cdd_info", "cqc_info", "gyc_info"]
                        yujing_quans = [1, 0.456, 0.584]
                    else:
                        yujing_names = ["cdd_pro", "cqc_pro", "gyc_pro"]
                        yujing_quans = [1, 0.456, 0.584]
                    status = 'dong'
                elif duan in ke_sta:
                    all_data = [data_1, data_3, data_4]
                    if a == 0:
                        yujing_names = ["cdc_info", "cqc_info", "gyc_info"]
                        yujing_quans = [1, 0.456, 0.584]
                    else:
                        yujing_names = ["cdc_pro", "cqc_pro", "gyc_pro"]
                        yujing_quans = [1, 0.456, 0.584]
                    data = pd.concat([data_1, data_3, data_4], sort=False)
                    status = 'ke'
            elif TYPE == 'notice_link_cl_3':
                data_1 = data[data['station'] == '重庆西车辆段']
                data_2 = data[data['station'] == '成都北车辆段']
                data_3 = data[data['station'] == '贵阳南车辆段']
                all_data = [data_1, data_2, data_3]
                data = pd.concat([data_1, data_2, data_3], sort=False)
                if a == 0:
                    yujing_names = ["cqx_info", "cdb_info", "gyn_info"]
                    yujing_quans = [0.79, 1, 0.78]
                else:
                    yujing_names = ["cqx_pro", "cdb_pro", "gyn_pro"]
                    yujing_quans = [0.79, 1, 0.78]
                status = 'huo'
            # 预警联动表格
            if status == 'dong':
                get_fire = get_dong_check_fire
                get_fitting = get_dong_check_fire
                get_high_pressure = get_dong_check_high_pressure
                get_fire_disaster = get_dong_fire_disaster
                get_pro_fitting = get_dong_fitting
                get_pro_high_pressure = get_dong_high_pressure
            if status == 'ke':
                get_fire_disaster = get_ke_fire_disaster
                get_pro_fitting = get_ke_fitting
                get_pro_high_pressure = get_ke_brake
                get_fire = get_ke_check_fire
                get_fitting = get_ke_check_fitting
                get_high_pressure = get_ke_check_brake
            elif status == 'huo':
                get_fire = get_huo_check_fitting
                get_fitting = get_huo_check_fitting
                get_high_pressure = get_huo_check_brake
                get_fire_disaster = get_huo_fitting
                get_pro_fitting = get_huo_fenli
                get_pro_high_pressure = get_huo_brake
            fire_counts = []
            fitting_counts = []
            brake_counts = []
            for data_i in all_data:
                data_i = data_i.reset_index()
                if a == 0:  # 信息
                    data_i['fire'] = data_i.apply(
                        lambda x: get_fire(
                            x['CHECK_ITEM_NAMES'], x['RISK_NAME']),
                        axis=1)
                    data_i['fitting'] = data_i.apply(
                        lambda x: get_fitting(
                            x['CHECK_ITEM_NAMES'], x['RISK_NAME']),
                        axis=1)
                    data_i['high'] = data_i.apply(
                        lambda x: get_high_pressure(
                            x['CHECK_ITEM_NAMES'], x['RISK_NAME']),
                        axis=1)
                elif a == 1:  # 问题
                    data_i['fire'] = data_i.apply(
                        lambda x: get_fire_disaster(
                            x['CHECK_ITEM_NAMES'], x['CHECK_RISK'],
                            x['RISK_NAMES']),
                        axis=1)
                    data_i['fitting'] = data_i.apply(
                        lambda x: get_pro_fitting(
                            x['CHECK_ITEM_NAMES'],
                            x['CHECK_RISK'], x['RISK_NAMES']),
                        axis=1)
                    data_i['high'] = data_i.apply(
                        lambda x: get_pro_high_pressure(
                            x['CHECK_ITEM_NAMES'],
                            x['CHECK_RISK'], x['RISK_NAMES']),
                        axis=1)
                fire_counts.append(
                    len(data_i[data_i['fire'] == fire_table[0]]))
                fitting_counts.append(len(
                    data_i[data_i['fitting'] == fitting_table[0]]))
                brake_counts.append(
                    len(data_i[data_i['high'] == high_table[0]]))
            # 计算预警联动信息
            fire_table.append(
                yujing_no_count(yujing_quans, fire_counts, yujing_names))
            fitting_table.append(
                yujing_no_count(yujing_quans, fitting_counts, yujing_names))
            high_table.append(
                yujing_no_count(yujing_quans, brake_counts, yujing_names))
            # 计算风险分布
            if a == 0:  # 只计算问题
                if duan != 'all':
                    data = data[data.DEPARTMENT_ALL_NAME.str.contains(
                        duan, regex=False)]
                all_risk = [
                    "行车设备质量", "生产设备质量", "安全管理", "专业管理",
                    "劳动安全", "火灾爆炸", "反恐怖防范", "应急处理",
                    "季节性（阶段性）风险", "防溜风险", "调车风险", "施工风险"
                ]
                if len(data) == 0:
                    all_counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                    rank_data = [{
                        "amount": 0,
                        "rank": index + 1,
                        "name": risk
                    } for index, risk in enumerate(all_risk)]
                    columns = ["risk", "count", "count"]
                    count_data = pd.DataFrame(
                        {"risk": all_risk, "count": all_counts})
                    risk_pie = get_pie_data(all_risk, count_data, columns)
                    result = [{
                        "rank": rank_data,
                        "table": [fire_table, fitting_table, high_table]
                    }, risk_pie]
                else:
                    # 行车设备质量
                    all_counts = []
                    for risk in all_risk:
                        detail_risk_data = data[data.RISK_NAME.str.contains(
                            risk, regex=False)]
                        all_counts.append(len(detail_risk_data))
                    total = sum(all_counts)
                    if total == 0:
                        rank_data = [{
                            "amount": 0,
                            "rank": index + 1,
                            "name": risk
                        } for index, risk in enumerate(all_risk)]
                        columns = ["risk", "count", "count"]
                        count_data = pd.DataFrame(
                            {"risk": all_risk, "count": all_counts})
                        risk_pie = get_pie_data(all_risk, count_data, columns)
                        result = [{
                            "rank": rank_data,
                            "table": [fire_table, fitting_table, high_table]
                        }, risk_pie]
                    else:
                        count_data = pd.DataFrame(
                            {"risk": all_risk, "count": all_counts})
                        count_data = count_data.sort_values(
                            'count', ascending=False).reset_index()
                        del count_data['index']
                        # 计算预警联动右侧排名
                        rank_data = [{
                            "amount":
                            '{:.1f}%'.format(
                                int(count_data.loc[index].at['count'])
                                / total * 100),
                            "rank":
                            index + 1,
                            "name":
                            count_data.loc[index].at['risk']
                        } for index in count_data.index]
                        # 计算安全风险分布图数据
                        columns = ["risk", "count", "count"]
                        risk_pie = get_pie_data(all_risk, count_data, columns)
                        result = [{
                            "rank": rank_data,
                            "table": [fire_table, fitting_table, high_table]
                        }, risk_pie]
            a += 1
    return result


def _get_risk_tendency_info_tendency(major, TYPE=None):
    duan = major[1]
    major = major[0]
    now_month = int(str(get_date(get_today()))[4:])
    now_year = get_date(get_today()) // 100
    keys = {
        "match": {
            "MAJOR": major,
            "MON": {
                "$gt": int('{}00'.format(now_year))
            }
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "CHECK_ITEM_NAMES": 1,
            "MON": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "RISK_NAMES": 1,
            "RISK_CONSEQUENCE_NAMES": 1
        }
    }
    coll = 'detail_check_problem'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        current_app.logger.debug("主要风险接口：检查问题信息无当月数据")
        return
    else:
        data = pd.DataFrame(documents)
        data = data.drop_duplicates(
            ['PK_ID', 'RISK_CONSEQUENCE_NAMES', 'RISK_NAMES'])
        data = data.fillna("无")
        data['station'] = data['DEPARTMENT_ALL_NAME'].apply(
            lambda x: str(x).split("-")[0])
        if TYPE == 'risk_tendency_cl_2':
            data_1 = data[data['station'] == '成都车辆段']
            data_2 = data[data['station'] == '成都动车段']
            data_3 = data[data['station'] == '重庆车辆段']
            data_4 = data[data['station'] == '贵阳车辆段']
            # 动车数据
            if duan == 'all':
                dong_data = pd.concat(
                    [data_2, data_3, data_4], sort=False)  # 动车数据
                ke_data = pd.concat(
                    [data_1, data_3, data_4], sort=False)  # 客车数据
            else:
                dong_data = data[data['station'] == duan]
                ke_data = data[data['station'] == duan]
            result = []
            date = [
                int('{}{:0>2}'.format(now_year, i))
                for i in range(1, now_month + 1)
            ]
            columns = ["name", "total", "total"]
            if duan != '成都车辆段':
                fire_data = dong_data[dong_data['RISK_NAMES'].str.contains(
                    "火灾爆炸")]
                fit_data = dong_data[dong_data['RISK_NAMES'].str.contains(
                    "脱落")]
                shunt_data = dong_data[(dong_data['RISK_NAMES'].str.contains(
                    "调车")) | (dong_data['RISK_NAMES'].str.contains("防溜"))]
                high_data = dong_data[dong_data['RISK_NAMES'].str.contains(
                    "高压牵引")]
                datas = [fire_data, fit_data, shunt_data, high_data]
                totals = [len(data_i) for data_i in datas]
                names = ["火灾爆炸", "配件脱落", "调车防溜", "高压牵引"]
                # 获取动车折线图数据
                dong_trendency = get_trendency_data(names, datas, date)
                pie_data = pd.DataFrame({"name": names, "total": totals})
                # 获取动车饼图数据
                dong_pie = get_pie_data(names, pie_data, columns)
                dong_rst = [dong_trendency, dong_pie]
                result.append({"title": "动车", "data": dong_rst})
            # 客车数据
            if duan != '成都动车段':
                fire_data = ke_data[ke_data['RISK_NAMES'].str.contains(
                    "火灾爆炸")]
                fit_data = ke_data[ke_data['RISK_NAMES'].str.contains(
                    "机辆部件脱落")]
                shunt_data = ke_data[(ke_data['RISK_NAMES'].str.contains(
                    "调车")) | (ke_data['RISK_NAMES'].str.contains("防溜"))]
                brake_data = ke_data[ke_data['RISK_NAMES'].str.contains(
                    "制动供风")]
                datas = [fire_data, fit_data, shunt_data, brake_data]
                totals = [len(data_i) for data_i in datas]
                names = ["火灾爆炸", "配件脱落", "调车防溜", "制动供风"]
                # 获取客车折线图数据
                ke_trendency = get_trendency_data(names, datas, date)
                pie_data = pd.DataFrame({"name": names, "total": totals})
                # 获取客车饼图数据
                ke_pie = get_pie_data(names, pie_data, columns)
                ke_rst = [ke_trendency, ke_pie]
                result.append({"title": "客车", "data": ke_rst})
        elif TYPE == 'risk_tendency_cl_3':
            data_1 = data[data['station'] == '重庆西车辆段']
            data_2 = data[data['station'] == '成都北车辆段']
            data_3 = data[data['station'] == '贵阳南车辆段']
            # 货车数据
            if duan == 'all':
                huo_data = pd.concat(
                    [data_2, data_3, data_1], sort=False)  # 货车数据
                lao_data = pd.concat(
                    [data_2, data_3, data_1], sort=False)  # 劳安数据
            else:
                huo_data = data[data['station'] == duan]
                lao_data = data[data['station'] == duan]
            fire_data = huo_data[huo_data['RISK_NAMES'].str.contains("火灾爆炸")]
            fit_data = huo_data[huo_data['RISK_NAMES'].str.contains("机辆部件脱落")]
            shunt_data = huo_data[(huo_data['RISK_NAMES'].str.contains(
                "调车")) | (huo_data['RISK_NAMES'].str.contains("防溜"))]
            brake_data = huo_data[huo_data['RISK_NAMES'].str.contains("制动抱闸")]
            tuo_data = huo_data[huo_data['RISK_NAMES'].str.contains("车辆脱轨")]
            fen_data = huo_data[huo_data['RISK_NAMES'].str.contains("列车分离")]
            names = ["火灾爆炸", "配件脱落", "调车防溜", "制动抱闸", "车辆脱轨", "列车分离"]
            datas = [
                fire_data, fit_data, shunt_data, brake_data, tuo_data, fen_data
            ]
            totals = [len(data_i) for data_i in datas]
            date = [
                int('{}{:0>2}'.format(now_year, i))
                for i in range(1, now_month + 1)
            ]
            # 货车折线图数据
            huo_trendency = get_trendency_data(names, datas, date)
            # 货车饼图数据
            pie_data = pd.DataFrame({"name": names, "total": totals})
            columns = ["name", "total", "total"]
            huo_pie = get_pie_data(names, pie_data, columns)
            # 货车总数据
            huo_rst = [huo_trendency, huo_pie]
            # 劳安数据
            cheliang_data = lao_data[lao_data['RISK_NAME'].str.contains(
                "车辆伤害")]
            jixie_data = lao_data[lao_data['RISK_NAME'].str.contains("机械伤害")]
            jiaotong_data = lao_data[lao_data['RISK_NAME'].str.contains(
                "交通安全")]
            dianji_data = lao_data[lao_data['RISK_NAME'].str.contains("电击伤害")]
            gaokong_data = lao_data[lao_data['RISK_NAME'].str.contains("高空坠落")]
            datas = [
                cheliang_data, jixie_data, jiaotong_data, dianji_data,
                gaokong_data
            ]
            totals = [len(data_i) for data_i in datas]
            names = ["车辆伤害", "机械伤害", "交通安全", "电击伤害", "高空坠落"]
            # 劳安折线图数据
            lao_trendency = get_trendency_data(names, datas, date)
            # 劳安饼图数据
            pie_data = pd.DataFrame({"name": names, "total": totals})
            columns = ["name", "total", "total"]
            lao_pie = get_pie_data(names, pie_data, columns)
            # 劳安总数据
            lao_rst = [lao_trendency, lao_pie]
            result = [{
                "title": "货车",
                "data": huo_rst
            }, {
                "title": "劳安",
                "data": lao_rst
            }]
    return result


def get_data(major, node, params):
    funcName = {
        # 车辆第一屏
        'safe_produce_cl_1': _get_safe_produce_info,  # 安全生产信息
        'safe_notice_cl_1': _get_safe_notice_info,  # 安全预警模块
        'important_work_cl_1': _get_important_work_info,  # 重点工作模块
        'larger_danger_cl_1': _get_larger_danger_info,  # 较大安全隐患管控情况
        'safe_risk_info_cl_1': _get_safe_risk_info_tendency,  # 安全风险管控检查信息走势图
        'safe_risk_problem_cl_1':
        _get_safe_risk_problem_tendency,  # 安全风险管控检查问题走势图
        'high_quality_issue_cl_1':
        _get_high_quality_issue_tendency,  # 高质量问题趋势图
        'cadre_evluate_cl_1': _get_cadre_evaluate_tendency,  # 干部履职评价

        # 车辆第二屏
        'safe_produce_cl_2': _get_safe_produce_info,  # 安全生产信息
        'safe_notice_cl_2': _get_safe_notice_info,  # 安全预警模块
        'important_work_cl_2': _get_important_work_info,  # 重点工作模块
        'high_issue_info_cl_2': _get_larger_danger_info,  # 高质量问题情况通报
        'notice_link_cl_2': _get_notice_linkage_info_tendency,  # 预警联动
        'risk_tendency_cl_2': _get_risk_tendency_info_tendency,  # 主要风险走势图
        'high_quality_issue_cl_2':
        _get_high_quality_issue_tendency,  # 高质量问题趋势图
        'cadre_evluate_cl_2': _get_cadre_evaluate_tendency,  # 干部履职评价

        # 车辆第三屏
        'safe_produce_cl_3': _get_safe_produce_info,  # 安全生产信息
        'safe_notice_cl_3': _get_safe_notice_info,  # 安全预警模块
        'important_work_cl_3': _get_important_work_info,  # 重点工作模块
        'notice_link_cl_3': _get_notice_linkage_info_tendency,  # 预警联动
        'risk_tendency_cl_3': _get_risk_tendency_info_tendency,  # 主要风险走势图
        'high_quality_issue_cl_3': _get_high_quality_issue_tendency,  # 高质量
        'cadre_evluate_cl_3': _get_cadre_evaluate_tendency,  # 干部履职评价
    }
    stations = ['all', 'gycl', 'cqcl', 'cqxcl',
                'cddc', 'cdcl', 'cdbcl', 'gyncl']
    if params not in stations:
        return '站段参数错误'
    if node not in funcName:
        return 'API参数错误'
    major = get_major(major)
    station = get_staiton_name(params)
    return funcName[node]([major, station], node)


if __name__ == '__main__':
    pass
