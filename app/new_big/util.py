#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    大屏API数据
'''
from datetime import datetime as dt
from datetime import timedelta

from dateutil.relativedelta import relativedelta
from flask import current_app
import pandas as pd

from app import mongo
from app.utils.common_func import get_today, get_date

now_month = dt.today().month
now_year = dt.today().year
now_day = dt.today().day


def _get_check_stat_data(today, mon, _prefix, coll_name, major, extra=None):
    '''获取当日/当月检查的新增数
    '''
    coll_name = '{}detail_{}'.format(_prefix, coll_name)
    cdn_mon = {"MON": {"$lte": int(mon), "$gte": int(mon)}}
    cdn_today = {"DATE": today}
    if major is not None:
        cdn_mon.update(major)
        cdn_today.update(major)
    if extra is not None:
        cdn_mon.update(extra)
        cdn_today.update(extra)
    mon_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_mon))
    today_num = 0
    if str(today)[-2:] != '01':
        # 如果不是月初1号
        today_num = len(mongo.db[coll_name].distinct("PK_ID", cdn_today))
    return {
        'amount': mon_num,
        'delta': today_num,
    }


def get_scce_data(major=None):
    today = get_today()
    today = int(str(today)[:10].replace("-", ''))
    now = get_today()
    mon = get_month()
    _prefix = 'daily_'
    if now.day == current_app.config.get('UPDATE_DAY'):
        # 如果是月初1号则显示上月的值
        _prefix = 'monthly_'
    if major is not None:
        major = {'MAJOR': major}
    check_info = _get_check_stat_data(today, mon, _prefix, 'check_info', major)
    check_problem = _get_check_stat_data(today, mon, _prefix, 'check_problem',
                                         major)
    safety_produce_info = _get_check_stat_data(today, mon, _prefix,
                                               'safety_produce_info', major)
    # 只统计路局检查的
    check_evaluate_info = _get_check_stat_data(
        today, mon, _prefix, 'evaluate_record', major, extra={"CHECK_TYPE": 1})
    return [
        safety_produce_info,
        check_problem,
        check_evaluate_info,
        check_info,
    ]


def get_month():
    now = get_today()
    if now.day > current_app.config.get('UPDATE_DAY'):
        last_mon = now + relativedelta(months=1)
        mon = int('{}{:0>2}'.format(last_mon.year, last_mon.month))
    else:
        mon = int('{}{:0>2}'.format(now.year, now.month))
    return mon


def get_coll_prefix(day=None):
    now = get_today()
    if now.day == current_app.config.get('UPDATE_DAY'):
        return 'monthly_'
    else:
        return 'daily_'


def data_format(hour):
    hour_section = [[0, 6], [6, 8], [8, 12], [12, 14], [14, 18], [18, 20],
                    [20, 24]]
    for idx, val in enumerate(hour_section):
        if hour >= val[0] and hour < val[1]:
            return idx + 1


def get_week_hours(start_time, end_time):
    start_time = dt.strptime(start_time, '%Y%m%d%H')
    end_time = dt.strptime(end_time, '%Y%m%d%H')
    res = set()
    res.add('{}-{}'.format(start_time.weekday() + 1,
                           data_format(start_time.hour)))
    res.add('{}-{}'.format(end_time.weekday() + 1, data_format(end_time.hour)))
    hour = 1
    while hour > 0:
        deltahour = timedelta(hours=hour)
        tmp_time = start_time + deltahour
        if tmp_time > end_time:
            break
        res.add('{}-{}'.format(tmp_time.weekday() + 1,
                               data_format(tmp_time.hour)))
        hour += 1

    return [[int(item[0]), int(item[-1])] for item in sorted(res)]


def get_data_from_mongo_by_find(coll, keys, prefix=None):
    documents = []
    if prefix is not None:
        coll_name = '{}{}'.format(prefix, coll)
        documents.extend(
            list(mongo.db[coll_name].find(keys['match'], keys['project'])))
    else:
        for prefix in ['history_', 'monthly_', 'daily_']:
            coll_name = '{}{}'.format(prefix, coll)
            documents.extend(
                list(mongo.db[coll_name].find(keys['match'], keys['project'])))
    return documents


def get_data_from_mongo_by_aggregate(coll, keys, prefix=None):
    documents = []
    if prefix is not None:
        coll_name = '{}{}'.format(prefix, coll)
        documents.extend(list(mongo.db[coll_name].aggregate(keys)))
    else:
        for prefix in ['history_', 'monthly_', 'daily_']:
            coll_name = '{}{}'.format(prefix, coll)
            documents.extend(list(mongo.db[coll_name].aggregate(keys)))
    return documents


def get_broken_line_group(data, date, col):
    group_result = []
    result = {}
    m = 1
    n = 0
    a = 0
    for name, group in data:
        if name == 0:
            continue
        else:
            a += 1
            group_result.append({
                name: [
                    int(group[col][group['MON'] == mon].values[0])
                    if mon in list(group['MON']) else 0 for mon in date
                ]
            })
            if m % 3 != 0:
                m += 1
            else:
                n += 1
                key = "part_" + str(n)
                result.update({key: group_result})
                group_result = []
                m = 1
    if len(group_result) < 3 and len(group_result) != 0:
        n += 1
        key = "part_" + str(n)
        result.update({key: group_result})
    return result


# 安全生产事故分ABCD类
def devide_type(ONEtype):
    if 'A' in ONEtype:
        return 'A'
    elif 'B' in ONEtype:
        return 'B'
    elif 'C' in ONEtype:
        return 'C'
    elif 'D' in ONEtype:
        return 'D'
    else:
        return 0


# 获取中位数
def get_median(data):
    '''获取中位数'''
    data = sorted(list(data), reverse=True)
    half = len(data) // 2
    return (data[half] + data[~half]) / 2


def get_major(major):
    majors = {
        "chewu": "车务",
        "jiwu": "机务",
        "dianwu": "电务",
        "gongwu": "工务",
        "gongdian": "供电",
        "cheliang": "车辆"
    }
    return majors[major]


def get_risk_name(all_name):
    if all_name is not None:
        names = all_name.split("-")
        if "接发列车" in names[1]:
            return "接发列车"
        elif "调车" in names[1]:
            return "调车"
        elif "防溜" in names[1]:
            return "防溜"
        elif "设备" in names[1]:
            return "设备问题"
        elif "客运" in names[1]:
            return "客运"
        elif "货" in names[1]:
            return "货运"
        else:
            return names[1]
    else:
        return


def get_station(stas):
    '''获取站段名称'''
    station = ""
    stations = []
    if len(stas.split(",")) > 1:
        for sta in stas.split(","):
            stations.append(sta.split('-')[0])
        station = ','.join(list(set(stations)))
    else:
        station = stas.split('-')[0]
    return station


def get_twelve_month(f_month=1):
    """
    获取十二个月的月份,及当年的月份，默认不含有当月的月份
    :param f_month: 若f_month=0,则为获取包含当月的
    :return:[[历史12个月的月份]，[本年度的月份]]  月份结构 年份/月份
    """
    month_li = [
        "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"
    ]
    today = get_today()
    # day = dt.today().day
    # month = dt.today().month
    # year_n = dt.today().year
    day = today.day
    month = today.month
    year_n = today.year
    if day >= current_app.config.get('UPDATE_DAY') and month < 12:
        month_n = month + 1
    elif day >= current_app.config.get('UPDATE_DAY') and month == 12:
        month_n = 1
        year_n = year_n + 1
    else:
        month_n = month
    # month = get_date(dt.today())
    # month_n = int(str(month)[4:])
    # year_n = int(str(month)[:4])
    # print(month_n, year_n)
    last_year = year_n - 1
    if month_n == 1:
        f_month = 0
    twelve_month_li = [
        str(last_year) + "/" + str(i) for i in month_li[month_n - f_month:]
    ]
    twelve_month_li1 = [
        str(year_n) + "/" + str(i) for i in month_li[:month_n - f_month]
    ]

    twelve_month_li.extend(twelve_month_li1)
    return [twelve_month_li, twelve_month_li1]


def get_broken_month(today, delta_1=2, delta_2=5):
    """获取指定折线图月份数据，规则：当前月份是一月时，共获取当三个月数据（11，12，1）
                        当前月份不是一月时，共获取六个月数据（前五个月加上当月）
    Arguments:
        today {date} -- 当前日期

    Keyword Arguments:
        delta_1 {int} -- [description] (default: {2})
        delta_2 {int} -- [description] (default: {5})

    Returns:
        [list] -- 起始月份和结束月份
    """
    to_month = get_date(today)
    month = to_month % 100
    now_date = dt(to_month//100, month, 1)
    if month == 1:
        # start_date = now_date - relativedelta(months=delta_1)
        month_list = [
            now_date - relativedelta(months=(delta_1-i))
            for i in range(0, delta_1+1)
        ]
    else:
        # start_date = now_date - relativedelta(months=delta_2)
        month_list = [
            now_date - relativedelta(months=(delta_2-i))
            for i in range(0, delta_2+1)
        ]
    # month_list = []
    # start_mon = int(f'{start_date.year}{start_date.month:0>2}')
    # end_mon = int(f'{today.year}{today.month:0>2}')
    return month_list


# 根据干部扣分计算分级
def singe_score_section(score):
    if score <= 2:
        return 1
    elif score <= 4:
        return 2
    elif score <= 6:
        return 3
    elif score <= 8:
        return 4
    elif score <= 10:
        return 5
    elif score <= 12:
        return 6
    else:
        return 7


def get_trendency_data(names, datas, title):
    """获取折线图数据函数"""
    trendency_rst = {}
    for idx, name in enumerate(names):
        data = datas[idx]
        data = data.groupby('MON').size().reset_index()
        trendency_rst[name] = [
            int(data[0][data['MON'] == d]) if d in list(data['MON']) else 0
            for d in title
        ]
    trendency_rst['date'] = [
        '{}/{:0>2}'.format(str(d)[:4],
                           str(d)[4:]) for d in title
    ]
    return trendency_rst


def get_pie_data(names, datas, columns):
    '''获取饼图数据
    names:
    datas: dataframe类型数据
    columns: len=3 (name,amount,value)'''
    name = columns[0]
    amount = columns[1]
    value = columns[2]
    datas = datas.sort_values(amount, ascending=False).reset_index()
    pivot = get_median(set(datas[amount]))
    big_half = datas[datas[amount] > pivot].reset_index()
    small_half = datas[datas[amount] <= pivot].reset_index()
    pie = []
    pie.extend([{
        "name": big_half.loc[index].at[name],
        "amount": int(big_half.loc[index].at[amount]),
        "value": int(big_half.loc[index].at[value]),
        "rank": index
    } for index in big_half.index])
    pie.extend([{
        "name": small_half.loc[index].at[name],
        "amount": int(small_half.loc[index].at[amount]),
        "value": int(small_half.loc[index].at[value]),
        "rank": len(small_half) - index - 1
    } for index in small_half.index])

    return {"data": pie, "pivot": pivot}


def get_department_name(_ID, TYPE, MAJOR, HIERARCHY, LEVEL):
    keys = {
        "match": {
            "MAJOR": MAJOR,
            "HIERARCHY": HIERARCHY,
            "TYPE": LEVEL,
            "IS_DELETE": 0,
            "SHORT_NAME": {
                "$ne": None
            }
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            TYPE: 1
        }
    }
    coll = 'base_department'
    documents = get_data_from_mongo_by_find(coll, keys, prefix="")
    result = {}
    for doc in documents:
        result[doc[TYPE]] = doc['NAME']
    return result.get(_ID)
    # return result


def get_check_info_data(major, mon):
    '''
    params: major: 业务处名称
            mon: 日期条件
    return: info_data: 检查信息数据'''
    duan = major[1]
    major = major[0]
    # 检查信息数据
    info_keys = {
        "match": {
            "MAJOR": major,
            "MON": mon
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "RISK_NAME": 1,
            "CHECK_WAY": 1,
            "RETRIVAL_TYPE_NAME": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "IS_DONGCHE": 1,
            "IS_KECHE": 1,
            "MON": 1,
            "DATE": 1,
            "ID_CARD": 1,
            "START_CHECK_HOUR": 1,
            "END_CHECK_HOUR": 1,
            "CHECK_WAY": 1,
        }
    }
    info_coll = 'detail_check_info'
    info_documents = get_data_from_mongo_by_find(info_coll, info_keys)
    if len(info_documents) == 0:
        current_app.logger.debug("电务无当月检查信息数据")
        return []
    else:
        # 数据去重，填充处理
        info_data = pd.DataFrame(info_documents)
        info_data = info_data.drop_duplicates('PK_ID')
        info_data = info_data.fillna('0-0-0')
        # 判断业务处和站段
        if duan != 'all':
            info_data = info_data[info_data.DEPARTMENT_ALL_NAME.str.contains(
                duan, regex=False)]
        info_data['station'] = info_data['DEPARTMENT_ALL_NAME'].apply(
            lambda x: str(x).split('-')[0])
    return info_data


def get_check_pro_data(major, mon):
    '''
    params: major: 业务处名称
    return: info_data: 检查问题数据'''
    duan = major[1]
    major = major[0]
    problem_keys = {
        "match": {
            "MAJOR": major,
            "MON": mon
        },
        "project": {
            "_id": 0,
            "PK_ID": 1,
            "ID_CARD": 1,
            "RISK_NAMES": 1,
            "LEVEL": 1,
            "ALL_NAME": 1,
            "DEPARTMENT_ALL_NAME": 1,
            "SERIOUS_VALUE": 1,
            "CHECK_ITEM_NAME": 1,
            "PROBLEM_SCORE": 1,
            "DESCRIPTION": 1,
            "IS_DONGCHE": 1,
            "IS_KECHE": 1,
            "PROBLEM_POINT": 1,
            "MON": 1,
            "DATE": 1,
        }
    }
    problem_coll = 'detail_check_problem'
    pro_documents = get_data_from_mongo_by_find(problem_coll, problem_keys)
    if len(pro_documents) == 0:
        current_app.logger.debug("电务无当月检查问题数据")
        return []
    else:
        # 数据去重，填充处理
        pro_data = pd.DataFrame(pro_documents)
        pro_data = pro_data.drop_duplicates(['PK_ID', 'RISK_NAMES'])
        pro_data = pro_data.fillna('0-0-0')
        # 判断业务处和站段
        if duan != 'all':
            pro_data = pro_data[pro_data.DEPARTMENT_ALL_NAME.str.contains(
                duan, regex=False)]
        pro_data['station'] = pro_data['DEPARTMENT_ALL_NAME'].apply(
            lambda x: str(x).split('-')[0])
    return pro_data


def get_detail_index(major, month, coll):
    keys = {
        "match": {
            "MAJOR": major,
            "HIERARCHY": 3,
            "MON": month
        },
        "project": {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "DEPARTMENT_ID": 1
        }
    }
    documents = get_data_from_mongo_by_find(coll, keys)
    return documents
