#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: 指数中间计算过程的数据导出相关函数
'''

import pandas as pd
from app import mongo
from app.data.util import get_history_months, get_coll_prefix, write_bulk_mongo
from app.data.index.common import append_major_column_to_df, df_merge_with_dpid


def combine_and_format_basic_data_to_mongo(data, major_data, months_ago,
                                           hierarchy, main_type, detail_type):
    """将中间过程各个子指数的组成部分数据拼接好

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    calc_df_data = pd.concat(data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, main_type, detail_type, hierarchy, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago,
                                     hierarchy, main_type, detail_type)


def write_export_basic_data_to_mongo(data, months_ago, hierarchy, main_type,
                                     detail_type):
    """将中间过程写入mongo，写之前需要删除之前的对应月份历史数据，避免冗余

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    prefix = get_coll_prefix(months_ago)
    mon = get_history_months(months_ago)[0]
    coll_name = f'{prefix}health_index_basic_data'
    # 删除历史数据
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


def format_export_basic_data(data, main_type, detail_type, hierarchy,
                             months_ago):
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in data.iterrows():
        data_rst.append({
            'TYPE': 2,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'CONTENT': row['CONTENT'],
        })
    return data_rst


def export_basic_data_dicttype(data, dpid_data, main_type, detail_type,
                               hierarchy, months_ago, func_html_desc):

    data = data.groupby(['TYPE3'])['COUNT'].sum()
    data = append_major_column_to_df(dpid_data, data.to_frame(name='CONTENT'))
    data.fillna(0, inplace=True)
    data['CONTENT'] = data['CONTENT'].apply(lambda x: func_html_desc(x))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def _combine_second_filed_to_str(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row.loc[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


def export_basic_data_tow_field_monthly(data, dpid_data, major_data, main_type,
                                        detail_type, hierarchy, months_ago,
                                        first_title, second_title):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()
    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str(
            row, first_title, second_title),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def export_basic_data_one_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        func_html_desc,
                                        title=None):
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    data['MONTH'] = data['MONTH'].apply(lambda x: func_html_desc(x))
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    if title is not None:
        title += '<br/>'
    else:
        title = ''
    data['CONTENT'] = data.apply(
        lambda row: title + '<br/>'
        .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def _export_basic_data_ratio_type(df_data, dpid_data, main_type, detail_type,
                                  hierarchy, months_ago, col_numerator,
                                  col_denominator, col_quotient, col_score):
    df_data = add_avg_score_by_major(df_data, dpid_data, col_quotient)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
            ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_SCORE': round(row['avg_major'], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def export_basic_data(df_data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, col_quotient, col_avg,
                      col_score):
    """将计算中间过程导出，包含分子、分母、专业均数、得分、排名

    Arguments:
        df_data {DataFrame} -- 数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
        col_numerator {float} -- 计算过程分子
        col_denominator {float} -- 计算过程分母
        col_quotient {float} -- 计算过程商
        col_avg {float} -- 专业均数
        col_score {float} -- 各单位得分
    """
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_score].groupby(df_data['MAJOR']).rank(
        ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_SCORE': round(row[col_avg], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def add_avg_score_by_major(df, dpid_data, column):
    """给dataframe增加专业均值

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='avg_major'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data
