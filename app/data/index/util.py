#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: common functions for diaoche_index module.
'''
import calendar
import os
import re
import logging
import time
import traceback
from functools import wraps
import datetime
from dateutil.relativedelta import relativedelta
from flask import current_app

from app.data.util import (get_history_months, get_previous_month, pd_query)

from app.utils.common_func import get_today
from app.utils.data_update_models import UpdateMonth


def get_year_month(months_ago):
    year_mon = get_history_months(months_ago)[0]
    last_month = get_previous_month(months_ago)
    return year_mon, last_month


def get_custom_month(months_ago):
    """获取前第-N个月的时间统计范围，一般是上月25号到次月24号

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        tuple(str) -- 前-N个月的时间统计范围
    """
    update_day = current_app.config.get('UPDATE_DAY')
    delta = -1
    today = get_today()
    if today.day >= update_day:
        delta = 0
    start_month = today + relativedelta(months=(months_ago + delta))
    end_month = today + relativedelta(months=(months_ago + delta + 1))
    start_date = '{}-{:0>2}-{}'.format(start_month.year, start_month.month,
                                       update_day)
    end_date = '{}-{:0>2}-{}'.format(end_month.year, end_month.month,
                                     update_day)
    return (start_date, end_date)


def get_months_from_201712(months_ago, months=None):
    """一直向前取6个月的日期（到2017年12月为止）

    Arguments:
        months_ago {int} -- 更新前-N个月份，比如更新上个月也是-1，依次-2，,-3...]

    Returns:
        list(tuple) -- 需要从数据库中加载的数据的月份
    """
    # 默认数据库开始的年月是2017年9月份
    init_month = current_app.config.get('DATABASE_INIT_MONTH', '20171125')
    if init_month.isdigit():
        init_month = int(init_month)
    else:
        init_month = 20171125
    rst = []
    i = -1
    while True:
        i_month = get_custom_month(i)
        if int(re.sub('\\D', '', i_month[0])) < init_month:
            break
        rst.append(i_month)
        i = i - 1
    months_end = 6
    if months is not None:
        months_end = months
    return rst[:months_end]


def get_months_from_201712_two(months_ago, months=None):
    """
    一直向前取months个月的日期（到2017年12月为止）
    :param months_ago: {int} -- 更新前-N个月份，比如更新上个月也是-1，依次-2，,-3...]
    :param months: 一直向前取months个月的日期
    :return: list(tuple) -- 需要从数据库中加载的数据的月份
    """
    # 默认数据库开始的年月是2017年9月份
    init_month = current_app.config.get('DATABASE_INIT_MONTH', '20171125')
    if init_month.isdigit():
        init_month = int(init_month)
    else:
        init_month = 20171125
    rst = []
    i = months_ago
    while True:
        i_month = get_custom_month(i)
        if int(re.sub('\\D', '', i_month[0])) < init_month:
            break
        rst.append(i_month)
        i = i - 1
    months_end = 6
    if months is not None:
        months_end = months
    return rst[:months_end]


def retrieve_all_check_item_ids(top_ids):
    """
    获取所有检查项目及其子检查项目的ID。
    top_ids: list - 顶层的check_item_id列表
    return: string - t_check_item.PK_ID的列表。如 101,102,103。
                     返回的列表中包含top_ids
    """
    # 查询所属的子check_item的PK_ID
    load_child_check_item_sql = "SELECT PK_ID FROM t_check_item WHERE PARENT_ID IN ({0}) AND IS_DELETE=0;"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret


def retrieve_all_risk_ids(top_ids):
    """
    获取所有风险的ID的清单
    top_ids: list - 顶层的risk的pk_id列表
    return: string - t_risk.PK_ID的列表。如 101,102,103。
                     返回的列表中包含top_ids
    """
    # 查询所属的子risk的PK_ID
    load_child_risk_sql = "SELECT PK_ID FROM t_risk WHERE PARENT_ID IN ({0}) AND IS_DELETE=0;"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_risk_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret


def get_query_condition_by_risktype(risktype_id):
    """
    :param risktype_id: int or list
    :return:
    """
    if isinstance(risktype_id, list):
        ids = pd_query("""
            SELECT
                    CHECK_ITEM_IDS, RISK_IDS, POSITION
                FROM
                    t_risk_statistics_config
                WHERE
                    PK_ID in {0}""".format(tuple(risktype_id)))
        risk_ids = ','.join(ids['RISK_IDS'].values.tolist())
        risk_ids = retrieve_all_risk_ids(risk_ids)
        check_ids = ','.join(ids['CHECK_ITEM_IDS'].values.tolist())
        check_ids = retrieve_all_check_item_ids(check_ids)
        position_str = ','.join(ids['POSITION'].values.tolist()).replace('\n\t', '')
        position = [i for i in position_str.split(',') if i]
        return (check_ids, risk_ids, ','.join(position))
    else:
        sql = """SELECT
                CHECK_ITEM_IDS, RISK_IDS, POSITION
            FROM
                t_risk_statistics_config
            WHERE
                PK_ID = {} LIMIT 1;
        """.format(risktype_id)
        record = pd_query(sql)
        if record.empty:
            raise ValueError(f'{risktype_id}不存在.')
        check_item_ids = retrieve_all_check_item_ids(record['CHECK_ITEM_IDS'].values[0])
        risk_ids = retrieve_all_risk_ids(record['RISK_IDS'].values[0])
        return (check_item_ids, risk_ids, record['POSITION'].values[0])


def validate_exec_month(func):
    """一个检查指数计算月份是否有效的装饰器

    Arguments:
        func  -- 指数计算入口函数

    Raises:
        ValueError -- [传入的月份错误]

    Returns:
        func --返回被调函数
    """

    @wraps(func)
    def wrapper(months_ago: UpdateMonth, *args, **kwargs):
        start = time.time()
        if isinstance(months_ago, UpdateMonth):
            # 如果是执行专业重点指数，则需要传入risk参数
            if months_ago.risk is not None:
                risk = months_ago.risk
                args = (risk,)
            months_ago = months_ago.get_month_delta()
        if months_ago < 0:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(
                f'months_agos-[{months_ago}] should not less than 0')

        months_ago *= -1
        if get_history_months(months_ago)[0] < 201710:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(f'months_agos-[{months_ago}] param error')

        # 判断是否是25-月底，如果是则更新指数
        if months_ago == 0 and get_today().day < current_app.config.get(
                'UPDATE_DAY'):
            return 'NO NEED TO UPDATE'

        current_app.logger.debug(
            f'── {func.__module__} index computing starts.')
        nowdate = datetime.date.today()
        startdate = nowdate.replace(day=25)
        # end = nowdate + datetime.timedelta(days=days)
        enddate = nowdate.replace(day=27)
        flag = True
        try:
            # 获取函数模块最后一个文件名
            module_name_fragments = str(func.__module__).split('.')
            if module_name_fragments[-1] == "combine_child_index":
                flag = True
            elif module_name_fragments[-1] not in [
                "assess_intensity",
                "evaluate_intensity",
                "problem_rectification",
                'health_index',
                'diaoche_index',
            ]:
                if startdate <= nowdate < enddate:
                    flag = True
                else:
                    flag = False
                    # current_app.logger.debug(
                    #     f"├── └── {func.__module__} can't execute in illegal date( {nowdate} )!")
            print("TMP_UPDATE_MODE: ", os.getenv("TMP_UPDATE_MODE"))
            if not os.getenv("TMP_UPDATE_MODE"):
                if flag:
                    flag = True
                else:
                    current_app.logger.debug(f"├── └── {func.__module__} can't execute in illegal date( {nowdate} )!")
            else:
                flag = True
            # 暂时屏蔽该功能
            # 判断运行环境是否为正式环境145(目前145每天运行)
            # machine_flag = True if "10.192.34.145" in current_app.config.get('MONGO_URI') else False
            # if machine_flag or flag:
            if flag:
                func(months_ago, *args, **kwargs)
        except Exception as e:
            # return 'ERROR'
            # 不再此异常处理,抛出让最外层处理
            current_app.logger.error(traceback.format_exc())
            raise e
        if flag:
            current_app.logger.debug(f'── {func.__module__} index done.')
            cost = round(time.time() - start, 3)
            current_app.logger.debug(f'── {func.__module__} had costed time --- {cost}.')
        return 'OK'

    return wrapper


def get_month_day(months_ago):
    """
    获取安监月份的天数
    """
    today = get_today()
    if 25 <= today.day < 32:
        date = today + relativedelta(months=months_ago)
    else:
        date = today + relativedelta(months=months_ago - 1)
    days = calendar.monthrange(date.year, date.month)[1]
    return days


if __name__ == '__main__':
    pass
