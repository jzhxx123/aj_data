#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: common functions for index module.
'''

import pandas as pd
from flask import current_app

from app import mongo

from app.data.util import (get_coll_prefix, get_history_months,
                           write_bulk_mongo, pd_query, get_mongodb_prefix)


def get_zhanduan_deparment(sql):
    data = pd_query(sql)
    # 将所属专业属性为客运的归纳为新的专业（客运）
    data['MAJOR'][data['BELONG_PROFESSION_NAME'] == '客运'] = '客运'
    # 将工电段单独计算
    data['MAJOR'][data['NAME'].str.contains('工电段')] = '工电'
    data.drop(['BELONG_PROFESSION_NAME'], inplace=True, axis=1)
    return data


def df_merge_with_dpid(data, dpid_data, how='inner'):
    """将指数计算需要的数据跟部门单位连接后获取单位dpid

    Arguments:
        data {dataFrame} -- 指数数据
        dpid_data {dataFrame} -- 部门单位(参与计算的)

    Keyword Arguments:
        how {str} -- 连接方式 (default: {'inner'})

    Returns:
        dataFrame --各单位的数据
    """
    data = pd.merge(
        data,
        dpid_data,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data


def _add_avg_number_by_major_for_check_count_per_person(
        months_ago, df, dpid_data, main_type=1, detail_type=2):
    # total_major_base_data 的键 是main-detail_type
    key = str(main_type) + '-' + str(detail_type)
    total_major_base_data = {
        # 换算单位检查频次
        "1-2": [('车务', 1.7), ('客运', 0.8), ('机务', 0.9), ('工务', 0.82),
                ('工电', 0.82), ('电务', 1), ('供电', 0.68), ('车辆', 0.6)],

        # 换算单位人均质量分
        "1-6": [('车务', 1.9), ('客运', 0.7), ('机务', 1.2), ('工务', 1.35),
                ('工电', 1.35), ('电务', 2.8), ('供电', 2.5), ('车辆', 0.87)],
        # 干部人均主动评价记分条数
        "2-2": [('车务', 0.15), ('客运', 0.15), ('机务', 0.15), ('工务', 0.15),
                ('工电', 0.15), ('电务', 0.15), ('供电', 0.15), ('车辆', 0.15)],
        # 问题暴露问题数
        # 问题暴露问题分
        # 换算单位考核问题数
        "3-1": [('车务', 0.5), ('客运', 0.35), ('机务', 0.8), ('工务', 0.45),
                ('工电', 0.45), ('电务', 1.02), ('供电', 0.45), ('车辆', 0.39)],
        # 换算单位考核金额
        "3-2": [('车务', 40), ('客运', 35), ('机务', 55), ('工务', 40),
                ('工电', 40), ('电务', 80.37), ('供电', 40), ('车辆', 35)],
        # 考核率
        "3-4": [('车务', 0.9), ('客运', 0.9), ('机务', 0.9), ('工务', 0.9),
                ('工电', 0.9), ('电务', 0.9), ('供电', 0.9), ('车辆', 0.9)],
    }
    major_base_data = total_major_base_data.get(key, total_major_base_data['1-2'])
    major_base_data_by_month = [[item[0], item[1], month]
                                for item in major_base_data
                                for month in range(1, 13)]
    # 处理个别专业个别月份特殊值
    if key == "1-2":
        major_base_data_by_month[6 * 12 + 1] = ['供电', 0.58, 2]
    mon = get_history_months(months_ago)[0] % 100
    major_base_data = [
        major_base_data_by_month[i * 12 + mon - 1][:2] for i in range(8)
    ]
    df_major_base = pd.DataFrame(
        data=major_base_data, columns=['MAJOR', 'AVG_NUMBER'])
    # 增加专业列
    df = append_major_column_to_df(dpid_data, df)
    data = pd.merge(
        df, df_major_base, how='left', left_on='MAJOR', right_on='MAJOR')
    data.fillna(0, inplace=True)
    return data


def calc_check_count_per_person(df_numerator,
                                df_denominator,
                                index_type,
                                main_type,
                                detail_type,
                                months_ago,
                                stats_field,
                                column,
                                calc_func,
                                dpid_func,
                                hierarchy_list=[3],
                                risk_type=None,
                                major_ratio_dict={'车务': [(1.1, 100), (0.9, 90), (0.5, 60)],
                                                  '客运': [(0.7, 100), (0.65, 90), (0.5, 60)],
                                                  '机务': [(1, 100), (0.9, 90), (0.6, 60)],
                                                  '工务': [(0.81, 100), (0.68, 80), (0.54, 60)],
                                                  '工电': [(0.8, 100), (0.67, 80), (0.53, 60)],
                                                  '车辆': [(0.442, 100), (0.423, 90), (0.385, 60)],
                                                  '电务': [(1.2, 100), (1, 90), (0.75, 60)],
                                                  '供电': [(0.816, 100), (0.68, 90), (0.51, 60)], }):
    """综合指数-换算单位检查频次的基数是路局提交的单独表格数据

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        column = f'{column}_{hierarchy}'
        # 需要跟基数（专业均值）作比较得出分数
        df_calc = _add_avg_number_by_major_for_check_count_per_person(
            months_ago, df_calc, dpid_func(hierarchy), main_type, detail_type)
        df_calc['SCORE'] = df_calc.apply(
            lambda row: calc_func(row['ratio'], row['MAJOR'], major_ratio_dict), axis=1)
        # 导出指数中间过程
        export_basic_data(df_calc, main_type, detail_type, hierarchy,
                          months_ago, 'numerator', 'denominator', 'ratio',
                          'AVG_NUMBER', 'SCORE', risk_type)
        rst_data = pd.DataFrame(
            index=df_calc['DEPARTMENT_ID'],
            data=df_calc['SCORE'].values,
            columns=[column])

        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(rst_data, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(rst_data[[column]])
    return rst_index_score


def calc_child_index_type_divide(df_numerator,
                                 df_denominator,
                                 index_type,
                                 main_type,
                                 detail_type,
                                 months_ago,
                                 stats_field,
                                 column,
                                 calc_func,
                                 dpid_func,
                                 is_calc_score_base_major=True,
                                 hierarchy_list=[3],
                                 risk_type=None):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        column = f'{column}_{hierarchy}'
        if is_calc_score_base_major:
            # 需要跟基数（专业均值）作比较得出分数
            df_calc = calc_score_groupby_major(
                df_calc, dpid_func(hierarchy), hierarchy, main_type,
                detail_type, 'numerator', 'denominator', 'ratio', column,
                calc_func, months_ago, risk_type, index_type=index_type)
        else:
            df_calc[column] = df_calc['ratio'].apply(lambda x: calc_func(x))
            # 导出中间过程
            if risk_type is None or risk_type == '车务-1':
                _export_basic_data_ratio_type(
                    df_calc, dpid_func(hierarchy), main_type, detail_type,
                    hierarchy, months_ago, 'numerator', 'denominator', 'ratio',
                    column, risk_type, index_type=index_type)
        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def calc_child_index_type_divide_major(df_numerator,
                                       df_denominator,
                                       index_type,
                                       main_type,
                                       detail_type,
                                       months_ago,
                                       stats_field,
                                       column,
                                       calc_func,
                                       dpid_func,
                                       is_calc_score_base_major=True,
                                       hierarchy_list=[3],
                                       risk_type=None,
                                       fraction=None):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        if fraction:
            # 将基础数据存起来
            write_cardinal_number_basic_data(df_calc, fraction,
            main_type, detail_type, months_ago)
        column = f'{column}_{hierarchy}'
        if is_calc_score_base_major:
            # 需要跟基数（专业均值）作比较得出分数
            df_calc = calc_score_groupby_major_two(
                df_calc, dpid_func(hierarchy), hierarchy, main_type,
                detail_type, 'numerator', 'denominator', 'ratio', column,
                calc_func, months_ago, risk_type, index_type=index_type,
                fraction=fraction)
        else:
            df_calc[column] = df_calc['ratio'].apply(lambda x: calc_func(x))
            # 导出中间过程
            if risk_type is None or risk_type == '车务-1':
                _export_basic_data_ratio_type_major(
                    df_calc, dpid_func(hierarchy), main_type, detail_type,
                    hierarchy, months_ago, 'numerator', 'denominator', 'ratio',
                    column, risk_type, index_type=index_type)
        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def calc_child_index_type_sum(data,
                              index_type,
                              main_type,
                              detail_type,
                              months_ago,
                              stats_field,
                              column,
                              calc_func,
                              dpid_func,
                              hierarchy_list=[3],
                              risk_type=None,
                              NA_value=True):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        data {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list} 
            -- 站段、车间、班组 (default: {[3]})
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
        NA_value {bool} 
            -- 没有统计到的单位,如果是计算扣分型数，则为0，(default: {Flase})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    rst_index_score = []
    for hierarchy in hierarchy_list:
        xdata = data.dropna(subset=[f'TYPE{hierarchy}'], axis=0)
        xdata = xdata.groupby([f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = xdata.to_frame(name='SCORE')
        # 给没有统计到的单位添加指定缺省值
        if NA_value:
            dpid_data = dpid_func(hierarchy)
            if risk_type is not None:
                dpid_data = dpid_data[dpid_data['MAJOR'] == risk_type.split(
                    '-')[0]]
            df_calc = pd.merge(
                df_calc,
                dpid_data,
                how='outer',
                left_index=True,
                right_on='DEPARTMENT_ID')
            df_calc.fillna(0, inplace=True)
            df_calc = pd.DataFrame(
                index=df_calc['DEPARTMENT_ID'],
                data=df_calc.loc[:, 'SCORE'].values,
                columns=['SCORE'])
        if df_calc.empty is True:
            continue
        column = f'{column}_{hierarchy}'
        df_calc[column] = df_calc['SCORE'].apply(lambda x: calc_func(x))
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def append_major_column_to_df(dpid_data, data):
    """增加各个站段/车间/班组增加专业列

    Arguments:
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        data {type} --

    Returns:
        DataFrame --
    """
    data = pd.merge(
        dpid_data, data, how='left', left_on='DEPARTMENT_ID', right_index=True)
    return data


def _get_risk_type(risk_type):
    return int(risk_type.split('-')[1])


def calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, risk_type):
    """按照专业分组排名，将排名、分数数据写入数据库

    Arguments:
        data {[type]} -- 带有分值的按单位分组选好的
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        risk_type {str} -- 表示某专业的第几个重点风险分析指数，ex: 车务-1
    """
    data.fillna(0, inplace=True)
    data['group_sort'] = data[column].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    score_rst = []
    mon = get_history_months(months_ago)[0]
    major_risk_type = 0 if index_type == 1 else index_type
    if risk_type is not None:
        major_risk_type = _get_risk_type(risk_type)
    for index, row in data.iterrows():
        score_rst.append({
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'TYPE': major_risk_type,
            'SCORE': round(row[column], 2),
            'RANK': int(row['group_sort']),
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME']
        })
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}detail_{coll_prefix}_index'
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    if risk_type is not None:
        r_condition.update({
            'MAJOR': risk_type.split('-')[0],
            'TYPE': major_risk_type
        })
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, score_rst)


def summizet_operation_set(data,
                           dpid_data,
                           column,
                           hierarchy,
                           index_type,
                           main_type,
                           detail_type,
                           months_ago,
                           risk_type=None):
    """拿到业务逻辑数据计算后的一套公共操作（排名、子指数汇总、入库）

    Arguments:
        data {DataFrame} -- index为部门ID
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    Keyword Arguments:
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
    """
    data = append_major_column_to_df(dpid_data, data)
    calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, risk_type)


def merge_all_child_item(row, hierarchy, item_name, item_weight):
    """将指数的各个子项按照权重合并成一个总数

    Arguments:
        row {[type]} --
        hierarchy {int} -- 单位层级
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Returns:
        float -- 指数分数
    """
    score = []
    for item in item_name:
        item = f'{item}_{hierarchy}'
        if item in row:
            score.append(round(row[item], 2))
        else:
            score.append(0)
    rst_score = sum([round(i[0], 2) * i[1] for i in zip(item_weight, score)])
    rst_score = 0 if rst_score < 0 else round(rst_score, 2)
    return rst_score


def summizet_child_index(child_score,
                         dpid_func,
                         index_type,
                         main_type,
                         months_ago,
                         item_name,
                         item_weight,
                         hierarchy_list=[3],
                         risk_type=None):
    """将指数的各个子项按照权重合并成一个总数， 计算排名、入库

    Arguments:
        child_score {list[DataFrame]} -- 各个子项的集合
        dpid_func {func} -- 对应层次单位的函数
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        months_ago {int} -- 第前{-N}个月
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Keyword Arguments:
        hierarchy_list {list} -- 站段、车间、班组 (default: {[3]})
    """
    if len(child_score) == 0:
        return
    for hierarchy in hierarchy_list:
        h_child_score = [
            x for x in child_score if x.columns.values[0][-1] == str(hierarchy)
        ]
        if h_child_score == []:
            continue
        data = pd.concat(h_child_score, axis=1, sort=False)
        data.fillna(0, inplace=True)
        data['SCORE'] = data.apply(
            lambda row: merge_all_child_item(row, hierarchy, item_name, item_weight),
            axis=1)

        data = append_major_column_to_df(dpid_func(hierarchy), data)
        calc_and_rank_index(data, 'SCORE', hierarchy, index_type, main_type, 0,
                            months_ago, risk_type)


def combine_and_format_basic_data_to_mongo(data,
                                           major_data,
                                           months_ago,
                                           hierarchy,
                                           main_type,
                                           detail_type,
                                           risk_type=None,
                                           index_type=0):
    """将中间过程各个子指数的组成部分数据拼接好

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    calc_df_data = pd.concat(data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join(['%s' % row[col] for col in columns if pd.notna(row[col])]), axis=1)
    calc_df_data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, main_type, detail_type, hierarchy, months_ago, risk_type, index_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago,
                                     hierarchy, main_type, detail_type,
                                     risk_type, index_type=index_type)


def write_export_basic_data_to_mongo(data,
                                     months_ago,
                                     hierarchy,
                                     main_type,
                                     detail_type,
                                     risk_type=None,
                                     index_type=1):
    """将中间过程写入mongo，写之前需要删除之前的对应月份历史数据，避免冗余

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    index_type = 1 if index_type in [0, 1] else index_type
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}{coll_prefix}_index_basic_data'
    # 删除历史数据
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    if risk_type is not None:
        coll_name = f'{prefix}major_index_basic_data'
        r_condition.update({
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1])
        })
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


def format_export_basic_data(data,
                             main_type,
                             detail_type,
                             hierarchy,
                             months_ago,
                             risk_type=None,
                             index_type=0):
    data_rst = []
    mon = get_history_months(months_ago)[0]
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in data.iterrows():
        data_rst.append({
            'TYPE': 2,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'CONTENT': row['CONTENT'],
        })
    return data_rst


def export_basic_data_dicttype(data,
                               dpid_data,
                               main_type,
                               detail_type,
                               hierarchy,
                               months_ago,
                               func_html_desc,
                               risk_type=None):
    data = data.groupby(['TYPE3'])['COUNT'].sum()
    data = append_major_column_to_df(dpid_data, data.to_frame(name='CONTENT'))
    data.fillna(0, inplace=True)
    data['CONTENT'] = data['CONTENT'].apply(lambda x: func_html_desc(x))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def _combine_second_filed_to_str(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row.loc[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


# 整改成效用--row.loc[idx] 改为 int(row[idx]
def _combine_second_filed_to_str_two(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串,
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


def export_basic_data_tow_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        first_title,
                                        second_title,
                                        risk_type=None):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()
    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str(row, first_title, second_title),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


# 整改成效
def export_basic_data_tow_field_monthly_two(data,
                                            dpid_data,
                                            major_data,
                                            main_type,
                                            detail_type,
                                            hierarchy,
                                            months_ago,
                                            first_title,
                                            second_title,
                                            risk_type=None,
                                            columns_list=[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                                                          (2, 3), (3, 1), (3, 2), (3, 3)]):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()

    # 按顺序填充缺失的责任类型
    # columns_list = [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)]
    data_columns = data.columns.values.tolist()
    for idx, col in enumerate(columns_list):
        if col not in data_columns:
            data[col] = 0
            col_data = data.pop(col)
            data.insert(idx, col, col_data)

    # 填充站段数据
    data = pd.merge(
        data,
        major_data["DEPARTMENT_ID"].to_frame(),
        how="right",
        left_index=True,
        right_on="DEPARTMENT_ID")
    data.set_index('DEPARTMENT_ID', inplace=True)

    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str_two(row, first_title, second_title),
        axis=1)

    data = data['CONTENT'].to_frame("middle_1")
    #
    # data = append_major_column_to_df(
    #     major_data,
    #     pd.DataFrame(
    #         index=data.index,
    #         data=data.loc[:, 'CONTENT'].values,
    #         columns=['CONTENT']))
    # data_rst = format_export_basic_data(data, main_type, detail_type,
    #                                     hierarchy, months_ago, risk_type)
    # write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
    #                                  main_type, detail_type, risk_type)
    return data


def export_basic_data_one_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        func_html_desc,
                                        title=None,
                                        risk_type=None,
                                        index_type=0):
    if len(data) == 0:
        return
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    data['MONTH'] = data['MONTH'].apply(lambda x: func_html_desc(x))
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    if title is not None:
        title += '<br/>'
    else:
        title = ''
    data['CONTENT'] = data.apply(
        lambda row: title + '<br/>'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type, index_type=index_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type, index_type=index_type)


def _export_basic_data_ratio_type(
        df_data, dpid_data, main_type, detail_type, hierarchy, months_ago,
        col_numerator, col_denominator, col_quotient, col_score, risk_type, index_type=1):
    df_data = add_avg_number_by_major(df_data, dpid_data, col_quotient)
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
        ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0 if index_type == 1 else index_type
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_QUOTIENT': round(row['AVG_NUMBER'], 4),
            'AVG_SCORE': round(row['AVG_SCORE'], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type,
                                     index_type=index_type)


def _export_basic_data_ratio_type_major(
        df_data, dpid_data, main_type, detail_type, hierarchy, months_ago,
        col_numerator, col_denominator, col_quotient, col_score, risk_type, index_type=1):
    df_data = add_avg_number_by_major_two(df_data, dpid_data, col_quotient)
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
        ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0 if index_type == 1 else index_type
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_QUOTIENT': round(row['AVG_NUMBER'], 4),
            'AVG_SCORE': round(row['AVG_SCORE'], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type,
                                     index_type=index_type)


def export_basic_data(df_data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, col_quotient, col_avg,
                      col_score, risk_type, index_type=1):
    """将计算中间过程导出，包含分子、分母、专业均数、得分、排名

    Arguments:
        df_data {DataFrame} -- 数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
        col_numerator {float} -- 计算过程分子
        col_denominator {float} -- 计算过程分母
        col_quotient {float} -- 计算过程商
        col_avg {float} -- 专业均数
        col_score {float} -- 各单位得分
    """
    # 增加平均分一列
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_score].groupby(df_data['MAJOR']).rank(
        ascending=0, method='first')

    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0 if index_type == 1 else index_type
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_QUOTIENT': round(row[col_avg], 4),
            'AVG_SCORE': round(row['AVG_SCORE'], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type, index_type=index_type)


def add_avg_score_by_major(df, column, dpid_data=None):
    """给dataframe增加专业平均分-
    """
    if dpid_data is not None:
        df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_SCORE'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def add_avg_number_by_major(df, dpid_data, column):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def add_avg_number_by_major_two(df, dpid_data, column,
                                numerator='numerator',
                                denominator='denominator',
                                fraction=None):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    from app.data.health_index.cardinal_number_common import get_cardinal_number
    df = append_major_column_to_df(dpid_data, df)
    if fraction:
        # 与重点指数分开
        if int(fraction.risk_type[-1]) == 0:
            major_avg_score = get_cardinal_number(fraction)
    else:
        major_avg_score = df.groupby(['MAJOR'])[numerator].sum() / df.groupby(['MAJOR'])[denominator].sum()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def calc_extra_child_score_groupby_major(df,
                                         dpid_data,
                                         column,
                                         func_calc_score,
                                         weight=None,
                                         detail_type=None,
                                         base_column="AVG_NUMBER",
                                         major_ratio_dict={}
                                         ):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, base_column, detail_type, major_ratio_dict),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def calc_extra_child_score_groupby_major_two(df,
                                             dpid_data,
                                             column,
                                             func_calc_score,
                                             weight=None,
                                             detail_type=None,
                                             base_column="AVG_NUMBER",
                                             major_ratio_dict={},
                                             numerator='numerator',
                                             denominator='denominator',
                                             fraction=None):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_two(df, dpid_data, column,
        numerator=numerator, denominator=denominator, fraction=fraction)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, base_column, detail_type, major_ratio_dict),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def calc_score_groupby_major(df, dpid_data, hierarchy, main_type, detail_type,
                             col_numerator, col_denominator, column,
                             rtn_column, func_calc_score, months_ago,
                             risk_type, index_type=1):
    """基于各专业均数，计算各个部门得分

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        main_type {int} -- 指数大类[1~6]
        detail_type {str} -- [子指数小类]
        col_numerator {str} -- 分子列名
        col_denomiator {str} -- 分母列名
        column {str} -- 列名
        rtn_column {str} -- [返回的分值字段名]
        func_calc_score {func} -- 计算公式
        months_ago {int} -- 第前{-N}个月
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨）
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, "AVG_NUMBER", detail_type),
        axis=1)
    # 暂时先导出安全综合、调车指数中间过程
    if risk_type is None or risk_type == '车务-1':
        export_basic_data(data, main_type, detail_type, hierarchy, months_ago,
                          col_numerator, col_denominator, column, 'AVG_NUMBER',
                          'SCORE', risk_type, index_type)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[rtn_column])
    return rst_data


def calc_score_groupby_major_two(df, dpid_data, hierarchy, main_type, detail_type,
                                 col_numerator, col_denominator, column,
                                 rtn_column, func_calc_score, months_ago,
                                 risk_type, index_type=1, fraction=None):
    """基于各专业均数，计算各个部门得分

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        main_type {int} -- 指数大类[1~6]
        detail_type {str} -- [子指数小类]
        col_numerator {str} -- 分子列名
        col_denomiator {str} -- 分母列名
        column {str} -- 列名
        rtn_column {str} -- [返回的分值字段名]
        func_calc_score {func} -- 计算公式
        months_ago {int} -- 第前{-N}个月
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨）
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_two(df, dpid_data, column, fraction=fraction)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, "AVG_NUMBER", detail_type),
        axis=1)
    # 暂时先导出安全综合、调车指数中间过程
    if risk_type is None or risk_type == '车务-1':
        export_basic_data(data, main_type, detail_type, hierarchy, months_ago,
                          col_numerator, col_denominator, column, 'AVG_NUMBER',
                          'SCORE', risk_type, index_type)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[rtn_column])
    return rst_data


def combine_child_index_func(func_list, months_ago):
    """传入子指数计算的函数列表，将计算结果存放在list中

    Arguments:
        func_list {list[func]} -- [description]
        months_ago {int} -- 更新前第-N个月份，比如更新上个月也是-1，依次-2，,-3...]

    Returns:
        list[dataframe] -- 各个指数
    """
    # 存放所有子指数项的分数
    child_score = []
    for c_func in func_list:
        current_app.logger.debug(
            f'├── \'{c_func.__name__}\' index start computing!')
        func_result = c_func(months_ago)
        if func_result is not None:
            child_score.extend(func_result)
        current_app.logger.debug(
            f'├── \'{c_func.__name__}\' index has been figured out!')
    return child_score


def write_cardinal_number_basic_data(
    df_calc, fraction,
    main_type, detail_type, months_ago,
    columns=['numerator', 'denominator']):
    """[summary]
    将基数计算数据导入数据库中
    Arguments:
        fraction {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]
    """
    df_calc_cp = df_calc.copy()
    df_calc_cp.index.rename('TYPE3', inplace=True)
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}health_index_cardinal_number_basic_data'
    df_calc_cp = pd.merge(
                df_calc_cp,
                fraction.zhanduan_dpid_data,
                left_on='TYPE3',
                right_on='DEPARTMENT_ID',
                how='inner'
            )
    rst_data = []
    for idx, row in df_calc_cp.iterrows():
        _tmp_rst = {
            'HIERARCHY': 3,
            'MAJOR': row['MAJOR'],
            'INDEX_TYPE': int(fraction.risk_type.split('-')[1]),
            'MON': mon,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'DENOMINATOR':row[columns[1]],
            'DENOMINATOR_NAME': fraction.denominator.name,
            'DENOMINATOR_DESCRIPTION': fraction.denominator.description,
            'DENOMINATOR_VERSION': fraction.denominator.version,
            'NUMERATOR': row[columns[0]],
            'NUMERATOR_NAME': fraction.numerator.name,
            'NUMERATOR_DESCRIPTION': fraction.numerator.description,
            'NUMERATOR_VERSION': fraction.numerator.version,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME']
        }
        rst_data.append(
            _tmp_rst
        )
    mongo.db[coll_name].remove({
        'HIERARCHY': 3,
        'INDEX_TYPE': int(fraction.risk_type.split('-')[1]),
        'MON': mon,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type,
        'DENOMINATOR_NAME': fraction.denominator.name,
        'DENOMINATOR_VERSION': fraction.denominator.version,
        'NUMERATOR_NAME': fraction.numerator.name,
        'NUMERATOR_VERSION': fraction.numerator.version,
    })
    write_bulk_mongo(coll_name, rst_data)


if __name__ == '__main__':
    pass
