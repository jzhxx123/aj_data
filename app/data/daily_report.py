import datetime
import json

#!/usr/bin/python3
# -*- coding: utf-8 -*-
#from line_profiler import LineProfiler
import numpy as np
import pandas as pd
from flask import current_app, jsonify, request

from app import mongo
from app.data.util import (get_previous_month, paste_daily_to_monthly,
                           paste_monthly_to_history, pd_query,
                           write_bulk_mongo)


def get_safe_baseInfo():

    sql = '''
        SELECT 
            DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m-%%d') AS date,
            COUNT(*) AS count,
            MAIN_TYPE AS main_type,
            DETAIL_TYPE AS detail_type,
            NAME AS name
        FROM
            t_safety_produce_info
        WHERE
            MAIN_TYPE in (1,2)
        GROUP BY date , MAIN_TYPE , DETAIL_TYPE , NAME;
    '''
    safe_data = pd_query(sql)

    current_app.logger.debug(safe_data)
    return safe_data


def get_daily_safe():
    date_sql = '''
        select distinct DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m-%%d') AS date from t_safety_produce_info
    '''
    date = pd_query(date_sql)
    days = date['date'].tolist()
    dailyReport = []
    id = 0
    for day in days:
        id += 1
        #  运输安全基本情况信息
        # 运输安全基本情况title
        total = "截至3月21日：全局实现无责任一般A类事故？天。至2018年3月21日，全局实现安全生产1100天。"

        # 安全情况基本日信息
        date = day[5:7] + '月' + day[8:] + '日'
        detail = []
        day_sql = '''SELECT 
                DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m-%%d') AS date,
                COUNT(*) AS count,
                MAIN_TYPE AS main_type,
                DETAIL_TYPE AS detail_type,
                NAME AS name
            FROM
                t_safety_produce_info
            WHERE
                MAIN_TYPE IN (1,2) AND DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m-%%d') = '{}'
            GROUP BY date , MAIN_TYPE , DETAIL_TYPE , NAME;
            '''.format(day)
        safe_day_data = pd_query(day_sql)
        day_detail = get_safe_situation(safe_day_data, date)

        # 安全情况基本周信息
        week_sql = '''SELECT 
                COUNT(*) AS count,
                MAIN_TYPE AS main_type,
                DETAIL_TYPE AS detail_type,
                NAME AS name
            FROM
                t_safety_produce_info
            WHERE
                MAIN_TYPE IN (1 , 2)
                    AND OCCURRENCE_TIME BETWEEN DATE_SUB('{}',
                    INTERVAL 7 DAY) AND '{}'
            GROUP BY  MAIN_TYPE , DETAIL_TYPE , NAME;
             '''.format(day, day)
        safe_week_data = pd_query(week_sql)
        nDay = datetime.datetime.strptime(day, '%Y-%m-%d')
        wDay = datetime.timedelta(days=7)
        week_day = (nDay - wDay).strftime('%Y-%m-%d')
        week_date = week_day[5:7] + '月' + week_day[8:] + '日-' + day[
            5:7] + '月' + day[8:] + '日'
        week_detail = get_safe_situation(safe_week_data, week_date)

        # 安全情况基本月信息
        month_sql = '''SELECT 
                COUNT(*) AS count,
                DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m') AS date,
                MAIN_TYPE AS main_type,
                DETAIL_TYPE AS detail_type,
                NAME AS name
            FROM
                t_safety_produce_info
            WHERE
                MAIN_TYPE IN (1 , 2) AND DATE_FORMAT(OCCURRENCE_TIME, '%%Y-%%m') = '{}'
            GROUP BY date , MAIN_TYPE , DETAIL_TYPE , NAME;
            '''.format(day[:7])
        month_data = pd_query(month_sql)
        month_date = day[5:7] + "月份"
        month_detail = get_safe_situation(month_data, month_date)
        detail = [day_detail, week_detail, month_detail]
        transport = {
            "safe": {
                "total": total,
                "detail": detail
            },
            "accidentInfo": "",
            "faultInfo": "",
            "compositeInfo": ""
        }
        # 监督检查基本情况
        # 全局检查情况/重点问题信息
        # 获取当日检查总数
        check_total_sql = '''SELECT 
                COUNT(*) AS count
            FROM
                t_check_info
            WHERE
                DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d') = '{}'
            '''.format(day)
        check_total = pd_query(check_total_sql)
        checkTotal = check_total.loc[0].at['count']

        # 获取当日检查总数
        check_total_sql = '''SELECT 
                COUNT(*) AS count
            FROM
                t_check_info
            WHERE
                DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d') = '{}'
            '''.format(day)
        check_total = pd_query(check_total_sql)
        checkTotal = check_total.loc[0].at['count']

        # 获取当日发现问题总数
        problem_total_sql = '''SELECT 
                COUNT(*) AS count
            FROM
                t_check_problem
            WHERE
                DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d') = '{}'
            '''.format(day)
        problem_total = pd_query(problem_total_sql)
        problemTotal = problem_total.loc[0].at['count']

        # 发现问题详细数量
        problem_sql = '''SELECT 
                DATE_FORMAT(a.SUBMIT_TIME, '%%Y%%u') AS weeks,
                a.PROBLEM_CLASSITY_NAME,
                COUNT(a.PROBLEM_CLASSITY_NAME) AS count
            FROM
                t_check_problem AS a
                    INNER JOIN
                t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            WHERE
                b.RISK_LEVEL IN (1 , 2)
                    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y%%u') = DATE_FORMAT('{}', '%%Y%%u')
            GROUP BY weeks , PROBLEM_CLASSITY_NAME;
        '''.format(week_day)
        week_problem_data = pd_query(problem_sql)
        problem_count = week_problem_data['count'].sum()
        work_count = 0
        equ_count = 0
        manage_count = 0
        for index in week_problem_data.index:
            if '作业' in week_problem_data.loc[index].at[
                    'PROBLEM_CLASSITY_NAME']:
                work_count += week_problem_data.loc[index].at['count']
            elif '设备' in week_problem_data.loc[index].at[
                    'PROBLEM_CLASSITY_NAME']:
                equ_count += week_problem_data.loc[index].at['count']
            elif '管理' in week_problem_data.loc[index].at[
                    'PROBLEM_CLASSITY_NAME']:
                manage_count += week_problem_data.loc[index].at['count']

        # 红线问题信息
        red_sql = '''SELECT 
                DATE_FORMAT(a.SUBMIT_TIME, '%%Y%%u') AS weeks,
                COUNT(a.DESCRIPTION) AS count,
                a.IS_RED_LINE AS red,
                GROUP_CONCAT(a.DESCRIPTION) AS description
            FROM
                t_check_problem AS a
                    INNER JOIN
                t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            WHERE
                b.RISK_LEVEL IN (1 , 2)
                    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y%%u') = DATE_FORMAT('{}', '%%Y%%u')
            GROUP BY weeks , IS_RED_LINE
        '''.format(week_day)
        red_data = pd_query(red_sql)
        # current_app.logger.debug(red_data)
        for index in red_data.index:
            if red_data.loc[index].at['red'] == 1:
                road_red = red_data.loc[index].at['count']
                road_red_info = red_data.loc[index].at['description']
            elif red_data.loc[index].at['red'] == 2:
                satition_red = red_data.loc[index].at['count']

        basicInfo = "上周集团公司下现场检查{}人次，发现问题{}个。性质严重问题{}个，其中作业项{}个、设备设施项{}个、管理项{}个。路局查处红线问题{}个、站段查处红线问题{}个".format(
            checkTotal, problemTotal, problem_count, work_count, equ_count,
            manage_count, road_red, satition_red)
        punishInfo = road_red_info

        # 风险问题信息
        risk_sql = '''SELECT 
                COUNT(DISTINCT b.RISK_TYPE_NAME) AS count,
                GROUP_CONCAT(DISTINCT b.RISK_TYPE_NAME) AS risk_name
            FROM
                t_check_problem AS a
                    INNER JOIN
                t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            WHERE
                b.RISK_LEVEL IN (1 , 2)
                    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') = '{}'
        '''.format(day)
        risk_data = pd_query(risk_sql)
        risk_count = risk_data.loc[0].at['count']
        risk_info = risk_data.loc[0].at['risk_name']

        riskInfo = "今日检查风险{}大类，分别是：{}等主要风险数及严重问题数。".format(
            risk_count, risk_info)

        # 全局检查情况/安全分析中心工作基本情况
        # 工作量情况
        work_sql_a = '''SELECT 
                a.FLAG AS flag, SUM(a.DAILY_COMPLETE) AS count,
                SUM(a.DAILY_PROBLEM_NUMBER) AS issus_count
            FROM
                t_analysis_center_daily_details AS a
                    INNER JOIN
                t_analysis_center_daily AS b ON b.PK_ID = a.FK_ANALYSIS_CENTER_DAILY_ID
            WHERE
                DATE_FORMAT(b.DATE, '%%Y-%%m-%%d') = '{}'
            GROUP BY a.FLAG;
            '''.format(day)
        work_statis_a = pd_query(work_sql_a)
        read_times = 0
        evaluate_times = 0
        read_count = 0
        read_re_count = 0
        read_times = 0
        evaluate_count = 0
        evaluate_people_count = 0
        evaluate_times = 0
        evaluate_re_count = 0
        evaluate_people_re_count = 0
        stage_count = 0
        for index in work_statis_a.index:
            if work_statis_a.loc[index].at['flag'] == 1:
                read_count = work_statis_a.loc[index].at['count']
            elif work_statis_a.loc[index].at['flag'] == 2:
                read_re_count = work_statis_a.loc[index].at['count']
                read_times += work_statis_a.loc[index].at['issus_count']
            elif work_statis_a.loc[index].at['flag'] == 3:
                evaluate_count = work_statis_a.loc[index].at['count']
            elif work_statis_a.loc[index].at['flag'] == 4:
                evaluate_people_count = work_statis_a.loc[index].at['count']
            elif work_statis_a.loc[index].at['flag'] == 5:
                evaluate_times += work_statis_a.loc[index].at['issus_count']
                evaluate_re_count = work_statis_a.loc[index].at['count']
            elif work_statis_a.loc[index].at['flag'] == 6:
                evaluate_times += work_statis_a.loc[index].at['issus_count']
                evaluate_people_re_count = work_statis_a.loc[index].at['count']
            elif work_statis_a.loc[index].at['flag'] == 7:
                stage_count = work_statis_a.loc[index].at['count']

        work_sql_b = '''SELECT
                GROUP_CONCAT(DISTINCT CENTER_PROBLEM) AS problem    
            FROM
                t_analysis_center_daily
            WHERE
                DATE_FORMAT(DATE, '%%Y-%%m-%%d') = '{}'
            '''.format(day)
        work_statis_b = pd_query(work_sql_b)
        center_problem = work_statis_b.loc[0].at['problem']
        workInfo = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{}小时，干部履职评价{}条，干部履职评价{}人次，复查履职评价{}条，干部履职复查{}人次，阶段评价{}人次。 问题查处情况：音视频调阅（复查）发现问题{}个，干部履职评价（复查）发现{}个问题'.format(
            read_re_count, evaluate_count, evaluate_people_count,
            evaluate_re_count, evaluate_people_re_count, stage_count,
            read_times, evaluate_times)
        analyzeMainInfo = center_problem
        daily_report = {
            "ID": id,
            "TITLE": "中国铁路成都局集团有限公司运输安全生产日报表",
            "DATE": day,
            "TYPE": "SAFETY",
            "VALUE": {
                "transport": transport,
                "check": {
                    "total": "总公司、特派办等发现的问题（按问题性质严重度显示前5条）",
                    "wholeCheck": {
                        "basicInfo": basicInfo,
                        "punishInfo": punishInfo,
                        "riskInfo": riskInfo
                    },
                    "safe": {
                        "workInfo": workInfo,
                        "punishModelInfo": "问题查处数量，安全分析中心查处的较大及重大风险问题罗列",
                        "analyzeMainInfo": analyzeMainInfo
                    }
                },
                "safeTrack": {
                    "main": [],
                    "part": []
                },
                "safeWarn": {
                    "headquartersWarn": "",
                    "branchWarn": "",
                    "SystemPredictWarn": "处室级内容"
                },
                "leaderGuide": ""
            }
        }
        dailyReport.append(daily_report)
# for index in result.index:
# 	value = result.loc[index]
# 	rs.append(json.loads(value.to_json()))
# 	# current_app.logger.debug("e")
# current_app.logger.debug('complete: format data.')
    mongo.db.daily_report_V1.drop()
    write_bulk_mongo('daily_report_V1', dailyReport)
    # return transport


# 安全信息拼接函数
def get_safe_situation(safe_data, date):
    drive_times = 0
    road_times = 0
    work_times = 0
    accident = 0
    fault = 0
    equ_fault = 0
    fault_all = ''
    accident_info = ''
    for index in safe_data.index:
        if safe_data.loc[index].at['main_type'] == 2:
            fault += safe_data.loc[index].at['count']
            if safe_data.loc[index].at['name'] != None:
                equ_fault += safe_data.loc[index].at['count']
                fault_all = fault_all + ',' + safe_data.loc[index].at[
                    'name'] + str(safe_data.loc[index].at['count']) + '件'
        elif safe_data.loc[index].at['main_type'] == 1:
            accident += safe_data.loc[index].at['count']
            if safe_data.loc[index].at['detail_type'] == 1:
                accident_info = accident_info + ',行车安全事故：' + str(
                    safe_data.loc[index].at['name'])
                drive_times += safe_data.loc[index].at['count']
            elif safe_data.loc[index].at['detail_type'] == 2:
                accident_info = accident_info + ',劳动安全事故：' + str(
                    safe_data.loc[index].at['name'])
                work_times += safe_data.loc[index].at['count']
            elif safe_data.loc[index].at['detail_type'] == 3:
                accident_info = accident_info + ',路外安全事故：' + str(
                    safe_data.loc[index].at['name'])
                road_times += safe_data.loc[index].at['count']
    if equ_fault == 0:
        detail = '''{}，发生事故{}件、故障{}件。<br>1.行车安全：发生事故{}次。
            <br>2.劳动安全：发生事故{}次。<br>3.路外安全：发生事故{}次。<br>4.设备故障0件。
            '''.format(date, accident, fault, drive_times, work_times,
                       road_times)
    else:
        detail = '''{}，发生事故{}件、故障{}件。<br>1.行车安全：发生事故{}次。
            <br>2.劳动安全：发生事故{}次。<br>3.路外安全：发生事故{}次。<br>4.设备故障{}件：其中{}。
            '''.format(date, accident, fault, drive_times, work_times,
                       road_times, equ_fault, fault_all)
    return detail
