#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    警告通知书相关数据
'''

import json

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper


def get_check_end_date(start, period):
    if period == 1:
        return int(str(start + relativedelta(weeks=1)).replace("-", ""))
    elif period == 2:
        return int(str(start + relativedelta(days=15)).replace("-", ""))
    elif period == 3:
        return int(str(start + relativedelta(months=1)).replace("-", ""))
    else:
        return 0


def get_important_end_date(start, period):
    if period == 1:
        return int(str(start + relativedelta(weeks=1)).replace("-", ""))
    elif period == 2:
        return int(str(start + relativedelta(months=1)).replace("-", ""))
    elif period == 3:
        return int(str(start + relativedelta(months=3)).replace("-", ""))
    elif period == 4:
        return int(str(start + relativedelta(months=6)).replace("-", ""))
    elif period == 5:
        return int(str(start + relativedelta(months=12)).replace("-", ""))
    else:
        return 0


def get_warn_type_name(type_int):
    if type_int == 1:
        return "安全事故警告预警"
    elif type_int == 2:
        return "差异化精准警告预警"
    elif type_int == 3:
        return "劳动安全专项警告预警"
    elif type_int == 4:
        return "其他警告预警"
    else:
        return "无"


def get_data(data, TYPE):
    if TYPE == 1:  # 安全警告
        data['START_DATE'] = data['CREATE_TIME'].apply(
            lambda x: int(str(x)[:10].replace('-', '')))
        data['MON'] = mon
        data['END_DATE'] = data.apply(lambda x: int(str(
            x['CREATE_TIME'] + relativedelta(days=x['WARNING_DAYS'])
        )[:10].replace('-', '')), axis=1)
        del data['CREATE_TIME'], data['WARNING_DAYS']
    elif TYPE == 2:  # 安全提示
        data['START_DATE'] = data['DATE'].apply(
            lambda x: int(str(x)[:10].replace('-', '')))
        data['END_DATE'] = data['DATE'].apply(
            lambda x: int(
                str(x + relativedelta(days=7))[:10].replace('-', '')))
        data['MON'] = mon
        del data['DATE']
    elif TYPE == 3:  # 自动提示
        data['START_DATE'] = data['WARN_START_DATE'].apply(
            lambda x: int(str(x)[:10].replace('-', '')))
        data['MON'] = mon
        data = data.rename(index=str, columns={'DEPARTMENT': 'NAME'})
        del data['WARN_START_DATE'], data['PERIOD']
    data['TYPE'] = TYPE
    result = []
    for idx in data.index:
        val = data.loc[idx]
        result.append(json.loads(val.to_json()))
    current_app.logger.debug('warn_notifcation_complete: format data.')
    coll_name = '{}warn_notification'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].delete_many({'TYPE': TYPE})
    else:
        mongo.db[coll_name].delete_many({
            'MON': mon,
            'TYPE': TYPE
        })
    write_bulk_mongo(coll_name, result)


def load_safe_warn_data(mon_point):
    sql = '''SELECT
            a.PK_ID,
            a.DUTY_DEPARTMENT_NAME AS DUTY_NAME,
            a.DEPARTMENT_NAME AS CREATE_NAME,
            a.WARNING_DAYS,
            a.CREATE_TIME,
            a.CONTENT,STATUS,
            a.PROFESSION AS MAJOR,
            a.HIERARCHY,
            a.RANK,
            a.SHORT_DESCRIPTION,
            a.TYPE AS WARN_TYPE
        FROM
            t_warning_notification as a
        WHERE
            CREATE_TIME >= '{}' AND CREATE_TIME < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    data = data[data['STATUS'] != 1]
    data = data[data['STATUS'] != 8]
    data = data[data['STATUS'] != 9]
    data = data[data['CONTENT'] != '测试']
    data['WARN_TYPE'] = data['WARN_TYPE'].apply(get_warn_type_name)
    del data['STATUS']
    return data


def load_safe_notice_data(mon_point):
    report_sql = '''SELECT
            a.RISK_WARNING AS CONTENT, a.DATE, b.NAME AS MAJOR
        FROM
            t_analysis_center_daily AS a
                LEFT JOIN
            t_department AS b ON b.DEPARTMENT_ID = a.PROFESSION_ID
        WHERE
            a.STATUS = 2 AND
            DATE >= '{}' AND DATE < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(report_sql)
    data = data.fillna(0)
    data = data[data['CONTENT'] != 0]
    data = data[data['CONTENT'] != ""]
    data = data[data['CONTENT'] != "无"]
    return data


def get_risk_content(x, r, o, y):
    if r is not None and x > r:
        return "红色预警"
    elif o is not None and r is not None and x > o and x < r:
        return "橙色预警"
    else:
        return "黄色预警"


def load_safe_auto_notice_data(mon_point):
    # 检查问题提示预警
    problem_sql = '''SELECT
        a.PK_ID,
        a.TYPE,
        a.PERIOD,
        a.WARN_START_DATE,
        b.ALL_NAME AS DEPARTMENT,
        c.ALL_NAME AS RISK,
        e.NAME AS MAJOR
    FROM
        t_warning_check_problem AS a
            LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            LEFT JOIN
        t_risk AS c ON c.PK_ID = a.FK_RISK_ID
            LEFT JOIN
        t_warning_problem_prompt_config as d ON d.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_department as e on e.DEPARTMENT_ID = b.TYPE2
    WHERE
        WARN_START_DATE >= '{}' AND WARN_START_DATE < '{}' AND d.STATUS = 2;
    '''.format(mon_point[0], mon_point[1])
    problem_data = pd_query(problem_sql)
    problem_data = problem_data.fillna(0)
    if problem_data.empty is False:
        problem_data['CONTENT'] = problem_data.apply(
            lambda x: x['RISK'] + '问题个数' + '超过上限' if x[
                'TYPE'] == 1 else x['RISK'] + '问题个数' + '未达下限',
            axis=1)
        problem_data['DEPARTMENT'] = problem_data['DEPARTMENT'].apply(
            lambda x: x.split("-")[0])
        problem_data['END_DATE'] = problem_data.apply(
            lambda x: get_check_end_date(x['WARN_START_DATE'], x['PERIOD']),
            axis=1)
        del problem_data['TYPE'], problem_data['RISK']

    # 检查信息提示预警
    check_sql = '''SELECT
        a.PK_ID,
        a.WARN_START_DATE,
        a.PERIOD,
        a.CHECK_ITEM_NAME,
        a.NUMBER,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR
    FROM
        t_warning_check_info_ywc AS a
            LEFT JOIN
        t_warning_check_prompt_office_config AS b
                ON b.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_department AS d ON b.EXECUTE_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e on e.DEPARTMENT_ID = d.TYPE2
    WHERE
        WARN_START_DATE >= '{}' AND WARN_START_DATE < '{}' AND b.STATUS = 2;
    '''.format(mon_point[0], mon_point[1])
    check_data = pd_query(check_sql)
    check_data = check_data.fillna(0)
    if check_data.empty is False:
        check_data['CONTENT'] = check_data.apply(
            lambda x: x['CHECK_ITEM_NAME'] + '检查次数不足{}次'.format(x['NUMBER']),
            axis=1)
        check_data['DEPARTMENT'] = check_data['DEPARTMENT'].apply(
            lambda x: x.split("-")[0])
        check_data['END_DATE'] = check_data.apply(
            lambda x: get_check_end_date(x['WARN_START_DATE'], x['PERIOD']),
            axis=1)
        del check_data['CHECK_ITEM_NAME'], check_data['NUMBER']

    # 风险预警
    risk_sql = '''SELECT
        a.PK_ID,
        a.WARN_START_DATE,
        a.PERIOD,
        a.RISK_NAME,
        a.NUMBER,
        a.YELLOW,
        a.RISK_TYPE,
        a.ORANGE,
        a.RED,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR
    FROM
        t_warning_risk AS a
            LEFT JOIN
        t_warning_risk_prompt_config AS b ON b.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_department AS d ON a.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e on e.DEPARTMENT_ID = d.TYPE2
    WHERE
        WARN_START_DATE >= '{}' AND WARN_START_DATE < '{}' AND b.STATUS = 2;
    '''.format(mon_point[0], mon_point[1])
    risk_data = pd_query(risk_sql)
    risk_data = risk_data.fillna(0)
    if check_data.empty is False:
        risk_data['risk'] = risk_data['RISK_TYPE'].apply(
            lambda x: "风险类型" if int(x) == 1 else "风险后果")
        risk_data['DEPARTMENT'] = risk_data['DEPARTMENT'].apply(
            lambda x: x.split("-")[0])
        risk_data['CONTENT'] = risk_data.apply(
            lambda x: '{}{}为{}达到{}'.format(
                x['DEPARTMENT'], x['risk'], x['RISK_NAME'],
                get_risk_content(
                    x['NUMBER'], x['RED'], x['YELLOW'], x['ORANGE'])), axis=1)
        risk_data['END_DATE'] = risk_data.apply(
            lambda x: get_check_end_date(x['WARN_START_DATE'], x['PERIOD']),
            axis=1)
        del risk_data['RISK_TYPE'], risk_data['risk'], risk_data[
            'RISK_NAME'], risk_data['NUMBER'], risk_data['RED'], risk_data[
                'YELLOW'], risk_data['ORANGE']

    # 重点问题预警提示
    important_sql = '''SELECT
        a.PK_ID,
        a.WARN_START_DATE,
        a.PERIOD,
        a.HIERARCHY,
        a.CURRENT_NUMBER,
        a.YELLOW,
        a.ORANGE,
        a.RED,
        b.PROBLEM_POINT,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR
    FROM
        t_important_problem_prompt AS a
            LEFT JOIN
        t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_important_problem_config AS c
            ON c.PK_ID = a.FK_IMPORTANT_PROBLEM_CONFIG_ID
            LEFT JOIN
        t_department AS d ON a.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e on e.DEPARTMENT_ID = d.TYPE2
    WHERE
        WARN_START_DATE >= '{}' AND WARN_START_DATE < '{}' AND c.STATUS = 2;
    '''.format(mon_point[0], mon_point[1])
    important_data = pd_query(important_sql)
    important_data = important_data.drop_duplicates('PK_ID')
    important_data = important_data.fillna(0)
    if important_data.empty is False:
        important_data['DEPARTMENT'] = important_data['DEPARTMENT'].apply(
            lambda x: x.split("-")[0])
        important_data['CONTENT'] = important_data.apply(
            lambda x: '{}问题项点为{}达到{}'.format(
                x['DEPARTMENT'], x['PROBLEM_POINT'],
                get_risk_content(
                    x['CURRENT_NUMBER'], x['RED'], x['YELLOW'], x['ORANGE'])),
            axis=1)
        important_data['END_DATE'] = important_data.apply(
            lambda x: get_important_end_date(
                x['WARN_START_DATE'], x['PERIOD']),
            axis=1)
        important_data = important_data.drop_duplicates('CONTENT')
        del important_data['CURRENT_NUMBER'], important_data[
            'PROBLEM_POINT'], important_data['RED'], important_data[
                'YELLOW'], important_data['ORANGE']

    # 人员预警提示
    person_sql = '''SELECT
        a.PK_ID,
        a.PERSON_NAME,
        a.WARN_START_DATE,
        b.PERIOD,
        b.TYPE AS HIERARCHY,
        a.DEDUCT_SCORE,
        b.WARNING_VALUE,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR
    FROM
        t_warning_key_person AS a
            LEFT JOIN
        t_warning_key_person_config AS b ON b.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e ON e.DEPARTMENT_ID = d.TYPE2
    WHERE WARN_START_DATE >= '{}' AND WARN_START_DATE < '{}' AND b.STATUS =2;
    '''.format(mon_point[0], mon_point[1])
    person_data = pd_query(person_sql)
    if person_data.empty is False:
        person_data['DEPARTMENT'] = person_data['DEPARTMENT'].apply(
            lambda x: x.split("-")[0])
        person_data['CONTENT'] = person_data.apply(
            lambda x: '{}的责任扣分为{}，达到配置预警值{}'.format(
                x['PERSON_NAME'], x['DEDUCT_SCORE'], x['WARNING_VALUE']),
            axis=1)
        person_data['END_DATE'] = person_data.apply(
            lambda x: get_important_end_date(
                x['WARN_START_DATE'], x['PERIOD']),
            axis=1)
    del person_data['PERSON_NAME'], person_data['DEDUCT_SCORE'], person_data[
        'WARNING_VALUE']
    data = pd.concat([check_data, problem_data, risk_data, important_data],
                     ignore_index=True, sort=False)
    return data


def handle(mon_point):
    safe_warn_data = load_safe_warn_data(mon_point)
    safe_notice_data = load_safe_notice_data(mon_point)
    auto_notice_data = load_safe_auto_notice_data(mon_point)
    if safe_warn_data.iloc[:, 0].size != 0:
        get_data(safe_warn_data.copy(), 1)
    else:
        current_app.logger.debug('safe_warn_daily report fetch 0 record.')
    if safe_notice_data.iloc[:, 0].size != 0:
        get_data(safe_notice_data.copy(), 2)
    else:
        current_app.logger.debug('safe_notice_daily report fetch 0 record.')
    if auto_notice_data.iloc[:, 0].size != 0:
        get_data(auto_notice_data.copy(), 3)
    else:
        current_app.logger.debug('auto_notice_daily report fetch 0 record.')


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
