#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    月考核量化指标详细数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import (get_coll_prefix, get_history_months,
                           get_mon_from_date, get_previous_month, pd_query)


def load_assess_month_data(mon_point):
    sql = '''SELECT
            a.PK_ID,
            a.ID_CARD,
            a.PERSON_NAME,
            a.ALL_NAME,
            a.REFINEMENT_QUOTA_NUMBER,
            a.REFINEMENT_QUOTA_COUNT,
            a.MIN_QUALITY_GRADES_COUNT,
            b.SUBMIT_TIME,
            d.NAME AS MAJOR
        FROM
            t_safety_assess_month_quantify_detail AS a
                LEFT JOIN
            t_safety_assess_month AS b ON b.PK_ID = a.FK_SAFETY_ASSESS_MONTH_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
        WHERE
            SUBMIT_TIME >= '{}' AND SUBMIT_TIME < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    data['DATE'] = data['SUBMIT_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = data['SUBMIT_TIME'].apply(get_mon_from_date)
    data = data.fillna(0)
    data['station'] = data['ALL_NAME'].apply(lambda x: x.split("-")[0])
    del data['SUBMIT_TIME']
    current_app.logger.debug('complete: format data.')
    prefix = get_coll_prefix(months_ago)
    coll_name = '{}assess_month_quality'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        # current_app.logger.debug(
            # "目前日期判断有错，日后需要更改，错在get_history_months")
        mongo.db[coll_name].delete_many({'MON': get_history_months(months_ago)[0]})
    mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())


def handle(mon_point):
    current_app.logger.debug(
        'handle month-{} data[assess_month_quality]'.format(mon_point[0]))
    month_data = load_assess_month_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug(
            '{}assess_month_quality report fetch 0 record.'.format(mon_point))


def execute(months):
    months *= -1
    global months_ago
    months_ago = months
    mon_point = get_previous_month(months)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
