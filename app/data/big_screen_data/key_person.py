#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    月考核量化指标详细数据
'''

import json

import pandas as pd
from flask import current_app

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper


def official_get_status_names(status):
    if status == 0:
        return "待处置"
    elif status == 1:
        return "已处置"
    else:
        return "正式"


def pre_get_status_names(status):
    if status == 0:
        return "待确认"
    elif status == 1:
        return "已确认"
    else:
        return "预选"


def load_key_person_month_data(mon_point):
    # 正式重点人员库
    official_sql = '''SELECT
        a.PK_ID,
        a.PERSON_NAME,
        a.STATUS,
        a.DISPOSE_METHOD_NAME,
        b.PERIOD,
        b.TYPE AS HIERARCHY,
        b.IS_MAIN,
        b.IS_EXTERNAL,
        b.DEDUCT_SCORE,
        b.WARNING_VALUE,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR,
        a.MONTH
    FROM
        t_warning_key_person_library_official AS a
            LEFT JOIN
        t_warning_key_person_config AS b ON b.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_person AS c ON c.ID_CARD = a.ID_CARD
            LEFT JOIN
        t_department AS d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e ON e.DEPARTMENT_ID = d.TYPE2
    WHERE
        b.STATUS = 2 AND a.YEAR = {0} AND 
        a.MONTH BETWEEN 1 AND {1};
        '''.format(mon_point[0], mon_point[1])
    official_data = pd_query(official_sql)
    official_data['STATUS_NAME'] = official_data['STATUS'].apply(
        official_get_status_names)
    # 预选重点人员
    pre_sql = '''SELECT
        a.PK_ID,
        a.PERSON_NAME,
        a.STATUS,
        b.PERIOD,
        b.TYPE AS HIERARCHY,
        b.IS_MAIN,
        b.IS_EXTERNAL,
        b.DEDUCT_SCORE,
        b.WARNING_VALUE,
        d.ALL_NAME AS DEPARTMENT,
        e.NAME AS MAJOR,
        a.MONTH
    FROM
        t_warning_key_person_library AS a
            LEFT JOIN
        t_warning_key_person_config AS b ON b.PK_ID = a.FK_CONFIG_ID
            LEFT JOIN
        t_person AS c ON c.ID_CARD = a.ID_CARD
            LEFT JOIN
        t_department AS d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            LEFT JOIN
        t_department AS e ON e.DEPARTMENT_ID = d.TYPE2
    WHERE
        b.STATUS = 2 AND a.YEAR = {0}  AND 
        a.MONTH  BETWEEN 1 AND {1};
        '''.format(mon_point[0], mon_point[1])
    pre_data = pd_query(pre_sql)
    pre_data['STATUS_NAME'] = pre_data['STATUS'].apply(
        pre_get_status_names)
    data = pd.concat([official_data, pre_data],
                     axis=0, sort=False).reset_index()
    # data = data.fillna("a"),
    # 增加截止到目前的所有关键人员处置方式的统计
    total_data_dic = _get_current_total_data(data.copy())
    data = data[data['MONTH'] == mon_point[1]]
    data.drop(['MONTH'], inplace=True, axis=1)
    total_data_dic['MON'] = int(mon)
    return data, total_data_dic


def _get_current_total_data(data):
    """[summary]
    统计截止到目前月份各专业的重点人员违章信息
    Arguments:
        data {[type]} -- [description]
    """
    data = data.drop_duplicates(['PERSON_NAME', 'DISPOSE_METHOD_NAME'])
    data = data.fillna('a')
    majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
    dai_total = 0
    pei_total = 0
    gang_total = 0
    bang_total = 0
    he_total = 0
    result = []
    for major in majors:
        tr_rst = [major]
        major_data = data[data['MAJOR'] == major]
        dai = len(major_data[major_data['STATUS_NAME'] == "待处置"]) + \
            len(major_data[major_data['STATUS_NAME'] == "待确认"])
        pei = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
            '培训', regex=False)])
        gang = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
            '岗', regex=False)])
        bang = len(major_data[major_data.DISPOSE_METHOD_NAME.str.contains(
            '帮促', regex=False)])
        # he = len(major_data[major_data['STATUS_NAME'] == "待确认"]) + \
        #     len(major_data[~ major_data.DISPOSE_METHOD_NAME.str.contains(
        #         "前期已处置", regex=False)]) - len(major_data[
        #             major_data.DISPOSE_METHOD_NAME.str.contains(
        #                 "a", regex=False)])
        he = dai + pei + gang + bang 
        tr_rst.append(dai)
        tr_rst.append(pei)
        tr_rst.append(gang)
        tr_rst.append(bang)
        tr_rst.append(he)
        dai_total += dai
        pei_total += pei
        gang_total += gang
        bang_total += bang
        he_total += he
        result.append(tr_rst)
    result.append(
        ["合计", dai_total, pei_total, gang_total, bang_total, he_total])
    title = ["", '待处置', '培训', '待岗、离岗、转岗', '帮促', '合计']
    return {
        'DATA': result, 
        'TITLE': title,
    }


def get_data(data, total_data):
    result = []
    result.append(total_data)
    coll_name = '{}key_person'.format(prefix)
    if len(data) == 0:
        current_app.logger.debug("{}无重点人员违章数据".format(mon))
    else:
        data['MON'] = mon
        for idx in data.index:
            val = data.loc[idx]
            result.append(json.loads(val.to_json()))
        current_app.logger.debug('key_person_complete: format data.')
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({
            'MON': int(mon)
        })
    write_bulk_mongo(coll_name, result)


def handle(mon_point):
    month_data, total_data = load_key_person_month_data(mon_point)
    if month_data.iloc[:, 0].size != 0 or total_data:
        get_data(month_data.copy(), total_data)
    else:
        current_app.logger.debug(
            'key_person report fetch 0 record.')


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    year = months.end_date.year
    month = months.end_date.month
    mon_point = [year, month]
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
