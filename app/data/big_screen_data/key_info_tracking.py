#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    重点工作督办数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper


def load_assess_month_data(mon_point):
    sql = '''SELECT
        PK_ID,
        SURVEY_DEPARTMENT_NAMES,
        SURVEY_PERSON_NAMES,
        HIERARCHY,
        SURVEY_PERSON_ID_CARDS,
        TITLE,
        CONTENT,
        RESULT,
        CREATE_TIME
    FROM
        t_key_information_tracking
    WHERE
        CREATE_TIME >= '{}' AND CREATE_TIME < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("{}无重点信息数据".format(mon))
    else:
        data['MON'] = data['CREATE_TIME'].apply(
            lambda x: int(str(x)[:7].replace('-', '')))
        data['DATE'] = data['CREATE_TIME'].apply(
            lambda x: str(x)[:10])
        data = data.fillna(0)
        result = []
        for idx in data.index:
            val = data.loc[idx]
            result.append(json.loads(val.to_json()))
        current_app.logger.debug('key_info_track_complete: format data.')
        coll_name = '{}key_info_track'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            current_app.logger.debug("目前日期判断有错，日后需要更改，错在get_history_months")
            mongo.db[coll_name].delete_many({
                'MON': mon
            })
        write_bulk_mongo(coll_name, result)


def handle(mon_point):
    current_app.logger.debug('handle month-{} data[key_info_track]'.format(
        mon_point[0]))
    month_data = load_assess_month_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug(
            '{}key_info_track report fetch 0 record.'.format(mon_point))


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
