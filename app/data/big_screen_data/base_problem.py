#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    月考核量化指标详细数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def load_check_address_data():
    sql = '''SELECT DISTINCT
            c.NAME AS MAJOR,
            d.NAME AS STATION,
            a.DEPARTMENT_NAME,
            a.PROBLEM_POINT,
            a.RISK_LEVEL
        FROM
            t_problem_base AS a
                LEFT JOIN
            t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = b.TYPE3
        WHERE
            a.IS_HIDDEN_KEY_PROBLEM = 1
                AND c.NAME = '机务';
    '''
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("基础问题库数据出错（mysql取不到数据）")
    else:
        data = data.fillna(0)
        # result = []
        # for idx in data.index:
        #     val = data.loc[idx]
        #     result.append(json.loads(val.to_json()))
        current_app.logger.debug('base_problem_lib_complete: format data.')
        coll_name = 'base_problem_lib'
        mongo.db[coll_name].drop()
        mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())


def handle():
    month_data = load_check_address_data()
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug('base_problem_lib report fetch 0 record.')


@init_wrapper
def execute(months):
    handle()
    return 'OK'


if __name__ == '__main__':
    pass
