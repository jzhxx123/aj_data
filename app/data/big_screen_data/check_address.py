#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    检查点与地图数据数据
'''

import json

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo)
from flask import current_app

from app.utils.decorator import init_wrapper


def load_check_address_data():
    sql = '''SELECT
        a.TYPE,
        b.NAME,
        c.NAME AS MAJOR,
        d.NAME AS STATION,
        e.NAME AS CHECK_NAME,
        e.ALL_NAME AS CHECK_ALL_NAME,
        e.IS_DELETE,
        e.PK_ID AS POINT_ID,
        f.NAME AS PARENT_NAME,
        a.LATITUDE,
        a.LONGITUDE
    FROM
        t_address_and_map AS a
            LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = b.TYPE3
            LEFT JOIN
        t_check_point AS e ON e.PK_ID = a.FK_ADDRESS_ID
            LEFT JOIN
        t_check_point AS f ON f.PK_ID = e.PARENT_ID;
    '''
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("检查地点通知数据")
    else:
        data = data.fillna(0)
        result = []
        for idx in data.index:
            val = data.loc[idx]
            result.append(json.loads(val.to_json()))
        current_app.logger.debug('check_address_complete: format data.')
        coll_name = 'check_address'
        mongo.db[coll_name].drop()
        write_bulk_mongo(coll_name, result)


def handle():
    month_data = load_check_address_data()
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug('check_address report fetch 0 record.')


@init_wrapper
def execute(months):
    handle()
    return 'OK'


if __name__ == '__main__':
    pass
