#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    重点工作督办数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import (get_mon_from_date, pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper


def load_assess_month_data(mon_point):
    sql = '''SELECT
        a.PK_ID,
        a.PROFESSION AS MAJOR,
        a.LEVEL,
        a.SOLVE_TIME,
        a.ITEM,
        a.CLAIM,
        a.TARGET,
        a.RESPONSIBILITY_DEPARTMENT_NAMES,
        a.SUBMIT_TIME,
        a.LIMIT_DATE,
        b.REAL_SITUATION,
        b.STATUS
    FROM
        t_safety_work_supervise AS a
            LEFT JOIN
        t_safety_work_supervise_department AS b
            ON b.FK_SAFETY_WORK_SUPERVISE_ID = a.PK_ID
    WHERE
        a.SUBMIT_TIME >= '{}' AND a.SUBMIT_TIME < '{}' AND a.STATUS != 5;
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("{}无安全督办数据".format(mon))
    else:
        data['START_DATE'] = data['SUBMIT_TIME'].apply(
            lambda x: int(str(x)[:10].replace('-', '')))
        data['MON'] = data['SUBMIT_TIME'].apply(get_mon_from_date)
        data = data.fillna(0)
        data['END_DATE'] = data['LIMIT_DATE'].apply(
            lambda x: int(str(x).replace("-", "")))
        del data['SUBMIT_TIME'], data['LIMIT_DATE']
        result = []
        for idx in data.index:
            val = data.loc[idx]
            result.append(json.loads(val.to_json()))
        current_app.logger.debug(
            'safety_work_supervise_complete: format data.')
        coll_name = '{}safety_work_supervise'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            current_app.logger.debug("目前日期判断有错，日后需要更改，错在get_history_months")
            mongo.db[coll_name].delete_many({'MON': mon})
        write_bulk_mongo(coll_name, result)


def handle(mon_point):
    current_app.logger.debug(
        'handle month-{} data[safety_work_supervise]'.format(mon_point[0]))
    month_data = load_assess_month_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug(
            '{}safety_work_supervise report fetch 0 record.'.format(mon_point))


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
