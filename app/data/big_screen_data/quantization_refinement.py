#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    量化细化指标详细数据
'''
import json

import numpy as np
from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def load_quantization_refinement_data(mon_point):
    # 获取量化细化指标
    sql = '''SELECT
        d.NAME AS MAJOR,
        c.NAME,
        c.ALL_NAME,
        a.PK_ID,
        a.PERSON_NAME,
        a.ID_CARD,
        a.YEAR,
        a.MONTH,
        a.NUMBER_TOTAL,
        a.COMPLETE_NUMBER,
        a.MONITOR_TIME_TOTAL,
        a.MONITOR_COMPLETE_TIME
    FROM
        t_quantization_refinement_quota AS a
            LEFT JOIN
        t_person AS b ON b.ID_CARD = a.ID_CARD
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
    WHERE
        a.YEAR = {} AND a.MONTH = {};
        '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_complete_situation(a, b, c, d):
    if np.isnan(b) or np.isnan(d):
        return None
    elif b >= a and d >= c:
        return 1
    else:
        return 0


def get_data(data):
    # print(data[:100])
    if len(data) == 0:
        current_app.logger.debug("{}无量化细化指标数据".format(mon))
    else:
        data['MON'] = mon
        data['COMPLETE'] = data.apply(
            lambda x: get_complete_situation(
                x['NUMBER_TOTAL'], x['COMPLETE_NUMBER'],
                x['MONITOR_TIME_TOTAL'], x['MONITOR_COMPLETE_TIME']),
            axis=1)
        current_app.logger.debug(
            'quantization_refinement_complete: format data.')
        coll_name = '{}quantization_refine_quota'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            mongo.db[coll_name].delete_many({'MON': mon})
        mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
        current_app.logger.debug(f'{mon} insert done.')


def handle(mon_point):
    month_data = load_quantization_refinement_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug('fetch 0 record.')


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    year = months.end_date.year
    month = months.end_date.month
    mon_point = [year, month]
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
