#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    月考核量化指标详细数据
'''
import json

from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def load_analysis_center_assess_data(mon_point):
    # 获取安全分析中心量化考核信息
    sql = '''SELECT
        b.DEPARTMENT_NAME,
        b.YEAR,
        b.MONTH,
        d.NAME AS MAJOR,
        a.CHECK_ITEM_NAME,
        a.ASSESS_POINT,
        a.ASSESS_CONDITION,
        a.ASSESS_STANDRAD,
        a.GRADES_TYPE,
        a.ACTUAL_SCORE,
        a.SOURCE_TYPE
    FROM
        t_analysis_center_assess_detail AS a
            LEFT JOIN
        t_analysis_center_assess AS b
                    ON b.PK_ID = a.FK_ANALYSIS_CENTER_ASSESS_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
    WHERE
        b.YEAR = {} AND b.MONTH = {};
        '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("{}无安全分析中心考核数据".format(mon))
    else:
        data['MON'] = mon
        current_app.logger.debug(
            'analysis_center_assess_complete: format data.')
        coll_name = '{}analysis_center_assess'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            mongo.db[coll_name].delete_many({'MON': mon})
        mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())


def handle(mon_point):
    month_data = load_analysis_center_assess_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug(
            'analysis_center_assess report fetch 0 record.')


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    year = months.end_date.year
    month = months.end_date.month
    mon_point = [year, month]
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
