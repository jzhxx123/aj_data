#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    基础人员表详细数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def load_person_data():
    sql = '''SELECT
        a.PERSON_ID,
        a.PERSON_NAME,
        a.ID_CARD,
        a.IDENTITY,
        a.LEVEL,
        a.JOB,
        a.PERSON_JOB_TYPE,
        b.NAME,
        b.TYPE,
        b.BUSINESS_CLASSIFY,
        b.ALL_NAME,
        c.NAME AS MAJOR,
        d.DEPARTMENT_ID AS STATION_ID,
        d.ALL_NAME as STATION_NAME
    FROM
        t_person AS a
            LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = b.type2
            LEFT JOIN
        t_department as d ON d.DEPARTMENT_ID = b.type3
    WHERE
        a.IS_DELETE = 0;
    '''
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("基础人员数据出错（mysql取不到数据）")
    else:
        data = data.fillna('')
        current_app.logger.debug('base_person: format data.')
        coll_name = 'base_person'
        mongo.db[coll_name].drop()
        mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
        current_app.logger.debug('base_person: insert done.')


def handle():
    month_data = load_person_data()
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug('base_person report fetch 0 record.')


@init_wrapper
def execute(months):
    handle()
    return 'OK'


if __name__ == '__main__':
    pass
