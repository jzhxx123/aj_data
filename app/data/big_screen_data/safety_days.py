#!/usr/bin/python3
# -*- coding: utf-8 -*-

from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
import pandas as pd
from flask import current_app

from app import db, mongo
from app.data.safety_start import safety_start_days
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def get_big_screen_data():
    # doc_count = mongo.db['base_data'].count({
    # 'TYPE': 'days_without_accident'})
    # if doc_count > 0:
    #     return
    SQL = """SELECT
                OCCURRENCE_TIME AS OT
            FROM
                t_safety_produce_info
            WHERE
                CATEGORY <= 104
                    AND RESPONSIBILITY_DIVISION_NAME = '局属单位责任'
            ORDER BY OCCURRENCE_TIME DESC
            LIMIT 1;
            """
    data = list(db.session.execute(SQL))
    year = current_app.config.get('SAFETY_START_YEAR', '2015')
    month = current_app.config.get('SAFETY_START_MONTH', '5')
    day = current_app.config.get('SAFETY_START_DAY', '20')
    if year.isdigit():
        year = int(year)
    else:
        year = 2015
    if month.isdigit():
        month = int(month)
    else:
        month = 2015
    if day.isdigit():
        day = int(day)
    else:
        day = 2015
    start_day = dt(year=year, month=month, day=day)
    today = dt.today()
    if len(data) != 0:
        start_day = data[0]['OT']
    days_without_accident = (today - start_day).days
    mongo.db.base_data.delete_many({'TYPE': 'days_without_accident'})
    mongo.db.base_data.insert_one({
        'TYPE': 'days_without_accident',
        'VALUE': days_without_accident,
    })


def devide_type(ONEtype):
    if 'A' in ONEtype:
        return 'A'
    elif 'B' in ONEtype:
        return 'B'
    elif 'C' in ONEtype:
        return 'C'
    elif 'D' in ONEtype:
        return 'D'
    else:
        return 0


def get_chewu_own_name(own):
    if own is not None:
        _id = "type_" + str(own)
        owns = {"type_1": "运输", "type_2": "客运", "type_3": "货运", "type_4": "其他"}
        return owns[_id]
    else:
        return


def get_right_date_str(date, right_format):
    # day = int(date.split(' ')[0].replace("/",''))
    time = dt.strptime(date, right_format)
    clock = int(date.split(' ')[1].replace(":", ''))
    if clock >= 180000:
        time = time + relativedelta(days=1)
    int_time = int(str(time)[:10].replace("-", ""))
    return int_time


def get_right_date_date(date):
    # day = int(date.split(' ')[0].replace("/",''))
    clock = int(str(date).split(' ')[1].replace(":", ''))
    if clock >= 180000:
        date = date + relativedelta(days=1)
    return date


def get_days_without_accident():
    SQL = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            d.NAME AS MAJOR,
            e.CHEWU_OWN AS TYPE
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
                LEFT JOIN
            t_check_problem AS e ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
        WHERE
            a.RESPONSIBILITY_DIVISION_NAME = '局属单位责任';
            """
    data = pd_query(SQL)
    data['type'] = data['CODE'].apply(devide_type)
    start_days = safety_start_days
    start_days_data = pd.DataFrame(start_days)
    start_days_data['date'] = start_days_data['date'].apply(
        lambda x: get_right_date_str(x, "%m/%d/%Y %H:%M:%S"))
    # 计算专业距离最近的日期
    day_data = start_days_data.groupby('major').max(by='date').reset_index()
    today = dt.today()
    majors = ['车务', '机务', '工务', '电务', '供电', '车辆']
    types = ['A', 'B', 'C', 'D']
    for major in majors:
        major_data_1 = data[data['MAJOR'] == major]
        major_data_3 = data[data['PROFESSION'] == major]
        major_data = pd.concat(
            [major_data_1, major_data_3], axis=0, sort=False)
        # 得到专业默认日期
        start_day = list(day_data[day_data['major'] == major]['date'])[0]
        year = int(str(start_day)[:4])
        month = int(str(start_day)[4:6])
        day = int(str(start_day)[6:])
        start_day = dt(year, month, day)
        # 计算无责任事故ABCD天数
        for ty in types:
            major_data_ty = major_data[major_data['type'] == ty]
            if len(major_data_ty) != 0:
                major_data_ty = major_data_ty.sort_values(
                    'OT', ascending=False).reset_index()[0:1]
                start_day = get_right_date_date(list(major_data_ty['OT'])[0])
            days_without_accident = (today - start_day).days + 1
            mongo.db.without_accident_data.delete_many(
                {'TYPE': ty, 'MAJOR': major})
            mongo.db.without_accident_data.insert_one({  # 专业安全天数
                'TYPE': ty,
                'MAJOR': major,
                'VALUE': days_without_accident,
                "HIERARCHY": 1,
            })
            # 计算车务运输，货运，客运安全天数
            if major == '车务':
                chewu_owns = [1, 2, 3]
                for own in chewu_owns:
                    own_name = get_chewu_own_name(own)
                    major_data_ty = major_data[major_data['type'] == ty]
                    major_data_own_1 = major_data_ty[major_data_ty['TYPE'] ==
                                                     str(own)]
                    major_data_own_2 = major_data_ty[
                        major_data_ty.RESPONSIBILITY_UNIT.str.contains(
                            own_name, regex=False)]
                    major_data_own = pd.concat(
                        [major_data_own_1, major_data_own_2], axis=0, sort=False)
                    if len(major_data_own) != 0:
                        major_data_own = major_data_own.sort_values(
                            'OT', ascending=False).reset_index()[0:1]
                        start_day = get_right_date_date(
                            list(major_data_ty['OT'])[0])
                    else:
                        start_day = dt(year=year, month=month, day=day)
                    days_without_accident = (today - start_day).days + 1
                    mongo.db.without_accident_data.delete_many({
                        'TYPE': ty,
                        'MAJOR': major,
                        'chewu_own': own
                    })
                    mongo.db.without_accident_data.insert_one({
                        'TYPE': ty,
                        'MAJOR': major,
                        'chewu_own': get_chewu_own_name(own),
                        'VALUE': days_without_accident,
                        "HIERARCHY": 2,
                    })
        station_days_data = start_days_data[start_days_data['major'] == major]
        stations = list(station_days_data['station'])
        for station in stations:
            # 实际站段安全生产信息数据
            station_data = major_data[major_data['RESPONSIBILITY_UNIT'] ==
                                      station]
            # 默认初始日期
            start_day = list(station_days_data[station_days_data['station'] ==
                                               station]['date'])[0]
            year = int(str(start_day)[:4])
            month = int(str(start_day)[4:6])
            day = int(str(start_day)[6:])
            start_day = dt(year, month, day)
            # 计算无责任事故ABCD天数
            for ty in types:
                station_data_ty = station_data[station_data['type'] == ty]
                if len(station_data_ty) != 0:
                    station_data_ty = station_data_ty.sort_values(
                        'OT', ascending=False).reset_index()[0:1]
                    start_day = get_right_date_date(
                        list(major_data_ty['OT'])[0])
                days_without_accident = (today - start_day).days + 1
                mongo.db.without_accident_data.delete_many({
                    'TYPE': ty,
                    'MAJOR': major,
                    "STATION": station
                })
                mongo.db.without_accident_data.insert_one({  # 专业安全天数
                    'TYPE': ty,
                    'MAJOR': major,
                    "STATION": station,
                    'VALUE': days_without_accident,
                    "HIERARCHY": 3,
                })


def handle():
    # 计算路局安全天数
    get_big_screen_data()
    # 获取大屏无责任安全天数
    get_days_without_accident()


@init_wrapper
def execute(months):
    handle()
    return 'OK'


if __name__ == '__main__':
    current_app.logger.debug('Done!')
