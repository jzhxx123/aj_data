#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    量化考核实时表数据
'''
import json

from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.decorator import init_wrapper


def load_quantify_assess_real_data(mon_point):
    # 获取量化考核实时表
    sql = '''SELECT
        a.PK_ID,
        a.REALITY_MIN_QUALITY_GRADES,
        a.ALL_NAME,
        b.NAME,
        c.NAME AS MAJOR
    FROM
        t_quantify_assess_real_time AS a
            LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
    WHERE
        a.YEAR = {} AND a.MONTH = {};
        '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("{}无量化考核实时表数据".format(mon))
    else:
        data['MON'] = mon
        current_app.logger.debug('quantify_assess_real_complete: format data.')
        coll_name = '{}quantify_assess_real'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            mongo.db[coll_name].delete_many({'MON': mon})
        mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
        current_app.logger.debug(f'{mon} insert done.')


def handle(mon_point):
    month_data = load_quantify_assess_real_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug('fetch 0 record.')


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    year = months.end_date.year
    month = months.end_date.month
    mon_point = [year, month]
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
