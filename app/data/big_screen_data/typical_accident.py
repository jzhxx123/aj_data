#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    典型事故数据相关数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import pd_query, write_bulk_mongo
from app.utils.decorator import init_wrapper


def get_data(data):
    data['DATE'] = data['OCCURRENCE_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = mon
    result = []
    for idx in data.index:
        val = data.loc[idx]
        result.append(json.loads(val.to_json()))
    coll_name = '{}typical_accident'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def load_data(mon_point):
    sql = '''SELECT
            OCCURRENCE_TIME, OVERVIEW, IS_TRAFFIC_ACCIDENT
        FROM
            t_analysis_center_typical_problem
        WHERE
            IS_TRAFFIC_ACCIDENT = 1
            AND OCCURRENCE_TIME >= '{}' AND OCCURRENCE_TIME < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def handle(mon_point):

    current_app.logger.debug('handle month-{} data[typical_accdient]'.format(
        mon_point[0]))
    data = load_data(mon_point)
    if data.iloc[:, 0].size == 0:
        current_app.logger.debug('daily report fetch 0 record.')
    else:
        get_data(data.copy())


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)

    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
