#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    月考核量化指标详细数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import pd_query, write_bulk_mongo
from app.utils.decorator import init_wrapper


def load_notify_month_data(mon_point):
    sql = '''SELECT
            a.PK_ID,
            a.CREATE_TIME,
            a.TITLE,
            a.STATUS,
            a.TYPE,
            a.CONTENT,
            a.DEPARTMENT_NAMES,
            d.NAME AS MAJOR,
            f.ALL_NAME
        FROM
            t_system_notify AS a
                LEFT JOIN
            t_system_notify_and_department AS b
                        ON b.FK_SYSTEM_NOTIFY_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            left join
            t_person as e on e.ID_CARD = a.ID_CARD
            left join
            t_department AS f ON f.DEPARTMENT_ID = e.FK_DEPARTMENT_ID
        WHERE
            CREATE_TIME >= '{}' AND CREATE_TIME < '{}';
    '''.format(mon_point[0], mon_point[1])
    data = pd_query(sql)
    return data


def get_data(data):
    if len(data) == 0:
        current_app.logger.debug("{}无外局通知数据".format(mon))
    else:
        data['DATE'] = data['CREATE_TIME'].apply(
            lambda x: int(str(x)[:10].replace("-", "")))
        data['MON'] = mon
        result = []
        for idx in data.index:
            val = data.loc[idx]
            result.append(json.loads(val.to_json()))
        current_app.logger.debug('system_notify_complete: format data.')
        coll_name = '{}system_notify'.format(prefix)
        if prefix == 'daily_':
            mongo.db[coll_name].drop()
        else:
            mongo.db[coll_name].delete_many({'MON': mon})
        write_bulk_mongo(coll_name, result)


def handle(mon_point):
    month_data = load_notify_month_data(mon_point)
    if month_data.iloc[:, 0].size != 0:
        get_data(month_data.copy())
    else:
        current_app.logger.debug(
            '{}system_notify report fetch 0 record.'.format(mon_point))


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
