

import json
import math

from flask import current_app, jsonify, request

#!/usr/bin/python3
# -*- coding: utf-8 -*-
#from line_profiler import LineProfiler
import numpy as np
import pandas as pd
from app import mongo
from app.data.util import (get_previous_month, paste_daily_to_monthly,
                           paste_monthly_to_history, pd_query,
                           write_bulk_mongo)

def get_safe_evaluate_index():
    cardes = get_carde_person()
    check_quality = get_check_and_quality()
    safeTalk_score = get_safe_talk_heart()
    evaluate_score = get_evaluate_score()
    result_score = pd.merge(cardes, check_quality,
                            how='left', on=['id_card', 'level'])
    result_score = pd.merge(result_score, safeTalk_score, how='left', on=[
                            'id_card', 'year', 'month'])
    result_score = pd.merge(result_score, evaluate_score, how='left', on=[
                            'id_card', 'year', 'month'])
    result_score['check_score'] = result_score.fillna(0)['check_score']
    result_score['quality_score'] = result_score.fillna(0)['quality_score']
    result_score['talk_score'] = result_score.fillna(0)['talk_score']
    result_score['evaluate_score'] = result_score.fillna(40)['evaluate_score']
    result_score['evaluate_index'] = result_score.apply(
        lambda x: x['check_score']+x['quality_score']+x['talk_score']+x['evaluate_score'], axis=1)

    result_score = result_score.dropna(subset=['month', 'year'])
    result_score['level_rank'] = result_score.groupby(['profession_id', 'level', 'year', 'month'])[
        'evaluate_index'].rank(ascending=0, method='min')
    result_score['duty_rank'] = result_score.groupby(['profession_id', 'duty', 'year', 'month'])[
        'evaluate_index'].rank(ascending=0, method='min')
    result_score['level_pct'] = result_score.groupby(['profession_id', 'level', 'year', 'month'])[
        'evaluate_index'].rank(ascending=0, pct=True, method='min')
    result_score['duty_pct'] = result_score.groupby(['profession_id', 'duty', 'year', 'month'])[
        'evaluate_index'].rank(ascending=0, pct=True, method='min')
    result_score['level_count'] = result_score.apply(
        lambda x: int(x['level_rank']/x['level_pct']), axis=1)
    result_score['duty_count'] = result_score.apply(lambda x: None if math.isnan(
        x['duty_pct']) else int(x['duty_rank']/x['duty_pct']), axis=1)
    del result_score['duty_pct'], result_score['level_pct']
    result_score['date'] = result_score.apply(lambda x: str(int(x['year']))+str(int(x['month']))+'25' if int(
        x['month']) >= 10 else str(int(x['year']))+'0'+str(int(x['month']))+'25', axis=1)
    result = result_score.fillna("***")
    # 删除数据
    # mongo.db.daily_detail_check_info.drop()
    # 写入新履职安全指数信息数据到mongoDB
    rs = []
    for index in result.index:
        value = result.loc[index]
        rs.append(json.loads(value.to_json()))
        # current_app.logger.debug("e")
    current_app.logger.debug('complete: format data.')
    mongo.db.safe_evaluate_index.drop()
    write_bulk_mongo('safe_evaluate_index', rs)

# 从系统人员表中得到干部基本信息集合


def get_carde_person():
    '''
            得到所有工人中干部的集合，返回一个干部信息的dataFrame
    '''
    carde_sql = '''
				SELECT DISTINCT
				    a.ID_CARD AS id_card,
				    a.PERSON_NAME AS name,
				    a.LEVEL AS level,
				    a.PERSON_JOB_TYPE AS duty,
				    a.FK_DEPARTMENT_ID,
				    b.NAME as department,
				    c.NAME AS satition_name,
				    b.TYPE2 AS profession_id,
				    b.TYPE3 AS satition_id,
				    d.NAME AS profession_name
				FROM
				    t_person AS a
				        INNER JOIN
				    t_department AS b ON b.department_ID = a.FK_department_ID
				        INNER JOIN
				    t_department AS c ON c.DEPARTMENT_ID = b.TYPE3
				        INNER JOIN
				    t_department AS d ON d.DEPARTMENT_ID = b.TYPE2
				WHERE
				    a.IDENTITY = '干部'
				        AND a.level IN ('一般管理' , '副处级',
				        '正处级',
				        '副科级',
				        '正科级');'''
    allCarde = pd_query(carde_sql)
    current_app.logger.debug("allCarde")
    return allCarde


# 得到专业内各层级人数
def get_profession_count():
    level_count_sql = '''
		SELECT DISTINCT
			COUNT(a.ID_CARD),
			a.LEVEL AS level,
			d.NAME AS profession_name
		FROM
			t_person AS a
				INNER JOIN
			t_department AS b ON b.department_ID = a.FK_department_ID
				INNER JOIN
			t_department AS d ON d.DEPARTMENT_ID = b.TYPE2
		WHERE
			a.IDENTITY = '干部'
				AND a.level IN ('一般管理' , '副处级',
				'正处级',
				'副科级',
				'正科级')
		GROUP BY profession_name , level;'''
    level_count = pd_query(level_count_sql)
    duty_count_sql = '''
		SELECT DISTINCT
			COUNT(a.ID_CARD),
			a.PERSON_JOB_TYPE AS duty,
			d.NAME AS profession_name
		FROM
			t_person AS a
				INNER JOIN
			t_department AS b ON b.department_ID = a.FK_department_ID
				INNER JOIN
			t_department AS d ON d.DEPARTMENT_ID = b.TYPE2
		WHERE
			a.IDENTITY = '干部'
				AND a.level IN ('一般管理' , '副处级',
				'正处级',
				'副科级',
				'正科级')
		GROUP BY profession_name,duty;'''
    duty_count = pd_query(duty_count_sql)
    current_app.logger.debug(level_count, duty_count)


# 从系统中得到人员信息时间
def get_date():
    '''
            得到系统所能查看的时间范围
    '''
    sql = '''
		SELECT DISTINCT
		    year, month
		FROM
		    t_check_evaluate_info;
	'''
    date = pd_query(sql)
    return date

# 得到干部每层级每月人均安全检查次数和人均发现问题质量分作为基数


def get_base_number():
    '''
            得到人物所在层级各月份人均安全检查基数,存入mongoDB
    '''
    totalCheck_sql = '''
			SELECT
			    b.YEAR as year,
			    b.MONTH as month,
			    c.LEVEL as level,
			    SUM(a.CHECK_COUNT) as check_counts,
			    SUM(a.MIN_QUALITY_GRADES_COUNT) as quality_scores
			FROM
			    t_safety_assess_month_quantify_detail AS a
			        INNER JOIN
			    t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
			        LEFT JOIN
			    t_person AS c ON c.ID_CARD = a.ID_CARD
			GROUP BY YEAR , MONTH , LEVEL'''
    baseDate = pd_query(totalCheck_sql)
    totalCount_sql = '''
				SELECT 
					level, COUNT(DISTINCT ID_CARD) AS count
				FROM
					t_person
				WHERE
					IDENTITY = '干部'
						AND level IN ('一般管理' , '副处级',
						'正处级',
						'副科级',
						'正科级')
				GROUP BY level ;
			'''
    totalCount = pd_query(totalCount_sql)
    base_date = pd.merge(
        baseDate,
        totalCount,
        how='inner',
        on='level'
    )
    base_date['base_check'] = (
        lambda x, y: x/y)(base_date['check_counts'], base_date['count'])
    base_date['base_score'] = (
        lambda x, y: x/y)(base_date['quality_scores'], base_date['count'])
    del base_date['check_counts']
    del base_date['quality_scores']
    del base_date['count']
    return base_date

# 计算得到安全检查得分


def get_check_and_quality():
    base_date = get_base_number()
    count_sql = '''
			SELECT
			    a.ID_CARD as id_card,
			    a.CHECK_COUNT as count,
			    a.MIN_QUALITY_GRADES_COUNT as score,
			    b.YEAR as year,
			    b.MONTH as month,
			    c.LEVEL as level
			FROM
			    t_safety_assess_month_quantify_detail AS a
			        LEFT JOIN
			    t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
			        LEFT JOIN
			    t_person AS c ON c.ID_CARD = a.ID_CARD;'''

    month_score = pd_query(count_sql)
    check_quality = pd.merge(month_score, base_date,
                             how='inner',
                             on=['level', 'year', 'month'],
                             )
    check_quality['check_score'] = check_quality.apply(lambda x: 0 if x['count'] == 0
                                                       else 15 if (x['count']-x['base_check'])/x['base_check'] >= 0.30
                                                       else 15+((x['count']-x['base_check'])/x['base_check']-0.30)*10, axis=1)

    check_quality['quality_score'] = check_quality.apply(lambda x: 0 if x['score'] == 0
                                                         else 40 if (x['score']-x['base_score'])/x['base_score'] >= 0.30
                                                         else (40+((x['score']-x['base_score'])/x['base_score']-0.30)*20), axis=1)

    return check_quality

# 计算月份信息


def match_date(date):
    if date != None:
        date = date.strftime('%Y-%m-%d').split("-")
        if int(date[2]) <= 24:
            return date[0]+'-'+date[1]
        else:
            if int(date[1]) == 12:
                return str(int(date[0])+1)+'-'+'01'
            elif int(date[1]) > 8:
                return date[0]+'-'+str(int(date[1])+1)
            else:
                return date[0]+'-0'+str(int(date[1])+1)
    else:
        return

# 统计月度安全谈心得分


def get_safe_talk_heart():
    def get_score(x, y):
        if y == 0:
            return 5.0
        else:
            if x == 1:
                return 1.0
            elif x == 2:
                return 3.0
            elif x == 3:
                return 5.0
            else:
                return 0.0

    sql = '''
		SELECT
		    APPRAISAL_RESULT AS result,
		    ID_CARD AS id_card,
		    TALK_DATE
		FROM
		    t_talk_safety_heart
		WHERE
		    IS_DELETE = 0
	'''
    talk_data = pd_query(sql)
    talk_data['date'] = talk_data['TALK_DATE'].apply(match_date)
    del talk_data['TALK_DATE']
    talk_person = talk_data.groupby(['id_card', 'date']).count()
    talk_person['rs'] = talk_person['result'].apply(
        lambda x: 1 if x == 1 else 0)
    del talk_person['result']
    talk_data.drop_duplicates(['id_card', 'date'], inplace=True)
    talk_person = talk_person.reset_index()
    talk_data = pd.merge(talk_data, talk_person,
                         how='inner', on=['id_card', 'date'])
    talk_data['talk_score'] = talk_data.apply(
        lambda x: get_score(x['result'], x['rs']), axis=1)
    talk_data['year'] = talk_data['date'].apply(lambda x: int(x[:4]))
    talk_data['month'] = talk_data['date'].apply(lambda x: int(x[5:]))
    del talk_data['result'], talk_data['rs'], talk_data['date']
    return talk_data

# 计算履职评价得分


def get_evaluate_score():
    sql = '''
		SELECT
		    SUM(score) AS points, RESPONSIBE_ID_CARD as id_card, year, month
		FROM
		    t_check_evaluate_info
		GROUP BY RESPONSIBE_ID_CARD , year , month;
	'''
    score_data = pd_query(sql)
    score_data['evaluate_score'] = score_data['points'].apply(
        lambda x: 40-x*20 if 40-x*20 > 0 else 0)
    del score_data['points']
    current_app.logger.debug("evaluate_data")
    return score_data

# 按照层级时间分组排序算法


def get_index_sort(groups, keys, key):
    allCols = groups.columns.values.tolist()
    allCols.append(key)
    countKey = key.split('_')[0]+'_counts'
    allCols.append(countKey)
    result = pd.DataFrame(columns=allCols)
    allGroup = groups.groupby(keys)
    for name, group in allGroup:
        group[key] = group['evaluate_index'].rank(ascending=0, method='min')
        group[countKey] = group.count().values[0]
        result = pd.concat([result, group], ignore_index=True, sort=False)
    return result

# 获取干部安全履职检查写实表


def get_evaluate_info():
    '''
            干部安全履职检查写实信息存入mongoDB
    '''
    check_sql = '''
	SELECT
	    a.ID_CARD AS id_card,
	    a.START_CHECK_TIME AS time,
	    a.CHECK_WAY AS method,
	    c.CHECK_ADDRESS_NAMES AS location,
	    c.CHECK_ITEM_NAMES AS description,
	    d.RESPONSIBILITY_DEPARTMENT_NAME AS duty_department,
	    e.NAME AS item_name,
	    f.NAME AS risk_name
	FROM
	    t_statistics_check_info AS a
	        LEFT JOIN
	    t_check_info AS c ON c.PK_ID = a.FK_CHECK_INFO_ID
	        LEFT JOIN
	    t_check_problem AS d ON d.FK_CHECK_INFO_ID = c.PK_ID
	        LEFT JOIN
	    t_check_item AS e ON e.PK_ID = a.CHECK_ITEM_IDS
	        LEFT JOIN
	    t_risk AS f ON f.PK_ID = a.CHECK_RISK_IDS;'''

    check_info_data = pd_query(check_sql)
    check_info_data.rename(columns={'description': 'desc'}, inplace=True)
    check_info_data['date'] = check_info_data['time'].apply(match_date)
    del check_info_data['time']
    # 安全检查信息存入mongo
    check_rs = []
    for index in check_info_data.index:
        value = check_info_data.loc[index]
        check_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.check_info_data.drop()
    write_bulk_mongo('check_info_data', check_rs)

    # 计算不同方式检查次数存入mongo
    check = check_info_data.groupby(['id_card', 'date', 'method']).count()
    del check['desc'], check['duty_department'], check['item_name'], check['risk_name']
    check.rename(columns={'location': 'times'}, inplace=True)
    check = check.reset_index()
    checkTimes_rs = []
    for index in check.index:
        value = check.loc[index]
        checkTimes_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.check_times_data.drop()
    write_bulk_mongo('check_times_data', checkTimes_rs)

    # 计算每个月检查项目个数
    item_data = check_info_data.copy()
    del item_data['desc'], item_data['duty_department'], item_data['risk_name'], item_data['location']
    item = item_data.groupby(['id_card', 'date', 'item_name']).count()
    item.rename(columns={'method': 'times'}, inplace=True)
    item = item.reset_index()
    item['rank'] = item.groupby(['date', 'id_card'])[
        'times'].rank(ascending=0, method='first')
    # 项目前五项完成，存入mongo
    item_rs = []
    for index in item.index:
        value = item.loc[index]
        item_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.item_info_data.drop()
    write_bulk_mongo('item_info_data', item_rs)

    # 计算风险前五项
    risk_data = check_info_data.copy()
    del risk_data['desc'], risk_data['duty_department'], risk_data['item_name'], risk_data['location']
    issus = risk_data.groupby(['id_card', 'date', 'risk_name']).count()
    issus.rename(columns={'method': 'times'}, inplace=True)
    issus = issus.reset_index()
    issus['rank'] = issus.groupby(['date', 'id_card'])[
        'times'].rank(ascending=0, method='first')
    # 风险前五项完成，存入mongo
    risk_rs = []
    for index in issus.index:
        value = issus.loc[index]
        risk_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.risk_info_data.drop()
    write_bulk_mongo('risk_info_data', risk_rs)
    return

    qulaity_sql = '''
		SELECT
		    a.ID_CARD AS id_card,
		    a.SUBMIT_TIME AS time,
		    a.CHECK_WAY AS method,
		    c.CHECK_ADDRESS_NAMES AS location,
		    c.CHECK_ITEM_NAMES AS description,
		    d.RESPONSIBILITY_DEPARTMENT_NAME AS duty_department,
		    a.LEVEL AS issue_item,
		    a.RISK_LEVEL AS risk_level
		FROM
		    t_statistics_check_problem AS a
		        INNER JOIN
		    t_check_info AS c ON c.PK_ID = a.FK_CHECK_INFO_ID
		        LEFT JOIN
		    t_check_problem AS d ON d.FK_CHECK_INFO_ID = c.PK_ID;
	'''
    problem_info_data = pd_query(qulaity_sql)
    problem_info_data.rename(columns={'description': 'desc'}, inplace=True)
    problem_info_data['date'] = problem_info_data['time'].apply(match_date)
    # 发现问题信息存入mongo
    problem_rs = []
    for index in problem_info_data.index:
        value = problem_info_data.loc[index]
        problem_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.problem_info_data.drop()
    write_bulk_mongo('problem_info_data', problem_rs)

    # 计算发现问题等级个数
    problem = problem_info_data.groupby(
        ['id_card', 'date', 'risk_level']).count()
    del problem['desc'], problem['duty_department'], problem['issue_item'], problem['method'], problem['time']
    problem.rename(columns={'location': 'times'}, inplace=True)
    problem = problem.reset_index()
    # 问题个数解决写入mongo
    problemCount_rs = []
    for index in problem.index:
        value = problem.loc[index]
        problemCount_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.problem_times_data.drop()
    write_bulk_mongo('problem_times_data', problemCount_rs)

    evaluate_sql = '''
		SELECT DISTINCT
			RESPONSIBE_ID_CARD AS id_card,
			YEAR,
			MONTH,
			SCORE AS score,
			EVALUATE_CONTENT AS evaluate
		FROM
			t_check_evaluate_info;
	'''
    evaluate_data = pd_query(evaluate_sql)
    evaluate_data['date'] = evaluate_data.apply(lambda x: str(x['YEAR'])+'-'+str(
        x['MONTH']) if int(x['MONTH']) >= 10 else str(x['YEAR'])+'-0'+str(x['MONTH']), axis=1)
    del evaluate_data['YEAR'], evaluate_data['MONTH']
    evaluate_rs = []
    for index in evaluate_data.index:
        value = evaluate_data.loc[index]
        evaluate_rs.append(json.loads(value.to_json()))
    current_app.logger.debug('complete: format data.')
    mongo.db.evaluate_info_data.drop()
    write_bulk_mongo('evaluate_info_data', evaluate_rs)


if __name__ == '__main__':
    pass
