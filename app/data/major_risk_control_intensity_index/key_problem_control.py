#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:  关键问题卡控指数
"""

import pandas as pd
import numpy as np
from flask import current_app

from app.data.major_risk_control_intensity_index.common_data import init_common_data
from app.data.control_intensity_index.common_sql import MAJOR_SQL
from app.data.major_risk_control_intensity_index.const import KeyControlQualityDetailType, INDEX_TYPE, MainType, HIERARCHY
from app.data.control_intensity_index.util import validate_exec_month
from app.data.util import update_major_maintype_weight, pd_query
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.major_risk_control_intensity_index.common import summarize_child_index, summarize_operation_set, \
    write_export_basic_data_to_mongo, format_export_basic_data, calc_unit_ratio
from app.data.control_intensity_index import cache_client


def _get_sql_data(months_ago):
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, CHECK_ADDRESS, RISK_AND_ITEM

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    CHECK_ADDRESS = cache_client.get('CHECK_ADDRESS', True)
    RISK_AND_ITEM = cache_client.get('RISK_AND_ITEM', True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def check_item_list():
    item_id = RISK_AND_ITEM.copy()
    item_id = item_id['ITEM_IDS'].apply(lambda x: x.split(',')).tolist()[0]
    return item_id


def add_data_col(row, deduct_dict):
    department_dict = deduct_dict.get(row['DEPARTMENT_ID'])
    content = '关键问题数：{}，未查出的问题项数：{} <br/>'
    if department_dict:
        deduct = department_dict.get('DEDUCT', 0)
        fail_item = department_dict.get('PROBLEMS', [])
        content = content.format(department_dict.get('ITEMS_COUNT', 0), department_dict.get('FAILED_ITEMS_COUNT', 0))
        content += '<br/>'.join(['{}个月内未查出的问题项点：{}'.format(k, '<br/>'.join(v)) for k, v in fail_item.items()])
    else:
        deduct = 0
        content = content.format(0, 0)
        content += ''
    score = max(100 - deduct, 0)
    return pd.Series([score, content.replace('\n', '').replace('\t', '')], index=['SCORE', 'CONTENT'])


problem_records_cache = {}


# 关键问题项点排查
def stats_key_problem_control(months_ago):
    rst_index_score = []
    station_data = ZHANDUAN_DPID_DATA.copy()
    problem_base = cache_client.get('problem_base')
    key_problem_lib = cache_client.get('key_problem_lib')
    deduct_dict = {}

    problem_base = problem_base[(problem_base['IS_DELETE'] == 0) & (problem_base['STATUS'] == 3)]
    key_problem_lib = pd.merge(key_problem_lib[['FK_PROBLEM_BASE_ID', 'FK_DEPARTMENT_ID']],
                               pd.DataFrame(problem_base, columns=['PK_ID', 'PROBLEM_POINT']),
                               left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')
    major_problems = pd.merge(key_problem_lib, pd_query(MAJOR_SQL),
                              left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    major_items_dict = {}
    for major, major_df in major_problems.groupby('MAJOR'):
        major_items_dict[major] = major_df['FK_PROBLEM_BASE_ID'].tolist()

    # 当月自查问题数据
    check_problems_and_resp = problem_records_cache.setdefault(months_ago, get_problem_records(months_ago))

    for _, row in station_data.iterrows():
        department_id = row['DEPARTMENT_ID']
        department_dict = deduct_dict.setdefault(department_id, {})
        failed_problems = department_dict.setdefault('PROBLEMS', {})  # 未排查的关键问题
        major = row['MAJOR']
        key_problems = key_problem_lib[key_problem_lib['FK_DEPARTMENT_ID'] == department_id]
        if major == '客运':
            major_items = major_items_dict.get('车务', [])
        elif major == '工电':
            major_items = major_items_dict.get('工务', []) + major_items_dict.get('电务', []) \
                          + major_items_dict.get('供电', [])
        else:
            major_items = major_items_dict[major]
        key_problems = key_problems.append(major_problems[major_problems['FK_PROBLEM_BASE_ID'].isin(major_items)],
                                           sort=False)

        key_problems['PROBLEM_BASE_ID'] = key_problems['PK_ID']
        key_problems.drop(['PK_ID'], axis=1, inplace=True)
        base_id = problem_base[problem_base['FK_CHECK_ITEM_ID'].isin(check_item_list())]['PK_ID'].tolist()
        key_problems = key_problems[key_problems['PROBLEM_BASE_ID'].isin(base_id)]
        if key_problems.empty:
            department_dict['DEDUCT'] = 0
            continue

        # 当前部门当月自查问题数据
        problem_records = check_problems_and_resp[check_problems_and_resp['TYPE3_CHECK'] == department_id]
        # 当月已经排查的关键问题
        has_checked_problem = set()
        # 当月未排查的关键问题
        not_checked_problem = set()

        child_problem_dict = {}

        for _, problem_row in key_problems.iterrows():
            # 关键问题库里配置的是专业问题，这里要加上站段的继承了此关键问题的问题
            children_data = problem_base[
                (problem_base['FK_DEPARTMENT_ID'] == department_id)
                & (problem_base['TYPE2'] == problem_row['PROBLEM_BASE_ID'])]
            children = children_data['PK_ID'].tolist()
            children.append(problem_row['PROBLEM_BASE_ID'])
            child_problem_dict[problem_row['PROBLEM_BASE_ID']] = children
            if problem_records[problem_records['FK_PROBLEM_BASE_ID'].isin(children)].index.size > 0:
                has_checked_problem.add(problem_row['PROBLEM_BASE_ID'])
            else:
                not_checked_problem.add(problem_row['PROBLEM_BASE_ID'])

        months_problem_deduct = {}  # n个月内未查出的问题记录字典

        for i in range(1, 4):
            """
            倒查4个月未发现扣a分； a=100/单位关键问题数
            """
            if not not_checked_problem:
                break
            mon = months_ago - i
            problem_records = problem_records_cache.setdefault(mon, get_problem_records(mon))
            problem_records = problem_records[problem_records['TYPE3_CHECK'] == department_id]
            has_checked_problem = set()

            for problem_id in not_checked_problem:
                children = child_problem_dict[problem_id]
                if problem_records[problem_records['FK_PROBLEM_BASE_ID'].isin(children)].index.size > 0:
                    has_checked_problem.add(problem_id)
            not_checked_problem = not_checked_problem - has_checked_problem

            if i >= 3:  # 4个月内查出了不扣分,往后开始扣分
                # i+1>=3，表示i+1个月内未排查出问题
                months_problem_deduct[i + 1] = not_checked_problem
                last_month_problem = months_problem_deduct.get(i)
                if last_month_problem:
                    # i个月内未查出的问题更新
                    months_problem_deduct[i] = last_month_problem - not_checked_problem

        base_point = 100 / key_problems.index.size if not key_problems.empty else 0
        department_dict['ITEMS_COUNT'] = key_problems.index.size
        for mon, problems in months_problem_deduct.items():
            # deduct = base_point * (1 + 0.3 * (mon - 3)) * len(problems)
            deduct = base_point * len(problems)
            department_dict['DEDUCT'] = department_dict.setdefault('DEDUCT', 0) + deduct
            department_dict['FAILED_ITEMS_COUNT'] = department_dict.setdefault('FAILED_ITEMS_COUNT', 0) + len(problems)
            if problems:
                failed_problems[mon] = key_problems[key_problems['PROBLEM_BASE_ID'].isin(problems)][
                    'PROBLEM_POINT'].tolist()

    rst_data = station_data.copy()
    append_df = rst_data.apply(lambda r: add_data_col(r, deduct_dict),
                               result_type='expand', axis=1)
    rst_data = rst_data.join(append_df)
    # 保存中间计算过程
    calc_basic_data_rst = format_export_basic_data(rst_data, MainType.key_problem_control,
                                                   KeyControlQualityDetailType.stats_key_problem_control,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.key_problem_control,
                                     KeyControlQualityDetailType.stats_key_problem_control,
                                     index_type=INDEX_TYPE)

    column = f'SCORE_a_{HIERARCHY}'

    rst_data = pd.DataFrame(index=rst_data['DEPARTMENT_ID'],
                            data=rst_data['SCORE'].values,
                            columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summarize_operation_set(rst_data, ZHANDUAN_DPID_DATA, column,
                            HIERARCHY, INDEX_TYPE, MainType.key_problem_control,
                            KeyControlQualityDetailType.stats_key_problem_control,
                            months_ago)
    rst_index_score.append(rst_data[[column]])
    deduct_dict.clear()
    return rst_index_score


def get_problem_records(months_ago):
    """
    获取自查问题实例数据
    :param months_ago:
    :return:
    """
    check_problems_and_resp = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    department_data = pd.DataFrame(DEPARTMENT_DATA, columns=['DEPARTMENT_ID', 'TYPE3'])
    check_problems_and_resp = pd.merge(check_problems_and_resp, department_data,
                                       left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    check_problems_and_resp.rename(columns={'TYPE3': 'TYPE3_RESP'}, inplace=True)
    check_problems_and_resp = pd.merge(check_problems_and_resp, department_data,
                                       left_on='FK_CHECK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    check_problems_and_resp.rename(columns={'TYPE3': 'TYPE3_CHECK'}, inplace=True)
    check_problems_and_resp = check_problems_and_resp[
        check_problems_and_resp['TYPE3_CHECK'] == check_problems_and_resp['TYPE3_RESP']]
    return check_problems_and_resp


def handle(months_ago, major, risk_id):
    # 部门按站段聚合
    init_common_data(major, risk_id)
    _get_sql_data(months_ago)

    child_index_func = [
        stats_key_problem_control
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    child_index_list = [KeyControlQualityDetailType.stats_key_problem_control]
    update_major_maintype_weight(index_type=INDEX_TYPE, main_type=MainType.key_problem_control,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summarize_child_index(child_score, ZHANDUAN_DPID_DATA, INDEX_TYPE, MainType.key_problem_control, months_ago,
                          item_name, item_weight)
    current_app.logger.debug(
        '├── └── key_problem_control index has been figured out!')


@validate_exec_month
def execute(months_ago, major, risk_id):
    handle(months_ago, major, risk_id)


if __name__ == '__main__':
    execute(-1)