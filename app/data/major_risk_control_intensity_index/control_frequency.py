#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:  卡控频率指数
"""

import pandas as pd
import numpy as np
from flask import current_app
from dateutil.relativedelta import SU, MO
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from dateutil import rrule

from app.data.major_risk_control_intensity_index.common_data import init_common_data
from app.data.major_risk_control_intensity_index.const import ControlFrequencyDetailType, INDEX_TYPE, MainType, HIERARCHY
from app.data.index.util import get_custom_month
from app.data.util import update_major_maintype_weight
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.major_risk_control_intensity_index.common import summarize_child_index, summarize_operation_set, \
    write_export_basic_data_to_mongo, format_export_basic_data, calc_unit_ratio
from app.data.control_intensity_index import cache_client
from app.data.control_intensity_index.control_frequency_sql import CHECK_INFO_AND_ADDRESS_SQL
from app.data.control_intensity_index.util import pd_query, validate_exec_month


def _get_sql_data(months_ago):
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, CHECK_ADDRESS

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    CHECK_ADDRESS = cache_client.get('CHECK_ADDRESS', True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def get_children_items(pk_id, df, res):
    temp = df[df['PARENT_ID'].isin(pk_id)]

    if not temp.empty:
        res.extend(temp['PK_ID'].tolist())
        get_children_items(temp['PK_ID'].tolist(), df, res)
    return


def get_parent_item(pk_id, df, hierarchy):
    temp = df[df['PK_ID'] == pk_id]

    if not temp.empty:
        if temp['HIERARCHY'].values[0] == hierarchy + 1:
            return temp['PARENT_ID'].values[0]
        return get_parent_item(temp['PARENT_ID'].values[0], df, hierarchy)
    return None


def add_data_col(row, deduct_dict):
    department_dict = deduct_dict.get(row['DEPARTMENT_ID'])
    content = '检查地点未按周期检查风险卡控点：<br/>'
    if department_dict:
        score = department_dict['SCORE']
        item_count = department_dict['ITEM_COUNT']
        fail_item = department_dict['ADDRESS']
        formula_ratio_sum = department_dict.get('FORMULA_RATIO_SUM', 0)
        content += '<br/>'.join(['{}:{}'.format(k, '、'.join(v)) for k, v in fail_item.items()])
    else:
        score = 0
        item_count = 0
        formula_ratio_sum = 0
        content = ''
    return pd.Series([score, content, item_count, formula_ratio_sum],
                     index=['SCORE', 'CONTENT', 'ITEM_COUNT', 'FORMULA_RATIO_SUM'])


def calc_score(row):
    """
    得分=Σ高风险加分+Σ中风险加分+Σ低风险加分
    危害程度“高”的加5a*计分频率换算=分/处，“中”的加2.5a计分频率换算=分/处，“低”的加a*计分频率换算=分/处。
    :param row:
    :return:
    """
    if row['FORMULA_RATIO_SUM'] == 0:
        return 0
    point_a = 100 / row['FORMULA_RATIO_SUM']
    score = row['SCORE'] * point_a
    return min(score, 100)


def get_weeks_nums(start_day, end_day):
    """
    获取间隔之间的星期
    起始日期或者往前的最近的星期一为第一个星期一
    结束日期或者往前的最近的星期天为最后一个星期天
    :param start_day: 起始日期
    :param end_day:  结束日期
    :return:
    """
    first_mon = parse(start_day) + relativedelta(weekday=MO(-1))  # 一个月中的第一个星期一
    last_sun = parse(end_day) - relativedelta(days=1) + relativedelta(weekday=SU(-1))  # 一个月中的最后一个星期天
    # 一周为一个间隔，获取每个间隔的起始结束日期
    interval_days = list(
        rrule.rrule(rrule.WEEKLY, until=last_sun, dtstart=first_mon, byweekday=(MO, SU)))
    return interval_days


def get_cycle_convert_number(cycle, months_ago):
    """
    频率系数：‘周’为1，月为1/当月周数，季度为1/本月开始倒推3个月的自然周数，半年的为0
    :param months_ago:
    :param cycle:
    :return:
    """
    if cycle == 1:  # 周
        stats_month = get_custom_month(months_ago)
        start_day = stats_month[0]
        end_day = stats_month[1]
        return len(get_weeks_nums(start_day, end_day)) / 2, 1
    elif cycle == 2:  # 月
        stats_month = get_custom_month(months_ago)
        start_day = stats_month[0]
        end_day = stats_month[1]
        res = 1 / (len(get_weeks_nums(start_day, end_day)) / 2)
        return res, res
    elif cycle == 3:  # 季
        start_day = get_custom_month(months_ago - 3)[0]
        end_day = get_custom_month(months_ago)[1]
        res = 1 / (len(get_weeks_nums(start_day, end_day)) / 2)
        return res, res
    elif cycle == 4:  # 半年
        return 0, 0
    return 0, 0


def get_convert_points(df, cycle, convert_number):
    """
    高风险点换算数=Σ（高风险点数*5*频率换算数）
    中风险点换算数=Σ（中风险点数*2.5*频率换算数）
    低风险点换算数=Σ（低风险点数*1*频率换算数）
    频率换算数：‘周’为1*当月周数，其他均为1
    :param df:
    :param cycle:
    :return:
    """
    result = df[df['LEVEL'] == 1].index.size * 5
    result += df[df['LEVEL'] == 2].index.size * 2.5
    result += df[df['LEVEL'] == 3].index.size * 1
    return result * convert_number


def stats_control_frequency(months_ago):
    rst_index_score = []
    station_data = ZHANDUAN_DPID_DATA.copy()
    stats_month = get_custom_month(months_ago)
    start_day = stats_month[0]
    end_day = stats_month[1]
    check_address_and_risk_control = cache_client.get('check_address_score_config_and_risk_control')
    risk_control_level_rate = {1: 5, 2: 2.5, 3: 1}  # 危害等级得分
    score_dict = {}

    # 检查地点配置站段归类
    check_address = CHECK_ADDRESS
    # 检查地点与风险控制关联
    check_address_and_risk_control = check_address_and_risk_control[
        check_address_and_risk_control['IS_DELETE'] == 0].copy()
    check_address_and_risk_control.rename(columns={'NAME': 'RISK_CONTROL_NAME'}, inplace=True)
    check_address = pd.merge(pd.DataFrame(check_address,
                                          columns=['PK_ID', 'TYPE3', 'TYPE', 'FK_CHECK_POINT_ID', 'FK_DEPARTMENT_ID',
                                                   'NAME', 'ADDRESS_NAME']),
                             check_address_and_risk_control,
                             left_on='PK_ID', right_on='FK_CHECK_ADDRESS_SCORE_CONFIG_ID')

    check_address['CHECK_DAY_NUMBER'] = check_address['CHECK_DAY_NUMBER'].astype('int', errors='ignore')
    check_address['RC_ID'] = check_address['RISK_STATISTICS_IDS'].dropna().apply(lambda x: x.split(',')).\
        apply(lambda x: 1 if '48' in x else 0)
    check_address = check_address[check_address['RC_ID'] == 1]

    for department_id, department_group in check_address.groupby(['TYPE3']):
        department_group = department_group.sort_values(['CHECK_DAY_NUMBER'], ascending=False)
        department_records_dict = {}  # 存储部门不同周期的检查信息
        department_dict = score_dict.setdefault(department_id, {})
        address_fail_items = department_dict.setdefault('ADDRESS', {})
        department_deduct = {}
        for cycle, cycle_group in department_group.groupby(['CHECK_DAY_NUMBER'], sort=False):
            # 以周期分组
            check_records = department_records_dict.setdefault(cycle, get_check_records(department_id, cycle,
                                                                                        department_records_dict,
                                                                                        months_ago))

            cycle_convert_number, score_cycle_convert_number = get_cycle_convert_number(
                cycle, months_ago)  # 频率换算系数, 计分频率换算系数

            for config_id, config_group in cycle_group.groupby('PK_ID'):
                # 以地点配置分组，一条地点配置会对应n调风险卡控点配置
                score = 0
                # 1: 地点，对应FK_DEPARTMENT_ID 2： 重要检查点，对应FK_CHECK_POINT_ID
                if config_group.iloc[0]['TYPE'] == 1:
                    address_match_key = 'FK_DEPARTMENT_ID'
                    address_name = config_group.iloc[0]['NAME']
                else:
                    address_match_key = 'FK_CHECK_POINT_ID'
                    address_name = config_group.iloc[0]['ADDRESS_NAME']
                if cycle == 1:
                    # 周
                    interval_days = get_weeks_nums(start_day, end_day)
                    interval_list = [interval_days[i: i + 2] for i in range(0, len(interval_days), 2)]
                    for interval in interval_list:
                        records = check_records[
                            (check_records['SUBMIT_TIME'].between(interval[0], interval[1]))
                            & (check_records[address_match_key] == config_group.iloc[0][address_match_key])]
                        if not records.empty:
                            # 检查信息中有一栏RISK_CONTROL_ID，形式为'219, 1233'记录着风险点id
                            records = records.drop("RISK_CONTROL_IDS", axis=1).join(
                                records["RISK_CONTROL_IDS"].str.split(",", expand=True).stack().reset_index(
                                    level=1, drop=True).rename("RISK_CONTROL_ID"))
                            records.dropna(subset=['RISK_CONTROL_ID'], inplace=True)
                            records = records[records['RISK_CONTROL_ID'].str.isdigit()]
                            records['RISK_CONTROL_ID'] = records['RISK_CONTROL_ID'].astype('int')
                            records.drop_duplicates(subset=['SUBMIT_TIME', 'RISK_CONTROL_ID'], inplace=True)
                        for _, row in config_group.iterrows():
                            if records.empty or\
                                    records[records['RISK_CONTROL_ID'] == row['FK_RISK_CONTROL_ID']].index.size\
                                    < row['NUMBER']:
                                address_fail_items.setdefault(address_name, set()).add(row['RISK_CONTROL_NAME'])
                            else:
                                score += risk_control_level_rate.get(row['LEVEL']) * score_cycle_convert_number

                elif cycle in (2, 3, 4):
                    # 月、季、半年
                    records = check_records[check_records[address_match_key] == config_group.iloc[0][address_match_key]]
                    if not records.empty:
                        records = records.drop("RISK_CONTROL_IDS", axis=1).join(
                            records["RISK_CONTROL_IDS"].str.split(",", expand=True).stack().reset_index(
                                level=1, drop=True).rename("RISK_CONTROL_ID"))
                        records.dropna(subset=['RISK_CONTROL_ID'], inplace=True)
                        records = records[records['RISK_CONTROL_ID'].str.isdigit()]
                        records['RISK_CONTROL_ID'] = records['RISK_CONTROL_ID'].astype('int')
                        records.drop_duplicates(subset=['SUBMIT_TIME', 'RISK_CONTROL_ID'], inplace=True)

                    for _, row in config_group.iterrows():
                        if records.empty or records[records['RISK_CONTROL_ID'] == row['FK_RISK_CONTROL_ID']].index.size\
                                < row['NUMBER']:
                            department_deduct[cycle] = department_deduct.setdefault(cycle, 0)\
                                                       + risk_control_level_rate.get(row['LEVEL'], 0)
                            address_fail_items.setdefault(address_name, set()).add(row['RISK_CONTROL_NAME'])
                        else:
                            score += risk_control_level_rate.get(row['LEVEL']) * score_cycle_convert_number
                else:
                    pass

                department_dict['SCORE'] = department_dict.get('SCORE', 0) + score
                department_dict['ITEM_COUNT'] = department_dict.get('ITEM_COUNT', 0) + config_group.index.size
                department_dict['FORMULA_RATIO_SUM'] = department_dict.get('FORMULA_RATIO_SUM', 0) + get_convert_points(
                    config_group,
                    cycle,
                    cycle_convert_number)

        del department_records_dict

    rst_data = station_data.copy()

    append_df = rst_data.apply(lambda r: add_data_col(r, score_dict), result_type='expand', axis=1)
    rst_data = rst_data.join(append_df)
    rst_data['SCORE'] = rst_data.apply(lambda r: calc_score(r), axis=1)

    # 保存中间计算过程
    calc_basic_data_rst = format_export_basic_data(rst_data, MainType.control_frequency,
                                                   ControlFrequencyDetailType.stats_control_frequency,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.control_frequency,
                                     ControlFrequencyDetailType.stats_control_frequency,
                                     index_type=INDEX_TYPE)

    column = f'SCORE_a_{HIERARCHY}'

    rst_data = pd.DataFrame(index=rst_data['DEPARTMENT_ID'],
                            data=rst_data['SCORE'].values,
                            columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summarize_operation_set(rst_data, station_data, column,
                            HIERARCHY, INDEX_TYPE, MainType.control_frequency,
                            ControlFrequencyDetailType.stats_control_frequency,
                            months_ago)
    rst_index_score.append(rst_data[[column]])

    return rst_index_score


def get_check_records(department_id, cycle, department_records_dict, months_ago):
    stats_month = get_custom_month(months_ago)
    start_day = stats_month[0]
    end_day = stats_month[1]
    check_records = None
    if cycle == 1:
        # 周
        # 从最后一个星期天开始，直到包含开始日期的那个星期
        first_mon = parse(start_day) + relativedelta(weekday=MO(-1))
        last_sun = parse(end_day) - relativedelta(days=1) + relativedelta(weekday=SU(-1))
        check_records = pd_query(CHECK_INFO_AND_ADDRESS_SQL.format(department_id,
                                                                   first_mon.strftime('%Y-%m-%d'),
                                                                   last_sun.strftime('%Y-%m-%d')),
                                 parse_dates=['SUBMIT_TIME'])
    elif cycle == 2:
        # 月
        # 直接查询统计月份
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        for key in (3, 4):
            more_records = department_records_dict.get(key)
            if more_records is not None:
                check_records = more_records[more_records['SUBMIT_TIME'].between(parse(start_day), parse(_end_day))]
                return check_records
        check_records = pd_query(CHECK_INFO_AND_ADDRESS_SQL.format(department_id, start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])
    elif cycle == 3:
        # 季度
        # 反推3个月
        _start_day = get_custom_month(months_ago - 2)[0]
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        for key in (4,):
            more_records = department_records_dict.get(key)
            if more_records is not None:
                check_records = more_records[more_records['SUBMIT_TIME'].between(parse(_start_day), parse(_end_day))]
                return check_records
        check_records = pd_query(CHECK_INFO_AND_ADDRESS_SQL.format(department_id, _start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])
    elif cycle == 4:
        # 半年
        # 反推6个月
        _start_day = get_custom_month(months_ago - 5)[0]
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        check_records = pd_query(CHECK_INFO_AND_ADDRESS_SQL.format(department_id, _start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])

    if check_records is not None and not check_records.empty:
        check_records['RISK_CONTROL_IDS'] = check_records['RISK_CONTROL_IDS'].replace('', np.NaN)
    check_records.dropna(subset=['RISK_CONTROL_IDS'], inplace=True)
    return check_records


def handle(months_ago, major, risk_id):
    # 部门按站段聚合
    init_common_data(major, risk_id)
    _get_sql_data(months_ago)
    # 子指数计算过程
    child_index_func = [
        stats_control_frequency
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    child_index_list = [ControlFrequencyDetailType.stats_control_frequency]
    update_major_maintype_weight(index_type=INDEX_TYPE, main_type=MainType.control_frequency,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summarize_child_index(child_score, ZHANDUAN_DPID_DATA, INDEX_TYPE, MainType.control_frequency, months_ago,
                          item_name, item_weight)
    current_app.logger.debug(
        '├── └── control_frequency index has been figured out!')


@validate_exec_month
def execute(months_ago, major, risk_id):
    handle(months_ago, major, risk_id)


if __name__ == '__main__':
    execute(-1)
