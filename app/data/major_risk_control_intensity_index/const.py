#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""

INDEX_TYPE = -3

HIERARCHY = 3


class MainType:
    """
    卡控频率指数	control_frequency
    较大及以上问题	great_risk_problem
    一般及以上问题	    general_risk_problem
    关键问题卡控指数	    key_problem_control
    """
    control_frequency = 2
    great_risk_problem = 3
    general_risk_problem = 4
    key_problem_control = 5


class ControlFrequencyDetailType:
    stats_control_frequency = 1


class GreatRiskProblemDetailType:
    """
    一般及以上问题卡控: stats_general_risk_problem_control
    较大及以上问题卡控: stats_great_risk_problem_control
    关键问题卡控: stats_key_problem_control
    """
    stats_great_risk_problem_control = 1


class GeneralRiskProblemDetailType:
    """
    一般及以上问题卡控: stats_general_risk_problem_control
    较大及以上问题卡控: stats_great_risk_problem_control
    关键问题卡控: stats_key_problem_control
    """
    stats_general_risk_problem_control = 1


class KeyControlQualityDetailType:
    """
    问题卡控: stats_problem_control
    一般及以上问题卡控: stats_general_risk_problem_control
    关键问题卡控: stats_key_problem_control
    """
    stats_key_problem_control = 1


