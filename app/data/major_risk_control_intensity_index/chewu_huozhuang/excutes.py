from app.data.major_risk_control_intensity_index.combine_child_index import execute
from app.data.major_risk_control_intensity_index.index_dict import index


def use_api(months_ago):
    execute(months_ago, '车务', index['车务-货装安全'])