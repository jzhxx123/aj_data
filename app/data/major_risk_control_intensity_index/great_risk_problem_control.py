import pandas as pd
from flask import current_app
from app.data.major_risk_control_intensity_index.common_data import init_common_data
from app.data.major_risk_control_intensity_index.const import GreatRiskProblemDetailType, INDEX_TYPE, MainType, HIERARCHY
from app.data.control_intensity_index.util import validate_exec_month
from app.data.util import update_major_maintype_weight, pd_query
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.major_risk_control_intensity_index.common import summarize_child_index, summarize_operation_set, \
    write_export_basic_data_to_mongo, format_export_basic_data
from app.data.control_intensity_index import cache_client
from app.data.control_intensity_index.common_sql import MAJOR_SQL


def _get_sql_data(months_ago):
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, CHECK_ADDRESS, RISK_AND_ITEM

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    CHECK_ADDRESS = cache_client.get('CHECK_ADDRESS', True)
    RISK_AND_ITEM = cache_client.get('RISK_AND_ITEM', True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def check_item_list():
    item_id = RISK_AND_ITEM.copy()
    item_id = item_id['ITEM_IDS'].apply(lambda x: x.split(',')).tolist()[0]
    return item_id


def general_risk_problem_control_add_data_col(row, score_dict):
    department_dict = score_dict.get(row['DEPARTMENT_ID'])
    content = '检查项目总数：{}，未覆盖的项目数量：{} <br/>'
    content += '未发现相应问题的检查项目：<br/>{}'
    if department_dict:
        score = department_dict.get('SCORE', 0)
        failed_items = department_dict.get('FAILED_ITEMS', [])
        content = content.format(department_dict['ITEMS_COUNT'],
                                 len(failed_items),
                                 '<br/>'.join(failed_items))
    else:
        score = 0
        content = content.format(0, 0, '')
    score = min(score, 100)
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def get_check_item_and_problem_by_department(department_row, check_items, major_items_dict, problem_base):
    """
    根据部门获取检查项目关联检查问题数据集
    :param department_row:
    :param check_items:
    :param major_items_dict:
    :param problem_base:
    :return:
    """
    department_id = department_row['DEPARTMENT_ID']
    if department_id in check_item_and_problem_dict.keys():
        return check_item_and_problem_dict[department_id]
    major = department_row['MAJOR']
    item_df = check_items[check_items['FK_DEPARTMENT_ID'] == department_id]
    # 专业的项目也要算
    if major == '客运':
        extra_items = major_items_dict.get('车务', [])
    elif major == '工电':
        extra_items = major_items_dict.get('工务', []) + major_items_dict.get('电务', []) \
                      + major_items_dict.get('供电', [])
    else:
        extra_items = major_items_dict.get(major, [])
    item_df = item_df.append(check_items[check_items['CHECK_ITEM_ID'].isin(extra_items)], sort=False)

    check_item_and_problem = pd.merge(pd.DataFrame(item_df,
                                                   columns=['CHECK_ITEM_ID', 'FK_DEPARTMENT_ID', 'HIERARCHY',
                                                            'NAME']),
                                      pd.DataFrame(problem_base,
                                                   columns=['FK_CHECK_ITEM_ID', 'PROBLEM_BASE_ID', 'RISK_LEVEL']),
                                      left_on='CHECK_ITEM_ID', right_on='FK_CHECK_ITEM_ID')
    check_item_and_problem_dict[department_id] = check_item_and_problem
    return check_item_and_problem


check_item_and_problem_dict = {}


def stats_problem_control(months_ago, risk_level, months_range, score_column, detail_type):
    rst_index_score = []
    score_dict = {}

    station_data = ZHANDUAN_DPID_DATA.copy()
    check_items = cache_client.get('check_items')
    check_items['TYPE'].fillna('', inplace=True)
    check_items = check_items[(check_items['IS_DELETE'] == 0)
                              & ~(check_items['TYPE'].str.contains('1'))
                              & (check_items['HIERARCHY'] <= 3)
                              & (check_items['HIERARCHY'] > 1)]
    check_items = check_items.rename(columns={'PK_ID': 'CHECK_ITEM_ID'})

    problem_base = cache_client.get('problem_base')
    problem_base = problem_base[(problem_base['IS_DELETE'] == 0) & (problem_base['STATUS'] == 3)]
    problem_base = problem_base.rename(columns={'PK_ID': 'PROBLEM_BASE_ID'})

    # 处理专业检查项目
    # 三级的检查项目和没有下级的二级项目
    major_items = pd.merge(check_items, pd_query(MAJOR_SQL), left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    parent_items = list(check_items[check_items['HIERARCHY'] == 3].groupby('PARENT_ID').groups)
    major_items.drop(major_items[(major_items['HIERARCHY'] == 2)
                                 & (major_items['CHECK_ITEM_ID'].isin(parent_items))].index, inplace=True)
    major_items_dict = {}
    for major, major_df in major_items.groupby('MAJOR'):
        major_items_dict[major] = major_df['CHECK_ITEM_ID'].tolist()

    # 检查问题记录数据
    check_problems_list = []
    for i in range(months_range):
        check_problems_list.append(cache_client.get('check_problem_and_info', months_ago=months_ago - i))
    check_problems = pd.concat(check_problems_list, ignore_index=True)
    check_problems = df_merge_with_dpid(check_problems, DEPARTMENT_DATA)
    check_problems = pd.DataFrame(check_problems, columns=['PK_ID', 'FK_PROBLEM_BASE_ID', 'TYPE3'])
    item_id_list = check_item_list()
    for _, row in station_data.iterrows():
        department_id = row['DEPARTMENT_ID']
        department_dict = score_dict.setdefault(department_id, {})

        check_item_and_problem = get_check_item_and_problem_by_department(row, check_items, major_items_dict,
                                                                          problem_base)
        check_item_and_problem = check_item_and_problem[check_item_and_problem['RISK_LEVEL'] <= risk_level]
        check_item_and_problem = pd.merge(check_item_and_problem,
                                          check_problems[check_problems['TYPE3'] == department_id],
                                          how='left', left_on='PROBLEM_BASE_ID', right_on='FK_PROBLEM_BASE_ID')
        check_item_and_problem.fillna(0, inplace=True)
        check_item_count = 0  # 该部门检查项目数
        found_item_count = 0  # 已检查的项目数
        for item, item_df in check_item_and_problem.groupby('CHECK_ITEM_ID'):
            if str(item) in item_id_list:
                check_item_count += 1
                if item_df[item_df['PK_ID'] > 0].empty:
                    department_dict['FAILED_ITEMS'] = department_dict.get('FAILED_ITEMS', [])
                    department_dict['FAILED_ITEMS'].append(item_df.iloc[0]['NAME'])
                else:
                    found_item_count += 1
        # a=100/该单位有效检查项目数（非管理类）。得分=∑加分
        department_dict['SCORE'] = (100 / check_item_count) * found_item_count if check_item_count > 0 else 0
        department_dict['ITEMS_COUNT'] = check_item_count

    rst_data = ZHANDUAN_DPID_DATA.copy()
    append_df = rst_data.apply(lambda r: general_risk_problem_control_add_data_col(r, score_dict),
                               result_type='expand', axis=1)
    rst_data = rst_data.join(append_df)

    # 保存中间计算过程
    calc_basic_data_rst = format_export_basic_data(rst_data, MainType.great_risk_problem,
                                                   detail_type,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.great_risk_problem,
                                     detail_type,
                                     index_type=INDEX_TYPE)

    column = f'SCORE_{score_column}_{HIERARCHY}'

    rst_data = pd.DataFrame(index=rst_data['DEPARTMENT_ID'],
                            data=rst_data['SCORE'].values,
                            columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summarize_operation_set(rst_data, ZHANDUAN_DPID_DATA, column,
                            HIERARCHY, INDEX_TYPE, MainType.general_risk_problem,
                            detail_type,
                            months_ago)
    rst_index_score.append(rst_data[[column]])
    score_dict.clear()
    return rst_index_score


def stats_great_risk_problem_control(months_ago):
    risk_level = 2
    months_range = 2
    return stats_problem_control(months_ago, risk_level, months_range, 'b',
                                 GreatRiskProblemDetailType.stats_great_risk_problem_control)


def handle(months_ago, major, risk_id):
    # 部门按站段聚合
    init_common_data(major, risk_id)
    _get_sql_data(months_ago)

    child_index_func = [
        stats_great_risk_problem_control
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['b']]
    item_weight = [1]
    child_index_list = [
                        GreatRiskProblemDetailType.stats_great_risk_problem_control]
    update_major_maintype_weight(index_type=INDEX_TYPE, main_type=MainType.great_risk_problem,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summarize_child_index(child_score, ZHANDUAN_DPID_DATA, INDEX_TYPE, MainType.great_risk_problem, months_ago,
                          item_name, item_weight)
    current_app.logger.debug(
        '├── └── total_control_quality index has been figured out!')


@validate_exec_month
def execute(months_ago, major, risk_id):
    handle(months_ago, major, risk_id)


if __name__ == '__main__':
    execute(-1)