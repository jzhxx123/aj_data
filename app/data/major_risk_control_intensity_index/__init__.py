#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)
from . import (common,
               cheliang_hzbz, cheliang_ldaq,
               chewu_diaoche, chewu_huozhuang, chewu_jflc,
               dianwu_dianwaixiu, dianwu_laoan, dianwu_shigong,
               gongdian_sgfx, gongdian_zlsbyyfx,
               gongwu_fangduan, gongwu_laoan, gongwu_sgaq, gongwu_zlsb,
               jiwu_cwcz, jiwu_jdlw, jiwu_laoan)