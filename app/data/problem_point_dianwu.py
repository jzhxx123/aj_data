#!/usr/bin/python3
# -*- coding: utf-8 -*-

'''
    author: ermenghuan
    date: 2018/08/15
    desc:  查找电务安全信息SQL
'''

import calendar
import json
import time
import datetime
import traceback
from datetime import datetime as dt
from pymysql.err import OperationalError

import numpy as np
import pandas as pd

from app import mongo
from app.data.util import (get_coll_prefix, get_history_months,
                           get_previous_month, paste_daily_to_monthly,
                           paste_monthly_to_history, pd_query,
                           write_bulk_mongo)

def get_problem_point_dianwu(mon_point):
    #查找电务处的问题项点数据mon_point 要查寻时间再定现在默认是当前时间  不能用啊？？？？
    data = []
    mon_point = '2018-07-31 22:00:00'
    DIANWU_TYPE2_NUM = '1ACE7D1C80B34456E0539106C00A2E70KSC'
    SQL_PROBLEM_POINT_DIANWU = "SELECT B.problem FROM " \
                               "(SELECT t_check_problem.problem_point AS problem, t_check_problem.execute_department_id AS tc_id, t_check_problem.FK_CHECK_INFO_ID as ck_id FROM t_check_problem WHERE DATE_FORMAT(submit_time, '%Y%m') = DATE_FORMAT('{}','%Y%m')) AS B " \
                               "INNER JOIN " \
                               "(SELECT t_department.department_id AS dp_id FROM t_department WHERE t_department.type2 = '{}') AS A " \
                               "ON B.tc_id = A.dp_id".format(mon_point,DIANWU_TYPE2_NUM)
    print(SQL_PROBLEM_POINT_DIANWU)
    try:
        data = pd_query(SQL_PROBLEM_POINT_DIANWU)
        # data = pd.read_sql(SQL_PROBLEM_POINT_DIANWU,)
    except Exception as e:
        print(e)
        print('except: get mysql map_check_problem[{}] data.'.format(mon_point[0]))
        return None
    return data


def get_super_check_id(mon_point):
    data = []
    SQL_SUPER_CHECK_NUM_DIANWU = "select t_check_info.PK_ID as ck_id, t_check_info.CHECK_TYPE as check_type from t_check_info where DATE_FORMAT(SUBMIT_TIME, '%Y%m') = DATE_FORMAT('2018-08-31 22:00:00','%Y%m') and CHECK_TYPE > 400;"
    try:
        data = pd_query(SQL_SUPER_CHECK_NUM_DIANWU)
    except:
        print('except: get mysql map_check_problem[{}] data.'.format(mon_point[0]))
        return None
    return data

if __name__ == '__main__':
    a = get_problem_point_dianwu(1)
    print(a)