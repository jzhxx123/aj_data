#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""


INDEX_TYPE = -2

HIERARCHY = 4


class MainType:
    """
    检查力度指数	check_intensity
    评估力度指数	evaluate_intensity
    考核力度指数	assess_intensity
    检查均衡度	    check_evenness
    问题暴露度	    problem_exposure
    问题整改	    problem_rectification
    自控能力        self_control_ability
    关键卡控
    """
    check_intensity = 1
    evaluate_intensity = 2
    assess_intensity = 3
    check_evenness = 4
    problem_exposure = 5
    problem_rectification = 6
    self_control_ability = 7
    key_control = 8


class CheckIntensityDetailType:
    """
    检查力度指数子指数
    stats_check_per_person: 人均检查频次
    stats_check_problem_ratio: 问题查处力度
    stats_check_problem_assess_ratio: 查处问题考核率
    stats_score_per_check_problem：问题平均质量分
    stats_score_per_person: 人均质量分
    stats_yecha_ratio: 夜查率
    stats_check_address_ratio: 覆盖率
    stats_media_intensity: 监控调阅力度
    stats_general_risk_problem_ratio: 一般及以上风险问题占比
    """
    stats_check_per_person = 2
    stats_check_problem_ratio = 3
    stats_check_problem_assess_ratio = 4
    stats_score_per_check_problem = 5
    stats_score_per_person = 6
    stats_yecha_ratio = 7
    stats_great_risk_problem_ratio = 8
    stats_check_address_ratio = 9
    stats_media_intensity = 10
    stats_general_risk_problem_ratio = 11


class EvaluateIntensityDetailType:
    evaluate_deduct_score = 1


class CheckEvennessDetailType:
    """
    检查均衡度子指数
    stats_check_day_evenness： 检查日期均衡度
    stats_check_address_evenness： 地点均衡度
    stats_check_hour_evenness： 检查时段均衡度
    """
    stat_check_problem_evenness = 1
    stats_check_day_evenness = 2
    stats_check_address_evenness = 3
    stats_check_hour_evenness = 4


class ProblemExposureDetailType:
    """
    问题暴露度子指数
    stats_total_problem_exposure： 普遍性暴露
    stats_hidden_problem_exposure: 较严重隐患暴露
    stats_banzu_problem_exposure： 班组问题暴露
    stats_problem_exposure: 事故隐患问题暴露度
    """
    stats_total_problem_exposure = 1
    stats_hidden_problem_exposure = 2
    stats_problem_exposure = 3
    stats_banzu_problem_exposure = 4
    other_problem_exposure = 5


class AssessIntensityDetailType:
    """
    考核力度子指数
    stats_check_problem_assess_ratio： 人均考核问题数
    stats_assess_money_per_person： 人均考核金额
    stats_award_return_ratio： 返奖率
    stats_problem_assess_ratio: 考核率
    """
    stats_check_problem_assess_ratio = 1
    stats_assess_money_per_person = 2
    stats_award_return_ratio = 3
    stats_problem_assess_ratio = 4


class ProblemRectificationDetailType:
    """
    stats_rectification_overdue: 超期
    stats_check_evaluate：履职整改
    stats_repeatedly_index： 问题控制
    stats_rectification_review： 整改复查
    stats_rectification_effect： 整改成效
    stats_peril_renovation：隐患整治
    """
    stats_rectification_overdue = 1
    stats_check_evaluate = 2
    stats_repeatedly_index = 3
    stats_rectification_review = 4
    stats_peril_renovation = 5
    stats_rectification_effect = 6


class SelfControlAbilityDetailType:
    """
    stats_self_check_per_person: 换算车间自查频次
    stats_score_per_person: 车间人均质量分
    stats_serious_problem_ratio: 较严重及以上风险问题
    other_problem_exposure他查问题扣分
    stats_evaluate_intensity: 评价力度指数
    stats_check_problem_assess_radio: 换算车间考核问题数
    stats_yecha_ratio: 夜查率
    stats_check_address_ratio： 覆盖率
    stats_medium_quality_above_ratio: 中高质量问题占比
    stats_assess_money_per_person: 换算单位考核金额
    """
    stats_self_check_per_person = 1
    stats_score_per_person = 2
    stats_serious_problem_ratio = 3
    other_problem_exposure = 4
    stats_evaluate_intensity = 5
    stats_assess_problem_ratio = 6
    stats_yecha_ratio = 7
    stats_check_address_ratio = 8
    stats_medium_quality_above_ratio = 9
    stats_check_hour_evenness = 10
    stats_assess_money_ratio = 11


class KeyControlDetailType:
    stats_key_control_1 = 1
