#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/7
Description: 
"""
import time
import logging
from functools import wraps

import pandas as pd
from flask import current_app

from app import mongo
from app.data.index.common import df_merge_with_dpid
from app.data.util import pd_query, get_history_months
from app.utils.common_func import get_today
from app.utils.data_update_models import UpdateMonth


def group_and_merge_with_dpid(data, dpid_data, col_group_by, col_sum, col_out):
    """
    将传过来的数据集与部门数据连接之后再按照字段分组统计
    :param data:
    :param dpid_data:
    :param col_group_by: 分组统计字段
    :param col_sum: 求和字段名
    :param col_out: 输出字段名
    :return:
    """
    # if not col_sum:
    #     col_sum = 'COUNT'
    #     # data.loc[:, col_sum] = 1
    #     data[col_sum] = 1
    if not col_sum:
        df = data.groupby(col_group_by).size().to_frame(name=col_out)
    else:
        df = data.groupby(col_group_by)[col_sum].sum().to_frame(name=col_out)
    df = df.reset_index().rename(columns={col_group_by: 'FK_DEPARTMENT_ID'})
    return df_merge_with_dpid(df, dpid_data)


def get_department_classify(classify_config_id, department_classify_config):
    """
    从部门分类ID判断部门所属的分类
    :param classify_config_id:
    :param department_classify_config:
    :return:
    """
    for k, v in department_classify_config.items():
        if classify_config_id in v:
            return k
    return None


def get_work_load_from_sql(sql):
    df = pd_query(sql)
    if df.empty:
        return 0
    return df.iloc[0]['COUNT']


def get_past_basic_data(main_type, major, detail_type, months_ago, classify, length=3):
    """
    从workshop_health_index_basic_data中获取某指数过去月份的基数数据
    :param main_type:
    :param major:
    :param detail_type:
    :param months_ago:
    :param classify:
    :param length:
    :return:
    """
    past_months = [get_history_months(i)[0] for i in range(months_ago-1, months_ago-length, -1)]
    condition = {'MAJOR': major, 'MON': {'$in': past_months}, 'CLASSIFY': classify, 'MAIN_TYPE': main_type,
                 'DETAIL_TYPE': detail_type}
    record = mongo.db['monthly_workshop_health_index_basic_data'].find(condition, {'_id': 0, 'BASIC_CURRENT': 1})
    df = pd.DataFrame(list(record))
    return df


def validate_exec_month(func):
    """一个检查指数计算月份是否有效的装饰器

    Arguments:
        func  -- 指数计算入口函数

    Raises:
        ValueError -- [传入的月份错误]

    Returns:
        func --返回被调函数
    """

    @wraps(func)
    def wrapper(months_ago: UpdateMonth, *args, **kwargs):
        start = time.time()
        if isinstance(months_ago, UpdateMonth):
            # 如果是执行专业重点指数，则需要传入risk参数
            if months_ago.risk is not None:
                risk = months_ago.risk
                args = (risk, )
            months_ago = months_ago.get_month_delta()
        if months_ago < 0:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(
                f'months_agos-[{months_ago}] should not less than 0')

        months_ago *= -1
        if get_history_months(months_ago)[0] < 201710:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(f'months_agos-[{months_ago}] param error')

        # 判断是否是25-月底，如果是则更新指数
        if months_ago == 0 and get_today().day < current_app.config.get(
                'UPDATE_DAY'):
            return 'NO NEED TO UPDATE'

        current_app.logger.debug(
            f'── {func.__module__} index computing starts.')
        try:
            func(months_ago, *args, **kwargs)
        except Exception:
            logging.exception('Index Error')
            return 'ERROR'
        current_app.logger.debug(f'── {func.__module__} index done.')
        cost = round(time.time() - start, 3)
        current_app.logger.debug(f'── {func.__module__} had costed time --- {cost}.')
        return 'OK'

    return wrapper