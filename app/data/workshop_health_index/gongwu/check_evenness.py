#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    date: 2019/07/31
    desc: 检查均衡度指数
"""

import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, CHECK_POINT_DEPARTMENT_SQL, BANZU_DEPARTMENT_SQL, CHECK_PROBLEM_COUNT_SQL)
from app.data.workshop_health_index.common.common import (
    summizet_child_index)
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.index.util import get_custom_month
from app.data.util import pd_query, get_history_months, update_major_maintype_weight
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, CheckEvennessDetailType
from app.data.workshop_health_index.gongwu.common_data import cache_client
from app.data.workshop_health_index.gongwu.common import MAJOR
from app.data.workshop_health_index.common import check_eveness


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


# 计算检查点扣分
def _calc_score_by_formula(row, column, avg_column):
    """
    实际检查人次/比较值  ＜ 1-60%的 扣分
    实际检查人次/比较值 ＞ 1+600%  扣分
    :return:
    """
    if row[column] == 0:
        return -10
    if row[avg_column] == 0:
        return -5
    else:
        _ratio = (row[column] - row[avg_column]) / row[avg_column]
        if _ratio >= 4:
            return -5
        elif _ratio <= -0.5:
            return -5
        else:
            return 0


# 部门按车间聚合
def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, DAILY_CHECK_BANZU_COUNT, \
        WORK_LOAD, CHECK_PROBLEM_COUNT, CHECK_INFO_DF

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    stats_month = get_custom_month(months_ago)
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 单位总人数
    WORK_LOAD = cache_client.get('WORK_LOAD', True)

    DAILY_CHECK_BANZU_COUNT = pd_query(DAILY_CHECK_BANZU_COUNT_SQL)

    CHECK_INFO_DF = cache_client.get('check_info_and_person_with_check_time', months_ago=months_ago)
    CHECK_INFO_DF = df_merge_with_dpid(CHECK_INFO_DF,
                                       pd.DataFrame(DEPARTMENT_DATA, columns=['DEPARTMENT_ID', 'TYPE4']))
    CHECK_INFO_DF.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)


def _calc_problem_evenness_ratio(row, column, major_column=None):
    """
    问题均衡度
    :param row:
    :param column:
    :param major_column:
    :return:
    """
    return min(100, row[column] * 100)


def _stat_check_problem_evenness(months_ago):
    check_problem = df_merge_with_dpid(cache_client.get('check_problem_and_responsible', months_ago=months_ago),
                                       DEPARTMENT_DATA)
    problem_base = cache_client.get('problem_base')
    return check_eveness.stat_check_problem_evenness(check_problem, problem_base, months_ago,
                                                     _calc_problem_evenness_ratio, _choose_dpid_data)


# 检查时间均衡度 - 检查时段均衡度
def _stats_check_hour_evenness(months_ago):
    return check_eveness.stats_check_hour_evenness(CHECK_INFO_DF, DAILY_CHECK_BANZU_COUNT, DEPARTMENT_DATA, months_ago,
                                                   _choose_dpid_data)


# 检查时间均衡度 - 检查日期均衡度
def _stats_check_day_evenness(months_ago):
    return check_eveness.stats_check_day_evenness(CHECK_INFO_DF, DAILY_CHECK_BANZU_COUNT, DEPARTMENT_DATA, months_ago,
                                                  _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_eveness.stats_check_address_evenness(
        CHECK_POINT_COUNT_SQL, CHECK_POINT_DEPARTMENT_SQL, CHECK_BANZU_COUNT_SQL, BANZU_DEPARTMENT_SQL,
        DEPARTMENT_DATA, _choose_dpid_data, _calc_score_by_formula, months_ago, cache_client)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stat_check_problem_evenness,  # a
        _stats_check_day_evenness,  # b
        _stats_check_address_evenness,  # c
        _stats_check_hour_evenness  # d
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    item_weight = [0.55, 0.1, 0.35, 0.1]
    child_index_list = [CheckEvennessDetailType.stat_check_problem_evenness,
                        CheckEvennessDetailType.stats_check_day_evenness,
                        CheckEvennessDetailType.stats_check_address_evenness,
                        CheckEvennessDetailType.stats_check_hour_evenness]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.check_evenness,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.check_evenness, months_ago,
                         item_name, item_weight, major=MAJOR)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
