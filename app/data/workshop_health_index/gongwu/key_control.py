#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/9/24
Description: 
"""

import pandas as pd
from flask import current_app

from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.index.util import get_custom_month
from app.data.util import update_major_maintype_weight

from app.data.workshop_health_index.common.common import summizet_child_index, summizet_operation_set
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, KeyControlDetailType
from app.data.workshop_health_index.gongwu.common_data import cache_client
from app.data.workshop_health_index.gongwu.common import MAJOR


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA
    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)

    current_app.logger.debug('|   └── esxtract data from mysql have done!')


def _stats_key_control_1(months_ago):
    rst_index_score = []
    rst_data = pd.DataFrame(CHEJIAN_DPID_DATA, columns=['DEPARTMENT_ID'])

    column = f'SCORE_a_{HIERARCHY}'
    rst_data[column] = 100
    rst_data.set_index('DEPARTMENT_ID', drop=True, inplace=True)

    summizet_operation_set(rst_data, _choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.key_control, KeyControlDetailType.stats_key_control_1,
                           months_ago)
    rst_index_score.append(rst_data[[column]])

    return rst_index_score


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    child_index_func = [
        _stats_key_control_1
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    item_name = [
        f'SCORE_{x}' for x in ['a']
    ]
    item_weight = [1]

    child_index_list = [KeyControlDetailType.stats_key_control_1]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.key_control,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.key_control, months_ago,
                         item_name, item_weight, [HIERARCHY], major=MAJOR)

    current_app.logger.debug(
        '├── └── self_control_ability index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass