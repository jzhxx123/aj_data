#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

MAJOR = '工务'

department_classify_config = {
    'A': (43,),
    'B': (308,),
    'C': (107,),
    'D': (139,),
    'E': (307,),
    'F': (140,),
    'G': (316,),
    'H': (311,),
    'I': (138,),
    'J': (310,),
    'K': (312, 313, 314, 259, 315, 260),
    'L': (126,),
    'M': (127,)
}

department_classify_desc = {
    'A': '线路车间普',
    'B': '桥路车间普',
    'C': '工电车间普',
    'D': '线路车间高',
    'E': '桥路车间高',
    'F': '线桥车间高',
    'G': '工电车间高',
    'H': '探伤（检）车间',
    'I': '综合机修车间',
    'J': '换轨车间',
    'K': '换岔车间',
    'L': '线路清筛车间',
    'M': '大机线路维修车间',
    'N': '大机道岔维修车间',
    'O': '站场标准化大修车间',
    'P': '风动卸砟车间',
    'Q': '道砟生产车间',
    'R': '线桥车间普'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])


def get_work_load(row, labor_time_df):
    classify = row['CLASSIFY']
    if classify in ('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K'):
        labor_time = labor_time_df[labor_time_df['TYPE4'] == row['DEPARTMENT_ID']]
        if not labor_time.empty:
            labor_time = labor_time.iloc[0]['COUNT']
        else:
            labor_time = 0
        return labor_time * 0.55 + row['COUNT'] * 0.45
    else:
        return row['COUNT']

