TOTAL_LABOR_TIME_SQL = """
SELECT
    gw.DEPARTID AS FK_DEPARTMENT_ID,
    SUM( gw.ON_RAILWAY_NUMBER * gw.COUNT ) / 3600 AS COUNT 
FROM
    (
    SELECT
        max(a.DEPARTID) as DEPARTID,
        max(a.ON_RAILWAY_NUMBER) as ON_RAILWAY_NUMBER,
        ( UNIX_TIMESTAMP( max(b.END_TIMES) ) - UNIX_TIMESTAMP( max(b.START_TIMES) ) ) AS COUNT 
    FROM
        gw_t_ps_dailyplan AS a
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) <= DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )  
        AND 
        b.DISPATCH_ORDER_STATUS = 1   
        group by a.pk_id ) AS gw 
GROUP BY
    gw.DEPARTID
"""