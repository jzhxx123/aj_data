#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description:
"""
from app.data.workshop_health_index.const import INDEX_TYPE, MainType
from app.data.workshop_health_index.jiwu import (check_intensity, check_evenness, problem_exposure, assess_intensity,
                                                 problem_rectification, self_control_ability)
from app.data.workshop_health_index.jiwu.common_data import init_common_data
from app.data.workshop_health_index.jiwu.common import MAJOR
from app.data.workshop_health_index.common import combine_child_index
from app.data.workshop_health_index.utils import validate_exec_month
from app.data.util import (update_major_maintype_weight)
from app.data.workshop_health_index.jiwu import cache_client


@validate_exec_month
def execute(months_ago):
    init_common_data(months_ago)
    for func in [
        check_intensity, check_evenness, problem_exposure, assess_intensity,
        problem_rectification, self_control_ability
    ]:
        func.execute(months_ago)
    child_index_list = [MainType.check_intensity,
                        MainType.assess_intensity,
                        MainType.check_evenness,
                        MainType.problem_exposure,
                        MainType.problem_rectification,
                        MainType.self_control_ability]
    child_index_weight = [0.3, 0.05, 0.15, 0.20, 0.1, 0.1]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR,
                                 child_index_list=child_index_list, child_index_weight=child_index_weight)
    chejian_data = cache_client.get('CHEJIAN_DPID_DATA', module_cache=True)
    combine_child_index.merge_child_index(chejian_data,
                                          MAJOR,
                                          months_ago,
                                          child_index_list=child_index_list,
                                          child_index_weight=child_index_weight)


if __name__ == '__main__':
    pass
