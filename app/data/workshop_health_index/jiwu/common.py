#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

department_classify_config = {
    'A': (32,),
    'B': (33,),
    'C': (8, 30, 281),
    'D': (29,),
    'E': (31,),
    'F': (34,)
}

department_classify_desc = {
    'A': '普速运用车间',
    'B': '动车运用车间',
    'C': '检修车间',
    'D': '整备车间',
    'E': '设备车间',
    'F': '救援车间'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])

MAJOR = '机务'

