#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Author: seerbigdata
Date: 2019/8/2
Description: 考核力度指数
"""


import pandas as pd
from flask import current_app
from app.data.workshop_health_index.common.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_POINT_SQL, MEDIA_COST_TIME_SQL)
from app.data.workshop_health_index.common.common_funcs import get_on_site_checked_count_data, \
    merge_problem_check_score, get_department_media_checked_time
from app.data.workshop_health_index.utils import group_and_merge_with_dpid
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.workshop_health_index.common.common import (summizet_child_index, )
from app.data.index.util import get_custom_month
from app.data.util import pd_query, update_major_maintype_weight

from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, CheckIntensityDetailType
from app.data.workshop_health_index.jiwu.common_data import cache_client
from app.data.workshop_health_index.common import check_intensity
from app.data.workshop_health_index.jiwu.common import MAJOR


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column):
    """
    与基数对比后得分情况表
    :param row:
    :param column:
    :param major_column:
    :return:
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_address_ratio(row, column, major_column=None):
    """
    覆盖率得分计算
    :param row:
    :param column:
    :param major_column:
    :return:
    """
    return min(100, row[column] * 100)


def _get_sql_data(months_ago):
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, CHECK_COUNT, PROBLEM_COUNT, \
        PROBLEM_SCORE, YECHA_COUNT, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA,\
        DEPARTMENT_DATA, STAFF_NUMBER, XIANCHANG_CHECK_COUNT
    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 单位总人数
    WORK_LOAD = cache_client.get('WORK_LOAD', True)

    XIANCHANG_CHECK_COUNT, YECHA_COUNT = get_on_site_checked_count_data(DEPARTMENT_DATA, months_ago, cache_client)

    # 检查问题数
    check_problem_and_info = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    PROBLEM_COUNT = check_problem_and_info[check_problem_and_info['RISK_LEVEL'] < 4]  # 低风险问题不统计
    PROBLEM_COUNT = group_and_merge_with_dpid(PROBLEM_COUNT, DEPARTMENT_DATA, 'FK_DEPARTMENT_ID',
                                              '', 'COUNT')

    # 问题质量分
    problem_base = cache_client.get('problem_base')
    problem_score = pd.merge(check_problem_and_info,
                             pd.DataFrame(problem_base, columns=['PK_ID', 'CHECK_SCORE']),
                             left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')
    problem_score = problem_score[problem_score['RISK_LEVEL'] < 4]  # 低风险问题不统计
    PROBLEM_SCORE = group_and_merge_with_dpid(problem_score, DEPARTMENT_DATA, 'FK_DEPARTMENT_ID',
                                              'CHECK_SCORE', 'COUNT')
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    return check_intensity.stats_check_per_person(
        XIANCHANG_CHECK_COUNT, WORK_LOAD, months_ago,
        _calc_score_by_formula, _choose_dpid_data)


# 问题查处力度
def _stats_check_problem_ratio(months_ago):
    return check_intensity.stats_check_problem_ratio(
        PROBLEM_COUNT, WORK_LOAD, months_ago,
        _calc_score_by_formula, _choose_dpid_data)


# 人均质量分
def _stats_score_per_person(months_ago):
    return check_intensity.stats_score_per_person(
        PROBLEM_SCORE, WORK_LOAD, months_ago,
        _calc_score_by_formula, _choose_dpid_data)


# 夜查率
def _stats_yecha_ratio(months_ago):
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT, XIANCHANG_CHECK_COUNT, months_ago,
        _calc_score_by_formula, _choose_dpid_data)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    media_cost_time = get_department_media_checked_time(months_ago, cache_client)
    checked_problem = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    checked_problem = merge_problem_check_score(checked_problem, cache_client)
    checked_problem['PROBLEM_NUMBER'] = 1
    df_list = [media_cost_time,
               checked_problem,
               checked_problem]
    return check_intensity.stats_media_intensity(
        DEPARTMENT_DATA, WORK_LOAD, months_ago, _calc_score_by_formula,
        _choose_dpid_data, df_list,
        [0.35, 0.35, 0.3])


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio(
        DEPARTMENT_DATA, months_ago, _calc_score_address_ratio,
        _choose_dpid_data, CHECK_POINT_SQL, BANZU_POINT_SQL, cache_client)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    child_index_func = [
        _stats_check_per_person,  # b
        _stats_check_problem_ratio,  # c
        _stats_score_per_person,  # f
        _stats_media_intensity,  # j
        _stats_check_address_ratio,  # i
        _stats_yecha_ratio,  # g
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'f', 'g', 'i', 'j']
    ]
    item_weight = [0.3, 0.25, 0.15, 0.05, 0.05, 0.20]

    child_index_list = [CheckIntensityDetailType.stats_check_per_person,
                        CheckIntensityDetailType.stats_check_problem_ratio,
                        CheckIntensityDetailType.stats_score_per_person,
                        CheckIntensityDetailType.stats_yecha_ratio,
                        CheckIntensityDetailType.stats_check_address_ratio,
                        CheckIntensityDetailType.stats_media_intensity]
    update_major_maintype_weight(index_type=INDEX_TYPE,  major=MAJOR, main_type=MainType.check_intensity,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.check_intensity, months_ago,
                         item_name, item_weight, [HIERARCHY], MAJOR)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
