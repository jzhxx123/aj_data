# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.check_intensity_sql import BANZU_POINT_SQL
from app.data.workshop_health_index.common.problem_exposure_sql import (
    EXPOSURE_PROBLEM_DEPARTMENT_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL)
from app.data.workshop_health_index.common.common import (summizet_child_index)
from app.data.index.common import (combine_child_index_func)
from app.data.index.util import (get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, ProblemExposureDetailType
from app.data.workshop_health_index.cheliang.common_data import cache_client
from app.data.workshop_health_index.cheliang.common import MAJOR
from app.data.workshop_health_index.common import problem_exposure


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, WORK_LOAD
    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 单位总人数
    WORK_LOAD = cache_client.get('WORK_LOAD', True)
    WORK_LOAD = WORK_LOAD.groupby(['TYPE4'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')


def _calc_score_by_formula(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


# 普遍性暴露
def _stats_total_problem_exposure(months_ago):
    weight_item = [0.3, 0.2, 0.3, 0.2]
    weight_part = [0.4, 0.6]
    # 问题
    base_data = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    base_data = base_data[base_data['RISK_LEVEL'].between(1, 4)]
    # 一般及以上风险
    risk_data = base_data[base_data['RISK_LEVEL'] <= 3]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] <= 3)]
    df_list = [base_data, risk_data, level_data, level_risk_data]
    title = [
        '总问题数({0})/工作量({1})', '一般及以上风险问题数({0})/工作量({1})', '作业项问题数({0})/工作量({1})',
        '一般及以上风险作业项问题数({0})/工作量({1})'
    ]
    return problem_exposure.stats_total_problem_exposure(
        df_list, title, weight_item, weight_part, DEPARTMENT_DATA, WORK_LOAD,
        months_ago, _choose_dpid_data, _calc_score_by_formula)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    return problem_exposure.stats_problem_exposure(
        HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
        DEPARTMENT_DATA, months_ago, _choose_dpid_data)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    deduct_dict = {1: 2, 2: 4, 3: 6}
    return problem_exposure.stats_banzu_problem_exposure(
        BANZU_POINT_SQL, DEPARTMENT_DATA, _choose_dpid_data, months_ago, deduct_dict, cache_client)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)
    child_index_func = [
        _stats_total_problem_exposure,  # a
        _stats_problem_exposure,  # c
        _stats_banzu_problem_exposure  # d
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'd']]
    item_weight = [0.5, 0.35, 0.15]
    child_index_list = [ProblemExposureDetailType.stats_total_problem_exposure,
                        ProblemExposureDetailType.stats_problem_exposure,
                        ProblemExposureDetailType.stats_banzu_problem_exposure]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.problem_exposure,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.problem_exposure, months_ago,
                         item_name, item_weight, major=MAJOR)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass

