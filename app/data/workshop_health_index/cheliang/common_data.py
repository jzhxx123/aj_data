#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/14
Description: 
"""
import pandas as pd

from app.data.workshop_health_index.utils import get_department_classify
from app.data.index.common import df_merge_with_dpid, get_zhanduan_deparment
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.workshop_health_index.common.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    EXTERNAL_PERSON_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL)
from app.data.workshop_health_index.cheliang import cache_client
from app.data.workshop_health_index.cheliang.common import (classify_config_ids, department_classify_config,
                                                            get_work_load, MAJOR)
from app.data.workshop_health_index.const import HIERARCHY


def init_common_data(months_ago):
    zhanduan_dpid_data = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(classify_config_ids))
    chejian_dpid_data['MAJOR'] = MAJOR
    chejian_dpid_data['CLASSIFY'] = chejian_dpid_data.apply(lambda row: get_department_classify(
        row['CLASSIFY_CONFIG'], department_classify_config), axis=1)
    chejian_dpid_list = tuple(chejian_dpid_data['DEPARTMENT_ID'].values.tolist())
    department_data = pd_query(DEPARTMENT_SQL.format(chejian_dpid_list))
    stats_month = get_custom_month(months_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 正式职工人数
    staff_number = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(chejian_dpid_list)), department_data)
    # 车间外聘人员
    external_staff = pd_query(EXTERNAL_PERSON_SQL.format(month, chejian_dpid_list))
    # 处理工作量
    work_load = pd.concat([staff_number, external_staff], axis=0, sort=False)
    work_load = work_load.groupby(f'TYPE{HIERARCHY}', as_index=False)['COUNT'].sum()
    work_load = pd.merge(chejian_dpid_data, work_load, left_on='DEPARTMENT_ID', right_on='TYPE4', how='left')
    work_load['TYPE4'] = work_load['DEPARTMENT_ID']
    work_load.fillna(0, inplace=True)
    work_load['COUNT'] = work_load.apply(lambda row: get_work_load(row, months_ago), axis=1)

    cache_data = {
        'ZHANDUAN_DPID_DATA': zhanduan_dpid_data,
        'CHEJIAN_DPID_DATA': chejian_dpid_data,
        'DEPARTMENT_DATA': department_data,
        'STAFF_NUMBER': staff_number,
        'WORK_LOAD': work_load
    }

    cache_client.set_all(cache_data)
