#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

from app.data.workshop_health_index.common.common_sql import INTEGRATED_MANAGEMENT_SQL
from app.data.workshop_health_index.utils import get_work_load_from_sql
from app.data.index.util import get_custom_month


MAJOR = '车辆'

department_classify_config = {
    'A': (128,),
    'B': (131, 286),
    'C': (4,),
    'D': (129,),
    'E': (5,),
    'F': (130,),
    'G': (150, 151),
    'H': (152,)
}

department_classify_desc = {
    'A': '货车检修',
    'B': '货车运用',
    'C': '动车检修',
    'D': '动车运用',
    'E': '客车检修',
    'F': '客车运用',
    'G': '动、客、货车设备',
    'H': '其他车间'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])


def get_work_load(row, months_ago):
    classify = row['CLASSIFY']
    stats_month = get_custom_month(months_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    if classify == 'A':
        return row['COUNT']
        # return get_work_load_from_sql(CONFIG_WORK_LOAD_SQL.format('货车检修工作量', month, row['DEPARTMENT_ID']))
    elif classify == 'B':
        # 站修车间系数1.1，其余车间系数为1
        if row['CLASSIFY_CONFIG'] == 286:
            return row['COUNT'] * 1.1
        return row['COUNT']
    elif classify == 'C':
        return get_work_load_from_sql(INTEGRATED_MANAGEMENT_SQL.format('动车检修工作量', month, row['DEPARTMENT_ID']))
    elif classify == 'D':
        # 工作量=“动车运用工作量”*0.8+“动车乘务工作量”*0.6
        apply_work_load = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('动车运用工作量', month, row['DEPARTMENT_ID']))
        crew_work_load = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('动车乘务工作量', month, row['DEPARTMENT_ID']))
        return apply_work_load * 0.8 + crew_work_load * 0.6
    elif classify == 'E':
        return get_work_load_from_sql(INTEGRATED_MANAGEMENT_SQL.format('客车检修工作量', month, row['DEPARTMENT_ID']))
    elif classify == 'F':
        # 工作量=“客车乘务工作量”*0.5+“客车库检工作量”*0.8+“客车列检工作量”*0.7
        crew_work_load = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('客车乘务工作量', month, row['DEPARTMENT_ID']))
        store_check_work_load = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('客车库检工作量', month, row['DEPARTMENT_ID']))
        train_check_work_load = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('客车列检工作量', month, row['DEPARTMENT_ID']))
        return crew_work_load * 0.5 + store_check_work_load * 0.8 + train_check_work_load * 0.7
    elif classify == 'G':
        # 动监车间工作量=“动监车间”设备台数*1
        # 设备车间工作量=“设备车间”设备台数*0.8
        equipment_count = get_work_load_from_sql(
            INTEGRATED_MANAGEMENT_SQL.format('设备台数', month, row['DEPARTMENT_ID']))
        if row['CLASSIFY_CONFIG'] == 150:
            return equipment_count * 1
        return equipment_count * 0.8
    elif classify == 'H':
        return row['COUNT']
    else:
        return row['COUNT']
