#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/31
Description: 
"""

from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)

from . import (check_intensity, check_evenness, problem_exposure, assess_intensity, problem_rectification,
               self_control_ability, health_index)
