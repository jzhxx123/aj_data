#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""

# 人员
person = """SELECT 
        PERSON_ID,
        PERSON_NAME,
        ID_CARD,
        FK_DEPARTMENT_ID,
        POSITION,
        IDENTITY,
        LEVEL,
        POSITION_LEVEL
    FROM
        t_person
"""

# 部门
department = """SELECT 
        DEPARTMENT_ID,
        NAME,
        FK_PARENT_ID,
        HIERARCHY,
        ALL_NAME,
        TYPE,
        TYPE2,
        TYPE3,
        TYPE4,
        FK_DEPARTMENT_CLASSIFY_CONFIG_ID,
        WORK_TIME,
        SHORT_NAME,
        MEDIA_TYPE,
        IS_DELETE
    FROM
        t_department;
"""

# 基础问题库
problem_base = """SELECT
        PK_ID, CHECK_SCORE, RISK_LEVEL, IS_HIDDEN_KEY_PROBLEM, ASSESS_MONEY, FK_DEPARTMENT_ID, IS_DELETE, STATUS,
        TYPE, FK_CHECK_ITEM_ID, PROBLEM_POINT, TYPE1, TYPE2, TYPE3 
    FROM
        t_problem_base
"""

# 关键问题库
key_problem_lib = """SELECT 
    PK_ID, FK_PROBLEM_BASE_ID, FK_DEPARTMENT_ID, OPERATION_TIME
FROM
    t_key_problem_lib;"""

# 重要检查点
check_point = """SELECT
        TYPE, PK_ID, HIERARCHY, FK_DEPARTMENT_ID, IS_DELETE, NAME
    FROM
        t_check_point
"""

# 检查信息关联检查人员
check_info_and_person = """SELECT
        b.FK_DEPARTMENT_ID,
        a.PK_ID, a.CHECK_WAY, a.CHECK_TYPE, a.IS_YECHA, a.PROBLEM_NUMBER, a.PROBLEM_REVIEW_NUMBER,
        a.SUBMIT_TIME, a.START_CHECK_TIME, a.END_CHECK_TIME, c.TYPE2, c.TYPE3
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_department AS c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE 1
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查问题关联检查信息、检查人员
check_problem_and_info = """SELECT
        a.PK_ID, a.EXECUTE_DEPARTMENT_ID, a.FK_PROBLEM_BASE_ID, a.PROBLEM_SCORE, a.IS_ASSESS,
        a.LEVEL, a.RISK_LEVEL, a.IS_SPAN_DEPARTMENT, a.IS_EXTERNAL, a.CHECK_PERSON_ID_CARD,
        b.CHECK_WAY, b.CHECK_TYPE, c.FK_DEPARTMENT_ID
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON c.ID_CARD = a.CHECK_PERSON_ID_CARD
    WHERE 1
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 媒体检查
check_info_and_media = """SELECT
        a.FK_CHECK_INFO_ID, a.COST_TIME
    FROM
        t_check_info_and_media
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查地点关联检查信息
check_info_and_address = """SELECT
        a.TYPE AS ADDRESS_TYPE, a.FK_CHECK_POINT_ID, a.FK_DEPARTMENT_ID as ADDRESS_FK_DEPARTMENT_ID,
        b.CHECK_WAY, b.PK_ID AS CHECK_INFO_PK_ID, b.CHECK_TYPE, b.IS_YECHA, b.START_CHECK_TIME, b.END_CHECK_TIME
    FROM
        t_check_info_and_address AS a   
    INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE 1
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查问题关联责任部门、检查信息
check_problem_and_responsible = """SELECT
        a.FK_DEPARTMENT_ID, 
        b.PK_ID, b.LEVEL, b.PROBLEM_SCORE, b.RISK_LEVEL, b.FK_PROBLEM_BASE_ID, b.IS_ASSESS, b.IS_RED_LINE,
        b.IS_EXTERNAL, b.CHECK_PERSON_ID_CARD, b.IS_SPAN_DEPARTMENT,
        c.CHECK_TYPE, c.CHECK_WAY,
        d.FK_DEPARTMENT_ID AS FK_CHECK_DEPARTMENT_ID
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON b.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person AS d on d.ID_CARD = b.CHECK_PERSON_ID_CARD
    WHERE 1
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查信息关联检查人员
check_info_and_person_with_check_time = """SELECT
        b.FK_DEPARTMENT_ID,
        a.CHECK_WAY, a.CHECK_TYPE, a.START_CHECK_TIME, a.END_CHECK_TIME
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE 1
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 实际检查点关联班组数
real_check_point_relative_banzu = """
SELECT 
    a.FK_CHECK_POINT_ID,
    d.FK_CHECK_ITEM_ID,
    a.FK_CHECK_INFO_ID,
    e.SOURCE_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
    1 AS COUNT
FROM
    t_check_info_and_address AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN
    t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
        INNER JOIN
    t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
        INNER JOIN
    t_department_and_main_production_site AS e ON a.FK_CHECK_POINT_ID = e.FK_ADDRESS_ID
WHERE
    a.TYPE = 2 AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.type =1
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
"""

# 重要生产场所
department_and_main_production_site = """SELECT 
        SOURCE_DEPARTMENT_ID, FK_ADDRESS_ID, TYPE
    FROM
        t_department_and_main_production_site;
"""

# td数据
td_data = """SELECT 
    PK_ID, NODE, CFCC, DDCC, DDSJ, NODE_CODE
FROM
    t_td_data
WHERE
    DATE_FORMAT(DDSJ, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(DDSJ, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 安全生产信息分析
safety_info_analysis_sql = """SELECT 
    a.PK_ID,
    a.RANK,
    a.MAIN_TYPE,
    a.FK_RESPONSIBILITY_DIVISION_ID,
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m') AS MONTH,
    b.RESPONSIBILITY_IDENTIFIED,
    c.NAME AS DEPARTMENT_NAME,
    c.TYPE,
    c.TYPE3,
    c.BELONG_JURISDICTION_NAME,
    f.NAME AS RISK_NAME,
    g.NAME AS PROFESSION,
    1 AS COUNT
FROM
    t_safety_produce_info a
        LEFT JOIN
    t_safety_produce_info_responsibility_unit b ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
        LEFT JOIN
    t_department c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
        LEFT JOIN
    t_safety_produce_info_responsibility_unit_and_risk d ON d.FK_RESPONSIBILITY_UNIT_ID = b.PK_ID
        LEFT JOIN
    t_risk e ON e.PK_ID = d.FK_RISK_ID
        LEFT JOIN
    t_risk f ON e.PARENT_ID = f.PK_ID
        LEFT JOIN
    t_department AS g ON g.DEPARTMENT_ID = c.TYPE2
WHERE
    b.STATUS not in (1)
"""

# 检查地点配置
check_address_config = """SELECT 
    PK_ID,
    FK_UNIT_ID,
    FK_CHECK_POINT_ID,
    FK_DEPARTMENT_ID,
    TYPE,
    CHECK_ITEM_IDS,
    CHECK_DAY_NUMBER,
    PROBLEM_BASE_IDS,
    NUMBER
FROM
    t_check_address_score_config
"""

# 检查项目
check_items = """
SELECT 
    PK_ID,
    PARENT_ID,
    FK_DEPARTMENT_ID,
    HIERARCHY,
    NAME,
    IS_DELETE,
    SOURCE,
    FK_SOURCE_ID,
    CODE,
    TYPE
FROM
    t_check_item;
"""

# 检查地点配置与风险卡控点
check_address_score_config_and_risk_control = """SELECT 
    a.FK_CHECK_ADDRESS_SCORE_CONFIG_ID,
    a.FK_RISK_CONTROL_ID,
    a.LEVEL,
    a.CHECK_DAY_NUMBER,
    a.NUMBER,
    b.NAME,
    b.IS_DELETE,
    b.CHECK_ITEM_IDS,
    b.RISK_STATISTICS_IDS
FROM
    t_check_address_score_config_and_risk_control a
        LEFT JOIN
    t_risk_control b ON a.FK_RISK_CONTROL_ID = b.PK_ID;
"""


check_problem_and_address = """
SELECT 
    b.FK_PROBLEM_BASE_ID,
    b.FK_CHECK_INFO_ID,
    a.TYPE as ADDRESS_TYPE,
    a.FK_CHECK_POINT_ID,
    a.FK_CHECK_PROBLEM_ID,
    a.FK_DEPARTMENT_ID,
    a.TYPE,
    c.FK_DEPARTMENT_ID as CHECK_DEPARTMENT_ID
FROM
    t_check_problem_and_address a
        LEFT JOIN
    t_check_problem b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
        LEFT JOIN
    t_person c ON b.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE 1
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d');
"""


# 部门类型配置
department_classify_config = """SELECT 
    PK_ID,
    PARENT_ID,
    FK_DEPARTMENT_ID,
    HIERARCHY,
    COEFFICIENT,
    TYPE,
    NAME,
    ALL_NAME
FROM
    t_department_classify_config;"""


media_cost_time = """SELECT
        c.FK_DEPARTMENT_ID, a.COST_TIME AS TIME, a.FK_CHECK_INFO_ID as CHECK_INFO_PK_ID, b.CHECK_WAY
    FROM
        t_check_info_and_media AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""
