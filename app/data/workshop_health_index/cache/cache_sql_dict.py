#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""

from app.data.workshop_health_index.cache.cache_sql import *


cache_sql_dict = {
    'person': {'sql': person, 'need_date_params': False},
    'department': {'sql': department, 'need_date_params': False},
    'check_info_and_person': {'sql': check_info_and_person, 'need_date_params': True},
    'check_problem_and_info': {'sql': check_problem_and_info, 'need_date_params': True},
    'problem_base': {'sql': problem_base, 'need_date_params': False},
    'check_info_and_address': {'sql': check_info_and_address, 'need_date_params': True},
    'check_point': {'sql': check_point, 'need_date_params': False},
    'check_problem_and_responsible': {'sql': check_problem_and_responsible, 'need_date_params': True},
    'check_info_and_person_with_check_time': {'sql': check_info_and_person_with_check_time,
                                              'need_date_params': True},
    'real_check_point_relative_banzu': {'sql': real_check_point_relative_banzu, 'need_date_params': True},
    'department_and_main_production_site': {'sql': department_and_main_production_site},
    'td_data': {'sql': td_data, 'need_date_params': True, 'db_name': 'db_mid'},
    'safety_info_analysis': {'sql': safety_info_analysis_sql, 'need_date_params': False},
    'check_address_config': {'sql': check_address_config, 'need_date_params': False},
    'check_items': {'sql': check_items, 'need_date_params': False},
    'check_address_score_config_and_risk_control': {'sql': check_address_score_config_and_risk_control},
    'key_problem_lib': {'sql': key_problem_lib},
    'check_problem_and_address': {'sql': check_problem_and_address, 'need_date_params': True},
    'department_classify_config': {'sql': department_classify_config},
    'media_cost_time': {'sql': media_cost_time, 'need_date_params': True}
}
