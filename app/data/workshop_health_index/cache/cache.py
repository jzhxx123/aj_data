#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""
import sys
import os
from datetime import datetime

import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month
from app.data.util import pd_query, get_history_months
from app import redis
from app.data.workshop_health_index.cache.cache_sql_dict import cache_sql_dict
from app.utils.common_func import get_today
from app.utils.data_update_models import UpdateMonth
from config import BASEDIR


class RedisCache:
    """
    DataFrame redis实现
    """
    max_single_size = 512000000

    def __init__(self):
        self.redis_client = redis

    def set(self, key, value):
        """
        设置DataFrame缓存
        :param key:
        :param value:
        :return:
        """
        if isinstance(value, (pd.DataFrame, pd.Series)):
            value = value.to_msgpack(compress='zlib')
        if sys.getsizeof(value) > self.max_single_size:
            current_app.logger.error('to large to cache for key {}'.format(key))
        self.redis_client.set(key, value)

    def delete(self, key):
        self.redis_client.delete(key)

    def get(self, key):
        res = self.redis_client.get(key)
        if res:
            try:
                res = pd.read_msgpack(res)
            except Exception as e:
                current_app.logger.error('transform error for key {}, msg: {}'.format(key, e))
        return res

    def init_data(self, months_ago=-1, cover=False):
        """
        从cache_dict中逐个获取sql对应的数据并设置缓存
        :param months_ago:
        :param cover: 强制覆盖
        :return:
        """
        for key, target in cache_sql_dict.items():
            params = ()
            stats_month = get_custom_month(months_ago)
            if target.get('need_date_params'):
                key = key + f'_{stats_month[1][:7]}'
                params = stats_month
            if cover or not self.redis_client.exists(key):
                current_app.logger.debug(f'start cache key {key}')
                df = pd_query(target['sql'].format(*params))
                self.set(key, df)
                current_app.logger.debug(f'end cache key {key}')

    def get_or_set(self, key, sql):
        res = self.get(key)
        if res is None:
            self.set(key, pd_query(sql))
        return self.get(key)


class FileCache:
    """
    DataFrame 基于msgpacks实现
    """

    def __init__(self):
        self.cache_path = os.path.join(BASEDIR, 'msgpacks')
        if not os.path.exists(self.cache_path):
            os.mkdir(self.cache_path)

    def get_file_path(self, key, mid_dir=''):
        """
        获取文件路径
        :param key: 缓存的key，作为文件名
        :param mid_dir: 中间文件夹名称
        :return:
        """
        if not mid_dir:
            mid_dir = 'common'
        dir_path = os.path.join(self.cache_path, mid_dir)
        if not os.path.exists(dir_path):
            os.mkdir(dir_path)
        return os.path.join(dir_path, f'{key}.msgpack')

    def set(self, key, value, mid_dir=''):
        """
        设置DataFrame缓存到msgpack文件
        :param key:
        :param value:
        :param mid_dir:
        :return:
        """
        if isinstance(value, (pd.DataFrame, pd.Series)):
            file_path = self.get_file_path(key, mid_dir)
            value.to_msgpack(path_or_buf=file_path, compress='zlib')
        else:
            current_app.logger.warning(f'only support cache for DataFrame')

    def delete(self, key):
        """
        删除缓存的DataFrame
        :param key:
        :return:
        """
        pass

    def get(self, key, mid_dir=''):
        """
        获取缓存的DataFrame
        :param key:
        :param mid_dir:
        :return:
        """
        file_path = self.get_file_path(key, mid_dir)
        res = None
        if os.path.exists(file_path):
            try:
                res = pd.read_msgpack(file_path)
            except Exception as e:
                current_app.logger.error('transform error for key {}, msg: {}'.format(key, e))
        return res

    def set_by_key(self, key, months_ago):
        params = ()
        month = ''
        target = cache_sql_dict.get(key)
        if not target:
            return
        need_date_params = target.get('need_date_params')
        if months_ago and need_date_params:
            stats_month = get_custom_month(months_ago)
            month = stats_month[1][:7]
            key = key + f'_{month}'
            params = stats_month

        current_app.logger.debug(f'start cache key {key}')
        df = pd_query(target['sql'].format(*params), target.get('db_name', 'db_aj'))
        self.set(key, df, month)
        current_app.logger.debug(f'end cache key {key}')

    def init_data(self, months_ago=-1, cover=False):
        """
        从cache_dict中逐个获取sql对应的数据并设置缓存
        :param months_ago:
        :param cover: 强制覆盖
        :return:
        """
        for key, target in cache_sql_dict.items():
            params = ()
            stats_month = get_custom_month(months_ago)
            month = stats_month[1][:7]
            need_date_params = target.get('need_date_params')
            if need_date_params:
                key = key + f'_{month}'
                params = stats_month
            file_path = self.get_file_path(key, month if need_date_params else '')
            if cover or not os.path.exists(file_path):
                current_app.logger.debug(f'start cache key {key}')
                df = pd_query(target['sql'].format(*params), target.get('db_name', 'db_aj'))
                self.set(key, df, month if need_date_params else '')
                current_app.logger.debug(f'end cache key {key}')


# redis_cache = RedisCache()
file_cache = FileCache()


class GlobalCache:
    """
    全局缓存
    cache_dict：用于存储从缓存系统中获取的DataFrame
    """
    cache_dict = dict()

    def __init__(self):
        # self._redis_cache = redis_cache
        self._file_cache = file_cache
        self.last_date = datetime.today().date()

    @property
    def client(self):
        """
        获取当前缓存系统，暂时只使用文件缓存
        :return:
        """
        return self._file_cache

    def get(self, key, months_ago=None):
        """
        根据key获取缓存数据
        :param key:
        :param months_ago: n个月以前
        :return:
        """
        self.validate_expire()
        month = ''
        if months_ago:
            month = get_custom_month(months_ago)[1][:7]
            key += f'_{month}'
        if key in self.cache_dict.keys():
            return self.cache_dict.get(key)
        res = self.client.get(key, month)
        if res is not None:
            self.cache_dict[key] = res
        return res

    def init_data(self, months_ago, cover=False):
        """
        从cache_dict中逐个获取sql对应的数据并设置缓存
        :param months_ago:
        :param cover:
        :return:
        """
        self.client.init_data(months_ago, cover)
        self.reset()

    def reset(self):
        """
        清空cache_dict缓存
        :return:
        """
        self.cache_dict = {}

    def validate_expire(self):
        """
        如果当前日期大于上一次记录日期，清空cache_dict缓存
        :return:
        """
        today = datetime.today().date()
        if today > self.last_date:
            self.reset()
            self.last_date = today

    def set_by_key(self, key, months_ago):
        self.client.set_by_key(key, months_ago)
        return self.get(key, months_ago)


global_cache = GlobalCache()


class CacheClient:
    """
    供模块调用的缓存客户端
    模块局部缓存与全局的缓存分开
    __module_cache： 只属于当前对象的缓存字典
    __global_cache： 代理global_cache对象
    """

    def __init__(self, module):
        self.module = module
        self.__module_cache = {}
        self.__global_cache = global_cache

    def get(self, key, module_cache=False, months_ago=None):
        """
        获取
        :param key:
        :param module_cache: 表示是否从当前对象的缓存字典中获取，False则从全局缓存中获取
        :param months_ago: n个月之前
        :return:
        """
        if module_cache:
            return self.__module_cache.get(key)
        else:
            return self.get_or_set_global(key, months_ago)

    def set(self, key, value):
        """
        设置模块缓存
        :param key:
        :param value:
        :return:
        """
        self.__module_cache[key] = value

    def set_all(self, data_dict):
        self.__module_cache = data_dict

    def get_or_set_global(self, key, months_ago=None):
        res = self.__global_cache.get(key, months_ago)
        if res is None:
            res = self.__global_cache.set_by_key(key, months_ago)
        return res


def get_cache_client(module):
    """
    获取模块缓存客户端
    :param module:
    :return:
    """
    return CacheClient(module)


def run_cache_timed_task(months_ago):
    """
    缓存的定时任务
    :param months_ago:
    :return:
    """

    if isinstance(months_ago, UpdateMonth):
        months_ago = months_ago.get_month_delta()

    if months_ago > 0:
        months_ago *= -1

    global_cache.init_data(months_ago, cover=True)

    return 'OK'
