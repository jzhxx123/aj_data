#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/1
Description: 
"""
import pandas as pd
import sys
import time

from flask import current_app


from app.data.util import pd_query

sql = """
SELECT  a.PK_ID AS FK_CHECK_INFO_ID,    b.FK_CHECK_ITEM_ID,    a.IS_YECHA,    a.CHECK_WAY,    a.CHECK_TYPE,    a.FK_SAFETY_PRODUCE_INFO_ID,    a.STATUS,    a.ID_CARD,    a.SUBMIT_TIME,    a.IS_GAOTIE,    a.IS_DONGCHE,    a.IS_KECHE,    a.IS_GENBAN
FROM    t_check_info AS a        INNER JOIN    t_check_info_and_item AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('2019-05-25', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('2019-06-25', '%%Y-%%m-%%d')
"""


def main():
    print(current_app.extensions['redis'])
    client = current_app.extensions['redis']['REDIS']
    print(client.get('a'))

    client.set('a', 2)

    # a1 = global_cache.get('test_df')
    # b1 = global_cache.get('test_df')
    # print(a1 is b1)


    df = pd_query(sql)

    # df.to_csv('a.csv')

    print(df.index.size)

    # t1 = time.time()
    # df2 = pd.read_csv('a.csv')
    # print('t1 is {}'.format(time.time() - t1))

    df_bytes = df.to_msgpack(path_or_buf='b.msgpack', compress='zlib')
    print('size is {}'.format(sys.getsizeof(df_bytes)))
    # client.set('test_df', df_bytes)

    t1 = time.time()
    df_from_redis = pd.read_msgpack(client.get('test_df'))
    print('t1 is {}'.format(time.time() - t1))
    print(df_from_redis.index.size)

    t1 = time.time()
    df_from_redis = pd.read_msgpack('b.msgpack')
    print('t2 is {}'.format(time.time() - t1))

    print(df_from_redis.index.size)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        main()
