#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce
from app.data.workshop_health_index.common.common_sql import OTHER_MANAGEMENT_SQL
from app.data.workshop_health_index.utils import get_work_load_from_sql

department_classify_config = {
    'A': (111, 112),
    'B': (114, 115, 108),
    'C': (117,),
    'D': (122, 104)
}

department_classify_desc = {
    'A': '信号车间（高铁、普铁）',
    'B': '通信车间（高铁、普铁）',
    'C': '车载设备车间',
    'D': '基地车间'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])

MAJOR = '电务'


def get_work_load(row, months_ago):
    classify = row['CLASSIFY']
    if classify == 'A':
        return get_work_load_from_sql(OTHER_MANAGEMENT_SQL.format('现场设备换算道岔组数', row['DEPARTMENT_ID']))
    elif classify == 'B':
        return get_work_load_from_sql(OTHER_MANAGEMENT_SQL.format('换算设备匹长公里数', row['DEPARTMENT_ID']))
    elif classify == 'C':
        return get_work_load_from_sql(OTHER_MANAGEMENT_SQL.format('机车运用台次', row['DEPARTMENT_ID']))
    elif classify == 'D':
        return row['COUNT']
    return row['COUNT']
