#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Author: seerbigdata
Date: 2019/8/2
Description: 自控能力指数
"""


import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.common_funcs import get_on_site_checked_count_data
from app.data.workshop_health_index.common.self_control_ability_sql import (
    SAFETY_PRODUCE_INFO_SQL, EVALUATE_SCORE_SQL)
from app.data.workshop_health_index.common.check_evenness_sql import DAILY_CHECK_BANZU_COUNT_SQL
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.workshop_health_index.common.check_intensity_sql import (BANZU_POINT_SQL, CHECK_POINT_SQL)
from app.data.workshop_health_index.common.common import calc_check_count_per_person, summizet_child_index
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, SelfControlAbilityDetailType
from app.data.workshop_health_index.dianwu.common_data import cache_client
from app.data.workshop_health_index.dianwu.common import MAJOR
from app.data.workshop_health_index.common import self_control_ability


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio > 0.1:
        _score = 100
    elif -0.1 <= _ratio <= 0.1:
        _score = 60
    elif _ratio <= -0.3:
        _score = 0
    return _score


def _calc_score_address_ratio(row, column, major_column=None):
    """
    覆盖率得分计算
    :param row:
    :param column:
    :param major_column:
    :return:
    """
    return min(100, row[column] * 100)


def _get_sql_data(months_ago):
    global WORK_LOAD,ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        XIANCHANG_CHECK_COUNT, YECHA_COUNT, PROBLEM_DF, DAILY_CHECK_BANZU_COUNT
    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)

    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 工作量
    WORK_LOAD = cache_client.get('WORK_LOAD', True)
    # 检查总次数
    XIANCHANG_CHECK_COUNT, YECHA_COUNT = get_on_site_checked_count_data(DEPARTMENT_DATA,
                                                                        months_ago, cache_client,
                                                                        filter_self_check=True)

    PROBLEM_DF = get_problem_df(months_ago)

    DAILY_CHECK_BANZU_COUNT = pd_query(DAILY_CHECK_BANZU_COUNT_SQL)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def get_problem_df(months_ago):
    problem_and_resp = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    problem_and_resp = pd.DataFrame(problem_and_resp,
                                    columns=['RISK_LEVEL', 'PK_ID', 'FK_DEPARTMENT_ID', 'FK_PROBLEM_BASE_ID',
                                             'IS_ASSESS', 'LEVEL', 'IS_EXTERNAL'])
    problem_and_resp.rename(columns={'FK_DEPARTMENT_ID': 'RESP_DEPARTMENT_ID'}, inplace=True)
    problem_and_info = cache_client.get('check_problem_and_info', months_ago=months_ago)
    problem_and_info = pd.DataFrame(problem_and_info,
                                    columns=['FK_DEPARTMENT_ID', 'PK_ID', 'CHECK_WAY', 'CHECK_TYPE'])
    problem_and_info.rename(columns={'FK_DEPARTMENT_ID': 'CHECK_DEPARTMENT_ID'}, inplace=True)
    problem_df = pd.merge(problem_and_resp, problem_and_info, left_on='PK_ID', right_on='PK_ID')
    problem_df = problem_df[~(problem_df['CHECK_WAY'].between(5, 6))
                            & ~(problem_df['CHECK_TYPE'].between(400, 499))
                            & ~(problem_df['CHECK_TYPE'].isin((102, 103)))]
    # 部门数据，用于连接检查部门
    check_dpid_df = pd.DataFrame(DEPARTMENT_DATA, columns=['DEPARTMENT_ID', 'TYPE4'])
    check_dpid_df = check_dpid_df.rename(columns={'TYPE4': 'TYPE4_CHECK'})
    # 部门数据，用于连接责任部门
    resp_dpid_df = pd.DataFrame(DEPARTMENT_DATA, columns=['DEPARTMENT_ID', 'TYPE4'])
    resp_dpid_df = resp_dpid_df.rename(columns={'TYPE4': 'TYPE4_RESP'})
    # 筛选责任部门车间与检查部门车间相同的记录
    problem_df = pd.merge(problem_df, check_dpid_df, left_on='CHECK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    problem_df = pd.merge(problem_df, resp_dpid_df, left_on='RESP_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    return problem_df


# 换算车间自查频次
def _stats_self_check_per_person(months_ago):
    return self_control_ability.stats_self_check_per_person(
        XIANCHANG_CHECK_COUNT, WORK_LOAD, months_ago, _calc_score_by_formula, _choose_dpid_data)


# 人均质量分
def _stats_score_per_person(months_ago):
    return self_control_ability.stats_score_per_person(
        PROBLEM_DF, WORK_LOAD, months_ago, _calc_score_by_formula,
        _choose_dpid_data, cache_client)


# 较严重及以上风险问题
def _stats_serious_problem_ratio(months_ago):
    return self_control_ability.stats_serious_problem_ratio(
        PROBLEM_DF, WORK_LOAD, months_ago, _calc_score_by_formula,
        _choose_dpid_data)


def _stats_other_problem_exposure(months_ago):
    problem_base = cache_client.get('problem_base')
    return self_control_ability.stats_other_problem_exposure(
        SAFETY_PRODUCE_INFO_SQL, PROBLEM_DF, problem_base, DEPARTMENT_DATA,
        months_ago, _choose_dpid_data)


def _calc_evaluate_score_with_ratio(row):
    if 1 <= row['FK_PERSON_GRADATION_RATIO_ID'] < 4:
        return row['SCORE'] * row['GRADATION_RATIO']
    return row['SCORE']


def _calc_department_evaluate_score(row):
    """
    100-被评价记分总和（干部乘以职务系数，非干部不乘系数）/职工人数*100*4
    :param row:
    :return:
    """
    deduct = round(row['EVALUATE_SCORE'], 2)
    score = max(0, round(100 - deduct, 2))
    content = '得分({0})=100-∑评价扣分({1})'.format(score, deduct)
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def _stats_evaluate_intensity(months_ago):
    return self_control_ability.stats_evaluate_intensity(
        EVALUATE_SCORE_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, _choose_dpid_data, _calc_evaluate_score_with_ratio, _calc_department_evaluate_score)


# 换算车间考核问题数
def _stats_check_problem_assess_radio(months_ago):
    problem_df = PROBLEM_DF[(PROBLEM_DF['IS_ASSESS'] == 1) & (PROBLEM_DF['IS_EXTERNAL'] == 0)]
    return self_control_ability.stats_check_problem_assess_radio(
        problem_df, STAFF_NUMBER, months_ago, _calc_score_by_formula,
        _choose_dpid_data)


# 换算车间夜查频次
def _stats_self_check_yecha_ratio(months_ago):
    return self_control_ability.stats_self_check_yecha_ratio(
        YECHA_COUNT, XIANCHANG_CHECK_COUNT, months_ago, _calc_score_by_formula, _choose_dpid_data)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return self_control_ability.stats_check_address_ratio(
        DEPARTMENT_DATA, months_ago, _calc_score_address_ratio,
        _choose_dpid_data, CHECK_POINT_SQL, BANZU_POINT_SQL, cache_client)


# 中高质量问题占比
def _stats_medium_quality_above_ratio(months_ago):
    self_check_problem = PROBLEM_DF[PROBLEM_DF['TYPE4_CHECK'] == PROBLEM_DF['TYPE4_RESP']].copy()
    self_check_problem.rename(columns={'TYPE4_CHECK': 'TYPE4'}, inplace=True)
    self_check_problem['COUNT'] = 1
    # medium_above_problem = self_check_problem[~self_check_problem['LEVEL'].isin(('D', 'E4', 'F3', 'K3', 'K4'))]
    medium_above_problem = self_check_problem[
        self_check_problem['LEVEL'].isin(('A', 'B', 'F1', 'F2', 'E1', 'E2', 'E3', 'C', 'F3', 'G1'))]
    return self_control_ability.stats_medium_quality_above_ratio(
        medium_above_problem, self_check_problem, months_ago, _calc_score_by_formula, _choose_dpid_data)


# 检查时间均衡度 - 检查时段均衡度
def _stats_check_hour_evenness(months_ago):
    return self_control_ability.stats_check_hour_evenness(
        DAILY_CHECK_BANZU_COUNT, DEPARTMENT_DATA, months_ago,
        _choose_dpid_data, cache_client)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    child_index_func = [
        _stats_other_problem_exposure,  # d
        _stats_evaluate_intensity,  # e
        _stats_self_check_yecha_ratio,  # g
        _stats_check_address_ratio,  # h
        _stats_medium_quality_above_ratio,  # i
        _stats_check_hour_evenness  # j
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    item_name = [
        f'SCORE_{x}' for x in ['d', 'e', 'g', 'h', 'i', 'j']
    ]
    item_weight = [0.15, 0.15, 0.15, 0.15, 0.25, 0.15]

    child_index_list = [SelfControlAbilityDetailType.other_problem_exposure,
                        SelfControlAbilityDetailType.stats_evaluate_intensity,
                        SelfControlAbilityDetailType.stats_yecha_ratio,
                        SelfControlAbilityDetailType.stats_check_address_ratio,
                        SelfControlAbilityDetailType.stats_medium_quality_above_ratio,
                        SelfControlAbilityDetailType.stats_check_hour_evenness
                        ]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.self_control_ability,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.self_control_ability, months_ago,
                         item_name, item_weight, [HIERARCHY], major=MAJOR)

    current_app.logger.debug(
        '├── └── self_control_ability index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
