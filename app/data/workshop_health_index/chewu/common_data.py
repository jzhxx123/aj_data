#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/14
Description: 
"""
import math

import pandas as pd

from app.data.workshop_health_index.chewu.common_sql import (LOAD_AND_UNLOAD_COUNT_SQL, UNIT_ASSOCIATE_DEPARTMENT_SQL,
                                                             CW_VISITOR_RECEIVED_COUNT_SQL,
                                                             SHUNTING_OPERATION_WORK_NUMBER)
from app.data.workshop_health_index.utils import get_department_classify
from app.data.index.common import df_merge_with_dpid, get_zhanduan_deparment
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.workshop_health_index.common.common_sql import (
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL, DEPARTMENT_TYPE_CW_SQL, NOT_CADRE_COUNT_SQL, CHEJIAN_DPID_SQL,
    DEPARTMENT_SQL, EXTERNAL_PERSON_SQL)
from app.data.workshop_health_index.chewu import cache_client
from app.data.workshop_health_index.chewu.common import (classify_config_ids, MAJOR, department_classify_config,
                                                         get_work_load)
from app.data.workshop_health_index.const import HIERARCHY


def handle_department_data(row, workshop_dpid_list):
    if row['TYPE'] == 9 and row['CLASSIFY_CONFIG'] in classify_config_ids:
        return row['DEPARTMENT_ID']
    if row['TYPE'] == 10 and row['TYPE5'] in workshop_dpid_list:
        return row['TYPE5']
    return row['TYPE4']


def init_common_data(months_ago):
    """
    车务中某些班组车站也需要作为车间计算
    :param months_ago:
    :return:
    """
    zhanduan_dpid_data = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(classify_config_ids))
    chejian_dpid_data['CLASSIFY'] = chejian_dpid_data.apply(lambda row: get_department_classify(
        row['CLASSIFY_CONFIG'], department_classify_config), axis=1)
    chejian_dpid_data['MAJOR'] = MAJOR
    chejian_dpid_list = tuple(chejian_dpid_data['DEPARTMENT_ID'].values.tolist())
    department_data = pd_query(DEPARTMENT_SQL.format(chejian_dpid_list))
    # workshop_dpid_list = chejian_dpid_data['DEPARTMENT_ID'].values.tolist()
    # department_data['TYPE4'] = department_data.apply(lambda row: handle_department_data(row, workshop_dpid_list),
    #                                                  axis=1)
    stats_month = get_custom_month(months_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 正式职工人数
    staff_number = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(chejian_dpid_list)), department_data)
    # 车间外聘人员
    external_staff = pd_query(EXTERNAL_PERSON_SQL.format(month, chejian_dpid_list))
    # 车间非干部人数/客运职工数
    not_cadre_staff_number = df_merge_with_dpid(pd_query(NOT_CADRE_COUNT_SQL.format(chejian_dpid_list)), department_data)

    # 处理工作量
    work_load = pd.concat([not_cadre_staff_number], axis=0, sort=False)
    work_load = work_load.groupby(f'TYPE{HIERARCHY}', as_index=False)['COUNT'].sum()
    work_load = pd.merge(chejian_dpid_data, work_load, left_on='DEPARTMENT_ID', right_on='TYPE4', how='left')
    work_load['TYPE4'] = work_load['DEPARTMENT_ID']
    work_load.fillna(0, inplace=True)

    # 月接发列车列数
    td_data = cache_client.get('td_data', months_ago=months_ago)
    td_data = td_data.groupby('NODE').size().to_frame('COUNT')
    td_data.reset_index(inplace=True)
    department_cw = df_merge_with_dpid(pd_query(DEPARTMENT_TYPE_CW_SQL), department_data)
    department_cw = department_cw.dropna(subset=['ASSOCIATED_STATION'])
    department_cw = department_cw.drop('ASSOCIATED_STATION', axis=1).join(
        department_cw['ASSOCIATED_STATION'].str.split(',', expand=True).stack().reset_index(level=1, drop=True).rename(
            'ASSOCIATED_STATION'))
    train_receive_count = pd.merge(department_cw, td_data, left_on='ASSOCIATED_STATION', right_on='NODE', how='left')

    # 装卸车数
    begin_date = int(stats_month[0][:4] + stats_month[0][5:7] + stats_month[0][8:10])
    end_date = int(stats_month[1][:4] + stats_month[1][5:7] + stats_month[1][8:10])
    load_and_unload_count = pd_query(LOAD_AND_UNLOAD_COUNT_SQL.format(begin_date, end_date), db_name='db_mid')
    # CW数据关联部门
    unit_associate_department = df_merge_with_dpid(pd_query(UNIT_ASSOCIATE_DEPARTMENT_SQL), department_data)
    load_and_unload_count = pd.merge(load_and_unload_count, unit_associate_department, how='inner', left_on='UNIT',
                                     right_on='UNIT')

    # 月旅客发送量
    visitor_received_count = pd_query(CW_VISITOR_RECEIVED_COUNT_SQL.format(begin_date, end_date), db_name='db_mid')
    visitor_received_count = pd.merge(visitor_received_count, unit_associate_department, how='inner', left_on='UNIT',
                                      right_on='UNIT')

    # 月调车工作量
    shunting_operation_work_number = df_merge_with_dpid(pd_query(SHUNTING_OPERATION_WORK_NUMBER), department_data)
    shunting_operation_workload = pd.merge(shunting_operation_work_number,
                                           pd.DataFrame(unit_associate_department,
                                                        columns=['SHUNTING_OPERATION_TIME', 'DEPARTMENT_ID']),
                                           left_on='DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    shunting_operation_workload['COUNT'] = shunting_operation_workload['WORK_NUMBER'] * shunting_operation_workload[
        'SHUNTING_OPERATION_TIME'] * 30
    shunting_operation_workload.fillna(0)

    work_load['COUNT'] = work_load.apply(
        lambda row: get_work_load(row, months_ago, train_receive_count, load_and_unload_count, visitor_received_count,
                                  shunting_operation_workload, unit_associate_department),
        axis=1)

    cache_data = {
        'ZHANDUAN_DPID_DATA': zhanduan_dpid_data,
        'CHEJIAN_DPID_DATA': chejian_dpid_data,
        'DEPARTMENT_DATA': department_data,
        'STAFF_NUMBER': staff_number,
        'WORK_LOAD': work_load
    }

    cache_client.set_all(cache_data)
