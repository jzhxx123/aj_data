#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

from app.data.index.util import get_custom_month
from app.data.workshop_health_index.common.common_sql import INTEGRATED_MANAGEMENT_SQL
from app.data.workshop_health_index.utils import get_work_load_from_sql

MAJOR = '车务'

department_classify_config = {
    'A': (234,),
    'B': (232, 300),
    'C': (237,),
    'D': (236,),
    'E': (238, 324),
    'F': (235, 325, 326),
    'J': (303, 304)
}

department_classify_desc = {
    'A': '调度车间',
    'B': '运转车间',
    'C': '货运车间',
    'D': '货检车间',
    'E': '区段站车间',
    'F': '中间站车间',
    'J': '其他车间'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])


def calc_train_receive_count(department_id, count_data):
    """
    获取月接发列车列数
    :param department_id:
    :param count_data:
    :return:
    """
    workshop_data = count_data[count_data['TYPE4'] == department_id]
    workshop_data = workshop_data.sort_values('COUNT').drop_duplicates(subset=['TYPE4', 'ASSOCIATED_STATION'],
                                                                       keep='last')
    return workshop_data['COUNT'].sum()


def calc_load_and_unload_count(department_id, count_data):
    """
    获取月装卸车数
    :param department_id:
    :param count_data:
    :return:
    """
    workshop_data = count_data[count_data['TYPE4'] == department_id]
    return workshop_data['COUNT'].sum()


def calc_visitor_received_count(department_id, count_data):
    """
    获取月旅客发送量
    :param department_id:
    :param count_data:
    :return:
    """
    workshop_data = count_data[count_data['TYPE4'] == department_id]
    return workshop_data['COUNT'].sum()


def calc_shunting_workload(department_id, count_data):
    """
    获取月调车工作量
    :param department_id:
    :param count_data:
    :return:
    """
    workshop_data = count_data[count_data['TYPE4'] == department_id]
    return workshop_data['COUNT'].sum()


def group_sum_from_cw_data(department_id, cw_data, col):
    workshop_data = cw_data[cw_data['TYPE4'] == department_id]
    return workshop_data[col].sum()


def get_work_load(row, months_ago, td_data, load_and_unload_data, visitor_received_data, shunting_operation_workload,
                  cw_data):
    """
    根据车间属性计算工作量
    :param cw_data:
    :param row:
    :param months_ago:
    :param td_data:
    :param load_and_unload_data:
    :param visitor_received_data:
    :param shunting_operation_workload:
    :return:
    """
    classify = row['CLASSIFY']
    stats_month = get_custom_month(months_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    department_id = row['DEPARTMENT_ID']
    td_count = calc_train_receive_count(department_id, td_data)  # 月接发列车列数
    shunting_work_load = calc_shunting_workload(department_id, shunting_operation_workload)  # 月调车工作量
    load_and_unload_count = calc_load_and_unload_count(department_id, load_and_unload_data)  # 月装卸车数
    visitor_received_count = calc_visitor_received_count(department_id, visitor_received_data)  # 月旅客发送量
    receive_person_count = group_sum_from_cw_data(department_id, cw_data, 'ACCEPT_DEPARTURE_NUMBER')  # 接发列车人数
    if classify == 'A':
        # 调度车间工作量=（月接发列车列数/10000）*70+（调车作业量/50000）*30
        return (td_count / 10000) * 70 + (shunting_work_load / 50000) * 30
    elif classify == 'B':
        if row['CLASSIFY_CONFIG'] == 232:
            # 运转车间1工作量=（月接发列车列数/10000）*30+（调车作业量/50000）*70
            return (td_count / 10000) * 30 + (shunting_work_load / 50000) * 70
        else:
            # 运转车间2工作量=（月接发列车列数/10000）*60+（调车作业量/50000）*40
            return (td_count / 10000) * 60 + (shunting_work_load / 50000) * 40
    elif classify == 'C':
        # 货运车间工作量=月装卸车数/13500*100
        return (load_and_unload_count / 13500) * 100
    elif classify == 'D':
        # 货检车间工作量=月接发列车列数/9000*100
        return (td_count / 9000) * 100
    elif classify == 'E':
        if row['CLASSIFY_CONFIG'] == 238:
            # 区段站车间=
            # （月接发列车列数/10000）*30+（调车作业量/9000）*30+（月装卸车数/7000）*20+（客运职工数/180）*20
            return (td_count / 10000) * 30 + (shunting_work_load / 9000) * 30 + (load_and_unload_count / 7000) * 20 + (
                        row['COUNT'] / 180) * 20
        else:
            # 区段站车间2=
            # （月接发列车列数/10000）*30+（调车作业量/9000）*40+（月装卸车数/7000）*30
            return (td_count / 10000) * 30 + (shunting_work_load / 9000) * 40 + (load_and_unload_count / 7000) * 30
    elif classify == 'F':
        if row['CLASSIFY_CONFIG'] == 235:
            # 中间站车间1工作量=（月接发列车列数/10000）*30+（调车作业量/3500）*30+（月装卸车数/7000）*20+（客运职工数/200）*20
            return (td_count / 10000) * 30 + (shunting_work_load / 3500) * 30 + (load_and_unload_count / 7000) * 20 + (
                    row['COUNT'] / 200) * 20
        elif row['CLASSIFY_CONFIG'] == 325:
            # 中间站车间2工作量=（月接发列车列数/3000）*30+（调车作业量/3500）*30+（月装卸车数/7000）*40
            return (td_count / 3000) * 30 + (shunting_work_load / 3500) * 30 + (load_and_unload_count / 7000) * 40
        else:
            # 中间站车间3工作量=（月接发列车列数/3000）*40+（客运职工数/200）*60
            return (td_count / 3000) * 40 + (row['COUNT'] / 200) * 60
    elif classify == 'J':
        if row['CLASSIFY_CONFIG'] == 303:
            # 其他车间1工作量=（接发列车人数/10）*20+（客运职工数/100）*80
            return (receive_person_count / 10) * 20 + (row['COUNT'] / 100) * 80
        else:
            # 工作量=（接发列车人数/10）*10+（调车作业量/3500）*20+（月装卸车数/7000）*20+（客运职工数/200）*50
            return (receive_person_count / 10) * 10 + (shunting_work_load / 3500) * 20 \
                   + (load_and_unload_count / 7000) * 20 + (row['COUNT'] / 200) * 50
    else:
        return row['COUNT']
