# 装卸车数
# 装车数Report_name为ZX1,entry001的数据
# 卸车数Report_name为ZX1,entry025的数据
# 地铁卸空数--Report_name为ZX1,entry074的数据
LOAD_AND_UNLOAD_COUNT_SQL = """SELECT
    UNIT, SUM(ENTRY001 + ENTRY025 + ENTRY074) AS COUNT   
    FROM 
        t_data_date
    WHERE
        REPORT_NAME = 'ZX1'
        AND D18_DATE >= {0}
        AND D18_DATE < {1}
    GROUP BY UNIT
"""

# 车站关联部门id
UNIT_ASSOCIATE_DEPARTMENT_SQL = """SELECT
        FK_DEPARTMENT_ID,
        PASSENGER_STATION as UNIT,
        SHUNTING_OPERATION_TIME,
        ACCEPT_DEPARTURE_NUMBER
    FROM
        t_department_type_cw
"""

# 每月旅客发送量信息统计
CW_VISITOR_RECEIVED_COUNT_SQL = """
SELECT 
    UNIT, ENTRY002 AS COUNT
FROM
    t_data_date
WHERE
    REPORT_NAME = 'SBZ4' AND D18_DATE >= {0}
        AND D18_DATE < {1}
"""


# 月调车工作人数
SHUNTING_OPERATION_WORK_NUMBER = """SELECT 
    FK_DEPARTMENT_ID, WORK_NUMBER
FROM
    t_department_and_info
WHERE
    main_type = 1 AND source_id = 504
"""

# 车间
# 车务专业中，某些班组也需要作为车间计算
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, a.TYPE3, b.NAME AS MAJOR,
        a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID as CLASSIFY_CONFIG, a.TYPE
    FROM t_department AS a
            LEFT JOIN
        t_department AS b on a.TYPE2 = b.DEPARTMENT_ID
    WHERE a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID IN {0}
        AND a.TYPE in (8, 9)
        AND a.IS_DELETE = 0;
"""


DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE2, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.HIERARCHY, a.MEDIA_TYPE,
        a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID as CLASSIFY_CONFIG
    FROM
        t_department AS a
    WHERE
        (a.TYPE4 in {0} or a.TYPE5 in {0})
        AND a.IS_DELETE = 0;
"""

EXTERNAL_PERSON_SQL = """SELECT
    b.DEPARTMENT_ID, a.FK_DEPARTMENT_ID AS TYPE4, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0}
    AND b.TYPE in (8, 9)
    AND b.DEPARTMENT_ID IN {1};
"""