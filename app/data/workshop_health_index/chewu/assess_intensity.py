#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Author: seerbigdata
Date: 2019/8/2
Description: 考核力度指数
"""
import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.assess_intensity_sql import (
    ASSESS_QUANTIFY_SQL, ASSESS_RESPONSIBLE_SQL, ASSESS_REVISE_SQL,
    ASSESS_RROBLEM_SQL, AWARD_RETURN_SQL)
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.workshop_health_index.common.common import (summizet_child_index)
from app.data.index.util import get_custom_month
from app.data.util import pd_query, update_major_maintype_weight
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, AssessIntensityDetailType
from app.data.workshop_health_index.chewu.common_data import cache_client
from app.data.workshop_health_index.chewu.common import MAJOR
from app.data.workshop_health_index.common import assess_intensity
from app.data.workshop_health_index.utils import group_and_merge_with_dpid


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _summizet_assess_money(year, month):
    data = pd.concat(
        [
            pd_query(each_sql.format(year, month)) for each_sql in [
            ASSESS_REVISE_SQL, ASSESS_RROBLEM_SQL, ASSESS_QUANTIFY_SQL,
            ASSESS_RESPONSIBLE_SQL
        ]
        ],
        axis=0)
    return data


def _get_sql_data(months_ago):
    global ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, STAFF_NUMBER, PROBLEM_COUNT_NOPUOTWAY, DEPARTMENT_DATA

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 单位总人数
    WORK_LOAD = cache_client.get('WORK_LOAD', True)

    # 问题总数（剔除路外问题）
    # PROBLEM_COUNT_NO_OUT_WAY = df_merge_with_dpid(
    #     pd_query(CHECK_PROBLEM_NOT_OUT_WAY_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 考核问题数
    responsible_problem = cache_client.get('check_problem_and_responsible', months_ago=months_ago)

    assess_problem = responsible_problem[(responsible_problem['IS_SPAN_DEPARTMENT'] == 0) &
                                         (responsible_problem['IS_EXTERNAL'] == 0) &
                                         (responsible_problem['IS_ASSESS'] == 1)]
    ASSESS_PROBLEM_COUNT = group_and_merge_with_dpid(assess_problem, DEPARTMENT_DATA,
                                                     'FK_DEPARTMENT_ID', '', 'COUNT')

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month)), DEPARTMENT_DATA)
    ASSESS_RESPONSIBLE_MONEY = ASSESS_RESPONSIBLE_MONEY[
        ASSESS_RESPONSIBLE_MONEY['FK_CHECK_PROBLEM_ID'].isin(responsible_problem['PK_ID'].values)]

    # 月度返奖明细
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column):
    _score = 60
    if row[major_column] == 0:
        return 0
    if row[column] == 0.0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio <= -0.1:
        deduction = _ratio * 100
        _score = 100 + deduction
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_score_by_formula_return(row, column, major_column):
    _score = 60
    if row[major_column] == 0:
        return 0
    if row[column] == 0.0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        deduction = _ratio * 100
        _score = 100 + deduction
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score

# 换算人均考核问题数
# def _stats_check_problem_assess_radio(months_ago):
#     return calc_child_index_type_divide(
#         ASSESS_PROBLEM_COUNT, STAFF_NUMBER, 1, 3, 1, months_ago, 'COUNT',
#         'SCORE_a', _calc_score_by_formula, _choose_dpid_data)


# 换算人均考核问题数
def _stats_check_problem_assess_radio(months_ago):
    content_tpl = "<p>得分：{0}</p><p>同类车间平均得分：{6}</p><p>排名: {1}</p><p>基数: {2}</p>" \
                  "<p>换算人均考核问题数({3}) = 考核问题数({4})/工作量 ({5})</p>"
    return assess_intensity.stats_check_problem_assess_radio(
        ASSESS_PROBLEM_COUNT, WORK_LOAD, months_ago, _calc_score_by_formula, _choose_dpid_data, content_tpl)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    return assess_intensity.stats_assess_money_per_person(
        ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER, months_ago, _calc_score_by_formula, _choose_dpid_data)


# 返奖率
def _stats_award_return_ratio(months_ago):
    return assess_intensity.stats_award_return_ratio(
        AWARD_RETURN_MONEY, ASSESS_RESPONSIBLE_MONEY, months_ago, _calc_score_by_formula_return, _choose_dpid_data)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio,
        _stats_assess_money_per_person,
        # _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b']]
    item_weight = [0.5, 0.5]
    child_index_list = [AssessIntensityDetailType.stats_check_problem_assess_ratio,
                        AssessIntensityDetailType.stats_assess_money_per_person]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.assess_intensity,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.assess_intensity, months_ago,
                         item_name, item_weight, major=MAJOR)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
