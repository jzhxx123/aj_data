#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
Author: seerbigdata
Date: 2019/8/2
Description: 自控能力指数
"""


import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.self_control_ability_sql import (EVALUATE_SCORE_SQL,)
from app.data.workshop_health_index.utils import group_and_merge_with_dpid
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.index.util import get_custom_month
from app.data.util import pd_query, update_major_maintype_weight

from app.data.workshop_health_index.common.common import calc_check_count_per_person, summizet_child_index
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, EvaluateIntensityDetailType
from app.data.workshop_health_index.chewu.common_data import cache_client
from app.data.workshop_health_index.chewu.common import MAJOR
from app.data.workshop_health_index.common import evaluate_intensity


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago):
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, CHECK_COUNT, PROBLEM_COUNT, \
        ASSESS_PROBLEM_COUNT, PROBLEM_SCORE, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER
    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 工作量
    WORK_LOAD = cache_client.get('WORK_LOAD', True)

    current_app.logger.debug('|   └── esxtract data from mysql have done!')


def _calc_evaluate_score_with_ratio(row):
    if 1 <= row['FK_PERSON_GRADATION_RATIO_ID'] < 4:
        return row['SCORE'] * row['GRADATION_RATIO']
    return row['SCORE']


def _calc_department_evaluate_score(row):
    """
    100分-评价扣分合计*8/车间人数*100
    :param row:
    :return:
    """
    deduct = round((row['EVALUATE_SCORE'] * 8 / row['NUMBER']) * 100, 2)
    score = max(0, round(100 - deduct, 2))
    content = '得分({0})=100-评价扣分合计({1})*8/职工人数({2})*100'.format(
        score, round(row['EVALUATE_SCORE'], 2), row['NUMBER'])
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def _stats_evaluate_intensity(months_ago):
    return evaluate_intensity.stats_evaluate_intensity(
        EVALUATE_SCORE_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, _choose_dpid_data, _calc_evaluate_score_with_ratio, _calc_department_evaluate_score)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    child_index_func = [
        _stats_evaluate_intensity
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    item_name = [
        f'SCORE_{x}' for x in ['a']
    ]
    item_weight = [1]

    child_index_list = [EvaluateIntensityDetailType.evaluate_deduct_score
                        ]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.evaluate_intensity,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.evaluate_intensity, months_ago,
                         item_name, item_weight, [HIERARCHY], major=MAJOR)

    current_app.logger.debug(
        '├── └── self_control_ability index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
