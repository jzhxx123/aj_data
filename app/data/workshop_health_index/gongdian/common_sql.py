# STATUS = 4(工作结束), RECEIPT_TIME发票日期
GD_T_WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT ty.PK_ID) as COUNT
    FROM
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(ty.RECEIPT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(ty.RECEIPT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND ty.STATUS = 4
    GROUP BY tu.DEPART_ID
"""

# 作业车工作票 STATUS = 4(工作结束), RECEIPT_TIME发票日期
GD_T_WORK_TICKET_FOR_TRUCK_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT tr.PK_ID) as COUNT
    FROM
        gd_t_work_ticket_for_truck AS tr
            INNER JOIN
        gd_t_unit AS tu ON tr.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(tr.RECEIPT_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(tr.RECEIPT_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND tr.STATUS = 4
    GROUP BY tu.DEPART_ID
"""

#  牵引变电所第一种工作票 STATUS = 5(工作结束), RECEIPT_TIME发票日期
GD_BD_WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT bd.PK_ID) as COUNT
    FROM
        gd_bd_work_ticket AS bd
            INNER JOIN
        gd_t_unit AS tu ON bd.UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND bd.STATUS = 5
    GROUP BY tu.DEPART_ID
"""