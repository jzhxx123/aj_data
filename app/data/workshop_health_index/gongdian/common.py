#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

import pandas as pd

from app.data.workshop_health_index.common.common_sql import OTHER_MANAGEMENT_SQL
from app.data.workshop_health_index.utils import get_work_load_from_sql
from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_custom_month
from app.data.util import pd_query


MAJOR = '供电'
PROFESSION_ID = 2140

department_classify_config = {
    'A': (107, 105, 83, 85),
    'B': (84, 106),
    'C': (257,)
}

department_classify_desc = {
    'A': 'Ⅰ类',
    'B': 'Ⅱ类',
    'C': 'Ⅲ类'
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])


def get_work_load(row, months_ago, tickets_count):
    classify = row['CLASSIFY']
    stats_month = get_custom_month(months_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    if classify == 'A':
        kilometre = get_work_load_from_sql(OTHER_MANAGEMENT_SQL.format('换算条公里总量', month, row['DEPARTMENT_ID']))
        tickets = tickets_count.loc[tickets_count['TYPE4'] == row['DEPARTMENT_ID']]
        tickets = tickets.iloc[0]['COUNT'] if not tickets.empty else 0
        return (kilometre / 800) * 0.4 + (tickets / 30) * 0.6
    elif classify == 'B':
        kilometre = get_work_load_from_sql(OTHER_MANAGEMENT_SQL.format('换算条公里总量', month, row['DEPARTMENT_ID']))
        tickets = tickets_count.loc[tickets_count['TYPE4'] == row['DEPARTMENT_ID']]
        tickets = tickets.iloc[0]['COUNT'] if not tickets.empty else 0
        return (kilometre / 800) * 0.4 + (tickets / 30) * 0.6
    elif classify == 'C':
        return row['COUNT']
    else:
        return row['COUNT']


def get_ticket_amount(gd_t_work_ticket_sql, gd_t_work_ticket_for_truck_sql,
                      gd_bd_work_ticket_sql, dpid, months_ago):
    """获取工作票总数"""
    stats_month = get_custom_month(months_ago)
    gd_t_work_ticket = pd_query(gd_t_work_ticket_sql.format(*stats_month), db_name='db_mid')
    gd_ticket_for_truck = pd_query(gd_t_work_ticket_for_truck_sql.format(*stats_month), db_name='db_mid')
    gd_bd_work_ticket = pd_query(gd_bd_work_ticket_sql.format(*stats_month), db_name='db_mid')

    all_ticket = df_merge_with_dpid(
        pd.concat([gd_t_work_ticket, gd_ticket_for_truck, gd_bd_work_ticket]),
        dpid
    )
    return all_ticket
