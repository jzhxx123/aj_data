# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.workshop_health_index.common.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_NUMBER_SQL,
    HAPPEN_PROBLEM_POINT_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL, PERIL_COUNT_SQL,
    RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL)
from app.data.index.common import (df_merge_with_dpid, combine_child_index_func)
from app.data.workshop_health_index.common.common import (calc_child_index_type_divide, summizet_child_index)
from app.data.index.util import (get_custom_month, get_year_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.workshop_health_index.const import MainType, INDEX_TYPE, HIERARCHY, ProblemRectificationDetailType
from app.data.workshop_health_index.keyun.common_data import cache_client
from app.data.workshop_health_index.keyun.common import department_classify_config, MAJOR
from app.data.workshop_health_index.common import problem_rectification
from app.data.workshop_health_index.utils import group_and_merge_with_dpid

SCORE = []


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


# 部门按车间聚合
def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, \
        STAFF_NUMBER, ZHANDUAN_STAFF

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = cache_client.get('STAFF_NUMBER', True)
    # 单位总人数
    WORK_LOAD = cache_client.get('WORK_LOAD', True)


def _calc_rectification_score(problem_number):
    """
    100-问题超期整改扣分；问题超期整改扣分：问题整改超期1条扣1分
    :param problem_number:
    :return:
    """
    val = 100 - 1 * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


# 整改时效
def _stats_rectification_overdue(months_ago):
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    overdue_problem_data = group_and_merge_with_dpid(
        pd_query(OVERDUE_PROBLEM_NUMBER_SQL.format(year_mon // 100, year_mon % 100)),
        DEPARTMENT_DATA, 'FK_DEPARTMENT_ID', 'COUNT', 'COUNT')
    return problem_rectification.stats_rectification_overdue(
        overdue_problem_data, DEPARTMENT_DATA, months_ago, _choose_dpid_data, _calc_rectification_score)


# # 整改履责指数
# def _stats_check_evaluate(months_ago):
#     calc_month = get_custom_month(months_ago)
#     data = df_merge_with_dpid(
#         pd_query(CHECK_EVALUATE_SZ_SCORE_SQL.format(*calc_month)),
#         DEPARTMENT_DATA)
#     # 导出中间过程数据
#     export_basic_data_dicttype(data, _choose_dpid_data(3), 6, 2, 3, months_ago,
#                                lambda x: f'本月ZG-1、2、3、4、5履职评价发生条数：{int(x)}条')
#     rst_index_score = calc_child_index_type_sum(
#         data,
#         1,
#         6,
#         2,
#         months_ago,
#         'SCORE',
#         'SCORE_b',
#         lambda x: min(round(40 + x, 2), 100),
#         _choose_dpid_data,
#         NA_value=True)
#     return rst_index_score


def _calc_check_evaluate_score(row, code_dict):
    score = 100
    for code in code_dict:
        score -= row[f'{code}_DEDUCT']
    return max(0, score)


# 整改履责指数
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_EVALUATE_SZ_NUMBER_SQL, DEPARTMENT_DATA, months_ago,
        _choose_dpid_data, _calc_check_evaluate_score)


def _calc_problem_score(risk_level, i_month):
    """根据风险等级和月份进行扣分

    Arguments:
        risk_level {str} -- 风险等级
        i_month {str} -- 前第{-i_month}月

    Returns:
        float/int -- 得分
    """
    _score = {
        '1': 1,
        '2': 0.5,
        '3': 0.2,
    }
    problem_score = _score.get(risk_level, 0) * (4 + int(i_month))
    return problem_score


# 问题控制
def _stats_repeatedly_index(months_ago):
    return problem_rectification.stats_repeatedly_index(
        HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA, WORK_LOAD, months_ago,
        _choose_dpid_data, cache_client)


def _peril_count_score_formula(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.5:
        _score = 100
    elif _ratio >= 0:
        _score = 70
    elif _ratio > -0.5:
        _score = 50
    elif _ratio > -0.6:
        _score = 30
    else:
        _score = 0
    return _score


def _calc_score(row):
    """整改成效的记分规则
    """
    base_score = [10, 2, 1]
    step_score = [5, 2, 1]
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    # 主要
    if resp_level in [1, 2, 3, 8]:
        resp_level = 2
    # 重要
    elif resp_level in [4, 5, 9]:
        resp_level = 1
    # 次要
    else:
        resp_level = 0
    main_type = row['MAIN_TYPE'] - 1
    return base_score[main_type] + (step_score[main_type] * resp_level)


# 整改成效
def _stats_rectification_effect(months_ago):
    return problem_rectification.stats_rectification_effect(
        RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, DEPARTMENT_DATA, WARNING_DELAY_SQL, months_ago, _choose_dpid_data,
        _calc_score)


def handle(months_ago):
    # 部门按车间聚合
    _get_sql_data(months_ago)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.2, 0.3, 0.5]
    child_index_list = [ProblemRectificationDetailType.stats_rectification_overdue,
                        ProblemRectificationDetailType.stats_check_evaluate,
                        ProblemRectificationDetailType.stats_repeatedly_index]
    update_major_maintype_weight(index_type=INDEX_TYPE, major=MAJOR, main_type=MainType.problem_rectification,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, INDEX_TYPE, MainType.problem_rectification, months_ago,
                         item_name, item_weight, major=MAJOR)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
