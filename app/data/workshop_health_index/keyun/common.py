#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/6
Description: 
"""
from functools import reduce

MAJOR = '客运'

department_classify_config = {
    'A': (241,),
    'B': (242,),
    'C': (263,),
    # 'D': (235,),
    'E': (252,),
    'F': (253,),
    'G': (254,),
    'H': (255,),
}

department_classify_desc = {
    'A': '客运车间',
    'B': '售票车间',
    'C': '其他车间',
    'D': '班组级中间站',
    'E': '乘务车队',
    'F': '整备车间',
    'G': '旅服车间',
    'H': '其他车间',
}

classify_config_ids = reduce(lambda x, y: x + y, [v for k, v in department_classify_config.items()])


def calc_visitor_received_count(department_id, count_data):
    """
    获取月旅客发送量
    :param department_id:
    :param count_data:
    :return:
    """
    workshop_data = count_data[count_data['TYPE4'] == department_id]
    return workshop_data['COUNT'].sum()


def get_work_load(row, visitor_received_data):
    classify = row['CLASSIFY']
    department_id = row['DEPARTMENT_ID']
    visitor_received_count = calc_visitor_received_count(department_id, visitor_received_data)
    if classify == 'A':
        # 工作量为每月旅客发送日均数/100
        return visitor_received_count / 30 / 100
    elif classify == 'B':
        return row['COUNT']
    elif classify == 'C':
        return row['COUNT']
    elif classify == 'D':
        return visitor_received_count / 30 / 10
    elif classify == 'E':
        return row['COUNT']
    elif classify == 'F':
        return row['COUNT']
    elif classify == 'G':
        return row['COUNT']
    elif classify == 'H':
        return row['COUNT']
    else:
        return row['COUNT']