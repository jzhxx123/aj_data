# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        a.FK_DEPARTMENT_ID, 1 AS COUNT, a.FK_CHECK_PROBLEM_ID
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {}
            AND b.MONTH = {}
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE_STANDARD*c.GRADATION_RATIO)*8 AS SCORE,
        COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_person_gradation_ratio AS c
        ON a.FK_PERSON_GRADATION_RATIO_ID = c.PK_ID
    WHERE
        CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, CODE, 1 AS COUNT, a.SCORE_STANDARD
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
    WHERE
        CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, a.FK_PROBLEM_BASE_ID, IS_EXTERNAL
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
    WHERE 1
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 库内问题（实际中年问题）复查数
# IMPORTANT_PROBLEM_RECHECK_COUNT_SQL = """SELECT
#         b.FK_DEPARTMENT_ID,
#         SUM(a.IMPORTANT_PROBLEM_RECHECK_COUNT) AS COUNT
#     FROM
#         t_safety_assess_month_quantify_detail AS a
#             INNER JOIN
#         t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
#     WHERE
#         b.`STATUS` = 3 AND b.YEAR = {0}
#             AND b.MONTH = {1}
#     GROUP BY b.FK_DEPARTMENT_ID;
IMPORTANT_PROBLEM_RECHECK_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID,
        IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER AS COUNT
    FROM
        t_quantify_assess_real_time
    WHERE
        YEAR = {0} AND MONTH = {1};
"""

# 以下涉及隐患库问题都(不包含已销号)
# 隐患库隐患数量
PERIL_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
    GROUP BY FK_DEPARTMENT_ID;
"""

# 延期隐患
PERIL_OVERDUE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        STATUS < 5
            AND COMPLETE_DATE < CURDATE()
    GROUP BY FK_DEPARTMENT_ID;
"""

# 长期整治
PERIL_PERIOD_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
            AND COMPLETE_DATE >= CURDATE()
            AND IS_PERIOD_RECTIFY = 1
    GROUP BY FK_DEPARTMENT_ID;
"""
#
# # 检查出的的隐患
# CHECKED_PERIL_ID_SQL = """SELECT
#         c.FK_DEPARTMENT_ID, a.FK_SAFETY_PERIL_LIB_ID AS PID
#     FROM
#         t_check_info_and_safety_peril_lib AS a
#             INNER JOIN
#         t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
#             INNER JOIN
#         t_check_info_and_person AS c ON b.ID_CARD = c.ID_CARD
#     WHERE
#         DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
#                 >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#             AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
#                 <= DATE_FORMAT('{1}', '%%Y-%%m-%%d');
# """

# 检查出的的隐患
CHECKED_PERIL_ID_SQL = """SELECT
       DISTINCT c.FK_DEPARTMENT_ID, c.PK_ID AS PID
    FROM
        t_check_info_and_safety_peril_lib AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_peril_lib AS c ON a.FK_SAFETY_PERIL_LIB_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    ;
"""
# 隐患库里的隐患
PERIL_ID_SQL = """SELECT
        FK_DEPARTMENT_ID, PK_ID AS PID
    FROM
        t_safety_peril_lib
    WHERE
        HIERARCHY = 3 AND `STATUS` < 5;
"""

# 长期整治没录入的
PERIL_RECTIFY_NO_ENTRY_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib_and_person AS a
            INNER JOIN
        t_safety_peril_lib AS b ON a.FK_SAFETY_PERIL_LIB_ID = b.PK_ID
            INNER JOIN
        t_safety_peril_lib_and_department AS c
            ON a.FK_SAFETY_PERIL_AND_DEPARTMENT_ID = c.PK_ID
    WHERE
        b.IS_PERIOD_RECTIFY
            AND b.HIERARCHY = 3
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# # 责任安全信息
# RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
#         b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
#     FROM
#         t_safety_produce_info_responsibility_unit AS a
#             INNER JOIN
#         t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
#     WHERE
#         b.RESPONSIBILITY_DIVISION_NAME = '局属单位责任'
#         AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
#             >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#         AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
#             <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
# """

# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
    WHERE
        b.RESPONSIBILITY_DIVISION_NAME = '涉及局属单位责任'
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# # 警告性预警延期--WARNING_DAYS:预警期限,WARNING_END_TIME:预警解除时间,CREATE_TIME:预警时间
# WARNING_DELAY_SQL = """
# SELECT
#         FK_DUTY_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
#     FROM
#         t_warning_notification
#     WHERE
#         DATE_FORMAT(date_add(CREATE_TIME,INTERVAL WARNING_DAYS DAY), '%%Y-%%m-%%d')
#         >= DATE_FORMAT(WARNING_END_TIME, '%%Y-%%m-%%d')
#         AND DATE_FORMAT(WARNING_END_TIME, '%%Y-%%m-%%d')
#             >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#         AND DATE_FORMAT(WARNING_END_TIME, '%%Y-%%m-%%d')
#             <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
#     GROUP BY FK_DUTY_DEPARTMENT_ID
# """

# 警告性预警延期--CREATE_TIME:延长时间（开始延长）,HIERARCHY(路局层),TYPE=2(延长预警考核)
WARNING_DELAY_SQL = """
SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_warning_notification_assess
    WHERE
        HIERARCHY = 1
        AND
        TYPE = 2
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY FK_DEPARTMENT_ID
"""
