#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/17
Description: 
"""

import pandas as pd

from app.data.workshop_health_index.common.common import (summizet_operation_set, calc_check_count_per_person,
                                                          combine_and_format_basic_data_to_mongo,
                                                          add_avg_number_by_classify)
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, AssessIntensityDetailType


# 换算人均考核问题数
def stats_check_problem_assess_radio(assess_problem_count, staff_number, months_ago, calc_func,
                                     choose_dpid_data, content_tpl=None):
    return calc_check_count_per_person(
        assess_problem_count, staff_number, INDEX_TYPE, MainType.assess_intensity,
        AssessIntensityDetailType.stats_check_problem_assess_ratio, months_ago, 'COUNT',
        'SCORE_a', calc_func, choose_dpid_data, content_tpl=content_tpl)


# 月人均考核金额
def stats_assess_money_per_person(assess_responsible_money, staff_number, months_ago, calc_func,
                                  choose_dpid_data):
    return calc_check_count_per_person(
        assess_responsible_money, staff_number, INDEX_TYPE, MainType.assess_intensity,
        AssessIntensityDetailType.stats_assess_money_per_person, months_ago, 'COUNT',
        'SCORE_b', calc_func, choose_dpid_data)


# 返奖率
def stats_award_return_ratio(award_return_money, assess_responsible_money, months_ago, calc_func,
                             choose_dpid_data):
    return calc_check_count_per_person(
        award_return_money, assess_responsible_money, INDEX_TYPE, MainType.assess_intensity,
        AssessIntensityDetailType.stats_award_return_ratio, months_ago, 'COUNT',
        'SCORE_c', calc_func, choose_dpid_data)


# 返奖率 按问题分类
def stats_award_return_ratio_by_problem(award_return_money, months_ago, calc_func, choose_dpid_data):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    if award_return_money.empty:
        return None
    main_type = MainType.assess_intensity
    detail_type = AssessIntensityDetailType.stats_award_return_ratio
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = award_return_money[award_return_money['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby([f'TYPE{HIERARCHY}']).size()
        prob_number = idata[idata['MONEY'] > 0]
        prob_number = prob_number.groupby([f'TYPE{HIERARCHY}']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        # rst_child_data = calc_extra_child_score_groupby_major(
        #     idata.copy(),
        #     choose_dpid_data(HIERARCHY),
        #     'ratio',
        #     calc_score_by_formula,
        #     weight=child_weight[i],
        #     detail_type=3)

        rst_child_data = add_avg_number_by_classify(idata, choose_dpid_data(HIERARCHY), 'award', 'prob', main_type,
                                                    detail_type, months_ago)

        rst_child_data['SCORE'] = rst_child_data.apply(
            lambda row: calc_func(row, 'ratio', 'AVG_NUMBER') * child_weight[i],
            axis=1)
        # if weight is not None:
        #     data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
        rst_child_data = pd.DataFrame(
                index=rst_child_data['DEPARTMENT_ID'],
                data=rst_child_data.loc[:, 'SCORE'].values,
                columns=['SCORE'])
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 返奖时限问题个数（{2}）'.format(title[i], row['award'],
                                                                           row['prob']), axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data, choose_dpid_data(HIERARCHY), months_ago, HIERARCHY, main_type, detail_type,
        index_type=INDEX_TYPE)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_{HIERARCHY}'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(df_rst, choose_dpid_data(HIERARCHY), column, HIERARCHY, INDEX_TYPE,
                           main_type, detail_type, months_ago)
    return [df_rst]


# 考核率
def stats_problem_assess_ratio(assess_problem_count, problem_count_not_outway, months_ago,
                               calc_func, choose_dpid_data):
    return calc_check_count_per_person(
        assess_problem_count, problem_count_not_outway, INDEX_TYPE, MainType.assess_intensity,
        AssessIntensityDetailType.stats_problem_assess_ratio, months_ago, 'COUNT',
        'SCORE_d', calc_func, choose_dpid_data)
