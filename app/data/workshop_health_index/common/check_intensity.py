#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/16
Description: 
"""
import pandas as pd

from app.data.workshop_health_index.utils import group_and_merge_with_dpid
from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, CheckIntensityDetailType
from app.data.workshop_health_index.common.common import (
    calc_child_index_type_divide, calc_check_count_per_person,
    summizet_operation_set, write_export_basic_data_to_mongo,
    add_avg_number_by_classify, format_export_basic_data)


# 人均检查频次
def stats_check_per_person(check_count, work_load, months_ago, calc_func,
                           choose_dpid_data):
    return calc_check_count_per_person(
        check_count, work_load, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_check_per_person, months_ago, 'COUNT', 'SCORE_b', calc_func,
        choose_dpid_data)


# 问题查处力度
def stats_check_problem_ratio(problem_count, work_load, months_ago,
                              calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        problem_count, work_load, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_check_problem_ratio, months_ago, 'COUNT', 'SCORE_c',
        calc_func, choose_dpid_data)


# 查处问题考核率
def stats_check_problem_assess_ratio(assess_problem_count, problem_count, months_ago,
                                     calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        assess_problem_count, problem_count, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_check_problem_assess_ratio, months_ago, 'COUNT', 'SCORE_d',
        calc_func, choose_dpid_data)


# 问题平均质量分
def stats_score_per_check_problem(problem_score, problem_count,  months_ago,
                                  calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        problem_score, problem_count, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_score_per_check_problem, months_ago, 'COUNT', 'SCORE_e',
        calc_func, choose_dpid_data)


# 人均质量分
def stats_score_per_person(problem_score, work_load, months_ago,
                           calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        problem_score, work_load, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_score_per_person, months_ago, 'COUNT', 'SCORE_f',
        calc_func, choose_dpid_data)


# 夜查率
def stats_yecha_ratio(yecha_count, check_count, months_ago,
                      calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        yecha_count, check_count, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_yecha_ratio, months_ago, 'COUNT',
        'SCORE_g', calc_func, choose_dpid_data)


def _calc_media_val_person(series, work_load, calc_func, choose_dpid_data, idx, main_type, detail_type, months_ago):
    work_load = work_load.groupby([f'TYPE{HIERARCHY}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data.dropna(inplace=True)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    # 计算结果

    data = add_avg_number_by_classify(data, choose_dpid_data(HIERARCHY), 'media', 'PERSON_NUMBER', main_type,
                                      detail_type, months_ago, f'BASIC_CURRENT_{idx}')

    data['SCORE'] = data.apply(
        lambda row: calc_func(row, 'ratio', 'AVG_NUMBER'),
        axis=1)
    # if weight is not None:
    #     data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])

    # 中间计算数据
    title = [
        '监控调阅时长累计({0})/工作量({1})', '监控调阅发现问题数({0})/工作量({1})',
        '监控调阅发现问题质量分累计({0})/工作量({1})', '调阅班组数({0})/班组数({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["media"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data = data.set_index('DEPARTMENT_ID')
    common_df = pd.DataFrame(
        index=data.index,
        data=data.loc[:, ['CLASSIFY', 'MAJOR', 'NAME']].values,
        columns=['CLASSIFY', 'MAJOR', 'NAME'])
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER', 'CLASSIFY', 'CLASSIFY_CONFIG', 'TYPE3', 'NAME', 'MAJOR'],
        inplace=True, axis=1)
    data.rename(columns={'AVG_NUMBER': f'AVG_NUMBER_{idx}',
                         'SCORE': f'SCORE_{idx}', 'BASIC_CURRENT': f'BASIC_CURRENT_{idx}'}, inplace=True)
    return rst_data, data, common_df


# 监控调阅人均时长
def _calc_media_time_per_person(media_cost_time, work_load, calc_func, choose_dpid_data,
                                department_data, idx, main_type, detail_type, months_ago):
    media_time = df_merge_with_dpid(
        media_cost_time, department_data)
    media_time = media_time.groupby([f'TYPE{HIERARCHY}'])['TIME'].sum()
    return _calc_media_val_person(media_time, work_load, calc_func, choose_dpid_data, idx,
                                  main_type, detail_type, months_ago)


# 监控调阅人均问题个数
def _calc_media_problem_per_person(check_info_and_person, work_load, calc_func, choose_dpid_data, department_data,
                                   idx, main_type, detail_type, months_ago):
    media_problem_number = check_info_and_person[check_info_and_person['CHECK_WAY'] == 3]
    media_problem_number = group_and_merge_with_dpid(media_problem_number, department_data, 'FK_DEPARTMENT_ID',
                                                     'PROBLEM_NUMBER', 'NUMBER')
    media_problem_number = media_problem_number.groupby([f'TYPE{HIERARCHY}'])['NUMBER'].sum()
    return _calc_media_val_person(media_problem_number, work_load, calc_func, choose_dpid_data, idx,
                                  main_type, detail_type, months_ago)


# 监控调阅人均质量分
def _calc_media_score_per_person(check_problem_and_info, work_load, calc_func, choose_dpid_data, department_data,
                                 idx, main_type, detail_type, months_ago):
    media_problem_score = check_problem_and_info[check_problem_and_info['CHECK_WAY'] == 3]
    media_problem_score = group_and_merge_with_dpid(media_problem_score, department_data, 'FK_DEPARTMENT_ID',
                                                    'CHECK_SCORE', 'SCORE')
    media_problem_score = media_problem_score.groupby([f'TYPE{HIERARCHY}'])['SCORE'].sum()
    return _calc_media_val_person(media_problem_score, work_load, calc_func, choose_dpid_data, idx,
                                  main_type, detail_type, months_ago)


# 监控调阅覆盖比例(调阅班组数/作业或施工班组数)
def _calc_media_banzu_val_workbanzu(df_list, work_load, calc_func, choose_dpid_data, department_data, idx,
                                    main_type, detail_type, months_ago):
    # 调阅班组数
    watch_media_banzu_count = df_list[0]
    # 作业班组数
    work_banzu_count = df_list[1]
    watch_media_banzu_count['NUMBER'] = 1
    media_time = watch_media_banzu_count.groupby(
        [f'TYPE{HIERARCHY}'])['NUMBER'].sum()
    return _calc_media_val_person(media_time, work_banzu_count, lambda row, col, n: row[col] * 100,
                                  choose_dpid_data, idx, main_type, detail_type, months_ago)


# 监控调阅力度
def stats_media_intensity(department_data, work_load, months_ago,
                          calc_func, choose_dpid_data,
                          df_list=(), child_weight=None):
    main_type = MainType.check_intensity
    detail_type = CheckIntensityDetailType.stats_media_intensity
    if not child_weight:
        child_weight = [0.35, 0.35, 0.3]
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    # 保存中间计算过程数据
    calc_basic_data = []
    score = []
    child_func = [
        _calc_media_time_per_person, _calc_media_problem_per_person,
        _calc_media_score_per_person, _calc_media_banzu_val_workbanzu
    ]
    common_df = None
    for idx, weight in enumerate(child_weight):
        if weight <= 0:
            continue
        rst_func, rst_basic_data, common_df = child_func[idx](df_list[idx], work_load, calc_func, choose_dpid_data,
                                                                  department_data, idx, main_type, detail_type, months_ago)
        calc_basic_data.append(rst_basic_data)
        if rst_func is not None:
            score.append(rst_func * child_weight[idx])
    # 保存导出中间计算数据到mongo
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns if col.startswith('middle')]), axis=1)
    for com_col in ['AVG_NUMBER', 'SCORE', 'BASIC_CURRENT']:
        calc_df_data[com_col] = calc_df_data.apply(
            lambda row: {col: round(row[col], 2) for col in columns if col.startswith(com_col)}, axis=1)

    calc_df_data = pd.merge(calc_df_data, common_df, how='left', left_index=True, right_index=True)

    calc_df_data['DEPARTMENT_ID'] = calc_df_data.index
    calc_basic_data_rst = format_export_basic_data(calc_df_data, main_type, detail_type, HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, main_type, detail_type,
                                     index_type=INDEX_TYPE)
    # 合并计算子指数
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_j_{HIERARCHY}'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(df_rst, choose_dpid_data(HIERARCHY), column, HIERARCHY, INDEX_TYPE, main_type,
                           detail_type, months_ago)
    rst_child_score.append(df_rst)
    return rst_child_score


# 覆盖率
def stats_check_address_ratio(department_data, months_ago, calc_func, choose_dpid_data,
                              check_point_sql, banzu_point_sql, cache_client):
    """
    被检查地点数/车间地点总数
    :param department_data:
    :param months_ago:
    :param calc_func:
    :param choose_dpid_data:
    :param check_point_sql:
    :param banzu_point_sql:
    :param cache_client:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    # 检查地点数
    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)
    check_point = cache_client.get('check_point')
    real_check_point = pd.merge(check_info_and_address, check_point, left_on='FK_CHECK_POINT_ID', right_on='PK_ID')
    real_check_point = real_check_point[(real_check_point['HIERARCHY'] == 2)
                                        & (real_check_point['IS_DELETE'] == 0)
                                        & (real_check_point['ADDRESS_TYPE'] == 2)
                                        & (real_check_point['CHECK_WAY'].between(1, 2))]
    real_check_point.drop_duplicates(subset=['FK_CHECK_POINT_ID'], keep='first', inplace=True)
    real_check_point = real_check_point.groupby('FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_point = real_check_point.reset_index().rename(columns={'FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})

    real_check_banzu = pd.merge(check_info_and_address, department_data, left_on='ADDRESS_FK_DEPARTMENT_ID',
                                right_on='DEPARTMENT_ID')
    real_check_banzu = real_check_banzu[(real_check_banzu['ADDRESS_TYPE'] == 1)
                                        & (real_check_banzu['TYPE'] == 9)
                                        & (real_check_banzu['CHECK_WAY'].between(1, 2))]
    real_check_banzu = real_check_banzu.groupby('ADDRESS_FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_banzu = real_check_banzu.reset_index().rename(columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})
    real_check_banzu['COUNT'] = 1
    data_real = pd.concat(
        [real_check_point, real_check_banzu],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, department_data)

    # 地点总数
    check_point = pd_query(check_point_sql)
    check_banzu = pd_query(banzu_point_sql)
    data_total = pd.concat(
        [check_point, check_banzu],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, department_data)
    return calc_child_index_type_divide(
        data_real,
        data_total,
        INDEX_TYPE,
        MainType.check_intensity,
        CheckIntensityDetailType.stats_check_address_ratio,
        months_ago,
        'COUNT',
        'SCORE_i',
        calc_func,
        choose_dpid_data,
        is_calc_score_base_major=False)


# 换算较严重及以上风险问题
def stats_great_risk_problem_ratio(problem_df, work_load, months_ago, calc_func, choose_dpid_data):
    """
    换算较严重及以上风险问题
    """
    problem_df = problem_df[problem_df['RISK_LEVEL'] <= 2].copy()
    problem_df['COUNT'] = 1
    return calc_child_index_type_divide(
        problem_df, work_load, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_great_risk_problem_ratio, months_ago, 'COUNT',
        'SCORE_h', calc_func, choose_dpid_data)


# 换算较一般及以上风险问题
def stats_general_risk_problem_ratio(problem_df, work_load, months_ago, calc_func, choose_dpid_data):
    """
    换算较一般及以上风险问题
    """
    problem_df = problem_df[problem_df['RISK_LEVEL'] <= 3].copy()
    problem_df['COUNT'] = 1
    return calc_child_index_type_divide(
        problem_df, work_load, INDEX_TYPE, MainType.check_intensity,
        CheckIntensityDetailType.stats_general_risk_problem_ratio, months_ago, 'COUNT',
        'SCORE_l', calc_func, choose_dpid_data)
