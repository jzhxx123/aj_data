#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd

from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, EvaluateIntensityDetailType
from app.data.workshop_health_index.common.common import (
    format_export_basic_data, summizet_operation_set, write_export_basic_data_to_mongo)


def _calc_evaluate_deduct(row, evaluate_df):
    record_df = evaluate_df[evaluate_df['TYPE4'] == row['DEPARTMENT_ID']]
    railway_check = record_df[(record_df['CHECK_TYPE'] == 1) & ~(record_df['CODE_ADDITION'] == 'JL-2-1')].index.size
    station_check = record_df[(record_df['CHECK_TYPE'] == 2) & ~(record_df['CODE_ADDITION'] == 'JL-2-1')].index.size
    jl_2_1_check = record_df[record_df['CODE_ADDITION'] == 'JL-2-1'].index.size
    score = 100 - (railway_check * 5 + station_check * 3 + jl_2_1_check * 0.5)
    score = 0 if score < 0 else score
    content = '被路局评价：{0}条<br/>站段评价：{1}条<br/>JL-2-1：{2}条'.format(railway_check,
                                                                station_check, jl_2_1_check)
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def stat_evaluate_deduct_score(evaluate_df, months_ago, choose_dpid_data):
    """
    被评价力度指数（扣分方式，简化）：被路局评价每条扣5分，站段评价每条扣3分（其中JL-2-1扣0.5分）。
    :param evaluate_df:
    :param months_ago:
    :param choose_dpid_data:
    :return:
    """
    rst_index_score = []
    chejian_dpid_data = choose_dpid_data(HIERARCHY).copy()
    res_df = chejian_dpid_data.apply(lambda row: _calc_evaluate_deduct(row, evaluate_df), axis=1, result_type='expand')
    res_df = chejian_dpid_data.join(res_df)
    calc_basic_data_rst = format_export_basic_data(res_df, MainType.evaluate_intensity,
                                                   EvaluateIntensityDetailType.evaluate_deduct_score,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.evaluate_intensity,
                                     EvaluateIntensityDetailType.evaluate_deduct_score,
                                     index_type=INDEX_TYPE)
    column = f'SCORE_a_{HIERARCHY}'
    rst_data = pd.DataFrame(
        index=res_df['DEPARTMENT_ID'],
        data=res_df['SCORE'].values,
        columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.evaluate_intensity,
                           EvaluateIntensityDetailType.evaluate_deduct_score,
                           months_ago)
    rst_index_score.append(rst_data[[column]])
    return rst_index_score


def stats_evaluate_intensity(evaluate_score_sql, work_load, department_data, months_ago, choose_dpid_data,
                             calc_evaluate_score_with_ratio,
                             calc_department_evaluate_score):
    rst_index_score = []
    stats_month = get_custom_month(months_ago)
    evaluate_score_df = df_merge_with_dpid(pd_query(evaluate_score_sql.format(*stats_month)), department_data)
    evaluate_score_df['EVALUATE_SCORE'] = evaluate_score_df.apply(calc_evaluate_score_with_ratio, axis=1)
    evaluate_score_df.drop(columns=['SCORE'])
    evaluate_score_df = evaluate_score_df.groupby(f'TYPE{HIERARCHY}')['EVALUATE_SCORE'].sum()
    evaluate_score_df = evaluate_score_df.to_frame(name='EVALUATE_SCORE')
    work_load = work_load.groupby(f'TYPE{HIERARCHY}')['COUNT'].sum()
    df_calc = pd.concat(
        [
            evaluate_score_df,
            work_load.to_frame(name='NUMBER')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['NUMBER'], inplace=True)
    df_calc.fillna(0, inplace=True)
    df_calc = pd.merge(choose_dpid_data(HIERARCHY), df_calc, how='left', left_on='DEPARTMENT_ID', right_index=True)
    rst_df = df_calc.apply(calc_department_evaluate_score, axis=1, result_type='expand')
    df_calc = df_calc.join(rst_df)

    # 导出指数中间过程
    calc_basic_data_rst = format_export_basic_data(df_calc, MainType.evaluate_intensity,
                                                   EvaluateIntensityDetailType.evaluate_deduct_score,
                                                   HIERARCHY, months_ago, index_type=INDEX_TYPE)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.evaluate_intensity,
                                     EvaluateIntensityDetailType.evaluate_deduct_score,
                                     index_type=INDEX_TYPE)
    column = f'SCORE_a_{HIERARCHY}'
    rst_data = pd.DataFrame(
        index=df_calc['DEPARTMENT_ID'],
        data=df_calc['SCORE'].values,
        columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.evaluate_intensity,
                           EvaluateIntensityDetailType.evaluate_deduct_score,
                           months_ago, None)
    rst_index_score.append(rst_data[[column]])
    return rst_index_score
