# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
    b.TYPE3 AS FK_DEPARTMENT_ID,
    COUNT(DISTINCT a.PROBLEM_POINT) AS COUNT
FROM
    t_check_problem AS a
        LEFT JOIN
    t_department AS b ON a.EXECUTE_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <= 3
GROUP BY b.TYPE3
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PROBLEM_POINT) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 3 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
    GROUP BY FK_DEPARTMENT_ID;
"""

# 重要检查点
CHECK_POINT_COUNT_SQL = """
    SELECT 
    a.PK_ID AS CHECK_POINT_ID,
    b.PERSON_NUMBER,
    COUNT(1) AS PERSON_COUNT,
    b.SOURCE_DEPARTMENT_ID
FROM
    t_check_point AS a
        LEFT JOIN
    t_department_and_main_production_site AS b ON b.FK_ADDRESS_ID = a.PK_ID
        INNER JOIN
    t_person AS c ON b.SOURCE_DEPARTMENT_ID = c.FK_DEPARTMENT_ID
        LEFT JOIN
    t_department AS d ON d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
WHERE
    a.IS_DELETE = 0 AND c.IS_DELETE = 0
        AND a.HIERARCHY = 2
        AND b.TYPE = 2
        AND d.IS_DELETE = 0
        AND a.TYPE = 1
GROUP BY a.PK_ID , b.PERSON_NUMBER , b.SOURCE_DEPARTMENT_ID
"""

# 重要检查点关联站段
CHECK_POINT_DEPARTMENT_SQL = """SELECT
        cp.PK_ID, cp.ALL_NAME AS ADDRESS_NAME, dep.TYPE3
    FROM
        t_check_point AS cp
            INNER JOIN
        t_department AS dep
            ON cp.FK_DEPARTMENT_ID = dep.DEPARTMENT_ID
    WHERE
        cp.IS_DELETE = 0 AND cp.HIERARCHY = 2
        AND cp.TYPE = 1
"""

# 重要检查点受检次数
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS d ON a.FK_CHECK_POINT_ID = d.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND d.HIERARCHY = 2
        AND d.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND d.TYPE = 1
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.TYPE5 AS FK_DEPARTMENT_ID, COUNT(1) AS PERSON_COUNT
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
    GROUP BY a.TYPE5;
"""

# 班组关联站段
BANZU_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME, TYPE3
    FROM
        t_department
    WHERE
        TYPE = 9 AND IS_DELETE = 0
"""

# 班组受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.TYPE5 AS DEPARTMENT_ID, COUNT(1) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
    GROUP BY c.TYPE5;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID,  a.WORK_TYPE, a.WORK_TIME
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
        AND b.TYPE = 9 AND b.IS_DELETE = 0
"""

# 每日检查数-按日期
DAILY_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DAY(a.START_CHECK_TIME) AS START_DAY,
        DAY(a.END_CHECK_TIME) AS END_DAY,
        COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN 
        t_department AS c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND c.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
    GROUP BY b.FK_DEPARTMENT_ID , START_DAY, END_DAY;
"""

# 每天检查书-按时段
HOUR_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR,
        COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN 
        t_department AS c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND c.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
    GROUP BY b.FK_DEPARTMENT_ID , START_HOUR, END_HOUR;
"""

# 每月自查问题总数
MONTHLY_SELF_CHECK_PROBLEM_COUNT_SQL = """
SELECT 
    b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem AS a
    LEFT JOIN t_statistics_check_problem AS b ON b.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 15-25单位每天提交问题数
CHECK_PROBLEM_COUNT_SQL = """
SELECT 
    b.FK_DEPARTMENT_ID, DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') AS TIME,COUNT(1) AS COUNT
    FROM
        t_check_problem AS a
    LEFT JOIN t_statistics_check_problem AS b ON b.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
    GROUP BY b.FK_DEPARTMENT_ID,DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
"""
