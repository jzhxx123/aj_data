# 部门
# DEPARTMENT_SQL = """SELECT
#         a.DEPARTMENT_ID, a.TYPE2, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.HIERARCHY, a.MEDIA_TYPE
#     FROM
#         t_department AS a
#             INNER JOIN
#         t_department AS b ON a.TYPE4 = b.DEPARTMENT_ID
#     WHERE
#         b.TYPE = 8 AND b.IS_DELETE = 0
#         AND b.SHORT_NAME != ''
#         AND a.TYPE2 = '{0}'
#         AND a.IS_DELETE = 0;
# """

DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE2, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.HIERARCHY, a.MEDIA_TYPE
    FROM
        t_department AS a
    WHERE
        a.TYPE4 in {0}
        AND a.IS_DELETE = 0;
"""


ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR, a.BELONG_PROFESSION_NAME
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, a.TYPE3, b.NAME AS MAJOR,
        a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID as CLASSIFY_CONFIG
    FROM t_department AS a
            LEFT JOIN
        t_department AS b on a.TYPE2 = b.DEPARTMENT_ID
            LEFT JOIN
        t_department AS c on a.TYPE3 = c.DEPARTMENT_ID
    WHERE a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID IN {0}
        AND a.TYPE = 8
        AND a.IS_DELETE = 0
        AND c.SHORT_NAME != "";
"""


CHEJIAN_BY_PROFESSION_SQL = """SELECT 
    a.DEPARTMENT_ID,
    a.NAME,
    a.TYPE3,
    b.NAME AS MAJOR,
    a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID AS CLASSIFY_CONFIG
FROM
    t_department AS a
        LEFT JOIN
    t_department AS b ON a.TYPE2 = b.DEPARTMENT_ID
WHERE
    a.TYPE3 IN (SELECT 
            DEPARTMENT_ID
        FROM
            t_department
        WHERE
            FK_PROFESSION_DICTIONARY_ID IN ({0}))
        AND a.TYPE = 8
        AND a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID IN {1}
"""


# 职工总人数
WORK_LOAD_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
        LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0 AND b.IS_DELETE=0
        AND b.TYPE4 in {0}
    GROUP BY FK_DEPARTMENT_ID;
"""

# 外聘人员数
EXTERNAL_PERSON_SQL = """SELECT
    b.DEPARTMENT_ID, a.FK_DEPARTMENT_ID AS TYPE4, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0}
    AND b.TYPE = 8
    AND b.DEPARTMENT_ID IN {1};
"""

#
INTEGRATED_MANAGEMENT_SQL = """SELECT
        a.MANAGEMENT_INDEX AS COUNT 
    FROM
        t_department_integrated_management AS a
            INNER JOIN
        t_profession_dictionary AS b
        ON a.FK_TYPE_ID = b.PK_ID
    WHERE
        b.NAME = '{0}'
        AND a.MONTH = {1}
        AND a.FK_DEPARTMENT_ID = '{2}'
"""


OTHER_MANAGEMENT_SQL = """SELECT
        a.MANAGEMENT_INDEX AS COUNT 
    FROM
        t_department_other_management AS a
            INNER JOIN
        t_profession_dictionary AS b
        ON a.FK_TYPE_ID = b.PK_ID
    WHERE
        b.NAME = '{0}'
        AND a.FK_DEPARTMENT_ID = '{1}'
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person
    WHERE
        IS_DELETE = 0 AND FK_DEPARTMENT_ID != ''
            AND IDENTITY = '干部'
    GROUP BY FK_DEPARTMENT_ID;
"""

DEPARTMENT_TYPE_CW_SQL = """SELECT 
    FK_DEPARTMENT_ID, ASSOCIATED_STATION
FROM
    t_department_type_cw;
"""

# 车间非干部人数
NOT_CADRE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
        LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0 AND b.IS_DELETE=0
        AND b.TYPE4 in {0}
        AND IDENTITY != '干部'
    GROUP BY FK_DEPARTMENT_ID;
"""
