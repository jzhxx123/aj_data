#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/17
Description: 
"""
import pandas as pd

from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_custom_month, get_months_from_201712
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, ProblemExposureDetailType
from app.data.workshop_health_index.common.common import (
    divide_two_col, export_basic_data_one_field_monthly, calc_child_index_type_sum,
    format_export_basic_data, summizet_child_index, export_basic_data_tow_field_monthly_two,
    summizet_operation_set, write_export_basic_data_to_mongo, add_avg_number_by_classify,
    append_major_column_to_df)
from app.data.util import pd_query


def _calc_value_per_person(series, work_load, weight, choose_dpid_data,
                           calc_func, hierarchy, title, idx, sub_type,
                           main_type, detail_type, months_ago):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data.apply(lambda row: divide_two_col(row, 'prob', 'PERSON_NUMBER'), axis=1)
    data = add_avg_number_by_classify(data, choose_dpid_data(hierarchy), 'prob', 'PERSON_NUMBER',
                                      main_type, detail_type, months_ago, f'BASIC_CURRENT_{sub_type}_{idx}')

    data['SCORE'] = data.apply(
        lambda row: calc_func(row, 'ratio', 'AVG_NUMBER'),
        axis=1)

    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])

    data = data.set_index('DEPARTMENT_ID')
    common_df = pd.DataFrame(
        index=data.index,
        data=data.loc[:, ['CLASSIFY', 'MAJOR', 'NAME']].values,
        columns=['CLASSIFY', 'MAJOR', 'NAME'])

    # 计算中间过程
    if sub_type == 'number':
        data[f'number_{idx}'] = data.apply(
            lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    else:
        data[f'score_{idx}'] = data.apply(
            lambda row: title.format(f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
            axis=1)

    data.drop(
        columns=['prob', 'ratio', 'PERSON_NUMBER', 'CLASSIFY', 'CLASSIFY_CONFIG', 'TYPE3', 'NAME', 'MAJOR'],
        inplace=True, axis=1)
    data.rename(columns={'AVG_NUMBER': f'AVG_NUMBER_{sub_type}_{idx}',
                         'SCORE': f'SCORE_{sub_type}_{idx}',
                         'BASIC_CURRENT': f'BASIC_CURRENT_{sub_type}_{idx}'},
                inplace=True)

    return df_rst, data, common_df


def _calc_prob_number_per_person(df_data, department_data, work_load, weight, choose_dpid_data, calc_func,
                                 hierarchy, title, flag, main_type, detail_type, months_ago):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, choose_dpid_data,
                                  calc_func, hierarchy, title, flag, 'number',
                                  main_type, detail_type, months_ago)


def _calc_prob_score_per_person(df_data, department_data, work_load, weight, choose_dpid_data, calc_func,
                                hierarchy, title, flag, main_type, detail_type, months_ago):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(
        [f'TYPE{hierarchy}'])['PROBLEM_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, choose_dpid_data,
                                  calc_func, hierarchy, title, flag, 'score',
                                  main_type, detail_type, months_ago)


# 普遍性暴露
def stats_total_problem_exposure(problem_df_list, title, weight_item, weight_part, department_data, work_load,
                                 months_ago, choose_dpid_data, calc_func):
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    main_type = MainType.problem_exposure
    detail_type = ProblemExposureDetailType.stats_total_problem_exposure
    # 计算子指数
    for hierarchy in [HIERARCHY]:
        score = []
        common_df = None
        for i, data in enumerate(problem_df_list):
            # 人均问题数，人均质量分
            for j, func in enumerate([_calc_prob_number_per_person,
                                      _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score_data, basic_data, common_df = func(data.copy(), department_data, work_load, weight,
                                                         choose_dpid_data, calc_func,
                                                         hierarchy, title[i], i,
                                                         main_type, detail_type, months_ago)
                score.append(score_data)
                calc_basic_data.append(basic_data)

        # 中间过程
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns if col.startswith(('number', 'score'))]), axis=1)

        for com_col in ['AVG_NUMBER', 'SCORE', 'BASIC_CURRENT']:
            calc_df_data[com_col] = calc_df_data.apply(
                lambda row: {col: round(row[col], 4) for col in columns if col.startswith(com_col)}, axis=1)

        calc_df_data = pd.merge(calc_df_data, common_df, how='left', left_index=True, right_index=True)
        calc_df_data['DEPARTMENT_ID'] = calc_df_data.index
        calc_basic_data_rst = format_export_basic_data(calc_df_data, main_type, detail_type,
                                                       HIERARCHY, months_ago, index_type=INDEX_TYPE)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, main_type, detail_type,
                                         index_type=INDEX_TYPE)

        # 计算得分
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(df_rst, choose_dpid_data(hierarchy), column,
                               hierarchy, INDEX_TYPE, main_type, detail_type, months_ago)
        rst_child_score.append(df_rst)
    return rst_child_score


def stats_banzu_problem_exposure(banzu_point_sql, department_data,
                                 choose_dpid_data, months_ago, deduct_dict, cache_client):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    months_range = 3
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(df_merge_with_dpid(pd_query(banzu_point_sql), department_data)['DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    # 用来记录每个班组每个月问题为空白的个数
    exposure_banzu_num_monthly = []
    for idx in range(months_range):
        mon = get_custom_month(months_ago - idx)
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            df_merge_with_dpid(cache_client.get('check_problem_and_responsible', months_ago=months_ago),
                               department_data)['DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        if idx >= 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + deduct_dict.get(idx+1, 1)
                })
                exposure_banzu_num_monthly.append([dpid, mon[1]])
        # 一直到初始月份未暴露问题的班组
        if idx == (months_range - 1):
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + deduct_dict.get(idx+1, 1)
                })
                exposure_banzu_num_monthly.append([dpid, mon[1]])
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, department_data)
    # 导出中间计算数据

    export_basic_data_one_field_monthly(
        exposure_banzu_num_monthly,
        department_data,
        choose_dpid_data(HIERARCHY),
        MainType.problem_exposure,
        ProblemExposureDetailType.stats_banzu_problem_exposure,
        HIERARCHY,
        months_ago,
        lambda x: f'{x[:4]}年{x[5:7]}月',
        title='问题为空白的班组',
        index_type=INDEX_TYPE)
    rst_child_score = calc_child_index_type_sum(
        data, INDEX_TYPE, MainType.problem_exposure, ProblemExposureDetailType.stats_banzu_problem_exposure,
        months_ago, 'SCORE', 'SCORE_d',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data, hierarchy_list=(HIERARCHY,))
    return rst_child_score


# 事故隐患问题暴露度
def stats_problem_exposure(hidden_key_problem_sql, hidden_key_problem_month_sql,
                           department_data, months_ago, choose_dpid_data):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712(months_ago)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in choose_dpid_data(HIERARCHY).loc[:, 'DEPARTMENT_ID'].values
    }
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(pd_query(hidden_key_problem_sql)['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(hidden_key_problem_month_sql.format(
                mon[0], mon[1]))['PID'].values)
        if idx > 2 and idx < (len(stats_months) - 1):
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            # exposure_problem = hidden_problem.intersection(
            #     month_hidden_problem)
            # for each_problem in exposure_problem:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 1
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(HIERARCHY),
        MainType.problem_exposure,
        ProblemExposureDetailType.stats_problem_exposure,
        HIERARCHY,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数', index_type=INDEX_TYPE)
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob, INDEX_TYPE, MainType.problem_exposure, ProblemExposureDetailType.stats_problem_exposure,
        months_ago, 'SCORE', 'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data)
    return rst_child_score


def stats_other_problem_exposure(safety_produce_info_sql, problem_df, department_data, months_ago,
                                 choose_dpid_data):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    safety_produce_info = pd_query(safety_produce_info_sql.format(*calc_month))
    safety_produce_info = pd.merge(safety_produce_info,
                                   pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE4']),
                                   left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    self_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']],
                                      columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP'])
    self_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    other_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] != problem_df['TYPE4_RESP']],
                                       columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP'])
    other_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in choose_dpid_data(HIERARCHY).loc[:, 'DEPARTMENT_ID'].values
    }
    problem_risk_score = {
        1: 4,
        2: 2,
        3: 0.1,
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    # other_not_self_problem = other_check_problem.difference(self_check_problem)
    other_not_self_problem = other_check_problem.copy()
    other_not_self_problem = other_not_self_problem.append(self_check_problem, sort=False)
    other_not_self_problem = other_not_self_problem.append(self_check_problem, sort=False)
    other_not_self_problem = other_not_self_problem.drop_duplicates(
        subset=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4'], keep=False)

    for idx, row in other_not_self_problem.iterrows():
        problem_dpid = row['TYPE4']
        risk_level = int(row['RISK_LEVEL'])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby([f'TYPE{HIERARCHY}', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        choose_dpid_data(HIERARCHY),
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        problem_dpid = row['TYPE4']
        risk_level = row['RISK_LEVEL']
        problem_base_id = row['FK_PROBLEM_BASE_ID']
        main_type = row['MAIN_TYPE']
        if not self_check_problem[(self_check_problem['FK_PROBLEM_BASE_ID'] == problem_base_id)
                                  & (self_check_problem['TYPE4'] == problem_dpid)
                                  & (self_check_problem['RISK_LEVEL'] == risk_level)].empty:
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({

                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data,
                                                          choose_dpid_data(HIERARCHY),
                                                          MainType.problem_exposure,
                                                          ProblemExposureDetailType.other_problem_exposure,
                                                          HIERARCHY, months_ago, first_title, second_title,
                                                          columns_list=[(1, 1), (1, 2), (2, 1), (2, 2), (3, 1), (3, 2)])
    calc_df_data = pd.merge(
        other_problem_data,
        safety_data,
        right_index=True,
        left_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(lambda row:
                                                 row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"],
                                                 axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(HIERARCHY),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, MainType.problem_exposure,
                                                   ProblemExposureDetailType.other_problem_exposure,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.problem_exposure,
                                     ProblemExposureDetailType.other_problem_exposure,
                                     index_type=INDEX_TYPE)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, INDEX_TYPE, MainType.problem_exposure, ProblemExposureDetailType.other_problem_exposure,
        months_ago, 'SCORE', 'SCORE_f',
        lambda x: min(x, 30), choose_dpid_data)
    return rst_child_score


def _calc_hidden_problem_deduct(row, deduct_dict):
    score = max(100 - deduct_dict[row['DEPARTMENT_ID']]['SCORE'], 0)
    content = '本月未查出较大、重大安全风险问题项点{0}个'.format(deduct_dict[row['DEPARTMENT_ID']]['COUNT'])
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


# 较严重隐患暴露指数
def stats_hidden_problem_exposure(problem_df, problem_base_df, base_department, department_data, months_ago,
                                  choose_dpid_data):
    """同类车间内,其它单位查出的问题项点(较大、重大安全风险，且本单位基础问题库中有的),
       本单位未查出的按1分/项扣。分值=100-扣分。
       查找逻辑为
    """
    rst_index_score = []
    workshop_df = choose_dpid_data(HIERARCHY)
    checked_problem = dict()

    problem_df = problem_df[problem_df['RISK_LEVEL'] <= 2]
    self_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']],
                                      columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP'])
    self_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4', 'TYPE': 'PROBLEM_TYPE'}, inplace=True)
    self_check_problem = pd.merge(self_check_problem, workshop_df, left_on='TYPE4', right_on='DEPARTMENT_ID')
    problem_base_df = problem_base_df.rename(columns={'FK_DEPARTMENT_ID': 'BASE_DEPARTMENT_ID', 'TYPE': 'PROBLEM_TYPE'})
    self_check_problem = pd.merge(self_check_problem,
                                  pd.DataFrame(problem_base_df,
                                               columns=['PK_ID', 'PROBLEM_TYPE', 'BASE_DEPARTMENT_ID']),
                                  left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')

    deduct_dict = {
        k: {'COUNT': 0, 'SCORE': 0}
        for k in workshop_df.loc[:, 'DEPARTMENT_ID'].values
    }
    for idx, row in self_check_problem.iterrows():
        problem_base_id = row['FK_PROBLEM_BASE_ID']
        classify = row['CLASSIFY']
        problem_base_type = row['PROBLEM_TYPE']
        if (problem_base_id, classify) in checked_problem.keys():
            continue
        checked_problem[(problem_base_id, classify)] = 1
        # 每一个同类车间中，如果基础问题库中有这个问题，且未查出，则扣分
        other_workshops = workshop_df[(workshop_df['CLASSIFY'] == classify)
                                      & (workshop_df['DEPARTMENT_ID'] != row['DEPARTMENT_ID'])]
        for _, workshop_row in other_workshops.iterrows():
            department_id = workshop_row['DEPARTMENT_ID']
            if problem_base_type == 3:
                if workshop_row['TYPE3'] != row['BASE_DEPARTMENT_ID']:
                    continue
            if self_check_problem[(self_check_problem['TYPE4'] == department_id)
                                  & (self_check_problem['FK_PROBLEM_BASE_ID'] == problem_base_id)].empty:
                stat_dict = deduct_dict[department_id]
                stat_dict['COUNT'] = stat_dict.get('COUNT', 0) + 1
                stat_dict['SCORE'] = stat_dict['COUNT'] * 1

    res_df = workshop_df.apply(lambda row: _calc_hidden_problem_deduct(row, deduct_dict), axis=1, result_type='expand')
    res_df = workshop_df.join(res_df)

    calc_basic_data_rst = format_export_basic_data(res_df, MainType.problem_exposure,
                                                   ProblemExposureDetailType.stats_hidden_problem_exposure,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.problem_exposure,
                                     ProblemExposureDetailType.stats_hidden_problem_exposure,
                                     index_type=INDEX_TYPE)
    column = f'SCORE_b_{HIERARCHY}'
    rst_data = pd.DataFrame(
        index=res_df['DEPARTMENT_ID'],
        data=res_df['SCORE'].values,
        columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.problem_exposure,
                           ProblemExposureDetailType.stats_hidden_problem_exposure,
                           months_ago)
    rst_index_score.append(rst_data[[column]])

    return rst_index_score
