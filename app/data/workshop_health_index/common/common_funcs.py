#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/30
Description: 
"""

import pandas as pd

from app.data.index.common import df_merge_with_dpid


def get_on_site_checked_count_data(department_data, months_ago, cache_client, filter_self_check=False):
    """
    获取现场检查次数DataFrame（部门被检查）
    :param department_data:
    :param months_ago:
    :param cache_client:
    :param filter_self_check:
    :return:
     xianchang_check_count: 现场检查总次数
     yecha_count: 现场夜查次数
    """
    # 检查总次数（现场检查）从检查信息地点开始找，只查找地点类型为部门的
    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)
    department_checked_count = pd.merge(check_info_and_address, department_data,
                                        left_on='ADDRESS_FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    department_checked_count = department_checked_count[
        (department_checked_count['ADDRESS_TYPE'] == 1)
        # & ~(department_checked_count['CHECK_TYPE'].between(400, 499))
        & ~(department_checked_count['CHECK_TYPE'].isin((102, 103)))
        & (department_checked_count['CHECK_WAY'].between(1, 2))]

    department_yecha_check_count = department_checked_count[department_checked_count['IS_YECHA'] == 1].copy()
    department_yecha_check_count = department_yecha_check_count.drop_duplicates(subset=['TYPE4', 'CHECK_INFO_PK_ID'])

    department_checked_count = department_checked_count.drop_duplicates(subset=['TYPE4', 'CHECK_INFO_PK_ID'])

    # 多人检查算多次
    check_info_and_person = cache_client.get('check_info_and_person', months_ago=months_ago)

    # 过滤自查次数
    if filter_self_check:
        check_info_and_person = df_merge_with_dpid(check_info_and_person, department_data)
        check_info_and_person = pd.DataFrame(check_info_and_person, columns=['PK_ID', 'TYPE4'])
        check_info_and_person.rename(columns={'PK_ID': 'CHECK_INFO_PK_ID', 'TYPE4': 'TYPE4_CHECK'}, inplace=True)

        department_checked_count = pd.merge(department_checked_count, check_info_and_person, on='CHECK_INFO_PK_ID')
        department_yecha_check_count = pd.merge(department_yecha_check_count,
                                                check_info_and_person, on='CHECK_INFO_PK_ID')
        department_checked_count = department_checked_count[
            department_checked_count['TYPE4'] == department_checked_count['TYPE4_CHECK']]

        department_yecha_check_count = department_yecha_check_count[
            department_yecha_check_count['TYPE4'] == department_yecha_check_count['TYPE4_CHECK']]
    else:
        check_info_and_person = pd.DataFrame(check_info_and_person, columns=['PK_ID'])
        check_info_and_person = check_info_and_person.rename(columns={'PK_ID': 'CHECK_INFO_PK_ID'})
        department_checked_count = pd.merge(department_checked_count, check_info_and_person, on='CHECK_INFO_PK_ID')
        department_yecha_check_count = pd.merge(department_yecha_check_count,
                                                check_info_and_person, on='CHECK_INFO_PK_ID')

    department_yecha_check_count['COUNT'] = 1
    department_checked_count['COUNT'] = 1

    xianchang_check_count = department_checked_count.groupby('TYPE4', as_index=False)['COUNT'].sum()
    yecha_count = department_yecha_check_count.groupby('TYPE4', as_index=False)['COUNT'].sum()

    return xianchang_check_count, yecha_count


def get_department_checked_data(department_data, months_ago, cache_client, filter_self_check=False):
    """
    获取部门被查检查记录
    :param department_data:
    :param months_ago:
    :param cache_client:
    :param filter_self_check:
    :return:
    """
    # 从检查信息地点开始找，只查找地点类型为部门的
    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)
    department_checked_count = pd.merge(check_info_and_address, department_data,
                                        left_on='ADDRESS_FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    department_checked_count = department_checked_count[
        (department_checked_count['ADDRESS_TYPE'] == 1)]

    department_checked_count = department_checked_count.drop_duplicates(subset=['TYPE4', 'CHECK_INFO_PK_ID'])

    # 多人检查算多次
    check_info_and_person = cache_client.get('check_info_and_person', months_ago=months_ago)

    # 过滤自查次数
    if filter_self_check:
        check_info_and_person = df_merge_with_dpid(check_info_and_person, department_data)
        check_info_and_person = pd.DataFrame(check_info_and_person, columns=['PK_ID', 'TYPE4'])
        check_info_and_person.rename(columns={'PK_ID': 'CHECK_INFO_PK_ID', 'TYPE4': 'TYPE4_CHECK'}, inplace=True)

        department_checked_count = pd.merge(department_checked_count, check_info_and_person, on='CHECK_INFO_PK_ID')
        department_checked_count = department_checked_count[
            department_checked_count['TYPE4'] == department_checked_count['TYPE4_CHECK']]

    else:
        check_info_and_person = pd.DataFrame(check_info_and_person, columns=['PK_ID'])
        check_info_and_person = check_info_and_person.rename(columns={'PK_ID': 'CHECK_INFO_PK_ID'})
        department_checked_count = pd.merge(department_checked_count, check_info_and_person, on='CHECK_INFO_PK_ID')

    return department_checked_count


def merge_problem_check_score(problem_df, cache_client):
    problem_base = cache_client.get('problem_base')
    problem_df = pd.merge(problem_df,
                          pd.DataFrame(problem_base, columns=['PK_ID', 'CHECK_SCORE']),
                          left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')
    return problem_df


def get_department_media_checked_time(months_ago, cache_client):
    media_cost_time = cache_client.get('media_cost_time', months_ago=months_ago)
    media_cost_time = media_cost_time[media_cost_time['CHECK_WAY'] == 3]  # 监控调阅检查
    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)

    media_check_info_and_address = pd.merge(media_cost_time[['TIME', 'CHECK_INFO_PK_ID']],
                                            check_info_and_address, on='CHECK_INFO_PK_ID')
    media_check_info_and_address.rename(columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    return media_check_info_and_address
