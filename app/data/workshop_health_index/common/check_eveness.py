#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/17
Description: 
"""
import calendar

import pandas as pd
import numpy as np
from flask import current_app
from dateutil.relativedelta import relativedelta

from app import mongo
from app.data.workshop_health_index.common.common import (
    summizet_operation_set, calc_child_index_type_sum,
    write_export_basic_data_to_mongo, format_export_basic_data,
    calc_child_index_type_divide, export_basic_data, divide_two_col)
from app.data.workshop_health_index.utils import get_department_classify
from app.data.index.common import (df_merge_with_dpid)
from app.data.index.util import get_custom_month
from app.data.util import pd_query, get_history_months
from app.utils.common_func import get_today
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, CheckEvennessDetailType


def _get_workshop_base_problem_count(row, problem_base_count_dict, problem_base):
    station = row['TYPE3']
    if station not in problem_base_count_dict.keys():
        problem_base_count_dict[station] = problem_base[(problem_base['FK_DEPARTMENT_ID'] == station)
                                                        & (problem_base['IS_DELETE'] == 0)
                                                        & (problem_base['STATUS'] == 3)].index.size
    return problem_base_count_dict[station]


# 检查问题均衡度
def stat_check_problem_evenness(check_problem, problem_base, months_ago, calc_func, choose_dpid_data):
    rst_index_score = []
    check_problem = check_problem[check_problem['RISK_LEVEL'] <= 3]
    problem_base = problem_base[problem_base['RISK_LEVEL'] <= 3]
    check_problem = check_problem.drop_duplicates(subset=['TYPE4', 'FK_PROBLEM_BASE_ID'], keep='first')
    check_problem['COUNT'] = 1
    check_problem = check_problem.groupby(['TYPE4'], as_index=False)['COUNT'].sum()
    df_calc = pd.merge(check_problem, choose_dpid_data(HIERARCHY), left_on='TYPE4', right_on='DEPARTMENT_ID')
    problem_base_count_dict = {}

    df_calc['BASE_COUNT'] = df_calc.apply(lambda row: _get_workshop_base_problem_count(
        row, problem_base_count_dict, problem_base), axis=1)
    df_calc.rename(columns={'COUNT': 'numerator', 'BASE_COUNT': 'denominator'}, inplace=True)
    df_calc.dropna(subset=['denominator'], inplace=True)
    df_calc.fillna(0, inplace=True)
    if df_calc.empty is True:
        return rst_index_score
    df_calc['ratio'] = df_calc.apply(lambda row: divide_two_col(row, 'numerator', 'denominator'), axis=1)
    column = f'SCORE_a_{HIERARCHY}'

    df_calc['SCORE'] = df_calc.apply(
        lambda row: calc_func(row, 'ratio'), axis=1)

    # 导出指数中间过程
    export_basic_data(df_calc, MainType.check_evenness, CheckEvennessDetailType.stat_check_problem_evenness, HIERARCHY,
                      months_ago, 'numerator', 'denominator', 'ratio',
                      'AVG_NUMBER', 'SCORE', index_type=INDEX_TYPE)

    rst_data = pd.DataFrame(
        index=df_calc['DEPARTMENT_ID'],
        data=df_calc['SCORE'].values,
        columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.check_evenness,
                           CheckEvennessDetailType.stat_check_problem_evenness,
                           months_ago)
    rst_index_score.append(rst_data[[column]])
    return rst_index_score


# 判断是否是周六或者周日
def _is_weekends(year, month, day):
    if day >= current_app.config.get('UPDATE_DAY'):
        if month == 1:
            month = 12
            year -= 1
        else:
            month -= 1
    if calendar.weekday(year, month, day) > 4:
        return True
    else:
        return False


def get_month_day_start_end(months_ago, start=15, end=25):
    """获取前第-N个月的时间统计范围，该月15号到该月25号
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    Returns:
        tuple(str) -- 前-N个月的时间统计范围
    """
    today = get_today()
    date = today + relativedelta(months=months_ago)
    start_date = '{}-{:0>2}-{}'.format(date.year, date.month,
                                       start)
    end_date = '{}-{:0>2}-{}'.format(date.year, date.month,
                                     end)
    return start_date, end_date


def get_month_day(months_ago):
    """
    获取某月天数
    """
    today = get_today()
    if 25 <= today.day < 32:
        date = today + relativedelta(months=months_ago)
    else:
        date = today + relativedelta(months=months_ago - 1)
    days = calendar.monthrange(date.year, date.month)[1]
    return days


def _calc_daily_check_banzu_count(year, month, work_type_1, work_type_2,
                                  work_type_3, day):
    work_banzu = (work_type_1 + work_type_2 + work_type_3)
    if _is_weekends(year, month, int(day)):
        work_banzu -= work_type_1
    return work_banzu


def _calc_banzu_count_by_day(data):
    """班组数的统计是按照不同班组不同的工作时间来计算，且倒班班组数记为：倒班班组/倒班数

    增加描述：统计班组时，若存在多个工作制，以24小时工作制>自定义>日勤，
    （24小时-每天算几分之一班组[优先最大班制]，自定义-每天上班，日勤-工作日上班）,
    日勤制扣除周末
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, WORK_TYPE, WORK_TIME]
    """
    banzu_count = []
    for dpid, group in data.groupby(['FK_DEPARTMENT_ID']):
        work_type_list = group['WORK_TYPE'].values
        if 2 in work_type_list:
            work_time = group[group['WORK_TYPE'] == 2]['WORK_TIME'].values
            work_time = 1 / max([int(x) for x in work_time])
            work_unit = [dpid, 'work_type_2', work_time]
        elif 3 in work_type_list:
            work_unit = [dpid, 'work_type_3', 1]
        elif 1 in work_type_list:
            work_unit = [dpid, 'work_type_1', 1]
        else:
            continue
        banzu_count.append(work_unit)
    banzu_count_df = pd.DataFrame(
        data=banzu_count, columns=['FK_DEPARTMENT_ID', 'WORK_TYPE', 'COUNT'])
    return banzu_count_df


def _handle_cross_day(data, year, month):
    """检查日期组成部分，根据每次检查的跨度日期，计算检查开始到检查结束所占日期，
    若跨天，则计算为每天一次检查
    即：
    如果检查时间区间为1号23:00~2号01:00，那么记为1号、2号各一次检查
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, START_DAY, END_DAY, COUNT]
    """
    if month == 1:
        last_month = 12
        last_year = year - 1
    else:
        last_month = month - 1
        last_year = year
    day_data = []
    for idx, row in data.iterrows():
        start_day = row['START_DAY']
        end_day = row['END_DAY']
        if end_day > start_day:
            for day in range(start_day, end_day + 1):
                day_data.append([row['FK_DEPARTMENT_ID'], day, row['COUNT']])
        elif end_day == start_day:
            day_data.append([row['FK_DEPARTMENT_ID'], end_day, row['COUNT']])
        elif end_day < start_day:  # 跨月
            this_month_days = calendar.monthrange(last_year, last_month)[1]
            for day in range(start_day, this_month_days + 1):
                day_data.append([row['FK_DEPARTMENT_ID'], day, row['COUNT']])
            for day in range(1, end_day + 1):
                day_data.append([row['FK_DEPARTMENT_ID'], day, row['COUNT']])
        else:
            pass
    day_data_df = pd.DataFrame(
        data=day_data, columns=['FK_DEPARTMENT_ID', 'DAY', 'COUNT'])
    return day_data_df


def add_avg_number_by_classify(df, dpid_data, columns, main_type, detail_type, months_ago, internal_key=None):
    """
    基数取同类车间进行计算
    :return:
    """
    df = pd.merge(dpid_data, df, how='left', left_on='DEPARTMENT_ID', right_index=True)
    df.fillna(0, inplace=True)
    major = df.iloc[0]['MAJOR']
    # 获取过去的基础数据
    past_months = [get_history_months(i)[0] for i in range(months_ago - 1, months_ago - 3, -1)]
    condition = {'MAJOR': major, 'MON': {'$in': past_months}, 'MAIN_TYPE': main_type, 'DETAIL_TYPE': detail_type}
    past_basic_data = mongo.db['monthly_workshop_health_index_basic_data'].find(condition,
                                                                                {'_id': 0,
                                                                                 'BASIC_CURRENT': 1,
                                                                                 'MON': 1, 'CLASSIFY': 1})
    past_basic_data = pd.DataFrame(list(past_basic_data))

    for cls, df_cls in df.groupby('CLASSIFY'):
        total_check_count = df_cls.loc[:, columns].sum().sum()
        total_banzu_count = df_cls.loc[:, [f'banzu_count_{day}' for day in columns]].sum().sum()
        basic = total_check_count / total_banzu_count if total_banzu_count > 0 else 0
        df.loc[df['CLASSIFY'] == cls, 'BASIC_CURRENT'] = basic
        # 基数取三个月平均数
        stat_mon_count = 1
        if not past_basic_data.empty:
            for past_mon in past_months:
                record = past_basic_data[(past_basic_data['MON'] == past_mon) & (past_basic_data['CLASSIFY'] == cls)]
                if not record.empty:
                    record = record.iloc[0]
                    if isinstance(record['BASIC_CURRENT'], dict):
                        past_basic_current = record['BASIC_CURRENT'].get(internal_key)
                    else:
                        past_basic_current = record['BASIC_CURRENT']
                    if past_basic_current:
                        basic += past_basic_current
                        stat_mon_count += 1

        basic = basic / stat_mon_count
        df.loc[df['CLASSIFY'] == cls, 'AVG_NUMBER'] = basic

    df.fillna(0, inplace=True)
    return df


def _cal_check_banzu_evenness_score(row, columns):
    """计算逻辑：
    基准值（同类车间）=Σ日检查次数/Σ日作业班组数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算基准值:
    avg_check_count = row['AVG_NUMBER']

    # 保存中间计算过程
    unfinished_day = {
        '检查数低于比较值基数20%的日期': [],
        '检查数低于比较值基数50%的日期': [],
        '检查数低于比较值基数100%的日期': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for day in columns:
        day_banzu_count = row[f'banzu_count_{day}']  # 日班组数
        day_check_count = row[day]  # 日检查数
        base_check_count = day_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (day_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            daily_deduction = 0
        else:
            if ratio <= -1:
                daily_deduction = -3
                deduct_type = '检查数低于比较值基数100%的日期'
            elif ratio <= -0.5:
                daily_deduction = -2
                deduct_type = '检查数低于比较值基数50%的日期'
            elif ratio <= -0.2:
                daily_deduction = -1
                deduct_type = '检查数低于比较值基数20%的日期'
            deduct_day = unfinished_day.get(deduct_type)
            deduct_day.append(f'{day}号[基准值：{avg_check_count:.1f},' +
                              f'班组数：{day_banzu_count:.1f}, ' +
                              f'比较值：{base_check_count:.1f}，' +
                              f'实际检查值：{day_check_count:.1f}]')
            unfinished_day.update({deduct_type: deduct_day})
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_day.items()])
    return total_score, rst_calc_data


# 检查时间均衡度 - 检查日期均衡度
def stats_check_day_evenness(check_info_df, daily_check_banzu_count, department_data, months_ago, choose_dpid_data):
    main_type = MainType.check_evenness
    detail_type = CheckEvennessDetailType.stats_check_day_evenness
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [HIERARCHY]:
        banzu_count = _calc_banzu_count_by_day(daily_check_banzu_count)
        data = df_merge_with_dpid(banzu_count, department_data)
        if data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE'])['COUNT'].sum()
        xdata = xdata.unstack()
        columns = xdata.columns.tolist()
        for work_type in ['work_type_1', 'work_type_2', 'work_type_3']:
            if work_type not in columns:
                xdata[work_type] = np.nan
        xdata.dropna(
            how='all',
            subset=['work_type_1', 'work_type_2', 'work_type_3'],
            inplace=True)
        xdata = xdata.fillna(0)
        # 每日班组实际检查数
        # 如果检查的时间跨天，则分别计数，eg: DAY_START != DAY_END
        chejian_dpid_list = tuple(choose_dpid_data(hierarchy)['DEPARTMENT_ID'].values.tolist())
        daily_check_count = check_info_df[(check_info_df['CHECK_WAY'].between(1, 2)
                                          & (check_info_df['TYPE4'].isin(chejian_dpid_list)))].copy()
        daily_check_count['START_DAY'] = check_info_df['START_CHECK_TIME'].apply(lambda value: value.day)
        daily_check_count['END_DAY'] = check_info_df['END_CHECK_TIME'].apply(lambda value: value.day)
        daily_check_count['COUNT'] = 1
        daily_check_count = daily_check_count.groupby(['FK_DEPARTMENT_ID', 'START_DAY', 'END_DAY'],
                                                      as_index=False)['COUNT'].sum()
        data_check = df_merge_with_dpid(
            _handle_cross_day(
                daily_check_count, year,
                month), department_data)
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        cds = 0
        for day in columns:
            cds += 1
            new_column = f'banzu_count_{day}'
            xdata[new_column] = xdata.apply(
                lambda row: _calc_daily_check_banzu_count(
                    year, month,
                    row['work_type_1'],
                    row['work_type_2'],
                    row['work_type_3'],
                    day),
                axis=1)
        column = f'SCORE_b_{hierarchy}'
        xdata = add_avg_number_by_classify(xdata, choose_dpid_data(hierarchy), columns, main_type, detail_type,
                                           months_ago)
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(row, columns)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(row, columns)[1],
            axis=1)
        xdata['SCORE'] = xdata[column]
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), main_type, detail_type, HIERARCHY, months_ago)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, main_type, detail_type,
                                         index_type=INDEX_TYPE)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column, hierarchy, INDEX_TYPE, main_type,
                               detail_type, months_ago)
        rst_index_score.append(xdata)
    return rst_index_score


def _calc_hour_code(hour):
    """1-12 分别代表0-23时， 每2个小时一个时段，左闭又开,ex: [4: 6)
    Arguments:
        hour {int} -- 0 - 24时
    Returns:
        [int] -- 1- 12代表12个时段
    """
    code = 1 + (hour // 2)
    return code


def calc_between_hours(start_hour, end_hour):
    """计算开始结束时间之间的时段间隔
    Arguments:
        start_hour {int} -- 0 - 23
        end_hour {int} -- 0 -23
    Returns:
        [list[int]] --
    """
    start_hour = _calc_hour_code(start_hour)
    end_hour = _calc_hour_code(end_hour)
    # 跨天
    if start_hour > end_hour:
        return list(range(start_hour, 13)) + list(range(1, end_hour + 1))
    else:
        return list(range(start_hour, end_hour + 1))


def calc_banzu_count_by_hour(data):
    """班组数的统计是按照不同班组不同的工作时间来计算，且倒班班组数记为：倒班班组/倒班数

    增加描述：统计班组时，若存在多个工作制，24小时工作制>自定义（两小时一间隔）>日勤，
    （24小时-每天算几分之一班组[优先最大班制]，自定义-按时段计算，日勤-工作日上班, 上班时间）,
    上班时间为（8:00-12:00， 2:00-5:30）
    WORK_TIME: 24小时制度（2:二班倒、3:三班倒，1：四班倒）  自定义（两小时一个间隔）
    4、00:00 ~ 02:00   5、02:00~04:00   6、04:00~06:00 7、06:00~08:00
    8、08:00~10:00  9、10:00~12:00  10、12:00~14:00  11、14:00~16:00
    12、16:00~18:00  13、18:00~20:00   14、20:00~22:00  15、22:00~00:00
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, WORK_TYPE, WORK_TIME]
    """
    banzu_count = []
    for dpid, group in data.groupby(['FK_DEPARTMENT_ID']):
        # 计算每个班组在哪些时段有过工作
        work_type_list = group['WORK_TYPE'].values
        if 2 in work_type_list:
            work_time = group[group['WORK_TYPE'] == 2]['WORK_TIME'].values
            work_time = 1 / max([int(x) for x in work_time])
            work_unit = [[dpid, hour, work_time] for hour in range(1, 13)]
        else:
            # 保存日勤制和自定义工作的工作时段集合
            hour_list = []
            for each in group.values:
                if 3 == each[1]:
                    hour_list.extend([int(x) - 3 for x in each[2].split(',')])
                if 1 == each[1]:
                    hour_list.extend([5, 6, 8, 9])
                    if 3 not in work_type_list:
                        break
            work_unit = [[dpid, hour, 1] for hour in set(hour_list)]
        banzu_count.extend(work_unit)
    banzu_count_df = pd.DataFrame(
        data=banzu_count, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return banzu_count_df


def handle_cross_hour(data, year, month):
    """1.检查时段组成部分，根据每次检查的跨越时段，从0点开始，每两个小时算一个时段，
    计算从检查开始到检查结束的区间，其中每个时段都算在这个时段内的一次检查。
    即：如果从11:00至15:00的检查，
    那么就记为10:00~12:00、12:00~14:00、14:00~16:00各一次检查
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, START_HOUR, END_HOUR, COUNT]
    """
    if month == 1:
        last_month = 12
        last_year = year - 1
    else:
        last_month = month - 1
        last_year = year
    hour_data = []
    for idx, row in data.iterrows():
        start_day = int(row['START_HOUR'][5:7])
        end_day = int(row['END_HOUR'][5:7])
        start_hour = int(row['START_HOUR'][11:])
        end_hour = int(row['END_HOUR'][11:])
        for hour in calc_between_hours(start_hour, end_hour):
            hour_data.append([row['FK_DEPARTMENT_ID'], hour, row['COUNT']])
        if end_day > start_day:
            # 开始结束日期不在同一天，而且不跨月
            # 再加上中间间隔的天数
            interval_days = (end_day - start_day)
        elif end_day < start_day:
            # 开始时间结束日期不在同一天，而且跨月
            # 再加上中间间隔的天数
            this_month_days = calendar.monthrange(last_year, last_month)[1]
            interval_days = (this_month_days - start_day) + end_day
        else:
            continue
        if start_hour > end_hour:
            interval_days -= 1
        # 计算中间间隔天数的时段
        for hour in range(1, 13):
            hour_data.append(
                [row['FK_DEPARTMENT_ID'], hour, interval_days * row['COUNT']])
    hour_data_df = pd.DataFrame(
        data=hour_data, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return hour_data_df


def _cal_deduct_score_by_hour(row, columns):
    """计算逻辑：
    基准值（同类车间）=Σ时段检查次数/Σ时段作业班组数
    应检查值=该时段作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣2分/日，低于50%的扣4分/日，
    低于100%的扣8分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/时段和次数/时段]]
        columns {[list]} -- [所有日期列]]

    当站段为桥路大修段时，因为不存在班组，默认给检查班组30
    """
    # 计算基准值:
    avg_check_count = row['AVG_NUMBER']

    # 保存中间计算过程
    unfinished_hour = {
        '检查数低于比较值20%的时段': [],
        '检查数低于比较值50%的时段': [],
        '检查数低于比较值100%的时段': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for hour in columns:
        hour_banzu_count = row[f'banzu_count_{hour}']  # 该时段班组数
        hour_check_count = row[hour]  # 该时段检查数
        base_check_count = hour_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (hour_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            hour_deduction = 0
        else:
            if ratio <= -1:
                hour_deduction = -3
                deduct_type = '检查数低于比较值100%的时段'
            elif ratio <= -0.5:
                hour_deduction = -4
                deduct_type = '检查数低于比较值50%的时段'
            elif ratio <= -0.2:
                hour_deduction = -2
                deduct_type = '检查数低于比较值20%的时段'
            deduct_hour = unfinished_hour.get(deduct_type)
            hour_title = f'{(hour-1)*2} - {hour*2}时'
            deduct_hour.append(f'{hour_title}[基准值：{avg_check_count:.1f},' +
                               f'班组数：{hour_banzu_count:.1f}，' +
                               f'比较值：{base_check_count:.1f}' +
                               f'实际检查值：{hour_check_count:.1f}]')
            unfinished_hour.update({deduct_type: deduct_hour})
        score.append(hour_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_hour.items()])
    return total_score, rst_calc_data


def stats_check_hour_evenness(check_info_df, daily_check_banzu_count, department_data, months_ago, choose_dpid_data):
    main_type = MainType.check_evenness
    detail_type = CheckEvennessDetailType.stats_check_hour_evenness
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [HIERARCHY]:
        banzu_count = calc_banzu_count_by_hour(
            daily_check_banzu_count)
        data = df_merge_with_dpid(banzu_count, department_data)
        if data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])['COUNT'].sum()
        xdata = xdata.unstack()
        xdata = xdata.fillna(0)
        # 重命名列名
        xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        chejian_dpid_list = tuple(choose_dpid_data(hierarchy)['DEPARTMENT_ID'].values.tolist())
        hour_check_count = check_info_df[(check_info_df['CHECK_WAY'].between(1, 2)
                                           & (check_info_df['TYPE4'].isin(chejian_dpid_list)))].copy()
        hour_check_count['START_HOUR'] = hour_check_count['START_CHECK_TIME'].apply(
            lambda value: value.strftime('%Y-%m-%d %H'))
        hour_check_count['END_HOUR'] = hour_check_count['END_CHECK_TIME'].apply(
            lambda value: value.strftime('%Y-%m-%d %H'))
        hour_check_count['COUNT'] = 1
        hour_check_count = hour_check_count.groupby(['FK_DEPARTMENT_ID', 'START_HOUR', 'END_HOUR'],
                                                    as_index=False)['COUNT'].sum()

        data_check = df_merge_with_dpid(
            handle_cross_hour(
                hour_check_count, year,
                month), department_data)
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'WORK_HOUR'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)
        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)

        column = f'SCORE_d_{hierarchy}'
        xdata = add_avg_number_by_classify(xdata, choose_dpid_data(hierarchy), columns,
                                           main_type, detail_type, months_ago)
        xdata[column] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[0], axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[1], axis=1)

        xdata['SCORE'] = xdata[column]
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), main_type, detail_type, HIERARCHY,
                                                       months_ago, index_type=INDEX_TYPE)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, main_type, detail_type,
                                         index_type=INDEX_TYPE)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column, HIERARCHY, INDEX_TYPE, main_type,
                               detail_type, months_ago)
        rst_index_score.append(xdata)
    return rst_index_score


def _calc_avg_check_count_by_chejian(data):
    """基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    地点工作人数=该地点工作的班组总人数
    应检查值=地点作业人数*基准值
    """
    # 计算总工作人数(作业人数PERSON_NUMBER为0时补充为部门人数PERSON_COUNT)
    data['PERSON_NUMBER'] = data.apply(
        lambda row: row['PERSON_NUMBER'] if row['PERSON_NUMBER'] > 0 else row['PERSON_COUNT'], axis=1)
    chejian_person_number = data.groupby(['TYPE4'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        chejian_person_number.to_frame(name='SUM_PERSON_NUMBER'),
        how='left',
        left_on='TYPE4',
        right_index=True)
    # 每个地点工作人数(不同班组聚合)
    address_data = data.groupby(['ADDRESS_NAME'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        address_data.to_frame(name='ADDRESS_PERSON_NUMBER'),
        how='left',
        left_on='ADDRESS_NAME',
        right_index=True)
    data.drop_duplicates(subset=['ADDRESS_NAME'], keep='first', inplace=True)

    # 计算站段总检查次数
    chejian_check_count = data.groupby(['TYPE4'])['CHECK_COUNT'].sum()
    data = pd.merge(
        data,
        chejian_check_count.to_frame(name='SUM_CHECK_COUNT'),
        how='left',
        left_on='TYPE4',
        right_index=True)
    # 计算基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    data['BASE_CHECK_COUNT'] = data.apply(
        lambda row: row['SUM_CHECK_COUNT'] / row['SUM_PERSON_NUMBER'], axis=1)
    # 计算应检查值=地点作业人数*基准值
    # data['AVG_CHECK_COUNT'] = data.apply(
    #     lambda row: row['BASE_CHECK_COUNT'] * row['PERSON_COUNT'], axis=1)
    # 比较值= 该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)
    data['AVG_CHECK_COUNT'] = data.apply(
        lambda row: row['ADDRESS_PERSON_NUMBER'] * row['SUM_CHECK_COUNT']/row['SUM_PERSON_NUMBER'], axis=1)
    return data


def _export_calc_address_evenness_data(data, months_ago):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['CHECK_COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        person_number = row['ADDRESS_PERSON_NUMBER']
        point_name = row['ADDRESS_NAME']
        dpid = row['TYPE4']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        cnt['avg_check_count'] = avg_check_count
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if avg_check_count == 0:
            continue
        check_desc = (f'{point_name}(比较值：{int(avg_check_count)}, '
                      f'受检次数：{int(real_check_count)}, '
                      f'基准值：{base_check_count:.2f}, '
                      f'工作人数：{int(person_number)})')
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点数：': cnt.get('未检查地点数：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= 6:
                cnt.update({
                    '受检地点超过比较值600%以上地点数：':
                    cnt.get('受检地点超过比较值600%以上地点数：', '') + '<br/>' + check_desc
                })
            elif _ratio <= -0.4:
                cnt.update({
                    '受检地点低于比较值40%地点数：':
                    cnt.get('受检地点低于比较值40%地点数：', '') + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
            2,
            'MAIN_TYPE':
            MainType.check_evenness,
            'DETAIL_TYPE':
            CheckEvennessDetailType.stats_check_address_evenness,
            'MON':
            mon,
            'DEPARTMENT_ID':
            dpid,
            'HIERARCHY':
            HIERARCHY,
            'MAJOR':
            major_data[dpid],
            'AVG_QUOTIENT':
            cnt.pop('avg_check_count'),
            'CONTENT':
            ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(rst_data, months_ago, HIERARCHY, MainType.check_evenness,
                                     CheckEvennessDetailType.stats_check_address_evenness, index_type=INDEX_TYPE)


# 检查地点均衡度
def stats_check_address_evenness(check_point_count_sql, check_point_department_sql, check_banzu_count_sql,
                                 banzu_department_sql, department_data, choose_dpid_data, calc_func,
                                 months_ago, cache_client):
    """"
    v1
    根据地点作业的班组人数计算
    基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    地点工作人数=该地点工作的班组总人数
    应检查值=地点作业人数*基准值
    结果=（实际受检-应受检）/应受检"
    受检地点超过平均值600%以上的一处扣2分;
    受检地点低于基数40%的一处扣2分，
    未检查的一处扣5分。
    v2:
    每个地点受检次数超过比较值600%以上的一处扣2分;受检次数低于比较值50%的一处扣2分，未检查的一处扣5分。
    （每个地点的比较值=该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)，受检地点的工作量越大，比较值越高，本单位现场检查）
    检查地点中，每处检查点、重要检查点人数依据车间班组配置的该点作业人数为准，若配置的主要生产场所点作业人数为0或空，则取整个班组作业人数。
    若一个检查点的关联单位为车间，同样去主要生产场所中配置的改地点作业人数，若配置的作业人数为0或空，则取该车间的直属人数，即人员部门为车间单位的人数，
    """

    stats_month = get_custom_month(months_ago)
    # 重要检查点受检次数
    check_point_count = pd_query(check_point_count_sql)
    check_point_count = pd.merge(check_point_count, pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE4']),
                                 left_on='SOURCE_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    check_point_count.drop(['DEPARTMENT_ID'], axis=1, inplace=True)

    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)
    check_point = cache_client.get('check_point')
    check_point_checked_count = pd.merge(check_info_and_address, check_point, left_on='FK_CHECK_POINT_ID',
                                         right_on='PK_ID')
    check_point_checked_count = check_point_checked_count[(check_point_checked_count['ADDRESS_TYPE'] == 2)
                                                          & (check_point_checked_count['HIERARCHY'] == 2)
                                                          & (check_point_checked_count['IS_DELETE'] == 0)
                                                          & (check_point_checked_count['CHECK_WAY'].between(1, 2))]
    check_point_checked_count['CHECK_COUNT'] = 1
    check_point_checked_count = check_point_checked_count.groupby('FK_CHECK_POINT_ID',
                                                                  as_index=False)['CHECK_COUNT'].sum()

    data_check_point = pd.merge(
        check_point_count,
        check_point_checked_count,
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="FK_CHECK_POINT_ID")
    data_check_point.drop(["FK_CHECK_POINT_ID"], inplace=True, axis=1)
    check_point_department = pd_query(check_point_department_sql)
    data_check_point = pd.merge(
        data_check_point,
        check_point_department,
        how='inner',
        left_on='CHECK_POINT_ID',
        right_on='PK_ID')
    data_check_point.drop(["CHECK_POINT_ID", "PK_ID"], inplace=True, axis=1)

    # 班组受检次数
    check_banzu_count = pd_query(check_banzu_count_sql)

    banzu_department_checked_count = pd.merge(check_info_and_address, department_data,
                                              left_on='ADDRESS_FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    banzu_department_checked_count = banzu_department_checked_count[
        (banzu_department_checked_count['ADDRESS_TYPE'] == 1)
        & (banzu_department_checked_count['HIERARCHY'] >= 5)
        & (banzu_department_checked_count['CHECK_WAY'].between(1, 2))
        & (banzu_department_checked_count['TYPE'].between(9, 10))]
    banzu_department_checked_count = banzu_department_checked_count.groupby('TYPE5').size().to_frame(name='CHECK_COUNT')
    banzu_department_checked_count = banzu_department_checked_count.reset_index().rename(
        columns={'TYPE5': 'DEPARTMENT_ID'})
    data_check_banzu = pd.merge(
        check_banzu_count,
        banzu_department_checked_count,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    banzu_department = pd_query(banzu_department_sql)
    banzu_department = pd.merge(banzu_department,
                                pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE4']),
                                left_on='DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    data_check_banzu = pd.merge(
        data_check_banzu,
        banzu_department,
        how='inner',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(
        ["FK_DEPARTMENT_ID", "DEPARTMENT_ID"], inplace=True, axis=1)

    # 合并
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data.fillna(0, inplace=True)
    # data.drop(['NAME', 'TYPE'], inplace=True, axis=1)
    data = pd.merge(
        data,
        choose_dpid_data(HIERARCHY),
        how='inner',
        left_on='TYPE4',
        right_on='DEPARTMENT_ID')

    data.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data = _calc_avg_check_count_by_chejian(data)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: calc_func(row, 'CHECK_COUNT', 'AVG_CHECK_COUNT'),
        axis=1)
    rst_index_score = calc_child_index_type_sum(
        data, INDEX_TYPE, MainType.check_evenness, CheckEvennessDetailType.stats_check_address_evenness,
        months_ago, 'DEDUCT_SCORE', 'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100), choose_dpid_data)
    return rst_index_score
