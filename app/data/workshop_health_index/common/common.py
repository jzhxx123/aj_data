#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""

import pandas as pd
from flask import current_app

from app import mongo
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY
from app.data.workshop_health_index.utils import get_department_classify
from app.data.index.common import df_merge_with_dpid
from app.data.util import get_history_months, get_coll_prefix, get_mongodb_prefix, write_bulk_mongo


def append_major_column_to_df(dpid_data, data):
    """增加专业列与

    Arguments:
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        data {type} --

    Returns:
        DataFrame --
    """
    data = pd.merge(
        dpid_data, data, how='left', left_on='DEPARTMENT_ID', right_index=True)
    return data


def divide_two_col(row, numerator_col, denominator_col):
    if row[denominator_col] == 0:
        return 0
    return row[numerator_col] / row[denominator_col]


def calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, major):
    """按照专业分组排名，将排名、分数数据写入数据库

    Arguments:
        data {[type]} -- 带有分值的按单位分组选好的
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    """
    data.fillna(0, inplace=True)
    data['group_sort'] = data[column].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    data['classify_sort'] = data[column].groupby(data['CLASSIFY']).rank(
        ascending=0, method='first')
    score_rst = []
    mon = get_history_months(months_ago)[0]
    major_risk_type = 0 if index_type == 1 else index_type
    if not major:
        major = data.iloc[0]['MAJOR']
    cls_count = {}
    for index, row in data.iterrows():
        score_rst.append({
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'TYPE': major_risk_type,
            'SCORE': round(row[column], 2),
            'RANK': int(row['group_sort']),
            'CLS_RANK': int(row['classify_sort']),
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME'],
            'CLASSIFY': row.get('CLASSIFY', ''),
            'TYPE3': row.get('TYPE3', ''),
            'CLS_COUNT': cls_count.setdefault(row['CLASSIFY'],
                                              data[data['CLASSIFY'] == row['CLASSIFY']].index.size)
        })
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}detail_{coll_prefix}_index'
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type,
        'MAJOR': major
    }
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, score_rst)


def summizet_operation_set(data,
                           dpid_data,
                           column,
                           hierarchy,
                           index_type,
                           main_type,
                           detail_type,
                           months_ago,
                           major=None):
    """拿到业务逻辑数据计算后的一套公共操作（排名、子指数汇总、入库）

    Arguments:
        data {DataFrame} -- index为部门ID
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    Keyword Arguments:
    """
    data = append_major_column_to_df(dpid_data, data)
    calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, major)


def add_avg_number_by_classify(df, dpid_data, numerator_col, denominator_col, main_type, detail_type, months_ago,
                               internal_key=None):
    """
    基数取同类车间进行计算
    :param internal_key: 
    :param months_ago:
    :param detail_type:
    :param main_type:
    :param df:
    :param dpid_data:
    :param numerator_col:
    :param denominator_col:
    :return:
    """
    df = pd.merge(dpid_data, df, how='left', left_on='DEPARTMENT_ID', right_index=True)
    df.fillna(0, inplace=True)
    major = df.iloc[0]['MAJOR']
    # 获取过去的基础数据
    past_months = [get_history_months(i)[0] for i in range(months_ago - 1, months_ago - 3, -1)]
    condition = {'MAJOR': major, 'MON': {'$in': past_months}, 'MAIN_TYPE': main_type, 'DETAIL_TYPE': detail_type}
    past_basic_data = mongo.db['monthly_workshop_health_index_basic_data'].find(condition,
                                                                                {'_id': 0,
                                                                                 'BASIC_CURRENT': 1,
                                                                                 'MON': 1, 'CLASSIFY': 1})
    past_basic_data = pd.DataFrame(list(past_basic_data))

    for cls, df_cls in df.groupby('CLASSIFY'):
        df_cls = df_cls[df_cls[denominator_col] > 0]
        numerator = df_cls[numerator_col].sum()
        denominator = df_cls[denominator_col].sum()
        basic = numerator / denominator if denominator > 0 else 0
        df.loc[df['CLASSIFY'] == cls, 'BASIC_CURRENT'] = basic
        # 基数取三个月平均数
        stat_mon_count = 1
        if not past_basic_data.empty:
            for past_mon in past_months:
                record = past_basic_data[(past_basic_data['MON'] == past_mon) & (past_basic_data['CLASSIFY'] == cls)]
                if not record.empty:
                    record = record.iloc[0]
                    if isinstance(record['BASIC_CURRENT'], dict):
                        past_basic_current = record['BASIC_CURRENT'].get(internal_key)
                    else:
                        past_basic_current = record['BASIC_CURRENT']
                    if past_basic_current:
                        basic += past_basic_current
                        stat_mon_count += 1

        basic = basic / stat_mon_count
        df.loc[df['CLASSIFY'] == cls, 'AVG_NUMBER'] = basic

    df.fillna(0, inplace=True)
    return df


def calc_check_count_per_person(df_numerator,
                                df_denominator,
                                index_type,
                                main_type,
                                detail_type,
                                months_ago,
                                stats_field,
                                column,
                                calc_func,
                                dpid_func,
                                major=None,
                                hierarchy_list=(4,),
                                content_tpl=None):
    """
    Arguments:
        df_numerator {dataFrame} -- 检查次数
        df_denominator {dataFrame} -- 工作量
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3]})
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc.apply(lambda row: divide_two_col(row, 'numerator', 'denominator'), axis=1)
        column = f'{column}_{hierarchy}'
        # 需要跟基数（车间分类均值）作比较得出分数
        df_calc = add_avg_number_by_classify(df_calc, dpid_func(hierarchy), 'numerator', 'denominator',
                                             main_type, detail_type, months_ago)
        df_calc['SCORE'] = df_calc.apply(
            lambda row: calc_func(row, 'ratio', 'AVG_NUMBER'), axis=1)
        # 导出指数中间过程
        export_basic_data(df_calc, main_type, detail_type, hierarchy,
                          months_ago, 'numerator', 'denominator', 'ratio',
                          'AVG_NUMBER', 'SCORE', major, index_type=index_type, content_tpl=content_tpl)
        rst_data = pd.DataFrame(
            index=df_calc['DEPARTMENT_ID'],
            data=df_calc['SCORE'].values,
            columns=[column])

        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(rst_data, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, major)
        rst_index_score.append(rst_data[[column]])
    return rst_index_score


def add_avg_score_by_major(df, column, dpid_data=None):
    """给dataframe增加专业平均分-
    """
    if dpid_data is not None:
        df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_SCORE'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def export_basic_data(df_data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, col_quotient, col_avg,
                      col_score, major=None, index_type=INDEX_TYPE, content_tpl=None):
    """将计算中间过程导出，包含分子、分母、专业均数、得分、排名

    Arguments:
        df_data {DataFrame} -- 数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
        col_numerator {float} -- 计算过程分子
        col_denominator {float} -- 计算过程分母
        col_quotient {float} -- 计算过程商
        col_avg {float} -- 专业均数
        col_score {float} -- 各单位得分
    """
    # 增加平均分一列
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_score].groupby(df_data['MAJOR']).rank(
        ascending=0, method='first')
    df_data['classify_sort'] = df_data[col_score].groupby(df_data['CLASSIFY']).rank(
        ascending=0, method='first')
    data_rst = []
    cls_count = {}
    if not major:
        major = df_data.iloc[0]['MAJOR']
    mon = get_history_months(months_ago)[0]
    for idx, row in df_data.iterrows():
        content_type = 1
        content = None
        score = round(row.get(col_score, 0), 2)
        avg_quotient = round(row.get(col_avg, 0), 4)
        quotient = round(row.get(col_quotient, 0), 4)
        numerator = round(row.get(col_numerator, 0), 2)
        denominator = round(row.get(col_denominator, 0), 2)
        avg_score = round(row.get('AVG_SCORE', 0), 2)
        if content_tpl:
            content_type = 2
            content = content_tpl.format(score, int(row['classify_sort']), avg_quotient, quotient, numerator,
                                         denominator, avg_score)
        data_rst.append({
            'TYPE': content_type,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': numerator,
            'DENOMINATOR': denominator,
            'QUOTIENT': quotient,
            'AVG_QUOTIENT': avg_quotient,
            'AVG_SCORE': avg_score,
            'BASIC_CURRENT': round(row.get('BASIC_CURRENT', 0), 4),
            'SCORE': score,
            'CONTENT': content,
            'RANK': int(row['group_sort']),
            'CLS_RANK': int(row['classify_sort']),
            'CLASSIFY': row.get('CLASSIFY', ''),
            'CLS_COUNT': cls_count.setdefault(row['CLASSIFY'],
                                              df_data[df_data['CLASSIFY'] == row['CLASSIFY']].index.size),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, major, index_type=index_type)


def write_export_basic_data_to_mongo(data,
                                     months_ago,
                                     hierarchy,
                                     main_type,
                                     detail_type,
                                     major=None,
                                     index_type=INDEX_TYPE):
    """将中间过程写入mongo，写之前需要删除之前的对应月份历史数据，避免冗余
    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}{coll_prefix}_index_basic_data'
    if not major:
        major = data[0]['MAJOR']
    # 删除历史数据
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type,
        'MAJOR': major
    }
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


def merge_all_child_item(row, hierarchy, item_name, item_weight):
    """将指数的各个子项按照权重合并成一个总数

    Arguments:
        row {[type]} --
        hierarchy {int} -- 单位层级
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Returns:
        float -- 指数分数
    """
    score = []
    for item in item_name:
        item = f'{item}_{hierarchy}'
        if item in row:
            score.append(round(row[item], 2))
        else:
            score.append(0)
    rst_score = sum([round(i[0], 2) * i[1] for i in zip(item_weight, score)])
    rst_score = 0 if rst_score < 0 else round(rst_score, 2)
    return rst_score


def summizet_child_index(child_score,
                         dpid_func,
                         index_type,
                         main_type,
                         months_ago,
                         item_name,
                         item_weight,
                         hierarchy_list=(4,),
                         major=None):
    """将指数的各个子项按照权重合并成一个总数， 计算排名、入库

    Arguments:
        child_score {list[DataFrame]} -- 各个子项的集合
        dpid_func {func} -- 对应层次单位的函数
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        months_ago {int} -- 第前{-N}个月
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Keyword Arguments:
        hierarchy_list {list} -- 站段、车间、班组 (default: {[3]})
    """
    if len(child_score) == 0:
        return
    for hierarchy in hierarchy_list:
        h_child_score = [
            x for x in child_score if x.columns.values[0][-1] == str(hierarchy)
        ]
        if h_child_score == []:
            continue
        data = pd.concat(h_child_score, axis=1, sort=False)
        data.fillna(0, inplace=True)
        data['SCORE'] = data.apply(
            lambda row: merge_all_child_item(row, hierarchy, item_name, item_weight),
            axis=1)

        data = append_major_column_to_df(dpid_func(hierarchy), data)
        calc_and_rank_index(data, 'SCORE', hierarchy, index_type, main_type, 0,
                            months_ago, major)


def get_detail_avg(main_type, detail_type, months_ago, major):
    start_mon = get_history_months(months_ago - 3)[0]
    end_mon = get_history_months(months_ago - 1)[0]
    condition = {
        'DETAIL_TYPE': detail_type,
        'MAIN_TYPE': main_type,
        'MAJOR': major,
        'MON': {'$gte': start_mon, '$lte': end_mon}
    }
    rows = mongo.db[''].find(condition)
    df = pd.DataFrame(list(rows))
    avg = 0
    return avg


def add_avg_number_by_major(df, dpid_data, column):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def _export_basic_data_ratio_type(
        df_data, dpid_data, main_type, detail_type, hierarchy, months_ago,
        col_numerator, col_denominator, col_quotient, col_score, index_type=INDEX_TYPE):
    df_data = add_avg_number_by_major(df_data, dpid_data, col_quotient)
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
            ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in df_data.iterrows():
        data_rst.append({
            'TYPE': 1,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'MAJOR': row['MAJOR'],
            'MON': mon,
            'HIERARCHY': hierarchy,
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'NUMERATOR': round(row[col_numerator], 2),
            'DENOMINATOR': round(row[col_denominator], 2),
            'QUOTIENT': round(row[col_quotient], 4),
            'AVG_QUOTIENT': round(row['AVG_NUMBER'], 4),
            'AVG_SCORE': round(row['AVG_SCORE'], 2),
            'SCORE': round(row[col_score], 2),
            'RANK': int(row['group_sort']),
        })
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type,
                                     index_type=index_type)


def calc_child_index_type_divide(df_numerator,
                                 df_denominator,
                                 index_type,
                                 main_type,
                                 detail_type,
                                 months_ago,
                                 stats_field,
                                 column,
                                 calc_func,
                                 dpid_func,
                                 is_calc_score_base_major=True,
                                 hierarchy_list=(4,)):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc.apply(lambda row: divide_two_col(row, 'numerator', 'denominator'), axis=1)
        column = f'{column}_{hierarchy}'
        df_calc = add_avg_number_by_classify(df_calc, dpid_func(hierarchy), 'numerator', 'denominator', main_type,
                                             detail_type, months_ago)

        df_calc['SCORE'] = df_calc.apply(
            lambda row: calc_func(row, 'ratio', 'AVG_NUMBER'),
            axis=1)

        # 导出指数中间过程
        export_basic_data(df_calc, main_type, detail_type, hierarchy,
                          months_ago, 'numerator', 'denominator', 'ratio',
                          'AVG_NUMBER', 'SCORE', index_type=index_type)

        rst_data = pd.DataFrame(
            index=df_calc['DEPARTMENT_ID'],
            data=df_calc['SCORE'].values,
            columns=[column])

        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(rst_data, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago)
        rst_index_score.append(rst_data[[column]])
    return rst_index_score


def calc_extra_child_score_groupby_major(df,
                                         dpid_data,
                                         column,
                                         func_calc_score,
                                         weight=None,
                                         detail_type=None,
                                         base_column="AVG_NUMBER",
                                         major_ratio_dict={}):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, base_column, detail_type, major_ratio_dict),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def format_export_basic_data(data,
                             main_type,
                             detail_type,
                             hierarchy,
                             months_ago,
                             index_type=INDEX_TYPE):
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in data.iterrows():
        basic_current = row.get('BASIC_CURRENT', 0)
        if not isinstance(basic_current, dict):
            basic_current = round(basic_current, 4)
        avg_quotient = row.get('AVG_NUMBER', 0)
        if not isinstance(avg_quotient, dict):
            avg_quotient = round(avg_quotient, 4)
        data_rst.append({
            'TYPE': 2,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'CONTENT': row['CONTENT'],
            'AVG_QUOTIENT': avg_quotient,
            'SCORE': row.get('SCORE', ''),
            'CLASSIFY': row.get('CLASSIFY', ''),
            'BASIC_CURRENT': basic_current
        })
    return data_rst


def export_basic_data_one_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        func_html_desc,
                                        title=None,
                                        index_type=INDEX_TYPE):
    if len(data) == 0:
        return
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    data['MONTH'] = data['MONTH'].apply(lambda x: func_html_desc(x))
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby([f'TYPE{HIERARCHY}', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    if title is not None:
        title += '<br/>'
    else:
        title = ''
    data['CONTENT'] = data.apply(
        lambda row: title + '<br/>'
        .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, index_type=index_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, index_type=index_type)


def calc_child_index_type_sum(data,
                              index_type,
                              main_type,
                              detail_type,
                              months_ago,
                              stats_field,
                              column,
                              calc_func,
                              dpid_func,
                              hierarchy_list=(4,),
                              NA_value=True):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        data {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3]})
        NA_value {bool}
            -- 没有统计到的单位,如果是计算扣分型数，则为0，(default: {Flase})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    rst_index_score = []
    for hierarchy in hierarchy_list:
        xdata = data.dropna(subset=[f'TYPE{hierarchy}'], axis=0)
        xdata = xdata.groupby([f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = xdata.to_frame(name='SCORE')
        # 给没有统计到的单位添加指定缺省值
        if NA_value:
            dpid_data = dpid_func(hierarchy)
            df_calc = pd.merge(
                df_calc,
                dpid_data,
                how='outer',
                left_index=True,
                right_on='DEPARTMENT_ID')
            df_calc.fillna(0, inplace=True)
            df_calc = pd.DataFrame(
                index=df_calc['DEPARTMENT_ID'],
                data=df_calc.loc[:, 'SCORE'].values,
                columns=['SCORE'])
        if df_calc.empty is True:
            continue
        column = f'{column}_{hierarchy}'
        df_calc[column] = df_calc['SCORE'].apply(lambda x: calc_func(x))
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def export_basic_data_dicttype(data,
                               dpid_data,
                               main_type,
                               detail_type,
                               hierarchy,
                               months_ago,
                               func_html_desc):

    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    data = append_major_column_to_df(dpid_data, data.to_frame(name='CONTENT'))
    data.fillna(0, inplace=True)
    data['CONTENT'] = data['CONTENT'].apply(lambda x: func_html_desc(x))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


def _combine_second_filed_to_str(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        a = idx[1]
        b = int(row.loc[idx])
        val = f'{second_title[str(int(float(idx[1])))]}{int(row.loc[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


def export_basic_data_tow_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        first_title,
                                        second_title):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby([f'TYPE{hierarchy}', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()
    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str(row, first_title, second_title),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type)


# 整改成效用--row.loc[idx] 改为 int(row[idx]
def _combine_second_filed_to_str_two(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串,
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


# 整改成效
def export_basic_data_tow_field_monthly_two(data,
                                            dpid_data,
                                            major_data,
                                            main_type,
                                            detail_type,
                                            hierarchy,
                                            months_ago,
                                            first_title,
                                            second_title,
                                            columns_list =[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                                                            (2, 3), (3, 1), (3, 2), (3, 3)]):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby([f'TYPE{hierarchy}', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()

    # 按顺序填充缺失的责任类型
    # columns_list = [(1, 1), (1, 2), (1, 3), (2, 1), (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)]
    data_columns = data.columns.values.tolist()
    for idx, col in enumerate(columns_list):
        if col not in data_columns:
            data[col] = 0
            col_data = data.pop(col)
            data.insert(idx, col, col_data)

    # 填充车间数据
    data = pd.merge(
        data,
        major_data["DEPARTMENT_ID"].to_frame(),
        how="right",
        left_index=True,
        right_on="DEPARTMENT_ID")
    data.set_index('DEPARTMENT_ID', inplace=True)

    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str_two(row, first_title, second_title),
        axis=1)

    data = data['CONTENT'].to_frame("middle_1")
    #
    # data = append_major_column_to_df(
    #     major_data,
    #     pd.DataFrame(
    #         index=data.index,
    #         data=data.loc[:, 'CONTENT'].values,
    #         columns=['CONTENT']))
    # data_rst = format_export_basic_data(data, main_type, detail_type,
    #                                     hierarchy, months_ago, risk_type)
    # write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
    #                                  main_type, detail_type, risk_type)
    return data


def combine_and_format_basic_data_to_mongo(data,
                                           major_data,
                                           months_ago,
                                           hierarchy,
                                           main_type,
                                           detail_type,
                                           index_type=INDEX_TYPE):
    """将中间过程各个子指数的组成部分数据拼接好

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    calc_df_data = pd.concat(data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join(['%s' % row[col] for col in columns if pd.notna(row[col])]), axis=1)
    calc_df_data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, main_type, detail_type, hierarchy, months_ago, index_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago,
                                     hierarchy, main_type, detail_type,
                                     index_type=index_type)
