#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/17
Description: 
"""
import calendar

import pandas as pd

from app.data.workshop_health_index.common.common import (
    append_major_column_to_df, calc_child_index_type_divide, calc_check_count_per_person, summizet_child_index,
    summizet_operation_set, write_export_basic_data_to_mongo, export_basic_data_tow_field_monthly_two,
    calc_child_index_type_sum, export_basic_data, format_export_basic_data)
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, SelfControlAbilityDetailType
from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.workshop_health_index.common.self_control_ability_sql import (
    ASSESS_PROBLEM_AND_CHECK_INFO_SQL, ASSESS_PROBLEM_AND_RESPONSIBLE_SQL)
from app.data.workshop_health_index.common.check_eveness import (add_avg_number_by_classify, handle_cross_hour,
                                                                 calc_banzu_count_by_hour)


# 换算车间自查频次
def stats_self_check_per_person(check_count, work_load, months_ago, calc_func, choose_dpid_data):
    return calc_check_count_per_person(
        check_count, work_load, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_self_check_per_person, months_ago,
        'COUNT', 'SCORE_a', calc_func, choose_dpid_data)


# 人均质量分
def stats_score_per_person(problem_df, work_load, months_ago, clac_func, choose_dpid_data,
                           cache_client):
    stats_month = get_custom_month(months_ago)
    # 基础问题库
    problem_base_df = cache_client.get('problem_base')

    self_check_problem_df = problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']]
    # 连接基础问题库
    self_check_problem_df = pd.merge(self_check_problem_df, problem_base_df, how='left', left_on='FK_PROBLEM_BASE_ID',
                                     right_on='PK_ID')
    self_check_problem_df.fillna(0, inplace=True)
    self_check_problem_df.rename(columns={'CHECK_SCORE': 'COUNT', 'TYPE4_CHECK': 'TYPE4'}, inplace=True)
    return calc_child_index_type_divide(
        self_check_problem_df, work_load, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_score_per_person, months_ago, 'COUNT', 'SCORE_b',
        clac_func, choose_dpid_data)


# 较严重及以上风险问题
def stats_serious_problem_ratio(problem_df, work_load, months_ago, clac_func, choose_dpid_data):
    """c
    计算较大风险问题占比（总人数）
    """
    self_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']],
                                      columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP'])
    self_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    self_check_problem = self_check_problem[self_check_problem['RISK_LEVEL'] <= 2]
    self_check_problem['COUNT'] = 1
    return calc_child_index_type_divide(
        self_check_problem, work_load, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_serious_problem_ratio, months_ago, 'COUNT',
        'SCORE_c', clac_func, choose_dpid_data)


def apply_to_calc_with_bonus(row, self_check_problem):
    """
    他查问题扣分，计算发现问题加分
    :param row:
    :param self_check_problem:
    :return:
    """
    bonus_problem = self_check_problem[self_check_problem['TYPE4'] == row['DEPARTMENT_ID']]
    count_red_line = bonus_problem[bonus_problem['IS_RED_LINE'].isin([1, 2])].index.size
    count_other = bonus_problem[~(bonus_problem['IS_RED_LINE'].isin([1, 2]))
                                & (bonus_problem['LEVEL'].isin(['A', 'F1']))].index.size
    bonus = count_red_line * 10 + count_other * 2
    bonus = min(bonus, 30)
    content = '<br/>发现高质量问题<br/>红线: {}个，A、F1: {}个'.format(count_red_line, count_other)
    return pd.Series([bonus, content], index=['bonus', 'middle_3'])


def stats_other_problem_exposure(safety_produce_info_sql, problem_df, problem_base_df, department_data, months_ago,
                                 choose_dpid_data, problem_risk_score=None, calc_bonus=False):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    rst_index_score = []
    calc_month = get_custom_month(months_ago)
    if not problem_risk_score:
        problem_risk_score = {
            1: 4,
            2: 2,
            3: 1,
        }

    safety_produce_info = pd_query(safety_produce_info_sql.format(*calc_month))
    safety_produce_info = pd.merge(safety_produce_info,
                                   pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE4']),
                                   left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    self_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']],
                                      columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP', 'LEVEL',
                                               'IS_RED_LINE'])
    self_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    other_check_problem = pd.DataFrame(problem_df[problem_df['TYPE4_CHECK'] != problem_df['TYPE4_RESP']],
                                       columns=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4_RESP'])
    other_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in choose_dpid_data(HIERARCHY).loc[:, 'DEPARTMENT_ID'].values
    }
    if not problem_risk_score:
        problem_risk_score = {
            1: 4,
            2: 2,
            3: 1,
        }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}

    # 未自查出他查问题（检查问题）
    # other_not_self_problem = other_check_problem.difference(self_check_problem)
    other_not_self_problem = other_check_problem.copy()
    other_not_self_problem = other_not_self_problem.append(self_check_problem, sort=False)
    other_not_self_problem = other_not_self_problem.append(self_check_problem, sort=False)
    other_not_self_problem = other_not_self_problem.drop_duplicates(
        subset=['FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE4'], keep=False)
    # 过滤属于自身基础问题库的问题
    # other_not_self_problem = pd.merge(other_not_self_problem,
    #                                   pd.DataFrame(problem_base_df, columns=['PK_ID', 'FK_DEPARTMENT_ID']),
    #                                   left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')
    # other_not_self_problem = pd.merge(other_not_self_problem,
    #                                   pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE3']),
    #                                   left_on='TYPE4', right_on='DEPARTMENT_ID')
    # other_not_self_problem = other_not_self_problem[
    #     other_not_self_problem['TYPE3'] == other_not_self_problem['FK_DEPARTMENT_ID']]

    for idx, row in other_not_self_problem.iterrows():
        problem_dpid = row['TYPE4']
        risk_level = int(row['RISK_LEVEL'])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby([f'TYPE{HIERARCHY}', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'.join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        choose_dpid_data(HIERARCHY),
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        problem_dpid = row['TYPE4']
        risk_level = row['RISK_LEVEL']
        problem_base_id = row['FK_PROBLEM_BASE_ID']
        main_type = row['MAIN_TYPE']
        if not self_check_problem[(self_check_problem['FK_PROBLEM_BASE_ID'] == problem_base_id)
                                  & (self_check_problem['TYPE4'] == problem_dpid)
                                  & (self_check_problem['RISK_LEVEL'] == risk_level)].empty:
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程
    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data,
                                                          choose_dpid_data(HIERARCHY),
                                                          MainType.self_control_ability,
                                                          SelfControlAbilityDetailType.other_problem_exposure,
                                                          HIERARCHY, months_ago, first_title, second_title,
                                                          columns_list=[(1, 1), (1, 2), (2, 1), (2, 2), (3, 1), (3, 2)])
    calc_df_data = pd.merge(
        other_problem_data,
        safety_data,
        right_index=True,
        left_on="DEPARTMENT_ID",
        how="left"
    )
    # 如果计算发现问题加分
    if calc_bonus:
        extra_df = calc_df_data.apply(lambda r: apply_to_calc_with_bonus(r, self_check_problem),
                                      axis=1, result_type='expand')
        calc_df_data = calc_df_data.join(extra_df)
    else:
        calc_df_data['middle_3'] = ''
        calc_df_data['bonus'] = 0
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data["CONTENT"] = calc_df_data.apply(
        lambda row: row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"] + row["middle_3"], axis=1)
    bonus_score = pd.DataFrame(calc_df_data, index=calc_df_data.index, columns=['bonus']).to_dict()
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(HIERARCHY),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, MainType.self_control_ability,
                                                   SelfControlAbilityDetailType.other_problem_exposure,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.self_control_ability,
                                     SelfControlAbilityDetailType.other_problem_exposure,
                                     index_type=INDEX_TYPE)
    df_calc = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    # df_calc = df_merge_with_dpid(df_calc, department_data)

    column = f'SCORE_d_{HIERARCHY}'

    def _calc_score(r):
        bonus = bonus_score.get(r['FK_DEPARTMENT_ID'], 0)
        score = 100 + (bonus - min(r['SCORE'], 30))
        return min(score, 100)

    df_calc[column] = df_calc.apply(lambda r: _calc_score(r), axis=1)
    df_calc.set_index('FK_DEPARTMENT_ID', inplace=True)
    summizet_operation_set(df_calc, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.self_control_ability,
                           SelfControlAbilityDetailType.other_problem_exposure,
                           months_ago)
    rst_index_score.append(df_calc[[column]])

    return rst_index_score


def _calc_department_evaluate_score(row):
    """
    100-被评价记分总和（干部乘以职务系数，非干部不乘系数）/职工人数*100*4
    :param row:
    :return:
    """
    deduct = round((row['EVALUATE_SCORE'] / row['NUMBER']) * 100 * 4, 2)
    score = max(0, round(100 - deduct, 2))
    content = '得分({0})=100-被评价记分总和({1})（干部乘以职务系数，非干部不乘系数）/职工人数({2})*100*4'.format(
        score, round(row['EVALUATE_SCORE'], 2), row['NUMBER'])
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def stats_evaluate_intensity(evaluate_score_sql, work_load, department_data, months_ago, choose_dpid_data,
                             calc_evaluate_score_with_ratio,
                             calc_department_evaluate_score=_calc_department_evaluate_score):
    rst_index_score = []
    stats_month = get_custom_month(months_ago)
    evaluate_score_df = df_merge_with_dpid(pd_query(evaluate_score_sql.format(*stats_month)), department_data)
    evaluate_score_df['EVALUATE_SCORE'] = evaluate_score_df.apply(calc_evaluate_score_with_ratio, axis=1)
    evaluate_score_df.drop(columns=['SCORE'])
    evaluate_score_df = evaluate_score_df.groupby(f'TYPE{HIERARCHY}')['EVALUATE_SCORE'].sum()
    evaluate_score_df = evaluate_score_df.to_frame(name='EVALUATE_SCORE')
    work_load = work_load.groupby(f'TYPE{HIERARCHY}')['COUNT'].sum()
    df_calc = pd.concat(
        [
            evaluate_score_df,
            work_load.to_frame(name='NUMBER')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['NUMBER'], inplace=True)
    df_calc.fillna(0, inplace=True)
    df_calc = pd.merge(choose_dpid_data(HIERARCHY), df_calc, how='left', left_on='DEPARTMENT_ID', right_index=True)
    rst_df = df_calc.apply(calc_department_evaluate_score, axis=1, result_type='expand')
    df_calc = df_calc.join(rst_df)

    # 导出指数中间过程
    calc_basic_data_rst = format_export_basic_data(df_calc, MainType.self_control_ability,
                                                   SelfControlAbilityDetailType.stats_evaluate_intensity,
                                                   HIERARCHY, months_ago, index_type=INDEX_TYPE)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.self_control_ability,
                                     SelfControlAbilityDetailType.stats_evaluate_intensity,
                                     index_type=INDEX_TYPE)
    column = f'SCORE_e_{HIERARCHY}'
    rst_data = pd.DataFrame(
        index=df_calc['DEPARTMENT_ID'],
        data=df_calc['SCORE'].values,
        columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.self_control_ability,
                           SelfControlAbilityDetailType.stats_evaluate_intensity,
                           months_ago, None)
    rst_index_score.append(rst_data[[column]])
    return rst_index_score


def stats_check_problem_assess_radio(problem_df, staff_number, months_ago, calc_func,
                                     choose_dpid_data):
    # 问题数据
    self_check_problem = problem_df[problem_df['TYPE4_CHECK'] == problem_df['TYPE4_RESP']].copy()
    self_check_problem.rename(columns={'TYPE4_RESP': 'TYPE4'}, inplace=True)
    self_check_problem['COUNT'] = 1
    return calc_check_count_per_person(
        self_check_problem, staff_number, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_assess_problem_ratio, months_ago, 'COUNT',
        'SCORE_f', calc_func, choose_dpid_data)


def stats_self_check_yecha_ratio(yecha_count, xianchang_check_count, months_ago, calc_func, choose_dpid_data):
    return calc_check_count_per_person(
        yecha_count, xianchang_check_count, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_yecha_ratio, months_ago,
        'COUNT', 'SCORE_g', calc_func, choose_dpid_data)


# 覆盖率(现场检查)
def stats_check_address_ratio(department_data, months_ago, calc_func, choose_dpid_data,
                              check_point_sql, banzu_point_sql, cache_client):
    stats_month = get_custom_month(months_ago)
    # 检查地点数
    check_info_and_address = cache_client.get('check_info_and_address', months_ago=months_ago)
    # 过滤现场现场的记录
    check_info_and_address = check_info_and_address[(check_info_and_address['CHECK_WAY'].between(1, 2))]
    check_point = cache_client.get('check_point')
    real_check_point = pd.merge(check_info_and_address, check_point, left_on='FK_CHECK_POINT_ID', right_on='PK_ID')
    real_check_point = real_check_point[(real_check_point['HIERARCHY'] == 2)
                                        & (real_check_point['IS_DELETE'] == 0)
                                        & (real_check_point['ADDRESS_TYPE'] == 2)]
    real_check_point.drop_duplicates(subset=['FK_CHECK_POINT_ID'], keep='first', inplace=True)
    real_check_point = real_check_point.groupby('FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_point = real_check_point.reset_index().rename(columns={'FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})

    real_check_banzu = pd.merge(check_info_and_address, department_data, left_on='ADDRESS_FK_DEPARTMENT_ID',
                                right_on='DEPARTMENT_ID')
    real_check_banzu = real_check_banzu[(real_check_banzu['ADDRESS_TYPE'] == 1)
                                        & (real_check_banzu['TYPE'] == 9)]
    real_check_banzu = real_check_banzu.groupby('ADDRESS_FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_banzu = real_check_banzu.reset_index().rename(columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})
    real_check_banzu['COUNT'] = 1
    data_real = pd.concat(
        [real_check_point, real_check_banzu],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, department_data)

    # 地点总数
    check_point = pd_query(check_point_sql)
    check_banzu = pd_query(banzu_point_sql)
    data_total = pd.concat(
        [check_point, check_banzu],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, department_data)
    return calc_child_index_type_divide(
        data_real,
        data_total,
        INDEX_TYPE,
        MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_check_address_ratio,
        months_ago,
        'COUNT',
        'SCORE_h',
        calc_func,
        choose_dpid_data,
        is_calc_score_base_major=False)


# 中高质量问题占比
def stats_medium_quality_above_ratio(medium_above_problem, problem_df, months_ago, calc_func, choose_dpid_data):
    return calc_child_index_type_divide(
        medium_above_problem, problem_df, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_medium_quality_above_ratio, months_ago, 'COUNT', 'SCORE_i',
        calc_func, choose_dpid_data)


# 自查换算单位考核金额
def stats_assess_money_ratio(department_data, staff_number, months_ago, calc_func, choose_dpid_data):
    """
    从月考核表问题明细找出关联问题是自查问题的记录
    :param department_data:
    :param staff_number:
    :param months_ago:
    :param calc_func:
    :param choose_dpid_data:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 考核问题关联检查信息
    assess_problem_and_check_info = pd.merge(
        pd_query(ASSESS_PROBLEM_AND_CHECK_INFO_SQL.format(year, month)), department_data,
        left_on='FK_DEPARTMENT_ID_CHECK', right_on='DEPARTMENT_ID')
    assess_problem_and_check_info = pd.DataFrame(
        assess_problem_and_check_info, columns=['ACTUAL_MONEY', 'FK_DEPARTMENT_ID', 'IS_EXTERNAL',
                                                'FK_CHECK_PROBLEM_ID', 'TYPE4'])
    assess_problem_and_check_info.rename(columns={'TYPE4': 'TYPE4_CHECK', 'ACTUAL_MONEY': 'COUNT'}, inplace=True)
    # 考核问题关联责任部门
    assess_problem_and_responsible = pd.merge(
        pd_query(ASSESS_PROBLEM_AND_RESPONSIBLE_SQL.format(year, month)), department_data,
        left_on='FK_DEPARTMENT_ID_RESPONSIBLE', right_on='DEPARTMENT_ID')
    assess_problem_and_responsible = pd.DataFrame(
        assess_problem_and_responsible, columns=['FK_CHECK_PROBLEM_ID', 'TYPE4'])
    assess_problem_and_responsible.rename(columns={'TYPE4': 'TYPE4_RESPONSIBLE'}, inplace=True)
    # 过滤自查问题
    self_assess = pd.merge(assess_problem_and_check_info, assess_problem_and_responsible, on='FK_CHECK_PROBLEM_ID')
    self_assess = self_assess[(self_assess['TYPE4_RESPONSIBLE'] == self_assess['TYPE4_CHECK'])
                              & (self_assess['IS_EXTERNAL'] == 0)]
    self_assess = df_merge_with_dpid(self_assess, department_data)
    return calc_child_index_type_divide(
        self_assess, staff_number, INDEX_TYPE, MainType.self_control_ability,
        SelfControlAbilityDetailType.stats_assess_money_ratio, months_ago, 'COUNT', 'SCORE_k',
        calc_func, choose_dpid_data)


def _cal_deduct_score_by_hour(row, columns):
    """计算逻辑：
    基准值（同类车间）=Σ时段检查次数/Σ时段作业班组数
    应检查值=该时段作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣2分/日，低于50%的扣4分/日，
    低于100%的扣8分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/时段和次数/时段]]
        columns {[list]} -- [所有日期列]]

    当站段为桥路大修段时，因为不存在班组，默认给检查班组30
    """
    # 计算基准值:
    avg_check_count = row['AVG_NUMBER']

    # 保存中间计算过程
    unfinished_hour = {
        '检查数低于比较值20%的时段': [],
        '检查数低于比较值50%的时段': [],
        '检查数低于比较值100%的时段': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for hour in columns:
        hour_banzu_count = row[f'banzu_count_{hour}']  # 该时段班组数
        hour_check_count = row[hour]  # 该时段检查数
        base_check_count = hour_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (hour_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            hour_deduction = 0
        else:
            if ratio <= -1:
                hour_deduction = -3
                deduct_type = '检查数低于比较值100%的时段'
            elif ratio <= -0.5:
                hour_deduction = -4
                deduct_type = '检查数低于比较值50%的时段'
            elif ratio <= -0.2:
                hour_deduction = -2
                deduct_type = '检查数低于比较值20%的时段'
            deduct_hour = unfinished_hour.get(deduct_type)
            hour_title = f'{(hour - 1) * 8} - {hour * 8}时'
            deduct_hour.append(f'{hour_title}[基准值：{avg_check_count:.1f},' +
                               f'班组数：{hour_banzu_count:.1f}，' +
                               f'比较值：{base_check_count:.1f}' +
                               f'实际检查值：{hour_check_count:.1f}]')
            unfinished_hour.update({deduct_type: deduct_hour})
        score.append(hour_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_hour.items()])
    return total_score, rst_calc_data


def combine_hours(data, columns):
    data_copy = data.copy()
    data.drop(columns, axis=1, inplace=True)
    data.drop([f'banzu_count_{x}' for x in columns], axis=1, inplace=True)
    new_columns = (1, 2, 3)
    for paragraph in new_columns:
        right_b = 4 * paragraph + 1
        data[f'banzu_count_{paragraph}'] = sum([data_copy[f'banzu_count_{x}'] for x in range(right_b - 4, right_b)])
        data[paragraph] = sum([data_copy[x] for x in range(right_b - 4, right_b)])
    return data, new_columns


# 检查时间段均衡度
def stats_check_hour_evenness(daily_check_banzu_count, department_data, months_ago, choose_dpid_data,
                              cache_client):
    main_type = MainType.self_control_ability
    detail_type = SelfControlAbilityDetailType.stats_check_hour_evenness
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [HIERARCHY]:
        banzu_count = calc_banzu_count_by_hour(
            daily_check_banzu_count)
        data = df_merge_with_dpid(banzu_count, department_data)
        if data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])['COUNT'].sum()
        xdata = xdata.unstack()
        xdata = xdata.fillna(0)
        # 重命名列名
        xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        check_info_df = cache_client.get('check_info_and_person_with_check_time', months_ago=months_ago)
        check_info_df = df_merge_with_dpid(check_info_df,
                                           pd.DataFrame(department_data, columns=['DEPARTMENT_ID', 'TYPE4']))
        check_info_df.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
        chejian_dpid_list = tuple(choose_dpid_data(hierarchy)['DEPARTMENT_ID'].values.tolist())
        hour_check_count = check_info_df[(check_info_df['CHECK_WAY'].between(1, 2)
                                           & (check_info_df['TYPE4'].isin(chejian_dpid_list)))].copy()
        hour_check_count['START_HOUR'] = hour_check_count['START_CHECK_TIME'].apply(
            lambda value: value.strftime('%Y-%m-%d %H'))
        hour_check_count['END_HOUR'] = hour_check_count['END_CHECK_TIME'].apply(
            lambda value: value.strftime('%Y-%m-%d %H'))
        hour_check_count['COUNT'] = 1
        hour_check_count = hour_check_count.groupby(['FK_DEPARTMENT_ID', 'START_HOUR', 'END_HOUR'],
                                                    as_index=False)['COUNT'].sum()

        data_check = df_merge_with_dpid(
            handle_cross_hour(
                hour_check_count, year,
                month), department_data)
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'WORK_HOUR'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)
        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)

        column = f'SCORE_j_{hierarchy}'
        xdata = add_avg_number_by_classify(xdata, choose_dpid_data(hierarchy), columns,
                                           main_type, detail_type, months_ago)
        # 8小时为一个时间段，合并时间
        xdata, columns = combine_hours(xdata, columns)

        xdata[column] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[0], axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[1], axis=1)

        xdata['SCORE'] = xdata[column]
        calc_basic_data_rst = format_export_basic_data(xdata, main_type, detail_type, HIERARCHY, months_ago,
                                                       index_type=INDEX_TYPE)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, main_type, detail_type,
                                         index_type=INDEX_TYPE)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column, HIERARCHY, INDEX_TYPE, main_type,
                               detail_type, months_ago)
        rst_index_score.append(xdata)
    return rst_index_score
