#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/17
Description: 
"""
import pandas as pd

from app import mongo
from app.data.index.common import df_merge_with_dpid
from app.data.index.util import get_year_month, get_custom_month
from app.data.util import pd_query, get_coll_prefix, get_mongodb_prefix, get_history_months
from app.data.workshop_health_index.common.common import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_extra_child_score_groupby_major, calc_child_index_type_sum,
    summizet_child_index, summizet_operation_set, export_basic_data_tow_field_monthly,
    write_export_basic_data_to_mongo, export_basic_data_dicttype,
    export_basic_data_tow_field_monthly_two, format_export_basic_data)
from app.data.workshop_health_index.const import INDEX_TYPE, HIERARCHY, MainType, ProblemRectificationDetailType


# 整改时效
def stats_rectification_overdue(overdue_problem_data, department_data, months_ago, choose_dpid_data,
                                calc_func):
    # 导出中间过程数据
    export_basic_data_dicttype(overdue_problem_data, choose_dpid_data(HIERARCHY), MainType.problem_rectification,
                               ProblemRectificationDetailType.stats_rectification_overdue, HIERARCHY,
                               months_ago, lambda x: f'本月问题超期发生条数：{int(x)}条')
    rst_index_score = calc_child_index_type_sum(
        overdue_problem_data,
        INDEX_TYPE,
        MainType.problem_rectification,
        ProblemRectificationDetailType.stats_rectification_overdue,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_func,
        choose_dpid_data,
        hierarchy_list=[HIERARCHY],
        NA_value=True,)
    return rst_index_score


# 整改履责指数（扣分制）
def stats_check_evaluate(check_evaluate_sz_number_sql, department_data, months_ago, choose_dpid_data,
                         calc_check_evaluate_score, code_dict=None):
    calc_month = get_custom_month(months_ago)
    check_evaluate_sz_number = df_merge_with_dpid(
        pd_query(check_evaluate_sz_number_sql.format(*calc_month)),
        department_data)

    # 获取每个站段各个code的数量
    # code_list = ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-5"]
    if not code_dict:
        code_dict = {"ZG-1": 50,
                     "ZG-2": 50,
                     "ZG-3": 60,
                     "ZG-4": 40,
                     "ZG-5": 20}
    data = choose_dpid_data(HIERARCHY)
    for code in code_dict:
        code_data = check_evaluate_sz_number[check_evaluate_sz_number["CODE"] == code].copy()
        code_data['DEDUCT'] = code_data['SCORE_STANDARD'] * 2
        code_data = code_data.groupby([f'TYPE{HIERARCHY}'])['COUNT', 'DEDUCT'].sum().reset_index()
        code_data.rename(columns={"COUNT": code, 'DEDUCT': f'{code}_DEDUCT'}, inplace=True)
        data = pd.merge(
            data,
            code_data.loc[:, [code, f'{code}_DEDUCT', f'TYPE{HIERARCHY}']],
            how="left",
            left_on="DEPARTMENT_ID",
            right_on=f'TYPE{HIERARCHY}')
        data.drop([f'TYPE{HIERARCHY}'], inplace=True, axis=1)
    data[f'TYPE{HIERARCHY}'] = data["DEPARTMENT_ID"]
    data.fillna(0, inplace=True)

    data['CONTENT'] = data.apply(
        lambda row: '<br/>'.join([f'本月{col}履职评价发生条数：{int(row[col])}条' for col in code_dict]), axis=1
    )

    data['SCORE'] = data.apply(lambda row: calc_check_evaluate_score(row, code_dict), axis=1)
    # 中间过程
    data_rst = format_export_basic_data(data, MainType.problem_rectification,
                                        ProblemRectificationDetailType.stats_check_evaluate, HIERARCHY, months_ago,
                                        index_type=INDEX_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, HIERARCHY,
                                     MainType.problem_rectification,
                                     ProblemRectificationDetailType.stats_check_evaluate,
                                     index_type=INDEX_TYPE)
    # 最终结果
    rst_index_score = calc_child_index_type_sum(
        data,
        INDEX_TYPE,
        MainType.problem_rectification,
        ProblemRectificationDetailType.stats_check_evaluate,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        choose_dpid_data,
        NA_value=True)
    return rst_index_score


def _get_appoint_month_happen_problem(happen_problem_point_sql, department_data, risk_level_threshold,
                                      months_ago, hierarchy, cache_client):
    """获取前第{-months_ago}月的发生问题项点数

    Arguments:
        months_ago {int} -- [description]
    Returns:
        set -- 反复问题
    """
    problem_base = cache_client.get('problem_base')
    i_month_data = cache_client.get('check_problem_and_responsible', months_ago=months_ago)
    if i_month_data is None:
        i_month_data = pd_query(happen_problem_point_sql.format(*get_custom_month(months_ago)))
    i_month_data = pd.DataFrame(i_month_data, columns=['FK_DEPARTMENT_ID', 'FK_PROBLEM_BASE_ID', 'IS_EXTERNAL'])
    i_month_data = df_merge_with_dpid(i_month_data, department_data)
    i_month_data = pd.merge(i_month_data, problem_base, left_on='FK_PROBLEM_BASE_ID', right_on='PK_ID')
    i_month_data = i_month_data[i_month_data['RISK_LEVEL'].isin(list(risk_level_threshold.keys()))]
    # 路外设备质量考核金额为0的不统计
    # i_month_data['ASSESS_MONEY'].fillna(0, inplace=True)
    # i_month_data['ASSESS_MONEY'] = i_month_data['ASSESS_MONEY'].astype('int')
    i_month_data = i_month_data[(i_month_data['IS_EXTERNAL'] == 0) & (i_month_data['ASSESS_MONEY'] > 0)]
    i_month_data['COUNT'] = 1
    i_month_data = i_month_data.groupby(
        [f'TYPE{hierarchy}', 'PK_ID', 'RISK_LEVEL'], as_index=False)['COUNT'].sum()
    return i_month_data


def _repeatedly_index_apply(row, rst_dict, risk_level_deduct):
    first_title = {0: '当前月', -1: '前一个月', -2: '前二个月', -3: '前三个月'}
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    department_dict = rst_dict.get(row['DEPARTMENT_ID'], {})
    score = department_dict.get('deduct', 0)
    score = 0 if (100 - score) < 0 else round((100 - score), 2)
    content = ''
    for month, levels in risk_level_deduct.items():
        content += '{}: '.format(first_title.get(month))
        temp_content = []
        for level in levels.keys():
            temp_content.append('{}{}个'.format(second_title.get(level), department_dict.get(month, {}).get(level, 0)))
        content += '，'.join(temp_content)
        content += '<br/>'
    return pd.Series([score, content], index=['SCORE', 'CONTENT'])


def _calc_ratio_for_repeatedly_index(workshop_df, months_ago):
    """
    计算问题控制中的各种系数
    :return:
    """
    # 单位大小系数=单位总人数/（专业各单位总人数/单位数）
    all_divide = workshop_df['COUNT'].sum() / workshop_df.index.size
    # 单位大小系数 = 单位总人数/（专业各单位总人数/单位数）
    workshop_df['unit_ratio'] = workshop_df['COUNT'] / all_divide

    # 问题暴露度系数=1+（单位问题暴露度指数-专业平均数）/专业平均数； (专业平均数大于60 的一律按60 计算）
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(INDEX_TYPE)
    coll_name = f'{prefix}detail_{coll_prefix}_index'
    stat_month = get_history_months(months_ago)[0]
    major = workshop_df.iloc[0]['MAJOR']
    condition = {
        'MAJOR': major,
        'MON': stat_month,
        'MAIN_TYPE': MainType.problem_exposure,
        'DETAIL_TYPE': 0
    }
    problem_exposure_score = mongo.db[coll_name].find(condition)
    problem_exposure_score = pd.DataFrame(list(problem_exposure_score)).set_index('DEPARTMENT_ID')
    major_avg = min(int(problem_exposure_score['SCORE'].mean()), 60)
    workshop_df['exposure_ratio'] = workshop_df['DEPARTMENT_ID'].apply(
        lambda value: 1 - (problem_exposure_score.loc[value]['SCORE'] - major_avg) / major_avg)
    return workshop_df


# 问题控制
def stats_repeatedly_index(happen_problem_point_sql, department_data, work_load, months_ago, choose_dpid_data,
                           cache_client):
    """
    以车间为统计单位同一问题项点是否反复发生（超过阀门值的才算重复发生），发生的每项扣a 分。
    严重风险的问题基础阀门值为2 次，较大风险的基础阀门值5 次，低风险问题不计算
    :param happen_problem_point_sql:
    :param department_data:
    :param work_load:
    :param months_ago:
    :param choose_dpid_data:
    :param cache_client:
    :return:
    """
    rst_index_score = []
    risk_level_threshold = {1: 2, 2: 3}
    risk_level_deduct = {0: {1: 3, 2: 1.5},
                         -1: {1: 2, 2: 1},
                         -2: {1: 1, 2: 0.5}}

    work_load = work_load.groupby(f'TYPE{HIERARCHY}', as_index=False)['COUNT'].sum()
    workshop_df = choose_dpid_data(HIERARCHY)
    workshop_df = pd.merge(workshop_df, work_load, left_on='DEPARTMENT_ID', right_on='TYPE4', how='left')
    workshop_df.fillna(0, inplace=True)
    workshop_df = _calc_ratio_for_repeatedly_index(workshop_df, months_ago)

    past_months_data = {}
    base_repeatedly_problem = _get_appoint_month_happen_problem(
        happen_problem_point_sql, department_data, risk_level_threshold,
        months_ago, HIERARCHY, cache_client)
    for i_month in range(-1, -3, -1):
        i_month_repeatedly_problem = _get_appoint_month_happen_problem(
            happen_problem_point_sql, department_data, risk_level_threshold,
            months_ago + i_month, HIERARCHY, cache_client)
        past_months_data[i_month] = i_month_repeatedly_problem

    rst_dict = {}
    for idx, row in base_repeatedly_problem.iterrows():
        problem_id = row['PK_ID']
        department_id = row['TYPE4']
        risk_level = row['RISK_LEVEL']
        # 先判断当前月是否达到阀门值
        current_month_count = row['COUNT']
        ratio_row = workshop_df.loc[workshop_df['DEPARTMENT_ID'] == department_id].iloc[0]
        threshold = int(ratio_row['unit_ratio'] * risk_level_threshold[risk_level])
        if current_month_count >= threshold:
            # 当月扣分
            department_rst = rst_dict.setdefault(department_id, dict())
            month_rst = department_rst.setdefault(0, dict())
            month_rst[risk_level] = month_rst.get(risk_level, 0) + 1
            department_rst['deduct'] = \
                department_rst.get('deduct', 0) + risk_level_deduct[0][risk_level]
            # 前两个月扣分
            for i_month in range(-1, -3, -1):
                past_data = past_months_data.get(i_month)
                rs = past_data[(past_data['PK_ID'] == problem_id) & (past_data[f'TYPE{HIERARCHY}'] == department_id)]
                if not rs.empty:
                    _count = rs.iloc[0]['COUNT']
                    if _count >= threshold:
                        department_rst = rst_dict.setdefault(department_id, dict())
                        month_rst = department_rst.setdefault(i_month, dict())
                        month_rst[risk_level] = month_rst.get(risk_level, 0) + 1
                        department_rst['deduct'] = \
                            department_rst.get('deduct', 0) + risk_level_deduct[i_month][risk_level]

    rst_df = workshop_df.apply(lambda r: _repeatedly_index_apply(r, rst_dict, risk_level_deduct),
                               axis=1, result_type='expand')
    rst_df = workshop_df.join(rst_df)
    column = f'SCORE_c_{HIERARCHY}'
    calc_basic_data_rst = format_export_basic_data(rst_df, MainType.problem_rectification,
                                                   ProblemRectificationDetailType.stats_repeatedly_index,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.problem_rectification,
                                     ProblemRectificationDetailType.stats_repeatedly_index,
                                     index_type=INDEX_TYPE)

    rst_data = pd.DataFrame(
        index=rst_df['DEPARTMENT_ID'],
        data=rst_df['SCORE'].values,
        columns=[column])
    # 计算排名、入库
    summizet_operation_set(rst_data, choose_dpid_data(HIERARCHY), column,
                           HIERARCHY, INDEX_TYPE, MainType.problem_rectification,
                           ProblemRectificationDetailType.stats_repeatedly_index, months_ago)
    rst_score = rst_data[[column]]
    if len(rst_score) != 0:
        rst_index_score.append(rst_score)
    return rst_index_score


def export_rectification_effect(data, department_data, choose_dpid_data, main_type, detail_type, months_ago,
                                _calc_score):
    """导出各类事故的次数

    Arguments:
        data {pandas.DataFrame} --
    """
    # 保存中间过程数据
    monthly_basic_data = []
    for idx, row in data.iterrows():
        resp_level = row['RESPONSIBILITY_IDENTIFIED']
        if resp_level in [1, 2, 3, 8]:
            resp_level = 1
        elif resp_level in [4, 5, 9]:
            resp_level = 2
        else:
            resp_level = 3
        monthly_basic_data.append(
            [row['FK_DEPARTMENT_ID'], row['MAIN_TYPE'], resp_level])
    first_title = {1: '事故', 2: '故障', 3: '综合信息'}
    second_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    return export_basic_data_tow_field_monthly_two(monthly_basic_data, department_data,
                                                   choose_dpid_data(HIERARCHY), main_type, detail_type,
                                                   HIERARCHY, months_ago, first_title, second_title)


def export_waring_effect(data, department_data, choose_dpid_data, hierarchy):
    """导出警告预警次数"""
    data = df_merge_with_dpid(data, department_data)
    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # 补全站段数据
    data = pd.merge(
        data.to_frame(),
        choose_dpid_data(HIERARCHY)["DEPARTMENT_ID"].to_frame(),
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index('DEPARTMENT_ID', inplace=True)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '警告性预警延期次数: {0}'.format(
            f'{int(row["COUNT"])}'),
        axis=1)
    data.pop("COUNT")
    return data


# 整改成效
def stats_rectification_effect(responsibe_safety_produce_info_sql, department_data, warning_delay_sql, months_ago,
                               choose_dpid_data, calc_score):
    """警告性预警延期一次扣10分(暂未加入)；责任事故主要、全部责任的1个扣20分、重要扣15分、
    次要的扣10分（含追究责任）；
    D21等同故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分；
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。直接在总分中扣，
    改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    """
    calc_month = get_custom_month(months_ago)
    responsibe_info = pd_query(responsibe_safety_produce_info_sql.format(*calc_month))
    warning_delay = pd_query(warning_delay_sql.format(*calc_month))
    responsibe_data = export_rectification_effect(responsibe_info, department_data, choose_dpid_data,
                                                  MainType.problem_rectification,
                                                  ProblemRectificationDetailType.stats_rectification_effect,
                                                  months_ago, calc_score)
    warning_data = export_waring_effect(warning_delay, department_data, choose_dpid_data, HIERARCHY)
    calc_df_data = pd.concat([responsibe_data, warning_data], axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([str(row[col]) for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(HIERARCHY),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, MainType.problem_rectification,
                                                   ProblemRectificationDetailType.stats_rectification_effect, HIERARCHY,
                                                   months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.problem_rectification,
                                     ProblemRectificationDetailType.stats_rectification_effect, index_type=INDEX_TYPE)

    # 责任分
    data = department_data.copy()
    if responsibe_info.empty:
        data["SCORE_1"] = 0
    else:
        responsibe_info['SCORE_1'] = responsibe_info.apply(lambda row: calc_score(row), axis=1)
        responsibe_info.set_index("FK_DEPARTMENT_ID", inplace=True)
        responsibe_info.drop(['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
        data = pd.merge(
            responsibe_info,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    # 警告分
    if warning_delay.empty:
        data["SCORE_2"] = 0
    else:
        warning_delay['SCORE_2'] = warning_delay.apply(lambda row: int(row['COUNT'] * 10), axis=1)
        warning_delay.drop(['COUNT'], inplace=True, axis=1)
        warning_delay.set_index("FK_DEPARTMENT_ID", inplace=True)
        data = pd.merge(
            warning_delay,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    data.fillna(0, inplace=True)
    data['SCORE'] = data.apply(lambda row: round(row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    data.drop(["SCORE_1", "SCORE_2"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        INDEX_TYPE,
        MainType.problem_rectification,
        ProblemRectificationDetailType.stats_rectification_effect,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: 40 if x > 40 else x,
        choose_dpid_data,
        NA_value=True)
    return rst_child_score


# 整改复查
def stats_rectification_review(check_info_df, work_load, department_data, months_ago,
                               calc_func, choose_dpid_data):
    """库内问题复查数/总人数，与专业基数比较
    """
    check_info_df = check_info_df.copy()
    check_info_df.drop_duplicates(subset=['PK_ID', 'FK_DEPARTMENT_ID'], inplace=True)
    recheck_count = df_merge_with_dpid(check_info_df, department_data)
    recheck_count.rename(columns={'PROBLEM_REVIEW_NUMBER': 'COUNT'}, inplace=True)

    return calc_child_index_type_divide(
        recheck_count,
        work_load, INDEX_TYPE, MainType.problem_rectification,
        ProblemRectificationDetailType.stats_rectification_review, months_ago, 'COUNT', 'SCORE_d',
        calc_func, choose_dpid_data)


# 隐患整治 - 隐患库
def _peril_count_score(peril_count_sql, choose_dpid_data, department_data, work_load, peril_count_score_formula):
    """隐患库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    peril_count = df_merge_with_dpid(
        pd_query(peril_count_sql), department_data)
    peril_count = peril_count.groupby([f'TYPE{HIERARCHY}'])['COUNT'].sum()
    peril_count = peril_count.to_frame(name='peril')
    work_load = work_load.groupby(['TYPE4'], as_index=False)['COUNT'].sum()
    work_load.rename(columns={'COUNT': 'PERSON_NUMBER', 'TYPE4': 'DEPARTMENT_ID'}, inplace=True)
    data = pd.merge(peril_count, work_load,
                    how='right', right_on='DEPARTMENT_ID', left_index=True)

    # 补全
    data = pd.merge(
        choose_dpid_data(HIERARCHY), data, how='left', left_on='DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    data.drop(columns=['NAME', "MAJOR"], inplace=True)
    data = pd.DataFrame(data, columns=['PERSON_NUMBER', 'DEPARTMENT_ID', 'peril'])
    data.set_index('DEPARTMENT_ID', inplace=True)

    data.fillna(0, inplace=True)
    data['ratio'] = data['peril'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(HIERARCHY), 'ratio',
        peril_count_score_formula)
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: '隐患库(40%):<br/>单位级隐患数量({0})/单位总人数({1})'.format(
            f'{int(row["peril"])}', int(row['PERSON_NUMBER'])),
        axis=1)
    data.drop(
        columns=['peril', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, pd.DataFrame(data, columns=['middle_1'])


# 隐患整治 - 隐患整治延期
def _peril_overdue(peril_overdue_count_sql, peril_period_count_sql, peril_count_score,
                   choose_dpid_data, department_data):
    """隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    peril_overdue_data = df_merge_with_dpid(
        pd_query(peril_overdue_count_sql), department_data)
    peril_period_data = df_merge_with_dpid(
        pd_query(peril_period_count_sql), department_data)
    peril_overdue_count = peril_overdue_data.groupby(
        [f'TYPE{HIERARCHY}'])['COUNT'].sum()
    peril_period_count = peril_period_data.groupby(
        [f'TYPE{HIERARCHY}'])['COUNT'].sum()
    data = pd.concat(
        [
            peril_overdue_count.to_frame(name='COUNT_OVERDUE'),
            peril_period_count.to_frame(name='COUNT_PERIL')
        ],
        axis=1,
        sort=False)

    # 补全站段数据
    data = pd.merge(
        data,
        choose_dpid_data(HIERARCHY),
        how="right",
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index(data['DEPARTMENT_ID'], inplace=True)
    data.drop(columns=['DEPARTMENT_ID', 'NAME', "MAJOR"], inplace=True)
    data.fillna(0, inplace=True)

    series_rst = data.apply(lambda row: max(0, 100 - (row['COUNT_PERIL'] * 2 + row['COUNT_OVERDUE'])), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '隐患整治延期（20%）:<br/>延期数量：({0})/转为长期整治数量：({1})'.format(
            int(row['COUNT_OVERDUE']), int(row['COUNT_PERIL'])),
        axis=1)
    data.drop(columns=['COUNT_OVERDUE', 'COUNT_PERIL'], inplace=True, axis=1)
    return df_rst, pd.DataFrame(data, columns=['middle_2'])


# 隐患整治 - 隐患整治督促
def _peril_urge(checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql, department_data, months_ago,
                peril_count_score, choose_dpid_data):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    calc_month = get_custom_month(months_ago)
    # 未检查的
    CHECKED_PERIL_ID = pd_query(checked_peril_id_sql.format(*calc_month))
    PERIL_ID = pd_query(peril_id_sql)
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID.drop_duplicates(subset=['FK_DEPARTMENT_ID', 'PID'], keep=False, inplace=True)
    PERIL_ID = df_merge_with_dpid(PERIL_ID, department_data)

    # 长期整治的没录入
    PERIL_RECTIFY_NO_ENTRY = df_merge_with_dpid(pd_query(
        peril_rectify_no_entry_sql.format(*calc_month)), department_data)

    data = pd.concat(
        [
            PERIL_ID.groupby(['TYPE3'])['PID'].size(),
            PERIL_RECTIFY_NO_ENTRY.groupby(['TYPE3'
                                            ])['COUNT'].size()
        ],
        axis=1,
        sort=False)
    # 补全站段数据
    data = pd.merge(
        data,
        choose_dpid_data(HIERARCHY),
        how="right",
        left_index=True,
        right_on='DEPARTMENT_ID',
    )
    data.set_index(data['DEPARTMENT_ID'], inplace=True)
    data.drop(columns=['DEPARTMENT_ID', 'NAME', "MAJOR"], inplace=True)
    data.fillna(0, inplace=True)
    # 计算分数
    series_rst = data.apply(lambda row: min(100, (row['PID'] + row['COUNT']) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)

    data['middle_3'] = data.apply(
        lambda row: '隐患整治督促（40%）:<br/>未检查的个数：({0})/长期整治没录入({1})'.format(int(row['PID']), int(row['COUNT'])), axis=1)
    data.drop(columns=['COUNT', 'PID'], inplace=True, axis=1)
    return df_rst, pd.DataFrame(data, columns=['middle_3'])


# 隐患整治
def stats_peril_renovation(peril_count_sql, peril_overdue_count_sql, peril_period_count_sql,
                           checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql,
                           peril_count_score_formula,
                           department_data, work_load, months_ago, choose_dpid_data,
                           weight_dict=None):
    weight_dict = [0.4, 0.2, 0.4]
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in [HIERARCHY]:
        score = []
        # 隐患库
        peril_count_score, peril_count_score_basic_data = _peril_count_score(
            peril_count_sql, choose_dpid_data, department_data, work_load, peril_count_score_formula)
        score.append(peril_count_score * weight_dict[0])
        calc_basic_data.append(peril_count_score_basic_data)

        # 隐患整治延期
        peril_overdue_score, peril_overdue_score_basic_data = _peril_overdue(
            peril_overdue_count_sql, peril_period_count_sql, peril_count_score, choose_dpid_data, department_data)
        score.append(peril_overdue_score * weight_dict[1])
        calc_basic_data.append(peril_overdue_score_basic_data)

        # 隐患整治督促
        peril_urge_score, peril_urge_score_basic_data = _peril_urge(
            checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql, department_data, months_ago,
            peril_count_score, choose_dpid_data
        )
        score.append(peril_urge_score * weight_dict[2])
        calc_basic_data.append(peril_urge_score_basic_data)

        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(HIERARCHY),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(calc_df_data, MainType.problem_rectification,
                                                       ProblemRectificationDetailType.stats_peril_renovation,
                                                       HIERARCHY,
                                                       months_ago)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.problem_rectification,
                                         ProblemRectificationDetailType.stats_peril_renovation)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_e_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(df_rst, choose_dpid_data(hierarchy), column,
                               hierarchy, INDEX_TYPE, MainType.problem_rectification,
                               ProblemRectificationDetailType.stats_peril_renovation, months_ago)
        rst_child_score.append(df_rst)
    return rst_child_score
