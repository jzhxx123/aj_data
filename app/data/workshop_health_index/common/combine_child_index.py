#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/13
Description: 
"""

import pandas as pd

from app import mongo
from app.data.workshop_health_index.common.common import (append_major_column_to_df,
                                                          merge_all_child_item)
from app.data.workshop_health_index.const import INDEX_TYPE
from app.data.index.common import get_zhanduan_deparment
from app.data.util import (get_coll_prefix, get_history_months, pd_query,
                           write_bulk_mongo, update_major_maintype_weight, get_mongodb_prefix)


def _choose_dpid_data(hierarchy, zhanduan_dpid_sql, chejian_dpid_sql):
    dpid_data = {
        3: get_zhanduan_deparment(zhanduan_dpid_sql),
        4: pd_query(chejian_dpid_sql),
    }
    return dpid_data.get(hierarchy)


def merge_child_index(chejian_data,
                      major,
                      months_ago,
                      child_index_list,
                      child_index_weight,
                      vitual_major_ids=None):
    """将各个子指数加权求和

    Arguments:
        months_ago {int} -- 第前-N个月
    """
    year_mon = get_history_months(months_ago)[0]
    _prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(INDEX_TYPE)
    coll_name = f'{_prefix}detail_{coll_prefix}_index'
    data = []
    # 获取子指数数据
    for main_type in child_index_list:
        for hierarchy in [4]:
            child_data = pd.DataFrame(
                list(mongo.db[coll_name].find({
                    'MAJOR': major,
                    "MAIN_TYPE": main_type,
                    "DETAIL_TYPE": 0,
                    "MON": year_mon,
                    'HIERARCHY': hierarchy,
                }, {
                    "_id": 0,
                    "SCORE": 1,
                    "DEPARTMENT_ID": 1,
                })))
            if child_data.empty is True:
                continue
            child_data = pd.DataFrame(
                index=child_data['DEPARTMENT_ID'],
                data=child_data.loc[:, 'SCORE'].values,
                columns=[f'SCORE_{main_type}_{hierarchy}'])
            data.append(child_data)
    item_name = [f'SCORE_{x}' for x in child_index_list]
    item_weight = child_index_weight
    for hierarchy in [4]:
        h_child_score = [
            x for x in data if x.columns.values[0][-1] == str(hierarchy)
        ]
        xdata = pd.concat(h_child_score, axis=1, sort=False)
        xdata.fillna(0, inplace=True)
        xdata['SCORE'] = xdata.apply(
            lambda row: merge_all_child_item(
                row, hierarchy, item_name, item_weight),
            axis=1)
        xdata = append_major_column_to_df(chejian_data, xdata)
        xdata['group_sort'] = xdata['SCORE'].groupby(xdata['MAJOR']).rank(
            ascending=0, method='first')
        xdata['classify_sort'] = xdata['SCORE'].groupby(xdata['CLASSIFY']).rank(
            ascending=0, method='first')
        xdata.dropna(inplace=True)
        rst = []
        cls_count = {}
        for index, row in xdata.iterrows():
            rst.append({
                'MON': year_mon,
                'MAJOR': row['MAJOR'],
                'HIERARCHY': hierarchy,
                'DEPARTMENT_ID': row['DEPARTMENT_ID'],
                'DEPARTMENT_NAME': row['NAME'],
                'SCORE': round(row['SCORE'], 2),
                'RANK': int(row['group_sort']),
                'CLS_RANK': int(row['classify_sort']),
                'TYPE3': row['TYPE3'],
                'CLASSIFY': row['CLASSIFY'],
                'CLS_COUNT': cls_count.setdefault(row['CLASSIFY'],
                                                  xdata[xdata['CLASSIFY'] == row['CLASSIFY']].index.size)
            })
        # 存入mongo
        coll_name = f'{_prefix}{coll_prefix}_index'
        mongo.db[coll_name].remove({
            'MON': year_mon,
            'HIERARCHY': hierarchy,
            'MAJOR': major
        })
        write_bulk_mongo(coll_name, rst)


if __name__ == '__main__':
    pass
