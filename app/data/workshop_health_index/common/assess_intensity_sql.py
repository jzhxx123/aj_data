
# 月考核超期问题详细
ASSESS_RROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY a.FK_DEPARTMENT_ID;
"""
# 月考核量化详细
ASSESS_QUANTIFY_SQL = """SELECT
        a.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_quantify_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY a.FK_DEPARTMENT_ID;
"""
# 月考核问题修订
ASSESS_REVISE_SQL = """SELECT
        a.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_revise_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 月度返奖明细
AWARD_RETURN_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON a.ID_CARD = c.ID_CARD
    WHERE
        b.STATUS = 3
            AND a.IS_RETURN = 1
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND c.IDENTITY != '干部'
"""

# 月度返奖明细
AWARD_RETURN_DETAIL_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN,a.MONEY
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON a.ID_CARD = c.ID_CARD
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND c.IDENTITY != '干部'
"""

# 非路外问题数
CHECK_PROBLEM_NOT_OUT_WAY_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND IS_EXTERNAL = 0
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

ASSESS_RESPONSIBLE_SQL = """SELECT 
    a.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT, FK_CHECK_PROBLEM_ID
FROM
    t_safety_assess_month_responsible_detail AS a
        LEFT JOIN
    t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
WHERE
    b.STATUS = 3
    AND b.YEAR = {0}
    AND b.MONTH = {1};
"""