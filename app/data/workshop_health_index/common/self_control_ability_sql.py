#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/8
Description: 
"""

# 安全生产信息问题
SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.FK_PROBLEM_BASE_ID,
        c.FK_DEPARTMENT_ID,
        a.RANK as RISK_LEVEL,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
    WHERE
        a.RANK BETWEEN 1 AND 2
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

EVALUATE_SCORE_SQL = """SELECT
        FK_DEPARTMENT_ID, a.SCORE, a.FK_PERSON_GRADATION_RATIO_ID, c.GRADATION_RATIO
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            LEFT JOIN
        t_person_gradation_ratio AS c on c.PK_ID = a.FK_PERSON_GRADATION_RATIO_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 月考核金额关联问题检查人部门
ASSESS_PROBLEM_AND_CHECK_INFO_SQL = """SELECT 
    a.FK_DEPARTMENT_ID,
    a.ACTUAL_MONEY,
    a.FK_CHECK_PROBLEM_ID,
    c.IS_EXTERNAL,
    e.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID_CHECK
FROM
    t_safety_assess_month_responsible_detail AS a
        LEFT JOIN
    t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
        LEFT JOIN
    t_check_problem c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
        LEFT JOIN
    t_check_info d ON d.PK_ID = c.FK_CHECK_INFO_ID
        LEFT JOIN
    t_check_info_and_person e ON e.FK_CHECK_INFO_ID = d.PK_ID
WHERE
    b.STATUS = 3
    AND b.YEAR = {0}
    AND b.MONTH = {1};
"""


# 月考核金额关联责任部门
ASSESS_PROBLEM_AND_RESPONSIBLE_SQL = """SELECT 
    a.FK_DEPARTMENT_ID,
    a.ACTUAL_MONEY,
    a.FK_CHECK_PROBLEM_ID,
    d.FK_DEPARTMENT_ID as FK_DEPARTMENT_ID_RESPONSIBLE
FROM
    t_safety_assess_month_responsible_detail AS a
        LEFT JOIN
    t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
        LEFT JOIN
    t_check_problem c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
        LEFT JOIN
    t_check_problem_and_responsible_department d ON d.FK_CHECK_PROBLEM_ID = c.PK_ID
WHERE
    b.STATUS = 3
    AND b.YEAR = {0}
    AND b.MONTH = {1};
"""