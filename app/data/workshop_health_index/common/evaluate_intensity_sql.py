# 干部评价记分总条数
EVALUATE_RECORD_SQL = """SELECT
        a.RESPONSIBE_DEPARTMENT_ID as FK_DEPARTMENT_ID,
        a.CODE_ADDITION,
        a.CODE,
        a.EVALUATE_TYPE,
        a.EVALUATE_WAY,
        a.CHECK_TYPE
    FROM
        t_check_evaluate_info AS a
    WHERE 1
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

