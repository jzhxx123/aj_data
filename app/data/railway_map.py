#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from math import atan2, cos, degrees, radians, sin, sqrt

import pandas as pd
import pyproj
from flask import current_app

from app import mongo
from app.data.util import (get_today, pd_query, write_bulk_mongo)
import datetime

# from line_profiler import LineProfiler


def trans_cor(lo, la):
    p1 = pyproj.Proj(init="epsg:4507")
    lo_trans, la_trans = p1(lo, la, inverse=True)
    return lo_trans, la_trans


def center_geolocation(geolocations):
    x = 0
    y = 0
    z = 0
    lenth = len(geolocations)
    if lenth == 0:
        return None
    for lon, lat in geolocations:
        lon = radians(float(lon))
        lat = radians(float(lat))

        x += cos(lat) * cos(lon)
        y += cos(lat) * sin(lon)
        z += sin(lat)

    x = float(x / lenth)
    y = float(y / lenth)
    z = float(z / lenth)

    return (degrees(atan2(y, x)), degrees(atan2(z, sqrt(x * x + y * y))))


def get_longlatitue_TYPE3(df_address):
    '''如果下级科室部门没有设置经纬度，
        则纳入下级车间进行计算,如果下级科室部门有党群领导、行政领导，则取他们做平均，
        若没有，则使用其他的部门求重心
    '''
    rst_all = []
    rst_keshi = []
    rst_special = []
    for lon, lat, name, TYPE in list(df_address.values):
        rst_all.append((lon, lat))
        if TYPE < 8:
            rst_keshi.append((lon, lat))
            if name in ['党群领导', '行政领导']:
                rst_special.append((lon, lat))
    if len(rst_keshi) > 0:
        if len(rst_special) > 0:
            return center_geolocation(rst_special)
        return center_geolocation(rst_keshi)
    return center_geolocation(rst_all)


def get_map_data(mon_point):
    db = mongo.db
    last_mon_point = int('{}{:0>2}'.format(mon_point[0].year, mon_point[0].month))
    mon_point = int('{}{:0>2}'.format(mon_point[1].year, mon_point[1].month))
    evaluate = pd.DataFrame(
        list(db['{}map_evaluate'.format(prefix)].find({
            "MON": mon_point
        }, {"_id": 0})))
    safety_produce_info = pd.DataFrame(
        list(db['{}map_safety_produce_info'.format(prefix)].find(
            {
                "MON": mon_point
            }, {"_id": 0})))
    check_problem = pd.DataFrame(
        list(db['{}map_check_problem'.format(prefix)].find({
            "MON": mon_point
        }, {"_id": 0})))
    check_info = pd.DataFrame(
        list(db['{}map_check_info'.format(prefix)].find({
            "MON": mon_point
        }, {"_id": 0})))
    if any([
        x.empty for x in
        [evaluate, safety_produce_info, check_info, check_problem]
    ]):
        raise ValueError(f'某些项数据为空，{mon_point}月数据异常')
    data = pd.merge(
        evaluate,
        safety_produce_info,
        how="outer",
        left_on=["DPID", "MON"],
        right_on=["DPID", "MON"])
    data.rename(
        columns={
            "COUNT": "SAFETY_COUNT",
        }, inplace=True)
    data = pd.merge(
        data,
        check_problem,
        how="outer",
        left_on=["DPID", "MON"],
        right_on=["DPID", "MON"])
    data.rename(
        columns={
            "COUNT": "CHECK_PROBLEM_COUNT",
        }, inplace=True)
    data = pd.merge(
        data,
        check_info,
        how="outer",
        left_on=["DPID", "MON"],
        right_on=["DPID", "MON"])
    data.rename(columns={"COUNT": "CHECK_INFO_COUNT"}, inplace=True)

    data = data.loc[data['MON'].notnull()]  # 去除MON为空的行
    # 去除3个属性值都为空的行
    data = data[data.SAFETY_COUNT.notnull()
                | data.CHECK_PROBLEM_COUNT.notnull()
                | data.SCORE.notnull() | data.CHECK_INFO_COUNT.notnull()]
    data.drop_duplicates(inplace=True)

    major_sql = "SELECT DEPARTMENT_ID, TYPE2, HIERARCHY, SOURCE_PARENT_ID, \
                    NAME, ALL_NAME, TYPE FROM t_department;"

    major_data = pd_query(major_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID_x': major_data['DEPARTMENT_ID'],
            'TYPE2': major_data['TYPE2']
        }),
        how='inner',
        left_on='DPID',
        right_on='DPID_x')
    del data['DPID_x']
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID_y': major_data['DEPARTMENT_ID'],
            'MAJOR': major_data['NAME']
        }),
        how='inner',
        left_on='TYPE2',
        right_on='DPID_y')
    del data['DPID_y']
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID_z':
                major_data['DEPARTMENT_ID'],
            'NAME':
                major_data['NAME'],
            'HIERARCHY':
                major_data['HIERARCHY'].apply(lambda x: x - 2),
            'SOURCE_PARENT_ID':
                major_data['SOURCE_PARENT_ID'],
            'ALL_NAME':
                major_data['ALL_NAME'],
            'TYPE':
                major_data['TYPE'],
        }),
        how='inner',
        left_on='DPID',
        right_on='DPID_z')
    del data['DPID_z']
    del data['TYPE2']
    data.rename(columns={"DPID_x": "DPID"}, inplace=True)
    majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    data = data.loc[data['MAJOR'].isin(majors)]
    address_sql = """SELECT 
                    a.*,
                    b.TYPE3
                FROM
                    t_address_and_map AS a
                        INNER JOIN
                    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
                    WHERE a.FK_DEPARTMENT_ID IS NOT NULL;
                    """
    address_data = pd_query(address_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID_a':
                address_data['FK_DEPARTMENT_ID'],
            'LONGITUDE':
                address_data.apply(
                    lambda row: trans_cor(row['LONGITUDE'], row['LATITUDE'])[0],
                    axis=1),
            'LATITUDE':
                address_data.apply(
                    lambda row: trans_cor(row['LONGITUDE'], row['LATITUDE'])[1],
                    axis=1)
        }),
        how='left',
        left_on='DPID',
        right_on='DPID_a')
    _address = {}
    # type3的部门的经纬度又下级部门经纬度的重心所得
    for each in data[data['HIERARCHY'] == 1].values:
        dpid = each[0]
        if dpid not in _address:
            res = get_longlatitue_TYPE3(
                data[data['SOURCE_PARENT_ID'] == dpid]
                    .loc[:, ['LONGITUDE', 'LATITUDE', 'NAME', 'TYPE']].dropna(
                    axis=0))
            if res is not None:
                _address.update({dpid: res})
    del data['DPID_a']
    today = get_today()
    if (prefix == 'daily_') and (today.day !=
                                 current_app.config.get('UPDATE_DAY')):
        pass
    else:
        # 添加综合指数
        index_prefix = 'monthly_' if prefix == 'daily_' else prefix
        # 判断当前时间是否25号后，25号之前，使用上一个的综合指数
        today = datetime.date.today()
        if today.day >= 25:
            target_mon_point = mon_point
        else:
            target_mon_point = last_mon_point
        health_index = pd.DataFrame(
            list(db['{}health_index'.format(index_prefix)].find(
                {
                    "MON": target_mon_point,
                    "HIERARCHY": 3
                }, {
                    "_id": 0,
                    "DEPARTMENT_ID": 1,
                    "RANK": 1,
                    "SCORE": 1
                })))
        data = pd.merge(
            data,
            health_index,
            how="left",
            left_on=["DPID"],
            right_on=["DEPARTMENT_ID"])
    data = data.fillna(0)
    result = []
    for index, row in data.iterrows():
        record = {
            'DPID': row['DPID'],
            'MON': row['MON'],
            'MAJOR': row['MAJOR'],
            'LEVEL': row['HIERARCHY'],
            'NAME': row['NAME'],
            'ALL_NAME': row['ALL_NAME'],
            'PARENT': row['SOURCE_PARENT_ID'],
            'SAFETY_PRODUCE_INFO': int(row['SAFETY_COUNT']),
            'CHECK_PROBLEM': int(row['CHECK_PROBLEM_COUNT']),
            'CHECK_INFO': int(row['CHECK_INFO_COUNT']),
        }
        if 'RANK' in row:
            record.update({
                'EVALUATE': row['SCORE_x'],
                'RANK': row['RANK'],
                'SCORE': row['SCORE_y']
            })
        else:
            record.update({'EVALUATE': row['SCORE']})
        if row['HIERARCHY'] != 1:
            record.update({
                'LONGITUDE': row['LONGITUDE'],
                'LATITUDE': row['LATITUDE']
            })
        else:
            if row['DPID'] not in _address:
                continue
            record.update({
                'LONGITUDE': _address.get(row['DPID'])[0],
                'LATITUDE': _address.get(row['DPID'])[1]
            })
        result.append(record)
    current_app.logger.debug('complete: format data.')
    coll_name = '{}map_data'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    current_app.logger.debug(
        f'handle({mon_point[1].year}年{mon_point[1].month}月) data.')
    # 获取各个部门的安全生产信息（主屏）
    get_map_data(mon_point)
    return 'OK'


# def profile():
#     lp = LineProfiler(execute)
#     lp.add_function(get_map_data)
#     lp.runcall(execute)
#     lp.current_app.logger.debug_stats()

if __name__ == '__main__':
    pass
