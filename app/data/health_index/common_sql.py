# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.HIERARCHY
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND b.DEPARTMENT_ID NOT IN ('19B8C3534DE85665E0539106C00A58FD' , 
                                        '19B8C3534E2C5665E0539106C00A58FD',
                                        '19B8C3534E33512345539106C00A58FD',
                                        '19B8C3534E3A566512345106C00A58FD')
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR, a.BELONG_PROFESSION_NAME
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND a.DEPARTMENT_ID NOT IN ('19B8C3534DE85665E0539106C00A58FD' , 
                                        '19B8C3534E2C5665E0539106C00A58FD',
                                        '19B8C3534E33512345539106C00A58FD',
                                        '19B8C3534E3A566512345106C00A58FD');
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != '';
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
        LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0 AND b.IS_DELETE=0
    GROUP BY FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.is_delete = 0
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person
    WHERE
        IS_DELETE = 0 AND FK_DEPARTMENT_ID != ''
            AND IDENTITY = '干部'
    GROUP BY FK_DEPARTMENT_ID;
"""

# 量化人员数量,去除各项指标（包括基础指标和细化指标）都为零的
QUANTIZATION_PERSON_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_quantization_base_quota
    WHERE
        YEAR = {0} AND MONTH = {1}
            AND `STATUS` = 1
            AND (ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_base_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(CHECK_TIMES_TOTAL, 0)
                    + IFNULL(PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_NUMBER_TOTAL, 0)
                    + IFNULL(WORK_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(CHECK_NOTIFICATION_TIMES_TOTAL, 0)
                    + IFNULL(MIN_QUALITY_GRADES_TOTAL, 0)
                    + IFNULL(HIDDEN_DANGER_RECHECK_TIMES_TOTAL, 0
                    + IFNULL(RISK_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL, 0) > 0))
            OR ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_refinement_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_TIME_TOTAL, 0)) > 0)))
    GROUP BY FK_DEPARTMENT_ID
"""

# 检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b
        ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 检查次数（现场检查）
XIANCHANG_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b
        ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 发现问题数
CHECK_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 非路外问题数
CHECK_PROBLEM_ONOUTWAY_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND IS_EXTERNAL = 0
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 一般及以上风险问题数(量化人员及干部)
RISK_LEVEL_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_LEVEL <= 3
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""


# 问题质量分累计(量化人员及干部)
PROBLEM_CHECK_SCORE_SQL = """SELECT
    EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
FROM
    t_check_problem AS a
        LEFT JOIN
    t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
        LEFT JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
WHERE
    c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND c.CHECK_TYPE NOT IN (102, 103)
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 较大及以上风险问题数
JIAODA_RISK_LEVEL_PROBLEM_SQL = """
    SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_LEVEL <= 2
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# MAIN_TYPE=1为事故
# MAIN_TYPE=2为故障
BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT,
    a.MAIN_TYPE
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_department AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND a.MAIN_TYPE in (1, 2)
"""