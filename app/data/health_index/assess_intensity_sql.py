# 月度考核问题责任人详细
ASSESS_RESPONSIBLE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY b.FK_DEPARTMENT_ID;
"""
# 月考核超期问题详细
ASSESS_RROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY b.FK_DEPARTMENT_ID;
"""
# 月考核量化详细
ASSESS_QUANTIFY_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_quantify_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY b.FK_DEPARTMENT_ID;
"""
# 月考核问题修订
ASSESS_REVISE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_revise_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND a.ID_CARD IN (SELECT ID_CARD FROM t_person)
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 月度返奖明细
AWARD_RETURN_SQL = """SELECT
        b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN,a.MONEY
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON a.ID_CARD = c.ID_CARD
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND c.IDENTITY != '干部'
"""

#
# # 达到返奖时限问题个数(IS_RETURN未返奖,RETURN_TIME理论返奖日期)
# NOT_AWARD_RETURN_SQL = """
#     SELECT
#         c.FK_DEPARTMENT_ID, b.LEVEL
#     FROM
#         t_check_problem AS a
#             LEFT JOIN
#         t_safety_award_responsible_return_detail AS b
#             ON b.FK_CHECK_PROBLEM_ID = a.PK_ID
#             LEFT JOIN
#         t_safety_award_responsible_return AS c
#             ON b.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = c.PK_ID
#             LEFT JOIN
#         t_responsible_waiting_return AS d
#             ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
#     WHERE
#         c.STATUS = 3
#         AND b.IS_RETURN = 0
#         AND DATE_FORMAT(d.RETURN_TIME, '%%Y-%%m-%%d')
#             >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#         AND DATE_FORMAT(d.RETURN_TIME, '%%Y-%%m-%%d')
#             <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
# """

# 月度返奖问题明细
AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON a.ID_CARD = c.ID_CARD
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND c.IDENTITY != '干部'
            AND a.LEVEL in {2}
            AND a.MONEY > 0
"""

# 月度实际返奖问题明细
REAL_AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID,1 AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_person AS c ON a.ID_CARD = c.ID_CARD
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND c.IDENTITY != '干部'
            AND a.LEVEL in {2}
            AND a.ACTUAL_MONEY > 0
            AND a.IS_RETURN = 1
"""