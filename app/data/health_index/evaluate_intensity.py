#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd
import math

from app.data.health_index.common_sql import (
    CADRE_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.health_index.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, ANALYSIS_CENTER_ASSESS_SQL,
    DUAN_CADRE_COUNT_SQL, EVALUATE_COUNT_SQL, ACCUMULATIVE_EVALUATE_SCORE_SQL,
    LUJU_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
    ZHANDUAN_EVALUATE_SCORE_SQL, PERSON_ID_CARD_SQL)
from app.data.index.common import (
    calc_child_index_type_divide_major, calc_child_index_type_sum,
    combine_child_index_func, df_merge_with_dpid, get_zhanduan_deparment,
    summizet_child_index, write_export_basic_data_to_mongo, calc_check_count_per_person, append_major_column_to_df,
    format_export_basic_data)
from app.data.index.util import get_custom_month, validate_exec_month
from app.data.util import pd_query, update_major_maintype_weight
from app.data.health_index.calc_cardinal_number import evaluate_intensity_cardinal_number


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _calc_score_for_check_count_per_person(self_ratio, major, major_ratio_dict):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    major_ratio = major_ratio_dict.get(major)
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    N1 = major_ratio[0][0]
    Sn1 = major_ratio[0][1]
    N2 = major_ratio[1][0]
    Sn2 = major_ratio[1][1]
    N3 = major_ratio[2][0]
    Sn3 = major_ratio[2][1]
    C = self_ratio
    if level == 1:
        score = Sn1
    elif level == 2:
        score = Sn2 + (C - N2) * (Sn1 - Sn2) / (N1 - N2)
    elif level == 3:
        score = Sn3 + (C - N3) * ((Sn2 - Sn3) / (N2 - N3))
    else:
        score = Sn3 + (C - N3) * (((Sn2 - Sn3) / (N2 - N3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score


def _get_sql_data(months_ago):
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, \
        ACTIVE_EVALUATE_KEZHI_COUNT, ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DUAN_CADRE_COUNT, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, CHILD_INDEX_SQL_DICT
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    stats_month = get_custom_month(months_ago)
    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL), DEPARTMENT_DATA)

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 干部主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month)), DEPARTMENT_DATA)
    # CHILD_INDEX_SQL_DICT = evaluate_intensity_cardinal_number(months_ago)
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    # 各个站段的分数
    # fraction = CHILD_INDEX_SQL_DICT.get('2-1')[0]
    return calc_child_index_type_divide_major(
        ACTIVE_EVALUATE_COUNT,
        TOTAL_EVALUATE_COUNT,
        1,
        2,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        _choose_dpid_data,
        hierarchy_list=[3])


# 干部人均主动评价记分条数
def _stats_count_per_person(months_ago):
    major_ratio_dict = {
        '车务': [(0.16, 100), (0.15, 90), (0.055, 60)],
        '客运': [(0.16, 100), (0.15, 90), (0.04, 60)],
        '机务': [(0.16, 100), (0.15, 90), (0.06, 60)],
        '工务': [(0.16, 100), (0.15, 90), (0.06, 60)],
        '工电': [(0.16, 100), (0.15, 90), (0.06, 60)],
        '车辆': [(0.16, 100), (0.15, 90), (0.098, 60)],
        '电务': [(0.16, 100), (0.15, 90), (0.1, 60)],
        '供电': [(0.16, 100), (0.16, 90), (0.08, 60)],
    }
    return calc_check_count_per_person(
        ACTIVE_EVALUATE_COUNT, CADRE_COUNT, 1, 2, 2, months_ago, 'COUNT',
        'SCORE_b', calc_func=_calc_score_for_check_count_per_person, dpid_func=_choose_dpid_data,
        major_ratio_dict=major_ratio_dict)


# 干部人均主动评价记分分数
def _stats_score_per_person(months_ago):
    # fraction = CHILD_INDEX_SQL_DICT.get('2-3')[0]
    return calc_child_index_type_divide_major(
        ACTIVE_EVALUATE_SCORE, CADRE_COUNT, 1, 2, 3, months_ago, 'COUNT',
        'SCORE_c', _calc_score_by_formula, _choose_dpid_data,
    )


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    # fraction = CHILD_INDEX_SQL_DICT.get('2-4')[0]
    return calc_child_index_type_divide_major(
        ACTIVE_EVALUATE_KEZHI_COUNT, TOTAL_EVALUATE_COUNT, 1, 2, 4, months_ago,
        'COUNT', 'SCORE_d', _calc_score_by_formula, _choose_dpid_data,
    )


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    # fraction = CHILD_INDEX_SQL_DICT.get('2-5')[0]
    return calc_child_index_type_divide_major(
        DUAN_CADRE_COUNT,
        TOTAL_EVALUATE_COUNT,
        1,
        2,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        _calc_score_by_formula,
        _choose_dpid_data,
        hierarchy_list=[3],
    )


# 分析中心得分
def _stats_analysis_center_assess(months_ago):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    data = df_merge_with_dpid(
        pd_query(ANALYSIS_CENTER_ASSESS_SQL.format(year, month)),
        DEPARTMENT_DATA)
    if data.empty is True:
        return
    # GRADES_TYPE：0扣分1加分
    data['SCORE'] = data.apply(
        lambda row: row['ACTUAL_SCORE'] if row['GRADES_TYPE'] == 1 else (-1 * row['ACTUAL_SCORE']),
        axis=1)
    xdata = data.groupby(['DEPARTMENT_ID'])['SCORE'].sum().reset_index()
    xdata['CONTENT'] = xdata.apply(lambda row: '本月分析中心得分为: {0}'.format(min(round(row['SCORE'], 2)+80, 100)), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 2, 6, 3, months_ago)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 2, 6)
    data.drop(['ACTUAL_SCORE', 'GRADES_TYPE'], inplace=True, axis=1)
    return calc_child_index_type_sum(
        data,
        1,
        2,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(100, (80 + x)),
        _choose_dpid_data,
        hierarchy_list=[3])


def _calc_evaluate_per_person(score_data):
    result_score = 0
    all_score = 0
    other_score = 0
    for item in score_data:
        all_score += item[3]
        if item[2] != 2:
            other_score += item[3]
    all_level = int(all_score) // 2
    other_level = int(other_score) // 2
    if all_level > other_level:
        result_score = 1 + int(math.pow(2, all_level - 1))
    return result_score


def _calc_score_because_zhanduan_evaluate(id_card, row):
    """按照指定规则计算该人员是否存在因站段评价导致扣分
        站段评价中的自动评价也属于路局评级
    A[统计今年初至上月累计评价总计分] -->B(统计当前月路局评价计分)
    G -->H(路局评价分加累计分已升级, 站段评价剔除JL-2是否还有剩余条数, 如果没有则跳过，
    如果有，加上站段评价分算所属档次进行扣分)
    G -->I(路局评价分加累计分不升级，则加上站段评价的分数进行判断是否升级扣分)
    """
    before_score = row['BEFORE_SCORE']
    luju_score = row['LUJU_SCORE']
    add_luju_score = before_score + luju_score
    add_luju_zhanduan_not_jl2_score = add_luju_score + row['ZHANDUAN_SCORE_NOT_JL2']
    add_luju_zhanduan_score = add_luju_score + row['ZHANDUAN_SCORE']
    # 截止到上个月今年累计得分所属档次
    before_level = min(6, int(before_score) // 2)
    # 加上路局评价分后的累计分所属档次
    add_luju_level = min(6, int(add_luju_score) // 2)
    if add_luju_level > before_level:
        # 加上路局评价分后导致档次升级
        if row['ZHANDUAN_SCORE_NOT_JL2'] > 0:
            # 剔除JL-2后还有剩余评价得分
            add_luju_zhanduan_not_jl2_level = min(
                6,
                int(add_luju_zhanduan_not_jl2_score) // 2)
            return [
                id_card, before_score, luju_score, row['ZHANDUAN_SCORE'],
                row['ZHANDUAN_SCORE_NOT_JL2'],
                math.pow(2, add_luju_zhanduan_not_jl2_level - 1)
            ]
    else:
        # 加上路局评价分后未导致档次升级
        add_luju_zhanduan_level = min(6, int(add_luju_zhanduan_score) // 2)
        if add_luju_zhanduan_level > before_level:
            return [
                id_card, before_score, luju_score, row['ZHANDUAN_SCORE'],
                row['ZHANDUAN_SCORE_NOT_JL2'],
                math.pow(2, add_luju_zhanduan_level - 1)
            ]
    return []


def _export_detail_concentartion_ratio_of_evaluation(data, months_ago, mon):
    """导出评价集中度中间过程
    """
    data = pd.merge(
        ZHANDUAN_DPID_DATA,
        data,
        how='inner',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(["DEPARTMENT_ID_x", "DEPARTMENT_ID_y"], inplace=True, axis=1)
    rst_data = []
    data = data.groupby(['TYPE3'])
    for idx, val in data:
        content = '评价集中度分数详情：'
        for each in val.values:
            content += f'<br/>{each[7]}:  {each[6]}' \
                f'<br/>本月前累计分  {each[2]}' \
                f'<br/>本月路局评价分  {each[3]}' \
                f'<br/>本月站段评级分  {each[4]}' \
                f'<br/>本月站段评价分（不含JL-2）  {each[5]}'
        rst_data.append({
            'TYPE': 2,
            'MAIN_TYPE': 2,
            'DETAIL_TYPE': 7,
            'MON': mon,
            'DEPARTMENT_ID': each[8],
            'HIERARCHY': 3,
            'MAJOR': each[1],
            'CONTENT': content,
        })
    write_export_basic_data_to_mongo(rst_data, months_ago, 3, 2, 7)


# 评价集中度
def _stats_concentartion_ratio_of_evaluation(months_ago):
    """评价集中度计：单人比较，因站段评价造成单人处置方式升级的1人+N分。
    （累计积分2、4、6、8、10、12为升级档次）

    Arguments:
        months_ago {[type]} -- [description]
    """
    stats_month = get_custom_month(months_ago)
    this_year, this_mon = int(stats_month[1][:4]), int(stats_month[1][5:7])
    this_month_end_time = stats_month[1]
    this_month_start_time = stats_month[0]
    this_year_start_time = f'{this_year - 1}-12-{this_month_end_time[-2:]}'

    data = pd.concat(
        [
            pd_query(
                ACCUMULATIVE_EVALUATE_SCORE_SQL.format(
                    this_month_start_time, this_year_start_time,
                    this_year)).set_index('ID_CARD'),
            pd_query(
                LUJU_EVALUATE_SCORE_SQL.format(
                    this_month_end_time, this_month_start_time,
                    this_year)).set_index('ID_CARD'),
            pd_query(
                ZHANDUAN_EVALUATE_SCORE_SQL.format(
                    this_month_end_time, this_month_start_time,
                    this_year)).set_index('ID_CARD'),
            pd_query(
                ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL.format(
                    this_month_end_time, this_month_start_time,
                    this_year)).set_index('ID_CARD')
        ],
        axis=1,
        join='outer',
        sort=True)
    data.fillna(0, inplace=True)
    person_score = []
    for idx, row in data.iterrows():
        score = _calc_score_because_zhanduan_evaluate(idx, row)
        if score != []:
            person_score.append(score)
    df_pdata = pd.merge(
        pd.DataFrame(
            data=person_score,
            columns=[
                'ID_CARD', 'BEFORE_SCORE', 'LUJU_SCORE', 'ZHANDUAN_SCORE',
                'ZHANDUAN_SCORE_NOT_JL2', 'SCORE'
            ]),
        pd_query(PERSON_ID_CARD_SQL),
        how='left',
        left_on='ID_CARD',
        right_on='ID_CARD')
    df_pdata.drop(columns=['ID_CARD'], inplace=True)
    df_pdata = df_merge_with_dpid(df_pdata, DEPARTMENT_DATA)
    _export_detail_concentartion_ratio_of_evaluation(
        df_pdata.copy(), months_ago, this_year * 100 + this_mon)
    rst_child_score = calc_child_index_type_sum(
        df_pdata, 1, 2, 7, months_ago, 'SCORE', 'SCORE_g',
        lambda x: min(100, x), _choose_dpid_data)
    return rst_child_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)
    child_index_func = [
        _stats_count_per_person, _stats_score_per_person, _stats_active_ratio,
        _stats_gradation_ratio, _stats_duan_ratio,
        _stats_analysis_center_assess,
        _stats_concentartion_ratio_of_evaluation
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f', 'g']]
    item_weight = [0.4, 0.08, 0.18, 0.07, 0.1, 0.05, 0.12]
    summizet_child_index(child_score, _choose_dpid_data, 1, 2, months_ago,
                         item_name, item_weight, [3])
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd']]
    # item_weight = [0.4, 0.4, 0.2]
    # summizet_child_index(child_score, _choose_dpid_data, 1, 2, months_ago,
    #                      item_name, item_weight, [4])
    update_major_maintype_weight(index_type=0, main_type=2,
                                 child_index_list=[1, 2, 3, 4, 5, 6, 7],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
