# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.health_index.common_sql import (CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
                                              EXTERNAL_PERSON_SQL,
                                              WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.health_index.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_NUMBER_SQL, CHECKED_PERIL_ID_SQL,
    HAPPEN_PROBLEM_POINT_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL, PERIL_COUNT_SQL, PERIL_ID_SQL,
    PERIL_OVERDUE_COUNT_SQL, PERIL_PERIOD_COUNT_SQL,
    PERIL_RECTIFY_NO_ENTRY_SQL, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL,
    CHECK_EVALUATE_SZ_SCORE_SQL, WARNING_DELAY_SQL,
    REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_divide_major,
    calc_child_index_type_sum, calc_extra_child_score_groupby_major_two,
    combine_child_index_func, df_merge_with_dpid, export_basic_data_dicttype,
    export_basic_data_tow_field_monthly, format_export_basic_data,
    get_zhanduan_deparment, summizet_child_index, summizet_operation_set,
    write_export_basic_data_to_mongo, calc_and_rank_index, export_basic_data_tow_field_monthly_two)
from app.data.index.util import (get_custom_month, get_year_month,
                                 validate_exec_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.health_index import problem_ctrl_calc

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


# 部门按站段聚合
def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, \
        STAFF_NUMBER, ZHANDUAN_STAFF, WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_DATA, \
        PROBLEM_POINT_INFO_DATA

    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 正式职工人数
    WORKER_COUNT = pd_query(WORK_LOAD_SQL)
    STAFF_NUMBER = df_merge_with_dpid(WORKER_COUNT, DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    WORK_LOAD = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    PROBLEM_POINT_INFO_DATA = pd_query(PROBLEM_POINT_INFO_SQL)


def _calc_rectification_score(problem_number):
    val = 100 - 0.8 * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


# 超期问题整改
def _stats_rectification_overdue(months_ago):
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    data = df_merge_with_dpid(
        pd_query(
            OVERDUE_PROBLEM_NUMBER_SQL.format(
                year_mon // 100, year_mon % 100)), DEPARTMENT_DATA)
    # 导出中间过程数据
    export_basic_data_dicttype(data, _choose_dpid_data(3), 6, 1, 3, months_ago,
                               lambda x: f'本月问题超期发生条数：{int(x)}条')
    rst_index_score = calc_child_index_type_sum(
        data,
        1,
        6,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_rectification_score,
        _choose_dpid_data,
        NA_value=True)
    return rst_index_score


# # 整改履责指数
# def _stats_check_evaluate(months_ago):
#     calc_month = get_custom_month(months_ago)
#     data = df_merge_with_dpid(
#         pd_query(CHECK_EVALUATE_SZ_SCORE_SQL.format(*calc_month)),
#         DEPARTMENT_DATA)
#     # 导出中间过程数据
#     export_basic_data_dicttype(data, _choose_dpid_data(3), 6, 2, 3, months_ago,
#                                lambda x: f'本月ZG-1、2、3、4、5履职评价发生条数：{int(x)}条')
#     rst_index_score = calc_child_index_type_sum(
#         data,
#         1,
#         6,
#         2,
#         months_ago,
#         'SCORE',
#         'SCORE_b',
#         lambda x: min(round(40 + x, 2), 100),
#         _choose_dpid_data,
#         NA_value=True)
#     return rst_index_score


def _calc_check_evaluate_score(row, code_dict):
    score = 0
    for code in code_dict:
        score += int(row[code]) * int(code_dict[code])
    return min(100, score)


# 整改履责指数
def _stats_check_evaluate(months_ago):
    calc_month = get_custom_month(months_ago)
    check_evaluate_sz_number = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(*calc_month)),
        DEPARTMENT_DATA)

    # 获取每个站段各个code的数量
    # code_list = ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-5"]
    code_dict = {"ZG-1": 30,
                 "ZG-2": 30,
                 "ZG-3": 35,
                 "ZG-4": 20,
                 "ZG-5": 10}
    data = ZHANDUAN_DPID_DATA
    for code in code_dict:
        code_data = check_evaluate_sz_number[check_evaluate_sz_number["CODE"] == code]
        code_data = code_data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
        code_data.rename(columns={"COUNT": code}, inplace=True)
        data = pd.merge(
            data,
            code_data.loc[:, [code, "TYPE3"]],
            how="left",
            left_on="DEPARTMENT_ID",
            right_on="TYPE3")
        data.drop(["TYPE3"], inplace=True, axis=1)
    data["TYPE3"] = data["DEPARTMENT_ID"]
    data.fillna(0, inplace=True)

    data['CONTENT'] = data.apply(
        lambda row: '<br/>'.join([f'本月{col}履职评价发生条数：{int(row[col])}条' for col in code_dict]), axis=1
    )

    data['SCORE'] = data.apply(lambda row: _calc_check_evaluate_score(row, code_dict), axis=1)
    # 中间过程
    data_rst = format_export_basic_data(data, 6, 2, 3, months_ago)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 2)
    # 最终结果
    rst_index_score = calc_child_index_type_sum(
        data,
        1,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        _choose_dpid_data,
        NA_value=True)
    return rst_index_score


def _get_appoint_month_happen_problem(months_ago, hierarchy):
    """获取前第{-months_ago}月的发生问题项点数

    Arguments:
        months_ago {int} -- [description]

    Returns:
        set -- 反复问题
    """
    i_month_data = df_merge_with_dpid(
        pd_query(
            HAPPEN_PROBLEM_POINT_SQL.format(*get_custom_month(months_ago))),
        DEPARTMENT_DATA)
    if i_month_data.empty is True:
        return set()
    i_month_data = i_month_data.groupby(
        [f'TYPE{hierarchy}', 'PK_ID', 'RISK_LEVEL']).size()
    repeatedly_problem = set()
    for idx in i_month_data.index:
        risk_level = idx[2]
        problem_number = int(i_month_data[idx])
        if risk_level == 1 and problem_number < 2:
            continue
        elif risk_level == 2 and problem_number < 5:
            continue
        elif risk_level == 3 and problem_number < 20:
            continue
        else:
            repeatedly_problem.add(f'{idx[0]}||{idx[1]}||{idx[2]}')
    return repeatedly_problem


def _calc_problem_score(risk_level, i_month):
    """根据风险等级和月份进行扣分

    Arguments:
        risk_level {str} -- 风险等级
        i_month {str} -- 前第{-i_month}月

    Returns:
        float/int -- 得分
    """
    _score = {
        '1': 1,
        '2': 0.5,
        '3': 0.2,
    }
    problem_score = _score.get(risk_level, 0) * (4 + int(i_month))
    return problem_score


def _fillna_for_zhanduan(df):
    global ZHANDUAN_DPID_DATA
    all_zhanduan = ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    na_zhanduan = []
    df_zhanduan = df.index.values
    for each in all_zhanduan:
        if each not in df_zhanduan:
            na_zhanduan.append(each)
    df_na_zhanduan = pd.DataFrame(
        index=na_zhanduan, data=[0 for x in na_zhanduan], columns=['SCORE'])
    return pd.concat([df, df_na_zhanduan], sort=False)


def _calc_repeatedly_index(months_ago, hierarchy):
    repeatedly_problem = []
    base_repeatedly_problem = _get_appoint_month_happen_problem(
        months_ago, hierarchy)
    for i_month in range(-1, -4, -1):
        i_month_repeatedly_problem = _get_appoint_month_happen_problem(
            months_ago + i_month, hierarchy)
        # 获取每个月反复的问题项点
        common_problem = base_repeatedly_problem.intersection(
            i_month_repeatedly_problem)
        repeatedly_problem.extend(
            [[x.split('||')[0], i_month,
              x.split('||')[2]] for x in common_problem])
    if repeatedly_problem == []:
        return []
    # 导出中间过程数据
    first_title = {-1: '前一个月', -2: '前二个月', -3: '前三个月'}
    second_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_tow_field_monthly(repeatedly_problem, DEPARTMENT_DATA,
                                        _choose_dpid_data(3), 6, 3, 3,
                                        months_ago, first_title, second_title)
    df_prob = pd.DataFrame(
        data=repeatedly_problem, columns=['DEP_DPID', 'I_MONTH', 'RISK_LEVEL'])
    df_prob['SCORE'] = df_prob.apply(
        lambda row: _calc_problem_score(row['RISK_LEVEL'], row['I_MONTH']),
        axis=1)
    # 将站段分组求和
    df_prob = df_prob.groupby(['DEP_DPID'])['SCORE'].sum()
    df_prob = _fillna_for_zhanduan(df_prob.to_frame(name='SCORE'))
    column = f'SCORE_c_{hierarchy}'
    df_prob[column] = df_prob['SCORE'].apply(
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2))

    # 计算排名、入库
    summizet_operation_set(df_prob, _choose_dpid_data(hierarchy), column,
                           hierarchy, 1, 6, 3, months_ago)
    return df_prob[[column]]


# 问题控制
def _stats_repeatedly_index(months_ago):
    """以站段为统计单位同一问题项点是否反复发生（超过阀门值的才算重复发生），发生的每项扣a分。
    严重风险的问题基础阀门值为2次，较大风险的基础阀门值5次，一般风险的基础阀门值20次，
    低风险问题不计算

    Arguments:
        months_ago {int} --
    """
    # rst_index_score = []
    # for hierarchy in HIERARCHY:
    #     rst_score = _calc_repeatedly_index(months_ago, hierarchy)
    #     if len(rst_score) != 0:
    #         rst_index_score.append(rst_score)
    # return rst_index_score
    return problem_ctrl_calc.stats_repeatedly_index(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA)


# 整改复查
def _stats_rectification_review(months_ago):
    """库内问题复查数/总人数，与专业基数比较
    """
    year_mon, last_month = get_year_month(months_ago)
    return calc_child_index_type_divide_major(
        df_merge_with_dpid(
            pd_query(
                IMPORTANT_PROBLEM_RECHECK_COUNT_SQL.format(
                    year_mon // 100, year_mon % 100)), DEPARTMENT_DATA),
        pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0,
                  sort=False), 1, 6, 4, months_ago, 'COUNT', 'SCORE_d',
        _calc_score_by_formula, _choose_dpid_data)


def _peril_count_score_formula(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.5:
        _score = 100
    elif _ratio >= 0:
        _score = 70
    elif _ratio > -0.5:
        _score = 50
    elif _ratio > -0.6:
        _score = 30
    else:
        _score = 0
    return _score


# 隐患整治 - 隐患库
def _peril_count_score(hierarchy):
    """隐患库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    peril_count = df_merge_with_dpid(
        pd_query(PERIL_COUNT_SQL), DEPARTMENT_DATA)
    peril_count = peril_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # data = pd.concat(
    #     [peril_count.to_frame(name='peril'), WORK_LOAD], axis=1, sort=False)
    data = pd.merge(peril_count.to_frame(name='peril'), WORK_LOAD,
                    how='right', right_index=True, left_index=True)

    # 补全站段/剔除worl_load中多余的站段(西昌公务段等)
    data = pd.merge(
        ZHANDUAN_DPID_DATA, data, how='left', left_on='DEPARTMENT_ID', right_index=True)
    data.set_index(data['DEPARTMENT_ID'], inplace=True)
    data.drop(columns=['DEPARTMENT_ID', 'NAME', "MAJOR"], inplace=True)

    data.fillna(0, inplace=True)
    data['ratio'] = data['peril'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major_two(
        data, _choose_dpid_data(hierarchy), 'ratio',
        _peril_count_score_formula,
        numerator='peril',
        denominator='PERSON_NUMBER')
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: '隐患库(40%):<br/>单位级隐患数量({0})/单位总人数({1})'.format(
            f'{int(row["peril"])}', int(row['PERSON_NUMBER'])),
        axis=1)
    data.drop(
        columns=['peril', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 隐患整治 - 隐患整治延期
def _peril_overdue(hierarchy, peril_count_score):
    """隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    pass
    peril_overdue_data = df_merge_with_dpid(
        pd_query(PERIL_OVERDUE_COUNT_SQL), DEPARTMENT_DATA)
    peril_period_data = df_merge_with_dpid(
        pd_query(PERIL_PERIOD_COUNT_SQL), DEPARTMENT_DATA)
    peril_overdue_count = peril_overdue_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    peril_period_count = peril_period_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [
            peril_overdue_count.to_frame(name='COUNT_OVERDUE'),
            peril_period_count.to_frame(name='COUNT_PERIL')
        ],
        axis=1,
        sort=False)

    # 补全站段数据
    data = pd.merge(
        data,
        ZHANDUAN_DPID_DATA,
        how="right",
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index(data['DEPARTMENT_ID'], inplace=True)
    data.drop(columns=['DEPARTMENT_ID', 'NAME', "MAJOR"], inplace=True)
    data.fillna(0, inplace=True)

    series_rst = data.apply(lambda row: max(0, 100 - sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '隐患整治延期（20%）:<br/>延期数量：({0})/转为长期整治数量：({1})'.format(
            int(row['COUNT_OVERDUE']), int(row['COUNT_PERIL'])),
        axis=1)
    data.drop(columns=['COUNT_OVERDUE', 'COUNT_PERIL'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治督促
def _peril_urge(months_ago, hierarchy, peril_count_score):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    calc_month = get_custom_month(months_ago)
    # 未检查的
    checked_peril_id = pd_query(CHECKED_PERIL_ID_SQL.format(*calc_month))
    peril_id = pd_query(PERIL_ID_SQL)
    peril_id = peril_id.append(checked_peril_id, sort=False)
    peril_id.drop_duplicates(subset=['FK_DEPARTMENT_ID', 'PID'], keep=False, inplace=True)
    peril_id = df_merge_with_dpid(peril_id, DEPARTMENT_DATA)

    # 长期整治的没录入
    peril_rectify_no_entry = df_merge_with_dpid(pd_query(
        PERIL_RECTIFY_NO_ENTRY_SQL.format(*calc_month)), DEPARTMENT_DATA)

    data = pd.concat(
        [
            peril_id.groupby(['TYPE3'])['PID'].size(),
            peril_rectify_no_entry.groupby(['TYPE3'
                                            ])['COUNT'].sum()
        ],
        axis=1,
        sort=False)
    # 补全站段数据
    data = pd.merge(
        data,
        ZHANDUAN_DPID_DATA,
        how="right",
        left_index=True,
        right_on='DEPARTMENT_ID',
    )
    data.set_index(data['DEPARTMENT_ID'], inplace=True)
    data.drop(columns=['DEPARTMENT_ID', 'NAME', "MAJOR"], inplace=True)
    data.fillna(0, inplace=True)
    # 计算分数
    series_rst = data.apply(lambda row: min(100, sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)

    data['middle_3'] = data.apply(
        lambda row: '隐患整治督促（40%）:<br/>未检查的个数：({0})/长期整治没录入({1})'.format(int(row['PID']), int(row['COUNT'])), axis=1)
    data.drop(columns=['COUNT', 'PID'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治
def _stats_peril_renovation(months_ago):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        # 隐患库
        peril_count_score, peril_count_score_basic_data = _peril_count_score(
            hierarchy)
        score.append(peril_count_score * 0.4)
        calc_basic_data.append(peril_count_score_basic_data)

        # 隐患整治延期
        peril_overdue_socre, peril_overdue_score_basic_data = _peril_overdue(
            hierarchy, peril_count_score)
        score.append(peril_overdue_socre * 0.2)
        calc_basic_data.append(peril_overdue_score_basic_data)

        # 隐患整治督促
        peril_urge_score, peril_urge_score_basic_data = _peril_urge(months_ago, hierarchy, peril_count_score)
        score.append(peril_urge_score * 0.4)
        calc_basic_data.append(peril_urge_score_basic_data)

        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 5, 3,
                                                       months_ago)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
                                         5)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_e_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(df_rst, _choose_dpid_data(hierarchy), column,
                               hierarchy, 1, 6, 5, months_ago)
        rst_child_score.append(df_rst)
    return rst_child_score


def export_recification_effect(data, main_type, detail_type, months_ago, _calc_score):
    """导出各类事故的次数

    Arguments:
        data {pandas.DataFrame} --
    """
    # 保存中间过程数据
    monthly_basic_data = []
    for idx, row in data.iterrows():
        resp_level = row['RESPONSIBILITY_IDENTIFIED']
        if resp_level in [1, 2, 3, 8]:
            resp_level = 1
        elif resp_level in [4, 5, 9]:
            resp_level = 2
        else:
            resp_level = 3
        monthly_basic_data.append(
            [row['FK_DEPARTMENT_ID'], row['MAIN_TYPE'], resp_level])
    first_title = {1: '事故', 2: '故障', 3: '综合信息'}
    second_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    return export_basic_data_tow_field_monthly_two(monthly_basic_data, DEPARTMENT_DATA,
                                                   _choose_dpid_data(3), 6, 6, 3,
                                                   months_ago, first_title, second_title)


def export_waring_effect(data, hierarchy):
    """导出警告预警次数"""
    data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # 补全站段数据
    data = pd.merge(
        data.to_frame(),
        ZHANDUAN_DPID_DATA["DEPARTMENT_ID"].to_frame(),
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index('DEPARTMENT_ID', inplace=True)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '警告性预警延期次数: {0}'.format(
            f'{int(row["COUNT"])}'),
        axis=1)
    data.pop("COUNT")
    return data


def _calc_score(row):
    """整改成效的记分规则
    """
    base_score = [10, 2, 1]
    step_score = [5, 2, 1]
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    # 主要
    if resp_level in [1, 2, 3, 8]:
        resp_level = 2
    # 重要
    elif resp_level in [4, 5, 9]:
        resp_level = 1
    # 次要
    else:
        resp_level = 0
    main_type = row['MAIN_TYPE'] - 1
    return base_score[main_type] + (step_score[main_type] * resp_level)


# 整改成效
def _stats_recification_effect(months_ago):
    """警告性预警延期一次扣10分(暂未加入)；责任事故主要、全部责任的1个扣20分、重要扣15分、
    次要的扣10分（含追究责任）；
    D21等同故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分；
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。直接在总分中扣，
    改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    """
    # calc_month = get_custom_month(months_ago)
    # responsibe_info = pd_query(RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(*calc_month))
    # warning_delay = pd_query(WARNING_DELAY_SQL.format(*calc_month))
    # # if data.empty:
    # #     return None
    # # basic_data
    # responsibe_data = export_recification_effect(responsibe_info, 6, 6, months_ago, _calc_score)
    # warning_data = export_waring_effect(warning_delay, 3)
    # calc_df_data = pd.concat([responsibe_data, warning_data], axis=1, sort=False)
    # columns = calc_df_data.columns.tolist()
    # calc_df_data['CONTENT'] = calc_df_data.apply(
    #     lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    # calc_df_data= append_major_column_to_df(
    #     ZHANDUAN_DPID_DATA,
    #     pd.DataFrame(
    #         index=calc_df_data.index,
    #         data=calc_df_data.loc[:, 'CONTENT'].values,
    #         columns=['CONTENT']))
    #
    # calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 6, 3,
    #                                                months_ago)
    # write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
    #                                  6)
    #
    # # 责任分
    # responsibe_info['SCORE_1'] = responsibe_info.apply(lambda row: _calc_score(row), axis=1)
    # responsibe_info.drop(['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
    #
    # # 警告分
    # warning_delay['SCORE_2'] = warning_delay.apply(lambda row: int(row['COUNT']*10), axis=1)
    # warning_delay.drop(['COUNT'], inplace=True, axis=1)
    # data = pd.merge(
    #     warning_delay,
    #     responsibe_info,
    #     how='outer',
    #     left_on="FK_DEPARTMENT_ID",
    #     right_on="FK_DEPARTMENT_ID",
    # )
    # data.fillna(0, inplace=True)
    # data['SCORE'] = data.apply(lambda row: round(row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    # data.drop(['SCORE_1', 'SCORE_2'], inplace=True, axis=1)
    #
    # data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    # rst_child_score = calc_child_index_type_sum(
    #     data,
    #     1,
    #     6,
    #     6,
    #     months_ago,
    #     'SCORE',
    #     'SCORE_f',
    #     lambda x: 40 if x > 40 else x,
    #     _choose_dpid_data,
    #     NA_value=True)
    calc_month = get_custom_month(months_ago)
    responsibe_info = pd_query(RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(*calc_month))
    warning_delay = pd_query(WARNING_DELAY_SQL.format(*calc_month))
    responsibe_data = export_recification_effect(responsibe_info, 6, 6, months_ago, _calc_score)
    warning_data = export_waring_effect(warning_delay, 3)
    calc_df_data = pd.concat([responsibe_data, warning_data], axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        ZHANDUAN_DPID_DATA,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 6, 3,
                                                   months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
                                     6)

    # # 责任分
    # responsibe_info['SCORE_1'] = responsibe_info.apply(lambda row: _calc_score(row), axis=1)
    # responsibe_info.drop(['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
    #
    # # 警告分
    # warning_delay['SCORE_2'] = warning_delay.apply(lambda row: int(row['COUNT'] * 10), axis=1)
    # warning_delay.drop(['COUNT'], inplace=True, axis=1)
    # data = pd.merge(
    #     warning_delay,
    #     responsibe_info,
    #     how='outer',
    #     left_on="FK_DEPARTMENT_ID",
    #     right_on="FK_DEPARTMENT_ID",
    # )
    # data.fillna(0, inplace=True)
    # data['SCORE'] = data.apply(lambda row: round(row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    # data.drop(['SCORE_1', 'SCORE_2'], inplace=True, axis=1)
    #
    # data = df_merge_with_dpid(data, department_data)
    #

    # 责任分
    data = DEPARTMENT_DATA
    if responsibe_info.empty:
        data["SCORE_1"] = 0
    else:
        responsibe_info['SCORE_1'] = responsibe_info.apply(lambda row: _calc_score(row), axis=1)
        responsibe_info.set_index("FK_DEPARTMENT_ID", inplace=True)
        responsibe_info.drop(['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
        data = pd.merge(
            responsibe_info,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    # 警告分
    if warning_delay.empty:
        data["SCORE_2"] = 0
    else:
        warning_delay['SCORE_2'] = warning_delay.apply(lambda row: int(row['COUNT'] * 10), axis=1)
        warning_delay.drop(['COUNT'], inplace=True, axis=1)
        warning_delay.set_index("FK_DEPARTMENT_ID", inplace=True)
        data = pd.merge(
            warning_delay,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    data.fillna(0, inplace=True)
    data['SCORE'] = data.apply(lambda row: round(row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    data.drop(["SCORE_1", "SCORE_2"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        1,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: 40 if x > 40 else x,
        _choose_dpid_data,
        NA_value=True)
    return rst_child_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_review,
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index,
        _stats_peril_renovation,
        _stats_recification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f']]
    item_weight = [0.1, 0.2, 0.3, 0.15, 0.25, -1]
    update_major_maintype_weight(index_type=0, main_type=6,
                                 child_index_list=[1, 2, 3, 4, 5, 6],
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, 1, 6, months_ago,
                         item_name, item_weight)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
