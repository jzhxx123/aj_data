#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""

HIERARCHY = 3


class MainType:
    """
    检查力度指数	check_intensity
    评估力度指数	evaluate_intensity
    考核力度指数	assess_intensity
    检查均衡度	    check_evenness
    问题暴露度	    problem_exposure
    问题整改	    problem_rectification
    安全生产效果指数        safety_produce_effect
    """
    check_intensity = 1
    evaluate_intensity = 2
    assess_intensity = 3
    check_evenness = 4
    problem_exposure = 5
    problem_rectification = 6
    safety_produce_effect = 7


class CheckIntensityDetailType:
    """
    检查力度指数子指数
    stats_check_per_person: 换算单位人均检查频次
    stats_check_problem_ratio: 问题查处力度
    stats_check_problem_assess_ratio: 查处问题考核率
    stats_score_per_person: 换算问题平均质量分
    stats_risk_score_per_person: 较大和重大安全风险问题质量分累计
    stats_yecha_ratio: 夜查率
    stats_check_address_ratio: 覆盖率
    stats_media_intensity: 监控调阅力度
    """
    stats_check_per_person = 2
    stats_check_problem_ratio = 3
    stats_check_problem_assess_ratio = 4
    stats_score_per_person = 5
    stats_risk_score_per_person = 6
    stats_yecha_ratio = 7
    stats_great_risk_problem_ratio = 8
    stats_check_address_ratio = 9
    stats_media_intensity = 10


class EvaluateIntensityDetailType:
    pass


class CheckEvennessDetailType:
    """
    检查均衡度子指数
    stats_problem_point_evenness: 检查问题均衡度
    stats_check_time_evenness: 检查时间均衡度
    stats_check_day_evenness： 检查日期均衡度
    stats_check_address_evenness： 地点均衡度
    stats_check_hour_evenness： 检查时段均衡度
    """
    stats_problem_point_evenness = 1
    stats_check_time_evenness = 2
    stats_check_address_evenness = 3


class ProblemExposureDetailType:
    """
    问题暴露度子指数
    stats_total_problem_exposure： 普遍性暴露
    stats_hidden_problem_exposure: 较严重隐患暴露
    stats_banzu_problem_exposure： 班组问题暴露
    stats_problem_exposure: 事故隐患问题暴露度
    other_problem_exposure: 他查问题暴露
    """
    stats_total_problem_exposure = 1
    stats_hidden_problem_exposure = 2
    stats_problem_exposure = 3
    stats_banzu_problem_exposure = 4
    other_problem_exposure = 5


class AssessIntensityDetailType:
    """
    考核力度子指数
    stats_check_problem_assess_ratio： 人均考核问题数
    stats_assess_money_per_person： 人均考核金额
    stats_award_return_ratio： 返奖率
    stats_problem_assess_ratio: 考核率
    """
    stats_check_problem_assess_ratio = 1
    stats_assess_money_per_person = 2
    stats_award_return_ratio = 3
    stats_problem_assess_ratio = 4


class ProblemRectificationDetailType:
    """
    stats_rectification_overdue: 整改时效（超期整改）
    stats_check_evaluate：履职整改
    stats_repeatedly_index： 问题控制
    stats_rectification_review： 整改复查
    stats_rectification_effect： 整改成效
    stats_peril_renovation：隐患整治
    """
    stats_rectification_overdue = 1
    stats_check_evaluate = 2
    stats_repeatedly_index = 3
    stats_rectification_review = 4
    stats_peril_renovation = 5
    stats_rectification_effect = 6


class SafetyProduceEffectDetailType:
    """
    安全生产效果指数
    stats_locomotive_malfunction_deduct: 机车故障扣分计算
    """
    stats_locomotive_malfunction_deduct = 2


class CommonCalcDataType:
    """
    通用的计算的main, detail的数据项类别
    初始通用版本，实际各专业各指数可以改变解释含义，
    版本更迭也是遵循各专业各指数的延续性
    version:版本更迭约定同类（以本模块的列出来的数据为类）数据，更改风险等级，
    关联风险或者项目等，则实例化，修改版本，每个指数版本前后留有档案，记得更迭次数内容
    """

    def __init__(self, name, description, version, func_version):
        self.name = name
        self.description = description
        self.version = version
        self.func_version = func_version


class IndexDivider:
    """
    指数计算配置类
    """

    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator



PERSON_LOAD_INFO = ('PersonLoad', '干部职工总数（职工总数+外聘）', 'v1', 'double_df')
CADRE_COUNT_INFO = ('CadreCount', '干部总数（干部人数）', 'v1', 'single_df')
WORKER_COUNT_INFO = ('WorkerCount', '作业人数（非干部职工+外聘）', 'v1', 'double_df')
WORKER_LOAD_INFO = ('WorkerLoad', '工作量', 'v1', 'double_df')
CHECK_COUNT_INFO = ('CheckCount', '现场检查总数', 'v1', 'single_df')
PROBLEM_SCORE_INFO = ('ProblemScore', '检查出问题质量分', 'v1', 'single_df')
YECHA_COUNT_INFO = ('YeChaCount', '现场夜查次数', 'v1', 'single_df')
STAFF_NUMBER_INFO = ('StaffNumber', '职工总数', 'v1', 'single_df')
MEDIA_COST_TIME_INFO = ('MedialCostTime', '监控调阅时长', 'v1', 'single_df')
MEDIA_PROBLEM_NUMBER_INFO = (
    'MediaProblemsNumber', '监控调阅发现问题数', 'v1', 'single_df')
MEDIA_PROBLME_SCORE_INFO = (
    'MediaProblemsScore', '监控调阅质量分', 'v1', 'single_df')
ALL_PROBLEM_NUMBER_INFO = (
    'AllProblemsNumber', '所有问题数', 'v1', 'single_df')
AWARD_RETURN_MONEY_PROBLEM_INFO =  (
    'AwardReturnMoneyProblem', '返奖问题个数(A, B, E1, E2)', 'v1', 'assess_df')
REAL_AWARD_RETURN_MONEY_PROBLEM_INFO =  (
    'RealAwardReturnMoneyProblem', '实际返奖问题个数(A, B, E1, E2)', 'v1', 'assess_df')
EVALUATE_COUNT_INFO = (
    'EvaluateCount', '评价记分总条数(干部)', 'v1', 'single_df')
ACTIVE_EVALUATE_COUNT_INFO = (
    'ActiveEvaluateCount', '主动评价记分总条数（干部）', 'v1', 'single_df')
ACTIVE_EVALUATE_SCORE_INFO = (
    'ActiveEvaluateScore', '主动评价记分总分数(干部)', 'v1', 'single_df')