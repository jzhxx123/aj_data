# -*- coding: utf-8 -*-
"""安全效果指数
    date: 2018/12/19
    author: 何强胜
"""

from flask import current_app

from app.data.health_index.common_sql import (CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
                                              ZHANDUAN_DPID_SQL)
from app.data.health_index.respon_safety_produce_sql import \
    RESPONSIBE_SAFETY_PRODUCE_INFO_SQL
from app.data.index.common import (calc_child_index_type_sum,
                                   combine_child_index_func,
                                   df_merge_with_dpid, summizet_child_index)
from app.data.index.util import get_custom_month, validate_exec_month
from app.data.util import pd_query

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data():
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)


def _calc_respon_score(row):
    """加权计算分数

    Arguments:
        row {[type]} -- 扣分数据项

    Returns:
        float  -- 扣分
    """
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    if resp_level in [1, 2, 3, 8]:
        resp_level == 1
    elif resp_level in [4, 5, 9]:
        resp_level = 0.6
    else:
        resp_level = 0.3
    score = row['PONDERANCE_NUMBER'] * 0.1 * resp_level
    return score


def _stats_respon_safety_produce(months_ago):
    """扣责任生产信息严重性值*0.1*责任系数。电务故障另算，
    责任系数：全部主要1，重要0.6，次要0.3

    Arguments:
        months_ago {int} -- 前第-N个月
    """
    calc_month = get_custom_month(months_ago)
    data = pd_query(RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(*calc_month))
    # 计算每条记录最终扣分
    if data.empty:
        return []
    data['SCORE'] = data.apply(lambda row: _calc_respon_score(row), axis=1)
    data.drop(
        ['RESPONSIBILITY_IDENTIFIED', 'PONDERANCE_NUMBER'],
        inplace=True,
        axis=1)
    rst_child_score = calc_child_index_type_sum(
        df_merge_with_dpid(data,
                           DEPARTMENT_DATA), 1, 7, 1, months_ago, 'SCORE',
        'SCORE_a', lambda x: 0 if x < 0 else round(x, 2), _choose_dpid_data)
    return rst_child_score


def handle(months_ago):
    _get_sql_data()
    child_index_func = [_stats_respon_safety_produce]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    summizet_child_index(child_score, _choose_dpid_data, 1, 7, months_ago,
                         item_name, item_weight)
    current_app.logger.debug(
        '├── └── respon_safety_produce index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == "__main__":
    pass
