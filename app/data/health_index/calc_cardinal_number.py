# -*- coding: utf-8 -*-
"""
综合指数，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)
from app.data.major_risk_index.cw_keyun_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.cw_keyun_laoan import GLV
import pandas as pd
from app.data.util import (
    pd_query, get_coll_prefix, get_history_months)
from app.data.health_index.cardinal_number_common import (
    calc_cardinal_number, COMMON_INDEX_ORGANIZATION)
from app.data.health_index.common_sql import (
    CADRE_COUNT_SQL, XIANCHANG_CHECK_COUNT_SQL,
    PROBLEM_CHECK_SCORE_SQL, CHECK_PROBLEM_SQL, BASE_UNIT_INFO_SQL, 
    WORK_LOAD_SQL, JIAODA_RISK_LEVEL_PROBLEM_SQL)
from app.data.health_index.check_intensity_sql import (
    MEDIA_COST_TIME_SQL, YECHA_CHECK_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
     MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST)
from app.data.health_index.assess_intensity_sql import (
    AWARD_RETURN_PROBLEM_SQL, REAL_AWARD_RETURN_PROBLEM_SQL
)
from app.data.health_index.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, ANALYSIS_CENTER_ASSESS_SQL,
    DUAN_CADRE_COUNT_SQL, EVALUATE_COUNT_SQL, ACCUMULATIVE_EVALUATE_SCORE_SQL,
    LUJU_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
    ZHANDUAN_EVALUATE_SCORE_SQL, PERSON_ID_CARD_SQL)
from app.data.health_index.const import (
    CHECK_COUNT_INFO, PERSON_LOAD_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    CADRE_COUNT_INFO, WORKER_COUNT_INFO,
    YECHA_COUNT_INFO,MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    ALL_PROBLEM_NUMBER_INFO,
    AWARD_RETURN_MONEY_PROBLEM_INFO, 
    EVALUATE_COUNT_INFO, ACTIVE_EVALUATE_COUNT_INFO, 
    ACTIVE_EVALUATE_SCORE_INFO,
    REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = '综合-0'
    zhanduan_dpid_data = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


_ALL_ZHANDUAN_DPID_SQL = """
        SELECT 
        a.DEPARTMENT_ID,
        a.NAME,
        (CASE
            WHEN a.TYPE3 IN {0} THEN '工电'
            WHEN a.TYPE3 IN {1} THEN '客运'
            ELSE b.NAME
        END) AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4 AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ''
        """

_ZHANDUAN_DPID_SQL = """
SELECT 
    a.DEPARTMENT_ID,
    a.NAME,
    (CASE
        WHEN a.TYPE3 IN {1} THEN '工电'
        WHEN a.TYPE3 IN {2} THEN '客运'
        ELSE b.NAME
    END) AS MAJOR
FROM
    t_department AS a
        LEFT JOIN
    t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
WHERE
    a.TYPE = 4 AND a.IS_DELETE = 0
        AND b.TYPE2 = '{0}'
        AND a.SHORT_NAME != ''
"""

_DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

_SPECIAL_DEPARTMENT_SQL = """
        SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.TYPE3 IN {0}
            AND b.SHORT_NAME != ''
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.is_delete = 0
"""

def _get_vitual_major_ids(major):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    FK_PROFESSION_DICTIONARY_ID = {"工电": 2140}
    BELONG_PROFESSION_ID = {"客运": 898}
    if major == '工电':
        profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(major, 2140)
        GET_VM_MAJORS_IDS_SQL = """
            SELECT 
            DEPARTMENT_ID
        FROM
            t_department
        WHERE
            FK_PROFESSION_DICTIONARY_ID in ({0})
        """
        major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    else:
        BELONG_PROFESSION_ID = {"客运": 898}
        profession_dictionary_id = BELONG_PROFESSION_ID.get(major, 898)
        GET_VM_MAJORS_IDS_SQL = """
            SELECT 
            DEPARTMENT_ID
        FROM
            t_department
        WHERE
            TYPE = 4
            AND 
            SHORT_NAME != ""
            AND 
            BELONG_PROFESSION_ID in ({0})
        """
        major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _get_calc_month(months_ago):
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    calc_month = end, start
    return calc_month

# ------------------------获取比值型相应基数------------------------ #

def check_intensity_cardinal_number(
    months_ago):
    """[summary]
    检查力度基数选择
    Arguments:
        months_ago {[type]} -- [description]
        risk_name {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    IndexDetails.months_ago = months_ago

    # 问题质量分
    PROBLEM_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_SCORE.value = [PROBLEM_CHECK_SCORE_SQL]

    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.value = [CHECK_PROBLEM_SQL]

    # 正式职工人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL)
    # 总人数
    PERSON_LOAD = CommonCalcDataType(*PERSON_LOAD_INFO)
    PERSON_LOAD.value = [STAFF_NUMBER, EXTERNAL_PERSON_SQL]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value =[MEDIA_COST_TIME_SQL]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.value = [MEDIA_PROBLEM_NUMBER_SQL]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.value = [MEDIA_PROBLME_SCORE_SQL]

    # 较严重及以上风险问题数
    JIAO_RISK_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    JIAO_RISK_PROBLEM_NUMBER.version = 'v2'
    JIAO_RISK_PROBLEM_NUMBER.description = '较严重及以上风险问题数'
    JIAO_RISK_PROBLEM_NUMBER.value = [JIAODA_RISK_LEVEL_PROBLEM_SQL]

    # 夜查次数
    YECHA_COUNT = CommonCalcDataType(*YECHA_COUNT_INFO)
    YECHA_COUNT.value = [YECHA_CHECK_SQL]

    # 检查总次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.value = [XIANCHANG_CHECK_COUNT_SQL]
    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 问题平均质量分
        '1-5': (IndexDetails(PROBLEM_SCORE, ALL_PROBLEM_NUMBER),),
        # 人均质量分
        '1-6': (IndexDetails(PROBLEM_SCORE, PERSON_LOAD),),
        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, PERSON_LOAD),
                IndexDetails(MEDIA_PROBLEM_NUMBER, PERSON_LOAD),
                IndexDetails(MEDIA_PROBLME_SCORE, PERSON_LOAD)),
        # 换算较严重及以上风险问题
        '1-8': (IndexDetails(JIAO_RISK_PROBLEM_NUMBER, PERSON_LOAD),),

        # 夜查率
        '1-7': (IndexDetails(YECHA_COUNT, CHECK_COUNT),),

    }
    # IndexDetails.zhanduan_dpid_data = pd_query(_ALL_ZHANDUAN_DPID_SQL.format(
    #     ('99990002001499A10010','99990002001499A10012',
    #     '99990002001499A10013','99990002001499A10015'),
    #      ('19B8C3534E055665E0539106C00A58FD','19B8C3534E1E5665E0539106C00A58FD',
    #      '19B8C3534E205665E0539106C00A58FD','19B8C3534E045665E0539106C00A58FD',
    #      '19B8C3534E125665E0539106C00A58FD','19B8C3534E3B5665E0539106C00A58FD')))
    cardinal_number_execute(months_ago, CHILD_INDEX_SQL_DICT)
    return CHILD_INDEX_SQL_DICT


def assess_intensity_cardinal_number(months_ago):
    """[summary]
    检查力度基数选择
    Arguments:
        months_ago {[type]} -- [description]
        risk_name {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    IndexDetails.months_ago = months_ago

    # A, B, E1, E2问题
    AWARD_RETURN_MONEY_PROBLEM_ABE1E2 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_PROBLEM_ABE1E2.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('A','B','E1','E2'))]
    # A, B, E1, E2问题返奖数
    AWARD_RETURN_MONEY_ABE1E2 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_ABE1E2.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('A','B','E1','E2'))]

    # C, E3问题
    AWARD_RETURN_MONEY_PROBLEM_CE3 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_PROBLEM_CE3.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('C','E3'))]
    AWARD_RETURN_MONEY_PROBLEM_CE3.version = 'v2'
    AWARD_RETURN_MONEY_PROBLEM_CE3.description = '返奖问题个数(C, E3)'
    # C, E3问题返奖数
    AWARD_RETURN_MONEY_CE3 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_CE3.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('C','E3'))]
    AWARD_RETURN_MONEY_CE3.version = 'v2'
    AWARD_RETURN_MONEY_CE3.description = '实际返奖问题个数(C, E3)'

    # D, E4问题
    AWARD_RETURN_MONEY_PROBLEM_DE4 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_PROBLEM_DE4.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('D','E4'))]
    AWARD_RETURN_MONEY_PROBLEM_DE4.version = 'v3'
    AWARD_RETURN_MONEY_PROBLEM_DE4.description = '返奖问题个数(D, E4)'
    # D, E4问题返奖数
    AWARD_RETURN_MONEY_DE4 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_DE4.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('D','E4'))]
    AWARD_RETURN_MONEY_DE4.version = 'v3'
    AWARD_RETURN_MONEY_DE4.description = '实际返奖问题个数(D, E4)'

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 返奖率
        '3-3': (IndexDetails(AWARD_RETURN_MONEY_ABE1E2,AWARD_RETURN_MONEY_PROBLEM_ABE1E2),
                IndexDetails(AWARD_RETURN_MONEY_CE3, AWARD_RETURN_MONEY_PROBLEM_CE3),
                IndexDetails(AWARD_RETURN_MONEY_DE4,AWARD_RETURN_MONEY_PROBLEM_DE4)),

    }

    cardinal_number_execute(months_ago, CHILD_INDEX_SQL_DICT)
    
    return CHILD_INDEX_SQL_DICT


def evaluate_intensity_cardinal_number(months_ago):
    IndexDetails.months_ago = months_ago

    # 主动评价积分条数
    ACTIVE_EVALUATE_COUNT = CommonCalcDataType(*ACTIVE_EVALUATE_COUNT_INFO)
    ACTIVE_EVALUATE_COUNT.value = [ACTIVE_EVALUATE_COUNT_SQL]
    # 评价记分总条数
    EVALUATE_COUNT = CommonCalcDataType(*EVALUATE_COUNT_INFO)
    EVALUATE_COUNT.value = [EVALUATE_COUNT_SQL]
    # （主动）科职干部评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = CommonCalcDataType(*ACTIVE_EVALUATE_COUNT_INFO)
    ACTIVE_EVALUATE_KEZHI_COUNT.value = [ACTIVE_KEZHI_EVALUATE_COUNT_SQL]
    ACTIVE_EVALUATE_KEZHI_COUNT.version = 'v2'
    ACTIVE_EVALUATE_KEZHI_COUNT.description = '主动科职干部评价记分条数'
    # （主动）段机关干部评价记分条数
    DUAN_CADRE_EVALUATE_COUNT = CommonCalcDataType(*ACTIVE_EVALUATE_COUNT_INFO)
    DUAN_CADRE_EVALUATE_COUNT.value = [DUAN_CADRE_COUNT_SQL]
    DUAN_CADRE_EVALUATE_COUNT.version = 'v3'
    DUAN_CADRE_EVALUATE_COUNT.description = '主动段机关干部评价记分条数'
    # 干部总人数
    CADRE_COUNT = CommonCalcDataType(*CADRE_COUNT_INFO)
    CADRE_COUNT.value = [CADRE_COUNT_SQL]
    # 主动评价记分分数
    ACTIVE_EVALUATE_SCORE = CommonCalcDataType(*ACTIVE_EVALUATE_SCORE_INFO)
    ACTIVE_EVALUATE_SCORE.value = [ACTIVE_EVALUATE_SCORE_SQL]

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 主动评价记分占比
        '2-1': (IndexDetails(ACTIVE_EVALUATE_COUNT, EVALUATE_COUNT),),

        # 干部人均主动评价记分分数
        '2-3': (IndexDetails(ACTIVE_EVALUATE_SCORE, CADRE_COUNT),),

        # 评价职务占比
        '2-4': (IndexDetails(ACTIVE_EVALUATE_KEZHI_COUNT, EVALUATE_COUNT),),

        # 段机关干部占比
        '2-5': (IndexDetails(DUAN_CADRE_EVALUATE_COUNT, EVALUATE_COUNT),),

    }
    cardinal_number_execute(months_ago, CHILD_INDEX_SQL_DICT)
    
    return CHILD_INDEX_SQL_DICT


def cardinal_number_execute(months_ago, child_index_sql_dict):
    from app.data.major_risk_index.common.common import get_major_dpid
    gongdian_dpids = _get_vitual_major_ids('工电')
    keyun_dpids = _get_vitual_major_ids('客运')
    IndexDetails.zhanduan_dpid_data = pd_query(_ALL_ZHANDUAN_DPID_SQL.format(gongdian_dpids, keyun_dpids))
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*_get_calc_month(months_ago))
    for major in ["供电", "车辆", "机务", "车务", "工务", "电务", "客运", "工电"]:
        ids = get_major_dpid(major)
        department_data = pd_query(_DEPARTMENT_SQL.format(ids))
        if major in ["客运", "工电"]:
            tmp_dict = {"客运":"车务", "工电":"工务"}
            ids = get_major_dpid(tmp_dict[major])
            tmp_dict_dpids = {"客运":keyun_dpids, "工电":gongdian_dpids}
            department_data = pd_query(_SPECIAL_DEPARTMENT_SQL.format(tmp_dict_dpids[major]))
        zhanduan_dpid_data = pd_query(_ZHANDUAN_DPID_SQL.format(ids, gongdian_dpids, keyun_dpids))
        zhanduan_dpid_data = zhanduan_dpid_data[zhanduan_dpid_data['MAJOR']==major]
        print(major)
        calc_cardinal_number(months_ago,
                            f'{major}-0',
                            zhanduan_dpid_data,
                            department_data,
                            child_index_sql_dict,
                            base_unit_info_sql, __package__)