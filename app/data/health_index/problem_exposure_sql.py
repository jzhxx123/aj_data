# 检查问题
CHECK_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, b.LEVEL, b.PROBLEM_SCORE, b.RISK_LEVEL
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON b.FK_CHECK_INFO_ID = c.PK_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 基础问题库里的事故隐患问题
HIDDEN_KEY_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(b.DEPARTMENT_ID, '||', a.PK_ID) AS PID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.`TYPE` = 3
            AND a.IS_DELETE = 0
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND a.`STATUS` = 3
            AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND b.TYPE = 4
"""

# 每月发生事故隐患问题
HIDDEN_KEY_PROBLEM_MONTH_SQL = """SELECT DISTINCT
        CONCAT(c.DEPARTMENT_ID, '||', b.PK_ID) AS PID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        b.IS_HIDDEN_KEY_PROBLEM = 1
            AND b.IS_DELETE = 0
            AND b.`TYPE` = 3
            AND b.`STATUS` = 3
            AND c.SHORT_NAME != ''
            AND c.IS_DELETE = 0
            AND c.`TYPE` = 4
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{}', '%%Y-%%m-%%d')
"""

# 出现问题的部门（应该取的是班组）
# 这里因为多个连接查询筛选班组耗时比较多，而且班组数占出现问题部门的大部分，
# 所以这部分筛选先放进程序代码里筛选
EXPOSURE_PROBLEM_DEPARTMENT_SQL = """SELECT
        DISTINCT a.FK_DEPARTMENT_ID
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SOLVE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SOLVE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{}', '%%Y-%%m-%%d')
"""

# 他查问题-检查部门不是本站段的视为他查
OTHER_CHECK_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(a.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RISK_LEVEL) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS d
            ON a.PK_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_department AS e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE
        c.TYPE3 != e.TYPE3
            AND a.RISK_LEVEL BETWEEN 1 AND 2
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 安全生产信息问题
# 安全信息管联多个部门,需判断基础问题是否属于该站段
SAFETY_PRODUCE_INFO_SQL = """SELECT
        DISTINCT 
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                d.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_department AS d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN 
        t_problem_base AS e ON b.FK_PROBLEM_BASE_ID = e.PK_ID 
    WHERE
        a.RANK BETWEEN 1 AND 2
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                AND e.FK_DEPARTMENT_ID = d.TYPE3
"""

# 自查问题-检查部门是本站段的视为自查
SELF_CHECK_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(a.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RISK_LEVEL) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS d
            ON a.PK_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_department AS e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE
        c.TYPE3 = e.TYPE3
            AND a.RISK_LEVEL BETWEEN 1 AND 2
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 查出的隐患问题项点(本单位基础问题库中有的)
CHECKED_HIDDEN_PROBLEM_POINT_SQL = """SELECT DISTINCT
    c.DEPARTMENT_ID, b.PK_ID, c.TYPE2 AS MAJOR
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
        INNER JOIN
    t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
    b.IS_HIDDEN_KEY_PROBLEM = 1
        AND b.IS_DELETE = 0
        AND b.`TYPE` = 3
        AND b.`STATUS` = 3
        AND c.SHORT_NAME != ''
        AND c.IS_DELETE = 0
        AND c.`TYPE` = 4
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 隐患问题项点
HIDDEN_PROBLEM_POINT_SQL = """SELECT
        b.DEPARTMENT_ID, b.PK_ID, b.TYPE2 AS MAJOR
    FROM
        t_problem_base AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.RISK_LEVEL = 2 AND `STATUS` = 3
            AND a.IS_DELETE = 0
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND b.TYPE = 4
            AND b.SHORT_NAME != ''
            AND b.IS_DELETE = 0;
"""
