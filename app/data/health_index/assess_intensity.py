#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    date: 2018/07/31
    desc: 考核力度指数
"""
import pandas as pd
from flask import current_app
from math import fabs
from app.data.health_index.assess_intensity_sql import (
    ASSESS_QUANTIFY_SQL, ASSESS_RESPONSIBLE_SQL, ASSESS_REVISE_SQL,
    ASSESS_RROBLEM_SQL, AWARD_RETURN_SQL)
from app.data.health_index.check_intensity_sql import KAOHE_PROBLEM_SQL
from app.data.health_index.common_sql import (
    CHECK_PROBLEM_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL,
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL, CHECK_PROBLEM_ONOUTWAY_SQL)
from app.data.index.common import (
    calc_child_index_type_divide, calc_extra_child_score_groupby_major_two,
    combine_and_format_basic_data_to_mongo, combine_child_index_func,
    df_merge_with_dpid, get_zhanduan_deparment, summizet_child_index,
    write_cardinal_number_basic_data,
    summizet_operation_set, calc_check_count_per_person)
from app.data.index.util import get_custom_month, validate_exec_month
from app.data.util import pd_query, update_major_maintype_weight
from app.data.health_index.calc_cardinal_number import assess_intensity_cardinal_number


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _summizet_assess_money(year, month):
    data = pd.concat(
        [
            pd_query(each_sql.format(year, month)) for each_sql in [
            ASSESS_REVISE_SQL, ASSESS_RROBLEM_SQL, ASSESS_QUANTIFY_SQL,
            ASSESS_RESPONSIBLE_SQL
        ]
        ],
        axis=0)
    return data


def _get_sql_data(months_ago):
    global ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, STAFF_NUMBER, PROBLEM_COUNT_NOPUOTWAY, CHILD_INDEX_SQL_DICT

    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])

    # 统计工作量
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    # 检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 问题总数（剔除路外问题）
    PROBLEM_COUNT_NOPUOTWAY = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_ONOUTWAY_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        _summizet_assess_money(year, month), DEPARTMENT_DATA)
    # 月度返奖明细
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month)), DEPARTMENT_DATA)
    CHILD_INDEX_SQL_DICT = assess_intensity_cardinal_number(months_ago)
    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type, major_ratio_dict):
    _score = 60
    if row[major_column] == 0:
        return 0
    if row[column] == 0.0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        deduction = _ratio * 100
        if detail_type == 3:
            deduction *= -1
        _score = 100 + deduction
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_score_for_check_count_per_person(self_ratio, major,
                                           major_ratio_dict={
                                               '车务': [(0.55, 100), (0.45, 90), (0.25, 60)],
                                               '客运': [(0.7, 100), (0.65, 90), (0.5, 60)],
                                               '机务': [(0.95, 100), (0.8, 90), (0.6, 60)],
                                               '工务': [(0.5, 100), (0.45, 80), (0.3, 60)],
                                               '工电': [(0.8, 100), (0.67, 80), (0.53, 60)],
                                               '车辆': [(0.51, 100), (0.39, 90), (0.31, 60)],
                                               '电务': [(1.13, 100), (1.02, 90), (0.68, 60)],
                                               '供电': [(0.7, 100), (0.65, 90), (0.6, 60)], }):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    major_ratio = major_ratio_dict.get(major)
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    N1 = major_ratio[0][0]
    Sn1 = major_ratio[0][1]
    N2 = major_ratio[1][0]
    Sn2 = major_ratio[1][1]
    N3 = major_ratio[2][0]
    Sn3 = major_ratio[2][1]
    C = self_ratio
    if level == 1:
        score = Sn1
    elif level == 2:
        score = Sn2 + (C - N2) * (Sn1 - Sn2) / (N1 - N2)
    elif level == 3:
        score = Sn3 + (C - N3) * ((Sn2 - Sn3) / (N2 - N3))
    else:
        score = Sn3 + (C - N3) * (((Sn2 - Sn3) / (N2 - N3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score


def _calc_score_by_formula_asses_ratio(self_ratio, major, major_ratio_dict):
    major_ratio = major_ratio_dict.get(major)
    _score = 0
    if self_ratio > major_ratio:
        _score = (self_ratio - major_ratio) * 100
    return _score


# 换算人均考核问题数
# def _stats_check_problem_assess_radio(months_ago):
#     return calc_child_index_type_divide(
#         ASSESS_PROBLEM_COUNT, STAFF_NUMBER, 1, 3, 1, months_ago, 'COUNT',
#         'SCORE_a', _calc_score_by_formula, _choose_dpid_data)

def _stats_check_problem_assess_radio(months_ago):
    major_ratio_dict = {
        '车务': [(0.55, 100), (0.5, 90), (0.25, 60)],
        '客运': [(0.4, 100), (0.35, 90), (0.22, 60)],
        '机务': [(0.95, 100), (0.8, 90), (0.6, 60)],
        '工务': [(0.6, 100), (0.45, 90), (0.3, 60)],
        '工电': [(0.6, 100), (0.45, 90), (0.3, 60)],
        '车辆': [(0.51, 100), (0.39, 90), (0.31, 60)],
        '电务': [(1.13, 100), (1.02, 90), (0.68, 60)],
        '供电': [(0.6, 100), (0.45, 90), (0.3, 60)],
    }
    return calc_check_count_per_person(
        ASSESS_PROBLEM_COUNT, STAFF_NUMBER, 1, 3, 1, months_ago, 'COUNT',
        'SCORE_a', _calc_score_for_check_count_per_person, _choose_dpid_data, major_ratio_dict=major_ratio_dict)


# 月人均考核金额
# def _stats_assess_money_per_person(months_ago):
#
#     return calc_child_index_type_divide(
#         ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER, 1, 3, 2, months_ago, 'COUNT',
#         'SCORE_b', _calc_score_by_formula, _choose_dpid_data)

def _stats_assess_money_per_person(months_ago):
    major_ratio_dict = {
        '车务': [(50, 100), (40, 90), (30, 60)],
        '客运': [(45, 100), (35, 90), (20, 60)],
        '机务': [(60, 100), (55, 90), (40, 60)],
        '工务': [(50, 100), (40, 90), (20, 60)],
        '工电': [(50, 100), (40, 90), (20, 60)],
        '车辆': [(45, 100), (35, 90), (20, 60)],
        '电务': [(89.3, 100), (80.37, 90), (53.58, 60)],
        '供电': [(50, 100), (40, 90), (20, 60)],
    }
    return calc_check_count_per_person(
        ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER, 1, 3, 2, months_ago, 'COUNT',
        'SCORE_b', _calc_score_for_check_count_per_person, _choose_dpid_data, major_ratio_dict=major_ratio_dict)


# 返奖率
def _stats_award_return_ratio(months_ago):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    fraction_list = CHILD_INDEX_SQL_DICT.get('3-3')
    if AWARD_RETURN_MONEY.empty:
        return None
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = AWARD_RETURN_MONEY[AWARD_RETURN_MONEY['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby(['DEPARTMENT_ID']).size()
        prob_number = idata[idata['MONEY'] > 0]
        prob_number = prob_number.groupby(['DEPARTMENT_ID']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        if fraction_list[i]:
            write_cardinal_number_basic_data(idata, 
            fraction_list[i], 2, 3, fraction_list[i].months_ago,
            columns=['award','prob'])
        rst_child_data = calc_extra_child_score_groupby_major_two(
            idata.copy(),
            _choose_dpid_data(3),
            'ratio',
            _calc_score_by_formula,
            weight=child_weight[i],
            detail_type=3,
            numerator='award',
            denominator='prob',
            fraction=fraction_list[i])
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 返奖时限问题个数（{2}）'
                .format(title[i], row['award'], row['prob']),
            axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data, _choose_dpid_data(3), months_ago, 3, 3, 3)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(df_rst, _choose_dpid_data(3), column, 3, 1, 3, 3,
                           months_ago)
    return [df_rst]


# 考核率
def _stats_problem_assess_radio(months_ago):
    major_ratio_dict = {
        '车务': 0.9,
        '客运': 0.9,
        '机务': 0.9,
        '工务': 0.9,
        '工电': 0.9,
        '车辆': 0.9,
        '电务': 0.9,
        '供电': 0.9,
    }
    return calc_check_count_per_person(
        ASSESS_PROBLEM_COUNT, PROBLEM_COUNT_NOPUOTWAY, 1, 3, 4, months_ago, 'COUNT',
        'SCORE_d', _calc_score_by_formula_asses_ratio, _choose_dpid_data, major_ratio_dict=major_ratio_dict)


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio, _stats_problem_assess_radio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    item_weight = [0.25, 0.25, 0.5, -1]
    update_major_maintype_weight(index_type=0, major=None, main_type=3,
                                 child_index_list=[1, 2, 3, 4],
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, 1, 3, months_ago,
                         item_name, item_weight)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
