# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        a.FK_DEPARTMENT_ID,
        a.RESPONSIBILITY_IDENTIFIED,
        b.PONDERANCE_NUMBER
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
    WHERE
        b.RESPONSIBILITY_DIVISION_NAME = '局属单位责任'
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""
