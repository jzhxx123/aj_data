# -*- coding: utf-8 -*-
"""
基数选择的公共函数
"""
import time
import datetime
from flask import current_app
import pandas as pd
from app import mongo
from app.data.util import (
    pd_query,
    write_bulk_mongo, get_coll_prefix, get_history_months)
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two)
import calendar
from pandas import Series
from app.data.major_risk_index.util import(
    df_merge_with_dpid)


# 通用描述模版
_COMMON_DESCRIPTIONS = {
    # key: 'main_type - detail_type'
    '1-2': '{0}{1}月问题({2})个, 总人数({3})',
    '1-3-2-1': '{0}{1}月作业问题({2})个, 作业人数({3})',
    '1-3-2-2': '{0}{1}月管理问题({2})个, 管理人数({3})',
    '1-5-1-1': '{0}{1}月问题质量分({2}), 总人数({3})',
    '1-6-2-1': '{0}{1}月较大和重大问题质量分({2}), 总人数({3})',
    '1-6-2-2': '{0}{1}月现场检查较大和重大问题质量分({2}), 总人数({3})',
    '1-7-1': '{0}{1}月夜查次数({2}), 作业人数({3})',
    '1-10-1-1': '{0}{1}月监控调阅检查时长({2}), 作业人数({3})',
    '1-10-1-2': '{0}{1}月监控调阅检查问题数({2}), 作业人数({3})',
    '1-10-1-3': '{0}{1}月监控调阅检查问题质量分({2}), 作业人数({3})',
    '3-1-2-1': '{0}{1}月作业问题考核数({2}), 作业人数({3})',
    '3-1-2-2': '{0}{1}月管理问题考核数({2}), 管理人数({3})',
    '3-2-2-1': '{0}{1}月职工月度考核总金额({2}), 作业人数({3})',
    '3-2-2-2': '{0}{1}月干部月度考核总金额({2}), 管理人数({3})',
    '5-1-1-1': '{0}{1}月一般及以上问题数({2}), 总人数({3})',
    '5-1-1-2': '{0}{1}月作业项问题数({2}), 作业人数({3})',
    '5-1-1-3': '{0}{1}月一般以上作业项问题数({2}), 作业人数({3})',
    '5-1-2-1': '{0}{1}月一般及以上问题质量分({2}), 总人数({3})',
    '5-1-2-2': '{0}{1}月作业项问题质量分({2}), 作业人数({3})',
    '5-1-2-3': '{0}{1}月一般及以上作业项问题质量分({2}), 作业人数({3})',
}

_KEY_MAP_A = ('single_df')

_KEY_MAP_B = ('double_df')

_KEY_MAP_C = ('assess_df')

# 通用指数结构模版
COMMON_INDEX_ORGANIZATION = [
    'MAIN_TYPE', 'DETAIL_TYPE', 'SON_DETAIL_TYPE', 'GRANDSON_DETAIL_TYPE']

# ------------------------- 计算前三个月中每月最低故障率最低的单位 ------------------------- #


class CardinalNumberError(Exception):
    def __init__(self,ErrorInfo):
        super().__init__(self) #初始化父类
        self.errorinfo=ErrorInfo
    def __str__(self):
        return self.errorinfo


def _get_history_base_unit_data(stats_months_list, risk_type, base):
    n = 0
    rst_data = []
    for pos, month_tuple in enumerate(stats_months_list):
        mon_ago = base - pos
        mon = int(month_tuple[1].replace('-', '')[:-2])
        prefix = get_coll_prefix(mon_ago)
        data = list(mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
            {'ACCIDENT_COUNT':{'$exists':'true'},
            "MAJOR": risk_type.split('-')[0], 
            "INDEX_TYPE": int(risk_type.split('-')[1]),
            "MON": mon},
            {"_id": 0}
        ))
        if data == [] or not data[0]:
            break
        n += 1
        dpid_list =  data[0]['BASE_UNIT_ID']
        for main_type, key in zip([1, 2], ["ACCIDENT_COUNT", "TROUBLE_COUNT"]):
            for dpid in dpid_list:
                _tmp_data = {
                    "DEPARTMENT_ID": dpid,
                    "COUNT":data[0][key],
                    "MAIN_TYPE": main_type,
                    "MON_AGO": mon_ago,
                }
                rst_data.append(_tmp_data)
    data = pd.DataFrame(data=rst_data)
    flag =  True if n > 0 else False
    return data, flag


def _translate_mon_ago(mon, stats_months_index, months_ago):
    # 相对计算月的多少个月以前
    mon = str(mon)
    pos = stats_months_index.index(mon)
    return months_ago - pos


def _get_least_ratio_dpid(least_count, base_unit_info_data):
    """[summary]
    根据最低故障分得到，相应的单位
    Arguments:
        least_count {[type]} -- [description]
        base_unit_info_data {[type]} -- [description]
    """
    least_dpid = base_unit_info_data[base_unit_info_data['COUNT']
                                     == least_count]
    least_dpid = set(least_dpid['DEPARTMENT_ID'].values.tolist())
    return list(least_dpid)


def _diff_mon_ago_by_time(base_unit_info_data, stats_months_list, months_ago):
    base_unit_info_data['MON_AGO'] = months_ago
    stats_months_index = [item[1] for item in stats_months_list]
    for idx, row in base_unit_info_data.iterrows():
        # 判断这个时间是否大于当月25号，大于则为下一个月，小于则为当月
        try:
            _now = datetime.datetime.strptime(
                str(row['OCCURRENCE_TIME']), "%Y-%m-%d %H:%M:%S")
            _now = _now.date()
            _tmp_time = _now.replace(day=25)
            _real_mon = _tmp_time
            if _now >= _tmp_time:
                # 下一月
                _days_num = calendar.monthrange(_now.year, _now.month)[1]
                _real_mon = _tmp_time + datetime.timedelta(days=_days_num)
            base_unit_info_data.loc[idx, 'MON_AGO'] = _translate_mon_ago(
                _real_mon, stats_months_index, months_ago)
        except Exception as e:
            current_app.logger.debug('├── └── cardinal_number_common._diff_mon_ago_by_time error,+\n ' +
                                     'detail_reason: {0}'.format(e))
            continue
        # stats_months_list 中找到这个月份的位置，则为相当于这个月的几个月前
    base_unit_info_data.drop(['OCCURRENCE_TIME'], inplace=True, axis=1)
    # 删除默认值-1的行
    # base_unit_info_data=base_unit_info_data[~base_unit_info_data['MON_AGO'].isin([-1])]
    return base_unit_info_data


def _calc_accdient_occurrence_ratio(base_unit_info_sql, months_ago, zhanduan_dpid_data, risk_type):
    """[summary]
    # MAIN_TYPE=1为事故
    # MAIN_TYPE=2为故障
    condition:
    选取事故率最低的单位，事故率相同，选取故障率最低，都相同，全取
    Arguments:
        base_unit_info_sql {[type]} -- [description]
        months_ago {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]

    Returns:
        [type] -- [description]
    """
    # 前3个月，不包括当月
    major = risk_type.split('-')[0]
    index_type = int(risk_type.split('-')[1])
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    # 这里需要判断一下，前三个月的部门信息数据是否存起来，不再重复查询。
    base_unit_info_data, flag = _get_history_base_unit_data(stats_months_list, risk_type, months_ago)
    if flag:
        # 从历史库查询的数据不需要填充其站段部门信息
        base_unit_info_data = base_unit_info_data
        # 根据站段信息补齐
        zhanduan_dpid_set = set(
            zhanduan_dpid_data['DEPARTMENT_ID'].values.tolist())
        base_unit_info_data = base_unit_info_data[
            base_unit_info_data['DEPARTMENT_ID'].isin(list(zhanduan_dpid_set))]
    else:
        base_unit_info_data = pd_query(base_unit_info_sql)
        base_unit_info_data = _diff_mon_ago_by_time(
            base_unit_info_data, stats_months_list, months_ago)
        # 根据站段信息补齐
        zhanduan_dpid_set = set(
            zhanduan_dpid_data['DEPARTMENT_ID'].values.tolist())
        base_unit_info_data = base_unit_info_data[
            base_unit_info_data['DEPARTMENT_ID'].isin(list(zhanduan_dpid_set))]
        # mon,count,dpid
        mon_agos = [months_ago - i for i in range(0, 4)]
        for _dpid in zhanduan_dpid_set:
            for mon_ago in mon_agos:
                for main_type in [1, 2]:
                    _tmp_dict = {
                        'DEPARTMENT_ID': _dpid,
                        'MON_AGO': mon_ago,
                        'COUNT': 0,
                        "MAIN_TYPE": main_type
                    }
                    base_unit_info_data = base_unit_info_data.append(
                        [_tmp_dict], ignore_index=True)

        base_unit_info_data = base_unit_info_data.groupby(
            ['DEPARTMENT_ID', 'MON_AGO', 'MAIN_TYPE'])['COUNT'].sum().reset_index()
    # 初始化三个月份的字典
    least_accidental_dpid = dict()
    for mon_ago in set(base_unit_info_data['MON_AGO'].values.tolist()):
        _mon = get_custom_month(mon_ago)
        _mon = int(_mon[1].replace('-', '')[:-2])
        _data = {
            "MAJOR": major,
            "MON": _mon,
            "INDEX_TYPE": index_type
        }
        # 获得得到该月事故最低的单位
        _tmp_df = base_unit_info_data[(base_unit_info_data['MON_AGO'] == mon_ago) &
            (base_unit_info_data['MAIN_TYPE'] == 1)]
        _count_list = _tmp_df['COUNT'].values.tolist()
        _least_count = sorted(_count_list)[0]
        _least_dpid = _get_least_ratio_dpid(_least_count, _tmp_df)
        _data['ACCIDENT_COUNT'] = _least_count

        # 在事故率最低的基础上进一步筛选，获得得到该月故障率最低的单位
        _tmp_df = base_unit_info_data[
            (base_unit_info_data['DEPARTMENT_ID'].isin(list(_least_dpid))) &
            (base_unit_info_data['MAIN_TYPE'] == 2) &
            (base_unit_info_data['MON_AGO'] == mon_ago)]
        _count_list = _tmp_df['COUNT'].values.tolist()
        _least_count = sorted(_count_list)[0]
        _least_dpid = _get_least_ratio_dpid(_least_count, _tmp_df)
        _data['TROUBLE_COUNT'] = _least_count
        _data['BASE_UNIT_ID'] = list(_least_dpid)
        _data['HIERARCHY'] = 3
        _unit_names = zhanduan_dpid_data[zhanduan_dpid_data['DEPARTMENT_ID'].isin(
            list(_least_dpid))]
        _data['BASE_UNIT_NAME'] = _unit_names['NAME'].values.tolist()
        # 不把当前计算月的最优部门参与到本次计算中
        if mon_ago != months_ago:
            least_accidental_dpid[mon_ago] = _least_dpid
        _write_data_to_mongo([_data],
                            mon_ago,
                            [{"ACCIDENT_COUNT": {'$exists': 'true'}}], risk_type)
    return least_accidental_dpid


# 比较两个列表是否相等
def _get_diff_compare_list(former, latter, mon):
    _same = []
    _diff = []
    _keys_list = [['DENOMINATOR_NAME', 'DENOMINATOR_VERSION', 'DENOMINATOR_DESCRIPTION'],
                  ['NUMERATOR_NAME', 'NUMERATOR_VERSION', 'NUMERATOR_DESCRIPTION']]
    _latter = [dict(t) for t in set([tuple(d.items()) for d in latter])]
    _former = [dict(t) for t in set([tuple(d.items()) for d in former])]
    for item in _latter:
        if item in _former:
            _same.append(item)
        else:
            # 比较两个相同键字典，找出不同的值的键
            # 这里如果出现DESCRIPTION描述不一致，则报错
            # 先找version，跟name对得上的字典
            for _f in _former:
                _index = list(item.keys())
                keys = _keys_list[0] if 'DENOMINATOR' in _index[0] else _keys_list[1]
                if set(_index) == set(list(_f.keys())):
                    if item[keys[0]] == _f[keys[0]] and item[keys[1]] == _f[keys[1]]:
                        if item[keys[2]] != _f[keys[2]]:
                            raise CardinalNumberError(
                                f'本月数据项 --- {item[keys[0]]}-{item[keys[1]]}({item[keys[2]]}) \n' + \
                                f' 与 已存数据库{mon}月对应数据项 --- {_f[keys[0]]}-{_f[keys[1]]}({_f[keys[2]]}) 描述不统一，\n' + \
                                '易造成数据版本混乱，请及时修复'
                            )
            _diff.append(item)
    return _same, _diff


# 将对应月份的最低故障率部门中历史库的指数结构配置与当前月要计算的指数结构比较
def _compare_major_index_organization(mon_ago_dpids, this_mon_index_organization, risk_type):
    # 初始化一个故障率不同计算方式的字典
    # recalc: 使用当前月份的方案重新计算
    # from_history: 使用历史数据计算
    """[summary]
    this_mon_index_organization:[{'DENOMINATOR_NAME':1,'DENOMINATOR_VERSION':1},.....]
    Returns:
        [dict] -- {
            '-1':{
                'from_history':[dpids,[index_organizations]]
            }
        }
    """
    major = risk_type.split('-')[0]
    index_type = int(risk_type.split('-')[1])
    diff_calc_caridnal_dpid = dict()
    for mon_ago, dpids in mon_ago_dpids.items():
        diff_calc_caridnal_dpid[mon_ago] = dict()
        mon = get_custom_month(mon_ago)
        mon = int(mon[1].replace('-', '')[:-2])
        prefix = get_coll_prefix(mon_ago)
        # 因为同一个指数，所有的部门的指数结构是一样的
        # 暂时不考虑将部门之间不同考虑进去（但是避免以后出现同一个指数不同的部门指数内容不一样）
        # 获取含TYPE的字段
        rst_data = []
        for _var in [['DENOMINATOR_NAME', 'DENOMINATOR_VERSION', 'DENOMINATOR_DESCRIPTION'],
                     ['NUMERATOR_NAME', 'NUMERATOR_VERSION', 'NUMERATOR_DESCRIPTION']]:
            _s_condition = {
                "_id": 0,
                _var[0]: 1,
                _var[1]: 1,
                _var[2]: 1,
            }
            _data = list(mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
                {'DEPARTMENT_ID': {"$in": list(dpids)}, "MON": mon,
                    "MAJOR": major, "INDEX_TYPE": index_type},
                _s_condition
            ))
            rst_data.extend(_data)
        # 预期_data格式：
        # [{'MAIN_TYPE':1,'DETAIL_TYPE':2},{'MAIN_TYPE':1,'DETAIL_TYPE':4},.....]
        _same, _diff = _get_diff_compare_list(
            rst_data, this_mon_index_organization, mon)
        diff_calc_caridnal_dpid[mon_ago]['recalc'] = [dpids, _diff]
        diff_calc_caridnal_dpid[mon_ago]['from_history'] = [dpids, _same]
    return diff_calc_caridnal_dpid


# ------------------------- 比值型基数存入到mongo中 a/b -------------------------
def _campare_accident_count_to_zero(months_ago, risk_type):
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}health_index_cardinal_number_basic_data'
    data = list(mongo.db[coll_name].find(
        {"MON": mon, "MAJOR":risk_type.split('-')[0],
        "INDEX_TYPE":int(risk_type.split('-')[1]),
        "ACCIDENT_COUNT": {'$exists': 'true'}
        }, {"_id": 0, "ACCIDENT_COUNT":1}
    ))
    flag = False
    try:
        if data[0]["ACCIDENT_COUNT"] > 0:
            flag = True
    except Exception as e:
        current_app.logger.debug(
            f"├── └── {risk_type.split('-')[0]} {int(risk_type.split('-')[1])} error --- {e}."
        )
        return flag
    return flag


def _write_data_to_mongo(data,
                         months_ago,
                         r_condition_list,
                         risk_type):
    """[summary]
    将基数计算过程数据存入mongo中
    Arguments:
        data {[type]} -- [description]
    """
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}health_index_cardinal_number_basic_data'
    # 删除历史数据
    for r_condition in r_condition_list:
        r_condition.update({
            'MON': mon,
            'HIERARCHY': 3
        })
        r_condition.update({
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1])
        })
        mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


# ------------------------- 比值型基数计算 a/b 并存储------------------------- #
def calc_cardinal_number_divide(
        df_fraction_list, record_type,
        months_ago, risk_type, fraction_list):
    """[summary]
    计算比值型基数，
    存入mongo中
    Arguments:
        df_numerator {[type]} -- [列包含DEPARTMENT_ID, DENOMINATOR, NUMERATOR, NAME]
        risk_type {[type]} -- [description]
    """
    mon = get_custom_month(months_ago)
    mon = int(mon[1].replace('-', '')[:-2])
    # fraction 类传进来模版
    _tpl = '{0}{1}月,{2}({3}), {4}({5})'
    r_condition = dict()
    # 存在基数计算时，6，7，8-9，7，8，9-10，相交月份
    # 这其中的数据更新应该删除重复版本的数据，不同的版本数据应该保留
    record_type_list = record_type.split('-')
    for i in range(len(record_type_list)):
        _key = COMMON_INDEX_ORGANIZATION[i]
        r_condition[_key] = int(record_type_list[i])
    rst_data = []
    r_condition_list = [r_condition.copy() for i in range(len(fraction_list))]
    # 这里加一个fraction_list满足多个分数对象（指数多个组成）同时存储
    for pos, fraction in enumerate(fraction_list):
        if hasattr(fraction, 'tmplation'):
            _tpl = fraction.tmplation
        content = ''
        r_condition_list[pos].update({
            'DENOMINATOR_NAME': fraction.denominator.name,
            'DENOMINATOR_VERSION': fraction.denominator.version,
            'NUMERATOR_NAME': fraction.numerator.name,
            'NUMERATOR_VERSION': fraction.numerator.version,
        })
        data = dict()
        for _, row in df_fraction_list[pos].iterrows():
            _tmp_rst = {
                'HIERARCHY': 3,
                'MAJOR': risk_type.split('-')[0],
                'INDEX_TYPE': int(risk_type.split('-')[1]),
                'MON': mon,
                'DENOMINATOR': row['DENOMINATOR'],
                'DENOMINATOR_NAME': fraction.denominator.name,
                'DENOMINATOR_DESCRIPTION': fraction.denominator.description,
                'DENOMINATOR_VERSION': fraction.denominator.version,
                'NUMERATOR': row['NUMERATOR'],
                'NUMERATOR_NAME': fraction.numerator.name,
                'NUMERATOR_DESCRIPTION': fraction.numerator.description,
                'NUMERATOR_VERSION': fraction.numerator.version,
                'DEPARTMENT_ID': row['DEPARTMENT_ID'],
                'DEPARTMENT_NAME': row['NAME']
            }
            _tmp_rst.update(
                r_condition
            )
            rst_data.append(
                _tmp_rst
            )
        numerator = sum(df_fraction_list[pos]['NUMERATOR'])
        denominator = sum(df_fraction_list[pos]['DENOMINATOR'])
        if _campare_accident_count_to_zero(months_ago, risk_type):
            cardinal_number = round(numerator / denominator, 3) * 1.2
        else:
            cardinal_number = round(numerator / denominator, 3)
        names = set(df_fraction_list[pos]['NAME'].values.tolist())
        content = _tpl.format(names, mon, fraction.numerator.description,
                              numerator, fraction.denominator.description, denominator)
        data.update(r_condition_list[pos])
        data.update({
            'HIERARCHY': 3,
            'CARDINAL_NUMBER': cardinal_number,
            'CONTENT': content,
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'MON': mon,
            'DENOMINATOR': denominator,
            'NUMERATOR': numerator
        })
        rst_data.append(data)
    _write_data_to_mongo(rst_data,
                         months_ago,
                         r_condition_list,
                         risk_type)
    return rst_data

# ------------------------- 转换标准格式函数区 ------------------------ #


def _calc_sum_by_type3(df, filed, department_data):
    # 按照站段聚合求和
    df = df_merge_with_dpid(df, department_data)
    df = df.groupby(['TYPE3'])[filed].sum().reset_index()
    return df


def is_number(s):
    try:  # 如果能运行float(s)语句，返回True（字符串s是浮点数）
        float(s)
        return True
    except ValueError:  # ValueError为Python的一种标准异常，表示"传入无效的参数"
        pass  # 如果引发了ValueError这种异常，不做任何事情（pass：不做任何事情，一般用做占位语句）
    try:
        import unicodedata  # 处理ASCii码的包
        for i in s:
            unicodedata.numeric(i)  # 把一个表示数字的字符串转换为浮点数返回的函数
            # return True
        return True
    except (TypeError, ValueError):
        pass
    return False


def _diff_dfcolumn_digital_str(df):
    """[summary]
    区分字符串，返回字符型列名，数字型列名
    Arguments:
        df {[df]} -- [description]
    """
    column_type_dict = {
        'digit_str': [],
        'complex_str': [],
    }
    columns = df.columns.values.tolist()
    for col in columns:
        _test_list = []
        for i in df[col].values.tolist():
            if is_number(i):
                _test_list.append(1)
            else:
                _test_list.append(0)
        _key = 'complex_str'
        if sum(_test_list) / len(_test_list) == 1:
            _key = 'digit_str'
        column_type_dict[_key].append(col)
    return column_type_dict


def _get_assess_single_df(sqllist,  zhanduan_dpid_data, department_data, mon_ago, column, dpids):
    # 为了兼容多种类型的，应该动态区分哪个为字符类型列，哪个为数字类型列
    stats_month = get_custom_month(mon_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    _df = pd_query(sqllist[0].format(year, month)) if isinstance(
        sqllist[0], str) else sqllist[0]
    _column_type_dict = _diff_dfcolumn_digital_str(_df)
    _df = _calc_sum_by_type3(
        _df,
        _column_type_dict['digit_str'][0],
        department_data)
    _df.rename(columns={
        'TYPE3': 'DEPARTMENT_ID',
        _column_type_dict['digit_str'][0]: column
    }, inplace=True)
    _df = _df[_df['DEPARTMENT_ID'].isin(dpids)]
    return _df


def _get_common_single_df(sqllist,  zhanduan_dpid_data, department_data, mon_ago, column, dpids):
    # 为了兼容多种类型的，应该动态区分哪个为字符类型列，哪个为数字类型列
    stats_month = get_custom_month(mon_ago)
    _df = pd_query(sqllist[0].format(*stats_month)) if isinstance(
        sqllist[0], str) else sqllist[0]
    _column_type_dict = _diff_dfcolumn_digital_str(_df)
    _df = _calc_sum_by_type3(
        _df,
        _column_type_dict['digit_str'][0],
        department_data)
    _df.rename(columns={
        'TYPE3': 'DEPARTMENT_ID',
        _column_type_dict['digit_str'][0]: column
    }, inplace=True)
    _df = _df[_df['DEPARTMENT_ID'].isin(dpids)]
    return _df


def _get_total_person_df(sqllist,  zhanduan_dpid_data, department_data, mon_ago, column, dpids):
    # 为了兼容多种类型的，应该动态区分哪个为字符类型列，哪个为数字类型列
    stats_month = get_custom_month(mon_ago)
    _, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    _df = pd_query(sqllist[0].format(*stats_month)) if isinstance(
        sqllist[0], str) else sqllist[0]
    # 列表第二个为了适配外聘人数
    _df_a = pd_query(sqllist[1].format(month)) if isinstance(
        sqllist[1], str) else sqllist[1]
    _df = pd.concat(
        [_df, _df_a], axis=0, sort=False)
    _column_type_dict = _diff_dfcolumn_digital_str(_df)
    _df = _calc_sum_by_type3(
        _df,
        _column_type_dict['digit_str'][0],
        department_data)
    _df.rename(columns={
        'TYPE3': 'DEPARTMENT_ID',
        _column_type_dict['digit_str'][0]: column
    }, inplace=True)
    _df = _df[_df['DEPARTMENT_ID'].isin(dpids)]
    return _df


_TRASLATE_TO_STANDARD_CALC_CARDINAL_DF_FUNC = {
    # 函数转换成标准格式，DEPARTMENT_ID, NUMERATOR, DENOMINATOR
    # 元组key:(record_type1,record_type2,....)
    # 单一sqll
    _KEY_MAP_A: _get_common_single_df,
    # 总人数（包含职工+外聘）或（非干部职工+外聘）
    _KEY_MAP_B: _get_total_person_df,
    _KEY_MAP_C: _get_assess_single_df,
}


def _calc_data_forum(
    record_type,
    package,
    diff_calc_caridnal_dpid,
    divider_list,
    zhanduan_dpid_data,
    department_data,
    risk_type):
    risk_type_list = risk_type.split('-')
    _func_keys = _TRASLATE_TO_STANDARD_CALC_CARDINAL_DF_FUNC.keys()
    # 根据sql转换成标准的基数计算格式
    for mon_ago in diff_calc_caridnal_dpid.keys():
        fraction_list = []
        for divider in divider_list:
            numerator_dict = {
                'NUMERATOR_NAME': divider.numerator.name,
                'NUMERATOR_VERSION': divider.numerator.version
            }

            denominator_dict = {
                'DENOMINATOR_NAME': divider.denominator.name,
                'DENOMINATOR_VERSION': divider.denominator.version
            }
            # 分子
            _key = divider.numerator.func_version
            _func_key = [item for item in _func_keys if _key in item][0]
            _func = _TRASLATE_TO_STANDARD_CALC_CARDINAL_DF_FUNC[_func_key]
            _df_numerator = _transform_cardinal_number(
                numerator_dict,
                diff_calc_caridnal_dpid,
                zhanduan_dpid_data,
                department_data,
                mon_ago,
                risk_type_list,
                divider,
                _func
            )

            # 分母
            _key = divider.denominator.func_version
            _func_key = [item for item in _func_keys if _key in item][0]
            _func = _TRASLATE_TO_STANDARD_CALC_CARDINAL_DF_FUNC[_func_key]
            _df_denominator = _transform_cardinal_number(
                denominator_dict,
                diff_calc_caridnal_dpid,
                zhanduan_dpid_data,
                department_data,
                mon_ago,
                risk_type_list,
                divider,
                _func
            )
            # 同理获取_df_denominator
            _df_numerator = pd.merge(
                _df_numerator, _df_denominator, how='inner', on='DEPARTMENT_ID')
            _df_numerator = pd.merge(
                _df_numerator, zhanduan_dpid_data, how='inner', on='DEPARTMENT_ID')
            fraction_list.append(_df_numerator)
        # 公共函数计算 fraction_list应该跟divider_list（child_index_sql_dict[record_type]）是序号一一对应的
        calc_cardinal_number_divide(
            fraction_list, record_type, mon_ago, risk_type, divider_list)


# 基数计算a/b，根据不同的指数类型，a/b的个数不同
def calc_cardinal_number(
        months_ago,
        risk_type,
        zhanduan_dpid_data,
        department_data,
        child_index_sql_dict,
        base_unit_info_sql, package):
    """[summary]

    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
        child_index_sql_dict {[dict]} -- [子指数类型，一级一级sql]
        child_index_sql_dict = {
            'MAIN_TYPE-DETAIL_TYPE-SON_DETAIL_TYPE-GRANDSON_DETAIL_TYPE':[DATACLASSLIST](numerator,denominator)
        }
    """
    # 当前月的指数计算结构
    this_mon_index_organization = []
    for dividerclass_list in child_index_sql_dict.values():
        for divider in dividerclass_list:
            this_mon_index_organization.append({
                'DENOMINATOR_VERSION': divider.denominator.version,
                'DENOMINATOR_NAME': divider.denominator.name,
                'DENOMINATOR_DESCRIPTION': divider.denominator.description,
            })
            this_mon_index_organization.append({
                'NUMERATOR_VERSION': divider.numerator.version,
                'NUMERATOR_NAME': divider.numerator.name,
                'NUMERATOR_DESCRIPTION': divider.numerator.description,
            })
    mon_ago_dpids = _calc_accdient_occurrence_ratio(
        base_unit_info_sql, months_ago, zhanduan_dpid_data, risk_type)
    diff_calc_caridnal_dpid = _compare_major_index_organization(
        mon_ago_dpids, this_mon_index_organization, risk_type)
    # 拆解不同的月份部门，执行不同的基数计算流程
    # 从历史中读取数据我们用不到字典的值，用键即可
    # 但是重新计算时的字典会动态变化，需要提前用列表存储键
    for record_type in child_index_sql_dict.keys():
        _calc_func = _record_func_runtime(_calc_data_forum)
        _calc_func(record_type, package, diff_calc_caridnal_dpid, child_index_sql_dict[record_type],
                   zhanduan_dpid_data, department_data, risk_type)


def _transform_cardinal_number(cond,
                               diff_calc_caridnal_dpid,
                               zhanduan_dpid_data,
                               department_data,
                               mon_ago,
                               risk_type_list,
                               fraction,
                               func):
    mon = get_custom_month(mon_ago)
    prefix = get_coll_prefix(mon_ago)
    mon = int(mon[1].replace('-', '')[:-2])
    _key = list(cond.keys())[0].split('_')[0]
    if cond in diff_calc_caridnal_dpid[mon_ago]['from_history'][1]:
        dpids = diff_calc_caridnal_dpid[mon_ago]['from_history'][0]
        cond.update({'DEPARTMENT_ID': {"$in": dpids}, "MON": mon,
                     "MAJOR": risk_type_list[0], "INDEX_TYPE": int(risk_type_list[1])})
        _numerator = mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
            cond,
            {"_id": 0, _key: 1, "DEPARTMENT_ID": 1}
        )
        _df_numerator = pd.DataFrame(list(_numerator))
        _df_numerator.drop_duplicates(
            subset=['DEPARTMENT_ID'], keep='first', inplace=True)
    else:
        dpids = diff_calc_caridnal_dpid[mon_ago]['recalc'][0]
        _sqllist_numerator = []
        # 动态获取属性值
        _sqllist_numerator = eval(f"fraction.{_key.lower()}.value")
        _df_numerator = func(
            _sqllist_numerator, zhanduan_dpid_data, department_data, mon_ago, _key, dpids)
    return _df_numerator


def _record_func_runtime(func):
    """[记录函数运行时间]
Arguments:
    func {[functions]} -- [description]

Returns:
    [functions] -- [description]
    """
    from functools import wraps
    @wraps(func)
    def wrapper(record_type, package, *args, **kwargs):
        start = time.time()
        current_app.logger.debug(
            f'├── └── {package} {record_type} calc_cardinal_number has start computing')
        # try:
        func(record_type, package, *args, **kwargs)
        # except Exception as e:
        #     current_app.logger.debug(
        #     f'├── └── {package} {record_type} calc_cardinal_number exsits error: {e}')
        cost = round(time.time() - start, 3)
        current_app.logger.debug(
            f'├── └── {package} {record_type} calc_cardinal_number had costed time --- {cost}.')
    return wrapper


def calc_fitting_cardinal_number_by_formula(cardinal_number_data_list, cond=None):
    """[summary]
    获取合适的基数（目前为取三个月中最大值）
    Arguments:
        cardinal_number_data_list {[type]} -- [description]

    Keyword Arguments:
        cond {[type]} -- [description] (default: {None})

    Returns:
        [dict] -- [{"CARDINAL_NUMBER":,
                        "MON":,
                        "CONTENT":,}]
    """
    from operator import itemgetter
    rst_data = []
    for major in ["供电", "车辆", "机务", "车务", "工务", "电务", "客运", "工电"]:
        tmp_list = []
        for cardinal_number_data in cardinal_number_data_list:
            if cardinal_number_data['MAJOR'] == major:
                tmp_list.append(cardinal_number_data)
        tmp_list = sorted(tmp_list, key=itemgetter(
        'CARDINAL_NUMBER'), reverse=True)
        rst_data.append(tmp_list[0])
    return rst_data


def get_cardinal_number(fraction):
    history_three_months = [fraction.months_ago - i for i in range(1,4)]
    # 获取三个月前对应的基数，选择最大值
    risk_type_list = fraction.risk_type.split('-')
    cardinal_number = []
    for mon_ago in history_three_months:
        prefix = get_coll_prefix(mon_ago)
        mon = get_history_months(mon_ago)[0]
        cardinal_number.extend(list(mongo.db[f'{prefix}health_index_cardinal_number_basic_data'].find(
            {"MON": mon, "DENOMINATOR_NAME":fraction.denominator.name,
            "DENOMINATOR_VERSION": fraction.denominator.version,
            "NUMERATOR_VERSION": fraction.numerator.version,
            "NUMERATOR_NAME": fraction.numerator.name,
            "INDEX_TYPE": int(risk_type_list[1]),"CARDINAL_NUMBER": {'$exists':'true'}},
            {"_id": 0, "CARDINAL_NUMBER": 1, "MAJOR":1}
        )))
    cardinal_number = calc_fitting_cardinal_number_by_formula(cardinal_number)
    # 定义，可以混合定义
    major_avg_score = Series(
        [number["CARDINAL_NUMBER"] for number in cardinal_number], 
        index=[number["MAJOR"] for number in cardinal_number])
    major_avg_score.index.rename('MAJOR', inplace=True)
    return major_avg_score