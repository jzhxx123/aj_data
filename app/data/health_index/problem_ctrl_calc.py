# -*- coding: utf-8 -*-

import math
from app.data.index.util import (
    get_custom_month, get_months_from_201712)
import pandas as pd
from app.data.util import (
    get_data_from_mongo_by_find, get_coll_prefix,
    pd_query,
    write_bulk_mongo)
from app.data.index.common import (
    write_export_basic_data_to_mongo, df_merge_with_dpid,
    calc_child_index_type_sum, format_export_basic_data)


def _get_repeate_hidden_problem_data(calc_month, repeate_happen_problem_sql, department_data):
    """[根据需要更新mongo数据]

    Arguments:
        months_ago {[type]} -- [description]
        condition {[type]} -- [description]
        set_condition {[type]} -- [description]
        coll_name {[type]} -- [description]
    """
    repeate_happen_problem_data = df_merge_with_dpid(
        pd_query(repeate_happen_problem_sql.format(*calc_month)),
        department_data)
    repeate_happen_problem_data = repeate_happen_problem_data.groupby(
        ['TYPE3', 'FK_PROBLEM_BASE_ID', 'RISK_LEVEL'])['COUNT'].sum().reset_index()
    return repeate_happen_problem_data


# 问题控制
def stats_repeatedly_index(department_data, zhanduan_dpid_data,
                           months_ago, choose_dpid_data,
                           worker_count,
                           repeate_happen_problem_sql,
                           major_problem_point_info_data,
                           index_type=1):
    """[
        问题控制，包括计算整改系数，单位问题暴露度，单位系数等
    ]
    最终得分=100-∑扣分*整改系数/问题暴露度系数
    单位系数 - 某个站段的总人数/（参与计算的站段总人数/参与计算的站段数)
    问题暴露度系数 - 针对某个单位的暴露度系数
    扣分 - 针对某个单位某个问题的扣分
    """
    # 以站段为统计单位,计算单位系数
    worker_count = pd.merge(
        department_data,
        worker_count,
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID',
        how='left'
    )
    worker_count.drop(['DEPARTMENT_ID', 'FK_DEPARTMENT_ID'],
                      inplace=True, axis=1)
    # 人数补零
    worker_count.fillna({'COUNT': 0}, inplace=True)
    # 各站段部门数
    worker_count['TYPE3_COUNT'] = len(
        set(worker_count['TYPE3'].values.tolist()))
    worker_count['TOTAL_PERSON_COUNT'] = sum(
        worker_count['COUNT'].values.tolist())
    worker_count_df = worker_count.groupby(
        ['TYPE3'])['COUNT'].sum().reset_index()
    worker_count_df.rename(
        columns={'COUNT': 'TYPE3_PERSON_COUNT'}, inplace=True)
    worker_count = pd.merge(
        worker_count,
        worker_count_df,
        on='TYPE3',
        how='inner'
    )
    del worker_count_df
    worker_count.drop(['COUNT'], inplace=True, axis=1)
    # 各部门单位系数（以站段为单位）
    worker_count['DP_COEFFICIENT'] = worker_count.apply(
        lambda row: row['TYPE3_PERSON_COUNT'] / (row['TOTAL_PERSON_COUNT']/row['TYPE3_COUNT']), axis=1)

    # 获取当月问题暴露度指数分数
    calc_month = get_custom_month(months_ago)
    calc_month = calc_month[1].replace('-', '')
    calc_month = calc_month[:-2]
    condition = {
        "MON": int(calc_month),
        "TYPE": 0,
        "MAIN_TYPE": 5,
        "DETAIL_TYPE": 0,
        "HIERARCHY": 3
    }
    saved_condition = {
        '_id': 0,
        "DEPARTMENT_ID": 1,
        "SCORE": 1
    }
    current_month_problem_exposure_data = pd.DataFrame(
        get_data_from_mongo_by_find(
            months_ago, condition, saved_condition, 'detail_health_index')
    )
    mean = float(current_month_problem_exposure_data['SCORE'].mean())
    mean = min(60, mean)
    current_month_problem_exposure_data['EXPOSURE_COEFFICIENT'] = current_month_problem_exposure_data.apply(
        lambda row: 1 + (row['SCORE'] - mean) / mean, axis=1)
    # 将暴露度指数合并
    worker_count = pd.merge(
        worker_count,
        current_month_problem_exposure_data[[
            'EXPOSURE_COEFFICIENT', 'DEPARTMENT_ID']],
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
        how='left'
    )
    worker_count.drop(['DEPARTMENT_ID'], inplace=True, axis=1)
    worker_count.drop_duplicates(subset=['TYPE3'], keep='first', inplace=True)
    # 整理df格式
    # 当前月份的数据跟上个月数据对比
    # 查询的当前月份数据格式应该如下
    """
    columns : FK_DEPARTMENT_ID, FK_PROBLEM_BASE_ID, RISK_LEVEL, COUNT, PROBLEM_POINT
    意思为某个部门某个风险问题，上个月发生了多少次
    """
    stats_months = get_months_from_201712(months_ago)

    repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[0], repeate_happen_problem_sql, department_data)
    # 将专业中问题项点合并
    repeate_happen_problem_data = pd.merge(
        repeate_happen_problem_data,
        major_problem_point_info_data,
        on='FK_PROBLEM_BASE_ID',
        how='inner'
    )
    # 初始化前一个月，前2个月数据
    last_month_repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[1], repeate_happen_problem_sql, department_data)
    last_month_repeate_happen_problem_data = last_month_repeate_happen_problem_data[[
        'FK_PROBLEM_BASE_ID', 'COUNT']]
    last_month_repeate_happen_problem_data.rename(
        columns={'COUNT': 'PROBLEM_COUNT_2_MONTH'}, inplace=True)
    repeate_happen_problem_data = pd.merge(
        last_month_repeate_happen_problem_data,
        repeate_happen_problem_data,
        how='outer',
        on='FK_PROBLEM_BASE_ID'
    )
    repeate_happen_problem_data.fillna(
        {'COUNT': 0, 'PROBLEM_COUNT_2_MONTH': 0}, inplace=True)
    # 前2个月数据
    last_month_repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[2], repeate_happen_problem_sql, department_data)
    last_month_repeate_happen_problem_data = last_month_repeate_happen_problem_data[[
        'FK_PROBLEM_BASE_ID', 'COUNT']]
    last_month_repeate_happen_problem_data.rename(
        columns={'COUNT': 'PROBLEM_COUNT_3_MONTH'}, inplace=True)

    repeate_happen_problem_data = pd.merge(
        last_month_repeate_happen_problem_data,
        repeate_happen_problem_data,
        how='outer',
        on='FK_PROBLEM_BASE_ID'
    )
    repeate_happen_problem_data.fillna(
        {'COUNT': 0, 'PROBLEM_COUNT_3_MONTH': 0}, inplace=True)
    del last_month_repeate_happen_problem_data

    repeate_happen_problem_data = pd.merge(
        repeate_happen_problem_data,
        zhanduan_dpid_data,
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    repeate_happen_problem_data.drop(
        ['DEPARTMENT_ID', 'NAME'], inplace=True, axis=1)

    # 将当前月的重复问题数据导入到mongo中
    # 将人数跟重复问题df合并
    repeate_happen_problem_data.rename(columns={
        'COUNT': 'PROBLEM_COUNT'
    }, inplace=True)

    repeate_happen_problem_data = pd.merge(
        worker_count,
        repeate_happen_problem_data,
        how='inner',
        on='TYPE3'
    )
    # 计算分数,初始扣0分
    repeate_happen_problem_data['SCORE'] = 0
    repeate_happen_problem_data['REAL_THRESHOLD_VALUES'] = 0
    repeate_happen_problem_data['BASE_THRESHOLD_VALUES'] = 0
    repeate_happen_problem_data['CONTENT'] = ''
    deduct_config_dict = {
        # 当前月
        0: {
            1: 3,
            2: 1.5,
            3: 0.6
        },
        # 前一个月
        1: {
            1: 2,
            2: 1,
            3: 0.4
        },
        # 前2个月
        2: {
            1: 1,
            2: 0.5,
            3: 0.2
        }
    }
    risk_desc = {
        1: '重大风险',
        2: '较严重风险',
        3: '一般风险'
    }
    condition.update({'MON': calc_month})
    content_description_title = dict()
    for idx, row in repeate_happen_problem_data.iterrows():
        _base_threshold_values = 20
        risk_level = int(row['RISK_LEVEL'])
        if risk_level == 1:
            _base_threshold_values = 2
        elif risk_level == 2:
            _base_threshold_values = 5
        elif risk_level == 3:
            _base_threshold_values = 20
        else:
            pass
        _real_threshold_values = _base_threshold_values * \
            row['DP_COEFFICIENT'] * row['EXPOSURE_COEFFICIENT']
        # 向上取整
        _real_threshold_values = math.ceil(_real_threshold_values)
        _str = "<p>{0}：实际阀门值：{1}"+\
                "（基础阀门值{2}*单位系数{3}*问题暴露度系数{4}）</p>"
        if row['TYPE3'] not in content_description_title.keys():
            content_description_title[row['TYPE3']] = dict()
            content_description_title[row['TYPE3']][risk_level] = _str.format(risk_desc[risk_level],
                _real_threshold_values, _base_threshold_values, round(row['DP_COEFFICIENT'], 4),
                round(row['EXPOSURE_COEFFICIENT'], 4))
        else:
            if risk_level not in content_description_title[row['TYPE3']].keys():
                content_description_title[row['TYPE3']][risk_level] = _str.format(risk_desc[risk_level],
                _real_threshold_values, _base_threshold_values, round(row['DP_COEFFICIENT'], 4),
                round(row['EXPOSURE_COEFFICIENT'], 4))
        # 求a值扣分, 目标月超出实际阀门值才继续其他月份扣分
        _deduct_score = 0
        count_col_list = ['PROBLEM_COUNT', 'PROBLEM_COUNT_2_MONTH', 'PROBLEM_COUNT_3_MONTH']
        for i, month_pro_count in enumerate(count_col_list):
            if row[month_pro_count] > _real_threshold_values:
                _deduct_score += deduct_config_dict[i][risk_level]
            else:
                break
        if _deduct_score > 0:
            _score = _deduct_score / row['EXPOSURE_COEFFICIENT']
            repeate_happen_problem_data.loc[idx,
                                            'REAL_THRESHOLD_VALUES'] = _real_threshold_values
            repeate_happen_problem_data.loc[idx,
                                            'BASE_THRESHOLD_VALUES'] = _base_threshold_values
            repeate_happen_problem_data.loc[idx, 'SCORE'] = _score
            repeate_happen_problem_data.loc[idx, 'CONTENT'] = f"<p>问题项({str(row['FK_PROBLEM_BASE_ID']).strip()})</p>"+\
                f"<p>描述: {row['PROBLEM_POINT'].strip()}</p>" +\
                f"<p>风险级别:{risk_desc[risk_level]}</p>"+\
                f"<p>当月发生{row['PROBLEM_COUNT']}次,上个月发生{row['PROBLEM_COUNT_2_MONTH']}次," +\
                f"前2月发生{row['PROBLEM_COUNT_3_MONTH']}次.</p>"
        else:
            continue

    # 按站段将文本内容组合
    rst_data = []
    for type3 in set(repeate_happen_problem_data['TYPE3'].values.tolist()):
        desc_title = sorted(content_description_title[type3].items(), key=lambda x:x[0])
        desc_title = [_title[1] for _title in desc_title]
        desc_title = ''.join(desc_title)
        content_df = repeate_happen_problem_data[repeate_happen_problem_data['TYPE3'] == type3]
        lsiss = [item for item in content_df['CONTENT'].values.tolist()
                 if item]
        lsiss.insert(0, desc_title)
        content = '不存在连续重复发生的问题'
        if lsiss:
            content = '\n'.join(lsiss)
        score = float(content_df['SCORE'].sum())
        rst_data.append([type3, score, content])
    data = pd.DataFrame(data=rst_data, columns=[
                        'FK_DEPARTMENT_ID', 'SCORE', 'CONTENT'])
    # 删除引用
    del repeate_happen_problem_data
    # 导出中间过程数据
    rst = pd.merge(
        choose_dpid_data(3),
        data,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_rst = format_export_basic_data(rst, 6, 3,
                                        3, months_ago, index_type=index_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 3, index_type=index_type)
    data = df_merge_with_dpid(data, department_data)

    rst_child_score = calc_child_index_type_sum(
        data,
        index_type,
        6,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: max(100 - x, 0),
        choose_dpid_data,
        NA_value=True)
    return rst_child_score
