#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json

import pandas as pd
from pymongo import HASHED

from app import mongo
from app.data.util import pd_query, write_bulk_mongo
from app.utils.decorator import init_wrapper


def get_department():
    # 如果是初始化更新，则不需要每个月数据都更新部门表，
    # 如果是月更新，则需要更新部门表， 保证部门信息也同步更新
    doc_count = mongo.db['base_department'].estimated_document_count()
    if doc_count > 0:
        return
    SQL_DEPARTMENT = """SELECT
            a.DEPARTMENT_ID,
            a.SOURCE_PARENT_ID,
            b.NAME AS PARENT_NAME,
            b.HIERARCHY AS PARENT_HIERARCHY,
            a.NAME,
            a.ALL_NAME,
            a.HIERARCHY,
            a.TYPE,
            a.TYPE1,
            a.TYPE2,
            a.TYPE3,
            a.TYPE4,
            a.TYPE5,
            a.TYPE6,
            a.TYPE7,
            a.IS_DELETE,
            a.SHORT_NAME
        FROM
            t_department as a
        LEFT JOIN
            t_department AS b ON b.DEPARTMENT_ID = a.SOURCE_PARENT_ID
           ;
    """
    data = pd_query(SQL_DEPARTMENT)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': data['DEPARTMENT_ID'],
            'MAJOR': data['NAME']
        }),
        how='left',
        left_on='TYPE2',
        right_on='DPID')
    majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    data = data.loc[data['MAJOR'].isin(majors)]
    del data['DPID']
    result = []
    for index in data.index:
        value = data.loc[index]
        result.append(json.loads(value.to_json()))
    mongo.db.base_department.drop()
    write_bulk_mongo('base_department', result)
    mongo.db.base_department.create_index([('DPID', HASHED)], background=True)


@init_wrapper
def execute(months_ago=0):
    get_department()
    return 'OK'


if __name__ == '__main__':
    pass
