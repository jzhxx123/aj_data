#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json

import pandas as pd
from flask import current_app

from app import mongo
from app.data.util import (get_coll_prefix, get_history_months,
                           get_previous_month, pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper

sql_dep = "SELECT DEPARTMENT_ID, HIERARCHY TYPE3, TYPE4, \
                TYPE5 FROM t_department"

sql_person = "SELECT PERSON_NAME, ID_CARD FROM t_person"


def data_format(hour):
    hour = int(str(hour)[11:13])
    hour_section = [[0, 6], [6, 8], [8, 12], [12, 14], [14, 18], [18, 20],
                    [20, 24]]
    for idx, val in enumerate(hour_section):
        if hour >= val[0] and hour < val[1]:
            return idx + 1


def sure_chewu_own(_ids):
    '''车务所属'''
    if _ids is not None and len(_ids.split(',')) > 1:
        rst = []
        for _id in _ids.split(','):
            if int(_id) == 1:
                rst.append("客运")
            elif int(_id) == 2:
                rst.append("货运")
            elif int(_id) == 3:
                rst.append("运输")
            elif int(_id) == 4:
                rst.append("其他")
        result = ','.join(rst)
    elif _ids is not None and len(_ids) == 1:
        if int(_ids) == 1:
            a = "客运"
        elif int(_ids) == 2:
            a = "货运"
        elif int(_ids) == 3:
            a = "运输"
        elif int(_ids) == 4:
            a = "其他"
        result = a
    else:
        result = ''
    return result


def get_detail_data_while(mon_point):
    data = []
    SQL_CHECK_PROBLEM = """SELECT
            a.PK_ID,
            a.SERIOUS_VALUE,
            a.IS_RED_LINE,
            a.DESCRIPTION,
            a.PROBLEM_SCORE,
            a.PROBLEM_DIVIDE_NAMES,
            a.FK_CHECK_INFO_ID,
            a.CHEWU_OWN,
            a.PROBLEM_POINT,
            a.CHECK_ITEM_NAME,
            a.RISK_NAMES,
            a.RISK_CONSEQUENCE_NAMES,
            a.PROBLEM_CLASSITY_NAME,
            a.ID_CARD,
            a.TYPE,
            a.CHECK_PERSON_ID_CARD AS CHECK_ID_CARD,
            a.IS_SPAN_DEPARTMENT,
            a.IS_EXTERNAL,
            a.IS_ASSESS,
            b.RISK_LEVEL,
            b.ASSESS_MONEY,
            b.LEVEL,
            b.FK_PROBLEM_CLASSIFY_ID AS PROBLEM_CLASSIFY,
            c.FK_RISK_CONSEQUENCE_ID AS RISK_CONSEQUENCE,
            c.RISK_CONSEQUENCES_NAME AS RISK_CONSEQUENCE_NAME,
            d.FK_RISK_ID AS RISK_TYPE,
            d.RISK_NAME,
            e.END_CHECK_TIME,
            e.CHECK_TYPE,
            e.IS_DONGCHE,
            e.IS_KECHE,
            e.CHECK_WAY,
            e.DEPARTMENT_ALL_NAME,
            e.CHECK_ITEM_NAMES,
            e.RISK_NAME AS CHECK_RISK,
            f.FK_DEPARTMENT_ID AS DPID,
            f.ALL_NAME,
            g.TYPE2,
            g.TYPE3,
            g.TYPE4,
            g.TYPE5,
            h.ID_CARD AS RESPONSIBILITY_ID_CARD,
            h.RESPONSIBILITY_SCORE,
            h.RESPONSIBILITY_LEVEL
        FROM
            t_check_problem AS a
                LEFT JOIN
            t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
                LEFT JOIN
            t_problem_base_risk_consequence AS c
                        ON c.FK_PROBLEM_BASE_ID = b.PK_ID
                LEFT JOIN
            t_problem_base_risk AS d
                        ON d.FK_PROBLEM_BASE_ID = b.PK_ID
                LEFT JOIN
            t_check_info AS e ON a.FK_CHECK_INFO_ID = e.PK_ID
                INNER JOIN
            t_check_problem_and_responsible_department AS f
                        ON a.PK_ID = f.FK_CHECK_PROBLEM_ID
                LEFT JOIN
            t_department AS g ON g.DEPARTMENT_ID = f.FK_DEPARTMENT_ID
                LEFT JOIN
            t_check_problem_and_responsibility_person AS h
                        ON h.FK_CHECK_PROBLEM_ID = a.PK_ID
        WHERE e.END_CHECK_TIME >= '{0}' AND e.END_CHECK_TIME < '{1}'
    """.format(mon_point[0], mon_point[1])
    data = pd_query(SQL_CHECK_PROBLEM)
    return data


def get_detail_data(mon_point):
    data = get_detail_data_while(mon_point)
    # for dtype in ['float','int','object']:
    #     selected_dtype = data.select_dtypes(include=[dtype])
    #     mean_usage_b = selected_dtype.memory_usage(deep=True).mean()
    #     mean_usage_mb = mean_usage_b / 1024 ** 2
    #     current_app.logger.debug("Average memory usage for {} columns:
    # {:03.2f} MB".format(dtype,mean_usage_mb))
    # exit(-1)

    if data.empty or data.iloc[:, 0].size == 0:
        current_app.logger.debug(
            'detail_check_problem[{}] fetch 0 records.'.format(mon_point[0]))
        return
    current_app.logger.debug('complete: get data from mysql.')
    data['DATE'] = data['END_CHECK_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = int(f'{mon_point[1].year}{mon_point[1].month:0>2}')
    data['hour_section'] = data['END_CHECK_TIME'].apply(data_format)
    data['PROBLEM_CLASSIFY'] = data['PROBLEM_CLASSIFY'].apply(
        lambda x: int(x) if x > 0 else -1)
    data['RISK_CONSEQUENCE'] = data['RISK_CONSEQUENCE'].apply(
        lambda x: int(x) if x > 0 else -1)
    data['RISK_LEVEL'] = data['RISK_LEVEL'].apply(
        lambda x: int(x) if x > 0 else -1)
    data['RISK_TYPE'] = data['RISK_TYPE'].apply(
        lambda x: int(x) if x > 0 else -1)
    data['weekday'] = data['END_CHECK_TIME'].apply(lambda x: x.weekday() + 1)
    data['OWN'] = data['CHEWU_OWN'].apply(sure_chewu_own)
    del data['END_CHECK_TIME'], data['CHEWU_OWN']
    major_sql = "SELECT DEPARTMENT_ID, NAME FROM t_department;"
    major_data = pd_query(major_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'MAJOR': major_data['NAME']
        }),
        how='left',
        left_on='TYPE2',
        right_on='DPID')
    del data['TYPE2']
    del data['DPID_x']
    del data['DPID_y']
    coll_name = '{}detail_check_problem'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
    mongo.db[coll_name].create_index(
        [('ID_CARD', 1), ('MON', 1)], background=True)
    mongo.db[coll_name].create_index(
        [('MAJOR', 1), ('MON', 1)], background=True)


def handle(mon_point):
    current_app.logger.debug(
        f'handle ({mon_point[0]}, {mon_point[1]}) data.')
    get_detail_data(mon_point)
    get_map_data(mon_point)


def get_map_data_while(mon_point):
    data = []
    SQL_CHECK_PROBLEM = """
        SELECT
            a.PK_ID,
            a.SERIOUS_VALUE,
            a.RISK_NAMES,
            a.RISK_LEVEL,
            a.PROBLEM_SCORE,
            b.END_CHECK_TIME,
            c.FK_DEPARTMENT_ID AS DPID
        FROM
            t_check_problem AS a
                LEFT JOIN
            t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
                INNER JOIN
            t_check_problem_and_responsible_department AS c
        ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
        WHERE b.END_CHECK_TIME >= '{0}' AND b.END_CHECK_TIME < '{1}';
    """.format(mon_point[0], mon_point[1])
    data = pd_query(SQL_CHECK_PROBLEM)
    return data


def get_map_data(mon_point):
    data = get_map_data_while(mon_point)
    if data.empty or data.iloc[:, 0].size == 0:
        current_app.logger.debug(
            'map_check_problem[{}] fetch 0 records.'.format(mon_point[0]))
        return
    data = data.loc[::]
    current_app.logger.debug('complete: get data from mysql.')
    data['MON'] = int(f'{mon_point[1].year}{mon_point[1].month:0>2}')
    data['DATE'] = data['END_CHECK_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    del data['END_CHECK_TIME']

    sql_dep = "SELECT DEPARTMENT_ID, TYPE2, TYPE3, TYPE4, \
                TYPE5 FROM t_department"

    dep = pd_query(sql_dep)
    data = pd.merge(
        data, dep, how='left', left_on="DPID", right_on="DEPARTMENT_ID")
    del data['DEPARTMENT_ID']
    dep_data = pd_query("SELECT DEPARTMENT_ID, \
                NAME AS STATION FROM t_department")
    data = pd.merge(
        data, dep_data, how='left', left_on='TYPE3', right_on='DEPARTMENT_ID')
    del data['DEPARTMENT_ID']
    maj_data = pd_query(
        "SELECT DEPARTMENT_ID, NAME AS MAJOR FROM t_department")
    data = pd.merge(
        data, maj_data, how='left', left_on='TYPE2', right_on='DEPARTMENT_ID')
    del data['DEPARTMENT_ID']
    result = []
    for x in range(3, 6):
        xdata = data.groupby(['TYPE{}'.format(x),
                              'MON'])['SERIOUS_VALUE'].agg('sum')
        for index in xdata.index:
            value = xdata.loc[index]
            result.append({
                'DPID': index[0],
                'MON': index[1],
                'COUNT': int(value),
            })
    data['TIMES'] = 1
    data['SERIOUS_COUNT'] = data['RISK_LEVEL'].apply(
        lambda x: 1 if x in [1, 2] else 0)
    ydata = data.groupby(['DATE', 'MAJOR', 'STATION']).sum()
    for index in ydata.index:
        value = ydata.loc[index]
        result.append({
            'DATE': index[0],
            'MAJOR': index[1],
            'STATION': index[2],
            'TIMES': int(value['TIMES']),
            'SERIOUS_COUNT': int(value['SERIOUS_COUNT']),
            'SCORE': int(value['PROBLEM_SCORE']),
        })
    current_app.logger.debug('complete: format data.')
    coll_name = '{}map_check_problem'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def get_condition_item():
    # 获取检查问题筛选条件的动态配置项，并写入数据库
    doc_count = mongo.db['base_check_problem_item'].estimated_document_count()
    if doc_count > 0:
        return
    SQL = """SELECT
                PK_ID, CODE, NAME
            FROM
                t_dictionary
            WHERE
                CODE IN ('risk_consequence' ,
                    'problem_classify',
                    'problem_level');
        """
    result = []
    result.append({
        'TYPE':
        'risk_level',
        'MAJOR':
        'global',
        'VALUE': [{
            'ID': 1,
            'NAME': '重大安全风险'
        }, {
            'ID': 2,
            'NAME': '较大安全风险'
        }, {
            'ID': 3,
            'NAME': '一般安全风险'
        }, {
            'ID': 4,
            'NAME': '低安全风险'
        }]
    })
    data = pd_query(SQL)
    for key, group in data.groupby('CODE'):
        items = []
        for idx in group.index:
            PK_ID = int(group.ix[idx, 'PK_ID'])
            # t_problem_base表里problem_lelve没有用id，而是直接用的NAME
            if key == 'problem_level':
                PK_ID = group.at[idx, 'NAME']
            items.append({
                # numpy.int64 转化成 int，否则报bson.error.InvalidDocument
                'ID': PK_ID,
                'NAME': group.at[idx, 'NAME']
            })
        result.append({'TYPE': key, 'VALUE': items, 'MAJOR': 'global'})
    SQL = """SELECT DISTINCT
                a.PK_ID, a.NAME, b.NAME AS MAJOR
            FROM
                t_risk AS a
                    LEFT JOIN
                t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            WHERE
                a.HIERARCHY = 1 AND a.IS_DELETE = 0;
        """
    data = pd_query(SQL)
    for key, group in data.groupby('MAJOR'):
        items = []
        for idx in group.index:
            PK_ID = int(group.ix[idx, 'PK_ID'])
            items.append({'ID': PK_ID, 'NAME': group.at[idx, 'NAME']})
        result.append({'TYPE': 'risk_type', 'VALUE': items, 'MAJOR': key})
    mongo.db.base_check_problem_item.drop()
    write_bulk_mongo('base_check_problem_item', result)


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    get_condition_item()
    return 'OK'


if __name__ == '__main__':
    current_app.logger.debug('Done!')
