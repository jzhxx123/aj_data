# 检查问题
CHECK_PROBLEM_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, f.LEVEL,
        f.RISK_LEVEL, f.CHECK_SCORE, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
"""


# 发生事故隐患问题次数
HIDDEN_KEY_PROBLEM_MONTH_SQL = """SELECT DISTINCT
        c.TYPE3 AS FK_DEPARTMENT_ID, COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE f.IS_HIDDEN_KEY_PROBLEM = 1
            AND f.`TYPE` = 3
            AND f.`STATUS` = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND c.`TYPE` = 4
            AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    GROUP BY c.TYPE3
"""

# 出现问题的部门（应该取的是班组）
# 这里因为多个连接查询筛选班组耗时比较多，而且班组数占出现问题部门的大部分，
# 所以这部分筛选先放进程序代码里筛选
EXPOSURE_PROBLEM_DEPARTMENT_SQL = """SELECT
        DISTINCT a.FK_DEPARTMENT_ID
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=b.FK_CHECK_INFO_ID
    WHERE
        b.FK_CHECK_ITEM_ID IN ({2})
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
"""

# 他查问题
# 筛选项:责任部门、是否路外:否、
# 检查方式:问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、
# 月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:是
OTHER_CHECK_PROBLEM_SQL = """SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 1
    AND c.CHECK_WAY BETWEEN 1 AND 4
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND c.END_CHECK_TIME BETWEEN '{0}' AND '{1}'
"""

# 自查问题
# 筛选项:检查部门、是否路外:否、
# 问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:否
SELF_CHECK_PROBLEM_SQL = """SELECT
    DISTINCT
        CONCAT(d.PK_ID,
            '||',
            a.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
            LEFT JOIN
        t_problem_base AS d ON a.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE a.RISK_LEVEL <= 3
    AND a.IS_EXTERNAL = 0
    AND a.TYPE > 2
    AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND b.END_CHECK_TIME BETWEEN '{0}' AND '{1}' 
"""

# 安全生产信息问题
SAFETY_PRODUCE_INFO_SQL = """SELECT
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE
        a.RANK BETWEEN 1 AND 3
            AND d.FK_CHECK_ITEM_ID IN ({2})
            AND a.OCCURRENCE_TIME BETWEEN '{0}' AND '{1}'
"""

