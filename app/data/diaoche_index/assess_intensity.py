#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
import pandas as pd
from flask import current_app

from app.data.diaoche_index._calc_cardinal_number import assess_intensity_cardinal_number
from app.data.diaoche_index.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL,
    GANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.diaoche_index.common import calc_work_load, add_avg_number_by_major_for_check_count_per_person
from app.data.diaoche_index.common_sql import (
    CADRE_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    KAOHE_GL_CHECK_PROBLEM_SQL, KAOHE_ZY_CHECK_PROBLEM_SQL, WORK_LOAD_SQL,
    WORKER_COUNT_SQL, ZHANDUAN_DPID_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_extra_child_score_groupby_major,
    combine_and_format_basic_data_to_mongo, combine_child_index_func,
    df_merge_with_dpid, format_export_basic_data, summizet_child_index,
    summizet_operation_set, write_export_basic_data_to_mongo, calc_extra_child_score_groupby_major_two)
from app.data.index.util import (
    get_custom_month, get_query_condition_by_risktype, validate_exec_month)
from app.data.major_risk_index.util import write_cardinal_number_basic_data, calc_extra_child_score_groupby_major_third
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type, major_ratio_dict={}):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]

    if _ratio > 0.1 or _ratio >= -0.1:
        _score = 90
    else:
        deduction = _ratio * 100
        if detail_type == 3:
            deduction *= -1
        _score = 100 + deduction
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_score_for_check_count_per_person(row, major_ratio_dict):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    if row['NAME'] in ['贵阳车站', '重庆车站', '成都车站']:
        major_ratio = major_ratio_dict.get('三大车站')
    else:
        major_ratio = major_ratio_dict.get('调车')
    self_ratio = row['ratio']
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    N1 = major_ratio[0][0]
    Sn1 = major_ratio[0][1]
    N2 = major_ratio[1][0]
    Sn2 = major_ratio[1][1]
    N3 = major_ratio[2][0]
    Sn3 = major_ratio[2][1]
    C = self_ratio
    if level == 1:
        score = Sn1
    elif level == 2:
        score = Sn2 + (C - N2) * (Sn1 - Sn2) / (N1 - N2)
    elif level == 3:
        score = Sn3 + (C - N3) * ((Sn2 - Sn3) / (N2 - N3))
    else:
        score = Sn3 + (C - N3) * (((Sn2 - Sn3) / (N2 - N3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score


def _get_sql_data(months_ago):
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, ASSESS_ZUOYE_PROBLEM_COUNT, ASSESS_GUANLI_PROBLEM_COUNT, \
        CADRE_ASSESS_RESPONSIBLE_MONEY, WORKER_ASSESS_RESPONSIBLE_MONEY, \
        CADRE_COUNT, WORKER_COUNT, CHILD_INDEX_SQL_DICT

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    CHILD_INDEX_SQL_DICT = assess_intensity_cardinal_number(months_ago, 2, '车务-1')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(2)
    check_item_ids = diaoche[0]
    # diaoche_position = ','.join(
    #     [f'"{postion}"' for postion in diaoche[2].split(',')])
    # diaoche_position = ','.join(
    #     [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        calc_work_load(WORK_LOAD_SQL, month), DEPARTMENT_DATA)
    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL), DEPARTMENT_DATA)

    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL), DEPARTMENT_DATA)

    # 考核作业项问题数
    ASSESS_ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_ZY_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核管理项问题数
    ASSESS_GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_GL_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month)), DEPARTMENT_DATA)
    # 月度考核总金额（干部）
    CADRE_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(GANBU_ASSESS_RESPONSIBLE_SQL.format(year, month)),
        DEPARTMENT_DATA)
    # 月度考核总金额（非干部）
    WORKER_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(FEIGANBU_ASSESS_RESPONSIBLE_SQL.format(year, month)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_check_problem_val_person(series, hierarchy, idx, months_ago, major_ratio_dict):
    global WORK_LOAD
    work_load = WORK_LOAD.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = add_avg_number_by_major_for_check_count_per_person(months_ago, data, _choose_dpid_data(hierarchy),
                                                                  main_type=1, detail_type=3)
    rst_data['SCORE'] = rst_data.apply(
        lambda row: _calc_score_for_check_count_per_person(row, major_ratio_dict), axis=1)
    rst_data = pd.DataFrame(
        index=rst_data['DEPARTMENT_ID'],
        data=rst_data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    # 中间计算数据
    title = ['作业项调车问题考核数({0})/调车工作量({1})', '管理项调车问题考核数({0})/调车工作量({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_zuoye_check_problem(hierarchy, idx, months_ago, major_ratio_dict):
    """作业项考核问题数"""
    global ASSESS_ZUOYE_PROBLEM_COUNT
    zuoye_problem = ASSESS_ZUOYE_PROBLEM_COUNT
    zuoye_problem = zuoye_problem.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(zuoye_problem, hierarchy, idx, months_ago, major_ratio_dict)


def _calc_guanli_check_problem(hierarchy, idx, months_ago, major_ratio_dict):
    """管理项考核问题数"""
    global ASSESS_GUANLI_PROBLEM_COUNT
    guanli_problem = ASSESS_GUANLI_PROBLEM_COUNT
    guanli_problem = guanli_problem.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(guanli_problem, hierarchy, idx, months_ago, major_ratio_dict)


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    rst_child_score = []
    major_ratio_dict = {'调车': [(0.012, 100), (0.008, 90), (0.004, 60)],
                        '三大车站': [(0.008, 100), (0.0065, 90), (0.003, 60)]}
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_zuoye_check_problem, _calc_guanli_check_problem]
        child_weight = [0.8, 0.2]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(hierarchy, idx, months_ago, major_ratio_dict)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 3, 1, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 3, 1, risk_type='车务-1')
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            3,
            1,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_assess_money_per_person(series_numerator, series_denominator,
                                  hierarchy, idx, fraction=None):
    if series_denominator.empty or series_numerator.empty:
        return False
    data = pd.concat(
        [
            series_numerator.to_frame(name='left'),
            series_denominator.to_frame(name='right')
        ],
        axis=1,
        sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['left'] / data['right']
    if fraction:
        write_cardinal_number_basic_data(data, _choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, 3,
                                         2, fraction.months_ago,
                                         columns=['left', 'right'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, _choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula,
        numerator='left', denominator='right', fraction=fraction)
    # 中间计算数据
    title = [
        '干部月度考核总金额({0})/干部总人数({1})',
        '职工月度考核总金额({0})/职工总人数(非干部)({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["left"], 2)}', row['right']),
        axis=1)
    data.drop(columns=['left', 'ratio', 'right'], inplace=True, axis=1)
    return rst_data, data


def _calc_worker_assess_money(hierarchy, idx, fraction=None):
    """职工月度考核总金额/职工总人数"""
    global WORKER_COUNT, WORKER_ASSESS_RESPONSIBLE_MONEY
    worker_count = WORKER_COUNT
    assess_money = WORKER_ASSESS_RESPONSIBLE_MONEY
    worker_count = worker_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    assess_money = assess_money.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_assess_money_per_person(assess_money, worker_count, hierarchy,
                                         idx, fraction=fraction)


def _calc_cadre_assess_money(hierarchy, idx, fraction=None):
    """干部月度考核总金额/干部总人数"""
    global CADRE_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY
    cader_count = CADRE_COUNT
    assess_money = CADRE_ASSESS_RESPONSIBLE_MONEY
    cader_count = cader_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    assess_money = assess_money.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_assess_money_per_person(assess_money, cader_count, hierarchy,
                                         idx, fraction=fraction)


# 换算单位考核金额
def _stats_assess_money_per_person(months_ago):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_cadre_assess_money, _calc_worker_assess_money]
        child_weight = [0.2, 0.8]
        fraction_list = CHILD_INDEX_SQL_DICT.get('3-2')
        for idx, ifunc in enumerate(child_func):
            rst_func = ifunc(hierarchy, idx, fraction_list[idx])
            if rst_func is False:
                continue
            rst_data, rst_basic_data = rst_func
            calc_basic_data.append(rst_basic_data)
            if rst_data is not None:
                score.append(rst_data * child_weight[idx])
        if score == []:
            continue
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 3, 2, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 3, 2, risk_type='车务-1')
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_b_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            3,
            2,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


# 返奖率
def _stats_award_return_ratio(months_ago):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    if AWARD_RETURN_MONEY.empty:
        return None
    fraction_list = CHILD_INDEX_SQL_DICT.get('3-3')
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = AWARD_RETURN_MONEY[AWARD_RETURN_MONEY['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby(['DEPARTMENT_ID']).size()
        prob_number = idata.groupby(['DEPARTMENT_ID']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        if fraction_list[i]:
            write_cardinal_number_basic_data(
                idata, ZHANDUAN_DPID_DATA, fraction_list[i],
                fraction_list[i].risk_type, 3, 3, fraction_list[i].months_ago,
                columns=['award', 'prob']
            )
        rst_child_data = calc_extra_child_score_groupby_major_third(
            idata.copy(),
            _choose_dpid_data(3),
            'ratio',
            _calc_score_by_formula,
            weight=child_weight[i],
            detail_type=3,
            numerator='award',
            denominator='prob', fraction=fraction_list[i])
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 问题个数（{2}）'
                .format(title[i], row['award'], row['prob']),
            axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data,
        _choose_dpid_data(3),
        months_ago,
        3,
        3,
        3,
        risk_type='车务-1')
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(df_rst, _choose_dpid_data(3), column, 3, 2, 3, 3,
                           months_ago, risk_type='车务-1')
    return [df_rst]


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.45, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type='车务-1')

    update_major_maintype_weight(index_type=1, major='车务-1', main_type=3,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
