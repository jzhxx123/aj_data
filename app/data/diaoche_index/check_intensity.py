# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.diaoche_index._calc_cardinal_number import check_intensity_cardinal_number
from app.data.diaoche_index.check_intensity_sql import (
    BANZU_POINT_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, REAL_CHECK_BANZU_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL)
from app.data.diaoche_index.common import calc_work_load, calc_check_count_per_person, \
    add_avg_number_by_major_for_check_count_per_person
from app.data.diaoche_index.common_sql import (
    CHECK_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    GUANLI_CHECK_PROBLEM_SQL, KAOHE_GL_CHECK_PROBLEM_SQL,
    KAOHE_ZY_CHECK_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL,
    QUANTIZATION_PERSON_SQL, RISK_LEVEL_PROBLEM_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL, ZUOYE_CHECK_PROBLEM_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_extra_child_score_groupby_major, combine_child_index_func,
    df_merge_with_dpid, format_export_basic_data, summizet_child_index,
    summizet_operation_set, write_export_basic_data_to_mongo,
    calc_extra_child_score_groupby_major_two)
from app.data.index.util import (
    get_custom_month, get_query_condition_by_risktype, validate_exec_month)
from app.data.major_risk_index.util import write_cardinal_number_basic_data, calc_child_index_type_divide_major, \
    calc_extra_child_score_groupby_major_third
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None, major_ratio_dict={}):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_for_check_count_per_person(row, major_ratio_dict):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    if row['NAME'] in ['贵阳车站', '重庆车站', '成都车站']:
        major_ratio = major_ratio_dict.get('三大车站')
    else:
        major_ratio = major_ratio_dict.get('调车')
    self_ratio = row['ratio']
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    N1 = major_ratio[0][0]
    Sn1 = major_ratio[0][1]
    N2 = major_ratio[1][0]
    Sn2 = major_ratio[1][1]
    N3 = major_ratio[2][0]
    Sn3 = major_ratio[2][1]
    C = self_ratio
    if level == 1:
        score = Sn1
    elif level == 2:
        score = Sn2 + (C - N2) * (Sn1 - Sn2) / (N1 - N2)
    elif level == 3:
        score = Sn3 + (C - N3) * ((Sn2 - Sn3) / (N2 - N3))
    else:
        score = Sn3 + (C - N3) * (((Sn2 - Sn3) / (N2 - N3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score


def _get_sql_data(months_ago):
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, QUANTIZATION_PERSON, CHECK_COUNT, GUANLI_PROBLEM_COUNT, \
        ZUOYE_PROBLEM_COUNT, ASSESS_ZUOYE_PROBLEM_COUNT, \
        ASSESS_GUANLI_PROBLEM_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, CHILD_INDEX_SQL_DICT, \
        CHECK_ITEM_IDS
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    CHILD_INDEX_SQL_DICT = check_intensity_cardinal_number(months_ago, 2, '车务-1')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(2)
    CHECK_ITEM_IDS = diaoche[0]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        calc_work_load(WORK_LOAD_SQL, month), DEPARTMENT_DATA)

    # 量化人员数
    QUANTIZATION_PERSON = df_merge_with_dpid(
        pd_query(
            QUANTIZATION_PERSON_SQL.format(year, month, diaoche_position)),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 考核作业项问题数
    ASSESS_ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_ZY_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 考核管理项问题数
    ASSESS_GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_GL_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
            *stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 换算单位检查频次
def _stats_check_per_person(months_ago):
    major_ratio_dict = {
        '调车': [(0.07, 100), (0.06, 90), (0.03, 60)],
        '三大车站': [(0.05, 100), (0.035, 90), (0.01, 60)]
    }

    return calc_check_count_per_person(
        CHECK_COUNT, WORK_LOAD, 2, 1, 2, months_ago, 'COUNT', 'SCORE_b',
        _calc_score_for_check_count_per_person, _choose_dpid_data,
        major_ratio_dict=major_ratio_dict, risk_type='车务-1')


def _calc_check_problem_val_person(months_ago, series, hierarchy, idx, major_ratio_dict):
    global WORK_LOAD
    work_load = WORK_LOAD.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = add_avg_number_by_major_for_check_count_per_person(
        months_ago, data, _choose_dpid_data(hierarchy),
        main_type=1, detail_type=3)
    rst_data['SCORE'] = rst_data.apply(
        lambda row:
        _calc_score_for_check_count_per_person(row, major_ratio_dict), axis=1)
    rst_data = pd.DataFrame(
        index=rst_data['DEPARTMENT_ID'],
        data=rst_data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    # 中间计算数据
    title = ['作业项问题数({0})/调车工作量({1})', '管理项问题数({0})/调车工作量({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row:
        title[idx].format(f'{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_zuoye_check_problem_ratio(
        months_ago, hierarchy, idx, major_ratio_dict):
    """作业项问题查处率"""
    global ZUOYE_PROBLEM_COUNT
    zuoye_problem = ZUOYE_PROBLEM_COUNT
    zuoye_problem = zuoye_problem.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(
        months_ago, zuoye_problem, hierarchy, idx, major_ratio_dict)


def _calc_guanli_check_problem_ratio(
        months_ago, hierarchy, idx, major_ratio_dict):
    """管理项问题查处率"""
    global GUANLI_PROBLEM_COUNT
    guanli_problem = GUANLI_PROBLEM_COUNT
    guanli_problem = guanli_problem.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(
        months_ago, guanli_problem, hierarchy, idx, major_ratio_dict)


# 查出问题率
def _stats_check_problem_ratio(months_ago):
    """(作业项问题数*80% + 管理项问题数*20%)/调车工作量
    """
    rst_child_score = []
    major_ratio_dict = {
        '调车': [(0.008, 100), (0.007, 90), (0.0045, 60)],
        '三大车站': [(0.007, 100), (0.006, 90), (0.0045, 60)]
    }
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_zuoye_check_problem_ratio, _calc_guanli_check_problem_ratio
        ]
        child_weight = [0.8, 0.2]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                months_ago, hierarchy, idx, major_ratio_dict)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 3, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 3, risk_type='车务-1')
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_c_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            3,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_check_problem_assess_ratio(series_numerator, series_denominator,
                                     hierarchy, idx):
    data = pd.concat(
        [
            series_numerator.to_frame(name='left'),
            series_denominator.to_frame(name='right')
        ],
        axis=1,
        sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['left'] / data['right']
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_two(
        data, _choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula,
        numerator='left', denominator='right')
    # 中间计算数据
    title = [
        '作业项考核问题数({0})/作业项问题数({1})',
        '管理项考核问题数({0})/管理项问题数({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["left"], 2)}', row['right']),
        axis=1)
    data.drop(columns=['left', 'ratio', 'right'], inplace=True, axis=1)
    return rst_data, data


# 换算问题质量分
def _stats_score_per_person(months_ago):
    """问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    major_ratio_dict = {
        '调车': [(0.055, 100), (0.04, 90), (0.015, 60)],
        '三大车站': [(0.03, 100), (0.02, 90), (0.005, 60)]
    }

    return calc_check_count_per_person(
        CHECK_COUNT, WORK_LOAD, 2, 1, 5, months_ago, 'COUNT', 'SCORE_e',
        _calc_score_for_check_count_per_person, _choose_dpid_data,
        major_ratio_dict=major_ratio_dict, risk_type='车务-1')


def _calc_risk_score_per_person(series, hierarchy, idx, fraction=None):
    global WORK_LOAD
    work_load = WORK_LOAD.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['score'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(
            data, _choose_dpid_data(hierarchy), fraction,
            fraction.risk_type, 1,
            6, fraction.months_ago,
            columns=['score', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, _choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula,
        numerator='score', denominator='PERSON_NUMBER', fraction=fraction)
    # 中间计算数据
    title = [
        '较大和重大安全风险问题质量分累计({0})/调车工作量({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/调车工作量({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row:
        title[idx].format(f'{round(row["score"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_risk_score(hierarchy, idx, fraction=None):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    global XC_JIAODA_RISK_SCORE
    sc_risk_score = XC_JIAODA_RISK_SCORE
    sc_risk_score = sc_risk_score.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(
        sc_risk_score, hierarchy, idx, fraction=fraction)


def _calc_risk_score(hierarchy, idx, fraction=None):
    """较大和重大安全风险问题质量分累计"""
    global JIAODA_RISK_SCORE
    risk_score = JIAODA_RISK_SCORE
    risk_score = risk_score.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(
        risk_score, hierarchy, idx, fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    fraction_list = CHILD_INDEX_SQL_DICT.get('1-6')
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score, _calc_xc_risk_score]
        child_weight = [0.6, 0.4]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(hierarchy, idx, fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type='车务-1')
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


# 夜查率
def _stats_yecha_ratio(months_ago):
    """现场检查调车夜查次数/调车工作量
    现场检查调车夜查次数:
    数据源：安全监督检查->安全信息采集->检查信息查询->车务->**站段->条数
    筛选项:检查信息基础筛选项、检查方式:“现场检查”“添乘检查”、是否夜查:是
    """
    fraction = CHILD_INDEX_SQL_DICT.get('1-7')[0]
    return calc_child_index_type_divide_major(
        YECHA_COUNT,
        CHECK_COUNT,
        2,
        1,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type='车务-1',
        fraction=fraction)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """（检查地点数/有调车作业班组数)*100
    """
    stats_month = get_custom_month(months_ago)
    # 检查作业班组数(所有)
    data_real = pd_query(REAL_CHECK_BANZU_SQL.format(*stats_month))
    data_real = df_merge_with_dpid(data_real, DEPARTMENT_DATA)

    # 具有调车作业班组数
    data_total = pd_query(BANZU_POINT_SQL.format(CHECK_ITEM_IDS))
    data_total = df_merge_with_dpid(data_total, DEPARTMENT_DATA)

    # 检查调车作业班组数
    data_real = data_real[
        data_real.DEPARTMENT_ID.isin(data_total.DEPARTMENT_ID)]

    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        lambda x: min(100, x * 100),
        _choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type='车务-1')


def _calc_value_per_person(series, hierarchy):
    global STAFF_NUMBER
    data = pd.concat(
        [series.to_frame(name='prob'), STAFF_NUMBER], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    return calc_extra_child_score_groupby_major(
        data, _choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula)


def _calc_media_val_person(series, hierarchy, idx, fraction=None):
    work_load = WORK_LOAD.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(
            data, _choose_dpid_data(hierarchy), fraction,
            fraction.risk_type, 1,
            10, fraction.months_ago,
            columns=['media', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, _choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula,
        numerator='media', denominator='PERSON_NUMBER', fraction=fraction)
    # 中间计算数据
    title = [
        '监控调阅时长累计({0})/总人数({1})', '监控调阅发现问题数({0})/总人数({1})',
        '监控调阅发现问题质量分累计({0})/总人数({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row:
        title[idx].format(f'{round(row["media"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅人均时长
def _calc_media_time_per_person(stats_month, hierarchy, idx, fraction=None):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['TIME'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, fraction=fraction)


# 监控调阅人均问题个数
def _calc_media_problem_per_person(stats_month, hierarchy, idx, fraction=None):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, fraction=fraction)


# 监控调阅人均质量分
def _calc_media_score_per_person(stats_month, hierarchy, idx, fraction=None):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_PROBLME_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    fraction_list = CHILD_INDEX_SQL_DICT.get('1-10')

    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person, _calc_media_problem_per_person,
            _calc_media_score_per_person
        ]
        child_weight = [0.35, 0.35, 0.3]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                stats_month, hierarchy, idx, fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type='车务-1')
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        # _stats_check_problem_assess_radio,
        _stats_score_per_person,
        _stats_risk_score_per_person,
        _stats_yecha_ratio,
        _stats_check_address_ratio,
        _stats_media_intensity,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'g', 'i', 'j']]
    item_weight = [0.15, 0.15, 0.2, 0.2, 0.1, 0.05, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type='车务-1')
    update_major_maintype_weight(index_type=1, major='车务-1', main_type=1,
                                 child_index_list=[2, 3, 5, 6, 7, 9, 10],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
