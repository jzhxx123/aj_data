# 月度考核总金额
ASSESS_RESPONSIBLE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
    GROUP BY b.FK_DEPARTMENT_ID;
"""
        #     INNER JOIN
        # t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
        # AND c.FK_CHECK_ITEM_ID IN ({2})
# AND a.IS_OUT_SIDE_PERSON = 0

# 月度考核总金额(干部)
GANBU_ASSESS_RESPONSIBLE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY = '干部'
    GROUP BY b.FK_DEPARTMENT_ID;
"""
# t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
# AND c.FK_CHECK_ITEM_ID IN ({2})

# 月度考核总金额(非干部)
FEIGANBU_ASSESS_RESPONSIBLE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.ACTUAL_MONEY) AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY <> '干部'
    GROUP BY b.FK_DEPARTMENT_ID;
"""
#     INNER JOIN
# t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
# AND c.FK_CHECK_ITEM_ID IN ({2})

# 月度返奖金额
AWARD_RETURN_SQL = """SELECT
        b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
"""
        #     INNER JOIN
        # t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
        # AND c.FK_CHECK_ITEM_ID IN ({2})
