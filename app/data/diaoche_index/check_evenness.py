#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 检查均衡度指数
'''

import calendar

import pandas as pd
from flask import current_app

from app.data.diaoche_index.check_evenness_sql import (
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    CHECK_BANZU_PERSON_COUNT_SQL, BANZU_CONNECT_DEPARTMENT_SQL,
    BANZU_CHECKED_COUNT_SQL)
from app.data.diaoche_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_sum, combine_child_index_func, df_merge_with_dpid,
    summizet_child_index, summizet_operation_set,
    write_export_basic_data_to_mongo, format_export_basic_data,
    calc_child_index_type_divide_major)
from app.data.index.util import (
    get_custom_month, get_query_condition_by_risktype, validate_exec_month)
from app.data.major_risk_index.common.common import (
    get_major_dpid)
from app.data.util import (
    pd_query, get_history_months, update_major_maintype_weight)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column):
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


# 判断是否是周六或者周日
def _is_weekends(year, month, day):
    if day >= current_app.config.get('UPDATE_DAY'):
        if month == 1:
            month = 12
            year -= 1
        else:
            month -= 1
    if calendar.weekday(year, month, day) > 4:
        return True
    else:
        return False


def _get_daily_check_banzu_count(year, month, work_type_1, work_type_2,
                                 work_type_3, day):
    work_banzu = (work_type_1 + work_type_2 + work_type_3)
    if _is_weekends(year, month, int(day)):
        work_banzu -= work_type_1
    return work_banzu


def _cal_check_banzu_evenness_score(row, columns):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 保存中间计算过程
    calc_basic_data = {
        '日均班组检查数低于基数20%': 0,
        '日均班组检查数低于基数50%': 0,
        '日均班组检查数低于基数100%': 0,
    }
    score = [100]
    for day in columns:
        # 取某天的某个专业的班组数
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        # 取某天的某个专业的平均检查数
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
            calc_basic_data.update({
                '日均班组检查数低于基数100%':
                    calc_basic_data.get('日均班组检查数低于基数100%') + 1
            })
        elif ratio <= -0.5:
            daily_deduction = -2
            calc_basic_data.update({
                '日均班组检查数低于基数50%':
                    calc_basic_data.get('日均班组检查数低于基数50%') + 1
            })
        elif ratio <= -0.2:
            daily_deduction = -1
            calc_basic_data.update({
                '日均班组检查数低于基数20%':
                    calc_basic_data.get('日均班组检查数低于基数20%') + 1
            })
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {v}天' for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _cal_check_banzu_evenness_score_new(row, columns, basedata=None, day_content=None):
    """
    基准值（站段级）=Σ日检查次数/Σ日作业班组数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算站段基准值:
    total_check_count = sum([row[day] for day in columns])
    total_banzu_count = sum(
        [row[f'daily_banzu_count_{day}'] for day in columns])
    avg_check_count = total_check_count / total_banzu_count

    # 保存中间计算过程
    day_basic_data = {
        '检查数低于基数20%的日期': 0,
        '检查数低于基数50%的日期': 0,
        '检查数低于基数100%的日期': 0
    }
    calc_basic_data = {
        '检查数低于基数20%的日期': [],
        '检查数低于基数50%的日期': [],
        '检查数低于基数100%的日期': []
    }
    score = [100]
    for day in columns:
        # 取某天站段的班组数及检查数
        day_banzu_count = row[f'daily_banzu_count_{day}']
        day_check_count = row[day]
        # 比较值(基数)
        base_check_count = day_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (day_check_count - base_check_count) / base_check_count
        if ratio <= -1:
            daily_deduction = -3
            deduct_type = '检查数低于基数100%的日期'
        elif ratio <= -0.5:
            daily_deduction = -2
            deduct_type = '检查数低于基数50%的日期'
        elif ratio <= -0.2:
            daily_deduction = -1
            deduct_type = '检查数低于基数20%的日期'
        else:
            daily_deduction = 0
            deduct_type = None
        score.append(daily_deduction)
        deduct_day = calc_basic_data.get(deduct_type)
        if not deduct_type:
            continue
        if not day_content:
            day_content = '{0}号[基准值：{1:.3f},班组数：{2:.0f},比较值：{3:.3f},实际检查值：{4:.0f}]'
        deduct_day.append(day_content.format(day, avg_check_count, day_banzu_count, base_check_count, day_check_count))
        # deduct_day.append(f'{day}号[基准值：{avg_check_count:.3f},' +
        #                   f'班组数：{day_banzu_count:.0f}, ' +
        #                   f'比较值：{base_check_count:.3f}，' +
        #                   f'实际检查值：{day_check_count:.0f}]')
        calc_basic_data.update({deduct_type: deduct_day})
        day_basic_data.update(
            {deduct_type: day_basic_data.get(deduct_type) + 1})

    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {day_basic_data.get(k)}天<br/>{"<br/>".join(v)}' for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


# 部门按站段聚合
def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, CHECK_BANZU_COUNT, BANZU_CONNECT_DEPARTMENT, \
        BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_COUNT, CHECK_POINT_CONNECT_DEPARTMENT, CHECK_POINT_CHECKED_COUNT, \
        CHECK_ITEM_IDS

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    major = get_major_dpid('车务-1')
    CHECK_ITEM_IDS = get_query_condition_by_risktype(2)[0]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 检查班组数统计(具备检查项目的班组)
    CHECK_BANZU_COUNT = pd_query(
        CHECK_BANZU_PERSON_COUNT_SQL.format(major, CHECK_ITEM_IDS))

    # 班组关联站段(选取部门全称)
    BANZU_CONNECT_DEPARTMENT = pd_query(
        BANZU_CONNECT_DEPARTMENT_SQL.format(major))

    # 班组受检次数
    BANZU_DEPARTMENT_CHECKED_COUNT = pd_query(
        BANZU_CHECKED_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS))


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    rst_index_score = calc_child_index_type_divide_major(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT,
        2,
        4,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        lambda x: min(100, x * 100),
        _choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type='车务-1')
    return rst_index_score


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        # 每日检查班组数
        data = df_merge_with_dpid(
            pd_query(DAILY_CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS)),
            DEPARTMENT_DATA)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        xdata = xdata.unstack()
        for x in [1, 2, 3]:
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        xdata = xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(DAILY_CHECK_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)),
            DEPARTMENT_DATA)
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(ZHANDUAN_DPID_DATA, xdata)
        for day in columns:
            new_column = f'daily_banzu_count_{day}'
            xdata[new_column] = xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(row, columns)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(row, columns)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type='车务-1')
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type='车务-1')
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type='车务-1')
        rst_index_score.append(xdata)
    return rst_index_score


def _export_calc_address_evenness_data_new(
        data, months_ago, risk_type, weight_list=None, content=None):
    """
    将检查地点数据中间统计结果导出
        显示详细的地点信息
    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    :param data: data {pandas.DataFrame} -- 统计数据
    :param months_ago: {int} -- 前第 -N 个月
    :param risk_type: 指数类型
    :param weight_list: {list} 第一个为大于的比较率,第二为小与的比较率
    :return:
    """
    calc_data = {}
    major_data = {}
    high_ratio = weight_list[0] if weight_list else 4
    low_ratio = weight_list[1] if weight_list else -0.5
    address_count_dict = {
        '总检查地点数:': 0,
        '受检地点超过比较值{}00%以上地点:'.format(high_ratio): 0,
        '受检地点低于比较值{0:.0f}0%以上地点:'.format(low_ratio * -10): 0,
        '未检查地点:': 0
    }
    dpid_address_count = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['CHECK_COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        person_number = row['ADDRESS_PERSON_NUMBER']
        point_name = row['ADDRESS_NAME']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)

        if dpid not in dpid_address_count:
            dpid_address_count.update({dpid: address_count_dict.copy()})
        address_count = dpid_address_count.get(dpid)
        address_count.update(
            {'总检查地点数:': address_count.get('总检查地点数:', 0) + 1})

        if avg_check_count == 0:
            continue
        if not content:
            content = '{0}(比较值：{1:.0f}, 受检次数：{2:.0f}, 基准值：{3:.2f},工作人数：{4:.0f})'
        check_desc = content.format(
            point_name, avg_check_count, real_check_count,
            base_check_count, person_number)

        _ratio = (real_check_count - avg_check_count) / avg_check_count
        if int(real_check_count) == 0:
            deduct_type = '未检查地点:'
        elif _ratio >= high_ratio:
            deduct_type = '受检地点超过比较值{}00%以上地点:'.format(high_ratio)
        elif _ratio <= low_ratio:
            deduct_type = '受检地点低于比较值{0:.0f}0%以上地点:'.format(
                low_ratio * -10)
        else:
            dpid_address_count.update({dpid: address_count})
            calc_data.update({dpid: cnt})
            continue
        cnt.update(
            {deduct_type: cnt.get(deduct_type, '') + '<br/>' + check_desc})
        address_count.update(
            {deduct_type: address_count.get(deduct_type, 0) + 1})
        calc_data.update({dpid: cnt})
        dpid_address_count.update({dpid: address_count})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join(
                    [f'总检查地点数: {dpid_address_count.get(dpid).get("总检查地点数:")}个'] +
                    [f'{k} {dpid_address_count.get(dpid).get(k)}个{v}'
                     for k, v in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


def _calc_avg_check_count_by_zhanduan(data):
    """基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    地点工作人数=该地点工作的班组总人数
    应检查值=地点作业人数*基准值
    """
    # 计算站段总工作人数(作业人数PERSON_NUMBER为0时补充为部门人数PERSON_COUNT)
    data['PERSON_NUMBER'] = data.apply(
        lambda row: row['PERSON_NUMBER']
        if row['PERSON_NUMBER'] > 0 else row['PERSON_COUNT'], axis=1)
    zhanduan_person_number = data.groupby(['TYPE3'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        zhanduan_person_number.to_frame(name='SUM_PERSON_NUMBER'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 每个地点工作人数(不同班组聚合)
    address_data = data.groupby(['ADDRESS_NAME'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        address_data.to_frame(name='ADDRESS_PERSON_NUMBER'),
        how='left',
        left_on='ADDRESS_NAME',
        right_index=True)
    data.drop_duplicates(subset=['ADDRESS_NAME'], keep='first', inplace=True)

    # 计算站段总检查次数
    zhanduan_check_count = data.groupby(['TYPE3'])['CHECK_COUNT'].sum()
    data = pd.merge(
        data,
        zhanduan_check_count.to_frame(name='SUM_CHECK_COUNT'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 计算基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    data['BASE_CHECK_COUNT'] = data.apply(
        lambda row: row['SUM_CHECK_COUNT'] / row['SUM_PERSON_NUMBER'], axis=1)
    # 计算应检查值=地点作业人数*基准值
    # 比较值= 该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)
    data['AVG_CHECK_COUNT'] = data.apply(
        lambda row:
        row['ADDRESS_PERSON_NUMBER'] * row['SUM_CHECK_COUNT']
        / row['SUM_PERSON_NUMBER'], axis=1)
    return data


# 检查地点均衡度 - 显示详细信息
def _stats_check_address_evenness(
        months_ago, content=None, risk_type='车务-1', ):
    """
    每个地点受检次数超过比较值600%以上的一处扣2分;
    受检次数低于比较值50%的一处扣2分，未检查的一处扣5分。
    （每个地点的比较值=该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)，
    受检地点的工作量越大，比较值越高，本单位现场检查）
    检查地点中，每处检查点、重要检查点人数依据车间班组配置的该点作业人数为准，
    若配置的主要生产场所点作业人数为0或空，则取整个班组作业人数。
    若一个检查点的关联单位为车间，同样去主要生产场所中配置的改地点作业人数，
    若配置的作业人数为0或空，则取该车间的直属人数，即人员部门为车间单位的人数
    """
    zhanduan_dpid_data = ZHANDUAN_DPID_DATA
    department_data = DEPARTMENT_DATA
    choose_dpid_data = _choose_dpid_data
    check_banzu_count = CHECK_BANZU_COUNT
    banzu_department_checked_count = BANZU_DEPARTMENT_CHECKED_COUNT
    banzu_connect_department = BANZU_CONNECT_DEPARTMENT

    # 班组受检次数
    data_check_banzu = pd.merge(
        check_banzu_count,
        banzu_department_checked_count,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu = pd.merge(
        data_check_banzu,
        banzu_connect_department,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)

    # 合并班组及重要检查点
    # data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = data_check_banzu
    data.fillna(0, inplace=True)
    data = pd.merge(
        data,
        zhanduan_dpid_data.rename(
            columns={'NAME': 'ZHANDUAN_NAME', 'DEPARTMENT_ID': 'TYPE3'}),
        how='inner',
        left_on='TYPE3',
        right_on='TYPE3')

    # 统计数据
    data = _calc_avg_check_count_by_zhanduan(data)

    # 导出中间计算过程
    _export_calc_address_evenness_data_new(
        data.copy(), months_ago, risk_type, content=content)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'CHECK_COUNT', 'AVG_CHECK_COUNT'),
        axis=1)
    # 计算得分
    rst_index_score = calc_child_index_type_sum(
        data, 2, 4, 3, months_ago, 'DEDUCT_SCORE', 'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data, risk_type=risk_type)
    return rst_index_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.25, 0.4, 0.35]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type='车务-1')
    update_major_maintype_weight(index_type=1, major='车务-1', main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
