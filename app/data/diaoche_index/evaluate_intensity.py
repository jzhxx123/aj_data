#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import current_app

from app.data.diaoche_index.common import calc_check_count_per_person
from app.data.diaoche_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.diaoche_index.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_KEZHI_EVALUATE_COUNT_SQL,
    EVALUATE_COUNT_SQL)
from app.data.index.common import (combine_child_index_func,
                                   df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (
    get_custom_month, get_query_condition_by_risktype, validate_exec_month)
from app.data.major_risk_index.util import calc_child_index_type_divide_major
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _calc_score_for_check_count_per_person(row, major_ratio_dict):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    if row['NAME'] in ['贵阳车站', '重庆车站', '成都车站']:
        major_ratio = major_ratio_dict.get('三大车站')
    else:
        major_ratio = major_ratio_dict.get('调车')
    self_ratio = row['ratio']
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    N1 = major_ratio[0][0]
    Sn1 = major_ratio[0][1]
    N2 = major_ratio[1][0]
    Sn2 = major_ratio[1][1]
    N3 = major_ratio[2][0]
    Sn3 = major_ratio[2][1]
    C = self_ratio
    if level == 1:
        score = Sn1
    elif level == 2:
        score = Sn2 + (C - N2) * (Sn1 - Sn2) / (N1 - N2)
    elif level == 3:
        score = Sn3 + (C - N3) * ((Sn2 - Sn3) / (N2 - N3))
    else:
        score = Sn3 + (C - N3) * (((Sn2 - Sn3) / (N2 - N3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score


def _get_sql_data(months_ago):
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, \
        ACTIVE_EVALUATE_KEZHI_COUNT, ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        ANALYSIS_CENTER_ASSESS_SCORE, DUAN_CADRE_COUNT, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, CHILD_INDEX_SQL_DICT
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    # CHILD_INDEX_SQL_DICT = evaluate_intensity_cardinal_number(
    # months_ago, 2, '车务-1')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(2)
    risk_ids = diaoche[1]
    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(
            ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    # 各个站段的分数
    major_ratio_dict = {
        '调车': [(0.02, 100), (0.015, 90), (0.005, 60)],
        '三大车站': [(0.015, 100), (0.01, 90), (0.005, 60)]
    }

    return calc_check_count_per_person(
        ACTIVE_EVALUATE_COUNT, TOTAL_EVALUATE_COUNT, 2, 2, 1,
        months_ago, 'COUNT', 'SCORE_a',
        _calc_score_for_check_count_per_person, _choose_dpid_data,
        major_ratio_dict=major_ratio_dict, risk_type='车务-1')


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    # fraction = CHILD_INDEX_SQL_DICT.get('2-4')[0]
    return calc_child_index_type_divide_major(
        ACTIVE_EVALUATE_KEZHI_COUNT,
        TOTAL_EVALUATE_COUNT,
        2,
        2,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type='车务-1',
    )


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)
    child_index_func = [_stats_active_ratio, _stats_gradation_ratio]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'd']]
    item_weight = [0.8, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type='车务-1')
    update_major_maintype_weight(index_type=1, major='车务-1', main_type=2,
                                 child_index_list=[1, 4],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
