# 基数选择:选择专业内连续3个月无责任事故、故障（机务、电务不统计D21事故）的单位、
# 月份（3个月）的均值指数作为专业基数参考。（多次比较得出基数）。若找不出相应比较单位，
# 找出选择3个月故障率（无责任事故）（每个月）最低的单位均数上浮20%作为专业基数。
# 电务专业：机务优先选择以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
BASE_UNIT_SQL = """SELECT DISTINCT
    a.FK_DEPARTMENT_ID
FROM
    t_safety_produce_info_responsibility_unit AS a
        LEFT JOIN
    t_safety_produce_info AS b ON b.PK_ID = a.FK_SAFETY_PRODUCE_INFO_ID
WHERE
    a.RESPONSIBILITY_IDENTIFIED < 11
        AND b.MAIN_TYPE < 3
        AND ((b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {}))
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """SELECT
        distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, 
        f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 AND 2
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        AND a.RISK_LEVEL <= 2
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND a.IS_YECHA=1
        AND a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 实际重要检查地点数
REAL_CHECK_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        a.TYPE = 2
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 实际检查班组数(所有被检查班组)
REAL_CHECK_BANZU_SQL = """ SELECT 
    d.FK_DEPARTMENT_ID,1 AS COUNT
    FROM
    (SELECT
    distinct
    a.FK_DEPARTMENT_ID
        FROM
    t_check_info_and_address AS a
    INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE 
    b.CHECK_WAY BETWEEN 1 AND 2 
    and b.SUBMIT_TIME BETWEEN '{0}' and  '{1}'
    and  a.TYPE = 1
    ) as d
    INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = d.FK_DEPARTMENT_ID
    WHERE
    c.IS_DELETE = 0
    and c.TYPE = 9
"""

# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0
    GROUP BY FK_DEPARTMENT_ID;
"""

# 班组--具备检查项目的班组
# (班组下的工区包含检查项目也将该班组统计)
# 班组包含调车风险的项目的父类或任一子类也统计
BANZU_POINT_SQL = """select
    b.DEPARTMENT_ID as FK_DEPARTMENT_ID,
    1 as COUNT
from
    t_department_and_info as a
    INNER JOIN
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on 
    (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
WHERE
    b.IS_DELETE =0
    AND b.TYPE = 9
    AND a.MAIN_TYPE=1
UNION
select
    b.DEPARTMENT_ID as FK_DEPARTMENT_ID,
    1 as COUNT
from
    t_department_and_info as a
    INNER JOIN
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on 
    (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
INNER JOIN 
    t_department as d on b.FK_PARENT_ID = d.DEPARTMENT_ID
WHERE
    b.IS_DELETE =0
    AND b.TYPE = 10
    AND a.MAIN_TYPE=1
    AND d.TYPE=9
    AND d.IS_DELETE=0
"""

# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
            AND e.CHECK_WAY BETWEEN 3 AND 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 and 4
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""
