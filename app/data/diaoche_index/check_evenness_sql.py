# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
            AND a.RISK_LEVEL <= 3
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PROBLEM_POINT) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 3 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
            AND FK_CHECK_ITEM_ID IN ({0})
    GROUP BY FK_DEPARTMENT_ID;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
        (SELECT f.PK_ID AS CHILD_ID, g.PK_ID, g.PARENT_ID  FROM
            t_check_item as f
            INNER JOIN
            t_check_item as g on f.PARENT_ID=g.PK_ID
            WHERE
            f.pk_id in ({0})
    ) AS e on 
    (e.CHILD_ID =a.SOURCE_ID or e.PK_ID=a.SOURCE_ID or e.PARENT_ID=a.SOURCE_ID)
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE BETWEEN 9 AND 10
            AND a.MAIN_TYPE = 1
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item as b on a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
    a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY NOT BETWEEN 4 AND 6
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 检查班组数及其部门人数统计
CHECK_BANZU_PERSON_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 
        COUNT(distinct b.PK_ID) AS PERSON_COUNT
    FROM
        t_department AS a
            LEFT JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
            LEFT JOIN
        t_department_and_info AS c ON a.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
        INNER JOIN
        (SELECT f.PK_ID AS CHILD_ID, g.PK_ID, g.PARENT_ID  FROM
            t_check_item as f
            INNER JOIN
            t_check_item as g on f.PARENT_ID=g.PK_ID
            WHERE
            f.pk_id in ({1})
    ) AS e on 
    (e.CHILD_ID =c.SOURCE_ID or e.PK_ID=c.SOURCE_ID or e.PARENT_ID=c.SOURCE_ID)
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
            AND c.MAIN_TYPE = 1
    GROUP BY a.DEPARTMENT_ID
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME, b.PERSON_NUMBER
    FROM
        t_department as a
        left JOIN
        t_department_and_main_production_site as b 
            on a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND TYPE2 = '{0}'
"""

# 班组受检次数
BANZU_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_check_info_and_item as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
            AND d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY c.DEPARTMENT_ID;
"""
