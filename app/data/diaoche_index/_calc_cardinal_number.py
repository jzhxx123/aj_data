# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     _calc_cardinal_number
   Author :       hwj
   date：          2019/11/26上午10:15
   Change Activity: 2019/11/26上午10:15
-------------------------------------------------
"""
from app.data.diaoche_index.assess_intensity_sql import (
    GANBU_ASSESS_RESPONSIBLE_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.diaoche_index.check_intensity_sql import (
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL)
from app.data.diaoche_index.common_sql import (
    ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    WORKER_COUNT_SQL, RISK_LEVEL_PROBLEM_SQL, CADRE_COUNT_SQL)
from app.data.diaoche_index.evaluate_intensity_sql import (
    EVALUATE_COUNT_SQL, ACTIVE_KEZHI_EVALUATE_COUNT_SQL)
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two,
    get_query_condition_by_risktype)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import BASE_UNIT_INFO_SQL
from app.data.major_risk_index.common.const import (
    IndexDivider, CommonCalcDataType, JIAODA_RISK_SCORE_INFO,
    XC_JIAODA_RISK_SCORE_INFO, YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO,
    MEDIA_PROBLEM_NUMBER_INFO, MEDIA_PROBLME_SCORE_INFO,
    GANBU_ASSESS_RESPONSIBLE_INFO, FEIGANBU_ASSESS_RESPONSIBLE_INFO,
    AWARD_RETURN_MONEY_PROBLEM_INFO, REAL_AWARD_RETURN_MONEY_PROBLEM_INFO,
    EVALUATE_COUNT_INFO, ACTIVE_EVALUATE_COUNT_INFO, ALL_PROBLEM_NUMBER_INFO,
    PROBLEM_SCORE_INFO, ZUOYE_PROBLEM_COUNT_INFO, ZUOYE_PROBLEM_CHECK_SCORE_INFO,
    QUANTIZATION_PERSON_INFO, CADRE_COUNT_INFO, STAFF_NUMBER_INFO)
from app.data.util import pd_query

# 职工总人数/调车作业人数/调车工作量
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.MANAGEMENT_INDEX AS COUNT
    FROM
        t_department_integrated_management AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            INNER JOIN
        t_profession_dictionary AS c ON a.FK_TYPE_ID = c.PK_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND b.FK_PROFESSION_DICTIONARY_ID LIKE '%%2168%%'
            AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
            AND b.SHORT_NAME != ''
            AND a.MONTH = {1}
            AND c.IS_DELETE = 0
"""

# 月度实际返奖问题明细(关联项目)
REAL_AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND a.ACTUAL_MONEY > 0
            AND a.LEVEL in {2}
"""
# 月度返奖问题明细
AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND a.LEVEL in {2}
"""

# 检查问题(总问题数)
CHECK_PROBLEM_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        COUNT(DISTINCT a.PK_ID) as COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    GROUP BY c.DEPARTMENT_ID
"""
PROBLEM_CHECK_SCORE_SQL = """select 
    mps.FK_DEPARTMENT_ID, sum(mps.CHECK_SCORE) as COUNT 
from (
    SELECT
        distinct 
        c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        a.PK_ID,
        f.CHECK_SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    )   as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 一般以上问题数
ABOVE_YIBAN_PROBLEM_NUMBER_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        COUNT(DISTINCT a.PK_ID) as NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.RISK_LEVEL <= 3
    GROUP BY c.DEPARTMENT_ID
"""

# 一般以上问题质量分
ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL = """select 
    mps.FK_DEPARTMENT_ID, sum(mps.CHECK_SCORE) as COUNT 
from (
    SELECT
        distinct 
        c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        a.PK_ID,
        f.CHECK_SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.RISK_LEVEL <= 3
    )   as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""
# 一般以上作业问题数
ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        COUNT(DISTINCT a.PK_ID) as NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.RISK_LEVEL <= 3
        AND f.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY c.DEPARTMENT_ID
"""
# 一般以上作业问题质量分
ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL = """select 
    mps.FK_DEPARTMENT_ID, sum(mps.CHECK_SCORE) as COUNT 
from (
    SELECT
        distinct 
        c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        a.PK_ID,
        f.CHECK_SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.RISK_LEVEL <= 3
        AND f.LEVEL IN ('A', 'B', 'C', 'D')
    )   as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""
# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        COUNT(DISTINCT a.PK_ID) as NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY c.DEPARTMENT_ID
"""
# 作业项问题质量分
ZUOYE_PROBLEM_CHECK_SCORE_SQL = """select 
    mps.FK_DEPARTMENT_ID, sum(mps.CHECK_SCORE) as COUNT 
from (
    SELECT
        distinct 
        c.DEPARTMENT_ID as FK_DEPARTMENT_ID, 
        a.PK_ID,
        f.CHECK_SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND e.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND f.LEVEL IN ('A', 'B', 'C', 'D')
    )   as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    major_dpid = get_major_dpid(risk_type)
    global STATS_MONTH, CALC_MONTH, \
        CHECK_ITEM_IDS, RISK_IDS, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, STAFF_NUMBER, WORKER_COUNT_DATA, base_unit_info_sql
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    risk_config = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_config[0]
    RISK_IDS = risk_config[1]
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    month = int(STATS_MONTH[1][5:7])
    CALC_MONTH = end, start
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, major_dpid)


def check_intensity_cardinal_number(months_ago, risk_name, risk_type):
    """检查力度基数选择"""
    _get_data(months_ago, risk_name, risk_type)
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 调车工作量
    quantization_person = CommonCalcDataType(*QUANTIZATION_PERSON_INFO)
    quantization_person.value = [WORK_LOAD_SQL]
    quantization_person.version = 'v2'
    quantization_person.description = '调车工作量'
    # 较大问题质量分
    jiaoda_risk_score = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    jiaoda_risk_score.value = [
        RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 现场检查较大问题质量分
    xc_jiaoda_risk_score = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    xc_jiaoda_risk_score.value = [
        XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 夜查次数
    yecha_count = CommonCalcDataType(*YECHA_COUNT_INFO)
    yecha_count.value = [YECHA_CHECK_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅时长
    media_cost_time = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    media_cost_time.value = [
        MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    media_problem_number = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    media_problem_number.value = [
        MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅质量分
    media_problme_score = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    media_problme_score.value = [
        MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]
    # 参与基数计算的sql字典
    child_index_sql_dict = {

        # 较大风险问题质量均分
        '1-6': (IndexDetails(jiaoda_risk_score, quantization_person),
                IndexDetails(xc_jiaoda_risk_score, quantization_person)),

        # 夜查率
        '1-7': (IndexDetails(yecha_count, quantization_person),),

        # 监控调阅力度
        '1-10': (IndexDetails(media_cost_time, quantization_person),
                 IndexDetails(media_problem_number, quantization_person),
                 IndexDetails(media_problme_score, quantization_person)),
    }
    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)
    return child_index_sql_dict


def assess_intensity_cardinal_number(months_ago, risk_name, risk_type):
    """考核力度基数选择"""
    _get_data(months_ago, risk_name, risk_type)

    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 干部数
    cadre_count = CommonCalcDataType(*CADRE_COUNT_INFO)
    cadre_count.value = [CADRE_COUNT_SQL]

    # 职工总数-非干部
    staff_number = CommonCalcDataType(*STAFF_NUMBER_INFO)
    staff_number.value = [WORKER_COUNT_SQL]

    # 干部考核金额
    ganbu_assess_responsible = CommonCalcDataType(*GANBU_ASSESS_RESPONSIBLE_INFO)
    ganbu_assess_responsible.value = [GANBU_ASSESS_RESPONSIBLE_SQL]

    # 非干部考核金额
    feiganbu_assess_responsible = CommonCalcDataType(*FEIGANBU_ASSESS_RESPONSIBLE_INFO)
    feiganbu_assess_responsible.value = [FEIGANBU_ASSESS_RESPONSIBLE_SQL]

    # A, B, E1, E2问题
    award_return_money_problem_abe1e2 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_abe1e2.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('A', 'B', 'E1', 'E2'))]
    # A, B, E1, E2问题返奖数
    award_return_money_abe1e2 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_abe1e2.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('A', 'B', 'E1', 'E2'))]

    # C, E3问题
    award_return_money_problem_ce3 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_ce3.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('C', 'E3'))]
    award_return_money_problem_ce3.version = 'v2'
    award_return_money_problem_ce3.description = '返奖问题个数(C, E3)'
    # C, E3问题返奖数
    award_return_money_ce3 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_ce3.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('C', 'E3'))]
    award_return_money_ce3.version = 'v2'
    award_return_money_ce3.description = '实际返奖问题个数(C, E3)'

    # D, E4问题
    award_return_money_problem_de4 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_de4.value = [AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('D', 'E4'))]
    award_return_money_problem_de4.version = 'v3'
    award_return_money_problem_de4.description = '返奖问题个数(D, E4)'
    # D, E4问题返奖数
    award_return_money_de4 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_de4.value = [REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', ('D', 'E4'))]
    award_return_money_de4.version = 'v3'
    award_return_money_de4.description = '实际返奖问题个数(D, E4)'

    # 参与基数计算的sql字典
    child_index_sql_dict = {

        # 考核力度指数
        # 换算单位考核金额
        '3-2': (
            IndexDetails(ganbu_assess_responsible, cadre_count),
            IndexDetails(feiganbu_assess_responsible, staff_number)),

        # 返奖率
        '3-3': (IndexDetails(award_return_money_abe1e2, award_return_money_problem_abe1e2),
                IndexDetails(award_return_money_ce3, award_return_money_problem_ce3),
                IndexDetails(award_return_money_de4, award_return_money_problem_de4)),

    }
    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)
    return child_index_sql_dict


def evaluate_intensity_cardinal_number(months_ago, risk_name, risk_type):
    """评价力度基数选择"""
    _get_data(months_ago, risk_name, risk_type)
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 评价记分总条数
    evaluate_count = CommonCalcDataType(*EVALUATE_COUNT_INFO)
    evaluate_count.value = [EVALUATE_COUNT_SQL.format(
        '{0}', '{1}', RISK_IDS)]

    # （主动）科职干部评价记分条数
    active_evaluate_kezhi_count = CommonCalcDataType(*ACTIVE_EVALUATE_COUNT_INFO)
    active_evaluate_kezhi_count.value = [
        ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format('{0}', '{1}', RISK_IDS)]
    active_evaluate_kezhi_count.version = 'v2'
    active_evaluate_kezhi_count.description = '主动科职干部评价记分条数'

    # 参与基数计算的sql字典
    child_index_sql_dict = {
        # 评价职务占比
        '2-4': (IndexDetails(active_evaluate_kezhi_count, evaluate_count),),

    }
    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)
    return child_index_sql_dict


def problem_exposure_cardinal_number(months_ago, risk_name, risk_type):
    """问题暴露度基数选择"""
    _get_data(months_ago, risk_name, risk_type)

    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 调车工作量
    quantization_person = CommonCalcDataType(*QUANTIZATION_PERSON_INFO)
    quantization_person.value = [WORK_LOAD_SQL]
    quantization_person.version = 'v2'
    quantization_person.description = '调车工作量'
    # 一般及以上问题数
    above_yiban_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    above_yiban_problem_number.version = 'v2'
    above_yiban_problem_number.description = '一般及以上问题数'
    above_yiban_problem_number.value = [
        ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般及以上问题质量分
    above_yiban_problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    above_yiban_problem_check_score.version = 'v2'
    above_yiban_problem_check_score.description = '一般及以上问题质量分'
    above_yiban_problem_check_score.value = [
        ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format(
            '{0}', '{1}', CHECK_ITEM_IDS)]

    # 总问题数
    all_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    all_problem_number.value = [
        CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题质量分
    problem_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    problem_score.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般以及以上作业项问题数
    above_yiban_zuoye_check_problem = CommonCalcDataType(
        *ZUOYE_PROBLEM_COUNT_INFO)
    above_yiban_zuoye_check_problem.version = 'v2'
    above_yiban_zuoye_check_problem.description = '一般以及以上作业项问题数'
    above_yiban_zuoye_check_problem.value = [
        ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL.format(
            '{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般及以上作业项问题质量分
    above_yiban_zuoye_problem_check_score = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    above_yiban_zuoye_problem_check_score.version = 'v2'
    above_yiban_zuoye_problem_check_score.description = '一般及以上作业项问题质量分'
    above_yiban_zuoye_problem_check_score.value = [
        ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format(
            '{0}', '{1}', CHECK_ITEM_IDS)]

    # 作业项问题数
    zuoye_problem_count = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    zuoye_problem_count.value = [ZUOYE_CHECK_PROBLEM_SQL.format(
        '{0}', '{1}', CHECK_ITEM_IDS)]

    # 作业项问题质量分
    zuoye_problem_check_score = CommonCalcDataType(
        *ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    zuoye_problem_check_score.value = [ZUOYE_PROBLEM_CHECK_SCORE_SQL.format(
        '{0}', '{1}', CHECK_ITEM_IDS)]

    # 参与基数计算的sql字典
    child_index_sql_dict = {
        # 普遍性暴露
        '5-1': (
            IndexDetails(all_problem_number, quantization_person),
            IndexDetails(problem_score, quantization_person),
            IndexDetails(above_yiban_problem_number, quantization_person),
            IndexDetails(above_yiban_problem_check_score, quantization_person),
            IndexDetails(zuoye_problem_count, quantization_person),
            IndexDetails(zuoye_problem_check_score, quantization_person),
            IndexDetails(above_yiban_zuoye_check_problem, quantization_person),
            IndexDetails(above_yiban_zuoye_problem_check_score, quantization_person),
        ),
    }
    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)
    return child_index_sql_dict
