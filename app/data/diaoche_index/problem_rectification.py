# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''

import pandas as pd
from flask import current_app

from app.data.diaoche_index import problem_ctrl_calc
from app.data.diaoche_index.check_intensity_sql import BANZU_POINT_SQL
from app.data.diaoche_index.common import calc_work_load
from app.data.diaoche_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.diaoche_index.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, HAPPEN_PROBLEM_POINT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL, REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL, CHECK_WORK_SHOP_COUNT_SQL)
from app.data.index.common import (
    calc_child_index_type_sum, combine_child_index_func, df_merge_with_dpid,
    export_basic_data_dicttype, export_basic_data_tow_field_monthly,
    summizet_child_index, summizet_operation_set)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype,
                                 get_year_month, validate_exec_month)
from app.data.util import pd_query, update_major_maintype_weight

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_department_coefficient(months_ago):
    """单位大小系数:
    调车工作量/（∑专业各单位调车总工作量/专业有调车作业车间数）
    """
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        calc_work_load(WORK_LOAD_SQL, month), DEPARTMENT_DATA)
    total_work_load = WORK_LOAD['COUNT'].sum()
    work_load = WORK_LOAD.groupby('TYPE3')['COUNT'].sum().to_frame(
        name='work_load')
    BANZU_POINT = df_merge_with_dpid(
        pd_query(BANZU_POINT_SQL), DEPARTMENT_DATA)
    banzu_point = BANZU_POINT.groupby(
        ['TYPE3'])['COUNT'].sum().to_frame(name='banzu_number')
    data = pd.concat([work_load, banzu_point], axis=1, sort=True)
    data.fillna(0, inplace=True)
    rst = {}
    for idx in data.index:
        if data.loc[idx]['banzu_number'] == 0:
            rst[idx] == 1
        else:
            rst[idx] = data.loc[idx]['work_load'] / (
                    total_work_load / data.loc[idx]['banzu_number'])
    global DEPARTMENT_COEFFICIENT
    DEPARTMENT_COEFFICIENT = rst


# 部门按站段聚合
def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, PROBLEM_POINT_INFO_DATA, \
        WORK_LOAD, CHECK_WORK_SHOP_COUNT, CHECK_ITEM_IDS, RISK_IDS
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    diaoche = get_query_condition_by_risktype(2)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    # _calc_department_coefficient(months_ago)
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL)
    # 调车工作量
    WORK_LOAD = calc_work_load(WORK_LOAD_SQL, month)
    # 调车作业车间数
    CHECK_WORK_SHOP_COUNT = pd_query(
        CHECK_WORK_SHOP_COUNT_SQL.format(CHECK_ITEM_IDS))


def _calc_rectification_score(problem_number):
    val = 100 - 2 * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


# 整改时效
def _stats_rectification_effect(months_ago):
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    data = df_merge_with_dpid(
        pd_query(
            OVERDUE_PROBLEM_NUMBER_SQL.format(
                year_mon // 100, year_mon % 100, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        _choose_dpid_data(3),
        6,
        1,
        3,
        months_ago,
        lambda x: f'本月问题超期发生条数：{int(x)}条',
        risk_type='车务-1')
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_rectification_score,
        _choose_dpid_data,
        risk_type='车务-1',
        NA_value=True)
    return rst_index_score


# 整改履责
def _stats_check_evaluate(months_ago):
    calc_month = get_custom_month(months_ago)
    data = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_SCORE_SQL.format(*calc_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        _choose_dpid_data(3),
        6,
        2,
        3,
        months_ago,
        lambda x: f'本月ZG-1、2、3、4、5履职评价发生条数：{int(x)}条',
        risk_type='车务-1')
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: min(60 + x, 100),
        _choose_dpid_data,
        risk_type='车务-1',
        NA_value=True)
    return rst_index_score


def _filter_external_assess_problem(row):
    flag = True
    if row['LEVEL'] not in ('E1', 'E2', 'E3', 'E4'):
        return flag
    else:
        if int(row['IS_EXTERNAL']) == 1 and row['ASSESS_MONEY'] > 0:
            return flag
        return not flag


def _get_appoint_month_happen_problem(months_ago, hierarchy):
    """获取前第{-months_ago}月的发生问题项点数

    Arguments:
        months_ago {int} -- [description]

    Returns:
        set -- 反复问题
    """
    happen_problem_point_data = pd_query(
        HAPPEN_PROBLEM_POINT_SQL.format(
            *get_custom_month(months_ago), CHECK_ITEM_IDS))
    happen_problem_point_data = happen_problem_point_data[
        happen_problem_point_data.apply(
            lambda row: _filter_external_assess_problem(row), axis=1)]
    happen_problem_point_data = happen_problem_point_data[
        ['FK_DEPARTMENT_ID', 'PK_ID', 'RISK_LEVEL']]
    i_month_data = df_merge_with_dpid(
        happen_problem_point_data,
        DEPARTMENT_DATA)
    if i_month_data.empty is True:
        return set()
    i_month_data = i_month_data.groupby(
        [f'TYPE{hierarchy}', 'PK_ID', 'RISK_LEVEL']).size()
    repeatedly_problem = set()
    for idx in i_month_data.index:
        risk_level = idx[2]
        problem_number = int(i_month_data[idx])
        if risk_level == 1 and problem_number < 2:
            continue
        elif risk_level == 2 and problem_number < 4:
            continue
        elif risk_level == 3 and problem_number < 15:
            continue
        else:
            repeatedly_problem.add(f'{idx[0]}||{idx[1]}||{idx[2]}')
    return repeatedly_problem


def _calc_problem_score(risk_level, i_month):
    """根据风险等级和月份进行扣分

    Arguments:
        risk_level {str} -- 风险等级
        i_month {str} -- 前第{-i_month}月

    Returns:
        float/int -- 得分
    """
    _score = {
        '1': 1,
        '2': 0.5,
        '3': 0.2,
    }
    problem_score = _score.get(risk_level, 0) * (4 + int(i_month))
    return problem_score


def _fillna_for_zhanduan(df):
    global ZHANDUAN_DPID_DATA
    all_zhanduan = ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    na_zhanduan = []
    df_zhanduan = df.index.values
    for each in all_zhanduan:
        if each not in df_zhanduan:
            na_zhanduan.append(each)
    df_na_zhanduan = pd.DataFrame(
        index=na_zhanduan, data=[0 for x in na_zhanduan], columns=['SCORE'])
    return pd.concat([df, df_na_zhanduan])


def _calc_repeatedly_index(months_ago, hierarchy):
    repeatedly_problem = []
    base_repeatedly_problem = _get_appoint_month_happen_problem(
        months_ago, hierarchy)
    for i_month in range(-1, -4, -1):
        i_month_repeatedly_problem = _get_appoint_month_happen_problem(
            months_ago + i_month, hierarchy)
        # 获取每个月反复的问题项点
        common_problem = base_repeatedly_problem.intersection(
            i_month_repeatedly_problem)
        repeatedly_problem.extend(
            [[x.split('||')[0], i_month,
              x.split('||')[2]] for x in common_problem])
    if repeatedly_problem == []:
        return []
    # 导出中间过程数据
    first_title = {-1: '前一个月', -2: '前二个月', -3: '前三个月'}
    second_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_tow_field_monthly(
        repeatedly_problem,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        6,
        3,
        3,
        months_ago,
        first_title,
        second_title,
        risk_type='车务-1')
    df_prob = pd.DataFrame(
        data=repeatedly_problem, columns=['DEP_DPID', 'I_MONTH', 'RISK_LEVEL'])
    df_prob['SCORE'] = df_prob.apply(
        lambda row: _calc_problem_score(row['RISK_LEVEL'], row['I_MONTH']),
        axis=1)
    # 将站段分组求和
    df_prob = df_prob.groupby(['DEP_DPID'])['SCORE'].sum()
    df_prob = _fillna_for_zhanduan(df_prob.to_frame(name='SCORE'))
    column = f'SCORE_c_{hierarchy}'
    df_prob[column] = df_prob['SCORE'].apply(
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2))

    # 计算排名、入库
    summizet_operation_set(
        df_prob,
        _choose_dpid_data(hierarchy),
        column,
        hierarchy,
        2,
        6,
        3,
        months_ago,
        risk_type='车务-1')
    return df_prob[[column]]


# 整改成效
def _stats_repeatedly_index(months_ago):
    """以站段为统计单位同一问题项点是否发生，严重风险的问题3月内重复发生一个扣a分，
    较大风险的（当月累计发生3次以上算重复发生）问题3月内重复发生一项扣a分，
    一般风险的（当月累计发生10次以上算重复发生）问题3月内重复发生一项扣a分。
    低风险问题不计算

    Arguments:
        months_ago {int} -- [description]
    """
    # rst_index_score = []
    # for hierarchy in HIERARCHY:
    #     rst_score = _calc_repeatedly_index(months_ago, hierarchy)
    #     if len(rst_score) != 0:
    #         rst_index_score.append(rst_score)
    # return rst_index_score
    stats_month = get_custom_month(months_ago)

    repeate_happen_problem_sql = REPEATE_HAPPEN_PROBLEM_SQL.format(
        *stats_month, CHECK_ITEM_IDS)
    return problem_ctrl_calc.stats_repeatedly_index(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, _choose_dpid_data,
        WORK_LOAD, CHECK_WORK_SHOP_COUNT, repeate_happen_problem_sql,
        PROBLEM_POINT_INFO_DATA)


# 整改复查
def _rectification_review(months_ago):
    """库内问题复查数/总人数，与专业基数比较
    """
    pass


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_effect, _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.1, 0.1, 0.8]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type='车务-1')
    update_major_maintype_weight(index_type=1, major='车务-1', main_type=6,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
