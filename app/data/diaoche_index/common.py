#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: common functions for diaoche_index module.
'''
from app.data.index.common import export_basic_data, summizet_operation_set
from app.data.major_risk_index.util import append_major_column_to_df
from app.data.util import pd_query, get_history_months
import pandas as pd


def _calc_work_time(row, days):
    """计算工作量

    Arguments:
        row {} -- 每个单位的工作量情况
        days {int} -- 当月天数

    Returns:
        int/float -- 返回工作量
    """
    work_length = row['WORK_LENGTH']
    work_number = row['WORK_NUMBER']
    if pd.isnull(work_length) or pd.isnull(work_number):
        return 0
    if row['WORK_TYPE'] == 1:
        # 先固定日勤制每个月工作天数为22天
        return int(work_length * work_number * 22)
    elif row['WORK_TYPE'] == 2:
        # work_time:24小时制度（2:二班倒、3:三班倒，1：四班倒）
        work_time = row['WORK_TIME']
        if pd.isnull(work_time):
            return 0
        work_time = int(work_time)
        if work_time == 1:
            work_time = 4
        return int(work_number * work_length / work_time * days)

    return 0


def calc_work_load(sql, month):
    """计算每个单位的调车作业工作量

    Arguments:
        sql {sql} -- 调车作业sql语句
        months_ago {int} -- 月份

    Returns:
        DataFrame -- 每个单位的调车作业工作量
    """
    # 计算指定月的天数

    data = pd_query(sql.format(month))
    return data


def get_major_ratio(row, major_base_data):
    if row['NAME'] in ['贵阳车站', '重庆车站', '成都车站']:
        return major_base_data.get('三大车站', 0)
    else:
        return major_base_data.get('调车', 0)


def add_avg_number_by_major_for_check_count_per_person(
        months_ago, df, dpid_data, main_type=1, detail_type=2):
    # total_major_base_data 的键 是main-detail_type
    key = str(main_type) + '-' + str(detail_type)
    total_major_base_data = {
        # 换算单位检查频次
        "1-2": {'调车': 0.06, '三大车站': 0.035},
        # 查出问题率
        "1-3": {'调车': 0.007, '三大车站': 0.006},
        # 换算人均质量份
        '1-5': {'调车': 0.04, '三大车站': 0.02},
        # 干部人均主动评价记分条数
        "2-2": {'调车': 0.015, '三大车站': 0.01},
        # 换算单位考核问题数
        "3-1": {'调车': 0.008, '三大车站': 0.0065},
    }
    major_base_data = total_major_base_data.get(key, total_major_base_data['1-2'])

    # 增加专业列
    data = append_major_column_to_df(dpid_data, df)
    # data = pd.merge(
    #     df, df_major_base, how='left', left_on='MAJOR', right_on='MAJOR')
    data['AVG_NUMBER'] = data.apply(lambda row: get_major_ratio(row, major_base_data), axis=1)

    data.fillna(0, inplace=True)
    return data


def calc_check_count_per_person(df_numerator,
                                df_denominator,
                                index_type,
                                main_type,
                                detail_type,
                                months_ago,
                                stats_field,
                                column,
                                calc_func,
                                dpid_func,
                                hierarchy_list=[3],
                                risk_type=None,
                                major_ratio_dict={
                                    '调车': [(0.07, 100), (0.06, 90), (0.03, 60)],
                                    '三大车站': [(0.05, 100), (0.035, 90), (0.01, 60)]
                                }):
    """综合指数-换算单位检查频次的基数是路局提交的单独表格数据

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        column = f'{column}_{hierarchy}'
        # 需要跟基数（专业均值）作比较得出分数
        df_calc = add_avg_number_by_major_for_check_count_per_person(
            months_ago, df_calc, dpid_func(hierarchy), main_type, detail_type)
        df_calc['SCORE'] = df_calc.apply(
            lambda row: calc_func(row, major_ratio_dict), axis=1)
        # 导出指数中间过程
        export_basic_data(df_calc, main_type, detail_type, hierarchy,
                          months_ago, 'numerator', 'denominator', 'ratio',
                          'AVG_NUMBER', 'SCORE', risk_type)
        rst_data = pd.DataFrame(
            index=df_calc['DEPARTMENT_ID'],
            data=df_calc['SCORE'].values,
            columns=[column])

        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(rst_data, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(rst_data[[column]])
    return rst_index_score
