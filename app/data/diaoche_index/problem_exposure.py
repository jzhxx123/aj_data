# -*- coding: utf-8 -*-
"""
    desc: 问题暴露度指数（调车）
    author: 何强胜
    date: 2019/02/21
"""

import pandas as pd
from flask import current_app

from app.data.diaoche_index._calc_cardinal_number import problem_exposure_cardinal_number
from app.data.diaoche_index.check_intensity_sql import BANZU_POINT_SQL
from app.data.diaoche_index.common import calc_work_load
from app.data.diaoche_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.diaoche_index.problem_exposure_sql import (
    CHECK_PROBLEM_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
    HIDDEN_KEY_PROBLEM_MONTH_SQL, OTHER_CHECK_PROBLEM_SQL,
    SELF_CHECK_PROBLEM_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_sum,
    calc_extra_child_score_groupby_major, combine_child_index_func,
    df_merge_with_dpid, export_basic_data_dicttype,
    export_basic_data_one_field_monthly, format_export_basic_data,
    summizet_child_index, summizet_operation_set,
    write_export_basic_data_to_mongo, calc_extra_child_score_groupby_major_two)
from app.data.index.util import (get_custom_month, get_months_from_201712,
                                 get_query_condition_by_risktype,
                                 validate_exec_month)
from app.data.major_risk_index.util import write_cardinal_number_basic_data, calc_extra_child_score_groupby_major_third
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, WORK_LOAD, \
        CHILD_INDEX_SQL_DICT
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    CHILD_INDEX_SQL_DICT = problem_exposure_cardinal_number(months_ago, 2, '车务-1')
    diaoche = get_query_condition_by_risktype(2)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        calc_work_load(WORK_LOAD_SQL, month), DEPARTMENT_DATA)
    WORK_LOAD = data.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = diaoche[0]


def _calc_score_by_formula(row, column, major_column, detail_type=None, major_ratio_dict={}):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 6 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_value_per_person(series, weight, hierarchy, fraction=None):
    global WORK_LOAD
    data = pd.concat(
        [series.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, _choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, 5,
                                         1, fraction.months_ago,
                                         ['prob', 'PERSON_NUMBER'])
    return calc_extra_child_score_groupby_major_third(data,
                                                      _choose_dpid_data(hierarchy),
                                                      'ratio',
                                                      _calc_score_by_formula, weight=weight,
                                                      numerator='prob',
                                                      denominator='PERSON_NUMBER',
                                                      fraction=fraction)


def _calc_prob_number_per_person(df_data, weight, hierarchy, fraction=None):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, weight, hierarchy, fraction=fraction)


def _calc_prob_score_per_person(df_data, weight, hierarchy, fraction=None):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, weight, hierarchy, fraction=fraction)


def _calc_basic_prob_number_per_person(df_data, i, title):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby(['TYPE3']).size()
    global WORK_LOAD
    data = pd.concat(
        [prob_number.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, i, title):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    global WORK_LOAD
    data = pd.concat(
        [prob_score.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    weight_item = [0.3, 0.3, 0.2, 0.2]
    weight_part = [0.4, 0.6]
    fraction_list = CHILD_INDEX_SQL_DICT.get('5-1')
    fraction_list = [
        [fraction_list[0], fraction_list[1]],
        [fraction_list[2], fraction_list[3]],
        [fraction_list[4], fraction_list[5]],
        [fraction_list[6], fraction_list[7]],
    ]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
        '一般及以上作业项问题数({0})/人数({1})'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(func(data.copy(), i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type='车务-1')
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type='车务-1')
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(func(data.copy(), weight, hierarchy, fraction=fraction_list[i][j]))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type='车务-1')
        rst_child_score.append(df_rst)
    return rst_child_score


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """数据源:每个基础调车问题项点的出现频率
    计算标准:每查处一条（去重）加5分，满分100

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_month = get_custom_month(months_ago)
    # 超期问题数
    data = df_merge_with_dpid(
        pd_query(
            HIDDEN_KEY_PROBLEM_MONTH_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        _choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'本月发生基础调车问题项点条数（去重）：{int(x)}条',
        risk_type='车务-1')
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        5,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        lambda x: min(5 * x, 100),
        _choose_dpid_data,
        risk_type='车务-1',
        NA_value=True)
    return rst_index_score


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """计算标准:满分100，一个月未暴露任何基础调车问题的扣1分/个调车作业班组，
    连续2月问题未暴露任何基础调车问题的扣2分/个调车作业班组，
    连续3个月问题未暴露任何基础调车问题的扣3分/个调车作业班组，最低0分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_months = get_months_from_201712(months_ago, 3)
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(pd_query(
        BANZU_POINT_SQL.format(CHECK_ITEM_IDS))['FK_DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    # 用来记录每个班组每个月问题为空白的个数
    exposure_banzu_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            pd_query(EXPOSURE_PROBLEM_DEPARTMENT_SQL.format(
                mon[0], mon[1], CHECK_ITEM_IDS))['FK_DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        if len(hidden_banzu) == 0:
            break
        if idx > 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({dpid: deduct_score.get(dpid, 0) + idx + 1})
                exposure_banzu_num_monthly.append([dpid, idx + 1])
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    # 导出中间计算数据
    export_basic_data_one_field_monthly(
        exposure_banzu_num_monthly,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        4,
        3,
        months_ago,
        lambda x: f'连续第{x}个月未暴露基础调车问题个数',
        title='暴露班组数:',
        risk_type='车务-1')
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        4,
        months_ago,
        'SCORE',
        'SCORE_d',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type='车务-1')
    return rst_child_score


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """从他查问题分析,1个月未自查出该问题项点，一般风险问题一条扣0.1分，较大风险扣1分，
    严重风险扣3分；若该问题项点属于事故隐患关键问题的在上述基础上*3。
    每个问题项点最高扣30分。满分100，最低0分。
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(SELF_CHECK_PROBLEM_SQL.format(*calc_month, CHECK_ITEM_IDS))[
            'PROBLEM_DPID_RISK'].values)
    other_check_problem = set(
        pd_query(OTHER_CHECK_PROBLEM_SQL.format(*calc_month, CHECK_ITEM_IDS))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    }
    problem_risk_score = {
        '1': 3,
        '2': 1,
        '3': 0.1,
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        problem_score = problem_risk_score.get(each_problem[1], 0)
        if each_problem[3] == '1':
            problem_score *= 3
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, each_problem[1]])
    if len(calc_problems) < 1:
        return None
    # 导出中间计算过程
    first_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_one_field_monthly(
        calc_problems,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        5,
        3,
        months_ago,
        lambda x: first_title.get(x),
        title='他查问题个数',
        risk_type='车务-１')
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        5,
        5,
        months_ago,
        'SCORE',
        'SCORE_e',
        lambda x: 100 if x > 100 else x,
        _choose_dpid_data,
        risk_type='车务-1')
    return rst_child_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'd', 'e']]
    item_weight = [0.50, 0.20, 0.30, -0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type='车务-1')
    update_major_maintype_weight(index_type=1, major='车务-1', main_type=5,
                                 child_index_list=[1, 3, 4, 5],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
