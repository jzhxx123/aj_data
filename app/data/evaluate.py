#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from flask import current_app
from pymongo import ASCENDING

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo, get_previous_month)
from app.utils.decorator import init_wrapper

sql_dep = "SELECT DEPARTMENT_ID, HIERARCHY, TYPE1, TYPE2, TYPE3, TYPE4, \
                TYPE5, TYPE6, TYPE7 FROM t_department"

sql_person = "SELECT PERSON_NAME, ID_CARD FROM t_person"


def classify(val):
    val = int(val)
    if val == 0:
        return 1
    count = 0
    while val > 0:
        val = val // 2
        count += 1
    return count


def singe_score_section(score):
    if score <= 1:
        return 1
    elif score <= 2:
        return 2
    elif score <= 4:
        return 3
    elif score <= 6:
        return 4
    elif score <= 8:
        return 5
    elif score <= 10:
        return 6
    else:
        return 7


def format_mean(arr):
    _mean = arr.mean()
    return '{:.2f}'.format(_mean)


def get_mean_evaluate(data):
    result = []
    for x in range(3, 6):
        xdata = data.groupby(['TYPE{}'.format(x), 'MON', 'ID']).agg({
            'SCORE':
            np.sum
        })
        xdata = xdata.groupby(['TYPE{}'.format(x), 'MON']).agg({
            'SCORE':
            format_mean
        })
        for index in xdata.index:
            value = xdata.loc[index].values[0]
            result.append({
                'DPID': index[0],
                'MON': index[1],
                'SCORE': value,
            })
    current_app.logger.debug('complete: format data.')
    
    coll_name = '{}map_evaluate'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_index([('MON', ASCENDING)], background=True)


def my_concat(arr):
    return '/'.join([str(x) for x in arr])


def get_evaluate_count(data):
    data['GRADA_ID'] = data['GRADA_ID'].apply(lambda x: 3 if x > 3 else x)
    person = pd.DataFrame({"ID": data['ID'], "NAME": data['NAME']})
    person.drop_duplicates(inplace=True)
    person_name = {}
    for each in person.values:
        person_name.update({each[0]: each[1]})
    result = {}
    for x in range(3, 6):
        score = data.groupby(['TYPE{}'.format(x), 'MON', 'GRADA_ID',
                              'ID']).sum()
        score['LEVEL'] = score.loc[:, 'SCORE'].apply(classify)
        score = score.reset_index()
        score1 = score.groupby(['TYPE{}'.format(x), 'MON', 'LEVEL']).agg({
            'ID':
            my_concat,
            'SCORE':
            my_concat
        })
        score2 = score.groupby(
            ['TYPE{}'.format(x), 'MON', 'LEVEL', 'GRADA_ID']).agg({
                'ID':
                my_concat,
                'SCORE':
                my_concat
            })
        score2 = score2.unstack()
        tmp_data = pd.concat([score1, score2], axis=1, levels=0, sort=False)
        for each in tmp_data.index:
            # SCORE                                     1.95/1.95
            # ID            522101197707263216/522601197012201219
            # (SCORE, 1)                                     None
            # (SCORE, 2)                                1.95/1.95
            # (SCORE, 3)                                     None
            # (ID, 1)                                        None
            # (ID, 2)       522101197707263216/522601197012201219
            # (ID, 3)                                        None
            node_key = each[0] + str(each[1])
            node_val = tmp_data.loc[each].values
            node_idx = tmp_data.loc[each].index

            node = {'DPID': each[0], 'MON': each[1], 'LEVEL': []}
            if node_key in result:
                node = result.get(node_key)
            LEVEL = node.get('LEVEL')
            person_score = [None for _ in range(8)]
            for nid, nidx in enumerate(node_idx):
                _tmp_val = node_val[nid]
                if isinstance(nidx, str):
                    if nidx == 'SCORE':
                        person_score[4] = _tmp_val
                    else:
                        person_score[0] = _tmp_val
                elif isinstance(nidx, tuple):
                    if nidx[0] == 'SCORE':
                        person_score[nidx[1] + 4] = _tmp_val
                    else:
                        person_score[nidx[1]] = _tmp_val
                else:
                    raise ValueError("数据格式错误")
            seg = []
            for step in range(4):
                if person_score[step] is None:
                    seg.append(None)
                else:
                    seg.append({
                        'COUNT':
                        str(person_score[step]).count('/') + 1,
                        'PERSON': {
                            person_name.get(j, j): k
                            for j, k in zip(
                                str(person_score[step]).split('/'),
                                str(person_score[step + 4]).split('/'))
                        }
                    })
            LEVEL.append({'level': each[2], 'VALUE': seg})
            node.update({'LEVEL': LEVEL})
            result.update({node_key: node})
    current_app.logger.debug('complete: format data.')
    
    coll_name = '{}detail_evaluate_score'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, list(result.values()))
    mongo.db[coll_name].create_index(
        [('DPID', ASCENDING), ('MON', ASCENDING)], background=True)


def get_code_value(value):
    '''
        防止一些空缺某个干部level的值
    '''
    res = [0 for _ in range(4)]
    for idx, val in enumerate(value.index.values):
        res[val] = value.values[idx]
    return res


def get_childcode_values(value):
    res = [None for _ in range(4)]

    childs = value.index.values
    grada_id = value.columns.values
    for idx, val in enumerate(grada_id):
        codes = {
            x: int(y)
            for x, y in zip(childs, list(value.loc[:, val].values))
        }
        codes = sorted(codes.items(), key=lambda d: d[1], reverse=True)
        for cidx, cval in enumerate(codes):  # 去除数量为0的item
            if int(cval[1]) == 0:
                codes = codes[:cidx]
                if codes == []:
                    codes = None
                break
        res[val] = codes
    return res


def merge_child(pdata, cdata):
    res = []
    for idx, val in enumerate(pdata):
        if val is None:
            res.append(None)
            pass
        res.append({
            'SIZE': int(val),
            'DETAIL': cdata[idx],
        })
    return res


def get_code_count(data):
    result = {}
    for x in range(3, 6):
        TX1 = data.groupby(['TYPE{}'.format(x), 'MON', 'CCODE', 'CODE']).size()
        TX2 = data.groupby(
            ['TYPE{}'.format(x), 'MON', 'CCODE', 'GRADA_ID', 'CODE']).size()
        TX2 = TX2.unstack(-2)
        data1 = data.groupby(['TYPE{}'.format(x), 'MON', 'CCODE']).size()
        data2 = data.groupby(['TYPE{}'.format(x), 'MON', 'CCODE',
                              'GRADA_ID']).size()
        data2 = data2.unstack()
        tmp_data = pd.concat([data1, data2], axis=1, sort=False)
        tmp_data = tmp_data.fillna(0)
        TX = pd.concat([TX1, TX2], axis=1, sort=False)
        TX = TX.fillna(0)

        for each in tmp_data.index:
            key = each[0] + str(each[1])
            value = tmp_data.loc[each]
            tx_value = TX.loc[each]
            code_value = merge_child(
                get_code_value(value), get_childcode_values(tx_value))
            node = {'DPID': each[0], 'MONTH': each[1], 'CODE': []}
            if key in result:
                node = result.get(key)
            CODE = node.get('CODE')
            CODE.append({
                'NAME': each[2],
                'VALUE': code_value,
            })
            node.update({'CODE': CODE})
            result.update({key: node})
    current_app.logger.debug('complete: format data.')
    write_bulk_mongo('evaluate_code', list(result.values()))


def get_history_dpid_code_grada(data):
    for x in range(3, 6):
        data1 = data.groupby(['TYPE{}'.format(x), 'CCODE', 'MON']).size()
        data2 = data.groupby(['TYPE{}'.format(x), 'CCODE', 'MON',
                              'GRADA_ID']).size()
        data2 = data2.unstack()
        tmp_data = pd.concat([data1, data2], axis=1, sort=False)
        tmp_data = tmp_data.unstack(-1)
        tmp_data = tmp_data.fillna(0)
        result = []
        for index in tmp_data.index:
            key = index
            value = tmp_data.loc[index]
            mon_idx = value.index.values
            mon_val = value.values
            mon_node = {}
            for idx, val in enumerate(mon_idx):
                node = {}
                idx_key = str(val[0])
                if idx_key in mon_node:
                    node = mon_node.get(idx_key)
                node.update({val[1]: int(mon_val[idx])})
                mon_node.update({idx_key: node})
            for each in mon_node:
                mon_node[each] = sorted(
                    mon_node[each].items(), key=lambda d: d[0])
            result.append({
                'DPID': key[0],
                'CODE': key[1],
                'VALUE': mon_node,
            })
    current_app.logger.debug('format done!')
    write_bulk_mongo('evaluate_code_history', result)


def analysis_check_type(data):
    # 按检查类型【路局评价、站段评价】分类统计
    result = []
    for x in range(3, 6):
        xdata = data[data['TYPE{}'.format(x)].notnull()]
        xdata = xdata.groupby(
            ['TYPE{}'.format(x), 'MON', 'CCODE', 'CHECK_TYPE'],
            as_index=False).size()
        xdata = xdata.reset_index()
        for val in xdata.values:
            result.append({
                'DPID': val[0],
                'MON': val[1],
                'CODE': val[2],
                'CHECK_TYPE': val[3],
                'COUNT': val[4]
            })
    current_app.logger.debug('complete: format data.')
    
    coll_name = '{}detail_evaluate_check_type'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_index(
        [('DPID', ASCENDING), ('MON', ASCENDING)], background=True)


def analysis_score(data):
    # 单个问题分数段统计
    result = []
    for x in range(3, 6):
        xdata = data[data['TYPE{}'.format(x)].notnull()]
        xdata = xdata.groupby(
            ['TYPE{}'.format(x), 'MON', 'CCODE', 'LEVEL'],
            as_index=False).size()
        xdata = xdata.reset_index()
        for val in xdata.values:
            result.append({
                'DPID': val[0],
                'MON': val[1],
                'CODE': val[2],
                'LEVEL': val[3],
                'COUNT': val[4]
            })
    current_app.logger.debug('complete: format data.')
    
    coll_name = '{}detail_evaluate_single_score'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_index(
        [('DPID', ASCENDING), ('MON', ASCENDING)], background=True)


def analysis_grada(data):
    # 职务分类：局管领导人员、正科级干部、副科级及以下
    result = []
    for x in range(3, 6):
        xdata = data[data['TYPE{}'.format(x)].notnull()]
        xdata = xdata.groupby(
            ['TYPE{}'.format(x), 'MON', 'CCODE', 'GRADA_ID'],
            as_index=False).size()
        xdata = xdata.reset_index()
        for val in xdata.values:
            result.append({
                'DPID': val[0],
                'MON': val[1],
                'CODE': val[2],
                'GRADA_ID': val[3],
                'COUNT': val[4]
            })
    current_app.logger.debug('complete: format data.')
    
    coll_name = '{}detail_evaluate_grada'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_index(
        [('DPID', ASCENDING), ('MON', ASCENDING)], background=True)


def analysis_base(data):
    evaluate_config_sql = """SELECT
            PK_ID, SITUATION
        FROM
            t_check_evaluate_config;
    """
    evaluate_config = pd_query(evaluate_config_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'CONFIG_ID': evaluate_config['PK_ID'],
            'SITUATION': evaluate_config['SITUATION'],
        }),
        how='left',
        left_on='FK_CHECK_EVALUATE_CONFIG_ID',
        right_on='CONFIG_ID')

    major_sql = "SELECT DEPARTMENT_ID, TYPE2, NAME FROM t_department;"
    major_data = pd_query(major_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'TYPE2': major_data['TYPE2']
        }),
        how='left',
        left_on='TYPE3',
        right_on='DPID')
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'MAJOR': major_data['NAME']
        }),
        how='left',
        left_on='TYPE2',
        right_on='DPID')
    del data['TYPE2']
    del data['DPID_x']
    del data['DPID_y']
    majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    data = data.loc[data['MAJOR'].isin(majors)]
    result = []
    for idx in data.index:
        documnet = data.loc[idx]
        result.append({
            'PK_ID': int(documnet['PK_ID']),
            'ALL_NAME': documnet['ALL_NAME'],
            'MAJOR': documnet['MAJOR'],
            'TYPE3': documnet['TYPE3'],
            'TYPE4': documnet['TYPE4'],
            'TYPE5': documnet['TYPE5'],
            'PERSON_NAME': documnet['NAME'],
            'PERSON_ID': documnet['ID'],
            'MON': int(documnet['MON']),
            'DATE': int(documnet['DATE']),
            'CODE_ADDITION': documnet['CODE_ADDITION'],
            'CODE': documnet['CCODE'],
            'CHECK_TYPE': int(documnet['CHECK_TYPE']),
            'GRADA_ID': int(documnet['GRADA_ID']),
            'LEVEL': int(documnet['LEVEL']),
            'SITUATION': documnet['SITUATION'].strip(),
            'GRADATION': documnet['GRADATION'],
            'SCORE': documnet['SCORE'],
            'ITEM_NAME': documnet['ITEM_NAME'],
            "IDENTITY": documnet['IDENTITY']
        })
    current_app.logger.debug('complete: format data.')
    coll_name = '{}detail_evaluate_record'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def merge_mon(year, mon):
    return (year * 100) + mon


def analysis_data(data):
    # 对数据按照业务逻辑各种计算
    data.rename(
        columns={
            "FK_PERSON_GRADATION_RATIO_ID": "GRADA_ID",
            "RESPONSIBE_ID_CARD": "ID",
            "RESPONSIBE_PERSON_NAME": "NAME"
        },
        inplace=True)
    data['CCODE'] = data['CODE_ADDITION'].apply(lambda x: x[:2])
    data['GRADA_ID'] = data['GRADA_ID'].apply(lambda x: 3 if x > 3 else x)
    data['LEVEL'] = data.loc[:, 'SCORE'].apply(singe_score_section)
    data['MON'] = data.apply(
        lambda row: merge_mon(row['YEAR'], row['MONTH']), axis=1)
    data['DATE'] = data['CREATE_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    del data['CREATE_TIME']
    analysis_base(data.copy())
    analysis_check_type(data.copy())
    analysis_score(data.copy())
    analysis_grada(data.copy())
    get_evaluate_count(data.copy())
    get_mean_evaluate(data)  # yes
    # get_history_dpid_code_grada(data)
    # get_code_count(data)


def handle(year, month):
    sql_evaluate = """SELECT
        a.PK_ID,
        a.FK_CHECK_EVALUATE_CONFIG_ID,
        a.CODE_ADDITION,
        a.CHECK_TYPE,
        a.SCORE,
        a.CREATE_TIME,
        a.YEAR,
        a.MONTH,
        a.FK_PERSON_GRADATION_RATIO_ID,
        a.RESPONSIBE_ID_CARD,
        a.RESPONSIBE_PERSON_NAME,
        a.GRADATION,
        b.IDENTITY,
        c.ALL_NAME,
        c.TYPE3,
        c.TYPE4,
        c.TYPE5,
        d.ITEM_NAME
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_check_evaluate_config AS d on d.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
    WHERE a.YEAR = {0} AND a.MONTH = {1};
    """.format(year, month)

    data = pd_query(sql_evaluate)
    if data.iloc[:, 0].size == 0:
        current_app.logger.debug('evaluate fetch 0 records.')
        return
    analysis_data(data.copy())


def revocation(mon_point):
    SQL = f'''SELECT
        YEAR, MONTH, REVOCATION_TIME, FK_CHECK_EVALUATE_INFO_ID
    FROM
        t_check_evaluate_revocation
    WHERE
        REVOCATION_TIME >= '{mon_point[0]}'
        and REVOCATION_TIME <= '{mon_point[1]}';
    '''
    data = pd_query(SQL)
    pk_ids = list(set(data['FK_CHECK_EVALUATE_INFO_ID']))
    prefixes = ['history_', 'monthly_', 'daily_']
    coll_name = 'detail_evaluate_record'
    for prefix in prefixes:
        coll = prefix + coll_name
        mongo.db[coll].delete_many({'PK_ID': {"$in": pk_ids}})


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    year = months.end_date.year
    month = months.end_date.month
    handle(year, month)
    revocation(mon_point)  # 删除已经撤销的历史记录
    return 'OK'


if __name__ == '__main__':
    current_app.logger.debug('Done!')
