#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/5
Description: 综合指数-电务 指数预警
"""
import pymongo
import pandas as pd

from app import mongo
from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex, MongoCollection
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.util import (get_coll_prefix, get_history_months)

MAJOR = MajorClass.dianwu
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 60),
    MainIndex.check_intensity: Interval.closed_open(0, 60),
    MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
    MainIndex.assess_intensity: Interval.closed_open(0, 75),
    MainIndex.check_evenness: Interval.closed_open(0, 35),
    MainIndex.problem_exposure: Interval.closed_open(0, 30)
}

_desc_dict = {
    'inspect': """
I级警告预警：最终安全综合管理指数单月得分在50分及以下的单位，其中成都电务综合维修段为45分及以下。
对其进行为期100天的差异化精准I级警告预警。
II级警告预警：最终安全综合管理指数单月得分在50分（不含）至60分（含）的单位，
其中成都电务综合维修段为45分（不含）至55分（含）。对其进行为期30天的差异化精准II级警告预警。
III级警告预警：
1.最终安全综合管理指数在60分（不含）至65分（含）的单位，其中成都电务综合维修段为55分（不含）至60分（含）。
2.检查力度低于80分及以下、考核力度低于80分及以下、评价力度低于60分及以下，满足其中任何一项条件的单位，
其中成都电务综合维修段预警指标均下调5分。
3.安全综合管理指数连续3月呈下降趋势，且降幅达到20分（含）。
""",
    'review': """I级、II级、III级警告预警解除：预警期内未再次达到III级及以上差异化精准警告预警标准。"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_closed(0, 50)
    }

    def before_execute_inspection(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            self.threshold_dict = {
                MainIndex.total_index: Interval.closed_closed(0, 45)
            }

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.open_closed(50, 60)
    }

    def before_execute_inspection(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            self.threshold_dict = {
                MainIndex.total_index: Interval.open_closed(45, 55)
            }

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 1
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 0

    threshold_dict = {
        MainIndex.total_index: Interval.open_closed(60, 65),
        MainIndex.check_intensity: Interval.closed_open(0, 80),
        MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
        MainIndex.assess_intensity: Interval.closed_open(0, 80)
    }

    specific_triggered_index = (MainIndex.total_index,)

    def before_execute_inspection(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            self.threshold_dict = {
                MainIndex.total_index: Interval.open_closed(55, 60),
                MainIndex.check_intensity: Interval.closed_open(0, 75),
                MainIndex.evaluate_intensity: Interval.closed_open(0, 55),
                MainIndex.assess_intensity: Interval.closed_open(0, 75)
            }

    def inspect_type_dianwu(self):
        """
        安全综合管理指数连续3月呈下降趋势，且降幅达到20分（含）
        :return:
        """
        mon_de_list = [self.basic_data.months_ago - x for x in range(3, -1, -1)]
        prefix_list = [get_coll_prefix(x) for x in mon_de_list]
        if len(set(prefix_list)) == 1:  # 如果prefix都相同
            past_months = list(map(lambda x: get_history_months(x)[0], mon_de_list))
            index_coll_name = '{}{}'.format(prefix_list[0],
                                            MongoCollection.health_index.value)
            past_data = mongo.db[index_coll_name].find(
                {'MON': {'$in': past_months},
                 'MAJOR': MAJOR.value,
                 'DEPARTMENT_ID': self.basic_data.row_data['DEPARTMENT_ID']},
                {'_id': 0}).sort('MON', pymongo.ASCENDING)
            data_df = pd.DataFrame(list(past_data))
            data_df = data_df['SCORE'].diff().dropna()
            if data_df.count() >= 3 and data_df.max() <= -20:
                return True
        else:
            data_list = []
            for mon in mon_de_list:
                calc_month = get_history_months(mon)[0]
                index_coll_name = '{}{}'.format(get_coll_prefix(mon),
                                                MongoCollection.health_index.value)
                past_data = mongo.db[index_coll_name].find(
                    {'MON': calc_month,
                     'MAJOR': MAJOR.value,
                     'DEPARTMENT_ID': self.basic_data.row_data['DEPARTMENT_ID']},
                    {'_id': 0})
                data_list.extend(list(past_data))
            data_df = pd.DataFrame(list(data_list))
            data_df = data_df['SCORE'].diff().dropna()
            if data_df.count() >= 3 and data_df.max() <= -20:
                return True
        return False

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
