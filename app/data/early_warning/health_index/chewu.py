#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/3
Description: 综合指数-车务 指数预警
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval


MAJOR = MajorClass.chewu
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 70),
    MainIndex.check_intensity: Interval.closed_open(0, 80),
    MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
    MainIndex.assess_intensity: Interval.closed_open(0, 80),
    MainIndex.check_evenness: Interval.closed_open(0, 65),
    MainIndex.problem_exposure: Interval.closed_open(0, 50)
}


_desc_dict = {
    'inspect': """
综合指数得分：得分70分以下
检查力度指数：得分80分以下
评价力度指数：得分60分以下
考核力度指数：得分80分以下
检查均衡度指数：得分65分以下
问题暴露指数：得分50分以下
I级差异化精准预警：以上6项全部满足；综合指数55分以下；
II级差异化精准预警：以上6项满足4项；检查力度、评价力度、考核力度3项满足；综合指数60分以下；
III级差异化精准预警：以上第2项～第6项任意2项满足；第2项～第6项任意1项指标连续3个月在门限值以下; 
检查均衡度指数60分以下，问题暴露指数45分以下。
""",
    'review': """
I级警告预警：6项指标全部达到门限值直接解除预警。综合指数达到60分以上，且各项指标在III级预警控制范围内。
II级警告预警：4项指标全部达到门限值直接解除预警，检查力度、评价力度、考核力度3项指标达到门限值，
综合指数达到60分以上，且其余各项指标在III级预警控制范围内。
III级警告预警：各项指标均达到门限值解除预警。
"""}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 6

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }

    def review(self):
        """
        6项指标全部达到门限值直接解除预警。
        综合指数达到60分以上，且各项指标在III级预警控制范围内。
        :return:
        """
        if len(self.basic_data.triggered_index_list) > 0:
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60)
    }

    specific_triggered_index = (MainIndex.check_intensity, MainIndex.evaluate_intensity, MainIndex.assess_intensity)

    def review(self):
        """
        1. 4项指标全部达到门限值直接解除预警
        2. 检查力度、评价力度、考核力度3项指标达到门限值
        3. 综合指数达到60分以上
        4. 且其余各项指标在III级预警控制范围内。
        :return:
        """
        if set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                [x.value for x in self.basic_data.triggered_index_list]):
            return False
        if set(self.specific_triggered_index).intersection(self.basic_data.triggered_index_list):
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        if set([x for x in MainIndex if x != MainIndex.total_index]).intersection(
                self.basic_data.triggered_index_list):
            return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 2
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 1
    # 不加入个数比较指数列表
    triggered_index_exclude_list = [MainIndex.total_index]
    continuous_triggered_index_exclude_list = [MainIndex.total_index]

    extra_threshold_dict = {
        MainIndex.check_intensity: Interval.closed_open(0, 60),
        MainIndex.problem_exposure: Interval.closed_open(0, 45)
    }

    def review(self):
        """各项指标均达到门限值解除预警"""
        if len(self.basic_data.triggered_index_list) == 0:
            return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        # manager.execute_inspection(-8)
        # manager.review_warning(-8)
        # manager.execute_inspection(-7)
        # manager.review_warning(-7)
        # manager.execute_inspection(-6)
        # manager.review_warning(-6)
        manager.execute_inspection(-5)
        # manager.review_warning(-5)
        # manager.execute_inspection(-4)
        # manager.review_warning(-4)
        # manager.execute_inspection(-3)
        # manager.review_warning(-3)
        # manager.execute_inspection(-2)
        # manager.review_warning(-2)
        # manager.execute_inspection(-1)
        # manager.review_warning(-1)

