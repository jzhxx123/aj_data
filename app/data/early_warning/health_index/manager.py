#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/4
Description: 
"""

import pandas as pd

from flask import current_app
from app import mongo
from app.data.early_warning.enums import WarningLevel, MajorClass, MongoCollection, MainIndex
from app.data.util import (get_coll_prefix, get_history_months, write_bulk_mongo)
from app.data.early_warning.base import WaringLevelInspection, IndexWarningBasicData, InspectManagerBase


class InspectManager(InspectManagerBase):
    index_coll_name = MongoCollection.health_index.value
    detail_index_coll_name = MongoCollection.detail_health_index.value
    early_warning_coll_name = MongoCollection.health_index_early_warning

    def __init__(self, index_name, major, hierarchy, level_check_class, threshold_dict, desc_dict):
        super(InspectManager, self).__init__(index_name, major, hierarchy, level_check_class, threshold_dict, desc_dict)


def get_manager(index_name, major, hierarchy, level_check_class, threshold_dict, desc_dict):
    return InspectManager(index_name, major, hierarchy, level_check_class, threshold_dict, desc_dict)
