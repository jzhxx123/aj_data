#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/5
Description: 综合指数-工电 指数预警
"""


from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval

MAJOR = MajorClass.gw_gongdian
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {}

_review_threshold_dict = {
        MainIndex.total_index: Interval.closed_closed(68, 100),
        MainIndex.check_intensity: Interval.closed_closed(74, 100),
        MainIndex.evaluate_intensity: Interval.closed_closed(57, 100),
        MainIndex.assess_intensity: Interval.closed_closed(82, 100),
        MainIndex.check_evenness: Interval.closed_closed(45, 100),
        MainIndex.problem_exposure: Interval.closed_closed(45, 100),
        MainIndex.problem_rectification: Interval.closed_closed(45, 100)
}

_desc_dict = {
    'inspect': """
I级警告预警条件：
①单月安全综合指数总指数低于55分(不含)；
②安全综合指数总指数连续三个月介于55分（含）至62分（不含）。
Ⅱ级警告预警条件：
①单月安全综合指数总指数介于55分(含) 至62分（不含）；
②安全综合指数总指数连续三个月介于62分（含）至66分（不含）。
Ⅲ级警告预警条件：
①单月安全综合指数总指数介于62分（含）至66分(不含) 或连续三个月介于66分（含）至68分（不含）；
②单月检查力度指数低于72分(不含) 或连续三个月介于72分（含）至74分（不含）；
③单月检查均衡度指数低于42分(不含) 或连续三个月介于42（含）至45分（不含）；
④单月评价力度指数低于55分(不含) 或连续三个月介于55（含）至57分（不含）；
⑤单月问题整改效果指数低于40分(不含) 或连续三个月介于40（含）至42分（不含）；
⑥单月考核力度指数低于80分(不含) 或连续三个月介于80（含）至82分（不含）；
⑦单月问题暴露指数低于40分(不含) 或连续三个月介于40（含）至45分（不含）。
以上条件第1项满足或第2项～第7项满足任意两项即自动预警。
""",
    'review': """
（1）次月安全综合指数总指数高于68分（含）；
（2）次月检查力度指数高于74分（含）；
（3）次月检查均衡度指数高于45分（含）；
（4）次月评价力度指数高于57分（含）；
（5）次月问题整改效果指数高于42分（含）；
（6）次月考核力度指数高于82分（含）；
（7）次月问题暴露指数高于45分(含) 。
各项预警验收第1项必须满足且第2项～第7项要满足任意5项即解除预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1
    continuous_triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }

    continuous_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(55, 62)
    }

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第7项要满足任意5项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 6:
                return True
        return False


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 1
    continuous_triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(55, 62)
    }

    continuous_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(62, 66)
    }

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第7项要满足任意5项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 6:
                return True
        return False


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 0
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 0

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(62, 66),
        MainIndex.check_intensity: Interval.closed_open(0, 72),
        MainIndex.evaluate_intensity: Interval.closed_open(0, 55),
        MainIndex.assess_intensity: Interval.closed_open(0, 80),
        MainIndex.check_evenness: Interval.closed_open(0, 42),
        MainIndex.problem_exposure: Interval.closed_open(0, 40),
        MainIndex.problem_rectification: Interval.closed_open(0, 40)
    }

    continuous_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(66, 68),
        MainIndex.check_intensity: Interval.closed_open(72, 74),
        MainIndex.evaluate_intensity: Interval.closed_open(55, 57),
        MainIndex.assess_intensity: Interval.closed_open(80, 82),
        MainIndex.check_evenness: Interval.closed_open(42, 45),
        MainIndex.problem_exposure: Interval.closed_open(40, 45),
        MainIndex.problem_rectification: Interval.closed_open(40, 42)
    }

    def inspect_type_gongdian(self):
        if MainIndex.total_index in self.basic_data.single_or_continuous_triggered_index_list:
            return True
        elif len([x for x in self.basic_data.single_or_continuous_triggered_index_list
                 if x != MainIndex.total_index]) >= 2:
            return True
        return False

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第7项要满足任意5项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 6:
                return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
