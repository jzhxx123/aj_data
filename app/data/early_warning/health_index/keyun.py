    #!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/3
Description: 综合指数-客运 指数预警
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval


MAJOR = MajorClass.keyun
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 60),
    MainIndex.check_intensity: Interval.closed_open(0, 60),
    MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
    MainIndex.assess_intensity: Interval.closed_open(0, 75),
    MainIndex.check_evenness: Interval.closed_open(0, 35),
    MainIndex.problem_exposure: Interval.closed_open(0, 30)
}

_desc_dict = {
    'inspect': """
安全综合指数得分60分以下
检查力度指数60分以下
评价力度指数 60分以下
考核力度指数75分以下
检查均衡度指数35分以下
问题暴露指数30分以下
I级差异化精准预警：以上6项全部满足；综合指数50分以下；
II级差异化精准预警：以上6项满足4项；检查力度、评价力度、考核力度3项满足；综合指数55分以下；
III级差异化精准预警：以上2至6项任意2项满足；2至6项任意1项指标连续3个月在门限值以下。
""",
    'review': """
I级差异化精准预警解除及延续条件：6项指标全部达到门限值直接解除预警。综合指数达到60分以上，
且各项指标在III级预警控制范围内。
II级差异化精准预警解除及延续条件：4项指标全部达到门限值，检查力度、评价力度、考核力度3项指标达到门限值，
综合指数达到60分以上，且其余各项指标在III级预警控制范围内。
III级差异化精准预警解除及延续条件：各项指标均达到门限值解除预警。未达到解除预警条件的自动延续预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 6

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 50)
    }

    def review(self):
        """
        6项指标全部达到门限值直接解除预警。
        综合指数达到60分以上，且各项指标在III级预警控制范围内。
        :return:
        """
        if len(self.basic_data.triggered_index_list) > 0:
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }

    specific_triggered_index = (MainIndex.check_intensity, MainIndex.evaluate_intensity, MainIndex.assess_intensity)

    def review(self):
        """
        1. 4项指标全部达到门限值直接解除预警
        2. 检查力度、评价力度、考核力度3项指标达到门限值
        3. 综合指数达到60分以上
        4. 且其余各项指标在III级预警控制范围内。
        :return:
        """
        if set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                [x.value for x in self.basic_data.triggered_index_list]):
            return False
        if set(self.specific_triggered_index).intersection(self.basic_data.triggered_index_list):
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        if set([x for x in MainIndex if x != MainIndex.total_index]).intersection(
                self.basic_data.triggered_index_list):
            return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 2
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 1
    # 不加入个数比较指数列表
    triggered_index_exclude_list = [MainIndex.total_index]
    continuous_triggered_index_exclude_list = [MainIndex.total_index]

    def review(self):
        """各项指标均达到门限值解除预警"""
        if len(self.basic_data.triggered_index_list) == 0:
            return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
