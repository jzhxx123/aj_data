#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/4
Description: 综合指数-机务 指数预警
"""


from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval

MAJOR = MajorClass.jiwu
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 55)
}

_desc_dict = {
    'inspect': """
1.单项指数预警
（1）I级警告预警条件：单月安全综合指数低于55分。
（2）II级警告预警条件（以下条件任意一项；分值处于区段时，含下限、不含上限）：
①单月安全综合指数在55分至60分之间；
②单月检查力度指数低于70分；
③单月评价力度指数低于60分；
④单月考核力度指数低于70分；
⑤单月检查均衡度指数低于43分。
（3）III级警告预警条件（以下条件任意一项；分值处于区段时，含下限、不含上限）：
①单月安全综合指数在60分至63分之间；
②单月检查力度指数在70分至75分之间；
③单月评价力度指数在60分至63分之间；
④单月考核力度指数在70分至80分之间；
⑤单月检查均衡度指数在43分至48分之间；
⑥单月问题暴露指数低于40分。
2.组合指数预警
（1）II级警告预警条件：满足单项指数预警“III级预警条件”中的任意三个条件。
""",
    'review': """
（1）I级警告预警（到期前30天内）：单月安全综合指数高于55分，且各项指数不在III级预警范围内。
（2）II级警告预警（预警指数达到以下相应值）：单月安全综合指数高于60分，单月检查力度指数高于70分，
单月评价力度指数高于60分，单月考核力度指数高于70分，单月检查均衡度指数高于43分，且各项指数不在III级预警范围内。
（3）III级警告预警：各项预警指数不再预警范围内。
评价力度指数受事故、故障倒查评价影响时除外。如果同时满足多级预警条件时，执行高级别预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1

    def review(self):
        """单月安全综合指数高于55分，且各项指数不在III级预警范围内。"""
        if self.basic_data.triggered_index_list:
            return False
        self.threshold_dict = LevelThree.threshold_dict
        self.calc_triggered_index()
        if self.basic_data.triggered_index_list:
            return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(55, 60),
        MainIndex.check_intensity: Interval.closed_open(0, 70),
        MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
        MainIndex.assess_intensity: Interval.closed_open(0, 70),
        MainIndex.check_evenness: Interval.closed_open(0, 43)
    }

    def inspect_type_jiwu(self):
        """
        满足自身阈值其中一个或者满足三级预警阈值其中3个
        :return:
        """
        if len(self.basic_data.triggered_index_list) > 0:
            return True
        self.threshold_dict = LevelThree.threshold_dict
        self.calc_triggered_index()
        if len(self.basic_data.triggered_index_list) >= 3:
            return True
        return False

    def review(self):
        """
        1:（预警指数达到以下相应值）：单月安全综合指数高于60分，单月检查力度指数高于70分，
        单月评价力度指数高于60分，单月考核力度指数高于70分，单月检查均衡度指数高于43分，
        2: 且各项指数不在III级预警范围内。
        :return:
        """
        self.threshold_dict = {
            MainIndex.total_index: Interval.open_closed(60, 100),
            MainIndex.check_intensity: Interval.open_closed(70, 100),
            MainIndex.evaluate_intensity: Interval.open_closed(60, 100),
            MainIndex.assess_intensity: Interval.open_closed(70, 100),
            MainIndex.check_evenness: Interval.open_closed(43, 100)
        }
        self.calc_triggered_index()
        fail_index_list = set(self.basic_data.row_data['INSPECT_DATA']['triggered_index'])
        if not fail_index_list == fail_index_list.intersection([x.value for x in self.basic_data.triggered_index_list]):
            return False
        self.threshold_dict = LevelThree.threshold_dict
        self.calc_triggered_index()
        if self.basic_data.triggered_index_list:
            return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(60, 63),
        MainIndex.check_intensity: Interval.closed_open(70, 75),
        MainIndex.evaluate_intensity: Interval.closed_open(60, 63),
        MainIndex.assess_intensity: Interval.closed_open(70, 80),
        MainIndex.check_evenness: Interval.closed_open(43, 48),
        MainIndex.problem_exposure: Interval.closed_open(0, 40)
    }

    # 要求低于阈值的指数个数
    triggered_index_require = 1

    def review(self):
        """
        各项预警指数不再预警范围内。评价力度指数受事故、故障倒查评价影响时除外。
        :return:
        """
        if self.basic_data.triggered_index_list:
            return False
        return True


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        # manager.execute_inspection(-1)
        manager.review_warning(-1)
