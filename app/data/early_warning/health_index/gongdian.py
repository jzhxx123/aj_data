#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/5
Description: 综合指数-供电 指数预警
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.scheme import scheme
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval


MAJOR = MajorClass.gongdian
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 60),
    MainIndex.check_intensity: Interval.closed_open(0, 80),
    MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
    MainIndex.check_evenness: Interval.closed_open(0, 45),
    MainIndex.problem_exposure: Interval.closed_open(0, 50)
}

_desc_dict = {
    'inspect': """
①安全综合指数得分：月度安全综合指数得分60分以下；
②检查力度指数：检查力度80分以下；
③评价力度指数：得分60分以下；
④检查均衡度指数：得分45分以下；
⑤问题暴露指数：得分50分以下；
I级警告预警条件：以上5项全部满足且综合指数50分以下；
II级警告预警条件：以上第2-第5项满足其中3项或综合指数60分以下；
III级警告预警条件：以上第2项-第5项中任意2项满足；第2项-第5项中任意1项指标连续3个月在门限值以下。
""",
    'review': """
I级警告预警
5项指标全部达到门限值。综合指数达到60分以上，且各项指标在III级预警控制范围内。
II级警告预警
3项指标全部达到门限值，综合指数达到60分以上，且其余各项指标在III级预警控制范围内。
III级警告预警
各项指标均达到门限值。未达到门限值的自动延续预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 5

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 50)
    }

    def review(self):
        """
        5项指标全部达到门限值。
        综合指数达到60分以上，
        且各项指标在III级预警控制范围内。
        :return:
        """
        if set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                [x.value for x in self.basic_data.triggered_index_list]):
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        if self.basic_data.triggered_index_list:
            return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 0
    continuous_triggered_index_require = 0

    def inspect_type_gongdian(self):
        if MainIndex.total_index in self.basic_data.single_or_continuous_triggered_index_list:
            return True
        elif len([x for x in self.basic_data.single_or_continuous_triggered_index_list
                  if x != MainIndex.total_index]) >= 3:
            return True
        return False

    def review(self):
        """
        3项指标全部达到门限值，
        综合指数达到60分以上，
        且其余各项指标在III级预警控制范围内。
        :return:
        """
        if set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                [x.value for x in self.basic_data.triggered_index_list]):
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        if self.basic_data.triggered_index_list:
            return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 2
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 1
    # 不加入个数比较指数列表
    triggered_index_exclude_list = [MainIndex.total_index]
    continuous_triggered_index_exclude_list = [MainIndex.total_index]

    def review(self):
        """
        各项指标均达到门限值
        :return:
        """
        if self.basic_data.triggered_index_list:
            return False
        return True


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)

