#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/5
Description:  综合指数-车辆 指数预警
"""


from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.health_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

MAJOR = MajorClass.cheliang
INDEX_NAME = '{}安全综合指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.check_intensity: Interval.closed_open(0, 67),
    MainIndex.evaluate_intensity: Interval.closed_open(0, 60),
    MainIndex.assess_intensity: Interval.closed_open(0, 75),
    MainIndex.check_evenness: Interval.closed_open(0, 65),
    MainIndex.problem_exposure: Interval.closed_open(0, 55),
    MainIndex.problem_rectification: Interval.closed_open(0, 50)
}

_desc_dict = {
    'inspect': """
(一)分项指数预警阀门值
（1）检查力度指数70分
（2）评价力度指数60分
（3）考核力度75分
（4）检查均衡度65分
（5）问题暴露度55分
（6）问题整改度50分
(二)安全综合指数预警条件
I级预警
1.总指数：得分低于55分；
2.分项指数阀门值：以上6项任意5项低于阀门值。
II级预警
1.总指数：得分低于60分；
2.分项指数阀门值：以上6项任意4项低于阀门值。
III级预警
1.总指数：得分低于65分或连续三个月低于70分；
2.分项指数阀门值：以上6项任意3项低于阀门值
""",
    'review': """
I级预警
次月总指数高于75分，同时所有被预警指数不得低于阀门值分数。次月未能达到解除条件将继续一级预警。
II级预警
次月总指数得分高于70分，同时所有被预警指数不得低于阀门值分数。次月未能达到解除条件将继续二级预警，
二级预警延期超过一个月则自动升级为一级预警。
III级预警
次月总指数得分高于65分，同时被预警分项指数不得低于阀门值。次月未能达到解除条件将继续三级预警，
三级预警延期超过一个月则自动升级为二级预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 5

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }

    def review(self):
        """
        次月总指数高于75分，
        同时所有被预警指数不得低于阀门值分数。次月未能达到解除条件将继续一级预警。
        :return:
        """
        if round(self.basic_data.score_dict.get(MainIndex.total_index)) > 75:
            if not set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                    [x.value for x in self.basic_data.triggered_index_list]):
                return True
        return False


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60)
    }

    def review(self):
        """
        次月总指数得分高于70分，同时所有被预警指数不得低于阀门值分数。
        次月未能达到解除条件将继续二级预警，二级预警延期超过一个月则自动升级为一级预警。
        :return:
        """
        if round(self.basic_data.score_dict.get(MainIndex.total_index)) > 70:
            if not set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                    [x.value for x in self.basic_data.triggered_index_list]):
                return True
        return False


class LevelThree(WaringLevelInspection):

    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 3

    triggered_index_exclude_list = [MainIndex.total_index]

    continuous_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 70)
    }

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 65)
    }

    def inspect_type_cheliang(self):
        """
        1.总指数：得分低于65分或连续三个月低于70分；
        2.分项指数阀门值：以上6项任意3项低于阀门值
        :return:
        """
        if MainIndex.total_index in self.basic_data.continuous_triggered_index_list:
            return True
        return False

    def review(self):
        """
        次月总指数得分高于65分，同时被预警分项指数不得低于阀门值。
        次月未能达到解除条件将继续三级预警，三级预警延期超过一个月则自动升级为二级预警。
        :return:
        """
        if round(self.basic_data.score_dict.get(MainIndex.total_index)) > 65:
            if not set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                    [x.value for x in self.basic_data.triggered_index_list]):
                return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, HIERARCHY, level_check_class, _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        # manager.execute_inspection(-2)

        manager.review_warning(-1)
