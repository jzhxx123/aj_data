#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/9
Description: 
"""
import os
import json

import requests
from flask import current_app

from app import mongo
from app.data.util import get_history_months
from app.utils.common_func import get_today
from app.data.early_warning.enums import MongoCollection
from app.data.early_warning.inspect.service import execute_inspection


class Scheme(object):
    api = 'http://10.192.32.119:8777/external/addWarningNotificationIndex'

    def __init__(self):
        self.inspect_index_list = []
        # for coll in [MongoCollection.health_index_early_warning.value,
        #              MongoCollection.major_index_early_warning.value]:
        #     if mongo.db is not None:
        #         mongo.db[coll].create_index('PARENT_ID', unique=False)

    def register_inspect_index(self, manager):
        if manager not in self.inspect_index_list:
            self.inspect_index_list.append(manager)

    def save_to_anjian(self, months_ago):
        """
        发送到安监系统的数据接口
        :param months_ago:
        :return:
        """
        data = []
        months_ago = self.validate_month(months_ago)
        mon = get_history_months(months_ago)[0]
        condition = {'INSPECT_MON': mon}
        health_index_data = mongo.db[MongoCollection.health_index_early_warning.value].find(condition)
        major_index_data = mongo.db[MongoCollection.major_index_early_warning.value].find(condition)
        data_set = {0: health_index_data, 1: major_index_data}  # 0：综合指数 1：重点指数
        for key, index_data in data_set.items():
            for row in index_data:
                record = dict()
                record['recordId'] = str(row['_id'])
                record['departmentId'] = row['DEPARTMENT_ID']
                record['departmentName'] = row['DEPARTMENT_NAME']
                record['recordType'] = row['RECORD_TYPE']
                record['level'] = row['CURRENT_WARNING_LEVEL']
                record['year'] = int(str(row['INSPECT_MON'])[:4])
                record['month'] = int(str(row['INSPECT_MON'])[4:])
                record['indexType'] = key
                record['riskId'] = ','.join([str(x) for x in row['RISK_ID']]) if row.get('RISK_ID') else ''
                record['parentId'] = row['PARENT_ID']
                record['content'] = row['DESC_DATA']
                record['indexName'] = row['INDEX_NAME']
                record['indexCode'] = row.get('INDEX_CODE', '')
                record['score'] = row['SCORE_DATA'].get('0', 0)
                data.append(record)
        current_app.logger.debug('|--early warning start sending request to api'.format(self.api))
        max_retry = 5
        while max_retry > 0:
            try:
                res = requests.post(self.api,
                                    data=dict(warningNotificationIndexArray=json.dumps(data, ensure_ascii=False)),
                                    timeout=300)
                response_json = res.json()
                if res.status_code != 200:
                    current_app.logger.error('early warning failed sending request to api{}, get code: {}'.format(
                        self.api, res.status_code))
                    max_retry -= 1
                    continue
                if response_json.get('success'):
                    current_app.logger.debug('early warning success sending request to api'.format(self.api))
                else:
                    current_app.logger.error('early warning failed sending request to api{}, msg: {}'.format(
                        self.api, response_json.get('msg')))
                return
            except requests.exceptions.ReadTimeout:
                current_app.logger.warning('early warning timeout when request for {}'.format(self.api))
                return
            except Exception as e:
                current_app.logger.error('early warning error when request for {}, msg: {}'.format(self.api, e))
                return

    @staticmethod
    def validate_month(months_ago):
        if months_ago < 0:
            raise ValueError(
                f'months_agos-[{months_ago}] should not less than 0')

        months_ago *= -1
        if get_history_months(months_ago)[0] < 201710:
            raise ValueError(f'months_ago-[{months_ago}] param error')

        # # 判断是否是25-月底
        #
        # if months_ago == 0 and get_today().day < current_app.config.get(
        #         'UPDATE_DAY'):
        #     return 'NO NEED TO UPDATE'

        return months_ago

    def execute_inspection(self, months_ago):
        """
        执行预警检查任务
        :param months_ago:
        :return:
        """
        months_ago = self.validate_month(months_ago)
        current_app.logger.debug('|--early warning start execute inspect scheme')
        # for manager in self.inspect_index_list:
        #     current_app.logger.debug('||--early warning start execute inspect index: {}'.format(manager))
        #     try:
        #         manager.execute_inspection(months_ago)
        #     except Exception as e:
        #         current_app.logger.error(e)
        #         current_app.logger.debug('||--early warning error in execute inspect index: {}'.format(manager))
        #         continue
        #     current_app.logger.debug('||--early warning end execute inspect index: {}'.format(manager))
        execute_inspection(months_ago)
        current_app.logger.debug('|--early warning end execute inspect scheme')

    def execute_review(self, months_ago):
        """
        执行解除检查任务
        :param months_ago:
        :return:
        """
        months_ago = self.validate_month(months_ago)
        current_app.logger.debug('|--early warning start execute review scheme')
        for manager in self.inspect_index_list:
            current_app.logger.debug('||--early warning start execute review index: {}'.format(manager))
            try:
                manager.review_warning(months_ago)
            except Exception as e:
                current_app.logger.error(e)
                current_app.logger.debug('||--early warning error in execute review index: {}'.format(manager))
                continue
            current_app.logger.debug('||--early warning end execute review index: {}'.format(manager))
        current_app.logger.debug('|--early warning end execute review scheme')

    def run_task(self, months_ago, ignore_day_limit=False):
        """
        执行每月任务
        :param months_ago:
        :param ignore_day_limit:
        :return:
        """
        today = get_today()
        if not ignore_day_limit and today.day != 1:
            current_app.logger.debug('only on day 1 can run task')
            return 'only on day 1 can run task'
        current_app.logger.debug('|--early warning task run start')
        self.execute_inspection(months_ago)
        # self.execute_review(months_ago)
        if os.environ.get('SEND_EARLY_WARNING'):
            self.save_to_anjian(months_ago)
        current_app.logger.debug('|--early warning task run finished')
        return 'ok'


scheme = Scheme()

if __name__ == '__main__':
    from manage import app
    with app.app_context():
        scheme.run_task(3, True)


