#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description: 重点指数预警 供电-设备质量
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

RISK_TYPE = 1  # 供电-1
RISK_CONFIG_ID = [9]
MAJOR = MajorClass.gongdian
INDEX_NAME = '{}设备质量指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {}

_desc_dict = {
    'inspect': """
I级预警：单月该风险总指数得分低于50分(不含)。
Ⅱ级预警：单月该风险总指数得分介于50分（含）至60分（不含）。
Ⅲ级预警：单月该风险总指数得分介于60分（含）至65分（不含）。
单项指数预警条件：检查力度小于70分，检查均衡度小于55分，问题暴露度小于60分，其中2项指标不达标，下达Ⅲ级预警，
3项指标不达标下达Ⅱ级预警。
""",
    'review': """
次月该风险总指数得分高于65分（含）且安全综合指数得分未被预警
次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 50)
    }

    def review(self):
        """
        次月该风险总指数得分高于65分（含）且安全综合指数得分未被预警
        :return:
        """
        return False


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 0

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(50, 60),
        MainIndex.check_intensity: Interval.closed_open(0, 70),
        MainIndex.check_evenness: Interval.closed_open(0, 55),
        MainIndex.problem_exposure: Interval.closed_open(0, 60)
    }

    def inspect_type_gongdian(self):

        if MainIndex.total_index in self.basic_data.triggered_index_list:
            return True
        elif len([x for x in self.basic_data.triggered_index_list
                 if x != MainIndex.total_index]) >= 3:
            return True
        return False

    def review(self):
        """
        次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
        :return:
        """
        if len([x for x in self.basic_data.triggered_index_list
                if x != MainIndex.total_index]) <= 1:
            return True
        return False


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 0

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(60, 65),
        MainIndex.check_intensity: Interval.closed_open(0, 70),
        MainIndex.check_evenness: Interval.closed_open(0, 55),
        MainIndex.problem_exposure: Interval.closed_open(0, 60)
    }

    def inspect_type_gongdian(self):
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            return True
        elif len([x for x in self.basic_data.triggered_index_list
                  if x != MainIndex.total_index]) >= 2:
            return True
        return False

    def review(self):
        """
        次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
        :return:
        """
        if len([x for x in self.basic_data.triggered_index_list
                if x != MainIndex.total_index]) <= 1:
            return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
