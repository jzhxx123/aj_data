#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description: 重点指数预警 供电劳安
"""
from app import mongo
from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex, MongoCollection
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme
from app.data.util import get_history_months

RISK_TYPE = 6  # 供电-6
RISK_CONFIG_ID = [35]
MAJOR = MajorClass.gongdian
INDEX_NAME = '{}劳动安全指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.check_intensity: Interval.closed_open(0, 60),
    MainIndex.check_evenness: Interval.closed_open(0, 36),
    MainIndex.problem_exposure: Interval.closed_open(0, 60)
}

_desc_dict = {
    'inspect': """
I级：单月总指数得分低于45分(不含)。
Ⅱ级：单月总指数得分介于45分（含）至50分（不含）。
Ⅲ级：单月总指数得分介于50分（含）至60分（不含）。
单项指数预警条件：检查力度小于60分，检查均衡度小于36分，问题暴露度小于60分，其中2项指标不达标，下达Ⅲ级预警，
3项指标不达标下达Ⅱ级预警。
""",
    'review': """
次月总指数得分高于60分（含）且安全综合指数得分未被预警。
次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 45)
    }

    def review(self):
        """
        次月总指数得分高于60分（含）且安全综合指数得分未被预警。
        :return:
        """
        if self.basic_data.score_dict.get(MainIndex.total_index) < 60:
            return False
        condition = {
            'MAJOR': MAJOR.value,
            'INSPECT_MON': get_history_months(self.basic_data.months_ago)[0],
            'DEPARTMENT_ID': self.basic_data.row_data.get('DEPARTMENT_ID'),
            'STATUS': 1
        }
        if mongo.db[MongoCollection.health_index_early_warning.value].findone(condition):
            return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(45, 50),
        **_general_threshold_dict
    }

    def inspect_type_gongdian(self):
        """
        单项指数预警条件：检查力度小于60分，检查均衡度小于36分，问题暴露度小于60分，
        其中2项指标不达标，下达Ⅲ级预警，3项指标不达标下达Ⅱ级预警。
        :return:
        """
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            return True
        elif len([x for x in self.basic_data.triggered_index_list
                 if x != MainIndex.total_index]) >= 3:
            return True
        return False

    def review(self):
        """
        次月总指数得分高于60分（含）且安全综合指数得分未被预警。
        次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
        :return:
        """
        if round(self.basic_data.score_dict.get(MainIndex.total_index)) >= 60:
            return True
        if len([x for x in self.basic_data.triggered_index_list
                if x != MainIndex.total_index]) <= 1:
            return True
        return False


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(50, 60),
        **_general_threshold_dict
    }

    def inspect_type_gongdian(self):
        """
        单项指数预警条件：检查力度小于60分，检查均衡度小于36分，问题暴露度小于60分，
        其中2项指标不达标，下达Ⅲ级预警，3项指标不达标下达Ⅱ级预警。
        :return:
        """
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            return True
        elif len([x for x in self.basic_data.triggered_index_list
                 if x != MainIndex.total_index]) >= 2:
            return True
        return False

    def review(self):
        """
        次月总指数得分高于60分（含）且安全综合指数得分未被预警。
        次月单项指数其中2项得分高于预警条件，则取消Ⅱ、Ⅲ级预警。
        :return:
        """
        if round(self.basic_data.score_dict.get(MainIndex.total_index)) >= 60:
            return True
        if len([x for x in self.basic_data.triggered_index_list
                if x != MainIndex.total_index]) <= 1:
            return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
