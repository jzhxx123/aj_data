#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description: 重点指数预警 工电-劳安
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

RISK_TYPE = 1  # 工电-1
RISK_CONFIG_ID = [66, 30, 35]
MAJOR = MajorClass.gw_gongdian
INDEX_NAME = '{}劳动安全指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {}

_review_threshold_dict = {
        MainIndex.total_index: Interval.closed_closed(65, 100),
        MainIndex.check_intensity: Interval.closed_closed(65, 100),
        MainIndex.check_evenness: Interval.closed_closed(50, 100),
        MainIndex.problem_exposure: Interval.closed_closed(40, 100)
}

_desc_dict = {
    'inspect': """
I级：
①单月总指数低于50分(不含)；
Ⅱ级：
①单月总指数介于50分（含）至58分（不含）。
Ⅲ级：
①单月总指数介于58分（含）至65分（不含）。
②单月检查力度指数低于65分(不含)；
③单月问题暴露指数低于40分(不含)；
④单月检查均衡度指数低于50分(不含)。
以上条件第1项满足或第2项～第4项满足任意2项即自动预警。
""",
    'review': """
①次月总指数得分高于65分（含）；
②次月检查力度指数得分高于65分(含)；
③次月问题暴露指数得分高于40分(含)；
④次月检查均衡度指数得分高于50分(含)。
各项预警验收第1项必须满足且第2项～第4项要满足任意2项即解除预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 50)
    }

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第4项要满足任意2项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 3:
                return True
        return False


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 1

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(50, 58)
    }

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第4项要满足任意2项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 3:
                return True
        return False


class LevelThree(WaringLevelInspection):

    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 0
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 0

    threshold_dict = {
        MainIndex.total_index: Interval.closed_open(58, 65),
        MainIndex.check_intensity: Interval.closed_open(0, 65),
        MainIndex.check_evenness: Interval.closed_open(0, 50),
        MainIndex.problem_exposure: Interval.closed_open(0, 40)
    }

    def inspect_type_gw_gongdian(self):
        """
        ①单月总指数介于58分（含）至65分（不含）。
        ②单月检查力度指数低于65分(不含)；
        ③单月问题暴露指数低于40分(不含)；
        ④单月检查均衡度指数低于50分(不含)。
        以上条件第1项满足或第2项～第4项满足任意2项即自动预警
        """
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            return True
        elif len([x for x in self.basic_data.triggered_index_list
                 if x != MainIndex.total_index]) >= 2:
            return True
        return False

    def review(self):
        """
        各项预警验收第1项必须满足且第2项～第4项要满足任意2项即解除预警。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if MainIndex.total_index in self.basic_data.triggered_index_list:
            if len(self.basic_data.triggered_index_list) >= 3:
                return True
        return False


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
