#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description: 重点指数预警 电务劳安
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

RISK_TYPE = 5  # 电务-5
MAJOR = MajorClass.dianwu
RISK_CONFIG_ID = [30]
INDEX_NAME = '{}劳动安全指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 70),
    MainIndex.check_intensity: Interval.closed_open(0, 80),
    MainIndex.check_evenness: Interval.closed_open(0, 70),
    MainIndex.problem_exposure: Interval.closed_open(0, 70)
}

_desc_dict = {
    'inspect': """
I级：最终劳动安全风险总指数单月得分在50分及以下的单位（不含成都电务综合维修段），
对其进行为期100天的差异化精准I级警告预警。
II级：最终劳动安全风险总指数单月得分在50分（不含）至60分（含）的单位（不含成都电务综合维修段），
对其进行为期30天的差异化精准II级警告预警。
III级：最终劳动安全风险总指数在60分（不含）至65分（含）的单位，检查力度低于80分及以下、检查均衡度低于60分及以下、
问题暴露度低于70分及以下，满足其中任何一项条件的单位（以上预警条件均不含成都电务综合维修段）。
对其进行为期30天的差异化精准III级警告预警。
""",
    'review': """I级、II级、III级警告预警解除条件：预警期内未再次达到III级及以上差异化精准警告预警标准。"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    threshold_dict = {
        MainIndex.total_index: Interval.closed_closed(0, 50)
    }

    def inspect_type_dianwu(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            return False
        if len([x for x in self.basic_data.triggered_index_list
                if x not in self.triggered_index_exclude_list]) >= 1:
            return True
        return False

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2

    threshold_dict = {
        MainIndex.total_index: Interval.open_closed(50, 60)
    }

    def inspect_type_dianwu(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            return False
        if len([x for x in self.basic_data.triggered_index_list
                if x not in self.triggered_index_exclude_list]) >= 1:
            return True
        return False

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3

    threshold_dict = {
        MainIndex.total_index: Interval.open_closed(60, 65),
        MainIndex.check_intensity: Interval.closed_open(0, 80),
        MainIndex.check_evenness: Interval.closed_open(0, 60),
        MainIndex.problem_exposure: Interval.closed_open(0, 70)
    }

    def inspect_type_dianwu(self):
        if self.basic_data.row_data['DEPARTMENT_NAME'] == '成都电务综合维修段':
            return False
        if len([x for x in self.basic_data.triggered_index_list
                if x not in self.triggered_index_exclude_list]) >= 1:
            return True
        return False

    def review(self):
        """
        预警期内未再次达到III级及以上差异化精准警告预警标准。
        :return:
        """
        for cls in level_check_class:
            if cls == self.__class__:
                inst = self
            else:
                inst = cls(self.basic_data)
            result = inst.execute_level_inspection()
            if False not in result and True in result:
                return False
        return True


level_check_class = [LevelOne, LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
