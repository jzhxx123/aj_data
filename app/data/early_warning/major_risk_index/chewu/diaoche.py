#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description:  指数预警 调车风险
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

RISK_TYPE = 1  # 车务-1
RISK_CONFIG_ID = [2]
MAJOR = MajorClass.chewu
INDEX_NAME = '{}调车风险指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 65),
        MainIndex.check_intensity: Interval.closed_open(0, 65),
        MainIndex.check_evenness: Interval.closed_open(0, 65),
        MainIndex.problem_exposure: Interval.closed_open(0, 60)
    }

_special_departments = ['成都车站', '贵阳车站', '重庆车站']
_special_departments_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60),
        MainIndex.check_intensity: Interval.closed_open(0, 55),
        MainIndex.check_evenness: Interval.closed_open(0, 55),
        MainIndex.problem_exposure: Interval.closed_open(0, 55)
    }

_desc_dict = {
    'inspect': """
总指数得分：得分65分以下（三大客站60分以下）
检查力度指数：得分65以下（三大客站55分以下）
检查均衡度指数：得分65分以下（三大客站55分以下）
问题暴露指数：得分60分以下（三大客站55分以下）
II级警告预警：以上4项全满足；总指数得分55分以下。三大客站总指数得分50分以下。
III级警告预警：总指数得分65分以下（三大客站总指数得分60分以下）；第2项～第4项任意2项满足；
第2项～第4项任意1项指标连续3个月在门限值以下；检查均衡度指数60分以下（三大客站50分以下），
问题暴露指数55分以下（三大客站50分以下）。
""",
    'review': """
II级警告预警：4项指标全部达到门限值直接解除预警，总指数达到65分以上（三大客站得分60分以上），
且其余各项指标在III级预警控制范围内。
III级警告预警：各项指标均达到门限值解除预警。
未达到解除预警条件的自动延续或升级预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 6

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }

    def before_execute_inspection(self):
        """三大客站特殊对待"""
        if self.basic_data.row_data['DEPARTMENT_NAME'] in _special_departments:
            self.threshold_dict = _special_departments_threshold_dict
            self.extra_threshold_dict = {
                MainIndex.total_index: Interval.closed_open(0, 50)
            }

    def review(self):
        """
        4项指标全部达到门限值直接解除预警，总指数达到65分以上（三大客站得分60分以上），
        且其余各项指标在III级预警控制范围内。
        :return:
        """
        if self.basic_data.row_data['DEPARTMENT_NAME'] in _special_departments:
            self.threshold_dict = _special_departments_threshold_dict
        if len(self.basic_data.triggered_index_list) == 0:
            return True
        return False


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    triggered_index_exclude_list = [MainIndex.total_index]

    continuous_triggered_index_exclude_list = [MainIndex.total_index]

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 65),
        MainIndex.check_evenness: Interval.closed_open(0, 60),
        MainIndex.problem_exposure: Interval.closed_open(0, 55)
    }

    def before_execute_inspection(self):
        """三大客站特殊对待"""
        if self.basic_data.row_data['DEPARTMENT_NAME'] in _special_departments:
            self.threshold_dict = _special_departments_threshold_dict
            self.extra_threshold_dict = {
                MainIndex.total_index: Interval.closed_open(0, 60),
                MainIndex.check_evenness: Interval.closed_open(0, 50),
                MainIndex.problem_exposure: Interval.closed_open(0, 50)
            }

    def inspect_extra_threshold(self):
        return None

    def inspect_type_chewu(self):
        """
        指数得分65分以下（三大客站总指数得分60分以下）；
        第2项～第4项任意2项满足；
        第2项～第4项任意1项指标连续3个月在门限值以下；
        检查均衡度指数60分以下（三大客站50分以下），问题暴露指数55分以下（三大客站50分以下）。
        :return:
        """
        flag = False
        if self.basic_data.score_dict.get(MainIndex.total_index) in self.extra_threshold_dict[MainIndex.total_index]:
            flag = True
        if len([x for x in self.basic_data.triggered_index_list
                if x not in self.triggered_index_exclude_list]) >= 2:
            flag = True
        if len([x for x in self.basic_data.continuous_triggered_index_list
                if x not in self.continuous_triggered_index_exclude_list]) >= 1:
            flag = True
        if self.basic_data.score_dict.get(MainIndex.check_evenness)\
                in self.extra_threshold_dict[MainIndex.check_evenness]\
                and self.basic_data.score_dict.get(MainIndex.problem_exposure)\
                in self.extra_threshold_dict[MainIndex.problem_exposure]:
            flag = True
        return flag

    def review(self):
        """各项指标均达到门限值解除预警"""
        if self.basic_data.row_data['DEPARTMENT_NAME'] in _special_departments:
            self.threshold_dict = _special_departments_threshold_dict
        if len(self.basic_data.triggered_index_list) == 0:
            return True
        return False


level_check_class = [LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-3)
