#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/8
Description: 重点指数预警 客运-劳安
"""
from app.data.early_warning.scheme import scheme
from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval

RISK_TYPE = 1  # 客运-1
RISK_CONFIG_ID = [18]
MAJOR = MajorClass.keyun
INDEX_NAME = '{}劳动安全指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 70),
    MainIndex.check_intensity: Interval.closed_open(0, 80),
    MainIndex.check_evenness: Interval.closed_open(0, 55),
    MainIndex.problem_exposure: Interval.closed_open(0, 70)
}

_desc_dict = {
    'inspect': """
总指数得分：得分70分以下
检查力度指数：得分80分以下
检查均衡度指数：得分55分以下
问题暴露指数：得分70分以下
II级：总指数得分60分以下；以上4项全满足；
III级：总指数得分70分以下；第2项～第4项任意2项满足；第2项～第4项任意1项指标连续3个月在门限值以下；
检查均衡度指数50分以下，问题暴露指数65分以下。
""",
    'review': """
II级：4项指标全部达到门限值直接解除预警，总指数达到70分以上，且其余各项指标在III级预警控制范围内。
III级：各项指标均达到门限值解除预警。
延长预警：未达到解除预警条件的自动延续或升级预警。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 6

    threshold_dict = _general_threshold_dict

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60)
    }


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60)
    }

    def review(self):
        """1. 4项指标全部达到门限值直接解除预警，
           2. 总指数达到70分以上
           3. 且其余各项指标在III级预警控制范围内。"""
        if set(self.basic_data.row_data['INSPECT_DATA']['triggered_index']).intersection(
                [x.value for x in self.basic_data.triggered_index_list]):
            return False
        if self.basic_data.score_dict.get(MainIndex.total_index) < 70:
            return False
        if self.basic_data.triggered_index_list:
            return False
        return True


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 2
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 1
    # 不加入个数比较指数列表
    triggered_index_exclude_list = [MainIndex.total_index]
    continuous_triggered_index_exclude_list = [MainIndex.total_index]

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 70),
        MainIndex.check_evenness: Interval.closed_open(0, 50),
        MainIndex.problem_exposure: Interval.closed_open(0, 65)
    }

    def review(self):
        """
        各项指标均达到门限值解除预警
        :return:
        """
        if self.basic_data.triggered_index_list:
            return False
        return True


level_check_class = [LevelTwo, LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
