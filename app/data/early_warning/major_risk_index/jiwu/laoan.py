#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/6
Description: 重点指数预警 机务-劳安
"""

from app.data.early_warning.enums import WarningLevel, MajorClass, MainIndex
from app.data.early_warning.base import WaringLevelInspection
from app.data.early_warning.major_risk_index.manager import get_manager
from app.data.early_warning.common import Interval
from app.data.early_warning.scheme import scheme

RISK_TYPE = 5  # 机务-5
RISK_CONFIG_ID = [19]
MAJOR = MajorClass.jiwu
INDEX_NAME = '{}劳动安全指数'.format(MAJOR.value)
HIERARCHY = 3

# 指数阈值字典
_general_threshold_dict = {
    MainIndex.total_index: Interval.closed_open(0, 60),
    MainIndex.check_intensity: Interval.closed_open(0, 75),
    MainIndex.assess_intensity: Interval.closed_open(0, 80),
    MainIndex.check_evenness: Interval.closed_open(0, 45),
    MainIndex.problem_exposure: Interval.closed_open(0, 35)
}

_review_threshold_dict = {
        MainIndex.total_index: Interval.closed_closed(60, 100),
        MainIndex.check_intensity: Interval.closed_closed(75, 100),
        MainIndex.assess_intensity: Interval.closed_closed(80, 100),
        MainIndex.check_evenness: Interval.closed_closed(45, 100),
        MainIndex.problem_exposure: Interval.closed_closed(35, 100)
}

_desc_dict = {
    'inspect': """
1.单项指数预警
III级警告预警条件（以下条件任意一项；分值处于区段时，含下限、不含上限）：
①单月劳安总指数低于60分；
②单月劳安检查力度指数低于75分；
③单月劳安考核力度指数低于80分；
④单月劳安风险暴露度指数低于35分。
⑤单月劳安检查均衡度指数低于45分。
2.组合指数预警
III级警告预警条件（分值含下限、不含上限）：
单月劳安总指数在60分至65分之间，单月劳安检查力度指数在75分至80分之间，单月劳安考核力度指数在80分至85分之间，
单月劳安风险暴露度指数在35分至40分之间，单月劳安检查均衡度指数在45分至50分之间。以上五个条件同时满足。
""",
    'review': """
III级警告预警（到期前30天内）：单月劳安总指数不低于60分，单月劳安检查力度指数不低于75分，单月劳安考核力度指数不低于80分，
单月劳安风险暴露度指数不低于35分，单月劳安检查均衡度指数不低于45分，以上五个条件同时满足。
"""
}


class LevelOne(WaringLevelInspection):
    warning_level = WarningLevel.level_1
    days = 100

    triggered_index_require = 6

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 55)
    }


class LevelTwo(WaringLevelInspection):
    warning_level = WarningLevel.level_2
    # 要求低于阈值的指数个数
    triggered_index_require = 4

    extra_threshold_dict = {
        MainIndex.total_index: Interval.closed_open(0, 60)
    }


class LevelThree(WaringLevelInspection):
    warning_level = WarningLevel.level_3
    # 要求低于阈值的指数个数
    triggered_index_require = 0
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 0

    def inspect_type_jiwu(self):
        """
        单项不及格
        或者以下五项同时处于区间：
        单月劳安总指数在60分至65分之间，单月劳安检查力度指数在75分至80分之间，
        单月劳安考核力度指数在80分至85分之间，单月劳安风险暴露度指数在35分至40分之间，
        单月劳安检查均衡度指数在45分至50分之间
        :return:
        """
        if len(self.basic_data.triggered_index_list) > 0:
            return True
        else:
            self.threshold_dict = {
                MainIndex.total_index: Interval.closed_open(60, 65),
                MainIndex.check_intensity: Interval.closed_open(75, 80),
                MainIndex.assess_intensity: Interval.closed_open(80, 85),
                MainIndex.check_evenness: Interval.closed_open(45, 50),
                MainIndex.problem_exposure: Interval.closed_open(35, 40)
            }
            self.calc_triggered_index()
            if len(self.basic_data.triggered_index_list) >= 5:
                return True
        return False

    def review(self):
        """
        单月劳安总指数不低于60分，单月劳安检查力度指数不低于75分，单月劳安考核力度指数不低于80分，
        单月劳安风险暴露度指数不低于35分，单月劳安检查均衡度指数不低于45分，以上五个条件同时满足。
        :return:
        """
        self.threshold_dict = _review_threshold_dict
        self.calc_triggered_index()
        if len(self.basic_data.triggered_index_list) >= 5:
            return True
        return False


level_check_class = [LevelThree]

manager = get_manager(INDEX_NAME, MAJOR, RISK_TYPE, RISK_CONFIG_ID,  HIERARCHY, level_check_class,
                      _general_threshold_dict, _desc_dict)

scheme.register_inspect_index(manager)


if __name__ == '__main__':
    from manage import app
    with app.app_context():
        manager.execute_inspection(-1)
