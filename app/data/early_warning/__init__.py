from . import scheme, health_index, major_risk_index

from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)
