#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/5
Description: 
"""
from app import mongo

from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.utils.safety_index_common_func import get_index_title
from app.data.early_warning.enums import IndexType, MongoCollection

index_name_dict = {
    0: '指数总得分',
    1: '检查力度指数',
    2: '评估力度指数',
    3: '考核力度指数',
    4: '检查均衡度',
    5: '问题暴露度',
    6: '问题整改'
}

# 暂时不用html标签
# inspect_report_template = """<p>指数名称：{}</p><p>检查数据月份：{}</p><p>预警级别：{}</p><p>指数得分：</br>{}</p>
# p>单月触发阈值的指数：{}</p><p>连续三个月触发阈值的指数：{}</p><p>预警条件描述：{}</p>"""

inspect_report_template = """指数名称：{}
检查数据月份：{}
预警级别：{} 
指数得分：{}
单月触发阈值的指数：{}
连续三个月触发阈值的指数：{}
预警条件描述：{}"""

review_report_template = """指数名称：{}
检查数据月份：{} 
指数得分：{}
解除条件描述：{}"""


inspect_desc_template = """指数名称：{}
检查数据月份：{}
预警级别：{} 
指数得分：{}"""


def get_index_name(major, index_type, risk_type):
    """
    获取指数名称
    :param major:
    :param index_type:
    :param risk_type:
    :return:
    """
    if index_type == IndexType.health_index.value:
        return '{}安全综合指数'.format(major), 'ZH'
    else:
        d = major_risk_map.get('{}-{}'.format(major, risk_type), {})
        return d.get('name', '重点指数'), d.get('index_code')


def get_sub_index_name(main_type):
    """
    获取子指数名称
    :param main_type:
    :return:
    """
    if main_type == 0:
        return '指数总得分'
    return get_index_title(main_type)


def get_index_score_coll(index_type):
    """
    根据指数类型获取保存指数得分的mongo表名
    :param index_type:
    :return:
    """
    if index_type == IndexType.health_index.value:
        return MongoCollection.health_index.value, MongoCollection.detail_health_index.value
    else:
        return MongoCollection.major_index.value, MongoCollection.detail_major_index.value


def get_warning_coll(index_type):
    """
    根据指数类型获取预警结果保存的mongo表名
    :param index_type:
    :return:
    """
    if index_type == IndexType.health_index.value:
        return MongoCollection.health_index_early_warning.value
    else:
        return MongoCollection.major_index_early_warning.value


def get_user_info(request):
    """
    从request的cookie中获取用户信息
    :param request:
    :return:
    """
    token = request.cookies.get('TOKEN')
    id_card = token.split('#')[0]
    person = mongo.db['base_person'].find_one({"ID_CARD": id_card}, {"_id": 0})
    return person


class Interval:
    OPEN = 0
    CLOSED = 1

    def __init__(self, left, right, left_boundary=OPEN, right_boundary=CLOSED):
        self.left = left
        self.right = right
        self.left_boundary = left_boundary
        self.right_boundary = right_boundary

    def __str__(self):
        if self.left_boundary == self.OPEN and self.right_boundary == self.OPEN:
            return '({}, {})'.format(self.left, self.right)
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.OPEN:
            return '[{}, {})'.format(self.left, self.right)
        elif self.left_boundary == self.OPEN and self.right_boundary == self.CLOSED:
            return '({}, {}]'.format(self.left, self.right)
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.CLOSED:
            return '[{}, {}]'.format(self.left, self.right)

    @classmethod
    def open_closed(cls, left, right):
        return cls(left, right, cls.OPEN, cls.CLOSED)

    @classmethod
    def closed_open(cls, left, right):
        return cls(left, right, cls.CLOSED, cls.OPEN)

    @classmethod
    def closed_closed(cls, left, right):
        return cls(left, right, cls.CLOSED, cls.CLOSED)

    @classmethod
    def open_open(cls, left, right):
        return cls(left, right, cls.OPEN, cls.OPEN)

    def __contains__(self, item):
        item = round(item)
        if self.left_boundary == self.OPEN and self.right_boundary == self.OPEN:
            return self.left < item < self.right
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.OPEN:
            return self.left <= item < self.right
        elif self.left_boundary == self.OPEN and self.right_boundary == self.CLOSED:
            return self.left < item <= self.right
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.CLOSED:
            return self.left <= item <= self.right

    def get_mongo_condition_dict(self):
        if self.left_boundary == self.OPEN and self.right_boundary == self.OPEN:
            return {'$gt': self.left, '$lt': self.right}
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.OPEN:
            return {'$gte': self.left, '$lt': self.right}
        elif self.left_boundary == self.OPEN and self.right_boundary == self.CLOSED:
            return {'$gt': self.left, '$lte': self.right}
        elif self.left_boundary == self.CLOSED and self.right_boundary == self.CLOSED:
            return {'$gte': self.left, '$lte': self.right}

