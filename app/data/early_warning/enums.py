#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/3
Description: 枚举类
"""

from enum import Enum, unique


@unique
class WarningLevel(Enum):
    """
    差异化预警等级
    level_0: 未预警
    level_1: 一级预警
    level_2: 一级预警
    level_3: 一级预警
    """
    level_0 = 0
    level_1 = 1
    level_2 = 2
    level_3 = 3


@unique
class MajorClass(Enum):
    """
    专业名称
    """
    chewu = '车务'
    keyun = '客运'
    jiwu = '机务'
    gongwu = '工务'
    gw_gongdian = '工电'
    dianwu = '电务'
    gongdian = '供电'
    cheliang = '车辆'


class IndexType(Enum):
    health_index = 0
    major_index = 1


@unique
class MongoCollection(Enum):
    """
    使用到的mongodbb表名
    """
    health_index = 'health_index'
    detail_health_index = 'detail_health_index'
    health_index_early_warning = 'health_index_early_warning'

    major_index = 'major_index'
    detail_major_index = 'detail_major_index'
    major_index_early_warning = 'major_index_early_warning'


@unique
class MainIndex(Enum):
    """
    指数大类：
    总得分          total_index
    检查力度指数	check_intensity
    评估力度指数	evaluate_intensity
    考核力度指数	assess_intensity
    检查均衡度	    check_evenness
    问题暴露度	    problem_exposure
    问题整改	    problem_rectification
    """
    total_index = 0
    check_intensity = 1
    evaluate_intensity = 2
    assess_intensity = 3
    check_evenness = 4
    problem_exposure = 5
    problem_rectification = 6


@unique
class RecordType(Enum):
    """
    检查结果类型
    normal: 非预警
    warning: 预警
    extension: 延期
    release: 解除
    """
    normal = 0
    warning = 1
    extension = 2
    release = 3


@unique
class ActionType(Enum):
    """
    执行类型
    inspect：预警检查
    release：解除检查
    """
    inspect = 1
    release = 2


@unique
class WarningStatus(Enum):
    """
    预警状态
    """
    released = 0
    executing = 1


@unique
class WarningConfigRecordStatus(Enum):
    disabled = 0
    enabled = 1


class ConfigOperationRecordType(Enum):
    """
    用户操作记录操作类型
    add_config：添加配置
    discard_config：废弃配置
    """
    add_config = 1
    discard_config = 2
