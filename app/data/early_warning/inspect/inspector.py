#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/5
Description: 
"""

from app import mongo
from app.big_screen_analysis.major_risk_index_map import major_risk_map
from app.data.early_warning.common import get_sub_index_name, inspect_desc_template
from app.data.early_warning.enums import MongoCollection, WarningLevel, WarningStatus, RecordType, IndexType
from app.data.util import get_history_months


class SingleConditionInspector:
    """
    单项预警
    """

    def __init__(self, threshold_dict, score_dict, months_list):
        self.threshold_dict = threshold_dict
        self.score_dict = score_dict
        self.months_list = months_list

    def calc_triggered_index(self, threshold_dict):
        """
        根据阈值字典计算触发的指数
        :param threshold_dict:
        :return:
        """
        triggered_index_list = []
        for index, threshold_score in threshold_dict.items():
            if not threshold_score or threshold_score == 0:
                continue
            index = int(index)
            score = self.score_dict.get(self.months_list[0], {}).get(index)
            if score is None:
                continue
            if score < threshold_score:
                triggered_index_list.append(index)
        return triggered_index_list

    @staticmethod
    def get_desc_content(threshold_dict, triggered_index):
        """
        获取指数预警条件描述与触发的指数
        :param threshold_dict:
        :param triggered_index:
        :return:
        """
        content = '单项预警条件:\n'
        content += '，'.join(['{}: {}'.format(get_sub_index_name(int(k)), v) for k, v in threshold_dict.items()])
        content += '\n'
        content += '触发阈值指数：{}'.format('，'.join([get_sub_index_name(x) for x in triggered_index]))
        return content

    def execute_inspect(self, manager):
        """
        执行检查
        :param manager:
        :return:
        """
        for level, threshold in self.threshold_dict.items():
            triggered_index_list = self.calc_triggered_index(threshold)
            if len(triggered_index_list) > 0:
                # 记录
                warning_level = int(level.split('_')[1])
                manager.triggered_condition_desc.append(self.get_desc_content(threshold, triggered_index_list))
                manager.warning_level = warning_level
                break


class CombineConditionInspector:
    """
    组合预警
    """

    def __init__(self, threshold_dict, score_dict, months_list):
        self.threshold_dict = threshold_dict
        self.score_dict = score_dict
        self.months_list = months_list

    def calc_triggered_index(self, threshold_dict, is_continuous=False):
        """
        根据阈值字典计算触发的指数
        :param is_continuous: 是否要求连续三个月触发
        :param threshold_dict:
        :return:
        """
        triggered_index_list = []
        for index, threshold_score in threshold_dict.items():
            if not threshold_score or threshold_score == 0:
                continue
            index = int(index)
            if not is_continuous:
                score = round(self.score_dict.get(self.months_list[0], {}).get(index))
                if score is None:
                    continue
                if score < threshold_score:
                    triggered_index_list.append(index)
            else:
                score_list = [round(self.score_dict.get(x, {}).get(index)) for x in self.months_list]
                triggered_count = len([x for x in score_list if x is not None and x < threshold_score])
                if triggered_count == len(self.months_list):
                    triggered_index_list.append(index)

        return triggered_index_list

    @staticmethod
    def get_desc_content(threshold_dict, triggered_index):
        """
        获取指数预警条件描述与触发的指数
        :param threshold_dict:
        :param triggered_index:
        :return:
        """
        content = '组合预警条件:\n'
        content += '连续三月:{}'.format('是' if threshold_dict.get('is_continuous') else '否')
        if threshold_dict['necessary_threshold']:
            content += '\n'
            content += '固定池:'
            content += '，'.join(
                ['{}: {}'.format(get_sub_index_name(int(k)), v) for k, v in
                 threshold_dict['necessary_threshold'].items()])
        if threshold_dict['random_threshold']:
            content += '\n'
            content += '随机池:'
            content += '，'.join(
                ['{}: {}'.format(get_sub_index_name(int(k)), v) for k, v in threshold_dict['random_threshold'].items()])
            content += '\n'
            content += '随机池要求触发数量：{}'.format(threshold_dict['random_trigger_count'])
        content += '\n'
        content += '触发阈值指数：{}'.format('，'.join([get_sub_index_name(x) for x in triggered_index]))
        return content

    def execute_inspect(self, manager):
        """
        执行检查
        :param manager:
        :return:
        """
        warning_flag = False
        for level, conditions in self.threshold_dict.items():
            for condition in conditions:
                condition_flag = False
                necessary_threshold = condition['necessary_threshold']
                random_threshold = condition['random_threshold']
                random_trigger_count = condition['random_trigger_count']
                is_continuous = condition.get('is_continuous')

                trigger_list_necessary = self.calc_triggered_index(necessary_threshold, is_continuous)
                trigger_list_random = self.calc_triggered_index(random_threshold, is_continuous)

                if necessary_threshold and random_threshold:
                    # 固定池+随机池
                    if len(trigger_list_necessary) == len(necessary_threshold)\
                            and len(trigger_list_random) >= random_trigger_count:
                        warning_flag = True
                        condition_flag = True
                elif necessary_threshold and not random_threshold:
                    # 只有固定池
                    if len(trigger_list_necessary) == len(necessary_threshold):
                        warning_flag = True
                        condition_flag = True
                else:
                    # 只有随机池
                    if len(trigger_list_random) >= random_trigger_count:
                        warning_flag = True
                        condition_flag = True
                if condition_flag:
                    manager.triggered_condition_desc.append(self.get_desc_content(
                        condition, trigger_list_necessary + trigger_list_random))
                if warning_flag:
                    warning_level = int(level.split('_')[1])
                    if manager.warning_level == 0 or warning_level < manager.warning_level:
                        manager.warning_level = warning_level
            if warning_flag:
                break


class InspectManager:

    def __init__(self, config, score_dict, department_info, months_ago, months_list, index_name, index_code):
        """
        :param config: 预警配置条件
        :param score_dict:  指数得分
        """
        self.config = config
        self.score_dict = score_dict
        self.department_info = department_info
        self.months_ago = months_ago
        self.month_list = months_list
        self.index_name = index_name
        self.index_code = index_code
        self.warning_level = WarningLevel.level_0.value
        self.condition_desc = ''
        self.triggered_condition_desc = []
        self.risk_id = None
        if self.config['INDEX_TYPE'] == IndexType.health_index.value:
            self.early_warning_coll_name = MongoCollection.health_index_early_warning.value
        else:
            self.early_warning_coll_name = MongoCollection.major_index_early_warning.value
            self.risk_id = self.get_risk_id()

    def get_risk_id(self):
        """
        获取重点指数风险配置的ID列表
        :return:
        """
        res = []
        risk_id = major_risk_map.get('{}-{}'.format(self.config['MAJOR'], self.config['RISK_TYPE'])).get('risk_name')
        if isinstance(risk_id, list):
            res.extend(risk_id)
        else:
            res.append(risk_id)
        return res

    def get_desc(self):
        """
        组成保存用的整体描述
        :return:
        """
        index_score = '，'.join(
            ['{}: {}'.format(get_sub_index_name(int(k)), v) for k, v in self.score_dict[self.month_list[0]].items()])
        content = inspect_desc_template.format(self.index_name, self.department_info['MON'], self.warning_level,
                                               index_score)
        return content + '\n' + '\n'.join(self.triggered_condition_desc)

    def get_parent_record(self):
        """
        获取某部门正在被预警的指数的初始记录
        :return:
        """
        condition = {
            'DEPARTMENT_ID': self.department_info['DEPARTMENT_ID'],
            'WARNING_STATUS': WarningStatus.executing.value,
            'MAJOR': self.department_info['MAJOR'],
            'PARENT_ID': None,
            'INSPECT_MON': {'$lt': self.department_info['MON']},
            'RECORD_TYPE': RecordType.warning.value
        }
        if self.config['INDEX_TYPE'] == IndexType.major_index.value:
            condition['TYPE'] = self.config['RISK_TYPE']
        return mongo.db[self.early_warning_coll_name].find_one(condition)

    def execute(self, preview=False):
        """
        执行检查并返回检查记录
        :param preview:
        :return:
        """
        single_condition_inspector = SingleConditionInspector(self.config['SINGLE_CONDITION'], self.score_dict,
                                                              self.month_list)
        single_condition_inspector.execute_inspect(self)

        combine_condition_inspector = CombineConditionInspector(self.config['COMBINE_CONDITION'], self.score_dict,
                                                                self.month_list)
        combine_condition_inspector.execute_inspect(self)

        if self.warning_level == WarningLevel.level_0.value:
            record_type = RecordType.normal.value
            warning_status = WarningStatus.released.value
        else:
            record_type = RecordType.warning.value
            warning_status = WarningStatus.executing.value

        # 判断是否延期、解除
        # parent_record = self.get_parent_record()
        parent_record = None
        #
        # if parent_record and not preview:
        #     if self.warning_level == WarningLevel.level_0.value:
        #         # 解除
        #         record_type = RecordType.release.warning.value
        #         mongo.db[self.early_warning_coll_name].update_one(
        #             {'_id': parent_record['_id']},
        #             {'$set': {'WARNING_STATUS': WarningStatus.released.value}})
        #         mongo.db[self.early_warning_coll_name].update_many(
        #             {'PARENT_ID': str(parent_record['_id'])},
        #             {'$set': {'WARNING_STATUS': WarningStatus.released.value}})
        #     else:
        #         # 延期
        #         record_type = RecordType.extension.warning.value

        inspect_record = {'INSPECT_MON': self.department_info['MON'],
                          'WARNING_MON': get_history_months(self.months_ago + 1)[0],
                          'MAJOR': self.department_info['MAJOR'],
                          'INDEX_NAME': self.index_name,
                          'INDEX_CODE': self.index_code,
                          'DEPARTMENT_ID': self.department_info['DEPARTMENT_ID'],
                          'DEPARTMENT_NAME': self.department_info['DEPARTMENT_NAME'],
                          'HIERARCHY': 3,
                          'ACTION_TYPE': '',
                          'CONDITION_ID': str(self.config.get('_id', '')),
                          'RECORD_TYPE': record_type,
                          'PARENT_ID': str(parent_record['_id']) if parent_record else '',
                          'WARNING_LEVEL': self.warning_level,
                          'CURRENT_WARNING_LEVEL': self.warning_level,
                          'INSPECT_DATA': '',
                          'DESC_DATA': self.get_desc(),
                          'WARNING_STATUS': warning_status,
                          'IS_UPGRADE': '',
                          'SCORE_DATA': {str(k): v for k, v in self.score_dict[self.month_list[0]].items()}}
        if self.config['INDEX_TYPE'] == IndexType.major_index.value:
            inspect_record['RISK_ID'] = self.risk_id
            inspect_record['TYPE'] = self.config['RISK_TYPE']

        return inspect_record
