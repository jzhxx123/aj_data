#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/16
Description: 
"""
import pandas as pd


from app import mongo
from app.big_screen_analysis.util import get_range_history_months
from app.data.early_warning.common import get_sub_index_name, get_index_name, get_index_score_coll, get_warning_coll
from app.data.early_warning.enums import MainIndex, WarningConfigRecordStatus, IndexType, WarningLevel
from app.data.util import get_history_months, get_coll_prefix, write_bulk_mongo
from app.data.early_warning.condition.model import IndexWarningConfig
from app.data.early_warning.inspect.inspector import InspectManager


def _calc_score(row, total_df, detail_df, month_list):
    """
    获取指数得分字典
    :param row:
    :param detail_df:
    :return:
    """
    result = {}
    for mon in month_list:
        total_score = total_df[(total_df['MON'] == mon) & (total_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])]
        if total_score.index.size > 0:
            total_score = total_score.iloc[0]['SCORE']
        else:
            total_score = None
        score_dict = {MainIndex.total_index.value: total_score}
        df = detail_df[(detail_df['MON'] == mon) & (detail_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])]
        for _, _row in df.iterrows():
            score_dict[_row['MAIN_TYPE']] = _row['SCORE']
        result[mon] = score_dict
    return result


def execute_inspection(months_ago=-1):
    """
    基于所有激活的配置执行检查任务并保存结果到数据库
    :param months_ago:
    :return:
    """
    documents = mongo.db['index_warning_config'].find({'STATUS': WarningConfigRecordStatus.enabled.value})

    result = {}
    year_mon = get_history_months(months_ago)[0]
    months_list = get_range_history_months(year_mon, 3)
    _prefix = get_coll_prefix(months_ago)

    for config_data in documents:
        major = config_data['MAJOR']
        index_type = config_data['INDEX_TYPE']
        risk_type = config_data['RISK_TYPE']
        department_id = config_data['DEPARTMENT_ID']
        index_name, index_code = get_index_name(major, index_type, risk_type)

        index_coll_name, detail_index_coll_name = get_index_score_coll(index_type)
        index_coll_name = '{}{}'.format(_prefix, index_coll_name)
        detail_index_coll_name = '{}{}'.format(_prefix, detail_index_coll_name)
        # 指数总得分数据
        condition = {'MON': {'$in': months_list}, 'MAJOR': major, 'DEPARTMENT_ID': department_id}
        if index_type == IndexType.major_index.value:
            condition['TYPE'] = risk_type

        index_score_data = mongo.db[index_coll_name].find(condition, {'_id': 0})
        index_score_df = pd.DataFrame(list(index_score_data))
        # 子指数得分数据
        condition['DETAIL_TYPE'] = 0
        detail_index_score_data = mongo.db[detail_index_coll_name].find(condition, {'_id': 0})
        detail_index_score_df = pd.DataFrame(list(detail_index_score_data))

        if index_score_df.empty or detail_index_score_df.empty:
            continue

        for _, row in index_score_df[index_score_df['MON'] == year_mon].iterrows():
            score_dict = _calc_score(row, index_score_df, detail_index_score_df, months_list)
            manager = InspectManager(config_data, score_dict, row, months_ago, months_list, index_name, index_code)
            record = manager.execute()
            result.setdefault(index_type, []).append(record)

    # 根据不同的指数类型写入数据
    for _index_type, records in result.items():
        coll_name = get_warning_coll(_index_type)
        rm_condition = {'INSPECT_MON': year_mon}
        mongo.db[coll_name].remove(rm_condition)
        write_bulk_mongo(coll_name, records)


def result_preview(config_data):
    """
    根据预警配置数据计算检查结果返回
    :param config_data:
    :return:
    """
    major = config_data['major']
    index_type = int(config_data['index_type'])
    risk_type = int(config_data['risk_type'])
    coverage_departments = config_data['coverage_departments']
    months_ago = -1
    year_mon = config_data.get('month') or get_history_months(months_ago)[0]
    year_mon = int(year_mon)
    months_list = get_range_history_months(year_mon, 3)
    _prefix = get_coll_prefix(months_ago)
    index_name, index_code = get_index_name(major, index_type, risk_type)
    result = []

    index_coll_name, detail_index_coll_name = get_index_score_coll(index_type)
    index_coll_name = '{}{}'.format(_prefix, index_coll_name)
    detail_index_coll_name = '{}{}'.format(_prefix, detail_index_coll_name)
    condition = {'MON': {'$in': months_list}, 'MAJOR': major, 'DEPARTMENT_ID': {'$in': coverage_departments}}
    if index_type == IndexType.major_index.value:
        condition['TYPE'] = risk_type

    index_score_data = mongo.db[index_coll_name].find(condition, {'_id': 0})
    index_score_df = pd.DataFrame(list(index_score_data))

    condition['DETAIL_TYPE'] = 0
    detail_index_score_data = mongo.db[detail_index_coll_name].find(condition,  {'_id': 0})
    detail_index_score_df = pd.DataFrame(list(detail_index_score_data))

    for col in IndexWarningConfig.columns:
        if col.lower() not in config_data:
            pass
        config_data[col] = config_data.pop(col.lower())

    for _, row in index_score_df[index_score_df['MON'] == year_mon].iterrows():
        score_dict = _calc_score(row, index_score_df, detail_index_score_df, months_list)
        manager = InspectManager(config_data, score_dict, row, months_ago, months_list, index_name, index_code)
        record = manager.execute(preview=True)
        result.append(record)

    result = get_serializer_result(pd.DataFrame(result))
    return result


def get_serializer_result(records_df):
    """
    格式化检查结果数据
    :param records_df:
    :return:
    """
    result = {'departments': [], 'result': {}}
    if not records_df.empty:
        for level, df in records_df.groupby(['WARNING_LEVEL']):
            if level > 0:
                result['result'][f'level_{level}'] = []
            for _, row in df.iterrows():
                result['departments'].append({'label': row['DEPARTMENT_NAME'],
                                              'warning': True if row['WARNING_LEVEL'] > 0 else False})
                if level > 0:
                    result['result'][f'level_{level}'].append({'label': row['DEPARTMENT_NAME'],
                                                               'desc': row['DESC_DATA']})

    for level in [WarningLevel.level_1, WarningLevel.level_2, WarningLevel.level_3]:
        if f'level_{level.value}' not in result['result'].keys():
            result['result'][f'level_{level.value}'] = []
    return result


def get_inspect_records(param_dict):
    """
    获取检查记录
    :param param_dict:
    :return:
    """
    major = param_dict['MAJOR']
    index_type = int(param_dict['INDEX_TYPE'])
    risk_type = int(param_dict['RISK_TYPE'])
    month = int(param_dict['MONTH'])

    coll_name = get_warning_coll(index_type)

    condition = {'MAJOR': major, 'INSPECT_MON': month}
    if index_type == IndexType.major_index.value:
        condition['TYPE'] = risk_type

    blank = {'departments': [], 'result': {'level_1': [], 'level_2': [], 'level_3': []}}

    documents = list(mongo.db[coll_name].find(condition, {'_id': 0}))
    if len(documents) == 0:
        return blank

    records_df = pd.DataFrame(documents)
    records_df.sort_values(['WARNING_LEVEL'], ascending=True, inplace=True)

    result = get_serializer_result(records_df)

    return result


def get_config_record(param_dict):
    """
    获取某条预警配置详情（用于前端配置修改页面）
    :param param_dict:
    :return:
    """
    record = IndexWarningConfig.find_one(param_dict['MAJOR'], param_dict['INDEX_TYPE'], param_dict['RISK_TYPE'],
                                         param_dict['DEPARTMENT_ID'])
    record.pop('STATUS', None)
    for col in IndexWarningConfig.columns + ['DEPARTMENT_ID', 'DEPARTMENT_NAME']:
        if col.lower() not in record.keys():
            record[col.lower()] = record.pop(col)

    return record
