#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/3
Description:
"""
from functools import wraps

from flask import current_app

from app import mongo
from app.data.early_warning.enums import MainIndex
from app.data.util import get_coll_prefix, get_history_months
from app.utils.common_func import get_today


def calc_continuous_months_data(continuous_months_num_require, months_ago, index, condition, interval,
                                index_coll, detail_index_coll):
    """
    判断连续n个月低于阈值的条件是否成立
    :param detail_index_coll:
    :param index_coll:
    :param condition: mongo查询条件字典
    :param continuous_months_num_require: 连续n个月
    :param months_ago: n月之前，当前预警正在检查的
    :param index: 指数，枚举类型
    :return:
    """
    mon_de_list = [months_ago - x for x in range(continuous_months_num_require - 1, 0, -1)]
    prefix_list = [get_coll_prefix(x) for x in mon_de_list]
    temp_count = 0
    condition.pop('SCORE')
    if len(set(prefix_list)) == 1:  # 如果prefix都相同
        past_months = list(map(lambda x: get_history_months(x)[0], mon_de_list))
        index_coll_name = '{}{}'.format(prefix_list[0], index_coll)
        detail_index_coll_name = '{}{}'.format(prefix_list[0], detail_index_coll)
        condition['MON'] = {'$in': past_months}
        if index == MainIndex.total_index:
            past_data = mongo.db[index_coll_name].find(condition, {'_id': 0, 'SCORE': 1})
        else:
            condition['DETAIL_TYPE'] = 0
            condition['MAIN_TYPE'] = index.value
            past_data = mongo.db[detail_index_coll_name].find(condition, {'_id': 0, 'SCORE': 1})
        for r in past_data:
            if round(r['SCORE']) in interval:
                temp_count += 1
        if temp_count >= continuous_months_num_require - 1:
            return True
        # if past_data and past_data.count() >= continuous_months_num_require - 1:
        #     return True
    else:  # 如果prefix不全相同，则一个月一个月查
        for mon in mon_de_list:
            prefix = get_coll_prefix(mon)
            calc_month = get_history_months(mon)[0]
            index_coll_name = '{}{}'.format(prefix, index_coll)
            detail_index_coll_name = '{}{}'.format(prefix, detail_index_coll)

            condition['MON'] = calc_month
            if index == MainIndex.total_index:
                record = mongo.db[index_coll_name].find(condition, {'_id': 0, 'SCORE': 1})
            else:
                condition['DETAIL_TYPE'] = 0
                condition['MAIN_TYPE'] = index.value
                record = mongo.db[detail_index_coll_name].find(condition, {'_id': 0, 'SCORE': 1})
            if record and record['SCORE'] in interval:
                temp_count += 1
        if temp_count >= continuous_months_num_require - 1:
            return True


def validate_warning_exec_month(func):
    """一个检查指数计算月份是否有效的装饰器

    Arguments:
        func  -- 指数计算入口函数

    Raises:
        ValueError -- [传入的月份错误]

    Returns:
        func --返回被调函数
    """

    @wraps(func)
    def wrapper(self, months_ago, *args, **kwargs):
        if months_ago < 0:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(
                f'months_agos-[{months_ago}] should not less than 0')

        months_ago *= -1
        if get_history_months(months_ago)[0] < 201710:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(f'months_agos-[{months_ago}] param error')

        # 判断是否是25-月底
        if months_ago == 0 and get_today().day < current_app.config.get(
                'UPDATE_DAY'):
            return 'NO NEED TO UPDATE'

        func(months_ago, *args, **kwargs)

        return 'OK'

    return wrapper


def calc_months_difference(start_date, end_date):
    """
    计算相差月份数
    :param start_date: 201902
    :param end_date:  201905
    :return: 3
    """
    start_date = str(start_date)
    end_date = str(end_date)
    start_year = int(start_date[:4])
    end_year = int(end_date[:4])
    start_mon = int(start_date[4:])
    end_mon = int(end_date[4:])

    return (end_year - start_year) * 12 + (end_mon - start_mon)


if __name__ == '__main__':
    print(calc_months_difference(201809, 201905))
