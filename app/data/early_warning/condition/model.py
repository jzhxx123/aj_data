#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/12/3
Description: 
"""
from datetime import datetime

from app import mongo
from app.data.early_warning.common import get_sub_index_name, get_user_info, get_index_name
from app.data.early_warning.enums import IndexType, WarningConfigRecordStatus, ConfigOperationRecordType
from app.data.util import write_bulk_mongo

from app.utils.common_func import get_department_name_by_dpid
from app.utils.safety_index_common_func import get_index_title
from app.data.early_warning.lock import file_lock


class IndexWarningConfig:
    coll_name = 'index_warning_config'
    columns = ['MAJOR', 'INDEX_TYPE', 'RISK_TYPE', 'SINGLE_CONDITION', 'COMBINE_CONDITION']

    @classmethod
    def insert(cls, data, request):
        """
        插入配置数据
        :param request:
        :param data:
        :return:
        """
        try:
            file_lock.acquire()
            user = get_user_info(request)
            records = []
            condition = {'MAJOR': data['major'],
                         'STATUS': WarningConfigRecordStatus.enabled.value,
                         'INDEX_TYPE': data['index_type']}
            index_name, index_code = get_index_name(data['major'], data['index_type'], data['risk_type'])

            if data['index_type'] == IndexType.major_index.value:
                condition['RISK_TYPE'] = data['risk_type']

            for col in cls.columns:
                if col.lower() in data:
                    data[col] = data.pop(col.lower())

            affected_departments = []
            for department in data.pop('coverage_departments'):
                condition['DEPARTMENT_ID'] = department
                department_name = get_department_name_by_dpid(department)
                # 把过去该部门该指数的配置设为失效
                mongo_result = mongo.db[cls.coll_name].update_many(
                    condition, {'$set': {'STATUS': WarningConfigRecordStatus.disabled.value}})
                records.append(dict(DEPARTMENT_ID=department,
                                    DEPARTMENT_NAME=department_name,
                                    STATUS=WarningConfigRecordStatus.enabled.value,
                                    INDEX_CODE=index_code,
                                    **data))
                if mongo_result.modified_count > 0:
                    affected_departments.append(department_name)

            if affected_departments:
                # 写入操作记录-旧配置被废止
                IndexWarningConfigOperationRecord.insert(ConfigOperationRecordType.discard_config.value,
                                                         user, index_name, affected_departments)

            write_bulk_mongo(cls.coll_name, records)

            # 写入操作记录-新配置添加
            IndexWarningConfigOperationRecord.insert(ConfigOperationRecordType.add_config.value,
                                                     user, index_name, [x['DEPARTMENT_NAME'] for x in records])
            return 'ok'
        except Exception as e:
            return ''
        finally:
            file_lock.release()

    @classmethod
    def get_detail(cls, major, index_type, risk_type, department_id):
        record = cls.find_one(major, index_type, risk_type, department_id)
        result = {'single_condition': {}, 'combine_condition': {}}
        for level, threshold in record['SINGLE_CONDITION'].items():
            level_dict = []
            for main_type, value in threshold.items():
                level_dict.append({'key': main_type, 'value': value, 'label': get_sub_index_name(int(main_type))})
            result['single_condition'][level] = level_dict

        for level, conditions in record['COMBINE_CONDITION'].items():
            condition_list = []
            for condition in conditions:
                condition_dict = {'necessary_threshold': [], 'random_threshold': [], }
                for main_type, value in condition['necessary_threshold'].items():
                    condition_dict['necessary_threshold'].append(
                        {'key': main_type, 'value': value, 'label': get_sub_index_name(int(main_type))})
                for main_type, value in condition['random_threshold'].items():
                    condition_dict['random_threshold'].append(
                        {'key': main_type, 'value': value, 'label': get_sub_index_name(int(main_type))})
                condition_dict['random_trigger_count'] = condition['random_trigger_count']
                condition_dict['is_continuous'] = condition.get('is_continuous')
                condition_list.append(condition_dict)
            result['combine_condition'][level] = condition_list
        return result

    @classmethod
    def find_one(cls, major, index_type, risk_type, department_id):
        """
        查找某部门某指数当前生效的配置
        :param major:
        :param index_type:
        :param risk_type:
        :param department_id:
        :return:
        """
        index_type = int(index_type)
        risk_type = int(risk_type)
        condition = {'MAJOR': major, 'INDEX_TYPE': index_type, 'DEPARTMENT_ID': department_id,
                     'STATUS': WarningConfigRecordStatus.enabled.value}
        if index_type == IndexType.major_index.value:
            condition['RISK_TYPE'] = risk_type

        record = mongo.db[cls.coll_name].find_one(condition, {'_id': 0})
        if record:
            return record
        else:
            raise ValueError('暂无相应记录')


class IndexWarningConfigOperationRecord:
    coll_name = 'index_warning_config_operation_record'
    columns = ['ID_CARD', 'USER_NAME', 'OPERATION_TYPE', 'CREATED_AT', 'DESC']

    @staticmethod
    def get_desc(operation_type, index_name, departments):
        if operation_type == ConfigOperationRecordType.add_config.value:
            desc = '添加预警配置，指数名称：{}，涉及部门：{}'.format(index_name, '，'.join(departments))
        elif operation_type == ConfigOperationRecordType.discard_config.value:
            desc = '废止预警配置，指数名称：{}，涉及部门：{}'.format(index_name, '，'.join(departments))
        else:
            desc = ''
        return desc

    @classmethod
    def insert(cls, operation_type, user, index_name, departments):
        desc = IndexWarningConfigOperationRecord.get_desc(operation_type, index_name, departments)
        record = {'PERSON_ID': user.get('PERSON_ID'),
                  'PERSON_NAME': user.get('PERSON_NAME'),
                  'OPERATION_TYPE': operation_type,
                  'DESC': desc,
                  'CREATED_AT': datetime.today()}
        mongo.db[cls.coll_name].insert(record)

