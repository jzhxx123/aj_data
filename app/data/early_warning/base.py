#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/7/3
Description: 
"""
import pandas as pd
import pymongo
from flask import current_app

from app import mongo
from app.data.early_warning.common import index_name_dict, inspect_report_template, review_report_template
from app.data.early_warning.enums import MainIndex, RecordType, ActionType, WarningLevel, WarningStatus
from app.data.early_warning.util import calc_continuous_months_data, calc_months_difference
from app.data.util import (get_coll_prefix, get_history_months, write_bulk_mongo)


class WaringLevelInspection:
    """
    风险等级预警检查基本类, 被具体检查等级继承
    预警检查：
    1. 校验方式：收集所有以inspect_开头的方法，方法内根据具体逻辑判断条件是否满足，是则返回True, 否则返回False，
       返回None表示该方法不参与比较结果，所有方法被执行之后返回一个列表，如果包含True，表示满足本等级
    2. 有几个基本的检查方法（括号内为通过设置属性关闭该方法）:
        a. 是否满足要求低于阈值的个数    (triggered_index_require=0）)
        b. 是否满足要求连续n个月低于阈值的个数   (continuous_triggered_index_require=0)
        c. 检查指定低于阈值的指数是否存在   (specific_triggered_index=None)
        d. 检查指定连续低于阈值的指数是否存在    (specific_continuous_triggered_index=None)
        e. 检查指定某些指数是否低于某分数     (extra_threshold_dict=None)
    3. 关闭默认检查方法：例如关闭检查“要求低于阈值的指数个数”，把triggered_index_require设为0（默认为0）即会返回None，
       所有检查方法默认都为关闭
    4. 新增自定义检查方法：利用已经计算出的比较数据，自定义检查方法，在继承的等级检查类中定义以 inspect_ 开头的方法，
       即会被收集，返回同样是True, False, None
       如果有比较特殊判断条件则可以直接关闭所有默认检查方法然后自定义检查方法

    预警解除检查：
    1. 校验方式： 每个子类实现review方法，根据方法的返回判断是否能解除或者需要延长升级预警
    """
    # 预警等级
    warning_level = None
    # 警告天数
    days = 30
    # 延期第几次开始升级
    extend_to_upgrade_require = 2

    # 要求低于阈值的指数个数
    triggered_index_require = 0
    # 要求连续低于阈值的指数的个数
    continuous_triggered_index_require = 0
    # 要求连续n个月低于阈值
    continuous_months_num_require = 3

    # 额外的阈值要求
    extra_threshold_dict = None
    # 必须包含的低于阈值的指数
    specific_triggered_index = None
    # 必须包含的连续低于阈值的指数
    specific_continuous_triggered_index = None

    # 本级别专用阈值字典
    threshold_dict = None
    # 本级别专用连续阈值字典
    continuous_threshold_dict = None
    # 比较低于阈值指数个数时除外的指数
    triggered_index_exclude_list = None
    # 比较连续低于阈值指数个数时除外的指数
    continuous_triggered_index_exclude_list = None

    def __init__(self, basic_data):
        self.basic_data = basic_data
        self.continuous_threshold_dict = {}
        self.triggered_index_exclude_list = []
        self.continuous_triggered_index_exclude_list = []

    def calc_triggered_index(self):
        """如果自己有指定的阈值字典，重新计算不合格指数列表"""
        threshold_dict = self.threshold_dict
        if threshold_dict:
            triggered_index_list = []
            for index, interval in threshold_dict.items():
                score = self.basic_data.score_dict.get(index, 0)
                if score in interval:
                    triggered_index_list.append(index)
            self.basic_data.triggered_index_list = triggered_index_list

    def calc_continuous_triggered_index(self):
        """当自己有指定阈值字典或这连续阈值字典时，重新计算连续不合格指数列表"""
        continuous_threshold_dict = self.continuous_threshold_dict or self.threshold_dict
        if continuous_threshold_dict:
            continuous_triggered_index_list = []
            for index, interval in continuous_threshold_dict.items():
                score = self.basic_data.score_dict.get(index, 0)
                if score in interval:
                    condition = {
                     'MAJOR': self.basic_data.major.value,
                     'SCORE': interval.get_mongo_condition_dict(),
                     'DEPARTMENT_ID': self.basic_data.row_data['DEPARTMENT_ID']}
                    if self.basic_data.major_risk_type:
                        condition['TYPE'] = self.basic_data.major_risk_type
                    if calc_continuous_months_data(self.continuous_months_num_require,
                                                   self.basic_data.months_ago, index,
                                                   condition, interval,
                                                   self.basic_data.index_coll_name,
                                                   self.basic_data.detail_index_coll_name):
                        continuous_triggered_index_list.append(index)

            self.basic_data.continuous_triggered_index_list = continuous_triggered_index_list

    def calc_mix_triggered_index_list(self):
        """生成存在于单月不合格或者连续不合格列表中的指数列表"""
        single_or_continuous_triggered_index_list = []
        for index in MainIndex:
            if index in self.basic_data.triggered_index_list or index in self.basic_data.continuous_triggered_index_list:
                single_or_continuous_triggered_index_list.append(index)
        self.basic_data.single_or_continuous_triggered_index_list = single_or_continuous_triggered_index_list

    def calc_data(self):
        """
        如果有自己特定的阈值，重新计算比较数据
        """
        self.calc_triggered_index()
        self.calc_continuous_triggered_index()
        self.calc_mix_triggered_index_list()

    def get_inspect_methods(self):
        """获得所有检查方法"""
        return list(filter(lambda x: x.startswith('inspect_') and callable(getattr(self, x)), dir(self)))

    def before_execute_inspection(self):
        """在计算比较数据与执行检查之前执行"""
        pass

    def after_execute_inspection(self):
        """在执行检查之后执行"""
        pass

    def execute_level_inspection(self):
        """执行检查，返回结果列表"""
        self.before_execute_inspection()
        self.calc_data()
        res = [getattr(self, func)() for func in self.get_inspect_methods()]
        self.after_execute_inspection()
        return res

    def inspect_triggered_index_require(self):
        """判断是否满足低于阈值的个数"""
        if self.triggered_index_require > 0:
            if len([x for x in self.basic_data.triggered_index_list
                    if x not in self.triggered_index_exclude_list]) >= self.triggered_index_require:
                return True
            else:
                return False
        return None

    def inspect_continuous_triggered_index_require(self):
        """判断满足连续n个月低于阈值的个数"""
        if self.continuous_triggered_index_require > 0:
            if len([x for x in self.basic_data.continuous_triggered_index_list
                    if x not in self.continuous_triggered_index_exclude_list]) >= self.continuous_triggered_index_require:
                return True
            else:
                return False
        return None

    def inspect_extra_threshold(self):
        """判断额外的指数阈值需求"""
        if self.extra_threshold_dict:
            flag = True
            for index, interval in self.extra_threshold_dict.items():
                if not self.basic_data.score_dict.get(index, 0) in interval:
                    flag = False
                    break
            return flag
        return None

    def inspect_specific_triggered(self):
        """判断指定的低于阈值的指数"""
        if self.specific_triggered_index:
            flag = True
            for index in self.specific_triggered_index:
                if index not in self.basic_data.triggered_index_list:
                    flag = False
                    break
            return flag
        return None

    def inspect_specific_continuous_triggered(self):
        """判断指定的连续低于阈值的指数"""
        if self.specific_continuous_triggered_index:
            flag = True
            for index in self.specific_continuous_triggered_index:
                if index not in self.basic_data.continuous_triggered_index_list:
                    flag = False
                    break
            return flag
        return None

    def get_inspect_result_dict(self):
        """
        获取本级别指数检查结果，返回触发指数与连续触发指数
        :return:
        """
        inspect_data = dict()
        inspect_data['triggered_index'] = [x.value for x in self.basic_data.triggered_index_list]
        inspect_data['continuous_triggered_index'] = [x.value for x in self.basic_data.continuous_triggered_index_list]
        return inspect_data

    def execute_level_review(self):
        """执行级别解除检查"""
        self.calc_data()
        return self.review()

    def review(self):
        """判断是否满足解除条件"""
        raise NotImplementedError

    def extend_to_upgrade(self, parent_record, level_check_class):
        """
        在有父记录的情况下，判断是否满足延期次数而导致升级预警
        三级延期到第二次升级到二级，两级延期到第二次升级到一级
        目前等级必须是二级或者三级才能升级，距离上一次升级必须隔一个月
        :return:
        """
        flag = False
        if not parent_record['CURRENT_WARNING_LEVEL'] in (WarningLevel.level_2.value, WarningLevel.level_3.value):
            return flag
        if level_check_class[0].warning_level == self.warning_level:
            # 如果当前等级已经是最大等级
            return flag
        past_month = get_history_months(self.basic_data.months_ago - (self.extend_to_upgrade_require - 1))[0]
        current_month = get_history_months(self.basic_data.months_ago)[0]
        if parent_record:
            if self.extend_to_upgrade_require <= 1:
                # 满足，表示每次延期都升级
                flag = True
            else:
                condition = {
                    'DEPARTMENT_ID': self.basic_data.row_data['DEPARTMENT_ID'],
                    'RECORD_TYPE': RecordType.extension.value,
                    'PARENT_ID': str(parent_record['_id']),
                    'INSPECT_MON': {'$gte': past_month, '$lt': current_month}
                    # '$or': [{'PARENT_ID': str(parent_record['_id'])}, {'_id': parent_record['_id']}]
                }
                count = 1
                past_records = mongo.db[self.basic_data.manager.early_warning_coll_name.value].find(
                    condition).sort('INSPECT_MON', pymongo.DESCENDING)
                for r in past_records:
                    if not r['IS_UPGRADE'] and r['RECORD_TYPE'] == RecordType.extension.value:
                        count += 1
                if count >= self.extend_to_upgrade_require:
                    flag = True
        return flag


class IndexWarningBasicData:
    """
    检查时每行(部门)的基础数据，供每一个检查级别使用s
    """

    # 要求连续n个月低于阈值
    continuous_months_num_require = 3

    def __init__(self, manager, months_ago):
        self.manager = manager
        # 阈值字典
        self.threshold_dict = manager.threshold_dict
        # 专业
        self.major = manager.major
        # 在重点指数开发中指定的type
        self.major_risk_type = manager.major_risk_type
        # n个月之前
        self.months_ago = months_ago
        # 存放指数得分
        self.score_dict = dict()
        # 存储低于阈值的指数的列表
        self.triggered_index_list = []
        # 存储连续失败的低于阈值的指数的列表
        self.continuous_triggered_index_list = []
        # 存在于单月或者连续的的指数列表，即满足低于单月阈值或者连续n月阈值的就会被放进去
        self.single_or_continuous_triggered_index_list = []
        # DataFrame的行数据
        self.row_data = None
        # 被检查指数在mongo中的表名
        self.index_coll_name = manager.index_coll_name
        # 被检查子指数在mongo中的表名
        self.detail_index_coll_name = manager.detail_index_coll_name
        self.risk_id = manager.risk_id

    def calc_score(self, row, detail_df):
        """
        填充得分数据
        :param row:
        :param detail_df:
        :return:
        """
        score_dict = {MainIndex.total_index: row['SCORE']}
        for index in MainIndex:
            if index == MainIndex.total_index:
                continue
            score = detail_df[
                (detail_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID'])
                & (detail_df['DETAIL_TYPE'] == 0)
                & (detail_df['MAIN_TYPE'] == index.value)]
            if not score.empty:
                score = score.iloc[0]['SCORE']
            else:
                score = 0
            if index not in score_dict:
                score_dict[index] = score
        self.score_dict = score_dict

    def calc_data(self, row):
        """
        根据得分与阈值得出对应的不及格的指数列表
        :param row:
        :return:
        """
        self.row_data = row
        for index, interval in self.threshold_dict.items():
            score = self.score_dict.get(index, 0)
            if score in interval:
                self.triggered_index_list.append(index)
                condition = {
                    'MAJOR': self.major.value,
                    'SCORE': interval.get_mongo_condition_dict(),
                    'DEPARTMENT_ID': self.row_data['DEPARTMENT_ID']}
                if self.major_risk_type:
                    condition['TYPE'] = self.major_risk_type
                if calc_continuous_months_data(self.continuous_months_num_require,
                                               self.months_ago, index, condition, interval,
                                               self.index_coll_name,
                                               self.detail_index_coll_name):
                    self.continuous_triggered_index_list.append(index)


class InspectManagerBase:
    # 指数名称
    index_name = ''
    # 专业
    major = None
    # 指数阈值字典
    threshold_dict = None
    # 检查级别类
    level_check_class = None
    # 级别
    hierarchy = 3
    # 在重点指数开发中指定的type
    major_risk_type = None
    # 重点指数风险配置id
    risk_id = None
    # 被检查指数在mongo中的表名
    index_coll_name = None
    # 被检查子指数在mongo中的表名
    detail_index_coll_name = None
    # 保存检查结果的表名
    early_warning_coll_name = None

    def __init__(self, index_name, major, hierarchy, level_check_class, threshold_dict, desc_dict=None):
        self.index_name = index_name
        self.major = major
        self.hierarchy = hierarchy
        self.threshold_dict = threshold_dict
        self.level_check_class = level_check_class
        self.desc_dict = desc_dict

    def __str__(self):
        return self.index_name

    def get_parent_record(self, department_id, mon):
        """
        获取某部门正在被预警的指数的初始记录
        :param department_id:
        :param mon:
        :return:
        """
        condition = {
            'DEPARTMENT_ID': department_id,
            'WARNING_STATUS': WarningStatus.executing.value,
            'MAJOR': self.major.value,
            'PARENT_ID': None,
            'INSPECT_MON': {'$lt': mon},
            'RECORD_TYPE': RecordType.warning.value
        }
        if self.major_risk_type:
            condition['TYPE'] = self.major_risk_type
        return mongo.db[self.early_warning_coll_name.value].find_one(condition)

    def get_last_record(self, parent_record):
        """
        获取某初始警告记录最近一条延期记录
        :param parent_record:
        :return:
        """
        condition = {
            'DEPARTMENT_ID': parent_record['DEPARTMENT_ID'],
            'WARNING_STATUS': WarningStatus.executing.value,
            'MAJOR': self.major.value,
            'PARENT_ID': str(parent_record['_id']),
            'RECORD_TYPE': RecordType.extension.value
        }
        if self.major_risk_type:
            condition['TYPE'] = self.major_risk_type
        return mongo.db[self.early_warning_coll_name.value].find_one(condition,
                                                                     sort=[('INSPECT_MON', pymongo.DESCENDING)])

    def get_desc_content(self, basic_data, inst=None):
        """
        返回存档用的检查结果文字描述
        :param basic_data: 正在检查的部门指数的基础数据
        :param inst: 检查级别示例
        :return:
        """
        # index_score = ''.join(['{}: {}</br>'.format(index_name_dict.get(k.value), v)
        #                        for k, v in inst.basic_data.score_dict.items()])
        index_score = '，'.join(['{}: {}'.format(index_name_dict.get(k.value), v)
                               for k, v in basic_data.score_dict.items()])
        if inst:
            triggered_index = ','.join([index_name_dict.get(x.value) for x in inst.basic_data.triggered_index_list])
            continuous_triggered_index = ','.join([
                index_name_dict.get(x.value) for x in inst.basic_data.continuous_triggered_index_list])
            warning_level = inst.warning_level.value
        else:
            triggered_index = "无"
            continuous_triggered_index = "无"
            warning_level = WarningLevel.level_0.value
        condition = self.desc_dict.get('inspect')
        content = inspect_report_template.format(
            basic_data.manager.index_name, get_history_months(basic_data.months_ago)[0],
            warning_level, index_score, triggered_index, continuous_triggered_index, condition)
        # return content.replace('\n', '</br>')
        content = content.replace("'", '')
        return content

    def get_review_desc(self, basic_data):
        """
        返回存档用的解除结果文字描述
        :param basic_data: 正在检查的部门指数的基础数据
        :return:
        """
        index_score = '，'.join(['{}: {}'.format(index_name_dict.get(k.value), v)
                               for k, v in basic_data.score_dict.items()])
        condition = self.desc_dict.get('review')
        content = review_report_template.format(basic_data.manager.index_name,
                                                get_history_months(basic_data.months_ago)[0], index_score, condition)
        content = content.replace("'", '')
        return content

    def update_current_level(self, parent_record, current_level):
        mongo.db[self.early_warning_coll_name.value].update_one(
            {'_id': parent_record['_id']},
            {'$set': {'CURRENT_WARNING_LEVEL': current_level}})
        mongo.db[self.early_warning_coll_name.value].update_many(
            {'PARENT_ID': str(parent_record['_id'])},
            {'$set': {'CURRENT_WARNING_LEVEL': current_level}})

    def __inspect_row(self, row, detail_df, months_ago):
        """
        DataFrame apply函数，检查某月份某部门指数
        :param row: DataFrame行数据
        :param detail_df: 子指数得分DataFrame
        :param months_ago: n月之前
        :return:
        """
        basic_data = IndexWarningBasicData(self, months_ago)
        # 保存子指数得分
        basic_data.calc_score(row, detail_df)
        # 计算基本比较数据，使用的在具体文件中定义的通用阈值字典，如果预警级别类中重新定义了，在执行检查之前会再计算
        basic_data.calc_data(row)
        is_upgrade = False
        score_dict = {str(k.value): v for k, v in basic_data.score_dict.items()}
        return_data = [RecordType.normal.value, WarningLevel.level_0.value, WarningLevel.level_0.value,
                       0, self.risk_id, None, '', self.get_desc_content(basic_data), WarningStatus.released.value,
                       is_upgrade, score_dict]
        for cls in self.level_check_class:
            inst = cls(basic_data)
            result = inst.execute_level_inspection()
            inspect_data = inst.get_inspect_result_dict()
            desc_data = self.get_desc_content(basic_data, inst)
            if True in result:
                current_app.logger.debug('{} get level {}'.format(row['DEPARTMENT_NAME'], inst.warning_level.value))
                parent_record = self.get_parent_record(row['DEPARTMENT_ID'], row['MON'])
                parent_id = None
                record_type = RecordType.warning.value
                warning_level = inst.warning_level.value
                warning_days = inst.days
                current_level = inst.warning_level.value
                if parent_record:  # 如果不是初始预警
                    parent_id = str(parent_record['_id'])
                    record_type = RecordType.extension.value
                    if parent_record['CURRENT_WARNING_LEVEL'] == WarningLevel.level_1.value:
                        # 如果是一级预警，在前两个月的检查或者解除中始终返回未预警
                        last_record = self.get_last_record(parent_record) or parent_record
                        if calc_months_difference(last_record['INSPECT_MON'], get_history_months(months_ago)[0]) < 3:
                            record_type = RecordType.normal.value
                            # warning_level = 0
                            # warning_days = 0
                    if record_type != RecordType.normal.value:
                        if inst.warning_level.value < parent_record['CURRENT_WARNING_LEVEL']:
                            # 代表延期并且升级，更新父记录预警级别
                            is_upgrade = True
                            self.update_current_level(parent_record, inst.warning_level.value)
                        elif inst.warning_level.value >= parent_record['CURRENT_WARNING_LEVEL']:
                            # 代表延期
                            current_level = parent_record['CURRENT_WARNING_LEVEL']
                            # 再检查一下延期升级
                            if inst.extend_to_upgrade(parent_record, self.level_check_class):
                                # 一级一级升
                                current_level = current_level - 1
                                is_upgrade = True
                                # 更新父记录预警级别, 不改变自身WARNING_LEVEL
                                self.update_current_level(parent_record, current_level)
                return_data = [record_type, warning_level, current_level, warning_days, inst.basic_data.risk_id,
                               parent_id, inspect_data, desc_data, WarningStatus.executing.value, is_upgrade,
                               score_dict]
                break
        return pd.Series(return_data,
                         index=['RECORD_TYPE', 'WARNING_LEVEL', 'CURRENT_WARNING_LEVEL', 'WARNING_DAYS', 'RISK_ID',
                                'PARENT_ID', 'INSPECT_DATA', 'DESC_DATA', 'WARNING_STATUS', 'IS_UPGRADE', 'SCORE_DATA'])

    def execute_inspection(self, months_ago):
        """
        从数据库中查出指数分数数据，执行检查并写入数据库
        :return:
        """
        year_mon = get_history_months(months_ago)[0]
        _prefix = get_coll_prefix(months_ago)
        index_coll_name = '{}{}'.format(_prefix, self.index_coll_name)
        detail_index_coll_name = '{}{}'.format(_prefix, self.detail_index_coll_name)
        # 总指数得分数据
        condition = {'MON': year_mon, 'MAJOR': self.major.value}
        # condition['DEPARTMENT_NAME'] = '遵义车务段'
        if self.major_risk_type:
            condition['TYPE'] = self.major_risk_type
        index_score_data = mongo.db[index_coll_name].find(condition, {'_id': 0})
        index_score_df = pd.DataFrame(list(index_score_data))
        # 小指数得分数据
        detail_index_score_data = mongo.db[detail_index_coll_name].find(condition, {'_id': 0})
        detail_index_score_df = pd.DataFrame(list(detail_index_score_data))

        if index_score_df.empty or detail_index_score_df.empty:
            return
        # 对每一行进行检查
        res_df = index_score_df.apply(
            lambda row: self.__inspect_row(row, detail_index_score_df, months_ago), axis=1, result_type='expand')
        res_df = index_score_df.join(res_df)

        columns = ['MON', 'MAJOR', 'HIERARCHY', 'DEPARTMENT_ID', 'DEPARTMENT_NAME', 'RECORD_TYPE',
                   'PARENT_ID', 'WARNING_LEVEL', 'CURRENT_WARNING_LEVEL', 'WARNING_DAYS', 'INSPECT_DATA', 'DESC_DATA',
                   'WARNING_STATUS', 'IS_UPGRADE', 'SCORE_DATA']
        if self.major_risk_type:
            columns.append('TYPE')
            columns.append('RISK_ID')

        res_df = pd.DataFrame(res_df, columns=columns)
        # 检查上个月的数据，被预警的月份为本月
        warning_mon = get_history_months(months_ago+1)[0]
        res_df.rename(columns={'MON': 'INSPECT_MON'}, inplace=True)
        res_df['WARNING_MON'] = warning_mon
        res_df['INDEX_NAME'] = self.index_name
        res_df['ACTION_TYPE'] = ActionType.inspect.value
        # 存档
        coll_name = self.early_warning_coll_name.value
        r_condition = {
            'INSPECT_MON': year_mon,
            'MAJOR': self.major.value,
            'ACTION_TYPE': ActionType.inspect.value
        }
        if self.major_risk_type:
            r_condition['TYPE'] = self.major_risk_type
        mongo.db[coll_name].remove(r_condition)
        write_bulk_mongo(coll_name, res_df.to_dict(orient='records'))

    def __review_row(self, row, index_score_df, detail_index_score_df, months_ago):
        """
        对每一条初始警告数据进行解除检查
        :param row: 初始警告记录
        :param index_score_df: 指数总得分DateFrame
        :param detail_index_score_df: 子指数总得分DateFrame
        :param months_ago: n月之前
        :return:
        """
        year_mon = get_history_months(months_ago)[0]
        row['SCORE'] = index_score_df[index_score_df['DEPARTMENT_ID'] == row['DEPARTMENT_ID']].iloc[0]['SCORE']
        level_inst = None
        basic_data = IndexWarningBasicData(self, months_ago)
        # 保存子指数得分
        basic_data.calc_score(row, detail_index_score_df)
        basic_data.calc_data(row)
        for level in self.level_check_class:
            if level.warning_level.value == row['CURRENT_WARNING_LEVEL']:
                level_inst = level(basic_data)
                break
        if not level_inst:
            current_app.logger.error('level matching error')
        res = level_inst.execute_level_review()
        last_record = self.get_last_record(row) or row
        if row['CURRENT_WARNING_LEVEL'] == WarningLevel.level_1.value \
                and calc_months_difference(last_record['INSPECT_MON'], year_mon) < 3:
            # 如果是一级预警，在前两个月的检查或者解除中始终返回未预警
            row['RECORD_TYPE'] = RecordType.normal.value
        elif isinstance(res, WaringLevelInspection):
            # 升级
            row['WARNING_LEVEL'] = res.warning_level.value
            row['CURRENT_WARNING_LEVEL'] = row['WARNING_LEVEL']
            row['RECORD_TYPE'] = RecordType.extension.value
            row['WARNING_MON'] = get_history_months(months_ago + 1)[0]
            row['WARNING_STATUS'] = WarningStatus.executing.value
            if res.warning_level.value < row['CURRENT_WARNING_LEVEL']:
                row['IS_UPGRADE'] = True
                # 更新父记录预警级别
                self.update_current_level(row, res.warning_level.value)
        elif res:
            # 解除
            print('预警解除 部门：{}，等级：{}'.format(row['DEPARTMENT_NAME'], row['WARNING_LEVEL']))
            mongo.db[self.early_warning_coll_name.value].update_one(
                {'_id': row['_id']}, {'$set': {'WARNING_STATUS': WarningStatus.released.value}})
            mongo.db[self.early_warning_coll_name.value].update_many(
                {'PARENT_ID': str(row['_id'])}, {'$set': {'WARNING_STATUS': WarningStatus.released.value}})
            row['RECORD_TYPE'] = RecordType.release.value
            row['WARNING_STATUS'] = WarningStatus.released.value
        else:
            # 延期
            row['RECORD_TYPE'] = RecordType.extension.value
            row['WARNING_MON'] = get_history_months(months_ago + 1)[0]
            row['WARNING_STATUS'] = WarningStatus.executing.value
            if level_inst.extend_to_upgrade(row, self.level_check_class):
                row['IS_UPGRADE'] = True
                # 一级一级升
                upgrade_to = level_inst.warning_level.value - 1
                # 更新父记录预警级别, 不改变自身WARNING_LEVEL
                self.update_current_level(row, upgrade_to)
                row['CURRENT_WARNING_LEVEL'] = upgrade_to
        row['ACTION_TYPE'] = ActionType.release.value
        row['PARENT_ID'] = str(row['_id'])
        row['INSPECT_DATA'] = None
        row['DESC_DATA'] = self.get_review_desc(basic_data)
        row['INSPECT_MON'] = year_mon
        row.setdefault('IS_UPGRADE', False)
        row.pop('_id')
        row.pop('SCORE')
        score_dict = {str(k.value): v for k, v in basic_data.score_dict.items()}
        row['SCORE_DATA'] = score_dict
        return row

    def review_warning(self, months_ago):
        """
        查出所有初始预警记录，对每一条进行解除检查，将结果存入数据库
        :param months_ago:
        :return:
        """
        year_mon = get_history_months(months_ago)[0]
        condition = {
            'MAJOR': self.major.value,
            # 'WARNING_MON': year_mon,
            'WARNING_STATUS': WarningStatus.executing.value,
            'PARENT_ID': None
        }
        condition_ex = {
            'MAJOR': self.major.value,
            'INSPECT_MON': year_mon,
            'WARNING_STATUS': WarningStatus.executing.value,
            'ACTION_TYPE': ActionType.inspect.value,
            'RECORD_TYPE': {'$in': [RecordType.warning.value, RecordType.extension.value]}
        }
        if self.major_risk_type:
            condition['TYPE'] = self.major_risk_type
            condition_ex['TYPE'] = self.major_risk_type
        # 找出该月份数据已经被预警的部门
        exclude_departments = [
            r.get('DEPARTMENT_ID') for r in
            mongo.db[self.early_warning_coll_name.value].find(condition_ex, {'DEPARTMENT_ID': 1, '_id': 0})]
        condition['DEPARTMENT_ID'] = {'$nin': exclude_departments}
        warning_data = mongo.db[self.early_warning_coll_name.value].find(condition, {'DESC_DATA': 0})

        _prefix = get_coll_prefix(months_ago)
        index_coll_name = '{}{}'.format(_prefix, self.index_coll_name)
        detail_index_coll_name = '{}{}'.format(_prefix, self.detail_index_coll_name)
        condition = {'MON': year_mon, 'MAJOR': self.major.value}
        if self.major_risk_type:
            condition['TYPE'] = self.major_risk_type
        index_score_data = mongo.db[index_coll_name].find(condition, {'_id': 0})
        index_score_df = pd.DataFrame(list(index_score_data))
        detail_index_score_data = mongo.db[detail_index_coll_name].find(condition, {'_id': 0})
        detail_index_score_df = pd.DataFrame(list(detail_index_score_data))
        warning_data = list(warning_data)
        if index_score_df.empty or not warning_data:
            return
        records = []
        for row in warning_data:
            r = self.__review_row(row, index_score_df, detail_index_score_df, months_ago)
            if r:
                records.append(r)
        r_condition = {
            'INSPECT_MON': year_mon,
            'MAJOR': self.major.value,
            'ACTION_TYPE': ActionType.release.value
        }
        if self.major_risk_type:
            r_condition['TYPE'] = self.major_risk_type
        mongo.db[self.early_warning_coll_name.value].remove(r_condition)
        write_bulk_mongo(self.early_warning_coll_name.value, records)
