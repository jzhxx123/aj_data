#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    This module defines all functions about extracting data from MySQL、
    calculation process and store in MongoDB .
    @date: 2018/04/08
    @author: heqiangsheng
'''
