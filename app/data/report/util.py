#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    履职报告计算基本函数
    Author: WeiBo
    Date: 2018/11/06
    Desc: Evaluate Report common function
    Method:  Get the evaluate report to Mongo
'''

# import json

# from flask import current_app

from app import mongo
import pandas as pd
from app.data.util import pd_query
from flask import g

# from datetime import datetime as dt


def get_department_structrue(major=None, station=None):
    '''获取部门结构'''
    if major is not None and station is None:
        match = {"MAJOR": major}
        majors = [major]
    elif major is not None and station is not None:
        match = {"ALL_NAME": {"$regex": station}}
        majors = [major]
    else:
        match = {}
        majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    match['HIERARCHY'] = {"$in": [2, 3, 4, 5]}
    project = {"_id": 0, "NAME": 1, "PARENT_NAME": 1}
    department = pd.DataFrame(
        list(mongo.db['base_department'].find(match, project)))
    result = {}
    for major in majors:
        result[major] = {}
        major_depart = department[department['PARENT_NAME'] == major]
        # levels = list(set(
        #     major_depart['PARENT_HIERARCHY'])).sort(reverse=False)
        # for level in leves:
        if station is not None:
            stations = [station]
        else:
            stations = list(set(major_depart['NAME']))
        for sta in stations:
            station_depart = department[department['PARENT_NAME'] == sta]
            result[major][sta] = {}
            chejians = list(set(station_depart['NAME']))
            if len(chejians) != 0:
                for chejian in chejians:
                    che_depart = department[department['PARENT_NAME'] == chejian]
                    if len(che_depart) != 0:
                        result[major][sta][chejian] = list(
                            set(che_depart['NAME']))
                    else:
                        result[major][sta][chejian] = []
    return result


def fc_to_zc(level, job):
    if level == '副处级' and '党委正职' in job:
        return '正处级'
    else:
        return level


def get_station_carde_data():
    '''得到站段干部数据'''
    SQL = '''SELECT
            a.ID_CARD AS _id,
            a.PERSON_NAME AS NAME,
            a.LEVEL,
            a.POSITION,
            a.PERSON_JOB_TYPE AS JOB,
            b.NAME AS DEPARTMENT,
            b.BUSINESS_CLASSIFY,
            b.ALL_NAME,
            c.NAME AS MAJOR,
            d.TYPE AS GRADE,
            d.ALL_NAME AS DEPARTMENT_CLASS
        FROM
            t_person AS a
                LEFT JOIN
            t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
        LEFT JOIN
            t_department_classify_config AS d
            ON b.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = d.PK_ID
        WHERE
            a.IS_DELETE = 0
                AND a.IDENTITY = '干部'
                AND b.TYPE3 NOT IN (SELECT DISTINCT
                    DEPARTMENT_ID
                FROM
                    t_department
                WHERE
                    TYPE = 3);
    '''
    data = pd_query(SQL)
    data = data.fillna(value={'JOB': 'a', 'GRADE': 0})
    majors = ['车辆', '车务', '电务', '机务', '供电', '工务']
    data = data[data['MAJOR'].isin(majors)]
    data['LEVEL'] = data.apply(lambda x: fc_to_zc(x['LEVEL'], x['JOB']),
                               axis=1)
    data['CLASS'] = data.apply(
        lambda x: get_station_carde_class(x['LEVEL'], x['JOB'], x['GRADE']),
        axis=1)
    data['STATION'] = data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    station_data = data.groupby('STATION').size().reset_index()
    station_data['station_count'] = station_data[0]
    del station_data[0]
    data = pd.merge(data, station_data, how='left', on='STATION')
    return data


def get_major_carde_data():
    '''得到业务处干部数据'''
    SQL = '''SELECT
            a.ID_CARD AS _id,
            a.PERSON_NAME AS NAME,
            a.LEVEL,
            a.PERSON_JOB_TYPE AS JOB,
            b.TYPE,
            b.NAME AS DEPARTMENT,
            b.ALL_NAME,
            c.NAME AS MAJOR
        FROM
            t_person AS a
                LEFT JOIN
            t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
        WHERE
            a.IS_DELETE = 0
                AND a.IDENTITY = '干部'
                AND b.TYPE3 IN (SELECT DISTINCT
                    DEPARTMENT_ID
                FROM
                    t_department
                WHERE
                    TYPE = 3);
    '''
    data = pd_query(SQL)
    data = data.fillna('a')
    data['CLASS'] = data.apply(
        lambda x: get_major_carde_class(x['LEVEL'], x['TYPE'], x['JOB']),
        axis=1)
    del data['TYPE']
    data = data[data['CLASS'] != '0']
    return data


def safe_major_check_data(mon):
    year = mon // 100
    month = mon % 100
    if month != 0:
        limit = f'a.`YEAR` = {year} AND a.`MONTH` = {month}'
    else:
        limit = f'a.`YEAR` = {year}'
    sql = f'''SELECT
            a.ID_CARD,
            a.CHECK_TIMES_TOTAL AS INFO,
            a.PROBLEM_NUMBER_TOTAL AS PROBLEM,
            a.MONITOR_NUMBER_TOTAL AS MONITOR,
            a.HIDDEN_DANGER_RECHECK_TIMES_TOTAL AS HIDDEN,
            a.RISK_RECHECK_TIMES_TOTAL AS RISK,
            a.IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL AS IMPORTANT,
            b.REALITY_NUMBER AS INFO_NUM,
            b.MEDIA_REALITY_PROBLEM_NUMBER AS MEDIA_NUM,
            b.REALITY_PROBLEM_NUMBER AS PROBLEM_NUM,
            b.HIDDEN_DANGER_RECHECK_REALITY_NUMBER AS HIDDEN_NUM,
            b.RISK_RECHECK_REALITY_NUMBER AS RISK_NUM,
            b.IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER AS IMPORTANT_NUM
        FROM
            t_quantization_base_quota AS a
        LEFT JOIN
            t_quantify_assess_real_time as b
                on b.ID_CARD=a.ID_CARD AND a.YEAR=b.YEAR AND a.MONTH=b.MONTH
        WHERE
            {limit}
                AND EXISTS( SELECT
                    DISTINCT d1.DEPARTMENT_ID
                FROM
                    t_department d1
                        LEFT JOIN
                    t_department d2 ON d1.TYPE3 = d2.DEPARTMENT_ID
                WHERE
                    d1.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                        AND d2.TYPE = 3);
    '''
    data = pd.DataFrame(pd_query(sql))
    return data


def get_station_carde_class(LEVEL, JOB, GRADE):
    '''计算站段干部分类'''
    '''
    params: LEVEL: 行政级别
            JOB: 职务级别
            GRADE: 部门档次
    return： 级别代码'''
    if LEVEL == '正处级':
        if '行政正职' in JOB or '党委正职' in JOB:
            return 'ZC-1'
        elif '后勤副职' in JOB or '总工' in JOB:
            return 'ZC-2'
        else:
            return 'ZC-0'
    elif LEVEL == '副处级':
        if '安全副职' in JOB or '技术副职' in JOB or '职教副职' in JOB or '生产副职' in JOB:
            return 'FC-1'
        elif '后勤副职' in JOB or '总工' in JOB:
            return 'FC-2'
        elif '财务副职' in JOB or '纪委' in JOB or '党委副职' in JOB or '工会' in JOB:
            return 'FC-3'
        else:
            return 'FC-0'
    elif LEVEL == '正科级':
        if GRADE != 0:
            return f'ZK-{int(GRADE)}'
        else:
            return f'ZK-0'
    elif LEVEL == '副科级':
        if GRADE != 0:
            return f'FK-{int(GRADE)}'
        else:
            return 'FK-0'
    elif LEVEL in ['股级', '股', '一般管理', '科员', '干事', '干事级']:
        if GRADE != 0:
            return f'YB-{int(GRADE)}'
        else:
            return 'YB-0'


def get_major_carde_class(LEVEL, TYPE, JOB):
    '''计算干部分类'''
    '''
    params: LEVEL: 行政级别
            TYPE: 部门级别
            JOB: 职务级别
    return： 级别代码'''
    if TYPE == 6:
        if LEVEL == '正处级':
            return 'Y-1'
        elif LEVEL == '副处级':
            return 'Y-2'
        elif LEVEL == '正科级':
            return 'Y-3'
        elif LEVEL == '副科级':
            return 'Y-4'
        else:
            return 'Y-5'
    elif TYPE in [4, 7, 8, 9, 10]:
        if '行政正职' in JOB or '党委正职' in JOB:
            return 'Z-1'
        elif LEVEL == '副处级':
            return 'Z-2'
        elif LEVEL == '正科级':
            return 'Z-3'
        elif LEVEL == '副科级':
            return 'Z-4'
        else:
            return 'Z-5'
    else:
        return '0'


def get_check_info_data(mon):
    """获取检查信息数据

    Arguments:
        mon {int or dict} -- mongo表中月份字段索取条件
    """
    sql = """SELECT
        c.ID_CARD,
        a.IS_YECHA,
        a.START_CHECK_TIME,
        a.END_CHECK_TIME,
        a.CHECK_WAY,
        a.CHECK_TYPE,
        b.COST_TIME,
        b.RETRIVAL_TYPE_NAME
    FROM
        t_check_info AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        LEFT JOIN t_check_info_and_person AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
        where a.SUBMIT_TIME >= '{}' and a.SUBMIT_TIME <= '{}'"""
    data = pd_query(sql.format(mon.start_date, mon.end_date))
    # coll_name = f'{g.prefix}detail_check_info'
    # keys = {
    #     "match": {
    #         "MON": mon
    #     },
    #     "project": {
    #         "PK_ID": 1,
    #         "ID_CARD": 1,
    #         "IS_YECHA": 1,
    #         "START_CHECK_TIME": 1,
    #         "END_CHECK_TIME": 1,
    #         "CHECK_WAY": 1,
    #         "COST_TIME": 1,
    #         "CHECK_TYPE": 1,
    #         "RETRIVAL_TYPE_NAME": 1
    #     }
    # }
    # documents = list(mongo.db[coll_name].find(keys['match'], keys['project']))
    # data = pd.DataFrame(documents)
    # data = data.drop_duplicates(['PK_ID', 'ID_CARD'])
    return data


sql = '''SELECT
            a.*
        FROM
            t_safety_assess_month_quantify_detail AS a
        WHERE
            a.ID_CARD IN (SELECT DISTINCT
                    c.ID_CARD
                FROM
                    t_person AS c
                        LEFT JOIN
                    t_department AS d ON d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
                WHERE
                    d.TYPE3 IN (SELECT DISTINCT
                            DEPARTMENT_ID
                        FROM
                            t_department
                        WHERE
                            type = 3))'''


def get_check_problem_data(mon):
    coll_name = f'{g.prefix}detail_check_problem'
    documents = list(mongo.db[coll_name].find({
        'MON': mon
    }, {
        "_id": 0,
        "PK_ID": 1,
        "LEVEL": 1,
        "IS_RED_LINE": 1,
        "RISK_LEVEL": 1,
        "CHECK_ID_CARD": 1,
        "ASSESS_MONEY": 1,
        "ALL_NAME": 1,
        "MAJOR": 1,
        "IS_EXTERNAL": 1,
        "IS_SPAN_DEPARTMENT": 1
    }))
    data = pd.DataFrame(documents)
    data['STATUS'] = data.apply(
        lambda x: x['IS_EXTERNAL'] + x['IS_SPAN_DEPARTMENT'], axis=1)
    data = data.drop_duplicates('PK_ID')
    data = data.rename(index=str, columns={'CHECK_ID_CARD': 'ID_CARD'})
    return data


def get_problem(mon):
    coll_name = f'{g.prefix}detail_check_problem'
    documents = list(mongo.db[coll_name].find({
        'MON': mon
    }, {
        "_id": 0,
        "LEVEL": 1,
        "IS_RED_LINE": 1,
        "RISK_LEVEL": 1,
        "ID_CARD": 1,
        "IS_EXTERNAL": 1,
        "IS_SPAN_DEPARTMENT": 1
    }))
    data = pd.DataFrame(documents)
    data['STATUS'] = data.apply(
        lambda x: x['IS_EXTERNAL'] + x['IS_SPAN_DEPARTMENT'], axis=1)
    data['TOTAL'] = data.apply(
        lambda x: get_problem_score(
            x['LEVEL'], x['IS_RED_LINE'], x['RISK_LEVEL'], x['STATUS']),
        axis=1)
    del data['LEVEL'], data['IS_RED_LINE'], data['RISK_LEVEL']
    del data['IS_EXTERNAL'], data['IS_SPAN_DEPARTMENT'], data['STATUS']
    data = data.groupby('ID_CARD').sum().reset_index()
    return data


def get_problem_score(level, red, risk, status):
    base = 1
    if status >= 1:
        base = 0.1
    levels = [
        'A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4',
        'G1', 'G2', 'G3', 'K1', 'K2', 'K3', 'K4'
    ]
    score_1 = [10, 8, 4, 1, 6, 4, 2, 0.8, 8, 6, 3, 1, 3, 2, 0.8, 3, 2, 1, 0.6]
    score_2 = [
        8, 6, 3, 0.6, 4, 3, 1, 0.5, 6, 4, 2, 0.6, 2, 1, 0.5, 2, 1, 0.8, 0.4
    ]
    score_3 = [
        5, 3, 2, 0.4, 3, 2, 0.5, 0.2, 4, 2, 1, 0.4, 1, 0.5, 0.2, 1, 0.8, 0.6,
        0.2
    ]
    score_4 = [
        3, 2, 1, 0.2, 2, 1, 0.2, 0.1, 2, 1, 0.5, 0.2, 0.5, 0.2, 0.1, 0.5, 0.6,
        0.4, 0.1
    ]
    if red != 3:
        return 20 * base
    elif level not in levels:
        return 0
    else:
        if risk == 1:
            return score_1[levels.index(level)] * base
        elif risk == 2:
            return score_2[levels.index(level)] * base
        elif risk == 3:
            return score_3[levels.index(level)] * base
        elif risk == 4:
            return score_4[levels.index(level)] * base


def get_problem_info(mon):
    '''获取问题质量分信息'''
    coll_name = f'{g.prefix}detail_check_problem'
    if g.prefix != '':
        documents = list(mongo.db[coll_name].aggregate([{
            "$match": {
                'MON': mon
            }
        }, {
            "$group": {
                "_id": '$ID_CARD',
                "TOTAL": {
                    "$sum": '$PROBLEM_SCORE'
                }
            }
        }]))
    else:
        documents = []
        for prefix in ['history_', 'monthly_']:
            coll_name = f'{prefix}detail_check_problem'
            doc = list(mongo.db[coll_name].aggregate(
                [{
                    "$match": {
                        'MON': mon
                    }
                },
                    {
                    "$group": {
                        "_id": '$ID_CARD',
                        "TOTAL": {
                            "$sum": '$PROBLEM_SCORE'
                        }
                    }
                }]))
            documents = documents + doc
    data = pd.DataFrame(documents)
    return data


def get_safe_talk_info(mon_point):
    '''获取安全谈心信息'''
    SQL = f'''SELECT
        ID_CARD AS _id,
        APPRAISAL_RESULT AS result
    FROM
        t_talk_safety_heart
    WHERE
        IS_DELETE = 0
            AND TALK_DATE >= '{mon_point[0]}'
            AND TALK_DATE <= '{mon_point[1]}';'''
    data = pd_query(SQL)
    data = data.fillna(0)
    data['time'] = 1
    return data


def get_evaluate_info(mon):
    year = mon // 100
    month = mon % 100
    if month == 0:
        start_time = f'{year-1}-01-25 00:00:00'
        end_time = f'{year}-12-25 23:59:59'
    else:
        end_time = f'{year}-{month:0>2}-24 23:59:59'
        if month == 1:
            start_time = f'{year-1}-12-25 00:00:00'
        else:
            start_time = f'{year}-{month-1:0>2}-25 00:00:00'
    # if month == 0:
    #     limit = f'year={year}'
    # else:
    #     limit = f'year = {year} AND month = {month}'
    SQL = f'''SELECT
        SUM(score) AS POINTS, COUNT(1) AS COUNT, RESPONSIBE_ID_CARD AS ID_CARD
    FROM
        t_check_evaluate_info
    WHERE
        CREATE_TIME >= '{start_time}' and CREATE_TIME <= '{end_time}'
        AND PK_ID NOT IN (SELECT
            FK_CHECK_EVALUATE_INFO_ID
        FROM
            t_check_evaluate_revocation)
    GROUP BY RESPONSIBE_ID_CARD;'''
    data = pd_query(SQL)
    return data
