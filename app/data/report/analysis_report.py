#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    履职报告计算
    Author: WeiBo
    Date: 2018/11/06
    Desc: Evaluate Report
    Method:  Get the evaluate report to Mongo
'''
import logging

from flask import current_app

from app.report.analysisReport_wb import get_data
from app import mongo


def execute(months):
    """计算全履职得分调用函数
    """
    month = months.year_month
    start_date = months.start_date
    end_date = months.end_date
    # if months.end_date.day >= 25 or months.end_date.day == 1:
    #     return 'NO NEED UPDATE'
    try:
        result = get_data(start_date, end_date)
        mongo.db.detail_analysis_report.delete_one(
            {'year': months.end_date.year, 'month': months.end_date.month})
        mongo.db.detail_analysis_report.insert_one(result)
        current_app.logger.debug(f'({month}) to mongo is done!!!')
        return 'OK'
    except Exception:
        logging.exception('Analysis Report Error')
        current_app.logger.debug(f'analysis_report {month} is wrong')
        return f'{month} ERROR'
