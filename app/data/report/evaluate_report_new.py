#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    履职报告计算
    Author: WeiBo
    Date: 2018/11/06
    Desc: Evaluate Report
    Method:  Get the evaluate report to Mongo
'''
import json
import logging
from datetime import datetime as dt

import pandas as pd
from flask import g, current_app
# from line_profiler import LineProfiler

from app import mongo
from app.data.report.util import (get_check_info_data, get_check_problem_data,
                                  get_evaluate_info, get_station_carde_data)


def timestamp_to_time(s):
    """mongo时间戳转化为时间

    Arguments:
        s {int} -- 时间戳（毫秒）

    Returns:
        datetime -- 时间类型
    """
    return dt.utcfromtimestamp(s / 1000)


def get_site_score_cl(timing, end):
    """计算车辆现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if end >= 30:
            return 0.5
        elif end >= 2230:
            return 0.3
        elif (730 <= end <= 900) or (1230 <= end <= 1330) or (1730 <= end <= 1900):
            return 0.2
        else:
            return 0
    else:
        return 0


def get_site_score_cw(timing, end):
    """计算车务现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if end >= 30 and end <= 630:
            return 0.5
        elif end >= 2230:
            return 0.3
        elif (630 <= end <= 800) or (1230 <= end <= 1400) or (1730 <= end <= 1900):
            return 0.1
        else:
            return 0
    else:
        return 0


def get_site_score_gd(timing, end):
    """计算供电现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if 30 <= end <= 600:
            return 0.5
        elif end >= 2230:
            return 0.3
        elif (630 <= end <= 800) or (1230 <= end <= 1400) or (1730 <= end <= 1900):
            return 0.1
        else:
            return 0
    else:
        return 0


def get_site_score_dw(timing, end):
    """计算电务现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if 30 <= end <= 600:
            return 0.3
        elif end >= 2230:
            return 0.3
        else:
            return 0
    else:
        return 0


def get_site_score_gw(timing, end):
    """计算工务现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if 30 <= end <= 600:
            return 0.3
        elif end >= 2230:
            return 0.3
        else:
            return 0
    else:
        return 0


def get_site_score_jw(timing, end):
    """计算机务现场检查得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    if timing >= 0.5:
        if 30 <= end <= 600:
            return 0.5
        elif 630 <= end <= 800:
            return 0.3
        elif end >= 2230:
            return 0.3
        elif (1230 <= end <= 1400) or (1730 <= end <= 1900):
            return 0.1
        else:
            return 0
    else:
        return 0


def get_check_base_score(data):
    """获取现场检查得分的基础得分

    Arguments:
        data {series} -- dataframe中的一行

    Returns:
        score: {num} -- 得分
    """
    base_score = {
        "chewu": [0.2, 1, 1.5, 1.8, 2],
        "cheliang": [0.2, 1, 1.5, 1.8, 2],
        "dianwu": [0.2, 1, 1.5, 1.8, 2],
        "gongwu": [0.2, 1, 1.5, 1.8, 2],
        "gongdian": [0.2, 1, 1.5, 1.8, 2],
        "jiwu": [0.2, 0.8, 1, 1.3, 1.6]
    }
    major = data['MAJOR']
    check_way = data['CHECK_WAY']
    timing = data['TIMING']
    # start = int(str(data['START'])[11:16].replace(":", ""))
    end = int(str(data['END_CHECK_TIME'])[11:16].replace(":", ""))
    if check_way <= 2:
        if major == "车务":
            score_list = base_score['chewu']
            score_2 = get_site_score_cw(timing, end)
        elif major == "电务":
            score_list = base_score['dianwu']
            score_2 = get_site_score_dw(timing, end)
        elif major == "机务":
            score_list = base_score['jiwu']
            score_2 = get_site_score_jw(timing, end)
        elif major == "车辆":
            score_list = base_score['cheliang']
            score_2 = get_site_score_cl(timing, end)
        elif major == "供电":
            score_list = base_score['gongdian']
            score_2 = get_site_score_gd(timing, end)
        elif major == "工务":
            score_list = base_score['gongwu']
            score_2 = get_site_score_gw(timing, end)
        else:
            return 0
    else:
        return 0
    if timing < 0.5:
        score_1 = score_list[0]
    elif timing < 1:
        score_1 = score_list[1]
    elif timing < 2:
        score_1 = score_list[2]
    elif timing < 3:
        score_1 = score_list[3]
    else:
        score_1 = score_list[4]
    return score_1 + score_2


def get_media_score_cw(timing, way, name):
    """计算车务视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if "现场作业记录仪" in name:
        scores = [0.3, 0.8, 1, 1.3, 1.5]
    elif "综合" in name or "机车" in name:
        scores = [0.3, 0.6, 0.8, 1, 1.2]
    else:
        scores = [0.2, 0.4, 0.6, 0.8, 1]
    if way == 3 or way == 4:
        if 0.25 < timing < 0.5:
            score = scores[0]
        elif 0.5 <= timing < 1:
            score = scores[1]
        elif 1 <= timing < 2:
            score = scores[2]
        elif 2 <= timing < 3:
            score = scores[3]
        elif timing >= 3:
            score = scores[4]
        else:
            score = 0
        if way == 4:
            score = score * 0.5
    return score


def get_media_score_dw(timing, way, name):
    """计算电务视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if "现场作业" in name:
        scores = [0.5, 1.5, 2.5, 3.5, 4]
    elif "综视频监控调阅" in name or "其他音视频" in name:
        scores = [0.1, 0.3, 0.5, 0.6, 0.6]
    else:
        return 0
    if way == 3 or way == 4:
        if 0.5 < timing < 1:
            score = scores[0]
        elif 1 <= timing < 1.5:
            score = scores[1]
        elif 1.5 <= timing < 2:
            score = scores[2]
        elif 2 <= timing < 2.5:
            score = scores[3]
        elif timing >= 2.5:
            score = scores[4]
        else:
            score = 0
        if way == 4:
            score = score * 0.5
    return score


def get_media_score_jw(timing, way, name):
    """计算机务视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if way == 3:
        if "数据分析" in name:
            score = 0.5
        if "机车视频" in name or "其他音视频" in name:
            score = timing * 0.8
        else:
            score = 0
    else:
        score = 0.5
    return score


def get_media_score_gw(timing, way, name):
    """计算工务视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if way == 3:
        # if "现场作业音视频" in name:
        #     scores = [0.5, 1.5, 2.5, 3.5, 4]
        # elif "录音笔" in name:
        #     scores = [0.5, 1.5, 2.5, 3.5, 4]
        # elif ("综合" in name) or ("GPS" in name) or ("自轮设备" in name) or (
        #         "GYK" in name) or ("探伤" in name) or ("道口" in name):
        #     scores = [0.5, 1.5, 2.5, 3.5, 4]
        # else:
        #     return 0
        scores = [0.5, 1.5, 2.5, 3.5, 4]
        if 0.5 < timing < 1:
            score = scores[0]
        elif 1 <= timing < 1.5:
            score = scores[1]
        elif 1.5 <= timing < 2:
            score = scores[2]
        elif 2 <= timing < 2.5:
            score = scores[3]
        elif timing >= 2.5:
            score = scores[4]
        else:
            score = 0
    else:
        score = 0
    return score


def get_media_score_cl(timing, way, name):
    """计算车辆视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if way == 3:
        if "移动视频信息" in name:
            score = timing * 0.9
        elif "车辆5T信息" in name or "车辆安全质量监控系统" in name or "其他" in name:
            score = timing * 0.7
        elif "固定视频" in name:
            if 0.1 < timing <= 0.5:
                score = 0.5
            elif 0.5 < timing <= 1:
                score = 0.8
            elif 1 < timing <= 2:
                score = 1.2
            elif timing > 2:
                score = 1.5
        else:
            score = 0
    else:
        score = 0.5
    return score


def get_media_score_gd(timing, way, name):
    """计算供电视频监控得分

    Arguments:
        timing {num} -- 检查市场
        end {date} -- 检查结束时间

    Returns:
        score [num] -- 最后得分
    """
    score = 0
    if way == 3:
        if "作业记录" in name or "作业车综合" in name or "综合" in name or "摄像手电" in name:
            score = timing * 0.8
        elif "录音" in name or "车机联控" in name or "6A" in name or "6C" in name:
            score = timing * 0.5
        elif "GYK" in name or "复视终端" in name or "GPS" in name:
            score = timing * 0.4
        elif "生产管理系统" in name or "安全管理信息系统" in name:
            score = timing * 0.3
        else:
            score = 0
    else:
        score = 0.5
    return score


def get_check_monitor_score(data):
    """计算视频监控得分

    Arguments:
        data {series} -- dataframe 每行数据

    Returns:
        score [num] -- 最终得分
    """
    major = data['MAJOR']
    timing = data['COST_TIME']
    check_way = data['CHECK_WAY']
    name = data['RETRIVAL_TYPE_NAME']
    if check_way == 3 or check_way == 4:
        if major == "车务":
            score = get_media_score_cw(timing, check_way, name)
        elif major == "电务":
            score = get_media_score_dw(timing, check_way, name)
        elif major == "机务":
            score = get_media_score_jw(timing, check_way, name)
        elif major == "车辆":
            score = get_media_score_cl(timing, check_way, name)
        elif major == "供电":
            score = get_media_score_gd(timing, check_way, name)
        elif major == "工务":
            score = get_media_score_gw(timing, check_way, name)
        else:
            score = 0
    else:
        score = 0
    return score


def get_check_score_rst(data):
    """计算安全检查得分，计算两项之和

    Arguments:
        data {series} -- dataframe 每行数据

    Returns:
        score [num] -- 安全检查最终得分
    """
    depart = data['DEPARTMENT']
    site = data['check_site_score']
    monitor = data['check_monitor_score']
    if '分析中心' in depart:
        if monitor >= 20:
            monitor = 20
    elif monitor >= 10:
        monitor = 10
    total = site + monitor
    if total >= 20:
        return 20
    else:
        return total


def get_safe_check_score(mon, months):
    """计算安全检查得分

    Arguments:
        mon {int or dict} -- 年月字段索取限制信息

    Returns:
        data -- 个人安全检查得分dataframe
    """
    data = get_check_info_data(months)
    data = data.fillna({"RETRIVAL_TYPE_NAME": "aaa", "COST_TIME": 0})
    data = data[(data['CHECK_WAY'] >= 1) & (data['CHECK_WAY'] <= 4)]
    data = data[(data['CHECK_TYPE'] != 103) & (data['CHECK_TYPE'] <= 400)]
    # data['START'] = data['START_CHECK_TIME'].apply(timestamp_to_time)
    # data['END'] = data['END_CHECK_TIME'].apply(timestamp_to_time)
    # del data['START_CHECK_TIME'], data['END_CHECK_TIME']
    # 求检查时长 单位小时
    data['TIMING'] = (data['END_CHECK_TIME'] - data['START_CHECK_TIME']).dt.total_seconds() / 3600
    data = pd.merge(
        data, station_crades, how='inner', left_on='ID_CARD', right_on='_id')
    data['check_site_score'] = data.apply(get_check_base_score, axis=1)
    data['check_monitor_score'] = data.apply(get_check_monitor_score, axis=1)
    score_data = data.groupby(
        'ID_CARD')['check_site_score', 'check_monitor_score'].sum()
    data = pd.merge(
        station_crades,
        score_data,
        how='left',
        left_on='_id',
        right_on='ID_CARD')
    data = data.fillna(0)
    # 计算安全检查最终得分
    data['check_score'] = data.apply(lambda x: get_check_score_rst(x), axis=1)
    data = data.rename(index=str, columns={'_id': 'ID_CARD'})
    return data


def get_problem_score(data):
    """计算安全问题得分中的问题质量得分和问题考核得分

    Arguments:
        data {series} --人员问题dataframe中的一行

    Returns:
        list -- [问题质量得分，问题考核得分]
    """
    major = data['MAJOR']
    level = data['LEVEL']
    assess = data['ASSESS_MONEY']
    status = data['STATUS']
    red = data['IS_RED_LINE']
    levels = [
        'A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4',
        'G1', 'G2', 'G3', 'K1', 'K2', 'K3', 'K4'
    ]
    if level not in levels:
        return [0, 0]
    if major == "车务":
        scores = [
            2.5, 2, 1, 0.5, 1.5, 1, 0.5, 0.1, 2, 1.5, 1, 0.5, 0.8, 0.5, 0.1,
            1.5, 1, 0.5, 0.1
        ]
        if status == 0:
            score_2 = assess * 0.15
            if 0 < red <= 2:
                score_2 = 37.5
        else:
            if level not in ['G1', 'G2', 'G3']:
                score_2 = assess * 0.05
                if '装管部' in data['ALL_NAME']:
                    score_2 = assess * 0.1
            else:
                score_2 = 0
    elif major == "电务":
        scores = [
            2, 1.5, 1, 0.2, 1.5, 1, 0.5, 0.1, 2, 1.5, 1, 0.2, 0.8, 0.5, 0.1, 1,
            0.6, 0.4, 0.1
        ]
        if status == 0:
            score_2 = assess * 0.1
            if 0 < red <= 2:
                score_2 = 37
        else:
            score_2 = 0
    elif major == "机务":
        scores = [
            2, 1.5, 1, 0.2, 1.5, 1, 0.5, 0.1, 2, 1.5, 1, 0.2, 0.8, 0.5, 0.1, 1,
            0.6, 0.4, 0.1
        ]
        if status == 0:
            score_2 = assess * 0.05
            if 0 < red <= 2:
                score_2 = 20
        else:
            score_2 = 0
    elif major == "车辆":
        scores = [
            3, 2, 1, 0.5, 2, 1.5, 1, 0.2, 3, 2, 1, 0.5, 1, 0.5, 0.2, 2, 1, 0.5,
            0.2
        ]
        if status == 0:
            if 0 < red <= 2:
                score_2 = 37
            else:
                score_2 = assess * 0.1
        else:
            score_2 = assess * 0.09
    elif major == "供电":
        scores = [
            3, 2, 1, 0.5, 1.5, 1, 0.5, 0.1, 2, 1.5, 1, 0.2, 0.8, 0.5, 0.1, 1,
            0.6, 0.4, 0.1
        ]
        if status == 0:
            if 0 < red <= 2:
                score_2 = 30
            else:
                score_2 = assess * 0.08
        else:
            score_2 = 0
    elif major == "工务":
        scores = [
            3, 2, 1, 0.5, 2, 1.5, 1, 0.2, 2, 1.5, 1, 0.2, 2, 1, 0.3, 2, 1, 0.5,
            0.2
        ]
        if status == 0:
            if 0 < red <= 2:
                score_2 = 37
            else:
                score_2 = assess * 0.1
        else:
            score_2 = 0
    else:
        return [0, 0]
        # score = get_media_score_gw(timing, check_way, name)
    score_1 = scores[levels.index(level)]
    return [score_1, score_2]


def get_problem_score_rst(data):
    """计算安全问题得分最终得分（问题质量得分+问题考核得分

    Arguments:
        data {series} -- dataframe的一行

    Returns:
        num -- 最终得分
    """
    assess = data['problem_assess_score']
    quality = data['problem_quality_score']
    total = assess + quality
    if total >= 40:
        return 40
    else:
        return total


def get_safe_problem_score(mon):
    """计算安全问题得分

    Arguments:
        mon {int or dict} -- 检查问题按照月份索取条件进行取值

    Returns:
        data [dataframe] -- id_card 与个人得分结果的dataframe
    """
    data = get_check_problem_data(mon)
    data = data.fillna({'IS_RED_LINE': 0, 'ASSESS_MONEY': 0})
    # data = pd.merge(data, station_crades, how='inner',
    #                 left_on='ID_CARD', right_on='_id')
    data['score'] = data.apply(lambda x: get_problem_score(x), axis=1)
    # 计算安全问题得分
    data['problem_quality_score'] = data['score'].apply(
        lambda x: 40 if x[0] >= 40 else x[0])
    # 计算问题考核得分
    data['problem_assess_score'] = data['score'].apply(
        lambda x: 40 if x[1] >= 40 else x[1])
    # 计算个人月得分
    data = data.groupby('ID_CARD')['problem_quality_score',
                                   'problem_assess_score'].sum().reset_index()
    # 个人最高40分
    data['problem_quality_score'] = data['problem_quality_score'].apply(
        lambda x: 40 if x >= 40 else x)
    data['problem_assess_score'] = data['problem_assess_score'].apply(
        lambda x: 40 if x >= 40 else x)
    # 计算个人安全问题总分
    data['problem_score'] = data.apply(
        lambda x: get_problem_score_rst(x), axis=1)
    return data


def get_safe_evaluate_score(mon):
    """计算安全履职得分

    Arguments:
        mon {int} -- 获取数据的时间取值条件

    Returns:
        data -- 个人履职得分信息
    """
    data = get_evaluate_info(mon)
    data['evaluate_score'] = data['POINTS'].apply(
        lambda x: 0 if (40 - x * 15) <= 0 else 40 - x * 15)
    return data


def score_rank(data, _prefix):
    """最终得分排序

    Arguments:
        data {dataframe} -- 整体数据
        _prefix {str} -- 排序的列的前缀

    Returns:
        dataframe -- 排序后的结果
    """
    _score = _prefix + '_score'
    _rank = _prefix + '_rank'
    _aver = _prefix + '_aver'
    # 计算排名
    data[_rank] = data.groupby(['MAJOR', 'CLASS'])[_score].rank(
        ascending=0, method='min')
    # 计算平均值
    aver_data = data.groupby(['MAJOR', 'CLASS'])[_score].mean().reset_index()
    aver_data = aver_data.rename(index=str, columns={_score: _aver})
    data = pd.merge(data, aver_data, how='left', on=['MAJOR', 'CLASS'])
    return data


def get_detail_score_rank(mon, months):
    """各个小分排序和拼接

    Arguments:
        mon {int} -- 月份信息

    Returns:
        dataframe -- 排序和整体数据拼接结果
    """
    evaluate_data = get_safe_evaluate_score(mon)  # 安全履职得分
    if mon % 100 == 0:
        mon = {"$gt": mon, "$lt": mon + 100}
    check_data = get_safe_check_score(mon, months)  # 安全检查得分
    problem_data = get_safe_problem_score(mon)  # 安全问题得分
    # 计算安全检查得分排序和平均值
    check_data = check_data.fillna(value={'check_score': 0})
    data = score_rank(check_data, 'check')
    # 计算安全问题得分排序和平均值
    problem_data = pd.merge(data, problem_data, how='left', on='ID_CARD')
    problem_data = problem_data.fillna(value={'problem_score': 0})
    data = score_rank(problem_data, 'problem')
    # 计算安全履职得分排序和平均值
    evaluate_data = pd.merge(data, evaluate_data, how='left', on='ID_CARD')
    evaluate_data = evaluate_data.fillna(value={
        'evaluate_score': 40,
        'POINTS': 0,
        "COUNT": 0
    })
    data = score_rank(evaluate_data, 'evaluate')
    # 计算最终安全履职得分
    data['score'] = data.apply(
        lambda x: x['check_score'] + x['problem_score'] + x['evaluate_score'],
        axis=1)
    return data


def get_total_person(data, group_col, column):
    """计算对应条件的干部总数

    Arguments:
        data {dataframe} -- 整体数据
        group_col {list} -- 需要按照分组的列名
        column {str} -- 产生新的列名

    Returns:
        dataframe -- 计算结果
    """
    count_data = data.groupby(group_col).size().reset_index()
    count_data[column] = count_data[0]
    del count_data[0]
    data = pd.merge(data, count_data, how='left', on=group_col)
    return data


def get_data_rank(data, group_cols, _prefix):
    """计算最终结果排序

    Arguments:
        data {dataframe} -- 整体数据
        group_cols {str or list} -- 分组依赖的列
        _prefix {str} -- 排序和平均值产生的新列前缀

    Returns:
        dataframe -- 产生排序和平均值的结果
    """
    rank = _prefix + '_rank'
    aver = _prefix + '_aver'
    data[rank] = data.groupby(group_cols)['score'].rank(
        ascending=0, method='min')
    aver_data = data.groupby(group_cols)['score'].mean().reset_index()
    aver_data = aver_data.rename(index=str, columns={'score': aver})
    data = pd.merge(data, aver_data, how='left', on=group_cols)
    return data


def _get_job_grade(job):
    first_job = [
        '行政正职', '党委正职', '安全副职', '技术副职', '职教副职', '生产副职大中修副职', '桥路副职', '线路副职',
        '运用副职'
    ]
    second_job = ['后勤副职', '总工']
    third_job = ['财务副职', '纪委', '党委副职', '工会']
    if job in first_job:
        return 1
    elif job in second_job:
        return 2
    elif job in third_job:
        return 3
    else:
        return 4


def get_evaluate_score(data):
    """得到最终的安全履职得分结果

    Arguments:
        mon {int or dict} -- 月份信息

    Returns:
        dataframe -- 最终得分情况
    """
    # 行政级别排序
    data = get_total_person(data, ['MAJOR', 'CLASS'], 'level_count')
    data = get_data_rank(data, ['MAJOR', 'CLASS'], 'level')
    # 计算单位全部干部排序
    data = get_data_rank(data, 'STATION', 'station')
    # 计算单位行政级别干部排序
    data = get_total_person(data, ['STATION', 'CLASS'], 'station_level_count')
    data = get_data_rank(data, ['STATION', 'CLASS'], 'station_level')
    # 拆分多个职务人员进行多项排序
    columns = list(data.columns)
    columns.remove('JOB')
    job_data = data[['JOB', 'ID_CARD']]
    job_data = job_data.set_index('ID_CARD')['JOB'].str.split(
        ',', expand=True).stack().reset_index(
            level=1, drop=True).reset_index(name='JOB')
    job_data['job_grade'] = job_data['JOB'].apply(_get_job_grade)
    del data['JOB']
    data = pd.merge(job_data, data, how='left', on=['ID_CARD'])
    data = data.sort_values(by='job_grade', ascending=False).reset_index()
    # 计算职务排序
    data = get_total_person(data, ['MAJOR', 'JOB'], 'job_count')
    data = get_data_rank(data, ['MAJOR', 'JOB'], 'job')
    # 计算相同可是相同岗位人员排序
    data = get_total_person(data, ['MAJOR', 'DEPARTMENT_CLASS', 'POSITION'],
                            'position_count')
    data = get_data_rank(data, ['MAJOR', 'DEPARTMENT_CLASS', 'POSITION'],
                         'position')
    return data


def _get_health_index(mon):
    key = {'MON': mon}
    project = {"_id": 0, "DEPARTMENT_NAME": 1, "SCORE": 1}
    coll = f'{g.prefix}health_index'
    data = pd.DataFrame(list(mongo.db[coll].find(key, project)))
    data = data.rename(columns={
        "DEPARTMENT_NAME": "STATION",
        "SCORE": "HEALTH_INDEX"
    })
    return data


def _get_finaly_score(data):
    if data['BUSINESS_CLASSIFY'] and '领导' in data['BUSINESS_CLASSIFY']:
        score = data['score'] * 0.8 + data['HEALTH_INDEX'] * 0.2
    else:
        score = data['score']
    if score != score:
        score = data['score']
    return score


def get_evaluate_index(mon, months):
    """数据储存

    Arguments:
        mon {int} -- 月份信息
    """
    data = get_detail_score_rank(mon, months)
    data = get_evaluate_score(data)
    data['MON'] = mon
    data['TYPE'] = 0
    del data['GRADE']
    data['GRADE'] = data['CLASS'].apply(
        lambda x: int(x[-1]) if isinstance(x, str) else x)
    health_data = _get_health_index(mon)
    data = pd.merge(data, health_data, how='left', on='STATION')
    data['score'] = data.apply(_get_finaly_score, axis=1)
    mongo.db[coll_name].delete_many({'MON': mon})
    mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
    mongo.db[coll_name].update_many({
        'LEVEL': {
            "$in": ['股级', '股', '一般管理', '科员', '干事', '干事级']
        }
    }, {"$set": {
        'LEVEL': "一般管理"
    }},
        upsert=True)
    mongo.db[coll_name].delete_many({
        'LEVEL': {
            "$in": ['工人', '其他', '其他级', '未配置', '办事员', '8', '4', 4, 8]
        }
    })
    current_app.logger.debug(f'evaluate_report: {mon} done')


def get_year_data(mon):
    mon = {'$gt': mon, '$lt': mon + 100}
    doc = []
    for prefix in ['monthly_', 'history_']:
        collName = f'{prefix}detail_evaluate_index'
        doc = doc + list(mongo.db[collName].find({'MON': mon}, {'_id': 0}))
    data = pd.DataFrame(doc)
    data = data[[
        'ID_CARD', 'POSITION', 'BUSINESS_CLASSIFY', 'CLASS', 'LEVEL', 'JOB',
        'MAJOR', 'ALL_NAME', 'DEPARTMENT', 'STATION', 'GRADE', 'check_score',
        'station_count', 'problem_score', 'NAME', 'POINTS', 'COUNT',
        'evaluate_score', 'score', 'level_count', 'job_count',
        'position_count', 'station_level_count'
    ]]
    #  'problem_quality_score', 'problem_assess_score',
    #  'check_site_score', 'check_monitor_score',]]
    count_data = data[[
        'ID_CARD', 'POSITION', 'CLASS', 'LEVEL', 'JOB', 'MAJOR', 'ALL_NAME',
        'DEPARTMENT', 'STATION', 'GRADE', 'NAME', 'level_count', 'job_count',
        'position_count', 'station_level_count', 'station_count'
    ]].drop_duplicates(['ID_CARD', 'JOB'])
    cols = ['ID_CARD', 'JOB']
    data = data.groupby(cols).sum().reset_index()
    del data['level_count'], data['job_count'], data['station_count']
    del data['position_count'], data['station_level_count'], data['GRADE']
    data = pd.merge(data, count_data, how='left', on=['ID_CARD', 'JOB'])
    return data


def get_year_evaluate_index(year):
    """数据储存

    Arguments:
        mon {int} -- 月份信息
    """
    data = get_year_data(year * 100)
    # data = get_detail_score_rank(year*100)
    data = score_rank(data, 'check')
    data = score_rank(data, 'problem')
    data = score_rank(data, 'evaluate')
    data = get_data_rank(data, 'STATION', 'station')
    data = get_data_rank(data, ['MAJOR', 'CLASS'], 'level')
    data = get_data_rank(data, ['STATION', 'CLASS'], 'station_level')
    data = get_data_rank(data, ['MAJOR', 'DEPARTMENT', 'POSITION'], 'position')
    data = get_data_rank(data, ['MAJOR', 'JOB'], 'job')
    data['YEAR'] = year
    data['TYPE'] = 1
    del data['GRADE']
    data['GRADE'] = data['CLASS'].apply(
        lambda x: int(x[-1]) if isinstance(x, str) else x)
    mongo.db[coll_name].delete_many({'YEAR': year})
    mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
    current_app.logger.debug(f'evaluate_report: {year} done')


def execute(months):
    """计算全履职得分调用函数
    """
    mon = months.year_month
    if months.end_date.day >= 25 or months.end_date.day == 1:
        return 'NO NEED UPDATE'
    global coll_name, station_crades, major_crades
    station_crades = get_station_carde_data()
    majors = ['车辆', '车务', '电务', '供电', '工务', '机务']
    station_crades = station_crades[station_crades['MAJOR'].isin(majors)]
    g.prefix = months.prefix
    coll_name = f'{g.prefix}detail_evaluate_index'
    try:
        get_evaluate_index(mon, months)
        result = 'OK'
    except Exception:
        logging.exception('Evaluate Index Error')
        result = f'{mon} ERROR'
    if (mon % 100) == 12 and (mon // 100) > 2017:
        year = months.end_date.year
        try:
            get_year_evaluate_index(year)
            result = 'OK'
        except Exception:
            logging.exception(f'Evaluate Index Error: {year}')
            result = f'{year} ERROR'
    return result


# def profile():
#     lp = LineProfiler(execute)
# lp.add_function(get_evaluate_score)
# lp.add_function(get_year_evaluate_index)
# lp.add_function(get_safe_check_score)
# lp.add_function(get_detail_score_rank)
# lp.add_function(get_safe_problem_score)
# lp.add_function(get_safe_evaluate_score)
# lp.add_function(get_check_info_data)
# lp.runcall(execute)
# lp.print_stats()
