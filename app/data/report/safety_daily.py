#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    安全中心日报分析相关数据
'''

import json

from flask import current_app

from app import mongo
from app.data.util import (get_coll_prefix, get_history_months,
                           get_previous_month, pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper


def get_data(data):
    data['DATE'] = data['DATE'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = data['DATE'].apply(
        lambda x: int(str(x)[:6].replace('-', '')))
    result = []
    for idx in data.index:
        val = data.loc[idx]
        result.append(json.loads(val.to_json()))
    current_app.logger.debug('complete: format data.')
    coll_name = '{}analysis_center_daily'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def load_data(mon_point):
    SQL_ANALYSIS_DAILY = """
        SELECT
            a.Pk_ID,
            a.DATE,
            a.MONITOR_PROBLEM_DESCRIPTION,
            a.EVALUATION_PROBLEM_DESCRIPTION,
            a.IMPORTANT_WORK_PLAN,
            b.TYPE,
            b.FLAG,
            b.DAILY_COMPLETE,
            b.DAILY_PROBLEM_NUMBER,
            b.MONTHLY_COMPLETE,
            b.MONTHLY_PROBLEM_NUMBER,
            c.NAME AS MAJOR
        FROM
            t_analysis_center_daily AS a
                LEFT JOIN
            t_department AS c ON a.PROFESSION_ID = c.DEPARTMENT_ID
                LEFT JOIN
            t_analysis_center_daily_details AS b
                ON a.PK_ID = b.FK_ANALYSIS_CENTER_DAILY_ID
        WHERE
            a.STATUS = 2 AND a.DATE >= '{}' AND a.DATE < '{}';
    """.format(mon_point[0], mon_point[1])
    data = pd_query(SQL_ANALYSIS_DAILY)
    current_app.logger.debug('complete: get data from mysql.')
    return data


def handle(mon_point):
    current_app.logger.debug(
        'handle month-{} data[analysis_center_daily]'.format(mon_point[0]))
    data = load_data(mon_point)
    if data.iloc[:, 0].size == 0:
        current_app.logger.debug('daily report fetch 0 record.')
        return
    get_data(data.copy())


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    pass
