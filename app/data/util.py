#!/usr/bin/python3
# -*- coding: utf-8 -*-

from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app
from pymongo.errors import OperationFailure

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontManager, FontProperties
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
from matplotlib.figure import Figure

from app import db, mongo
from app.utils.decorator import retry
from app.utils.common_func import get_today, get_date


def write_bulk_mongo(collection, result):
    _len = len(result)
    current_app.logger.debug(
        '├── writing {} documents to 「{}」 in {} bulks.'.format(
            _len, collection, (_len // 10000 + 1)))
    for x in range(0, _len, 10000):
        end_index = x + 10000 if (x + 10000) < _len else _len
        bulk_data = result[x:end_index]
        current_app.logger.debug('|   └── writing bulk-{} data.'.format(
            int(x / 10000 + 1)))
        mongo.db[collection].insert_many(bulk_data)
    current_app.logger.debug('├── writing: complete')


def get_previous_month(months_ago):
    """获取前第-N个月的时间统计范围，一般是上月25号到次月24号

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        tuple(str) -- 前-N个月的时间统计范围
    """
    delta = -1
    today = get_today()
    if today.day >= current_app.config.get('UPDATE_DAY'):
        delta = 0
    start_month = today + relativedelta(months=(months_ago + delta))
    end_month = today + relativedelta(months=(months_ago + delta + 1))
    start_date = dt(start_month.year, start_month.month,
                    current_app.config.get('UPDATE_DAY'))
    end_date = dt(end_month.year, end_month.month,
                  current_app.config.get('UPDATE_DAY'))
    return (start_date, end_date)

    # today = dt.today()
    # today = dt(today.year, today.month, 1)
    # start_mon = today + relativedelta(months=mondelta)
    # end_mon = today + relativedelta(months=(mondelta + 1))
    # return start_mon, end_mon


def get_history_months(mondelta):
    today = get_today()
    delta = 0
    if today.day >= current_app.config.get('UPDATE_DAY'):
        delta = 1
    start_mon = today + relativedelta(months=mondelta + delta)
    start_mon = '{}{:0>2}'.format(start_mon.year, start_mon.month)
    if int(start_mon) < 201710:
        start_mon = '201710'
    end_mon = today + relativedelta(months=delta)
    end_mon = '{}{:0>2}'.format(end_mon.year, end_mon.month)
    return (int(start_mon), int(end_mon))


def get_last_monthly(collection_name, field):
    MONS = mongo.db[collection_name].distinct(field)
    if len(MONS) == 0:
        return False
    return min(MONS)


def is_out_year(mon):
    today = get_today()
    delta = -12
    if today.day >= current_app.config.get('UPDATE_DAY'):
        delta = -11
    end_mon = today + relativedelta(months=delta)
    end_mon = '{}{:0>2}'.format(end_mon.year, end_mon.month)
    if mon < int(end_mon):
        return True
    else:
        return False


@retry
def pd_query(sql, db_name='db_aj'):
    """

    :param sql:
    :param db_name: db_aj(安监数据库) / db_mid(新数据库)
    :return:
    """
    return pd.read_sql_query(sql, db.get_engine(bind=db_name))


def get_coll_prefix(months_ago):
    if months_ago == 0:
        return 'daily_'
    elif months_ago <= -13:
        return 'history_'
    elif 0 > months_ago >= -12:
        return 'monthly_'
    else:
        return ValueError('months: {} valid.'.format(months_ago))


def get_mongodb_prefix(index_type):
    coll_map = {
        -3: 'control_intensity',
        -2: 'workshop_health',
        -1: 'autohealth',
        1: 'health',
        2: 'major',
    }
    return coll_map.get(index_type,
                        ValueError('index_type: {} valid.'.format(index_type)))


def paste_monthly_to_history(collection_name):
    monthly_coll_name = 'monthly_{}'.format(collection_name)
    history_coll_name = 'history_{}'.format(collection_name)
    last_month = get_last_monthly(monthly_coll_name, 'MON')
    if not last_month:
        return True
    if is_out_year(last_month):
        try:
            dataset = list(mongo.db[monthly_coll_name].find({
                'MON': last_month
            }))
            write_bulk_mongo(history_coll_name, dataset)
        except Exception as e:
            current_app.logger.error(
                '[fail] insert data to {}'.format(history_coll_name))
            mongo.db[history_coll_name].delete_many({'MON': last_month})
            raise

        rtn = mongo.db[monthly_coll_name].delete_many({'MON': last_month})
        if rtn.raw_result['ok'] != 1:
            raise OperationFailure(
                error=f'[fail] remove data from {monthly_coll_name}')
    return True


def paste_daily_to_monthly(collection_name):
    monthly_coll_name = 'monthly_{}'.format(collection_name)
    daily_coll_name = 'daily_{}'.format(collection_name)
    try:
        dataset = list(mongo.db[daily_coll_name].find())
        write_bulk_mongo(monthly_coll_name, dataset)
    except Exception as e:
        current_app.logger.debug(
            '[fail] insert data to {}'.format(monthly_coll_name))
        mon = mongo.db[daily_coll_name].find_one({}, {'MON': 1})
        mongo.db[monthly_coll_name].remove({'MON': mon})
        return
    try:
        mongo.db[daily_coll_name].drop()
    except Exception as e:
        current_app.logger.debug('[fail] drop {}'.format(daily_coll_name))


def get_mon_from_date(date):
    date = str(date)[:10].split("-")
    day = int(date[2])
    month = int(date[1])
    year = int(date[0])
    if day >= current_app.config.get('UPDATE_DAY') and month < 12:
        return int('{}{:0>2}'.format(year, month + 1))
    elif day >= current_app.config.get('UPDATE_DAY') and month == 12:
        return int('{}{:0>2}'.format(year + 1, 1))
    elif day <= 24:
        return int('{}{:0>2}'.format(year, month))


def update_major_maintype_weight(index_type, major=None, main_type=None,
                                 child_index_list=[1, 2, 3, 4, 5, 6],
                                 child_index_weight=[0.3, 0.25, 0.2, 0.05, 0.1, 0.1]):
    """
    更新指数权重
    :param index_type: risk_type，if index_type==0,health_index
    :param major: 专业名称
    :param child_index_list: 指数权重索引
    :param child_index_weight: 指数权重列表
    :return:
    """
    # major = "工电"
    # index_type = 2
    if major is not None:
        major = major.split('-')[0]
    # 新增字母转数字的函数，节省child_index_list为字母的人工转换时间
    # a-->1,...z-->26
    child_index_list = [ord(item) - 96 if not isinstance(item, int) else item for item in child_index_list]
    weight_dict = dict(zip(child_index_list, child_index_weight))
    from app import mongo

    today = get_today()
    today = today + relativedelta(months=-1)
    get_date(today)
    day = today.day
    month = today.month
    year = today.year
    if day >= current_app.config.get('UPDATE_DAY') and month < 12:
        mon = month + 1
    elif day >= current_app.config.get('UPDATE_DAY') and month == 12:
        mon = 1
        year = year + 1
    else:
        mon = month
    date = int(f'{year}{mon:0>2}')

    if main_type is not None:
        if int(index_type) == 0:
            condition = {"INDEX_TYPE": index_type,
                         "MAIN_TYPE": main_type, "MON": date}
        else:
            condition = {"INDEX_TYPE": index_type, "MAJOR": major,
                         "MAIN_TYPE": main_type, "MON": date}

        for key, value in weight_dict.items():
            condition["DETAIL_TYPE"] = key
            mongo.db['monthly_base_detail_index_weight'].update(
                condition, {"$set": {"WEIGHT": value}}, upsert=True)
    else:
        if int(index_type) in (0, -1, -3):
            # condition = {"INDEX_TYPE": index_type,  "MON": date}
            condition = {"INDEX_TYPE": index_type}
        else:
            # condition = {"MAJOR": major, "INDEX_TYPE": index_type,  "MON": date}
            condition = {"MAJOR": major, "INDEX_TYPE": index_type}

        for key, value in weight_dict.items():
            condition["MAIN_TYPE"] = key
            mongo.db['base_index_weight'].update(
                condition, {"$set": {"WEIGHT": value}}, upsert=True)


# 更新专业的计算基数
def update_major_index_cardinal_number(index_type, major,
                                       main_type, detail_type,
                                       cardinal_number=None):
    """
    存储最小单位是detail_type,例如存储换算单位问题数的基数
    :param index_type: 0表示综合指数，1-N表示重点指数
    :param major:
    :param main_type:
    :param detail_type:
    :param up_tobase: 需要更新的值
    :return:
    """
    base = None
    prefix = 'major_'
    try:
        if major is not None:
            major = major.split('-')[0]
        if int(index_type) == 0:
            prefix = 'health_'

        condition = {"MAJOR": major, "MAIN_TYPE": main_type,
                     "DETAIL_TYPE": detail_type}

        baselist = list(mongo.db[f'{prefix}_index_cardinal_number'].find(condition,
                                                                         {"_id": 0, "BASE_VALUE": 1}))
        if not baselist or baselist[0]["BASE_VALUE"] is None:
            base = cardinal_number
            mongo.db[f'{prefix}_index_cardinal_number'].update(condition,
                                                               {"$set": {"BASE_VALUE": base}}, upsert=True)
        else:
            base = baselist[0]["BASE_VALUE"]
        return base
    except Exception as e:
        return base


def _create_fig(labels, data, title, subtags):
    from .font import FONT_STYLE_FILE
    from matplotlib import rcParams
    ymajorLocator = MultipleLocator(0.1)  # 将y轴主刻度标签设置为0.5的倍数
    n = len(labels)

    angles = np.linspace(0, 2 * np.pi, n, endpoint=False)  # 旋转90度，从正上方开始！
    angles = np.concatenate((angles, [angles[0]]))  # 闭合

    fig = Figure(figsize=(12, 9))
    ax = fig.add_subplot(111, polar=True)  # 参数polar，表示极坐标！！
    ax.yaxis.set_major_locator(ymajorLocator)
    # 自己画grid线（5条环形线）
    for i in [30, 60, 90, 100]:
        ax.plot(angles, [i] * (n + 1), '-',
                c="#BDC3C7", lw=0.5)  # 之所以 n +1，是因为要闭合！

    # 填充底色
    ax.fill(angles, [100] * (n + 1), facecolor='w', alpha=0.5)

    # 自己画grid线（6条半径线）
    for i in range(n):
        ax.plot([angles[i], angles[i]], [0, 100], '-', c="#BDC3C7", lw=0.5)
    colors = ['#f0a626', '#1d97fc', '#2fc75c', 'yellow',  'black']
    # 画线
    for pos, item in enumerate(data):
        item = np.array(item)
        item = np.concatenate((item, [item[0]]))  # 闭合
        if pos == 0:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{subtags[pos]}')
            ax.fill(angles, item, c=colors[pos], alpha=0.2)
        else:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{subtags[pos]}')
    # 填充
    ax.tick_params(axis='both', direction='out', pad=27)  # 刻度跟标签的距离
    ax.legend(loc='best', prop=FontProperties(fname=FONT_STYLE_FILE),
              fontsize=16, bbox_to_anchor=(1.05, 1.0), borderaxespad=0.)
    ax.set_thetagrids(angles * 180 / np.pi, labels, fontproperties=FontProperties(fname=FONT_STYLE_FILE),
                      fontsize=16)
    rcParams['axes.titlepad'] = 30
    ax.set_title("{0}指数雷达图".format(title), va='bottom',
                 fontproperties=FontProperties(fname=FONT_STYLE_FILE), fontsize=24)
    ax.set_rlim(0, 100)
    # 下两行去掉所有默认的grid线
    ax.spines['polar'].set_visible(False)  # 去掉最外围的黑圈
    ax.grid(False)  # 去掉中间的黑圈
    # 关闭数值刻度
    ax.set_yticks([])
    return fig


# 雷达图
def plot_radar(labels, data, title, subtags, file_dir=None):
    """
    :param labels:np.array(['艺术A','调研I','实际R','常规C','企业E','社会S']) #标签
    :param data: np.array([1,4,3,6,4,8]) # 数据
    :param title: 图像标题 #
    :return:
    """
    from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
    import io
    import os
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    if not file_dir:
        file_dir = current_app.config.get('DOWNLOAD_FILE_HEALTH')
    dir_path = os.path.join(root_path, file_dir)
    if not isinstance(data, list):
        data = [data]
    pic_name = "{0}指数雷达图.png".format(title)
    pic_path = os.path.join(dir_path, pic_name)
    labels = np.array(labels)
    fig = _create_fig(labels, data, title, subtags)
    fig.savefig(pic_path)
    return pic_path


def filter_dianwu_laoan_func(row):
    if row["MAIN_TYPE"] == 1 and row["DETAIL_TYPE"] == 5:
        row["DETAIL_TYPE"] = 17
    if row["MAIN_TYPE"] == 5 and row["DETAIL_TYPE"] == 3:
        row["DETAIL_TYPE"] = 8
    return row


# 处理电务劳安的detail_type问题
def filter_dianwu_laoan(data, type, major):
    if major == '电务' and type == 5:
        data = data.apply(lambda row: filter_dianwu_laoan_func(row), axis=1)
    return data


def init_monthly_index_map(months_ago, risk_type):
    """[summary]
    初始化指数内容库
    Arguments:
        months_ago {[int]} -- [description]
        risk_type {[str]} -- [description]
    """
    from app.data.index.util import (get_custom_month)
    try:
        risk_type_list = risk_type.split('-')
        major = risk_type_list[0]
        mon = int(get_custom_month(months_ago)[1].replace('-', '')[:-2])
        index_type = int(risk_type_list[1])
        mongo.db['monthly_major_index_basic_data'].remove(
            {"MAJOR": major, "INDEX_TYPE": index_type, "MON": mon})
        mongo.db['monthly_detail_major_index'].remove({"MAJOR": major, "TYPE": index_type,
                                                    "MON": mon})
        mongo.db['base_index_weight'].remove(
            {"MAJOR": major, "INDEX_TYPE": index_type})
        mongo.db['monthly_base_detail_index_weight'].remove(
            {"MAJOR": major, "INDEX_TYPE": index_type, "MON": mon})
        mongo.db['monthly_major_index'].remove(
            {"MAJOR": major, "TYPE": index_type, "MON": mon})
        current_app.logger.info(f'── {risk_type} {mon} init_index success !')
        return f'{risk_type} {mon} init_index success ! '
    except Exception as e:
        error_info = f'── {risk_type} {mon} init_index failed ! RESON: {e}'
        current_app.logger.error(error_info)
        return error_info

def copy_special_index_to_another(months_ago, raw_index_type, target_index_type):
    """[将特殊需求的指数内容copy到目标指数内容里]

    Arguments:
        months_ago {[int]} -- [几个月前]
        raw_index_type {[str]} -- [原指数内容]
        target_index_type {[str]} -- [目标指数内容]
    """
    from app.data.index.util import (get_custom_month)
    from operator import itemgetter
    mon = int(get_custom_month(months_ago)[1].replace('-', '')[:-2])
    # 暂不对201906以前月份进行数据库合并
    if mon < 201906:
        return False
    # 指数计算涉及的3个库，major_index,detail_major_index, basic_index
    raw_index_type_list = raw_index_type.split('-')
    target_index_type_list = target_index_type.split('-')
    prefix = get_coll_prefix(months_ago)

    # 洗major_index数据
    major_index_coll_name = f'{prefix}major_index'
    major_index_update_condition = {
        "MON": mon,
        "MAJOR": raw_index_type_list[0],
        "TYPE": int(raw_index_type_list[1]),
        "HIERARCHY": 3
    }
    raw_data = list(mongo.db[major_index_coll_name].find(
        major_index_update_condition, {"_id": 0}))
    if len(raw_data) == 0:
        return False
    documents = list(mongo.db[major_index_coll_name].find(
        {
            "MON": mon,
            "MAJOR": target_index_type_list[0],
            "TYPE": int(target_index_type_list[1]),
            "HIERARCHY": 3
        }))
    # 删除documents中旧的raw数据
    raw_dp = [item['DEPARTMENT_ID'] for item in raw_data]
    documents = [item for item in documents if item['DEPARTMENT_ID'] not in raw_dp]

    all_documents = raw_data + documents
    rows_by_score = sorted(all_documents, key=itemgetter('SCORE'))
    for idx, item in enumerate(rows_by_score):
        # 根据目标风险中找部门id，不存在就插入，存在就更新
        mongo.db[major_index_coll_name].update(
            {
                    "MON": mon,
                    "TYPE": int(target_index_type_list[1]),
                    "MAJOR": target_index_type_list[0],
                    "DEPARTMENT_ID": item['DEPARTMENT_ID']
                },
            {"$set": {
                    "DEPARTMENT_ID": item['DEPARTMENT_ID'],
                    "DEPARTMENT_NAME": item['DEPARTMENT_NAME'],
                    "RANK": idx+1,
                    "TYPE": int(target_index_type_list[1]),
                    "MAJOR": target_index_type_list[0],
                    "MON":mon,
                    "HIERARCHY":3,
                    "SCORE":item['SCORE']
                }
                },
            upsert=True)


    # 洗detail_major_index数据
    detail_major_coll_name = f'{prefix}detail_major_index'
    raw_data = list(mongo.db[detail_major_coll_name].find(
        {
            "MON": mon,
            "HIERARCHY": 3,
            "MAJOR": raw_index_type_list[0],
            "TYPE": int(raw_index_type_list[1])
        },
        {"_id": 0}))
    
    documents = list(mongo.db[detail_major_coll_name].find(
        {
            "MON": mon,
            "HIERARCHY": 3,
            "MAJOR": target_index_type_list[0],
            "TYPE": int(target_index_type_list[1])
        },
        {"_id": 0}))
    documents = [item for item in documents if item['DEPARTMENT_ID'] not in raw_dp]
    # 获取（main_type, detail_type)种类
    # 合并到的数据不一定存在，保证原数据不为空,以原数据的main_type,detail_type为准
    sub_index_type_list = list(mongo.db[detail_major_coll_name].find(
        {
            "MON": mon,
            "HIERARCHY": 3,
            "MAJOR": raw_index_type_list[0],
            "TYPE": int(raw_index_type_list[1])
        },
        {"_id": 0,
        "MAIN_TYPE": 1,
        "DETAIL_TYPE": 1
         }
    )
    )

    # 去重main_type,detail_type的组合
    sub_index_tuple = []
    for idx, item in enumerate(sub_index_type_list):
        if item not in sub_index_tuple:
            sub_index_tuple.append(item)
    all_documents = raw_data + documents
       # 根据目标风险中找部门id，不存在就插入，存在就更新

    # 同时洗basic_data_index的数据
    raw_basic_data = list(mongo.db[f'{prefix}major_index_basic_data'].find(
        {
            "MON": mon,
            "HIERARCHY": 3,
            "MAJOR": raw_index_type_list[0],
            "INDEX_TYPE": int(raw_index_type_list[1])
        },
        {"_id": 0}))
    basic_data_documents = list(mongo.db[f'{prefix}major_index_basic_data'].find(
        {
            "MON": mon,
            "HIERARCHY": 3,
            "MAJOR": target_index_type_list[0],
            "INDEX_TYPE": int(target_index_type_list[1])
        },
        {"_id": 0}))
    basic_data_documents = [item for item in basic_data_documents if item['DEPARTMENT_ID'] not in raw_dp]
    all_basic_data_documents = raw_basic_data + basic_data_documents

    # 循环遍历不同main_type，detail_type组合，洗数据
    for pos, cusor in enumerate(sub_index_tuple):
             # 为了排序先根据main_type,detail_type筛选一遍数据排序
        # 洗detail_major_index数据
        tartget_documents = [item for item in all_documents if (
            item['MAIN_TYPE'] == cusor['MAIN_TYPE'] 
            and 
            item['DETAIL_TYPE'] == cusor['DETAIL_TYPE']
            )]
        rows_by_score = sorted(tartget_documents, key=itemgetter('SCORE'), reverse=True)
        for indx, elemt in enumerate(rows_by_score):
            detail_major_setvalues = elemt.copy()
            detail_major_setvalues.update({
                "RANK": indx+1,
                "TYPE": int(target_index_type_list[1]),
                "MAJOR": target_index_type_list[0]
                })
            mongo.db[detail_major_coll_name].update(
                {
                        "MON": mon,
                        "TYPE": int(target_index_type_list[1]),
                        "MAJOR": target_index_type_list[0],
                        "DEPARTMENT_ID": elemt['DEPARTMENT_ID'],
                        "MAIN_TYPE": cusor['MAIN_TYPE'],
                        "DETAIL_TYPE": cusor['DETAIL_TYPE']
                    },
                {"$set": detail_major_setvalues
                    },
                upsert=True)
        

        # 洗basic_data 数据
        tartget_basic_data_documents = [item for item in all_basic_data_documents if (
            item['MAIN_TYPE'] == cusor['MAIN_TYPE'] 
            and 
            item['DETAIL_TYPE'] == cusor['DETAIL_TYPE']
            and 
            cusor['DETAIL_TYPE'] != 0
            )]
        # basic_data中不存在detail_type=0的数据，避免插入，跳过
        if cusor['DETAIL_TYPE'] == 0:
            continue
        basic_rows_by_score = tartget_basic_data_documents
        if 'SCORE' in tartget_basic_data_documents[0].keys():
            basic_rows_by_score = sorted(tartget_basic_data_documents, key=itemgetter('SCORE'), reverse=True)
        for step, content in enumerate(basic_rows_by_score):
            basic_data_setvalue = content.copy()
            if 'RANK' in content.keys():
                basic_data_setvalue.update({"RANK": step+1})
            basic_data_setvalue.update({
                "INDEX_TYPE": int(target_index_type_list[1]),
                "MAJOR": target_index_type_list[0]
            })
            mongo.db[f'{prefix}major_index_basic_data'].update(
                {
                    "MON": mon,
                    "INDEX_TYPE": int(target_index_type_list[1]),
                    "MAJOR": target_index_type_list[0],
                    "DEPARTMENT_ID": content['DEPARTMENT_ID'],
                    "MAIN_TYPE": cusor['MAIN_TYPE'],
                    "DETAIL_TYPE": cusor['DETAIL_TYPE']
                    },
                {"$set": basic_data_setvalue
                    },
                upsert=True)

    # T这里有个注意的地方basic内数据中有的content已经给好排名了。可能导致最后前端显示和文档渲染跟实际不一致
    # 新增basic_data中type3类型，自定义模版，自定义参数
    # 绑定两个指数，一定要一个先后顺序，避免数据重复洗。（init_map_func要调整）


def df_data_topkl(sql,filename, filepath, db_name='db_aj'):
    """[将sql查出来的数据静态化pkl文件]
    
    Arguments:
        sql {[str]} -- [查询的sql]
        filepath {[str]} -- [pkl文件存储位置]
    """
    import os
    from pathlib import Path
    df = pd_query(sql,db_name=db_name)
    output_dir = Path(filepath + '/data_pkl/')
    output_dir.mkdir(parents=True, exist_ok=True)
    output_file = filename + '.pkl'
    df.to_pickle(output_dir / output_file)


def pkl_todf(sql, filename, filepath, db_name='db_aj'):
    """[将pkl数据转换成df，先查找是否存在，不存在执行查询]
    
    Arguments:
        sql {[str]} -- [查询的sql]
        filename {[查询]} -- [description]
        filepath {[type]} -- [description]
    
    Keyword Arguments:
        db_name {str} -- [description] (default: {'db_aj'})
    """
    import os
    from pathlib import Path
    save_path = filepath + '/data_pkl/' + filename + '.pkl'
    if os.path.exists(save_path):
        data = pd.read_pickle(save_path)
    else:
        data = pd_query(sql, db_name)     
        output_dir = Path(filepath + '/data_pkl/')
        output_dir.mkdir(parents=True, exist_ok=True)
        output_file = filename + '.pkl'
        data.to_pickle(output_dir / output_file)
    return data


def get_data_from_mongo_by_find(months_ago, condition, saved_condition, collection_name):
    """[根据获取条件，返回条件，获取mongo数据]
    
    Arguments:
        condition {[type]} -- [description]
        saved_condition {[type]} -- [description]
    """
    from app.data.index.util import (get_custom_month)
    prefix = get_coll_prefix(months_ago)
    data = list(mongo.db[f'{prefix}{collection_name}'].find(
        condition,
        saved_condition
        ))
    return data

if __name__ == '__main__':
    current_app.logger.debug(get_previous_month(-2))
