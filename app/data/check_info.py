#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json

# from line_profiler import LineProfiler
import pandas as pd
from flask import current_app

from app import mongo
from app.data.util import (get_coll_prefix, get_history_months,
                           get_previous_month, pd_query, write_bulk_mongo)
from app.utils.decorator import init_wrapper

'''
    由于check_info数据量比较多，mongodb里只存储上年数据，
    如果用户要查看更多历史数据，则去甲方mysql数据库里获取
'''


def data_format(hour):
    hour = int(str(hour)[11:13])
    hour_section = [[0, 6], [6, 8], [8, 12], [12, 14], [14, 18], [18, 20],
                    [20, 24]]
    for idx, val in enumerate(hour_section):
        if hour >= val[0] and hour < val[1]:
            return idx + 1


def get_detail_data(data, mon):
    data['MON'] = mon
    data['DATE'] = data['END_CHECK_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['START_CHECK_HOUR'] = data['START_CHECK_TIME'].apply(
        lambda x: int(str(x)[:13].replace('-', '').replace(' ', '')))
    data['END_CHECK_HOUR'] = data['END_CHECK_TIME'].apply(
        lambda x: int(str(x)[:13].replace('-', '').replace(' ', '')))
    data['OWN'] = data['CHEWU_OWN'].apply(lambda x: sure_chewu_own(x))
    # del data['END_CHECK_TIME']
    # del data['START_CHECK_TIME']
    del data['CHEWU_OWN']
    # major_sql = "SELECT DEPARTMENT_ID, NAME FROM t_department;"
    # major_data = pd_query(major_sql)
    # data = pd.merge(
    #     data,
    #     pd.DataFrame({
    #         'DPID': major_data['DEPARTMENT_ID'],
    #         'MAJOR': major_data['NAME']
    #     }),
    #     how='left',
    #     left_on='TYPE2',
    #     right_on='DPID')
    del data['TYPE2']
    current_app.logger.debug('complete: format data.')
    coll_name = '{}detail_check_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    # write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].insert_many(json.loads(data.T.to_json()).values())
    mongo.db[coll_name].create_index(
        [('ID_CARD', 1), ('MON', 1)], background=True)
    mongo.db[coll_name].create_index(
        [('MAJOR', 1), ('MON', 1)], background=True)


def get_map_data(data, mon):
    data.drop_duplicates('PK_ID', keep='first', inplace=True)
    data['MON'] = mon
    data['DATE'] = data['END_CHECK_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    result = []
    for x in range(3, 6):
        data_i = data.copy()
        data_i.dropna(subset=[f'TYPE{x}'], inplace=True)
        xdata = data_i.groupby(['TYPE{}'.format(x), 'MON']).size()
        for index in xdata.index:
            value = xdata.loc[index]
            result.append({
                'DPID': index[0],
                'MON': index[1],
                'COUNT': int(value),
            })
    coll_name = '{}map_check_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    result = []
    dep_data = pd_query("SELECT DEPARTMENT_ID, \
                NAME AS STATION FROM t_department")
    data = pd.merge(
        data, dep_data, how='left', left_on='TYPE3', right_on='DEPARTMENT_ID')
    del data['DEPARTMENT_ID']
    ydata = data.groupby(['DATE', 'MAJOR', 'STATION', 'MON']).size()
    for index in ydata.index:
        value = ydata.loc[index]
        result.append({
                'DATE': index[0],
                'MAJOR': index[1],
                'STATION': index[2],
                'MON': index[3],
                'COUNT': int(value)
        })
    current_app.logger.debug('complete: format data.')
    coll_name = '{}detail_map_check_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def sure_chewu_own(_ids):
    '''车务所属'''
    if _ids is not None and len(_ids.split(',')) > 1:
        rst = []
        for _id in _ids.split(','):
            if int(_id) == 1:
                rst.append("客运")
            elif int(_id) == 2:
                rst.append("货运")
            elif int(_id) == 3:
                rst.append("运输")
            elif int(_id) == 4:
                rst.append("其他")
        result = ','.join(rst)
    elif _ids is not None and len(_ids) == 1:
        if int(_ids) == 1:
            a = "客运"
        elif int(_ids) == 2:
            a = "货运"
        elif int(_ids) == 3:
            a = "运输"
        elif int(_ids) == 4:
            a = "其他"
        result = a
    else:
        result = ''
    return result


def get_mysql_data(mon_point):
    data = []
    SQL_CHECK_INFO = """SELECT
            a.PK_ID,
            a.CHECK_WAY,
            a.CHECK_TYPE,
            a.START_CHECK_TIME,
            a.END_CHECK_TIME,
            a.SUBMIT_TIME,
            a.CHEWU_OWN,
            a.CHECK_ITEM_NAMES,
            a.RISK_NAME,
            a.DEPARTMENT_ALL_NAME,
            a.IS_GAOTIE,
            a.IS_DONGCHE,
            a.IS_KECHE,
            a.IS_YECHA,
            a.CHECK_ADDRESS_NAMES,
            a.PROBLEM_NUMBER,
            b.FK_DEPARTMENT_ID AS CHECKED_DPID,
            b.FK_CHECK_POINT_ID,
            c.TYPE2,
            c.TYPE3,
            c.TYPE4,
            c.TYPE5,
            d.ID_CARD,
            d.FK_DEPARTMENT_ID AS INSPECT_DPID,
            d.PERSON_NAME,
            e.RETRIVAL_TYPE_NAME,
            e.COST_TIME,
            f.DEPARTMENT_ID AS DPID,
            f.NAME AS MAJOR
        FROM
            t_check_info AS a
                LEFT JOIN
            t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
                LEFT JOIN
            t_check_info_and_person AS d ON d.FK_CHECK_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = d.FK_DEPARTMENT_ID
                LEFT JOIN
            t_check_info_and_media AS e ON e.FK_CHECK_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS f ON f.DEPARTMENT_ID = c.TYPE2
        WHERE a.END_CHECK_TIME >= '{}' AND a.END_CHECK_TIME < '{}'
            AND f.NAME IN ('电务', '车务',
            '工务',
            '车辆',
            '供电',
            '机务')
    """.format(mon_point[0], mon_point[1])
    data = pd_query(SQL_CHECK_INFO)
    return data


def load_data(mon_point):
    data = get_mysql_data(mon_point)
    current_app.logger.debug('complete: get data from mysql.')
    return data


def handle(mon_point):
    current_app.logger.debug(f'handle ({mon_point[0]}, {mon_point[1]}) data.')
    data = load_data(mon_point)
    if data is None or data.empty:
        current_app.logger.debug(
            'check info({mon_point[0]}, {mon_point[1]}) fetch 0 record.')
        return
    get_detail_data(data.copy(),
                    int(f'{mon_point[1].year}{mon_point[1].month:0>2}'))
    get_map_data(data.copy(),
                 int(f'{mon_point[1].year}{mon_point[1].month:0>2}'))


def _analysis_pd_memory(pd_data):
    print('--------------- ||| --------------------')
    gl_obj = pd_data.select_dtypes(include=['object']).copy()
    gl_obj.info(memory_usage='deep')
    # print(gl_obj.describe())
    after_data = pd.DataFrame()
    for col in gl_obj.columns:
        uni_val = len(gl_obj[col].unique())
        t_val = len(gl_obj[col])
        if uni_val / t_val > 0.2:
            print(col)
            after_data.loc[:, col] = gl_obj[col].astype('category')
        else:
            after_data.loc[:, col] = gl_obj[col]
    after_data.info(memory_usage='deep')

    # for dtype in ['float', 'int', 'object']:
    #     selected_dtype = pd_data.select_dtypes(include=[dtype])
    #     mean_usage_b = selected_dtype.memory_usage(deep=True).mean()
    #     mean_usage_mb = mean_usage_b / 1024**2
    #     print("Average memory usage for {} columns: {:03.2f} MB".format(
    #         dtype, mean_usage_mb))
    print('--------------- ||| --------------------')
    exit(0)


@init_wrapper
def execute(months):
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    return 'OK'


# def profile():
#     lp = LineProfiler(execute)
#     lp.add_function(load_data)
#     lp.add_function(get_detail_data)
#     lp.add_function(get_map_data)
#     lp.add_function(write_bulk_mongo)
#     lp.runcall(execute)
#     lp.print_stats()


if __name__ == '__main__':
    # 测试运行时间
    # profile()
    pass
