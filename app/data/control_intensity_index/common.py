#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""

import pandas as pd

from app import mongo
from app.data.control_intensity_index.const import INDEX_TYPE
from app.data.util import get_history_months, get_coll_prefix, get_mongodb_prefix, write_bulk_mongo


def merge_all_child_item(row, hierarchy, item_name, item_weight):
    """将指数的各个子项按照权重合并成一个总数

    Arguments:
        row {[type]} --
        hierarchy {int} -- 单位层级
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Returns:
        float -- 指数分数
    """
    score = []
    for item in item_name:
        item = f'{item}_{hierarchy}'
        if item in row:
            score.append(round(row[item], 2))
        else:
            score.append(0)
    rst_score = sum([round(i[0], 2) * i[1] for i in zip(item_weight, score)])
    rst_score = 0 if rst_score < 0 else round(rst_score, 2)
    return rst_score


def summarize_child_index(child_score, dpid_data, index_type, main_type, months_ago, item_name, item_weight,
                          hierarchy_list=(3,), major=None):
    """将指数的各个子项按照权重合并成一个总数， 计算排名、入库
    Arguments:
        child_score {list[DataFrame]} -- 各个子项的集合
        dpid_func {func} -- 对应层次单位的函数
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        months_ago {int} -- 第前{-N}个月
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Keyword Arguments:
        hierarchy_list {list} -- 站段、车间、班组 (default: {[3]})
    """
    if len(child_score) == 0:
        return
    for hierarchy in hierarchy_list:
        h_child_score = [
            x for x in child_score if x.columns.values[0][-1] == str(hierarchy)
        ]
        if not h_child_score:
            continue
        data = pd.concat(h_child_score, axis=1, sort=False)
        data.fillna(0, inplace=True)
        data['SCORE'] = data.apply(
            lambda row: merge_all_child_item(row, hierarchy, item_name, item_weight),
            axis=1)

        data = append_major_column_to_df(dpid_data, data)
        calc_and_rank_index(data, 'SCORE', hierarchy, index_type, main_type, 0,
                            months_ago, major)


def append_major_column_to_df(dpid_data, data):
    """增加专业列与

    Arguments:
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        data {type} --

    Returns:
        DataFrame --
    """
    data = pd.merge(
        dpid_data, data, how='left', left_on='DEPARTMENT_ID', right_index=True)
    return data


def divide_two_col(row, numerator_col, denominator_col):
    if row[denominator_col] == 0:
        return 0
    return row[numerator_col] / row[denominator_col]


def calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, major):
    """按照专业分组排名，将排名、分数数据写入数据库

    Arguments:
        data {[type]} -- 带有分值的按单位分组选好的
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    """
    data.fillna(0, inplace=True)
    data['group_sort'] = data[column].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    score_rst = []
    mon = get_history_months(months_ago)[0]
    for index, row in data.iterrows():
        score_rst.append({
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'SCORE': round(row[column], 2),
            'RANK': int(row['group_sort']),
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME']
        })
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}detail_{coll_prefix}_index'
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, score_rst)


def summarize_operation_set(data,
                            dpid_data,
                            column,
                            hierarchy,
                            index_type,
                            main_type,
                            detail_type,
                            months_ago,
                            major=None):
    """拿到业务逻辑数据计算后的一套公共操作（排名、子指数汇总、入库）

    Arguments:
        data {DataFrame} -- index为部门ID
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    Keyword Arguments:
    """
    data = append_major_column_to_df(dpid_data, data)
    calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, major)


def write_export_basic_data_to_mongo(data,
                                     months_ago,
                                     hierarchy,
                                     main_type,
                                     detail_type,
                                     major=None,
                                     index_type=INDEX_TYPE):
    """将中间过程写入mongo，写之前需要删除之前的对应月份历史数据，避免冗余
    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(index_type)
    coll_name = f'{prefix}{coll_prefix}_index_basic_data'
    # 删除历史数据
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type,
    }
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


def format_export_basic_data(data,
                             main_type,
                             detail_type,
                             hierarchy,
                             months_ago,
                             index_type=INDEX_TYPE):
    data_rst = []
    mon = get_history_months(months_ago)[0]
    for idx, row in data.iterrows():
        data_rst.append({
            'TYPE': 2,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'CONTENT': row['CONTENT'],
            'SCORE': row.get('SCORE', '')
        })
    return data_rst


def calc_unit_ratio(value):
    """
    单位系数换算
    :return:
    """
    if value <= -0.3:
        return 2
    elif value <= -0.2:
        return 1.5
    elif value <= 0.1:
        return 1.2
    elif value <= 0.1:
        return 1
    elif value <= 0.2:
        return 0.8
    elif value <= 0.3:
        return 0.7
    else:
        return 0.5

