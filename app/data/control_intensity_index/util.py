#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/11/8
Description: 
"""

import time
import logging
from functools import wraps

import pandas as pd
from flask import current_app

from app import db
from app.utils.decorator import retry
from app.data.util import pd_query, get_history_months
from app.utils.common_func import get_today
from app.utils.data_update_models import UpdateMonth


@retry
def pd_query(sql, db_name='db_aj', parse_dates=None):
    """
    :param parse_dates:
    :param sql:
    :param db_name: db_aj(安监数据库) / db_mid(新数据库)
    :return:
    """
    return pd.read_sql_query(sql, db.get_engine(bind=db_name), parse_dates=parse_dates)


def validate_exec_month(func):
    """一个检查指数计算月份是否有效的装饰器

    Arguments:
        func  -- 指数计算入口函数

    Raises:
        ValueError -- [传入的月份错误]

    Returns:
        func --返回被调函数
    """

    @wraps(func)
    def wrapper(months_ago: UpdateMonth, *args, **kwargs):
        start = time.time()
        if isinstance(months_ago, UpdateMonth):
            months_ago = months_ago.get_month_delta()
        if months_ago < 0:
            pass
        else:
            months_ago *= -1
        if get_history_months(months_ago)[0] < 201710:
            current_app.logger.error(
                f'{func.__module__}.{func.__name__}: param invalid')
            raise ValueError(f'months_agos-[{months_ago}] param error')

        # 判断是否是25-月底，如果是则更新指数
        if months_ago == 0 and get_today().day < current_app.config.get(
                'UPDATE_DAY'):
            return 'NO NEED TO UPDATE'

        current_app.logger.debug(
            f'── {func.__module__} index computing starts.')
        try:
            func(months_ago, *args, **kwargs)
        except Exception:
            logging.exception('Index Error')
            return 'ERROR'
        current_app.logger.debug(f'── {func.__module__} index done.')
        cost = round(time.time() - start, 3)
        current_app.logger.debug(f'── {func.__module__} had costed time --- {cost}.')
        return 'OK'

    return wrapper