#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:  关键点卡控质量指数
"""

import pandas as pd
import numpy as np
from flask import current_app

from app.data.control_intensity_index.common_data import init_common_data
from app.data.control_intensity_index.const import KeyControlQualityDetailType, INDEX_TYPE, MainType, HIERARCHY
from app.data.control_intensity_index.util import validate_exec_month
from app.data.index.util import get_custom_month
from app.data.util import update_major_maintype_weight, pd_query
from app.data.index.common import (combine_child_index_func, df_merge_with_dpid)
from app.data.control_intensity_index.common import summarize_child_index, summarize_operation_set, \
    write_export_basic_data_to_mongo, format_export_basic_data, calc_unit_ratio
from app.data.control_intensity_index import cache_client
from app.data.control_intensity_index.common_sql import MAJOR_SQL


def _get_sql_data(months_ago):
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, CHECK_ADDRESS

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    CHECK_ADDRESS = cache_client.get('CHECK_ADDRESS', True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def get_children_items(pk_id, df):
    """
    只看2、3级的项目
    :param pk_id:
    :param df:
    :return:
    """
    return df[df['PARENT_ID'] == pk_id]


def add_data_col(row, score_dict):
    department_dict = score_dict.get(row['DEPARTMENT_ID'])
    content = '风险卡控点总数：{}，未覆盖的卡控点数量：{} <br/>'
    if department_dict:
        score = department_dict['SCORE']
        item_count = department_dict['ITEM_COUNT']
        fail_item = department_dict['ADDRESS']
        content = content.format(item_count, department_dict['FAILED_ITEMS_COUNT'])
        content += '未覆盖的风险卡控点：<br/>'
        content += '<br/>'.join(['{}:{}'.format(k, '，'.join(v)) for k, v in fail_item.items()])
    else:
        score = 0
        item_count = 0
        content = content.format(0, 0)
    return pd.Series([score, content, item_count], index=['SCORE', 'CONTENT', 'ITEM_COUNT'])


def calc_score(row):
    """
    a=100/风险点总数。得分=∑加分
    :param row:
    :return:
    """
    if row['ITEM_COUNT'] == 0:
        return 0
    point_a = 100 / row['ITEM_COUNT']
    score = row['SCORE'] * point_a
    return min(score, 100)


def get_check_item_and_problem_by_address(config_row, check_items, problem_base):
    """
    根据风险点配置的检查项目找出所有关联的基础问题
    :param config_row:
    :param check_items:
    :param problem_base:
    :return:
    """
    risk_control_id = config_row['FK_RISK_CONTROL_ID']
    check_item_and_problem = check_item_and_problem_dict.get(risk_control_id, None)
    if check_item_and_problem is not None:
        return check_item_and_problem
    split_items = [int(x) for x in config_row['CHECK_ITEM_IDS'].split(',') if x.isdigit()]
    item_df = check_items[check_items['PK_ID'].isin(split_items)].copy()
    extra_items = []
    drop_items = []
    for _, item_row in item_df.iterrows():
        item = item_row['PK_ID']
        if item_row['HIERARCHY'] == 2:
            if item in children_item_cache.keys():
                children = children_item_cache[item]
            else:
                children_item_cache[item] = get_children_items(item, check_items)
                children = children_item_cache[item]
            if not children.empty:
                extra_items.append(children)
                drop_items.append(item)
    item_df.drop(item_df[item_df['PK_ID'].isin(drop_items)].index, inplace=True)
    item_df = pd.concat([item_df] + extra_items)
    item_df.rename(columns={'PK_ID': 'CHECK_ITEM_ID'}, inplace=True)
    check_item_and_problem = pd.merge(pd.DataFrame(item_df,
                                                   columns=['CHECK_ITEM_ID', 'FK_DEPARTMENT_ID',
                                                            'HIERARCHY',
                                                            'NAME']),
                                      pd.DataFrame(
                                          problem_base[problem_base['FK_DEPARTMENT_ID'] == config_row['TYPE3']],
                                          columns=['FK_CHECK_ITEM_ID', 'PROBLEM_BASE_ID', 'RISK_LEVEL', 'TYPE2']),
                                      left_on='CHECK_ITEM_ID', right_on='FK_CHECK_ITEM_ID')
    check_item_and_problem_dict[risk_control_id] = check_item_and_problem
    return check_item_and_problem


check_problems_dict = {}  # 存储月份对应检查问题
check_info_and_address_dict = {}  # 存储月份对应检查信息和地点
children_item_cache = {}
address_item = {}
check_item_and_problem_dict = {}
department_check_problem_dict = {}  # 存储部门月份检查问题


def _stats_problem_control(months_ago, risk_level, score_column, detail_type, filter_key_problem=False):
    """
    每个风险卡控点具有的检查项目，筛选当月检查出的问题实体的检查项目（即问题类型），
    来确定风险卡控点中的任意一个项目的排查，若一个风险卡控点有一个检查项目发现了问题，即可视为风险点被覆盖
    :param months_ago: n月之前
    :param risk_level: 过滤该风险级别以上问题
    :param score_column:
    :param detail_type:
    :return:
    """
    rst_index_score = []
    station_data = ZHANDUAN_DPID_DATA.copy()
    check_address_and_risk_control = cache_client.get_or_set_global('check_address_score_config_and_risk_control')
    check_items = cache_client.get('check_items')
    problem_base = cache_client.get('problem_base')
    key_problem_lib = cache_client.get('key_problem_lib')
    score_dict = {}

    # 检查地点配置站段归类
    check_address = CHECK_ADDRESS
    # 地点配置与风险卡控点关联
    check_address_and_risk_control = check_address_and_risk_control[
        check_address_and_risk_control['IS_DELETE'] == 0].copy()
    check_address_and_risk_control.rename(columns={'NAME': 'RISK_CONTROL_NAME'}, inplace=True)
    check_address = pd.merge(pd.DataFrame(check_address,
                                          columns=['PK_ID', 'TYPE3', 'TYPE', 'FK_CHECK_POINT_ID', 'FK_DEPARTMENT_ID',
                                                   'NAME', 'ADDRESS_NAME']),
                             check_address_and_risk_control,
                             left_on='PK_ID', right_on='FK_CHECK_ADDRESS_SCORE_CONFIG_ID')
    check_address['CHECK_DAY_NUMBER'] = check_address['CHECK_DAY_NUMBER'].astype('int', errors='ignore')

    check_items = check_items[(check_items['IS_DELETE'] == 0) & (check_items['HIERARCHY'] >= 2)
                              & (check_items['HIERARCHY'] <= 3)]
    problem_base = problem_base[(problem_base['IS_DELETE'] == 0) & (problem_base['STATUS'] == 3)].copy()
    problem_base.rename(columns={'PK_ID': 'PROBLEM_BASE_ID'}, inplace=True)
    key_problem_list = key_problem_lib['FK_PROBLEM_BASE_ID'].values

    for department_id, department_group in check_address.groupby(['TYPE3']):
        # 逐个部门计算
        # if department_id not in ('19B8C3534E035665E0539106C00A58FD',
        #                          # '19B9D8D920DD589FE0539106C00A1189',
        #                          # '19B8C3534E095665E0539106C00A58FD', '19B8C3534E285665E0539106C00A58FD'
        #                          ):
        #     continue
        department_group = department_group.sort_values(['CHECK_DAY_NUMBER'], ascending=False)
        department_dict = score_dict.setdefault(department_id, {})
        address_fail_items = department_dict.setdefault('ADDRESS', {})  # 存储每个地点未被覆盖的风险点
        department_score = 0
        failed_items_count = 0
        for cycle, cycle_group in department_group.groupby(['CHECK_DAY_NUMBER'], sort=False):  # 以检查周期分组
            for config_id, config_group in cycle_group.groupby('PK_ID'):  # 以地点配置分组
                # 1: 地点，对应FK_DEPARTMENT_ID 2： 重要检查点，对应FK_CHECK_POINT_ID
                if config_group.iloc[0]['TYPE'] == 1:
                    address_name = config_group.iloc[0]['NAME']
                    address_match_key = 'FK_DEPARTMENT_ID'
                else:
                    address_match_key = 'FK_CHECK_POINT_ID'
                    address_name = config_group.iloc[0]['ADDRESS_NAME']
                for _, row in config_group.iterrows():  # 遍历每个风险点
                    # 找到这个风险点配置的检查项目对应的基础问题
                    check_item_and_problem = get_check_item_and_problem_by_address(row, check_items, problem_base)
                    check_item_and_problem = check_item_and_problem[check_item_and_problem['RISK_LEVEL'] <= risk_level]
                    if filter_key_problem:
                        # 处理关键问题
                        risk_key_problem_ids = []
                        for _, item_problem_row in check_item_and_problem.iterrows():
                            # 找出来的关键问题都是专业级的，这里要加上站段的继承了此关键问题的问题
                            if item_problem_row['TYPE2'] in key_problem_list:
                                risk_key_problem_ids.append(item_problem_row['PROBLEM_BASE_ID'])
                        check_item_and_problem = check_item_and_problem[
                            check_item_and_problem['PROBLEM_BASE_ID'].isin(risk_key_problem_ids)]
                    if check_item_and_problem.empty:
                        continue
                    risk_control_checked = False
                    for i in range(get_months_range(row['CHECK_DAY_NUMBER'], row['NUMBER'], detail_type)):
                        # 在得到的月份范围中逐月检查
                        # 找出当前月份所有检查问题记录
                        check_problems = check_problems_dict.get(months_ago - i, None)
                        if check_problems is None:
                            check_problems = cache_client.get('check_problem_and_address',
                                                              months_ago=months_ago - i)
                            check_problems = pd.merge(check_problems,
                                                      DEPARTMENT_DATA,
                                                      left_on='CHECK_DEPARTMENT_ID', right_on='DEPARTMENT_ID')
                            check_problems = pd.DataFrame(check_problems,
                                                          columns=['FK_CHECK_PROBLEM_ID', 'FK_PROBLEM_BASE_ID',
                                                                   'TYPE3', 'ADDRESS_TYPE', 'FK_CHECK_POINT_ID',
                                                                   'FK_DEPARTMENT_ID', 'CHECK_DEPARTMENT_ID'])
                            check_problems_dict[months_ago - i] = check_problems

                        if check_problems.empty:
                            continue
                        # 当前部门当前月份对某个地点所有的检查问题
                        _key = (department_id, months_ago-i, config_id)
                        checked_problems_ids = department_check_problem_dict.get(_key, None)
                        if checked_problems_ids is None:
                            checked_problems_ids = check_problems[
                                (check_problems['TYPE3'] == department_id) &
                                (check_problems[address_match_key] == row[address_match_key])
                                ]['FK_PROBLEM_BASE_ID'].values
                            department_check_problem_dict[_key] = checked_problems_ids

                        # 如果这个风险点配置对应的基础问题有一个被查到，就代表这个风险点被覆盖
                        if not check_item_and_problem[
                             check_item_and_problem['PROBLEM_BASE_ID'].isin(checked_problems_ids)].empty:
                            risk_control_checked = True
                            break
                    if not risk_control_checked:
                        address_fail_items.setdefault(address_name, []).append(row['RISK_CONTROL_NAME'])
                        failed_items_count += 1
                    else:
                        department_score += 1
                department_dict['ITEM_COUNT'] = department_dict.get('ITEM_COUNT', 0) + config_group.index.size
        department_dict['SCORE'] = department_score
        department_dict['FAILED_ITEMS_COUNT'] = failed_items_count

    rst_data = station_data.copy()

    append_df = rst_data.apply(lambda r: add_data_col(r, score_dict), result_type='expand', axis=1)
    rst_data = rst_data.join(append_df)
    rst_data['SCORE'] = rst_data.apply(lambda r: calc_score(r), axis=1)

    # 保存中间计算过程
    calc_basic_data_rst = format_export_basic_data(rst_data, MainType.key_control_quality,
                                                   detail_type,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.key_control_quality,
                                     detail_type,
                                     index_type=INDEX_TYPE)

    column = f'SCORE_{score_column}_{HIERARCHY}'

    rst_data = pd.DataFrame(index=rst_data['DEPARTMENT_ID'],
                            data=rst_data['SCORE'].values,
                            columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summarize_operation_set(rst_data, station_data, column,
                            HIERARCHY, INDEX_TYPE, MainType.key_control_quality,
                            detail_type,
                            months_ago)
    rst_index_score.append(rst_data[[column]])
    score_dict.clear()
    return rst_index_score


# 问题卡控
def stats_problem_control(months_ago):
    return _stats_problem_control(months_ago, 4, 'a', KeyControlQualityDetailType.stats_problem_control)


# 一般及以上问题卡控
def stats_general_risk_problem_control(months_ago):
    return _stats_problem_control(months_ago, 3, 'b', KeyControlQualityDetailType.stats_general_risk_problem_control)


# 关键问题卡控
def stats_key_problem_control(months_ago):
    return _stats_problem_control(months_ago, 4, 'c', KeyControlQualityDetailType.stats_key_problem_control, True)


def get_months_range(cycle, check_number, detail_type):
    """
    根据检查周期与天数获取应查找的月份范围
    :param detail_type:
    :param cycle:
    :param check_number:
    :return:
    """
    months_range = 1
    detail_type_multiple = {KeyControlQualityDetailType.stats_problem_control: 1,
                            KeyControlQualityDetailType.stats_general_risk_problem_control: 2,
                            KeyControlQualityDetailType.stats_key_problem_control: 3}
    if cycle == 1:
        months_range = 1
    elif cycle == 2:
        if check_number > 2:
            months_range = 1
        else:
            months_range = 2
    elif cycle == 3:
        if check_number > 3:
            months_range = 3
        else:
            months_range = 6
    elif cycle == 4:
        if check_number > 4:
            months_range = 6
        else:
            months_range = 12
    return months_range * detail_type_multiple[detail_type]


def handle(months_ago):
    # 部门按站段聚合
    init_common_data()
    _get_sql_data(months_ago)

    child_index_func = [
        stats_problem_control,
        stats_general_risk_problem_control,
        stats_key_problem_control
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.3, 0.35, 0.35]
    child_index_list = [KeyControlQualityDetailType.stats_problem_control,
                        KeyControlQualityDetailType.stats_general_risk_problem_control,
                        KeyControlQualityDetailType.stats_key_problem_control]
    update_major_maintype_weight(index_type=INDEX_TYPE, main_type=MainType.key_control_quality,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summarize_child_index(child_score, ZHANDUAN_DPID_DATA, INDEX_TYPE, MainType.key_control_quality, months_ago,
                          item_name, item_weight)
    current_app.logger.debug(
        '├── └── key_control_quality index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    execute(-1)
