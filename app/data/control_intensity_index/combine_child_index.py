#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""
import pandas as pd

from app import mongo
from app.data.control_intensity_index.common_data import init_common_data
from app.data.control_intensity_index import (item_check_cycle, control_frequency, total_control_quality,
                                              key_control_quality, key_problem_control, cache_client)
from app.data.control_intensity_index.const import MainType, INDEX_TYPE, HIERARCHY
from app.data.control_intensity_index.common import merge_all_child_item, append_major_column_to_df
from app.data.control_intensity_index.util import validate_exec_month
from app.data.util import (update_major_maintype_weight, pd_query, get_history_months, get_coll_prefix,
                           get_mongodb_prefix, write_bulk_mongo)


@validate_exec_month
def execute(months_ago, merge_only=True):
    init_common_data()
    if not merge_only:
        for func in [
            item_check_cycle, control_frequency, total_control_quality, key_control_quality, key_problem_control
        ]:
            func.execute(months_ago)
    child_index_list = [MainType.item_check_cycle,
                        MainType.control_frequency,
                        MainType.total_control_quality,
                        MainType.key_control_quality,
                        MainType.key_problem_control]
    child_index_weight = [0.15, 0.15, 0.25, 0.25, 0.20]
    update_major_maintype_weight(index_type=INDEX_TYPE,
                                 child_index_list=child_index_list, child_index_weight=child_index_weight)
    department_data = cache_client.get('ZHANDUAN_DPID_DATA', module_cache=True)
    merge_child_index(department_data,
                      months_ago,
                      child_index_list=child_index_list,
                      child_index_weight=child_index_weight)


def merge_child_index(department_data,
                      months_ago,
                      child_index_list,
                      child_index_weight):
    """将各个子指数加权求和

    Arguments:
        months_ago {int} -- 第前-N个月
    """
    year_mon = get_history_months(months_ago)[0]
    _prefix = get_coll_prefix(months_ago)
    coll_prefix = get_mongodb_prefix(INDEX_TYPE)
    coll_name = f'{_prefix}detail_{coll_prefix}_index'
    data = []
    # 获取子指数数据
    for main_type in child_index_list:
        for hierarchy in [HIERARCHY]:
            child_data = pd.DataFrame(
                list(mongo.db[coll_name].find({
                    "MAIN_TYPE": main_type,
                    "DETAIL_TYPE": 0,
                    "MON": year_mon,
                    'HIERARCHY': hierarchy,
                }, {
                    "_id": 0,
                    "SCORE": 1,
                    "DEPARTMENT_ID": 1,
                })))
            if child_data.empty is True:
                continue
            child_data = pd.DataFrame(
                index=child_data['DEPARTMENT_ID'],
                data=child_data.loc[:, 'SCORE'].values,
                columns=[f'SCORE_{main_type}_{hierarchy}'])
            data.append(child_data)
    item_name = [f'SCORE_{x}' for x in child_index_list]
    item_weight = child_index_weight
    for hierarchy in [HIERARCHY]:
        h_child_score = [
            x for x in data if x.columns.values[0][-1] == str(hierarchy)
        ]
        xdata = pd.concat(h_child_score, axis=1, sort=False)
        xdata.fillna(0, inplace=True)
        xdata['SCORE'] = xdata.apply(
            lambda row: merge_all_child_item(
                row, hierarchy, item_name, item_weight),
            axis=1)
        xdata = append_major_column_to_df(department_data, xdata)
        xdata['group_sort'] = xdata['SCORE'].groupby(xdata['MAJOR']).rank(
            ascending=0, method='first')
        xdata.dropna(inplace=True)
        rst = []
        for index, row in xdata.iterrows():
            rst.append({
                'MON': year_mon,
                'MAJOR': row['MAJOR'],
                'HIERARCHY': hierarchy,
                'DEPARTMENT_ID': row['DEPARTMENT_ID'],
                'DEPARTMENT_NAME': row['NAME'],
                'SCORE': round(row['SCORE'], 2),
                'RANK': int(row['group_sort'])
            })
        # 存入mongo
        coll_name = f'{_prefix}{coll_prefix}_index'
        mongo.db[coll_name].remove({
            'MON': year_mon,
            'HIERARCHY': hierarchy
        })
        write_bulk_mongo(coll_name, rst)


if __name__ == '__main__':
    pass
