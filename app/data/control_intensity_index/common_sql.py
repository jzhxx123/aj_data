#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""

# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.HIERARCHY
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
        AND b.DEPARTMENT_ID NOT IN ('19B8C3534DE85665E0539106C00A58FD' , 
                                        '19B8C3534E2C5665E0539106C00A58FD',
                                        '19B8C3534E33512345539106C00A58FD',
                                        '19B8C3534E3A566512345106C00A58FD');
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR, a.BELONG_PROFESSION_NAME
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND a.DEPARTMENT_ID NOT IN ('19B8C3534DE85665E0539106C00A58FD' , 
                                        '19B8C3534E2C5665E0539106C00A58FD',
                                        '19B8C3534E33512345539106C00A58FD',
                                        '19B8C3534E3A566512345106C00A58FD');
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
        LEFT JOIN
        t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0 AND b.IS_DELETE=0
    GROUP BY FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""


MAJOR_SQL = """SELECT 
    DEPARTMENT_ID, NAME AS MAJOR
FROM
    t_department
WHERE
    TYPE = 2 and NAME in ('车务', '客运', '机务', '供电', '工电', '工务', '车辆', '电务');"""


# 风险对应检查项目ID
RISK_AND_ITEMS_ID = """SELECT a.CHECK_ITEM_IDS as ITEM_IDS FROM `t_risk_statistics_config` a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
where b.`NAME` = '{0}'
AND FIND_IN_SET({1},a.PK_ID)"""