#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/29
Description: 
"""


CHECK_INFO_AND_ADDRESS_SQL = """SELECT 
    a.PK_ID, DATE(a.SUBMIT_TIME) AS SUBMIT_TIME, a.RISK_CONTROL_IDS,
    d.TYPE, d.FK_DEPARTMENT_ID, d.FK_CHECK_POINT_ID
FROM
    t_check_info a
        INNER JOIN
    t_check_info_and_person c ON c.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_address d ON d.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_department e ON e.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
        AND a.SUBMIT_TIME >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.SUBMIT_TIME <= DATE_FORMAT('{2}', '%%Y-%%m-%%d')
WHERE
    e.TYPE3 = '{0}'
"""