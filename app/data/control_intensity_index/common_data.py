#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""
import pandas as pd

from app.data.control_intensity_index import cache_client
from app.data.control_intensity_index.common_sql import DEPARTMENT_SQL, ZHANDUAN_DPID_SQL
from app.data.index.common import get_zhanduan_deparment
from app.data.util import pd_query


def get_check_address(department_data, station_department_data):
    # 检查地点配置站段归类
    check_address = cache_client.get('check_address_config')
    check_points = cache_client.get('check_point')
    check_address = pd.merge(check_address,
                             pd.DataFrame(department_data[department_data['TYPE'] == 9],
                                          columns=['DEPARTMENT_ID', 'TYPE3', 'NAME']),
                             left_on='FK_DEPARTMENT_ID', right_on='DEPARTMENT_ID',
                             how='left')
    check_points = check_points[(check_points['IS_DELETE'] == 0) & (check_points['TYPE'].isin((1, 2)))].copy()
    check_address.drop(
        check_address[(check_address['TYPE'] == 2)
                      & ~(check_address['FK_CHECK_POINT_ID']).isin(check_points['PK_ID'].tolist())].index,
        inplace=True)
    check_points.rename(columns={'PK_ID': 'FK_CHECK_POINT_ID', 'NAME': 'ADDRESS_NAME'}, inplace=True)
    check_address = pd.merge(check_address,
                             pd.DataFrame(check_points, columns=['FK_CHECK_POINT_ID', 'ADDRESS_NAME']),
                             how='left', left_on='FK_CHECK_POINT_ID', right_on='FK_CHECK_POINT_ID')
    check_address['TYPE3'] = check_address.apply(
        lambda _row: _row['TYPE3'] if not pd.isnull(_row['TYPE3']) else _row['FK_UNIT_ID'],
        axis=1)
    check_address.dropna(subset=['TYPE3'], inplace=True)
    check_address = check_address[check_address['TYPE3'].isin(station_department_data['DEPARTMENT_ID'].tolist())]
    check_address['CHECK_DAY_NUMBER'] = check_address['CHECK_DAY_NUMBER'].astype(int, errors='ignore')
    return check_address


def init_common_data():
    station_department_data = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    station_department_data = station_department_data[
        station_department_data['MAJOR'].isin(['车务', '客运', '机务', '供电', '工电', '工务', '车辆', '电务'])]
    department_data = pd_query(DEPARTMENT_SQL)
    check_address = get_check_address(department_data, station_department_data)

    cache_data = {
        'ZHANDUAN_DPID_DATA': station_department_data,
        'DEPARTMENT_DATA': department_data,
        'CHECK_ADDRESS': check_address
    }

    cache_client.set_all(cache_data)

