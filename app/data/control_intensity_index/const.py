#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description: 
"""

INDEX_TYPE = -3

HIERARCHY = 3


class MainType:
    """
    项目排查指数	item_check_cycle
    卡控频率指数	control_frequency
    总体卡控质量指数	total_control_quality
    关键点卡控质量指数	    key_control_quality
    关键问题卡控指数	    key_problem_control
    """
    item_check_cycle = 1
    control_frequency = 2
    total_control_quality = 3
    key_control_quality = 4
    key_problem_control = 5


class ItemCheckCycleDetailType:
    stats_item_check_cycle = 1


class ControlFrequencyDetailType:
    stats_control_frequency = 1


class TotalControlQualityDetailType:
    """
    一般及以上问题卡控: stats_general_risk_problem_control
    较大及以上问题卡控: stats_great_risk_problem_control
    关键问题卡控: stats_key_problem_control
    """
    stats_general_risk_problem_control = 1
    stats_great_risk_problem_control = 2
    stats_key_problem_control = 3


class KeyControlQualityDetailType:
    """
    问题卡控: stats_problem_control
    一般及以上问题卡控: stats_general_risk_problem_control
    关键问题卡控: stats_key_problem_control
    """
    stats_problem_control = 1
    stats_general_risk_problem_control = 2
    stats_key_problem_control = 3


class KeyProblemControlDetailType:
    """
    关键问题项点排查
    关键问题数加分
    """
    stats_key_problem_inspect = 1
    stats_key_problem_award = 2

