#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/10/21
Description:  项目排查指数
"""
import gc

import pandas as pd
from flask import current_app
from dateutil.relativedelta import SU, MO
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse
from dateutil import rrule

from app.data.control_intensity_index.common_data import init_common_data
from app.data.control_intensity_index.const import ItemCheckCycleDetailType, INDEX_TYPE, MainType, HIERARCHY
from app.data.index.util import get_custom_month
from app.data.util import update_major_maintype_weight
from app.data.index.common import (combine_child_index_func,)
from app.data.control_intensity_index.common import summarize_child_index, summarize_operation_set, \
    write_export_basic_data_to_mongo, format_export_basic_data, calc_unit_ratio
from app.data.control_intensity_index import cache_client
from app.data.control_intensity_index.item_check_cycle_sql import CHECK_ITEM_AND_ADDRESS_SQL
from app.data.control_intensity_index.util import pd_query, validate_exec_month


def _get_sql_data(months_ago):
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, CHECK_ADDRESS

    ZHANDUAN_DPID_DATA = cache_client.get('ZHANDUAN_DPID_DATA', True)
    CHEJIAN_DPID_DATA = cache_client.get('CHEJIAN_DPID_DATA', True)
    DEPARTMENT_DATA = cache_client.get('DEPARTMENT_DATA', True)
    CHECK_ADDRESS = cache_client.get('CHECK_ADDRESS', True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def get_children_items(pk_id, df, res):
    temp = df[df['PARENT_ID'].isin(pk_id)]

    if not temp.empty:
        res.extend(temp['PK_ID'].tolist())
        get_children_items(temp['PK_ID'].tolist(), df, res)
    return


def get_parent_item(pk_id, df):
    temp = df.loc[df['PK_ID'] == pk_id, ['PK_ID', 'PARENT_ID', 'HIERARCHY']]
    if temp.empty:
        return pd.DataFrame(columns=['PK_ID', 'PARENT_ID', 'HIERARCHY'])
    else:
        return get_parent_item(temp['PARENT_ID'].values[0], df).append(temp)


def add_data_col(row, score_dict):
    department_dict = score_dict.get(row['DEPARTMENT_ID'])
    content = '检查地点未按周期检查的项目：<br/>'
    if department_dict:
        score = department_dict['SCORE']
        item_count = department_dict['ITEM_COUNT']
        fail_item = department_dict['ADDRESS']
        formula_ratio_sum = department_dict.get('FORMULA_RATIO_SUM', 0)
        content += '<br/>'.join(['{}：{}'.format(k, '，'.join(v)) for k, v in fail_item.items()])
    else:
        score = 0
        item_count = 0
        formula_ratio_sum = 0
        content = ''
    return pd.Series([score, content, item_count, formula_ratio_sum],
                     index=['SCORE', 'CONTENT', 'ITEM_COUNT', 'FORMULA_RATIO_SUM'])


def calc_score(row):
    """
    排查重要加3a*频率系数 分、一般加a*频率系数 分
    a=100/（重要项目数*3*频率系数 + 一般项目数*1*频率系数）
    得分=∑加分
    :param row:
    :return:
    """
    if row['FORMULA_RATIO_SUM'] == 0:
        return 0
    point_a = 100 / row['FORMULA_RATIO_SUM']
    score = row['SCORE'] * point_a
    return min(score, 100)


def get_weeks_nums(start_day, end_day):
    """
    获取间隔之间的星期
    起始日期或者往前的最近的星期一为第一个星期一
    结束日期或者往前的最近的星期天为最后一个星期天
    :param start_day: 起始日期
    :param end_day:  结束日期
    :return:
    """
    first_mon = parse(start_day) + relativedelta(weekday=MO(-1))  # 一个月中的第一个星期一
    last_sun = parse(end_day) - relativedelta(days=1) + relativedelta(weekday=SU(-1))  # 一个月中的最后一个星期天
    # 一周为一个间隔，获取每个间隔的起始结束日期
    interval_days = list(
        rrule.rrule(rrule.WEEKLY, until=last_sun, dtstart=first_mon, byweekday=(MO, SU)))
    return interval_days


def get_cycle_convert_number(cycle, months_ago):
    """
    频率系数：‘周’为1，月为1/当月周数，季度为1/本月开始倒推3个月的自然周数，半年的为0
    :param months_ago:
    :param cycle:
    :return:
    """
    if cycle == 1:  # 周
        stats_month = get_custom_month(months_ago)
        start_day = stats_month[0]
        end_day = stats_month[1]
        return len(get_weeks_nums(start_day, end_day)) / 2, 1
    elif cycle == 2:  # 月
        stats_month = get_custom_month(months_ago)
        start_day = stats_month[0]
        end_day = stats_month[1]
        res = 1 / (len(get_weeks_nums(start_day, end_day)) / 2)
        return res, res
    elif cycle == 3:  # 季
        start_day = get_custom_month(months_ago - 3)[0]
        end_day = get_custom_month(months_ago)[1]
        res = 1 / (len(get_weeks_nums(start_day, end_day)) / 2)
        return res, res
    elif cycle == 4:  # 半年
        return 0, 0
    return 0, 0


def _stats_item_cycle_check(months_ago):
    """
    根据地点配置中检查项目检验是否符合检查周期
    检查项目处理
    配置的项目如果是二级，就匹配此项目下面所有的子项目
    如果是三级，就加入它的父项目也作为匹配项目
    :param months_ago:
    :return:
    """
    rst_index_score = []
    station_data = ZHANDUAN_DPID_DATA.copy()
    stats_month = get_custom_month(months_ago)
    start_day = stats_month[0]
    end_day = stats_month[1]
    check_items = cache_client.get('check_items')
    score_dict = {}

    check_items = check_items[(check_items['IS_DELETE'] == 0)].copy()
    # 排查重要加3a分、一般加a分
    check_items['SCORE_RATE'] = 1
    check_items['TYPE'].fillna('', inplace=True)
    check_items.loc[check_items['TYPE'].str.contains('5'), 'SCORE_RATE'] = 3
    # 检查地点配置站段归类
    check_address = CHECK_ADDRESS

    children_item_cache = {}
    ancestors_item_cache = {}

    for department_id, department_group in check_address.groupby(['TYPE3']):
        # if department_id not in (#'19B8C3534E035665E0539106C00A58FD',
        #                          '19B9D8D920DD589FE0539106C00A1189',
        #                          # '19B8C3534E095665E0539106C00A58FD',
        #                          # '19B8C3534E285665E0539106C00A58FD'
        #                          ):
        #     continue
        department_group = department_group.sort_values(['CHECK_DAY_NUMBER'], ascending=False)
        department_records_dict = {}
        department_dict = score_dict.setdefault(department_id, {})
        address_fail_items = department_dict.setdefault('ADDRESS', {})
        for cycle, cycle_group in department_group.groupby(['CHECK_DAY_NUMBER'], sort=False):
            check_records = department_records_dict.setdefault(cycle, get_check_records(department_id, cycle,
                                                                                        department_records_dict,
                                                                                        months_ago))
            cycle_convert_number, score_cycle_convert_number = get_cycle_convert_number(
                cycle, months_ago)  # 频率换算系数, 计分频率换算系数

            for _, row in cycle_group.iterrows():  # 遍历每个地点
                check_number_per = row.get('NUMBER', 1)
                split_items = [int(x) for x in row['CHECK_ITEM_IDS'].split(',') if x.isdigit()]  # 地点配置中的检查项目
                items_df = check_items[check_items['PK_ID'].isin(split_items)][
                    ['PK_ID', 'HIERARCHY', 'TYPE', 'SCORE_RATE']]

                """
                排查重要加3a*频率系数 分、一般加a*频率系数 分
                a=100/Σ（重要项目数*3*频率系数 + 一般项目数*1*频率系数）
                这里formula_ratio_sum算的是a值公式分母
                """
                address_item_count = items_df.index.size
                formula_ratio_sum = (items_df.index.size + items_df[items_df['SCORE_RATE'] == 3].index.size * 2)
                formula_ratio_sum = formula_ratio_sum * cycle_convert_number

                # 1: 地点，对应FK_DEPARTMENT_ID 2： 重要检查点，对应FK_CHECK_POINT_ID
                if row['TYPE'] == 1:
                    address_match_key = 'FK_DEPARTMENT_ID'
                    address_name = row['NAME']
                else:
                    address_match_key = 'FK_CHECK_POINT_ID'
                    address_name = row['ADDRESS_NAME']
                not_checked_items = set()  # 未检查的项目
                score = 0
                if cycle == 1:
                    # 周
                    # 一周为一个间隔，获取每个间隔的起始结束日期
                    interval_days = get_weeks_nums(start_day, end_day)
                    interval_list = [interval_days[i: i + 2] for i in range(0, len(interval_days), 2)]

                    for interval in interval_list:
                        records = check_records[(check_records['SUBMIT_TIME'].between(interval[0], interval[1]))
                                                & (check_records[address_match_key] == row[address_match_key])]
                        for _, item in items_df.iterrows():
                            # if item['PK_ID'] in not_checked_items:
                            #     continue
                            item_id = item['PK_ID']
                            children = children_item_cache.get(item_id, None)
                            if children is None:
                                children = children_item_cache.setdefault(item_id, [item_id])
                                get_children_items([item_id], check_items, children)

                            ancestors = ancestors_item_cache.get(item_id, None)
                            if ancestors is None:
                                ancestors = get_parent_item(item_id, check_items)['PK_ID'].values.tolist()
                                ancestors_item_cache[item_id] = ancestors

                            if len(records[records['FK_CHECK_ITEM_ID'].isin(children + ancestors)].groupby(
                                    ['FK_CHECK_ITEM_ID', 'SUBMIT_TIME']).groups) < check_number_per:
                                not_checked_items.add(item_id)
                            else:
                                score += item['SCORE_RATE'] * score_cycle_convert_number
                elif cycle in (2, 3, 4):
                    # 月，季度，半年
                    records = check_records[(check_records[address_match_key] == row[address_match_key])]
                    for _, item in items_df.iterrows():
                        # if item['PK_ID'] in not_checked_items:
                        #     continue
                        item_id = item['PK_ID']
                        children = children_item_cache.get(item_id, None)
                        if children is None:
                            children = children_item_cache.setdefault(item_id, [item_id])
                            get_children_items([item_id], check_items, children)

                        ancestors = ancestors_item_cache.get(item_id, None)
                        if ancestors is None:
                            ancestors = get_parent_item(item_id, check_items)['PK_ID'].values.tolist()
                            ancestors_item_cache[item_id] = ancestors

                        if len(records[records['FK_CHECK_ITEM_ID'].isin(children + ancestors)].groupby(
                                ['FK_CHECK_ITEM_ID', 'SUBMIT_TIME']).groups) < check_number_per:
                            not_checked_items.add(item_id)
                        else:
                            score += item['SCORE_RATE'] * score_cycle_convert_number
                else:
                    pass

                not_checked_items_df = check_items[check_items['PK_ID'].isin(not_checked_items)]
                department_dict['SCORE'] = department_dict.get('SCORE', 0) + score
                department_dict['ITEM_COUNT'] = department_dict.get('ITEM_COUNT', 0) + address_item_count
                department_dict['FORMULA_RATIO_SUM'] = department_dict.get('FORMULA_RATIO_SUM', 0) + formula_ratio_sum
                if not not_checked_items_df.empty:
                    address_fail_items[address_name] = not_checked_items_df['NAME'].tolist()

        department_records_dict.clear()

    rst_data = station_data.copy()

    append_df = rst_data.apply(lambda r: add_data_col(r, score_dict), result_type='expand', axis=1)
    rst_data = rst_data.join(append_df)
    rst_data['SCORE'] = rst_data.apply(lambda r: calc_score(r), axis=1)

    # 保存中间计算过程
    calc_basic_data_rst = format_export_basic_data(rst_data, MainType.item_check_cycle,
                                                   ItemCheckCycleDetailType.stats_item_check_cycle,
                                                   HIERARCHY, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, HIERARCHY, MainType.item_check_cycle,
                                     ItemCheckCycleDetailType.stats_item_check_cycle,
                                     index_type=INDEX_TYPE)

    column = f'SCORE_a_{HIERARCHY}'

    rst_data = pd.DataFrame(index=rst_data['DEPARTMENT_ID'],
                            data=rst_data['SCORE'].values,
                            columns=[column])

    # 将最终的各单位的得分进行排名入库等一系列操作
    summarize_operation_set(rst_data, station_data, column,
                            HIERARCHY, INDEX_TYPE, MainType.item_check_cycle,
                            ItemCheckCycleDetailType.stats_item_check_cycle,
                            months_ago)
    rst_index_score.append(rst_data[[column]])
    score_dict.clear()
    children_item_cache.clear()
    return rst_index_score


def get_check_records(department_id, cycle, department_records_dict, months_ago):
    stats_month = get_custom_month(months_ago)
    start_day = stats_month[0]
    end_day = stats_month[1]
    if cycle == 1:
        # 周
        # 从最后一个星期天开始，直到包含开始日期的那个星期
        first_mon = parse(start_day) + relativedelta(weekday=MO(-1))
        last_sun = parse(end_day) - relativedelta(days=1) + relativedelta(weekday=SU(-1))
        check_records = pd_query(CHECK_ITEM_AND_ADDRESS_SQL.format(department_id,
                                                                   first_mon.strftime('%Y-%m-%d'),
                                                                   last_sun.strftime('%Y-%m-%d')),
                                 parse_dates=['SUBMIT_TIME'])
        return check_records
    elif cycle == 2:
        # 月
        # 直接查询统计月份
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        for key in (3, 4):
            more_records = department_records_dict.get(key)
            if more_records is not None:
                check_records = more_records[more_records['SUBMIT_TIME'].between(parse(start_day), parse(_end_day))]
                return check_records
        check_records = pd_query(CHECK_ITEM_AND_ADDRESS_SQL.format(department_id, start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])
        return check_records
    elif cycle == 3:
        # 季度
        # 反推3个月
        _start_day = get_custom_month(months_ago - 2)[0]
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        for key in (4,):
            more_records = department_records_dict.get(key)
            if more_records is not None:
                check_records = more_records[more_records['SUBMIT_TIME'].between(parse(_start_day), parse(_end_day))]
                return check_records
        check_records = pd_query(CHECK_ITEM_AND_ADDRESS_SQL.format(department_id, _start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])
        return check_records
    elif cycle == 4:
        # 半年
        # 反推6个月
        _start_day = get_custom_month(months_ago - 5)[0]
        _end_day = (parse(end_day) - relativedelta(days=1)).strftime('%Y-%m-%d')
        check_records = pd_query(CHECK_ITEM_AND_ADDRESS_SQL.format(department_id, _start_day, _end_day),
                                 parse_dates=['SUBMIT_TIME'])
        return check_records
    else:
        return None


def handle(months_ago):
    # 部门按站段聚合
    init_common_data()
    _get_sql_data(months_ago)

    child_index_func = [
        _stats_item_cycle_check
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)

    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    child_index_list = [ItemCheckCycleDetailType.stats_item_check_cycle]
    update_major_maintype_weight(index_type=INDEX_TYPE, main_type=MainType.item_check_cycle,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summarize_child_index(child_score, ZHANDUAN_DPID_DATA, INDEX_TYPE, MainType.item_check_cycle, months_ago,
                          item_name, item_weight)
    current_app.logger.debug(
        '├── └── item_check_cycle index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    execute(-1)
