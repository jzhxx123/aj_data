#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019-06-03
-------------------------------------------------
Change Activity:2019-06-03 18:52
-------------------------------------------------
"""

from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, CHECK_EVALUATE_SZ_SCORE_SQL, REPEATE_HAPPEN_PROBLEM_SQL,
    MAJOR_PROBLEM_POINT_INFO_SQL)