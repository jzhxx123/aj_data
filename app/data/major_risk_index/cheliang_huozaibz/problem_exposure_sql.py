#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_exposure_sql
Description:
Author:    
date:         2019-06-03
-------------------------------------------------
Change Activity:2019-06-03 17:24
-------------------------------------------------
"""

from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import(
    CHECK_PROBLEM_SQL,EXPOSURE_PROBLEM_DEPARTMENT_SQL, OTHER_CHECK_PROBLEM_SQL,
    SELF_CHECK_PROBLEM_SQL,
    HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL)