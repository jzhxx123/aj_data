# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.cheliang_huozaibz import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.cheliang_huozaibz.common_sql import (
    WORK_LOAD_SQL, KECHE_PEISHULIANG_COUNT_SQL)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.cheliang_huozaibz.check_intensity_sql import (
    REAL_CHECK_BANZU_SQLIST, RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, CHECK_COUNT_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, 
    BANZU_POINT_SQL
)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, \
        ALL_PROBLEM_NUMBER, REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA,\
        WORKER_COUNT, JIAODA_RISK_SCORE, XC_JIAODA_RISK_SCORE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    global RISK_IDS
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    # month = int(stats_month[1][5:7])
    RISK_IDS = risk_ids

    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = GLV.get_value("ZHANDUAN_STAFF")
    # 单位总人数
    WORKER_COUNT = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 客车工作量以各段客车配属量计算
    WORK_LOAD = df_merge_with_dpid(pd_query(
        KECHE_PEISHULIANG_COUNT_SQL.format(major)),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)

    REAL_CHECK_BANZU_DATA = pd.merge(
        pd_query(REAL_CHECK_BANZU_SQLIST[0].format(major)),
        pd_query(REAL_CHECK_BANZU_SQLIST[1].format(
            *stats_month, check_item_ids)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )
    REAL_CHECK_BANZU_DATA.drop(["PK_ID"], inplace=True, axis=1)
    # 删除重复部门
    REAL_CHECK_BANZU_DATA.drop_duplicates(
        subset='FK_DEPARTMENT_ID', keep='first', inplace=True)
    REAL_CHECK_BANZU_DATA['COUNT'] = 1
    BANZU_POINT_DATA = pd_query(BANZU_POINT_SQL.format(risk_ids))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 换算单位检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 换算单位查处问题率
def _stats_check_problem_ratio(months_ago):
    fraction = GLV.get_value('stats_check_problem_ratio', (None,))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        ALL_PROBLEM_NUMBER,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均质量分({3}) = " + \
                        "问题质量分累计({4})/ 总人数({5})*100%</p>"
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORKER_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        CHECK_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    title=[
        '较大和重大安全风险问题质量分累计({0})/总人数({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/总人数({1})'
    ]
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORKER_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month,RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_month, RISK_IDS)
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORKER_COUNT,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
         _stats_score_per_person,
        _stats_yecha_ratio, _stats_risk_score_per_person,
        _stats_media_intensity, _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'g', 'j', 'i']
    ]
    item_weight = [0.25, 0.10, 0.25, 0.15, 0.07, 0.15, 0.03]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 6, 7, 10, 9],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd', 'f', 'h', 'i']]
    # item_weight = [0.25, 0.2, 0.08, 0.25, 0.9, 0.13]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 1, months_ago,
    #                      item_name, item_weight, [4])
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
