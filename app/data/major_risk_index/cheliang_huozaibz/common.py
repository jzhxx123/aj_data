#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-06-03
-------------------------------------------------
Change Activity:2019-06-03 14:44
-------------------------------------------------
"""

from app.data.util import pd_query

DEPARTMENT_DPIDS = ('19B8C3534E0A5665E0539106C00A58FD',
        '19B8C3534E185665E0539106C00A58FD',
        '19B8C3534E1F5665E0539106C00A58FD')

def retrieve_all_dp_configid(top_ids):
    """
    获取所有部门配置id
    """
    # 查询所属的子check_item的PK_ID
    load_child_check_item_sql = "SELECT PK_ID FROM t_department_classify_config WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret


def retrieve_all_check_items_ids():
    """
    获取所有部门配置id
    """
    # 查询所属的子check_item的PK_ID
    # 车辆-客车运用作业 1049、车辆-客车检修作业 1050
    top_ids = '1049, 1050'
    load_child_check_item_sql = "select * from t_check_item WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret


def retrieve_all_risk_ids(top_ids):
    """
    获取所有部门配置id
    """
    # 查询所属的子check_item的PK_ID
    # 火灾爆炸-罐车火灾隐患 522
    load_child_check_item_sql = "select * from t_risk WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret
