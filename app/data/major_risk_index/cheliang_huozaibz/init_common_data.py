#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.cheliang_huozaibz import GLV
from app.data.major_risk_index.cheliang_huozaibz.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.cheliang_huozaibz.common import DEPARTMENT_DPIDS

def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    major = get_major_dpid(risk_type)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, DEPARTMENT_DPIDS))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, DEPARTMENT_DPIDS))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, DEPARTMENT_DPIDS))
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major, DEPARTMENT_DPIDS))
    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "ZHANDUAN_STAFF": ZHANDUAN_STAFF,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
