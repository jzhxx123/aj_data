from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, CHECK_EVALUATE_SZ_SCORE_SQL,
    REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL
)