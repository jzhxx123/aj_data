from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, NOITEM_CHECK_COUNT_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, NOITEM_YECHA_CHECK_SQL, NOITEM_MEDIA_COST_TIME_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    BANZU_POINT_SQL)


# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct( if(d.TYPE !=9, d.FK_PARENT_ID,b.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE !=''
"""

# 具有调阅属性的班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct (if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
         b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]
