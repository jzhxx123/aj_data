# -*- coding: utf-8 -*-

import pandas as pd

def calc_night_work_banzu_count(banzu_info, dp_id):
    """[计算夜间作业班组数]
    夜间工作时间22:00 - 06:00，开区间，不包含两个端点时间
    WORK_TYPE为1，表示日勤制，早八晚五。(为空默认日勤)
    2:表示24小时制
    3:表示自定义时间间隔，两小时一个区间,其中4，5，6，15算夜间
    Arguments:
        banzu_info {df} -- [班组工作配置信息]
        dp_id {[df]} -- [部门信息]
    """
    banzu_info['COUNT'] = 0
    night_work_time_by3 = {4, 5, 6, 15}
    for idx, row in banzu_info.iterrows():
        if pd.isnull(row['WORK_TYPE']):
            continue
        elif int(row['WORK_TYPE']) == 1:
            continue
        elif int(row['WORK_TYPE']) == 2:
            banzu_info.loc[idx, 'COUNT'] = 1
        elif int(row['WORK_TYPE']) != 2:
            if pd.isnull(row['WORK_TIME']):
                continue
            else:
                work_time_list = [ int(item) for item in row['WORK_TIME'].split(',')  if item.isdigit() ]
                work_time_set = set(work_time_list)
                intersection = night_work_time_by3 & work_time_set
                if not intersection:
                    continue
                banzu_info.loc[idx,'COUNT'] = 1
            
    data = pd.merge(
        banzu_info,
        dp_id,
        how='inner',
        left_on='FK_DEPARTMENT_ID', 
        right_on='DEPARTMENT_ID'
    )
    data.drop(["FK_DEPARTMENT_ID", "WORK_TYPE", "WORK_TIME"], inplace=True, axis=1)
    return data
