# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd
from app.data.major_risk_index.cheliang_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.cheliang_laoan.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL,
    EXTERNAL_PERSON_SQL, BANZU_INFO_SQL)
from app.data.major_risk_index.cheliang_laoan.check_intensity_sql import(
    WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL,
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, NOITEM_CHECK_COUNT_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, NOITEM_YECHA_CHECK_SQL, NOITEM_MEDIA_COST_TIME_SQL,
    BANZU_POINT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_check_address_standard_data)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.cheliang_laoan.common import calc_night_work_banzu_count


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    major = get_major_dpid(risk_type)
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, \
        PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, CHECK_PROBLEM_COUNT, \
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, REAL_CHECK_BANZU_DATA, \
        REAL_CHECK_POINT_DATA, BANZU_POINT_DATA, CHECK_POINT_DATA, \
        YEJIAN_WORK_BANZU_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    global RISK_IDS, CHECK_ITEM_IDS
    RISK_IDS, CHECK_ITEM_IDS = risk_ids, check_item_ids
    # 统计工作量【职工总人数】
    month = int(stats_month[1][5:7])

    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major))

    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_CHECK_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 劳动项问题数
    CHECK_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_YECHA_CHECK_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 夜间作业班组数
    YEJIAN_WORK_BANZU_COUNT = calc_night_work_banzu_count(
        pd_query(BANZU_INFO_SQL.format(major, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)

    # 监控调阅覆盖率
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [
        WATCH_MEDIA_BANZU_COUNT_SQL.format(*stats_month),
        WORK_BANZU_COUNT_SQL
    ]

    # 工作班组信息
    work_banzu_info_data = pd_query(BANZU_POINT_SQL.format(major))
    work_banzu_info_data.drop(['COUNT'], inplace=True, axis=1)

    check_address_ratio_data = get_check_address_standard_data(
        work_banzu_info_data, DEPARTMENT_DATA, months_ago, check_item_ids, major, is_base_item=False)
    REAL_CHECK_BANZU_DATA, REAL_CHECK_POINT_DATA, BANZU_POINT_DATA, CHECK_POINT_DATA = check_address_ratio_data

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = " + \
                       "现场检查次数({4})/ 总人数({5})</p>"
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = " + \
                       "劳动安全项问题数({4})/ 总人数({5})</p>"
    fraction = GLV.get_value('stats_check_problem_ratio', (None,))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        CHECK_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = " + \
                       "问题质量分累计({4})/ 总人数({5})</p>"
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                       "夜查次数({4})/ 夜间作业班组数({5})*100%</p>"
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        YEJIAN_WORK_BANZU_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = NOITEM_MEDIA_COST_TIME_SQL.format(
        *stats_month, CHECK_ITEM_IDS)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(
        *stats_month, RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(
        *stats_month, RISK_IDS)
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0],
        fraction_list[1],
        fraction_list[2],
        None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = " + \
                       "现场检查地点数({4})/ 地点总数（班组及重要检查点统计）({5})*100%</p>"
    return check_intensity.stats_check_address_ratio_include_check_point_excellent(
        REAL_CHECK_BANZU_DATA,
        REAL_CHECK_POINT_DATA,
        BANZU_POINT_DATA,
        CHECK_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        calc_func=lambda x: min(x * 100, 100),
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_person, _stats_yecha_ratio,
        _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']
    ]
    item_weight = [0.25, 0.10, 0.25, 0.15, 0.15, 0.07, 0.03]
    update_major_maintype_weight(index_type=2, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 6, 10, 7, 9],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
