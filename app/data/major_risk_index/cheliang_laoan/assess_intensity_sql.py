#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   assess_intensity_sql
Description:
Author:    
date:         2019-05-22
-------------------------------------------------
Change Activity:2019-05-22 16:43
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import (
    KAOHE_PROBLEM_SQL, LEVEL_AWARD_RETURN_SQL, NORISK_ASSESS_RESPONSIBLE_SQL,
    AWARD_RETURN_PROBLEM_SQL, REAL_AWARD_RETURN_PROBLEM_SQL)
