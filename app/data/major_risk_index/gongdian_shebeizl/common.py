# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/9/3上午10:06
   Change Activity: 2019/9/3上午10:06
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import df_merge_with_dpid, calc_extra_child_score_groupby_major, \
    calc_extra_child_score_groupby_major_third, append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 50
        _score = 0 if _score < 0 else _score
    return _score


def _calc_equipment_trip_score_by_formula(row, column, major_column, detail_type=None):
    """设备跳闸无数据,甲方要求100分"""
    _score = 60
    if row[major_column] == 0 or row[column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 50
        _score = 0 if _score < 0 else _score
    return _score


def _calc_peril_count_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio > 0:
        _score = 100
    elif 0 >= _ratio > -0.5:
        _score = 70
    elif -0.5 >= _ratio > -0.6:
        _score = 50
    elif 0.6 >= _ratio > -0.8:
        _score = 30
    else:
        _score = 0
    return _score


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER')


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


def stats_total_problem_exposure(check_item_ids, check_problem_sql, work_load,
                                 department_data, months_ago, risk_type,
                                 choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]

    weight_item = [0.5, 0.5]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '总问题数({0})/本单位换算设备数量({1})', '一般及以上问题数({0})/本单位换算设备数量({1})']
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in [3]:
        score = []
        for i, data in enumerate(
                [base_data, risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load, department_data,
                         choose_dpid_data, weight, hierarchy))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_trips_score_major(df, work_load, choose_dpid_data, hierarchy, idx, zhanduan_filter_list=None,
                            calc_score_by_formula=_calc_score_by_formula, title=None):
    df = df.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    df = df.to_frame(name='numerator')
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='denominator')
    data = pd.concat(
        [df, work_load], axis=1, sort=False)
    data['ratio'] = data.apply(
        lambda row: (row['numerator'] / row['denominator'])
        if row['denominator'] > 0 else 0, axis=1)
    data.fillna(0, inplace=True)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        zhanduan_filter_list=zhanduan_filter_list)
    # 中间计算数据
    if not title:
        title = [
            '馈线跳闸次数({0})/本单位设备换算数量({1})',
            '馈线跳闸次数({0})/本单位牵引供电量({1})'
        ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["numerator"], 2)}', round(row['denominator'], 2)),
        axis=1)
    data.drop(
        columns=['numerator', 'ratio', 'denominator'], inplace=True, axis=1)
    return rst_data, data


# 设备跳闸管控力度
def stats_equipment_trip_intensity(
        months_ago,
        risk_type,
        df_list=None,
        work_load_list=None,
        choose_dpid_data=None,
        child_weight=None,
        zhanduan_filter_list=None,
        title=None
):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    if not df_list or not work_load_list:
        return
    if not child_weight:
        child_weight = [0.5, 0.5]
    score = []
    for idx, df in enumerate(df_list):
        rst_func, rst_basic_data = _calc_trips_score_major(df, work_load_list[idx],
                                                           choose_dpid_data, 3, idx, zhanduan_filter_list,
                                                           calc_score_by_formula=_calc_equipment_trip_score_by_formula,
                                                           title=title)
        calc_basic_data.append(rst_basic_data)
        if rst_func is not None:
            score.append(rst_func * child_weight[idx])
    # 保存导出中间计算数据到mongo
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join(
            [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 10, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 10, risk_type=risk_type)

    # 合并计算子指数
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = 'SCORE_f_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        column,
        3,
        2,
        5,
        10,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


# 隐患整治 - 缺陷库
def _peril_count_score(peril_count, work_load, department_data, hierarchy,
                       choose_dpid_data, peril_content=None):
    """缺陷库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    if not peril_content:
        peril_content = '缺陷库(40%):<br/>缺陷总数量({0:.0f})/单位总人数({1:.0f})'

    peril_count = df_merge_with_dpid(peril_count, department_data, how='right')
    peril_count.fillna(0, inplace=True)
    peril_count = peril_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [peril_count.to_frame(), work_load], axis=1, sort=False)
    data['ratio'] = data['COUNT'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_peril_count_score_by_formula)
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: peril_content.format(
            row["COUNT"], row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['COUNT', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 隐患整治 - 缺陷库缺陷整治超期
def _peril_overdue(peril_overdue_count, department_data, hierarchy, peril_count_score):
    """1条超期扣2分。（设备缺陷库得分低于70分的，该项分值=（100-∑扣分）*缺陷库得分/100；高于等于70分的不乘系数）
    """
    peril_overdue_data = df_merge_with_dpid(peril_overdue_count, department_data, how='right')
    peril_overdue_data.fillna(0, inplace=True)
    data = peril_overdue_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum().to_frame()
    if data.empty:
        return None, None
    series_rst = data.apply(lambda row: min(100 - sum(row) * 2, 100) if 100 - sum(row) * 2 > 0
    else 0, axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '缺陷整治超期(20%):缺陷超期数量：({0:.0f})'.format(
            row['COUNT']),
        axis=1)
    data.drop(columns=['COUNT'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治督促
def _peril_urge(checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql, department_data, zhanduan_dpid_data,
                months_ago, peril_count_score, check_risk_ids):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    calc_month = get_custom_month(months_ago)
    # 检查出的的隐患
    checked_peril_id = df_merge_with_dpid(
        pd_query(checked_peril_id_sql.format(*calc_month, check_risk_ids)),
        zhanduan_dpid_data)
    # 隐患库里的隐患
    peril_id = df_merge_with_dpid(
        pd_query(peril_id_sql.format(check_risk_ids)),
        zhanduan_dpid_data)
    peril_id = peril_id.append(checked_peril_id)
    peril_id.drop_duplicates(subset=['DEPARTMENT_ID', 'PID'], keep=False, inplace=True)

    # 长期整治没录入的
    peril_rectify_no_entry = df_merge_with_dpid(
        pd_query(peril_rectify_no_entry_sql.format(*calc_month)),
        department_data)

    data = pd.concat(
        [
            peril_id.groupby(['DEPARTMENT_ID'])['PID'].size(),
            peril_rectify_no_entry.groupby(['DEPARTMENT_ID'
                                            ])['COUNT'].sum()
        ],
        axis=1,
        sort=False)
    data.fillna(0, inplace=True)
    series_rst = data.apply(lambda row: min(100 - sum(row) * 2, 100) if 100 - sum(row) * 2 > 0
    else 0, axis=1)
    data['middle_3'] = data.apply(
        lambda row: '隐患整治督促(40%):<br/>隐患未检查数量:({0:.0f}),长期整治的没录入数量:({1:.0f})'.format(
            row['PID'], row['COUNT']),
        axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.drop(columns=['PID', 'COUNT'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治
def stats_peril_renovation(
        defects_count,
        defects_overdue_count,
        checked_peril_id_sql,
        peril_id_sql,
        peril_rectify_no_entry_sql,
        staff_number,
        department_data,
        zhanduan_dpid_data,
        months_ago,
        risk_type,
        choose_dpid_data,
        check_risk_ids,
        peril_content=None
):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    score = []

    # 缺陷库
    peril_count_score, peril_count_score_basic_data = _peril_count_score(
        defects_count, staff_number, department_data, 3,
        choose_dpid_data, peril_content=peril_content)
    score.append(peril_count_score * 0.4)
    calc_basic_data.append(peril_count_score_basic_data)

    # 缺陷库缺陷整治超期
    peril_overdue_socre, peril_overdue_score_basic_data = _peril_overdue(
        defects_overdue_count, department_data, 3, peril_count_score)
    if peril_overdue_socre is not None:
        score.append(peril_overdue_socre * 0.2)
        calc_basic_data.append(peril_overdue_score_basic_data)

    # 隐患整治督促
    peril_urge_score, peril_urge_score_basic_data = _peril_urge(
        checked_peril_id_sql, peril_id_sql,
        peril_rectify_no_entry_sql,
        department_data,
        zhanduan_dpid_data,
        months_ago,
        peril_count_score, check_risk_ids)
    score.append(peril_urge_score * 0.4)
    calc_basic_data.append(peril_urge_score_basic_data)

    # 保存导出中间计算数据到mongo
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 6, 5, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 6, 5, risk_type=risk_type)
    # 合并计算子指数
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = 'SCORE_e_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        column,
        3,
        2,
        6,
        5,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def get_overdue_equipment_count(equipment_count, overdue_count, department_data):
    """
    获取未超期设备数量
    :param department_data: 部门数据
    :param equipment_count: 所有设备数量
    :param overdue_count:  超期设备数量
    :return:
    """
    data = pd.merge(
        equipment_count,
        overdue_count,
        how='outer',
        on='FK_DEPARTMENT_ID',
    )
    data.fillna(0, inplace=True)
    data['COUNT'] = data.apply(
        lambda row: (row['ALL_COUNT'] - row['OVERDUE_COUNT'])
        if row['ALL_COUNT'] > row['OVERDUE_COUNT'] else 0, axis=1
    )
    data = data[['FK_DEPARTMENT_ID', 'COUNT']]
    data = df_merge_with_dpid(data, department_data)
    return data
