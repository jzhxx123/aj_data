# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.problem_exposure_sql import (
    EXPOSURE_PROBLEM_DEPARTMENT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import \
    EX_CHECKED_HIDDEN_PROBLEM_POINT_SQL, EX_HIDDEN_PROBLEM_POINT_SQL, \
    HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL
from app.data.major_risk_index.common_sql import (
    WORK_LOAD_SQL)
from app.data.major_risk_index.gongdian_shebeizl import GLV
from app.data.major_risk_index.gongdian_shebeizl.common import stats_total_problem_exposure, \
    stats_equipment_trip_intensity, get_vitual_major_ids, get_overdue_equipment_count
from app.data.major_risk_index.gongdian_shebeizl.problem_exposure_sql import CHECK_PROBLEM_SQL, SELF_CHECK_PROBLEM_SQL, \
    OTHER_CHECK_PROBLEM_SQL, SAFETY_PRODUCE_INFO_SQL, OVERDUE_EQUIPMENT_COUNT_SQL, FEEDER_TRIPS_COUNT_SQL, \
    TRACTION_POWER_SUPPLY_COUNT_SQL, ALL_EQUIPMENT_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide_major)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA, EQUIPMENT_COUNT, \
        NOT_OVERDUE_EQUIPMENT_COUNT, FEEDER_TRIPS_COUNT, TRACTION_POWER_SUPPLY_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    # 换算单位设备数量
    EQUIPMENT_COUNT = GLV.get_value('EQUIPMENT_COUNT')

    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    # 查出较严重隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(EX_CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 单位应检查问题项点
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(EX_HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)),
        DEPARTMENT_DATA)

    # 未超期设备数量
    all_equipment_count = pd_query(ALL_EQUIPMENT_COUNT_SQL)
    overdue_equipment_count = pd_query(OVERDUE_EQUIPMENT_COUNT_SQL.format(month))
    NOT_OVERDUE_EQUIPMENT_COUNT = get_overdue_equipment_count(
        all_equipment_count, overdue_equipment_count, DEPARTMENT_DATA)

    # 馈线跳闸次数
    FEEDER_TRIPS_COUNT = df_merge_with_dpid(
        pd_query(FEEDER_TRIPS_COUNT_SQL.format(major, ids)),
        DEPARTMENT_DATA)

    # 本单位牵引供电量
    TRACTION_POWER_SUPPLY_COUNT = df_merge_with_dpid(
        pd_query(TRACTION_POWER_SUPPLY_COUNT_SQL.format(major, ids)),
        DEPARTMENT_DATA)


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """设备质量问题数（质量分）/本单位换算设备数量
    一般及以上设备质量问题数（质量分）/本单位换算设备数量"""
    equipment_count = EQUIPMENT_COUNT.groupby(['TYPE3'])['COUNT'].sum()
    equipment_count = equipment_count.to_frame(name='PERSON_NUMBER')
    return stats_total_problem_exposure(
        RISK_IDS, CHECK_PROBLEM_SQL, equipment_count, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 较严重隐患暴露度
def _stats_hidden_problem_exposure(months_ago):
    """其它单位查出的问题项点(较大、重大安全风险，且本单位基础问题库中有的),本单位未查出的按1分/项扣"""
    title = '本月问题个数: {1}个'
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA,
        DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, title=title)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    customizededuct = {
        4: 2,
        5: 4,
        6: 4
    }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct, months=7)



# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_ITEM_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return problem_exposure.stats_other_problem_exposure(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def _stats_overdue_equipment_intensity(months_ago):
    """未超期设备（未超过检修周期的30%）/本单位换算设备数量,专业基数比较"""
    return calc_child_index_type_divide_major(
        NOT_OVERDUE_EQUIPMENT_COUNT,
        EQUIPMENT_COUNT,
        2,
        5,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
    )


# TODO 设备跳闸管控力度
def _stats_equipment_trip_intensity(months_ago):
    """馈线跳闸次数（生产管理系统采集）/本单位设备换算数量*50%
    +馈线跳闸次数（生产管理系统采集）/本单位牵引供电量（生产管理系统采集）*50%，分别与专业基数比较"""
    df_list = [FEEDER_TRIPS_COUNT, FEEDER_TRIPS_COUNT]
    work_load_list = [EQUIPMENT_COUNT, TRACTION_POWER_SUPPLY_COUNT]
    return stats_equipment_trip_intensity(
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        work_load_list=work_load_list,
        df_list=df_list,
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，较严重隐患暴露, 事故隐患问题暴露度，班组问题暴露度，他查问题暴露度, 超期设备管控力度, 设备跳闸管控力度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure, _stats_overdue_equipment_intensity,
        _stats_equipment_trip_intensity
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'i', 'j']]
    item_weight = [0.45, 0.1, 0.15, 0.1, -1, 0.1, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=5, child_index_list=[1, 2, 3, 4, 5, 9, 10],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
