# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/9/3上午9:52
   Change Activity: 2019/9/3上午9:52
-------------------------------------------------
"""
import pandas as pd
from app.data.index.util import get_custom_month
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shebeizl import GLV
from app.data.major_risk_index.gongdian_shebeizl.check_intensity_sql import EQUIPMENT_COUNT_SQL
from app.data.major_risk_index.gongdian_shebeizl.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_shebeizl.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    stats_month = get_custom_month(months_ago)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(major, ids))

    # 换算单位设备数量
    equipment_count = pd.merge(
        pd_query(EQUIPMENT_COUNT_SQL),
        zhanduan_dpid_data,
        how='inner',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
    )
    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'EQUIPMENT_COUNT': equipment_count
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
