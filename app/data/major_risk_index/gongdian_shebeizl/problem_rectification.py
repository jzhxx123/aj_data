# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
from flask import current_app

from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import REPEATE_HAPPEN_PROBLEM_SQL
from app.data.major_risk_index.gongdian_shebeizl import GLV
from app.data.major_risk_index.gongdian_shebeizl.common import stats_peril_renovation, get_vitual_major_ids
from app.data.major_risk_index.gongdian_shebeizl.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_shebeizl.problem_rectification_sql import OVERDUE_PROBLEM_NUMBER_SQL, \
    CHECK_EVALUATE_SZ_SCORE_SQL, HAPPEN_PROBLEM_POINT_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL, DEFECTS_COUNT_SQL, \
    DEFECTS_OVERDUE_COUNT_SQL, CHECKED_PERIL_ID_SQL, PERIL_ID_SQL, PERIL_RECTIFY_NO_ENTRY_SQL, \
    MAJOR_PROBLEM_POINT_INFO_SQL
from app.data.major_risk_index.util import df_merge_with_dpid

from app.data.util import pd_query, update_major_maintype_weight

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, STAFF_NUMBER, DEFECTS_COUNT, \
        DEFECTS_OVERDUE_COUNT, PROBLEM_POINT_INFO_DATA, WORKER_COUNT
    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    # 正式职工人数
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(major, ids))
    STAFF_NUMBER = df_merge_with_dpid(WORKER_COUNT, DEPARTMENT_DATA)

    # 缺陷数量
    DEFECTS_COUNT = pd_query(DEFECTS_COUNT_SQL.format(*stats_month), db_name='db_mid')

    # 缺陷库缺陷整治超期数量
    DEFECTS_OVERDUE_COUNT = pd_query(DEFECTS_OVERDUE_COUNT_SQL.format(major, ids))
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major, ids))


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, calc_func=lambda x: min(round(40 + x, 2), 100),
        deduction_coefficient=8)


# 问题控制
def _stats_repeatedly_index(months_ago):
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20}
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
    #     problem_ctl_threshold_dict=problem_ctl_threshold_dict)
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


# 整改复查
def _stats_rectification_review(months_ago):
    return problem_rectification.stats_rectification_review(
        CHECK_ITEM_IDS, STAFF_NUMBER, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# TODO
# 设备缺陷、隐患整治
def _stats_peril_renovation(months_ago):
    """设备缺陷库（40%）-缺陷总数量/总人数
    缺陷库缺陷整治超期(20%)-1条超期扣2分。（设备缺陷库得分低于70分的，该项分值=（100-∑扣分）*缺陷库得分/100；高于等于70分的不乘系数）
    隐患整治督促（40%）单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；高于等于70分的不乘系数），
    """
    staff_number = STAFF_NUMBER.groupby(['TYPE3'])['COUNT'].sum()
    staff_number = staff_number.to_frame(name='PERSON_NUMBER')

    return stats_peril_renovation(
        DEFECTS_COUNT,
        DEFECTS_OVERDUE_COUNT,
        CHECKED_PERIL_ID_SQL,
        PERIL_ID_SQL,
        PERIL_RECTIFY_NO_ENTRY_SQL,
        staff_number,
        DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        CHECK_RISK_IDS
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index,
        _stats_rectification_review,
        _stats_peril_renovation
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.2, 0.2, 0.2, 0.2, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=6, child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
