# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.gongdian_shebeizl import GLV
from app.data.major_risk_index.gongdian_shebeizl.check_intensity_sql import \
    YIBAN_RISK_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL, YECHA_CHECK_SQL, REAL_CHECK_BANZU_SQL, \
    BANZU_POINT_SQL, PROBLEM_CHECK_SCORE_SQL, CHECK_COUNT_SQL, MEDIA_COST_TIME_SQL, \
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL
from app.data.major_risk_index.gongdian_shebeizl.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_shebeizl.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE, RISK_NAME, CHECK_ITEM_IDS, RISK_IDS
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, EQUIPMENT_COUNT, \
        ALL_PROBLEM_NUMBER, YIBAN_RISK_PROBLEM_NUMBER
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    EQUIPMENT_COUNT = GLV.get_value('EQUIPMENT_COUNT')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major, ids)),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 一般风险问题数
    YIBAN_RISK_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(
            YIBAN_RISK_PROBLEM_NUMBER_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)
    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    """（添乘检查（设备质量）+现场检查（设备质量））次数/本单位换算设备数量"""
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        EQUIPMENT_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 问题平均质量分
def _stats_score_per_check_problem(months_ago):
    """设备质量问题质量分累计/设备质量问题总数"""
    return check_intensity.stats_score_per_check_problem(
        PROBLEM_SCORE,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 一般以上风险占比
def _stats_yiban_risk_ratio(months_ago):
    """一般及以上风险问题数/设备质量总问题数"""
    return check_intensity.stats_yiban_risk_ratio_type_one(
        YIBAN_RISK_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 人均质量分
def _stats_score_per_person(months_ago):
    """问题质量分累计/总人数"""
    customizecontent = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = ' \
                       + '问题质量分累计({4})/ 总人数({5})</p>'
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula)


# 夜查率
def _stats_yecha_ratio(months_ago):
    """夜查（添乘检查（设备质量）+现场检查（设备质量））次数/现场检查次数*100"""
    return calc_child_index_type_divide(
        YECHA_COUNT,
        CHECK_COUNT,
        2,
        1,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        lambda x: min(100, x * 100),
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        is_calc_score_base_major=False)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    """
    监控调阅人均时长（35%）=监控调阅时长累计/本单位换算设备数量，与专业基数比较
    监控调阅人均问题个数（35%）=监控调阅发现设备质量问题数/本单位换算设备数量，与专业基数比较
    监控调阅人均质量分（20%）=监控调阅发现设备问题质量分累计/本单位换算设备数量，与专业基数比较
    监控调阅覆盖比例(10%)=调阅检查班组数/具备调阅条件班组数；得分=比例*100"""
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month, CHECK_ITEM_IDS),
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(CHECK_ITEM_IDS)
    ]
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        EQUIPMENT_COUNT,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        risk_ids=RISK_IDS,
        child_weight=[0.35, 0.35, 0.2, 0.1],
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        calc_score_by_formula=_calc_score_by_formula,
        title=['监控调阅时长累计({0})/本单位换算设备数量({1})',
               '监控调阅发现问题数({0})/本单位换算设备数量({1})',
               '监控调阅发现问题质量分累计({0})/本单位换算设备数量({1})',
               '调阅班组数({0})/具备调阅条件班组数({1})']
    )


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """检查设备质量地点数/本单位班组地点总数,与专业基数比较"""
    banzu_point_sql = BANZU_POINT_SQL.format(CHECK_ITEM_IDS)
    content = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = ' \
              + '检查班组数({4})/ 作业班组数({5})</p>'
    return check_intensity.stats_check_address_ratio(
        REAL_CHECK_BANZU_SQL,
        banzu_point_sql,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data,
        is_calc_score_base_major=True,
        calc_func=_calc_score_by_formula,
        customizecontent=content)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_score_per_check_problem,
        _stats_score_per_person,
        _stats_yecha_ratio,
        _stats_media_intensity,
        _stats_yiban_risk_ratio,
        _stats_check_address_ratio,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'e', 'g', 'h', 'i', 'j', 'k']]
    item_weight = [0.25, 0.1, 0.1, 0.1, 0.25, 0.1, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=1, major=risk_type, main_type=1,
                                 child_index_list=[2, 5, 7, 8, 9, 10, 11], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
