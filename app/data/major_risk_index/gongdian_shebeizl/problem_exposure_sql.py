# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure_sql
   Author :       hwj
   date：          2019/9/3下午2:09
   Change Activity: 2019/9/3下午2:09
-------------------------------------------------
"""


# 检查问题
CHECK_PROBLEM_SQL = """SELECT
        distinct c.DEPARTMENT_ID as FK_DEPARTMENT_ID, f.LEVEL,
        f.RISK_LEVEL, f.CHECK_SCORE, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN 
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        AND d.FK_RISK_ID IN ({2})
        AND e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)  
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 自查问题
# 筛选项:检查部门、是否路外:否、
# 问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:否
SELF_CHECK_PROBLEM_SQL = """
SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 2
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 3
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND c.CHECK_TYPE NOT IN (102, 103)  
            AND f.FK_RISK_ID IN ({0})
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 他查问题
# 筛选项:责任部门、是否路外:否、
# 检查方式:问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、
# 月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:是
OTHER_CHECK_PROBLEM_SQL = """
SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 2
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 1
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND c.CHECK_TYPE NOT IN (102, 103)  
            AND f.FK_RISK_ID IN ({0})
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 安全生产信息问题
SAFETY_PRODUCE_INFO_SQL = """SELECT
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            inner join
        t_problem_base_risk as f on d.pk_id = f.fk_problem_base_id
    WHERE
        a.RANK BETWEEN 1 AND 3
            AND f.fk_risk_id IN ({0})
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 基础问题库里的事故隐患问题
# 不过滤问题层级，基础问题的问题所属部门默认站段级别
HIDDEN_KEY_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(b.DEPARTMENT_ID, '||', a.PK_ID) AS PID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            inner join
        t_problem_base_risk as c on a.pk_id = c.FK_PROBLEM_BASE_ID
    WHERE
         a.IS_HIDDEN_KEY_PROBLEM = 1
            AND c.fk_risk_id IN ({0})
            AND a.`STATUS` = 3
            AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.IS_DELETE = 0
"""

# 每月发生事故隐患问题
HIDDEN_KEY_PROBLEM_MONTH_SQL = """SELECT DISTINCT
        CONCAT(c.DEPARTMENT_ID, '||', b.PK_ID) AS PID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            inner join
        t_problem_base_risk as d on a.pk_id = d.FK_PROBLEM_BASE_ID
    WHERE
        b.IS_HIDDEN_KEY_PROBLEM = 1
            AND b.`TYPE` = 3
            AND b.`STATUS` = 3
            AND b.is_delete = 0
            AND d.fk_risk_id IN ({0})
            AND c.SHORT_NAME != ''
            AND c.IS_DELETE = 0
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""
# 设备数量
ALL_EQUIPMENT_COUNT_SQL = """
SELECT
        FK_DEPARTMENT_ID, MANAGEMENT_INDEX AS ALL_COUNT
    FROM
        t_department_other_management 
    WHERE
        (FK_TYPE_ID = 2508 OR FK_TYPE_ID = 2510)
"""

# 超期设备数量
OVERDUE_EQUIPMENT_COUNT_SQL = """
SELECT
        FK_DEPARTMENT_ID, MANAGEMENT_INDEX AS OVERDUE_COUNT
    FROM
        t_department_integrated_management 
    WHERE
        (FK_TYPE_ID = 2563 OR FK_TYPE_ID = 2564)
        AND MONTH = {0}
"""

# todo 无数据暂时填0
# 馈线跳闸次数
FEEDER_TRIPS_COUNT_SQL = """
SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 0 AS COUNT
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
            AND b.SHORT_NAME != ''
"""


# todo 无数据暂时填0
# 本单位牵引供电量
TRACTION_POWER_SUPPLY_COUNT_SQL = """
SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 0 AS COUNT
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
            AND b.SHORT_NAME != ''
"""