# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common.py
   Author :       hwj
   date：          2019/7/22下午7:30
   Change Activity: 2019/7/22下午7:30
-------------------------------------------------
"""

# 他查问题暴露度
import string

import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import df_merge_with_dpid, \
    calc_child_index_type_sum, append_major_column_to_df, format_export_basic_data, write_export_basic_data_to_mongo
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data, problem_risk_score={'1': 4, '2': 2, '3': 0.1, }):
    """从他查问题分析,1个月未自查出该问题项点，一般风险问题一条扣0.1分，较大风险扣1分，
    严重风险扣3分；若该问题项点属于事故隐患关键问题的在上述基础上*3。
    每个问题项点最高扣30分。满分100，最低0分。
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(
            check_item_ids, *calc_month))['PROBLEM_DPID_RISK'].values)
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(
            check_item_ids, *calc_month))['PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(
        safety_produce_info_sql.format(check_item_ids, *calc_month))
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # problem_risk_score = {
    #     '1': 4,
    #     '2': 2,
    #     '3': 0.1,
    # }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        problem_score = problem_risk_score.get(each_problem[1], 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, each_problem[1]])
    # 未自查出安全生产信息问题
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            problem_rank = row['PROBLEM_DPID_RISK'].split('||')[2]
            problem_score = problem_risk_score.get(
                problem_rank, 0) * (4 - int(row['MAIN_TYPE']))
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, problem_rank])
    # 导出中间计算过程
    first_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}

    data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    data['MONTH'] = data['MONTH'].apply(lambda x: first_title.get(x))
    data = df_merge_with_dpid(data, department_data)
    data = data.groupby(['TYPE3', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    columns_list = ['一般风险', '较大风险', '严重风险']
    for col in columns_list:
        if col not in columns:
            data[col] = 0
    title = '他查问题个数'
    data['CONTENT'] = data.apply(
        lambda row: title + '<br/>' + '<br/>'.join(f'{col}: {int(row[col])}个' for col in columns_list),
        axis=1)
    data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, 5, 5,
                                        3, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     5, 5, risk_type)

    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        5,
        5,
        months_ago,
        'SCORE',
        'SCORE_e',
        lambda x: 30 if x > 30 else x,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def calc_number_func(row):
    reality_principal = row['REALITY_PRINCIPAL']
    reality_tester = row['REALITY_TESTER']
    reality_administrator = row['REALITY_ADMINISTRATOR']
    indoor_person = row['INDOOR_PERSON']
    outdoor_person = row['OUTDOOR_PERSON']
    person_set = set(
        reality_principal.strip().split(',') +
        reality_tester.strip().split(',') +
        reality_administrator.strip().split(',') +
        indoor_person.strip().split(',') +
        outdoor_person.strip().split(',')
    )
    # 去除特殊字符
    filter_list = ['无', '未上道', '不上道', '工务', '0', '室内作业', 'wu']
    for i in filter_list:
        person_set.discard(i)

    # 去除符号
    symbol_filter_list = []
    for i in person_set:
        if i in string.punctuation:
            symbol_filter_list.append(i)
    person_set.difference_update(symbol_filter_list)
    person_number = len(person_set)
    return person_number


# 统计施工人数及工作量
def calc_work_load_count(work_person, work_time):
    work_person['PERSON_NUMBER'] = work_person.apply(
        lambda row: calc_number_func(row), axis=1
    )

    work_time['PERSON_NUMBER'] = work_time.apply(
        lambda row: calc_number_func(row), axis=1
    )

    work_time['COUNT'] = work_time.apply(
        lambda row: (row['PERSON_NUMBER'] * row['TIME']), axis=1
    )

    work_person = work_person.groupby(['FK_DEPARTMENT_ID'])['PERSON_NUMBER'].sum().reset_index()
    work_person.rename(columns={'PERSON_NUMBER': 'COUNT'}, inplace=True)

    work_labor_time = work_time.groupby(['FK_DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    return work_labor_time, work_person


def cacl_work_person_count(construction_person, cooperate_person, special_department_person):
    """
    :param special_department_person: 特殊站段施工人数
    :param construction_person: 三级施工人数
    :param cooperate_person: 施工配合人数
    :return: work_person 施工总人数
    """
    if construction_person.empty:
        construction_person['COUNT'] = 0
    else:
        construction_person['COUNT'] = construction_person.apply(
            lambda row: calc_number_func(row), axis=1
        )

    construction_person = construction_person[['FK_DEPARTMENT_ID', 'COUNT']]
    work_person = pd.concat([construction_person, cooperate_person, special_department_person])
    work_person.groupby(['FK_DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    return work_person
