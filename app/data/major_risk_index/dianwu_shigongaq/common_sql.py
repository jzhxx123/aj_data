# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.ALL_NAME
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, '电务' AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        '电务' AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND (c.DEPARTMENT_ID = '{0}' or a.TYPE3 in {1})
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
        AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
    GROUP BY a.FK_DEPARTMENT_ID;
"""
# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 量化人员数量,去除各项指标（包括基础指标和细化指标）都为零的
QUANTIZATION_PERSON_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_quantization_base_quota AS a
            INNER JOIN
        t_person AS b ON b.ID_CARD = a.ID_CARD
    WHERE
        a.YEAR = {0} AND a.MONTH = {1}
            AND a.`STATUS` = 1
            AND b.POSITION IN ({2})
            AND (a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_base_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(CHECK_TIMES_TOTAL, 0)
                    + IFNULL(PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_NUMBER_TOTAL, 0)
                    + IFNULL(WORK_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(CHECK_NOTIFICATION_TIMES_TOTAL, 0)
                    + IFNULL(MIN_QUALITY_GRADES_TOTAL, 0)
                    + IFNULL(HIDDEN_DANGER_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(RISK_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL, 0) 
                    ) > 0
                )
            OR a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_refinement_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_TIME_TOTAL, 0)
                    ) > 0
                )
            )
    GROUP BY a.FK_DEPARTMENT_ID
"""

# 量化人员及干部检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 量化人员及干部发现问题数
CHECK_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND FK_CHECK_ITEM_ID IN ({2})
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 一般及以上风险问题数(量化人员及干部)
RISK_LEVEL_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_LEVEL <= 2
            AND FK_CHECK_ITEM_ID IN ({2})
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 问题质量分累计(量化人员及干部)
PROBLEM_CHECK_SCORE_SQL = """SELECT
    EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
FROM
    t_check_problem AS a
        LEFT JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_CHECK_ITEM_ID IN ({2})
GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 三级施工劳时
WORK_LABOR_TIME_SQL = """
    SELECT
        PK_ID,
        DEPARTMENT_ID AS FK_DEPARTMENT_ID, 
        START_SUBMIT_DATE,
        END_SUBMIT_DATE,
        TIMESTAMPDIFF(SECOND ,START_SUBMIT_DATE,END_SUBMIT_DATE)/3600 AS TIME,
        REALITY_PRINCIPAL,
        REALITY_TESTER,
        REALITY_ADMINISTRATOR,
        INDOOR_PERSON,
        OUTDOOR_PERSON
    FROM
        dw_t_construct_three_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS=2
"""

# 三级施工人数
CONSTRUCTION_PERSON_SQL = """
    SELECT
        PK_ID,
        DEPARTMENT_ID AS FK_DEPARTMENT_ID, 
        REALITY_PRINCIPAL,
        REALITY_TESTER,
        REALITY_ADMINISTRATOR,
        INDOOR_PERSON,
        OUTDOOR_PERSON
    FROM
        dw_t_construct_three_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS=2
"""

# 配合人数及临时配合人数
COOPERATE_PERSON_COUNT_SQL = """SELECT
    tc.FK_DEPARTMENT_ID,
    sum( tc.COUNT ) AS COUNT
FROM
    (
    SELECT
        DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        CONVERT ( PERSON_CONTENT, signed ) AS COUNT 
    FROM
        `dw_t_cooperate_work_select_control` 
    WHERE
        DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND `STATUS` = 2 UNION ALL
    SELECT
        DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        CONVERT ( PERSON_CONTENT, signed ) AS COUNT 
    FROM
        dw_t_temp_select_control 
    WHERE
        DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND `STATUS` = 2 
    ) AS tc 
GROUP BY
    tc.FK_DEPARTMENT_ID
"""

# 特殊站段施工人数
SPECIAL_DEPARTMENT_CONSTRUCTION_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS COUNT 
FROM
    `t_department_integrated_management` 
WHERE
    FK_TYPE_ID = 2514 
    AND `MONTH` = {0}
"""
