#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-5-16

import pandas as pd
from flask.globals import current_app

from app.data.index.common import combine_child_index_func, summizet_child_index
from app.data.index.util import get_year_month
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_shigongaq import GLV
from app.data.major_risk_index.dianwu_shigongaq.problem_rectification_sql import OVERDUE_DAYS_NUMBER_SQL, \
    OVERDUE_PROBLEM_NUMBER_SQL, CHECK_EVALUATE_SZ_SCORE_SQL, HAPPEN_PROBLEM_POINT_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, format_export_basic_data, \
    write_export_basic_data_to_mongo, calc_child_index_type_sum
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        OVERDUE_PROBLEM_NUMBER, OVERDUE_DAYS_NUMBER

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')

    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    OVERDUE_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(
            OVERDUE_PROBLEM_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                              CHECK_ITEM_IDS)), DEPARTMENT_DATA
    )
    # 问题超期消耗数
    OVERDUE_DAYS_NUMBER = df_merge_with_dpid(
        pd_query(
            OVERDUE_DAYS_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                           CHECK_ITEM_IDS)), DEPARTMENT_DATA
    )


def _calc_rectification_score(problem_number, deduction_coefficient):
    val = 100 - deduction_coefficient * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


def _calc_basic_overdue_func(df_data, i, title):
    overdue_problem_number = OVERDUE_PROBLEM_NUMBER.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    overdue_days_number = OVERDUE_DAYS_NUMBER.groupby(['TYPE3'])['COUNT'].sum().reset_index()

    overdue_problem_number.rename(columns={'COUNT': 'OVERDUE_PROBLEM',
                                           'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    overdue_days_number.rename(columns={'COUNT': 'OVERDUE_DAYS_NUMBER',
                                        'TYPE3': 'DEPARTMENT_ID'}, inplace=True)

    overdue_problem_number = pd.merge(_choose_dpid_data(3), overdue_problem_number,
                                      how="left", on="DEPARTMENT_ID")
    overdue_days_number = pd.merge(_choose_dpid_data(3), overdue_days_number,
                                   how="left", on="DEPARTMENT_ID")
    overdue_problem_number.fillna(0, inplace=True)
    overdue_days_number.fillna(0, inplace=True)

    data = pd.merge(overdue_problem_number, overdue_days_number)

    data[f'number_{i}'] = data.apply(
        lambda row: title.format(int(row['OVERDUE_PROBLEM']), int(row['OVERDUE_DAYS_NUMBER'])), axis=1)

    data.drop(columns=['OVERDUE_PROBLEM', 'OVERDUE_DAYS_NUMBER'],
              inplace=True, axis=1)
    data.rename(columns={f'number_{i}': 'CONTENT'}, inplace=True)
    return data


# 问题整改
def _stats_rectification_overdue(months_ago):
    """
    问题超期整改扣分：问题超期消耗1条扣1分(超期>7天),问题整改超期1条扣2分；
    :param months_ago:
    :return:
    """
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    overdue_problem_number = pd_query(
        OVERDUE_PROBLEM_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                          CHECK_ITEM_IDS))
    # 问题超期消耗数
    overdue_days_number = pd_query(
        OVERDUE_DAYS_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                       CHECK_ITEM_IDS))

    calc_basic_data = []
    title = [
        '本月问题超期发生条数: {0}条<br/>问题超期消耗条数: {1}条'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [[overdue_problem_number, overdue_days_number]]):
        for j, func in enumerate([
            _calc_basic_overdue_func,
        ]):
            calc_basic_data.append(
                func(data.copy(), i, title[i]))

    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 6, 1, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 6, 1, risk_type=RISK_TYPE)

    overdue_problem_number = OVERDUE_PROBLEM_NUMBER.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    overdue_days_number = OVERDUE_DAYS_NUMBER.groupby(['TYPE3'])['COUNT'].sum().reset_index()

    overdue_problem_number.rename(columns={'COUNT': 'OVERDUE_PROBLEM',
                                           'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    overdue_days_number.rename(columns={'COUNT': 'OVERDUE_DAYS_NUMBER',
                                        'TYPE3': 'DEPARTMENT_ID'}, inplace=True)

    overdue_problem_number = pd.merge(_choose_dpid_data(3), overdue_problem_number,
                                      how="left", on="DEPARTMENT_ID")
    overdue_days_number = pd.merge(_choose_dpid_data(3), overdue_days_number,
                                   how="left", on="DEPARTMENT_ID")
    overdue_problem_number.fillna(0, inplace=True)
    overdue_days_number.fillna(0, inplace=True)
    data = pd.merge(overdue_problem_number, overdue_days_number)

    data['SCORE'] = data.apply(
        lambda row: (100 - row['OVERDUE_PROBLEM'] * 2) - (row['OVERDUE_DAYS_NUMBER']), axis=1)
    data.drop(columns=['OVERDUE_PROBLEM', 'OVERDUE_DAYS_NUMBER', "NAME", "MAJOR"], inplace=True)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)

    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        1,
        months_ago,
        'SCORE',
        'SCORE_a',
        lambda x: x if x >= 0 else 0,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        NA_value=True)
    return rst_index_score


# 履职评价指数
def _stats_check_evaluate(months_ago):
    """
    履职评价指数：履职评价中一条ZG-1、2、3、4、5分别扣评价条款基础分*0.2。
    :param months_ago:
    :return:
    """
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=0.2)


# 反复指数
def _stats_repeatedly_index(months_ago):
    """
    以问题项点统计（路外设备质量考核金额为0的不统计。
    站段级：
    以站段为统计单位同一问题项点是否发生，
    严重风险的问题3月内重复发生一个扣a分，
    较大风险的（当月累计发生3次以上算重复发生）问题3月内重复发生一项扣a分，
    一般风险的（当月累计发生10次以上算重复发生）问题3月内重复发生一项扣a分。
    :param months_ago:
    :return:
    """
    return problem_rectification.stats_repeatedly_index(
        CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【整改时效，整改履责，问题控制】
    child_index_func = [
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.1, 0.2, 0.7]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(6, risk_type, main_type=6, child_index_list=[1, 2, 3],
                                 child_index_weight=[0.1, 0.2, 0.7])

    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
