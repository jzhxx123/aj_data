#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         2019/5/13

from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.check_intensity_sql import (
    REAL_CHECK_POINT_SQL, CHECK_POINT_SQL,
    REAL_CHECK_BANZU_SQL, BANZU_POINT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_shigongaq import GLV
from app.data.major_risk_index.dianwu_shigongaq.check_intensity_sql import \
    MEDIA_COST_TIME_SQL, CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, KAOHE_PROBLEM_SQL, MEDIA_PROBLEM_NUMBER_SQL, \
    MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, PROBLEM_CHECK_SCORE_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago: int, risk_name: int, risk_type: str):
    """
    获取计算所需的常量
    :param months_ago:
    :param risk_name:  91
    :param risk_type:   电务-7
    """
    major = get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME, RISK_IDS, CHECK_ITEM_IDS
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global CHECK_COUNT, WORK_LOAD, \
        PROBLEM_SCORE, MEDIA_COST_TIME, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, ASSESS_PROBLEM_COUNT, ALL_PROBLEM_NUMBER, ZHANDUAN_FILTER_LIST
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    # 施工劳时
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 需要过滤不参与基数计算的站段列表
    ZHANDUAN_FILTER_LIST = GLV.get_value('ZHANDUAN_FILTER_LIST')
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 施工类问题数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA
    )
    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 监控调阅时长
    MEDIA_COST_TIME = df_merge_with_dpid(
        pd_query(MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 现场检查频次
def _stats_check_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
                        + '量化人员及干部检查次数({4})/ 施工作业总人数({5})</p>', None]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = '
                        + '问题数({4})/ 施工作业总人数({5})</p>', None]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        ALL_PROBLEM_NUMBER,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题考核率({3}) = '
                        + '考核问题数({4})/ 发现问题数({5})</p>', None]
    return check_intensity.stats_check_problem_assess_radio_type_one(
        ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>质量均分({3}) = '
                        + '问题质量分累计({4})/ 施工作业总人数({5})</p>', None]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    title = ['监控调阅时长累计({0})/施工作业总人数({1})',
             '监控调阅发现问题数({0})/施工作业总人数({1})',
             '监控调阅发现问题质量分累计({0})/施工作业总人数({1})',
             '调阅班组数({0})/班组数({1})']
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        title=title,
        risk_ids=RISK_IDS,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        child_weight=[0.35, 0.35, 0.2, 0.1],
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
                        + '施工检查地点数({4})/ 施工点总数({5})*100%</p>', None]
    return check_intensity.stats_check_address_ratio_include_check_point(
        REAL_CHECK_BANZU_SQL,
        REAL_CHECK_POINT_SQL,
        BANZU_POINT_SQL,
        CHECK_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
    )


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【现场检查频次,查处问题率,查处问题考核率,人均质量分,监控调阅力度,覆盖率】
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_check_problem_assess_radio,
        _stats_score_per_person,
        _stats_media_intensity,
        _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd', 'e', 'j', 'i']]
    item_weight = [0.25, 0.2, 0.05, 0.2, 0.1, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(6, risk_type, main_type=1, child_index_list=[2, 3, 4, 5, 10, 9],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
