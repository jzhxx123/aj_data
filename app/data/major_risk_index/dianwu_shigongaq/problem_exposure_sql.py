#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         2019/5/14

# 基础问题库里的施工类问题总数
ALL_BASE_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
        left join 
        t_problem_base_risk as b on a.PK_ID = b.FK_PROBLEM_BASE_ID
    WHERE
        a.`TYPE` = 3
        AND a.STATUS = 3
        AND a.IS_DELETE=0
        AND b.FK_RISK_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID
"""

# 查处的施工类问题数(基础问题库被查出的问题)
ALL_PROBLEM_NUMBER_SQL = """SELECT
        f.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY f.FK_DEPARTMENT_ID
"""

# 当月施工总数
NORMAL_CONSTRUCTION_COUNT_SQL = """SELECT
    tc.FK_DEPARTMENT_ID,
    sum( tc.COUNT ) AS COUNT
FROM
    (
    SELECT
        DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        1 AS COUNT 
    FROM
        `dw_t_cooperate_work_select_control` 
    WHERE
        DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND `STATUS` = 2 
    UNION ALL
    SELECT
        DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        1 AS COUNT 
    FROM
        dw_t_temp_select_control 
    WHERE
        DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND `STATUS` = 2 
    UNION ALL
    SELECT
                DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        1 AS COUNT 
        FROM
                dw_t_construct_three_select_control
        WHERE
        DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( END_SUBMIT_DATE, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND `STATUS` = 2 
    ) AS tc 
GROUP BY
    tc.FK_DEPARTMENT_ID
"""

# 成都电务段施工次数
SPECIAL_DEPARTMENT_CONSTRUCTION_SQL = """SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS COUNT 
FROM
    `t_department_integrated_management` 
WHERE
    FK_TYPE_ID = 2514 
    AND `MONTH` = {0}
"""

# 查出的隐患问题项点(本单位基础问题库中有的)
CHECKED_HIDDEN_PROBLEM_POINT_SQL = """SELECT DISTINCT
    f.FK_DEPARTMENT_ID, COUNT(distinct f.PK_ID) AS COUNT
FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
WHERE
    f.IS_HIDDEN_KEY_PROBLEM = 1
    AND a.RISK_LEVEL <= 2
        AND f.`STATUS` = 3
        AND f.IS_DELETE = 0
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY f.FK_DEPARTMENT_ID
"""

# 隐患问题项点
HIDDEN_PROBLEM_POINT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
            INNER JOIN
        t_problem_base_risk as b on b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.STATUS = 3
            AND a.IS_DELETE=0
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID
"""


# 他查问题
# 筛选项:责任部门、是否路外:否、
# 检查方式:问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、
# 月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:是
OTHER_CHECK_PROBLEM_SQL = """SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 1
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND c.CHECK_TYPE NOT IN (102, 103)  
            AND f.FK_RISK_ID IN ({0})
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 安全生产信息问题
SAFETY_PRODUCE_INFO_SQL = """SELECT
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        a.RANK BETWEEN 1 AND 3
            AND f.FK_RISK_ID IN ({0})
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 自查问题
# 筛选项:检查部门、是否路外:否、
# 问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:否
SELF_CHECK_PROBLEM_SQL = """SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 3
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND c.CHECK_TYPE NOT IN (102, 103)  
            AND f.FK_RISK_ID IN ({0})
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

