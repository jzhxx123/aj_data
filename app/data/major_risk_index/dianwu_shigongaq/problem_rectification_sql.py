#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-5-18


# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            INNER JOIN
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 问题超期消耗(消耗时间大于7天)
OVERDUE_DAYS_NUMBER_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            INNER JOIN
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND a.OVERDUE_DAYS >= 7
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        mp.FK_DEPARTMENT_ID, SUM(mp.SCORE_STANDARD*mp.GRADATION_RATIO)*{3} AS SCORE,
        COUNT(mp.pk_id) AS COUNT
FROM
(SELECT
DISTINCT b.FK_DEPARTMENT_ID, a.SCORE_STANDARD, d.GRADATION_RATIO, a.pk_id
FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person_gradation_ratio AS d
            ON a.FK_PERSON_GRADATION_RATIO_ID = d.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    ) as mp
    GROUP BY mp.FK_DEPARTMENT_ID;
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_problem_base_risk as e on a.FK_PROBLEM_BASE_ID = e.FK_PROBLEM_BASE_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND e.FK_RISK_ID IN ({2})
        AND b.IS_DELETE = 0
"""
