#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         2019/5/13


# 检查次数（现场检查）
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 施工问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID    
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 3 AND 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND a.IS_EXTERNAL = 0
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 AND 4
        AND a.TYPE = 3
        AND a.IS_EXTERNAL = 0
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(TYPE !=9, FK_PARENT_ID, DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department 
WHERE
         TYPE BETWEEN 9 AND 10
        AND IS_DELETE = 0
        AND MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]