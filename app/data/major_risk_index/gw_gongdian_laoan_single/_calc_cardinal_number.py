# -*- coding: utf-8 -*-
"""
工电劳安（工电基数），计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)

from app.data.major_risk_index.gw_gongdian_laoan_single import GLV
import pandas as pd
from app.data.util import (
    pd_query, get_coll_prefix, get_history_months)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)

from app.data.major_risk_index.gw_gongdian_laoan_single.common import (
    calc_total_workload, calc_night_work_load, get_vitual_major_ids,
    get_all_check_item_ids, filter_with_check_info_id,
    get_all_check_info_ids)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gw_gongdian_laoan_single.check_intensity_sql import (
    BANZU_POINT_SQL, MEDIA_COST_TIME_SQL,
    RISK_LEVEL_PROBLEM_SQL, REAL_CHECK_POINT_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    CHECK_POINT_SQL, CHECK_COUNT_SQL, ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    REAL_CHECK_BANZU_SQLIST, CHECK_INFO_SQL,
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL)
from app.data.major_risk_index.gongwu_laoan.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL, KAOHE_PROBLEM_SQL)
from app.data.major_risk_index.gw_gongdian_laoan_single.common_sql import (
    WORK_LOAD_SQL,
    TOTAL_LABORTIME_SQL, ORDER_COUNT_SQL, WORK_TIME_RECORD_SQL)
from app.data.major_risk_index.gongwu_laoan.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_SCORE_SQL, SHIGONG_COUNT_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, PERSON_LOAD_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    STAFF_NUMBER_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    ASSESS_RESPONSIBLE_INFO, ALL_PROBLEM_NUMBER_INFO,
    ACTIVE_EVALUATE_SCORE_INFO, ASSESS_PROBLEM_INFO,
    AWARD_RETURN_MONEY_INFO, WORKER_LOAD_INFO,
    REAL_CHECK_ADDRESS_INFO, CHECK_ADDRESS_INFO)


# MAIN_TYPE=1为事故
# MAIN_TYPE=2为故障
BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT,
    a.MAIN_TYPE
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_department AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND c.TYPE3 in {2}
        AND a.MAIN_TYPE in (1, 2)
"""

# 外聘人员数 - 站段
EMPLOYED_OUTSIDE_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.TYPE3 in {1}
    AND b.is_delete = 0
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        STAFF_NUMBER, CHECK_ITEM_IDS, RISK_IDS
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = get_all_check_item_ids(get_major_dpid('工务-2'))
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    CALC_MONTH = END, START
    IDS = get_vitual_major_ids(risk_type)
    # 单位职工人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(IDS))


# ------------------------获取比值型相应基数------------------------ #
# 比值型指数（main_type-detail_type)
_DIVIDE_TYPE_CHILD_INDEX = {
    '1-2': [
        '{0}{1}月现场检查{2}次，干部职工数{3}',
        '虚拟单位前3个月现场检查均值{0}次(总现场检查{1}次), 次，干部职工数均值{2}(总干部职工{3})'
    ]
}


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    major_dpid = get_major_dpid('工务-2')
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, IDS)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总数(关联项目)'
    CHECK_COUNT.value = [CHECK_COUNT_SQL, major_dpid]
    CHECK_COUNT.func_version = 'customized_df'
    CHECK_COUNT.func_value = _calc_filter_with_check_info_id

    # 总人数
    PERSON_LOAD = CommonCalcDataType(*PERSON_LOAD_INFO)
    PERSON_LOAD.value = [EMPLOYED_OUTSIDE_PERSON_SQL.format('{0}', IDS),
                         STAFF_NUMBER]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 较大问题质量分
    JIAODA_RISK_SCORE = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    JIAODA_RISK_SCORE.value = [
        RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    XC_JIAODA_RISK_SCORE = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    XC_JIAODA_RISK_SCORE.value = [
        XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 夜查次数
    YECHA_COUNT = CommonCalcDataType(*YECHA_COUNT_INFO)
    YECHA_COUNT.version = 'v2'
    YECHA_COUNT.description = '现场夜查次数(关联项目)'
    YECHA_COUNT.value = [YECHA_CHECK_SQL, major_dpid]
    YECHA_COUNT.func_version = 'customized_df'
    YECHA_COUNT.func_value = _calc_filter_with_check_info_id

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.version = 'v2'
    MEDIA_COST_TIME.description = '监控调阅时长(关联项目)'
    MEDIA_COST_TIME.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.value = [
        MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.value = [
        MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核总金额
    ASSESS_RESPONSIBLE = CommonCalcDataType(*ASSESS_RESPONSIBLE_INFO)
    ASSESS_RESPONSIBLE.version = 'v2'
    ASSESS_RESPONSIBLE.description = '月度考核总金额(关联风险)'
    ASSESS_RESPONSIBLE.value = [
        ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题数
    ABOVE_YIBAN_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ABOVE_YIBAN_PROBLEM_NUMBER.version = 'v2'
    ABOVE_YIBAN_PROBLEM_NUMBER.description = '一般及以上问题数'
    ABOVE_YIBAN_PROBLEM_NUMBER.value = [
        ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题质量分
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.version = 'v2'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.description = '一般及以上问题质量分'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.value = [
        ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.value = [
        ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 干部主动评价记分
    ACTIVE_EVALUATE_SCORE = CommonCalcDataType(*ACTIVE_EVALUATE_SCORE_INFO)
    ACTIVE_EVALUATE_SCORE.value = [
        ACTIVE_EVALUATE_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核问题数
    ASSESS_PROBLEM_COUNT = CommonCalcDataType(*ASSESS_PROBLEM_INFO)
    ASSESS_PROBLEM_COUNT.version = 'v2'
    ASSESS_PROBLEM_COUNT.description = '考核问题数（包含路外，关联风险）'
    ASSESS_PROBLEM_COUNT.value = [
        KAOHE_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 职工总人数
    STAFF_NUMBER_COUNT = CommonCalcDataType(*STAFF_NUMBER_INFO)
    STAFF_NUMBER_COUNT.value = [STAFF_NUMBER]

    # 月度返奖金额
    AWARD_RETURN_MONEY = CommonCalcDataType(*AWARD_RETURN_MONEY_INFO)
    AWARD_RETURN_MONEY.value = [AWARD_RETURN_SQL.format('{0}', '{1}', RISK_IDS)]

    # 工作量
    WORKER_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    WORKER_LOAD_COUNT.description = '工电劳安工作量'
    WORKER_LOAD_COUNT.value = [TOTAL_LABORTIME_SQL, ORDER_COUNT_SQL]
    WORKER_LOAD_COUNT.func_version = 'customized_df'
    WORKER_LOAD_COUNT.func_value = _calc_total_work_load

    # 夜间工作量
    YEJIAN_WORKER_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    YEJIAN_WORKER_LOAD_COUNT.description = '工电夜间劳安工作量'
    YEJIAN_WORKER_LOAD_COUNT.value = [WORK_TIME_RECORD_SQL]
    YEJIAN_WORKER_LOAD_COUNT.func_version = 'customized_df'
    YEJIAN_WORKER_LOAD_COUNT.func_value = _calc_night_work_load

    # 实际检查地点数
    REAL_CHECK_ADDRESS_DATA = CommonCalcDataType(*REAL_CHECK_ADDRESS_INFO)
    REAL_CHECK_ADDRESS_DATA.value = [REAL_CHECK_BANZU_SQLIST[0].format(major_dpid), CHECK_INFO_SQL,
                                     REAL_CHECK_POINT_SQL, major_dpid]
    REAL_CHECK_ADDRESS_DATA.func_version = 'customized_df'
    REAL_CHECK_ADDRESS_DATA.func_value = _calc_real_check_address

    # 检查地点数
    CHECK_ADDRESS_DATA = CommonCalcDataType(*CHECK_ADDRESS_INFO)
    CHECK_ADDRESS_DATA.value = [BANZU_POINT_SQL.format(major_dpid), CHECK_POINT_SQL]
    CHECK_ADDRESS_DATA.func_version = 'customized_df'
    CHECK_ADDRESS_DATA.func_value = _calc_check_address

    # 劳安作业人数（总上道人数）
    SHIGONG_COUNT_INFO = (
    'ShiGongNumber', '劳安作业人数（总上道人数）', 'v1', 'customized_df')
    SHIGONG_COUNT = CommonCalcDataType(*SHIGONG_COUNT_INFO)
    SHIGONG_COUNT.value = [SHIGONG_COUNT_SQL]
    SHIGONG_COUNT.func_value = _calc_shigong_count

    # 派工单数
    ORDER_COUNT_INFO = (
    'OrderCount', '派工单总数', 'v1', 'customized_df')
    ORDER_COUNT = CommonCalcDataType(*ORDER_COUNT_INFO)
    ORDER_COUNT.value = [ORDER_COUNT_SQL]
    ORDER_COUNT.func_value = _calc_shigong_count

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, WORKER_LOAD_COUNT),),

        # 检查问题考核率
        '1-4': (IndexDetails(ASSESS_PROBLEM_COUNT, ALL_PROBLEM_NUMBER),),

        # 质量均分
        '1-5': (IndexDetails(PROBLEM_CHECK_SCORE, WORKER_LOAD_COUNT),),

        # 较大风险问题质量均分
        '1-6': (IndexDetails(JIAODA_RISK_SCORE, WORKER_LOAD_COUNT), IndexDetails(XC_JIAODA_RISK_SCORE, WORKER_LOAD_COUNT)),

        # 夜查率
        '1-7': (IndexDetails(YECHA_COUNT, YEJIAN_WORKER_LOAD_COUNT),),

        # 覆盖率
        '1-9': (IndexDetails(REAL_CHECK_ADDRESS_DATA, CHECK_ADDRESS_DATA, ),),
        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, WORKER_LOAD_COUNT), 
                IndexDetails(MEDIA_PROBLEM_NUMBER, WORKER_LOAD_COUNT),
                IndexDetails(MEDIA_PROBLME_SCORE, WORKER_LOAD_COUNT)),

        # 评价力度
        # 人均评价记分
        '2-3': (IndexDetails(ACTIVE_EVALUATE_SCORE, SHIGONG_COUNT),),

        # 考核力度指数
        # 换算单位考核问题数
        '3-1': (IndexDetails(ASSESS_PROBLEM_COUNT, STAFF_NUMBER_COUNT), ),

        # 换算单位考核金额
        '3-2': (IndexDetails(ASSESS_RESPONSIBLE, STAFF_NUMBER_COUNT),),

        # 返奖率
        '3-3': (IndexDetails(AWARD_RETURN_MONEY, ASSESS_RESPONSIBLE),),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
            IndexDetails(ALL_PROBLEM_NUMBER, ORDER_COUNT),
            IndexDetails(PROBLEM_CHECK_SCORE, ORDER_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_NUMBER, ORDER_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_CHECK_SCORE, ORDER_COUNT),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('1-4', CHILD_INDEX_SQL_DICT['1-4'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_score_per_person', CHILD_INDEX_SQL_DICT['1-5'])
    GLV.set_value('stats_yecha_ratio', CHILD_INDEX_SQL_DICT['1-7'])
    GLV.set_value('stats_check_address_ratio', CHILD_INDEX_SQL_DICT['1-9'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_active_score_per_person', CHILD_INDEX_SQL_DICT['2-3'])
    GLV.set_value('stats_check_problem_assess_radio',
                  CHILD_INDEX_SQL_DICT['3-1'])
    GLV.set_value('stats_assess_money_per_person', CHILD_INDEX_SQL_DICT['3-2'])
    GLV.set_value('stats_award_return_ratio', CHILD_INDEX_SQL_DICT['3-3'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])


def _calc_total_work_load(
    _sqllist_numerator, 
    zhanduan_dpid_data, 
    department_data, 
    mon_ago, column, dpids):
    """[summary]
    
    Arguments:
        _sqllist_numerator {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]
        department_data {[type]} -- [description]
        mon_ago {[type]} -- [description]
        _key {[type]} -- [description]
        dpids {[type]} -- [description]
    """
    stats_month = get_custom_month(mon_ago)
    labortime_data = pd_query(_sqllist_numerator[0].format(*stats_month), db_name='db_mid')
    order_count = pd_query(_sqllist_numerator[1].format(*stats_month), db_name='db_mid')
    data = calc_total_workload(labortime_data, order_count, department_data)
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data


def _calc_night_work_load(
    _sqllist_numerator, 
    zhanduan_dpid_data, 
    department_data, 
    mon_ago, column, dpids):
    """[summary]
    
    Arguments:
        _sqllist_numerator {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]
        department_data {[type]} -- [description]
        mon_ago {[type]} -- [description]
        column {[type]} -- [description]
        dpids {[type]} -- [description]
    """
    stats_month = get_custom_month(mon_ago)
    df_work_time = pd_query(_sqllist_numerator[0].format(*stats_month),
                 db_name='db_mid')
    data = calc_night_work_load(df_work_time, department_data)
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data


def _calc_real_check_address(
    _sqllist_numerator,
    zhanduan_dpid_data,
    department_data,
    mon_ago, column, dpids):
    import pandas as pd
    stats_month = get_custom_month(mon_ago)
    check_info_ids = get_all_check_info_ids(mon_ago, _sqllist_numerator[3])
    real_check_banzu_data = pd.merge(
        pd_query(_sqllist_numerator[0]),
        pd_query(_sqllist_numerator[1].format(*stats_month)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )
    real_check_banzu_data = real_check_banzu_data[
        real_check_banzu_data['PK_ID'].isin(check_info_ids)
    ]
    real_check_banzu_data.drop(["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)

    real_check_point_data = filter_with_check_info_id(
        pd_query(_sqllist_numerator[2].format(*stats_month)),
        check_info_ids)
    # 检查地点数
    data_real = pd.concat(
        [
            real_check_point_data,
            real_check_banzu_data,
        ],
        axis=0,
        sort=False)
    data_real = pd.merge(
        data_real,
        department_data,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')

    data_real = data_real.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data_real.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data_real = data_real[data_real['DEPARTMENT_ID'].isin(dpids)]
    return data_real


def _calc_check_address(
    _sqllist_numerator, 
    zhanduan_dpid_data, 
    department_data, 
    mon_ago, column, dpids):
    import pandas as pd
    banzu_point_data = pd_query(_sqllist_numerator[0])
    check_point_data = pd_query(_sqllist_numerator[1])

    # 地点总数
    data_total = pd.concat(
        [

            check_point_data,
            banzu_point_data,
        ],
        axis=0,
        sort=False)
    data_total = pd.merge(
        data_total, 
        department_data,
        left_on = 'FK_DEPARTMENT_ID',
        right_on = 'DEPARTMENT_ID',
        how='inner')

    data_total = data_total.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data_total.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data_total = data_total[data_total['DEPARTMENT_ID'].isin(dpids)]
    return data_total


def _calc_shigong_count(
    _sqllist_numerator, 
    zhanduan_dpid_data, 
    department_data, 
    mon_ago, column, dpids
    ):
    stats_month = get_custom_month(mon_ago)
    # 劳安作业人数（总上道人数）
    shigong_count = pd.merge(
        pd_query(_sqllist_numerator[0].format(*stats_month), db_name='db_mid'),
        department_data, 
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')
    shigong_count = shigong_count.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    shigong_count.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    shigong_count = shigong_count[shigong_count['DEPARTMENT_ID'].isin(dpids)]
    return shigong_count


def _calc_filter_with_check_info_id(
        _sqllist_numerator,
        zhanduan_dpid_data,
        department_data,
        mon_ago, column, dpids):
    stats_month = get_custom_month(mon_ago)
    check_info_ids = get_all_check_info_ids(mon_ago, _sqllist_numerator[1])
    # 劳安作业人数（总上道人数）
    data = pd.merge(
        filter_with_check_info_id(
        pd_query(_sqllist_numerator[0].format(*stats_month)),
        check_info_ids
        ),
        department_data,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data