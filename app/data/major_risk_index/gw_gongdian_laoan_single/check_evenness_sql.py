# 一般以上项点问题数
# PROBLEM_DIVIDE_IDS in(4) 其它归类id为4表示劳安
from app.data.major_risk_index.gongwu_laoan.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, CHECK_BANZU_COUNT_SQL, BANZU_CONNECT_DEPARTMENT_SQL,
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_POINT_COUNT_SQL, CHECK_POINT_CONNECT_DEPARTMENT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    DAILY_CHECK_BANZU_COUNT_SQL, )

# 重要检查点受检次数-不关联项目
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID AS CHECK_POINT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
        inner join
        t_check_info_and_item as tcia on a.FK_CHECK_INFO_ID = tcia.FK_CHECK_INFO_ID
        inner join
        (
        select tci.PK_ID from t_check_item as tci
        inner join
        t_department as td on tci.FK_DEPARTMENT_ID = td.DEPARTMENT_ID
        where td.is_delete = 0 and td.type2 = '{2}') 
        as fgs on tcia.FK_CHECK_ITEM_ID = fgs.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND c.TYPE = 1
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND tcia.FK_CHECK_ITEM_ID in ({3})
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS PERSON_COUNT_2
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
    GROUP BY a.DEPARTMENT_ID;
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME
    FROM
        t_department
    WHERE
        TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND TYPE2 = '{0}'
"""

# 班组受检次数-不关联项目
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            inner join
        t_check_info_and_item as tcia on a.FK_CHECK_INFO_ID = tcia.FK_CHECK_INFO_ID
        inner join
        (
        select tci.PK_ID from t_check_item as tci
        inner join
        t_department as td on tci.FK_DEPARTMENT_ID = td.DEPARTMENT_ID
        where td.is_delete = 0 and td.type2 = '{2}') 
        as fgs on tcia.FK_CHECK_ITEM_ID = fgs.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
            AND tcia.FK_CHECK_ITEM_ID IN ({3})
    GROUP BY c.DEPARTMENT_ID;
"""

# 每日检查数(关联项目)
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
    inner join
        t_check_info_and_item as tcia on a.PK_ID = tcia.FK_CHECK_INFO_ID
        inner join
        (
        select tci.PK_ID from t_check_item as tci
        inner join
        t_department as td on tci.FK_DEPARTMENT_ID = td.DEPARTMENT_ID
        where td.is_delete = 0 and td.type2 = '{2}') 
        as fgs on tcia.FK_CHECK_ITEM_ID = fgs.PK_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND tcia.FK_CHECK_ITEM_ID IN ({3})
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""