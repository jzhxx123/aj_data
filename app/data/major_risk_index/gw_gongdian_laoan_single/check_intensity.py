# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gw_gongdian_laoan_single import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongwu_laoan.check_intensity_sql import (
    BANZU_POINT_SQL, MEDIA_COST_TIME_SQL,
    RISK_LEVEL_PROBLEM_SQL, REAL_CHECK_POINT_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    CHECK_POINT_SQL, CHECK_COUNT_SQL,
    REAL_CHECK_BANZU_SQLIST, CHECK_INFO_SQL,
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL)
from app.data.major_risk_index.gongwu_laoan.assess_intensity_sql import (KAOHE_PROBLEM_SQL)
from app.data.major_risk_index.gw_gongdian_laoan_single.common import (
    calc_total_workload, get_vitual_major_ids, calc_night_work_load,
    get_all_check_item_ids,
    filter_with_check_info_id)
from app.data.major_risk_index.gw_gongdian_laoan_single.common_sql import (
    TOTAL_LABORTIME_SQL,
    ORDER_COUNT_SQL, WORK_TIME_RECORD_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query
from app.data.util import update_major_maintype_weight
import pandas as pd


def _clac_func(row, column, major_column, detail_type=None):
    """[计算覆盖率]
    
    Arguments:
        row {[df.row]} -- [description]
    """
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >=0.15:
        _score = 100
    elif _ratio >=0:
        _score = 90 + _ratio * 50
    elif _ratio >= -0.15:
        _score = 90 + _ratio * 66
    elif _ratio >= -0.3:
        _score = 80 + (_ratio + 0.15) * 160
    else:
        _score = 71 + _ratio * 50
    return _score


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    major = get_major_dpid('工务-2')
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT, \
        ASSESS_PROBLEM_COUNT, RISK_IDS, YEJIAN_WORK_LOAD, \
        REAL_CHECK_BANZU_DATA, REAL_CHECK_POINT_DATA, \
        BANZU_POINT_DATA, CHECK_POINT_DATA
        
    ids = get_vitual_major_ids("工电-1")
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = get_all_check_item_ids(major)
    risk_ids = diaoche[1]
    RISK_IDS = risk_ids

    # 统计工作量【人时+派单数】
    WORK_LOAD = calc_total_workload(
        pd_query(TOTAL_LABORTIME_SQL.format(*stats_month), db_name='db_mid'),
        pd_query(ORDER_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 夜间工作量
    YEJIAN_WORK_LOAD = calc_night_work_load(
        pd_query(WORK_TIME_RECORD_SQL.format(*stats_month), db_name='db_mid'),
                 DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        filter_with_check_info_id(
            pd_query(CHECK_COUNT_SQL.format(*stats_month)),
            GLV.get_value("CHECK_INFO_IDS")),
        DEPARTMENT_DATA)

    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        filter_with_check_info_id(
            pd_query(YECHA_CHECK_SQL.format(*stats_month)),
            GLV.get_value("CHECK_INFO_IDS")),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, risk_ids)),
                DEPARTMENT_DATA)

    REAL_CHECK_BANZU_DATA = pd.merge(
        pd_query(REAL_CHECK_BANZU_SQLIST[0].format(major)),
        pd_query(CHECK_INFO_SQL.format(*stats_month)),
        how='inner',
        left_on='FK_CHECK_INFO_ID', 
        right_on='PK_ID'
    )
    REAL_CHECK_BANZU_DATA = REAL_CHECK_BANZU_DATA[
        REAL_CHECK_BANZU_DATA['PK_ID'].isin(GLV.get_value("CHECK_INFO_IDS"))]
    REAL_CHECK_BANZU_DATA.drop(["PK_ID","FK_CHECK_INFO_ID"], inplace=True, axis=1)
    REAL_CHECK_BANZU_DATA.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)

    REAL_CHECK_POINT_DATA = filter_with_check_info_id(
        pd_query(REAL_CHECK_POINT_SQL.format(*stats_month)),
        GLV.get_value("CHECK_INFO_IDS"))
    BANZU_POINT_DATA = pd_query(BANZU_POINT_SQL.format(major))
    CHECK_POINT_DATA = pd_query(CHECK_POINT_SQL)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = "+ \
    "量化人员及干部检查次数({4})/ 劳安工作量({5})</p>", None]
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_clac_func,
        fraction=fraction)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题考核率({3}) = "+ \
        "考核问题数({4})/ 发现问题数({5})</p>", None]
    fraction = GLV.get_value('1-4', (None,))[0]
    return check_intensity.stats_check_problem_assess_radio_type_one(
        ASSESS_PROBLEM_COUNT,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_clac_func,
        customizecontent=customizecontent,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """
    （70%）较大和重大安全风险问题质量分累计/劳安工作量。
    （30%）现场检查较大风险问题质量均分：较大和重大安全风险问题质量分累计（现场检查问题）/劳安工作量。
    :param months_ago:
    :return:
    """
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None,None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_clac_func,
        fraction_list=fraction_list)


# 质量均分
def _stats_score_per_person(months_ago):
    """
    问题质量分累计/劳安工作量
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = "+ \
        "问题质量分累计({4})/ 工作量({5})</p>", None]
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_clac_func,
        customizecontent=customizecontent,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    """
    夜查次数/夜间劳安工作量
    :param months_ago:
    :return:
    """
    content = ['得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '\
        + '夜查次数({4})/ 夜间劳安工作量({5})*100%', None]
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        YEJIAN_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        calc_score_by_formula=_clac_func,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    """
    监控调阅单位工作量：监控调阅时长累计/劳安工作量。
    监控调阅问题单位个数：监控调阅发现问题数/劳安工作量。
    监控调阅问题质量单位均分：监控调阅发现问题质量分累计/劳安工作量。
    监控调阅覆盖比例：调阅班组数/作业班组数
    :param months_ago:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)
    media_problme_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_month, RISK_IDS)
    title = ['监控调阅时长累计({0})/劳安工作量({1})',
               '监控调阅发现问题数({0})/劳安工作量({1})', '监控调阅发现问题质量分累计({0})/劳安工作量({1})',
               '调阅班组数({0})/作业班组数({1})']
    fraction_list = GLV.get_value('stats_media_intensity', (None,None,None))
    fraction_list = (
        fraction_list[0], fraction_list[1], 
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problme_score_sql,
        monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        title=title,
        calc_score_by_formula=_clac_func,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """
    检查地点数/地点总数（作业班组）。得分=比例*100
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = " + \
        "检查地点数({4})/ 地点总数（作业班组数）({5})*100%</p>", None]
    fraction = GLV.get_value('stats_check_address_ratio', (None,))[0]
    return check_intensity.stats_check_address_ratio_include_check_point_excellent(
        REAL_CHECK_BANZU_DATA,
        REAL_CHECK_POINT_DATA,
        BANZU_POINT_DATA,
        CHECK_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_func=_clac_func,
        is_calc_score_base_major=True,
        customizecontent=customizecontent,
        fraction=fraction)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_assess_radio,
        _stats_risk_score_per_person,
        _stats_score_per_person, _stats_yecha_ratio,
        _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'd', 'e', 'f', 'g', 'i', 'j']
    ]
    item_weight = [0.25, 0.15, 0.1, 0.25, 0.07, 0.03, 0.15]
    update_major_maintype_weight(index_type=7, major=risk_type, main_type=1,
                                 child_index_list=[2, 4, 5, 6, 7, 9, 10],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
