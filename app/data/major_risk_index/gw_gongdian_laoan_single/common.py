#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from app.data.util import pd_query
import datetime
import pandas as pd
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.gongwu_laoan.check_intensity_sql import (
    CHECK_INFO_AND_ITEM_SQL, ALL_CHECK_ITEM_IDS_SQL)

def calc_work_load(on_railway_data, xianchang_data, department_data):
    """工作量的统计：工务生产系统中所有上道人时（占比60%）+现场检查人时（占比40%）。
    (其中人时=上道人数或现场检查人数*作业时间）。
    """
    work_hour = pd.concat(
        [on_railway_data, xianchang_data], axis=0, sort=False)
    work_hour.fillna(0, inplace=True)
    work_hour = work_hour.groupby(['FK_DEPARTMENT_ID'])['HOURS'].sum()
    work_hour_department = pd.merge(
        work_hour.to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return work_hour_department


def calc_total_workload(labortime_data, order_count, dpid_data):
    """
    按照要求计算工务施工安全的工作量=作业人时+次数（其中人时=作业人数*作业时间、占比85%,次数=派工单数量、占比15%
    上道人时按51万*0.85，派工单按7千*0.15测算一次看看结果
    :return:
    """
    labortime_data['COUNT'] = labortime_data['COUNT'] * 0.85 / 510000
    order_count['COUNT'] = order_count['COUNT'] * 0.15 / 7000
    work_load = labortime_data.append(order_count)
    work_load = work_load.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    data = pd.merge(
        work_load,
        dpid_data,
        how='inner',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    FK_PROFESSION_DICTIONARY_ID = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(major,2140)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_work_load_oneday(time, flag=0):
    workload_a = 0
    zero = (datetime.datetime.strptime(time[:10] + '00:00:00',
                                       "%Y-%m-%d%H:%M:%S")).timestamp()
    six = (datetime.datetime.strptime(time[:10] + '06:00:00',
                                      "%Y-%m-%d%H:%M:%S")).timestamp()
    twentytw0 = (datetime.datetime.strptime(time[:10] + '22:00:00',
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
    twentyfour = (datetime.datetime.strptime(time[:10] + '23:59:59',
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    calc_value = (datetime.datetime.strptime(time,
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    if flag ==0:
        if zero <= calc_value <= six:
            workload_a = six - calc_value + twentyfour - twentytw0
        elif six < calc_value <= twentytw0:
            workload_a = twentyfour - twentytw0
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = twentyfour - calc_value
    else:
        if zero <= calc_value <= six:
            workload_a = calc_value - zero
        elif six < calc_value <= twentytw0:
            workload_a = six - zero
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = calc_value - twentytw0 + six - zero
    return workload_a


def _calc_work_load_otherday(start, end, zero, six, twentytw0, twentyfour):
    from datetime import date
    workload = 0
    distance = date(int(end[:4]), int(end[5:7]), int(end[8:10])) - \
               date(int(start[:4]), int(start[5:7]), int(start[8:10]))
    days = int(distance.days)
    if days > 1:
        workload = _calc_work_load_oneday(start) + \
                   (days - 1)*(six - zero + twentyfour - twentytw0) + \
                   _calc_work_load_oneday(end, 1)
    else:
        workload = _calc_work_load_oneday(start) + _calc_work_load_oneday(end, 1)
    return workload


def _calc_work_load_sameday(start, end, zero, six, twentytw0, twentyfour):
    workload = 0
    if zero <= start < end <= six or twentytw0 <= start < end <= twentyfour:
        workload = end - start
    elif start < six < end <= twentytw0:
        workload = six - start
    elif start <= six < twentytw0 < end <= twentyfour:
        workload = six - start + end - twentyfour
    elif six <= start < twentytw0 < end <= twentyfour:
        workload = end - twentytw0
    return workload


def _calc_start_time(row):
    start_times_list = row["START_TIMES"].split(',')
    end_times_list = row['END_TIMES'].split(',')
    work_load_list = []
    for idx in range(len(start_times_list)):
        try:
            start = (datetime.datetime.strptime(start_times_list[idx].replace(' ', ''),
                                                "%Y-%m-%d%H:%M:%S")).timestamp()
            end = (datetime.datetime.strptime(end_times_list[idx].replace(' ', ''),
                                              "%Y-%m-%d%H:%M:%S")).timestamp()
            flag = True if start_times_list[idx][:10] == end_times_list[idx][:10] else False

            six = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '06:00:00',
                                              "%Y-%m-%d%H:%M:%S")).timestamp()
            zero = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '00:00:00',
                                               "%Y-%m-%d%H:%M:%S")).timestamp()
            twentytw0 = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '22:00:00',
                                                    "%Y-%m-%d%H:%M:%S")).timestamp()
            twentyfour = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '23:59:59',
                                                     "%Y-%m-%d%H:%M:%S")).timestamp()
            if flag:
                work_load_list.append(_calc_work_load_sameday(start, end, zero,
                                                  six, twentytw0, twentyfour))
            else:
                work_load_list.append(_calc_work_load_otherday(
                    start_times_list[idx].replace(' ', ''),
                    end_times_list[idx].replace(' ', ''),
                    zero, six, twentytw0, twentyfour))
        except Exception:
            work_load_list.append(0)
            continue
    row["COUNT"] = sum(work_load_list)

    return row


def calc_night_work_load(df_work_time, dpid_data):
    """
    计算夜间工作量
    :param df_work_time:
    :param dpid_data:
    :return:
    """
    df_work_time = df_work_time.apply(
        lambda row: _calc_start_time(row),
        axis=1)
    df_work_time.fillna(0)
    df_work_time["COUNT"] = (df_work_time["COUNT"] * df_work_time["ON_RAILWAY_NUMBER"]) / 3600
    data = pd.merge(
        df_work_time,
        dpid_data,
        how="inner",
        left_on="DEPARTID",
        right_on="DEPARTMENT_ID"
    )
    data.drop(["DEPARTID", "ON_RAILWAY_NUMBER", "END_TIMES", "START_TIMES"], inplace=True, axis=1)
    return data


def get_all_check_item_ids(major_dpid):
    all_check_item_data = pd_query(ALL_CHECK_ITEM_IDS_SQL.format(major_dpid))
    ids_list = all_check_item_data['PK_ID'].values.tolist()
    check_item_ids = ','.join([str(ids) for ids in ids_list])
    return check_item_ids


def get_all_check_info_ids(months_ago, major_dpid):
    stats_month = get_custom_month(months_ago)
    all_check_item_ids = get_all_check_item_ids(major_dpid)
    all_check_item_ids = all_check_item_ids.split(',')
    check_info_and_item_data = pd_query(CHECK_INFO_AND_ITEM_SQL.format(*stats_month))
    check_info_and_item_data = check_info_and_item_data[
        check_info_and_item_data['FK_CHECK_ITEM_ID'].isin(all_check_item_ids)]
    check_info_ids = set(check_info_and_item_data['PK_ID'].values.tolist())
    return list(check_info_ids)


def filter_with_check_info_id(raw_data, check_info_ids):
    raw_data = raw_data[raw_data['PK_ID'].isin(check_info_ids)]
    raw_data.drop(['PK_ID'], inplace=True, axis=1)
    return raw_data