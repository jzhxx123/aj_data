#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

from flask import current_app
from app.data.major_risk_index.jiwu_diaoche import GLV
from app.data.index.util import (
    get_custom_month)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.jiwu_diaoche.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL,
    EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    CHECK_INFO_AND_CHECK_POINT_SQL
)
from app.data.major_risk_index.jiwu_diaoche.common_sql import (
    CHECK_POINT_SQL
)
from datetime import datetime as dt
import calendar
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import (
    summizet_child_index,
    combine_child_index_func,
    calc_child_index_type_sum, df_merge_with_dpid, format_export_basic_data,
    write_export_basic_data_to_mongo)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT,\
        CHECK_POINT_DATA, WORK_LOAD_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    check_point_ids = GLV.get_value("CHECK_POINT_IDS")
    diaoche = GLV.get_value('RISK_CONFIGS')
    risk_ids = diaoche[1]
    global CHECK_ITEM_IDS, CHECK_POINT_IDS
    CHECK_ITEM_IDS = diaoche[0]
    CHECK_POINT_IDS = check_point_ids
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                risk_ids)), DEPARTMENT_DATA)

    CHECK_POINT_DATA = pd_query(CHECK_POINT_SQL.format(major))
    CHECK_POINT_DATA = CHECK_POINT_DATA[
        CHECK_POINT_DATA['FK_CHECK_POINT_ID'].isin(check_point_ids.split(','))
    ]

    WORK_LOAD_DATA = df_merge_with_dpid(
        GLV.get_value("IO_STATION_DATA"),
        DEPARTMENT_DATA
    )


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)

def _calc_check_time_evenness(base, daily_work_load, check_data):
    """
    计算得到某个段的得分，以及文字描述
    Args:
        base:
        daily_work_load: 每天工作量
        check_data:

    Returns:

    """
    score = 0
    content_dict = {
        "检查低于基数40%的日期": '',
        "检查低于基数60%的日期": '',
        "检查低于基数100%的日期": '',
    }
    for day in set(check_data['DATE'].values.tolist()):
        # 当天检查 检查次数
        a = check_data[
            check_data['DATE'] == day
            ]['COUNT'].sum()
        ratio = (a / daily_work_load - base) / base
        d = ''
        if ratio <= -1:
            c = 2
            d = '检查低于基数100%的日期'
        elif ratio <= -0.6:
            c = 1
            d = '检查低于基数60%的日期'
        elif ratio <= -0.4:
            c = 0.5
            d = '检查低于基数40%的日期'
        else:
            c = 0
        score += c
        if d:
            content_dict[d] += f"<p>检查日期:{day}, 当日检查次数:{a}, 当日工作量:{daily_work_load}</p>"
    content = f"段比较值:{base}\n"
    for a, b in content_dict.items():
        content += a + ':\n' + b
    return content, score


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    stats_month = get_custom_month(months_ago)
    # 当月天数
    days = calendar.monthrange(
        dt.strptime(stats_month[0], '%Y-%m-%d').year,
        dt.strptime(stats_month[0], '%Y-%m-%d').month
    )[1] - int(dt.strptime(stats_month[0], '%Y-%m-%d').day) + int(dt.strptime(stats_month[1], '%Y-%m-%d').day)
    check_info_and_check_point_data = pd_query(CHECK_INFO_AND_CHECK_POINT_SQL.format(*stats_month))
    # 去重
    check_info_and_check_point_data.drop_duplicates(
        subset=['FK_CHECK_POINT_ID', 'ID_CARD', 'FK_CHECK_INFO_ID'], inplace=True
    )
    # 筛选符合条件的地点
    check_info_and_check_point_data = check_info_and_check_point_data[
        check_info_and_check_point_data['FK_CHECK_POINT_ID'].isin(CHECK_POINT_IDS.split(','))
    ]
    # 新增字段 检查日期（20190910），检查次数
    # 检查日期以提交时间日期为准
    check_info_and_check_point_data['DATE'] = 0
    check_info_and_check_point_data['COUNT'] = 1
    for idx, row in check_info_and_check_point_data.iterrows():
        start = dt.strptime(str(row['START_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        end = dt.strptime(str(row['END_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        # 判断是否跨天
        if start != end:
            check_info_and_check_point_data.loc[idx, 'COUNT'] = 2
        # 检查日期
        submit_time = dt.strptime(str(row['SUBMIT_TIME']), '%Y-%m-%d %H:%M:%S')
        check_info_and_check_point_data.loc[idx, 'DATE'] = str(dt.strftime(submit_time, "%Y%m%d"))
    rst = []
    for _, station in ZHANDUAN_DPID_DATA.iterrows():
        # 段 全月调车检查次数
        a = check_info_and_check_point_data[
            check_info_and_check_point_data['CHECK_STATION_ID'] == station['DEPARTMENT_ID']
        ]['COUNT'].sum()
        # 段 全月工作量
        b = WORK_LOAD_DATA[
            WORK_LOAD_DATA['TYPE3'] == station['DEPARTMENT_ID']
        ]['COUNT'].sum()
        # 计算得到这个段的描述内容， 分数
        content, score = _calc_check_time_evenness(a/b, b/days, check_info_and_check_point_data)
        rst.append(
            {
                'FK_DEPARTMENT_ID': station['DEPARTMENT_ID'],
                'CONTENT': content,
                'SCORE': score,
                'MAJOR': station['MAJOR'],
                'DEPARTMENT_ID': station['DEPARTMENT_ID'],
            }
        )
    rst_data = pd.DataFrame(rst)
    data_rst = format_export_basic_data(
        rst_data[['MAJOR', 'DEPARTMENT_ID', 'CONTENT']],
        4,
        2,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     4, 2, RISK_TYPE)

    rst_data = df_merge_with_dpid(rst_data[['FK_DEPARTMENT_ID', 'SCORE']], DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        rst_data,
        2,
        4,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


def _calc_check_address_evenness(base, check_data, check_point_data):
    """
    计算检查地点均衡度，分数，以及整理文字描述
    Args:
        base: 比较值
        check_data: 某个段检查信息数据

    Returns:
    content, score
    """
    score = 0
    content_dict = {
        "受检次数超过比较值500%的地点": '',
        "受检次数低于比较值20%的地点": '',
        "未检查的地点": '',
    }
    for _, row in check_point_data.iterrows():
        a = check_data[
            check_data['FK_CHECK_POINT_ID'] == row['FK_CHECK_POINT_ID']
        ]
        # 该点受检次数
        n = 0 if a.empty else a['COUNT'].sum()
        a = n / base
        d = ''
        if n == 0:
            c = 10
            d = "未检查的地点"
        elif a > 5:
            c = 1
            d = "受检次数超过比较值500%的地点"
        elif a < 0.2:
            c = 1
            d = "受检次数低于比较值20%的地点"
        else:
            c = 0
        score += c
        if d:
            content_dict[d] += f"<p>检查地点:{row['ALL_NAME']}, 受检查次数:{n}</p>"
    content = f"段比较值:{base}\n"
    for a, b in content_dict.items():
        content += a + ':\n' + b
    return content, score


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    stats_month = get_custom_month(months_ago)
    # 当月天数
    days = calendar.monthrange(
        dt.strptime(stats_month[0], '%Y-%m-%d').year,
        dt.strptime(stats_month[0], '%Y-%m-%d').month
    )[1] - int(dt.strptime(stats_month[0], '%Y-%m-%d').day) + int(dt.strptime(stats_month[1], '%Y-%m-%d').day)
    check_info_and_check_point_data = pd_query(CHECK_INFO_AND_CHECK_POINT_SQL.format(*stats_month))
    # 去重
    check_info_and_check_point_data.drop_duplicates(
        subset=['FK_CHECK_POINT_ID', 'ID_CARD', 'FK_CHECK_INFO_ID'], inplace=True
    )
    # 筛选符合条件的地点
    check_info_and_check_point_data = check_info_and_check_point_data[
        check_info_and_check_point_data['FK_CHECK_POINT_ID'].isin(CHECK_POINT_IDS.split(','))
    ]
    # 新增字段 检查次数
    check_info_and_check_point_data['COUNT'] = 1
    for idx, row in check_info_and_check_point_data.iterrows():
        start = dt.strptime(str(row['START_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        end = dt.strptime(str(row['END_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        # 判断是否跨天
        if start != end:
            check_info_and_check_point_data.loc[idx, 'COUNT'] = 2
    # 聚合各个检查点的检查次数
    check_info_and_check_point_data = check_info_and_check_point_data.groupby(
        ['CHECK_STATION_ID', 'FK_CHECK_POINT_ID']
    )['COUNT'].sum().reset_index()

    rst = []
    for _, station in ZHANDUAN_DPID_DATA.iterrows():
        # 段 全月调车检查次数
        a = check_info_and_check_point_data[
            check_info_and_check_point_data['CHECK_STATION_ID'] == station['DEPARTMENT_ID']
            ]['COUNT'].sum()
        # 段 所有检查地点数
        b = CHECK_POINT_DATA[
            CHECK_POINT_DATA['FK_DEPARTMENT_ID'] == station['DEPARTMENT_ID']
            ]['FK_CHECK_POINT_ID'].values.tolist()
        b = len(set(b))
        # 计算得到这个段的描述内容， 分数
        content, score = _calc_check_address_evenness(
            a / b,
            check_info_and_check_point_data[
                check_info_and_check_point_data['CHECK_STATION_ID'] == station['DEPARTMENT_ID']
            ],
            CHECK_POINT_DATA[
                CHECK_POINT_DATA['FK_DEPARTMENT_ID'] == station['DEPARTMENT_ID']
            ])
        rst.append(
            {
                'FK_DEPARTMENT_ID': station['DEPARTMENT_ID'],
                'CONTENT': content,
                'SCORE': score,
                'MAJOR': station['MAJOR'],
                'DEPARTMENT_ID': station['DEPARTMENT_ID'],
            }
        )
    rst_data = pd.DataFrame(rst)
    data_rst = format_export_basic_data(
        rst_data[['MAJOR', 'DEPARTMENT_ID', 'CONTENT']],
        4,
        3,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     4, 3, RISK_TYPE)

    rst_data = df_merge_with_dpid(rst_data[['FK_DEPARTMENT_ID', 'SCORE']], DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        rst_data,
        2,
        4,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


# 关键时段检查均衡度
def _stats_key_times_check_evenness_check_point(months_ago):
    return check_evenness.stats_key_times_check_evenness_check_point(
        CHECK_INFO_AND_CHECK_POINT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA,
        CHECK_POINT_DATA,
        months_ago, RISK_TYPE,
        _choose_dpid_data
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'f']]
    item_weight = [0.15, 0.25, 0.3, 0.3]
    update_major_maintype_weight(
        index_type=int(risk_type.split('-')[1]),
        major=risk_type,
        main_type=4,
        child_index_list=['a', 'b', 'c', 'f'],
        child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
