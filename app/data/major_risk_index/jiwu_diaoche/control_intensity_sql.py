#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   control_intensity_sql
Description:
Author:    
date:         2019/12/17
-------------------------------------------------
Change Activity:2019/12/17 6:34 下午
-------------------------------------------------
"""

CHECK_PROBLEM_AND_POINT_SQL = """
    SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    e.ID_CARD,
    b.LEVEL,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    c.TYPE,
    c.FK_CHECK_POINT_ID,
    f.FK_DEPARTMENT_ID AS TYPE3
     FROM 
    t_check_info as a 
        inner join
    t_check_problem as b on a.PK_ID = b.FK_CHECK_INFO_ID
        inner join
    t_check_info_and_address as c on c.FK_CHECK_INFO_ID = a.PK_ID
        inner join
    t_check_problem_and_risk as d on b.PK_ID = d.FK_CHECK_PROBLEM_ID
        inner join
    t_person as e on e.ID_CARD = b.CHECK_PERSON_ID_CARD
        inner join
	t_check_point as f on c.FK_CHECK_POINT_ID = f.PK_ID
    where 
    d.FK_RISK_ID IN ({2})
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.type = 2
"""

# 运用车间信息
APPLY_WORKSHOP_INFO_SQL = """
select 
DEPARTMENT_ID as FK_DEPARTMENT_ID
from t_department 
where 
FK_DEPARTMENT_CLASSIFY_CONFIG_ID in ({0})
 and TYPE5 is null
 AND IS_DELETE = 0
"""

# 部门信息
DEPARTMENT_INFO_SQL = """SELECT
        a.DEPARTMENT_ID, 
        a.TYPE3, 
        a.TYPE4, 
        a.TYPE5, 
        a.ALL_NAME
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

CHECK_PROBLEM_AND_BANZU_SQL = """
    SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    e.ID_CARD,
    b.LEVEL,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    c.TYPE,
    e.FK_DEPARTMENT_ID as CHECK_DEPARTMENT_ID,
    f.TYPE3
     FROM 
    t_check_info as a 
        inner join
    t_check_problem as b on a.PK_ID = b.FK_CHECK_INFO_ID
        inner join
    t_check_info_and_address as c on c.FK_CHECK_INFO_ID = a.PK_ID
        inner join
    t_check_problem_and_risk as d on b.PK_ID = d.FK_CHECK_PROBLEM_ID
        inner join
    t_person as e on e.ID_CARD = b.CHECK_PERSON_ID_CARD
        inner join
    t_department as f on e.FK_DEPARTMENT_ID = f.DEPARTMENT_ID
    where 
    d.FK_RISK_ID IN ({2})
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.type = 1
"""