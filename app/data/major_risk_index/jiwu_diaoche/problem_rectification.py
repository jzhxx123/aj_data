# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
from flask import current_app
from app.data.major_risk_index.jiwu_diaoche import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.common.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.jiwu_diaoche.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL, TOTAL_PROBLEM_NUMBER_SQL,
    WARINING_NOTIFICATION_COUNT_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL
)
from app.data.index.util import (get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.util import (
    data_complete_by_condition, get_major_index_data_bymon, df_merge_with_dpid)
import pandas as pd

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
            PROBLEM_COUNT, TOTAL_PROBLEM_COUNT, WARINING_NOTIFICATION_COUNT_DATA,\
            DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA, MAJOR_INDEX_RANK_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value("ZHANDUAN_DPID_DATA")
    CHEJIAN_DPID_DATA = GLV.get_value("CHEJIAN_DPID_DATA")
    DEPARTMENT_DATA = GLV.get_value("DEPARTMENT_DATA")
    risktype_data = GLV.get_value("RISK_CONFIGS")
    stats_months = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    PROBLEM_COUNT =df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_months, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )

    TOTAL_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(TOTAL_PROBLEM_NUMBER_SQL.format(*stats_months)),
        DEPARTMENT_DATA
    )

    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_months)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )

    WARINING_NOTIFICATION_COUNT_DATA = data_complete_by_condition(
        pd_query(WARINING_NOTIFICATION_COUNT_SQL.format(*stats_months)),
        ZHANDUAN_DPID_DATA,
        ['TYPE', 'RANK'],
        [1, 1],
        DEPARTMENT_DATA
    )

    # 过去4个月重点指数排名数据
    MAJOR_INDEX_RANK_DATA = []
    for i in range(1, 5):
        MAJOR_INDEX_RANK_DATA.extend(
            get_major_index_data_bymon(months_ago - i, risk_type)
        )
    MAJOR_INDEX_RANK_DATA = pd.DataFrame(MAJOR_INDEX_RANK_DATA)
    # 避免空数据，做一些初始化操作
    if MAJOR_INDEX_RANK_DATA.empty:
        a = [months_ago - i for i in range(1, 5)]
    else:
        a = set([months_ago - i for i in range(1, 5)]) - set(MAJOR_INDEX_RANK_DATA['MON_AGO'].values.tolist())
    for i in a:
        for _, j in ZHANDUAN_DPID_DATA.iterrows():
            MAJOR_INDEX_RANK_DATA = MAJOR_INDEX_RANK_DATA.append(
                [{
                        'DEPARTMENT_ID': j['DEPARTMENT_ID'],
                        'MON_AGO': i,
                        'RANK': 0,
                        "DEPARTMENT_NAME": j['NAME']
                    }], ignore_index=True
            )


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        _score = int(row['COUNT_A']) * (10 * 2**(3 - int(row['RESP_LEVEL'])))
    return _score


def _calc_rectification_effect_type_d(row):
    """[计算根据级别的预警通知书]
    III级警告预警通知书一次扣5分；
    II级警告预警通知书一次扣10分；
    I级警告预警通知书一次扣20分。
    注:警告通知书的层级为“路局”或“专业”，不包括“站段”。
    Arguments:
        row {[type]} -- [description]
    """
    _score = int(row['COUNT_D']) * 5 * 2**(3 - int(row['RANK']))
    return _score


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_ITEM_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def _stats_repeatedly_index_ratio(months_ago):
    fraction = GLV.get_value("stats_repeatedly_index_ratio")[0]
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题控制率({3}) = " + \
                       "段调车风险问题数({4})/ 段问题总数({5})*100%</p>"
    return problem_rectification.stats_repeatedly_index_ratio(
        PROBLEM_COUNT, TOTAL_PROBLEM_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data, fraction=fraction,
        customizecontent=customizecontent)


def _stats_rectification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,
        'D': WARINING_NOTIFICATION_COUNT_DATA,
        'E': MAJOR_INDEX_RANK_DATA,
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a,
        'D': _calc_rectification_effect_type_d
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        df_dict,rectification_type_calc_func_dict=rectification_type_calc_func_dict,
        calc_score_by_formula=lambda x: 60 if x > 60 else x)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_rectification_effect,
        _stats_repeatedly_index_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'f']]
    item_weight = [0.3, 0.7, -1]
    update_major_maintype_weight(index_type=int(risk_type.split('-')[1]),
                                                major=risk_type,
                                                main_type=6,
                                                child_index_list=['a', 'c', 'f'],
                                                child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
