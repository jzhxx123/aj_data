#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   control_intensity
Description:
Author:    
date:         2019/12/17
-------------------------------------------------
Change Activity:2019/12/17 6:34 下午
-------------------------------------------------
"""

from flask import current_app
from app.data.major_risk_index.jiwu_diaoche import GLV
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import control_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_diaoche.control_intensity_sql import (
    CHECK_PROBLEM_AND_POINT_SQL, APPLY_WORKSHOP_INFO_SQL, DEPARTMENT_INFO_SQL,
    CHECK_PROBLEM_AND_BANZU_SQL)
from app.data.major_risk_index.jiwu_diaoche.common_sql import (
    CHECK_POINT_SQL
)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.jiwu_diaoche.common import (
    retrieve_all_dp_configid)
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, CHECK_POINT_DATA, APPLY_WORKSHOP_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value("ZHANDUAN_DPID_DATA")
    CHEJIAN_DPID_DATA = GLV.get_value("CHEJIAN_DPID_DATA")
    DEPARTMENT_DATA = GLV.get_value("DEPARTMENT_DATA")
    stats_month = get_custom_month(months_ago)
    diaoche = GLV.get_value("RISK_CONFIGS")
    check_point_ids = GLV.get_value("CHECK_POINT_IDS")
    risk_ids = diaoche[1]
    global RISK_IDS
    RISK_IDS = risk_ids
    # todo 之后都需要将这些部门配置信息，做成配置文件类型
    JIWU_APPLY_WORKSHOP_PARENTID = '10'
    apply_workshop_ids = retrieve_all_dp_configid(JIWU_APPLY_WORKSHOP_PARENTID)

    # 运用车间信息
    apply_workshop_info = pd_query(APPLY_WORKSHOP_INFO_SQL.format(apply_workshop_ids))

    # 所有运用车间以及一下班组信息
    apply_workshop_info = pd.merge(
        apply_workshop_info,
        pd_query(DEPARTMENT_INFO_SQL.format(major)),
        left_on='FK_DEPARTMENT_ID',
        right_on='TYPE4',
        how='inner'
    )

    apply_workshop_info.drop(['FK_DEPARTMENT_ID'], inplace=True, axis=1)
    apply_workshop_info.rename(columns={
        "DEPARTMENT_ID": 'CHECK_DEPARTMENT_ID',
        }, inplace=True)
    CHECK_POINT_DATA = pd_query(CHECK_POINT_SQL.format(major))
    CHECK_POINT_DATA, APPLY_WORKSHOP_DATA = CHECK_POINT_DATA[
                                                CHECK_POINT_DATA['FK_CHECK_POINT_ID'].isin(check_point_ids.split(','))
                                            ], apply_workshop_info


def _stats_control_quality(months_ago):
    return control_intensity.stats_control_quality(
        RISK_IDS,
        [CHECK_PROBLEM_AND_POINT_SQL, CHECK_PROBLEM_AND_BANZU_SQL],
        [CHECK_POINT_DATA, APPLY_WORKSHOP_DATA],
        DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        title_list=["问题卡控", "AB类问题卡控"], address_type=1,
        months=6
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_control_quality
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    child_index_list = [1]
    update_major_maintype_weight(index_type=int(risk_type.split('-')[1]),
                                 major=risk_type,
                                 main_type=14,
                                 child_index_list=child_index_list, child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        14,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')


if __name__ == '__main__':
    pass
