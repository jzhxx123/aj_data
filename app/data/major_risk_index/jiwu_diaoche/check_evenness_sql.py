#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql
Description:
Author:    
date:         2019/12/16
-------------------------------------------------
Change Activity:2019/12/16 4:34 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL, EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL
)

DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103) 
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 检查信息跟地点关联
CHECK_INFO_AND_CHECK_POINT_SQL = """
SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    b.FK_CHECK_POINT_ID,
    d.FK_DEPARTMENT_ID AS CHECK_STATION_ID,
    c.ID_CARD,
    a.START_CHECK_TIME,
    a.END_CHECK_TIME,
    a.SUBMIT_TIME
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_point as d on b.FK_CHECK_POINT_ID = d.PK_ID
WHERE
    b.type = 2
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102 , 103)
"""