#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql
Description:
Author:    
date:         2019/12/16
-------------------------------------------------
Change Activity:2019/12/16 2:24 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    PROBLEM_CHECK_SCORE_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL
)

# 检查次数（现场检查）
# 检查信息里默认不要102，103等
# 调车检查次数（检查地点为“机务-出入库点”、“机务-调车点”的添乘检查和现场检查数
CHECK_COUNT_SQL = """
select d.FK_DEPARTMENT_ID, COUNT(1) as COUNT from
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_address as c on b.PK_ID = c.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY BETWEEN 1 AND 2
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_POINT_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf
inner join
t_check_info_and_person as d on qf.PK_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_WITH_POINTS_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
        inner join
        t_check_info_and_address as f on b.FK_CHECK_INFO_ID = f.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND f.FK_CHECK_POINT_ID in ({3})
            group by b.pk_id
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.FK_DEPARTMENT_ID,a.PK_ID,b.ID_CARD) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_address AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND c.FK_CHECK_POINT_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅检查
# （调阅类型为“机车视频”、“其他音视频”，且检查地点为“出入库点”、“调车点”的设备监控调阅）
# 监控调阅检查时间
MEDIA_COST_TIME_SQL = """
select d.FK_DEPARTMENT_ID, sum(a.COST_TIME) as TIME from
t_check_info_and_media as a
inner join 
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_address as c on b.PK_ID = c.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_POINT_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf on a.FK_CHECK_INFO_ID = qf.PK_ID
inner join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
and (
a.TYPE = -1 OR 
a.TYPE = 5)
group by d.FK_DEPARTMENT_ID
"""
