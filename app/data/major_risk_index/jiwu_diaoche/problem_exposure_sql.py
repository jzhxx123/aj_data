#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_exposure_sql
Description:
Author:    
date:         2019/12/17
-------------------------------------------------
Change Activity:2019/12/17 4:54 下午
-------------------------------------------------
"""

from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import (
    CHECK_PROBLEM_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL
)

# 质量问题信息
LEVEL_CHECK_PROBLEM_INFO_SQL = """SELECT
        MAX(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as e on a.pk_id = e.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND e.fk_risk_id IN ({2})
        AND b.LEVEL IN {3}
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        group by a.PK_ID
"""

# 质量问题质量分累计信息
LEVEL_PROBLEM_CHECK_SCORE_INFO_SQL = """SELECT
        MAX(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        MAX(b.CHECK_SCORE) as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as e on a.pk_id = e.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND e.fk_risk_id IN ({2})
        AND b.LEVEL IN {3}
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        group by a.PK_ID
"""