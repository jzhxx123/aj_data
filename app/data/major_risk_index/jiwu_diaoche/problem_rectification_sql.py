#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019/12/17
-------------------------------------------------
Change Activity:2019/12/17 8:41 上午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, WARINING_NOTIFICATION_COUNT_SQL
)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL)

# 所有问题数
TOTAL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""

# 责任安全信息
DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.code in ('D1', 'D2', 'D3', 'D4')
"""