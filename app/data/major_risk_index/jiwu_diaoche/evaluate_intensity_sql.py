#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   evaluate_intensity_sql
Description:
Author:    
date:         2019/12/16
-------------------------------------------------
Change Activity:2019/12/16 7:18 下午
-------------------------------------------------
"""
"""
JL-2-1条款、事故故障倒查（是否事故故障评价-是）、自动评价不纳入任何统计，非干部评价不统计。
"""

# 站段干部评价条数
EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 站段非干部评价条数
COMMON_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID NOT BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 站段干部评价记分总分数
EVALUATE_SCORE_ALL_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 站段非干部评价记分总分数
COMMON_EVALUATE_SCORE_ALL_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.FK_PERSON_GRADATION_RATIO_ID NOT BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 路局对站段的评价条数（干部）
LUJU_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 1
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 路局对站段的评价条数（非干部）
COMMON_LUJU_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 1
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID NOT BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 路局对站段的评价分数（干部）
LUJU_EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 1
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 路局对站段的评价分数（非干部）
COMMON_LUJU_EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            inner join  
        t_check_evaluate_config as d on a.FK_CHECK_EVALUATE_CONFIG_ID = d.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 1
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID NOT BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            AND d.IS_TRACEABILITY = 0
    GROUP BY b.FK_DEPARTMENT_ID
"""