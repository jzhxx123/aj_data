# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_diaoche import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.jiwu_diaoche.check_intensity_sql import (
    CHECK_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, YECHA_CHECK_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLME_SCORE_SQL
)

from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = GLV.get_value('RISK_CONFIGS')
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    check_point_ids = GLV.get_value('CHECK_POINT_IDS')
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 统计工作量【出入段（所）数】

    WORK_LOAD = df_merge_with_dpid(
        GLV.get_value("IO_STATION_DATA"),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_point_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_point_ids)),
        DEPARTMENT_DATA)


    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value("stats_check_per_person")[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction)


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value("stats_score_per_person")[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                        "夜查次数({4})/ 工作量({5})*100%</p>"
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=lambda x: min(100, x * 100),
        is_calc_score_base_major=False,
        customizecontent=customizecontent)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_months = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_months, GLV.get_value('CHECK_POINT_IDS'))
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_months, GLV.get_value('RISK_CONFIGS')[1])
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_months, GLV.get_value('RISK_CONFIGS')[1])
    fraction_list = GLV.get_value("stats_media_intensity")
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None,
        None, None
    )
    title = ['监控调阅时长累计({0})/工作量({1})',
             '监控调阅发现问题数({0})/工作量({1})',
             '监控调阅发现问题质量分累计({0})/工作量({1})',
             None, None, None]
    return check_intensity.new_stats_media_intensity(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        choose_dpid_data=_choose_dpid_data,
        child_weight=[0.3, 0.35, 0.35, 0, 0],
        calc_score_by_formula=_calc_score_by_formula,
        fraction_list=fraction_list,
        title=title)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_score_per_person,
        _stats_yecha_ratio,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'e', 'j', 'g']]
    item_weight = [0.35, 0.25, 0.3, 0.1]
    update_major_maintype_weight(index_type=int(risk_type.split('-')[1]), major=risk_type, main_type=1,
                                 child_index_list=['b', 'e', 'j', 'g'],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
