#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   init_common_data
Description:
Author:    
date:         2019/12/16
-------------------------------------------------
Change Activity:2019/12/16 11:59 上午
-------------------------------------------------
"""

from app.data.util import pd_query
from app.data.major_risk_index.jiwu_diaoche import GLV
from app.data.major_risk_index.jiwu_diaoche.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL,
    CHECK_POINT_SQL, IO_STATION_COUNT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_all_sub_dpids)
from app.data.index.util import (get_custom_month, get_query_condition_by_risktype)
import pandas as pd
import datetime
import calendar



def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    stats_month = get_custom_month(months_ago)
    d_time =  datetime.datetime.strptime(
                str(stats_month[1]), "%Y-%m-%d")
    d_time = d_time.date()
    days_num = calendar.monthrange(d_time.year, d_time.month)[1] #获取一个月有多少天
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # TODO 基本工作量（出入段（所）列数）,静态文件数据
    # io_work_load_list = [
    #     # 成都机务段, 贵阳机务段
    #     ['19B8C3534E095665E0539106C00A58FD', 16996], ['19B8C3534E035665E0539106C00A58FD', 25507],
    #     # 重庆机务段, 西昌机务段
    #     ['19B8C3534E285665E0539106C00A58FD', 18042], ['19B9D8D920DD589FE0539106C00A1189', 15271],
    # ]
    # IO_STATION_DATA = pd.DataFrame(data=io_work_load_list, columns=['FK_DEPARTMENT_ID', 'COUNT'])
    IO_STATION_DATA = pd_query(IO_STATION_COUNT_SQL)
    IO_STATION_DATA['COUNT'] = IO_STATION_DATA['COUNT'] * days_num

    # 检查地点（机务-出入库点，机务-调车点）检查点id
    CHECK_POINT_DICT = {
        '机务-出入库点': 13251,
        '机务-调车点': 13254
    }
    CHECK_POINT_DATA = pd_query(CHECK_POINT_SQL.format(major))
    # 向下递归获取检查点
    check_points = []
    for _, value in CHECK_POINT_DICT.items():
        check_points.extend(
            get_all_sub_dpids(str(value), CHECK_POINT_DATA,
                              super_column='PARENT_ID', sub_column='FK_CHECK_POINT_ID')
        )
    check_points = ','.join(check_points)
    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "IO_STATION_DATA": IO_STATION_DATA,
        "CHECK_POINT_IDS": check_points,
        "RISK_CONFIGS": get_query_condition_by_risktype(risk_name),
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
