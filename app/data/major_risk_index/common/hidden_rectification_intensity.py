# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_rectification_intensity_sql
   Author :       hwj
   date：          2019/10/21上午11:28
   Change Activity: 2019/10/21上午11:28
-------------------------------------------------
"""
from datetime import datetime

import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import format_export_basic_data, write_export_basic_data_to_mongo, \
    summizet_operation_set, df_merge_with_dpid


def calc_warning_eliminate_count_data(warning_eliminate_count, zhanduan_dpid_data):
    """
    获取隐患整改力度--道岔设备报警销号指数的基础数据
    :return:
    warning_eliminate: 中间过程数据
    df_rst: 得分数据
    """
    if warning_eliminate_count.empty:
        zhanduan_dpid_data['一级'] = 0
        zhanduan_dpid_data['SCORE_a_3'] = 100
        warning_eliminate = zhanduan_dpid_data
    else:
        # 获取站段名称
        warning_eliminate_count['NAME'] = warning_eliminate_count.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1)
        # warning_eliminate_count.dropna(inplace=True)
        warning_eliminate_count['NAME'].fillna('未配置', inplace=True)

        # 统计各站段一二三级条数及分数
        warning_eliminate_count = warning_eliminate_count.groupby(['NAME', 'WARNING_LEVEL']).size().unstack()
        warning_eliminate_count.fillna(0, inplace=True)
        columns_list = list(warning_eliminate_count.columns)
        for columns in ['一级', '二级', '三级']:
            if columns not in columns_list:
                warning_eliminate_count[columns] = 0
        warning_eliminate = pd.merge(
            warning_eliminate_count,
            zhanduan_dpid_data,
            how='right',
            left_on='NAME',
            right_on='NAME'
        )
        warning_eliminate.fillna(0, inplace=True)
        warning_eliminate['SCORE_a_3'] = warning_eliminate.apply(
            lambda row: max(100 - (row['一级'] * 20), 0), axis=1
        )
    tmp = '一级报警未销号条数:{0:.0f}'
    warning_eliminate['CONTENT'] = warning_eliminate.apply(
        lambda row: tmp.format(row['一级']), axis=1
    )
    df_rst = warning_eliminate.groupby(['DEPARTMENT_ID'])['SCORE_a_3'].sum().to_frame()
    return warning_eliminate, df_rst


# 道岔设备报警销号指数
def stats_warning_eliminate_count_ratio(months_ago,
                                        warning_eliminate_count,
                                        zhanduan_dpid_data,
                                        risk_type,
                                        choose_dpid_data):
    """
    I级报警未销号每条扣20分
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_warning_eliminate_count_data(warning_eliminate_count, zhanduan_dpid_data.copy())

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 10, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 10, 1, risk_type=risk_type)

    # 导出得分
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_a_3',
        3,
        2,
        10,
        1,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def calc_diff_time(group_data):
    """
    获取隐患整改力度--道岔设备报警控制指数-重复发生问题的时间间隔
    :return:
    """
    data_dict = {}
    for k, time in group_data:
        work_shop = k[0]
        work_shop_dict = data_dict.get(work_shop, {})
        time_list = list(time)
        begin_time = time_list[0]
        for i in time_list:
            next_time = i
            if next_time == begin_time:
                day = 0
            else:
                day = (datetime.strptime(next_time, '%Y/%m/%d %H:%M:%S') -
                       datetime.strptime(begin_time, '%Y/%m/%d %H:%M:%S')).days
            if 1 <= day <= 3:
                level = 'high'
            elif 3 < day <= 7:
                level = 'mid'
            elif 7 < day <= 15:
                level = 'low'
            else:
                level = None
            if level:
                work_shop_dict.update({level: work_shop_dict.get(level, 0) + 1})
            begin_time = next_time
        data_dict.update({work_shop: work_shop_dict})
    data = pd.DataFrame(data_dict)
    data = data.unstack().unstack()
    data = data.fillna(0).reset_index()
    data.rename(columns={'index': 'STATION'}, inplace=True)
    return data


def get_warning_control_data(repeated_warning, zhanduan_dpid_data):
    """
    获取隐患整改力度--道岔设备报警控制指数基础数据
    """
    if repeated_warning.empty:
        zhanduan_dpid_data['high'] = 0
        zhanduan_dpid_data['mid'] = 0
        zhanduan_dpid_data['low'] = 0
        data = zhanduan_dpid_data
    else:
        # 获取车站重复次数
        repeated_warning = repeated_warning[repeated_warning.duplicated(
            subset=['STATION', 'EQUIPMENT_NAME', 'WARNING_PROJECT'], keep=False)].copy()
        repeated_warning.sort_values(['STATION', 'EQUIPMENT_NAME', 'WARNING_PROJECT', 'WARNING_TIME'], inplace=True)
        data = repeated_warning.groupby(['STATION', 'EQUIPMENT_NAME', 'WARNING_PROJECT'])['WARNING_TIME']
        station_data = calc_diff_time(data)

        # 获取站段
        repeated_warning['NAME'] = repeated_warning.apply(lambda row: get_work_shop_department(row.WORK_SHOP), axis=1)
        repeated_warning.drop_duplicates(['STATION'], keep='first', inplace=True)
        repeated_warning.drop(columns=['WORK_SHOP', 'EQUIPMENT_NAME', 'WARNING_PROJECT', 'WARNING_TIME'], inplace=True)

        data = pd.merge(
            repeated_warning,
            station_data,
            how='inner',
            on='STATION',
        )

        data.dropna(inplace=True)
        data = data.groupby('NAME')['high', 'mid', 'low'].sum()
        data = pd.merge(
            zhanduan_dpid_data,
            data,
            how='left',
            left_on='NAME',
            right_index=True
        )
        data.fillna(0, inplace=True)

    tmp = '在1-3天内重复发生报警总次数: {0:.0f};<br/>在4-7天内重复发生报警总次数: {1:.0f};<br/>在8-15天内重复发生报警总次数: {2:.0f}'
    data['CONTENT'] = data.apply(lambda row: tmp.format(row.high, row.mid, row.low), axis=1)
    data['SCORE_b_3'] = data.apply(
        lambda row: max(100 - row.high * 10 - row.mid * 6 - row.low * 3, 0), axis=1
    )
    df_rst = data.groupby(['DEPARTMENT_ID'])['SCORE_b_3'].sum().to_frame()
    return data, df_rst


# 道岔设备报警控制指数
def stats_warning_control_ratio(months_ago,
                                repeated_warning,
                                zhanduan_dpid_data,
                                risk_type,
                                choose_dpid_data
                                ):
    """
    同一站名、同一设备、同一报警在1-3天内重复发生，每发生一次扣10分；在4-7天内重复发生，每发生一次扣6分；在8-15天内重复发生，每发生一次扣3分
    """
    rst_child_score = []

    # 获取有重复的条数
    calc_df_data, df_rst = get_warning_control_data(repeated_warning, zhanduan_dpid_data.copy())

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 10, 2, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 10, 2, risk_type=risk_type)

    # 导出得分
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_b_3',
        3,
        2,
        10,
        2,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def get_alarm_disposal_time_count_data(alarm_disposal_time_count, zhanduan_dpid_data):
    """
    获取隐患整改力度--报警处置及时率指数
    :param alarm_disposal_time_count: 告警基础数据
    :param zhanduan_dpid_data: 站段
    :return:
    processing_count: 告警及时处置条数
    all_equipment: 告警总条数
    """
    if alarm_disposal_time_count.empty:
        df_alarm = zhanduan_dpid_data.copy()
        df_alarm['TIMELY_PROCESSING'] = 0
        df_alarm['ACCESSED_QUANTITY'] = 0
        df_alarm['UNACCESSED_QUANTITY'] = 0
        df_alarm['TIMELY_PROCESSING'] = 0
    else:
        alarm_disposal_time_count.set_index('WORK_SHOP', inplace=True)
        alarm_information_analysis = alarm_disposal_time_count.astype(int)
        df_alarm = pd.merge(
            alarm_information_analysis,
            zhanduan_dpid_data,
            left_index=True,
            right_on='NAME',
            how='inner'
        )
    df_alarm['COUNT'] = df_alarm.apply(
        lambda row: row.UNACCESSED_QUANTITY + row.ACCESSED_QUANTITY, axis=1)

    all_equipment = df_alarm[['DEPARTMENT_ID', 'COUNT']].rename(
        columns={'DEPARTMENT_ID': 'TYPE3'})
    processing_count = df_alarm[['DEPARTMENT_ID', 'TIMELY_PROCESSING']].rename(
        columns={'TIMELY_PROCESSING': 'COUNT', 'DEPARTMENT_ID': 'TYPE3'})
    return processing_count, all_equipment


def get_rectification_overdue_data(overdue_problem_number, overdue_days_number, dpid_data, zhanduan_dpid_data):
    """
    获取隐患整改力度--整改时效基础数据
    """
    df_overdue = pd.merge(
        overdue_days_number,
        overdue_problem_number,
        how='outer',
        on='FK_DEPARTMENT_ID'
    )
    df_overdue = df_merge_with_dpid(df_overdue, dpid_data, how='right')
    df_overdue.fillna(0, inplace=True)
    df_overdue = df_overdue.groupby(['TYPE3'])['OVERDUE_PROBLEM', 'OVERDUE_DAYS_NUMBER'].sum()
    df_overdue = pd.merge(
        df_overdue,
        zhanduan_dpid_data,
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    df_overdue.fillna(0, inplace=True)
    tmp = '本月问题超期销号: {0:.0f}条<br/>问题整改超期: {1:.0f}条'
    df_overdue['CONTENT'] = df_overdue.apply(
        lambda row: tmp.format(row.OVERDUE_PROBLEM, row.OVERDUE_DAYS_NUMBER), axis=1
    )
    df_overdue['SCORE_d_3'] = df_overdue.apply(
        lambda row: max(100 - row.OVERDUE_PROBLEM * 1 - row.OVERDUE_DAYS_NUMBER * 2, 0), axis=1
    )

    df_rst = df_overdue.groupby(['DEPARTMENT_ID'])['SCORE_d_3'].sum().to_frame()
    return df_overdue, df_rst


# 整改时效
def stats_rectification_overdue(months_ago,
                                overdue_problem_number,
                                overdue_days_number,
                                department_data,
                                zhanduan_dpid_data,
                                risk_type,
                                choose_dpid_data):
    """
    问题销号超期1条扣1分；问题整改超期1条扣2分
    """
    rst_child_score = []

    # 获取有重复的条数
    calc_df_data, df_rst = get_rectification_overdue_data(
        overdue_problem_number, overdue_days_number, department_data, zhanduan_dpid_data)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 10, 4, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 10, 4, risk_type=risk_type)

    # 导出得分
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_d_3',
        3,
        2,
        10,
        4,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score
