# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''

import math
import pandas as pd
from app.data.major_risk_index.common.problem_rectification_common import (
    translate_rectification_effect_type_a, calc_rectification_effect_type_a,
    translate_rectification_effect_type_b, calc_rectification_effect_type_b,
    translate_rectification_effect_type_c, calc_rectification_effect_type_c,
    translate_rectification_effect_type_d, calc_rectification_effect_type_d,
    translate_rectification_effect_type_e, calc_rectification_effect_type_e)

from app.data.index.util import get_custom_month, get_year_month, get_months_from_201712
from app.data.major_risk_index.util import (
    get_last_month_alpha,
    init_calc_data_pro, update_mongo_data_by_condition,
    append_major_column_to_df, calc_child_index_type_divide,
    calc_child_index_type_sum, calc_extra_child_score_groupby_major,
    calc_extra_child_score_groupby_major_third,
    df_merge_with_dpid, export_basic_data_dicttype,
    export_basic_data_one_field_monthly, export_basic_data_tow_field_monthly,
    format_export_basic_data, summizet_operation_set,
    write_export_basic_data_to_mongo, calc_child_index_type_divide_two, export_basic_data_tow_field_monthly_two,
    calc_child_index_type_divide_major)
from app.data.util import pd_query, get_coll_prefix, get_data_from_mongo_by_find

SCORE = []
HIERARCHY = [3]

# 问题控制中不同级别问题的阈值
# 1 表示严重风险问题
# 2 表示较大风险问题
# 3 表示一般风险问题
PROBLEM_CTL_THRESHOLD_DICT = {
    1: 2,
    2: 4,
    3: 15
}

# 整改成效不同类型统计成标准格式
# 必须返回FK_DEPARTMENT_ID, CONTENT, COUNT
RECTIFICATION_EFFECT_FUN_DICT = {
    'A': translate_rectification_effect_type_a,
    'B': translate_rectification_effect_type_b,
    'C': translate_rectification_effect_type_c,
    'D': translate_rectification_effect_type_d,
    'E': translate_rectification_effect_type_e,
}

# 整改成效不同类型计算分数函数
RECTIFICATION_TYPE_CALC_FUNC_DICT = {
    'A': calc_rectification_effect_type_a,
    'B': calc_rectification_effect_type_b,
    'C': calc_rectification_effect_type_c,
    'D': calc_rectification_effect_type_d,
    'E': calc_rectification_effect_type_e,
}


# 按类型计算评价分


def _calc_check_evaluate_score(row, code_dict):
    score = 0
    for code in code_dict:
        score += int(row[code]) * int(code_dict[code])
    return min(100, score)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_repeatedly_index_ratio_score(
        row, column, major_column, detail_type, last_index_ratio
):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    # 如果问题数/总问题数 无这个部门， 该值给0
    _last_ratio = (last_index_ratio[1].get(row['DEPARTMENT_ID'], 0) -
                   last_index_ratio[0]) / last_index_ratio[0]
    _ratio_changed_ratio = (_ratio - _last_ratio) / _last_ratio
    if _ratio_changed_ratio >= 0.3:
        _score = 100 - 30
    elif _ratio >= 0.2:
        _score = 100 - 20
    elif _ratio >= 0.1:
        _score = 100 - 10
    else:
        _score = 100
    return _score


def _calc_rectification_score(problem_number, deduction_coefficient):
    val = 100 - deduction_coefficient * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


def _filter_external_assess_problem(row):
    flag = True
    if row['LEVEL'] not in ('E1', 'E2', 'E3', 'E4'):
        return flag
    else:
        if int(row['IS_EXTERNAL']) == 1 and row['ASSESS_MONEY'] > 0:
            return flag
        return not flag


# 整改时效（超期问题整改）
def stats_rectification_overdue(check_item_ids,
                                overdue_problem_number_sql,
                                department_data,
                                months_ago,
                                risk_type,
                                choose_dpid_data,
                                deduction_coefficient=0.2):
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    data = df_merge_with_dpid(
        pd_query(
            overdue_problem_number_sql.format(year_mon // 100, year_mon % 100,
                                              check_item_ids)),
        department_data)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        choose_dpid_data(3),
        6,
        1,
        3,
        months_ago,
        lambda x: f'本月问题超期发生条数：{int(x)}条',
        risk_type=risk_type)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        lambda x: _calc_rectification_score(x, deduction_coefficient),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_index_score


# 整改履责
def stats_check_evaluate(check_risk_ids,
                         check_evaluate_sz_score_sql,
                         department_data,
                         months_ago,
                         risk_type,
                         choose_dpid_data,
                         deduction_coefficient=0.2,
                         calc_func=lambda x: 0 if (100 - x) < 0 else round((100 - x), 2)):
    calc_month = get_custom_month(months_ago)
    data = df_merge_with_dpid(
        pd_query(
            check_evaluate_sz_score_sql.format(*calc_month, check_risk_ids,
                                               deduction_coefficient)),
        department_data)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        choose_dpid_data(3),
        6,
        2,
        3,
        months_ago,
        lambda x: f'本月ZG-1、2、3、4、5履职评价发生条数：{int(x)}条',
        risk_type=risk_type)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        calc_func=calc_func,
        dpid_func=choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_index_score


def _count_evaluate_type(row, colunm):
    """
    统计各评价类型个数
    :param row:
    :param colunm:
    :return:
    """
    pass


def _calc_score_by_formula_new_check_evaluate(row, column, major_column, detail_type, major_ratio_dict={}):
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100
    elif _ratio >= -0.1:
        _score = 90
    else:
        deduction = _ratio * 100
        if detail_type == 3:
            deduction *= -1
        _score = 100 + deduction
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


# 新整改履职
def new_stats_check_evaluate(check_risk_ids,
                             check_evaluate_sz_sql,
                             department_data,
                             months_ago,
                             risk_type,
                             choose_dpid_data,
                             calc_func=lambda x: 0 if (100 - x) < 0 else round((100 - x), 2)):
    calc_month = get_custom_month(months_ago)
    data = df_merge_with_dpid(
        pd_query(
            check_evaluate_sz_sql.format(*calc_month, check_risk_ids)),
        department_data)
    CODE_TYPE = ['ZG-1', 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5']
    child_weight = [75, 60, 45, 30, 15]
    for idx, item in enumerate(CODE_TYPE):
        idata = data[data['CODE'].isin(item)]
        if idata.empty:
            continue
        code_number = idata.groupby(['DEPARTMENT_ID']).size()
        idata = code_number.to_frame(name=f'COUNT_{item}')
        idata[f'SCORE_{item}'] = idata[f'COUNT_{item}'] * child_weight[idx]
        rst_child_data = calc_extra_child_score_groupby_major(
            idata.copy(),
            choose_dpid_data(3),
            'COUNT',
            _calc_score_by_formula_new_check_evaluate,
            weight=child_weight[idx],
            detail_type=3)
    # 导出中间过程数据
    export_basic_data_dicttype(
        data,
        choose_dpid_data(3),
        6,
        2,
        3,
        months_ago,
        lambda x: f'本月ZG-1、2、3、4、5履职评价发生条数：{int(x)}条',
        risk_type=risk_type)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        calc_func=calc_func,
        dpid_func=choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_index_score


# 履职评价指数 - 按评价类型表加分
def stats_check_evaluate_by_codetype(check_evaluate_sz_number_data,
                                     department_data,
                                     zhanduan_dpid_data,
                                     months_ago,
                                     risk_type,
                                     choose_dpid_data,
                                     calc_func=lambda x: 0 if (
                                                                      100 - x) < 0 else round((100 - x), 2),
                                     code_dict=None):
    """
    履职评价指数：履职评价指数：履职评价中一条ZG-1、2、3、4、5分别按下表加分。（施工、天窗修、点外修）。
    :param months_ago:
    :return:
    """
    check_evaluate_sz_number = check_evaluate_sz_number_data

    # 获取每个站段各个code的数量
    # code_list = ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-5"]
    if not code_dict:
        code_dict = {"ZG-1": 75,
                     "ZG-2": 60,
                     "ZG-3": 45,
                     "ZG-4": 30,
                     "ZG-5": 15}
    data = zhanduan_dpid_data
    for code in code_dict:
        code_data = check_evaluate_sz_number[check_evaluate_sz_number["CODE"] == code]
        code_data = code_data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
        code_data.rename(columns={"COUNT": code}, inplace=True)
        data = pd.merge(
            data,
            code_data.loc[:, [code, "TYPE3"]],
            how="left",
            left_on="DEPARTMENT_ID",
            right_on="TYPE3")
        data.drop(["TYPE3"], inplace=True, axis=1)
    data["TYPE3"] = data["DEPARTMENT_ID"]
    data.fillna(0, inplace=True)

    data['CONTENT'] = data.apply(
        lambda row: '<br/>'.join([f'本月{col}履职评价发生条数：{int(row[col])}条' for col in code_dict]), axis=1
    )

    data['SCORE'] = data.apply(
        lambda row: _calc_check_evaluate_score(row, code_dict), axis=1)
    # 中间过程
    data_rst = format_export_basic_data(
        data, 6, 2, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 2, risk_type=risk_type)
    # 最终结果
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_index_score


def _get_appoint_month_happen_problem(check_item_ids, happen_problem_point_sql,
                                      department_data, months_ago, hierarchy,
                                      problem_ctl_threshold_dict=PROBLEM_CTL_THRESHOLD_DICT):
    """获取前第{-months_ago}月的发生问题项点数

    Arguments:
        months_ago {int} -- [description]

    Returns:
        set -- 反复问题
    """
    happen_problem_point_data = pd_query(
        happen_problem_point_sql.format(*get_custom_month(months_ago),
                                        check_item_ids))
    if not happen_problem_point_data.empty:
        happen_problem_point_data = happen_problem_point_data[
            happen_problem_point_data.apply(
                lambda row: _filter_external_assess_problem(row), axis=1)]
    happen_problem_point_data = happen_problem_point_data[
        ['FK_DEPARTMENT_ID', 'PK_ID', 'RISK_LEVEL']]
    i_month_data = df_merge_with_dpid(
        happen_problem_point_data, department_data)
    if i_month_data.empty is True:
        return set()
    i_month_data = i_month_data.groupby(
        [f'TYPE{hierarchy}', 'PK_ID', 'RISK_LEVEL']).size()
    repeatedly_problem = set()
    for idx in i_month_data.index:
        risk_level = idx[2]
        problem_number = int(i_month_data[idx])
        # if risk_level == 1 and problem_number < 2:
        #     continue
        # elif risk_level == 2 and problem_number < 4:
        #     continue
        # elif risk_level == 3 and problem_number < 15:
        #     continue
        if problem_number < problem_ctl_threshold_dict.get(risk_level):
            continue
        else:
            repeatedly_problem.add(f'{idx[0]}||{idx[1]}||{idx[2]}')
    return repeatedly_problem


def _calc_problem_score(risk_level, i_month):
    """根据风险等级和月份进行扣分

    Arguments:
        risk_level {str} -- 风险等级
        i_month {str} -- 前第{-i_month}月

    Returns:
        float/int -- 得分
    """
    _score = {
        '1': 1,
        '2': 0.5,
        '3': 0.2,
    }
    problem_score = _score.get(risk_level, 0) * (4 + int(i_month))
    return problem_score


def _fillna_for_zhanduan(df, zhanduan_dpid_data):
    all_zhanduan = zhanduan_dpid_data.loc[:, 'DEPARTMENT_ID'].values
    na_zhanduan = []
    df_zhanduan = df.index.values
    for each in all_zhanduan:
        if each not in df_zhanduan:
            na_zhanduan.append(each)
    df_na_zhanduan = pd.DataFrame(
        index=na_zhanduan, data=[0 for x in na_zhanduan], columns=['SCORE'])
    return pd.concat([df, df_na_zhanduan])


def _calc_repeatedly_index(check_item_ids, happen_problem_point_sql,
                           department_data, zhanduan_dpid_data, months_ago,
                           hierarchy, risk_type, choose_dpid_data,
                           problem_ctl_threshold_dict=PROBLEM_CTL_THRESHOLD_DICT):
    repeatedly_problem = []
    base_repeatedly_problem = _get_appoint_month_happen_problem(
        check_item_ids, happen_problem_point_sql, department_data, months_ago,
        hierarchy, problem_ctl_threshold_dict=problem_ctl_threshold_dict)
    for i_month in range(-1, -4, -1):
        i_month_repeatedly_problem = _get_appoint_month_happen_problem(
            check_item_ids, happen_problem_point_sql, department_data,
            months_ago + i_month, hierarchy,
            problem_ctl_threshold_dict=problem_ctl_threshold_dict)
        # 获取每个月反复的问题项点
        common_problem = base_repeatedly_problem.intersection(
            i_month_repeatedly_problem)
        repeatedly_problem.extend(
            [[x.split('||')[0], i_month,
              x.split('||')[2]] for x in common_problem])
    if repeatedly_problem == []:
        return []
    # 导出中间过程数据
    first_title = {-1: '前一个月', -2: '前二个月', -3: '前三个月'}
    second_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_tow_field_monthly(
        repeatedly_problem,
        department_data,
        choose_dpid_data(3),
        6,
        3,
        3,
        months_ago,
        first_title,
        second_title,
        risk_type=risk_type)
    df_prob = pd.DataFrame(
        data=repeatedly_problem, columns=['DEP_DPID', 'I_MONTH', 'RISK_LEVEL'])
    df_prob['SCORE'] = df_prob.apply(
        lambda row: _calc_problem_score(row['RISK_LEVEL'], row['I_MONTH']),
        axis=1)
    # 将站段分组求和
    df_prob = df_prob.groupby(['DEP_DPID'])['SCORE'].sum()
    df_prob = _fillna_for_zhanduan(
        df_prob.to_frame(name='SCORE'), zhanduan_dpid_data)
    column = f'SCORE_c_{hierarchy}'
    df_prob[column] = df_prob['SCORE'].apply(
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2))

    # 计算排名、入库
    summizet_operation_set(
        df_prob,
        choose_dpid_data(hierarchy),
        column,
        hierarchy,
        2,
        6,
        3,
        months_ago,
        risk_type=risk_type)
    return df_prob[[column]]


def stats_repeatedly_index_type_jiwu(check_item_ids, repeat_problem_sql,
                                     department_data, months_ago, risk_type,
                                     choose_dpid_data):
    start_mon = get_custom_month(months_ago - 2)[0]
    end_mon = get_custom_month(months_ago)[1]
    repeat_problem = list(
        pd_query(
            repeat_problem_sql.format(start_mon, end_mon,
                                      check_item_ids))['DIP'].values)
    # 记录扣分
    deduct_score = {}
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    repeat_prbo_dict = {}
    for item in repeat_problem:
        repeat_prbo_dict[item] = repeat_prbo_dict.get(item, 0) + 1
    for k, v in repeat_prbo_dict.items():
        score = 5
        if v >= 4:
            score = 30
            v = 4
        elif v > 2:
            score = 15
        elif v == 2:
            score = 5
        else:
            continue
        dpid = item.split('||')[0]
        deduct_score.update({dpid: deduct_score.get(dpid, 0) + score})
        calc_problems.append([dpid, v])
    # 导出中间计算过程
    first_title = {2: '反复发生两次', 3: '反复发生三次', 4: '反复发生四次及以上'}
    export_basic_data_one_field_monthly(
        calc_problems,
        department_data,
        choose_dpid_data(3),
        6,
        3,
        3,
        months_ago,
        lambda x: first_title.get(x),
        title='反复发生劳安问题',
        risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        6,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: max(0, 100 - x),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


# 问题控制
def stats_repeatedly_index(check_item_ids, happen_problem_point_sql,
                           department_data, zhanduan_dpid_data, months_ago,
                           risk_type, choose_dpid_data,
                           problem_ctl_threshold_dict=PROBLEM_CTL_THRESHOLD_DICT):
    """以站段为统计单位同一问题项点是否发生，严重风险的问题3月内重复发生一个扣a分，
    较大风险的（当月累计发生3次以上算重复发生）问题3月内重复发生一项扣a分，
    一般风险的（当月累计发生10次以上算重复发生）问题3月内重复发生一项扣a分。
    低风险问题不计算

    Arguments:
        months_ago {int} -- [description]
    """
    rst_index_score = []
    for hierarchy in HIERARCHY:
        rst_score = _calc_repeatedly_index(
            check_item_ids, happen_problem_point_sql, department_data,
            zhanduan_dpid_data, months_ago, hierarchy, risk_type,
            choose_dpid_data, problem_ctl_threshold_dict=problem_ctl_threshold_dict)
        if len(rst_score) != 0:
            rst_index_score.append(rst_score)
    return rst_index_score


# 整改复查
def stats_rectification_review(check_item_ids, work_load,
                               import_prob_recheck_count_sql, department_data,
                               months_ago, risk_type, choose_dpid_data,
                               zhanduan_filter_list=[],
                               customizecontent=None,
                               calc_score_by_formula=_calc_score_by_formula,
                               ):
    """库内问题复查数/总人数，与专业基数比较
    """

    stats_month = get_custom_month(months_ago)
    return calc_child_index_type_divide_major(
        df_merge_with_dpid(
            pd_query(
                import_prob_recheck_count_sql.format(
                    *stats_month, check_item_ids)), department_data),
        work_load,
        2,
        6,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 整改复查--部分专业基数
def stats_rectification_review_filter(check_item_ids, work_load,
                                      import_prob_recheck_count_sql, department_data,
                                      months_ago, risk_type, choose_dpid_data, zhanduan_filter_list=[],
                                      customizecontent=None):
    """库内问题复查数/总人数，与专业基数比较
    """
    stats_month = get_custom_month(months_ago)
    import_prob_recheck_count = df_merge_with_dpid(
        pd_query(
            import_prob_recheck_count_sql.format(
                *stats_month, check_item_ids)), department_data)
    return calc_child_index_type_divide_two(
        import_prob_recheck_count,
        work_load,
        2,
        6,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


def _peril_count_score_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.5:
        _score = 100
    elif _ratio >= 0:
        _score = 70
    elif _ratio > -0.5:
        _score = 50
    elif _ratio > -0.6:
        _score = 30
    else:
        _score = 0
    return _score


# 隐患整治 - 隐患库
def _peril_count_score(peril_count_sql, work_load, department_data, hierarchy,
                       choose_dpid_data):
    """隐患库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    peril_count = df_merge_with_dpid(
        pd_query(peril_count_sql), department_data)
    peril_count = peril_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [peril_count.to_frame(name='peril'), work_load], axis=1, sort=False)
    data['ratio'] = data['peril'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _peril_count_score_formula)
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: '隐患库(40%):<br/>单位级隐患数量({0})/单位总人数({1})'.format(
            f'{round(row["peril"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['peril', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 隐患整治 - 隐患库，改sql为data
def _peril_count_score_excellent(peril_count_data, work_load, department_data, hierarchy,
                                 choose_dpid_data):
    """隐患库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    peril_count = peril_count_data
    peril_count = peril_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [peril_count.to_frame(name='peril'), work_load], axis=1, sort=False)
    data['ratio'] = data['peril'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', _peril_count_score_formula,
        numerator='peril', denominator='PERSON_NUMBER')
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: '隐患库(40%):<br/>单位级隐患数量({0})/单位总人数({1})'.format(
            f'{round(row["peril"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['peril', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 隐患整治 - 隐患库,补充站段数据
def _peril_count_score_two(peril_count_sql, work_load, department_data, hierarchy,
                           choose_dpid_data):
    """隐患库（40%）。单位级隐患数量/单位总人数，高于0的得100分；50%-0的得70分；
    低于均值50%得50分，低于60%的得30分，低于80%的得0分。
    隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    """
    peril_count = df_merge_with_dpid(
        pd_query(peril_count_sql), department_data)
    peril_count = peril_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [peril_count.to_frame(name='peril'), work_load], axis=1, sort=False)
    data['ratio'] = data['peril'] / data['PERSON_NUMBER']
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _peril_count_score_formula)
    data.fillna(0, inplace=True)
    data[f'middle_1'] = data.apply(
        lambda row: '隐患库(40%):<br/>单位级隐患数量({0})/单位总人数({1})'.format(
            f'{int(row["peril"])}', int(row['PERSON_NUMBER'])),
        axis=1)
    data.drop(
        columns=['peril', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 隐患整治 - 隐患整治延期
def _peril_overdue(peril_overdue_count_sql, peril_period_count_sql,
                   department_data, hierarchy, peril_count_score):
    """隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    peril_overdue_data = df_merge_with_dpid(
        pd_query(peril_overdue_count_sql), department_data)
    peril_period_data = df_merge_with_dpid(
        pd_query(peril_period_count_sql), department_data)
    peril_overdue_count = peril_overdue_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    peril_period_count = peril_period_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [
            peril_overdue_count.to_frame(name='COUNT_OVERDUE'),
            peril_period_count.to_frame(name='COUNT_PERIL')
        ],
        axis=1,
        sort=False)
    if data.empty:
        return None, None
    series_rst = data.apply(lambda row: sum(row) * 2, axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '隐患整治延期（20%）:<br/>延期数量：({0})/转为长期整治数量：({1})'.format(
            row['COUNT_OVERDUE'], row['COUNT_PERIL']),
        axis=1)
    data.drop(columns=['COUNT_OVERDUE', 'COUNT_PERIL'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治延期


def _peril_overdue_excellent(peril_overdue_count_data, peril_period_count_data,
                             department_data, hierarchy, peril_count_score):
    """隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    peril_overdue_count = peril_overdue_count_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    peril_period_count = peril_period_count_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [
            peril_overdue_count.to_frame(name='COUNT_OVERDUE'),
            peril_period_count.to_frame(name='COUNT_PERIL')
        ],
        axis=1,
        sort=False)
    if data.empty:
        return None, None
    series_rst = data.apply(lambda row: sum(row) * 2, axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '隐患整治延期（20%）:<br/>延期数量：({0})/转为长期整治数量：({1})'.format(
            row['COUNT_OVERDUE'], row['COUNT_PERIL']),
        axis=1)
    data.drop(columns=['COUNT_OVERDUE', 'COUNT_PERIL'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治延期,补充站段数据
def _peril_overdue_two(peril_overdue_count_sql, peril_period_count_sql,
                       department_data, hierarchy, peril_count_score, work_load):
    """隐患整治延期（20%），1条1次延期扣2分，转为长期整治的1条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    peril_overdue_data = df_merge_with_dpid(
        pd_query(peril_overdue_count_sql), department_data)
    peril_period_data = df_merge_with_dpid(
        pd_query(peril_period_count_sql), department_data)
    peril_overdue_count = peril_overdue_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    peril_period_count = peril_period_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum()
    data = pd.concat(
        [
            peril_overdue_count.to_frame(name='COUNT_OVERDUE'),
            peril_period_count.to_frame(name='COUNT_PERIL')
        ],
        axis=1,
        sort=False)
    # 补全站段数据
    data = pd.merge(
        data,
        work_load,
        how="right",
        left_index=True,
        right_index=True
    )
    data.drop(columns=['PERSON_NUMBER'], inplace=True)
    data.fillna(0, inplace=True)
    series_rst = data.apply(lambda row: min(100, sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '隐患整治延期（20%）:<br/>延期数量{0}条,转为长期整治数量{1}条'.format(
            int(row['COUNT_OVERDUE']), int(row['COUNT_PERIL'])), axis=1)
    data.drop(columns=['COUNT_OVERDUE', 'COUNT_PERIL'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治督促
def _peril_urge(checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql,
                months_ago, hierarchy, peril_count_score, check_risk_ids):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    calc_month = get_custom_month(months_ago)
    CHECKED_PERIL_ID = pd_query(
        checked_peril_id_sql.format(*calc_month, check_risk_ids))
    PERIL_ID = pd_query(peril_id_sql)
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID.drop_duplicates(subset=['FK_DEPARTMENT_ID', 'PID'], keep=False, inplace=True)

    PERIL_RECTIFY_NO_ENTRY = pd_query(
        peril_rectify_no_entry_sql.format(*calc_month))
    data = pd.concat(
        [
            PERIL_ID.groupby(['FK_DEPARTMENT_ID'])['PID'].size(),
            PERIL_RECTIFY_NO_ENTRY.groupby(['FK_DEPARTMENT_ID'
                                            ])['COUNT'].size()
        ],
        axis=1,
        sort=False)

    series_rst = data.apply(lambda row: min(100, sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    return df_rst


# 隐患整治 - 隐患整治督促
def _peril_urge_excellent(checked_peril_id_data, peril_id_data, peril_rectify_no_entry_data,
                          months_ago, hierarchy, peril_count_score, check_risk_ids):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    长期整治的没录入阶段整治情况的每条扣2分。
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    CHECKED_PERIL_ID = checked_peril_id_data
    PERIL_ID = peril_id_data
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID.drop_duplicates(subset=['FK_DEPARTMENT_ID', 'PID'], keep=False, inplace=True)

    PERIL_RECTIFY_NO_ENTRY = peril_rectify_no_entry_data
    data = pd.concat(
        [
            PERIL_ID.groupby(['FK_DEPARTMENT_ID'])['PID'].size(),
            PERIL_RECTIFY_NO_ENTRY.groupby(['FK_DEPARTMENT_ID'
                                            ])['COUNT'].sum()
        ],
        axis=1,
        sort=False)
    data.fillna(0, inplace=True)
    series_rst = data.apply(lambda row: min(100, sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)
    data['middle_3'] = data.apply(
        lambda row: '隐患整治督促（40%）:<br/>未检查的个数：({0})/长期整治没录入({1})'.format(int(row['PID']), int(row['COUNT'])), axis=1)
    data.drop(columns=['COUNT', 'PID'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治 - 隐患整治督促,补充站段数据,删除了长期整治的没录入阶段整治情况
def _peril_urge_two(checked_peril_id_sql, peril_id_sql, department_data, peril_rectify_no_entry_sql,
                    months_ago, hierarchy, peril_count_score, check_risk_ids, work_load):
    """隐患整治督促（40%）。单位级的隐患每月未检查的每个扣2分（检查的车间级等同单位级），
    （单位隐患库得分低于70分的，该项分值=结果*隐患库得分/100；等于高于70分的不*系数）
    """
    calc_month = get_custom_month(months_ago)
    CHECKED_PERIL_ID = pd_query(
        checked_peril_id_sql.format(*calc_month, check_risk_ids))
    PERIL_ID = pd_query(peril_id_sql.format(check_risk_ids))
    PERIL_ID = PERIL_ID.append(CHECKED_PERIL_ID)
    PERIL_ID.drop_duplicates(subset=['FK_DEPARTMENT_ID', 'PID'], keep=False, inplace=True)

    PERIL_ID = df_merge_with_dpid(PERIL_ID, department_data)
    data = PERIL_ID.groupby(['TYPE3'])['PID'].size()
    data = data.to_frame()
    data = pd.merge(
        data,
        work_load,
        how="right",
        left_index=True,
        right_index=True
    )
    data.drop(columns=['PERSON_NUMBER'], inplace=True)
    data.fillna(0, inplace=True)

    series_rst = data.apply(lambda row: min(100, sum(row) * 2), axis=1)
    df_rst = series_rst.to_frame(name='COUNT')
    for index, row in df_rst.iterrows():
        if index in peril_count_score.index:
            peril_score = peril_count_score.loc[index].values[0]
            if peril_score < 70:
                row['COUNT'] *= (peril_score / 100)

    data['middle_3'] = data.apply(
        lambda row: '隐患整治督促（40%）:<br/>未检查的个数：({0})'.format(
            (row['PID'])), axis=1)
    data.drop(columns=['PID'], inplace=True, axis=1)
    return df_rst, data


# 隐患整治
def stats_peril_renovation(
        peril_count_sql, peril_overdue_count_sql, peril_period_count_sql,
        checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql,
        work_load, department_data, months_ago, risk_type, choose_dpid_data, check_risk_ids):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        peril_count_score, peril_count_score_basic_data = _peril_count_score(
            peril_count_sql, work_load, department_data, hierarchy,
            choose_dpid_data)
        score.append(peril_count_score * 0.4)
        calc_basic_data.append(peril_count_score_basic_data)
        peril_overdue_socre, peril_overdue_score_basic_data = _peril_overdue(
            peril_overdue_count_sql, peril_period_count_sql, department_data,
            hierarchy, peril_count_score)
        if peril_overdue_socre is not None:
            score.append(peril_overdue_socre * 0.2)
            calc_basic_data.append(peril_overdue_score_basic_data)
        score.append(
            _peril_urge(checked_peril_id_sql, peril_id_sql,
                        peril_rectify_no_entry_sql, months_ago, hierarchy,
                        peril_count_score, check_risk_ids) * 0.4)
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 6, 5, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 6, 5, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_e_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            6,
            5,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 隐患整治
def stats_peril_renovation_excellent(
        peril_count_data, peril_overdue_count_data, peril_period_count_data,
        checked_peril_id_data, peril_id_data, peril_rectify_no_entry_data,
        work_load, department_data, months_ago, risk_type, choose_dpid_data, check_risk_ids):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        peril_count_score, peril_count_score_basic_data = _peril_count_score_excellent(
            peril_count_data, work_load, department_data, hierarchy,
            choose_dpid_data)
        score.append(peril_count_score * 0.4)
        calc_basic_data.append(peril_count_score_basic_data)
        peril_overdue_socre, peril_overdue_score_basic_data = _peril_overdue_excellent(
            peril_overdue_count_data, peril_period_count_data, department_data,
            hierarchy, peril_count_score)
        if peril_overdue_socre is not None:
            score.append(peril_overdue_socre * 0.2)
            calc_basic_data.append(peril_overdue_score_basic_data)
            peril_urge_score, peril_urge_score_basic_data = _peril_urge_excellent(
                checked_peril_id_data, peril_id_data,
                peril_rectify_no_entry_data, months_ago, hierarchy,
                peril_count_score, check_risk_ids)
            score.append(peril_urge_score * 0.4)
            calc_basic_data.append(peril_urge_score_basic_data)
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 6, 5, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 6, 5, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_e_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            6,
            5,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 隐患整治--填充站段数据
def stats_peril_renovation_two(
        peril_count_sql, peril_overdue_count_sql, peril_period_count_sql,
        checked_peril_id_sql, peril_id_sql, peril_rectify_no_entry_sql,
        work_load, department_data, months_ago, risk_type, choose_dpid_data,
        check_risk_ids):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        # 隐患库
        peril_count_score, peril_count_score_basic_data = _peril_count_score_two(
            peril_count_sql, work_load, department_data, hierarchy,
            choose_dpid_data)
        score.append(peril_count_score * 0.4)
        calc_basic_data.append(peril_count_score_basic_data)

        # 隐患整治延期
        peril_overdue_socre, peril_overdue_score_basic_data = _peril_overdue_two(
            peril_overdue_count_sql, peril_period_count_sql, department_data,
            hierarchy, peril_count_score, work_load)
        if peril_overdue_socre is not None:
            score.append(peril_overdue_socre * 0.2)
            calc_basic_data.append(peril_overdue_score_basic_data)

        # 隐患整治督促
        peril_urge_score, peril_urge_score_basic_data = _peril_urge_two(
            checked_peril_id_sql, peril_id_sql,
            department_data,
            peril_rectify_no_entry_sql, months_ago, hierarchy,
            peril_count_score, check_risk_ids, work_load)
        score.append(peril_urge_score * 0.4)
        calc_basic_data.append(peril_urge_score_basic_data)

        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 6, 5, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 6, 5, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_e_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            6,
            5,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def export_waring_effect(data, department_data, choose_dpid_data,
                         hierarchy, ):
    """导出警告预警次数"""
    data = df_merge_with_dpid(data, department_data)
    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # 补全站段数据
    data = pd.merge(
        data.to_frame(),
        choose_dpid_data(hierarchy)["DEPARTMENT_ID"].to_frame(),
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index('DEPARTMENT_ID', inplace=True)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '警告性预警延期次数: {0}'.format(
            f'{int(row["COUNT"])}'),
        axis=1)
    data.pop("COUNT")
    return data


def export_recification_effect(data, department_data, main_type, detail_type,
                               months_ago, choose_dpid_data, risk_type):
    """导出各类事故的次数

    Arguments:
        data {pandas.DataFrame} --
    """
    # 保存中间过程数据
    monthly_basic_data = []
    for idx, row in data.iterrows():
        resp_level = row['RESPONSIBILITY_IDENTIFIED']
        if resp_level in [1, 2, 3, 8]:
            resp_level = 1
        elif resp_level in [4, 5, 9]:
            resp_level = 2
        else:
            resp_level = 3
        monthly_basic_data.append(
            [row['FK_DEPARTMENT_ID'], row['MAIN_TYPE'], resp_level])
    first_title = {1: '事故', 2: '故障', 3: '综合信息'}
    second_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    export_basic_data_tow_field_monthly(
        data,
        department_data,
        choose_dpid_data(3),
        6,
        6,
        3,
        months_ago,
        first_title,
        second_title,
        risk_type=risk_type)


def export_recification_effect_two(data, department_data, main_type, detail_type,
                                   months_ago, choose_dpid_data, risk_type):
    """导出各类事故的次数

    Arguments:
        data {pandas.DataFrame} --
    """
    # 保存中间过程数据
    monthly_basic_data = []
    for idx, row in data.iterrows():
        resp_level = row['RESPONSIBILITY_IDENTIFIED']
        if resp_level in [1, 2, 3, 8]:
            resp_level = 1
        elif resp_level in [4, 5, 9]:
            resp_level = 2
        else:
            resp_level = 3
        monthly_basic_data.append(
            [row['FK_DEPARTMENT_ID'], row['MAIN_TYPE'], resp_level])
    first_title = {1: '事故', 2: '故障', 3: '综合信息'}
    second_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    return export_basic_data_tow_field_monthly_two(
        monthly_basic_data,
        department_data,
        choose_dpid_data(3),
        6,
        6,
        3,
        months_ago,
        first_title,
        second_title,
        risk_type=risk_type)


def _calc_score(row):
    """整改成效的记分规则
    """
    base_score = [10, 2, 1]
    step_score = [5, 2, 1]
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    if resp_level in [1, 2, 3, 8]:
        resp_level = 2
    elif resp_level in [4, 5, 9]:
        resp_level = 1
    else:
        resp_level = 0
    main_type = row['MAIN_TYPE'] - 1
    return base_score[main_type] + (step_score[main_type] * resp_level)


def _calc_responsible_score_prectl(row):
    """计算防控责任中的事故责任分

    Arguments:
        row {[type]} -- [description]
    """
    base_socre = [10, 15, 20]
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    if resp_level in [1, 2, 3, 8]:
        resp_level = 2
    elif resp_level in [4, 5, 9]:
        resp_level = 1
    else:
        resp_level = 0
    return base_socre[resp_level]


def _calc_rectification_score_excellent(row, rectification_types,
                                        rectification_type_calc_func_dict=None,
                                        is_multiply=False):
    """[根据整改不同类型，计算分数]

    Arguments:
        row {[type]} -- [description]
        rectification_types {[type]} -- [description]
    """
    # 使用命名空间动态计算各种类型的值
    names = locals()
    if not is_multiply:
        # 是否为复合类型（不同类型间混合计算）
        for key in rectification_types:
            names['COUNT_' + str(key)] = row[f'COUNT_{key}']
            if (not rectification_type_calc_func_dict) or (not rectification_type_calc_func_dict.get(key, None)):
                names['_score_' +
                      str(key)] = RECTIFICATION_TYPE_CALC_FUNC_DICT[key](row)
            else:
                names['_score_' +
                      str(key)] = rectification_type_calc_func_dict[key](row)
    else:
        # 暂未遇到，预留方案
        pass
    return sum([names['_score_' + str(key)] for key in rectification_types])


# 整改成效
def stats_rectification_effect(
        check_item_ids, responsibe_safety_produce_info_sql, department_data,
        months_ago, risk_type, choose_dpid_data):
    """警告性预警延期一次扣10分(暂未加入)；责任事故主要、全部责任的1个扣20分、重要扣15分、
    次要的扣10分（含追究责任）；
    D21等同故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分；
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。直接在总分中扣，
    改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    """
    calc_month = get_custom_month(months_ago)
    data = pd_query(
        responsibe_safety_produce_info_sql.format(*calc_month, check_item_ids))
    if data.empty:
        return None
    # 导出中间过程数据
    export_recification_effect(data, department_data, 6, 6, months_ago,
                               choose_dpid_data, risk_type)
    data['SCORE'] = data.apply(lambda row: _calc_score(row), axis=1)
    data.drop(['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
    data = df_merge_with_dpid(data, department_data)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: 40 if x > 40 else x,
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


# 整改成效
def stats_rectification_effect_excellent(
        department_data,
        months_ago, risk_type, choose_dpid_data,
        df_dict,
        func_dict=RECTIFICATION_EFFECT_FUN_DICT,
        calc_score_by_formula=lambda x: 40 if x > 40 else x,
        rectification_type_calc_func_dict=RECTIFICATION_TYPE_CALC_FUNC_DICT):
    """
    警告性预警延期一次扣10分(暂未加入)；
    责任事故主要、全部责任的1个扣20分、重要扣15分、次要的扣10分（含追究责任）；
    D21等同故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分；
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。
    直接在总分中扣，改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    因为存在有些扣分并不是按1个/几分扣除，摒弃不统计个数，直接扣分的操作，
    同时存在并不是只统计了事故，综合，故障，跟防控责任相关的一些统计信息，也可以通过合并df方式，
    使用一些约定的格式，完成整体记分，报告内容渲染。
    各键值含义：
    A - 责任类型（事故，故障，综合信息）
    B - 警告性预警延期
    C - 防控责任
    D - 预警通知书
    E - 指数排名扣分
    """
    from functools import reduce
    if not df_dict:
        return None
    result_dict = dict()
    # 根据需要的df，找到对应的函数，除以得到结果
    # 防止A,B等之类的数量记分互相影响，
    # 暂时result的结果里得到count_A等
    rectification_types = [key for key in df_dict.keys()]
    for key, item in df_dict.items():
        suite_func = func_dict.get(key, None)
        result = suite_func(key, item)
        result_dict[key] = result

    # 如果合并2个以及df的计数结果，文本内容
    if len(rectification_types) >= 2:
        df_final = reduce(lambda left, right: pd.merge(left,
                                                       right,
                                                       how='outer',
                                                       on='FK_DEPARTMENT_ID'),
                          [item for item in result_dict.values()]
                          )
    # 外连接后不同的count类型nan值补0
    else:
        df_final = result_dict[rectification_types[0]]
    # 外连接后不同的count类型nan值补0
    # 合并文本内容
    df_final['CONTENT'] = ''
    for key in rectification_types:
        df_final[f'COUNT_{key}'].fillna(0, inplace=True)
        df_final['CONTENT'] += df_final[f'CONTENT_{key}'] + '\n'
    # 相同的department_id的content合并
    for idx, row in df_final.iterrows():
        tmp_df = df_final[df_final['FK_DEPARTMENT_ID']
                          == row['FK_DEPARTMENT_ID']]
        content_list = []
        for item in tmp_df['CONTENT'].values.tolist():
            content_list.extend(item.split('\n'))
        content_list.sort()
        content = '\n'.join(set(content_list))
        df_final.loc[idx, 'CONTENT'] = content
    df_final['SCORE'] = df_final.apply(
        lambda row: _calc_rectification_score_excellent(row, rectification_types,
                                                        rectification_type_calc_func_dict), axis=1)

    # 保留 fk_department_id,score 两列
    data = df_final[['FK_DEPARTMENT_ID', 'SCORE']]
    # 前面合并了文本内容，但是fk_department_id,因为score分数不同还是有重复的内容。
    data_contents = df_final[['FK_DEPARTMENT_ID', 'CONTENT']].copy()
    data_contents.drop_duplicates(
        subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    # df_final
    del df_final
    # 导出中间过程数据
    rst = pd.merge(
        choose_dpid_data(3),
        data_contents,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_rst = format_export_basic_data(rst, 6, 6,
                                        3, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 6, risk_type)
    data = df_merge_with_dpid(data, department_data)

    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


# 整改成效--填充站段数据
def stats_rectification_effect_two(
        check_item_ids, responsibe_safety_produce_info_sql, warning_delay_sql, department_data,
        months_ago, risk_type, choose_dpid_data):
    """警告性预警延期一次扣10分(暂未加入)；责任事故主要、全部责任的1个扣20分、重要扣15分、
    次要的扣10分（含追究责任）；
    D21等同故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分；
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。直接在总分中扣，
    改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    """
    calc_month = get_custom_month(months_ago)
    responsibe_info = pd_query(
        responsibe_safety_produce_info_sql.format(*calc_month, check_item_ids))
    warning_delay = pd_query(warning_delay_sql.format(*calc_month))
    responsibe_data = export_recification_effect_two(responsibe_info, department_data, 6, 6,
                                                     months_ago, choose_dpid_data, risk_type)
    warning_data = export_waring_effect(
        warning_delay, department_data, choose_dpid_data, 3)
    calc_df_data = pd.concat(
        [responsibe_data, warning_data], axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 6, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
                                     6, risk_type=risk_type)
    # 责任分
    data = department_data
    if responsibe_info.empty:
        data["SCORE_1"] = 0
    else:
        responsibe_info['SCORE_1'] = responsibe_info.apply(
            lambda row: _calc_score(row), axis=1)
        responsibe_info.set_index("FK_DEPARTMENT_ID", inplace=True)
        responsibe_info.drop(
            ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
        data = pd.merge(
            responsibe_info,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    # 警告分
    if warning_delay.empty:
        data["SCORE_2"] = 0
    else:
        warning_delay['SCORE_2'] = warning_delay.apply(
            lambda row: int(row['COUNT'] * 10), axis=1)
        warning_delay.drop(['COUNT'], inplace=True, axis=1)
        warning_delay.set_index("FK_DEPARTMENT_ID", inplace=True)
        data = pd.merge(
            warning_delay,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    data.fillna(0, inplace=True)
    data['SCORE'] = data.apply(lambda row: round(
        row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    data.drop(["SCORE_1", "SCORE_2"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: 40 if x > 40 else x,
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


def _export_recification_effect_type_jiwu(data, department_data,
                                          choose_dpid_data, months_ago):
    """导出各类事故的次数

    Arguments:
        data {pandas.DataFrame} --
    """
    # 保存中间过程数据
    monthly_basic_data = []
    for idx, row in data.iterrows():
        resp_level = row['RESPONSIBILITY_IDENTIFIED']
        if resp_level in [1, 2, 3, 8]:
            resp_level = 1
        elif resp_level in [4, 5, 9]:
            resp_level = 2
        else:
            resp_level = 3
        monthly_basic_data.append([row['FK_DEPARTMENT_ID'], resp_level])
    first_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    export_basic_data_one_field_monthly(
        data,
        department_data,
        choose_dpid_data(3),
        6,
        6,
        3,
        months_ago,
        lambda x: first_title.get(x),
        title='事故发生次数')


def _calc_score_type_jiwu(row):
    """整改成效的记分规则
    """
    resp_level = row['RESPONSIBILITY_IDENTIFIED']
    if resp_level in [1, 2, 3, 8]:
        resp_level = 20
    elif resp_level in [4, 5, 9]:
        resp_level = 15
    else:
        resp_level = 5
    return resp_level


# 整改成效
def stats_recification_effect_type_jiwu(resp_data, department_data, months_ago,
                                        risk_type, choose_dpid_data):
    """事故主要、全部责任的1个扣20分，重要扣15分，次要的扣5分（含追究责任）。得分=100-扣分。
    """
    if resp_data.empty:
        return None
    # 导出中间过程数据
    _export_recification_effect_type_jiwu(resp_data, department_data,
                                          choose_dpid_data, months_ago)
    resp_data['SCORE'] = resp_data.apply(
        lambda row: _calc_score_type_jiwu(row), axis=1)
    resp_data.drop(
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
    resp_data = df_merge_with_dpid(resp_data, department_data)
    rst_child_score = calc_child_index_type_sum(
        resp_data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(40, x),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


def export_prectl_effect(data, department_data, choose_dpid_data,
                         hierarchy, ):
    """导出警告预警次数"""
    data = df_merge_with_dpid(data, department_data)
    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # 补全站段数据
    data = pd.merge(
        data.to_frame(),
        choose_dpid_data(hierarchy)["DEPARTMENT_ID"].to_frame(),
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index('DEPARTMENT_ID', inplace=True)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '事故防控责任个数: {0}'.format(
            f'{int(row["COUNT"])}'),
        axis=1)
    data.pop("COUNT")
    return data


# 整改成效-防控责任
def stats_recification_effect_precontrol(resp_data, spir_data, department_data, months_ago,
                                         risk_type, choose_dpid_data):
    """事故主要、全部责任的1个扣20分，重要扣15分，次要的扣5分（含追究责任）。得分=100-扣分。

    """
    # 导出事故责任信息
    responsibe_data = export_recification_effect_two(resp_data, department_data, 6, 6,
                                                     months_ago, choose_dpid_data, risk_type)
    # 导出防控责任信息
    prectl_data = export_prectl_effect(
        spir_data, department_data, choose_dpid_data, 3)

    calc_df_data = pd.concat(
        [responsibe_data, prectl_data], axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 6, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
                                     6, risk_type=risk_type)
    data = department_data
    if resp_data.empty:
        data["SCORE_1"] = 0
    else:
        resp_data['SCORE_1'] = resp_data.apply(
            lambda row: _calc_responsible_score_prectl(row), axis=1)
        resp_data.set_index("FK_DEPARTMENT_ID", inplace=True)
        resp_data.drop(
            ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
        data = pd.merge(
            resp_data,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    # 警告分
    if spir_data.empty:
        data["SCORE_2"] = 0
    else:
        spir_data['SCORE_2'] = spir_data.apply(
            lambda row: int(row['COUNT'] * 5), axis=1)
        spir_data.drop(['COUNT'], inplace=True, axis=1)
        spir_data.set_index("FK_DEPARTMENT_ID", inplace=True)
        data = pd.merge(
            spir_data,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    data.fillna(0, inplace=True)
    data['SCORE'] = data.apply(lambda row: round(
        row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    data.drop(["SCORE_1", "SCORE_2"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(x, 40),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


def export_warning_nofication_effect(data, department_data, choose_dpid_data,
                                     hierarchy, ):
    """导出下发预警通知书次数"""
    data = df_merge_with_dpid(data, department_data)
    data = data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    # 补全站段数据
    data = pd.merge(
        data.to_frame(),
        choose_dpid_data(hierarchy)["DEPARTMENT_ID"].to_frame(),
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    data.set_index('DEPARTMENT_ID', inplace=True)
    data.fillna(0, inplace=True)
    data['middle_2'] = data.apply(
        lambda row: '下发警告预警通知书次数: {0}'.format(
            f'{int(row["COUNT"])}'),
        axis=1)
    data.pop("COUNT")
    return data


# 整改成效-预警通知书
def stats_recification_effect_warning_nofication(resp_data, warnnofication_data, department_data, months_ago,
                                                 risk_type, choose_dpid_data):
    """事故主要、全部责任的1个扣20分，重要扣15分，次要的扣5分（含追究责任）。得分=100-扣分。

    """
    # 导出事故责任信息
    responsibe_data = export_recification_effect_two(resp_data, department_data, 6, 6,
                                                     months_ago, choose_dpid_data, risk_type)
    # 导出预警通知书信息
    prectl_data = export_warning_nofication_effect(
        warnnofication_data, department_data, choose_dpid_data, 3)

    calc_df_data = pd.concat(
        [responsibe_data, prectl_data], axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 6, 6, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 6,
                                     6, risk_type=risk_type)
    data = department_data
    if resp_data.empty:
        data["SCORE_1"] = 0
    else:
        resp_data['SCORE_1'] = resp_data.apply(
            lambda row: _calc_responsible_score_prectl(row), axis=1)
        resp_data.set_index("FK_DEPARTMENT_ID", inplace=True)
        resp_data.drop(
            ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'], inplace=True, axis=1)
        data = pd.merge(
            resp_data,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    # 警告分
    if warnnofication_data.empty:
        data["SCORE_2"] = 0
    else:
        warnnofication_data['SCORE_2'] = warnnofication_data.apply(
            lambda row: int(row['COUNT'] * 5), axis=1)
        warnnofication_data.drop(['COUNT'], inplace=True, axis=1)
        warnnofication_data.set_index("FK_DEPARTMENT_ID", inplace=True)
        data = pd.merge(
            warnnofication_data,
            data,
            how='right',
            right_on='DEPARTMENT_ID',
            left_index=True
        )
    data.fillna(0, inplace=True)
    data['SCORE'] = data.apply(lambda row: round(
        row['SCORE_1'] + row['SCORE_2'], 2), axis=1)
    data.drop(["SCORE_1", "SCORE_2"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(x, 40),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


def _get_repeate_hidden_problem_data(calc_month, repeate_happen_problem_sql, check_risk_ids, department_data):
    """[根据需要更新mongo数据]

    Arguments:
        months_ago {[type]} -- [description]
        condition {[type]} -- [description]
        set_condition {[type]} -- [description]
        coll_name {[type]} -- [description]
    """
    repeate_happen_problem_data = df_merge_with_dpid(
        pd_query(repeate_happen_problem_sql.format(
            *calc_month, check_risk_ids)),
        department_data)
    if repeate_happen_problem_data.empty:
        repeate_happen_problem_data = repeate_happen_problem_data[[
            'TYPE3', 'FK_PROBLEM_BASE_ID', 'RISK_LEVEL', 'COUNT'
        ]].copy()
    else:
        repeate_happen_problem_data = repeate_happen_problem_data.groupby(
            ['TYPE3', 'FK_PROBLEM_BASE_ID', 'RISK_LEVEL'])['COUNT'].sum().reset_index()
    return repeate_happen_problem_data


# 问题控制
def stats_repeatedly_index_excellent(department_data, zhanduan_dpid_data,
                                     months_ago, risk_type, check_risk_ids,
                                     choose_dpid_data,
                                     worker_count,
                                     repeate_happen_problem_sql,
                                     major_problem_point_info_data,
                                     problem_ctl_threshold_dict=PROBLEM_CTL_THRESHOLD_DICT):
    """[
        问题控制，包括计算整改系数，单位问题暴露度，单位系数等
    ]
    最终得分=100-∑扣分*整改系数/问题暴露度系数
    整改系数 - 针对某个部门的某个问题的整改系数
    单位系数 - 某个站段的总人数/（参与计算的站段总人数/参与计算的站段数)
    问题暴露度系数 - 针对某个单位的暴露度系数
    扣分 - 针对某个单位某个问题的扣分
    """
    # 以站段为统计单位,计算单位系数
    worker_count = pd.merge(
        department_data,
        worker_count,
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID',
        how='left'
    )
    worker_count.drop(['DEPARTMENT_ID', 'FK_DEPARTMENT_ID'],
                      inplace=True, axis=1)
    # 人数补零
    worker_count.fillna({'COUNT': 0}, inplace=True)
    # 各站段部门数
    worker_count['TYPE3_COUNT'] = len(
        set(worker_count['TYPE3'].values.tolist()))
    worker_count['TOTAL_PERSON_COUNT'] = sum(
        worker_count['COUNT'].values.tolist())
    worker_count_df = worker_count.groupby(
        ['TYPE3'])['COUNT'].sum().reset_index()
    worker_count_df.rename(
        columns={'COUNT': 'TYPE3_PERSON_COUNT'}, inplace=True)
    worker_count = pd.merge(
        worker_count,
        worker_count_df,
        on='TYPE3',
        how='inner'
    )
    del worker_count_df
    worker_count.drop(['COUNT'], inplace=True, axis=1)
    # 各部门单位系数（以站段为单位）
    worker_count['DP_COEFFICIENT'] = worker_count.apply(
        lambda row:
        (row['TYPE3_PERSON_COUNT'] * row['TYPE3_COUNT'] / row['TOTAL_PERSON_COUNT'])
        if row['TOTAL_PERSON_COUNT'] > 0 else 0
        , axis=1)

    # 获取当月问题暴露度指数分数
    calc_month = get_custom_month(months_ago)
    calc_month = calc_month[1].replace('-', '')
    calc_month = calc_month[:-2]
    condition = {
        "MON": int(calc_month),
        "MAJOR": risk_type.split('-')[0],
        "TYPE": int(risk_type.split('-')[1]),
        "MAIN_TYPE": 5,
        "DETAIL_TYPE": 0,
        "HIERARCHY": 3
    }
    saved_condition = {
        '_id': 0,
        "DEPARTMENT_ID": 1,
        "SCORE": 1
    }
    current_month_problem_exposure_data = pd.DataFrame(
        get_data_from_mongo_by_find(
            months_ago, condition, saved_condition, 'detail_major_index')
    )
    if current_month_problem_exposure_data.empty:
        mean = 0
        worker_count['EXPOSURE_COEFFICIENT'] = 1
    else:
        mean = float(current_month_problem_exposure_data['SCORE'].mean())
        mean = min(60, mean)
        current_month_problem_exposure_data['EXPOSURE_COEFFICIENT'] = current_month_problem_exposure_data.apply(
            lambda row: 1 + ((row['SCORE'] - mean) / mean) if mean > 0 else 0, axis=1)
        # 将暴露度指数合并
        worker_count = pd.merge(
            worker_count,
            current_month_problem_exposure_data[[
                'EXPOSURE_COEFFICIENT', 'DEPARTMENT_ID']],
            left_on='TYPE3',
            right_on='DEPARTMENT_ID',
            how='left'
        )
        worker_count.drop(['DEPARTMENT_ID'], inplace=True, axis=1)
    worker_count.drop_duplicates(subset=['TYPE3'], keep='first', inplace=True)
    # 求整改系数
    # 获取前2月的数据
    # 数据转成df标准格式

    # 整理df格式
    # 当前月份的数据跟上个月数据对比
    # 查询的当前月份数据格式应该如下
    """
    columns : FK_DEPARTMENT_ID, FK_PROBLEM_BASE_ID, RISK_LEVEL, COUNT, PROBLEM_POINT
    意思为某个部门某个风险问题，上个月发生了多少次
    """
    stats_months = get_months_from_201712(months_ago)

    repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[0], repeate_happen_problem_sql, check_risk_ids, department_data)
    # 将专业中问题项点合并
    repeate_happen_problem_data = pd.merge(
        repeate_happen_problem_data,
        major_problem_point_info_data,
        on='FK_PROBLEM_BASE_ID',
        how='inner'
    )
    # 初始化前一个月，前2个月数据
    last_month_repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[1], repeate_happen_problem_sql, check_risk_ids, department_data)
    last_month_repeate_happen_problem_data = last_month_repeate_happen_problem_data[[
        'FK_PROBLEM_BASE_ID', 'COUNT']]
    last_month_repeate_happen_problem_data.rename(
        columns={'COUNT': 'PROBLEM_COUNT_2_MONTH'}, inplace=True)
    repeate_happen_problem_data = pd.merge(
        last_month_repeate_happen_problem_data,
        repeate_happen_problem_data,
        how='outer',
        on='FK_PROBLEM_BASE_ID'
    )
    repeate_happen_problem_data.fillna(
        {'COUNT': 0, 'PROBLEM_COUNT_2_MONTH': 0}, inplace=True)
    # 前2个月数据
    last_month_repeate_happen_problem_data = _get_repeate_hidden_problem_data(
        stats_months[2], repeate_happen_problem_sql, check_risk_ids, department_data)
    last_month_repeate_happen_problem_data = last_month_repeate_happen_problem_data[[
        'FK_PROBLEM_BASE_ID', 'COUNT']]
    last_month_repeate_happen_problem_data.rename(
        columns={'COUNT': 'PROBLEM_COUNT_3_MONTH'}, inplace=True)

    repeate_happen_problem_data = pd.merge(
        last_month_repeate_happen_problem_data,
        repeate_happen_problem_data,
        how='outer',
        on='FK_PROBLEM_BASE_ID'
    )
    repeate_happen_problem_data.fillna(
        {'COUNT': 0, 'PROBLEM_COUNT_3_MONTH': 0}, inplace=True)
    del last_month_repeate_happen_problem_data

    repeate_happen_problem_data = pd.merge(
        repeate_happen_problem_data,
        zhanduan_dpid_data,
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    repeate_happen_problem_data.drop(
        ['DEPARTMENT_ID', 'NAME'], inplace=True, axis=1)

    # 将当前月的重复问题数据导入到mongo中
    # 将人数跟重复问题df合并
    repeate_happen_problem_data.rename(columns={
        'COUNT': 'PROBLEM_COUNT'
    }, inplace=True)
    repeate_happen_problem_data = pd.merge(
        worker_count,
        repeate_happen_problem_data,
        how='inner',
        on='TYPE3'
    )
    # 计算分数,初始扣0分
    repeate_happen_problem_data['SCORE'] = 0
    repeate_happen_problem_data['REAL_THRESHOLD_VALUES'] = 0
    repeate_happen_problem_data['BASE_THRESHOLD_VALUES'] = 0
    repeate_happen_problem_data['CONTENT'] = ''
    deduct_config_dict = {
        # 当前月
        0: {
            1: 3,
            2: 1.5,
            3: 0.6
        },
        # 前一个月
        1: {
            1: 2,
            2: 1,
            3: 0.4
        },
        # 前2个月
        2: {
            1: 1,
            2: 0.5,
            3: 0.2
        }
    }
    risk_desc = {
        1: '重大风险',
        2: '较严重风险',
        3: '一般风险'
    }
    condition.update({'MON': calc_month})
    content_description_title = dict()
    for idx, row in repeate_happen_problem_data.iterrows():
        _base_threshold_values = 20
        risk_level = int(row['RISK_LEVEL'])
        _base_threshold_values = problem_ctl_threshold_dict.get(risk_level, None)
        if not _base_threshold_values:
            continue
        _real_threshold_values = _base_threshold_values * \
                                 row['DP_COEFFICIENT'] * row['EXPOSURE_COEFFICIENT']
        # 向上取整
        _real_threshold_values = math.ceil(_real_threshold_values)
        _str = "<p>{0}：实际阀门值：{1}" + \
               "（基础阀门值{2}*单位系数{3}*问题暴露度系数{4}）</p>"
        if row['TYPE3'] not in content_description_title.keys():
            content_description_title[row['TYPE3']] = dict()
            content_description_title[row['TYPE3']][risk_level] = _str.format(risk_desc[risk_level],
                                                                              _real_threshold_values,
                                                                              _base_threshold_values,
                                                                              round(row['DP_COEFFICIENT'], 4),
                                                                              round(row['EXPOSURE_COEFFICIENT'], 4))
        else:
            if risk_level not in content_description_title[row['TYPE3']].keys():
                content_description_title[row['TYPE3']][risk_level] = _str.format(risk_desc[risk_level],
                                                                                  _real_threshold_values,
                                                                                  _base_threshold_values,
                                                                                  round(row['DP_COEFFICIENT'], 4),
                                                                                  round(row['EXPOSURE_COEFFICIENT'], 4))
        # 求a值扣分,目标月超出实际阀门值才继续其他月份扣分
        _deduct_score = 0
        count_col_list = ['PROBLEM_COUNT', 'PROBLEM_COUNT_2_MONTH', 'PROBLEM_COUNT_3_MONTH']
        for i, month_pro_count in enumerate(count_col_list):
            if row[month_pro_count] > _real_threshold_values:
                _deduct_score += deduct_config_dict[i][risk_level]
            else:
                break
        if _deduct_score > 0:
            _score = _deduct_score / row['EXPOSURE_COEFFICIENT']
            repeate_happen_problem_data.loc[idx,
                                            'REAL_THRESHOLD_VALUES'] = _real_threshold_values
            repeate_happen_problem_data.loc[idx,
                                            'BASE_THRESHOLD_VALUES'] = _base_threshold_values
            repeate_happen_problem_data.loc[idx, 'SCORE'] = _score
            repeate_happen_problem_data.loc[idx, 'CONTENT'] = f"<p>问题项({str(row['FK_PROBLEM_BASE_ID']).strip()})</p>" + \
                                                              f"<p>描述: {row['PROBLEM_POINT'].strip()}</p>" + \
                                                              f"<p>风险级别:{risk_desc[risk_level]}</p>" + \
                                                              f"<p>当月发生{row['PROBLEM_COUNT']}次,上个月发生{row['PROBLEM_COUNT_2_MONTH']}次," + \
                                                              f"前2月发生{row['PROBLEM_COUNT_3_MONTH']}次.</p>"
        else:
            continue
    # 按站段将文本内容组合
    rst_data = []
    for type3 in set(repeate_happen_problem_data['TYPE3'].values.tolist()):
        desc_title = sorted(content_description_title[type3].items(), key=lambda x: x[0])
        desc_title = [_title[1] for _title in desc_title]
        desc_title = ''.join(desc_title)
        content_df = repeate_happen_problem_data[repeate_happen_problem_data['TYPE3'] == type3]
        lsiss = [item for item in content_df['CONTENT'].values.tolist()
                 if item]
        lsiss.insert(0, desc_title)
        content = '不存在连续重复发生的问题'
        if lsiss:
            content = '<br/>'.join(lsiss)
        score = float(content_df['SCORE'].sum())
        rst_data.append([type3, score, content])
    data = pd.DataFrame(data=rst_data, columns=[
        'FK_DEPARTMENT_ID', 'SCORE', 'CONTENT'])
    # 删除引用
    del repeate_happen_problem_data
    # 导出中间过程数据
    rst = pd.merge(
        choose_dpid_data(3),
        data,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_rst = format_export_basic_data(rst, 6, 3,
                                        3, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 3, risk_type)
    data = df_merge_with_dpid(data, department_data)

    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        6,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: max(100 - x, 0),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


def stats_repeatedly_index_ratio(
        problem_count,
        total_problem_count, months_ago,
        risk_type, choose_dpid_data,
        customizecontent=None,
        calc_score_by_formula=_calc_repeatedly_index_ratio_score,
        fraction=None,
        calc_alpha_func=get_last_month_alpha
):
    # 获取上个月的ratio及专业基数
    last_index_ratio = calc_alpha_func(months_ago, risk_type, 6, 3)
    return calc_child_index_type_divide_major(
        problem_count,
        total_problem_count,
        2,
        6,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        fraction=fraction,
        last_index_ratio=last_index_ratio
    )
