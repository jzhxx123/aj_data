# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data_sql
   Author :       hwj
   date：          2019/11/12上午11:32
   Change Activity: 2019/11/12上午11:32
-------------------------------------------------
"""

# 检查信息关联检查项目(即所有符合项目的检查信息)
CHECK_INFO_ASSOCIATE_ITEM_SQL = """
    SELECT
        DISTINCT
        a.PK_ID AS CHECK_INFO_ID,
        a.IS_YECHA,
        a.IS_GENBAN,
        a.CHECK_WAY,
        a.CHECK_TYPE,
        DAY(a.END_CHECK_TIME) AS DAY,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR
    FROM
        t_check_info as a 
    INNER JOIN
        t_check_info_and_item as b on a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
            DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            and b.FK_CHECK_ITEM_ID in ({2})
"""

# 检查信息关联人员
CHECK_INFO_ASSOCIATE_PERSON_SQL = """SELECT
    DISTINCT
    a.PK_ID AS CHECK_INFO_ID,
    b.PK_ID AS CHECK_INFO_PERSON_ID,
    b.FK_DEPARTMENT_ID,
    1 as COUNT 
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查信息关联调阅时长
CHECK_INFO_MEDIA_TIME_SQL = """SELECT DISTINCT
    a.PK_ID AS CHECK_INFO_ID,
    b.PK_ID AS CHECK_MEDIA_ID,
    b.COST_TIME AS COUNT
    FROM 
    t_check_info AS a
    INNER JOIN 
    t_check_info_and_media AS b ON b.fk_check_info_id = a.pk_id
    WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 检查信息关联检查部门(所有受到检查的部门)
CHECK_INFO_DEPARTMENT_ADDRESS_SQL = """
SELECT DISTINCT
    a.PK_ID AS CHECK_INFO_ID,
    b.FK_DEPARTMENT_ID,
    1 AS COUNT 
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.TYPE = 1
"""

# 检查信息关联检查检查地点(所有受到检查的地点)
CHECK_INFO_POINT_ADDRESS_SQL = """
SELECT DISTINCT
    a.PK_ID AS CHECK_INFO_ID,
    b.FK_CHECK_POINT_ID AS CHECK_POINT_ID,
    1 AS COUNT 
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.TYPE = 2
"""



