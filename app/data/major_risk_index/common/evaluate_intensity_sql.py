# 干部评价记分总条数
# 筛选项:时间:系统月、附标:不含JL-2-1条目、
# 风险类型:“车务-调车-%”“车务-防溜-%”“车务-调乘一体化-%”
EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 干部主动评价记分总条数
# 筛选项:时间:系统月、附标: 主动评价不含事故故障倒查和JL-2-1条目及系统自动评价、
# 风险类型:“车务-调车-%”“车务-防溜-%”“车务-调乘一体化-%”。”、
# 检查类型:站段检查、是否自动评价:否
ACTIVE_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 科职主动评价记分条数(科职干部包含正科、副科职干部)
ACTIVE_KEZHI_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 2 AND 3
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 干部主动评价记分总分数
ACTIVE_EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# （主动）段机关干部评价记分条数
DUAN_CADRE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_check_evaluate_and_risk AS d
            ON d.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND b.IDENTITY = '干部'
            AND c.TYPE = 7
            AND d.FK_RISK_ID IN ({2})
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 分析中心得分
ANALYSIS_CENTER_ASSESS_SQL = """SELECT
        a.FK_DEPARTMENT_ID, b.GRADES_TYPE, b.ACTUAL_SCORE
    FROM
        t_analysis_center_assess AS a
            LEFT JOIN
        t_analysis_center_assess_detail AS b
            ON a.PK_ID = b.FK_ANALYSIS_CENTER_ASSESS_ID
    WHERE
        a.YEAR = {0}
            AND a.MONTH = {1}
            AND a.STATUS IN (1, 3)
            AND a.TYPE = 3
            AND b.ACTUAL_SCORE > 0
    ORDER BY a.FK_DEPARTMENT_ID;
"""

# 主动评价记录
ACTIVE_EVALUATE_SCORE_ALL_SQL = """SELECT
        b.ID_CARD, b.FK_DEPARTMENT_ID, a.CHECK_TYPE, a.SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND a.YEAR = {0}
            AND a.MONTH = {1}
            AND c.FK_RISK_ID IN ({2})
"""

# 干部评价记分总分数
EVALUATE_SCORE_ALL_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 干部评价记分总分数(指定风险)
EVALUATE_SCORE_PART_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""
# 路局对站段的评价
LUJU_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 1
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 近三个月被评价人次数量
EVALUATE_PEOPLE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 三个月所有评价人次总数
ZHANDUAN_EVALUATE_PEOPLE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_department AS d on d.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.TYPE2 = '{2}'
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 截止到上个月今年累计得分
ACCUMULATIVE_EVALUATE_SCORE_SQL = """
select sa.ID_CARD, sum(sa.BEFORE_SCORE) as BEFORE_SCORE
from
(
SELECT
     distinct(a.PK_ID),  b.ID_CARD, a.SCORE AS BEFORE_SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            > DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
        AND a.YEAR = {2}
        AND c.FK_RISK_ID in ({3})
        ) as sa
    GROUP BY sa.ID_CARD
"""

# 当月得分-路局评价
LUJU_EVALUATE_SCORE_SQL = """
select sa.ID_CARD, sum(sa.LUJU_SCORE)  as LUJU_SCORE
from
(
SELECT
     distinct(a.PK_ID), b.ID_CARD, a.SCORE AS LUJU_SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
        AND a.YEAR = {2}
        AND (a.CHECK_TYPE = 1
        OR (a.CHECK_TYPE = 2 AND a.EVALUATE_WAY = 0))
        AND c.FK_RISK_ID in ({3})
        ) as sa
    GROUP BY sa.ID_CARD
"""

# 当月得分-站段评价
ZHANDUAN_EVALUATE_SCORE_SQL = """
select sa.ID_CARD, sum(sa.ZHANDUAN_SCORE) as ZHANDUAN_SCORE
from
(
SELECT
      distinct(a.PK_ID), b.ID_CARD, a.SCORE AS ZHANDUAN_SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
        AND a.YEAR = {2}
        AND a.CHECK_TYPE = 2
        AND a.EVALUATE_WAY > 0
        AND c.FK_RISK_ID in ({3})
        ) as sa
    GROUP BY sa.ID_CARD
"""

# 当月得分-站段评价
ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL = """
select sa.ID_CARD, sum(sa.ZHANDUAN_SCORE_NOT_JL2) as ZHANDUAN_SCORE_NOT_JL2
from
(
SELECT
        distinct(a.PK_ID),b.ID_CARD, a.SCORE AS ZHANDUAN_SCORE_NOT_JL2
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
        AND a.YEAR = {2}
        AND a.CHECK_TYPE = 2
        AND a.EVALUATE_WAY > 0
        AND a.`CODE` NOT LIKE 'JL-2%%'
        AND c.FK_RISK_ID in ({3})
        ) as sa
    GROUP BY sa.ID_CARD
"""

# 人员表
PERSON_ID_CARD_SQL = """SELECT
        ID_CARD, PERSON_NAME, FK_DEPARTMENT_ID
    FROM
        t_person
"""
