# -*- coding: utf-8 -*-
'''
    问题整改效果指数。df转标准化，分数计算公共函数模块。
'''
import pandas as pd

# ---------------- 整改成效 df 转标准格式函数 ---------------#


def translate_rectification_effect_type_a(filed, raw_df):
    """[统计a类型的整改成效问题，
    目标导出部门对应的数量，
    以及对应的文字描述]

    Arguments:
        filed {[type]} -- [description]
        raw_df {[type]} -- [description]
        dpid {[type]} -- [description]
    """
    # main_type表示不同的类型，RESPONSIBILITY_IDENTIFIED表示责任级别
    # 默认type3 站段级别
    content = []
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    raw_df['RESP_LEVEL'] = 0
    for idx, row in raw_df.iterrows():
        if row['RESPONSIBILITY_IDENTIFIED'] in [1, 2, 3, 8]:
            raw_df.loc[idx, 'RESP_LEVEL'] = 1
        elif row['RESPONSIBILITY_IDENTIFIED'] in [4, 5, 9]:
            raw_df.loc[idx, 'RESP_LEVEL'] = 2
        else:
            raw_df.loc[idx, 'RESP_LEVEL'] = 3
    raw_df = raw_df.groupby(['TYPE3', 'MAIN_TYPE', 'RESP_LEVEL'])[
        'COUNT'].sum().reset_index()
    type3_set = set(raw_df['TYPE3'].values.tolist())
    for type3 in type3_set:
        data = raw_df[raw_df['TYPE3'] == type3]
        main_type_set = set(data['MAIN_TYPE'].values.tolist())
        type3_content = ''
        for main_type in main_type_set:
            main_type_data = data[data['MAIN_TYPE'] == main_type]
            first_title = main_type_title.get(int(row['MAIN_TYPE']))
            tpl = f"<p>类型：{first_title}</p>"
            for idx, row in main_type_data.iterrows():
                first_title = main_type_title.get(int(row['MAIN_TYPE']))
                second_title = resp_level_title.get(int(row['RESP_LEVEL']))
                count = int(row['COUNT'])
                tpl += f"{second_title}：{count}个、"
            type3_content += tpl
        content.append([type3, type3_content[:-1]])
    rst = pd.DataFrame(data=content, columns=[
                       'FK_DEPARTMENT_ID', f'CONTENT_{filed}'])
    raw_df.rename(columns={
        'COUNT': f"COUNT_{filed}",
        'TYPE3': "FK_DEPARTMENT_ID"
    }, inplace=True)
    raw_df = pd.merge(
        rst,
        raw_df,
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    return raw_df


def translate_rectification_effect_type_b(filed, raw_df):
    """[统计类型b，转换成标准格式，目标导出部门对应的数量，
    以及对应的文字描述]

    Arguments:
        filed {[type]} -- [description]
        raw_df {[type]} -- [description]
    """
    # 站段级别聚合
    raw_df = raw_df.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    raw_df[f'CONTENT_{filed}'] = ''
    for idx, row in raw_df.iterrows():
        raw_df.loc[idx,
                   f'CONTENT_{filed}'] = f"<p>警告性预警延期次数: {row['COUNT']}</p>"
    raw_df.rename(columns={
        "COUNT": f"COUNT_{filed}",
        'TYPE3': "FK_DEPARTMENT_ID"
    }, inplace=True)

    return raw_df


def translate_rectification_effect_type_c(filed, raw_df):
    """[统计类型d，转换成标准格式，目标导出部门对应的数量，
    以及对应的文字描述]
    防控责任部门
    Arguments:
        filed {[type]} -- [description]
        raw_df {[type]} -- [description]
    """
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    raw_df = raw_df.groupby(['TYPE3', 'MAIN_TYPE'])['COUNT'].sum().reset_index()
    raw_df[f'CONTENT_{filed}'] = ''
    for idx, row in raw_df.iterrows():
        title = main_type_title.get(int(row['MAIN_TYPE']))
        content = f"<p>{title}防控责任: {row['COUNT']}个</p>"
        raw_df.loc[idx, f'CONTENT_{filed}'] = content
    raw_df.rename(columns={
        'COUNT': f'COUNT_{filed}',
        'TYPE3': 'FK_DEPARTMENT_ID',
        'MAIN_TYPE': f'MAIN_TYPE_{filed}'}, inplace=True)
    return raw_df


def translate_rectification_effect_type_d(filed, raw_df):
    """[统计类型d，转换成标准格式，目标导出部门对应的数量，
    以及对应的文字描述]

    Arguments:
        filed {[type]} -- [description]
        raw_df {[type]} -- [description]
    """
    risk_rank_title = {
        1: 'I级警告预警通知书', 2: 'II级警告预警通知书', 3: 'III级警告预警通知书'
    }
    raw_df = raw_df.groupby(['TYPE3', 'RANK'])['COUNT'].sum().reset_index()
    raw_df[f'CONTENT_{filed}'] = ''
    for idx, row in raw_df.iterrows():
        title = risk_rank_title.get(int(row['RANK']))
        content = f"<p>{title}: {row['COUNT']}个</p>"
        raw_df.loc[idx, f'CONTENT_{filed}'] = content
    raw_df.rename(columns={
        'COUNT': f'COUNT_{filed}',
        'TYPE3': 'FK_DEPARTMENT_ID'}, inplace=True)
    return raw_df


def translate_rectification_effect_type_e(filed, raw_df):
    """[统计类型d，转换成标准格式，目标导出部门对应的数量，
    以及对应的文字描述]
    指数排名扣分
    调车风险指数连续两个月排末位，扣5分，连续三个月排末位，扣10分；连续四个月排末位，扣20分。
    Arguments:
        filed {[type]} -- [description]
        raw_df {[type]} -- [description]--columns: DEPARTMENT_ID, DEPARTMENT_NAME, RANK, MON_AGO
    """
    lenth = len(set(raw_df['DEPARTMENT_ID'].values.tolist()))
    # 找出每个月末尾的部门
    mons = set(raw_df['MON_AGO'].values.tolist())
    mons = sorted(list(mons), reverse=True)
    dp_id = ''
    flag = 0
    rst = dict()
    tmp_dict = dict()
    for mon in mons:
        _tmp_df = raw_df[
            (raw_df['MON_AGO'] == mon) &
            (raw_df['RANK'] == lenth)
             ]
        if _tmp_df.empty:
            continue
        if dp_id == _tmp_df['DEPARTMENT_ID'].values[0]:
            flag += 1
        else:
            if rst.get(dp_id, None):
                if rst[dp_id] >= flag:
                    pass
                else:
                    rst[dp_id] = flag
            else:
                rst[dp_id] = flag

            dp_id = _tmp_df['DEPARTMENT_ID'].values[0]
            flag = 1
            tmp_dict[dp_id] = flag
    set_key = set(tmp_dict.keys()) - set(rst.keys())
    for i in set_key:
        rst[i] = tmp_dict[i]
    raw_df[f'CONTENT_{filed}'] = ''
    raw_df[f'COUNT_{filed}'] = 0
    rst_data = []
    for dpid in set(raw_df['DEPARTMENT_ID'].values.tolist()):
        content = f"<p>不存在连续月排末位</p>"
        count = 0
        if rst.get(dpid, None):
            count = 1
            if rst[dpid] > 1:
                count = rst[dpid]
                content = f"<p>连续{rst[dpid]}个月排末位</p>"
        rst_data.append(
            {
                f'CONTENT_{filed}': content,
                f'COUNT_{filed}': count,
                'FK_DEPARTMENT_ID': dpid,
            }
        )
    raw_df = pd.DataFrame(rst_data)
    return raw_df


# ---------------- 整改成效 df 计算分数公共函数 ---------------#
def calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        if int(row['COUNT_A']) >= 1:
            _score = 100 if int(row['COUNT_A']) >= 2 else 50
    elif int(row['MAIN_TYPE']) == 2:
        if int(row['COUNT_A']) >= 1:
            if int(row['COUNT_A']) == 1:
                _score = 20
            elif int(row['COUNT_A']) == 2:
                _score = 45
            elif int(row['COUNT_A']) == 3:
                _score = 75
            else:
                _score = 100
    else:
        if int(row['COUNT_A']) >= 1:
            if int(row['COUNT_A']) == 1:
                _score = 10
            elif int(row['COUNT_A']) == 2:
                _score = 25
            elif int(row['COUNT_A']) == 3:
                _score = 45
            else:
                _score = 100
    return _score


def calc_rectification_effect_type_b(row):
    """[计算警告性预警延期的分数]

    Arguments:
        row {[type]} -- [description]
    """
    _score = 10 * row['COUNT_B']
    return _score


def calc_rectification_effect_type_c(row):
    """[计算事故防控责任]
    事故防控责任每件扣5分
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE_C']) == 1:
        _score = int(row['COUNT_C']) * 5
    return _score


def calc_rectification_effect_type_d(row):
    """[计算根据级别的预警通知书]
    III级警告预警通知书一次扣5分；II级警告预警通知书一次扣10分；I级警告预警通知书一次扣20分。
    Arguments:
        row {[type]} -- [description]
    """
    _score = 5 * (2 ** (3 - int(row['RANK']))) * int(row['COUNT_D'])
    return _score


def calc_rectification_effect_type_e(row):
    """[风险指数排名扣分]
    风险指数连续两个月排末位，扣5分，连续三个月排末位，扣10分；连续四个月排末位，扣20分
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if row['COUNT_E'] > 1:
        _score = 5 * (2 ** (row['COUNT_E'] - 2))
    return _score
