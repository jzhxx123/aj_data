# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.TYPE3 AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT a.PROBLEM_POINT) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_department AS b ON a.EXECUTE_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.TYPE3
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PROBLEM_POINT) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 3 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
            AND FK_CHECK_ITEM_ID IN ({0})
    GROUP BY FK_DEPARTMENT_ID;
"""

# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT
        PK_ID AS CHECK_POINT_ID, FK_DEPARTMENT_ID
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0 AND HIERARCHY = 2;
"""

# 重要检查点受检次数
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID
    FROM
        t_department AS a
            INNER JOIN
        t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 9
            AND b.SOURCE_ID IN ({0})
            AND a.IS_DELETE = 0;
"""

# 班组受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.TYPE5 AS DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            INNER JOIN
        t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
            INNER JOIN
        t_department_and_info AS e ON a.FK_DEPARTMENT_ID = e.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.HIERARCHY BETWEEN 5 AND 6
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY NOT BETWEEN 4 AND 6
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND e.SOURCE_ID IN ({2})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.TYPE5;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE = 9
            AND a.SOURCE_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT c.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY NOT BETWEEN 4 AND 6
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 每天检查数-按时段
HOUR_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR,
        COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_check_info_and_item as c on a.pk_id = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND c.FK_CHECK_ITEM_ID in ({2})
    GROUP BY b.FK_DEPARTMENT_ID , START_HOUR, END_HOUR;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_H_SQL = """SELECT
        DISTINCT
        a.FK_DEPARTMENT_ID,  a.WORK_TYPE, a.WORK_TIME
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE = 9 AND b.IS_DELETE = 0
            AND a.SOURCE_ID IN ({0})
"""

# 派单人时记录，先按END_TIME计算, 取出之后再做跨天的时长分配
# gw_t_ps_dailyplan表中的tyoe字段表示作业类型
# 天窗修：{0:"请点“,(5,8):"一级维修",(6,9):"二级维修"}
# 施工：{"一级施工","二级施工",(3,4):"三级施工"}
# 点外修:{(1,2):"点外作业"}
DAILY_CHECK_HOUR_SQL = """SELECT
        a.DEPARTID as FK_DEPARTMENT_ID,
        a.ON_RAILWAY_NUMBER,
        a.ON_RAILWAY_NUMBER * ( UNIX_TIMESTAMP( b.END_TIMES ) - UNIX_TIMESTAMP( b.START_TIMES ) ) / 3600 AS HOURS,
        CONVERT(DATE_FORMAT( b.END_TIMES, '%%d' ), UNSIGNED) AS DAY,
        DATE_FORMAT(b.END_TIMES, '%%Y-%%m-%%d %%H:%%i') as ET,
        DATE_FORMAT(b.START_TIMES, '%%Y-%%m-%%d %%H:%%i') as ST
    FROM
        gw_t_ps_dailyplan AS a
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND 
        a.TYPE IN ( 0, 1, 2, 3, 4, 5, 6, 8, 9 ) 
        AND 
        b.DISPATCH_ORDER_STATUS = 1
"""

# # 重要检查点--PERSON_NUMBER作业人数,PERSON_COUNT部门人数
# CHECK_POINT_PERSON_COUNT_SQL = """
#     SELECT
#     a.PK_ID AS CHECK_POINT_ID,
#     b.PERSON_NUMBER,
#     COUNT( distinct a.PK_ID ) AS PERSON_COUNT,
#     b.SOURCE_DEPARTMENT_ID,
#     a.ALL_NAME AS ADDRESS_NAME
#     FROM
#     t_check_point AS a
#     LEFT JOIN t_department_and_main_production_site AS b ON b.FK_ADDRESS_ID = a.PK_ID
#     INNER JOIN t_person AS c ON b.SOURCE_DEPARTMENT_ID = c.FK_DEPARTMENT_ID
#     LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
#     WHERE
#     a.IS_DELETE = 0
#     AND c.IS_DELETE = 0
#     AND a.HIERARCHY = 2
#     AND b.TYPE = 2
#     AND d.IS_DELETE = 0
#     AND TYPE2 = '{0}'
#     GROUP BY
#     a.PK_ID,
#     b.PERSON_NUMBER,
#     b.SOURCE_DEPARTMENT_ID,
#     a.ALL_NAME
# """


# 重要检查点--PERSON_NUMBER作业人数,
# 此项查出地点为主要生产场所及其关联的部门
CHECK_POINT_PERSON_COUNT_SQL = """SELECT DISTINCT
    tcp.PK_ID AS CHECK_POINT_ID,
    tcp.ALL_NAME AS ADDRESS_NAME,
    tds.SOURCE_DEPARTMENT_ID,
    tds.PERSON_NUMBER 
FROM
    t_check_point AS tcp
    INNER JOIN
    t_department_and_main_production_site AS tds ON tcp.PK_ID = tds.FK_ADDRESS_ID 
WHERE
    tds.TYPE = 2 
    AND tcp.TYPE = 1
    AND tcp.IS_DELETE = 0
    AND tcp.FK_DEPARTMENT_ID IN (
    SELECT
        a.DEPARTMENT_ID
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
    )
"""

# # 重要检查点关联站段
# CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
#         cp.PK_ID, dep.TYPE3
#     FROM
#         t_check_point AS cp
#             INNER JOIN
#         t_department AS dep
#             ON cp.FK_DEPARTMENT_ID = dep.DEPARTMENT_ID
#     WHERE
#         cp.IS_DELETE = 0
#         AND TYPE2 = '{0}'
#
# """

# 重要检查点-专业下所有具备检查项目的部门
CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
    a.FK_DEPARTMENT_ID,
    COUNT( DISTINCT c.PK_ID ) AS PERSON_COUNT 
FROM
    t_department_and_info AS a
    INNER JOIN t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    INNER JOIN t_person AS c ON b.DEPARTMENT_ID = c.FK_DEPARTMENT_ID 
WHERE
    MAIN_TYPE = 1 
    AND b.TYPE2 = '{0}' 
    AND a.SOURCE_ID IN ({1}) 
    AND b.IS_DELETE = 0 
    AND c.IS_DELETE = 0 
GROUP BY
    a.FK_DEPARTMENT_ID
"""

# 重要检查点受检次数
POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID as CHECK_POINT_ID, COUNT(DISTINCT a.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
                    LEFT JOIN
        t_check_info_and_item as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数及其部门人数统计
CHECK_BANZU_PERSON_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS PERSON_COUNT_2
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
    GROUP BY a.DEPARTMENT_ID;
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME
    FROM
        t_department
    WHERE
        TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND TYPE2 = '{0}'
"""

# 班组受检次数
BANZU_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_check_info_and_item as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
            AND d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY c.DEPARTMENT_ID;
"""