# -*- coding: utf-8 -*-
"""
    desc: 问题暴露度指数
"""

import pandas as pd

from app.data.index.util import (
    get_custom_month, get_months_from_201712, get_months_from_201712_two)
from app.data.major_risk_index.common.const import MainType, \
    ProblemExposureDetailType
from app.data.major_risk_index.util import (
    append_major_column_to_df, calc_child_index_type_sum,
    calc_extra_child_score_groupby_major, df_merge_with_dpid,
    export_basic_data_one_field_monthly, format_export_basic_data,
    export_basic_data_one_field_monthly_excellent,
    write_cardinal_number_basic_data,
    summizet_operation_set, write_export_basic_data_to_mongo,
    calc_extra_child_score_groupby_major_two,
    calc_extra_child_score_groupby_major_third,
    calc_child_index_type_divide_major)
from app.data.util import pd_query

HIERARCHY = [3]


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_score_by_formula_type_jiwu(row,
                                     column,
                                     major_column,
                                     detail_type=None):
    """【机务】该比值≥20%，得分为100分；10%≤该比值〈20%，得分90分；
        5%≤该比值〈10%，得分80分；该比值〈5%，得分70分。
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0.1:
        _score = 90
    elif _ratio >= 0.05:
        _score = 80
    else:
        _score = 70
    return _score


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False,
                           fraction=None,
                           zhanduan_filter_list=None,
                           ratio_df=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy),
                                         fraction,
                                         fraction.risk_type,
                                         MainType.problem_exposure,
                                         fraction.detail_type,
                                         fraction.months_ago,
                                         ['prob', 'PERSON_NUMBER'])
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula,
            weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula,
        weight=weight,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list, ratio_df=ratio_df)


# 部分专业基数
def _calc_value_per_person_two(series,
                               work_load,
                               weight,
                               hierarchy,
                               choose_dpid_data,
                               zhanduan_filter_list,
                               calc_score_formula=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    # data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    data['ratio'] = data.apply(lambda row: row['prob'] / row['PERSON_NUMBER']
    if row['PERSON_NUMBER'] > 0 else 0, axis=1)
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula,
        zhanduan_filter_list, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER')


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False,
                                 fraction=None,
                                 zhanduan_filter_list=None,
                                 ratio_df=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(
        prob_number, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list,
        ratio_df=ratio_df)


# 部分专业基数
def _calc_prob_number_per_person_two(df_data,
                                     work_load,
                                     department_data,
                                     choose_dpid_data,
                                     weight,
                                     hierarchy,
                                     zhanduan_filter_list,
                                     calc_score_formula=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person_two(prob_number, work_load, weight, hierarchy,
                                      choose_dpid_data, zhanduan_filter_list,
                                      calc_score_formula)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False,
                                fraction=None, zhanduan_filter_list=None,
                                ratio_df=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(
        prob_score, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list,
        ratio_df=ratio_df)


# 部分专业基数
def _calc_prob_score_per_person_two(df_data,
                                    work_load,
                                    department_data,
                                    choose_dpid_data,
                                    weight,
                                    hierarchy,
                                    zhanduan_filter_list,
                                    calc_score_formula=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person_two(prob_score, work_load, weight, hierarchy,
                                      choose_dpid_data, zhanduan_filter_list,
                                      calc_score_formula)


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


# 总体暴露度(机务专业)
def stats_total_problem_exposure_type_jiwu(
        check_item_ids, check_problem_sql, work_load, department_data,
        months_ago, risk_type, choose_dpid_data,
        calc_score_formula=_calc_score_by_formula_type_jiwu,
        weight_part=[0.6, 0.4],
        title=['总问题数({0})/人数({1})'],
        fraction_list=[None, None]):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    weight_part = weight_part
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    # 导出中间过程
    for i, data in enumerate([base_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate([base_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person,
                     _calc_prob_score_per_person]):
                weight = weight_part[j]
                score.append(
                    func(
                        data.copy(),
                        work_load,
                        department_data,
                        choose_dpid_data,
                        weight,
                        hierarchy,
                        calc_score_formula,
                        is_calc_score_base_major=True,
                        fraction=fraction_list[i][j]))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 总体暴露度--部分专业基数
def stats_total_problem_exposure_fliter(check_item_ids, check_problem_sql,
                                        work_load,
                                        department_data, months_ago, risk_type,
                                        choose_dpid_data,
                                        zhanduan_filter_list=[],
                                        title=['总问题数({0})/人数({1})',
                                               '一般及以上问题数({0})/人数({1})',
                                               '作业项问题数({0})/人数({1})',
                                               '一般及以上作业项问题数({0})/人数({1})'],
                                        weight_item=[0.3, 0.3, 0.2, 0.2],
                                        weight_part=[0.4, 0.6]
                                        ):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    # weight_item = [0.3, 0.3, 0.2, 0.2]
    # weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    # title = [
    #     '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
    #     '一般及以上作业项问题数({0})/人数({1})'
    # ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person_two,
                     _calc_prob_score_per_person_two]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load, department_data,
                         choose_dpid_data, weight, hierarchy,
                         zhanduan_filter_list))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 总体暴露度- 作业项人数分开
def stats_total_problem_exposure_type_chewu(
        check_item_ids, check_problem_sql, all_load, zuoye_load,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True,
        fraction_list=[[None, None], [None, None], [None, None], [None, None]],
        zhanduan_filter_list=[], calc_score_formula=None):
    """[summary]
    fraction_list中的每项数据保持跟title的数据描述要一致
    Arguments:
        check_item_ids {[type]} -- [description]
        check_problem_sql {[type]} -- [description]
        all_load {[type]} -- [description]
        zuoye_load {[type]} -- [description]
        department_data {[type]} -- [description]
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
        choose_dpid_data {[type]} -- [description]
    
    Keyword Arguments:
        title {[type]} -- [description] (default: {None})
        is_calc_score_base_major {bool} -- [description] (default: {True})
        fraction_list {list} --
        [description] (default:
        {[[None, None],[None, None],[None, None],[None, None]]})
    
    Returns:
        [type] -- [description]
    """
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    weight_item = [0.3, 0.3, 0.2, 0.2]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    if not title:
        title = [
            '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
            '一般及以上作业项问题数({0})/人数({1})'
        ]
    # 导出中间过程
    work_load = [all_load, zuoye_load]
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load[i // 2], department_data, i,
                     title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person,
                     _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load[i // 2], department_data,
                         choose_dpid_data, weight, hierarchy,
                         is_calc_score_base_major=is_calc_score_base_major,
                         zhanduan_filter_list=zhanduan_filter_list,
                         fraction=fraction_list[i][j],
                         calc_score_formula=calc_score_formula))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 总体暴露度
def stats_total_problem_exposure(check_item_ids, check_problem_sql, work_load,
                                 department_data, months_ago, risk_type,
                                 choose_dpid_data,
                                 fraction_list=None, calc_score_formula=None,
                                 ratio_df_dict=None,
                                 is_calc_score_base_major=True):
    if not ratio_df_dict:
        ratio_df_dict = {}
    if not fraction_list:
        fraction_list = [
            [None, None],
            [None, None],
            [None, None],
            [None, None],
        ]
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    weight_item = [0.3, 0.3, 0.2, 0.2]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
        '一般及以上作业项问题数({0})/人数({1})'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            ratio_df_list = ratio_df_dict.get(i)
            if isinstance(calc_score_formula, dict):
                score_formula = calc_score_formula.get(i)
            else:
                score_formula = calc_score_formula
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person,
                     _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                if ratio_df_list:
                    ratio_df = ratio_df_list[j]
                else:
                    ratio_df = None
                score.append(
                    func(data.copy(), work_load, department_data,
                         choose_dpid_data, weight, hierarchy,
                         fraction=fraction_list[i][j],
                         calc_score_formula=score_formula,
                         ratio_df=ratio_df,
                         is_calc_score_base_major=is_calc_score_base_major))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 分析中心查处问题暴露(机务专业)
def stats_analysis_problem_exposure_type_jiwu(
        prob_data, work_load,
        department_data, months_ago,
        risk_type, choose_dpid_data):
    """问题数（60%）：
    分析中心查处的间断瞭望问题数/乘务工作量。
    该比值≥20%，得分为100分；
    10%≤该比值〈20%，得分90分；
    5%≤该比值〈10%，得分80分；
    该比值〈5%，得分70分。
    质量分占（40%）：
    分析中心查处的间断瞭望问题质量分/乘务工作量。
    该比值≥20%，得分为100分；
    10%≤该比值〈20%，得分90分；
    5%≤该比值〈10%，得分80分；
    该比值〈5%，得分70分。
    Arguments:
        prob_data {dataFrame} -- 分析中心查处问题
        work_load {[type]} -- 工作量
    Returns:
        [type] -- [description]
    """
    weight_part = [0.6, 0.4]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['分析中心查处问题数({0})/人数({1})']
    # 导出中间过程
    for i, data in enumerate([prob_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 7, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 7, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate([prob_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person,
                     _calc_prob_score_per_person]):
                weight = weight_part[j]
                score.append(
                    func(
                        data.copy(),
                        work_load,
                        department_data,
                        choose_dpid_data,
                        weight,
                        hierarchy,
                        calc_score_formula=_calc_score_by_formula_type_jiwu))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_g_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            7,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 较严重隐患暴露指数
def stats_hidden_problem_exposure(
        check_item_ids,
        checked_hidden_problem_point_sql,
        hidden_problem_point_sql, department_data,
        months_ago, risk_type, choose_dpid_data,
        calc_score_formula=lambda x: 0 if (100 - x) < 0 else round(
            (100 - x), 2)):
    """较严重隐患暴露:本专业内,其它单位查出的问题项点(本单位基础问题库中有的),
    本单位未查出的按1分/项扣。分值=100-扣
    """
    stats_month = get_custom_month(months_ago)
    checked_pdata = pd_query(
        checked_hidden_problem_point_sql.format(*stats_month, check_item_ids))
    pdata = pd_query(hidden_problem_point_sql.format(check_item_ids))
    deduct_score = {}
    # 记录本月问题扣分次数
    deduct_number_month = []
    for idx, row in checked_pdata.iterrows():
        major_data = pdata[(pdata['MAJOR'] == row['MAJOR'])
                           & (pdata['PK_ID'] == row['PK_ID'])]
        for m_idx, m_row in major_data.iterrows():
            if checked_pdata[
                (checked_pdata['DEPARTMENT_ID'] == m_row['DEPARTMENT_ID']) & (
                        checked_pdata['PK_ID'] == m_row['PK_ID'])].empty:
                deduct_score.update({
                    m_row['DEPARTMENT_ID']:
                        deduct_score.get(m_row['DEPARTMENT_ID'], 0) + 1
                })
                deduct_number_month.append([m_row['DEPARTMENT_ID'], 1])
    df_pdata = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_pdata = df_merge_with_dpid(df_pdata, department_data)
    # 导出中间计算过程数据
    export_basic_data_one_field_monthly(
        deduct_number_month,
        department_data,
        choose_dpid_data(3),
        5,
        2,
        3,
        months_ago,
        lambda x: '本月个数',
        risk_type=risk_type)
    rst_child_score = calc_child_index_type_sum(
        df_pdata,
        2,
        5,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        calc_score_formula,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 较严重隐患暴露指数 - 优化，改sql为data，同时重构计算过程
def stats_hidden_problem_exposure_excellent(
        checked_hidden_problem_point_data,
        hidden_problem_point_data,
        department_data,
        months_ago, risk_type,
        choose_dpid_data,
        calc_score_formula=lambda x: 0 if (100 - x) < 0 else round(
            (100 - x), 2),
        title='前{0}月问题个数: {1}个'):
    """较严重隐患暴露:本专业内,其它单位查出的问题项点(本单位基础问题库中有的),
    本单位未查出的按1分/项扣。分值=100-扣
    """
    # 总检查项点
    total_check_point = set(
        checked_hidden_problem_point_data['FK_PROBLEM_BASE_ID'].values.tolist())

    # 扣分字典
    deduct_score = {}
    deduct_number_month = []
    for hierarchy in HIERARCHY:
        # 层级单位部门id
        all_dpids = set(
            hidden_problem_point_data[f'TYPE{hierarchy}'].values.tolist())
        dpid_list = list(all_dpids)
        for dpid in dpid_list:
            # 对应部门本次检查项点问题
            dpid_check_point_data = checked_hidden_problem_point_data[
                checked_hidden_problem_point_data[f'TYPE{hierarchy}'] == dpid]
            dpid_check_point = set(
                dpid_check_point_data['FK_PROBLEM_BASE_ID'].values.tolist())

            # 部门应检查项点
            should_dpid_check_point_data = hidden_problem_point_data[
                hidden_problem_point_data[f'TYPE{hierarchy}'] == dpid
                ]
            should_dpid_check_point = set(
                should_dpid_check_point_data[
                    'FK_PROBLEM_BASE_ID'].values.tolist())
            difference_set = total_check_point - dpid_check_point

            #  取交集
            intersection = should_dpid_check_point & difference_set

            count = len(intersection)
            deduct_score.update({
                dpid: count
            })
            deduct_number_month.append([dpid, 1, count])

    df_pdata = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])

    df_pdata = df_merge_with_dpid(df_pdata, department_data)

    # 导出中间计算过程数据
    export_basic_data_one_field_monthly_excellent(
        deduct_number_month,
        department_data,
        choose_dpid_data(3),
        5,
        2,
        3,
        months_ago,
        lambda x: '本月个数',
        risk_type=risk_type,
        title=title)

    rst_child_score = calc_child_index_type_sum(
        df_pdata,
        2,
        5,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        calc_score_formula,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 较严重隐患暴露指数-修复deduct_number_month为空无结果的bug,及补充空站段数据
def stats_hidden_problem_exposure_two(check_item_ids,
                                      checked_hidden_problem_point_sql,
                                      hidden_problem_point_sql, department_data,
                                      months_ago, risk_type, choose_dpid_data):
    """较严重隐患暴露:本专业内,其它单位查出的问题项点(本单位基础问题库中有的),
    本单位未查出的按1分/项扣。分值=100-扣
    """
    stats_month = get_custom_month(months_ago)
    checked_pdata = pd_query(
        checked_hidden_problem_point_sql.format(*stats_month, check_item_ids))
    pdata = pd_query(hidden_problem_point_sql.format(check_item_ids))
    deduct_score = {}
    # 记录本月问题扣分次数
    deduct_number_month = []
    for idx, row in checked_pdata.iterrows():
        major_data = pdata[(pdata['MAJOR'] == row['MAJOR'])
                           & (pdata['PK_ID'] == row['PK_ID'])]
        for m_idx, m_row in major_data.iterrows():
            if checked_pdata[
                (checked_pdata['DEPARTMENT_ID'] == m_row['DEPARTMENT_ID']) & (
                        checked_pdata['PK_ID'] == m_row['PK_ID'])].empty:
                deduct_score.update({
                    m_row['DEPARTMENT_ID']:
                        deduct_score.get(m_row['DEPARTMENT_ID'], 0) + 1
                })
                deduct_number_month.append([m_row['DEPARTMENT_ID'], 1])
    df_pdata = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])

    df_pdata = pd.merge(
        df_pdata,
        department_data,
        how='right',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    df_pdata.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    df_pdata.fillna(0, inplace=True)

    # 导出中间计算过程数据
    export_basic_data_one_field_monthly(
        deduct_number_month,
        department_data,
        choose_dpid_data(3),
        5,
        2,
        3,
        months_ago,
        lambda x: '本月个数',
        risk_type=risk_type)
    rst_child_score = calc_child_index_type_sum(
        df_pdata,
        2,
        5,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 较严重隐患暴露指数
def stats_hidden_problem_exposure_ratio(
        self_problem_count,
        problem_count,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=None,
        customizecontent=None,
        zhanduan_filter_list=[]):
    """自查较大隐患问题/所有劳安问题  与对应基数对比"""
    return calc_child_index_type_divide_major(
        self_problem_count,
        problem_count,
        2,
        5,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 事故隐患问题暴露度
def stats_problem_exposure(check_item_ids, zhanduan_dpid,
                           hidden_key_problem_sql,
                           hidden_key_problem_month_sql, department_data,
                           months_ago, risk_type, choose_dpid_data):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712(months_ago)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if idx > 2 and idx < (len(stats_months) - 1):
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 1
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type)
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def stats_problem_exposure_two(check_item_ids, zhanduan_dpid,
                               hidden_key_problem_sql,
                               hidden_key_problem_month_sql, department_data,
                               months_ago, risk_type, choose_dpid_data):
    """连续3月无的扣1分/条，连续6个月无的扣2分/条，连续9个月-4分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712_two(months_ago, months=10)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续3个月，6个月，9个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if idx == 3:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            # exposure_problem = hidden_problem.intersection(
            #     month_hidden_problem)
            # for each_problem in exposure_problem:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + 1
                })
                hidden_problem_num_monthly.append([dpid, idx])

        if idx == 6:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        if idx == 9:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + 4
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type)
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def stats_problem_exposure_three(check_item_ids, zhanduan_dpid,
                                 hidden_key_problem_sql,
                                 hidden_key_problem_month_sql, department_data,
                                 months_ago, risk_type, choose_dpid_data):
    """连续4月无的扣1分/条，连续5个月无的扣2分/条，…扣月份-2分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712_two(months_ago, months=7)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续4个月，5个月，6个,7月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if idx == 4:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            # exposure_problem = hidden_problem.intersection(
            #     month_hidden_problem)
            # for each_problem in exposure_problem:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + 1
                })
                hidden_problem_num_monthly.append([dpid, idx])

        if idx == 5:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + 2
                })
                hidden_problem_num_monthly.append([dpid, idx])

        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type)
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 事故隐患问题暴露度
def stats_problem_exposure_excellent(check_item_ids, zhanduan_dpid,
                                     hidden_key_problem_sql,
                                     hidden_key_problem_month_sql,
                                     department_data,
                                     months_ago, risk_type, choose_dpid_data,
                                     customizededuct=None, months=None):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。
    默认只记录前3，4，5，6个月未暴露的问题， 只扣最后连续月的分数
    a问题连续3，4，5未暴露，只扣5月的分数，
    b问题连续3，4未暴露，只扣4月的分数，
    总扣分等于a+b
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    Returns:
        [type] -- [description]
    """
    if not customizededuct:
        # key,values(mon, deduct)
        customizededuct = {
            3: 1,
            4: 2,
            5: 3,
            6: 4
        }
    stats_months = get_months_from_201712(months_ago, months)
    # 初始化一个各站段的扣分字典,  存储每个月的扣分值，最后只去最后一个连续月扣分
    deduct_score = {k: dict()
                    for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if idx > 0:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                # 因为查出关键隐患的sql中没有过滤不需要的站段或者部门
                if dpid not in deduct_score.keys():
                    continue
                # 问题项
                fk_problem_base_id = each_problem.split('||')[1]
                # 动态初始化字典
                if not isinstance(
                        deduct_score[dpid].get(fk_problem_base_id, None), dict):
                    deduct_score[dpid][fk_problem_base_id] = dict()
                deduct_score[dpid][fk_problem_base_id].update(
                    {
                        idx: deduct_score.get(dpid).get(fk_problem_base_id).get(
                            idx, 0) + customizededuct.get(idx, 0)
                    }
                )
                if idx in customizededuct.keys():
                    hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type)
    # 将扣分字典展开成标准格式
    deduct_score_list = []
    for key, values in deduct_score.items():
        # 针对每项问题只扣最后一个月分数
        # 当存在扣分字典为空时
        # 例如 {'19B8C3534E035665E0539106C00A58FD':{}},默认给0分
        _score = 0
        if bool(values.keys()):
            for pk_id, de_values in values.items():
                if not bool(de_values):
                    continue
                idx = sorted(de_values.keys())[-1]
                _score += de_values[idx]
        deduct_score_list.append([key, _score])
    df_hidden_prob = pd.DataFrame(
        data=deduct_score_list, columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 班组问题暴露度
def stats_banzu_problem_exposure(
        check_item_ids, banzu_point_sql, exposure_problem_department_sql,
        department_data, months_ago, risk_type, choose_dpid_data):
    """计算标准:满分100，一个月未暴露任何基础调车问题的扣1分/个调车作业班组，
    连续2月问题未暴露任何基础调车问题的扣2分/个调车作业班组，
    连续3个月问题未暴露任何基础调车问题的扣3分/个调车作业班组，最低0分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_months = get_months_from_201712(months_ago, 4)
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(
        pd_query(
            banzu_point_sql.format(check_item_ids))['FK_DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    # 用来记录每个班组每个月问题为空白的个数
    exposure_banzu_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            pd_query(
                exposure_problem_department_sql.format(
                    check_item_ids, mon[0],
                    mon[1]))['FK_DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        if len(hidden_banzu) == 0:
            break
        if idx > 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx
                })
                exposure_banzu_num_monthly.append([dpid, idx])
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, department_data)
    # 导出中间计算数据
    export_basic_data_one_field_monthly(
        exposure_banzu_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        4,
        3,
        months_ago,
        lambda x: f'连续{x}个月未暴露问题的班组个数',
        title='暴露班组数:',
        risk_type=risk_type)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        4,
        months_ago,
        'SCORE',
        'SCORE_d',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type,
        NA_value=True)
    return rst_child_score


# TODO 目前没有去判断查出的问题是否属于该站段的问题项点(本单位基础问题库中有的)
# 也没有考虑到a站段对b站段的跨站段查出问题（后续优化）
# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data,
        problem_risk_score={'1': 4, '2': 2, '3': 0.1, }):
    """从他查问题分析,1个月未自查出该问题项点，一般风险问题一条扣0.1分，较大风险扣1分，
    严重风险扣3分；若该问题项点属于事故隐患关键问题的在上述基础上*3。
    每个问题项点最高扣30分。满分100，最低0分。
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(
            check_item_ids, *calc_month))['PROBLEM_DPID_RISK'].values)
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(
            check_item_ids, *calc_month))['PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(
        safety_produce_info_sql.format(check_item_ids, *calc_month))
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # problem_risk_score = {
    #     '1': 4,
    #     '2': 2,
    #     '3': 0.1,
    # }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        problem_score = problem_risk_score.get(each_problem[1], 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, each_problem[1]])
    # 未自查出安全生产信息问题
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            problem_rank = row['PROBLEM_DPID_RISK'].split('||')[2]
            problem_score = problem_risk_score.get(
                problem_rank, 0) * (4 - int(row['MAIN_TYPE']))
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, problem_rank])
    # 导出中间计算过程
    first_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_one_field_monthly(
        calc_problems,
        department_data,
        choose_dpid_data(3),
        5,
        5,
        3,
        months_ago,
        lambda x: first_title.get(x),
        title='他查问题个数',
        risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        5,
        5,
        months_ago,
        'SCORE',
        'SCORE_e',
        lambda x: 30 if x > 30 else x,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# TODO 目前没有去判断查出的问题是否属于该站段的问题项点(本单位基础问题库中有的)
# 也没有考虑到a站段对b站段的跨站段查出问题（后续优化）
# 他查问题暴露度
def stats_other_problem_exposure_excellent(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type,
        choose_dpid_data, problem_risk_score=None):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    # 避免导入冲突
    from app.data.index.common import export_basic_data_tow_field_monthly_two

    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(*calc_month, check_item_ids))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(
        safety_produce_info_sql.format(*calc_month, check_item_ids))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(*calc_month, check_item_ids))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values
    }
    if not problem_risk_score:
        problem_risk_score = {
            1: 4,
            2: 2,
            3: 0.1,
        }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[1]
        risk_level = int(each_problem[2])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=[
        'FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(
        lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(
        other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(
        ['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna(
        '未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(
                risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data,
                                                          department_data,
                                                          zhanduan_dpid,
                                                          5, 5, 3, months_ago,
                                                          first_title,
                                                          second_title,
                                                          columns_list=[(1, 1),
                                                                        (1, 2),
                                                                        (2, 1),
                                                                        (2, 2),
                                                                        (3, 1),
                                                                        (3, 2)])
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(lambda row:
                                                 row["middle_2"] + \
                                                 "<br/><br/>安全生产信息问题<br/>" +
                                                 row["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 5, 3,
                                                   months_ago,
                                                   risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5,
                                     5, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type,
        NA_value=True)
    return rst_child_score


# 总体暴露度 - 优化 - 自定义计算的数据df，title,权重等
def stats_total_problem_exposure_excellent(check_problem_data_list, work_load,
                                           department_data, months_ago,
                                           risk_type,
                                           choose_dpid_data,
                                           zhanduan_filter_list=[],
                                           calc_score_formula=_calc_score_by_formula_type_jiwu,
                                           title=['总问题数({0})/工作量({1})',
                                                  '一般及以上问题数({0})/工作量({1})'],
                                           weight_item=[0.5, 0.5],
                                           weight_part=[0.4, 0.6],
                                           fraction_list=[]):
    """
    总问题数（质量分）/工作量与本专业基数比较（50%），一般及以上数与本专业基数比较（50%）。
    weight_item: 指不同问题类型的权重
    weight_part: 指同类型问题问题数与质量分的不同权重(问题数，分数)
    :param months_ago:
    :return:
    p.s check_problem_data_list 与权重一定要一一对应
    """
    if not fraction_list:
        fraction_list = [
            (None, None) for i in range(len(check_problem_data_list))
        ]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    # 导出中间过程
    for i, data in enumerate(check_problem_data_list):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(check_problem_data_list):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person,
                     _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(func(data.copy(),
                                  work_load,
                                  department_data,
                                  choose_dpid_data, weight, hierarchy,
                                  calc_score_formula,
                                  is_calc_score_base_major=True,
                                  fraction=fraction_list[i][j]))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def stats_key_problem_deduct(
        risk_ids, zhanduan_dpid,
        key_problem_lib_data,
        check_key_problem_month_sql, department_data,
        months_ago, risk_type, choose_dpid_data,
        customizededuct=None, months=None):
    """[summary]
    加分问题库中当月查出1条加3分，该项加分最高加30分；
    扣分以加分问题库中问题项点连续3个月未查出1条的扣3分、
    连续4个月未查出1条的扣5分、连续5个月未查出1条的扣7分、
    连续6个月未查出1条的扣9分........依此类推，该项加分最高加30分。
    Arguments:
        risk_ids {[type]} -- [description]
        zhanduan_dpid {[type]} -- [description]
        hidden_key_problem_sql {[type]} -- [description]
        hidden_key_problem_month_sql {[type]} -- [description]
        department_data {[type]} -- [description]
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
        choose_dpid_data {[type]} -- [description]
    
    Keyword Arguments:
        customizededuct {[type]} -- [description] (default: {None})
        months {[type]} -- [description] (default: {None})
    """
    if not customizededuct:
        # key,values(mon, deduct)
        customizededuct = {
            3: 1,
            4: 2,
            5: 3,
            6: 4
        }
    stats_months = get_months_from_201712(months_ago, months)
    # 初始化一个各站段的扣分字典,  存储每个月的扣分值，最后只去最后一个连续月扣分
    deduct_score = {k: dict()
                    for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    award_score = {k: dict()
                   for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(key_problem_lib_data['FK_PROBLEM_BASE_ID'].values)
    hidden_problem = set(
        [x + '||' + str(y) for x in
         zhanduan_dpid['DEPARTMENT_ID'].values.tolist() for y in
         hidden_problem])
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                check_key_problem_month_sql.format(
                    mon[0], mon[1], risk_ids))['PID'].values)
        # 当月查出问题数，加分
        for _dpid in award_score.keys():
            award_score[idx] = 0
            for _pid in month_hidden_problem:
                if _dpid in _pid:
                    award_score[idx] += 1
        if idx > 0:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                # 因为查出关键隐患的sql中没有过滤不需要的站段或者部门
                if dpid not in deduct_score.keys():
                    continue
                # 问题项
                fk_problem_base_id = each_problem.split('||')[1]
                # 动态初始化字典
                if not isinstance(
                        deduct_score[dpid].get(fk_problem_base_id, None), dict):
                    deduct_score[dpid][fk_problem_base_id] = dict()
                deduct_score[dpid][fk_problem_base_id].update(
                    {
                        idx: deduct_score.get(dpid).get(fk_problem_base_id).get(
                            idx, 0) + customizededuct.get(idx, 0)
                    }
                )
                if idx in customizededuct.keys():
                    hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='关键问题个数',
        risk_type=risk_type)
    # 将扣分字典展开成标准格式
    deduct_score_list = []
    for key, values in deduct_score.items():
        # 针对每项问题只扣最后一个月分数,加上当月的加分
        # 当存在扣分字典为空时
        # 例如 {'19B8C3534E035665E0539106C00A58FD':{}},默认给0分
        _score = 0
        if bool(values.keys()):
            for pk_id, de_values in values.items():
                if not bool(de_values):
                    continue
                idx = sorted(de_values.keys())[-1]
                _award_score = min(30, award_score.get(key).get(idx, 0))
                _score += de_values[idx] - _award_score
        deduct_score_list.append([key, _score])
    df_hidden_prob = pd.DataFrame(
        data=deduct_score_list, columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_g',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 问题数
def stats_problem_count_ratio(problem_count,
                              work_load,
                              months_ago,
                              risk_type,
                              calc_score_by_formula=_calc_score_by_formula,
                              zhanduan_filter_list=None,
                              choose_dpid_data=None,
                              customizecontent=None,
                              fraction=None,
                              major_ratio=None,
                              is_calc_score_base_major=True
                              ):
    """
    问题数/基本工作量
    :param problem_count: 问题数
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        problem_count,
        work_load,
        2,
        5,
        14,
        months_ago,
        'COUNT',
        'SCORE_n',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major
    )


# 质量分占比
def stats_problem_score_ratio(
        problem_score,
        work_load,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=None,
        choose_dpid_data=None,
        customizecontent=None,
        fraction=None,
        major_ratio=None,
        is_calc_score_base_major=True
):
    """
    问题质量分/基本工作量
    :param problem_score: 问题质量分
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        problem_score,
        work_load,
        2,
        5,
        15,
        months_ago,
        'COUNT',
        'SCORE_o',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major,
    )


# 中高质量问题数占比
def stats_middle_problem_count_ratio(
        middle_problem_count,
        work_load,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=None,
        choose_dpid_data=None,
        customizecontent=None,
        fraction=None,
        major_ratio=None,
        is_calc_score_base_major=True,
):
    """
     中高质量问题数/基本工作量
    :param middle_problem_count: 中高质量问题数
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        middle_problem_count,
        work_load,
        2,
        5,
        16,
        months_ago,
        'COUNT',
        'SCORE_p',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major,
    )


# 中高质量问题质量分
def stats_middle_problem_score_ratio(
        middle_problem_score,
        work_load,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=None,
        choose_dpid_data=None,
        customizecontent=None,
        fraction=None,
        major_ratio=None,
        is_calc_score_base_major=True
):
    """
     中高质量问题数/基本工作量
    :param middle_problem_score: 中高质量问题质量分
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        middle_problem_score,
        work_load,
        2,
        5,
        17,
        months_ago,
        'COUNT',
        'SCORE_q',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major
    )


# 高质量问题数占比
def stats_high_problem_count_ratio(
        high_problem_count,
        work_load,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=None,
        choose_dpid_data=None,
        customizecontent=None,
        fraction=None,
        major_ratio=None,
        is_calc_score_base_major=True
):
    """
     中高质量问题数/基本工作量
    :param high_problem_count: 高质量问题数
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        high_problem_count,
        work_load,
        2,
        5,
        18,
        months_ago,
        'COUNT',
        'SCORE_r',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major
    )


# 高质量问题质量分占比
def stats_high_problem_score_ratio(
        high_problem_score,
        work_load,
        months_ago,
        risk_type,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=None,
        choose_dpid_data=None,
        customizecontent=None,
        fraction=None,
        major_ratio=None,
        is_calc_score_base_major=True
):
    """
     中高质量问题数/基本工作量
    :param high_problem_score: 高质量问题数
    :param work_load: 工作量
    :param months_ago: 几月之前
    :param risk_type: 风险类型
    :param calc_score_by_formula: 得分计算公式
    :param zhanduan_filter_list: 不参与基数计算的站段id列表
    :param choose_dpid_data: 部门选择
    :param customizecontent: 自定义文本描述
    :param fraction: 是否专业基数版
    :param major_ratio: 手动专业基数
    :param is_calc_score_base_major: 是否与基数比较
    :return:
    """
    return calc_child_index_type_divide_major(
        high_problem_score,
        work_load,
        2,
        5,
        19,
        months_ago,
        'COUNT',
        'SCORE_s',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        is_calc_score_base_major=is_calc_score_base_major
    )


if __name__ == '__main__':
    pass
