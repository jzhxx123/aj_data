#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
import pandas as pd
from app.data.major_risk_index.common.const import MainType, AssessIntensityDetailType
from app.data.major_risk_index.util import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_extra_child_score_groupby_major, format_export_basic_data,
    calc_extra_child_score_groupby_major_third,
    summizet_operation_set, write_export_basic_data_to_mongo, calc_child_index_type_divide_two,
    calc_child_index_type_divide_major, write_cardinal_number_basic_data)

HIERARCHY = [3]


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100 if detail_type != 3 else 100 * (1 - _ratio)
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100 if detail_type == 3 else 100 * (1 + _ratio)
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_check_problem_val_person(series, work_load, hierarchy, idx,
                                   choose_dpid_data, title=None, fraction=None,
                                   major_ratio=None,
                                   calc_score_by_formula=None,
                                   ):
    if not calc_score_by_formula:
        calc_score_by_formula = _calc_score_by_formula
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.assess_intensity,
                                         AssessIntensityDetailType.stats_check_problem_assess_ratio,
                                         fraction.months_ago,
                                         columns=['prob', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction, major_ratio=major_ratio)
    # 中间计算数据
    if not title:
        title = ['作业项调车问题考核数({0})/工作量({1})', '管理项调车问题考核数({0})/工作量({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_zuoye_check_problem(zuoye_problem, work_load, choose_dpid_data,
                              hierarchy, idx, title=None, fraction=None,
                              major_ratio=None, calc_score_by_formula=None):
    """作业项考核问题数"""
    zuoye_problem = zuoye_problem.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(zuoye_problem, work_load, hierarchy,
                                          idx, choose_dpid_data, title=title,
                                          fraction=fraction, major_ratio=major_ratio,
                                          calc_score_by_formula=calc_score_by_formula)


def _calc_guanli_check_problem(guanli_problem, work_load, choose_dpid_data,
                               hierarchy, idx, title=None, fraction=None,
                               major_ratio=None, calc_score_by_formula=None):
    """管理项考核问题数"""
    guanli_problem = guanli_problem.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(guanli_problem, work_load, hierarchy,
                                          idx, choose_dpid_data, title=title,
                                          fraction=fraction, major_ratio=major_ratio,
                                          calc_score_by_formula=calc_score_by_formula)


# 换算人均考核问题数
def stats_check_problem_assess_radio_type_one(assess_problem,
                                              staff_number,
                                              months_ago,
                                              risk_type,
                                              calc_score_by_formula=_calc_score_by_formula,
                                              choose_dpid_data=None,
                                              customizecontent=None):
    return calc_child_index_type_divide(
        assess_problem,
        staff_number,
        2,
        3,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 换算人均考核问题数
def stats_check_problem_assess_radio_type_one_major(assess_problem,
                                                    staff_number,
                                                    months_ago,
                                                    risk_type,
                                                    calc_score_by_formula=_calc_score_by_formula,
                                                    choose_dpid_data=None,
                                                    customizecontent=None,
                                                    zhanduan_filter_list=[],
                                                    fraction=None,
                                                    major_ratio=None):
    return calc_child_index_type_divide_major(
        assess_problem,
        staff_number,
        2,
        3,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction,
        major_ratio=major_ratio)


# 换算人均考核问题数--部分专业基数
def stats_check_problem_assess_radio_type_one_fliter(assess_problem,
                                                     staff_number,
                                                     months_ago,
                                                     risk_type,
                                                     calc_score_by_formula=_calc_score_by_formula,
                                                     choose_dpid_data=None,
                                                     zhanduan_filter_list=[],
                                                     customizecontent=None):
    return calc_child_index_type_divide_two(
        assess_problem,
        staff_number,
        2,
        3,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 换算人均考核问题数
def stats_check_problem_assess_radio_type_two(zuoye_problem,
                                              guanli_problem,
                                              work_load,
                                              months_ago,
                                              risk_type,
                                              choose_dpid_data,
                                              child_weight=[0.8, 0.2]):
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [zuoye_problem, guanli_problem]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_zuoye_check_problem, _calc_guanli_check_problem]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 3, 1, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 3, 1, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            3,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 换算人均考核问题数(干部，作业人员分开计算)
def stats_check_problem_assess_radio_type_two_type_chewu(
        zuoye_problem,
        guanli_problem,
        zuoye_person_count,
        guanli_person_count,
        months_ago,
        risk_type,
        choose_dpid_data,
        child_weight=[0.8, 0.2],
        title=None,
        fraction_list=[None, None],
        calc_score_by_formula=None,
        major_ratio=None):
    if not title:
        title = ['作业项调车问题考核数({0})/工作量({1})', '管理项调车问题考核数({0})/工作量({1})']
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    work_load = [zuoye_person_count, guanli_person_count]
    df_list = [zuoye_problem, guanli_problem]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_zuoye_check_problem, _calc_guanli_check_problem]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load[idx],
                                             choose_dpid_data, hierarchy, idx,
                                             title=title, fraction=fraction_list[idx],
                                             calc_score_by_formula=calc_score_by_formula,
                                             major_ratio=major_ratio)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 3, 1, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 3, 1, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            3,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 换算人均考核金额
def stats_assess_money_per_person_type_one(assess_money,
                                           work_load,
                                           months_ago,
                                           risk_type,
                                           calc_score_by_formula=_calc_score_by_formula,
                                           choose_dpid_data=None,
                                           customizecontent=None):
    return calc_child_index_type_divide(
        assess_money,
        work_load,
        2,
        3,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 换算人均考核金额
def stats_assess_money_per_person_type_one_major(assess_money,
                                                 work_load,
                                                 months_ago,
                                                 risk_type,
                                                 calc_score_by_formula=_calc_score_by_formula,
                                                 choose_dpid_data=None,
                                                 customizecontent=None,
                                                 zhanduan_filter_list=[],
                                                 fraction=None,
                                                 major_ratio=None):
    return calc_child_index_type_divide_major(
        assess_money,
        work_load,
        2,
        3,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction,
        major_ratio=major_ratio)


# 换算人均考核金额--部分专业基数
def stats_assess_money_per_person_type_one_filter(assess_money,
                                                  work_load,
                                                  months_ago,
                                                  risk_type,
                                                  calc_score_by_formula=_calc_score_by_formula,
                                                  choose_dpid_data=None,
                                                  zhanduan_filter_list=[],
                                                  customizecontent=None):
    return calc_child_index_type_divide_two(
        assess_money,
        work_load,
        2,
        3,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


def _calc_assess_money_per_person(series_numerator, series_denominator,
                                  hierarchy, idx, choose_dpid_data, title=None,
                                  fraction=None, zhanduan_filter_list=None):
    if series_denominator.empty or series_numerator.empty:
        return False
    data = pd.concat(
        [
            series_numerator.to_frame(name='left'),
            series_denominator.to_frame(name='right')
        ],
        axis=1,
        sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['left'] / data['right']
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.assess_intensity,
                                         AssessIntensityDetailType.stats_assess_money_per_person, fraction.months_ago,
                                         columns=['left', 'right'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula,
        numerator='left', denominator='right', fraction=fraction, zhanduan_filter_list=zhanduan_filter_list)
    # 中间计算数据
    if not title:
        title = ['干部月度考核总金额({0})/干部总人数({1})', '职工月度考核总金额({0})/职工总人数(非干部)({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["left"], 2)}', row['right']),
        axis=1)
    data.drop(columns=['left', 'ratio', 'right'], inplace=True, axis=1)
    return (rst_data, data)


def _calc_worker_assess_money(worker_count, work_money, choose_dpid_data,
                              hierarchy, idx, title=None, fraction=None, zhanduan_filter_list=None):
    """职工月度考核总金额/职工总人数"""
    worker_count = worker_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    assess_money = work_money.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_assess_money_per_person(assess_money, worker_count, hierarchy,
                                         idx, choose_dpid_data, title=title,
                                         fraction=fraction, zhanduan_filter_list=zhanduan_filter_list)


def _calc_cadre_assess_money(cadre_count, cadre_money, choose_dpid_data,
                             hierarchy, idx, title=None, fraction=None, zhanduan_filter_list=None):
    """干部月度考核总金额/干部总人数"""
    cadre_count = cadre_count.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    assess_money = cadre_money.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_assess_money_per_person(assess_money, cadre_count, hierarchy,
                                         idx, choose_dpid_data, title=title,
                                         fraction=fraction, zhanduan_filter_list=zhanduan_filter_list)


# 换算单位考核金额
def stats_assess_money_per_person_type_two(
        df_cadre_count, df_cadre_money, df_worker_count, df_worker_money,
        months_ago, risk_type, choose_dpid_data,
        title=None, fraction_list=[None, None], child_weight=None, zhanduan_filter_list=[]):
    if not child_weight:
        child_weight = [0.2, 0.8]
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [
        df_cadre_count, df_cadre_money, df_worker_count, df_worker_money
    ]
    if not title:
        title = ''
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_cadre_assess_money, _calc_worker_assess_money]
        for idx, ifunc in enumerate(child_func):
            rst_func = ifunc(df_list[idx * 2], df_list[idx * 2 + 1],
                             choose_dpid_data, hierarchy, idx,
                             title=title, fraction=fraction_list[idx],
                             zhanduan_filter_list=zhanduan_filter_list)
            if rst_func is False:
                continue
            rst_data, rst_basic_data = rst_func
            calc_basic_data.append(rst_basic_data)
            if rst_data is not None:
                score.append(rst_data * child_weight[idx])
        if score == []:
            continue
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 3, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 3, 2, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_b_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            3,
            2,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 返奖率
def stats_award_return_ratio(award_return,
                             assess_money,
                             months_ago,
                             risk_type,
                             calc_score_by_formula=_calc_score_by_formula,
                             choose_dpid_data=None,
                             customizecontent=None,
                             zhanduan_filter_list=None,
                             fraction=None):
    return calc_child_index_type_divide_major(
        award_return,
        assess_money,
        2,
        3,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction)


if __name__ == '__main__':
    pass
