#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   control_intensity
Description:
Author:    
date:         2019/12/18
-------------------------------------------------
Change Activity:2019/12/18 11:16 上午
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import (
    get_months_from_201712, pd_query, get_months_from_201712_two)
from app.data.major_risk_index.util import (
    format_export_basic_data, write_export_basic_data_to_mongo,
    df_merge_with_dpid, calc_child_index_type_sum, append_major_column_to_df, summizet_operation_set)


def stats_calc_check_address_problem_exposure(
        risk_ids,
        check_problem_and_address_sql,
        check_address_data,
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title=None, address_type=1,
        customizededuct=None, months=None,
        problem_filter=None,
        type_dict=None
):
    """

    Args:
        risk_ids:
        check_problem_and_address_sql:
        check_address_data:
        department_data:
        months_ago:
        risk_type:
        choose_dpid_data:
        title:
        address_type: 1--》表示检查部门，2--》重要检查地点
        customizededuct:
        months:
        type_dict:
        problem_filter: 过滤条件，指定某个列，包含某个字段，之后优化成，一个lambda函数。
        problem_filter=[{
            "content": ['A', 'B'],
            "column": 'LEVEL',
            'func': _filter_data
        }]
    Returns:
    result: {
        dpid:{-->站段级别
        CONTENT:,
        SCORE:,
        }
    }
    """
    address_column = {
        1: "CHECK_DEPARTMENT_ID",
        2: "FK_CHECK_POINT_ID"
    }
    if not customizededuct:
        customizededuct = {
            1: 0.2,
            2: 0.4,
            3: 0.8
        }
    if not type_dict:
        type_dict = {
            1: '班组',
            2: '检查地点'
        }
    name_type = type_dict.get(address_type)
    result = dict()

    check_address_ids = check_address_data[address_column[address_type]].values.tolist()
    # 获取过去months个月的日期,严格遵循从当前月到目标月份的排列
    # （201903，201902，201901，201812）例如今年3月到去年12月
    stats_month_list = get_months_from_201712(months_ago, months)
    # 获取过去月份检查信息跟地点
    check_problem_and_address_data_list = []
    for stats_month in stats_month_list:
        df = pd.merge(
            pd_query(check_problem_and_address_sql.format(*stats_month, risk_ids)),
            department_data[['DEPARTMENT_ID']],
            left_on='TYPE3',
            right_on='DEPARTMENT_ID',
            how='inner'
        )
        if df.empty:
            continue
        check_problem_and_address_data_list.append(df)
    if not check_problem_and_address_data_list:
        for dpid in choose_dpid_data(3).DEPARTMENT_ID:
            result[dpid] = {
                "CONTENT": '<p>{0}: </p> \n无{1}'.format(
                    title, name_type), 'SCORE': 0}
        return result
    # 判断有无地点
    # if not check_problem_and_address_data_list:

    # check_problem_and_address_data_list = [
    #     pd.merge(
    #         pd_query(check_problem_and_address_sql.format(*stats_month, risk_ids)),
    #         department_data[['DEPARTMENT_ID']],
    #         left_on='TYPE3',
    #         right_on='DEPARTMENT_ID',
    #         how='inner'
    #     )
    #     for stats_month in stats_month_list]
    # 抽取过去月份每月检查的地点
    check_problem_and_address = [
        data[
            (data[address_column[address_type]].isin(check_address_ids)) &
            (data['TYPE'] == address_type)
            ]
        for data in check_problem_and_address_data_list
    ]
    check_problem_and_address = [
        data.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID', 'ID_CARD'])
        for data in check_problem_and_address]
    if problem_filter:
        for i in problem_filter:
            check_problem_and_address = [
                data[data.apply(
                    lambda row: i['func'](i['content'], i['column'], row), axis=1)]
                for data in check_problem_and_address]
    no_check_address_id_set_list = [
        set(check_address_ids)
        - set(data[address_column[address_type]].values.tolist())
        for data in check_problem_and_address]
    address_ids = set(check_address_ids)
    rst = calc_lasting_mon_data(no_check_address_id_set_list, address_ids)
    for i in set(choose_dpid_data(3)['DEPARTMENT_ID'].values.tolist()):
        result[i] = dict()
        result[i]['CONTENT'] = f'<p>{title}: </p> \n'
        result[i]['SCORE'] = 0
    for key, value in rst.items():
        dp_id = check_address_data[
            check_address_data[address_column[address_type]] == key
            ]['TYPE3'].values[0]
        check_point_name = check_address_data[
            check_address_data[address_column[address_type]] == key
            ]['ALL_NAME'].values[0]
        cnt = "<p>{0}:{1}, {2}未检查出问题 \n"
        if dp_id in list(result.keys()):
            mon_name = f'连续{value}个月' if value > 1 else '全月'
            result[dp_id]['CONTENT'] += cnt.format(
                name_type, check_point_name, mon_name)
            # result[dp_id]['CONTENT'] += f"<p>{'检查点' if address_type == 2 else '班组'}:{check_point_name}, " \
            #                             f"{'连续' + str(value) + '个月' if value > 1 else '全月'}" + \
            #                             f"未检查出问题 \n"
            if value > max(list(customizededuct.keys())):
                score = customizededuct[max(list(customizededuct.keys()))]
            elif value < min(list(customizededuct.keys())):
                score = 0
            else:
                score = customizededuct[value]
            result[dp_id]['SCORE'] += score
    return result


def calc_lasting_mon_data(data, initial_item):
    """
    统计连续月数据
    Args:
        data: 列表 -- 连续月份(从现在月到目标月的排列顺序)数据[{2}, {2,3},{3,4}]
        initial_item:

    Returns:
    dict -- {2:2,3:2,4:1} -- 2号事件连续两个月，3号事件连续两个月....
    """
    # 统计连续计数
    rst = dict()
    dp_ids = set()
    flag_dict = dict()
    for i in initial_item:
        flag_dict[i] = 0
    tmp_dict = dict()
    for item in data:
        # 连续两个月为检查
        s = dp_ids & item
        # 上月检查了， 这个月未检查
        u = item - s
        # 上个月未检查，这个月检查了
        r = dp_ids - s
        for i in s:
            flag_dict[i] += 1
            if rst.get(i, None):
                if rst[i] >= flag_dict[i]:
                    # 不更新值
                    pass
                else:
                    rst[i] = flag_dict[i]
            else:
                rst[i] = flag_dict[i]
        for i in r:
            if rst.get(i, None):
                if rst[i] >= flag_dict[i]:
                    # 不更新值
                    pass
                else:
                    rst[i] = flag_dict[i]
            else:
                rst[i] = flag_dict[i]
        for i in u:
            flag_dict[i] = 1
        dp_ids = item
        for i in set(flag_dict.keys()) - set(rst.keys()):
            if i:
                rst[i] = flag_dict[i]
    return rst


def _filter_data(content_list, colunm, row, reseve=False):
    # reseve 表示反选
    # 缺失值 直接返回
    flag = not reseve
    if not pd.notnull(row[colunm]):
        return not flag
    for item in content_list:
        item = item.strip()
        if item in row[colunm]:
            return flag
        else:
            continue
    return not flag


def stats_control_quality(
        risk_ids,
        check_problem_and_address_sqllist,
        check_address_data_list,
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title_list=[None, None], address_type=1,
        customizededuct=None, months=None):
    """
    卡控质量（机务_调车安全风险_V1_20191216）
    问题卡控：
        A.‘调车点’、‘出入库点’
            全月没有发现调车风险问题，一处扣0.2分。连续两个月没有发现调车风险问题，一处扣0.4分。
            连续三个月没有发现调车风险问题，一处扣0.8分。
        B.班组（父节点车间属性为运用车间-运用车间或动车运用车间）
            全月没有发现调车风险问题，一个班组扣0.1分。
            连续两个月没有发现调车风险问题，一个班组扣0.2分。
            连续三个月没有发现调车风险问题，一个班组扣0.4分。
    AB类问题卡控：
        A.“调车点”、“出入库点”
            连续四个月没有发现A或B类调车风险问题，一处扣0.5分。
            连续五个月没有发现A或B类调车风险问题，一处扣1分。
            连续六个月没有发现A或B类调车风险问题，一处扣1.5分。
        B.班组（父节点车间属性为运用车间-运用车间或动车运用车间）
            连续四个月没有发现A或B类调车风险问题，一个班组扣0.3分。
            连续五个月没有发现A或B类调车风险问题，一个班组扣0.6分。
            连续六个月没有发现A或B类调车风险问题，一个班组扣0.9分
    Args:
        risk_ids:
        check_problem_and_address_sqllist:
        check_address_data_list:
        department_data:
        months_ago:
        risk_type:
        choose_dpid_data:
        title_list:
        address_type:
        customizededuct:
        months:

    Returns:

    """
    # 问题卡控
    # 重要检查点
    check_point_rst = stats_calc_check_address_problem_exposure(
        risk_ids,
        check_problem_and_address_sqllist[0],
        check_address_data_list[0],
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title=title_list[0], address_type=2,
        customizededuct=None, months=None)

    banzu_rst = stats_calc_check_address_problem_exposure(
        risk_ids,
        check_problem_and_address_sqllist[1],
        check_address_data_list[1],
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title=title_list[0], address_type=1,
        customizededuct={
            1: 0.1,
            2: 0.2,
            3: 0.4,
        }, months=None)

    level_check_point_rst = stats_calc_check_address_problem_exposure(
        risk_ids,
        check_problem_and_address_sqllist[0],
        check_address_data_list[0],
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title=title_list[1], address_type=2,
        customizededuct={
            4: 0.5,
            5: 1,
            6: 1.5,
        }, months=None,
        problem_filter=[{
            "content": ['A', 'B'],
            "column": 'LEVEL',
            'func': _filter_data
        }]
    )

    level_banzu_rst = stats_calc_check_address_problem_exposure(
        risk_ids,
        check_problem_and_address_sqllist[1],
        check_address_data_list[1],
        department_data,
        months_ago, risk_type, choose_dpid_data,
        title=title_list[1], address_type=1,
        customizededuct={
            4: 0.3,
            5: 0.6,
            6: 0.9,
        }, months=None,
        problem_filter=[{
            "content": ['A', 'B'],
            "column": 'LEVEL',
            'func': _filter_data
        }]
    )

    # 合并上述字典中的分数，描述内容
    rst_data = []
    for dpid in check_point_rst:
        rst_data.append(
            {
                'FK_DEPARTMENT_ID': dpid,
                'CONTENT':
                    check_point_rst[dpid]['CONTENT'] + '\n' +
                    banzu_rst[dpid]['CONTENT'] + '\n' +
                    level_check_point_rst[dpid]['CONTENT'] + '\n' +
                    level_banzu_rst[dpid]['CONTENT'],
                'SCORE':
                    check_point_rst[dpid]['SCORE'] +
                    banzu_rst[dpid]['SCORE'] +
                    level_check_point_rst[dpid]['SCORE'] +
                    level_banzu_rst[dpid]['SCORE'],
            }
        )
    rst_data = pd.DataFrame(rst_data)
    rst_data = pd.merge(
        rst_data,
        choose_dpid_data(3),
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    data_rst = format_export_basic_data(
        rst_data[['MAJOR', 'DEPARTMENT_ID', 'CONTENT']],
        14,
        1,
        3,
        months_ago,
        risk_type=risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     14, 1, risk_type)

    rst_data = df_merge_with_dpid(
        rst_data[['FK_DEPARTMENT_ID', 'SCORE']], department_data)
    rst_child_score = calc_child_index_type_sum(
        rst_data,
        2,
        14,
        1,
        months_ago,
        'SCORE',
        'SCORE_a',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def stats_control_quality_diff(
        risk_ids,
        department_data,
        months_ago, risk_type, choose_dpid_data,
        sql_dict=None,
        data_dict=None,
        title=None, address_type=None,
        customizededuct=None, months=None, problem_filter=None,
        func_list=None,
        type_dict=None
):
    """
    卡控质量（机务_调车安全风险_V1_20191216）
    问题卡控：
    “检修库”、“整备场”:
        A:全月没有发现问题，一处扣0.5分
        C:连续两个月没有发现问题，一处扣1.0分
        E:连续三个月没有发现问题，一处扣2.0分
    “检修库”、“整备场”:
        G:连续四个月没有发现A或B类问题，一处扣3.0分
        I:连续五个月没有发现A或B类问题，一处扣4.0分
        K:连续六个月没有发现A或B类问题，一处扣5.0分
    检修车间、解体组装车间、整备车间:
        B:全月没有发现问题，一个车间扣5分
        D:连续两个月没有发现问题，一个车间扣10分
        F:连续三个月没有发现问题，一个车间扣15分
        H:连续四个月没有发现问题，一个车间扣20分
        J:连续五个月没有发现问题，一个车间扣25分
        L:连续六个月没有发现问题，一个车间扣30分
    Args:
        risk_ids:
        sql_dict:
        data_dict:
        department_data:
        months_ago:
        risk_type:
        choose_dpid_data:
        title:
        address_type:
        customizededuct:
        months:
        problem_filter:
        func_list:
        type_dict:
    Returns:

    """
    if not address_type:
        address_type = {
            'check_point_rst': 2,
            'banzu_rst': 1,
            'level_check_point_rst': 2,
            'level_banzu_rst': 1
        }
    if not customizededuct:
        customizededuct = {
            "check_point_rst": {},
            "banzu_rst": {1: 0.1, 2: 0.2, 3: 0.4},
            "level_check_point_rst": {4: 0.5, 5: 1, 6: 1.5},
            "level_banzu_rst": {4: 0.3, 5: 0.6, 6: 0.9, },
        }
    if not months:
        months = {
            'check_point_rst': None,
            'banzu_rst': None,
            'level_check_point_rst': None,
            'level_banzu_rst': None
        }
    if not problem_filter:
        problem_filter = {
            "check_point_rst": None,
            "banzu_rst": None,
            "level_check_point_rst": [{"content": ['A', 'B'],
                                       "column": 'LEVEL',
                                       'func': _filter_data}],
            "level_banzu_rst": [{"content": ['A', 'B'],
                                 "column": 'LEVEL',
                                 'func': _filter_data}]
        }

    if not func_list:
        func_list = ['check_point_rst', 'banzu_rst',
                     'level_check_point_rst', 'level_banzu_rst']
    if not title:
        title = {
            'check_point_rst': "问题卡控",
            'banzu_rst': '问题卡控',
            'level_check_point_rst': 'AB类问题卡控',
            'level_banzu_rst': 'AB类问题卡控'
        }
    chlid_data_list = []
    for key in func_list:
        chlid_data = stats_calc_check_address_problem_exposure(
            risk_ids,
            sql_dict.get(key),
            data_dict.get(key),
            department_data,
            months_ago, risk_type, choose_dpid_data,
            title.get(key),
            address_type.get(key),
            customizededuct.get(key),
            months.get(key),
            problem_filter.get(key),
            type_dict=type_dict
        )
        chlid_data_list.append(chlid_data)
    # 问题卡控

    # 合并上述字典中的分数，描述内容
    rst_data = []
    for dpid in choose_dpid_data(3).DEPARTMENT_ID:
        rst_data.append(
            {
                'FK_DEPARTMENT_ID': dpid,
                'CONTENT': '<br/><br/>'.join(
                    [df[dpid]['CONTENT'] for df in chlid_data_list]),
                'SCORE': sum(
                    [df[dpid]['SCORE'] for df in chlid_data_list]),
            }
        )
    rst_data = pd.DataFrame(rst_data)
    rst_data = pd.merge(
        rst_data,
        choose_dpid_data(3),
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    data_rst = format_export_basic_data(
        rst_data[['MAJOR', 'DEPARTMENT_ID', 'CONTENT']],
        14,
        1,
        3,
        months_ago,
        risk_type=risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     14, 1, risk_type)

    rst_data = df_merge_with_dpid(
        rst_data[['FK_DEPARTMENT_ID', 'SCORE']], department_data)
    rst_child_score = calc_child_index_type_sum(
        rst_data,
        2,
        14,
        1,
        months_ago,
        'SCORE',
        'SCORE_a',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score
