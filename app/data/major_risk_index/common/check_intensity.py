# -*- coding: utf-8 -*-

import pandas as pd

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common.check_intensity_sql import (
    MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL,
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL
)
from app.data.major_risk_index.common.const import MainType, CheckIntensityDetailType
from app.data.major_risk_index.util import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_extra_child_score_groupby_major, df_merge_with_dpid,
    format_export_basic_data, summizet_operation_set,
    write_export_basic_data_to_mongo, calc_child_index_type_divide_two, calc_extra_child_score_groupby_major_two,
    calc_child_index_type_divide_major, calc_extra_child_score_groupby_major_third,
    write_cardinal_number_basic_data, calc_extra_child_basic_data)
from app.data.util import pd_query

HIERARCHY = [3]


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_media_banzu(row, column, major_column, detail_type=None):
    """
    监控调阅班组数，按100%计算
    :param row:
    :param column:
    :param major_column:
    :param detail_type:
    :return:
    """
    _score = min(100, 100 * row[column])
    return round(_score, 2)


# 换算单位检查频次
def stats_check_per_person(check_count,
                           work_load,
                           months_ago,
                           risk_type,
                           calc_score_by_formula=_calc_score_by_formula,
                           choose_dpid_data=None,
                           customizecontent=None):
    return calc_child_index_type_divide(
        check_count,
        work_load,
        2,
        1,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 换算单位检查频次
def stats_check_per_person_major(check_count,
                                 work_load,
                                 months_ago,
                                 risk_type,
                                 calc_score_by_formula=_calc_score_by_formula,
                                 zhanduan_filter_list=[],
                                 choose_dpid_data=None,
                                 customizecontent=None,
                                 fraction=None,
                                 major_ratio=None,
                                 ratio_df=None):
    return calc_child_index_type_divide_major(
        check_count,
        work_load,
        2,
        1,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction,
        major_ratio=major_ratio,
        ratio_df=ratio_df)


# 换算单位检查频次 -- 部分专业基数
def stats_check_per_person_fliter(check_count,
                                  work_load,
                                  months_ago,
                                  risk_type,
                                  calc_score_by_formula=_calc_score_by_formula,
                                  choose_dpid_data=None,
                                  zhanduan_filter_list=[],
                                  customizecontent=None):
    return calc_child_index_type_divide_two(
        check_count,
        work_load,
        2,
        1,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


def _calc_check_count_val_person(df_data, choose_dpid_data, work_load,
                                 hierarchy, idx, title, fraction=None,
                                 calc_func=None, zhanduan_filter_list=None, major_ratio=None):
    """
    获取换算单位检查频次的子指数基础数据
    """
    if not calc_func:
        calc_func = _calc_score_by_formula
    series = df_data.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data.apply(
        lambda row: row['prob'] / row['PERSON_NUMBER'] if row['PERSON_NUMBER'] else 0, axis=1)
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_check_problem_ratio, fraction.months_ago,
                                         columns=['prob', 'PERSON_NUMBER'])
    # 计算结果
    rst_data, data = calc_extra_child_basic_data(
        data, choose_dpid_data(hierarchy), 'ratio', calc_func,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list, major_ratio=major_ratio)

    # 中间计算数据
    if not title:
        title = [
            '<p>现场检查得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            '现场检查({3:.4f}) = 现场检查次数(人次)({4:.1f})/ 工作量({5:.1f})*100%</p>',
            '<p>调阅检查得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            '调阅检查({3:.4f}) = 调阅检查次数(人次)({4:.1f})/ 工作量({5:.1f})*100%</p>',
        ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            row['SCORE'], row['group_sort'], row['AVG_NUMBER'], row['ratio'],
            row["prob"], row['PERSON_NUMBER'], row['AVG_SCORE']),
        axis=1)
    data = data.set_index('DEPARTMENT_ID')[[f'middle_{idx}']]
    return rst_data, data


# 换算单位检查频次
def stats_check_per_person_by_two_type(
        site_check_count,
        monitor_check_count,
        work_load,
        months_ago,
        risk_type,
        title=None,
        calc_score_by_formula=_calc_score_by_formula,
        child_weight=None,
        choose_dpid_data=None,
        zhanduan_filter_list=None,
        fraction_list=None):
    """
    现场检查占比70%：现场检查（包含添乘检查和现场检查）调乘一体化作业检查次数/工作量*70%
    调阅检查占比30%：调阅检查（检查方式为监控调阅检查）调乘一体化检查次数/工作量*30%
    :param site_check_count: 现场检查
    :param monitor_check_count: 调阅检查
    :param work_load: 工作量
    :param months_ago: 当前月与生成月份差值
    :param risk_type: 风险类型
    :param title: 文本描述列表
    :param calc_score_by_formula: 得分计算公式
    :param child_weight: 子指数权重
    :param choose_dpid_data: 部门选择
    :param zhanduan_filter_list: 不参与基数计算站段id列表
    :param fraction_list: 是否选择计算基数
    :return:
    """
    if child_weight is None:
        child_weight = [0.7, 0.3]
    if fraction_list is None:
        fraction_list = [None, None]
    if zhanduan_filter_list is None:
        zhanduan_filter_list = []

    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [site_check_count, monitor_check_count]
    for hierarchy in HIERARCHY:
        score = []
        for idx, df in enumerate(df_list):
            # 计算子指数
            rst_func, rst_basic_data = _calc_check_count_val_person(
                df, choose_dpid_data,
                work_load, hierarchy, idx,
                calc_func=calc_score_by_formula,
                fraction=fraction_list[idx], title=title)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 2, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_b_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            2,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_check_problem_val_person(series, choose_dpid_data, work_load,
                                   hierarchy, idx, title, fraction=None,
                                   calc_func=None, zhanduan_filter_list=None, major_ratio=None):
    if not calc_func:
        calc_func = _calc_score_by_formula
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_check_problem_ratio, fraction.months_ago,
                                         columns=['prob', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_func,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list, major_ratio=major_ratio)
    # 中间计算数据
    if not title:
        title = ['作业项问题数({0})/工作量({1})',
                 '管理项问题数({0})/工作量({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_check_problem_basic_data(df_data, choose_dpid_data, work_load,
                                   hierarchy, idx, title, fraction=None,
                                   calc_func=None, zhanduan_filter_list=None, major_ratio=None):
    """
    获取查出问题率子指数中间过程
    :param df_data: 子指数数据
    :param choose_dpid_data: 部门选择
    :param work_load: 工作量
    :param hierarchy: 层级
    :param idx: 第几个子指数
    :param title: 文本描述
    :param fraction: 是否选择基数
    :param calc_func: 得分计算
    :param zhanduan_filter_list: 不参与计算的站段id列表
    :param major_ratio: 手动专业基数
    :return:
    rst_data--得分
    data--中间数据
    """
    series = df_data.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    if not calc_func:
        calc_func = _calc_score_by_formula
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data.apply(
        lambda row: row['prob'] / row['PERSON_NUMBER'] if row['PERSON_NUMBER'] > 0 else 0, axis=1
    )
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_check_problem_ratio, fraction.months_ago,
                                         columns=['prob', 'PERSON_NUMBER'])
    # 计算结果
    rst_data, data = calc_extra_child_basic_data(
        data, choose_dpid_data(hierarchy), 'ratio', calc_func,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list, major_ratio=major_ratio)
    # 中间计算数据
    if not title:
        title = [
            '<p>作业项得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            ' 作业项问题({3:.4f}) = 作业项问题数({4:.1f})/ 工作量({5:.1f})*100%</p>',
            '<p>管理项得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            '管理项问题({3:.4f}) = 管理项问题数({4:.1f})/ 工作量({5:.1f})*100%</p>',
        ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            row['SCORE'], row['group_sort'], row['AVG_NUMBER'], row['ratio'],
            row["prob"], row['PERSON_NUMBER'], row['AVG_SCORE']),
        axis=1)
    data = data.set_index('DEPARTMENT_ID')[[f'middle_{idx}']]
    return rst_data, data


def _calc_zuoye_check_problem_ratio(df_data, choose_dpid_data, work_load,
                                    hierarchy, idx, title=None, fraction=None,
                                    calc_func=None, zhanduan_filter_list=None, major_ratio=None):
    """作业项问题查处率"""
    zuoye_problem = df_data.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(zuoye_problem, choose_dpid_data,
                                          work_load, hierarchy, idx, title=title,
                                          fraction=fraction, major_ratio=major_ratio,
                                          calc_func=calc_func, zhanduan_filter_list=zhanduan_filter_list)


def _calc_guanli_check_problem_ratio(df_data, choose_dpid_data, work_load,
                                     hierarchy, idx, title=None, fraction=None,
                                     calc_func=None, zhanduan_filter_list=None,
                                     major_ratio=None):
    """管理项问题查处率"""
    guanli_problem = df_data.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(guanli_problem, choose_dpid_data,
                                          work_load, hierarchy, idx, title=title,
                                          fraction=fraction, calc_func=calc_func,
                                          zhanduan_filter_list=zhanduan_filter_list,
                                          major_ratio=major_ratio)


# 查处问题率
def stats_check_problem_ratio_type_two(first_df,
                                       second_df,
                                       work_load,
                                       months_ago,
                                       risk_type,
                                       choose_dpid_data=None,
                                       child_weight=[0.8, 0.2],
                                       customizecontent=None,
                                       calc_score_by_formula=None,
                                       fraction_list=[None, None]):
    """(作业项问题数*80% + 管理项问题数*20%)/工作量
    """
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [first_df, second_df]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_zuoye_check_problem_ratio, _calc_guanli_check_problem_ratio
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], choose_dpid_data,
                                             work_load, hierarchy, idx,
                                             calc_func=calc_score_by_formula,
                                             fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 3, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 3, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_c_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            3,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 查处问题率
def stats_check_problem_ratio_type_two_type_chewu(first_df,
                                                  second_df,
                                                  first_load,
                                                  second_load,
                                                  months_ago,
                                                  risk_type,
                                                  choose_dpid_data=None,
                                                  child_weight=[0.8, 0.2],
                                                  title=['作业项问题数({0})/工作量({1})',
                                                         '管理项问题数({0})/工作量({1})'],
                                                  calc_score_by_formula=None,
                                                  fraction_list=[None, None],
                                                  zhanduan_filter_list=None,
                                                  major_ratio=None):
    """(作业项问题数*80% + 管理项问题数*20%)/工作量
    """
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [first_df, second_df]
    work_load = [first_load, second_load]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_zuoye_check_problem_ratio, _calc_guanli_check_problem_ratio
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], choose_dpid_data,
                                             work_load[idx], hierarchy, idx, title, fraction=fraction_list[idx],
                                             calc_func=calc_score_by_formula,
                                             zhanduan_filter_list=zhanduan_filter_list, major_ratio=major_ratio)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 3, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 3, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_c_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            3,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 查处问题率--中间过程显示详情
def stats_check_problem_ratio_type_two_type_major(first_df,
                                                  second_df,
                                                  work_load,
                                                  months_ago,
                                                  risk_type,
                                                  choose_dpid_data=None,
                                                  child_weight=None,
                                                  title=None,
                                                  calc_score_by_formula=None,
                                                  fraction_list=None,
                                                  zhanduan_filter_list=None,
                                                  major_ratio=None):
    """(作业项问题数*80% + 管理项问题数*20%)/工作量
    中间过程显示详情
    """
    if not fraction_list:
        fraction_list = [None, None]
    if not child_weight:
        child_weight = [0.8, 0.2]
    if not title:
        title = [
            '<p>作业项得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            ' 作业项问题({3:.4f}) = 作业项问题数({4:.1f})/ 工作量({5:.1f})*100%</p>',
            '<p>管理项得分：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>' +
            '管理项问题({3:.4f}) = 管理项问题数({4:.1f})/ 工作量({5:.1f})*100%</p>',
        ]
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [first_df, second_df]
    for hierarchy in HIERARCHY:
        score = []
        for idx, df in enumerate(df_list):
            # 获取子指数得分,中间数据
            rst_func, rst_basic_data = _calc_check_problem_basic_data(
                df, choose_dpid_data,
                work_load, hierarchy, idx, title, fraction=fraction_list[idx],
                calc_func=calc_score_by_formula,
                zhanduan_filter_list=zhanduan_filter_list, major_ratio=major_ratio)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 3, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 3, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_c_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            3,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 查处问题率
def stats_check_problem_ratio_type_one(problem_count,
                                       work_load,
                                       months_ago,
                                       risk_type,
                                       calc_score_by_formula=_calc_score_by_formula,
                                       choose_dpid_data=None,
                                       customizecontent=None):
    """问题数/工作量
    """
    return calc_child_index_type_divide(
        problem_count,
        work_load,
        2,
        1,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 查处问题率
def stats_check_problem_ratio_type_one_major(problem_count,
                                             work_load,
                                             months_ago,
                                             risk_type,
                                             calc_score_by_formula=_calc_score_by_formula,
                                             choose_dpid_data=None,
                                             customizecontent=None,
                                             zhanduan_filter_list=[],
                                             fraction=None):
    """问题数/工作量
    """
    return calc_child_index_type_divide_major(
        problem_count,
        work_load,
        2,
        1,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction)


# 视频查处问题率
def stats_check_problem_video_ratio_type_one_major(problem_count,
                                                   work_load,
                                                   months_ago,
                                                   risk_type,
                                                   calc_score_by_formula=_calc_score_by_formula,
                                                   choose_dpid_data=None,
                                                   customizecontent=None,
                                                   zhanduan_filter_list=[],
                                                   fraction=None):
    """问题数/工作量
    """
    return calc_child_index_type_divide_major(
        problem_count,
        work_load,
        2,
        1,
        18,
        months_ago,
        'COUNT',
        'SCORE_r',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction)


# 查处问题率 -- 部分专业基数
def stats_check_problem_ratio_type_one_filter(problem_count,
                                              work_load,
                                              months_ago,
                                              risk_type,
                                              calc_score_by_formula=_calc_score_by_formula,
                                              choose_dpid_data=None,
                                              zhanduan_filter_list=[],
                                              customizecontent=None):
    """问题数/工作量
    """
    return calc_child_index_type_divide_two(
        problem_count,
        work_load,
        2,
        1,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 查处问题考核率（类型一）-- 部分专业基数
def stats_check_problem_assess_radio_type_one_filter(assess_problem,
                                                     all_problem,
                                                     months_ago,
                                                     risk_type,
                                                     calc_score_by_formula=_calc_score_by_formula,
                                                     choose_dpid_data=None,
                                                     zhanduan_filter_list=[],
                                                     customizecontent=None):
    return calc_child_index_type_divide_two(
        assess_problem,
        all_problem,
        2,
        1,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 查处问题考核率（类型一）
def stats_check_problem_assess_radio_type_one(assess_problem,
                                              all_problem,
                                              months_ago,
                                              risk_type,
                                              calc_score_by_formula=_calc_score_by_formula,
                                              choose_dpid_data=None,
                                              zhanduan_filter_list=[],
                                              customizecontent=None,
                                              fraction=None):
    return calc_child_index_type_divide_major(
        assess_problem,
        all_problem,
        2,
        1,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction)


def _calc_check_problem_assess_ratio(series_numerator, choose_dpid_data,
                                     series_denominator, hierarchy, idx,
                                     calc_func=None):
    if calc_func is None:
        calc_func = _calc_score_by_formula
    data = pd.concat(
        [
            series_numerator.to_frame(name='left'),
            series_denominator.to_frame(name='right')
        ],
        axis=1,
        sort=False)
    data['ratio'] = data['left'] / data['right']
    if data.empty:
        return None, None
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_func,
        numerator='left', denominator='right')
    # 中间计算数据
    title = ['作业项考核问题数({0})/作业项问题数({1})', '管理项考核问题数({0})/管理项问题数({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["left"], 2)}', row['right']),
        axis=1)
    data.drop(columns=['left', 'ratio', 'right'], inplace=True, axis=1)
    return rst_data, data


def _calc_zuoye_check_problem_assess_ratio(zy_numerator, zy_denominator,
                                           choose_dpid_data, hierarchy, idx,
                                           calc_func=None):
    """作业项问题查处率"""
    zuoye_problem = zy_numerator.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    assess_problem = zy_denominator.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_assess_ratio(zuoye_problem, choose_dpid_data,
                                            assess_problem, hierarchy, idx,
                                            calc_func=calc_func)


def _calc_guanli_check_problem_assess_ratio(gl_numerator, gl_denominator,
                                            choose_dpid_data, hierarchy, idx,
                                            calc_func=None):
    """管理项问题查处率"""
    guanli_problem = gl_numerator.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    assess_problem = gl_denominator.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_assess_ratio(guanli_problem, choose_dpid_data,
                                            assess_problem, hierarchy, idx,
                                            calc_func=calc_func)


# 查处问题考核率
def stats_check_problem_assess_radio_type_two(df_zy_numerator,
                                              df_zy_denominator,
                                              df_gl_numerator,
                                              df_gl_denominator,
                                              months_ago,
                                              risk_type,
                                              choose_dpid_data=None,
                                              calc_score_by_formula=None):
    """(作业项考核问题数/作业项问题数)*80% + (管理项考核问题数/管理项问题数)*20%
    """
    rst_child_score = []
    df_list = [
        df_zy_numerator, df_zy_denominator, df_gl_numerator, df_gl_denominator
    ]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_zuoye_check_problem_assess_ratio,
            _calc_guanli_check_problem_assess_ratio
        ]
        child_weight = [0.8, 0.2]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx * 2],
                                             df_list[idx * 2 + 1],
                                             choose_dpid_data, hierarchy, idx,
                                             calc_func=calc_score_by_formula)
            if rst_func is not None:
                calc_basic_data.append(rst_basic_data)
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 4, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 4, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_d_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            4,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 换算问题质量分
def stats_score_per_person(problem_score,
                           work_load,
                           months_ago,
                           risk_type,
                           calc_score_by_formula=_calc_score_by_formula,
                           choose_dpid_data=None,
                           customizecontent=None):
    """问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    return calc_child_index_type_divide(
        problem_score,
        work_load,
        2,
        1,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 换算问题质量分
def stats_score_per_person_major(problem_score,
                                 work_load,
                                 months_ago,
                                 risk_type,
                                 calc_score_by_formula=_calc_score_by_formula,
                                 choose_dpid_data=None,
                                 customizecontent=None,
                                 zhanduan_filter_list=[],
                                 fraction=None,
                                 major_ratio=None):
    """问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    return calc_child_index_type_divide_major(
        problem_score,
        work_load,
        2,
        1,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction,
        major_ratio=major_ratio)


# 换算问题质量分(质量均分)--部分专业基数
def stats_score_per_person_filter(problem_score,
                                  work_load,
                                  months_ago,
                                  risk_type,
                                  calc_score_by_formula=_calc_score_by_formula,
                                  choose_dpid_data=None,
                                  zhanduan_filter_list=[],
                                  customizecontent=None):
    """问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    return calc_child_index_type_divide_major(
        problem_score,
        work_load,
        2,
        1,
        17,
        months_ago,
        'COUNT',
        'SCORE_e',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 问题平均质量分
def stats_score_per_check_problem(problem_score,
                                  problem_count,
                                  months_ago,
                                  risk_type,
                                  calc_score_by_formula=_calc_score_by_formula,
                                  choose_dpid_data=None,
                                  customizecontent=None,
                                  fraction=None):
    return calc_child_index_type_divide_major(
        problem_score,
        problem_count,
        2,
        1,
        11,
        months_ago,
        'COUNT',
        'SCORE_k',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        fraction=fraction)


def _calc_risk_score_per_person(series, work_load, choose_dpid_data,
                                hierarchy, idx,
                                calc_score_by_formula=_calc_score_by_formula,
                                title=None):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data['ratio'] = data['score'] / data['PERSON_NUMBER']
    data.fillna(0, inplace=True)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='score', denominator='PERSON_NUMBER')
    # 中间计算数据
    if not title:
        title = [
            '较大和重大安全风险问题质量分累计({0})/工作量({1})',
            '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
        ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["score"], 2)}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_risk_score_per_person_filter(series, work_load, choose_dpid_data, hierarchy,
                                       idx, zhanduan_filter_list, title):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    # data['ratio'] = data['score'] / data['PERSON_NUMBER']
    data['ratio'] = data.apply(lambda row: row['score'] / row['PERSON_NUMBER'] if
    row['PERSON_NUMBER'] > 0 else 0, axis=1)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_two(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula, zhanduan_filter_list)
    # 中间计算数据
    # title = [
    #     '较大和重大安全风险问题质量分累计({0})/工作量({1})',
    #     '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
    # ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["score"], 2)}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_risk_score_per_person_major(series, work_load, choose_dpid_data, hierarchy,
                                      idx, zhanduan_filter_list, title,
                                      calc_score_by_formula=_calc_score_by_formula,
                                      fraction=None):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['ratio'] = data.apply(
        lambda row: (row['score'] / row['PERSON_NUMBER']) if
        row['PERSON_NUMBER'] > 0 else 0, axis=1)
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_risk_score_per_person, fraction.months_ago,
                                         columns=['score', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula, zhanduan_filter_list,
        numerator='score', denominator='PERSON_NUMBER', fraction=fraction)
    # 中间计算数据
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["score"], 2) if pd.notnull(row["score"]) else 0.00}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_risk_score(df_xianchang, work_load, choose_dpid_data, hierarchy,
                        idx, calc_score_by_formula=_calc_score_by_formula,
                        title=None):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(sc_risk_score, work_load,
                                       choose_dpid_data, hierarchy,
                                       idx, calc_score_by_formula=calc_score_by_formula,
                                       title=title)


# 部分专业基数
def _calc_xc_risk_score_filter(df_xianchang, work_load, choose_dpid_data, hierarchy,
                               idx, zhanduan_filter_list, title):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_filter(sc_risk_score, work_load,
                                              choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title)


def _calc_xc_risk_score_major(df_xianchang, work_load, choose_dpid_data, hierarchy,
                              idx, zhanduan_filter_list, title,
                              calc_score_by_formula=_calc_score_by_formula, fraction=None):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_major(sc_risk_score, work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             fraction=fraction)


def _calc_risk_score(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx,
                     calc_score_by_formula=_calc_score_by_formula, title=None):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(risk_score, work_load, choose_dpid_data,
                                       hierarchy, idx,
                                       calc_score_by_formula=calc_score_by_formula,
                                       title=title)


# 部分专业基数
def _calc_risk_score_filter(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_filter(risk_score, work_load, choose_dpid_data,
                                              hierarchy, idx, zhanduan_filter_list, title)


def _calc_risk_score_major(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                           calc_score_by_formula=_calc_score_by_formula, fraction=None):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_major(risk_score, work_load, choose_dpid_data,
                                             hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             fraction=fraction)


# 较大风险问题质量均分
def stats_risk_score_per_person(df_jiaoda,
                                df_xianchang,
                                work_load,
                                months_ago,
                                risk_type,
                                child_weight=[0.6, 0.4],
                                choose_dpid_data=None,
                                calc_score_by_formula=_calc_score_by_formula,
                                title=[
                                    '较大和重大安全风险问题质量分累计({0})/工作量({1})',
                                    '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
                                ]):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score, _calc_xc_risk_score]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx,
                                             calc_score_by_formula=calc_score_by_formula,
                                             title=title)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 较大风险问题质量均分--部分专业基数
def stats_risk_score_per_person_filter(df_jiaoda,
                                       df_xianchang,
                                       work_load,
                                       months_ago,
                                       risk_type,
                                       child_weight=[0.6, 0.4],
                                       choose_dpid_data=None,
                                       zhanduan_filter_list=[],
                                       title=[
                                           '较大和重大安全风险问题质量分累计({0})/工作量({1})',
                                           '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
                                       ]):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score_filter, _calc_xc_risk_score_filter]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 较大风险问题质量均分
def stats_risk_score_per_person_major(df_jiaoda,
                                      df_xianchang,
                                      work_load,
                                      months_ago,
                                      risk_type,
                                      child_weight=[0.6, 0.4],
                                      choose_dpid_data=None,
                                      zhanduan_filter_list=[],
                                      calc_score_by_formula=_calc_score_by_formula,
                                      title=[
                                          '较大和重大安全风险问题质量分累计({0})/工作量({1})',
                                          '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
                                      ],
                                      fraction_list=[None, None]):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score_major, _calc_xc_risk_score_major]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 夜查率
def stats_yecha_ratio(df_yecha,
                      work_load,
                      months_ago,
                      risk_type,
                      calc_score_by_formula=_calc_score_by_formula,
                      choose_dpid_data=None,
                      customizecontent=None,
                      is_calc_score_base_major=True,
                      fraction=None):
    """夜查次数/工作量
    夜查次数:
    数据源：安全监督检查->安全信息采集->检查信息查询->车务->**站段->条数
    筛选项:检查信息基础筛选项、检查方式:“现场检查”“添乘检查”、是否夜查:是
    """
    return calc_child_index_type_divide_major(
        df_yecha,
        work_load,
        2,
        1,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        is_calc_score_base_major=is_calc_score_base_major,
        customizecontent=customizecontent,
        fraction=fraction)


# 夜查率
def stats_yecha_ratio_major(df_yecha,
                            work_load,
                            months_ago,
                            risk_type,
                            calc_score_by_formula=_calc_score_by_formula,
                            choose_dpid_data=None,
                            customizecontent=None,
                            is_calc_score_base_major=True,
                            fraction=None,
                            zhanduan_filter_list=[]):
    """夜查次数/工作量
    夜查次数:
    数据源：安全监督检查->安全信息采集->检查信息查询->车务->**站段->条数
    筛选项:检查信息基础筛选项、检查方式:“现场检查”“添乘检查”、是否夜查:是
    """
    return calc_child_index_type_divide_major(
        df_yecha,
        work_load,
        2,
        1,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        is_calc_score_base_major=is_calc_score_base_major,
        customizecontent=customizecontent,
        fraction=fraction,
        zhanduan_filter_list=zhanduan_filter_list)


# 夜查率
def stats_yecha_ratio_type_gd(yecha_ticket,
                              yecha_genban_ticket,
                              months_ago,
                              risk_type,
                              calc_score_by_formula=_calc_score_by_formula,
                              choose_dpid_data=None,
                              customizecontent=None):
    """夜查次数/工作量
    夜查次数:
    数据源：安全监督检查->安全信息采集->检查信息查询->车务->**站段->条数
    筛选项:检查信息基础筛选项、检查方式:“现场检查”“添乘检查”、是否夜查:是
    """
    return calc_child_index_type_divide_major(
        yecha_ticket,
        yecha_genban_ticket,
        2,
        1,
        13,
        months_ago,
        'COUNT',
        'SCORE_m',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


def stats_genban_ratio(df_genban,
                       work_load,
                       months_ago,
                       risk_type,
                       calc_score_by_formula=_calc_score_by_formula,
                       choose_dpid_data=None,
                       customizecontent=None):
    """跟班次数/工作量
    现场检查调车跟班次数:
    数据源：安全监督检查->安全信息采集->检查信息查询->车务->**站段->条数
    筛选项:检查信息基础筛选项、检查方式:“现场检查”“添乘检查”、是否跟班:是
    """
    return calc_child_index_type_divide_major(
        df_genban,
        work_load,
        2,
        1,
        12,
        months_ago,
        'COUNT',
        'SCORE_l',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


def stats_yiban_risk_ratio_type_one(df_yiban_risk_prob,
                                    df_all_prob,
                                    months_ago,
                                    risk_type,
                                    calc_score_by_formula=_calc_score_by_formula,
                                    choose_dpid_data=None,
                                    zhanduan_filter_list=[],
                                    customizecontent=None,
                                    fraction=None
                                    ):
    """一般及以上风险问题占比：一般及以上风险问题数/总问题数。得分：与基数比较按下表计算。
    """
    return calc_child_index_type_divide_major(
        df_yiban_risk_prob,
        df_all_prob,
        2,
        1,
        8,
        months_ago,
        'COUNT',
        'SCORE_h',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        fraction=fraction
    )


def stats_yiban_risk_ratio_type_one_filter(df_yiban_risk_prob,
                                           df_all_prob,
                                           months_ago,
                                           risk_type,
                                           calc_score_by_formula=_calc_score_by_formula,
                                           choose_dpid_data=None,
                                           zhanduan_filter_list=[],
                                           customizecontent=None):
    """一般及以上风险问题占比：一般及以上风险问题数/总问题数。得分：与基数比较按下表计算。
    """
    return calc_child_index_type_divide_two(
        df_yiban_risk_prob,
        df_all_prob,
        2,
        1,
        8,
        months_ago,
        'COUNT',
        'SCORE_h',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 覆盖率 - 安监类型，包含检查点
def stats_check_address_ratio_include_check_point(real_check_banzu_sql,
                                                  real_check_point_sql,
                                                  banzu_point_sql,
                                                  check_point_sql,
                                                  department_data,
                                                  months_ago,
                                                  risk_type,
                                                  risk_name,
                                                  choose_dpid_data=None,
                                                  calc_func=lambda x: x * 100,
                                                  is_calc_score_base_major=False,
                                                  customizecontent=None):
    """（检查地点数/作业班组数)*100
    """
    stats_month = get_custom_month(months_ago)
    check_item_ids = get_query_condition_by_risktype(risk_name)[0]
    # 检查地点数
    data_real = pd.concat(
        [
            pd_query(real_check_point_sql.format(
                *stats_month, check_item_ids)),
            pd_query(real_check_banzu_sql.format(
                *stats_month, check_item_ids)),
        ],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, department_data)
    # 地点总数
    data_total = pd.concat(
        [
            pd_query(check_point_sql),
            pd_query(banzu_point_sql),
        ],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, department_data)

    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        calc_func=calc_func,
        dpid_func=choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 覆盖率 - 安监类型，包含检查点
def stats_check_address_ratio_include_check_point_excellent(real_check_banzu_data,
                                                            real_check_point_data,
                                                            banzu_point_data,
                                                            check_point_data,
                                                            department_data,
                                                            months_ago,
                                                            risk_type,
                                                            choose_dpid_data=None,
                                                            calc_func=lambda x: x * 100,
                                                            is_calc_score_base_major=False,
                                                            customizecontent=None,
                                                            fraction=None):
    """（检查地点数/作业班组数)*100
    """
    # 检查地点数
    data_real = pd.concat(
        [
            real_check_point_data,
            real_check_banzu_data,
        ],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, department_data)
    # 地点总数
    data_total = pd.concat(
        [

            check_point_data,
            banzu_point_data,
        ],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, department_data)

    return calc_child_index_type_divide_major(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        calc_func=calc_func,
        dpid_func=choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent,
        fraction=fraction)


# 覆盖率
def stats_check_address_ratio(reak_check_banzu_sql,
                              banzu_point_sql,
                              department_data,
                              months_ago,
                              risk_type,
                              risk_name,
                              choose_dpid_data=None,
                              calc_func=lambda x: x * 100,
                              is_calc_score_base_major=False,
                              customizecontent=None):
    """（检查地点数/作业班组数)*100
    """
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    # 检查作业班组数
    data_real = pd_query(
        reak_check_banzu_sql.format(*stats_month, check_item_ids))
    data_real = df_merge_with_dpid(data_real, department_data)

    # 作业班组数
    data_total = pd_query(banzu_point_sql.format(risk_ids))
    data_total = df_merge_with_dpid(data_total, department_data)
    data_real = data_real[data_real.DEPARTMENT_ID.isin(data_total.DEPARTMENT_ID.tolist())]
    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        calc_func,
        choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 覆盖率--与专业基数比较
def stats_check_address_ratio_compare_with_major(reak_check_banzu_sql,
                                                 banzu_point_sql,
                                                 department_data,
                                                 months_ago,
                                                 risk_type,
                                                 risk_name,
                                                 choose_dpid_data=None):
    """（检查地点数/班组地点总数)
    """
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    # 检查作业班组数
    data_real = pd_query(
        reak_check_banzu_sql.format(*stats_month, check_item_ids))
    data_real = df_merge_with_dpid(data_real, department_data)

    # 作业班组数
    data_total = pd_query(banzu_point_sql.format(risk_ids))
    data_total = df_merge_with_dpid(data_total, department_data)
    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        lambda x: x,
        choose_dpid_data,
        risk_type=risk_type)


# 覆盖率
def stats_check_address_ratio_excellent(real_check_banzu_data,
                                        banzu_point_data,
                                        department_data,
                                        months_ago,
                                        risk_type,
                                        choose_dpid_data=None,
                                        calc_func=lambda x: x * 100,
                                        is_calc_score_base_major=False,
                                        customizecontent=None):
    """（检查地点数/作业班组数)*100
    """
    # 检查作业班组数
    data_real = real_check_banzu_data
    data_real = df_merge_with_dpid(data_real, department_data)

    # 作业班组数
    data_total = banzu_point_data
    data_total = df_merge_with_dpid(data_total, department_data)
    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        calc_func,
        choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent)


def _calc_media_val_person(choose_dpid_data, series, work_load, hierarchy,
                           idx, title=['监控调阅时长累计({0})/总人数({1})',
                                       '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
                                       '调阅班组数({0})/班组数({1})'],
                           calc_score_by_formula=_calc_score_by_formula):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula)
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(
                f'{round(row["media"], 4)}', round(row['PERSON_NUMBER'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_media_val_person_major(choose_dpid_data, series, work_load, hierarchy,
                                 idx, title=['监控调阅时长累计({0})/总人数({1})',
                                             '监控调阅发现问题数({0})/总人数({1})',
                                             '监控调阅发现问题质量分累计({0})/总人数({1})',
                                             '调阅班组数({0})/班组数({1})'],
                                 zhanduan_filter_list=None,
                                 calc_score_by_formula=_calc_score_by_formula, fraction=None):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_media_intensity, fraction.months_ago,
                                         columns=['media', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='media', denominator='PERSON_NUMBER', zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction
    )
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(
                f'{round(row["media"], 4)}', round(row['PERSON_NUMBER'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_media_val_person_two(choose_dpid_data, series, work_load, hierarchy,
                               idx, zhanduan_filter_list, title=['监控调阅时长累计({0})/总人数({1})',
                                                                 '监控调阅发现问题数({0})/总人数({1})',
                                                                 '监控调阅发现问题质量分累计({0})/总人数({1})',
                                                                 '调阅班组数({0})/班组数({1})']):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_two(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula, zhanduan_filter_list)
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(
                f'{round(row["media"], 4)}', round(row['PERSON_NUMBER'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅人均时长
def _calc_media_time_per_person(choose_dpid_data, stats_month, department_data,
                                work_load, check_item_ids, hierarchy, idx,
                                media_cost_time_sql, title,
                                calc_score_by_formula=_calc_score_by_formula):
    media_time = df_merge_with_dpid(
        pd_query(media_cost_time_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['TIME'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title,
                                  calc_score_by_formula=calc_score_by_formula)


# 监控调阅人均时长
def _calc_media_time_per_person_major(choose_dpid_data, stats_month, department_data,
                                      work_load, check_item_ids, hierarchy, idx,
                                      media_cost_time_sql, title,
                                      zhanduan_filter_list=None,
                                      calc_score_by_formula=_calc_score_by_formula,
                                      fraction=None):
    media_time = df_merge_with_dpid(
        pd_query(media_cost_time_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['TIME'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        zhanduan_filter_list=zhanduan_filter_list,
                                        fraction=fraction)


# 监控调阅人均问题个数
def _calc_media_problem_per_person(choose_dpid_data, stats_month,
                                   department_data, work_load, check_item_ids,
                                   hierarchy, idx, media_problem_number_sql, title,
                                   calc_score_by_formula=_calc_score_by_formula):
    media_time = df_merge_with_dpid(
        pd_query(
            media_problem_number_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title,
                                  calc_score_by_formula=calc_score_by_formula)


# 监控调阅人均问题个数
def _calc_media_problem_per_person_major(choose_dpid_data, stats_month,
                                         department_data, work_load, check_item_ids,
                                         hierarchy, idx, media_problem_number_sql, title,
                                         zhanduan_filter_list=None,
                                         calc_score_by_formula=_calc_score_by_formula, fraction=None):
    # 拓展新增df列表
    if isinstance(media_problem_number_sql, str):
        media_time = df_merge_with_dpid(
            pd_query(
                media_problem_number_sql.format(*stats_month, check_item_ids)),
            department_data)
        media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
        rst_data = _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                                hierarchy, idx, title=title,
                                                calc_score_by_formula=calc_score_by_formula,
                                                zhanduan_filter_list=zhanduan_filter_list,
                                                fraction=fraction)
    else:
        weight_list = [0.7, 0.3]
        sub_title_dict = {
            f'{idx}_0': '监控调阅发现A/B问题数({0})/基本工作量({1})',
            f'{idx}_1': '监控调阅发现A/B/C问题数({0})/基本工作量({1})'
        }
        _tmp_score = []
        _tmp_content = []
        for pos, media_time in enumerate(media_problem_number_sql):
            _df = df_merge_with_dpid(media_time, department_data)
            _media_time = _df.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
            # 兼容使用title取文字模版
            _rst_data = _calc_media_val_person_major(choose_dpid_data, _media_time, work_load,
                                                     hierarchy, f'{idx}_{pos}', title=sub_title_dict,
                                                     calc_score_by_formula=calc_score_by_formula,
                                                     zhanduan_filter_list=zhanduan_filter_list,
                                                     fraction=fraction)
            _rst_data[0]['SCORE'] = _rst_data[0]['SCORE'] * weight_list[pos]
            _tmp_score.append(_rst_data[0])
            _tmp_content.append(_rst_data[1])
        _tmp_score = pd.concat(
            _tmp_score, axis=0, sort=False)
        _tmp_content = pd.concat(
            _tmp_content, axis=1, sort=False)
        _tmp_content[f'middle_{idx}'] = ''
        for key in sub_title_dict.keys():
            _tmp_content[f'middle_{idx}'] += _tmp_content[f'middle_{key}'] + '\n'
        _tmp_score = _tmp_score.groupby(['DEPARTMENT_ID'])['SCORE'].sum()
        # 删除冗余列
        _tmp_content.drop([f'middle_{key}' for key in sub_title_dict.keys()], inplace=True, axis=1)
        rst_data = _tmp_score, _tmp_content
    return rst_data


# 监控调阅人均质量分
def _calc_media_score_per_person(choose_dpid_data, stats_month,
                                 department_data, work_load, check_item_ids,
                                 hierarchy, idx, media_problem_score_sql, title,
                                 calc_score_by_formula=_calc_score_by_formula):
    media_time = df_merge_with_dpid(
        pd_query(media_problem_score_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title,
                                  calc_score_by_formula=calc_score_by_formula)


# 监控调阅人均质量分
def _calc_media_score_per_person_major(choose_dpid_data, stats_month,
                                       department_data, work_load, check_item_ids,
                                       hierarchy, idx, media_problem_score_sql, title,
                                       zhanduan_filter_list=None,
                                       calc_score_by_formula=_calc_score_by_formula,
                                       fraction=None
                                       ):
    # 拓展新增df列表
    if isinstance(media_problem_score_sql, str):
        media_time = df_merge_with_dpid(
            pd_query(media_problem_score_sql.format(*stats_month, check_item_ids)),
            department_data)
        media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
        rst_data = _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                                hierarchy, idx, title=title,
                                                calc_score_by_formula=calc_score_by_formula,
                                                zhanduan_filter_list=zhanduan_filter_list,
                                                fraction=fraction)
    else:
        weight_list = [0.7, 0.3]
        sub_title_dict = {
            f'{idx}_0': '监控调阅发现A/B问题质量分({0})/基本工作量({1})',
            f'{idx}_1': '监控调阅发现A/B/C问题质量分({0})/基本工作量({1})'
        }
        _tmp_score = []
        _tmp_content = []
        for pos, media_time in enumerate(media_problem_score_sql):
            _df = df_merge_with_dpid(media_time, department_data)
            _media_time = _df.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
            # 兼容使用title取文字模版
            _rst_data = _calc_media_val_person_major(choose_dpid_data, _media_time, work_load,
                                                     hierarchy, f'{idx}_{pos}', title=sub_title_dict,
                                                     calc_score_by_formula=calc_score_by_formula,
                                                     zhanduan_filter_list=zhanduan_filter_list,
                                                     fraction=fraction)
            _rst_data[0]['SCORE'] = _rst_data[0]['SCORE'] * weight_list[pos]
            _tmp_score.append(_rst_data[0])
            _tmp_content.append(_rst_data[1])
        _tmp_score = pd.concat(
            _tmp_score, axis=0, sort=False)
        _tmp_content = pd.concat(
            _tmp_content, axis=1, sort=False)
        _tmp_content[f'middle_{idx}'] = ''
        for key in sub_title_dict.keys():
            _tmp_content[f'middle_{idx}'] += _tmp_content[f'middle_{key}'] + '\n'
        _tmp_score = _tmp_score.groupby(['DEPARTMENT_ID'])['SCORE'].sum()
        # 删除冗余列
        _tmp_content.drop([f'middle_{key}' for key in sub_title_dict.keys()], inplace=True, axis=1)
        rst_data = _tmp_score, _tmp_content
    return rst_data


# 监控调阅人均质量分-过滤部分站段做基数
def _calc_media_score_per_person_two(choose_dpid_data, stats_month,
                                     department_data, work_load, check_item_ids,
                                     hierarchy, idx, media_problem_score_sql, title, zhanduan_filter_list):
    media_time = df_merge_with_dpid(
        pd_query(media_problem_score_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person_two(choose_dpid_data, media_time, work_load,
                                      hierarchy, idx, zhanduan_filter_list, title)


# 监控调阅覆盖比例(调阅班组数/作业或施工班组数)
def _calc_media_banzu_val_workbanzu(choose_dpid_data, stats_month,
                                    department_data, work_load, check_item_ids,
                                    hierarchy, idx, monitor_watch_discovery_ratio_sqllist, title,
                                    calc_score_by_formula=_calc_score_by_formula):
    """
    :param choose_dpid_data:
    :param stats_month:
    :param department_data:
    :param work_load:
    :param check_item_ids:
    :param hierarchy:
    :param idx:
    :param media_problem_score_sql:
    :param monitor_watch_discovery_ratio_sqllist: 监控调阅覆盖比例sql列表（一般包含查询调阅班组数sql，查询施工班组数sql）
    :param watch_media_banzu_count:
    :return:
    """
    # 调阅班组数
    watch_media_banzu_count = df_merge_with_dpid(
        pd_query(monitor_watch_discovery_ratio_sqllist[0].format(
            *stats_month, check_item_ids)),
        department_data)
    # 作业班组数
    work_banzu_count = df_merge_with_dpid(
        pd_query(monitor_watch_discovery_ratio_sqllist[1].format(
            check_item_ids)),
        department_data)
    media_time = watch_media_banzu_count.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_banzu_count,
                                  hierarchy, idx, title=title,
                                  calc_score_by_formula=_calc_score_by_formula_media_banzu)


# 监控调阅覆盖比例(调阅班组数/作业或施工班组数)
def _calc_media_banzu_val_workbanzu_major(choose_dpid_data, stats_month,
                                          department_data, work_load, check_item_ids,
                                          hierarchy, idx, monitor_watch_discovery_ratio_sqllist, title,
                                          zhanduan_filter_list=None,
                                          calc_score_by_formula=_calc_score_by_formula,
                                          fraction=None):
    """
    :param choose_dpid_data:
    :param stats_month:
    :param department_data:
    :param work_load:
    :param check_item_ids:
    :param hierarchy:
    :param idx:
    :param media_problem_score_sql:
    :param monitor_watch_discovery_ratio_sqllist: 监控调阅覆盖比例sql列表（一般包含查询调阅班组数sql，查询施工班组数sql）
    :param watch_media_banzu_count:
    :return:
    """
    # 作业班组数(总班组)
    work_banzu = pd_query(monitor_watch_discovery_ratio_sqllist[1].format(check_item_ids))
    work_banzu_count = df_merge_with_dpid(work_banzu, department_data, how='right')

    # 调阅班组数
    watch_media_banzu = pd_query(monitor_watch_discovery_ratio_sqllist[0].format(
        *stats_month, check_item_ids))
    watch_media_banzu = pd.merge(
        watch_media_banzu,
        work_banzu[['FK_DEPARTMENT_ID']],
        how='inner',
        on='FK_DEPARTMENT_ID'
    )
    watch_media_banzu_count = df_merge_with_dpid(watch_media_banzu, department_data, how='right')

    watch_media_banzu_count.fillna(0, inplace=True)
    work_banzu_count.fillna(0, inplace=True)

    media_time = watch_media_banzu_count.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_banzu_count,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=_calc_score_by_formula_media_banzu,
                                        zhanduan_filter_list=zhanduan_filter_list,
                                        fraction=fraction)


# 监控调阅力度
def stats_media_intensity(department_data,
                          work_load,
                          months_ago,
                          risk_name,
                          risk_type,
                          choose_dpid_data=None,
                          media_cost_time_sql=None,
                          media_problem_number_sql=None,
                          media_problem_score_sql=None,
                          monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
                          child_weight=[0.35, 0.35, 0.3, 0.00],
                          title=['监控调阅时长累计({0})/总人数({1})',
                                 '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
                                 '调阅班组数({0})/班组数({1})'],
                          calc_score_by_formula=_calc_score_by_formula):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_cost_time_sql, media_problem_number_sql,
                  media_problem_score_sql, monitor_watch_discovery_ratio_sqllist
                  ]
    if media_cost_time_sql is None:
        media_func = [
            MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
            MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
        ]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person, _calc_media_problem_per_person,
            _calc_media_score_per_person, _calc_media_banzu_val_workbanzu
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load,
                check_item_ids, hierarchy, idx, media_func[idx], title=title,
                calc_score_by_formula=calc_score_by_formula)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 监控调阅力度
def stats_media_intensity_major(department_data,
                                work_load,
                                months_ago,
                                risk_name,
                                risk_type,
                                risk_ids=None,
                                choose_dpid_data=None,
                                media_cost_time_sql=None,
                                media_problem_number_sql=None,
                                media_problem_score_sql=None,
                                monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
                                child_weight=[0.35, 0.35, 0.3, 0.00],
                                zhanduan_filter_list=[],
                                title=['监控调阅时长累计({0})/总人数({1})',
                                       '监控调阅发现问题数({0})/总人数({1})',
                                       '监控调阅发现问题质量分累计({0})/总人数({1})',
                                       '调阅班组数({0})/班组数({1})'],
                                calc_score_by_formula=_calc_score_by_formula,
                                fraction_list=[None, None, None, None]):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    if not risk_ids:
        diaoche = get_query_condition_by_risktype(risk_name)
        check_item_ids = diaoche[0]
    else:
        check_item_ids = risk_ids
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_cost_time_sql, media_problem_number_sql,
                  media_problem_score_sql, monitor_watch_discovery_ratio_sqllist
                  ]
    if media_cost_time_sql is None:
        media_func = [
            MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
            MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
        ]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person_major, _calc_media_problem_per_person_major,
            _calc_media_score_per_person_major, _calc_media_banzu_val_workbanzu_major
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load, check_item_ids,
                hierarchy, idx, media_func[idx], title=title, zhanduan_filter_list=zhanduan_filter_list,
                calc_score_by_formula=calc_score_by_formula, fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def __calc_media_analysiscenter_check_intensity(choose_dpid_data, stats_month,
                                                department_data, work_load, check_item_ids,
                                                hierarchy, idx, analysis_center_check_intensity_sqllist,
                                                title, calc_score_by_formula=_calc_score_by_formula,
                                                fraction=None):
    # 安全分析中心发现问题数
    analysis_center_problem_count = df_merge_with_dpid(
        pd_query(analysis_center_check_intensity_sqllist[0].format(
            *stats_month, check_item_ids)),
        department_data)
    # 安全分析中心发现问题质量分 ANALYSIS_CENTER_PROBLEM_SCORE
    analysis_center_problem_score = df_merge_with_dpid(
        pd_query(analysis_center_check_intensity_sqllist[1].format(*stats_month,
                                                                   check_item_ids)),
        department_data)
    # todo 这里默认给问题数*0.6, 质量分*0.4,之后还是根据需要传值进来
    analysis_center_problem_count['NUMBER'] = analysis_center_problem_count['NUMBER'] * 0.6
    analysis_center_problem_score['NUMBER'] = analysis_center_problem_score['NUMBER'] * 0.4
    analysis_center_workload = analysis_center_problem_count.append(
        analysis_center_problem_score)

    media_time = analysis_center_workload.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        fraction=fraction)


def _calc_media_analysiscenter_check_intensity_problem(choose_dpid_data, stats_month,
                                                       department_data, work_load, check_item_ids,
                                                       hierarchy, idx, analysis_center_problem_count_sql,
                                                       title, calc_score_by_formula=_calc_score_by_formula,
                                                       fraction=None):
    # 安全分析中心发现问题数
    analysis_center_problem_count = df_merge_with_dpid(
        pd_query(analysis_center_problem_count_sql.format(
            *stats_month, check_item_ids)),
        department_data)
    media_time = analysis_center_problem_count.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        fraction=fraction)


def _calc_media_analysiscenter_check_intensity_score(choose_dpid_data, stats_month,
                                                     department_data, work_load, check_item_ids,
                                                     hierarchy, idx, analysis_center_problem_score_sql,
                                                     title, calc_score_by_formula=_calc_score_by_formula,
                                                     fraction=None):
    # 安全分析中心发现问题质量分 ANALYSIS_CENTER_PROBLEM_SCORE
    analysis_center_problem_score = df_merge_with_dpid(
        pd_query(analysis_center_problem_score_sql.format(*stats_month,
                                                          check_item_ids)),
        department_data)
    media_time = analysis_center_problem_score.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        fraction=fraction)


# 新的监控调阅力度指数计算函数
def new_stats_media_intensity(department_data,
                              work_load,
                              months_ago,
                              risk_name,
                              risk_type,
                              choose_dpid_data=None,
                              media_cost_time_sql=None,
                              media_problem_number_sql=None,
                              media_problem_score_sql=None,
                              monitor_watch_discovery_ratio_sqllist=None,
                              analysis_center_problem_count_sql=None,
                              analysis_center_problem_score_sql=None,
                              child_weight=[0.35, 0.35, 0.3, 0.00, 0.00, 0.00],
                              title=[],
                              calc_score_by_formula=_calc_score_by_formula,
                              fraction_list=[None, None, None, None, None, None]):
    """
    参数字典传参，动态识别数量
    :param fraction_list:
    :param calc_score_by_formula:
    :param analysis_center_problem_count_sql:
    :param analysis_center_problem_score_sql:
    :param department_data:
    :param work_load:
    :param months_ago:
    :param risk_name:
    :param risk_type:
    :param choose_dpid_data:
    :param media_cost_time_sql:
    :param media_problem_number_sql:
    :param media_problem_score_sql:
    :param monitor_watch_discovery_ratio_sqllist:
    :param child_weight:
    :param title:
    :return:
    """
    if not title:
        title = ['监控调阅时长累计({0})/总人数({1})',
                 '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
                 '调阅班组数({0})/班组数({1})', '安全分析中心检查问题数({0})/基本工作量({1})',
                 '安全分析中心检查问题质量分({0})/基本工作量({1})']
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_cost_time_sql, media_problem_number_sql,
                  media_problem_score_sql, monitor_watch_discovery_ratio_sqllist,
                  analysis_center_problem_count_sql, analysis_center_problem_score_sql,
                  ]
    # 实际参与计算的sql
    real_idx = [i for i in range(len(media_func)) if media_func[i]]
    if not real_idx:
        media_func = [
            MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
            MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
            ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL
        ]
        real_idx = [i for i in range(4)]
    real_media_func = [media_func[index] for index in real_idx]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person_major, _calc_media_problem_per_person_major,
            _calc_media_score_per_person_major, _calc_media_banzu_val_workbanzu_major,
            _calc_media_analysiscenter_check_intensity_problem,
            _calc_media_analysiscenter_check_intensity_score
        ]
        # 实际参与计算的函数
        real_child_func = [child_func[index] for index in real_idx]
        # 实际参与渲染的title
        real_title = [title[index] for index in real_idx]
        real_wegiht = [child_weight[index] for index in real_idx]
        for idx, ifunc in enumerate(real_child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load,
                check_item_ids, hierarchy, idx, real_media_func[idx], title=real_title,
                calc_score_by_formula=calc_score_by_formula,
                fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * real_wegiht[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 监控调阅力度子指数数据字典
def get_media_func_data_dict(
        months_ago,
        check_item_ids,
        check_risk_ids,
        department_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        monitor_watch_discovery_ratio_sql_list=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        check_ids_dict=None,
):
    """
    获取监控调阅力度子指数DataFrame数据字典
    :param months_ago: 需要计算的months_ago个月之前的月份
    :param check_item_ids: 检查项目id
    :param check_risk_ids: 检查风险id
    :param department_data: 部门数据
    :param media_cost_time_sql: 监控调阅时长sql
    :param media_problem_number_sql: 监控调阅问题数sql
    :param media_problem_score_sql: 监控调阅问题数质量分sql
    :param monitor_watch_discovery_ratio_sql_list: 监控调阅班组及作业班组sql列表
    :param check_ids_dict: 对应子指数的检查配置
    :return:
    """
    if not check_ids_dict:
        check_ids_dict = {
            'media_cost_time': check_item_ids,
            'media_problem_number': check_risk_ids,
            'media_problem_score': check_risk_ids,
            'monitor_watch_discovery_ratio_list': check_item_ids,
        }
    stats_month = get_custom_month(months_ago)
    # 时长
    media_cost_time = pd_query(
        media_cost_time_sql.format(
            *stats_month, check_ids_dict.get('media_cost_time')))
    media_cost_time = df_merge_with_dpid(
        media_cost_time, department_data, how='right')
    media_cost_time.fillna(0, inplace=True)

    # 问题数
    media_problem_number = pd_query(
        media_problem_number_sql.format(
            *stats_month, check_ids_dict.get('media_problem_number')))
    media_problem_number = df_merge_with_dpid(
        media_problem_number, department_data, how='right')
    media_problem_number.fillna(0, inplace=True)

    # 问题质量分
    media_problem_score = pd_query(
        media_problem_score_sql.format(
            *stats_month, check_ids_dict.get('media_problem_score')))
    media_problem_score = df_merge_with_dpid(
        media_problem_score, department_data, how='right')
    media_problem_score.fillna(0, inplace=True)

    # 作业班组数(具备检查项目的总班组数)
    work_department = pd_query(
        monitor_watch_discovery_ratio_sql_list[1].format(
            check_ids_dict.get('monitor_watch_discovery_ratio_list')))
    work_department_count = df_merge_with_dpid(
        work_department, department_data, how='right')
    work_department_count.fillna(0, inplace=True)

    # 调阅班组数(查询去检查的所有班组与总班组取交集)
    watch_media_department = pd_query(
        monitor_watch_discovery_ratio_sql_list[0].format(
            *stats_month, check_ids_dict.get('monitor_watch_discovery_ratio_list')))
    watch_media_department = pd.merge(
        watch_media_department,
        work_department[['FK_DEPARTMENT_ID']],
        how='inner',
        on='FK_DEPARTMENT_ID'
    )
    watch_media_department_count = df_merge_with_dpid(
        watch_media_department, department_data, how='right')
    watch_media_department_count.fillna(0, inplace=True)
    # 调阅班组,总班组列表
    monitor_watch_discovery_ratio_list = [
        watch_media_department_count, work_department_count]

    media_func_data_dict = {
        'media_cost_time': media_cost_time,
        'media_problem_number': media_problem_number,
        'media_problem_score': media_problem_score,
        'monitor_watch_discovery_ratio_list': monitor_watch_discovery_ratio_list,
    }
    return media_func_data_dict


# 监控调阅子指数-DataFrame
def _calc_media_child_func_excellent(
        choose_dpid_data,
        work_load,
        hierarchy,
        idx,
        child_data,
        title=None,
        zhanduan_filter_list=None,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=None):
    """
    监控调阅力度子指数统一计算函数
    :param choose_dpid_data: 可选部门数据函数
    :param work_load: 工作量
    :param hierarchy: 层级
    :param idx: 子指数在列表中的位置
    :param child_data: 子指数数据
    :param title: 子指数标题列表
    :param zhanduan_filter_list: 不参与基数计算的部门列表
    :param calc_score_by_formula: 得分计算函数
    :param fraction: 基数选择中的合成基数
    :return:
    """
    if isinstance(child_data, list):
        work_load = child_data[1]
        child_data = child_data[0]
    # 子指数数据
    child_data = child_data.groupby(
        [f'TYPE{hierarchy}'])['COUNT'].sum().to_frame()
    # 工作量数据
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='WORK_LOAD_COUNT')
    data = pd.concat([child_data, work_load], axis=1, sort=False)
    data['ratio'] = data['COUNT'] / data['WORK_LOAD_COUNT']
    # 是否为合成专业基数
    if fraction:
        write_cardinal_number_basic_data(
            data, choose_dpid_data(hierarchy), fraction,
            fraction.risk_type, MainType.check_intensity,
            CheckIntensityDetailType.stats_media_intensity, fraction.months_ago,
            columns=['COUNT', 'WORK_LOAD_COUNT'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='COUNT', denominator='WORK_LOAD_COUNT',
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction
    )
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(
                f'{round(row["COUNT"], 4)}', round(row['WORK_LOAD_COUNT'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['COUNT', 'ratio', 'WORK_LOAD_COUNT'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅力度-DataFrame
def stats_media_intensity_excellent(department_data,
                                    work_load,
                                    months_ago,
                                    risk_name,
                                    risk_type,
                                    choose_dpid_data=None,
                                    child_weight=None,
                                    zhanduan_filter_list=None,
                                    title=None,
                                    media_func_key_list=None,
                                    calc_score_by_formula_dict=None,
                                    media_func_data_dict=None,
                                    fraction_list=None
                                    ):
    """
    监控调阅力度,以DataFrame做计算
    :param department_data: 部门数据
    :param work_load: 工作量
    :param months_ago: 月份
    :param risk_name: 风险名称对应PK_ID
    :param risk_type: 风险编号
    :param choose_dpid_data: 可选择部门数据
    :param child_weight: 子指数权重
    :param zhanduan_filter_list: 不参与基数计算的部门列表
    :param title: 子指数标题
    :param media_func_key_list: 需要执行的子指数名称列表
    :param calc_score_by_formula_dict: 对应的子指数分数计算函数
    :param media_func_data_dict: 子指数执行需要的数据字典(部门id,count格式df)
    :param fraction_list: 基数选择
    :return:
    """
    if child_weight is None:
        child_weight = [0.35, 0.35, 0.3, 0.00]
    if title is None:
        title = ['监控调阅时长累计({0})/总人数({1})',
                 '监控调阅发现问题数({0})/总人数({1})',
                 '监控调阅发现问题质量分累计({0})/总人数({1})',
                 '调阅班组数({0})/班组数({1})']
    if not media_func_key_list:
        media_func_key_list = ['media_cost_time',
                               'media_problem_number',
                               'media_problem_score',
                               'monitor_watch_discovery_ratio_list']
    if not calc_score_by_formula_dict:
        calc_score_by_formula_dict = {
            'media_cost_time': _calc_score_by_formula,
            'media_problem_number': _calc_score_by_formula,
            'media_problem_score': _calc_score_by_formula,
            'monitor_watch_discovery_ratio_list':
                _calc_score_by_formula_media_banzu,
        }
    if not fraction_list:
        fraction_list = (None, None, None, None)
    if not media_func_data_dict:
        risk_data = get_query_condition_by_risktype(risk_name)
        check_item_ids, check_risk_ids, position = risk_data
        media_func_data_dict = get_media_func_data_dict(
            months_ago, check_item_ids, check_risk_ids, department_data)
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        for idx, func_key in enumerate(media_func_key_list):
            child_data = media_func_data_dict.get(func_key)
            rst_func, rst_basic_data = _calc_media_child_func_excellent(
                choose_dpid_data, work_load, hierarchy, idx, child_data,
                title=title,
                zhanduan_filter_list=zhanduan_filter_list,
                calc_score_by_formula=calc_score_by_formula_dict.get(
                    func_key, _calc_score_by_formula),
                fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


if __name__ == '__main__':
    pass
