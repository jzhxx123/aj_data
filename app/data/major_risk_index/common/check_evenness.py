#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    date: 2018/07/31
    desc: 检查均衡度指数
"""

import calendar
import datetime

import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_month_day
from app.data.major_risk_index.util import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_child_index_type_sum, df_merge_with_dpid, format_export_basic_data,
    summizet_operation_set, write_export_basic_data_to_mongo,
    calc_child_index_type_divide_major)
from app.data.util import (get_history_months, pd_query)


def _calc_score_by_formula(row, column, major_column, weight_list=None):
    high_ratio = weight_list[0] if weight_list else 4
    low_ratio = weight_list[1] if weight_list else -0.5
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= high_ratio:
            return -2
        elif _ratio <= low_ratio:
            return -2
        else:
            return 0


def _calc_score_by_formula_new(row, column, major_column, weight_list=None):
    """检查地点-工作量为人数,具备ADDRESS_NAME"""
    high_ratio = weight_list[0] if weight_list else 4
    low_ratio = weight_list[1] if weight_list else -0.5
    if 'ADDRESS_NAME' in list(row.index):
        if isinstance(row['ADDRESS_NAME'], int):
            return 0
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= high_ratio:
            return -2
        elif _ratio <= low_ratio:
            return -2
        else:
            return 0


def _calc_score_by_formula_chewu_laoan(row, column, major_column):
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 6:
            return -2
        elif _ratio <= -0.6:
            return -0.5
        else:
            return 0


def _calc_score_by_formula_workload(
        row, column, major_column, count_column, df_type3_count):
    """[7.10工务施工工作量相关]

    Arguments:
        row {[df]} -- [description]
        column {[str]} -- [description]
        major_column {[str]} -- [description]
    """
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio >= -0.15:
        _score = 90 + _ratio * 66
    elif _ratio >= -0.3:
        _score = 80 + (_ratio + 0.15) * 160
    else:
        _score = 71 + _ratio * 50
    return _score / df_type3_count[row[count_column]]


def _calc_hour_code(hour):
    """1-12 分别代表0-23时， 每2个小时一个时段，左闭又开,ex: [4: 6)
    Arguments:
        hour {int} -- 0 - 24时
    Returns:
        [int] -- 1- 12代表12个时段
    """
    code = 1 + (hour // 2)
    return code


def _calc_between_hours(start_hour, end_hour):
    """计算开始结束时间之间的时段间隔
    Arguments:
        start_hour {int} -- 0 - 23
        end_hour {int} -- 0 -23
    Returns:
        [list[int]] --
    """
    start_hour = _calc_hour_code(start_hour)
    end_hour = _calc_hour_code(end_hour)
    # 跨天
    if start_hour > end_hour:
        return list(range(start_hour, 13)) + list(range(1, end_hour + 1))
    else:
        return list(range(start_hour, end_hour + 1))


# 判断是否是周六或者周日
def _is_weekends(year, month, day):
    if day >= current_app.config.get('UPDATE_DAY'):
        if month == 1:
            month = 12
            year -= 1
        else:
            month -= 1
    if calendar.weekday(year, month, day) > 4:
        return True
    else:
        return False


def _get_daily_check_banzu_count(year, month, work_type_1, work_type_2,
                                 work_type_3, day):
    work_banzu = (work_type_1 + work_type_2 + work_type_3)
    if _is_weekends(year, month, int(day)):
        work_banzu -= work_type_1
    return work_banzu


def _cal_check_banzu_evenness_score(row, columns, basedata=None):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 保存中间计算过程
    calc_basic_data = {
        '日均班组检查数低于基数20%': 0,
        '日均班组检查数低于基数50%': 0,
        '日均班组检查数低于基数100%': 0,
    }
    score = [100]
    for day in columns:
        # 取某天的某个专业的班组数
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        # 取某天的某个专业的平均检查数
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        if basedata is not None:
            major_avg = float(basedata['AVG_NUMBER'][basedata['DAY'] == day])
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
            calc_basic_data.update({
                '日均班组检查数低于基数100%':
                    calc_basic_data.get('日均班组检查数低于基数100%') + 1
            })
        elif ratio <= -0.5:
            daily_deduction = -2
            calc_basic_data.update({
                '日均班组检查数低于基数50%':
                    calc_basic_data.get('日均班组检查数低于基数50%') + 1
            })
        elif ratio <= -0.2:
            daily_deduction = -1
            calc_basic_data.update({
                '日均班组检查数低于基数20%':
                    calc_basic_data.get('日均班组检查数低于基数20%') + 1
            })
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {v}天' for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _cal_check_banzu_evenness_score_new(
        row, columns, basedata=None, day_content=None,
        deduct_dict=None):
    """
    基准值（站段级）=Σ日检查次数/Σ日作业班组数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算站段基准值:
    if deduct_dict is None:
        deduct_dict = {
            0.2: 1,
            0.5: 2,
            1: 3
        }
    _key_list = list(deduct_dict.keys())
    # 默认升序排列符合渲染格式
    _key_list.sort()
    total_check_count = sum([row[day] for day in columns])
    total_banzu_count = sum(
        [row[f'daily_banzu_count_{day}'] for day in columns])
    avg_check_count = total_check_count / total_banzu_count

    # 保存中间计算过程
    # 这里沿用升序（0.2，0.5，1）
    day_basic_data = {
        f'检查数低于基数{_key_list[0] * 100}%的日期': 0,
        f'检查数低于基数{_key_list[1] * 100}%的日期': 0,
        f'检查数低于基数{_key_list[2] * 100}%的日期': 0
    }
    calc_basic_data = {
        f'检查数低于基数{_key_list[0] * 100}%的日期': [],
        f'检查数低于基数{_key_list[1] * 100}%的日期': [],
        f'检查数低于基数{_key_list[2] * 100}%的日期': []
    }
    score = [100]
    for day in columns:
        # 取某天站段的班组数及检查数
        day_banzu_count = row[f'daily_banzu_count_{day}']
        day_check_count = row[day]
        # 比较值(基数)
        base_check_count = day_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (day_check_count - base_check_count) / base_check_count
        # 这里沿用降序的字典键默认（1，0.5，0.2）
        if ratio <= -_key_list[2]:
            daily_deduction = - deduct_dict[_key_list[2]]
            deduct_type = f'检查数低于基数{_key_list[2] * 100}%的日期'
        elif ratio <= -_key_list[1]:
            daily_deduction = - deduct_dict[_key_list[1]]
            deduct_type = f'检查数低于基数{_key_list[1] * 100}%的日期'
        elif ratio <= -_key_list[0]:
            daily_deduction = - deduct_dict[_key_list[0]]
            deduct_type = f'检查数低于基数{_key_list[0] * 100}%的日期'
        else:
            daily_deduction = 0
            deduct_type = None
        score.append(daily_deduction)
        deduct_day = calc_basic_data.get(deduct_type)
        if not deduct_type:
            continue
        if not day_content:
            day_content = '{0}号[基准值：{1:.3f},班组数：{2:.0f},比较值：{3:.3f},实际检查值：{4:.0f}]'
        deduct_day.append(
            day_content.format(
                day, avg_check_count, day_banzu_count,
                base_check_count, day_check_count))
        # deduct_day.append(f'{day}号[基准值：{avg_check_count:.3f},' +
        #                   f'班组数：{day_banzu_count:.0f}, ' +
        #                   f'比较值：{base_check_count:.3f}，' +
        #                   f'实际检查值：{day_check_count:.0f}]')
        calc_basic_data.update({deduct_type: deduct_day})
        day_basic_data.update(
            {deduct_type: day_basic_data.get(deduct_type) + 1})

    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {day_basic_data.get(k)}天<br/>{"<br/>".join(v)}'
         for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _cal_check_banzu_evenness_score_fourth(row, columns, basedata=None,
                                           day_content=None,
                                           deduct_dict=None):
    """
    基准值（站段级）=Σ日检查次数/Σ日作业班组数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算站段基准值:
    if deduct_dict is None:
        deduct_dict = {
            0.2: 1,
            0.5: 2,
            1: 3
        }
    _key_list = list(deduct_dict.keys())
    # 默认升序排列符合渲染格式
    _key_list.sort()
    total_check_count = sum([row[day] for day in columns])

    # 保存中间计算过程
    # 这里沿用升序（0.2，0.5，1）
    day_basic_data = {
        '检查数低于检查总次数1%的日期': 0,
        '检查数低于检查总次数0.5%的日期': 0,
        '无检查的日期': 0,
    }
    calc_basic_data = {
        '检查数低于检查总次数1%的日期': [],
        '检查数低于检查总次数0.5%的日期': [],
        '无检查的日期': [],
    }
    score = [100]
    for day in columns:
        # 取某天站段的班组数及检查数
        day_check_count = row[day]
        ratio = day_check_count / total_check_count
        # 这里沿用降序的字典键默认（1，0.5，0.2）
        if ratio <= (1 / 100):
            daily_deduction = - 1
            deduct_type = '检查数低于检查总次数1%的日期'
        elif 0 < ratio <= (0.5 / 100):
            daily_deduction = -2
            deduct_type = '检查数低于检查总次数0.5%的日期'
        elif ratio == 0:
            daily_deduction = -4
            deduct_type = '无检查的日期'
        else:
            daily_deduction = 0
            deduct_type = None
        score.append(daily_deduction)
        deduct_day = calc_basic_data.get(deduct_type)
        if not deduct_type:
            continue
        if not day_content:
            day_content = \
                '{0}号[检查总次数：{1:.3f},实际检查值：{3:.0f}, 检查占比：{2:.4f}]'
        deduct_day.append(
            day_content.format(day, total_check_count, ratio, day_check_count))
        calc_basic_data.update({deduct_type: deduct_day})
        day_basic_data.update(
            {deduct_type: day_basic_data.get(deduct_type) + 1})

    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {day_basic_data.get(k)}天<br/>{"<br/>".join(v)}'
         for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _handle_cross_hour(data, year, month):
    """1.检查时段组成部分，根据每次检查的跨越时段，从0点开始，每两个小时算一个时段，
    计算从检查开始到检查结束的区间，其中每个时段都算在这个时段内的一次检查。
    即：如果从11:00至15:00的检查，
    那么就记为10:00~12:00、12:00~14:00、14:00~16:00各一次检查
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, START_HOUR, END_HOUR, COUNT]
    """
    if month == 1:
        last_month = 12
        last_year = year - 1
    else:
        last_month = month - 1
        last_year = year
    hour_data = []
    for idx, row in data.iterrows():
        start_day = int(row['START_HOUR'][5:7])
        end_day = int(row['END_HOUR'][5:7])
        start_hour = int(row['START_HOUR'][11:])
        end_hour = int(row['END_HOUR'][11:])
        for hour in _calc_between_hours(start_hour, end_hour):
            hour_data.append([row['FK_DEPARTMENT_ID'], hour, row['COUNT']])
        if end_day > start_day:
            # 开始结束日期不在同一天，而且不跨月
            # 再加上中间间隔的天数
            interval_days = (end_day - start_day)
        elif end_day < start_day:
            # 开始时间结束日期不在同一天，而且跨月
            # 再加上中间间隔的天数
            this_month_days = calendar.monthrange(last_year, last_month)[1]
            interval_days = (this_month_days - start_day) + end_day
        else:
            continue
        if start_hour > end_hour:
            interval_days -= 1
        # 计算中间间隔天数的时段
        for hour in range(1, 13):
            hour_data.append(
                [row['FK_DEPARTMENT_ID'], hour, interval_days * row['COUNT']])
    hour_data_df = pd.DataFrame(
        data=hour_data, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return hour_data_df


def _calc_banzu_count_by_hour(data):
    """班组数的统计是按照不同班组不同的工作时间来计算，且倒班班组数记为：倒班班组/倒班数

    增加描述：统计班组时，若存在多个工作制，24小时工作制>自定义（两小时一间隔）>日勤，
    （24小时-每天算几分之一班组[优先最大班制]，自定义-按时段计算，日勤-工作日上班, 上班时间）,
    上班时间为（8:00-12:00， 2:00-5:30）
    WORK_TIME: 24小时制度（2:二班倒、3:三班倒，1：四班倒）  自定义（两小时一个间隔）
    4、00:00 ~ 02:00   5、02:00~04:00   6、04:00~06:00 7、06:00~08:00
    8、08:00~10:00  9、10:00~12:00  10、12:00~14:00  11、14:00~16:00
    12、16:00~18:00  13、18:00~20:00   14、20:00~22:00  15、22:00~00:00
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, WORK_TYPE, WORK_TIME]
    """
    banzu_count = []
    for dpid, group in data.groupby(['FK_DEPARTMENT_ID']):
        # 计算每个班组在哪些时段有过工作
        work_type_list = group['WORK_TYPE'].values
        if 2 in work_type_list:
            work_time = group[group['WORK_TYPE'] == 2]['WORK_TIME'].values
            work_time = 1 / max([int(x) for x in work_time])
            work_unit = [[dpid, hour, work_time] for hour in range(1, 13)]
        else:
            # 保存日勤制和自定义工作的工作时段集合
            hour_list = []
            for each in group.values:
                if 3 == each[1]:
                    hour_list.extend([int(x) - 3 for x in each[2].split(',')])
                if 1 == each[1]:
                    hour_list.extend([5, 6, 8, 9])
                    if 3 not in work_type_list:
                        break
            work_unit = [[dpid, hour, 1] for hour in set(hour_list)]
        banzu_count.extend(work_unit)
    banzu_count_df = pd.DataFrame(
        data=banzu_count, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return banzu_count_df


def _cal_deduct_score_by_hour(row, columns, hour_content=None, deduct_dict=None):
    """计算逻辑：
    基准值（站段级）=Σ时段检查次数/Σ时段作业班组数
    应检查值=该时段作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣2分/日，低于50%的扣4分/日，
    低于100%的扣8分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/时段和次数/时段]]
        columns {[list]} -- [所有日期列]]
    """
    if deduct_dict is None:
        deduct_dict = {
            0.5: 4,
            1: 3,
            0.2: 2,
        }
    # 计算基准值:
    total_check_count = sum([row[hour] for hour in columns])
    total_banzu_count = sum([row[f'banzu_count_{hour}'] for hour in columns])
    avg_check_count = total_check_count / total_banzu_count

    # 保存中间计算过程
    unfinished_hour = {
        '检查数低于比较值20%的时段': [],
        '检查数低于比较值50%的时段': [],
        '检查数低于比较值100%的时段': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for hour in columns:
        hour_banzu_count = row[f'banzu_count_{hour}']  # 该时段班组数
        hour_check_count = row[hour]  # 该时段检查数
        base_check_count = hour_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (hour_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            hour_deduction = 0
        else:
            if ratio <= -1:
                hour_deduction = -deduct_dict[1]
                deduct_type = '检查数低于比较值100%的时段'
            elif -1 < ratio <= -0.5:
                hour_deduction = -deduct_dict[0.5]
                deduct_type = '检查数低于比较值50%的时段'
            else:
                hour_deduction = -deduct_dict[0.2]
                deduct_type = '检查数低于比较值20%的时段'
            deduct_hour = unfinished_hour.get(deduct_type)
            hour_title = f'{(hour - 1) * 2} - {hour * 2}时'
            if not hour_content:
                hour_content = '{0}[基准值：{1:.1f},班组数：{2:.1f},比较值：{3:.1f},实际检查值：{4:.1f}]'
            deduct_hour.append(hour_content.format(
                hour_title, avg_check_count,
                hour_banzu_count, base_check_count, hour_check_count))
            unfinished_hour.update({deduct_type: deduct_hour})
        score.append(hour_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_hour.items()])
    return total_score, rst_calc_data


def _cal_deduct_score_by_hour_fourth(
        row, columns, hour_content=None, deduct_dict=None):
    """计算逻辑：
    0-2点，2-4点，4-6点检查次数分别占总检查次数0.7%以下0.3%以上的扣2分；
    分别占总检查次数0.3%以下0%以上的扣4分；无检查的扣8分。
    6-8点检查次数占总检查次数2%以下1%以上的扣2分；
    占总检查次数1%以下0%以上的扣4分；无检查的扣8分。
    8-10点，10-12点，12-14点，14-16点，16-18,18-20点
    检查次数分别占总检查次数7%以下3%以上的扣2分；
    分别占总检查次数3%以下0%以上的扣4分；无检查的扣8分。
    20-22点检查次数占总检查次数3%以下1%以上的扣2分；
    占总检查次数1%以下0%以上的扣4分；无检查的扣8分。
    22-24点检查次数占总检查次数2%以下1%以上的扣2分；
    占总检查次数1%以下0%以上的扣4分；无检查的扣8分。

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/时段和次数/时段]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算基准值:
    total_check_count = sum([row[hour] for hour in columns])

    # 保存中间计算过程
    unfinished_hour = {
        '0-2点，2-4点，4-6点检查信息': [],
        '6-8点检查信息': [],
        '8-10点，10-12点，12-14点，14-16点，16-18,18-20点检查信息': [],
        '20-22点检查信息': [],
        '22-24点检查信息': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for hour in columns:
        hour_check_count = row[hour]  # 该时段检查数
        ratio = hour_check_count / total_check_count
        hour_deduction = 0
        if hour in (1, 2, 3):
            if (0.7 / 100) > ratio >= (0.3 / 100):
                hour_deduction = -2
            elif (0.3 / 100) > ratio > 0:
                hour_deduction = -4
            elif ratio == 0:
                hour_deduction = -8
            deduct_type = '0-2点，2-4点，4-6点检查信息'
        elif hour in (4,):
            if (2 / 100) > ratio >= (1 / 100):
                hour_deduction = -2
            elif (1 / 100) > ratio > 0:
                hour_deduction = -4
            elif ratio == 0:
                hour_deduction = -8
            deduct_type = '6-8点检查信息'
        elif hour in (5, 6, 7, 8, 9, 10):
            if (7 / 100) > ratio >= (3 / 100):
                hour_deduction = -2
            elif (3 / 100) > ratio > 0:
                hour_deduction = -4
            elif ratio == 0:
                hour_deduction = -8
            deduct_type = '8-10点，10-12点，12-14点，14-16点，16-18,18-20点检查信息'
        elif hour in (11,):
            if (3 / 100) > ratio >= (1 / 100):
                hour_deduction = -2
            elif (1 / 100) > ratio > 0:
                hour_deduction = -4
            elif ratio == 0:
                hour_deduction = -8
            deduct_type = '20-22点检查信息'
        else:
            if (2 / 100) > ratio >= (1 / 100):
                hour_deduction = -2
            elif (1 / 100) > ratio > 0:
                hour_deduction = -4
            elif ratio == 0:
                hour_deduction = -8
            deduct_type = '22-24点检查信息'
        deduct_hour = unfinished_hour.get(deduct_type)
        hour_title = f'{(hour - 1) * 2} - {hour * 2}时'
        if not hour_content:
            hour_content = \
                '{0}[总检查次数：{1:.1f},实际检查值：{3:.1f}, 检查占比：{2:.4f}]'
        deduct_hour.append(hour_content.format(
            hour_title, total_check_count, ratio, hour_check_count))
        unfinished_hour.update({deduct_type: deduct_hour})
        score.append(hour_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_hour.items()])
    return total_score, rst_calc_data


def _export_calc_address_evenness_data(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        # 保存各部门的专业
        dpid = row['TYPE3']
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if row['AVG_COUNT'] == 0:
            continue
        if int(row['COUNT']) == 0:
            cnt.update({'未检查地点数：': cnt.get('未检查地点数：', 0) + 1})
        else:
            _ratio = (row['COUNT'] - row['AVG_COUNT']) / row['AVG_COUNT']
            if _ratio >= 4:
                cnt.update({
                    '受检地点超过平均值400%以上地点数：':
                        cnt.get('受检地点超过平均值400%以上地点数：', 0) + 1
                })
            elif _ratio <= -0.5:
                cnt.update({
                    '受检地点低于基数50%地点数：': cnt.get(
                        '受检地点低于基数50%地点数：', 0) + 1
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}个' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


def _export_calc_address_evenness_data_chewu_laoan(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if row['AVG_COUNT'] == 0:
            continue
        if int(row['COUNT']) == 0:
            cnt.update({'未检查地点数：': cnt.get('未检查地点数：', 0) + 1})
        else:
            _ratio = (row['COUNT'] - row['AVG_COUNT']) / row['AVG_COUNT']
            if _ratio >= 6:
                cnt.update({
                    '受检地点超过平均值600%以上地点数：':
                        cnt.get('受检地点超过平均值600%以上地点数：', 0) + 1
                })
            elif _ratio <= -0.6:
                cnt.update({
                    '受检地点低于基数60%地点数：': cnt.get('受检地点低于基数60%地点数：', 0) + 1
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}个' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


def _export_calc_address_evenness_data_new(
        data, months_ago, risk_type, weight_list=None, content=None):
    """
    将检查地点数据中间统计结果导出
        显示详细的地点信息
    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    :param data: data {pandas.DataFrame} -- 统计数据
    :param months_ago: {int} -- 前第 -N 个月
    :param risk_type: 指数类型
    :param weight_list: {list} 第一个为大于的比较率,第二为小与的比较率
    :return:
    """
    calc_data = {}
    major_data = {}
    high_ratio = weight_list[0] if weight_list else 4
    low_ratio = weight_list[1] if weight_list else -0.5
    address_count_dict = {
        '总检查地点数:': 0,
        '受检地点超过比较值{}00%以上地点:'.format(high_ratio): 0,
        '受检地点低于比较值{0:.0f}0%以上地点:'.format(low_ratio * -10): 0,
        '未检查地点:': 0
    }
    dpid_address_count = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['CHECK_COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        person_number = row['ADDRESS_PERSON_NUMBER']
        point_name = row['ADDRESS_NAME']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)

        if dpid not in dpid_address_count:
            dpid_address_count.update({dpid: address_count_dict.copy()})
        address_count = dpid_address_count.get(dpid)
        address_count.update(
            {'总检查地点数:': address_count.get('总检查地点数:', 0) + 1})

        # 站段无检查地点
        if isinstance(point_name, int):
            address_count.update({'总检查地点数:': 0})
            calc_data.update({dpid: cnt})
            continue

        if not content:
            content = '{0}(比较值：{1:.0f}, 受检次数：{2:.0f}, 基准值：{3:.3f},工作人数：{4:.0f})'
        check_desc = content.format(
            point_name, avg_check_count, real_check_count,
            base_check_count, person_number)

        _ratio = ((real_check_count - avg_check_count)
                  / avg_check_count) if avg_check_count > 0 else 0
        if int(real_check_count) == 0:
            deduct_type = '未检查地点:'
        elif _ratio >= high_ratio:
            deduct_type = '受检地点超过比较值{}00%以上地点:'.format(high_ratio)
        elif _ratio <= low_ratio:
            deduct_type = '受检地点低于比较值{0:.0f}0%以上地点:'.format(low_ratio * -10)
        else:
            dpid_address_count.update({dpid: address_count})
            calc_data.update({dpid: cnt})
            continue
        cnt.update({deduct_type: cnt.get(
            deduct_type, '') + '<br/>' + check_desc})
        address_count.update(
            {deduct_type: address_count.get(deduct_type, 0) + 1})
        calc_data.update({dpid: cnt})
        dpid_address_count.update({dpid: address_count})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
            # ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
                ';<br/>'.join(
                    [f'总检查地点数: {dpid_address_count.get(dpid).get("总检查地点数:")}个'] +
                    [f'{k} {dpid_address_count.get(dpid).get(k)}个{v}'
                     for k, v in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


# 覆盖率
def stats_check_address_ratio(real_check_banzu,
                              real_check_point,
                              banzu_point,
                              check_point,
                              department_data,
                              months_ago,
                              risk_type,
                              risk_name,
                              choose_dpid_data=None,
                              customizecontent=None):
    """（检查地点数/作业班组数)*100
    """
    # 检查地点数
    data_real = pd.concat(
        [real_check_banzu, real_check_point],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, department_data)
    # 地点总数
    data_total = pd.concat(
        [banzu_point, check_point],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, department_data)

    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        4,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        lambda x: x * 100,
        choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 问题均衡度
def stats_problem_point_evenness(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count, months_ago,
        risk_type, choose_dpid_data,
        customizecontent=None,
        calc_score_by_formula=lambda x: min(100, x * 100),
        is_calc_score_base_major=False):
    rst_index_score = calc_child_index_type_divide(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        2,
        4,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent)
    return rst_index_score


# 问题均衡度
def stats_problem_point_evenness_major(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        months_ago,
        risk_type, choose_dpid_data,
        customizecontent=None,
        calc_score_by_formula=lambda x: min(100, x * 100),
        is_calc_score_base_major=False, zhanduan_filter_list=[],
        fraction=None):
    """
     [计算时跟专业基数比较，
        与其他方式不同为专业基数为总值A/总值B,
        其他方式为A的各个小项/B的各个小项得到小c，
        再求出每个c平均值]

        Arguments:
            generally_above_problem_point_count {[type]} -- [description]
            generally_above_problem_point_in_problem_base_count {[type]}
             -- [description]
            months_ago {[type]} -- [description]
            risk_type {[type]} -- [description]
            choose_dpid_data {[type]} -- [description]

        Keyword Arguments:
            customizecontent {[type]} --
            [description] (default: {None})
            calc_score_by_formula {[type]} --
            [description] (default: {lambdax:min(100, x * 100)})
            is_calc_score_base_major {bool} -- [description] (default: {False})
            zhanduan_filter_list {list} -- [description] (default: {[]})
    """
    rst_index_score = calc_child_index_type_divide_major(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        2,
        4,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        is_calc_score_base_major=is_calc_score_base_major,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction)
    return rst_index_score


# 检查时间均衡度
def stats_check_time_evenness(check_item_ids, daily_check_banzu_count_sql,
                              daily_check_count_sql, department_data,
                              zhanduan_dpid_data, months_ago, risk_type,
                              choose_dpid_data, basedata=None):
    """修改了_cal_check_banzu_evenness_score_new,显示详细时间信息"""
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        # 每日检查班组数
        data = df_merge_with_dpid(
            pd_query(daily_check_banzu_count_sql.format(check_item_ids)),
            department_data)
        # WORK_TYPE为空时填充默认工作制1
        data.fillna(1, inplace=True)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        xdata = xdata.unstack()
        for x in [1, 2, 3]:
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        xdata = xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        for day in columns:
            new_column = f'daily_banzu_count_{day}'
            xdata[new_column] = xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查时间均衡度(日期)
def stats_check_time_evenness_df(daily_check_banzu_count,
                                 daily_check_count, department_data,
                                 zhanduan_dpid_data, months_ago, risk_type,
                                 choose_dpid_data, basedata=None):
    """修改了_cal_check_banzu_evenness_score_new,显示详细时间信息, DataFrame"""
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        # 每日检查班组数
        data = df_merge_with_dpid(daily_check_banzu_count, department_data)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        xdata = xdata.unstack()
        for x in [1, 2, 3]:
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        xdata = xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(daily_check_count, department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        for day in columns:
            new_column = f'daily_banzu_count_{day}'
            xdata[new_column] = xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查时间均衡度 - 检查时段均衡度---无引用
def stats_check_hour_evenness(check_item_ids, daily_check_banzu_count_sql,
                              hour_check_count_sql, department_data,
                              zhanduan_dpid_data,
                              months_ago, risk_type, choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        banzu_count = _calc_banzu_count_by_hour(
            pd_query(daily_check_banzu_count_sql.format(check_item_ids)))
        data = df_merge_with_dpid(banzu_count, department_data)
        if data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])['COUNT'].sum()
        xdata = xdata.unstack()
        xdata = xdata.fillna(0)
        # 重命名列名
        xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        data_check = df_merge_with_dpid(
            _handle_cross_hour(
                pd_query(hour_check_count_sql.format(
                    *stats_month, check_item_ids)), year,
                month), department_data)
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'WORK_HOUR'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)
        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)

        column = f'SCORE_d_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[0], axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[1], axis=1)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), 4, 5, 3,
                                                       months_ago,
                                                       risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         5, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 5, months_ago,
                               risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查时间均衡度---日期加时间段
def stats_check_time_evenness_three(
        check_item_ids, daily_check_banzu_count_sql,
        daily_check_count_sql, daily_check_banzu_count_h_sql,
        hour_check_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None, item_weight=[0.5, 0.5],
        time_deduct_dict=None, date_deduct_dict=None):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    calc_basic_data = []
    for hierarchy in [3]:
        # 检查时段
        banzu_count = _calc_banzu_count_by_hour(
            pd_query(daily_check_banzu_count_h_sql.format(check_item_ids)))
        hour_data = df_merge_with_dpid(banzu_count, department_data)
        if hour_data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        hour_xdata = hour_data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])[
            'COUNT'].sum()
        hour_xdata = hour_xdata.unstack()
        hour_xdata = hour_xdata.fillna(0)
        # 重命名列名
        hour_xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        hour_data_check = df_merge_with_dpid(
            _handle_cross_hour(
                pd_query(hour_check_count_sql.format(
                    *stats_month, check_item_ids)), year,
                month), department_data)
        hour_xdata_check = hour_data_check.groupby([f'TYPE{hierarchy}',
                                                    'WORK_HOUR'])['COUNT'].sum()
        hour_xdata_check = hour_xdata_check.unstack()
        hour_xdata_check = hour_xdata_check.fillna(0)
        columns = hour_xdata_check.columns.values
        hour_xdata = pd.merge(
            hour_xdata, hour_xdata_check, how='left',
            left_index=True, right_index=True)

        hour_xdata['SCORE_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(
                row, columns, deduct_dict=time_deduct_dict)[0], axis=1)

        hour_xdata['CONTENT_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(
                row, columns, deduct_dict=time_deduct_dict)[1], axis=1)
        hour_xdata = append_major_column_to_df(zhanduan_dpid_data, hour_xdata)
        hour_xdata.drop(['MAJOR'], inplace=True, axis=1)
        str_tmp = "检查数低于比较值20%的时段: <br/> 检查数低于比较值50%的时段: <br/> 检查数低于比较值100%的时段: <br/>"
        hour_xdata['SCORE_HOUR'].fillna(0, inplace=True)
        hour_xdata['CONTENT_HOUR'].fillna(str_tmp, inplace=True)

        # 检查日期
        # 每日检查班组数
        day_data = df_merge_with_dpid(
            pd_query(daily_check_banzu_count_sql.format(check_item_ids)),
            department_data)
        if day_data.empty:
            continue
        day_xdata = day_data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        day_xdata = day_xdata.unstack()
        for x in [1, 2, 3]:
            if x not in day_xdata.columns.values.tolist():
                day_xdata[x] = 0
        day_xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        day_xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        day_xdata = day_xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        day_columns = xdata_check.columns.values
        day_xdata = pd.merge(
            day_xdata, xdata_check, how='left',
            left_index=True, right_index=True)
        day_xdata = append_major_column_to_df(zhanduan_dpid_data, day_xdata)
        for day in day_columns:
            new_column = f'daily_banzu_count_{day}'
            day_xdata[new_column] = day_xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        day_xdata['SCORE_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[0],
            axis=1)
        day_xdata['CONTENT_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[1],
            axis=1)

        # 聚合检查日期和检查时间
        column = f'SCORE_b_{hierarchy}'
        xdata = pd.merge(hour_xdata, day_xdata, how="right",
                         left_on="DEPARTMENT_ID", right_on="DEPARTMENT_ID")
        xdata['CONTENT'] = xdata.apply(
            lambda row: '<br/>'.join(
                [row['CONTENT_HOUR'], row['CONTENT_DAY']]), axis=1)
        xdata[column] = xdata.apply(
            lambda row:
            int(row['SCORE_HOUR'] * item_weight[0]) + int(
                row['SCORE_DAY'] * item_weight[1]), axis=1)

        # xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), 4, 2, 3,
                                                       months_ago,
                                                       risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 2, months_ago,
                               risk_type=risk_type)
        rst_index_score.append(xdata)

    return rst_index_score


# 检查时间均衡度---日期加时间段
def stats_check_time_evenness_three_df(
        daily_check_banzu_count,
        daily_check_count, daily_check_banzu_count_h, hour_check_count,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None, item_weight=[0.5, 0.5],
        time_deduct_dict=None, date_deduct_dict=None):
    """DataFrame"""
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    calc_basic_data = []
    for hierarchy in [3]:
        # 检查时段
        banzu_count = _calc_banzu_count_by_hour(daily_check_banzu_count_h)
        hour_data = df_merge_with_dpid(banzu_count, department_data)
        if hour_data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        hour_xdata = hour_data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])[
            'COUNT'].sum()
        hour_xdata = hour_xdata.unstack()
        hour_xdata = hour_xdata.fillna(0)
        # 重命名列名
        hour_xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        hour_data_check = df_merge_with_dpid(
            _handle_cross_hour(hour_check_count, year, month), department_data)
        hour_xdata_check = hour_data_check.groupby([f'TYPE{hierarchy}',
                                                    'WORK_HOUR'])['COUNT'].sum()
        hour_xdata_check = hour_xdata_check.unstack()
        hour_xdata_check = hour_xdata_check.fillna(0)
        columns = hour_xdata_check.columns.values
        hour_xdata = pd.merge(
            hour_xdata, hour_xdata_check,
            how='left', left_index=True, right_index=True)

        hour_xdata['SCORE_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(
                row, columns, deduct_dict=time_deduct_dict)[0], axis=1)

        hour_xdata['CONTENT_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(
                row, columns, deduct_dict=time_deduct_dict)[1], axis=1)
        hour_xdata = append_major_column_to_df(zhanduan_dpid_data, hour_xdata)
        hour_xdata.drop(['MAJOR'], inplace=True, axis=1)
        str_tmp = "检查数低于比较值20%的时段: <br/> 检查数低于比较值50%的时段: <br/> 检查数低于比较值100%的时段: <br/>"
        hour_xdata['SCORE_HOUR'].fillna(0, inplace=True)
        hour_xdata['CONTENT_HOUR'].fillna(str_tmp, inplace=True)

        # 检查日期
        # 每日检查班组数
        day_data = df_merge_with_dpid(
            daily_check_banzu_count,
            department_data)
        if day_data.empty:
            continue
        day_xdata = day_data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        day_xdata = day_xdata.unstack()
        for x in [1, 2, 3]:
            if x not in day_xdata.columns.values.tolist():
                day_xdata[x] = 0
        day_xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        day_xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        day_xdata = day_xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            daily_check_count,
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        day_columns = xdata_check.columns.values
        day_xdata = pd.merge(
            day_xdata, xdata_check, how='left', left_index=True, right_index=True)
        day_xdata = append_major_column_to_df(zhanduan_dpid_data, day_xdata)
        for day in day_columns:
            new_column = f'daily_banzu_count_{day}'
            day_xdata[new_column] = day_xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        day_xdata['SCORE_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[0],
            axis=1)
        day_xdata['CONTENT_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[1],
            axis=1)

        # 聚合检查日期和检查时间
        column = f'SCORE_b_{hierarchy}'
        xdata = pd.merge(hour_xdata, day_xdata, how="right",
                         left_on="DEPARTMENT_ID", right_on="DEPARTMENT_ID")
        xdata['CONTENT'] = xdata.apply(
            lambda row: '<br/>'.join([row['CONTENT_HOUR'], row['CONTENT_DAY']]), axis=1)
        xdata[column] = xdata.apply(
            lambda row: int(row['SCORE_HOUR'] * item_weight[0]) + int(row['SCORE_DAY'] * item_weight[1]), axis=1)

        # xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), 4, 2, 3,
                                                       months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 2, months_ago, risk_type=risk_type)
        rst_index_score.append(xdata)

    return rst_index_score


# 检查时间均衡度--每日检查人次数/当日人时---无引用
def stats_check_time_evenness_labortime(check_item_ids, daily_check_hour_sql,
                                        daily_check_count_sql, department_data,
                                        zhanduan_dpid_data, months_ago, risk_type,
                                        choose_dpid_data, basedata=None):
    stats_month = get_custom_month(months_ago)
    rst_index_score = []
    for hierarchy in [3]:
        # 每日人时
        data = df_merge_with_dpid(
            pd_query(daily_check_hour_sql.format(
                *stats_month), db_name='db_mid'),
            department_data)
        plus_time_dict = _distribute_labor_time(data, stats_month, hierarchy)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'DAY']).size()
        xdata = xdata.unstack()
        for x in range(1, 32):
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[x for x in range(1, 32)], inplace=True)
        origin_columns = xdata.columns.values
        for col in origin_columns:
            new_column = 'daily_check_banzu_count_{}'.format(col)
            xdata[new_column] = xdata.apply(
                lambda row: row[col],
                axis=1)
        xdata = xdata.drop(columns=origin_columns)
        xdata = xdata.fillna(0)
        # 跨天的作业时间，除了完成当天以外的，在聚合之后再补充回去
        for k, v in plus_time_dict.items():
            t, d = k.split('_')
            col_name = 'daily_check_banzu_count_{}'.format(d)
            if t in xdata.index.values and col_name in xdata.columns.values:
                xdata.loc[t, col_name] = xdata.loc[t, col_name] + v

        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        major_banzu_count = xdata.groupby(['MAJOR']).sum()
        xdata = pd.merge(
            xdata,
            major_banzu_count,
            how='left',
            left_on='MAJOR',
            right_index=True)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(
                row, columns, basedata)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(
                row, columns, basedata)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查时间均衡度--每日检查人次数/当日人时 - 改sql为data
def stats_check_time_evenness_labortime_excellent(
        daily_check_hour_data,
        daily_check_count_data, department_data,
        zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None,
        day_content='{0}号[基准值：{1:.3f},当日人时：{2:.0f},比较值：{3:.3f},实际检查值：{4:.0f}]',
        deduct_dict=None):
    stats_month = get_custom_month(months_ago)
    rst_index_score = []
    for hierarchy in [3]:
        # 每日人时
        data = daily_check_hour_data
        plus_time_dict = _distribute_labor_time(data, stats_month, hierarchy)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'DAY']).size()
        xdata = xdata.unstack()
        for x in range(1, 32):
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[x for x in range(1, 32)], inplace=True)
        origin_columns = xdata.columns.values
        for col in origin_columns:
            new_column = 'daily_banzu_count_{}'.format(col)
            xdata[new_column] = xdata.apply(
                lambda row: row[col],
                axis=1)
        xdata = xdata.drop(columns=origin_columns)
        xdata = xdata.fillna(0)
        # 跨天的作业时间，除了完成当天以外的，在聚合之后再补充回去
        for k, v in plus_time_dict.items():
            t, d = k.split('_')
            col_name = 'daily_banzu_count_{}'.format(d)
            if t in xdata.index.values and col_name in xdata.columns.values:
                xdata.loc[t, col_name] = xdata.loc[t, col_name] + v

        # 每日检查数
        data_check = daily_check_count_data
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        # major_banzu_count = xdata.groupby(['MAJOR']).sum()
        # xdata = pd.merge(
        #     xdata,
        #     major_banzu_count,
        #     how='left',
        #     left_on='MAJOR',
        #     right_index=True)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata, day_content=day_content,
                deduct_dict=deduct_dict)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, columns, basedata, day_content=day_content,
                deduct_dict=deduct_dict)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查时间均衡度---日期加时间段
def stats_check_time_evenness_three_wordload(
        check_item_ids, daily__work_load,
        daily_check_count_sql, hour_work_load, hour_check_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None, item_weight=[0.5, 0.5],
        hour_content=None, day_content=None, deduct_dict=None):
    """与工作量比较"""
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    calc_basic_data = []
    for hierarchy in [3]:
        # 检查时段
        # 时段工作量(分母)
        hour_data = df_merge_with_dpid(hour_work_load, department_data)
        if hour_data.empty:
            continue
        hour_xdata = hour_data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])[
            'HOUR_WORK_LOAD'].sum()
        hour_xdata = hour_xdata.unstack()
        hour_xdata = hour_xdata.fillna(0)
        # 重命名列名
        hour_xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 时段班组实际检查数(分子)
        hour_data_check = df_merge_with_dpid(
            _handle_cross_hour(
                pd_query(hour_check_count_sql.format(
                    *stats_month, check_item_ids)), year,
                month), department_data)
        hour_xdata_check = hour_data_check.groupby([f'TYPE{hierarchy}',
                                                    'WORK_HOUR'])['COUNT'].sum()
        hour_xdata_check = hour_xdata_check.unstack()
        hour_xdata_check = hour_xdata_check.fillna(0)
        columns = hour_xdata_check.columns.values
        hour_xdata = pd.merge(
            hour_xdata, hour_xdata_check, how='left', left_index=True, right_index=True)
        hour_xdata.fillna(0, inplace=True)
        hour_xdata['SCORE_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns, deduct_dict=deduct_dict)[0], axis=1)

        hour_xdata['CONTENT_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns, hour_content=hour_content)[1], axis=1)
        hour_xdata = append_major_column_to_df(zhanduan_dpid_data, hour_xdata)
        hour_xdata.drop(['MAJOR'], inplace=True, axis=1)
        str_tmp = "检查数低于比较值20%的时段: <br/> 检查数低于比较值50%的时段: <br/> 检查数低于比较值100%的时段: <br/>"
        hour_xdata['SCORE_HOUR'].fillna(0, inplace=True)
        hour_xdata['CONTENT_HOUR'].fillna(str_tmp, inplace=True)

        # 检查日期
        # 每日工作量(分母)
        day_data = df_merge_with_dpid(daily__work_load, department_data)
        if day_data.empty:
            continue
        day_xdata = day_data.groupby([f'TYPE{hierarchy}', 'WORK_DAY'])[
            'DAILY_WORK_LOAD'].sum()
        day_xdata = day_xdata.unstack()
        month_days = get_month_day(months_ago)
        for x in range(1, month_days + 1):
            if x not in day_xdata.columns.values.tolist():
                day_xdata[x] = 0
        day_xdata.dropna(how='all', inplace=True)
        day_xdata.rename(
            columns={
                day: f'daily_banzu_count_{day}' for day in range(1, month_days + 1)
            }, inplace=True)
        day_xdata = day_xdata.fillna(0)

        # 每日检查数(分子)
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        day_check_count = data_check.groupby([f'TYPE{hierarchy}',
                                              'DAY'])['COUNT'].sum()
        day_check_count = day_check_count.unstack()
        day_check_count = day_check_count.fillna(0)

        day_columns = day_check_count.columns.values
        day_xdata = pd.merge(
            day_xdata, day_check_count, how='left', left_index=True, right_index=True)
        day_xdata = append_major_column_to_df(zhanduan_dpid_data, day_xdata)

        day_xdata['SCORE_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, day_content=day_content)[0],
            axis=1)
        day_xdata['CONTENT_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_new(
                row, day_columns, basedata, day_content=day_content)[1],
            axis=1)

        # 聚合检查日期和检查时间
        column = f'SCORE_b_{hierarchy}'
        xdata = pd.merge(hour_xdata, day_xdata, how="right",
                         left_on="DEPARTMENT_ID", right_on="DEPARTMENT_ID")
        xdata['CONTENT'] = xdata.apply(
            lambda row: '<br/>'.join([row['CONTENT_HOUR'], row['CONTENT_DAY']]), axis=1)
        xdata[column] = xdata.apply(
            lambda row: int(row['SCORE_HOUR'] * item_weight[0]) + int(row['SCORE_DAY'] * item_weight[1]), axis=1)

        calc_basic_data_rst = format_export_basic_data(xdata.copy(), 4, 2, 3,
                                                       months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 2, months_ago, risk_type=risk_type)
        rst_index_score.append(xdata)

    return rst_index_score


# 检查时间均衡度---日期加时间段(细致到每个时段，日期)
def stats_check_time_evenness_fourth(
        check_item_ids,
        daily_check_count_sql, hour_check_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None, item_weight=[0.5, 0.5],
        time_deduct_dict=None, date_deduct_dict=None):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    calc_basic_data = []
    for hierarchy in [3]:
        # 检查时段
        # 如果检查的时段，2个小时算一个时段
        hour_data_check = df_merge_with_dpid(
            _handle_cross_hour(
                pd_query(hour_check_count_sql.format(
                    *stats_month, check_item_ids)), year,
                month), department_data)
        hour_xdata = hour_data_check.groupby([f'TYPE{hierarchy}',
                                              'WORK_HOUR'])['COUNT'].sum()
        hour_xdata = hour_xdata.unstack()
        hour_xdata = hour_xdata.fillna(0)
        columns = hour_xdata.columns.values

        hour_xdata['SCORE_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour_fourth(
                row, columns, deduct_dict=time_deduct_dict)[0], axis=1)

        hour_xdata['CONTENT_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour_fourth(
                row, columns, deduct_dict=time_deduct_dict)[1], axis=1)
        hour_xdata = append_major_column_to_df(zhanduan_dpid_data, hour_xdata)
        hour_xdata.drop(['MAJOR'], inplace=True, axis=1)
        str_tmp = "0-2点，2-4点，4-6点检查信息: <br/> 6-8点检查信息: <br/> +\
        8-10点，10-12点，12-14点，14-16点，16-18,18-20点检查信息: <br/> +\
        20-22点检查信息: <br/> 22-24点检查信息: <br/>"
        hour_xdata['SCORE_HOUR'].fillna(0, inplace=True)
        hour_xdata['CONTENT_HOUR'].fillna(str_tmp, inplace=True)

        # 检查日期
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        day_xdata = data_check.groupby([f'TYPE{hierarchy}',
                                        'DAY'])['COUNT'].sum()
        day_xdata = day_xdata.unstack()
        day_xdata = day_xdata.fillna(0)

        day_columns = day_xdata.columns.values
        day_xdata = append_major_column_to_df(zhanduan_dpid_data, day_xdata)
        day_xdata['SCORE_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_fourth(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[0],
            axis=1)
        day_xdata['CONTENT_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score_fourth(
                row, day_columns, basedata, deduct_dict=date_deduct_dict)[1],
            axis=1)

        # 聚合检查日期和检查时间
        column = f'SCORE_b_{hierarchy}'
        xdata = pd.merge(hour_xdata, day_xdata, how="right",
                         left_on="DEPARTMENT_ID", right_on="DEPARTMENT_ID")
        xdata['CONTENT'] = xdata.apply(
            lambda row: '<br/>'.join(
                [row['CONTENT_HOUR'], row['CONTENT_DAY']]), axis=1)
        xdata[column] = xdata.apply(
            lambda row:
            int(row['SCORE_HOUR'] * item_weight[0])
            + int(row['SCORE_DAY'] * item_weight[1]), axis=1)

        # xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3,
            months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 2, months_ago,
                               risk_type=risk_type)
        rst_index_score.append(xdata)

    return rst_index_score


# 检查地点均衡度
def stats_check_address_evenness(check_item_ids, check_banzu_count_sql,
                                 banzu_department_checked_count_sql,
                                 department_data, zhanduan_dpid_data,
                                 months_ago, risk_type, choose_dpid_data,
                                 calc_func=_calc_score_by_formula,
                                 is_deduct=True):
    stats_month = get_custom_month(months_ago)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(check_banzu_count_sql.format(check_item_ids)),
        pd_query(
            banzu_department_checked_count_sql.format(*stats_month,
                                                      check_item_ids)),
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    if data_check_banzu.empty:
        return
    data = pd.merge(
        zhanduan_dpid_data,
        data_check_banzu,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    if not is_deduct:
        df_type3_count = data['NAME'].value_counts()
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(
                row, 'COUNT', 'AVG_COUNT', 'NAME', df_type3_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(row, 'COUNT', 'AVG_COUNT'), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score


# 检查地点均衡度
def stats_check_address_evenness_excellent(
        check_banzu_count_data,
        banzu_department_checked_count_data,
        department_data, zhanduan_dpid_data,
        months_ago, risk_type, choose_dpid_data,
        calc_func=_calc_score_by_formula,
        is_deduct=True):
    # 班组受检次数
    data_check_banzu = pd.merge(
        check_banzu_count_data,
        banzu_department_checked_count_data,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    if data_check_banzu.empty:
        return
    data = pd.merge(
        zhanduan_dpid_data,
        data_check_banzu,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    if not is_deduct:
        df_type3_count = data['NAME'].value_counts()
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(
                row, 'COUNT', 'AVG_COUNT', 'NAME', df_type3_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(row, 'COUNT', 'AVG_COUNT'), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score


# 检查地点均衡度-部分专业基数
def stats_check_address_evenness_two(
        check_item_ids, check_banzu_count_sql,
        banzu_department_checked_count_sql,
        department_data, zhanduan_dpid_data,
        months_ago, risk_type, choose_dpid_data, zhanduan_filter_list=[]):
    stats_month = get_custom_month(months_ago)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(check_banzu_count_sql.format(check_item_ids)),
        pd_query(
            banzu_department_checked_count_sql.format(*stats_month,
                                                      check_item_ids)),
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    if data_check_banzu.empty:
        return
    data = pd.merge(
        zhanduan_dpid_data,
        data_check_banzu,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    if zhanduan_filter_list:
        department_name = list(data.NAME)
        for i in zhanduan_filter_list:
            if i in department_name:
                department_name.remove(i)
        df = data[data['COUNT'] > 0]
        major_avg_score = df[df.NAME.isin(department_name)].groupby(
            ['MAJOR'])['COUNT'].mean()
    else:
        major_avg_score = data[data['COUNT'] > 0].groupby(
            ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'COUNT', 'AVG_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 检查地点均衡度 使用工作量基数
def stats_check_address_evenness_work_load(
        check_item_ids, check_banzu_count_sql,
        banzu_department_checked_count_sql,
        department_data, zhanduan_dpid_data,
        months_ago, risk_type, choose_dpid_data, work_load,
        calc_func=_calc_score_by_formula,
        is_deduct=True):
    stats_month = get_custom_month(months_ago)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(check_banzu_count_sql.format(check_item_ids)),
        pd_query(
            banzu_department_checked_count_sql.format(*stats_month,
                                                      check_item_ids)),
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    if not work_load.empty:
        work_load = pd.DataFrame(work_load, columns=['DEPARTMENT_ID', 'COUNT'])
        work_load.rename(columns={'COUNT': 'WORK_LOAD'}, inplace=True)
        data_check_banzu = pd.merge(data_check_banzu, work_load, how='left',
                                    left_on='DEPARTMENT_ID',
                                    right_on='DEPARTMENT_ID')

    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    if data_check_banzu.empty:
        return

    data = pd.merge(
        zhanduan_dpid_data,
        data_check_banzu,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)

    # 计算专业工作量
    zhanduan_work_load = data.groupby('MAJOR')['WORK_LOAD'].sum()
    data = pd.merge(data,
                    zhanduan_work_load.to_frame(name='SUM_WORK_LOAD'),
                    how='left',
                    left_on='MAJOR',
                    right_index=True)
    # 计算站段检查次数
    zhanduan_check_count = data.groupby('MAJOR')['COUNT'].sum()
    data = pd.merge(data,
                    zhanduan_check_count.to_frame(name='SUM_CHECK_COUNT'),
                    how='left',
                    left_on='MAJOR',
                    right_index=True)
    # 计算基数（应该以专业为单位基数）
    data['BASE_COUNT'] = data.apply(
        lambda row: (row['SUM_CHECK_COUNT'] / row['SUM_WORK_LOAD'])
        if row['SUM_WORK_LOAD'] > 0 else 0, axis=1)
    # 计算应检查值
    data['AVG_COUNT'] = data.apply(
        lambda row: row['BASE_COUNT'] * row['WORK_LOAD'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    if not is_deduct:
        df_type3_count = data['TYPE3'].value_counts()
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: _calc_score_by_formula_workload(
                row, 'COUNT', 'AVG_COUNT', 'TYPE3', df_type3_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(row, 'COUNT', 'AVG_COUNT'), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score


# 检查地点均衡度 使用工作量基数
def stats_check_address_evenness_work_load_excellent(
        check_banzu_count_data,
        banzu_department_checked_count_data,
        department_data, zhanduan_dpid_data,
        months_ago, risk_type, choose_dpid_data, work_load,
        calc_func=_calc_score_by_formula,
        is_deduct=True):
    # 班组受检次数
    data_check_banzu = pd.merge(
        check_banzu_count_data,
        banzu_department_checked_count_data,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    if not work_load.empty:
        work_load = pd.DataFrame(work_load, columns=['DEPARTMENT_ID', 'COUNT'])
        work_load.rename(columns={'COUNT': 'WORK_LOAD'}, inplace=True)
        data_check_banzu = pd.merge(data_check_banzu, work_load, how='left',
                                    left_on='DEPARTMENT_ID',
                                    right_on='DEPARTMENT_ID')

    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    if data_check_banzu.empty:
        return

    data = pd.merge(
        zhanduan_dpid_data,
        data_check_banzu,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)

    # 计算专业工作量
    zhanduan_work_load = data.groupby('MAJOR')['WORK_LOAD'].sum()
    data = pd.merge(data,
                    zhanduan_work_load.to_frame(name='SUM_WORK_LOAD'),
                    how='left',
                    left_on='MAJOR',
                    right_index=True)
    # 计算站段检查次数
    zhanduan_check_count = data.groupby('MAJOR')['COUNT'].sum()
    data = pd.merge(data,
                    zhanduan_check_count.to_frame(name='SUM_CHECK_COUNT'),
                    how='left',
                    left_on='MAJOR',
                    right_index=True)
    # 计算基数（应该以专业为单位基数）
    data['BASE_COUNT'] = data.apply(
        lambda row: (row['SUM_CHECK_COUNT'] / row['SUM_WORK_LOAD'])
        if row['SUM_WORK_LOAD'] > 0 else 0, axis=1)
    # 计算应检查值
    data['AVG_COUNT'] = data.apply(
        lambda row: row['BASE_COUNT'] * row['WORK_LOAD'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    if not is_deduct:
        df_type3_count = data['TYPE3'].value_counts()
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: _calc_score_by_formula_workload(
                row, 'COUNT', 'AVG_COUNT', 'TYPE3', df_type3_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(row, 'COUNT', 'AVG_COUNT'), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score


# 检查地点均衡度 - 重要检查点配置
def stats_check_address_evenness_laoan(
        check_banzu_count_sql, check_point_count_sql,
        banzu_department_checked_count_sql, check_point_checked_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, check_item_ids=None, risk_ids=None):
    stats_month = get_custom_month(months_ago)
    # 重要检查点受检次数
    data_check_point = pd.merge(
        pd_query(check_point_count_sql),
        pd_query(check_point_checked_count_sql.format(
            *stats_month, check_item_ids)),
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="FK_CHECK_POINT_ID")
    data_check_point.drop(
        ["CHECK_POINT_ID", "FK_CHECK_POINT_ID"], inplace=True, axis=1)
    data_check_point.fillna(0, inplace=True)
    data_check_point = df_merge_with_dpid(
        data_check_point, department_data, how='inner')
    data_check_point.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(check_banzu_count_sql.format(risk_ids)),
        pd_query(banzu_department_checked_count_sql.format(
            *stats_month, check_item_ids)),
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(
        data_check_banzu, department_data, how='inner')
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 合并
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = pd.merge(
        zhanduan_dpid_data,
        data,
        how='right',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'COUNT', 'AVG_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 检查地点均衡度 - 重要检查点配置
def stats_check_address_evenness_laoan_chewu(
        check_banzu_count_sql, check_point_count_sql,
        banzu_department_checked_count_sql, check_point_checked_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, check_item_ids=None, risk_ids=None,
        calc_score_by_formula=_calc_score_by_formula_chewu_laoan):
    stats_month = get_custom_month(months_ago)
    # 重要检查点受检次数
    data_check_point = pd.merge(
        pd_query(check_point_count_sql),
        pd_query(check_point_checked_count_sql.format(
            *stats_month, check_item_ids)),
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="FK_CHECK_POINT_ID")
    data_check_point.drop(
        ["CHECK_POINT_ID", "FK_CHECK_POINT_ID"], inplace=True, axis=1)
    data_check_point.fillna(0, inplace=True)
    data_check_point = df_merge_with_dpid(
        data_check_point, department_data, how='inner')
    data_check_point.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(check_banzu_count_sql.format(risk_ids)),
        pd_query(banzu_department_checked_count_sql.format(
            *stats_month, check_item_ids)),
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(
        data_check_banzu, department_data, how='inner')
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 合并
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = pd.merge(
        zhanduan_dpid_data,
        data,
        how='right',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data_chewu_laoan(
        data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: calc_score_by_formula(row, 'COUNT', 'AVG_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 分配跨天的人时
def _distribute_labor_time(df, stats_month, hierarchy):
    """
    从数据库中查询出的派单完成记录，如果结束时间与开始时间跨天，那么结束时间当天的作业时间只从零点开始算，
    从开始时间到结束时间当天零点的剩余作业时间则每天计算然后累积到以hierarchy与日为索引的字典中，在原函数
    中聚合以后再补充回去
    :param df:
    :param stats_month:
    :return:
    """
    plus_time_dict = {}
    for i in range(0, len(df)):
        start = datetime.datetime.strptime(df.loc[i]['ST'], '%Y-%m-%d %H:%M')
        end = datetime.datetime.strptime(df.loc[i]['ET'], '%Y-%m-%d %H:%M')
        total_delta_seconds = (end - start).total_seconds()
        if start.date() < end.date():
            midnight = datetime.datetime(
                year=end.year, month=end.month, day=end.day, minute=0, second=0)
            seconds = (end - midnight).total_seconds()
            df.loc[i, 'HOURS'] = (seconds / 3600) * \
                                 df.loc[i]['ON_RAILWAY_NUMBER']
            total_delta_seconds -= seconds
            pre_date = end
            while total_delta_seconds > 0 and pre_date.date() >= \
                    datetime.datetime.strptime(stats_month[0], '%Y-%m-%d').date():
                midnight = datetime.datetime(
                    year=pre_date.year, month=pre_date.month,
                    day=pre_date.day, minute=0, second=0)
                ts = (midnight - start).total_seconds()
                ts = 86400 if ts >= 86400 else ts
                pre_date = pre_date - datetime.timedelta(days=1)
                key = '{}_{}'.format(
                    df.loc[i]['TYPE{}'.format(hierarchy)], pre_date.day)
                plus_time_dict[key] = plus_time_dict.get(
                    key, 0) + (ts / 3600) * df.loc[i]['ON_RAILWAY_NUMBER']
                total_delta_seconds -= ts
    return plus_time_dict


# 检查地点均衡度 - 拆解班组受检次数查询（优化查询速度）改sql为data
def stats_check_address_evenness_laoan_excellent(
        check_banzu_count_data, check_point_count_data,
        banzu_department_checked_count_data, check_point_checked_count_data,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data):
    # 重要检查点受检次数
    data_check_point = pd.merge(
        check_point_count_data,
        check_point_checked_count_data,
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="FK_CHECK_POINT_ID")
    data_check_point.drop(
        ["CHECK_POINT_ID", "FK_CHECK_POINT_ID"], inplace=True, axis=1)
    data_check_point.fillna(0, inplace=True)
    data_check_point = df_merge_with_dpid(
        data_check_point, department_data, how='inner')
    data_check_point.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 班组受检次数
    data_check_banzu = pd.merge(
        check_banzu_count_data,
        banzu_department_checked_count_data,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(
        data_check_banzu, department_data, how='inner')
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 合并
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = pd.merge(
        zhanduan_dpid_data,
        data,
        how='right',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'COUNT', 'AVG_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


def _calc_avg_check_count_by_zhanduan(data):
    """基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    地点工作人数=该地点工作的班组总人数
    应检查值=地点作业人数*基准值
    """
    # 计算站段总工作人数(作业人数PERSON_NUMBER为0时补充为部门人数PERSON_COUNT)
    data['PERSON_NUMBER'] = data.apply(
        lambda row: row['PERSON_NUMBER']
        if row['PERSON_NUMBER'] > 0 else row['PERSON_COUNT'], axis=1)
    zhanduan_person_number = data.groupby(['TYPE3'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        zhanduan_person_number.to_frame(name='SUM_PERSON_NUMBER'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 每个地点工作人数(不同班组聚合)
    address_data = data.groupby(['ADDRESS_NAME'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        address_data.to_frame(name='ADDRESS_PERSON_NUMBER'),
        how='left',
        left_on='ADDRESS_NAME',
        right_index=True)
    data.drop_duplicates(
        subset=['ADDRESS_NAME', 'ZHANDUAN_NAME'], keep='first', inplace=True)

    # 计算站段总检查次数
    zhanduan_check_count = data.groupby(['TYPE3'])['CHECK_COUNT'].sum()
    data = pd.merge(
        data,
        zhanduan_check_count.to_frame(name='SUM_CHECK_COUNT'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 计算基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    data['BASE_CHECK_COUNT'] = data.apply(
        lambda row: (row['SUM_CHECK_COUNT'] / row['SUM_PERSON_NUMBER'])
        if row['SUM_PERSON_NUMBER'] > 0 else 0, axis=1)
    # 计算应检查值=地点作业人数*基准值
    # 比较值= 该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)
    data['AVG_CHECK_COUNT'] = data.apply(
        lambda row: (row['ADDRESS_PERSON_NUMBER'] * row['SUM_CHECK_COUNT']
                     / row['SUM_PERSON_NUMBER'])
        if row['SUM_PERSON_NUMBER'] > 0 else 0, axis=1)
    return data


# 检查地点均衡度 - 显示详细信息
def stats_check_address_evenness_new(
        check_banzu_count, check_point_count,
        banzu_department_checked_count, check_point_checked_count,
        check_point_connect_department, banzu_connect_department,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, content=None, weight_list=None, is_deduct=True,
        calc_func=_calc_score_by_formula_new,
        work_load=None):
    """
        每个地点受检次数超过比较值400%以上的一处扣2分;受检次数低于比较值50%的一处扣2分，
        未检查的一处扣5分。
    （每个地点的比较值=该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)，
    受检地点的工作量越大，比较值越高，本单位现场检查）
    检查地点中，每处检查点、重要检查点人数依据车间班组配置的该点作业人数为准，
    若配置的主要生产场所点作业人数为0或空，则取整个班组作业人数。
    若一个检查点的关联单位为车间，同样去主要生产场所中配置的改地点作业人数，
    若配置的作业人数为0或空，则取该车间的直属人数，即人员部门为车间单位的人数
    :param check_banzu_count: 班组总数
    :param check_point_count: 地点总数
    :param banzu_department_checked_count:　班组受检次数
    :param check_point_checked_count: 地点受检次数
    :param check_point_connect_department: 为当前专业下所有具备检查项目的班组
    :param banzu_connect_department: 班组关联站段,选取班组全称
    :param department_data: 部门
    :param zhanduan_dpid_data: 站段
    :param months_ago: 月份
    :param risk_type: 指数类型
    :param choose_dpid_data:
    :param content: 可自定义文本
    :param weight_list: 自定义比较值 [high, low]
    :param is_deduct: 是否与专业基数比较
    :param calc_func: 分数计算公式
    :param work_load: 工作量
    :return:
    """
    # 重要检查点受检次数
    data_check_point = pd.merge(
        check_point_count,
        department_data,
        how='inner',
        left_on="SOURCE_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID"
    )
    data_check_point = pd.merge(
        data_check_point,
        check_point_checked_count,
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="CHECK_POINT_ID")
    data_check_point = pd.merge(
        data_check_point,
        check_point_connect_department,
        how='inner',
        left_on='SOURCE_DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    # data_check_point.drop(["CHECK_POINT_ID", "PK_ID", 'TYPE3'], inplace=True, axis=1)

    # 班组受检次数
    check_banzu_count = pd.merge(
        check_banzu_count,
        check_point_connect_department,
        how='inner',
        left_on='FK_DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')

    data_check_banzu = pd.merge(
        check_banzu_count,
        banzu_department_checked_count,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu = pd.merge(
        data_check_banzu,
        banzu_connect_department,
        how='inner',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu = df_merge_with_dpid(data_check_banzu, department_data)

    # 合并班组及重要检查点
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = pd.merge(
        data,
        zhanduan_dpid_data.rename(
            columns={'NAME': 'ZHANDUAN_NAME', 'DEPARTMENT_ID': 'TYPE3'}),
        how='outer',
        left_on='TYPE3',
        right_on='TYPE3')

    # 处理工作量类型的均衡度
    if work_load:
        data['PERSON_COUNT'] = 0
        data.drop(['PERSON_NUMBER'], inplace=True, axis=1)
        data = pd.merge(
            data,
            work_load.rename(
                columns={'FK_DEPARTMENT_ID': 'DEPARTMENT_ID',
                         'COUNT': 'PERSON_NUMBER'}),
            how='left',
            right_on='DEPARTMENT_ID',
            left_on='DEPARTMENT_ID'
        )
    data.fillna(0, inplace=True)
    # 统计数据
    data = _calc_avg_check_count_by_zhanduan(data)

    # 导出中间计算过程
    _export_calc_address_evenness_data_new(
        data.copy(), months_ago, risk_type, content=content,
        weight_list=weight_list)

    if not is_deduct:
        df_type3_count = data['TYPE3'].value_counts()
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(
                row, 'CHECK_COUNT', 'AVG_CHECK_COUNT',
                'TYPE3', df_type3_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        data['DEDUCT_SCORE'] = data.apply(
            lambda row: calc_func(
                row, 'CHECK_COUNT', 'AVG_CHECK_COUNT',
                weight_list=weight_list), axis=1)
        rst_index_score = calc_child_index_type_sum(
            data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score


def _export_calc_address_evenness_work_load_data_ex(
        data, months_ago, risk_type,
        deduct_dict=None):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    if deduct_dict is None:
        deduct_dict = {
            0.5: 2,
            4: 2
        }
    # 升序排列键
    keys = sorted(list(deduct_dict.keys()))
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_COUNT']
        real_check_count = row['COUNT']
        base_check_count = row['BASE_COUNT']
        work_load = row['WORK_LOAD_COUNT']
        point_name = row['NAME']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if avg_check_count <= 0 and work_load <= 0:
            continue
        check_desc = (f'{point_name}(比较值：{avg_check_count:.2f}, '
                      f'受检次数：{real_check_count:.0f}, '
                      f'基准值：{base_check_count:.4f}, '
                      f'工作量：{work_load:.5f})')
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= keys[-1]:
                cnt.update({
                    f'受检次数超过比较值{keys[-1] * 100}%以上地点：':
                        cnt.get(f'受检次数超过比较值{keys[-1] * 100}%以上地点：', '')
                        + '<br/>' + check_desc
                })
            elif _ratio <= -keys[0]:
                cnt.update({
                    f'受检次数低于比较值{keys[0] * 100}%地点：':
                        cnt.get(f'受检次数低于比较值{keys[0] * 100}%地点：', '')
                        + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


def stats_check_address_evenness_work_load_ex(
        check_banzu_count_data, banzu_checked_count_data, work_load,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, dp_level_name='ZHANDUAN',
        calc_func=_calc_score_by_formula,
        is_deduct=True,
        deduct_dict=None,
        is_recursive=False):
    """[检查地点工作量，并且文本描述带有检查值，应检查值，比较值等]
    默认基准值 为 全站段的检查数/全站段的工作量
    dp_level 表示求基准值的级别
    is_recursive 表示是否向上递归，车间检查次数1，
    该车间下某班组检查次数2，车间实际检查次数2+1次
    """
    from app.data.major_risk_index.common_diff_risk_and_item.common import \
        recursive_count_from_down_to_up
    # 只统计含工作量的班组
    work_load_cp = work_load.copy()
    work_load_cp = pd.merge(
        check_banzu_count_data,
        work_load_cp,
        how='inner',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    work_load_cp.drop(['FK_DEPARTMENT_ID'], inplace=True, axis=1)
    dp_level_dict = {
        "ZHANDUAN": 3,
        "MAJOR": 2,
    }
    dp_level = dp_level_dict.get(dp_level_name)

    # 将工作量跟应受检点合并起来
    work_load_cp.rename(columns={"COUNT": "WORK_LOAD_COUNT"}, inplace=True)
    banzu_checked_count_data = pd.merge(
        banzu_checked_count_data,
        work_load_cp,
        on='DEPARTMENT_ID',
        how='inner'
    )
    banzu_checked_count_data.drop_duplicates(
        subset=["DEPARTMENT_ID"], keep='first', inplace=True)
    # 检查次数，工作量缺失的补零
    banzu_checked_count_data.fillna(
        {
            "WORK_LOAD_COUNT": 0,
            "COUNT": 0
        }, inplace=True)

    # 求总检查次数（默认站段)
    total_check_count_data = banzu_checked_count_data.groupby(
        [f'TYPE{dp_level}'])['COUNT'].sum().reset_index()
    total_check_count_data.rename(
        columns={"COUNT": f"{dp_level_name}_CHECK_COUNT"}, inplace=True)

    # 求总工作量（默认站段级别）
    total_work_load_data = work_load_cp.groupby([f'TYPE{dp_level}'])[
        'WORK_LOAD_COUNT'].sum().reset_index()
    total_work_load_data.rename(
        columns={"WORK_LOAD_COUNT": f"{dp_level_name}_WORK_LOAD_COUNT"},
        inplace=True)

    # 合并总检查次数，总工作量df
    total_work_load_and_check_count_data = pd.merge(
        total_check_count_data,
        total_work_load_data,
        on=f'TYPE{dp_level}',
        how='inner'
    )
    # 删除引用
    del total_work_load_data, total_check_count_data

    # 求基准值
    total_work_load_and_check_count_data['BASE_COUNT'] = \
        total_work_load_and_check_count_data.apply(
            lambda row:
            row[f"{dp_level_name}_CHECK_COUNT"] /
            row[f"{dp_level_name}_WORK_LOAD_COUNT"], axis=1)

    # 根据dp_level,将基准值等信息合并到受检点中
    banzu_checked_count_data = pd.merge(
        banzu_checked_count_data,
        total_work_load_and_check_count_data,
        on=f'TYPE{dp_level}',
        how='inner'
    )

    # 删除引用
    del total_work_load_and_check_count_data
    # 求递归后的实际工作量，检查次数
    if is_recursive:
        banzu_checked_count_data = recursive_count_from_down_to_up(
            banzu_checked_count_data, department_data, column='WORK_LOAD_COUNT'
        )
        banzu_checked_count_data = recursive_count_from_down_to_up(
            banzu_checked_count_data, department_data, column='COUNT'
        )

    # 计算比较值
    banzu_checked_count_data['AVG_COUNT'] = banzu_checked_count_data.apply(
        lambda row: row['BASE_COUNT'] * row['WORK_LOAD_COUNT'], axis=1)

    # 添加站段信息
    banzu_checked_count_data['MAJOR'] = banzu_checked_count_data.apply(
        lambda row:
        zhanduan_dpid_data['MAJOR'][zhanduan_dpid_data['DEPARTMENT_ID']
                                    == row['TYPE3']].values[0], axis=1
    )

    # 外链接后删除type不大于9的行
    if is_recursive:
        banzu_checked_count_data = banzu_checked_count_data[
            banzu_checked_count_data['TYPE'] >= 8]
    else:
        banzu_checked_count_data = banzu_checked_count_data[
            banzu_checked_count_data['TYPE'] >= 9]

    # 导出中间计算过程
    _export_calc_address_evenness_work_load_data_ex(
        banzu_checked_count_data.copy(), months_ago, risk_type,
        deduct_dict=deduct_dict)
    if not is_deduct:
        # 统计dp_level的部门个数，例如成都工务段2个，成都高铁工务段3个
        df_dp_level_count = banzu_checked_count_data[
            f'TYPE{dp_level}'].value_counts(
        )
        banzu_checked_count_data['DEDUCT_SCORE'] = \
            banzu_checked_count_data.apply(
                lambda row:
                _calc_score_by_formula_workload(
                    row, 'COUNT', 'AVG_COUNT',
                    f'TYPE{dp_level}', df_dp_level_count), axis=1)
        rst_index_score = calc_child_index_type_sum(
            banzu_checked_count_data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > x else x,
            choose_dpid_data,
            risk_type=risk_type)
        return rst_index_score
    else:
        weight_list = None
        if deduct_dict:
            weight_list = sorted(list(deduct_dict.keys()), reverse=True)
        banzu_checked_count_data['DEDUCT_SCORE'] = \
            banzu_checked_count_data.apply(
                lambda row: calc_func(row, 'COUNT', 'AVG_COUNT',
                                      weight_list=weight_list), axis=1)
        rst_index_score = calc_child_index_type_sum(
            banzu_checked_count_data,
            2,
            4,
            3,
            months_ago,
            'DEDUCT_SCORE',
            'SCORE_c',
            lambda x: 0 if 0 > (x + 100) else (x + 100),
            choose_dpid_data,
            risk_type=risk_type,
            NA_value=1)
        return rst_index_score


def _calc_key_time_check_point_bydate(base, check_data):
    """
    按日期计算关键时段的分数，以及归纳描述文字
    Args:
        base: 比较基数，
        check_data: 对应统计数据

    Returns:

    """
    score = 0
    content = ''
    content_dict = {
        "检查低于30%的日期": '',
        "检查低于20%的日期": '',
        "检查低于10%的日期": '',
    }
    for day in set(check_data['DATE'].values.tolist()):
        # 当天 关键时段 检查次数
        a = check_data[
            check_data['DATE'] == day &
            check_data['IS_KEY_TIME'] == 1
            ]['COUNT'].sum()
        # 当天检查 检查次数
        b = check_data[
            check_data['DATE'] == day
            ]['COUNT'].sum()
        ratio = (a / b - base) / base
        d = ''
        if ratio <= -0.3:
            c = 0.2
            d = '检查低于30%的日期'
        elif ratio <= -0.2:
            c = 1
            d = '检查低于20%的日期'
        elif ratio <= -0.1:
            c = 2
            d = '检查低于10%的日期'
        else:
            c = 0
        score += c
        if d:
            content_dict[d] += \
                f"<p>检查日期:{day}, 关键时段检查次数:{a}, 总检查次数:{b}</p>"
    for a, b in content_dict.items():
        content += a + '\n' + b
    return content, score


# 关键时段检查均衡度
def stats_key_times_check_evenness_check_point(
        check_info_and_check_point_sql,
        department_data, zhanduan_dpid_data,
        check_point_data,
        months_ago, risk_type,
        choose_dpid_data):
    """
    关键时段检查均衡度。
    关键时段（23时-6时）：每日段关键时段调车检查次数（跨天检查算2次，多人检查，多个地点，
    每人每地点算一次）/段每日调车检查次数，
    低于30%的扣0.2分/日，低于20%的扣1分/日，低于10%的扣2分/日，得分=100-扣分。
    Args:
        check_info_and_check_point_sql:
        department_data:
        zhanduan_dpid_data:
        months_ago:
        risk_type:
        choose_dpid_data:
        check_point_data:限定检查地点

    Returns:

    """
    stats_month = get_custom_month(months_ago)
    check_point_ids = check_point_data['FK_CHECK_POINT_ID'].values.tolist()
    check_info_and_check_point_data = pd_query(
        check_info_and_check_point_sql.format(*stats_month))
    check_info_and_check_point_data = check_info_and_check_point_data[
        check_info_and_check_point_data['FK_CHECK_POINT_ID'].isin(
            check_point_ids)
    ]
    # 去重
    check_info_and_check_point_data = \
        check_info_and_check_point_data.drop_duplicates(
            subset=['FK_CHECK_INFO_ID', 'FK_CHECK_POINT_ID', 'ID_CARD'])
    # 新增字段是否为关键时段, 检查日期（20190910），检查次数
    # 检查日期以提交时间日期为准
    check_info_and_check_point_data['IS_KEY_TIME'] = 0
    check_info_and_check_point_data['DATE'] = 0
    check_info_and_check_point_data['COUNT'] = 1
    for idx, row in check_info_and_check_point_data.iterrows():
        start = datetime.datetime.strptime(
            str(row['START_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        end = datetime.datetime.strptime(
            str(row['END_CHECK_TIME']), '%Y-%m-%d %H:%M:%S')
        # 判断是否跨天
        if start != end:
            row.loc[idx, 'COUNT'] = 2
        # 判断是否关键时段
        if start.replace(hour=23, minute=0, second=0) < start \
                or start.replace(hour=6, minute=0, second=0) > start:
            row.loc[idx, 'IS_KEY_TIME'] = 1
        if end.replace(hour=23, minute=0, second=0) < end \
                or end.replace(hour=6, minute=0, second=0) > end:
            row.loc[idx, 'IS_KEY_TIME'] = 1
        if (start != end) and (
                start.replace(hour=23, minute=0, second=0) > start or
                end.replace(hour=6, minute=0, second=0) < end):
            row.loc[idx, 'IS_KEY_TIME'] = 1
        # 检查日期
        submit_time = datetime.datetime.strptime(
            str(row['SUBMIT_TIME']), '%Y-%m-%d %H:%M:%S')
        row.loc[idx, 'DATE'] = str(
            datetime.datetime.strftime(submit_time, "%Y%m%d"))
    # 段 关键时段 总检查 次数 / 段 总检查次数
    check_info_and_check_point_data = check_info_and_check_point_data.groupby(
        ['CHECK_STATION_ID', 'IS_KEY_TIME', 'DATE']
    )['COUNT'].sum().reset_index()
    rst = []
    for dpid in set(zhanduan_dpid_data['DEPARTMENT_ID'].values.tolist()):
        # 段 关键时段总检查次数
        a = check_info_and_check_point_data[
            check_info_and_check_point_data['CHECK_STATION_ID'] == dpid &
            check_info_and_check_point_data['IS_KEY_TIME'] == 1
            ]['COUNT'].sum()
        # 段 总检查次数
        b = check_info_and_check_point_data[
            check_info_and_check_point_data['CHECK_STATION_ID'] == dpid
            ]['COUNT'].sum()
        # 计算，每天段的检查情况记分
        # 返回 描述内容， 分数
        content, score = _calc_key_time_check_point_bydate(
            a / b,
            check_info_and_check_point_data[
                check_info_and_check_point_data['CHECK_STATION_ID'] == dpid
                ])
        rst.append(
            {
                'FK_DEPARTMENT_ID': dpid,
                'CONTENT': content,
                'SCORE': score,
                'MAJOR': zhanduan_dpid_data[
                    zhanduan_dpid_data['DEPARTMENT_ID']
                    == dpid]['MAJOR'].values[0],
                'DEPARTMENT_ID': dpid,
            }
        )
    rst_data = pd.DataFrame(rst)
    data_rst = format_export_basic_data(
        rst_data[['MAJOR', 'DEPARTMENT_ID', 'CONTENT']],
        4,
        6,
        3,
        months_ago,
        risk_type=risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     4, 6, risk_type)

    rst_data = df_merge_with_dpid(
        rst_data[['FK_DEPARTMENT_ID', 'SCORE']], department_data)
    rst_child_score = calc_child_index_type_sum(
        rst_data,
        2,
        4,
        6,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


if __name__ == '__main__':
    pass
