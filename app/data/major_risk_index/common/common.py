#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: common functions for diaoche_index module.
'''
from app.data.index.util import get_custom_month
from app.data.major_risk_index.common.check_evenness_sql import CHECK_BANZU_PERSON_COUNT_SQL, \
    BANZU_CONNECT_DEPARTMENT_SQL, BANZU_CHECKED_COUNT_SQL, CHECK_POINT_PERSON_COUNT_SQL, \
    CHECK_POINT_CONNECT_DEPARTMENT_SQL, POINT_CHECKED_COUNT_SQL
from app.data.util import pd_query
import pandas as pd


def get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0], None)


def _calc_work_time(row, days):
    """计算工作量

    Arguments:
        row {} -- 每个单位的工作量情况
        days {int} -- 当月天数

    Returns:
        int/float -- 返回工作量
    """
    work_length = row['WORK_LENGTH']
    work_number = row['WORK_NUMBER']
    if pd.isnull(work_length) or pd.isnull(work_number):
        return 0
    if row['WORK_TYPE'] == 1:
        # 先固定日勤制每个月工作天数为22天
        return int(work_length * work_number * 22)
    elif row['WORK_TYPE'] == 2:
        # work_time:24小时制度（2:二班倒、3:三班倒，1：四班倒）
        work_time = row['WORK_TIME']
        if pd.isnull(work_time):
            return 0
        work_time = int(work_time)
        if work_time == 1:
            work_time = 4
        return int(work_number * work_length / work_time * days)

    return 0


def calc_work_load(sql, month):
    """计算每个单位的调车作业工作量

    Arguments:
        sql {sql} -- 调车作业sql语句
        months_ago {int} -- 月份

    Returns:
        DataFrame -- 每个单位的调车作业工作量
    """
    # 计算指定月的天数

    data = pd_query(sql.format(month))
    return data


def get_check_address_basic_data(months_ago,
                                 major,
                                 check_item_ids,
                                 check_banzu_person_count_sql=CHECK_BANZU_PERSON_COUNT_SQL,
                                 banzu_connect_department_sql=BANZU_CONNECT_DEPARTMENT_SQL,
                                 banzu_checked_count_sql=BANZU_CHECKED_COUNT_SQL,
                                 check_point_person_count_sql=CHECK_POINT_PERSON_COUNT_SQL,
                                 check_point_connect_department_sql=CHECK_POINT_CONNECT_DEPARTMENT_SQL,
                                 point_checked_count_sql=POINT_CHECKED_COUNT_SQL):
    stats_month = get_custom_month(months_ago)

    # 检查班组数统计
    check_banzu_count = pd_query(check_banzu_person_count_sql.format(major))

    # 班组关联站段(选取部门全称)
    banzu_connect_department = pd_query(banzu_connect_department_sql.format(major))

    # 班组受检次数
    banzu_department_checked_count = pd_query(banzu_checked_count_sql.format(*stats_month, check_item_ids))

    # 重要检查点
    check_point_count = pd_query(check_point_person_count_sql.format(major))

    # 重要检查点关联部门(选取部门全称)
    check_point_connect_department = pd_query(check_point_connect_department_sql.format(major, check_item_ids))

    # 重要检查点受检次数
    check_point_checked_count = pd_query(point_checked_count_sql.format(*stats_month, check_item_ids))

    data_dict = {
        'check_banzu_count': check_banzu_count,
        'banzu_connect_department': banzu_connect_department,
        'banzu_department_checked_count': banzu_department_checked_count,
        'check_point_count': check_point_count,
        'check_point_connect_department': check_point_connect_department,
        'check_point_checked_count': check_point_checked_count,

    }
    return data_dict


def get_work_shop_department(work_shop_name):
    """
    获取middb中电务车间对应的站段
    """
    work_shop_dict = {
        '朝阳湖车间': '成都电务段',
        '成都北车间': '成都电务段',
        '成都车间': '成都电务段',
        '成都南车间': '成都电务段',
        '成都西车间': '成都电务段',
        '德阳车间': '成都电务段',
        '广元车间': '成都电务段',
        '江油城际场车间': '成都电务段',
        '乐山车间': '成都电务段',
        '绵阳车间': '成都电务段',
        '彭山车间': '成都电务段',
        '资阳北车间': '成都电务段',
        '广元南车间': '成都电务段',
        '资阳车间': '成都电务段',
        '巴中车间': '达州电务段',
        '达州车间': '达州电务段',
        '广安南车间': '达州电务段',
        '合川车间': '达州电务段',
        '金堂车间': '达州电务段',
        '阆中车间': '达州电务段',
        '南充北车间': '达州电务段',
        '南充车间': '达州电务段',
        '遂宁车间': '达州电务段',
        '万州车间': '达州电务段',
        '都匀车间': '贵阳电务段',
        '都匀东车间': '贵阳电务段',
        '贵阳北车间': '贵阳电务段',
        '贵阳车间': '贵阳电务段',
        '贵阳东车间': '贵阳电务段',
        '贵阳南车间': '贵阳电务段',
        '凯里车间': '贵阳电务段',
        '凯里南车间': '贵阳电务段',
        '龙坑车间': '贵阳电务段',
        '龙里车间': '贵阳电务段',
        '息烽车间': '贵阳电务段',
        '遵义车间': '贵阳电务段',
        '安顺西车间': '贵阳电务段',
        '白云车间': '贵阳电务段',
        '毕节车间': '贵阳电务段',
        '涪陵车间': '重庆电务段',
        '内江北车间': '重庆电务段',
        '内江车间': '重庆电务段',
        '綦江车间': '重庆电务段',
        '綦江东车间': '重庆电务段',
        '黔江车间': '重庆电务段',
        '兴隆场车间': '重庆电务段',
        '永川车间': '重庆电务段',
        '重庆客专车间': '重庆电务段',
        '重庆南车间': '重庆电务段',
        '重庆西车间': '重庆电务段',
        '重庆北车间': '重庆电务段',
        '安顺车间': '六盘水工电段',
        '草海车间': '六盘水工电段',
        '二道岩车间': '六盘水工电段',
        '六盘水车间': '六盘水工电段',
        '六盘水南车间': '六盘水工电段',
        '六枝车间': '六盘水工电段',
        '梅花山车间': '六盘水工电段',
        '织金北车间': '六盘水工电段',
        '大关车间': '宜宾工电段',
        '水富车间': '宜宾工电段',
        '盐津车间': '宜宾工电段',
        '宜宾南车间': '宜宾工电段',
        '宜宾西车间': '宜宾工电段',
        '昭通车间': '宜宾工电段',
        '北碚车间': '重庆工电段',
        '石柱车间': '重庆工电段',
        '长寿北车间': '重庆工电段',
        '重庆北工电车间': '重庆工电段',
        '峨眉车间': '西昌工电段',
        '汉源车间': '西昌工电段',
        '普雄车间': '西昌工电段',
        '西昌车间': '西昌工电段',
        '攀枝花车间': '西昌工电段',
    }
    zhanduan_name = work_shop_dict.get(work_shop_name)
    return zhanduan_name
