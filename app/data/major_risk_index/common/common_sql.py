# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != "";
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}';
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.IS_DELETE = 0
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""
            # AND a.POSITION IN ({0})

# 非干部人数
WORKER_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
            AND a.IDENTITY <> '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""
            # AND a.POSITION IN ({0})

# 量化人员数量,去除各项指标（包括基础指标和细化指标）都为零的
QUANTIZATION_PERSON_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_quantization_base_quota AS a
            INNER JOIN
        t_person AS b ON b.ID_CARD = a.ID_CARD
    WHERE
        a.YEAR = {0} AND a.MONTH = {1}
            AND a.`STATUS` = 1
            AND b.POSITION IN ({2})
            AND (a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_base_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(CHECK_TIMES_TOTAL, 0)
                    + IFNULL(PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_NUMBER_TOTAL, 0)
                    + IFNULL(WORK_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(CHECK_NOTIFICATION_TIMES_TOTAL, 0)
                    + IFNULL(MIN_QUALITY_GRADES_TOTAL, 0)
                    + IFNULL(HIDDEN_DANGER_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(RISK_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL, 0) 
                    ) > 0
                )
            OR a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_refinement_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_TIME_TOTAL, 0)
                    ) > 0
                )
            )
    GROUP BY a.FK_DEPARTMENT_ID
"""

# 检查次数（现场检查）
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 管理项问题数
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""
# 考核作业项问题数
KAOHE_ZY_CHECK_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 管理项考核问题数 - 管理项只包含F
KAOHE_GL_CHECK_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('F1', 'F2', 'F3', 'F4')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 较大和重大安全风险问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# MAIN_TYPE=1为事故
# MAIN_TYPE=2为故障
BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT,
    a.MAIN_TYPE
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_department AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND c.TYPE2 = '{2}'
        AND a.MAIN_TYPE in (1, 2)
"""

# 外聘人员数 - 站段(保留FK_DEPARTMENT_ID)
EMPLOYED_OUTSIDE_PERSON_SQL = """
SELECT
    a.FK_DEPARTMENT_ID, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.TYPE2 = '{1}'
    AND b.is_delete = 0
"""