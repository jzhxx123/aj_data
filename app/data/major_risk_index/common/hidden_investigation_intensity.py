# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_investigation_intensity
   Author :       hwj
   date：          2019/10/21上午10:36
   Change Activity: 2019/10/21上午10:36
-------------------------------------------------
"""
import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set, df_merge_with_dpid
from app.data.util import pd_query


def _calc_score_by_formula(row, column, major_column):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.3:
        _score = 100
    elif 0.3 > _ratio >= -0.1:
        _score = 100 - 100 * (0.3 - _ratio)
    elif -0.1 > _ratio >= -0.3:
        _score = 60 - 300 * (-0.1 - _ratio)
    else:
        _score = 0
    return _score


def get_check_problem_count(
        problem_number_sql, risk_ids, stats_months, department_data):
    """获取检查问题总数及干部检查问题数"""
    all_problem = pd_query(problem_number_sql.format(*stats_months, risk_ids))
    check_problem_1 = all_problem[all_problem.FK_KEY_ITEM_ID == 2501].copy()
    check_problem_2 = all_problem[all_problem.FK_KEY_ITEM_ID == 2502].copy()
    check_problem_3 = all_problem[all_problem.FK_KEY_ITEM_ID == 2503].copy()
    check_problem_1 = check_problem_1.groupby(
        'FK_DEPARTMENT_ID')['COUNT'].sum().to_frame('MIDDLE_1_PROBLEM')
    check_problem_2 = check_problem_2.groupby(
        'FK_DEPARTMENT_ID')['COUNT'].sum().to_frame('MIDDLE_2_PROBLEM')
    check_problem_3 = check_problem_3.groupby(
        'FK_DEPARTMENT_ID')['COUNT'].sum().to_frame('MIDDLE_3_PROBLEM')
    cadre_check_problem = pd.concat(
        [check_problem_1, check_problem_2, check_problem_3], axis=1, sort=False)
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem['ALL_PROBLEM'] = cadre_check_problem.apply(
        lambda row:
        row['MIDDLE_1_PROBLEM'] + row['MIDDLE_2_PROBLEM']
        + row['MIDDLE_3_PROBLEM'], axis=1
    )
    cadre_check_problem = pd.merge(
        cadre_check_problem,
        department_data,
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID')
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem = cadre_check_problem.groupby(
        'TYPE3')['MIDDLE_1_PROBLEM', 'MIDDLE_2_PROBLEM', 'MIDDLE_3_PROBLEM', 'ALL_PROBLEM'].sum()
    return cadre_check_problem


def calc_weak_security_basic_data(weak_check_count, check_count, choose_dpid_data):
    """
    获取隐患排查力度指数--隐患排查力度指数的基础数据
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    if weak_check_count.empty:
        basic_data = choose_dpid_data(3).copy()
        basic_data['CONTENT'] = '暂无车间报警数据'
        basic_data['SCORE_b_3'] = 0
        calc_df_data = basic_data
        df_rst = basic_data.groupby('DEPARTMENT_ID')['SCORE_b_3'].sum().to_frame()
    else:
        basic_data = pd.merge(weak_check_count, check_count, left_on='TYPE3', right_index=True)
        basic_data.fillna(0, inplace=True)
        basic_data['AVG_NUMBER'] = basic_data.apply(
            lambda row: row.COUNT / row.WARN_COUNT if row.WARN_COUNT > 0 else 0, axis=1
        )
        '车间扣分数=（1-车间实际检查次数/（车间报警次数*段内车间检查基数））*100'
        tmp = '{0}扣分数({5:.0f})= 1 - 检查次数({1:.0f}) / (报警次数({2:.0f}) * (段检查数({3:.0f}) / 段报警数({4:.0f})))'

        basic_data['SCORE'] = basic_data.apply(
            lambda row: max(min((1 - row['WEAK_COUNT']
                                 / (row['WEAK_WARN_COUNT'] * row['AVG_NUMBER'])) * 100
                                if (row['WEAK_WARN_COUNT'] * row['AVG_NUMBER']) > 0 else 0, 100), 0), axis=1
        )
        basic_data['CONTENT'] = basic_data.apply(
            lambda row: tmp.format(row['NAME'], row['WEAK_COUNT'], row['WEAK_WARN_COUNT'],
                                   row['COUNT'], row['WARN_COUNT'], row['SCORE']), axis=1
        )

        content_data = basic_data.groupby('TYPE3')['CONTENT'].apply(
            lambda row: '<br/>'.join(row)).to_frame(name='CONTENT')
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            content_data)
        df_rst = basic_data.groupby('TYPE3')['SCORE'].apply(
            lambda row: max(100 - sum(row), 0)).to_frame(name='SCORE_b_3')
    return calc_df_data, df_rst


# 检查均衡度
def stat_check_evenness_radio(months_ago,
                              weak_check_count,
                              check_count,
                              choose_dpid_data,
                              risk_type
                              ):
    """
    安全薄弱车间干部检查次数/全段道岔设备检查总次数
    基数 = 道岔设备检查总次数/车间总数量
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_weak_security_basic_data(weak_check_count, check_count, choose_dpid_data)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 9, 2, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 9, 2, risk_type=risk_type)

    # 导出得分
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_b_3',
        3,
        2,
        9,
        2,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def get_weak_security_department_count(weak_dpid, cadre_check_count, dpid):
    """获取安全薄弱部门的检查次数"""
    if weak_dpid.empty:
        return weak_dpid
    else:
        weak_dpid['zhanduan_name'] = weak_dpid.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1)
        # 获取车间真实名称
        weak_dpid = weak_dpid.sort_values(
            ['zhanduan_name', 'WARN_COUNT'], ascending=False).groupby('zhanduan_name').head(3)
        weak_dpid.dropna(inplace=True)
        dianwu_department = weak_dpid[weak_dpid.zhanduan_name.str.contains('电务')].copy()
        gongdian_department = weak_dpid[
            (~weak_dpid.zhanduan_name.str.contains('电务'))
            & (~weak_dpid.WORK_SHOP.str.contains('工电'))].copy()
        other_department = weak_dpid[weak_dpid.WORK_SHOP.str.contains('工电')].copy()
        if not dianwu_department.empty:
            dianwu_department['NAME'] = dianwu_department.apply(
                lambda row: row.WORK_SHOP.replace('车间', '信号车间'), axis=1
            )
        if not gongdian_department.empty:
            gongdian_department['NAME'] = gongdian_department.apply(
                lambda row: row.WORK_SHOP.replace('车间', '工电车间'), axis=1
            )
        if not other_department.empty:
            other_department['NAME'] = other_department.WORK_SHOP
        weak_dpid = pd.concat([dianwu_department, gongdian_department, other_department], sort=False)

        weak_dpid = pd.merge(
            weak_dpid,
            dpid[['DEPARTMENT_ID', 'NAME']],
            left_on='NAME',
            right_on='NAME',
            how='inner'
        )
        weak_dpid.drop(columns=['WORK_SHOP'], inplace=True)
        weak_dpid = pd.merge(
            weak_dpid,
            cadre_check_count,
            how='left',
            left_on='DEPARTMENT_ID',
            right_on='FK_DEPARTMENT_ID'
        )
        weak_check_count = pd.merge(
            weak_dpid[['DEPARTMENT_ID', 'WARN_COUNT', 'COUNT']],
            dpid,
            on='DEPARTMENT_ID',
            how='left'
        )
        weak_check_count.fillna(0, inplace=True)
        weak_check_count.rename(columns={'WARN_COUNT': 'WEAK_WARN_COUNT', 'COUNT': 'WEAK_COUNT'}, inplace=True)
    return weak_check_count


def get_all_department_count(all_dpid, cadre_check_count, dpid, zhanduan_dpid_data):
    """获取所有检查次数及总报警数"""
    if all_dpid.empty:
        all_dpid = zhanduan_dpid_data.copy()
        all_dpid['WARN_COUNT'] = 0
    else:
        all_dpid['NAME'] = all_dpid.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        all_dpid.dropna(subset=['NAME'], inplace=True)
        all_dpid.drop(columns=['WORK_SHOP'], inplace=True)
    all_dpid = all_dpid.groupby([all_dpid.NAME])['WARN_COUNT'].sum().to_frame()

    cadre_check_count = df_merge_with_dpid(cadre_check_count, dpid, how='right')
    cadre_check_count.fillna(0, inplace=True)
    cadre_check_count = cadre_check_count.groupby('TYPE3')['COUNT'].sum().to_frame()

    all_dpid = pd.merge(
        all_dpid,
        zhanduan_dpid_data,
        left_index=True,
        right_on='NAME',
        how='right'
    )
    all_dpid.fillna(0, inplace=True)
    all_dpid.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    all_dpid = all_dpid.groupby('TYPE3')['WARN_COUNT'].sum().to_frame()
    all_check_count = pd.concat([cadre_check_count, all_dpid], sort=False, axis=1)
    all_check_count.fillna(0, inplace=True)
    return all_check_count


def get_alarm_information_data(alarm_information_analysis, zhanduan_dpid_data):
    """"""

    df_alarm = pd.merge(
        alarm_information_analysis,
        zhanduan_dpid_data,
        left_index=True,
        right_on='NAME',
        how='right'
    )
    df_alarm.fillna(0, inplace=True)
    df_alarm['all_equipment'] = df_alarm.apply(
        lambda row: row.UNACCESSED_QUANTITY + row.ACCESSED_QUANTITY, axis=1)
    accessed_quantity = df_alarm[['DEPARTMENT_ID', 'ACCESSED_QUANTITY']].copy()
    all_equipment = df_alarm[['DEPARTMENT_ID', 'all_equipment']].copy()
    accessed_quantity.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID', 'ACCESSED_QUANTITY': 'COUNT'})
    all_equipment.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID', 'all_equipment': 'COUNT'})
    return accessed_quantity, all_equipment


def calc_alarm_information_data(alarm_information_analysis, choose_dpid_data, zhanduan_dpid_data):
    """报警分析指数"""
    if alarm_information_analysis.empty:
        df_alarm = zhanduan_dpid_data
        df_alarm['UNACCESSED_QUANTITY'] = 0
        df_alarm['ACCESSED_QUANTITY'] = 0
        df_alarm['TIMELY_ANALYSIS'] = 0
    else:
        alarm_information_analysis = pd.merge(
            alarm_information_analysis,
            zhanduan_dpid_data[['NAME']],
            on='NAME',
            how='right'
        )
        alarm_information_analysis.fillna(0, inplace=True)
        alarm_information_analysis.set_index('NAME', inplace=True)

        alarm_information_analysis = alarm_information_analysis.astype(int)
        df_alarm = pd.merge(
            alarm_information_analysis,
            zhanduan_dpid_data,
            left_index=True,
            right_on='NAME',
            how='inner'
        )
    df_alarm['all_equipment'] = df_alarm.apply(
        lambda row: row.UNACCESSED_QUANTITY + row.ACCESSED_QUANTITY, axis=1)

    df_alarm['analysis_rate'] = df_alarm.apply(
        lambda row: row.ACCESSED_QUANTITY / row.all_equipment * 100 if row.all_equipment > 0 else 0, axis=1)

    df_alarm['analytical_time_rate'] = df_alarm.apply(
        lambda row: row.TIMELY_ANALYSIS / row.ACCESSED_QUANTITY * 100 if row.ACCESSED_QUANTITY > 0 else 0, axis=1)

    tmp = '分析覆盖率指数({0:.2f})=已调阅数量({1:.1f}) / 调阅总数({2:.0f}) * 100<br/>' \
          '分析及时性指数({3:.2f})=及时分析条数({4:.0f}) / 已调阅数量({1:.0f}) * 100'
    df_alarm['CONTENT'] = df_alarm.apply(
        lambda row: tmp.format(
            row.analysis_rate, row.ACCESSED_QUANTITY, row.all_equipment, row.analytical_time_rate, row.TIMELY_ANALYSIS
        ), axis=1)

    df_alarm['SCORE_c_3'] = df_alarm.apply(
        lambda row: row.analysis_rate * 0.5 + row.analytical_time_rate * 0.5, axis=1)

    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=df_alarm.DEPARTMENT_ID,
            data=df_alarm.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    df_rst = df_alarm.groupby(['DEPARTMENT_ID'])['SCORE_c_3'].sum().to_frame()
    return calc_df_data, df_rst


# 报警分析指数
def alarm_information_analysis_ratio(
        alarm_information_analysis,
        zhanduan_dpid_data,
        _choose_dpid_data,
        months_ago,
        risk_type,
):
    """
    分析覆盖率指数(50%):已调阅数量/道岔设备总数
    分析及时性指数(50%):及时分析条数/已调阅数量
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_alarm_information_data(
        alarm_information_analysis, _choose_dpid_data, zhanduan_dpid_data.copy())

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 9, 3, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 9, 3, risk_type=risk_type)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_c_3',
        3,
        2,
        9,
        3,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score
