# 基数选择:选择专业内连续3个月无责任事故、故障（机务、电务不统计D21事故）的单位、
# 月份（3个月）的均值指数作为专业基数参考。（多次比较得出基数）。若找不出相应比较单位，
# 找出选择3个月故障率（无责任事故）（每个月）最低的单位均数上浮20%作为专业基数。
# 电务专业：机务优先选择以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
BASE_UNIT_SQL = """SELECT DISTINCT
    a.FK_DEPARTMENT_ID
FROM
    t_safety_produce_info_responsibility_unit AS a
        LEFT JOIN
    t_safety_produce_info AS b ON b.PK_ID = a.FK_SAFETY_PRODUCE_INFO_ID
WHERE
    a.RESPONSIBILITY_IDENTIFIED < 11
        AND b.MAIN_TYPE < 3
        AND ((b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {}))
"""

# 考核作业项问题数
KAOHE_PROBLEM_SQL = """SELECT
        c.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            left join
        t_check_info_and_person as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2}) 
    GROUP BY c.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.COUNT) as COUNT
from
(
SELECT
        distinct(a.pk_id),d.FK_DEPARTMENT_ID, b.CHECK_SCORE AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            LEFT JOIN
        t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY BETWEEN 1 AND 2
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 跟班次数
GENBAN_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_GENBAN = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 实际重要检查地点数
REAL_CHECK_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 实际检查班组数
REAL_CHECK_BANZU_SQL = """SELECT DISTINCT
        a.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.HIERARCHY = 5
        AND c.IS_DELETE = 0
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0
        AND HIERARCHY = 2
    GROUP BY FK_DEPARTMENT_ID;
"""
# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0;
"""

# 一般及以上风险问题数
YIBAN_RISK_PROBLEM_NUMBER_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 3
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 所有问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_check_info_and_person as b on a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅时长
MEDIA_COST_TIME_SQL = """
select mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from (
select distinct(a.PK_ID), a.COST_TIME, d.FK_DEPARTMENT_ID from 
t_check_info_and_media as a
left join 
t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID 
left join
 t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
left join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
where
 b.CHECK_WAY = 3
and c.FK_CHECK_ITEM_ID in ({2})
and 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
AND 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
)as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        c.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            left join
        t_check_info_and_person as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY c.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),d.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, b.CHECK_SCORE AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(TYPE !=9, FK_PARENT_ID, DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department 
WHERE
         TYPE BETWEEN 9 AND 10
        AND IS_DELETE = 0
        AND MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]

# 分析中心检查力度（问题数）
ANALYSIS_CENTER_PROBLEM_COUNT_SQL = """
SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            LEFT JOIN
        t_department as d on a.EXECUTE_DEPARTMENT_ID = d.DEPARTMENT_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.name like "%%安全分析中心%%"
    GROUP BY a.EXECUTE_DEPARTMENT_ID
"""

# 分析中心检查力度（质量分）
ANALYSIS_CENTER_PROBLEM_SCORE_SQL = """
SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS NUMBER
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            LEFT JOIN
        t_department as d on a.EXECUTE_DEPARTMENT_ID = d.DEPARTMENT_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.name like "%%安全分析中心%%"
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

ANALYSIS_CENTER_CHECK_INTENSITY_SQLLIST = [ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL]
