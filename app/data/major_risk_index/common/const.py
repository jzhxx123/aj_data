#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
Author: seerbigdata
Date: 2019/8/2
Description: 
"""

HIERARCHY = 3


class MainType:
    """
    检查力度指数	check_intensity
    评估力度指数	evaluate_intensity
    考核力度指数	assess_intensity
    检查均衡度	    check_evenness
    问题暴露度	    problem_exposure
    问题整改	    problem_rectification
    安全生产效果指数        safety_produce_effect
    检查质量指数    check_quality
    """
    check_intensity = 1
    evaluate_intensity = 2
    assess_intensity = 3
    check_evenness = 4
    problem_exposure = 5
    problem_rectification = 6
    safety_produce_effect = 7
    check_quality = 13


class CheckIntensityDetailType:
    """
    检查力度指数子指数
    stats_check_per_person: 换算单位人均检查频次
    stats_check_problem_ratio: 问题查处力度
    stats_check_problem_assess_ratio: 查处问题考核率
    stats_score_per_person: 换算问题平均质量分
    stats_risk_score_per_person: 较大和重大安全风险问题质量分累计
    stats_yecha_ratio: 夜查率
    stats_check_address_ratio: 覆盖率
    stats_media_intensity: 监控调阅力度
    """
    stats_check_per_person = 2
    stats_check_problem_ratio = 3
    stats_check_problem_assess_ratio = 4
    stats_score_per_person = 5
    stats_risk_score_per_person = 6
    stats_yecha_ratio = 7
    stats_great_risk_problem_ratio = 8
    stats_check_address_ratio = 9
    stats_media_intensity = 10


class EvaluateIntensityDetailType:
    pass


class CheckEvennessDetailType:
    """
    检查均衡度子指数
    stats_problem_point_evenness: 检查问题均衡度
    stats_check_time_evenness: 检查时间均衡度
    stats_check_day_evenness： 检查日期均衡度
    stats_check_address_evenness： 地点均衡度
    stats_check_hour_evenness： 检查时段均衡度
    """
    stats_problem_point_evenness = 1
    stats_check_time_evenness = 2
    stats_check_address_evenness = 3


class ProblemExposureDetailType:
    """
    问题暴露度子指数
    stats_total_problem_exposure： 普遍性暴露
    stats_hidden_problem_exposure: 较严重隐患暴露
    stats_banzu_problem_exposure： 班组问题暴露
    stats_problem_exposure: 事故隐患问题暴露度
    other_problem_exposure: 他查问题暴露
    """
    stats_total_problem_exposure = 1
    stats_hidden_problem_exposure = 2
    stats_problem_exposure = 3
    stats_banzu_problem_exposure = 4
    other_problem_exposure = 5


class AssessIntensityDetailType:
    """
    考核力度子指数
    stats_check_problem_assess_ratio： 人均考核问题数
    stats_assess_money_per_person： 人均考核金额
    stats_award_return_ratio： 返奖率
    stats_problem_assess_ratio: 考核率
    """
    stats_check_problem_assess_ratio = 1
    stats_assess_money_per_person = 2
    stats_award_return_ratio = 3
    stats_problem_assess_ratio = 4


class ProblemRectificationDetailType:
    """
    stats_rectification_overdue: 整改时效（超期整改）
    stats_check_evaluate：履职整改
    stats_repeatedly_index： 问题控制
    stats_rectification_review： 整改复查
    stats_rectification_effect： 整改成效
    stats_peril_renovation：隐患整治
    """
    stats_rectification_overdue = 1
    stats_check_evaluate = 2
    stats_repeatedly_index = 3
    stats_rectification_review = 4
    stats_peril_renovation = 5
    stats_rectification_effect = 6


class SafetyProduceEffectDetailType:
    """
    安全生产效果指数
    stats_locomotive_malfunction_deduct: 机车故障扣分计算
    """
    stats_locomotive_malfunction_deduct = 2


class CheckQualityDetailType:
    stats_check_problem_ratio = 1
    stats_high_check_problem_ratio = 2
    stats_problem_point_evenness = 3


class CommonCalcDataType:
    """
    通用的计算的main, detail的数据项类别
    初始通用版本，实际各专业各指数可以改变解释含义，
    版本更迭也是遵循各专业各指数的延续性
    version:版本更迭约定同类（以本模块的列出来的数据为类）数据，更改风险等级，
    关联风险或者项目等，则实例化，修改版本，每个指数版本前后留有档案，记得更迭次数内容
    """

    def __init__(self, name, description, version, func_version):
        self.__name = name
        self.description = description
        self.version = version
        self.func_version = func_version

    @property
    def name(self):
        return self.__name


class IndexDivider:
    """
    指数计算配置类
    """
    # 格式：__slots__ = ()
    # 小括号代表元组，括号内写字符串，构造函数时只能构造在元组中出现过的变量名
    __slots__ = ('numerator', 'denominator')

    def __init__(self, numerator, denominator):
        self.numerator = numerator
        self.denominator = denominator


# 公共部分
PERSON_LOAD_INFO = ('PersonLoad', '干部职工总数（职工总数+外聘）', 'v1', 'double_df')
STAFF_NUMBER_INFO = ('StaffNumber', '职工总数', 'v1', 'single_df')
CADRE_COUNT_INFO = ('CadreCount', '干部总数（干部人数）', 'v1', 'single_df')
WORKER_COUNT_INFO = ('WorkerCount', '作业人数（非干部职工+外聘）', 'v1', 'double_df')
WORKER_LOAD_INFO = ('WorkerLoad', '工作量', 'v1', 'double_df')
QUANTIZATION_PERSON_INFO = ('QuantizationPerson', '量化人员', 'v1', 'assess_df')
# 特殊专业属性的人员配属
SPECIAL_CONFIG_STAFF_INFO = ('SpecialConfigStaff','职工总人数（系统管理-部门管理-专业特殊属性[客运人数+管理人数]）', 'v1', 'single_df')

# 检查力度
CHECK_COUNT_INFO = ('CheckCount', '现场检查总数', 'v1', 'single_df')
MONITOR_CHECK_COUNT_INFO = ('MONITOR_CHECK_COUNT', '监控调阅检查总数', 'v1', 'single_df')
GUANLI_PROBLEM_COUNT_INFO = (
    'GuanLiProblemCount', '检查管理项问题数（关联风险）', 'v1', 'single_df')
ZUOYE_PROBLEM_COUNT_INFO = (
    'ZuoyeProblemCount', '检查作业项问题数（关联风险）', 'v1', 'single_df')
PROBLEM_SCORE_INFO = ('ProblemScore', '检查出问题质量分（关联风险）', 'v1', 'single_df')
YECHA_COUNT_INFO = ('YeChaCount', '现场夜查次数', 'v1', 'single_df')
JIAODA_RISK_SCORE_INFO = (
    'JiaoDaRiskScore', '较大以及以上风险问题质量分（关联风险）', 'v1', 'single_df')
XC_JIAODA_RISK_SCORE_INFO = (
    'XcJiaoDaRiskScore', '现场检查较大重大问题质量分（关联风险）', 'v1', 'single_df')
MEDIA_COST_TIME_INFO = ('MedialCostTime', '监控调阅时长', 'v1', 'single_df')
MEDIA_PROBLEM_NUMBER_INFO = (
    'MediaProblemsNumber', '监控调阅发现问题数（关联风险）', 'v1', 'single_df')
MEDIA_PROBLME_SCORE_INFO = (
    'MediaProblemsScore', '监控调阅质量分（关联风险）', 'v1', 'single_df')
ANALYSIS_CENTER_PROBLEM_COUNT_INFO = (
    'AnalysisCenterProblemsCount', '分析中心检查发现问题数（关联风险）', 'v1', 'single_df')
ANALYSIS_CENTER_PROBLEM_SCORE_INFO = (
    'AnalysisCenterProblemsScore', '分析中心检查发现质量分（关联风险）', 'v1', 'single_df')
REAL_CHECK_ADDRESS_INFO = (
    'RealCheckAddress', '实际检查地点数（班组+重要检查点）', 'v1', 'customized_df')
CHECK_ADDRESS_INFO = (
    'CheckAddress', '检查地点总数（班组+重要检查点）', 'v1', 'customized_df')
SITE_PROBLEM_INFO = (
    'SiteProblemCOUNT', '现场检查问题数', 'v1', 'single_df')
VIDEO_PROBLEM_INFO = (
    'VideoProblemCOUNT', '视频检查问题数', 'v1', 'single_df')


# 考核力度
ASSESS_PROBLEM_INFO = (
    'AssessProblem', '考核问题数（关联风险）', 'v1', 'single_df')
KAOHE_ZY_CHECK_PROBLEM_INFO = (
    'KaoheZyCheckProblem', '考核作业问题数（关联风险）', 'v1', 'single_df')
KAOHE_GL_CHECK_PROBLEM_INFO = (
    'KaoheGlCheckProblem', '考核管理问题数（关联风险）', 'v1', 'single_df')
GANBU_ASSESS_RESPONSIBLE_INFO = (
    'GanBuAssessResponsible', '干部月度考核总金额(关联项目)', 'v1', 'assess_df')
FEIGANBU_ASSESS_RESPONSIBLE_INFO = (
    'FeiGanBuAssessResponsible', '非干部月度考核总金额(关联项目)', 'v1', 'assess_df')
ASSESS_RESPONSIBLE_INFO = (
    'AssessResponsible', '月度考核总金额(关联项目)', 'v1', 'assess_df')
AWARD_RETURN_MONEY_INFO = (
    'AwardReturnMoney', '月度返奖总金额(关联风险)', 'v1', 'assess_df')
ALL_PROBLEM_NUMBER_INFO = (
    'AllProblemsNumber', '所有问题数（关联风险）', 'v1', 'single_df')
ZUOYE_PROBLEM_CHECK_SCORE_INFO = (
    'ZuoYeProblemCheckScore', '作业项问题质量分（关联风险）', 'v1', 'single_df')
WORK_BANZU_INFO = (
    'WorkBanZu', '工作班组数', 'v1', 'single_df')
AWARD_RETURN_MONEY_PROBLEM_INFO = (
    'AwardReturnMoneyProblem', '返奖问题个数(A, B, E1, E2)', 'v1', 'assess_df')
REAL_AWARD_RETURN_MONEY_PROBLEM_INFO = (
    'RealAwardReturnMoneyProblem', '实际返奖问题个数(A, B, E1, E2)', 'v1', 'assess_df')

# 检查均衡度
CHECK_PROBLEM_POINT_INFO = (
    'CheckProblemPoint', '风险较大及以上检查出问题项点数（关联风险）', 'v1', 'probpoint_df')
BASE_PROBLEM_POINT_INFO = (
    'BaseProblemPoint', '风险较大及以上基础库问题项点数（关联风险）', 'v1', 'single_df')
PROBLEM_POINT_EVENNESS_INFO = (
    'ProblemPointEvenness', '问题均衡度(查出一般及以上问题项点数/基础问题库一般及以上问题项点数)', 'v1', 'customized_df'
)

# 评价力度
ACTIVE_EVALUATE_SCORE_INFO = (
    'ActiveEvaluateScore', '主动评价记分总分数（干部）', 'v1', 'single_df')

ACTIVE_EVALUATE_COUNT_INFO = (
    'ActiveEvaluateCount', '主动评价记分总条数（干部）', 'v1', 'single_df')

EVALUATE_COUNT_INFO = (
    'EvaluateCount', '评价记分总条数(干部)', 'v1', 'single_df')
