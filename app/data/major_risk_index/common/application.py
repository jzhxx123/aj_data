# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     application
   Author :       hwj
   date：          2019/10/21上午9:24
   Change Activity: 2019/10/21上午9:24
-------------------------------------------------
"""
import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set


def _calc_score_by_formula(row, column, major_column):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.3:
        _score = 100
    elif 0.3 > _ratio > 0.2:
        _score = 90
    elif 0.2 >= _ratio > -0.1:
        _score = 60
    elif -0.3 >= _ratio:
        _score = 0
    return _score


def calc_warning_unit_count(df_count, zhanduan_dpid):
    """将报警总条数需要的数据关联到站段"""
    if df_count.empty:
        zhanduan_dpid['COUNT'] = 0
        data = zhanduan_dpid
    else:

        # 车间关联站段
        df_count['NAME'] = df_count.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_count.dropna(subset=['NAME'], inplace=True)
        df_count.drop(columns=['WORK_SHOP'], inplace=True)
        df_count = df_count.groupby([df_count.NAME])['COUNT'].sum().to_frame()
        data = pd.merge(
            df_count,
            zhanduan_dpid,
            left_index=True,
            right_on='NAME',
            how='right'
        )
        data.fillna(0, inplace=True)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return data


def calc_failure_notification_count(df_count, zhanduan_dpid):
    """将故障通知”按键次数以及重复发生次数需要的数据关联到站段"""

    if df_count.empty:
        zhanduan_dpid['BUTTON_COUNT'] = 0
        zhanduan_dpid['REPEAT_COUNT'] = 0
        data = zhanduan_dpid
    else:
        # 车间关联站段
        df_count['NAME'] = df_count.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_count.dropna(subset=['NAME'], inplace=True)
        # 车站故障通知重复次数
        repeat_count = df_count.groupby(['STATION'])['BUTTON_COUNT', 'REPEAT_COUNT'].sum()
        repeat_count.REPEAT_COUNT = repeat_count.REPEAT_COUNT - 1
        df_count.drop(columns=['WORK_SHOP', 'BUTTON_COUNT', 'REPEAT_COUNT'], inplace=True)
        df_count.drop_duplicates(['STATION'], keep='first', inplace=True)
        df_count = pd.merge(
            df_count,
            repeat_count,
            how='inner',
            on='STATION'
        )
        df_count = df_count.groupby([df_count.NAME]).sum()
        data = pd.merge(
            df_count,
            zhanduan_dpid,
            left_index=True,
            right_on='NAME',
            how='right'
        )
        data.fillna(0, inplace=True)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return data


def calc_driving_influence_basic_data(failure_notification_count, choose_dpid_data):
    """
    获取道岔设备运用--行车影响指数的基础数据
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    failure_notification_count['score'] = failure_notification_count.apply(
        lambda row: row['REPEAT_COUNT'] * 1.5 + min(row['BUTTON_COUNT'] * 1, 20), axis=1
    )
    basic_data = failure_notification_count.groupby(['TYPE3'])['score', 'REPEAT_COUNT', 'BUTTON_COUNT'].sum()
    basic_data['CONTENT'] = basic_data.apply(
        lambda row: '本月累计故障通知次数:{0:.0f}次, 累计重复发生次数:{1:.0f}次'.format(
            row['BUTTON_COUNT'], row['REPEAT_COUNT']), axis=1
    )
    basic_data['SCORE_b_3'] = basic_data.apply(
        lambda row: max(100 - row['score'], 0), axis=1
    )

    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=basic_data.index,
            data=basic_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_data['SCORE_b_3'].to_frame()

    return calc_df_data, df_rst


# 行车影响指数
def stats_driving_influence_radio(months_ago,
                                  failure_notification_count,
                                  choose_dpid_data,
                                  risk_type,
                                  ):
    """
    每一次“故障通知”按键记录扣1分，扣满20分为止。
    同一车站存在多次“故障通知”按键记录时，每次加扣0.5分,即1.5分一条,
    2条合计2.5,3条4分"""
    # 获取中间过程,得分数据
    calc_df_data, df_rst = calc_driving_influence_basic_data(failure_notification_count, choose_dpid_data)

    # 保存导出中间计算数据到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 8, 2, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 8, 2, risk_type=risk_type)

    # 导出得分
    rst_child_score = []
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_b_3',
        3,
        2,
        8,
        2,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score
