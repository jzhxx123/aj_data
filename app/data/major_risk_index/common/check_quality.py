#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_quality
   Author :       hwj
   date：          2019/12/12下午4:01
   Change Activity: 2019/12/12下午4:01
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import calc_child_index_type_divide_major, export_basic_data_one_field_monthly, \
    df_merge_with_dpid, calc_child_index_type_sum, append_major_column_to_df, export_basic_data_tow_field_monthly_two, \
    format_export_basic_data, write_export_basic_data_to_mongo
from app.data.util import pd_query


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


# 换算问题质量分
def stats_score_per_person_major(problem_score,
                                 work_load,
                                 months_ago,
                                 risk_type,
                                 calc_score_by_formula=_calc_score_by_formula,
                                 choose_dpid_data=None,
                                 customizecontent=None,
                                 zhanduan_filter_list=[],
                                 fraction=None,
                                 major_ratio=None):
    """问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    return calc_child_index_type_divide_major(
        problem_score,
        work_load,
        2,
        13,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction,
        major_ratio=major_ratio)


# 高质量问题质量分
def stats_high_score_per_person_major(problem_score,
                                      work_load,
                                      months_ago,
                                      risk_type,
                                      calc_score_by_formula=_calc_score_by_formula,
                                      choose_dpid_data=None,
                                      customizecontent=None,
                                      zhanduan_filter_list=[],
                                      fraction=None,
                                      major_ratio=None):
    """高问题质量分累计/调车工作量
    问题质量分：
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项
    """
    return calc_child_index_type_divide_major(
        problem_score,
        work_load,
        2,
        13,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction,
        major_ratio=major_ratio)


# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids,
        months_ago,
        zhanduan_dpid_data,
        department_data,
        self_check_problem_sql,
        safety_produce_info_sql,
        other_check_problem_sql,
        choose_dpid_data,
        risk_type
):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(safety_produce_info_sql.format(check_item_ids, *calc_month))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid_data.loc[:, 'DEPARTMENT_ID'].values
    }
    problem_risk_score = {
        1: 4,
        2: 2,
        3: 0.1,
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[1]
        risk_level = int(each_problem[2])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level, each_problem[0]])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL', 'BASE_ID'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid_data,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data, zhanduan_dpid_data,
                                                          13, 4, 3, months_ago, first_title, second_title,
                                                          columns_list=[(1, 1), (1, 2), (2, 1), (2, 2), (3, 1), (3, 2)])
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(
        lambda row:
        row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid_data,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 13, 4, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 13, 4, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 13, 4, months_ago, 'SCORE', 'SCORE_d',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type)
    return rst_child_score
