#!/usr/bin/python3
# -*- coding: utf-8 -*-
import math

import pandas as pd

from app.data.index.util import get_custom_month
from app.data.util import get_history_months
from app.data.major_risk_index.util import (
    calc_child_index_type_divide, calc_child_index_type_sum,
    df_merge_with_dpid, write_export_basic_data_to_mongo, calc_child_index_type_divide_two, format_export_basic_data,
    append_major_column_to_df, calc_child_index_type_divide_major)
from app.data.util import pd_query


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[column] == 0 and row[major_column] == 0:
        # 因为为空填补零的关系，所以比值跟平均值应该均为0
        return 0
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _calc_evaluate_count_difference_type(differce_count):
    differce_count = round(float(differce_count), 2)
    if 7 > differce_count > 0:
        return 40 + differce_count * 10

    elif differce_count == 0:
        return 40
    elif differce_count < 0:
        return 0

    else:
        return 100


# 主动评价记分占比


def stats_active_ratio(active_evaluate_count, total_evaluate_count, months_ago,
                       risk_type, choose_dpid_data,
                       zhanduan_filter_list=[], customizecontent=None
                       ):
    # 各个站段的分数
    return calc_child_index_type_divide_major(
        active_evaluate_count,
        total_evaluate_count,
        2,
        2,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 主动评价记分占比--部分专业基数
def stats_active_ratio_filter(active_evaluate_count, total_evaluate_count, months_ago,
                              risk_type, choose_dpid_data, zhanduan_filter_list=[], customizecontent=None):
    # 各个站段的分数
    return calc_child_index_type_divide_two(
        active_evaluate_count,
        total_evaluate_count,
        2,
        2,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 干部人均主动评价记分条数
def stats_count_per_person(active_evaluate_count,
                           cadre_count,
                           months_ago,
                           risk_type,
                           choose_dpid_data=None,
                           zhanduan_filter_list=[],
                           customizecontent=None
                           ):
    return calc_child_index_type_divide_major(
        active_evaluate_count,
        cadre_count,
        2,
        2,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 干部人均主动评价记分条数--部分专业基数
def stats_count_per_person_filter(active_evaluate_count,
                                  cadre_count,
                                  months_ago,
                                  risk_type,
                                  choose_dpid_data=None,
                                  zhanduan_filter_list=[],
                                  customizecontent=None
                                  ):
    return calc_child_index_type_divide_two(
        active_evaluate_count,
        cadre_count,
        2,
        2,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 人均评价记分
def stats_score_per_person(evaluate_score,
                           work_load,
                           months_ago,
                           risk_type,
                           choose_dpid_data=None,
                           customizecontent=None,
                           calc_score_by_formula=_calc_score_by_formula):
    return calc_child_index_type_divide(
        evaluate_score,
        work_load,
        2,
        2,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent)


# 人均评价记分
def stats_score_per_person_major(evaluate_score,
                                 work_load,
                                 months_ago,
                                 risk_type,
                                 choose_dpid_data=None,
                                 customizecontent=None,
                                 zhanduan_filter_list=[],
                                 calc_score_by_formula=_calc_score_by_formula,
                                 fraction=None):
    return calc_child_index_type_divide_major(
        evaluate_score,
        work_load,
        2,
        2,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction)


# 人均评价记分--部分专业基数
def stats_score_per_person_filter(evaluate_score,
                                  work_load,
                                  months_ago,
                                  risk_type,
                                  choose_dpid_data=None,
                                  zhanduan_filter_list=[],
                                  customizecontent=None):
    return calc_child_index_type_divide_two(
        evaluate_score,
        work_load,
        2,
        2,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 评价职务占比
def stats_gradation_ratio(kezhi_evaluate_count,
                          total_evaluate_count,
                          months_ago,
                          risk_type,
                          choose_dpid_data=None,
                          customizecontent=None,
                          zhanduan_filter_list=[]):
    return calc_child_index_type_divide_major(
        kezhi_evaluate_count,
        total_evaluate_count,
        2,
        2,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        zhanduan_filter_list=zhanduan_filter_list)


# 评价职务占比--部分专业基数
def stats_gradation_ratio_filter(kezhi_evaluate_count,
                                 total_evaluate_count,
                                 months_ago,
                                 risk_type,
                                 choose_dpid_data=None,
                                 zhanduan_filter_list=[],
                                 customizecontent=None):
    return calc_child_index_type_divide_two(
        kezhi_evaluate_count,
        total_evaluate_count,
        2,
        2,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent)


# 段机关干部占比
def stats_duan_ratio(duan_cadre_count,
                     total_evaluate_count,
                     months_ago,
                     risk_type,
                     choose_dpid_data=None,
                     zhanduan_filter_list=[],
                     customizecontent=None
                     ):
    return calc_child_index_type_divide_major(
        duan_cadre_count,
        total_evaluate_count,
        2,
        2,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 段机关干部占比--部分专业基数
def stats_duan_ratio_filter(duan_cadre_count,
                            total_evaluate_count,
                            months_ago,
                            risk_type,
                            choose_dpid_data=None,
                            zhanduan_filter_list=[],
                            customizecontent=None):
    return calc_child_index_type_divide_two(
        duan_cadre_count,
        total_evaluate_count,
        2,
        2,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent
    )


# 分析中心得分
def stats_analysis_center_assess(analysis_center_assess_sql, department_data,
                                 months_ago, risk_type, choose_dpid_data, score=80):
    """

    :param analysis_center_assess_sql:
    :param department_data:
    :param months_ago:
    :param risk_type:
    :param choose_dpid_data:
    :param score: 在真实分数基础上增加的分值,默认加80
    :return:
    """
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    data = df_merge_with_dpid(
        pd_query(analysis_center_assess_sql.format(year, month)),
        department_data, how='right')
    data.fillna(0, inplace=True)
    if data.empty is True:
        return
    # GRADES_TYPE：0扣分1加分
    data['SCORE'] = data.apply(
        lambda row: row['ACTUAL_SCORE'] if row['GRADES_TYPE'] == 1 else (
                -1 * row['ACTUAL_SCORE']),
        axis=1)
    xdata = data.groupby(['DEPARTMENT_ID'])['SCORE'].sum().reset_index()
    xdata['CONTENT'] = xdata.apply(
        lambda row: '本月分析中心得分为: {0}'.format(round(row['SCORE'], 2)), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 2, 6, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 2, 6, risk_type=risk_type)
    data.drop(['ACTUAL_SCORE', 'GRADES_TYPE'], inplace=True, axis=1)
    return calc_child_index_type_sum(
        data,
        2,
        2,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(100, (score + x)),
        choose_dpid_data,
        risk_type=risk_type)


def _calc_evaluate_per_person(score_data):
    result_score = 0
    all_score = 0
    other_score = 0
    for item in score_data:
        all_score += item[3]
        if item[2] != 2:
            other_score += item[3]
    all_level = int(all_score) // 2
    other_level = int(other_score) // 2
    if all_level > other_level:
        result_score = 1 + int(math.pow(2, all_level - 1))
    return result_score


def _calc_score_because_zhanduan_evaluate(id_card, row):
    """按照指定规则计算该人员是否存在因站段评价导致扣分
        站段评价中的自动评价也属于路局评级
    A[统计今年初至上月累计评价总计分] -->B(统计当前月路局评价计分)
    G -->H(路局评价分加累计分已升级, 站段评价剔除JL-2是否还有剩余条数, 如果没有则跳过，
    如果有，加上站段评价分算所属档次进行扣分)
    G -->I(路局评价分加累计分不升级，则加上站段评价的分数进行判断是否升级扣分)
    """
    before_score = row['BEFORE_SCORE']
    luju_score = row['LUJU_SCORE']
    add_luju_score = before_score + luju_score
    add_luju_zhanduan_not_jl2_score = add_luju_score + \
                                      row['ZHANDUAN_SCORE_NOT_JL2']
    add_luju_zhanduan_score = add_luju_score + row['ZHANDUAN_SCORE']
    # 截止到上个月今年累计得分所属档次
    before_level = min(6, int(before_score) // 2)
    # 加上路局评价分后的累计分所属档次
    add_luju_level = min(6, int(add_luju_score) // 2)
    if add_luju_level > before_level:
        # 加上路局评价分后导致档次升级
        if row['ZHANDUAN_SCORE_NOT_JL2'] > 0:
            # 剔除JL-2后还有剩余评价得分
            add_luju_zhanduan_not_jl2_level = min(
                6,
                int(add_luju_zhanduan_not_jl2_score) // 2)
            return [
                id_card, before_score, luju_score, row['ZHANDUAN_SCORE'],
                row['ZHANDUAN_SCORE_NOT_JL2'],
                math.pow(2, add_luju_zhanduan_not_jl2_level - 1)
            ]
    else:
        # 加上路局评价分后未导致档次升级
        add_luju_zhanduan_level = min(6, int(add_luju_zhanduan_score) // 2)
        if add_luju_zhanduan_level > before_level:
            return [
                id_card, before_score, luju_score, row['ZHANDUAN_SCORE'],
                row['ZHANDUAN_SCORE_NOT_JL2'],
                math.pow(2, add_luju_zhanduan_level - 1)
            ]
    return []


def _export_detail_concentartion_ratio_of_evaluation(
        data, zhanduan_dpid_data, months_ago, mon, risk_type):
    """导出评价集中度中间过程
    """
    data = pd.merge(
        zhanduan_dpid_data,
        data,
        how='inner',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(["DEPARTMENT_ID_x", "DEPARTMENT_ID_y"], inplace=True, axis=1)
    rst_data = []
    data = data.groupby(['TYPE3'])
    for idx, val in data:
        content = '以下人员得分情况：'
        for each in val.values:
            content += f'<br/>{each[7]}: 得分 {each[6]}' \
                       f'<br/>本月前累计分  {each[2]}' \
                       f'<br/>本月路局评价分  {each[3]}' \
                       f'<br/>本月站段评级分  {each[4]}' \
                       f'<br/>本月站段评价分（不含JL-2）  {each[5]}'
        rst_data.append({
            'TYPE': 2,
            'MAIN_TYPE': 2,
            'DETAIL_TYPE': 7,
            'MON': mon,
            'DEPARTMENT_ID': each[8],
            'HIERARCHY': 3,
            'MAJOR': each[1],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'CONTENT': content,
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 2, 7, risk_type=risk_type)


def _export_detail_concentartion_ratio_of_evaluation_two(
        data, zhanduan_dpid_data, months_ago, mon, risk_type):
    """导出评价集中度中间过程
    """

    zhanduan_data = zhanduan_dpid_data.rename(
        columns={"DEPARTMENT_ID": "TYPE3"})
    data = pd.merge(
        zhanduan_data,
        data,
        how='left',
        left_on='TYPE3',
        right_on='TYPE3')
    data.fillna(0, inplace=True)
    data.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data = data.groupby(['TYPE3'])
    rst_data = []

    for idx, val in data:
        content = '以下人员得分情况：'
        for each in val.values:
            if each[7] == 0:
                content += f'<br/>本月无因站段评价造成单人处置方式升级人数'

            else:
                content += f'<br/>{each[8]}: 得分 {each[7]}' \
                           f'<br/>本月前累计分  {each[3]}' \
                           f'<br/>本月路局评价分  {each[4]}' \
                           f'<br/>本月站段评级分  {each[5]}' \
                           f'<br/>本月站段评价分（不含JL-2）  {each[6]}'

        rst_data.append({
            'TYPE': 2,
            'MAIN_TYPE': 2,
            'DETAIL_TYPE': 7,
            'MON': mon,
            'DEPARTMENT_ID': each[0],
            'HIERARCHY': 3,
            'MAJOR': each[2],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'CONTENT': content,
        })

    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 2, 7, risk_type=risk_type)


# 评价集中度
def stats_concentartion_ratio_of_evaluation(
        accumulative_evaluate_score_sql,
        luju_evaluate_score_sql,
        zhanduan_evaluate_score_sql,
        zhanduan_evaluate_score_not_jl2_sql,
        person_id_card_sql,
        department_data,
        zhanduan_dpid_data,
        months_ago,
        risk_type,
        choose_dpid_data=None,
        risk_ids=None):
    """评价集中度计：单人比较，因站段评价造成单人处置方式升级的1人+N分。
    （累计积分2、4、6、8、10、12为升级档次）

    Arguments:
        months_ago {[type]} -- [description]
    """
    stats_month = get_custom_month(months_ago)
    this_year, this_mon = int(stats_month[1][:4]), int(stats_month[1][5:7])
    this_month_end_time = stats_month[1]
    this_month_start_time = stats_month[0]
    this_year_start_time = f'{this_year - 1}-12-{this_month_end_time[-2:]}'

    data = pd.concat(
        [
            pd_query(
                accumulative_evaluate_score_sql.format(
                    this_month_start_time, this_year_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                luju_evaluate_score_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                zhanduan_evaluate_score_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                zhanduan_evaluate_score_not_jl2_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD')
        ],
        axis=1,
        join='outer',
        sort=True)
    data.fillna(0, inplace=True)
    person_score = []
    for idx, row in data.iterrows():
        score = _calc_score_because_zhanduan_evaluate(idx, row)
        if score != []:
            person_score.append(score)
    df_pdata = pd.merge(
        pd.DataFrame(
            data=person_score,
            columns=[
                'ID_CARD', 'BEFORE_SCORE', 'LUJU_SCORE', 'ZHANDUAN_SCORE',
                'ZHANDUAN_SCORE_NOT_JL2', 'SCORE'
            ]),
        pd_query(person_id_card_sql),
        how='left',
        left_on='ID_CARD',
        right_on='ID_CARD')
    df_pdata.drop(columns=['ID_CARD'], inplace=True)
    df_pdata = df_merge_with_dpid(df_pdata, department_data)
    _export_detail_concentartion_ratio_of_evaluation(
        df_pdata.copy(), zhanduan_dpid_data, months_ago,
        this_year * 100 + this_mon, risk_type)
    rst_child_score = calc_child_index_type_sum(
        df_pdata, 2, 2, 7, months_ago, 'SCORE', 'SCORE_g',
        lambda x: min(100, x), choose_dpid_data, risk_type=risk_type)
    return rst_child_score


# 评价集中度
def stats_concentartion_ratio_of_evaluation_two(
        accumulative_evaluate_score_sql,
        luju_evaluate_score_sql,
        zhanduan_evaluate_score_sql,
        zhanduan_evaluate_score_not_jl2_sql,
        person_id_card_sql,
        department_data,
        zhanduan_dpid_data,
        months_ago,
        risk_type,
        choose_dpid_data=None,
        risk_ids=None):
    """评价集中度计：单人比较，因站段评价造成单人处置方式升级的1人+N分。
    （累计积分2、4、6、8、10、12为升级档次）---修改无数据前端中间过程的问题

    Arguments:
        months_ago {[type]} -- [description]
    """
    stats_month = get_custom_month(months_ago)
    this_year, this_mon = int(stats_month[1][:4]), int(stats_month[1][5:7])
    this_month_end_time = stats_month[1]
    this_month_start_time = stats_month[0]
    this_year_start_time = f'{this_year - 1}-12-{this_month_end_time[-2:]}'

    data = pd.concat(
        [
            pd_query(
                accumulative_evaluate_score_sql.format(
                    this_month_start_time, this_year_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                luju_evaluate_score_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                zhanduan_evaluate_score_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD'),
            pd_query(
                zhanduan_evaluate_score_not_jl2_sql.format(
                    this_month_end_time, this_month_start_time,
                    this_year, risk_ids)).set_index('ID_CARD')
        ],
        axis=1,
        join='outer',
        sort=True)
    data.fillna(0, inplace=True)
    person_score = []
    for idx, row in data.iterrows():
        score = _calc_score_because_zhanduan_evaluate(idx, row)
        if score != []:
            person_score.append(score)
    df_pdata = pd.merge(
        pd.DataFrame(
            data=person_score,
            columns=[
                'ID_CARD', 'BEFORE_SCORE', 'LUJU_SCORE', 'ZHANDUAN_SCORE',
                'ZHANDUAN_SCORE_NOT_JL2', 'SCORE'
            ]),
        pd_query(person_id_card_sql),
        how='left',
        left_on='ID_CARD',
        right_on='ID_CARD')

    df_pdata.drop(columns=['ID_CARD'], inplace=True)
    df_pdata = df_merge_with_dpid(df_pdata, department_data)

    data = pd.merge(
        zhanduan_dpid_data,
        df_pdata,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    # data.drop(["DEPARTMENT_ID_x", "DEPARTMENT_ID_y"], inplace=True, axis=1)
    data.drop(["TYPE3", "DEPARTMENT_ID_y"], inplace=True, axis=1)
    data.rename(columns={"DEPARTMENT_ID_x": "TYPE3"}, inplace=True)

    # df_ata = df_merge_with_dpid(df_pdata, department_data)
    # df_pdata = df_ata
    # df_pdata.fillna(0, inplace=True)
    _export_detail_concentartion_ratio_of_evaluation_two(
        df_pdata.copy(), zhanduan_dpid_data, months_ago,
        this_year * 100 + this_mon, risk_type)
    #
    # rst_child_score = calc_child_index_type_sum(
    #     df_pdata, 2, 2, 7, months_ago, 'SCORE', 'SCORE_g',
    #     lambda x: min(100, x), choose_dpid_data, risk_type=risk_type)

    rst_child_score = calc_child_index_type_sum(
        data, 2, 2, 7, months_ago, 'SCORE', 'SCORE_g',
        lambda x: min(100, x), choose_dpid_data, risk_type=risk_type)
    return rst_child_score


# 供电-履职评价占比
def stats_evaluate_ratio_type_gongdian(
        score_part,
        score_all,
        months_ago,
        risk_type,
        choose_dpid_data=None,
        customizecontent=None,
        major_ratio=None,
        calc_score_by_formula=_calc_score_by_formula):
    """评价记分（因施工风险被评价）/当月评价总分
    与对应当月专业平均数比较，a=（比值-对应平均数）/对应平均数，
    根据a值不同按以下规则计算，
    a≥30%，得分100；
    30%>a，得分70+a*100，最低为0。
    """
    # 各个站段的分数
    return calc_child_index_type_divide_major(
        score_part,
        score_all,
        2,
        2,
        8,
        months_ago,
        'COUNT',
        'SCORE_h',
        calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        customizecontent=customizecontent,
        major_ratio=major_ratio,
    )


def _calc_evaluate_count_difference_type_gongdian(differce_count):
    if differce_count >= 3:
        return 100
    elif differce_count >= 1:
        return 80
    elif differce_count >= 0:
        return 70
    else:
        return max(0, 70 + 5 * differce_count)


# 供电-站段路局评价分数差异
def stats_evaluate_count_difference_type_gongdian(count_zhanduan,
                                                  count_luju,
                                                  months_ago,
                                                  risk_type,
                                                  choose_dpid_data=None,
                                                  customizecontent=None):
    """站段劳安问题评价分与路局对段劳安问题评价分相减（40%），
    ≥3，得100分，
    ≥1，得80分，
    ≥0得70分，
    小于路局一分每个扣5分
    """
    count_zhanduan = count_zhanduan.groupby([f'TYPE3'])['COUNT'].sum()
    count_luju = count_luju.groupby([f'TYPE3'])['COUNT'].sum()
    difference_count = pd.concat(
        [
            count_zhanduan.to_frame(name='zhanduan'),
            count_luju.to_frame(name='luju')
        ],
        axis=1,
        sort=False)
    difference_count.fillna(0, inplace=True)
    difference_count[
        'SCORE'] = difference_count['zhanduan'] - difference_count['luju']
    data = difference_count.reset_index(0)
    data.rename(columns={'index': 'TYPE3'}, inplace=True)

    mg_data = data.copy()

    mg_data.rename(columns={'TYPE3': 'DEPARTMENT_ID',
                            'zhanduan': 'ZD_COUNT', 'luju': 'LJ_COUNT'}, inplace=True)
    mon = get_history_months(months_ago)[0]
    mg_data['MON'] = mon
    mg_data['HIERARCHY'] = 3
    mg_data['MAIN_TYPE'] = 2
    mg_data['DETAIL_TYPE'] = 10
    mg_data['DIFF_COUNT'] = mg_data['SCORE']
    mg_data['SCORE'] = mg_data['SCORE'].apply(
        _calc_evaluate_count_difference_type_gongdian)
    mg_data['MAJOR'] = choose_dpid_data(3)['MAJOR']

    mg_data['RANK'] = mg_data['SCORE'].groupby(
        mg_data['MON']).rank(ascending=0, method='first')
    mg_data['AVG_SCORE'] = mg_data['SCORE'].mean()
    my_data = []
    for idx, row in mg_data.iterrows():
        tmp_dict = dict()
        if customizecontent is None:
            tmp_dict['TYPE'] = 1
        elif isinstance(customizecontent, list):
            tmp_dict['TYPE'] = 3
            tmp_dict['CONTENT'] = customizecontent[0]
        else:
            tmp_dict['TYPE'] = 2
            tmp_dict['CONTENT'] = customizecontent.format(round(row['SCORE'], 2), int(row['RANK']),
                                                          round(row['AVG_NUMBER'], 4), round(
                    row['DIFF_COUNT'], 4),
                                                          round(row['ZD_COUNT'], 2), round(
                    row['LJ_COUNT'], 2),
                                                          round(row['AVG_SCORE'], 2))

        tmp_dict['DEPARTMENT_ID'] = row['DEPARTMENT_ID']
        tmp_dict['MAJOR'] = row['MAJOR']
        tmp_dict['MON'] = mon
        tmp_dict['HIERARCHY'] = 3
        tmp_dict['INDEX_TYPE'] = int(risk_type.split('-')[1])
        tmp_dict['MAIN_TYPE'] = 2
        tmp_dict['DETAIL_TYPE'] = 10
        tmp_dict['NUMERATOR'] = row['ZD_COUNT']
        tmp_dict['DENOMINATOR'] = round(row['LJ_COUNT'], 2)
        tmp_dict['QUOTIENT'] = round(row['DIFF_COUNT'], 4)
        tmp_dict['AVG_QUOTIENT'] = round(row['AVG_SCORE'], 2)
        tmp_dict['AVG_SCORE'] = round(row['AVG_SCORE'], 2)
        tmp_dict['SCORE'] = round(row['SCORE'], 2)
        tmp_dict['RANK'] = int(row['RANK'])
        my_data.append(tmp_dict)
    # 导出中间计算结果
    write_export_basic_data_to_mongo(my_data, months_ago, 3, 2, 10, risk_type)

    data.drop(columns=['zhanduan', 'luju'], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        2,
        10,
        months_ago,
        'SCORE',
        'SCORE_j',
        _calc_evaluate_count_difference_type_gongdian,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# 评价得分
def stats_score_per_dp(
        evaluate_people_count,
        zhanduan_evaluate_people_count,
        months_ago,
        _calc_score_by_formula,
        risk_type,
        choose_dpid_data=None,
        zhanduan_filter_list=[],
        customizecontent=None,
        major_ratio=None):
    """三个月被评价人次数量/近三个月该段所有评价人次总数"""
    return calc_child_index_type_divide_major(
        evaluate_people_count,
        zhanduan_evaluate_people_count,
        2,
        2,
        11,
        months_ago,
        'COUNT',
        'SCORE_k',
        _calc_score_by_formula,
        choose_dpid_data,
        risk_type=risk_type,
        zhanduan_filter_list=zhanduan_filter_list,
        customizecontent=customizecontent,
        major_ratio=major_ratio,
    )


# '履职评价条数差（站段与路局）'
def stats_evaluate_difference_count(active_evaluate_count,
                                    common_evaluate_count,
                                    luju_evaluate_count,
                                    common_luju_evaluate_count,
                                    months_ago, risk_type, choose_dpid_data,
                                    calc_func=_calc_evaluate_count_difference_type,
                                    nocadre_contained=True):
    count_zhanduan_ganbu = active_evaluate_count.groupby([f'TYPE3'])[
        'COUNT'].sum()
    count_zhanduan_feiganbu = common_evaluate_count.groupby([f'TYPE3'])[
        'COUNT'].sum()
    count_luju_ganbu = luju_evaluate_count.groupby([f'TYPE3'])['COUNT'].sum()
    count_luju_feiganbu = common_luju_evaluate_count.groupby([f'TYPE3'])[
        'COUNT'].sum()
    dpid_data = choose_dpid_data(3).groupby([f'DEPARTMENT_ID'])['NAME'].sum()
    difference_count = pd.concat(
        [
            count_zhanduan_ganbu.to_frame(name='zhanduan_ganbu'),
            count_zhanduan_feiganbu.to_frame(name='zhanduan_feiganbu'),
            count_luju_ganbu.to_frame(name='luju_ganbu'),
            count_luju_feiganbu.to_frame(name='luju_feiganbu'),
            dpid_data.to_frame(name='NAME'),
        ],
        axis=1,
        sort=False)
    difference_count.fillna(0, inplace=True)
    if nocadre_contained:
        difference_count['SCORE'] = difference_count['zhanduan_ganbu'] + 0.2 * difference_count['zhanduan_feiganbu'] \
                                    - (difference_count['luju_ganbu'] + 0.2 *
                                       difference_count['luju_feiganbu'])
    else:
        difference_count['SCORE'] = difference_count['zhanduan_ganbu'] - difference_count['luju_ganbu']
    data = difference_count.reset_index(0)
    data.rename(columns={'index': 'TYPE3'}, inplace=True)

    mg_data = data.copy()

    # mg_data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'zhanduan': 'ZD_COUNT', 'luju': 'LJ_COUNT'}, inplace=True)
    mg_data.rename(columns={'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    mon = get_history_months(months_ago)[0]
    mg_data['MON'] = mon
    mg_data['HIERARCHY'] = 3
    mg_data['MAIN_TYPE'] = 2
    mg_data['DETAIL_TYPE'] = 9
    # mg_data['DIFF_COUNT'] = mg_data['SCORE']
    mg_data['DIFF_COUNT'] = abs(mg_data['zhanduan_ganbu'] + mg_data['zhanduan_feiganbu'] - \
                                mg_data['luju_feiganbu'] - mg_data['luju_ganbu'])
    mg_data['SCORE'] = mg_data['SCORE'].apply(
        calc_func)

    mg_data['RANK'] = mg_data['SCORE'].groupby(
        mg_data['MON']).rank(ascending=0, method='first')
    mg_data['AVG_SCORE'] = mg_data['SCORE'].mean()
    my_data = []
    for idx, row in mg_data.iterrows():
        if nocadre_contained:
            content = '履职评价条数差（站段与路局）({0}), \n 干部履职评价条数:({1})（站段）/({2})（路局）,\n' \
                      '非干部履职评价条数:({3})（站段）/({4})（路局）'
        else:
            content = '履职评价条数差（站段与路局）({0}), \n 干部履职评价条数:({1})（站段）/({2})（路局）.'
        content = content.format(row['DIFF_COUNT'], row['zhanduan_ganbu'], row['luju_ganbu'], row['zhanduan_feiganbu'],
                                 row['luju_feiganbu'])
        my_data.append({
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'TYPE': 2,
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'MON': mon,
            'HIERARCHY': 3,
            'MAIN_TYPE': 2,
            'DETAIL_TYPE': 9,
            'CONTENT': content
        })

    # 导出中间计算结果
    write_export_basic_data_to_mongo(my_data, months_ago, 3, 2, 9, risk_type)

    data.drop(columns=['zhanduan_ganbu', 'zhanduan_feiganbu',
                       'luju_ganbu', 'luju_feiganbu', "NAME"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        2,
        9,
        months_ago,
        'SCORE',
        'SCORE_i',
        calc_func,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


# '履职评价分数差（站段与路局）'
def stats_evaluate_difference_score(evaluate_score_all,
                                    common_evaluate_score_all,
                                    luju_evaluate_score,
                                    common_luju_evaluate_score,
                                    months_ago, risk_type, choose_dpid_data,
                                    calc_func=_calc_evaluate_count_difference_type,
                                    nocadre_contained=True):
    score_zhanduan_ganbu = evaluate_score_all.groupby([f'TYPE3'])[
        'COUNT'].sum()
    score_zhanduan_feiganbu = common_evaluate_score_all.groupby([f'TYPE3'])[
        'COUNT'].sum()
    score_luju_ganbu = luju_evaluate_score.groupby([f'TYPE3'])['COUNT'].sum()
    score_luju_feiganbu = common_luju_evaluate_score.groupby([f'TYPE3'])[
        'COUNT'].sum()
    dpid_data = choose_dpid_data(3).groupby([f'DEPARTMENT_ID'])['NAME'].sum()

    difference_count = pd.concat(
        [
            score_zhanduan_ganbu.to_frame(name='zhanduan_ganbu'),
            score_zhanduan_feiganbu.to_frame(name='zhanduan_feiganbu'),
            score_luju_ganbu.to_frame(name='luju_ganbu'),
            score_luju_feiganbu.to_frame(name='luju_feiganbu'),
            dpid_data.to_frame(name='NAME'),
        ],
        axis=1,
        sort=False)
    difference_count.fillna(0, inplace=True)
    if nocadre_contained:
        difference_count['SCORE'] = difference_count['zhanduan_ganbu'] + 0.2 * difference_count['zhanduan_feiganbu'] \
                                    - (difference_count['luju_ganbu'] + 0.2 *
                                       difference_count['luju_feiganbu'])
    else:
        difference_count['SCORE'] = difference_count['zhanduan_ganbu'] - difference_count['luju_ganbu']
    data = difference_count.reset_index(0)
    data.rename(columns={'index': 'TYPE3'}, inplace=True)

    mg_data = data.copy()

    # mg_data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'zhanduan': 'ZD_COUNT', 'luju': 'LJ_COUNT'}, inplace=True)
    mg_data.rename(columns={'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    mon = get_history_months(months_ago)[0]
    mg_data['MON'] = mon
    mg_data['HIERARCHY'] = 3
    mg_data['MAIN_TYPE'] = 2
    mg_data['DETAIL_TYPE'] = 10
    mg_data['DIFF_COUNT'] = abs(mg_data['zhanduan_ganbu'] + mg_data['zhanduan_feiganbu'] - \
                                mg_data['luju_feiganbu'] - mg_data['luju_ganbu'])
    mg_data['SCORE'] = mg_data['SCORE'].apply(
        calc_func)

    mg_data['RANK'] = mg_data['SCORE'].groupby(
        mg_data['MON']).rank(ascending=0, method='first')
    mg_data['AVG_SCORE'] = mg_data['SCORE'].mean()
    my_data = []
    for idx, row in mg_data.iterrows():
        if nocadre_contained:
            content = '履职评价分数差（站段与路局）({0}),\n 干部履职评价分数:({1})（站段）/({2})（路局）,\n' \
                      '非干部履职评价分数:({3})（站段）/({4})（路局）'
            content = content.format(round(row['DIFF_COUNT'], 2), round(row['zhanduan_ganbu'], 2), row['luju_ganbu'],
                                     row['zhanduan_feiganbu'], row['luju_feiganbu'])
        else:
            content = '履职评价分数差（站段与路局）({0}),\n 干部履职评价分数:({1})（站段）/({2})（路局）.'
            content = content.format(round(row['DIFF_COUNT'], 2), round(row['zhanduan_ganbu'], 2), row['luju_ganbu'])
        my_data.append({
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'TYPE': 2,
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'MON': mon,
            'HIERARCHY': 3,
            'MAIN_TYPE': 2,
            'DETAIL_TYPE': 10,
            'CONTENT': content
        })

    # 导出中间计算结果
    write_export_basic_data_to_mongo(my_data, months_ago, 3, 2, 10, risk_type)

    difference_count['SCORE'] = difference_count['SCORE'].apply(
        calc_func)
    data.drop(columns=['zhanduan_ganbu', 'zhanduan_feiganbu',
                       'luju_ganbu', 'luju_feiganbu', "NAME"], inplace=True, axis=1)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        2,
        10,
        months_ago,
        'SCORE',
        'SCORE_j',
        calc_func,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


if __name__ == '__main__':
    pass
