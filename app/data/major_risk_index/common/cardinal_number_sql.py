# -*- coding: utf-8 -*-

"""
责任事故的判定，需要这个责任单位的责任等级为 全部责任、主要责任、同等主要责任、重要责任、同等重要责任
"""
VIRTUAL_BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_unit AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND c.TYPE3 in {2}
        AND a.MAIN_TYPE in (1, 2)
        AND b.RESPONSIBILITY_IDENTIFIED in (1, 2, 3, 4, 5)
"""


BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_unit AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND c.TYPE2 = {2}
        AND a.MAIN_TYPE in (1, 2)
        AND b.RESPONSIBILITY_IDENTIFIED in (1, 2, 3, 4, 5)
"""