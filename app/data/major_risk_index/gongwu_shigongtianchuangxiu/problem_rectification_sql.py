#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019-06-20
-------------------------------------------------
Change Activity:2019-06-20 16:56
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    HAPPEN_PROBLEM_POINT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL)

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_NUMBER_SQL = """SELECT
        distinct(a.pk_id), b.FK_DEPARTMENT_ID, CODE, 1 AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
        INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND b.IDENTITY = '干部'
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.fk_risk_id in ({2})
"""
