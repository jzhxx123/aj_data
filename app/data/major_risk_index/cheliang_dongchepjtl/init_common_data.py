#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.cheliang_dongchepjtl import GLV
from app.data.major_risk_index.cheliang_dongchepjtl.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL,
    EXTERNAL_PERSON_SQL, DONGCHE_WORK_LOAD_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.cheliang_dongchepjtl.common import get_vitual_major_ids
import pandas as pd
from app.data.major_risk_index.util import (df_merge_with_dpid)


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    ids = get_vitual_major_ids("动车")
    major = get_major_dpid(risk_type)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))

    # 动车 正式职工
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA)
    # 动车外聘人员数 - 站段
    EXTERNAL_PERSON = pd_query(EXTERNAL_PERSON_SQL.format(ids))
    # 动车工作量
    WORK_LOAD = df_merge_with_dpid(
        pd_query(DONGCHE_WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA
    )
    # 动车总人数
    PERSON_LOAD = pd.concat([STAFF_NUMBER, EXTERNAL_PERSON], axis=0, sort=False)
    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "WORK_LOAD": WORK_LOAD,
        "PERSON_LOAD": PERSON_LOAD,
        "STAFF_NUMBER": STAFF_NUMBER,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
