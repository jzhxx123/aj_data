# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
import pandas as pd
from flask import current_app
from app.data.major_risk_index.cheliang_dongchepjtl import GLV
from app.data.index.util import (
    get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.cheliang_dongchepjtl.common_sql import (
    WORKER_COUNT_SQL)
from app.data.major_risk_index.cheliang_pjtl.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, REPEATE_HAPPEN_PROBLEM_SQL,
    HAPPEN_PROBLEM_POINT_SQL, MAJOR_PROBLEM_POINT_INFO_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.cheliang_dongchepjtl.common import get_vitual_major_ids
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        WORKER_COUNT, MAJOR_PROBLEM_POINT_INFO_DATA
    ids = get_vitual_major_ids("动车")
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    MAJOR_PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(ids))
    WORKER_COUNT = pd_query(WORKER_COUNT_SQL.format(ids))

# 问题整改
def _stats_rectification_overdue(months_ago):
    overdue_problem_number_sql = OVERDUE_PROBLEM_NUMBER_SQL.format('{0}', '{1}', '{2}', CHECK_ITEM_IDS)
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, overdue_problem_number_sql, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=5)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=16,
        calc_func=lambda x: min(100, 50 + x))


def _stats_repeatedly_index(months_ago):
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20
    }
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
    #     problem_ctl_threshold_dict=problem_ctl_threshold_dict)
    repeate_happen_problem_sql = REPEATE_HAPPEN_PROBLEM_SQL.format('{0}', '{1}', '{2}', CHECK_ITEM_IDS)
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, 
        CHECK_RISK_IDS, _choose_dpid_data, WORKER_COUNT,
        repeate_happen_problem_sql, MAJOR_PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.25, 0.05, 0.7]
    update_major_maintype_weight(index_type=9, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
