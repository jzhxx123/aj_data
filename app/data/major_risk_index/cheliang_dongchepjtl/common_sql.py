# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            LEFT JOIN
        t_department_classify_config AS c
        ON a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = c.PK_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {1}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.TYPE3 in {1}
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != "";
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
a.DEPARTMENT_ID,
a.NAME,
a.TYPE3 AS PARENT_ID,
c.NAME AS MAJOR
FROM
t_department AS a
    INNER JOIN
t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    INNER JOIN
t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
WHERE
a.TYPE = 8 AND a.IS_DELETE = 0
    AND b.SHORT_NAME != ''
    AND c.DEPARTMENT_ID = '{0}'
    AND a.TYPE3 in {1}
"""

# 动车正式职工人数
WORK_LOAD_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID, a.MANAGEMENT_INDEX as COUNT
FROM
    t_department_other_management AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE3 in {0}
    AND a.FK_TYPE_ID = 2465
"""

# 动车工作量
DONGCHE_WORK_LOAD_SQL= """
SELECT 
    a.FK_DEPARTMENT_ID, a.MANAGEMENT_INDEX as COUNT
FROM
    t_department_other_management AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE3 in {0}
    AND a.FK_TYPE_ID = 2470
"""

# 动车外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT 
    b.TYPE3 as TYPE3, a.MANAGEMENT_INDEX as COUNT
FROM
    t_department_other_management AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE3 in {0}
    AND a.FK_TYPE_ID = 2466
"""

# 量化人员数量,去除各项指标（包括基础指标和细化指标）都为零的
QUANTIZATION_PERSON_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_quantization_base_quota AS a
            INNER JOIN
        t_person AS b ON b.ID_CARD = a.ID_CARD
    WHERE
        a.YEAR = {0} AND a.MONTH = {1}
            AND a.`STATUS` = 1
            AND (a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_base_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(CHECK_TIMES_TOTAL, 0)
                    + IFNULL(PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_NUMBER_TOTAL, 0)
                    + IFNULL(WORK_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(CHECK_NOTIFICATION_TIMES_TOTAL, 0)
                    + IFNULL(MIN_QUALITY_GRADES_TOTAL, 0)
                    + IFNULL(HIDDEN_DANGER_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(RISK_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL, 0) 
                    ) > 0
                )
            OR a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_refinement_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_TIME_TOTAL, 0)
                    ) > 0
                )
            )
    GROUP BY a.FK_DEPARTMENT_ID
"""

# 干部主动评价记分总分数
ACTIVE_EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE3 in {0}
    and b.is_delete = 0
    and a.is_delete = 0
"""

# 单位职工数
WORKER_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
            AND b.is_delete = 0
    GROUP BY a.FK_DEPARTMENT_ID;
"""
