# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.major_risk_index.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_POINT_SQL, KAOHE_PROBLEM_SQL, REAL_CHECK_BANZU_SQL,
    REAL_CHECK_POINT_SQL, YECHA_CHECK_SQL)
from app.data.index.common import (calc_child_index_type_divide,
                                   combine_child_index_func,
                                   df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.common_sql import (
    CHECK_COUNT_SQL, CHECK_PROBLEM_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    PROBLEM_CHECK_SCORE_SQL, QUANTIZATION_PERSON_SQL, RISK_LEVEL_PROBLEM_SQL,
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 6
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


# 统计干部量化率
def _stats_quantization_ratio(months_ago):
    '''
    量化率：量化人员数/正式职工总数。值越高检查力度越大。
    '''
    return calc_child_index_type_divide(
        QUANTIZATION_PERSON,
        WORK_LOAD,
        2,
        1,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 人均检查频次
def _stats_check_per_person(months_ago):
    return calc_child_index_type_divide(
        CHECK_COUNT,
        WORK_LOAD,
        2,
        1,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def _stats_check_problem_ratio(months_ago):
    return calc_child_index_type_divide(
        CHECK_COUNT,
        PROBLEM_COUNT,
        2,
        1,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    return calc_child_index_type_divide(
        ASSESS_PROBLEM_COUNT,
        PROBLEM_COUNT,
        2,
        1,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 问题平均质量分
def _stats_score_per_check_problem(months_ago):
    return calc_child_index_type_divide(
        PROBLEM_SCORE,
        PROBLEM_COUNT,
        2,
        1,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 人均质量分
def _stats_score_per_person(months_ago):
    return calc_child_index_type_divide(
        PROBLEM_SCORE,
        QUANTIZATION_PERSON,
        2,
        1,
        6,
        months_ago,
        'COUNT',
        'SCORE_f',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 夜查率
def _stats_yecha_ratio(months_ago):
    return calc_child_index_type_divide(
        YECHA_COUNT,
        CHECK_COUNT,
        2,
        1,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 监控调阅人均时长
def _stats_media_time_per_person():
    pass


# 监控调阅人均问题个数
def _stats_media_problem_per_person():
    pass


# 监控调阅人均质量分
def _stats_media_score_per_person():
    pass


# 较大及以上风险问题占比
def _stats_yiban_risk_ratio(months_ago):
    return calc_child_index_type_divide(
        YIBAN_RISK_COUNT,
        PROBLEM_COUNT,
        2,
        1,
        8,
        months_ago,
        'COUNT',
        'SCORE_h',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    stats_month = get_custom_month(months_ago)
    # 检查地点数
    data_real = pd.concat(
        [
            pd_query(REAL_CHECK_POINT_SQL.format(*stats_month)),
            pd_query(REAL_CHECK_BANZU_SQL.format(*stats_month)),
        ],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, DEPARTMENT_DATA)

    # 地点总数
    data_total = pd.concat(
        [
            pd_query(CHECK_POINT_SQL),
            pd_query(BANZU_POINT_SQL),
        ],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, DEPARTMENT_DATA)
    return calc_child_index_type_divide(
        data_real,
        data_total,
        2,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        lambda x: x * 100,
        _choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type=RISK_TYPE)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, QUANTIZATION_PERSON, CHECK_COUNT, PROBLEM_COUNT, \
        ASSESS_PROBLEM_COUNT, PROBLEM_SCORE, YECHA_COUNT, YIBAN_RISK_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 量化人员数
    QUANTIZATION_PERSON = df_merge_with_dpid(
        pd_query(
            QUANTIZATION_PERSON_SQL.format(year, month, diaoche_position)),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 一般及以上问题数
    YIBAN_RISK_COUNT = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_quantization_ratio, _stats_check_per_person,
        _stats_check_problem_assess_radio, _stats_score_per_check_problem,
        _stats_score_per_person, _stats_yecha_ratio, _stats_yiban_risk_ratio,
        _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['a', 'b', 'd', 'e', 'f', 'g', 'h', 'i']
    ]
    item_weight = [0.07, 0.1, 0.15, 0.15, 0.1, 0.2, 0.09, 0.14]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd', 'f', 'h', 'i']]
    # item_weight = [0.25, 0.2, 0.08, 0.25, 0.9, 0.13]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 1, months_ago,
    #                      item_name, item_weight, [4])
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
