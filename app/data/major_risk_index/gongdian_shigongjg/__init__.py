# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/8/20上午10:20
   Change Activity: 2019/8/20上午10:20
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import combine_child_index
