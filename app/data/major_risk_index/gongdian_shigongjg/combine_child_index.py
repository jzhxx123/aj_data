# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/8/20上午10:35
   Change Activity: 2019/8/20上午10:35
-------------------------------------------------
"""
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shigongjg import (init_common_data, evaluate_intensity, check_intensity,
                                                          check_evenness, problem_exposure, assess_intensity,
                                                          problem_rectification)
from app.data.major_risk_index.gongdian_shigongjg.common import get_vitual_major_ids

from app.data.major_risk_index.gongdian_shigongjg.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 54
    risk_type = '供电-10'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    child_index_weight = [0.30, 0.15, 0.15, 0.05, 0.15, 0.1]
    child_index_list = [1, 2, 3, 4, 5, 6]
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)

    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(zhanduan_dpid_sql, chejian_dpid_sql,
                                          months_ago, risk_name, risk_type, child_index_weight=child_index_weight,
                                          child_index_list=child_index_list)

    update_major_maintype_weight(10, risk_type,
                                 child_index_list=child_index_list,
                                 child_index_weight=child_index_weight)


if __name__ == '__main__':
    pass
