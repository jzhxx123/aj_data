# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_comom_data
   Author :       hwj
   date：          2019/8/20上午10:22
   Change Activity: 2019/8/20上午10:22
-------------------------------------------------
"""
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shigongjg import GLV
from app.data.major_risk_index.gongdian_shigongjg.common import get_ticket_amount, get_vitual_major_ids
from app.data.major_risk_index.gongdian_shigongjg.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, \
    GD_T_WORK_TICKET_SQL, GD_T_WORK_TICKET_FOR_TRUCK_SQL, GD_BD_WORK_TICKET_SQL
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(major, ids))
    # 工作量(工作票数量)
    work_load = get_ticket_amount(GD_T_WORK_TICKET_SQL,
                                  GD_T_WORK_TICKET_FOR_TRUCK_SQL,
                                  GD_BD_WORK_TICKET_SQL,
                                  department_data,
                                  months_ago)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        "WORK_LOAD": work_load
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)

