# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, '供电' AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        '供电' AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND (c.DEPARTMENT_ID = '{0}' or a.TYPE3 in {1})
"""


# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""


# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or b.TYPE3 in {1})
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""


# STATUS = 4(工作结束), RECEIPT_TIME发票日期
GD_T_WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT ty.PK_ID) as COUNT
    FROM
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(ty.RECEIPT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(ty.RECEIPT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND ty.STATUS = 4
    GROUP BY tu.DEPART_ID
"""

# 作业车工作票 STATUS = 4(工作结束), RECEIPT_TIME发票日期
GD_T_WORK_TICKET_FOR_TRUCK_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT tr.PK_ID) as COUNT
    FROM
        gd_t_work_ticket_for_truck AS tr
            INNER JOIN
        gd_t_unit AS tu ON tr.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(tr.RECEIPT_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(tr.RECEIPT_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND tr.STATUS = 4
    GROUP BY tu.DEPART_ID
"""

#  牵引变电所第一种工作票 STATUS = 5(工作结束), RECEIPT_TIME发票日期
GD_BD_WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT bd.PK_ID) as COUNT
    FROM
        gd_bd_work_ticket AS bd
            INNER JOIN
        gd_t_unit AS tu ON bd.UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND bd.STATUS = 5
    GROUP BY tu.DEPART_ID
"""
