# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/8/20上午10:21
   Change Activity: 2019/8/20上午10:21
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def get_ticket_amount(gd_t_work_ticket_sql, gd_t_work_ticket_for_truck_sql,
                      gd_bd_work_ticket_sql, dpid, months_ago):
    """获取工作票总数"""
    stats_month = get_custom_month(months_ago)
    gd_t_work_ticket = pd_query(gd_t_work_ticket_sql.format(*stats_month), db_name='db_mid')
    gd_ticket_for_truck = pd_query(gd_t_work_ticket_for_truck_sql.format(*stats_month), db_name='db_mid')
    gd_bd_work_ticket = pd_query(gd_bd_work_ticket_sql.format(*stats_month), db_name='db_mid')

    all_ticket = df_merge_with_dpid(
        pd.concat([gd_t_work_ticket, gd_ticket_for_truck, gd_bd_work_ticket]),
        dpid
    )

    return all_ticket
