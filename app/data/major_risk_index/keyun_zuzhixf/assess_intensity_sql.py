from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import(
    GANBU_ASSESS_RESPONSIBLE_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL, NORISK_KAOHE_GL_CHECK_PROBLEM_SQL)
