# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE, a.ALL_NAME
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
            AND a.TYPE3 not in {1}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
            AND a.TYPE3 not in {1}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
            AND a.TYPE3 not in {1}
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""
# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 接发列车数
JIEFALC_COUNT_SQL = """
SELECT
    a.NODE,
    COUNT( 1 ) AS CW_COUNT 
FROM
    (
    SELECT DISTINCT
        NODE,
        CFCC,
        CFSJ,
        DDSJ 
    FROM
        `t_td_data` 
    WHERE
        DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    ) AS a 
GROUP BY
    a.NODE
"""

# 接发列车操作方式
JIEFALC_CTC_WAY_SQL = """
SELECT
c.CTC,
c.FK_DEPARTMENT_ID,
c.NODE 
FROM
(
SELECT
    a.CTC_OPERATION_WAY AS CTC,
    substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
    a.FK_DEPARTMENT_ID 
FROM
    t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 ) 
    inner join 
    t_department as c on a.FK_DEPARTMENT_ID = c.department_id
WHERE 
    a.ACCEPT_DEPARTURE_NUMBER > 0
    AND a.CTC_OPERATION_WAY in (2, 5, 7)
    and c.IS_DELETE =0
    and a.ASSOCIATED_STATION is not null
) AS c
GROUP BY 	
c.NODE,
c.CTC,
c.FK_DEPARTMENT_ID 
"""

# 接发列车作业人数--接发列车人数
JIEFALC_PEOPLE_COUNT_SQL = """
SELECT
a.FK_DEPARTMENT_ID,
a.ACCEPT_DEPARTURE_NUMBER AS NUMBER,
a.ASSOCIATED_STATION as NODE,
CTC_OPERATION_WAY
FROM
t_department_type_cw a
WHERE 
    ASSOCIATED_STATION is not null
    and a.FK_DEPARTMENT_ID IN (
    SELECT 
        distinct a.FK_DEPARTMENT_ID
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
    WHERE
        a.MAIN_TYPE = 1
            AND c.PK_ID  in ({0})
    )
"""

# 接发列车工作量--接发列车数未知
JIEFALC_WORK_LOAD_SQL = """
SELECT FK_DEPARTMENT_ID, ACCEPT_DEPARTURE_NUMBER AS COUNT
    FROM  
    t_department_type_cw  AS a
    LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE b.TYPE2 = '{0}'
"""


# 站段下A类接发列车站点数
A_STATION_COUNT_SQL = """SELECT 
    tw.TYPE3,
    count(1) as A_NODE_COUNT
FROM
(

SELECT
distinct 
c.TYPE3,
substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
1 AS NODE_COUNT
FROM
t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 )
INNER JOIN
t_department as c on a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
c.IS_DELETE=0
and ACCEPT_DEPARTURE_NUMBER > 0
and ASSOCIATED_STATION != ''
and CTC_OPERATION_WAY in (2, 5, 7)
and a.FK_DEPARTMENT_ID IN (
    SELECT 
        distinct a.FK_DEPARTMENT_ID
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
    WHERE
        a.MAIN_TYPE = 1
            AND c.PK_ID  in ({0})
    )
) as tw
GROUP BY tw.TYPE3
"""

# 站段下B类接发列车站点数
B_STATION_COUNT_SQL = """SELECT 
    tw.TYPE3,
    count(1) as B_NODE_COUNT
FROM
(

SELECT
distinct 
c.TYPE3,
substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
1 AS NODE_COUNT
FROM
t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 )
INNER JOIN
t_department as c on a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
c.IS_DELETE=0
and ACCEPT_DEPARTURE_NUMBER > 0
and ASSOCIATED_STATION != ''
and CTC_OPERATION_WAY in (1, 3, 4, 6)
and a.FK_DEPARTMENT_ID IN (
    SELECT 
        distinct a.FK_DEPARTMENT_ID
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
    WHERE
        a.MAIN_TYPE = 1
            AND c.PK_ID  in ({0})
    )
) as tw
GROUP BY tw.TYPE3
"""

# 每个部门关联的A类接发列车站点数
A_DEPARTMENT_STATION_COUNT_SQL = """SELECT 
    tw.DEPARTMENT_ID,
    count(1) as A_DPID_NODE_COUNT
FROM
(
SELECT
distinct 
c.DEPARTMENT_ID,
substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
1 AS NODE_COUNT
FROM
t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 )
INNER JOIN
t_department as c on a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
c.IS_DELETE=0
and ACCEPT_DEPARTURE_NUMBER > 0
and ASSOCIATED_STATION != ''
and CTC_OPERATION_WAY IN (2, 5, 7)
and a.FK_DEPARTMENT_ID IN (
    SELECT 
        distinct a.FK_DEPARTMENT_ID
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
    WHERE
        a.MAIN_TYPE = 1
            AND c.PK_ID  in ({0})
    )
) as tw
GROUP BY tw.DEPARTMENT_ID
"""

# 每个部门关联的B类接发列车站点数
B_DEPARTMENT_STATION_COUNT_SQL = """SELECT 
    tw.DEPARTMENT_ID,
    count(1) as B_DPID_NODE_COUNT
FROM
(
SELECT
distinct 
c.DEPARTMENT_ID,
substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
1 AS NODE_COUNT
FROM
t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 )
INNER JOIN
t_department as c on a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
c.IS_DELETE=0
and ACCEPT_DEPARTURE_NUMBER > 0
and ASSOCIATED_STATION != ''
and CTC_OPERATION_WAY IN (1, 3, 4, 6)
and a.FK_DEPARTMENT_ID IN (
    SELECT 
        distinct a.FK_DEPARTMENT_ID
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
    WHERE
        a.MAIN_TYPE = 1
            AND c.PK_ID  in ({0})
    )
) as tw
GROUP BY tw.DEPARTMENT_ID
"""