#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import current_app

from app.data.major_risk_index.chewu_jiefalc import GLV
from app.data.major_risk_index.chewu_jiefalc.common import get_vitual_major_ids
from app.data.major_risk_index.chewu_jiefalc.evaluate_intensity_sql import ACTIVE_EVALUATE_COUNT_SQL, \
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, EVALUATE_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import (
    WORK_LOAD_SQL)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, ACTIVE_EVALUATE_KEZHI_COUNT, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,\
        CHEJIAN_DPID_DATA, DEPARTMENT_FILTER_LIST

    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    DEPARTMENT_FILTER_LIST = GLV.get_value('DEPARTMENT_FILTER_LIST')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(
            ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    """主动评价记分条数/评价记分总条数*100%,与专业平均数比较"""
    # 各个站段的分数
    return evaluate_intensity.stats_active_ratio(
        ACTIVE_EVALUATE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data, zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    """科职干部评价记分条数（主动）/评价记分总条数*100%，（科职干部包含正科、副科职干部）,与专业平均数比较"""
    return evaluate_intensity.stats_gradation_ratio(
        ACTIVE_EVALUATE_KEZHI_COUNT, TOTAL_EVALUATE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data, zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_active_ratio, _stats_gradation_ratio,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'd']]
    item_weight = [0.8, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=6, major=risk_type, main_type=2, child_index_list=[1, 4],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
