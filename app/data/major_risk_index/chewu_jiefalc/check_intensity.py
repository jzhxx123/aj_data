# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.chewu_jiefalc import GLV
from app.data.major_risk_index.chewu_jiefalc.check_intensity_sql import ZUOYE_CHECK_PROBLEM_SQL, \
    GUANLI_CHECK_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL, \
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLME_SCORE_SQL, MEDIA_PROBLEM_NUMBER_SQL, \
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, YECHA_JIEFALC_COUNT_SQL, CHECK_COUNT_SQL, WORK_BANZU_COUNT_SQL, \
    REAL_CHECK_BANZU_SQL
from app.data.major_risk_index.chewu_jiefalc.common import calc_jiefalc_work_load, \
    get_chewu_check_address_standard_data, calc_jiefalc_count
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH, RISK_IDS, CHECK_ITEM_IDS
    global WORK_LOAD, CHECK_COUNT, GUANLI_PROBLEM_COUNT, \
        ZUOYE_PROBLEM_COUNT, ASSESS_ZUOYE_PROBLEM_COUNT, \
        ASSESS_GUANLI_PROBLEM_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, ASSESS_PROBLEM_COUNT, JIEFALC_PEOPLE_COUNT, \
        REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA, A_STATION_COUNT, B_STATION_COUNT, NODE_CTC, \
        A_DPID_STATION_COUNT, B_DPID_STATION_COUNT, DEPARTMENT_FILTER_LIST
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')

    # 接发列车作业人数
    JIEFALC_PEOPLE_COUNT = GLV.get_value('JIEFALC_PEOPLE_COUNT')
    NODE_CTC = GLV.get_value('NODE_CTC')
    A_STATION_COUNT = GLV.get_value('A_STATION_COUNT')
    B_STATION_COUNT = GLV.get_value('B_STATION_COUNT')
    A_DPID_STATION_COUNT = GLV.get_value('A_DPID_STATION_COUNT')
    B_DPID_STATION_COUNT = GLV.get_value('B_DPID_STATION_COUNT')
    DEPARTMENT_FILTER_LIST = GLV.get_value('DEPARTMENT_FILTER_LIST')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【接发列车工作量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = pd_query(
        GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS))

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 实际检查班组数
    real_check_banzu_data = pd_query(
        REAL_CHECK_BANZU_SQL.format(
            *stats_month, CHECK_ITEM_IDS))

    # 工作班组信息
    work_banzu_info_data = pd_query(WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA = get_chewu_check_address_standard_data(
        work_banzu_info_data,
        real_check_banzu_data,
        DEPARTMENT_DATA, months_ago, CHECK_ITEM_IDS, major, is_base_item=True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    """现场检查接发列车作业检查次数/接发列车工作量"""
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    """接发列车问题数（作业项）/接发列车工作量(20%)
        接发列车问题数（专业管理项+设备质量）/接发列车工作量(80%)
    """
    guanli_problem_count = pd.merge(
        GUANLI_PROBLEM_COUNT,
        DEPARTMENT_DATA,
        how='right',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    guanli_problem_count.fillna(0, inplace=True)

    return check_intensity.stats_check_problem_ratio_type_two_type_chewu(
        ZUOYE_PROBLEM_COUNT,
        guanli_problem_count,
        WORK_LOAD,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """接发列车较大和重大安全风险问题（作业项+专业管理项+设备质量）质量分累计/接发列车工作量()60%
    现场检查接发列车较大及以上安全风险问题（作业项+专业管理项+设备质量）质量分累计/接发列车工作量(40%)"""
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 人均质量分
def _stats_score_per_person(months_ago):
    """接发列车（作业项+专业管理项+设备质量）问题质量分累计/接发列车工作量"""
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST
    )


# 夜查率
def _stats_yecha_ratio(months_ago):
    """现场检查接发列车夜查次数（22:00-6:00，时间段内的检查不少于30分钟）/∑站段管内各站“夜间（22:00-6:00）接发列车工作量"""
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                       "夜查次数({4})/ 接发列车夜间工作量({5})*100%</p>"
    stats_month = get_custom_month(months_ago)
    # 获取接发列车数(聚合到部门)
    yecha_jiefalc_count = pd_query(YECHA_JIEFALC_COUNT_SQL.format(*stats_month), db_name='db_mid')
    yecha_jiefalc_count = calc_jiefalc_count(yecha_jiefalc_count, NODE_CTC)
    data = calc_jiefalc_work_load(
        yecha_jiefalc_count, JIEFALC_PEOPLE_COUNT,
        A_STATION_COUNT, B_STATION_COUNT,
        A_DPID_STATION_COUNT, B_DPID_STATION_COUNT,
        DEPARTMENT_DATA, months_ago)[0]
    data.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    data = data[['FK_DEPARTMENT_ID', 'COUNT']]

    yecha_work_load = df_merge_with_dpid(
        data,
        DEPARTMENT_DATA)
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        yecha_work_load,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    """监控调阅接发列车时长累计/接发列车工作量(20%)
    监控调阅发现接发列车问题数/接发列车工作量(30%)
    监控调阅发现接发列车问题质量分累计/接发列车工作量(40%)
    调阅接发列车班组数/有接发列车作业班组数(10%)"""
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month, CHECK_ITEM_IDS),
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(CHECK_ITEM_IDS)
    ]
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        title=['监控调阅时长累计({0})/工作量({1})',
               '监控调阅发现问题数({0})/工作量({1})', '监控调阅发现问题质量分累计({0})/工作量({1})',
               '调阅班组数({0})/班组数({1})'],
        child_weight=[0.2, 0.3, 0.4, 0.1],
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """检查接发列车班组数/有接发列车作业班组数"""
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_person,
        _stats_yecha_ratio,
        _stats_check_address_ratio,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'g', 'i', 'j']
    ]
    item_weight = [0.15, 0.15, 0.20, 0.20, 0.10, 0.05, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=6, major=risk_type, main_type=1, child_index_list=[2, 3, 5, 6, 7, 9, 10],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
