# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.chewu_jiefalc import GLV
from app.data.major_risk_index.chewu_jiefalc.common import stats_other_problem_exposure, \
    stats_total_problem_exposure_type_chewu
from app.data.major_risk_index.chewu_jiefalc.problem_exposure_sql import SELF_CHECK_PROBLEM_SQL, \
    OTHER_CHECK_PROBLEM_SQL, SAFETY_PRODUCE_INFO_SQL, CHECKED_HIDDEN_PROBLEM_POINT_SQL, HIDDEN_PROBLEM_POINT_SQL, \
    CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_sql import (WORK_LOAD_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE, WORK_LOAD, RISK_IDS
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
    CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_FILTER_LIST
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    DEPARTMENT_FILTER_LIST = GLV.get_value('DEPARTMENT_FILTER_LIST')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【接发列车工作量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    WORK_LOAD = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    # 查出较严重隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 单位应检查问题项点
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    title = [
        '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})', '作业项问题数({0})/工作量({1})',
        '一般及以上作业项问题数({0})/人数({1})'
    ]
    return stats_total_problem_exposure_type_chewu(
        CHECK_ITEM_IDS, CHECK_PROBLEM_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, title=title,
        weight_item=[0.4, 0.3, 0.2, 0.1],
        work_load_list=[WORK_LOAD, WORK_LOAD, WORK_LOAD, STAFF_NUMBER],
    )


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续3月无的扣1分/项，连续6月无的扣2分/条，连续9个月无的扣4分/条。得分=100-扣分"""
    customizededuct = {
        3: 1,
        6: 2,
        9: 4
    }
    return problem_exposure.stats_problem_exposure_excellent(
        CHECK_ITEM_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct, months=10)


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    """较大、重大安全风险，且本单位基础问题库中有的),本单位未查出的按1分/项扣"""
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        calc_score_formula=lambda x: 0 if (100 - 1 * x) < 0 else round((100 - 1 * x), 2),
        title='本月问题个数: {1}个')


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_ITEM_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """从他查问题分析1个月全段未自查出该项问题，最高扣30分。较大风险扣1分，严重风险扣3分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题"""
    return stats_other_problem_exposure(
        CHECK_ITEM_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score={1: 3, 2: 1},
        columns_list=[(1, 1), (1, 2), (2, 1), (2, 2), (3, 1), (3, 2)])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，较严重隐患暴露,事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_hidden_problem_exposure,
        _stats_problem_exposure,
        _stats_banzu_problem_exposure,
        _stats_other_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.4, 0.15, 0.2, 0.25, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=6, major=risk_type, main_type=5, child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
