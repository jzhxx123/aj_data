# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     assess_intensity_sql
   Author :       hwj
   date：          2019/7/12上午9:47
   Change Activity: 2019/7/12上午9:47
-------------------------------------------------
"""

# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 总问题数--(a.TYPE = 3自查)
CHECK_PROBLEM_ONOUTWAY_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID    
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND IS_EXTERNAL = 0
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 月度考核总金额(干部)
GANBU_ASSESS_RESPONSIBLE_SQL = """select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY = '干部'
        AND c.FK_CHECK_ITEM_ID IN ({2})
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""

FEIGANBU_ASSESS_RESPONSIBLE_SQL =  """select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY != '干部'
        AND c.FK_CHECK_ITEM_ID IN ({2})
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""