# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/7/10下午6:45
   Change Activity: 2019/7/10下午6:45
-------------------------------------------------
"""
import calendar

import pandas as pd
from dateutil.relativedelta import relativedelta

from app.data.index.util import get_custom_month, get_query_condition_by_risktype, get_month_day
from app.data.major_risk_index.chewu_jiefalc.check_intensity_sql import MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, \
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL
from app.data.major_risk_index.common.problem_exposure import _calc_basic_prob_number_per_person, \
    _calc_basic_prob_score_per_person
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import BANZU_POINT_SQL, CHECK_POINT_SQL
from app.data.major_risk_index.util import calc_score_groupby_major, summizet_operation_set, \
    write_export_basic_data_to_mongo, df_merge_with_dpid, calc_child_index_type_sum, append_major_column_to_df, \
    export_basic_data_tow_field_monthly_two, format_export_basic_data, calc_extra_child_score_groupby_major, \
    add_avg_number_by_major, add_avg_score_by_major, add_avg_number_by_major_third, \
    calc_extra_child_score_groupby_major_third
from app.data.util import get_history_months, pd_query
from app.data.workshop_health_index.cache.cache import get_cache_client
from app.utils.common_func import get_today

cache_client = get_cache_client(__package__)

HIERARCHY = [3]


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    BELONG_PROFESSION_ID = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = BELONG_PROFESSION_ID.get(major, 898)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
        AND 
        FK_PROFESSION_DICTIONARY_ID NOT LIKE '%%2168%%'
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 6
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def calc_score_by_formula_address(row, column, zhanduan_column, node):
    if row[node] == 0:
        return 0
    if row[column] == 0:
        return -5
    if row[zhanduan_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[zhanduan_column]) / row[zhanduan_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


# 问题均衡度
def stats_problem_point_evenness(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        work_load, months_ago,
        risk_type, choose_dpid_data,
        zhanduan_filter_list=[]):
    rst_index_score = []
    df_numerator = generally_above_problem_point_count
    df_denominator = generally_above_problem_point_in_problem_base_count
    ser_numerator = df_numerator.groupby(
        ['TYPE3'])['COUNT'].sum()
    ser_denominator = df_denominator.groupby(
        ['TYPE3'])['COUNT'].sum()
    work_load = work_load.groupby(['TYPE3'])['COUNT'].sum()
    df_calc = pd.concat(
        [
            ser_numerator.to_frame(name='numerator'),
            ser_denominator.to_frame(name='denominator'),
            work_load.to_frame(name='work_load')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['denominator'], inplace=True)
    df_calc.fillna(0, inplace=True)
    # 分母为0时ratio赋予0
    df_calc['ratio'] = df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100 / row["work_load"])
        if row["work_load"] > 0 and row['denominator'] > 0 else 0, axis=1)

    # 问题均衡度
    filted_df_calc = df_calc[~df_calc.index.isin(zhanduan_filter_list)]
    radio = filted_df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100)
        if row['denominator'] > 0 else 0, axis=1)

    major_avg_score = sum(radio) / sum(filted_df_calc['work_load'])

    df_calc['AVG_NUMBER'] = major_avg_score
    data = append_major_column_to_df(choose_dpid_data(3), df_calc)

    data['SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'ratio', 'AVG_NUMBER', 1),
        axis=1)
    # 增加平均分一列
    data = add_avg_score_by_major(data, 'SCORE')
    data.fillna(0, inplace=True)
    data['group_sort'] = data['SCORE'].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    column = 'SCORE_a_3'
    df_calc = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[column])
    content_tmp = '<p>得分：{0:.2f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.6f}</p><p>问题查处均衡度({3:.6f}) = ' \
                  + '查出一般以上问题项点数({4:.0f})/ (基础问题库中一般以上问题项点数({5:.0f})* 工作量({7:.3f}))*100%</p>'

    data['CONTENT'] = data.apply(lambda row:
                                 content_tmp.format(row['SCORE'], row['group_sort'],
                                                    row['AVG_NUMBER'],
                                                    row['ratio'], row['numerator'],
                                                    row['denominator'], row['AVG_SCORE'],
                                                    row['work_load']), axis=1)
    calc_basic_data_rst = format_export_basic_data(
        data.copy(), 4, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 4, 1, risk_type=risk_type)

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(df_calc, choose_dpid_data(3), column,
                           3, 2, 4, 1,
                           months_ago, risk_type)
    rst_index_score.append(df_calc[[column]])

    return rst_index_score


def _export_calc_address_evenness_data(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        work_load = row['WORK_COUNT']
        point_name = row['ALL_NAME']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})

        if isinstance(point_name, int):
            cnt.update({'总检查地点数：': 0})
            calc_data.update({dpid: cnt})
            continue

        if avg_check_count == 0:
            continue
        check_desc = (f'{point_name}站(比较值：{avg_check_count:.2f}, '
                      f'受检次数：{real_check_count:.0f}, '
                      f'基准值：{base_check_count:.4f}, '
                      f'工作量：{work_load:.3f})')
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= 4:
                cnt.update({
                    '受检次数超过比较值400%以上地点：':
                        cnt.get('受检次数超过比较值400%以上地点：', '') + '<br/>' + check_desc
                })
            elif _ratio <= -0.5:
                cnt.update({
                    '受检次数低于比较值50%地点：':
                        cnt.get('受检次数低于比较值50%地点：', '') + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


# 检查地点均衡度
def stats_check_address_evenness(check_item_ids, dpid_work_load, zhanduan_work_count,
                                 station_connect_department_sql,
                                 check_point_connect_department_sql,
                                 banzu_department_checked_count_sql,
                                 point_check_count_sql,
                                 department_data, zhanduan_dpid_data,
                                 months_ago, risk_type, choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    dpid_work_load.rename(columns={
        "COUNT": "WORK_COUNT",
    }, inplace=True)
    zhanduan_work_count = zhanduan_work_count[['DEPARTMENT_ID', 'COUNT']].copy()
    zhanduan_work_count.rename(columns={'DEPARTMENT_ID': 'TYPE3', 'COUNT': 'WORK_COUNT'}, inplace=True)

    # 地点
    station_connect_department = pd_query(station_connect_department_sql.format(check_item_ids))
    check_point_connect_department = pd_query(check_point_connect_department_sql)
    point_check_count = pd_query(point_check_count_sql.format(*stats_month, check_item_ids))

    point_data = pd.merge(
        station_connect_department,
        check_point_connect_department,
        how='left',
        on='FK_DEPARTMENT_ID',
    )
    point_data = pd.merge(
        point_data,
        point_check_count,
        how='left',
        on='FK_CHECK_POINT_ID'
    )
    point_data.dropna(inplace=True)

    # 班组受检次数
    banzu_check = pd_query(
        banzu_department_checked_count_sql.format(*stats_month, check_item_ids))
    banzu_check = pd.merge(
        banzu_check,
        station_connect_department,
        how='right',
        on='FK_DEPARTMENT_ID',
    )

    # 只取班组
    # data = pd.concat([point_data, banzu_check], sort='False')
    data = banzu_check
    data.drop_duplicates(['FK_DEPARTMENT_ID', 'PK_ID'], keep='first', inplace=True)
    station_check_count = data.groupby(['FK_DEPARTMENT_ID']).size().to_frame(name='COUNT').reset_index()

    data_check_banzu = pd.merge(
        station_check_count,
        dpid_work_load,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.fillna(0, inplace=True)
    zhanduan_count = data_check_banzu.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='zhanduan_count')
    zhanduan_work_count = zhanduan_work_count.groupby(['TYPE3'])['WORK_COUNT'].sum().to_frame(
        name='zhanduan_work_count')
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_work_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu.drop(['DEPARTMENT_ID', 'NAME', "TYPE"], inplace=True, axis=1)
    data = pd.merge(
        data_check_banzu,
        zhanduan_dpid_data,
        how='right',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID')
    data.drop(['TYPE3'], inplace=True, axis=1)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    data.fillna(0, inplace=True)

    # 计算基准值
    data['BASE_CHECK_COUNT'] = data.apply(lambda row: (row['zhanduan_count'] / row['zhanduan_work_count'])
    if row['zhanduan_work_count'] > 0 else 0, axis=1)
    # 计算比较值=地点工作量*基准值
    data['AVG_CHECK_COUNT'] = data.apply(lambda row: row['WORK_COUNT'] * row['BASE_CHECK_COUNT'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: calc_score_by_formula_address(row, 'COUNT', 'AVG_CHECK_COUNT', 'NODE'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data, problem_risk_score={1: 4, 2: 2, 3: 0.1, },
        columns_list=[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                      (2, 3), (3, 1), (3, 2), (3, 3)]):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(safety_produce_info_sql.format(check_item_ids, *calc_month))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        risk_level = int(each_problem[1])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda x: title + ';'.join(f'{col}: {int(x[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data, zhanduan_dpid,
                                                          5, 5, 3, months_ago, first_title, second_title,
                                                          columns_list=columns_list)
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(
        lambda x: x["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + x["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 5, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5,
                                     5, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type)
    return rst_child_score


def _calc_risk_score_per_person(series, work_load, choose_dpid_data, hierarchy,
                                idx):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data['ratio'] = data.apply(lambda row: row['score'] / row['PERSON_NUMBER']
    if row['PERSON_NUMBER'] > 0 else 0, axis=1)
    data.fillna(0, inplace=True)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula)
    # 中间计算数据
    title = [
        '较大和重大安全风险问题质量分累计({0})/工作量({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["score"], 2)}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_risk_score(df_xianchang, work_load, choose_dpid_data, hierarchy,
                        idx):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(sc_risk_score, work_load,
                                       choose_dpid_data, hierarchy, idx)


def _calc_risk_score(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(risk_score, work_load, choose_dpid_data,
                                       hierarchy, idx)


# 较大风险问题质量均分
def stats_risk_score_per_person(df_jiaoda,
                                df_xianchang,
                                work_load,
                                months_ago,
                                risk_type,
                                child_weight=[0.6, 0.4],
                                choose_dpid_data=None):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score, _calc_xc_risk_score]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def calc_jiefalc_count(jiefalc_count, node_ctc):
    """"每个部门接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    data = data.groupby(['FK_DEPARTMENT_ID'])['CW_COUNT'].sum().reset_index()
    return data


def calc_hour_jiefalc_count(jiefalc_count, node_ctc):
    """"计算时段高动客接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    data = data.groupby(['FK_DEPARTMENT_ID', 'WORK_HOUR'])['HOUR_COUNT'].sum().reset_index()
    return data


def calc_day_jiefalc_count(jiefalc_count, node_ctc):
    """"计算每日高动客接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    data = data.groupby(['FK_DEPARTMENT_ID', 'WORK_DAY'])['DAY_COUNT'].sum().reset_index()
    return data


# 计算接发列车工作量及车站接法列车总人数
def calc_jiefalc_work_load(jiefalc_count, jiefalc_people_count,
                           a_station_count, b_station_count,
                           a_dpid_station_count, b_dpid_station_count,
                           department_data, months_ago):
    """
    接发列车工作量计算
    :param jiefalc_count: 接法列车数
    :param jiefalc_people_count: 接法列车人数
    :param department_data: 部门
    :param a_station_count: A站段接发列车站点(关联车站个数，但接发列车人数不为0。操作方式条件257)
    :param b_station_count: B站段接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件1346)
    :param a_dpid_station_count: A部门接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件257)
    :param b_dpid_station_count: B部门接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件1346)
    :param months_ago: 几月前
    :return: 接法列车工作量,车站接发列车总人数
    """
    data = df_merge_with_dpid(jiefalc_count, jiefalc_people_count, how='right')
    data.fillna(0, inplace=True)

    # 选取2,5,7ctc操作方式的车站
    # data = data[(data['NUMBER'] > 0) & (data['CTC_OPERATION_WAY'].isin([2, 5, 7]))]
    data = data[data['NUMBER'] > 0]
    month_day = get_month_day(months_ago)

    # 拼接车站数
    data = pd.merge(a_station_count, data, on='TYPE3', how='right')
    data = pd.merge(b_station_count, data, on='TYPE3', how='right')
    data = pd.merge(a_dpid_station_count, data, on='DEPARTMENT_ID', how='right')
    data = pd.merge(b_dpid_station_count, data, on='DEPARTMENT_ID', how='right')
    data.fillna(0, inplace=True)

    cw_count = data.groupby(['DEPARTMENT_ID'])['CW_COUNT'].sum().to_frame()
    data.drop(['CW_COUNT'], inplace=True, axis=1)
    data.drop_duplicates(subset=['DEPARTMENT_ID'], keep='first', inplace=True)
    data = pd.merge(data,
                    cw_count,
                    left_on='DEPARTMENT_ID',
                    right_index=True)
    data["AVERAGE_JIEFA_COUNT"] = data["NUMBER"] * data['CW_COUNT'] / month_day

    # 站段工作量计算
    work_load = data.groupby('TYPE3')['AVERAGE_JIEFA_COUNT'].sum().reset_index()
    station_count = pd.merge(a_station_count, b_station_count, how='outer', on='TYPE3')
    station_count.fillna(0, inplace=True)
    work_load = pd.merge(
        station_count,
        work_load,
        on='TYPE3',
    )
    work_load['COUNT'] = work_load.apply(
        lambda row:
        (row['A_NODE_COUNT'] + row['B_NODE_COUNT'] * 0.2) * 0.7 / 70 +
        row['AVERAGE_JIEFA_COUNT'] * 0.3 / 10000, axis=1
    )
    work_load.rename(columns={'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    work_load = pd.merge(
        work_load[['DEPARTMENT_ID', 'COUNT']],
        department_data,
        on='DEPARTMENT_ID'
    )
    # 接发列车合成工作量 0.2及以下的  不纳入基数计算
    filter_department_id_list = work_load[work_load['COUNT'] <= 0.3].DEPARTMENT_ID.tolist()

    # 每个部门工作量
    data['COUNT'] = data.apply(
        lambda row:
        (row['A_DPID_NODE_COUNT'] + row['B_DPID_NODE_COUNT'] * 0.2) * 0.7 / 70 +
        row['AVERAGE_JIEFA_COUNT'] * 0.3 / 10000, axis=1
    )
    data = data[data['CTC_OPERATION_WAY'].isin([2, 5, 7])]
    data = data[['DEPARTMENT_ID', 'COUNT', 'NODE', "NUMBER",
                 'A_DPID_NODE_COUNT', 'B_DPID_NODE_COUNT', 'AVERAGE_JIEFA_COUNT']]

    # 统计工作量【接发列车工作量】
    dpid_work_load = pd.merge(
        data,
        department_data,
        how='inner',
        left_on='DEPARTMENT_ID',
        right_on='DEPARTMENT_ID'
    )
    # work_load.drop_duplicates(subset=['NODE'], keep='first', inplace=True)
    return work_load, jiefalc_people_count, dpid_work_load, station_count, filter_department_id_list


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False,
                           zhanduan_filter_list=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER', zhanduan_filter_list=zhanduan_filter_list)


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False,
                                 zhanduan_filter_list=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major,
                                  zhanduan_filter_list=zhanduan_filter_list)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False,
                                zhanduan_filter_list=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major,
                                  zhanduan_filter_list=zhanduan_filter_list)


# 总体暴露度- 作业项人数分开
def stats_total_problem_exposure_type_chewu(
        check_item_ids, check_problem_sql,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True,
        weight_item=[0.3, 0.3, 0.2, 0.2],
        weight_part=[0.4, 0.6],
        work_load_list=[],
        zhanduan_filter_list=[]):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    if not title:
        title = [
            '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
            '一般及以上作业项问题数({0})/人数({1})'
        ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load_list[i], department_data, i,
                     title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load_list[i], department_data,
                         choose_dpid_data, weight, hierarchy,
                         is_calc_score_base_major=is_calc_score_base_major,
                         zhanduan_filter_list=zhanduan_filter_list))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def get_chewu_check_address_standard_data(work_banzu_info_data,
                                          real_check_banzu_data,
                                          department_data,
                                          months_ago,
                                          check_item_ids,
                                          major, is_base_item=True):
    """[将覆盖率数据转成标准格式]
    类型：检查地点数（实际检查班组+实际重要检查点）/地点总数（班组数+重要检查点）
    班组跟重要检查点的工作项目要复合特定指数检查项目
    """
    # 实际检查班组数
    real_check_banzu_data = pd.merge(
        real_check_banzu_data,
        work_banzu_info_data[['FK_DEPARTMENT_ID']],
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    # 删除重复班组id
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)

    banzu_point_data = work_banzu_info_data
    filled_data = pd_query(BANZU_POINT_SQL.format(major))
    filled_data['COUNT'] = 0
    if real_check_banzu_data.empty:
        # 两个均为空时，补充检查班组数
        real_check_banzu_data = filled_data
    if banzu_point_data.empty:
        # 两个均为空时，补充符合某工作项班组数
        banzu_point_data = filled_data
    return real_check_banzu_data, banzu_point_data
