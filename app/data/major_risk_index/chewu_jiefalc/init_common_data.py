#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query
from app.data.major_risk_index.chewu_jiefalc import GLV
from app.data.major_risk_index.chewu_jiefalc.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, JIEFALC_COUNT_SQL, JIEFALC_PEOPLE_COUNT_SQL,
    A_STATION_COUNT_SQL, B_STATION_COUNT_SQL, A_DEPARTMENT_STATION_COUNT_SQL, B_DEPARTMENT_STATION_COUNT_SQL,
    JIEFALC_CTC_WAY_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.chewu_jiefalc.common import get_vitual_major_ids, calc_jiefalc_work_load, \
    calc_jiefalc_count


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids('客运-1')
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(
        major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(
        major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(
        major, ids))
    check_item_ids = get_query_condition_by_risktype(risk_name)[0]
    # 接发列车数(关联聚合到部门)
    node_jiefalc_count = pd_query(JIEFALC_COUNT_SQL.format(*stats_month), db_name='db_mid')
    node_ctc = pd_query(JIEFALC_CTC_WAY_SQL)
    jiefalc_count = calc_jiefalc_count(node_jiefalc_count, node_ctc)
    # 接发列车工作人数
    jiefalc_people_count = df_merge_with_dpid(
        pd_query(JIEFALC_PEOPLE_COUNT_SQL.format(check_item_ids)),
        department_data)

    # 站段A类接发列车站点数
    a_station_count = pd_query(A_STATION_COUNT_SQL.format(check_item_ids))
    # 站段B类接发列车站点数
    b_station_count = pd_query(B_STATION_COUNT_SQL.format(check_item_ids))
    # 部门接发列车站点数
    a_dpid_station_count = pd_query(A_DEPARTMENT_STATION_COUNT_SQL.format(check_item_ids))
    # 部门接发列车站点数
    b_dpid_station_count = pd_query(B_DEPARTMENT_STATION_COUNT_SQL.format(check_item_ids))

    # 接法列车工作量,车站接发列车总人数
    work_load, jiefalc_people_count, dpid_work_load, station_count, filter_department_id_list = calc_jiefalc_work_load(
        jiefalc_count, jiefalc_people_count,
        a_station_count, b_station_count,
        a_dpid_station_count, b_dpid_station_count,
        department_data, months_ago)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'JIEFALC_COUNT': jiefalc_count,
        'NODE_CTC': node_ctc,
        'JIEFALC_PEOPLE_COUNT': jiefalc_people_count,
        'WORK_LOAD': work_load,
        'A_STATION_COUNT': a_station_count,
        'B_STATION_COUNT': b_station_count,
        'A_DPID_STATION_COUNT': a_dpid_station_count,
        'B_DPID_STATION_COUNT': b_dpid_station_count,
        'STATION_COUNT': station_count,
        'DPID_WORK_LOAD': dpid_work_load,
        'DEPARTMENT_FILTER_LIST': filter_department_id_list,

    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
