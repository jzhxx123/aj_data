#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.chewu_jiefalc import GLV
from app.data.major_risk_index.chewu_jiefalc.assess_intensity_sql import CHECK_PROBLEM_ONOUTWAY_SQL, KAOHE_PROBLEM_SQL, \
    GANBU_ASSESS_RESPONSIBLE_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL
from app.data.major_risk_index.common import assess_intensity

from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import (
    CADRE_COUNT_SQL,
    WORKER_COUNT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula_asses_ratio(row, column, major_column, detail_type):
    _score = 0
    _ratio = row[column]
    if _ratio > 0.9:
        _score = int((_ratio - 0.9) * 100)
    return _score if _score <= 10 else 10


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, \
        WORK_LOAD, ASSESS_ZUOYE_PROBLEM_COUNT, ASSESS_GUANLI_PROBLEM_COUNT, \
        CADRE_COUNT, WORKER_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, \
        WORKER_ASSESS_RESPONSIBLE_MONEY, PROBLEM_COUNT_NOPUOTWAY, DEPARTMENT_FILTER_LIST

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    DEPARTMENT_FILTER_LIST = GLV.get_value('DEPARTMENT_FILTER_LIST')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL), DEPARTMENT_DATA)

    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL), DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 问题总数（剔除路外问题）
    PROBLEM_COUNT_NOPUOTWAY = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_ONOUTWAY_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # # 月度考核总金额
    # ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
    #     pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
    #     DEPARTMENT_DATA)

    # 月度考核总金额（干部）
    CADRE_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(GANBU_ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额（非干部）
    WORKER_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(FEIGANBU_ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 0
    if row[column] == 0.0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100

    elif _ratio >= -0.1:
        _score = 90
    else:
        deduction = _ratio * 100
        _score = 100 + deduction
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


# 换算单位考核问题数
def _stats_check_problem_assess_radio(months_ago):
    """接发列车问题考核数/接发列车工作量"""
    customizecontent = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = ' \
                       + '考核问题数({4})/ 工作量 ({5})</p>'
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        zhanduan_filter_list=DEPARTMENT_FILTER_LIST
    )


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    """(干部月度考核总金额/干部总人数)*20% + (职工月度考核总金额/职工总人数)*80%"""
    return assess_intensity.stats_assess_money_per_person_type_two(
        CADRE_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, WORKER_COUNT,
        WORKER_ASSESS_RESPONSIBLE_MONEY, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 问题考核率
def _stats_problem_assess_radio(months_ago):
    """接发列车考核问题数/接发列车问题数"""
    return calc_child_index_type_divide(
        ASSESS_PROBLEM_COUNT,
        PROBLEM_COUNT_NOPUOTWAY,
        2,
        3,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula_asses_ratio,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【换算单位考核问题数、月人均考核金额, 考核率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_problem_assess_radio,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'd']]
    item_weight = [0.5, 0.5, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=6, major=risk_type, main_type=3, child_index_list=[1, 2, 4],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
