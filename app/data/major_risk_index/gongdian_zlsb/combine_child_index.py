#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-4上午10:41
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_zlsb import assess_intensity, check_evenness, check_intensity, \
    problem_exposure, problem_rectification, evaluate_intensity, init_common_data
from app.data.major_risk_index.gongdian_zlsb.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_zlsb.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 55
    risk_type = '供电-9'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)

    child_index_list = [1, 2, 3, 4, 5, 6]
    child_index_weight = [0.45, 0.1, 0.15, 0.05, 0.15, 0.1]

    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(zhanduan_dpid_sql, chejian_dpid_sql,
                                          months_ago, risk_name, risk_type,
                                          child_index_list=child_index_list,
                                          child_index_weight=child_index_weight)

    update_major_maintype_weight(index_type=9, major=risk_type, child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )


if __name__ == '__main__':
    pass
