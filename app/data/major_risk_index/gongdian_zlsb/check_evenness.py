#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-3下午7:22
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.common import get_major_dpid, get_check_address_basic_data
from app.data.major_risk_index.gongdian_zlsb import GLV
from app.data.major_risk_index.gongdian_zlsb.check_evenness_sql import GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, \
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, CHECK_BANZU_PERSON_COUNT_SQL, BANZU_CONNECT_DEPARTMENT_SQL, \
    BANZU_CHECKED_COUNT_SQL, CHECK_POINT_PERSON_COUNT_SQL, CHECK_POINT_CONNECT_DEPARTMENT_SQL, POINT_CHECKED_COUNT_SQL
from app.data.major_risk_index.gongdian_zlsb.common import get_vitual_major_ids
from app.data.major_risk_index.util import df_merge_with_dpid, summizet_child_index, combine_child_index_func
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, CHECK_ADDRESS_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_RISK_IDS, CHECK_ITEM_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    CHECK_RISK_IDS = get_query_condition_by_risktype(risk_name)[1]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    check_banzu_person_count_sql = CHECK_BANZU_PERSON_COUNT_SQL.format(major, ids)
    banzu_connect_department_sql = BANZU_CONNECT_DEPARTMENT_SQL.format(major, ids)
    banzu_checked_count_sql = BANZU_CHECKED_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)
    check_point_person_count_sql = CHECK_POINT_PERSON_COUNT_SQL.format(major, ids)
    check_point_connect_department_sql = CHECK_POINT_CONNECT_DEPARTMENT_SQL.format(major, ids, CHECK_ITEM_IDS)
    point_checked_count_sql = POINT_CHECKED_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)

    CHECK_ADDRESS_DATA = get_check_address_basic_data(
        months_ago, major, CHECK_ITEM_IDS,
        check_banzu_person_count_sql=check_banzu_person_count_sql,
        banzu_connect_department_sql=banzu_connect_department_sql,
        banzu_checked_count_sql=banzu_checked_count_sql,
        check_point_person_count_sql=check_point_person_count_sql,
        check_point_connect_department_sql=check_point_connect_department_sql,
        point_checked_count_sql=point_checked_count_sql
    )

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """
    每个地点受检次数超过比较值600%以上的一处扣2分;受检次数低于比较值50%的一处扣2分，未检查的一处扣5分。
    （每个地点的比较值=该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)，受检地点的工作量越大，比较值越高，本单位现场检查）
    检查地点中，每处检查点、重要检查点人数依据车间班组配置的该点作业人数为准，若配置的主要生产场所点作业人数为0或空，则取整个班组作业人数。
    若一个检查点的关联单位为车间，同样去主要生产场所中配置的改地点作业人数，若配置的作业人数为0或空，则取该车间的直属人数，即人员部门为车间单位的人数
    """
    check_banzu_count = CHECK_ADDRESS_DATA.get('check_banzu_count')
    check_point_count = CHECK_ADDRESS_DATA.get('check_point_count')
    banzu_department_checked_count = CHECK_ADDRESS_DATA.get('banzu_department_checked_count')
    check_point_checked_count = CHECK_ADDRESS_DATA.get('check_point_checked_count')
    check_point_connect_department = CHECK_ADDRESS_DATA.get('check_point_connect_department')
    banzu_connect_department = CHECK_ADDRESS_DATA.get('banzu_connect_department')

    return check_evenness.stats_check_address_evenness_new(
        check_banzu_count, check_point_count,
        banzu_department_checked_count, check_point_checked_count,
        check_point_connect_department, banzu_connect_department,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c']]
    item_weight = [0.5, 0.5]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=9, major=risk_type, main_type=4,
                                 child_index_list=[1, 3],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
