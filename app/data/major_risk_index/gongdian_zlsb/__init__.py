#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-1上午9:00


from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import evaluate_intensity, check_evenness, check_intensity, \
    problem_exposure, problem_rectification, assess_intensity, combine_child_index
