#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-3上午11:53

import pandas as pd
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta

from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import calc_extra_child_score_groupby_major, \
    calc_extra_child_score_groupby_major_third, df_merge_with_dpid, append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set
from app.data.util import pd_query


def _calc_score_for_by_major_ratio(self_ratio, major_ratio):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    n1 = major_ratio[0][0]
    sn1 = major_ratio[0][1]
    n2 = major_ratio[1][0]
    sn2 = major_ratio[1][1]
    n3 = major_ratio[2][0]
    sn3 = major_ratio[2][1]
    c = self_ratio
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _calc_score_for_by_major_ratio_df(row):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    n1 = row['ratio_1']
    sn1 = row['score_1']
    n2 = row['ratio_2']
    sn2 = row['score_2']
    n3 = row['ratio_3']
    sn3 = row['score_3']
    c = row['ratio']
    for idx, item in enumerate([n1, n2, n3]):
        if c > item:
            level = idx + 1
            break
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _cacl_bd_total_person_num(row):
    """按照给定逻辑计算变电所人数
    """
    total_num = 0
    for field in [
        'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
        'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
        'NON_PROFESSIONAL'
    ]:
        value = str(row[field])
        if len(value) > 0:
            total_num += len(value.split(','))
    return total_num


def get_ticket_amount(truck_ticket_data, bd_ticket_data, department_data):
    """工作量= 工作人数×工作小时数， 然后按照工区名称跟安监数据库部门表做关联
    """
    bd_ticket_data['TOTAL_PERSON_NUM'] = bd_ticket_data.apply(
        lambda row: _cacl_bd_total_person_num(row), axis=1)
    bd_ticket_data.drop(
        columns=[
            'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
            'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
            'NON_PROFESSIONAL'
        ],
        inplace=True)
    ticket_data = pd.concat(
        [truck_ticket_data, bd_ticket_data], axis=0, sort=False)
    ticket_data['COUNT'] = ticket_data.apply(
        lambda row: int(row['TOTAL_PERSON_NUM']) * max(1, (row['HOURS'] + 24) if row['HOURS'] < 0 else row['HOURS']),
        axis=1)
    ticket_data = ticket_data.groupby(['DEPART_ID'])['COUNT'].sum()
    ticket_department = pd.merge(
        ticket_data.to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return ticket_department


def _is_yecha(row):
    """判断是否是夜查，晚上10时到次日6时为夜查
    """
    if row['START_WORK_TIME'] >= 22 or row['END_WORK_TIME'] <= 6:
        return 1
    return 0


def _is_genban(row):
    """跟班率统计工作票上是否有跟班签字，接触网工作票叫做监控干部，
    变配电工作票叫做跟班干部，作业车工作票叫做添乘干部
    """
    if not row['GENBAN']:
        return 0
    if len(row['GENBAN'].strip()) > 0:
        return 1
    return 0


# 计算工作票总数， 跟班工作票数， 夜间工作票数， 夜间跟班工作票数
def stats_work_ticket(work_ticket, department_data):
    work_ticket['IS_YECHA'] = work_ticket.apply(
        lambda row: _is_yecha(row), axis=1)
    work_ticket['IS_GENBAN'] = work_ticket.apply(
        lambda row: _is_genban(row), axis=1)
    work_ticket.drop(
        columns=['START_WORK_TIME', 'END_WORK_TIME', 'GENBAN'], inplace=True)
    # 统计总票数
    total_tickets = pd.merge(
        work_ticket.groupby(['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 跟班总票数
    genban_tickets = pd.merge(
        work_ticket[work_ticket['IS_GENBAN'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查总票数
    yecha_tickets = pd.merge(
        work_ticket[work_ticket['IS_GENBAN'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查跟班总票数
    yecha_genban_tickets = pd.merge(
        work_ticket[(work_ticket['IS_GENBAN'] == 1)
                    & (work_ticket['IS_YECHA'] == 1)].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return total_tickets, genban_tickets, yecha_tickets, yecha_genban_tickets


def _calc_score_by_formula_exposure(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 50
        _score = 0 if _score < 0 else _score
    return _score


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False,
                           ratio_df=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula_exposure
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER',
        ratio_df=ratio_df)


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False,
                                 ratio_df=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(
        prob_number, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        ratio_df=ratio_df)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False,
                                ratio_df=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(
        prob_score, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        ratio_df=ratio_df)


# 总体暴露度
def stats_total_problem_exposure_type(
        check_item_ids, check_problem_sql, work_load,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True, ratio_df_dict=None,
        weight_item=[0.3, 0.3, 0.2, 0.2],
        weight_part=[0.4, 0.6]):
    hierarchy = 3
    func_key = ['总问题数', '一般及以上问题数']
    score_formula_dict = {
        '总问题数': _calc_score_for_by_major_ratio_df
    }

    if not title:
        title = [
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})']
    if not ratio_df_dict:
        ratio_df_dict = {}
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    data_dict = {
        "总问题数": base_data,
        '一般及以上问题数': risk_data
    }
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    # 导出中间过程
    for i, key in enumerate(func_key):
        data = data_dict.get(key)
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            sub_calc_basic_data = func(
                data.copy(), work_load, department_data, i, title[i])
            calc_basic_data.append(sub_calc_basic_data)
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    score = []
    for i, key in enumerate(func_key):
        data = data_dict.get(key)
        calc_score_formula = score_formula_dict.get(key)
        ratio_df_list = ratio_df_dict.get(key)
        # 人均问题数，人均质量分
        for j, func in enumerate(
                [_calc_prob_number_per_person, _calc_prob_score_per_person]):
            weight = weight_item[i] * weight_part[j]
            if ratio_df_list:
                ratio_df = ratio_df_list[j]
            else:
                ratio_df = None
            sub_score = func(data.copy(), work_load, department_data,
                             choose_dpid_data, weight, hierarchy,
                             calc_score_formula=calc_score_formula,
                             is_calc_score_base_major=is_calc_score_base_major,
                             ratio_df=ratio_df)
            score.append(sub_score)
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_a_{hierarchy}'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        choose_dpid_data(hierarchy),
        column,
        hierarchy,
        2,
        5,
        1,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def get_site_check_count(site_check_department, all_department):
    """获取现场检查地点数"""
    site_check_department = site_check_department[
        site_check_department.FK_DEPARTMENT_ID.isin(
            all_department.FK_DEPARTMENT_ID
        )
    ]
    return site_check_department


def get_ratio_df(work_load, high, middle, low, column='COUNT', is_stable=1):
    ratio_df = work_load[['DEPARTMENT_ID', column]].copy()
    ratio_df['score_1'] = 100
    ratio_df['score_2'] = 90
    ratio_df['score_3'] = 60
    ratio_df['ratio_1'] = (ratio_df[column] ** is_stable) * high
    ratio_df['ratio_2'] = (ratio_df[column] ** is_stable) * middle
    ratio_df['ratio_3'] = (ratio_df[column] ** is_stable) * low
    ratio_df = ratio_df.drop(column, axis=1)
    return ratio_df


def get_driver_count(sql, month, end_day):
    """
    获取司乘人员数, 当月未配置的一直向前取直到取到为止
    :param end_day:
    :param sql:
    :param month:
    :return:
    """
    driver_df = pd_query(sql)
    dp_id_set = set(driver_df.FK_DEPARTMENT_ID)
    df = driver_df[driver_df.MONTH == month].copy()
    diff_ip_set = dp_id_set.difference(set(df.FK_DEPARTMENT_ID))
    df_list = []
    if diff_ip_set:
        for dp_id in diff_ip_set:
            date = parse(end_day)
            for i in range(12):
                date = (date - relativedelta(months=1))
                mon = date.month
                mon_df = driver_df[
                    (driver_df.FK_DEPARTMENT_ID == dp_id)
                    & (driver_df.MONTH == mon)]
                if not mon_df.empty:
                    df_list.append(mon_df)
                    break
    df_list.append(df)
    data = pd.concat(df_list, sort=False)
    data = data[['FK_DEPARTMENT_ID', 'COUNT']]
    return data
