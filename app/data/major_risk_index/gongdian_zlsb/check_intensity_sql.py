#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-3上午10:28

# 工作票
WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID, ty.MONITORING_LEADER_SIGN AS GENBAN,
        HOUR(ty.START_WORK_TIME) AS START_WORK_TIME,
        HOUR(ty.END_WORK_TIME) AS END_WORK_TIME
    FROM
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(ty.END_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(ty.END_WORK_DAY, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND ty.STATUS = 4
    UNION ALL SELECT
        tu.DEPART_ID, tr.ADD_RIDE_PERSONNEL_SIGN AS GENBAN,
        HOUR(tr.WORK_START_TIME) AS START_WORK_TIME,
        HOUR(tr.WORK_END_TIME) AS END_WORK_TIME
    FROM
        gd_t_work_ticket_for_truck AS tr
            INNER JOIN
        gd_t_unit AS tu ON tr.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND tr.STATUS = 4
    UNION ALL SELECT
        tu.DEPART_ID, bd.EXECUTE_CADRE_SIGN AS GENBAN,
        HOUR(bd.REAL_DATE) AS START_WORK_TIME,
        HOUR(bd.FINISH_JOB_DATE) AS END_WORK_TIME
    FROM
        gd_bd_work_ticket AS bd
            INNER JOIN
        gd_t_unit AS tu ON bd.UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(bd.FINISH_TICKET_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(bd.FINISH_TICKET_DATE, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND bd.STATUS = 4
"""

# 本单位出车趟次
WORK_LOAD_SQL = """SELECT 	
    a.ORG_ID as FK_DEPARTMENT_ID,b.COUNT
    FROM
    
        gd_t_unit AS a
        INNER JOIN
        (SELECT
        tu.TYPE3 AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT tr.PK_ID) AS COUNT
    FROM
        gd_t_work_ticket_for_truck AS tr
            INNER JOIN
        gd_t_unit AS tu ON tr.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND tr.STATUS = 4
        GROUP BY  tu.TYPE3) AS b on a.REALID = b.FK_DEPARTMENT_ID
    WHERE a.UNIT_TYPE =3
"""


# 添乘检查+现场检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID        
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (a.CHECK_WAY = 2 OR a.CHECK_WAY = 1 AND a.IS_GENBAN = 1)
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 检查次数（现场检查）
CHECK_COUNT_XIANCHANG_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY = 1
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 查处问题数
CHECK_PROBLEM_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 问题总数(基础问题库)
ALL_PROBLEM_COUNT_SQL = """
    SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
    WHERE
        a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID
"""


# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 一般及以上风险问题质量均分
YIBAN_RISK_LEVEL_PROBLEM_SCORE_SQL = """SELECT
      distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        AND a.RISK_LEVEL <= 3
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# # 现场检查或添乘检查（自轮设备-运用）地点数
# CHECK_POINT_SQL = """SELECT
#         c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
#     FROM
#         t_check_info_and_address AS a
#             INNER JOIN
#         t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
#             INNER JOIN
#         t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
#             INNER JOIN
#         t_check_problem AS d ON d.FK_PROBLEM_BASE_ID = b.PK_ID
#     WHERE
#         c.HIERARCHY = 2
#         AND c.IS_DELETE = 0
#         AND b.CHECK_WAY BETWEEN 1 AND 2
#         AND d.FK_CHECK_ITEM_ID IN ({2})
#         AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
#             >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#         AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
#             <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
#     GROUP BY c.FK_DEPARTMENT_ID
# """


# 现场检查或添乘检查（自轮设备-运用）地点数
CHECK_POINT_SQL = """SELECT
        distinct  
        a.FK_DEPARTMENT_ID,
        1 as COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
           INNER JOIN
        t_check_info_and_item AS d ON d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        b.CHECK_WAY BETWEEN 1 AND 2
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 总地点数--施工监管点总数量
ALL_CHECK_POINT_SQL = """SELECT
        DISTINCT 
        tu.DEPART_ID AS FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        gd_t_work_ticket_for_truck AS tr
            INNER JOIN
        gd_t_unit AS tu ON tr.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(tr.END_WORK_DAY, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND tr.STATUS = 4
"""


# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 3 AND 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND a.IS_EXTERNAL = 0
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 AND 4
        AND a.TYPE = 3
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数--问题数均为站段自查(e.TYPE=3)
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem as e on a.PK_ID= e.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND e.TYPE = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)  
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 具备调阅条件班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.MEDIA_TYPE != ''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]
