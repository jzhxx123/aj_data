# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_ratification_sql
   Author :       hwj
   date：          2019/9/16上午10:59
   Change Activity: 2019/9/16上午10:59
-------------------------------------------------
"""

# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        mp.FK_DEPARTMENT_ID, SUM(mp.SCORE_STANDARD*mp.GRADATION_RATIO)*{3} AS SCORE,
        COUNT(mp.pk_id) AS COUNT
FROM
(SELECT
DISTINCT b.FK_DEPARTMENT_ID, a.SCORE_STANDARD, d.GRADATION_RATIO, a.pk_id
FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person_gradation_ratio AS d
            ON a.FK_PERSON_GRADATION_RATIO_ID = d.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    ) as mp
    GROUP BY mp.FK_DEPARTMENT_ID;
"""


# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND b.IS_DELETE = 0
"""

# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where (b.TYPE2 = '{0}' or b.TYPE3 in {1})
    and b.is_delete = 0
    and a.is_delete = 0
"""

# 重复发生问题信息
REPEATE_HAPPEN_PROBLEM_SQL = """
SELECT 
    MAX(a.FK_PROBLEM_BASE_ID) AS FK_PROBLEM_BASE_ID,
    MAX(a.RISK_LEVEL) AS RISK_LEVEL,
    MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
    1 AS COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND a.RISK_LEVEL <=3
GROUP BY a.PK_ID
"""
