#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gw_gongdian_laoan import GLV
from app.data.major_risk_index.gw_gongdian_laoan.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gw_gongdian_laoan.common import get_vitual_major_ids


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(ids))

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
