#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql
Description:
Author:    
date:         2019/11/13
-------------------------------------------------
Change Activity:2019/11/13 9:28 上午
-------------------------------------------------
"""
from app.data.major_risk_index.gongwu_laoan.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, CHECK_POINT_CONNECT_DEPARTMENT_SQL,
    BANZU_CONNECT_DEPARTMENT_SQL, CHECK_POINT_COUNT_SQL,
    CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    NOITEM_DAILY_CHECK_COUNT_SQL)
# 重要检查点受检次数-不关联项目
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID AS CHECK_POINT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND c.TYPE = 1
        AND b.CHECK_WAY BETWEEN 1 AND 2
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 班组受检次数-不关联项目
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
    GROUP BY c.DEPARTMENT_ID;
"""