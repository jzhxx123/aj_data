#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql
Description:
Author:    
date:         2019/11/13
-------------------------------------------------
Change Activity:2019/11/13 9:44 上午
-------------------------------------------------
"""
from app.data.major_risk_index.gongwu_laoan.check_intensity_sql import (
    BANZU_POINT_SQL,
    RISK_LEVEL_PROBLEM_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    CHECK_POINT_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    REAL_CHECK_BANZU_SQLIST, CHECK_INFO_SQL,
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    NOITEM_CHECK_COUNT_SQL, NOITEM_MEDIA_COST_TIME_SQL
)
# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT distinct (f.FK_DEPARTMENT_ID), 1 AS NUMBER
FROM
(
SELECT 
    if(d.TYPE !=9, d.FK_PARENT_ID,b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE !=''
        ) as f
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(TYPE !=9, FK_PARENT_ID, DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department
WHERE
         TYPE BETWEEN 9 AND 10
        AND IS_DELETE = 0
        AND MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]

# 实际重要检查地点数（不含检查项目）
NOITEM_REAL_CHECK_POINT_SQL = """SELECT
max(c.FK_DEPARTMENT_ID), 1 AS COUNT
FROM
t_check_info_and_address AS a
    INNER JOIN
t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    INNER JOIN
t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
WHERE
a.TYPE = 2
AND c.HIERARCHY = 2
AND c.IS_DELETE = 0
AND c.TYPE = 1
AND b.CHECK_WAY BETWEEN 1 AND 2
AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
GROUP BY a.FK_CHECK_POINT_ID
"""