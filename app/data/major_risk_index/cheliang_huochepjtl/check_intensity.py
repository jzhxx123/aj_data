# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.cheliang_huochepjtl import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.cheliang_pjtl.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    ZUOYE_CHECK_PROBLEM_SQL,
    GUANLI_CHECK_PROBLEM_SQL, 
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    RISK_WORK_BANZU_COUNT_SQL, YECHA_CHECK_SQL)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_check_address_standard_data)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, \
        ALL_PROBLEM_NUMBER, PERSON_LOAD,\
        JIAODA_RISK_SCORE, XC_JIAODA_RISK_SCORE, REAL_CHECK_BANZU_DATA,\
        BANZU_POINT_DATA, ZUOYE_PROBLEM_COUNT, GUANLI_PROBLEM_COUNT,\
        REAL_CHECK_POINT_DATA, CHECK_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    global RISK_IDS, CHECK_ITEM_IDS
    RISK_IDS, CHECK_ITEM_IDS = risk_ids, check_item_ids

    # 统计工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 统计总人数
    PERSON_LOAD = GLV.get_value('PERSON_LOAD')

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)

    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
            *stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)

    # BANZU_POINT_DATA = pd_query(RISK_WORK_BANZU_COUNT_SQL.format(check_item_ids))
    # 工作班组信息
    work_banzu_info_data = pd_query(RISK_WORK_BANZU_COUNT_SQL.format(check_item_ids))
    work_banzu_info_data.drop(['COUNT'], inplace=True, axis=1)

    check_address_ratio_data = get_check_address_standard_data(
        work_banzu_info_data, DEPARTMENT_DATA, months_ago, check_item_ids, major, is_base_item=True)
    REAL_CHECK_BANZU_DATA, REAL_CHECK_POINT_DATA, BANZU_POINT_DATA, CHECK_POINT_DATA = check_address_ratio_data

    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, risk_ids, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 查处问题考核率
def _stats_check_problem_ratio(months_ago):
    fraction_list = GLV.get_value('stats_check_problem_ratio', (None, None))
    return check_intensity.stats_check_problem_ratio_type_two(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = " +\
                        "问题质量分累计({4})/ 总人数({5})</p>"
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """
    （70%）较大和重大安全风险问题质量分累计/总人数。
    （30%）现场检查较大风险问题质量均分：较大和重大安全风险问题质量分累计（现场检查问题）/总人数。
    :param months_ago:
    :return:
    """
    title=['较大和重大安全风险问题质量分累计({0})/总人数({1})',
            '现场检查发现较大和重大安全风险问题质量分累计({0})/总人数({1})'
            ]
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list)


# 夜查率
def _stats_yecha_ratio(months_ago):
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        CHECK_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(
        *stats_month, RISK_IDS, CHECK_ITEM_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(
        *stats_month, RISK_IDS, CHECK_ITEM_IDS)
    title = ['监控调阅时长累计({0})/总人数({1})',
             '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
             '调阅班组数({0})/作业班组数({1})']
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1], fraction_list[2],
        None, None, None
    )
    return check_intensity.new_stats_media_intensity(
        DEPARTMENT_DATA,
        PERSON_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_score_sql=media_problem_score_sql,
        media_problem_number_sql=media_problem_number_sql,
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_score_per_person, _stats_risk_score_per_person,
        _stats_yecha_ratio, _stats_check_address_ratio,
         _stats_media_intensity,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']
    ]
    item_weight = [0.25, 0.1, 0.25, 0.15, 0.15, 0.07, 0.03]
    update_major_maintype_weight(index_type=8, major=risk_type, main_type=1,
    child_index_list=[2, 3, 5, 6, 10, 7, 9], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
