#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

from flask import current_app
from app.data.major_risk_index.cheliang_huochepjtl import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.cheliang_pjtl.check_evenness_sql import (
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL,
    EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, RISK_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS, CHECK_ITEM_IDS)), DEPARTMENT_DATA)


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    return check_evenness.stats_check_time_evenness(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    from app.data.major_risk_index.common_diff_risk_and_item.common import (
        get_check_address_evenness_data)
    major = get_major_dpid(RISK_TYPE)
    (check_banzu_person_count_data, check_point_person_count_data,
    banzu_checked_count_data, point_checked_count_data, 
    check_point_connect_department_data, 
    banzu_connect_department_data) = get_check_address_evenness_data(major, months_ago, CHECK_ITEM_IDS)
    return check_evenness.stats_check_address_evenness_new(
        check_banzu_person_count_data, check_point_person_count_data,
        banzu_checked_count_data, point_checked_count_data, 
        check_point_connect_department_data, banzu_connect_department_data,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    update_major_maintype_weight(index_type=8, major=risk_type, main_type=4,
                                child_index_list=[1, 2, 3], 
                                child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
