#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from app.data.util import pd_query

def get_vitual_major_ids(zhanduan_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    # 货车站段
    # 货车运用车间 id为131
    FK_PROFESSION_DICTIONARY_ID = {
        "货车": 2135, 
        "动车": 2133,
        "客车": 2134,
        }
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(zhanduan_type, '')
    GET_VM_MAJORS_IDS_SQL = """
    select distinct TYPE3 from t_department where locate('{0}', FK_PROFESSION_DICTIONARY_ID)
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['TYPE3'].values.tolist())
