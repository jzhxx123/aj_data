#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-05-21
-------------------------------------------------
Change Activity:2019-05-21 14:49
-------------------------------------------------
"""
from app.data.util import pd_query
import pandas as pd


def get_vitual_major_ids(zhanduan_type):
    """
    客运组织 - 车务段内容
    :param risk_type:
    :return:
    """
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
    a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
FROM
    t_department AS a
        LEFT JOIN
    t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
WHERE
    a.TYPE = 4 AND a.IS_DELETE = 0
        AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
        AND a.SHORT_NAME != ''
       and a.NAME like '%%{0}%%'
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(zhanduan_type))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def add_workshop_department_info(check_banzu_count_data, department_data):
    """[summary]
    根据班组的检查项目，给它所属的未配置的车间添加进去
    Arguments:
        check_banzu_count_data {[type]} -- [description]
        department_data {[type]} -- [description]
    """
    check_banzu_count_data = pd.merge(
        check_banzu_count_data,
        department_data,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')
    workshop_dpids = check_banzu_count_data['TYPE4'].values.tolist()
    check_banzu_count_data = check_banzu_count_data[['FK_DEPARTMENT_ID']]
    for dpid in workshop_dpids:
        check_banzu_count_data = check_banzu_count_data.append(
            [{"FK_DEPARTMENT_ID": dpid}], ignore_index=True)
    check_banzu_count_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    return check_banzu_count_data
