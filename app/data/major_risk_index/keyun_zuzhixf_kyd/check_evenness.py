#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

from flask import current_app
from app.data.major_risk_index.keyun_zuzhixf_kyd import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.keyun_zuzhixf.check_evenness_sql import (
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    NORISK_GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, HOUR_CHECK_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_H_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST,
    PASSENGER_NUMBER_SQL, GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import(
    ALL_CHECK_ITEM_IDS_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd
from app.data.major_risk_index.keyun_zuzhixf_cwz.common import add_workshop_department_info
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_all_superior_ids)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, weight_list=None):
    if row[column] == 0:
        return -3
    if row[major_column] == 0:
        return 0
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 4:
            return -1
        elif _ratio <= -0.5:
            return -1
        else:
            return 0


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT,\
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,\
        CWDUAN_WORK_LOAD, PASSENGER_NUMBER, STAFF_NUMBER
    major = get_major_dpid('车务-1')
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, RISK_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    department_info_source_id = get_all_superior_ids(
        CHECK_ITEM_IDS,
        pd_query(ALL_CHECK_ITEM_IDS_SQL)
    )
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            NORISK_GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)
    # 工作量
    CWDUAN_WORK_LOAD = GLV.get_value('CWDUAN_WORK_LOAD')
    # 添加车间的配置
    CHECK_BANZU_COUNT_DATA = add_workshop_department_info(
        pd_query(CHECK_BANZU_COUNT_SQL.format(department_info_source_id)),
        DEPARTMENT_DATA)
    # 保证客运人数不为0
    CHECK_BANZU_COUNT_DATA = pd.merge(CHECK_BANZU_COUNT_DATA,
        pd_query(PASSENGER_NUMBER_SQL.format(major)),
        on='FK_DEPARTMENT_ID',
        how='inner')

    # CHECK_BANZU_COUNT_DATA = pd_query(CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = pd.merge(
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[0].format(major)),
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[1].format(*stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA.drop(
        ["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = BANZU_DEPARTMENT_CHECKED_COUNT_DATA.groupby(
        ['DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    # 客运人数
    # PASSENGER_NUMBER = df_merge_with_dpid(
    #     pd_query(PASSENGER_NUMBER_SQL.format(major)),
    #     DEPARTMENT_DATA)
    
    # 正式员工数
    STAFF_NUMBER = GLV.get_value('STAFF_NUMBER')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# # 检查时间均衡度---日期加时间段
def _stats_check_time_evenness(months_ago):
    #  班组工作项目暂时用检查项目
    time_deduct_dict = {
        0.2: 1,
        0.5: 2,
        1: 5,
    }
    date_deduct_dict = {
        0.3: 0.5,
        0.6: 1,
        1: 2,
    }
    # daily_check_banzu_count_sql = DAILY_CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS)
    return check_evenness.stats_check_time_evenness_three(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
        DAILY_CHECK_BANZU_COUNT_H_SQL, HOUR_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, date_deduct_dict=date_deduct_dict,
        time_deduct_dict=time_deduct_dict)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    #  班组工作项目暂时用检查项目
    return check_evenness.stats_check_address_evenness_work_load_ex(
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,
        STAFF_NUMBER, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, 
        months_ago, RISK_TYPE, _choose_dpid_data, is_recursive=True,
        calc_func=_calc_score_by_formula)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.50, 0.35]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
