# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.keyun_zuzhixf_kyd import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import (
    CHECK_COUNT_SQL, NORISK_ZUOYE_CHECK_PROBLEM_SQL, NORISK_GUANLI_CHECK_PROBLEM_SQL,
    BANZU_POINT_SQL, NORISK_LEVEL_PROBLEM_SQL, NORISK_PROBLEM_CHECK_SCORE_SQL,
    NORISK_XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL, NORISK_MEDIA_PROBLEM_NUMBER_SQL, NORISK_MEDIA_PROBLME_SCORE_SQL,
    RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, RISK_WORK_BANZU_COUNT_SQL, 
    REAL_CHECK_BANZU_SQLIST, ALL_CHECK_ITEM_IDS_SQL)
from app.data.major_risk_index.keyun_zuzhixf.check_evenness_sql import (PASSENGER_NUMBER_SQL)
from app.data.major_risk_index.keyun_zuzhixf_cwz.common import add_workshop_department_info
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_all_superior_ids)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid('车务-0')
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, GUANLI_PROBLEM_COUNT, \
        ZUOYE_PROBLEM_COUNT, ASSESS_ZUOYE_PROBLEM_COUNT, \
        ASSESS_GUANLI_PROBLEM_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE,\
        REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    department_source_ids = get_all_superior_ids(
        check_item_ids,
        pd_query(ALL_CHECK_ITEM_IDS_SQL))
    global RISK_IDS, CHECK_ITEM_IDS, DEPARTMENT_SOURCE_IDS
    CHECK_ITEM_IDS = check_item_ids
    RISK_IDS = risk_ids
    DEPARTMENT_SOURCE_IDS = department_source_ids
    # 统计工作量
    WORK_LOAD = GLV.get_value('CWDUAN_WORK_LOAD')

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(NORISK_ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            NORISK_GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(NORISK_PROBLEM_CHECK_SCORE_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(NORISK_LEVEL_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            NORISK_XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, check_item_ids)), DEPARTMENT_DATA)

    REAL_CHECK_BANZU_DATA = pd.merge(
        pd_query(REAL_CHECK_BANZU_SQLIST[0].format(major)),
        pd_query(REAL_CHECK_BANZU_SQLIST[1].format(*stats_month, check_item_ids)),
        how='inner',
        left_on='FK_CHECK_INFO_ID', 
        right_on='PK_ID'
    )
    REAL_CHECK_BANZU_DATA.drop(["PK_ID","FK_CHECK_INFO_ID"], inplace=True, axis=1)
    filled_dpids = add_workshop_department_info(
        REAL_CHECK_BANZU_DATA[['FK_DEPARTMENT_ID']],
        DEPARTMENT_DATA
    )
    REAL_CHECK_BANZU_DATA = pd.merge(
        REAL_CHECK_BANZU_DATA,
        filled_dpids,
        on='FK_DEPARTMENT_ID',
        how='right'
    )
    REAL_CHECK_BANZU_DATA.fillna({"COUNT":1}, inplace=True)
    REAL_CHECK_BANZU_DATA.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    REAL_CHECK_BANZU_DATA = pd.merge(REAL_CHECK_BANZU_DATA,
        pd_query(PASSENGER_NUMBER_SQL.format(major)),
        on='FK_DEPARTMENT_ID',
        how='inner')

    BANZU_POINT_DATA = pd_query(RISK_WORK_BANZU_COUNT_SQL.format(department_source_ids))
    filled_dpids = add_workshop_department_info(
        BANZU_POINT_DATA[['FK_DEPARTMENT_ID']],
        DEPARTMENT_DATA
    )
    BANZU_POINT_DATA = pd.merge(
        BANZU_POINT_DATA,
        filled_dpids,
        on='FK_DEPARTMENT_ID',
        how='right'
    )
    BANZU_POINT_DATA.fillna({"COUNT":1}, inplace=True)
    BANZU_POINT_DATA = pd.merge(BANZU_POINT_DATA,
        pd_query(PASSENGER_NUMBER_SQL.format(major)),
        on='FK_DEPARTMENT_ID',
        how='inner')
    REAL_CHECK_BANZU_DATA = REAL_CHECK_BANZU_DATA[
        REAL_CHECK_BANZU_DATA["FK_DEPARTMENT_ID"].isin(
            BANZU_POINT_DATA['FK_DEPARTMENT_ID'].values.tolist())]
    filled_data = pd_query(BANZU_POINT_SQL.format(major))
    filled_data['COUNT'] = 0
    if REAL_CHECK_BANZU_DATA.empty and BANZU_POINT_DATA.empty:
        # 两个均为空时，补充检查班组数
        REAL_CHECK_BANZU_DATA = filled_data.copy()
        BANZU_POINT_DATA = filled_data.copy()
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    fraction_list = GLV.get_value('stats_check_problem_ratio', (None, None))
    return check_intensity.stats_check_problem_ratio_type_two(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        child_weight=[0.7, 0.3],
        fraction_list=fraction_list)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_problem_score_sql = NORISK_MEDIA_PROBLME_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)
    media_problem_number_sql = NORISK_MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_ITEM_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0],
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(DEPARTMENT_SOURCE_IDS)
    ]
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql, 
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_person, _stats_yecha_ratio,
        _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']
    ]
    item_weight = [0.3, 0.1, 0.25, 0.1, 0.20, 0.02, 0.03]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=1, 
    child_index_list=[2, 3, 5, 6, 10, 7, 9], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
