#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.keyun_zuzhixf_kyd import GLV
from app.data.major_risk_index.keyun_zuzhixf_kyd.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL,
    EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import (
    CW_SPECIAL_CONFIG_SQL
)
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.keyun_zuzhixf_kyd.common import get_vitual_major_ids
from app.data.major_risk_index.util import df_merge_with_dpid
import pandas as pd


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    ids = get_vitual_major_ids("客运段")
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(ids))
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, ids))

    # 客运段 - 正式职工数
    staff_number = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA
    )
    # 客运段工作量
    CWDUAN_WORK_LOAD = pd.concat([staff_number, ZHANDUAN_STAFF], axis=0, sort=False)

    cw_special_config_data = pd_query(CW_SPECIAL_CONFIG_SQL)
    # 客运段 - 客运职工数
    KEYUN_STAFF_NUMBER = df_merge_with_dpid(
        cw_special_config_data[['FK_DEPARTMENT_ID','PASSENGER_NUMBER']],
        DEPARTMENT_DATA
    ).rename(columns={"PASSENGER_NUMBER": "COUNT"})

    # 客运段 - 客运职工干部数
    KEYUN_CADRE_NUMBER = df_merge_with_dpid(
        cw_special_config_data[['FK_DEPARTMENT_ID','MANAGE_NUMBER']],
        DEPARTMENT_DATA
    ).rename(columns={"MANAGE_NUMBER": "COUNT"})

    # 客运段 - 正式职工数
    STAFF_NUMBER = pd.merge(
        pd.concat([
            KEYUN_STAFF_NUMBER[['DEPARTMENT_ID', 'COUNT']], 
            KEYUN_CADRE_NUMBER[['DEPARTMENT_ID', 'COUNT']]], ignore_index=True), 
        DEPARTMENT_DATA, 
        on='DEPARTMENT_ID',
        how='inner'
    )
    

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "CWDUAN_WORK_LOAD": CWDUAN_WORK_LOAD,
        "STAFF_NUMBER": STAFF_NUMBER,
        "KEYUN_STAFF_NUMBER": KEYUN_STAFF_NUMBER,
        "KEYUN_CADRE_NUMBER": KEYUN_CADRE_NUMBER,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
