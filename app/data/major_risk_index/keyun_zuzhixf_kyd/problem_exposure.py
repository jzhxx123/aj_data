# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.keyun_zuzhixf_kyd import GLV
from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.keyun_zuzhixf_cwz.common_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.keyun_zuzhixf.problem_exposure_sql import (
    NORISK_CHECK_PROBLEM_SQL, NORISK_CHECKED_HIDDEN_PROBLEM_POINT_SQL,
    NORISK_EXPOSURE_PROBLEM_DEPARTMENT_SQL, NORISK_HIDDEN_KEY_PROBLEM_MONTH_SQL,
    NORISK_HIDDEN_KEY_PROBLEM_SQL, NORISK_HIDDEN_PROBLEM_POINT_SQL, NORISK_OTHER_CHECK_PROBLEM_SQL,
    NORISK_SAFETY_PRODUCE_INFO_SQL, NORISK_SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_all_superior_ids)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import (
    ALL_CHECK_ITEM_IDS_SQL)

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER,\
        WORK_LOAD, HIDDEN_PROBLEM_POINT_DATA, CHECKED_HIDDEN_PROBLEM_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    department_source_ids = get_all_superior_ids(
        diaoche[0],
        pd_query(ALL_CHECK_ITEM_IDS_SQL))

    # 统计工作量
    data = GLV.get_value('CWDUAN_WORK_LOAD')
    WORK_LOAD = data.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    # 正式职工数
    data = GLV.get_value('STAFF_NUMBER')
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')

    global CHECK_ITEM_IDS, RISK_IDS, DEPARTMENT_SOURCE_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    DEPARTMENT_SOURCE_IDS = department_source_ids

    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(NORISK_CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(
            *stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA
    )

    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(NORISK_HIDDEN_PROBLEM_POINT_SQL.format(CHECK_ITEM_IDS)),
        DEPARTMENT_DATA
    )


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    fraction_list = GLV.get_value('stats_total_problem_exposure', 
                    (None, None, None, None, None, None, None, None))
    for fraction in fraction_list:
        if fraction:
            fraction.detail_type = 1
    fraction_list = (
        (fraction_list[0],fraction_list[1]),
        (fraction_list[2],fraction_list[3]),
        (fraction_list[4],fraction_list[5]),
        (fraction_list[6],fraction_list[7]),
    )
    title = [
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})', '作业项问题数({0})/职工总人数({1})',
            '一般及以上作业项问题数({0})/职工总人数({1})'
        ]
    return problem_exposure.stats_total_problem_exposure_type_chewu(
        CHECK_ITEM_IDS, NORISK_CHECK_PROBLEM_SQL, WORK_LOAD, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, 
        title=title,
        fraction_list=fraction_list)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    return problem_exposure.stats_problem_exposure(
        CHECK_ITEM_IDS, ZHANDUAN_DPID_DATA, NORISK_HIDDEN_KEY_PROBLEM_SQL,
        NORISK_HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 较严重隐患问题暴露度
def _stats_hidden_problem_exposure(months_ago):
    calc_func = lambda x: 0 if (
        100 - x) * 0.15 < 0 else round((100 - x) * 0.15, 2)
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, calc_score_formula=calc_func)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    banzu_point_sql = BANZU_POINT_SQL.format(DEPARTMENT_SOURCE_IDS)
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_ITEM_IDS, banzu_point_sql, NORISK_EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    problem_risk_score = {'1': 3, '2': 1, '3': 0.1, }
    return problem_exposure.stats_other_problem_exposure(
        CHECK_ITEM_IDS, NORISK_SELF_CHECK_PROBLEM_SQL, NORISK_OTHER_CHECK_PROBLEM_SQL,
        NORISK_SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        problem_risk_score=problem_risk_score)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.5, 0.15, 0.25, 0.1, -1]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=5,
                                 child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
