#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
from app.data.major_risk_index.keyun_zuzhixf_kyd.common import get_vitual_major_ids
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.keyun_zuzhixf_kyd.common_sql import (
    CADRE_COUNT_SQL, WORK_LOAD_SQL, WORKER_COUNT_SQL
)
from app.data.major_risk_index.keyun_zuzhixf.assess_intensity_sql import (
    NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL, NORISK_KAOHE_GL_CHECK_PROBLEM_SQL,
    GANBU_ASSESS_RESPONSIBLE_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from flask import current_app
from app.data.major_risk_index.keyun_zuzhixf_kyd import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import assess_intensity


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, \
        WORK_LOAD, ASSESS_ZUOYE_PROBLEM_COUNT, ASSESS_GUANLI_PROBLEM_COUNT, \
        CADRE_COUNT, WORKER_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, \
        WORKER_ASSESS_RESPONSIBLE_MONEY
    ids = get_vitual_major_ids('客运段')
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]

    # 统计工作量【职工总人数】
    # WORK_LOAD = df_merge_with_dpid(
    #     pd_query(WORK_LOAD_SQL.format(ids)),
    #     DEPARTMENT_DATA)
    WORK_LOAD = GLV.get_value('STAFF_NUMBER')
    # 干部数
    # CADRE_COUNT = df_merge_with_dpid(
        # pd_query(CADRE_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    CADRE_COUNT = GLV.get_value('KEYUN_CADRE_NUMBER')
    # 非干部
    # WORKER_COUNT = df_merge_with_dpid(
        # pd_query(WORKER_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    WORKER_COUNT = GLV.get_value('KEYUN_STAFF_NUMBER')
    # 考核作业项问题数
    ASSESS_ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核管理项问题数
    ASSESS_GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            NORISK_KAOHE_GL_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额（干部）
    CADRE_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(GANBU_ASSESS_RESPONSIBLE_SQL.format(
            year, month, check_item_ids)),
        DEPARTMENT_DATA)
    # 月度考核总金额（非干部）
    WORKER_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(FEIGANBU_ASSESS_RESPONSIBLE_SQL.format(
            year, month, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    title = ['客运组织及客运消防问题（作业类）考核数({0})/客运组织及客运消防作业人员总数({1})',
             '客运组织及客运消防问题（管理类+设备质量问题）考核数({0})/管理人员总数({1})']
    fraction_list = GLV.get_value('stats_check_problem_assess_radio', (None,None))
    return assess_intensity.stats_check_problem_assess_radio_type_two_type_chewu(
        ASSESS_ZUOYE_PROBLEM_COUNT,
        ASSESS_GUANLI_PROBLEM_COUNT,
        WORKER_COUNT,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        child_weight=[0.8, 0.2],
        title=title,
        fraction_list=fraction_list)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    fraction_list = GLV.get_value('stats_assess_money_per_person', (None,None))
    return assess_intensity.stats_assess_money_per_person_type_two(
        CADRE_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, WORKER_COUNT,
        WORKER_ASSESS_RESPONSIBLE_MONEY, months_ago, RISK_TYPE,
        _choose_dpid_data,
        fraction_list=fraction_list)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b']]
    item_weight = [0.5, 0.5]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=3,
                                 child_index_list=[1, 2], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
