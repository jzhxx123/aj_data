from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import(
    CHECK_BANZU_COUNT_SQL, SPECIAL_BANZU_CHECKED_COUNT_SQL,
    DAILY_CHECK_COUNT_SQL,
    BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST, GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    EX_GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)

# 派单人时记录，先按END_TIME计算, 取出之后再做跨天的时长分配
# gw_t_ps_dailyplan表中的tyoe字段表示作业类型
# 天窗修：{0:"请点“,(5,8):"一级维修",(6,9):"二级维修"}
# 施工：{"一级施工","二级施工",(3,4):"三级施工"}
# 点外修:{(1,2):"点外作业"}
DAILY_CHECK_HOUR_SQL = """SELECT
        a.DEPARTID as FK_DEPARTMENT_ID,
        a.ON_RAILWAY_NUMBER,
        a.ON_RAILWAY_NUMBER * ( UNIX_TIMESTAMP( b.END_TIMES ) - UNIX_TIMESTAMP( b.START_TIMES ) ) / 3600 AS HOURS,
        CONVERT(DATE_FORMAT( b.END_TIMES, '%%d' ), UNSIGNED) AS DAY,
        DATE_FORMAT(b.END_TIMES, '%%Y-%%m-%%d %%H:%%i') as ET,
        DATE_FORMAT(b.START_TIMES, '%%Y-%%m-%%d %%H:%%i') as ST
    FROM
        gw_t_ps_dailyplan AS a
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND 
        a.TYPE IN ( 1, 2 ) 
        AND 
        b.DISPATCH_ORDER_STATUS = 1
"""
