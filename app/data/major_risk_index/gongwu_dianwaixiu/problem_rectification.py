# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd
from app.data.major_risk_index.gongwu_dianwaixiu import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.gongwu_dianwaixiu.problem_rectification_sql import (
    HAPPEN_PROBLEM_POINT_SQL, CHECK_EVALUATE_SZ_NUMBER_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL)
from app.data.index.util import (get_custom_month)
from app.data.index.common import (
    format_export_basic_data, write_export_basic_data_to_mongo, calc_child_index_type_sum)
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.index.util import (get_query_condition_by_risktype)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common import problem_rectification

SCORE = []
HIERARCHY = [3]


def _calc_check_evaluate_score(row, code_dict):
    score = 0
    for code in code_dict:
        score += int(row[code]) * int(code_dict[code])
    return min(100, score)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]


# 问题整改
def _stats_rectification_overdue(months_ago):
    """
    问题超期整改扣分：问题整改超期1条扣2分；
    :param months_ago:
    :return:
    """
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=2)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    """
    履职评价指数：履职评价指数：履职评价中一条ZG-1、2、3、4、5分别按下表加分。（施工、天窗修、点外修）。
    :param months_ago:
    :return:
    """
    calc_month = get_custom_month(months_ago)
    check_evaluate_sz_number = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(*calc_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 获取每个站段各个code的数量
    # code_list = ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-5"]
    code_dict = {"ZG-1": 75,
                 "ZG-2": 60,
                 "ZG-3": 45,
                 "ZG-4": 30,
                 "ZG-5": 15}
    data = ZHANDUAN_DPID_DATA
    for code in code_dict:
        code_data = check_evaluate_sz_number[check_evaluate_sz_number["CODE"] == code]
        code_data = code_data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
        code_data.rename(columns={"COUNT": code}, inplace=True)
        data = pd.merge(
            data,
            code_data.loc[:, [code, "TYPE3"]],
            how="left",
            left_on="DEPARTMENT_ID",
            right_on="TYPE3")
        data.drop(["TYPE3"], inplace=True, axis=1)
    data["TYPE3"] = data["DEPARTMENT_ID"]
    data.fillna(0, inplace=True)

    data['CONTENT'] = data.apply(
        lambda row: '<br/>'.join([f'本月{col}履职评价发生条数：{int(row[col])}条' for col in code_dict]), axis=1
    )

    data['SCORE'] = data.apply(lambda row: _calc_check_evaluate_score(row, code_dict), axis=1)
    # 中间过程
    data_rst = format_export_basic_data(data, 6, 2, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 2, risk_type=RISK_TYPE)
    # 最终结果
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        NA_value=True)
    return rst_index_score


# 反复指数
# TODO 当月没有工作量的班组不扣分，倒查分析时倒查月份没有工作量的终止倒查。
def _stats_repeatedly_index(months_ago):
    """
    以问题项点统计（路外设备质量考核金额为0的不统计。
    站段级：
    以站段为统计单位同一问题项点是否发生，
    严重风险的问题3月内重复发生一个扣a分，
    较大风险的（当月累计发生3次以上算重复发生）问题3月内重复发生一项扣a分，
    一般风险的（当月累计发生10次以上算重复发生）问题3月内重复发生一项扣a分。
    :param months_ago:
    :return:
    """
    return problem_rectification.stats_repeatedly_index(
        CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.1, 0.2, 0.7]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
