# 月度考核总金额(关联问题)
ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            INNER JOIN
        t_check_info as e on c.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 月度返奖金额
AWARD_RETURN_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
            inner join 
        t_check_info as d on c.FK_CHECK_INFO_ID = d.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND d.CHECK_WAY NOT BETWEEN 5 AND 6
            AND d.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND d.CHECK_TYPE NOT IN (102, 103)
"""

# 考核问题数（基本问题库, 包含路外）
KAOHE_PROBLEM_BASE_SQL = """
    SELECT 
    max(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_info as e on a.FK_CHECK_INFO_ID = e.PK_ID
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_risk AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department as d on a.pk_id = d.FK_CHECK_PROBLEM_ID
WHERE
    (b.IS_PROFESSION_ASSESS = 1
        OR b.IS_UNIT_ASSESS = 1)
    AND e.CHECK_WAY NOT BETWEEN 5 AND 6
    AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND e.CHECK_TYPE NOT IN (102, 103)
    AND
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND c.FK_RISK_ID in ({2})
    GROUP BY a.PK_ID
"""

# 月度考核总金额(职工)
PERSON_ASSESS_RESPONSIBLE_SQL = """SELECT
        distinct a.pk_id, b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT,
        a.IS_OUT_SIDE_PERSON
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
            INNER JOIN
        t_check_info as d on c.FK_CHECK_INFO_ID = d.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.CHECK_WAY NOT BETWEEN 5 AND 6
        AND d.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND d.CHECK_TYPE NOT IN (102, 103)
"""

# 月度考核人员(职工)
PERSON_ASSESS_COUNT_SQL = """
SELECT 
    distinct a.pk_id,
    b.FK_DEPARTMENT_ID,
    1 AS COUNT,
    a.IS_OUT_SIDE_PERSON
FROM
    t_safety_assess_month_responsible_detail AS a
        INNER JOIN
    t_person AS b ON a.ID_CARD = b.ID_CARD
        INNER JOIN
    t_safety_assess_month AS c ON a.FK_SAFETY_ASSESS_MONTH_ID = c.PK_ID
        INNER JOIN
    t_check_problem AS d ON a.FK_CHECK_PROBLEM_ID = d.PK_ID
        INNER JOIN
    t_check_info as e on d.FK_CHECK_INFO_ID = e.PK_ID
WHERE
    c.STATUS = 3 
    AND c.YEAR = {0}
    AND c.MONTH = {1}
    AND d.FK_CHECK_ITEM_ID IN ({2})
    AND e.CHECK_WAY NOT BETWEEN 5 AND 6
    AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND e.CHECK_TYPE NOT IN (102, 103)
"""

# 考核问题数（基本问题库, 不包含路外）
NO_EXTERNAL_KAOHE_PROBLEM_BASE_SQL = """
    SELECT 
    max(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_info as e on a.FK_CHECK_INFO_ID = e.PK_ID
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_risk AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department as d on a.pk_id = d.FK_CHECK_PROBLEM_ID
WHERE
    (b.IS_PROFESSION_ASSESS = 1
        OR b.IS_UNIT_ASSESS = 1)
    AND e.CHECK_WAY NOT BETWEEN 5 AND 6
    AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND e.CHECK_TYPE NOT IN (102, 103)
    AND a.IS_EXTERNAL = 0
    AND
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND c.FK_RISK_ID in ({2})
    GROUP BY a.PK_ID
"""

# 所有问题数（非路外）
NO_EXTERNAL_ALL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.IS_EXTERNAL = 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""