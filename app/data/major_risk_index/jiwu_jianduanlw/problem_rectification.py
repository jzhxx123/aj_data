# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_jianduanlw import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index, df_merge_with_dpid)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.jiwu_jianduanlw.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL,
    CHECK_EVALUATE_SZ_NUMBER_SQL, PRECONTROL_RESPONSIBLE_UNIT_SQL,
    WARINING_NOTIFICATION_COUNT_SQL)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.util import calc_child_index_type_divide_major, data_complete_by_condition
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL)
SCORE = []
HIERARCHY = [3]


def _calc_problem_control_func(row, column, major_column, detail_type):
    """
    计算问题控制
    :return:
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = row[column] / row[major_column]

    if 1.3 >= _ratio >= 0.9:
        _score = 100
    elif 0.9 > _ratio >= 0.8 or 1.3 < _ratio <= 1.4:
        _score = 95
    elif 0.7 <= _ratio < 0.8 or 1.4 < _ratio <= 1.5:
        _score = 90
    else:
        _score = 85
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        CHECK_EVALUATE_SZ_NUMBER_DATA, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,\
        PRECONTROL_RESPONSIBLE_UNIT_DATA, WARINING_NOTIFICATION_COUNT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS, PROBLEM_COUNT, APPLY_WORKSHOP_COUNT
    stats_month = get_custom_month(months_ago)
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]

    # 间断瞭望问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )

    # 基本工作量（运用车间人数)
    APPLY_WORKSHOP_COUNT = GLV.get_value('APPLY_WORKSHOP_COUNT')

    # 评价履职类型统计
    CHECK_EVALUATE_SZ_NUMBER_DATA = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )

    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )

    PRECONTROL_RESPONSIBLE_UNIT_DATA = data_complete_by_condition(
        pd_query(PRECONTROL_RESPONSIBLE_UNIT_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE'], [1], DEPARTMENT_DATA
    )

    WARINING_NOTIFICATION_COUNT_DATA = data_complete_by_condition(
        pd_query(WARINING_NOTIFICATION_COUNT_SQL.format(*stats_month)),
        ZHANDUAN_DPID_DATA,
        ['TYPE', 'RANK'],
        [1, 1],
        DEPARTMENT_DATA
    )


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        _score = int(row['COUNT_A']) * (25 - 5 * (int(row['RESP_LEVEL'])))
    return _score


def _calc_rectification_effect_type_d(row):
    """[计算根据级别的预警通知书]
    III级警告预警通知书一次扣5分；
    II级警告预警通知书一次扣10分；
    I级警告预警通知书一次扣20分。
    注:警告通知书的层级为“路局”或“专业”，不包括“站段”。
    Arguments:
        row {[type]} -- [description]
    """
    _score = int(row['COUNT_D']) * 5 * 2**(3 - int(row['RANK']))
    return _score


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    code_dict = {"ZG-1": 35,
                 "ZG-2": 25,
                 "ZG-3": 30,
                 "ZG-4": 15,
                 "ZG-5": 15}
    return problem_rectification.stats_check_evaluate_by_codetype(
        CHECK_EVALUATE_SZ_NUMBER_DATA, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        calc_func=lambda x: min(100, x),
        code_dict=code_dict)


# 问题控制 - 机务间断瞭望
def _stats_problem_contrl_ration(months_ago):
    """
    间断瞭望问题数 / 基本工作量
    :param months_ago:
    :return:
    """
    return calc_child_index_type_divide_major(
        PROBLEM_COUNT, APPLY_WORKSHOP_COUNT, 2, 6, 3, months_ago,
        "COUNT", 'SCORE_c', calc_func=_calc_problem_control_func,
        dpid_func=_choose_dpid_data, is_calc_score_base_major=True,
        hierarchy_list=[3], risk_type=RISK_TYPE)


def _stats_rectification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,
        'C': PRECONTROL_RESPONSIBLE_UNIT_DATA,
        'D': WARINING_NOTIFICATION_COUNT_DATA,
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a,
        'D': _calc_rectification_effect_type_d
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        df_dict,rectification_type_calc_func_dict=rectification_type_calc_func_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_problem_contrl_ration,
        _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'f']]
    item_weight = [0.2, 0.3, 0.5, -1]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3, 6],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
