#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_jianduanlw import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.jiwu_jianduanlw.assess_intensity_sql import (
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL, ACTUAL_RETURN_MONTY_PROBLEM_SQl,
    RETURN_MONTY_PROBLEM_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]

    if _ratio >= 0.1:
        if detail_type == 3:
            _score = 90 - (_ratio - 0.1)*100
        else:
            _score = 100
    elif _ratio >= -0.1:
        _score = 90
    else:
        if detail_type == 3:
            _score = 100
        else:
            _score = 90 - (-0.1 - _ratio)*100
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, \
        APPLY_WORKSHOP_COUNT, \
        ACTUAL_RETURN_MONTY_PROBLEM_COUNT, RETURN_MONTY_PROBLEM_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]

    # 基本工作量（运用车间人数)
    APPLY_WORKSHOP_COUNT = GLV.get_value('APPLY_WORKSHOP_COUNT')

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, risk_ids)),
        DEPARTMENT_DATA)
    
    # 实际返奖错误操纵问题个数
    ACTUAL_RETURN_MONTY_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ACTUAL_RETURN_MONTY_PROBLEM_SQl.format(year, month, risk_ids)),
        DEPARTMENT_DATA)

    # 达到返奖时限错误操纵问题问题个数
    RETURN_MONTY_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(RETURN_MONTY_PROBLEM_SQL.format(year, month, risk_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = " +\
                        "考核问题数({4})/基本工作量 ({5})</p>"
    fraction = GLV.get_value("stats_check_problem_assess_radio",(None,))[0]
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        APPLY_WORKSHOP_COUNT,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) =" +\
                        "月度考核总金额({4})/ 基本工作量({5})</p>"
    fraction = GLV.get_value("stats_assess_money_per_person", (None,))[0]
    return assess_intensity.stats_assess_money_per_person_type_one_major(
        ASSESS_RESPONSIBLE_MONEY,
        APPLY_WORKSHOP_COUNT,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 返奖率
def _stats_award_return_ratio(months_ago):
    """
    返奖率=月度返奖金额/月度考核金额。低于基数10%开始扣分（返奖为高的扣分）
    :param months_ago:
    :return:
    """
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>返奖率({3}) = " + \
                        "实际返奖间断瞭望问题个数({4})/达到返奖时限间断瞭望问题问题个数({5})</p>"
    fraction = GLV.get_value("stats_award_return_ratio", (None,))[0]
    return assess_intensity.stats_award_return_ratio(
        ACTUAL_RETURN_MONTY_PROBLEM_COUNT,
        RETURN_MONTY_PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.50, 0.40, 0.10]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=3,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
