# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_jianduanlw import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_exposure

from app.data.major_risk_index.jiwu_jianduanlw.problem_exposure_sql import (
    HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL, CHECK_PROBLEM_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, ANALYSIS_CHECK_PROBLEM_SQL,
    CHECKED_HIDDEN_PROBLEM_POINT_SQL, HIDDEN_PROBLEM_POINT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


HIERARCHY = [3]


def _calc_score_by_formula_type_jiwu(row,
                                     column,
                                     major_column,
                                     detail_type=None):
    """【机务】该比值≥20%，得分为100分；10%≤该比值〈20%，得分90分；
        5%≤该比值〈10%，得分80分；该比值〈5%，得分70分。
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0.0:
        _score = 90 + _ratio*50
    elif _ratio >= -0.15:
        _score = 80 + (_ratio+0.15)*60
    elif _ratio >= -0.3:
        _score = 80 + (_ratio+0.15)*200
    else:
        _score = 50 + (_ratio+0.3)*300
    _score = max(60, _score)
    _score = min(100, _score)
    return _score


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER,\
            CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA,CHECK_PROBLEM_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)

    # 基本工作量【职工总人数】
    data = GLV.get_value('APPLY_WORKSHOP_COUNT')
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)),
         DEPARTMENT_DATA)
    
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)),
        DEPARTMENT_DATA
    )

    CHECK_PROBLEM_DATA = pd_query(CHECK_PROBLEM_SQL.format(*stats_month, RISK_IDS))

# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    title = [
        'A/B问题数({0})/基本工作量({1})',
        'A/B/C问题数({0})/基本工作量({1})',
        ]
    check_problem_data_list = [
        CHECK_PROBLEM_DATA[CHECK_PROBLEM_DATA['LEVEL'].isin(['A', 'B'])],
        CHECK_PROBLEM_DATA[CHECK_PROBLEM_DATA['LEVEL'].isin(['A', 'B', 'C'])],
    ]
    # return problem_exposure.stats_total_problem_exposure_type_jiwu(
    #     RISK_IDS, CHECK_PROBLEM_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
    #     months_ago, RISK_TYPE, _choose_dpid_data, 
    #     calc_score_formula=_calc_score_by_formula_type_jiwu,
    #     title=title)
    fraction_list = GLV.get_value('stats_total_problem_exposure', (None, None))
    for fraction in fraction_list:
        fraction.detail_type = 1
    fraction_list = (
        (fraction_list[0], fraction_list[1]),
        (fraction_list[2], fraction_list[3]),
    )
    return problem_exposure.stats_total_problem_exposure_excellent(
        check_problem_data_list, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, 
        calc_score_formula=_calc_score_by_formula_type_jiwu,
        title=title,
        weight_item=[0.7, 0.3],
        weight_part=[0.6, 0.4],
        fraction_list=fraction_list)


def _stats_analysis_problem_exposure(months_ago):
    stats_month = get_custom_month(months_ago)
    prob_data = ANALYSIS_CHECK_PROBLEM_SQL.format(*stats_month, RISK_IDS)
    return problem_exposure.stats_analysis_problem_exposure_type_jiwu(
        prob_data, STAFF_NUMBER, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 事故隐患问题暴露
def _stats_problem_exposure(months_ago):
    customizededuct = {
        3:1,
        4:2
    }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA,
        HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        customizededuct=customizededuct, months=5)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    problem_risk_score = {'1': 10, '2': 10, '3': 10, }
    return problem_exposure.stats_other_problem_exposure(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, 
        problem_risk_score=problem_risk_score)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_other_problem_exposure,
        _stats_hidden_problem_exposure,
        _stats_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'e']]
    item_weight = [0.7, 0.15, 0.15, -1]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=5,
                                 child_index_list=[1, 2, 3, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
