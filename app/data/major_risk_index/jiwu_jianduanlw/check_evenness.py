#!/usr/bin/python3
# -*- coding: utf-8 -*-


from flask import current_app
import pandas as pds
from app.data.major_risk_index.jiwu_jianduanlw import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.jiwu_jianduanlw.check_evenness_sql import (
    DAILY_CHECK_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST,
    CHECK_BANZU_COUNT_SQL, DAILY_CHECK_BANZU_COUNT_SQL,
    RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column):
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 6:
            return -2
        elif _ratio <= -0.6:
            return -2
        else:
            return 0


def _calc_time_evenness_compare_value(months_ago):
    """
    计算时间均衡度自定义基数值 每日检查次数/基本工作量
    :param check_item_ids:
    :param check_banzu_count_sql:
    :param banzu_department_checked_count_sql:
    :param department_data:
    :param ZHANDUAN_DPID_DATA:
    :param months_ago:
    :param RISK_TYPE:
    :param _choose_dpid_data:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    daily_check_count = df_merge_with_dpid(
        pd_query(DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 统计每日检查数
    daily_check_count = daily_check_count.groupby(
        ['DAY'])['COUNT'].sum().reset_index()
    # 统计基本工作量
    work_load = sum(APPLY_WORKSHOP_COUNT['COUNT'])
    # 每日基数
    daily_check_count['AVG_NUMBER'] = daily_check_count['COUNT'] / work_load
    return daily_check_count


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, RISK_ABOVE_PROBLEM_POINT_COUNT, \
        APPLY_WORKSHOP_COUNT, BANZU_DEPARTMENT_CHECKED_COUNT_DATA, CHECK_BANZU_COUNT_DATA

    major = get_major_dpid(risk_type)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    global RISK_IDS
    RISK_IDS = diaoche[1]
    # 一次乘务作业parent_id
    #  这种特殊的检查项目id，目前静态配置
    # ONE_TIME_ON_BOARD_ID = '885'
    # ALL_CHECK_ON_BOARD_ID = retrieve_all_check_item_ids(ONE_TIME_ON_BOARD_ID)


    # 基本工作量（运用车间人数)
    APPLY_WORKSHOP_COUNT = GLV.get_value('APPLY_WORKSHOP_COUNT')

    # 较大及以上问题项点数
    RISK_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中较大及以上风险项点问题数
    RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS)), DEPARTMENT_DATA)
    
    CHECK_BANZU_COUNT_DATA = pd_query(CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = pd.merge(
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[0].format(major)),
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[1].format(*stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA.drop(
        ["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = BANZU_DEPARTMENT_CHECKED_COUNT_DATA.groupby(
        ['DEPARTMENT_ID'])['COUNT'].sum().reset_index()


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = " + \
                        "查出较大及以上问题项点数({4})/ 基础问题库中较大及以上问题项点数({5})</p>"
    return check_evenness.stats_problem_point_evenness(
        RISK_ABOVE_PROBLEM_POINT_COUNT,
        RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data,
        customizecontent=customizecontent)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    # 获取基数
    basedata = _calc_time_evenness_compare_value(months_ago)

    return check_evenness.stats_check_time_evenness(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, basedata=basedata)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_evenness.stats_check_address_evenness_work_load_ex(
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,
        APPLY_WORKSHOP_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, 
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.2, 0.4, 0.4]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
