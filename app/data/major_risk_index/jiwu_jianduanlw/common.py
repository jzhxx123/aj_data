#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-05-29
-------------------------------------------------
Change Activity:2019-05-29 19:15
-------------------------------------------------
"""
from app.data.util import pd_query


def retrieve_all_dp_configid(top_ids):
    """
    获取所有检查项目及其子检查项目的ID。
    top_ids: list - 顶层的check_item_id列表
    return: string - t_check_item.PK_ID的列表。如 101,102,103。
                     返回的列表中包含top_ids
    """
    # 查询所属的子check_item的PK_ID
    load_child_check_item_sql = "SELECT PK_ID FROM t_department_classify_config WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret


def retrieve_all_check_item_ids(top_ids):
    """
    获取所有检查项目及其子检查项目的ID。
    top_ids: list - 顶层的check_item_id列表
    return: string - t_check_item.PK_ID的列表。如 101,102,103。
                     返回的列表中包含top_ids
    """
    # 查询所属的子check_item的PK_ID 一次乘务作业-间断瞭望（909）
    load_child_check_item_sql = "SELECT PK_ID FROM t_check_item WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret
