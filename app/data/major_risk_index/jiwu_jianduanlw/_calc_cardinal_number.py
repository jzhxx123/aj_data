# -*- coding: utf-8 -*-
"""
机务间断瞭望指数，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)

from app.data.major_risk_index.jiwu_jianduanlw import GLV
import pandas as pd
from app.data.util import (
    pd_query)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)
from app.data.major_risk_index.common.common_sql import (
    BASE_UNIT_INFO_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_jianduanlw.assess_intensity_sql import(
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL,
    ACTUAL_RETURN_MONTY_PROBLEM_SQl, RETURN_MONTY_PROBLEM_SQL)
from app.data.major_risk_index.common.common_sql import(
    WORKER_COUNT_SQL)
from app.data.major_risk_index.jiwu_jianduanlw.common_sql import (
    CADRE_COUNT_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.jiwu_jianduanlw.check_intensity_sql import (
    CHECK_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    WORKER_COUNT_INFO,ZUOYE_PROBLEM_COUNT_INFO,
    CADRE_COUNT_INFO, STAFF_NUMBER_INFO,
    YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    ASSESS_RESPONSIBLE_INFO, ZUOYE_PROBLEM_CHECK_SCORE_INFO,
    ASSESS_PROBLEM_INFO,ANALYSIS_CENTER_PROBLEM_COUNT_INFO,
    ANALYSIS_CENTER_PROBLEM_SCORE_INFO,
    AWARD_RETURN_MONEY_PROBLEM_INFO, REAL_AWARD_RETURN_MONEY_PROBLEM_INFO, WORKER_LOAD_INFO, )


# 作业项问题信息
ZUOYE_CHECK_PROBLEM_INFO_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        -- max(a.LEVEL) AS LEVEL,
        1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN {3}
            -- ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID
"""

# 监控调阅发现问题信息
MEDIA_PROBLEM_NUMBER_INFO_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
-- MAX(a.LEVEL) AS LEVEL,
1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
        INNER join
    t_check_problem_and_risk as d on a.PK_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY = 3 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID in ({2})
            AND a.LEVEL in {3}
            group by a.pk_id
"""

# 监控调阅问题得分信息
MEDIA_PROBLME_SCORE_INFO_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
    max(b.CHECK_SCORE) AS SCORE
    -- max(a.LEVEL) as LEVEL
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            left join
        t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
    WHERE
        c.CHECK_WAY = 3
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND f.FK_RISK_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.LEVEL in {3}
    GROUP BY a.PK_ID
"""

# 作业问题质量分
ZUOYE_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
            inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 4 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.LEVEL IN {3}
            group by b.pk_id
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        STAFF_NUMBER, CHECK_ITEM_IDS, RISK_IDS,\
        WORK_LOAD_DATA, DIAOCHE_POSITION
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    DIAOCHE_POSITION = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    CALC_MONTH = END, START
    # 单位职工人数
    IDS = get_major_dpid(risk_type)
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(IDS))
    # 工作量
    WORK_LOAD_DATA = GLV.get_value('APPLY_WORKSHOP_COUNT').copy()
    WORK_LOAD_DATA = WORK_LOAD_DATA[['DEPARTMENT_ID','COUNT']]
    WORK_LOAD_DATA.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)


# ------------------------获取比值型相应基数------------------------ #


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, IDS)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 公共部分
    
    # 职工总人数
    STAFF_NUMBER_COUNT = CommonCalcDataType(*STAFF_NUMBER_INFO)
    STAFF_NUMBER_COUNT.version = 'v1'
    STAFF_NUMBER_COUNT.value = [STAFF_NUMBER]

    # 工作量
    WORK_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    WORK_LOAD_COUNT.value = [WORK_LOAD_DATA]
    WORK_LOAD_COUNT.func_version = 'single_df'
    WORK_LOAD_COUNT.description = '运用车间人数'

    # 干部数
    CADRE_COUNT = CommonCalcDataType(*CADRE_COUNT_INFO)
    CADRE_COUNT.value = [CADRE_COUNT_SQL.format(IDS)]

    # 作业人数
    WORKER_COUNT = CommonCalcDataType(*WORKER_COUNT_INFO)
    WORKER_COUNT.value = [WORKER_COUNT_SQL.format(IDS)]
    WORKER_COUNT.func_version = 'single_df'


    # 检查力度指数
    
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总数（关联项目)'
    CHECK_COUNT.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]
    
    # ABC类问题质量分
    PROBLEM_CHECK_SCORE_ABC = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    PROBLEM_CHECK_SCORE_ABC.version = 'v3'
    PROBLEM_CHECK_SCORE_ABC.description = 'A/B/C类问题质量分(关联风险)'
    PROBLEM_CHECK_SCORE_ABC.value = [
        ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'C'))]
    
    # AB类问题质量分
    PROBLEM_CHECK_SCORE_AB = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    PROBLEM_CHECK_SCORE_AB.version = 'v4'
    PROBLEM_CHECK_SCORE_AB.description = 'A/B类问题质量分(关联风险)'
    PROBLEM_CHECK_SCORE_AB.value = [
        ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B'))]


    # 夜查次数
    YECHA_COUNT = CommonCalcDataType(*YECHA_COUNT_INFO)
    YECHA_COUNT.value = [YECHA_CHECK_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]
    
    # 监控调阅发现a/b/c类问题数
    MEDIA_PROBLEM_NUMBER_ABC = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER_ABC.version = 'v3'
    MEDIA_PROBLEM_NUMBER_ABC.description = '监控调阅发现A/B/C类问题数（关联风险）'
    MEDIA_PROBLEM_NUMBER_ABC.value = [
        MEDIA_PROBLEM_NUMBER_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'C'))]

    # 监控调阅发现a/b类问题数
    MEDIA_PROBLEM_NUMBER_AB = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER_AB.version = 'v4'
    MEDIA_PROBLEM_NUMBER_AB.description = '监控调阅发现A/B类问题数（关联风险）'
    MEDIA_PROBLEM_NUMBER_AB.value = [
        MEDIA_PROBLEM_NUMBER_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B'))]

    
    # 监控调阅a/b/c问题质量分
    MEDIA_PROBLME_SCORE_ABC = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE_ABC.version = 'v3'
    MEDIA_PROBLME_SCORE_ABC.description = '监控调阅发现A/B/C类问题质量分（关联风险）'
    MEDIA_PROBLME_SCORE_ABC.value = [
        MEDIA_PROBLME_SCORE_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'C'))]

    # 监控调阅a/b问题质量分
    MEDIA_PROBLME_SCORE_AB = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)

    MEDIA_PROBLME_SCORE_AB.version = 'v4'
    MEDIA_PROBLME_SCORE_AB.description = '监控调阅发现A/B类问题质量分（关联风险）'
    MEDIA_PROBLME_SCORE_AB.value = [
        MEDIA_PROBLME_SCORE_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B'))]

    # 分析中心检查发现问题数
    ANALYSIS_CENTER_PROBLEM_COUNT = CommonCalcDataType(*ANALYSIS_CENTER_PROBLEM_COUNT_INFO)
    ANALYSIS_CENTER_PROBLEM_COUNT.value = [
        ANALYSIS_CENTER_PROBLEM_COUNT_SQL.format('{0}', '{1}', RISK_IDS)]

    # 分析中心检查发现质量分（关联风险）
    ANALYSIS_CENTER_PROBLEM_SCORE = CommonCalcDataType(*ANALYSIS_CENTER_PROBLEM_SCORE_INFO)
    ANALYSIS_CENTER_PROBLEM_SCORE.value = [
        ANALYSIS_CENTER_PROBLEM_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核力度指数
    # 考核问题数
    ASSESS_PROBLEM_COUNT = CommonCalcDataType(*ASSESS_PROBLEM_INFO)
    ASSESS_PROBLEM_COUNT.value = [
        KAOHE_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核总金额
    ASSESS_RESPONSIBLE = CommonCalcDataType(*ASSESS_RESPONSIBLE_INFO)
    ASSESS_RESPONSIBLE.version = 'v2'
    ASSESS_RESPONSIBLE.description = '月度考核总金额(关联风险)'
    ASSESS_RESPONSIBLE.value = [
        ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 达到返奖时限错误操纵问题问题个数
    AWARD_RETURN_MONEY_PROBLEM = CommonCalcDataType(
        *AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_PROBLEM.version = 'v4'
    AWARD_RETURN_MONEY_PROBLEM.description = '达到返奖时限错误操纵问题问题个数(关联风险)'
    AWARD_RETURN_MONEY_PROBLEM.value = [RETURN_MONTY_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]
    # 实际返奖错误操纵问题个数
    AWARD_RETURN_MONEY_ACTUAL = CommonCalcDataType(
        *REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_ACTUAL.version = 'v4'
    AWARD_RETURN_MONEY_ACTUAL.description = '实际返奖错误操纵问题个数(关联风险)'
    AWARD_RETURN_MONEY_ACTUAL.value = [ACTUAL_RETURN_MONTY_PROBLEM_SQl.format('{0}', '{1}', RISK_IDS)]

    # 问题暴露度
    # 总问题数（A,B,C）
    ZUOYE_PROBLEM_COUNT_ABC = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ZUOYE_PROBLEM_COUNT_ABC.version = 'v2'
    ZUOYE_PROBLEM_COUNT_ABC.description = '检查作业项问题数（关联风险）(ABC类)'
    ZUOYE_PROBLEM_COUNT_ABC.value = [
        ZUOYE_CHECK_PROBLEM_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'C'))]

    # 总问题数（A,B）
    ZUOYE_PROBLEM_COUNT_AB = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ZUOYE_PROBLEM_COUNT_AB.version = 'v3'
    ZUOYE_PROBLEM_COUNT_AB.description = '检查作业项问题数（关联风险）(AB类)'
    ZUOYE_PROBLEM_COUNT_AB.value = [
        ZUOYE_CHECK_PROBLEM_INFO_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B'))]

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, WORK_LOAD_COUNT),),

        # A/B/C类问题质量均分
        "1-6": (IndexDetails(PROBLEM_CHECK_SCORE_AB, WORK_LOAD_COUNT), 
                IndexDetails(PROBLEM_CHECK_SCORE_ABC, WORK_LOAD_COUNT)),


        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, WORK_LOAD_COUNT), 
                IndexDetails(MEDIA_PROBLEM_NUMBER_AB, WORK_LOAD_COUNT),
                IndexDetails(MEDIA_PROBLEM_NUMBER_ABC, WORK_LOAD_COUNT),
                IndexDetails(PROBLEM_CHECK_SCORE_AB, WORK_LOAD_COUNT),
                IndexDetails(PROBLEM_CHECK_SCORE_ABC, WORK_LOAD_COUNT),
                IndexDetails(ANALYSIS_CENTER_PROBLEM_COUNT, WORK_LOAD_COUNT),
                IndexDetails(ANALYSIS_CENTER_PROBLEM_SCORE, WORK_LOAD_COUNT),
                ),
        # 考核力度指数
        # 换算单位考核问题数
        '3-1': (IndexDetails(ASSESS_PROBLEM_COUNT, WORK_LOAD_COUNT),),

        # 换算单位考核金额
        '3-2': (IndexDetails(ASSESS_RESPONSIBLE, WORK_LOAD_COUNT),),

        # 返奖率
        "3-3": (IndexDetails(AWARD_RETURN_MONEY_ACTUAL, AWARD_RETURN_MONEY_PROBLEM),),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
                IndexDetails(ZUOYE_PROBLEM_COUNT_AB, WORK_LOAD_COUNT),
                IndexDetails(PROBLEM_CHECK_SCORE_AB, WORK_LOAD_COUNT),
                IndexDetails(ZUOYE_PROBLEM_COUNT_ABC, WORK_LOAD_COUNT),
                IndexDetails(PROBLEM_CHECK_SCORE_ABC, WORK_LOAD_COUNT),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_check_problem_assess_radio',
                  CHILD_INDEX_SQL_DICT['3-1'])
    GLV.set_value('stats_assess_money_per_person', CHILD_INDEX_SQL_DICT['3-2'])
    GLV.set_value('stats_award_return_ratio', CHILD_INDEX_SQL_DICT['3-3'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])
