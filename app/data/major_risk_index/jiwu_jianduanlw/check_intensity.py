# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_jianduanlw import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.jiwu_jianduanlw.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_INFO_SQL,
    MEDIA_PROBLME_SCORE_INFO_SQL, ZUOYE_CHECK_PROBLEM_INFO_SQL,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT, APPLY_WORKSHOP_COUNT,\
        RISK_IDS, ZUOYE_CHECK_PROBLEM_INFO_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    RISK_IDS = risk_ids

    # 基本工作量（运用车间人数)
    APPLY_WORKSHOP_COUNT = GLV.get_value('APPLY_WORKSHOP_COUNT')
    

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 现场夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)
    # 作业项检查问题数
    ZUOYE_CHECK_PROBLEM_INFO_DATA = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_INFO_SQL.format(*stats_month, risk_ids)), 
        DEPARTMENT_DATA)
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value("stats_check_per_person", (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        APPLY_WORKSHOP_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 人均质量分
def _stats_score_per_person(months_ago):
    """
    换算人均质量分（统计A/B/C类问题查处情况，A+B占比70%;A+B+C占比30%）：
    现场检查发现间断瞭望问题（风险类型为“机务-作业风险-间断瞭望”问题）质量分累计/基本工作量。
    Arguments:
        months_ago {[int]} -- [description]
    
    Returns:
        [type] -- [description]
    """
    df_abc = ZUOYE_CHECK_PROBLEM_INFO_DATA[ZUOYE_CHECK_PROBLEM_INFO_DATA['LEVEL'].isin(['A', 'B', 'C'])]
    df_ab =  ZUOYE_CHECK_PROBLEM_INFO_DATA[ZUOYE_CHECK_PROBLEM_INFO_DATA['LEVEL'].isin(['A', 'B'])]
    fraction_list = GLV.get_value("stats_risk_score_per_person", (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        df_ab,
        df_abc,
        APPLY_WORKSHOP_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=[],
        title=[
            'A/B类问题质量分累计({0})/工作量({1})',
            'A/B/C类问题质量分累计({0})/工作量({1})'
        ],
        fraction_list=fraction_list)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    analysis_center_problem_count_sql = ANALYSIS_CENTER_PROBLEM_COUNT_SQL.format(*stats_month, RISK_IDS)
    analysis_center_problem_score_sql = ANALYSIS_CENTER_PROBLEM_SCORE_SQL.format(*stats_month, RISK_IDS)
    media_problem_number_info_data = pd_query(MEDIA_PROBLEM_NUMBER_INFO_SQL.format(
        *stats_month, RISK_IDS))
    media_problem_number_data_list = [
        media_problem_number_info_data[media_problem_number_info_data['LEVEL'].isin(['A', 'B'])],
        media_problem_number_info_data[media_problem_number_info_data['LEVEL'].isin(['A', 'B', 'C'])]
    ]
    media_problem_score_info_data = pd_query(MEDIA_PROBLME_SCORE_INFO_SQL.format(
        *stats_month, RISK_IDS))
    media_problem_score_info_data_list = [
        media_problem_score_info_data[media_problem_score_info_data['LEVEL'].isin(['A', 'B'])],
        media_problem_score_info_data[media_problem_score_info_data['LEVEL'].isin(['A', 'B', 'C'])]
    ]
    title = ['监控调阅时长累计({0})/基本工作量({1})',
             '监控调阅发现问题数({0})/基本工作量({1})', '监控调阅发现问题质量分累计({0})/基本工作量({1})',
             '调阅班组数({0})/班组数({1})', '安全分析中心检查问题数({0})/基本工作量({1})',
             '安全分析中心检查质量分({0})/基本工作量({1})']
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None,
        fraction_list[3], fraction_list[4]
    )
    return check_intensity.new_stats_media_intensity(
        DEPARTMENT_DATA,
        APPLY_WORKSHOP_COUNT,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_data_list,
        media_problem_score_sql=media_problem_score_info_data_list,
        analysis_center_problem_count_sql=analysis_center_problem_count_sql,
        analysis_center_problem_score_sql=analysis_center_problem_score_sql,
        child_weight=[0.1, 0.4, 0.4, 0.0, 0.06, 0.04],
        title=title,
        fraction_list=fraction_list)


# 夜查率
def _stats_yecha_ratio(months_ago):
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        CHECK_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=lambda x: min(100, 100 * x),
        is_calc_score_base_major=False)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_score_per_person,
        _stats_yecha_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'f', 'g', 'j']]
    item_weight = [0.35, 0.25, 0.05, 0.35]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=1,
                                 child_index_list=[2, 6, 7, 10],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
