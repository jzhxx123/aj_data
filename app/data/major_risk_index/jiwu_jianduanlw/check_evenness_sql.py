#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql.py
Description:
Author:    
date:         2019/11/4
-------------------------------------------------
Change Activity:2019/11/4 2:14 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    DAILY_CHECK_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST,
    CHECK_BANZU_COUNT_SQL, DAILY_CHECK_BANZU_COUNT_SQL)

# 基础问题库中较大及以上风险项点问题数
RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.PK_ID
"""

RISK_ABOVE_PROBLEM_POINT_COUNT_SQL = """
select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base_risk as c on a.FK_PROBLEM_BASE_ID = c.FK_PROBLEM_BASE_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 2
             AND c.fk_risk_id IN ({2})
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
 """