#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_exposure_sql.py
Description:
Author:    
date:         2019/11/4
-------------------------------------------------
Change Activity:2019/11/4 2:29 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import (
    HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL, CHECK_PROBLEM_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, ANALYSIS_CHECK_PROBLEM_SQL)

# 查出的隐患问题项点(本单位基础问题库中有的)
CHECKED_HIDDEN_PROBLEM_POINT_SQL = """SELECT DISTINCT
    c.FK_DEPARTMENT_ID, b.PK_ID as FK_PROBLEM_BASE_ID
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
        INNER JOIN
    t_check_problem_and_risk AS d ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    b.IS_HIDDEN_KEY_PROBLEM = 1
    AND a.RISK_LEVEL <= 2
        AND b.`STATUS` = 3
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 隐患问题项点
HIDDEN_PROBLEM_POINT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.PK_ID as FK_PROBLEM_BASE_ID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_problem_base_risk AS c ON c.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.STATUS = 3
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND c.FK_RISK_ID in ({0});
"""