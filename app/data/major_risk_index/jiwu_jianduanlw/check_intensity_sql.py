#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql.py
Description:
Author:    
date:         2019/11/4
-------------------------------------------------
Change Activity:2019/11/4 2:18 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_INFO_SQL,
    MEDIA_PROBLME_SCORE_INFO_SQL, ZUOYE_CHECK_PROBLEM_INFO_SQL,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL)