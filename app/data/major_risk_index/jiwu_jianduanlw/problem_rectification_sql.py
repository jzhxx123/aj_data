#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019/11/4
-------------------------------------------------
Change Activity:2019/11/4 2:32 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL,
    CHECK_EVALUATE_SZ_NUMBER_SQL, PRECONTROL_RESPONSIBLE_UNIT_SQL)

 # 发警告预警通知书
WARINING_NOTIFICATION_COUNT_SQL = """
SELECT 
    FK_DUTY_DEPARTMENT_ID as FK_DEPARTMENT_ID, 
    RANK, 
    TYPE, 
    1 as COUNT
FROM
    t_warning_notification
WHERE
    DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND HIERARCHY between 1 and 2 
"""