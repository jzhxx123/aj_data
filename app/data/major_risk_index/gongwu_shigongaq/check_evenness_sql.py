#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 11:39 上午
-------------------------------------------------
"""
from app.data.major_risk_index.common.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    DAILY_CHECK_HOUR_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL, EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST, SPECIAL_BANZU_CHECKED_COUNT_SQL)