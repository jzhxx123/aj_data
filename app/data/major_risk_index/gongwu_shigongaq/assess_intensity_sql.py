#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   assess_intensity_sql
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 11:37 上午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import (
    KAOHE_PROBLEM_BASE_SQL, NORISK_ASSESS_RESPONSIBLE_SQL, LEVEL_AWARD_RETURN_SQL,
    AWARD_RETURN_PROBLEM_SQL,
    REAL_AWARD_RETURN_PROBLEM_SQL
)