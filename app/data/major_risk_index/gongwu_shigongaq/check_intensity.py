# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gongwu_shigongaq import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)

from app.data.major_risk_index.gongwu_shigongaq.check_intensity_sql import (
     XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL, ALL_PROBLEM_NUMBER_SQL,
     PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, CHECK_COUNT_SQL,
     RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, WORK_BANZU_INFO_SQL)
from app.data.major_risk_index.gongwu_shigongaq.common_sql import (
    WORK_TIME_RECORD_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_check_address_standard_data)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.gongwu_shigongaq.common import ( 
    calc_night_work_load)


def _calc_func(row, column, major_column, detail_type=None):
    """
    计算覆盖率
    Arguments:
        row {[type]} -- [description]
        column {[type]} -- [description]
        major_column {[type]} -- [description]
    
    Keyword Arguments:
        detail_type {[type]} -- [description] (default: {None})
    """
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        return 100
    elif _ratio >= 0:
        return 90 + _ratio * 50
    elif _ratio >= -0.15:
        return 90 + _ratio * 66
    elif _ratio >= -0.3:
        return 80 + (_ratio + 0.15) * 160
    else:
        return 71 + _ratio * 50


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT, \
        YEJIAN_WORK_LOAD, REAL_CHECK_BANZU_DATA, \
        REAL_CHECK_POINT_DATA, BANZU_POINT_DATA, CHECK_POINT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    global RISK_IDS, CHECK_ITEM_IDS
    CHECK_ITEM_IDS = check_item_ids
    RISK_IDS = diaoche[1]
    # 统计工作量【人时+派单数】
    WORK_LOAD = GLV.get_value('SHIGONG_WORK_LOAD')

    # 夜间工作量
    YEJIAN_WORK_LOAD = calc_night_work_load(
        pd_query(WORK_TIME_RECORD_SQL.format(*stats_month),
                 db_name='db_mid'),
        DEPARTMENT_DATA)

    # 检查总次数(量化人员及干部检查（施工、天窗修、点外修检查）次数)
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 一般及以上问题数(施工、天窗修、点外修问题数,低风险问题不计算)
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 工作班组信息
    work_banzu_info_data = pd_query(WORK_BANZU_INFO_SQL.format(major, check_item_ids))
    check_address_ratio_data = get_check_address_standard_data(
        work_banzu_info_data, DEPARTMENT_DATA, months_ago, check_item_ids, major
    )
    REAL_CHECK_BANZU_DATA, REAL_CHECK_POINT_DATA, BANZU_POINT_DATA, CHECK_POINT_DATA = check_address_ratio_data

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 现场检查频次
def _stats_check_per_person(months_ago):
    """
    量化人员及干部检查（施工、天窗修、点外修检查）次数/施工、天窗修、点外修工作量
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = " +
                        "量化人员及干部检查（施工、天窗修、点外修检查）次数({4})/ 施工、天窗修、点外修工作量({5})</p>", None]
    fraction = GLV.get_value("stats_check_per_person", (None, ))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        customizecontent=customizecontent,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    """
    施工、天窗修、点外修问题数/施工、天窗修、点外修工作量。低风险问题不计算
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = " + \
                        "问题数({4})/ 工作量({5})</p>", None]
    fraction = GLV.get_value("stats_check_problem_ratio", (None, ))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        customizecontent=customizecontent,
        fraction=fraction)


# 质量均分（问题质量分累计/施工、天窗修、点外修工作量）
def _stats_score_per_check_problem(months_ago):
    """
    问题平均质量分
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = " + \
                        "问题质量分累计({4})/ 工作量({5})</p>", None]
    fraction = GLV.get_value("stats_score_per_person", (None, ))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        customizecontent=customizecontent,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """
    （70%）较大和重大安全风险问题质量分累计/施工、天窗修、点外修工作量
    （30%）现场检查较大风险问题质量均分：较大和重大安全风险问题质量分累计（现场检查问题）/施工、天窗修、点外修工作量
    :param months_ago:
    :return:
    """
    fraction_list = GLV.get_value("stats_risk_score_per_person", (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        fraction_list=fraction_list)


# 夜查率
def _stats_yecha_ratio(months_ago):
    """
    夜查次数/施工、天窗修、点外修夜间工作量
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                        "夜查次数({4})/ 施工、天窗修、点外修夜间工作量({5})*100%</p>", None]
    fraction = GLV.get_value("stats_yecha_ratio", (None,))[0]
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        YEJIAN_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_func,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
        MEDIA_COST_TIME_SQL,
        MEDIA_PROBLEM_NUMBER_SQL,
        MEDIA_PROBLME_SCORE_SQL
    )
    statmonth = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*statmonth, RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*statmonth, RISK_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*statmonth, CHECK_ITEM_IDS),
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(*statmonth, RISK_IDS)
    ]
    fraction_list = GLV.get_value("stats_media_intensity", (None, None, None,None, None))
    fraction_list = (
        fraction_list[0],fraction_list[1],
        fraction_list[2], None,
        None
    )
    title = ['监控调阅时长累计({0})/施工、天窗修、点外修工作量({1})',
             '监控调阅发现施工、天窗修、点外修问题数({0})/施工、天窗修、点外修工作量({1})',
             '监控调阅发现问题质量分累计({0})/施工、天窗修、点外修工作量({1})',
             '调阅班组数({0})/有施工、天窗修、点外修班组数({1})']
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        title=title,
        calc_score_by_formula=_calc_func,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """
    检查地点数/地点总数（有施工、天窗修、点外修班组）。得分=比例*100
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = " + \
        "检查地点数({4})/ 地点总数（有施工、天窗修、点外修班组）({5})*100%</p>", None]
    fraction = GLV.get_value("stats_check_address_ratio", (None,))[0]
    return check_intensity.stats_check_address_ratio_include_check_point_excellent(
        REAL_CHECK_BANZU_DATA,
        REAL_CHECK_POINT_DATA,
        BANZU_POINT_DATA,
        CHECK_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_func=_calc_func,
        is_calc_score_base_major=True,
        customizecontent=customizecontent,
        fraction=fraction)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_check_problem, _stats_yecha_ratio,
        _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']
    ]
    item_weight = [0.25, 0.2, 0.25, 0.1, 0.15, 0.02, 0.03]
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=1,
                                 child_index_list=[2, 4, 5, 6, 10, 7, 9],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
