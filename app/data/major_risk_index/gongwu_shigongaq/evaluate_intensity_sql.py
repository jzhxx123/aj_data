#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# 干部主动评价记分总分数
EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT 
    FROM 
        t_check_evaluate_info AS a 
            INNER JOIN 
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD 
            INNER JOIN 
        t_check_evaluate_and_risk AS c 
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID 
    WHERE 
        a.CODE_ADDITION != 'JL-2-1' 
            AND a.`CODE` NOT LIKE 'SZ-%%' 
            AND a.EVALUATE_WAY > 0 
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4 
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d') 
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d') 
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d') 
                    <= DATE_FORMAT('{1}', '%%Y-%%m-%%d') 
            AND c.FK_RISK_ID IN ({2}) 
    GROUP BY b.FK_DEPARTMENT_ID 
"""

# (施工、天窗修、点外修作业人数）工作量里面的人数，是派工单中的上单作业人数
# gw_t_ps_dailyplan表中的tyoe字段表示作业类型
# 天窗修：{0:"请点“,(5,8):"一级维修",(6,9):"二级维修"}
# 施工：{"一级施工","二级施工",(3,4):"三级施工"}
# 点外修:{(1,2):"点外作业"}
SHIGONG_COUNT_SQL = """
SELECT
        a.DEPARTID AS FK_DEPARTMENT_ID, 
        SUM( a.ON_RAILWAY_NUMBER ) AS COUNT 
    FROM
        gw_t_ps_dailyplan AS a 
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND 
        a.TYPE IN ( 0, 1, 2, 3, 4, 5, 6, 8, 9 ) 
        AND 
        b.DISPATCH_ORDER_STATUS = 1 
    GROUP BY
    a.DEPARTID 
"""
