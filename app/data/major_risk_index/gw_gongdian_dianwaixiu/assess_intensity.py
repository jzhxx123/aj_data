#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gw_gongdian_dianwaixiu import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.index.common import (
    combine_and_format_basic_data_to_mongo,
    summizet_operation_set)
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.gongwu_dianwaixiu.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL, KAOHE_PROBLEM_BASE_SQL, 
    PERSON_ASSESS_RESPONSIBLE_SQL, PERSON_ASSESS_COUNT_SQL,
    NO_EXTERNAL_KAOHE_PROBLEM_BASE_SQL, NO_EXTERNAL_ALL_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.gongwu_dianwaixiu.check_intensity_sql import ALL_PROBLEM_NUMBER_SQL
from app.data.major_risk_index.gw_gongdian_dianwaixiu.common_sql import (
    WORK_LOAD_SQL, EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index,
    calc_child_index_type_divide, calc_extra_child_score_groupby_major_third)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.gw_gongdian_dianwaixiu.common import get_vitual_major_ids
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula_problem_assess_ratio(ratio):
    _score = 0
    if ratio > 0.8:
        _score = (ratio - 0.8)*100
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, STAFF_NUMBER, SHIGONG_WORK_LOAD, SHIGONG_LABORTIME, \
        ALL_PROBLEM_NUMBER, PERSON_ASSESS_RESPONSIBLE_DATA, PERSON_ASSESS_COUNT_DATA, \
        NO_EXTERNAL_ASSESS_PROBLEM_COUNT
    ids = get_vitual_major_ids("工电-1")
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]

    # 职工总数（正式职工）
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA)

    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))

    # 统计工作量【职工总人数】
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 施工工作量
    SHIGONG_WORK_LOAD = GLV.get_value('SHIGONG_WORK_LOAD')

    # 施工总人时
    SHIGONG_LABORTIME = df_merge_with_dpid(
        GLV.get_value('SHIGONG_LABORTIME'),
        DEPARTMENT_DATA)

    # 考核问题数（路外问题不纳入）
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_BASE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 考核问题数（不包含路外问题）
    NO_EXTERNAL_ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(NO_EXTERNAL_KAOHE_PROBLEM_BASE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 总问题数(非路外)
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(NO_EXTERNAL_ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)
    
    # 考核金额信息
    PERSON_ASSESS_RESPONSIBLE_DATA = df_merge_with_dpid(
        pd_query(PERSON_ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核人员信息
    PERSON_ASSESS_COUNT_DATA = df_merge_with_dpid(
        pd_query(PERSON_ASSESS_COUNT_SQL.format(year, month, check_item_ids)), 
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type, major_ratio_dict={}):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100 if detail_type != 3 else 100 * (1 - _ratio)
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100 if detail_type == 3 else 100 * (1 + _ratio)
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_assess_deduct_formula(row,column, major_column):
    _ratio = row[column] / row[major_column]
    _score = 0
    if _ratio < 0.1:
        _score = 10
    return _score


# 人均考核问题数
def _stats_check_problem_assess_radio(months_ago):
    """
    人均考核问题数=考核问题数/工作量（路外问题不纳入）
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = " + \
        "考核问题数({4})/工作量 ({5})</p>", None]
    fraction = GLV.get_value("stats_check_problem_assess_radio", (None, ))[0]
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        SHIGONG_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    """
    人均考核金额=月度考核总金额（月考核表中）/总人时；
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) =" + \
        "月度考核总金额({4})/ 总人时({5})</p>", None]
    fraction = GLV.get_value("stats_assess_money_per_person", (None, ))[0]
    return assess_intensity.stats_assess_money_per_person_type_one_major(
        ASSESS_RESPONSIBLE_MONEY,
        SHIGONG_LABORTIME,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 返奖率
def _stats_award_return_ratio(months_ago):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    if AWARD_RETURN_MONEY.empty:
        return None
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = AWARD_RETURN_MONEY[AWARD_RETURN_MONEY['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby(['DEPARTMENT_ID']).size()
        prob_number = idata.groupby(['DEPARTMENT_ID']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        rst_child_data = calc_extra_child_score_groupby_major_third(
            idata.copy(),
            _choose_dpid_data(3),
            'ratio',
            _calc_score_by_formula,
            weight=child_weight[i],
            detail_type=3, numerator='award', denominator='prob')
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 问题个数（{2}）'
            .format(title[i], row['award'], row['prob']),
            axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data,
        _choose_dpid_data(3),
        months_ago,
        3,
        3,
        3,
        risk_type=RISK_TYPE)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        column,
        3,
        2,
        3,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    return [df_rst]


# 考核率
def _stats_problem_assess_ratio(months_ago):
    customizecontent = ["<p>得分(扣分)：{0}</p><p>专业平均得分(扣分)：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>考核率({3}) = "+ \
        "考核问题数({4})/ 问题总数({5})(非路外)</p>", None]
    return calc_child_index_type_divide(
        NO_EXTERNAL_ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM_NUMBER,
        2,
        3,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula_problem_assess_ratio,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        is_calc_score_base_major=False,
        customizecontent=customizecontent)


def _stats_assess_deduct(months_ago):
    if PERSON_ASSESS_RESPONSIBLE_DATA.empty or PERSON_ASSESS_COUNT_DATA.empty:
        return None
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    conten_list = ['<br/>职工考核金额({0}) / 考核总金额（{1}）', '<br/>职工考核人数({0}) / 考核总人数{1}）']
    for idx, item in enumerate([PERSON_ASSESS_RESPONSIBLE_DATA, PERSON_ASSESS_COUNT_DATA]):
        total_assess = item.groupby(['TYPE3'])['COUNT'].sum()
        staff_assess = item[item['IS_OUT_SIDE_PERSON']==0]
        staff_assess = staff_assess.groupby(['TYPE3'])['COUNT'].sum()
        staff_assess = pd.concat(
            [staff_assess.to_frame(name='COUNT'),
            total_assess.to_frame(name='TOTAL_COUNT')],
            axis=1,
            sort=False
            )
        staff_assess.index.rename('DEPARTMENT_ID', inplace=True)
        staff_assess[f'middle_{idx}'] = staff_assess.apply(
            lambda row: conten_list[idx].format(row['COUNT'], row['TOTAL_COUNT']),
            axis=1)
        staff_assess['SCORE'] = staff_assess.apply(lambda row: 
        _calc_assess_deduct_formula(row, 'COUNT', 'TOTAL_COUNT'), axis=1)
        staff_assess.drop(columns=['COUNT', 'TOTAL_COUNT'], inplace=True, axis=1)
        item_c = staff_assess.drop(columns=['SCORE'])
        calc_basic_data.append(item_c)
        staff_assess.drop(columns=[f'middle_{idx}'], inplace=True, axis=1)
        rst_child_score.append(staff_assess)
    combine_and_format_basic_data_to_mongo(
        calc_basic_data,
        _choose_dpid_data(3),
        months_ago,
        3,
        3,
        5,
        risk_type=RISK_TYPE)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_e_1'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        column,
        3,
        2,
        3,
        5,
        months_ago,
        risk_type=RISK_TYPE)
    return [df_rst]


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_problem_assess_ratio, _stats_assess_deduct
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'd', 'e']]
    item_weight = [0.4, 0.6, -1, -1]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=3,
                                 child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
