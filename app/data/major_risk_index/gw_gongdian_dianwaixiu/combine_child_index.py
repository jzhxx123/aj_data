#! /usr/bin/env python3
# -*- encoding:utf-8 -*-

from app.utils.decorator import record_func_runtime
from app.data.major_risk_index.gw_gongdian_dianwaixiu import (
    assess_intensity, check_evenness, check_intensity,
    problem_exposure, problem_rectification, init_common_data, GLV,
    _calc_cardinal_number)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.gw_gongdian_dianwaixiu.common_sql import (CHEJIAN_DPID_SQL,
                                                         ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight, init_monthly_index_map
from app.data.major_risk_index.gw_gongdian_dianwaixiu.common import get_vitual_major_ids


@validate_exec_month
def execute(months_ago):
    #TODO 暂时却供电点外修
    risk_name = [158, 147, ]
    risk_type = '工电-3'
    # init_monthly_index_map(months_ago, risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    _calc_cardinal_number.get_cardinal_number(months_ago, risk_name, risk_type)
    for func in [
            check_intensity,
            assess_intensity,
            check_evenness,
            problem_exposure,
            problem_rectification,
    ]:  
        x_func = func.execute
        _func = record_func_runtime(x_func)
        _func(months_ago, risk_name, risk_type)
    child_index_weight = [0.35, 0.20, 0.10, 0.25, 0.10]
    update_major_maintype_weight(index_type=3, major=risk_type, 
                                child_index_list=[1, 3, 4, 5, 6],
                                child_index_weight=child_index_weight)
    ids = get_vitual_major_ids("工电-3")
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_list=[1, 3, 4, 5, 6],
        child_index_weight=child_index_weight,
        vitual_major_ids=ids)

    # 清除本模块的共享数据
    GLV.rm_all_values()


if __name__ == '__main__':
    pass
