# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app
from app.data.major_risk_index.gw_gongdian_dianwaixiu import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.gongwu_dianwaixiu.problem_exposure_sql import (
    CHECK_PROBLEM_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
    HIDDEN_KEY_PROBLEM_MONTH_SQL, HIDDEN_KEY_PROBLEM_SQL,
    OTHER_CHECK_PROBLEM_SQL, SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.util import (
    append_major_column_to_df, calc_extra_child_score_groupby_major_third,
    combine_child_index_func, df_merge_with_dpid, format_export_basic_data,
    summizet_child_index, summizet_operation_set,
    write_cardinal_number_basic_data,
    write_export_basic_data_to_mongo)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)

    # 统计工作量【人时+派单数】
    data = GLV.get_value('SHIGONG_WORK_LOAD')

    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >=0.15:
        return 100
    elif _ratio >=0:
        return 90 + _ratio * 50
    elif _ratio >= -0.25:
        return 90 + _ratio * 40
    elif _ratio >= -0.4:
        return 80 + (_ratio + 0.25) * 100
    else:
        return 97 + _ratio * 80


def _calc_value_per_person(series, weight, hierarchy, fraction=None):
    global STAFF_NUMBER
    data = pd.concat(
        [series.to_frame(name='prob'), STAFF_NUMBER], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if fraction:
        write_cardinal_number_basic_data(data, ZHANDUAN_DPID_DATA, fraction, fraction.risk_type,
                                         5, 1, fraction.months_ago, columns=['prob', 'PERSON_NUMBER'])
    return calc_extra_child_score_groupby_major_third(data,
                                                _choose_dpid_data(hierarchy),
                                                'ratio',
                                                _calc_score_by_formula, weight=weight,
                                                numerator='prob', denominator='PERSON_NUMBER',
                                                fraction=fraction)


def _calc_prob_number_per_person(df_data, weight, hierarchy, fraction=None):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, weight, hierarchy, fraction=fraction)


def _calc_prob_score_per_person(df_data, weight, hierarchy, fraction=None):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, weight, hierarchy, fraction=fraction)


def _calc_basic_prob_number_per_person(df_data, i, title):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby(['TYPE3']).size()
    global STAFF_NUMBER
    data = pd.concat(
        [prob_number.to_frame(name='prob'), STAFF_NUMBER], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, i, title):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    global STAFF_NUMBER
    data = pd.concat(
        [prob_score.to_frame(name='prob'), STAFF_NUMBER], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """
    总问题数（质量分）/工作量与本专业基数比较（50%），一般及以上数与本专业基数比较（50%）。
    :param months_ago:
    :return:
    """
    fraction_list = GLV.get_value("stats_total_problem_exposure", (None, None, None, None))
    fraction_list = (
        (fraction_list[0], fraction_list[1]),
        (fraction_list[2], fraction_list[3])
    )
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        CHECK_PROBLEM_SQL.format(*stats_month, RISK_IDS))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]

    weight_item = [0.5, 0.5]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})']
    # 导出中间过程
    for i, data in enumerate([base_data, risk_data]):
        for j, func in enumerate([
                _calc_basic_prob_number_per_person,
                _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(func(data.copy(), i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=RISK_TYPE)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate([base_data, risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(func(data.copy(), weight, hierarchy, fraction=fraction_list[i][j]))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=RISK_TYPE)
        rst_child_score.append(df_rst)
    return rst_child_score


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """
    连续4月无的扣2分/条，连续5个月无的扣4分/条，…扣月份-2分/条。得分=100-扣分
    :param months_ago:
    :return:
    """
    customizededuct = {
        4: 2,
        5: 4
    }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct)


# 班组问题暴露度
# TODO 当月没有工作量的班组不扣分，倒查分析时倒查月份没有工作量的终止倒查
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """
    从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题
    problem_risk_score(问题危险等级权重): {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    :param months_ago:
    :return:
    """
    return problem_exposure.stats_other_problem_exposure(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score={'1': 5, '2': 3, '3': 1, })


# TODO 关键问题加分、扣分

def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'd', 'e']]
    item_weight = [0.65, 0.25, 0.1, -1]
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=5,
                                 child_index_list=[1, 3, 4, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
