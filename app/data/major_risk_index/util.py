#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    desc: common functions for index module.
'''
from pandas import Series, DataFrame
import pandas as pd
from flask import current_app
import time
from app import mongo

from app.data.util import (get_coll_prefix, get_history_months,
                           write_bulk_mongo, pd_query)


def get_zhanduan_deparment(sql):
    data = pd_query(sql)
    # 将所属专业属性为客运的归纳为新的专业（客运）
    data['MAJOR'][data['BELONG_PROFESSION_NAME'] == '客运'] = '客运'
    data.drop(['BELONG_PROFESSION_NAME'], inplace=True, axis=1)
    return data


def df_merge_with_dpid(data, dpid_data, how='inner'):
    """将指数计算需要的数据跟部门单位连接后获取单位dpid

    Arguments:
        data {dataFrame} -- 指数数据
        dpid_data {dataFrame} -- 部门单位(参与计算的)

    Keyword Arguments:
        how {str} -- 连接方式 (default: {'inner'})

    Returns:
        dataFrame --各单位的数据
    """
    data = pd.merge(
        data,
        dpid_data,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data


def get_major_avg_number(row, major_ratio):
    avg_number = 0
    for idx, item in enumerate(major_ratio):
        if row.ratio > item[0]:
            avg_number = item[0]
            break
        elif idx == len(major_ratio) - 1:
            avg_number = item[0]
    return avg_number


def get_major_avg_number_by_ratio_df(row):
    """获取对应的专业基数"""
    avg_number = 0
    ratio = row['ratio']
    col_list = ['ratio_1', 'ratio_2', 'ratio_3']
    for col in col_list:
        if ratio > row[col]:
            avg_number = row[col]
            break
        elif col == col_list[-1]:
            avg_number = row[col]
    return avg_number


def calc_child_index_type_divide(df_numerator,
                                 df_denominator,
                                 index_type,
                                 main_type,
                                 detail_type,
                                 months_ago,
                                 stats_field,
                                 column,
                                 calc_func,
                                 dpid_func,
                                 is_calc_score_base_major=True,
                                 hierarchy_list=[3],
                                 risk_type=None,
                                 customizecontent=None):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        df_calc['ratio'] = df_calc.apply(lambda row:
                                         row['numerator'] / row['denominator'] if row['denominator'] > 0 else 0, axis=1)
        column = f'{column}_{hierarchy}'
        if is_calc_score_base_major:
            # 需要跟基数（专业均值）作比较得出分数
            df_calc = calc_score_groupby_major(
                df_calc, dpid_func(hierarchy), hierarchy, main_type,
                detail_type, 'numerator', 'denominator', 'ratio', column,
                calc_func, months_ago, risk_type, customizecontent=customizecontent)
        else:
            df_calc[column] = df_calc['ratio'].apply(lambda x: calc_func(x))
            # 导出中间过程
            _export_basic_data_ratio_type(
                df_calc, dpid_func(hierarchy), main_type, detail_type,
                hierarchy, months_ago, 'numerator', 'denominator', 'ratio',
                column, risk_type, customizecontent=customizecontent)
        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def calc_child_index_type_divide_major(df_numerator,
                                       df_denominator,
                                       index_type,
                                       main_type,
                                       detail_type,
                                       months_ago,
                                       stats_field,
                                       column,
                                       calc_func,
                                       dpid_func,
                                       is_calc_score_base_major=True,
                                       hierarchy_list=[3],
                                       risk_type=None,
                                       customizecontent=None,
                                       zhanduan_filter_list=[],
                                       fraction=None,
                                       major_ratio=None,
                                       last_index_ratio=None,
                                       ratio_df=None
                                       ):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数
        customizecontent -- 自定义文本
        zhanduan_filter_list -- 不参与基数计算的站段id列表
        fraction -- 专业基数最优部门版
        major_ratio_dict -- 手动设置专业基数时的基数字典
        ratio_df -- dataframe 专业基数数据
        last_index_ratio -- 上月ratio及专业基数
    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        # df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        # df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        df_calc['ratio'] = df_calc.apply(
            lambda row: (row['numerator'] / row['denominator']) if row['denominator'] > 0 else 0, axis=1
        )
        # 一个将df_calc的分子分母存储到基数基础数据库中
        if fraction:
            write_cardinal_number_basic_data(df_calc,
                                             dpid_func(hierarchy), fraction, risk_type, main_type, detail_type,
                                             months_ago)
        column = f'{column}_{hierarchy}'
        if is_calc_score_base_major:
            # 需要跟基数（专业均值）作比较得出分数
            df_calc = calc_score_groupby_major_third(
                df_calc, dpid_func(hierarchy), hierarchy, main_type,
                detail_type, 'numerator', 'denominator', 'ratio', column,
                calc_func, months_ago, risk_type, customizecontent=customizecontent,
                zhanduan_filter_list=zhanduan_filter_list,
                fraction=fraction, major_ratio=major_ratio,
                last_index_ratio=last_index_ratio, ratio_df=ratio_df,
            )
        else:
            df_calc[column] = df_calc['ratio'].apply(lambda x: calc_func(x))
            # 导出中间过程
            _export_basic_data_ratio_type_major(
                df_calc, dpid_func(hierarchy), main_type, detail_type,
                hierarchy, months_ago, 'numerator', 'denominator', 'ratio',
                column, risk_type, customizecontent=customizecontent,
                zhanduan_filter_list=zhanduan_filter_list)
        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


# 部分专业基数
def calc_child_index_type_divide_two(df_numerator,
                                     df_denominator,
                                     index_type,
                                     main_type,
                                     detail_type,
                                     months_ago,
                                     stats_field,
                                     column,
                                     calc_func,
                                     dpid_func,
                                     is_calc_score_base_major=True,
                                     hierarchy_list=[3],
                                     zhanduan_filter_list=[],
                                     risk_type=None,
                                     customizecontent=None):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        df_numerator {dataFrame} -- 各单位的数据
        df_denominator {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        is_calc_score_base_major {boolean}
            -- 得到的比值是否需要基于专业再次计算 (default: {True})
        hierarchy_list {list}
            -- 站段、车间、班组 (default: {[3, 4]})
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
        zhanduan_filter_list {list}
             -- 不参与专业基数计算的站段列表

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    if df_numerator.empty is True or df_denominator.empty is True:
        return []
    rst_index_score = []
    for hierarchy in hierarchy_list:
        ser_numerator = df_numerator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        ser_denominator = df_denominator.groupby(
            [f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = pd.concat(
            [
                ser_numerator.to_frame(name='numerator'),
                ser_denominator.to_frame(name='denominator')
            ],
            axis=1,
            sort=False)
        df_calc.dropna(subset=['denominator'], inplace=True)
        df_calc.fillna(0, inplace=True)
        if df_calc.empty is True:
            continue
        # df_calc['ratio'] = df_calc['numerator'] / df_calc['denominator']
        df_calc['ratio'] = df_calc.apply(lambda row: row['numerator'] / row['denominator']
        if row['denominator'] > 0 else 0, axis=1)
        column = f'{column}_{hierarchy}'
        if is_calc_score_base_major:
            # 需要跟基数（专业均值）作比较得出分数
            df_calc = calc_score_groupby_major_two(
                df_calc, dpid_func(hierarchy), hierarchy, main_type,
                detail_type, 'numerator', 'denominator', 'ratio', column,
                calc_func, months_ago, risk_type, zhanduan_filter_list, customizecontent=customizecontent)
        else:
            df_calc[column] = df_calc['ratio'].apply(lambda x: calc_func(x))
            # 导出中间过程
            _export_basic_data_ratio_type(
                df_calc, dpid_func(hierarchy), main_type, detail_type,
                hierarchy, months_ago, 'numerator', 'denominator', 'ratio',
                column, risk_type, customizecontent=customizecontent)
        # 将最终的各单位的得分进行排名入库等一系列操作
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def calc_child_index_type_sum(data,
                              index_type,
                              main_type,
                              detail_type,
                              months_ago,
                              stats_field,
                              column,
                              calc_func,
                              dpid_func,
                              hierarchy_list=[3],
                              risk_type=None,
                              NA_value=0):
    """通过按不同层级单位分组，计算每个单位的得分

    Arguments:
        data {dataFrame} -- 各单位的数据
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        stats_field {str} -- 用来求值的字段/列名
        column {str} -- 返回数据结构里的分值列名
        calc_func {func} -- 对最终分值进行处理的函数
        dpid_func {func} -- 对应层次单位的函数

    Keyword Arguments:
        hierarchy_list {list} 
            -- 站段、车间、班组 (default: {[3]})
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
        NA_value {bool} 
            -- 没有统计到的单位,如果是计算扣分型数，则为0，(default: {Flase})

    Returns:
        dataFrame -- 子指数最终计算分值
    """
    rst_index_score = []
    for hierarchy in hierarchy_list:
        xdata = data.dropna(subset=[f'TYPE{hierarchy}'], axis=0)
        xdata = xdata.groupby([f'TYPE{hierarchy}'])[stats_field].sum()
        df_calc = xdata.to_frame(name='SCORE')
        # 给没有统计到的单位添加指定缺省值
        if NA_value:
            dpid_data = dpid_func(hierarchy)
            if risk_type is not None:
                dpid_data = dpid_data[dpid_data['MAJOR'] == risk_type.split(
                    '-')[0]]
            df_calc = pd.merge(
                df_calc,
                dpid_data,
                how='outer',
                left_index=True,
                right_on='DEPARTMENT_ID')
            df_calc.fillna(0, inplace=True)
            df_calc = pd.DataFrame(
                index=df_calc['DEPARTMENT_ID'],
                data=df_calc.loc[:, 'SCORE'].values,
                columns=['SCORE'])
        if df_calc.empty is True:
            continue
        column = f'{column}_{hierarchy}'
        df_calc[column] = df_calc['SCORE'].apply(lambda x: calc_func(x))
        summizet_operation_set(df_calc, dpid_func(hierarchy), column,
                               hierarchy, index_type, main_type, detail_type,
                               months_ago, risk_type)
        rst_index_score.append(df_calc[[column]])
    return rst_index_score


def append_major_column_to_df(dpid_data, data):
    """增加各个站段/车间/班组增加专业列

    Arguments:
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        data {type} --

    Returns:
        DataFrame --
    """
    data = pd.merge(
        dpid_data, data, how='left', left_on='DEPARTMENT_ID', right_index=True)
    return data


def init_calc_data_pro(raw_data, dpid, single=True, condition={'COUNT': 0}):
    """[初始化计算df，避免渲染无数据]
    Arguments:
        raw_data {[df]} -- [需要初始化的原始数据]
        dpid {[df]} -- [初始化的工具df]
    
    Keyword Arguments:
        single {bool} -- [是否只merge单个列] (default: {True})
    """
    tmp_data = dpid
    if single:
        tmp_data = pd.DataFrame(dpid, columns=['DEPARTMENT_ID'])
    data = pd.merge(tmp_data, raw_data,
                    how='left', left_on='DEPARTMENT_ID',
                    right_on='FK_DEPARTMENT_ID',
                    left_index=True, right_index=False)
    data.fillna(condition, inplace=True)
    return data


def _get_risk_type(risk_type):
    return int(risk_type.split('-')[1])


def calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, risk_type):
    """按照专业分组排名，将排名、分数数据写入数据库

    Arguments:
        data {[type]} -- 带有分值的按单位分组选好的
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
        risk_type {str} -- 表示某专业的第几个重点风险分析指数，ex: 车务-1
    """
    data.fillna(0, inplace=True)
    data['group_sort'] = data[column].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    score_rst = []
    mon = get_history_months(months_ago)[0]
    major_risk_type = 0
    if risk_type is not None:
        major_risk_type = _get_risk_type(risk_type)
    for index, row in data.iterrows():
        score_rst.append({
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'TYPE': major_risk_type,
            'SCORE': round(row[column], 2),
            'RANK': int(row['group_sort']),
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME']
        })
    prefix = get_coll_prefix(months_ago)
    coll_map = {
        1: 'health',
        2: 'major',
    }
    coll_name = f'{prefix}detail_{coll_map[index_type]}_index'
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    if risk_type is not None:
        r_condition.update({
            'MAJOR': risk_type.split('-')[0],
            'TYPE': major_risk_type
        })
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, score_rst)


def summizet_operation_set(data,
                           dpid_data,
                           column,
                           hierarchy,
                           index_type,
                           main_type,
                           detail_type,
                           months_ago,
                           risk_type=None):
    """拿到业务逻辑数据计算后的一套公共操作（排名、子指数汇总、入库）

    Arguments:
        data {DataFrame} -- index为部门ID
        dpid_data {DataFrame} -- 站段/车间/班组部门信息
        column {str} -- 分值列名
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        months_ago {int} -- 第前{-N}个月
    Keyword Arguments:
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨） (default: {None})
    """
    data = append_major_column_to_df(dpid_data, data)
    calc_and_rank_index(data, column, hierarchy, index_type, main_type,
                        detail_type, months_ago, risk_type)


def merge_all_child_item(row, hierarchy, item_name, item_weight):
    """将指数的各个子项按照权重合并成一个总数

    Arguments:
        row {[type]} --
        hierarchy {int} -- 单位层级
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Returns:
        float -- 指数分数
    """
    score = []
    for item in item_name:
        item = f'{item}_{hierarchy}'
        if item in row:
            score.append(round(row[item], 2))
        else:
            score.append(0)
    rst_score = sum([i[0] * i[1] for i in zip(item_weight, score)])
    rst_score = 0 if rst_score < 0 else round(rst_score, 2)
    return rst_score


def summizet_child_index(child_score,
                         dpid_func,
                         index_type,
                         main_type,
                         months_ago,
                         item_name,
                         item_weight,
                         hierarchy_list=[3],
                         risk_type=None):
    """将指数的各个子项按照权重合并成一个总数， 计算排名、入库

    Arguments:
        child_score {list[DataFrame]} -- 各个子项的集合
        dpid_func {func} -- 对应层次单位的函数
        index_type {int} -- 指数类型
        main_type {int} -- 指数大类[1~6]
        months_ago {int} -- 第前{-N}个月
        item_name {list[str]} -- 各个子项名称
        item_weight {list[float]} -- 各个子项的权重

    Keyword Arguments:
        hierarchy_list {list} -- 站段、车间、班组 (default: {[3]})
    """
    if len(child_score) == 0:
        return
    for hierarchy in hierarchy_list:
        h_child_score = [
            x for x in child_score if x.columns.values[0][-1] == str(hierarchy)
        ]
        if h_child_score == []:
            continue
        data = pd.concat(h_child_score, axis=1, sort=False)
        data.fillna(0, inplace=True)
        data['SCORE'] = data.apply(
            lambda row: merge_all_child_item(row, hierarchy, item_name, item_weight),
            axis=1)

        data = append_major_column_to_df(dpid_func(hierarchy), data)
        calc_and_rank_index(data, 'SCORE', hierarchy, index_type, main_type, 0,
                            months_ago, risk_type)


def combine_and_format_basic_data_to_mongo(data, major_data, months_ago,
                                           hierarchy, main_type, detail_type):
    """将中间过程各个子指数的组成部分数据拼接好

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    calc_df_data = pd.concat(data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, main_type, detail_type, hierarchy, months_ago)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago,
                                     hierarchy, main_type, detail_type)


def write_export_basic_data_to_mongo(data,
                                     months_ago,
                                     hierarchy,
                                     main_type,
                                     detail_type,
                                     risk_type=None):
    """将中间过程写入mongo，写之前需要删除之前的对应月份历史数据，避免冗余

    Arguments:
        data {pd.DataFrame} -- 待写入数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
    """
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}health_index_basic_data'
    # 删除历史数据
    r_condition = {
        'MON': mon,
        'HIERARCHY': hierarchy,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type
    }
    if risk_type is not None:
        coll_name = f'{prefix}major_index_basic_data'
        r_condition.update({
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1])
        })
    mongo.db[coll_name].remove(r_condition)
    write_bulk_mongo(coll_name, data)


def format_export_basic_data(data,
                             main_type,
                             detail_type,
                             hierarchy,
                             months_ago,
                             risk_type=None):
    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in data.iterrows():
        data_rst.append({
            'TYPE': 2,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'HIERARCHY': hierarchy,
            'MON': mon,
            'MAJOR': row['MAJOR'],
            'INDEX_TYPE': index_type,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'CONTENT': row['CONTENT'],
        })
    return data_rst


def export_basic_data_dicttype(data,
                               dpid_data,
                               main_type,
                               detail_type,
                               hierarchy,
                               months_ago,
                               func_html_desc,
                               risk_type=None):
    data = data.groupby(['TYPE3'])['COUNT'].sum()
    data = append_major_column_to_df(dpid_data, data.to_frame(name='CONTENT'))
    data.fillna(0, inplace=True)
    data['CONTENT'] = data['CONTENT'].apply(lambda x: func_html_desc(x))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def _combine_second_filed_to_str(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row.loc[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


# 整改成效
def _combine_second_filed_to_str_two(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


def export_basic_data_tow_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        first_title,
                                        second_title,
                                        risk_type=None):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()
    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str(row, first_title, second_title),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


# # 整改成效
def export_basic_data_tow_field_monthly_two(data,
                                            dpid_data,
                                            major_data,
                                            main_type,
                                            detail_type,
                                            hierarchy,
                                            months_ago,
                                            first_title,
                                            second_title,
                                            risk_type=None,
                                            columns_list=None):
    if columns_list is None:
        columns_list = [(1, 1), (1, 2), (1, 3), (2, 1),
                        (2, 2), (2, 3), (3, 1), (3, 2), (3, 3)]
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()

    # 按顺序填充缺失的责任类型
    data_columns = data.columns.values.tolist()
    for idx, col in enumerate(columns_list):
        if col not in data_columns:
            data[col] = 0
            col_data = data.pop(col)
            data.insert(idx, col, col_data)

    # 填充站段数据
    data = pd.merge(
        data,
        major_data["DEPARTMENT_ID"].to_frame(),
        how="right",
        left_index=True,
        right_on="DEPARTMENT_ID")
    data.set_index('DEPARTMENT_ID', inplace=True)

    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str_two(row, first_title, second_title),
        axis=1)

    data = data['CONTENT'].to_frame("middle_1")
    #
    # data = append_major_column_to_df(
    #     major_data,
    #     pd.DataFrame(
    #         index=data.index,
    #         data=data.loc[:, 'CONTENT'].values,
    #         columns=['CONTENT']))
    # data_rst = format_export_basic_data(data, main_type, detail_type,
    #                                     hierarchy, months_ago, risk_type)
    # write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
    #                                  main_type, detail_type, risk_type)
    return data

    # data = pd.DataFrame(
    #     data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    # data = data.rename(columns={"MAIN_TYPE": "FIRST", "RESPONSIBILITY_IDENTIFIED": 'SECOND'})
    # data = df_merge_with_dpid(data, dpid_data)
    # data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    # data = data.unstack().unstack()
    # data.fillna(0, inplace=True)
    # data['CONTENT'] = data.apply(
    #     lambda row: _combine_second_filed_to_str(row, first_title, second_title),
    #     axis=1)
    # data = append_major_column_to_df(
    #     major_data,
    #     pd.DataFrame(
    #         index=data.index,
    #         data=data.loc[:, 'CONTENT'].values,
    #         columns=['CONTENT']))
    # data.fillna('无', inplace=True)
    # data_rst = format_export_basic_data(data, main_type, detail_type,
    #                                     hierarchy, months_ago, risk_type)
    # write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
    #                                  main_type, detail_type, risk_type)


def export_basic_data_one_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        func_html_desc,
                                        title=None,
                                        risk_type=None):
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    # 因供电劳安事故隐患连续12月时,groupby顺序会出错,将func_html_desc调整至content
    # data['MONTH'] = data['MONTH'].apply(lambda x: func_html_desc(x))
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    if title is not None:
        title += '<br/>'
    else:
        title = ''
    data['CONTENT'] = data.apply(
        lambda row: title + '<br/>'
            .join(f'{func_html_desc(col)}: {int(row[col])}个' for col in columns),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def export_basic_data_one_field_monthly_excellent(data,
                                                  dpid_data,
                                                  major_data,
                                                  main_type,
                                                  detail_type,
                                                  hierarchy,
                                                  months_ago,
                                                  func_html_desc,
                                                  title='前{0}月问题个数: {1}个',
                                                  risk_type=None):
    # 还可以拓展，month表示几个月前，count表示对应的问题数
    # 针对不同月份同一个部门，这些数据要拼凑一个content
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH', 'COUNT'])
    # 月份种类
    months_set = set(data['MONTH'].values.tolist())

    # 部门种类
    dpid_set = set(data['FK_DEPARTMENT_ID'].values.tolist())
    rst = []
    for dpid in dpid_set:
        tmp_data = data[data['FK_DEPARTMENT_ID'] == dpid]
        content = ''
        for month in months_set:
            # 这里必须保证查出对应部门月份只有一条记录，例如成都工务段前1月的问题数只有一个记录，0默认取第一个
            # 所以务必数据传进来前已经聚合好
            content += title.format(month, tmp_data[tmp_data['MONTH'] == month]['COUNT'].values[0])
        rst.append([dpid, content])
    rst = pd.DataFrame(data=rst, columns=['FK_DEPARTMENT_ID', 'CONTENT'])
    rst = pd.merge(
        major_data,
        rst,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_rst = format_export_basic_data(rst, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def _export_basic_data_ratio_type(
        df_data, dpid_data, main_type, detail_type, hierarchy, months_ago,
        col_numerator, col_denominator, col_quotient, col_score, risk_type, customizecontent=None):
    df_data = add_avg_number_by_major(df_data, dpid_data, col_quotient)
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
        ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        tmp_dict = dict()
        if customizecontent is None:
            tmp_dict['TYPE'] = 1
        elif isinstance(customizecontent, list):
            tmp_dict['TYPE'] = 3
            tmp_dict['CONTENT'] = customizecontent[0]
        else:
            tmp_dict['TYPE'] = 2
            tmp_dict['CONTENT'] = customizecontent.format(round(row[col_score], 2), int(row['group_sort']),
                                                          round(row['AVG_NUMBER'], 4), round(row[col_quotient], 4),
                                                          round(row[col_numerator], 2), round(row[col_denominator], 2),
                                                          round(row['AVG_SCORE'], 2))
        tmp_dict['DEPARTMENT_ID'] = row['DEPARTMENT_ID']
        tmp_dict['MAJOR'] = row['MAJOR']
        tmp_dict['MON'] = mon
        tmp_dict['HIERARCHY'] = hierarchy
        tmp_dict['INDEX_TYPE'] = index_type
        tmp_dict['MAIN_TYPE'] = main_type
        tmp_dict['DETAIL_TYPE'] = detail_type
        tmp_dict['NUMERATOR'] = round(row[col_numerator], 2)
        tmp_dict['DENOMINATOR'] = round(row[col_denominator], 2)
        tmp_dict['QUOTIENT'] = round(row[col_quotient], 4)
        tmp_dict['AVG_QUOTIENT'] = round(row['AVG_NUMBER'], 4)
        tmp_dict['AVG_SCORE'] = round(row['AVG_SCORE'], 2)
        tmp_dict['SCORE'] = round(row[col_score], 2)
        tmp_dict['RANK'] = int(row['group_sort'])
        data_rst.append(tmp_dict)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def _export_basic_data_ratio_type_major(
        df_data, dpid_data, main_type, detail_type, hierarchy, months_ago,
        col_numerator, col_denominator, col_quotient, col_score, risk_type, customizecontent=None,
        zhanduan_filter_list=[]):
    df_data = add_avg_number_by_major_third(df_data, dpid_data, col_quotient, zhanduan_filter_list=zhanduan_filter_list)
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_quotient].groupby(
        df_data['MAJOR']).rank(
        ascending=0, method='first')
    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        tmp_dict = dict()
        if customizecontent is None:
            tmp_dict['TYPE'] = 1
        elif isinstance(customizecontent, list):
            tmp_dict['TYPE'] = 3
            tmp_dict['CONTENT'] = customizecontent[0]
        else:
            tmp_dict['TYPE'] = 2
            tmp_dict['CONTENT'] = customizecontent.format(round(row[col_score], 2), int(row['group_sort']),
                                                          round(row['AVG_NUMBER'], 4), round(row[col_quotient], 4),
                                                          round(row[col_numerator], 2), round(row[col_denominator], 2),
                                                          round(row['AVG_SCORE'], 2))
        tmp_dict['DEPARTMENT_ID'] = row['DEPARTMENT_ID']
        tmp_dict['MAJOR'] = row['MAJOR']
        tmp_dict['MON'] = mon
        tmp_dict['HIERARCHY'] = hierarchy
        tmp_dict['INDEX_TYPE'] = index_type
        tmp_dict['MAIN_TYPE'] = main_type
        tmp_dict['DETAIL_TYPE'] = detail_type
        tmp_dict['NUMERATOR'] = round(row[col_numerator], 2)
        tmp_dict['DENOMINATOR'] = round(row[col_denominator], 2)
        tmp_dict['QUOTIENT'] = round(row[col_quotient], 4)
        tmp_dict['AVG_QUOTIENT'] = round(row['AVG_NUMBER'], 4)
        tmp_dict['AVG_SCORE'] = round(row['AVG_SCORE'], 2)
        tmp_dict['SCORE'] = round(row[col_score], 2)
        tmp_dict['RANK'] = int(row['group_sort'])
        data_rst.append(tmp_dict)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def export_basic_data(df_data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, col_quotient, col_avg,
                      col_score, risk_type, customizecontent=None):
    """将计算中间过程导出，包含分子、分母、专业均数、得分、排名

    Arguments:
        df_data {DataFrame} -- 数据
        main_type {int} -- 指数大类[1~6]
        detail_type {int} -- 子指数小类[1~N]
        hierarchy {int} -- 单位层级[3、4、5分别代表站段、车间、班组]
        months_ago {int} -- 第前{-N}个月
        col_numerator {float} -- 计算过程分子
        col_denominator {float} -- 计算过程分母
        col_quotient {float} -- 计算过程商
        col_avg {float} -- 专业均数
        col_score {float} -- 各单位得分
    """
    # 增加平均分一列
    df_data = add_avg_score_by_major(df_data, col_score)
    df_data.fillna(0, inplace=True)
    df_data['group_sort'] = df_data[col_score].groupby(df_data['MAJOR']).rank(
        ascending=0, method='first')

    data_rst = []
    mon = get_history_months(months_ago)[0]
    index_type = 0
    if risk_type is not None:
        index_type = int(risk_type.split('-')[1])
    for idx, row in df_data.iterrows():
        tmp_dict = dict()
        if customizecontent is None:
            tmp_dict['TYPE'] = 1
        elif isinstance(customizecontent, list):
            tmp_dict['TYPE'] = 3
            tmp_dict['CONTENT'] = customizecontent[0]
        else:
            tmp_dict['TYPE'] = 2
            tmp_dict['CONTENT'] = customizecontent.format(round(row[col_score], 2), int(row['group_sort']),
                                                          round(row['AVG_NUMBER'], 4), round(row[col_quotient], 4),
                                                          round(row[col_numerator], 2), round(row[col_denominator], 2),
                                                          round(row['AVG_SCORE'], 2))
        tmp_dict['DEPARTMENT_ID'] = row['DEPARTMENT_ID']
        tmp_dict['MAJOR'] = row['MAJOR']
        tmp_dict['MON'] = mon
        tmp_dict['HIERARCHY'] = hierarchy
        tmp_dict['INDEX_TYPE'] = index_type
        tmp_dict['MAIN_TYPE'] = main_type
        tmp_dict['DETAIL_TYPE'] = detail_type
        tmp_dict['NUMERATOR'] = round(row[col_numerator], 2)
        tmp_dict['DENOMINATOR'] = round(row[col_denominator], 2)
        tmp_dict['QUOTIENT'] = round(row[col_quotient], 4)
        tmp_dict['AVG_QUOTIENT'] = round(row[col_avg], 2)
        tmp_dict['AVG_SCORE'] = round(row['AVG_SCORE'], 2)
        tmp_dict['SCORE'] = round(row[col_score], 2)
        tmp_dict['RANK'] = int(row['group_sort'])
        data_rst.append(tmp_dict)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


def add_avg_score_by_major(df, column, dpid_data=None):
    """给dataframe增加专业平均分-
    """
    if dpid_data is not None:
        df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_SCORE'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def add_avg_number_by_major(df, dpid_data, column):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    df = append_major_column_to_df(dpid_data, df)
    major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


# 部分专业基数
def add_avg_number_by_major_two(df, dpid_data, column, zhanduan_filter_list):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        zhanduan_filter_list {list}
             -- 不参与专业基数计算的站段列表

    Returns:
        [DataFrame] --
    """
    df = append_major_column_to_df(dpid_data, df)
    if zhanduan_filter_list:
        major_avg_score = df[~df.DEPARTMENT_ID.isin(zhanduan_filter_list)].groupby(['MAJOR'])[column].mean()
    else:
        major_avg_score = df.groupby(['MAJOR'])[column].mean()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def add_avg_number_by_major_third(
        df, dpid_data, column, zhanduan_filter_list=None,
        numerator='numerator', denominator='denominator',
        fraction=None, major_ratio=None, ratio_df=None):
    """给dataframe增加专业均值(比值)

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名

    Returns:
        [DataFrame] --
    """
    from app.data.major_risk_index.common.cardinal_number_common import (
        calc_fitting_cardinal_number_by_formula)
    df = append_major_column_to_df(dpid_data, df)
    # fraction = None
    if fraction:
        history_three_months = [fraction.months_ago - i for i in range(1, 4)]
        # 获取三个月前对应的基数，选择最大值
        risk_type_list = fraction.risk_type.split('-')
        cardinal_number = []
        for mon_ago in history_three_months:
            prefix = get_coll_prefix(mon_ago)
            mon = get_history_months(mon_ago)[0]
            cardinal_number.extend(list(mongo.db[f'{prefix}major_index_cardinal_number_basic_data'].find(
                {"MON": mon, "DENOMINATOR_NAME": fraction.denominator.name,
                 "DENOMINATOR_VERSION": fraction.denominator.version,
                 "NUMERATOR_VERSION": fraction.numerator.version,
                 "NUMERATOR_NAME": fraction.numerator.name,
                 "CARDINAL_NUMBER": {'$exists': 'true'},
                 "MAJOR": risk_type_list[0], "INDEX_TYPE": int(risk_type_list[1])},
                {"_id": 0, "CARDINAL_NUMBER": 1}
            )))
        cardinal_number = calc_fitting_cardinal_number_by_formula(cardinal_number)
        # 定义，可以混合定义
        major_avg_score = Series([cardinal_number["CARDINAL_NUMBER"]], index=[risk_type_list[0]])
        major_avg_score.index.rename('MAJOR', inplace=True)
    elif major_ratio:
        df['AVG_NUMBER'] = df.apply(
            lambda row: get_major_avg_number(row, major_ratio), axis=1
        )
        return df
    elif isinstance(ratio_df, DataFrame):
        major_df = pd.merge(df, ratio_df, on='DEPARTMENT_ID')
        major_df['AVG_NUMBER'] = major_df.apply(
            lambda row: get_major_avg_number_by_ratio_df(row), axis=1
        )
        return major_df
    elif zhanduan_filter_list:
        major_df = df[~df.DEPARTMENT_ID.isin(zhanduan_filter_list)]
        major_avg_score = major_df.groupby(['MAJOR'])[numerator].sum() / major_df.groupby(['MAJOR'])[denominator].sum()
    else:
        major_avg_score = df.groupby(['MAJOR'])[numerator].sum() / df.groupby(['MAJOR'])[denominator].sum()
    data = pd.merge(
        df,
        major_avg_score.to_frame(name='AVG_NUMBER'),
        how='inner',
        left_on='MAJOR',
        right_index=True)
    data.fillna(0, inplace=True)
    return data


def calc_extra_child_score_groupby_major(df,
                                         dpid_data,
                                         column,
                                         func_calc_score,
                                         weight=None,
                                         detail_type=None):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def calc_extra_child_score_groupby_major_third(df,
                                               dpid_data,
                                               column,
                                               func_calc_score,
                                               zhanduan_filter_list=[],
                                               numerator='numerator',
                                               denominator='denominator',
                                               weight=None,
                                               detail_type=None,
                                               fraction=None,
                                               major_ratio=None,
                                               ratio_df=None):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_third(
        df, dpid_data, column,
        zhanduan_filter_list=zhanduan_filter_list,
        numerator=numerator,
        denominator=denominator, fraction=fraction,
        major_ratio=major_ratio,
        ratio_df=ratio_df)
    if major_ratio:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row['ratio'], major_ratio),
            axis=1)
    elif isinstance(ratio_df, DataFrame):
        data['SCORE'] = data.apply(lambda row: func_calc_score(row), axis=1)
    else:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
            axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def calc_extra_child_basic_data(df,
                                dpid_data,
                                column,
                                func_calc_score,
                                zhanduan_filter_list=[],
                                numerator='numerator',
                                denominator='denominator',
                                weight=None,
                                detail_type=None,
                                fraction=None,
                                major_ratio=None):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        df_rst -- 基于专业的最终部门得分
        data -- 中间过程数据
    """
    data = add_avg_number_by_major_third(df, dpid_data, column, zhanduan_filter_list=zhanduan_filter_list,
                                         numerator=numerator,
                                         denominator=denominator, fraction=fraction,
                                         major_ratio=major_ratio)

    if major_ratio:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row['ratio'], major_ratio),
            axis=1)
    else:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
            axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    data['group_sort'] = data[column].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    data = add_avg_score_by_major(data, 'SCORE')
    return df_rst, data


# 部分专业基数
def calc_extra_child_score_groupby_major_two(df,
                                             dpid_data,
                                             column,
                                             func_calc_score,
                                             zhanduan_filter_list,
                                             weight=None,
                                             detail_type=None,
                                             ):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_two(df, dpid_data, column, zhanduan_filter_list)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


def calc_score_groupby_major(df, dpid_data, hierarchy, main_type, detail_type,
                             col_numerator, col_denominator, column,
                             rtn_column, func_calc_score, months_ago,
                             risk_type, customizecontent=None):
    """基于各专业均数，计算各个部门得分

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        main_type {int} -- 指数大类[1~6]
        detail_type {str} -- [子指数小类]
        col_numerator {str} -- 分子列名
        col_denomiator {str} -- 分母列名
        column {str} -- 列名
        rtn_column {str} -- [返回的分值字段名]
        func_calc_score {func} -- 计算公式
        months_ago {int} -- 第前{-N}个月
        risk_type {str} 
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨）
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
        axis=1)
    export_basic_data(data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, column, 'AVG_NUMBER',
                      'SCORE', risk_type, customizecontent=customizecontent)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[rtn_column])
    return rst_data


# 部分专业基数
def calc_score_groupby_major_two(df, dpid_data, hierarchy, main_type, detail_type,
                                 col_numerator, col_denominator, column,
                                 rtn_column, func_calc_score, months_ago,
                                 risk_type, zhanduan_filter_list, customizecontent=None):
    """基于各专业均数，计算各个部门得分

    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        main_type {int} -- 指数大类[1~6]
        detail_type {str} -- [子指数小类]
        col_numerator {str} -- 分子列名
        col_denomiator {str} -- 分母列名
        column {str} -- 列名
        rtn_column {str} -- [返回的分值字段名]
        func_calc_score {func} -- 计算公式
        months_ago {int} -- 第前{-N}个月
        risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨）
        zhanduan_filter_list {list}
             -- 不参与专业基数计算的站段列表
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_two(df, dpid_data, column, zhanduan_filter_list)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
        axis=1)
    export_basic_data(data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, column, 'AVG_NUMBER',
                      'SCORE', risk_type, customizecontent=customizecontent)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[rtn_column])
    return rst_data


# 专业基数计算修改
def calc_score_groupby_major_third(df, dpid_data, hierarchy, main_type, detail_type,
                                   col_numerator, col_denominator, column,
                                   rtn_column, func_calc_score, months_ago,
                                   risk_type, customizecontent=None,
                                   zhanduan_filter_list=[],
                                   fraction=None, major_ratio=None,
                                   ratio_df=None,
                                   last_index_ratio=None
                                   ):
    """基于各专业均数，计算各个部门得分

    Arguments:
        :param df {DataFrame}} -- 各部门raw data
        :param dpid_data {DataFrame} -- 单位
        :param hierarchy {int} -- 单位层级【3、4、5分别代表站段、车间、班组】
        :param main_type {int} -- 指数大类[1~6]
        :param detail_type {str} -- [子指数小类]
        :param col_numerator {str} -- 分子列名
        :param col_denominator {str} -- 分母列名
        :param column {str} -- 列名
        :param rtn_column {str} -- [返回的分值字段名]
        :param func_calc_score {func} -- 计算公式
        :param months_ago {int} -- 第前{-N}个月
        :param risk_type {str}
            -- 各专业重点风险指数的类型，fmt: 专业名-代号，
            ex: "车辆-6"（车辆脱轨）
        :param customizecontent 自定义指数文本
        :param zhanduan_filter_list 不参与基数计算的站段id列表
        :param fraction 是否为三月基数
        :param major_ratio 是否为固定基数
        :param ratio_df DataFrame, 依据给定公式计算得出的专业基数
        :param last_index_ratio 上月的ratio及专业基数

    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major_third(
        df, dpid_data, column,
        zhanduan_filter_list=zhanduan_filter_list,
        numerator=col_numerator, denominator=col_denominator,
        fraction=fraction, major_ratio=major_ratio, ratio_df=ratio_df)
    if major_ratio:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row['ratio'], major_ratio),
            axis=1)
    elif last_index_ratio:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(
                row, column, 'AVG_NUMBER', detail_type, last_index_ratio),
            axis=1)
    elif isinstance(ratio_df, DataFrame):
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row), axis=1)
    else:
        data['SCORE'] = data.apply(
            lambda row: func_calc_score(row, column, 'AVG_NUMBER', detail_type),
            axis=1)
    export_basic_data(data, main_type, detail_type, hierarchy, months_ago,
                      col_numerator, col_denominator, column, 'AVG_NUMBER',
                      'SCORE', risk_type, customizecontent=customizecontent)
    rst_data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[rtn_column])
    return rst_data


def combine_child_index_func(func_list, months_ago):
    """传入子指数计算的函数列表，将计算结果存放在list中

    Arguments:
        func_list {list[func]} -- [description]
        months_ago {int} -- 更新前第-N个月份，比如更新上个月也是-1，依次-2，,-3...]

    Returns:
        list[dataframe] -- 各个指数
    """
    # 存放所有子指数项的分数
    child_score = []
    for c_func in func_list:
        current_app.logger.debug(
            f'├── \'{c_func.__name__}\' index start computing!')
        start = time.time()
        func_result = c_func(months_ago)
        if func_result is not None:
            child_score.extend(func_result)
        current_app.logger.debug(
            f'├── \'{c_func.__name__}\' index has been figured out!')
        cost = round(time.time() - start, 3)
        current_app.logger.debug(
            f'├── \'{c_func.__name__}\' index had costed time --- {cost} ')
    return child_score


# 如果数据有缺失，按站段补全数据
def data_complete_by_condition(raw_df, filled_data, columns, default, merged_data):
    """[补全数据，填充默认值]
    1.数据全部缺失补全站段
    2.数据部分缺失，找出缺失站段补全
    3.数据完整不处理
    Arguments:
        raw_df {[type]} -- [description]
        filled_data {[type]} -- [description]
        default {[type]} -- [description]
    """
    if raw_df.empty is True:
        raw_df = filled_data[['DEPARTMENT_ID']].copy()
        raw_df.loc[:, 'COUNT'] = 0
        for idx, column in enumerate(columns):
            raw_df.loc[:, column] = default[idx]
        raw_df.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
        raw_df = df_merge_with_dpid(raw_df, merged_data)
    else:
        raw_df = df_merge_with_dpid(raw_df, merged_data)
        raw_type3_set = set(raw_df['TYPE3'].values.tolist())
        real_type3_set = set(filled_data['DEPARTMENT_ID'].values.tolist())
        subset = real_type3_set - raw_type3_set
        if subset:
            init_data = [[data] for data in subset]
            sub_df = pd.DataFrame(data=init_data, columns=['FK_DEPARTMENT_ID'])
            sub_df.loc[:, 'COUNT'] = 0
            sub_df = df_merge_with_dpid(sub_df, merged_data)
            for idx, column in enumerate(columns):
                sub_df.loc[:, column] = default[idx]
            raw_df = raw_df.append(sub_df, sort=False)
        else:
            pass
    return raw_df


def update_mongo_data_by_condition(months_ago, condition, data, coll_name):
    """[根据需要更新mongo数据]
    
    Arguments:
        months_ago {[type]} -- [description]
        condition {[type]} -- [description]
        set_condition {[type]} -- [description]
        coll_name {[type]} -- [description]
    """
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}{coll_name}'
    mongo.db[coll_name].remove(condition)
    write_bulk_mongo(coll_name, data)


def write_cardinal_number_basic_data(
        df_calc, zhanduan_dpid_data, fraction,
        risk_type, main_type, detail_type, months_ago,
        columns=['numerator', 'denominator']):
    """[summary]
    将基数计算数据导入数据库中
    Arguments:
        fraction {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]
    """
    df_calc_cp = df_calc.copy()
    df_calc_cp.index.rename('TYPE3', inplace=True)
    mon = get_history_months(months_ago)[0]
    prefix = get_coll_prefix(months_ago)
    coll_name = f'{prefix}major_index_cardinal_number_basic_data'
    df_calc_cp = pd.merge(
        df_calc_cp,
        zhanduan_dpid_data,
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    rst_data = []
    for idx, row in df_calc_cp.iterrows():
        _tmp_rst = {
            'HIERARCHY': 3,
            'MAJOR': risk_type.split('-')[0],
            'INDEX_TYPE': int(risk_type.split('-')[1]),
            'MON': mon,
            'MAIN_TYPE': main_type,
            'DETAIL_TYPE': detail_type,
            'DENOMINATOR': row[columns[1]],
            'DENOMINATOR_NAME': fraction.denominator.name,
            'DENOMINATOR_DESCRIPTION': fraction.denominator.description,
            'DENOMINATOR_VERSION': fraction.denominator.version,
            'NUMERATOR': row[columns[0]],
            'NUMERATOR_NAME': fraction.numerator.name,
            'NUMERATOR_DESCRIPTION': fraction.numerator.description,
            'NUMERATOR_VERSION': fraction.numerator.version,
            'DEPARTMENT_ID': row['DEPARTMENT_ID'],
            'DEPARTMENT_NAME': row['NAME']
        }
        rst_data.append(
            _tmp_rst
        )
    mongo.db[coll_name].remove({
        'HIERARCHY': 3,
        'MAJOR': risk_type.split('-')[0],
        'INDEX_TYPE': int(risk_type.split('-')[1]),
        'MON': mon,
        'MAIN_TYPE': main_type,
        'DETAIL_TYPE': detail_type,
        'DENOMINATOR_NAME': fraction.denominator.name,
        'DENOMINATOR_VERSION': fraction.denominator.version,
        'NUMERATOR_NAME': fraction.numerator.name,
        'NUMERATOR_VERSION': fraction.numerator.version,
    })
    write_bulk_mongo(coll_name, rst_data)


def get_last_month_alpha(months_ago, risk_type, main_type, detail_type):
    """
    获取某指数上个的a值
    a=（段调车风险问题数/段问题总数-专业基数）/专业基数。
    """
    from app.data.major_risk_index.common.cardinal_number_common import (
        calc_fitting_cardinal_number_by_formula)
    # 获取专业基数
    history_three_months = [months_ago - i for i in range(1, 4)]
    # 获取三个月前对应的基数，选择最大值（其实就是获取上个月的专业基数）
    risk_type_list = risk_type.split('-')
    cardinal_number = []
    for mon_ago in history_three_months:
        prefix = get_coll_prefix(mon_ago)
        mon = get_history_months(mon_ago)[0]
        cardinal_number.extend(list(mongo.db[f'{prefix}major_index_cardinal_number_basic_data'].find(
            {"MON": mon, "MAIN_TYPE": main_type,
             "DETAIL_TYPE": detail_type,
             "CARDINAL_NUMBER": {'$exists': 'true'},
             "MAJOR": risk_type_list[0], "INDEX_TYPE": int(risk_type_list[1])},
            {"_id": 0, "CARDINAL_NUMBER": 1}
        )))
    cardinal_number = calc_fitting_cardinal_number_by_formula(cardinal_number)
    # 获取上一个月 问题数,总问题数
    # celic = 段问题数/段总问题数
    prefix = get_coll_prefix(months_ago - 1)
    mon = get_history_months(months_ago - 1)[0]
    problem_data = list(mongo.db[f'{prefix}major_index_cardinal_number_basic_data'].find(
        {"MON": mon, "MAIN_TYPE": main_type,
         "DETAIL_TYPE": detail_type,
         "DEPARTMENT_ID": {'$exists': 'true'},
         "MAJOR": risk_type_list[0], "INDEX_TYPE": int(risk_type_list[1])},
        {"_id": 0, "DEPARTMENT_ID": 1, "DENOMINATOR": 1, "NUMERATOR": 1}
    ))
    celic_dict = dict()
    for row in problem_data:
        celic_dict[row['DEPARTMENT_ID']] = row['NUMERATOR'] / row['DENOMINATOR']
    return cardinal_number['CARDINAL_NUMBER'], celic_dict


def get_major_index_data_bymon(months_ago, risk_type):
    """
    获取某个月指数的数据
    """
    prefix = get_coll_prefix(months_ago)
    mon = get_history_months(months_ago)
    risk_type_list = risk_type.split('-')
    rst = list(mongo.db[f'{prefix}_major_index'].find(
        {"MON": mon, "MAJOR": risk_type_list[0],
         "TYPE": int(risk_type_list[1]), "HIERARCHY": 3},
        {"_id": 1, "DEPARTMENT_ID": 1,
         "DEPARTMENT_NAME": 1, "RANK": 1}
    )
    )
    for item in rst:
        item['MON_AGO'] = months_ago
    return rst


if __name__ == '__main__':
    pass
