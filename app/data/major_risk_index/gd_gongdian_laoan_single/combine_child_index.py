# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/9/19下午3:24
   Change Activity: 2019/9/19下午3:24
-------------------------------------------------
"""

from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.gd_gongdian_laoan_single import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification, init_common_data)
from app.data.major_risk_index.gongdian_laoan.common import (get_vitual_major_ids)
from app.data.major_risk_index.gd_gongdian_laoan_single.common_sql import (
    CHEJIAN_DPID_SQL, ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 35
    risk_type = '工电-10'
    # init_monthly_index_map(months_ago, risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    ids = get_vitual_major_ids("工电-10")
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        vitual_major_ids=ids)

    update_major_maintype_weight(index_type=10, major=risk_type)


if __name__ == '__main__':
    pass
