# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/9/19下午3:18
   Change Activity: 2019/9/19下午3:18
-------------------------------------------------
"""
from app.data.major_risk_index._global_var import GlobalVar
module = __package__
GLV = GlobalVar(module)


from . import (assess_intensity, check_evenness, check_intensity,
               evaluate_intensity, problem_exposure, problem_rectification,
               combine_child_index)
