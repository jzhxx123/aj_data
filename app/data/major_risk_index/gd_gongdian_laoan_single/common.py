#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2020/2/21下午4:06
   Change Activity: 2020/2/21下午4:06
-------------------------------------------------
"""


def _calc_score_for_by_major_ratio_df(row):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    n1 = row['ratio_1']
    sn1 = row['score_1']
    n2 = row['ratio_2']
    sn2 = row['score_2']
    n3 = row['ratio_3']
    sn3 = row['score_3']
    c = row['ratio']
    for idx, item in enumerate([n1, n2, n3]):
        if c > item:
            level = idx + 1
            break
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _calc_score_for_by_major_ratio(self_ratio, major_ratio):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    n1 = major_ratio[0][0]
    sn1 = major_ratio[0][1]
    n2 = major_ratio[1][0]
    sn2 = major_ratio[1][1]
    n3 = major_ratio[2][0]
    sn3 = major_ratio[2][1]
    c = self_ratio
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score
