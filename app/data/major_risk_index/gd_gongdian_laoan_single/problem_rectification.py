# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification
   Author :       hwj
   date：          2019/9/19下午3:25
   Change Activity: 2019/9/19下午3:25
-------------------------------------------------
"""
from flask import current_app

from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import REPEATE_HAPPEN_PROBLEM_SQL
from app.data.major_risk_index.gd_gongdian_laoan_single import GLV
from app.data.major_risk_index.gd_gongdian_laoan_single.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_laoan.problem_rectification_sql import \
    RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, OVERDUE_PROBLEM_NUMBER_SQL, HAPPEN_PROBLEM_POINT_SQL, \
    MAJOR_PROBLEM_POINT_INFO_SQL
from app.data.util import update_major_maintype_weight, pd_query

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        WORKER_COUNT, PROBLEM_POINT_INFO_DATA
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(ids))
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major, ids))
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 问题控制
def _stats_repeatedly_index(months_ago):
    """按安监室9月14日公布的劳安问题控制描述进行得分评价"""
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20}
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


# 整改成效
def _stats_repeatedly_effect(months_ago):
    """劳安警告性预警一次扣10分，延期一次扣20分；(暂未加入)
    劳安责任事故主要、全部责任的1个扣20分、重要扣15分、次要的扣10分（含追究责任）；
    综合劳安信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。
    直接在总分中扣，改项最多扣40分（总分不能低于0)。只统计“局属单位责任”的事故
    """
    return problem_rectification.stats_rectification_effect_two(
        CHECK_RISK_IDS, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_repeatedly_index,
        _stats_repeatedly_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'f']]
    item_weight = [0.4, 0.6, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=10, major=risk_type, main_type=6,
                                 child_index_list=[1, 3, 6],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
