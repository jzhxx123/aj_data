# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/9/19下午3:19
   Change Activity: 2019/9/19下午3:19
-------------------------------------------------
"""

# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, if(b.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND a.TYPE3 in {0}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        if(c.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""

# 电务信号三级施工 T_CONSTRUCT_THREE_SELECT_CONTROL
WORK_HOUR_CTSC_SQL = """SELECT
    DEPARTMENT_ID,
    REALITY_ADMINISTRATOR,
    REALITY_PRINCIPAL,
    REALITY_TESTER,
    INDOOR_PERSON,
    OUTDOOR_PERSON,
    hour(END_SUBMIT_DATE) - hour(START_SUBMIT_DATE) AS HOURS
FROM
    dw_t_construct_three_select_control
WHERE
    DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND STATUS = 2;
"""

# 电务生产人数工时
WORK_HOUR_SQL = """SELECT
        DEPARTMENT_ID,
        WORKER_COUNT AS PERSON_COUNT,
        REALITY_TIME / 60 AS HOURS
    FROM
        dw_t_daily_maintain_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION SELECT
        DEPARTMENT_ID,
        INPLACE_WORKER_COUNT AS PERSON_COUNT,
        (INDOOR_WORK_TIME + OUT_WORK_TIME) / 60 AS HOURS
    FROM
        dw_t_concentrate_overhaul_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION SELECT
        DEPARTMENT_ID,
        PERSON_CONTENT AS PERSON_COUNT,
        hour(END_TIME) - hour(START_TIME) AS HOURS
    FROM
        dw_t_cooperate_work_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION SELECT
        DEPARTMENT_ID,
        PERSON_UP_COUNT AS PERSON_COUNT,
        hour(WRITE_OFF_TIME) - hour(REGISTER_TIME) AS HOURS
    FROM
        dw_t_status_update_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
         < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION SELECT
        DEPARTMENT_ID,
        PERSON_CONTENT AS PERSON_COUNT,
        hour(END_SUBMIT_DATE) - hour(START_SUBMIT_DATE)  AS HOURS
    FROM
        dw_t_temp_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2;
"""

