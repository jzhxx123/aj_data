# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure
   Author :       hwj
   date：          2019/9/19下午3:25
   Change Activity: 2019/9/19下午3:25
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.gd_gongdian_laoan_single import GLV
from app.data.major_risk_index.gd_gongdian_laoan_single.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_laoan.check_intensity_sql import ALL_PROBLEM_NUMBER_SQL
from app.data.major_risk_index.gongdian_laoan.common import stats_total_problem_exposure_type, get_vitual_major_ids, \
    get_ratio_df
from app.data.major_risk_index.gongdian_laoan.problem_exposure_sql import (
    CHECK_PROBLEM_SQL,
    EXPOSURE_PROBLEM_DEPARTMENT_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL, SELF_HIDDEN_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_vitual_major_ids(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, WORK_LOAD, \
        ALL_PROBLEM_NUMBER, SELF_HIDDEN_PROBLEM_NUMBER
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    stats_month = get_custom_month(months_ago)
    # 劳安类问题数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA
    )

    # 自查较大隐患问题数
    SELF_HIDDEN_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(SELF_HIDDEN_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA
    )
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """数量占40%，质量分占60%）：总问题数（质量分）/工作量与本专业基数比较（50%），一般及以上数与本专业基数比较（50%）"""
    work_load = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='PERSON_NUMBER')
    ratio_df_1 = get_ratio_df(WORK_LOAD, 0.9, 0.8, 0.5)
    ratio_df_2 = get_ratio_df(WORK_LOAD, 5, 4, 2.5)
    ratio_df_dict = {
        '总问题数': [ratio_df_1, ratio_df_2]
    }
    return stats_total_problem_exposure_type(
        RISK_IDS, CHECK_PROBLEM_SQL, work_load, work_load, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, weight_item=[0.5, 0.5],
        ratio_df_dict=ratio_df_dict)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续4月无的扣2分/条，连续5个月无的扣4分/条，…扣月份-2分/条。得分=100-扣分。"""
    deduct = {i: 2 * i - 6 for i in range(4, 13)}
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=deduct, months=13)


# 较严重隐患暴露指数
def _stats_hidden_problem_exposure(months_ago):
    """自查较大隐患问题/所有劳安问题  与对应基数对比"""
    return problem_exposure.stats_hidden_problem_exposure_ratio(
        SELF_HIDDEN_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data
    )


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。"""
    problem_risk_score = {'1': 5, '2': 2, '3': 1, }

    return problem_exposure.stats_other_problem_exposure_excellent(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score=problem_risk_score)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.65, 0.1, 0.15, 0.1, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=10, major=risk_type, main_type=5,
                                 child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
