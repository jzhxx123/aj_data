#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql.py
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 10:57 上午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import(
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, BANZU_POINT_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL
)