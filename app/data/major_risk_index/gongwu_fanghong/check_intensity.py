# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gongwu_fanghong import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.gongwu_fanghong.check_intensity_sql import(
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, BANZU_POINT_SQL
)
from app.data.major_risk_index.gongwu_fanghong.common_sql import (
    WORK_LOAD_SQL,
    RISK_WORK_BANZU_COUNT_SQL)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_func(row, column, major_column, detail_type=None):
    """
    计算覆盖率
    Arguments:
        row {[type]} -- [description]
        column {[type]} -- [description]
        major_column {[type]} -- [description]

    Keyword Arguments:
        detail_type {[type]} -- [description] (default: {None})
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 90 + _ratio * 66
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 160
    else:
        _score = 71 + _ratio * 50
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT, \
        PRECTL_FLOOD_WORK_LOAD
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS, RISK_IDS = check_item_ids, risk_ids
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 防洪工作量
    PRECTL_FLOOD_WORK_LOAD = df_merge_with_dpid(
        GLV.get_value('PRECTL_FLOOD_WORK_LOAD'),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        PRECTL_FLOOD_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    fraction = GLV.get_value('stats_check_problem_ratio', (None,))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        PROBLEM_COUNT,
        PRECTL_FLOOD_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    title = [
        '较大和重大安全风险问题质量分累计({0})/防洪及地质灾害问题数({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/防洪及地质灾害问题数({1})'
    ]
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        title=title,
        fraction_list=fraction_list)


# 问题质量均分
def _stats_score_per_check_problem(months_ago):
    fraction = GLV.get_value('stats_score_per_check_problem', (None,))[0]
    return check_intensity.stats_score_per_check_problem(
        PROBLEM_SCORE,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    statmonth = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(
        *statmonth, RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(
        *statmonth, RISK_IDS)
    major = _get_major_dpid(RISK_TYPE)
    monitor_watch_discovery_ratio_sqllist = [
        RISK_WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS),
        BANZU_POINT_SQL.format(major)
    ]
    title = ['监控调阅时长累计({0})/工作量({1})',
             '监控调阅发现点外修问题数({0})/工作量({1})',
             '监控调阅发现问题质量分累计({0})/工作量({1})',
             '调阅班组数({0})/作业班组数({1})']
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        PRECTL_FLOOD_WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        title=title,
        calc_score_by_formula=_calc_func,
        fraction_list=fraction_list)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person, _stats_score_per_check_problem,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'f', 'k', 'j']
    ]
    item_weight = [0.25, 0.25, 0.1, 0.25, 0.15]
    item_list = [2, 3, 6, 11, 10]
    update_major_maintype_weight(index_type=4, major=risk_type, main_type=1,
                                 child_index_list=item_list, child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
