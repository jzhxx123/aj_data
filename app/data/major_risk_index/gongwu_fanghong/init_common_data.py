#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gongwu_fanghong import GLV
from app.data.major_risk_index.gongwu_fanghong.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL,
    WORK_LOAD_SQL)
from app.data.major_risk_index.gongwu_fanghong.common import(
    get_vitual_major_ids, calc_total_workload)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month)
import pandas as pd


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-3")

    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # 防洪工作量
    # PRECTL_FLOOD_WORK_LOAD = pd_query(WORK_LOAD_SQL.format(major))
    # 成都工务大机段，成都工务大修段，成都桥路大修段
    prectl_flood_work_load_list = [
        # 遂宁工务段, 涪陵工务段
        ['19B8C3534E065665E0539106C00A58FD', 626.925], ['19B8C3534E085665E0539106C00A58FD', 613.074],
        # 内江工务段, 达州工务段
        ['19B8C3534E0D5665E0539106C00A58FD', 748.122], ['19B8C3534E0F5665E0539106C00A58FD', 768.414],
        # 成都工务段, 成都工务大机段
        ['19B8C3534E135665E0539106C00A58FD', 849.569], 
        # ['19B8C3534E145665E0539106C00A58FD', 281],
        # 贵阳工务段, 成都工务大修段
        ['19B8C3534E1D5665E0539106C00A58FD', 1382.123], 
        # ['19B8C3534E255665E0539106C00A58FD', 63],
        # 重庆工务段, 绵阳工务段
        ['19B8C3534E275665E0539106C00A58FD', 1046.634], ['19B8C3534E2D5665E0539106C00A58FD', 992.863],
        # 凯里工务段, 贵阳高铁工务段
        ['19B8C3534E395665E0539106C00A58FD', 612.442], ['19B8C3534E505665E0539106C00A58FD', 938.449],
        # 成都高铁工务段, 重庆工电段
        ['19B9D8D920D8589FE0539106C00A1189', 939.312], ['99990002001499A10010', 679.568],
        # 宜宾工电段, 六盘水工电段
        ['99990002001499A10012', 664.837], ['99990002001499A10013', 1039.402],
        # 成都桥路大修段, 西昌工电段
        # ['99990002001499A10014', 2], 
        ['99990002001499A10015', 986.918],
    ]
    PRECTL_FLOOD_WORK_LOAD = pd.DataFrame(data=prectl_flood_work_load_list, columns=['FK_DEPARTMENT_ID', 'COUNT'])

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "PRECTL_FLOOD_WORK_LOAD": PRECTL_FLOOD_WORK_LOAD,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
