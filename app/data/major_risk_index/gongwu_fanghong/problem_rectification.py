# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gongwu_fanghong import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.gongwu_fanghong.problem_rectification_sql import(
    OVERDUE_PROBLEM_NUMBER_SQL,
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL
)
from app.data.index.util import (
    get_custom_month, get_query_condition_by_risktype)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.gongwu_fanghong.common import(
    data_complete_by_condition
)

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    stats_months = get_custom_month(months_ago)

    # 防控责任详细信息filled_data, columns, default, merged_data
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_months, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=2)


def _stats_recification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data, df_dict,
        calc_score_by_formula=lambda x: max(0, 100 - x))


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_recification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'f']]
    item_weight = [0.4, 0.6]
    update_major_maintype_weight(index_type=4, major=risk_type, main_type=6,
                                 child_index_list=[1, 6], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
