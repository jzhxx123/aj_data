#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2018/09/27 
'''

import pandas as pd

from app import mongo
from app.data.index.common import (append_major_column_to_df,
                                   merge_all_child_item)
from app.data.major_risk_index.common_sql import (CHEJIAN_DPID_SQL,
                                                  ZHANDUAN_DPID_SQL)
from app.data.index.util import validate_exec_month
from app.data.util import (get_coll_prefix, get_history_months, pd_query,
                           write_bulk_mongo)


def _choose_dpid_data(hierarchy, risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }

    major = major_dpid.get(risk_type.split('-')[0])
    dpid_data = {
        3: pd_query(ZHANDUAN_DPID_SQL.format(major)),
        4: pd_query(CHEJIAN_DPID_SQL.format(major)),
    }
    return dpid_data.get(hierarchy)


def _merge_child_index(months_ago, risk_name, risk_type):
    year_mon = get_history_months(months_ago)[0]
    _prefix = get_coll_prefix(months_ago)
    coll_name = '{}detail_major_index'.format(_prefix)
    data = []
    for main_type in range(1, 7):
        for hierarchy in [3]:
            child_data = pd.DataFrame(
                list(mongo.db[coll_name].find(
                    {
                        "MAJOR": risk_type.split('-')[0],
                        "TYPE": int(risk_type.split('-')[1]),
                        "MAIN_TYPE": main_type,
                        "DETAIL_TYPE": 0,
                        "MON": year_mon,
                        'HIERARCHY': hierarchy,
                    }, {
                        "_id": 0,
                        "SCORE": 1,
                        "DEPARTMENT_ID": 1,
                    })))
            if child_data.empty is True:
                continue
            child_data = pd.DataFrame(
                index=child_data['DEPARTMENT_ID'],
                data=child_data.loc[:, 'SCORE'].values,
                columns=[f'SCORE_{main_type}_{hierarchy}'])
            data.append(child_data)
    item_name = [f'SCORE_{x}' for x in [1, 2, 3, 4, 5, 6]]
    item_weight = [0.3, 0.25, 0.2, 0.05, 0.1, 0.1]
    for hierarchy in [3]:
        h_child_score = [
            x for x in data if x.columns.values[0][-1] == str(hierarchy)
        ]
        if len(h_child_score) == 0:
            continue
        xdata = pd.concat(h_child_score, axis=1, sort=False)
        xdata.fillna(0, inplace=True)
        xdata['SCORE'] = xdata.apply(
            lambda row: merge_all_child_item(row, hierarchy, item_name, item_weight),
            axis=1)
        xdata = append_major_column_to_df(
            _choose_dpid_data(hierarchy, risk_type), xdata)
        xdata['group_sort'] = xdata['SCORE'].groupby(xdata['MAJOR']).rank(
            ascending=0, method='first')
        xdata.dropna(inplace=True)
        rst = []
        for index, row in xdata.iterrows():
            rst.append({
                'MON': year_mon,
                'MAJOR': row['MAJOR'],
                "TYPE": int(risk_type.split('-')[1]),
                'HIERARCHY': hierarchy,
                'DEPARTMENT_ID': row['DEPARTMENT_ID'],
                'DEPARTMENT_NAME': row['NAME'],
                'SCORE': round(row['SCORE'], 2),
                'RANK': int(row['group_sort'])
            })
        coll_name = '{}major_index'.format(_prefix)
        mongo.db[coll_name].remove({
            "MAJOR": risk_type.split('-')[0],
            "TYPE": int(risk_type.split('-')[1]),
            'MON': year_mon,
            'HIERARCHY': hierarchy,
        })
        write_bulk_mongo(coll_name, rst)


def execute(months_ago, risk_name, risk_type):
    _merge_child_index(months_ago, risk_name, risk_type)
    # _get_dataviz_dataset()


if __name__ == '__main__':
    pass
