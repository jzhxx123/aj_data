# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            LEFT JOIN
        t_department_classify_config AS c
        ON a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = c.PK_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
            AND c.NAME = '高铁通信车间'
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != "";
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
            LEFT JOIN
        t_department_classify_config AS d
        ON a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = d.PK_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
            AND d.NAME = '高铁通信车间';
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            LEFT JOIN
        t_department_classify_config AS c
        ON b.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = c.PK_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
            AND c.NAME = '高铁通信车间'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        LEFT JOIN
    t_department_classify_config AS c
    ON b.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = c.PK_ID
WHERE
    a.MONTH = {0}
        AND b.TYPE = 4
        AND b.TYPE2 = '{1}'
        AND c.NAME = '高铁通信车间';
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            LEFT JOIN
        t_department_classify_config AS c
        ON b.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = c.PK_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
            AND a.IDENTITY = '干部'
            AND c.NAME = '高铁通信车间'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 量化人员及干部检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 量化人员及干部发现问题数
CHECK_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND FK_CHECK_ITEM_ID IN ({2})
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 一般及以上风险问题数(量化人员及干部)
RISK_LEVEL_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_LEVEL <= 2
            AND FK_CHECK_ITEM_ID IN ({2})
    GROUP BY EXECUTE_DEPARTMENT_ID;
"""

# 问题质量分累计(量化人员及干部)
PROBLEM_CHECK_SCORE_SQL = """SELECT
    EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
FROM
    t_check_problem AS a
        LEFT JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_CHECK_ITEM_ID IN ({2})
GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""