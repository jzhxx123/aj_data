# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL, BANZU_POINT_SQL, KAOHE_PROBLEM_SQL,
    REAL_CHECK_BANZU_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    YIBAN_RISK_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.common.common_sql import (
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL)
from app.data.major_risk_index.dianwu_gaotietxzl.common_sql import (
    CHECK_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, ASSESS_PROBLEM_COUNT, \
        YIBAN_RISK_PROBLEM_NUMBER, ALL_PROBLEM_NUMBER
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 一般风险问题数
    YIBAN_RISK_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(
            YIBAN_RISK_PROBLEM_NUMBER_SQL.format(
                *stats_month, check_item_ids)), DEPARTMENT_DATA)
    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, check_item_ids)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    return check_intensity.stats_check_per_person(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    return check_intensity.stats_check_problem_ratio_type_one(
        ALL_PROBLEM_NUMBER,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    return check_intensity.stats_check_problem_assess_radio_type_one(
        ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    return check_intensity.stats_risk_score_per_person(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data)


# 人均质量分
def _stats_score_per_person(months_ago):
    return check_intensity.stats_score_per_person(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 一般以上风险占比
def _stats_yiban_risk_ratio(months_ago):
    return check_intensity.stats_yiban_risk_ratio_type_one(
        YIBAN_RISK_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio(
        REAL_CHECK_BANZU_SQL,
        BANZU_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    return check_intensity.stats_media_intensity(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_check_problem_assess_radio, _stats_score_per_person,
        _stats_risk_score_per_person, _stats_media_intensity,
        _stats_yiban_risk_ratio, _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'd', 'e', 'f', 'h', 'i', 'j']
    ]
    item_weight = [0.25, 0.2, 0.1, 0.1, 0.1, 0.05, 0.05, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd', 'f', 'h', 'i']]
    # item_weight = [0.25, 0.2, 0.08, 0.25, 0.9, 0.13]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 1, months_ago,
    #                      item_name, item_weight, [4])
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
