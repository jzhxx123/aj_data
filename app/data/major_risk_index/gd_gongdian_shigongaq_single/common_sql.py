# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/9/20上午8:51
   Change Activity: 2019/9/20上午8:51
-------------------------------------------------
"""

# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, if(b.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND a.TYPE3 in {0}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        if(c.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
    FK_DEPARTMENT_ID, MANAGEMENT_INDEX AS COUNT 
FROM
    `t_department_integrated_management`
WHERE
    (FK_TYPE_ID = 2552
    OR
    FK_TYPE_ID = 2694)
    and MONTH={0}
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""

# 量化人员及干部检查次数
# 检查次数（现场检查）
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 读取指定时间段内，有结束工作的工作票的班组(department_id)的清单
# 返回 department_id, count
DEPARTMENT_TICKET_COUNT_SQL = """SELECT
DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT FROM (
    SELECT un.depart_id as DEPARTMENT_ID, 1 AS COUNT
    FROM gd_t_work_ticket_type_1 AS tk
    INNER JOIN
    gd_t_unit AS un on tk.fk_unit_id = un.realid
    WHERE tk.STATUS = 4
    AND WORK_END_TIME BETWEEN '{0} 00:00:00' AND '{1} 00:00:00'
    UNION
    select  un.depart_id as DEPARTMENT_ID, 1 AS COUNT
    from gd_bd_work_ticket AS tk
    INNER JOIN
    gd_t_unit AS un on tk.unit_id = un.realid
    WHERE tk.STATUS = 5
    AND FINISH_JOB_DATE BETWEEN '{0} 00:00:00' AND '{1} 00:00:00'
    UNION
    select un.depart_id as DEPARTMENT_ID, 1 AS COUNT
    FROM gd_t_work_ticket_for_truck as tk
    INNER JOIN
    gd_t_unit AS un ON tk.FK_UNIT_ID = un.REALID
    WHERE tk.STATUS = 4
    AND WORK_END_TIME BETWEEN '{0} 00:00:00' AND '{1} 00:00:00'
) t1
GROUP BY DEPARTMENT_ID;
"""
