# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/9/20上午9:15
   Change Activity: 2019/9/20上午9:15
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.check_intensity_sql import (
    REAL_CHECK_BANZU_SQL, REAL_CHECK_POINT_SQL, BANZU_POINT_SQL, CHECK_POINT_SQL)
from app.data.major_risk_index.gd_gongdian_shigongaq_single import GLV
from app.data.major_risk_index.gd_gongdian_shigongaq_single.common import _calc_score_for_by_major_ratio
from app.data.major_risk_index.gongdian_shigongaq.check_intensity_sql import CHECK_COUNT_SQL, \
    SHIGONG_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, \
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, \
    YECHA_CHECK_SQL, WORK_BANZU_COUNT_SQL
from app.data.major_risk_index.gongdian_shigongaq.common import get_check_address_standard_data, \
    stats_media_intensity_major
from app.data.major_risk_index.gongdian_shigongaq.common_sql import DEPARTMENT_TICKET_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide_major)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid('工务-1')
    global RISK_TYPE, RISK_NAME, CHECK_RISK_IDS, CHECK_ITEM_IDS
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, SHIGONG_PROBLEM_NUMBER, ALL_PROBLEM_NUMBER, \
        PROBLEM_SCORE, YIBAN_RISK_SCORE, YECHA_COUNT, REAL_CHECK_BANZU_DATA,\
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, BANZU_POINT_DATA,\
        DEPARTMENT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    '''
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in risk_stats_conf[2].split(',')])
    '''

    # 统计工作量【施工工作总量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 检查总次数
    # 计算[检查频次]分子
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 查处供电-施工问题数
    SHIGONG_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(SHIGONG_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA
    )

    # 问题库总问题数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(CHECK_RISK_IDS)), DEPARTMENT_DATA
    )

    # 累计质量分
    # 计算[质量均分]分子
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 一般风险问题质量均分
    YIBAN_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 具备工作票的部门
    department_ticket_count = pd_query(DEPARTMENT_TICKET_COUNT_SQL.format(*stats_month), db_name='db_mid')
    # 工作班组信息
    work_banzu_info_data = pd_query(WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))
    work_banzu_info_data = pd.merge(
        department_ticket_count,
        work_banzu_info_data[['FK_DEPARTMENT_ID']],
        on='FK_DEPARTMENT_ID')
    work_banzu_info_data.drop(['COUNT'], inplace=True, axis=1)

    REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA = get_check_address_standard_data(
        work_banzu_info_data,
        DEPARTMENT_DATA, months_ago, CHECK_ITEM_IDS, major, is_base_item=True)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
                        + '现场检查作业次数({4})/ 施工工作总量({5})</p>', None]
    major_ratio = [(1, 100), (0.9, 90), (0.6, 60)]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio
    )


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查处问题率({3}) = '
                        + '施工问题数({4})/ 问题库总问题数({5})</p>', None]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        SHIGONG_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = '
                        + '问题质量分累计({4})/ 施工工作总量({5})</p>', None]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 一般风险问题质量均分
def _stats_yiban_risk_score_per_person(months_ago):
    """一般及及以上问题质量分累计//本单位作业数量（采集接触网、变配电、电力生产管理系统工作票数量）"""
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>一般风险问题质量均分({3}) = '
                        + '一般风险及以上问题质量分累计({4})/施工工作总量({5})</p>', None]
    return calc_child_index_type_divide_major(
        YIBAN_RISK_SCORE,
        WORK_LOAD,
        2,
        1,
        15,
        months_ago,
        'COUNT',
        'SCORE_o',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        customizecontent=customizecontent)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """现场检查（供电-施工-某项施工作业-施工配合监管）地点数/施工监管点总数量"""
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
                        + '现场检查（施工）地点数({4})/ 工区总数量({5})*100%</p>', None]
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 夜查率
def _stats_yecha_ratio(months_ago):
    """现场检查夜查（施工）次数/现场检查总次数
    """
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '
                        + '夜间检查施工次数({4})/ 夜间工作量({5})*100%</p>', None]
    return check_intensity.stats_yecha_ratio_type_gd(
        YECHA_COUNT,
        CHECK_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 监控调阅力度
def _stats_media_intensity(months_ago):
    title = ['监控调阅时长累计({0})/施工工作总量({1})',
             '监控调阅发现问题数({0})/施工工作总量({1})', '监控调阅发现问题质量分累计({0})/施工工作总量({1})',
             '调阅班组数({0})/班组数({1})']
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month, CHECK_ITEM_IDS),
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(CHECK_ITEM_IDS)
    ]
    return stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.35, 0.35, 0.2, 0.1],
        title=title,
        risk_ids=CHECK_RISK_IDS,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        calc_score_by_formula=_calc_score_by_formula
    )


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_score_per_person,
        _stats_yiban_risk_score_per_person,
        _stats_media_intensity,
        _stats_yecha_ratio,
        _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'o', 'i', 'm', 'j']]
    item_weight = [0.40, 0.10, 0.1, 0.1, 0.1, 0.1, 0.1]
    update_major_maintype_weight(index_type=11, major=risk_type,
                                 main_type=1, child_index_list=[2, 3, 5, 15, 9, 13, 10], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
