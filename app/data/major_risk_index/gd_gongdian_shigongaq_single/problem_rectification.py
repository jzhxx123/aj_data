# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification
   Author :       hwj
   date：          2019/9/20上午9:03
   Change Activity: 2019/9/20上午9:03
-------------------------------------------------
"""

from flask import current_app

from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import CHECK_EVALUATE_SZ_NUMBER_SQL, \
    REPEATE_HAPPEN_PROBLEM_SQL
from app.data.major_risk_index.gd_gongdian_shigongaq_single import GLV
from app.data.major_risk_index.gd_gongdian_shigongaq_single.common import get_vitual_major_ids
from app.data.major_risk_index.gd_gongdian_shigongaq_single.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_shigongaq.problem_rectification_sql import \
    OVERDUE_PROBLEM_NUMBER_SQL, \
    MAJOR_PROBLEM_POINT_INFO_SQL
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import update_major_maintype_weight, pd_query

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid('工务-1')
    ids = get_vitual_major_ids("工电-1")
    stats_month = get_custom_month(months_ago)
    global RISK_TYPE, CHECK_ITEM_IDS, CHECK_RISK_IDS
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, CHECK_EVALUATE_SZ_NUMBER_DATA, \
        MAJOR_PROBLEM_POINT_INFO_DATA, WORKER_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    CHECK_EVALUATE_SZ_NUMBER_DATA = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )
    month = int(stats_month[1][5:7])
    MAJOR_PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major, ids))
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(month))
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题整改(整改时效)
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=0.2)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    code_dict = {"ZG-1": 30,
                 "ZG-2": 30,
                 "ZG-3": 35,
                 "ZG-4": 20,
                 "ZG-5": 10}
    return problem_rectification.stats_check_evaluate_by_codetype(
        CHECK_EVALUATE_SZ_NUMBER_DATA, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, code_dict=code_dict)


# 问题控制
def _stats_repeatedly_index(months_ago):
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20
    }
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    '''
    指标名称 | 计算方法 | 缓存Key
    整改指数 _stats_rectification_overdue SCORE_a
    问题控制 _stats_repeatedly_index SCORE_c
    整改成效 _stats_repeatedly_effect SCORE_f
    '''
    child_index_func = [
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index,

    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.5, 0.2, 0.3]
    update_major_maintype_weight(index_type=11, major=risk_type,
                                 main_type=6, child_index_list=[1, 2, 3], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
