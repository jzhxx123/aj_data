#!/usr/bin/python3
# -*- coding: utf-8 -*-

# 较大及以上问题项点数
RISK_ABOVE_PROBLEM_POINT_COUNT_SQL = """
select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base_risk as c on a.FK_PROBLEM_BASE_ID = c.FK_PROBLEM_BASE_ID
        inner join
        t_check_info AS cif on a.FK_CHECK_INFO_ID = cif.PK_ID
        INNER JOIN
        t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
            AND cif.CHECK_WAY NOT BETWEEN 5 AND 6
            AND cif.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND cif.CHECK_TYPE NOT IN (102, 103) 
            AND a.FK_CHECK_ITEM_ID in ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
"""

# 基础问题库中较大及以上风险项点问题数
EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
            AND a.FK_CHECK_ITEM_ID IN ({1})
    GROUP BY a.PK_ID
"""