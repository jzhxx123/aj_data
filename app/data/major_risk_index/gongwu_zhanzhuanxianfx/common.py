#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import pandas as pd
from app.data.util import pd_query
import datetime
from app.data.index.common import df_merge_with_dpid


# 如果数据有缺失，按站段补全数据
def data_complete_by_condition(raw_df, filled_data, columns, default, merged_data):
    """[补全数据，填充默认值]
    1.数据全部缺失补全站段
    2.数据部分缺失，找出缺失站段补全
    3.数据完整不处理
    Arguments:
        raw_df {[type]} -- [description]
        filled_data {[type]} -- [description]
        default {[type]} -- [description]
    """

    if raw_df.empty is True:
        raw_df = filled_data[['DEPARTMENT_ID']]
        raw_df.loc[:, 'COUNT'] = 0
        for idx, column in enumerate(columns):
            raw_df.loc[:, column] = default[idx]
        raw_df.rename(
            columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
        raw_df = df_merge_with_dpid(raw_df, merged_data)
    else:
        data = df_merge_with_dpid(raw_df, merged_data)
        raw_type3_set = set(data['TYPE3'].values.tolist())
        real_type3_set = set(filled_data['DEPARTMENT_ID'].values.tolist())
        subset = real_type3_set - raw_type3_set
        if subset:
            init_data = [[data] for data in subset]
            sub_df = pd.DataFrame(data=init_data, columns=['FK_DEPARTMENT_ID'])
            sub_df.loc[:, 'COUNT'] = 0
            sub_df = df_merge_with_dpid(sub_df, merged_data)
            for idx, column in enumerate(columns):
                sub_df.loc[:, column] = default[idx]
            raw_df = raw_df.append(sub_df)
        else:
            pass
    return raw_df


def retrieve_all_check_point_dpids(major):
    """
    获取所有符合重要生产场所-站线、大型站场、专用线、支线的班组
    {'站线':, '大型站场':, '专用线':, '支线':}
    """
    # 查询所属的子check_item的PK_ID
    raw_sql = """
    SELECT 
        a.PK_ID
    FROM
        t_check_point as a
            inner join
        t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.name IN ('站线' , '大型站场', '专用线', '支线')
        and a.IS_DELETE = 0
        and a.type = 1
        and b.is_delete = 0
        and b.TYPE2 = '{0}'
        """
    top_ids = pd_query(raw_sql.format(major))
    top_ids_list = top_ids['PK_ID'].values.tolist()
    top_ids = ','.join([str(i) for i in top_ids_list])
    load_child_check_point_sql = "SELECT PK_ID FROM t_check_point WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_point_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break
    
    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret