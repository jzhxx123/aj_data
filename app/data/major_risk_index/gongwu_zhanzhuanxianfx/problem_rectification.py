# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
from flask import current_app
import pandas as pd
from app.data.major_risk_index.gongwu_zhanzhuanxianfx import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL)
from app.data.major_risk_index.gongwu_zhanzhuanxianfx.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL)
from app.data.index.util import (
    get_query_condition_by_risktype, get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.gongwu_zhanzhuanxianfx.common import data_complete_by_condition
from app.data.major_risk_index.common.common import get_major_dpid
SCORE = []
HIERARCHY = [3]


def _calc_check_evaluate_score(row, code_dict):
    score = 0
    for code in code_dict:
        score += int(row[code]) * int(code_dict[code])
    return min(100, score)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, CHECK_POINT_IDS
    RISK_TYPE = risk_type
    major = get_major_dpid(risk_type)
    CHECK_POINT_IDS = GLV.get_value("CHECK_POINT_IDS")
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA
    stats_months = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]

    # 防控责任详细信息filled_data, columns, default, merged_data
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_months, CHECK_RISK_IDS, CHECK_ITEM_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]

    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        if int(row['COUNT_A']) >= 1:
            _score = 100 if int(row['COUNT_A']) >=2 else 50
    elif int(row['MAIN_TYPE']) == 2:
        if int(row['COUNT_A']) >= 1:
            if int(row['COUNT_A']) == 1:
                _score = 25
            elif int(row['COUNT_A']) == 2:
                _score = 50
            elif int(row['COUNT_A']) == 3:
                _score = 75
            else:
                _score = 100
    else:
        if int(row['COUNT_A']) >= 1:
            if int(row['COUNT_A']) == 1:
                _score = 10
            elif int(row['COUNT_A']) == 2:
                _score = 30
            else:
                _score = 100 
    return _score


def get_year_month(months_ago):
    from app.data.util import (get_history_months, get_previous_month)
    year_mon = get_history_months(months_ago)[0]
    last_month = get_previous_month(months_ago)
    return year_mon, last_month


# 问题整改
def _stats_rectification_overdue(months_ago):
    """
    问题超期整改扣分：问题整改超期1条扣2分；
    :param months_ago:
    :return:
    """
    year_mon, last_month = get_year_month(months_ago)
    overdue_problem_number_sql = OVERDUE_PROBLEM_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                              CHECK_RISK_IDS, CHECK_ITEM_IDS, CHECK_POINT_IDS)
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, overdue_problem_number_sql, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=2)


def _stats_recification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a
    } 
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data, df_dict,
        calc_score_by_formula=lambda x: max(0, 100 - x), 
        rectification_type_calc_func_dict=rectification_type_calc_func_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_recification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'f']]
    item_weight = [0.4, 0.6]
    update_major_maintype_weight(index_type=10, major=risk_type, main_type=6,
                                 child_index_list=[1, 6],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
