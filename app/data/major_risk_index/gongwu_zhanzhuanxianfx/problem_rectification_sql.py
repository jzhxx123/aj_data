# -*- coding: utf-8 -*-
# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_risk AS c ON c.FK_CHECK_PROBLEM_ID = a.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as d on c.FK_CHECK_PROBLEM_ID = d.PK_ID
        inner join
    t_check_info_and_address AS tciaa on d.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_RISK_ID IN ({2})
            AND d.FK_CHECK_ITEM_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 责任安全信息
DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
        INNER JOIN
        t_safety_produce_info_problem_base AS d
            ON d.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
        INNER JOIN
        t_problem_base AS e ON d.FK_PROBLEM_BASE_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
        AND e.FK_CHECK_ITEM_ID IN ({3})
"""