#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gongwu_zhanzhuanxianfx import GLV
from app.data.major_risk_index.gongwu_zhanzhuanxianfx.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL
)
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month, get_query_condition_by_risktype)
import pandas as pd
from app.data.major_risk_index.gongwu_zhanzhuanxianfx.common import retrieve_all_check_point_dpids


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    check_point_ids = retrieve_all_check_point_dpids(major)
    
    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))


    # 站专线总人时 TODO 暂时用总人数代替
    ZHANZHUANXIAN_LABORTIME = pd_query(WORK_LOAD_SQL.format(major))

    # 目前根据文档写死工作量（成都工务大机段，成都工务大修段，成都桥路大修段不参与计算）
    # ZHANZHUANXIAN_WORK_LOAD = df_merge_with_dpid(
    #     pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)), DEPARTMENT_DATA)
    zhanzhuanxian_work_load_list = [
        # 遂宁工务段, 涪陵工务段
        ['19B8C3534E065665E0539106C00A58FD', 606.815], ['19B8C3534E085665E0539106C00A58FD', 681.187],
        # 内江工务段, 达州工务段
        ['19B8C3534E0D5665E0539106C00A58FD', 1217.269], ['19B8C3534E0F5665E0539106C00A58FD', 1157.89],
        # 成都工务段, 成都工务大机段
        ['19B8C3534E135665E0539106C00A58FD', 2891.688],
        # ['19B8C3534E145665E0539106C00A58FD', 86.761],
        # 贵阳工务段, 成都工务大修段
        ['19B8C3534E1D5665E0539106C00A58FD', 2845.841],
        # ['19B8C3534E255665E0539106C00A58FD', 41.39],
        # 重庆工务段, 绵阳工务段
        ['19B8C3534E275665E0539106C00A58FD', 2592.16], ['19B8C3534E2D5665E0539106C00A58FD', 1527.81],
        # 凯里工务段, 贵阳高铁工务段
        ['19B8C3534E395665E0539106C00A58FD', 726.376], ['19B8C3534E505665E0539106C00A58FD', 826.104],
        # 成都高铁工务段, 重庆工电段
        ['19B9D8D920D8589FE0539106C00A1189', 649.827], ['99990002001499A10010', 811.784],
        # 宜宾工电段, 六盘水工电段
        ['99990002001499A10012', 646.93], ['99990002001499A10013', 1499.024],
        # 成都桥路大修段, 西昌工电段
        # ['99990002001499A10014', 2],
        ['99990002001499A10015', 1501.069],
    ]
    ZHANZHUANXIAN_WORK_LOAD = pd.DataFrame(data=zhanzhuanxian_work_load_list, columns=['FK_DEPARTMENT_ID', 'COUNT'])
    ZHANZHUANXIAN_WORK_LOAD = df_merge_with_dpid(
        ZHANZHUANXIAN_WORK_LOAD,
        DEPARTMENT_DATA
    )
    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "ZHANZHUANXIAN_WORK_LOAD": ZHANZHUANXIAN_WORK_LOAD,
        "ZHANZHUANXIAN_LABORTIME": ZHANZHUANXIAN_LABORTIME,
        "CHECK_POINT_IDS": check_point_ids,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
