#! /usr/bin/env python3
# -*- encoding:utf-8 -*-

from app.utils.decorator import record_func_runtime
from app.data.major_risk_index.gongwu_zhanzhuanxianfx import (
    assess_intensity, check_evenness, check_intensity,
    problem_exposure, problem_rectification, init_common_data,
    GLV, _calc_cardinal_number)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.gongwu_zhanzhuanxianfx.common_sql import (CHEJIAN_DPID_SQL,
                                                                 ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight, init_monthly_index_map


@validate_exec_month
def execute(months_ago):
    risk_name = 159
    risk_type = '工务-10'
    # init_monthly_index_map(months_ago, risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    _calc_cardinal_number.get_cardinal_number(months_ago, risk_name, risk_type)
    for func in [
        check_intensity, 
        assess_intensity,
        check_evenness, 
        problem_exposure, 
        problem_rectification,
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    child_index_weight = [0.35, 0.10, 0.10, 0.20, 0.25]
    update_major_maintype_weight(index_type=10, major=risk_type,
                                 child_index_list=[1, 3, 4, 5, 6],
                                 child_index_weight=child_index_weight)
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_list=[1, 3, 4, 5, 6],
        child_index_weight=child_index_weight)

    # 清除本模块的共享数据
    GLV.rm_all_values()


if __name__ == '__main__':
    pass
