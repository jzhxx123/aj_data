#!/usr/bin/python3
# -*- coding: utf-8 -*-
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    BANZU_POINT_SQL, WORK_BANZU_INFO_SQL)

# （检查项目、风险类型必须同时具备才算）
CHECK_COUNT_SQL = """
select d.FK_DEPARTMENT_ID, COUNT(1) as COUNT from
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_item as c on b.PK_ID = c.FK_CHECK_INFO_ID
    inner join
    t_check_info_and_risk as cr on b.PK_ID = cr.FK_CHECK_INFO_ID
    inner join
    t_check_info_and_address AS tciaa on b.PK_ID = tciaa.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY BETWEEN 1 AND 2
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_ITEM_ID in ({2})
    AND cr.FK_RISK_ID in ({3})
    AND tciaa.TYPE = 2
    AND tciaa.FK_CHECK_POINT_ID IN ({4}) 
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf
inner join
t_check_info_and_person as d on qf.PK_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 所有问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
        INNER JOIN
    t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND tciaa.TYPE = 2
        AND tciaa.FK_CHECK_POINT_ID IN ({4}) 
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
        inner join
        t_check_info_and_address AS tciaa on b.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            group by b.pk_id
"""

YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.FK_DEPARTMENT_ID,a.PK_ID,b.ID_CARD) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
            INNER JOIN
        t_check_info_and_risk AS cr on a.PK_ID = cr.FK_CHECK_INFO_ID
            inner join
        t_check_info_and_address AS tciaa on a.PK_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND cr.FK_RISK_ID in ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
    GROUP BY b.FK_DEPARTMENT_ID;
    """

# 较大及以上风险问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """
select rlp.FK_DEPARTMENT_ID, sum(rlp.CHECK_SCORE) as COUNT
from
(
SELECT
        distinct(b.pk_id), a.FK_DEPARTMENT_ID, d.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join 
        t_person as a on b.CHECK_PERSON_ID_CARD = a.ID_CARD
        left join
        t_check_problem_and_risk as c
        ON  b.PK_ID = c.FK_CHECK_PROBLEM_ID
        left join
        t_problem_base as d 
        on b.FK_PROBLEM_BASE_ID = d.pk_id
        inner join
        t_check_info as e on b.FK_CHECK_INFO_ID = e.PK_ID
        INNER JOIN
        t_check_info_and_address AS tciaa on b.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            ) as rlp
    GROUP BY rlp.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_person as d ON b.CHECK_PERSON_ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_check_problem_and_risk as a on a.fk_check_problem_id = b.pk_id
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
        INNER JOIN
        t_check_info_and_address AS tciaa on b.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND a.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 监控调阅检查时间
MEDIA_COST_TIME_SQL = """
select d.FK_DEPARTMENT_ID, sum(a.COST_TIME) as TIME from
t_check_info_and_media as a
inner join 
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_item as c on b.PK_ID = c.FK_CHECK_INFO_ID
    inner join
    t_check_problem as tcp on b.PK_ID = tcp.FK_CHECK_INFO_ID
    inner join
    t_check_problem_and_risk as tcpar on tcp.pk_id =tcpar.FK_CHECK_PROBLEM_ID
    INNER JOIN
    t_check_info_and_address AS tciaa on b.PK_ID = tciaa.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND tcpar.FK_RISK_ID IN ({3})
    AND tciaa.TYPE = 2
    AND tciaa.FK_CHECK_POINT_ID IN ({4})
) as qf on a.FK_CHECK_INFO_ID = qf.PK_ID
inner join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
        INNER join
    t_check_problem_and_risk as d on a.PK_ID = d.FK_CHECK_PROBLEM_ID
        inner join
    t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY between 3 and 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID in ({2})
            AND a.FK_CHECK_ITEM_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            group by a.pk_id
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            left join
        t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
            inner join
        t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY between 3 and 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND f.FK_RISK_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_CHECK_ITEM_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
    GROUP BY a.PK_ID
"""

# 一般及以上问题质量分累计
ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
        inner join
        t_check_info_and_address AS tciaa on b.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 一般及以上所有问题数
ABOVE_YIBAN_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
        INNER JOIN
    t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND tciaa.TYPE = 2
        AND tciaa.FK_CHECK_POINT_ID IN ({4}) 
        AND a.RISK_LEVEL <= 3
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""