#!/usr/bin/python3
# -*- coding: utf-8 -*-

# 月度考核总金额(关联项目)
NORISK_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            inner join 
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
    t_check_info_and_address AS tciaa on c.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
            inner join 
    t_check_info as tci on c.FK_CHECK_INFO_ID = tci.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.FK_RISK_ID IN ({3})
        AND tciaa.TYPE = 2
        AND tciaa.FK_CHECK_POINT_ID IN ({4})
        AND tci.CHECK_WAY NOT BETWEEN 5 AND 6
        AND tci.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND tci.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 月度返奖金额(关联项目)
LEVEL_AWARD_RETURN_SQL = """
SELECT
        distinct a.PK_ID, b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
        inner join 
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_info_and_address AS tciaa on c.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
            inner join 
        t_check_info as tci on c.FK_CHECK_INFO_ID = tci.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND d.FK_RISK_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            AND tci.CHECK_WAY NOT BETWEEN 5 AND 6
            AND tci.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND tci.CHECK_TYPE NOT IN (102, 103)
"""

# 考核问题数（基本问题库）
KAOHE_PROBLEM_BASE_SQL = """
    SELECT 
    max(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_info as e on a.FK_CHECK_INFO_ID = e.PK_ID
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_risk AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department as d on a.pk_id = d.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_info_and_address AS tciaa on a.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
WHERE
    (b.IS_PROFESSION_ASSESS = 1
        OR b.IS_UNIT_ASSESS = 1)
    AND e.CHECK_WAY NOT BETWEEN 5 AND 6
    AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND e.CHECK_TYPE NOT IN (102, 103)
    AND a.IS_EXTERNAL = 0
    AND
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND c.FK_RISK_ID in ({2})
    AND a.FK_CHECK_ITEM_ID IN ({3})
    AND tciaa.TYPE = 2
    AND tciaa.FK_CHECK_POINT_ID IN ({4})
    GROUP BY a.PK_ID
"""

# 月度返奖问题数
AWARD_RETURN_PROBLEM_INFO_SQL = """
SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 AS COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
        inner join 
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_info_and_address AS tciaa on c.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
            inner join 
        t_check_info as tci on tci.PK_ID= c.FK_CHECK_INFO_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND d.FK_RISK_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            AND a.LEVEL in {5}
            AND tci.CHECK_WAY NOT BETWEEN 5 AND 6
            AND tci.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND tci.CHECK_TYPE NOT IN (102, 103)
            group by a.FK_CHECK_PROBLEM_ID
"""

# 实际月度返奖问题
REAL_AWARD_RETURN_MONEY_INFO_SQL = """
SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 AS COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
        inner join 
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_info_and_address AS tciaa on c.FK_CHECK_INFO_ID = tciaa.FK_CHECK_INFO_ID
            inner join 
        t_check_info as tci on tci.PK_ID= c.FK_CHECK_INFO_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND a.ACTUAL_MONEY > 0
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND d.FK_RISK_ID IN ({3})
            AND tciaa.TYPE = 2
            AND tciaa.FK_CHECK_POINT_ID IN ({4})
            AND a.LEVEL in {5}
            AND tci.CHECK_WAY NOT BETWEEN 5 AND 6
            AND tci.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND tci.CHECK_TYPE NOT IN (102, 103)
            group by a.FK_CHECK_PROBLEM_ID
"""