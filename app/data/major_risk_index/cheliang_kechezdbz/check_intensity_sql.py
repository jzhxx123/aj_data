# 问题质量分累计(量化人员及干部)
PROBLEM_CHECK_SCORE_SQL = """SELECT
    EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
FROM
    t_check_problem AS a
        LEFT JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 考核作业项问题数
KAOHE_PROBLEM_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 4 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 一般及以上风险问题数
YIBAN_RISK_PROBLEM_NUMBER_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 3
        AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 一般及以上风险问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY = 3
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY = 3
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%'
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""