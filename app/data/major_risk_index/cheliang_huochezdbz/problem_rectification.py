# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.cheliang_huochezdbz.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.common.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, CHECKED_PERIL_ID_SQL,
    HAPPEN_PROBLEM_POINT_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL, PERIL_COUNT_SQL, PERIL_ID_SQL,
    PERIL_OVERDUE_COUNT_SQL, PERIL_PERIOD_COUNT_SQL,
    PERIL_RECTIFY_NO_ENTRY_SQL, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, \
        STAFF_NUMBER

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    risk_position = ','.join(
        [f'"{postion}"' for postion in risktype_data[2].split(',')])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(risk_position, major)), DEPARTMENT_DATA)

    WORK_LOAD = STAFF_NUMBER.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_ITEM_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def _stats_repeatedly_index(months_ago):
    return problem_rectification.stats_repeatedly_index(
        CHECK_ITEM_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 整改复查
def _stats_rectification_review(months_ago):
    return problem_rectification.stats_rectification_review(
        CHECK_ITEM_IDS, STAFF_NUMBER, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 隐患整治
def _stats_peril_renovation(months_ago):
    return problem_rectification.stats_peril_renovation(
        PERIL_COUNT_SQL, PERIL_OVERDUE_COUNT_SQL, PERIL_PERIOD_COUNT_SQL,
        CHECKED_PERIL_ID_SQL, PERIL_ID_SQL, PERIL_RECTIFY_NO_ENTRY_SQL,
        WORK_LOAD, DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data, CHECK_RISK_IDS)


# 整改成效
def _stats_rectification_effect(months_ago):
    return problem_rectification.stats_rectification_effect(
        CHECK_ITEM_IDS, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index, _stats_rectification_review,
        _stats_peril_renovation, _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f']]
    item_weight = [0.1, 0.2, 0.3, 0.15, 0.25, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
