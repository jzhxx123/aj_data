# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''  
            AND a.TYPE3 not in ('19B8C3534E255665E0539106C00A58FD',
                 '19B8C3534E145665E0539106C00A58FD',
                 '99990002001499A10014')
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
            AND a.TYPE3 not in ('19B8C3534E255665E0539106C00A58FD',
                 '19B8C3534E145665E0539106C00A58FD',
                 '99990002001499A10014')
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'  
            AND a.TYPE3 not in ('19B8C3534E255665E0539106C00A58FD',
                 '19B8C3534E145665E0539106C00A58FD',
                 '99990002001499A10014')

"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 防断工作量
FANGDUAN_WORK_LOAD_INFO_SQL = """
SELECT 
    c.PLAN_ID,
    d.SUM AS COUNT,
    IF(LOCATE('焊缝探伤', d.WORKLOAD_NAME) != 0,
        1000,
        1) AS UNIT
FROM
    gw_t_ps_dailyplan_item AS c
        INNER JOIN
    gw_t_ps_dailyplan_item_workload AS d ON d.PLANITEM_ID = c.PK_ID
WHERE
    (d.WORKLOAD_NAME LIKE '%%焊缝探伤%%'
        OR d.WORKLOAD_NAME LIKE '%%钢轨探伤%%'
        OR d.WORKLOAD_NAME LIKE '%%线路设备仪器检查%%'
        OR d.WORKLOAD_NAME LIKE '%%线路设备手工检查%%')
"""
# d.WORKLOAD_NAME like '%%焊缝探伤%%' OR

# 防断工作量计划
FANGDUAN_WORK_LOAD_PLAN_SQL = """
SELECT 
    a.DEPARTID AS FK_DEPARTMENT_ID, a.PK_ID AS PLAN_ID
FROM
    gw_t_ps_dailyplan AS a
        INNER JOIN
    gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID
WHERE
    DATE_FORMAT(b.END_TIMES, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.END_TIMES, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.DISPATCH_ORDER_STATUS = 1
"""

# 评价记分总分数
ACTIVE_EVALUATE_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""