# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/7/6下午3:05
   Change Activity: 2019/7/6下午3:05
-------------------------------------------------
"""
from app.data.util import pd_query

HIERARCHY = [3]


def calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 90 + _ratio * 66
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 160
    else:
        _score = 71 + _ratio * 50
    if _score < 0:
        _score = 0
    return _score


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    FK_PROFESSION_DICTIONARY_ID = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(major, 2140)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(
        GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())
