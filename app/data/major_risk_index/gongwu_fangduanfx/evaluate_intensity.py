#!/usr/bin/python3
# -*- coding: utf-8 -*-
from app.data.major_risk_index.gongwu_fangduanfx.common import get_vitual_major_ids
from app.data.major_risk_index.gongwu_fangduanfx.common_sql import (
    WORK_LOAD_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index,
    calc_child_index_type_divide_major)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongwu_fangduanfx import GLV
from app.data.major_risk_index.gongwu_fangduanfx.common_sql import \
    ACTIVE_EVALUATE_SCORE_SQL
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, FANGDUAN_WORK_LOAD
    ids = get_vitual_major_ids("工电-1")
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 防断工作量
    FANGDUAN_WORK_LOAD = df_merge_with_dpid(
        GLV.get_value('FANGDUAN_WORK_LOAD'),
        DEPARTMENT_DATA)

    # 干部主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = _ratio * 66 + 90
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 160 + 80
    else:
        _score = _ratio * 50 + 71
        _score = 0 if _score < 0 else _score
    return _score


def _stats_active_score_per_person(months_ago):
    fraction = GLV.get_value('stats_active_score_per_person', (None,))[0]
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>人均主动评价记分分数({3}) = " + \
                       "主动评价记分分数({4})/ 工作量({5})</p>"
    return calc_child_index_type_divide_major(
        ACTIVE_EVALUATE_SCORE,
        FANGDUAN_WORK_LOAD,
        2,
        2,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        fraction=fraction,
        customizecontent=customizecontent)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [_stats_active_score_per_person]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['c']]
    item_weight = [1]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=2,
                                 child_index_list=[3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd']]
    # item_weight = [0.4, 0.4, 0.2]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 2, months_ago,
    #                      item_name, item_weight, [4])
    # current_app.logger.debug(
    #     '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
