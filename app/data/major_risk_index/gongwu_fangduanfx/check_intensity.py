# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gongwu_fangduanfx import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.gongwu_fangduanfx.common import (
    calc_score_by_formula)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
    YECHA_CHECK_SQL, RISK_LEVEL_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME, CHECK_RISK_IDS, CHECK_ITEM_IDS
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, PROBLEM_COUNT, \
        FANGDUAN_WORK_LOAD
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    CHECK_RISK_IDS = diaoche[1]

    # 防断工作量
    FANGDUAN_WORK_LOAD = df_merge_with_dpid(
        GLV.get_value('FANGDUAN_WORK_LOAD'),
        DEPARTMENT_DATA)

    # 量化人员及干部检查
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        FANGDUAN_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=calc_score_by_formula,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    fraction = GLV.get_value('stats_check_problem_ratio', (None,))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        PROBLEM_COUNT,
        FANGDUAN_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=calc_score_by_formula,
        fraction=fraction)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    title = [
        '较大和重大安全风险问题质量分累计({0})/问题数({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/问题数({1})'
        ]
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=calc_score_by_formula,
        title=title,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = " +\
                        "问题质量分累计({4})/ 问题数({5})</p>"
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=calc_score_by_formula,
        customizecontent=customizecontent,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0],
        RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(CHECK_ITEM_IDS)
    ]
    title=['监控调阅时长累计({0})/工作量({1})',
            '监控调阅发现问题数({0})/工作量({1})', 
            '监控调阅发现问题质量分累计({0})/工作量({1})',
            '调阅班组数({0})/班组数({1})']
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1], 
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        FANGDUAN_WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql, 
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        choose_dpid_data=_choose_dpid_data,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        calc_score_by_formula=calc_score_by_formula,
        title=title,
        fraction_list=fraction_list)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_person,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j']
    ]
    item_weight = [0.25, 0.25, 0.25, 0.1, 0.15]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 6, 10],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
