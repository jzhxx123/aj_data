# -*- coding: utf-8 -*-
"""
工务防断风险，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)
import pandas as pd
from app.data.major_risk_index.gongwu_fangduanfx import GLV
from app.data.util import (
    pd_query, )
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)
from app.data.major_risk_index.common.common_sql import (
    BASE_UNIT_INFO_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongwu_fangduanfx.check_intensity_sql import (
    CHECK_COUNT_SQL,
    MEDIA_COST_TIME_SQL,
    ALL_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL, PROBLEM_CHECK_SCORE_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, RISK_LEVEL_PROBLEM_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL)
from app.data.major_risk_index.gongwu_fangduanfx.common_sql import (
    WORK_LOAD_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    FANGDUAN_WORK_LOAD_PLAN_SQL, FANGDUAN_WORK_LOAD_INFO_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    ALL_PROBLEM_NUMBER_INFO,
    ACTIVE_EVALUATE_SCORE_INFO,
    WORKER_LOAD_INFO,
    )


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        CHECK_ITEM_IDS, RISK_IDS,\
        YEJIAN_WORK_BANZU_COUNT, WORK_LOAD_DATA
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    CALC_MONTH = END, START
    IDS = get_major_dpid(risk_type)
    # 单位职工人数
    WORK_LOAD_DATA = GLV.get_value('FANGDUAN_WORK_LOAD').copy()


# ------------------------获取比值型相应基数------------------------ #
# 比值型指数（main_type-detail_type)


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, IDS)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总次数（关联项目）'
    CHECK_COUNT.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 公共部分

    # 工作量
    WORK_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    WORK_LOAD_COUNT.value = [FANGDUAN_WORK_LOAD_PLAN_SQL, FANGDUAN_WORK_LOAD_INFO_SQL]
    WORK_LOAD_COUNT.func_version = 'customized_df'
    WORK_LOAD_COUNT.description = '防断工作量'
    WORK_LOAD_COUNT.func_value = _calc_fangduan_workload


    # 检查力度指数
    
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总数（关联项目)'
    CHECK_COUNT.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 较大问题质量分
    JIAODA_RISK_SCORE = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    JIAODA_RISK_SCORE.value = [
        RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    XC_JIAODA_RISK_SCORE = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    XC_JIAODA_RISK_SCORE.value = [
        XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.value = [
        MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.value = [
        MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]


    # 问题暴露度

    # 一般及以上问题数
    ABOVE_YIBAN_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ABOVE_YIBAN_PROBLEM_NUMBER.version = 'v2'
    ABOVE_YIBAN_PROBLEM_NUMBER.description = '一般及以上问题数'
    ABOVE_YIBAN_PROBLEM_NUMBER.value = [
        ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题质量分
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.version = 'v2'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.description = '一般及以上问题质量分'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.value = [
        ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.value = [
        ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 评价力度

    # 干部主动评价记分
    ACTIVE_EVALUATE_SCORE = CommonCalcDataType(*ACTIVE_EVALUATE_SCORE_INFO)
    ACTIVE_EVALUATE_SCORE.value = [
        ACTIVE_EVALUATE_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, WORK_LOAD_COUNT),),

        # 查出问题率
        '1-3': (IndexDetails(ALL_PROBLEM_NUMBER, WORK_LOAD_COUNT),),

        # 质量均分
        '1-5': (IndexDetails(PROBLEM_CHECK_SCORE, ALL_PROBLEM_NUMBER),),

        # 较大风险问题质量均分
        '1-6': (IndexDetails(JIAODA_RISK_SCORE, ALL_PROBLEM_NUMBER), IndexDetails(XC_JIAODA_RISK_SCORE, ALL_PROBLEM_NUMBER)),

        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, WORK_LOAD_COUNT), 
                IndexDetails(MEDIA_PROBLEM_NUMBER, WORK_LOAD_COUNT),
                IndexDetails(MEDIA_PROBLME_SCORE, WORK_LOAD_COUNT)),

        # 评价力度
        # 人均评价记分
        '2-3': (IndexDetails(ACTIVE_EVALUATE_SCORE, WORK_LOAD_COUNT),),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
            IndexDetails(ALL_PROBLEM_NUMBER, WORK_LOAD_COUNT),
            IndexDetails(PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_NUMBER, WORK_LOAD_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('stats_check_problem_ratio', CHILD_INDEX_SQL_DICT['1-3'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_score_per_person', CHILD_INDEX_SQL_DICT['1-5'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_active_score_per_person', CHILD_INDEX_SQL_DICT['2-3'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])


def _calc_fangduan_workload(
        _sqllist_numerator,
        zhanduan_dpid_data,
        department_data,
        mon_ago, column, dpids):
    # 防断工作量
    stats_month = get_custom_month(mon_ago)
    FANGDUAN_WORK_LOAD = pd.merge(
        pd_query(_sqllist_numerator[0].format(*stats_month), db_name='db_mid'),
        pd_query(_sqllist_numerator[1], db_name='db_mid'),
        how='inner',
        on='PLAN_ID'
    )
    if FANGDUAN_WORK_LOAD.empty:
        FANGDUAN_WORK_LOAD = department_data[['DEPARTMENT_ID']]
        FANGDUAN_WORK_LOAD['COUNT'] = 0
        FANGDUAN_WORK_LOAD.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    else:
        FANGDUAN_WORK_LOAD['COUNT'] = FANGDUAN_WORK_LOAD.apply(
            lambda row: row['COUNT'] * row['UNIT'] / 11300000, axis=1)

    data = pd.merge(
        FANGDUAN_WORK_LOAD,
        department_data,
        how='inner',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data