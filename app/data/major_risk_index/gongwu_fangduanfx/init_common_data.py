#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gongwu_fangduanfx import GLV
from app.data.major_risk_index.gongwu_fangduanfx.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL,
    FANGDUAN_WORK_LOAD_PLAN_SQL, FANGDUAN_WORK_LOAD_INFO_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month)
import pandas as pd


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    
    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # 防断工作量
    FANGDUAN_WORK_LOAD = pd.merge(
        pd_query(FANGDUAN_WORK_LOAD_PLAN_SQL.format(*stats_month), db_name='db_mid'),
        pd_query(FANGDUAN_WORK_LOAD_INFO_SQL,db_name='db_mid'),
        how='inner',
        on='PLAN_ID'
    )
    if FANGDUAN_WORK_LOAD.empty:
        FANGDUAN_WORK_LOAD = DEPARTMENT_DATA[['DEPARTMENT_ID']]
        FANGDUAN_WORK_LOAD['COUNT'] = 0
        FANGDUAN_WORK_LOAD.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    else:
        FANGDUAN_WORK_LOAD['COUNT'] = FANGDUAN_WORK_LOAD.apply(
            lambda row: row['COUNT'] * row['UNIT'] / 11300000, axis=1)

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "FANGDUAN_WORK_LOAD": FANGDUAN_WORK_LOAD,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
