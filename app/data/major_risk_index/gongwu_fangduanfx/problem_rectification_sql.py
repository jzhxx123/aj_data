#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019-06-01
-------------------------------------------------
Change Activity:2019-06-01 16:36
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL)

