# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql
   Author :       hwj
   date：          2019/7/5下午5:11
   Change Activity: 2019/7/5下午5:11
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
    YECHA_CHECK_SQL, RISK_LEVEL_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL
)
