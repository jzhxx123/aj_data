#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0
"""

# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND a.source_id in ({0})
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]

# 实际重要检查地点数
REAL_CHECK_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
            inner join
        t_check_info_and_item as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND c.TYPE = 1
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.fk_check_item_id in ({2})
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """SELECT
        distinct (if(b.type !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID)) as FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_point as a
        left join
        t_department AS b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
        AND a.HIERARCHY = 2
        AND a.type = 1
        AND b.type between 9 and 10
        and b.is_delete = 0
"""
