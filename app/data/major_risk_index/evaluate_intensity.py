#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app.data.index.common import (
    calc_child_index_type_divide, calc_child_index_type_sum,
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.common_sql import (
    CADRE_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, ANALYSIS_CENTER_ASSESS_SQL,
    DUAN_CADRE_COUNT_SQL, EVALUATE_COUNT_SQL)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, \
        ACTIVE_EVALUATE_KEZHI_COUNT, ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        ANALYSIS_CENTER_ASSESS_SCORE, DUAN_CADRE_COUNT, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(
            ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    # 各个站段的分数
    return calc_child_index_type_divide(
        ACTIVE_EVALUATE_COUNT,
        TOTAL_EVALUATE_COUNT,
        2,
        2,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        _choose_dpid_data,
        hierarchy_list=[3],
        risk_type=RISK_TYPE)


# 干部人均主动评价记分条数
def _stats_count_per_person(months_ago):
    return calc_child_index_type_divide(
        ACTIVE_EVALUATE_COUNT,
        CADRE_COUNT,
        2,
        2,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 干部人均主动评价记分分数
def _stats_score_per_person(months_ago):
    return calc_child_index_type_divide(
        ACTIVE_EVALUATE_SCORE,
        CADRE_COUNT,
        2,
        2,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    return calc_child_index_type_divide(
        ACTIVE_EVALUATE_KEZHI_COUNT,
        TOTAL_EVALUATE_COUNT,
        2,
        2,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    return calc_child_index_type_divide(
        DUAN_CADRE_COUNT,
        TOTAL_EVALUATE_COUNT,
        2,
        2,
        5,
        months_ago,
        'COUNT',
        'SCORE_e',
        _calc_score_by_formula,
        _choose_dpid_data,
        hierarchy_list=[3],
        risk_type=RISK_TYPE)


# 分析中心得分
def _stats_analysis_center_assess(months_ago):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    data = df_merge_with_dpid(
        pd_query(ANALYSIS_CENTER_ASSESS_SQL.format(year, month)),
        DEPARTMENT_DATA)
    if data.empty is True:
        return
    # GRADES_TYPE：0扣分1加分
    data['SCORE'] = data.apply(
        lambda row: row['ACTUAL_SCORE'] if (row['GRADES_TYPE'] == 1) else (-1 * row['ACTUAL_SCORE']),
        axis=1)
    data.drop(['ACTUAL_SCORE', 'GRADES_TYPE'], inplace=True, axis=1)
    return calc_child_index_type_sum(
        data,
        2,
        2,
        6,
        months_ago,
        'SCORE',
        'SCORE_f',
        lambda x: min(100, (80 + x)),
        _choose_dpid_data,
        hierarchy_list=[3],
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_count_per_person, _stats_score_per_person, _stats_active_ratio,
        _stats_gradation_ratio, _stats_duan_ratio,
        _stats_analysis_center_assess
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f']]
    item_weight = [0.4, 0.13, 0.15, 0.17, 0.1, 0.05]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd']]
    # item_weight = [0.4, 0.4, 0.2]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 2, months_ago,
    #                      item_name, item_weight, [4])
    # current_app.logger.debug(
    #     '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
