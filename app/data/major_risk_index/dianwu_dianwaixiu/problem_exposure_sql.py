# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exprosure_sql
   Author :       hwj
   date：          2019/9/30下午3:24
   Change Activity: 2019/9/30下午3:24
-------------------------------------------------
"""

# 基础问题库中问题项点数
PROBLEM_BASE_SQL = """SELECT
    DISTINCT a.FK_DEPARTMENT_ID, b.FK_RISK_ID
    FROM
        t_problem_base AS a
        INNER join 
        t_problem_base_risk as b on a.PK_ID = b.FK_PROBLEM_BASE_ID
    WHERE
        a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
"""

# 所有三级检查项目
# “维修-日常修”以及“维修-状态修”所属
# 所有三级检查项目
ALL_CHECK_ITEM_COUNT_SQL = """SELECT
    FK_DEPARTMENT_ID, COUNT(distinct PK_ID) as MAJOR_COUNT  
FROM
    `t_check_item` 
WHERE
    PARENT_ID IN (1251, 1471)
    GROUP BY FK_DEPARTMENT_ID
"""

# 检查出问题的项目
# “维修-日常修”以及“维修-状态修”所属
CHECK_ITEM_SQL = """SELECT
        DISTINCT
        f.FK_DEPARTMENT_ID,
        count(DISTINCT a.FK_CHECK_ITEM_ID) as COUNT 
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.FK_CHECK_ITEM_ID IN (
                SELECT
                    PK_ID 
                FROM
                    t_check_item 
                WHERE
                    PARENT_ID IN (1251, 1471)
                            )
            AND f.IS_DELETE = 0
            AND f.STATUS = 3
            AND f.TYPE = 3
        GROUP BY f.FK_DEPARTMENT_ID       
"""


# 关键问题数
CHECK_KEY_PROBLEM_SQL = """SELECT
    f.FK_DEPARTMENT_ID, 
    COUNT(DISTINCT g.PK_ID) as COUNT
FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
            LEFT JOIN 
        t_key_problem_lib as g on f.PK_ID = g.FK_PROBLEM_BASE_ID  
WHERE
        f.`STATUS` = 3
        AND f.IS_DELETE=0
        AND e.CHECK_WAY NOT BETWEEN 4 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        GROUP BY f.FK_DEPARTMENT_ID
"""

# 基础问题库中所有关键数
ALL_KEY_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) as COUNT
    FROM
        t_problem_base AS a
            INNER JOIN
        t_key_problem_lib as b on b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.STATUS = 3
            AND a.IS_DELETE=0
            AND a.FK_CHECK_ITEM_ID IN ({0})
        GROUP BY a.FK_DEPARTMENT_ID  
"""

# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 3
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 总问题数
ALL_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 3
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""