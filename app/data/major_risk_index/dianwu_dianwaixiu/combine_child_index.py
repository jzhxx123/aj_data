# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/10/11上午9:44
   Change Activity: 2019/10/11上午9:44
-------------------------------------------------
"""
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_dianwaixiu import (plan_management,
                                                         problem_exposure,
                                                         stare_intensity,
                                                         problem_rectification,
                                                         init_common_data)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.dianwu_dianwaixiu.common import get_vitual_major_ids
from app.data.major_risk_index.dianwu_dianwaixiu.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL

from app.data.util import update_major_maintype_weight


@validate_exec_month
def execute(months_ago):
    risk_name = 147
    risk_type = '电务-8'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        plan_management,
        stare_intensity,
        problem_exposure,
        problem_rectification,
    ]:
        func.execute(months_ago, risk_name, risk_type)
    child_index_list = [5, 6, 11, 12]
    child_index_weight = [0.2, 0.4, 0.2, 0.2]
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(
        zhanduan_dpid_sql,
        chejian_dpid_sql,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight,
        child_index_list=child_index_list,
    )

    update_major_maintype_weight(index_type=8, major=risk_type, child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )


if __name__ == '__main__':
    pass
