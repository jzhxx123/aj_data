# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     plan_management
   Author :       hwj
   date：          2019/10/9上午9:56
   Change Activity: 2019/10/9上午9:56
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_dianwaixiu import GLV
from app.data.major_risk_index.dianwu_dianwaixiu.common import calc_warning_unit_count, df_merge_with_dpid_right
from app.data.major_risk_index.dianwu_dianwaixiu.plan_management_sql import (
    POINT_REPAIR_ALL_COUNT_SQL,
    POINT_REPAIR_COUNT_SQL,
    STATUS_REPAIR_COUNT_SQL, WARNING_COUNT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index, calc_child_index_type_divide_major,
    calc_child_index_type_divide)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_job_plan_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = row[column]
    if _ratio >= 0.9:
        _score = 100 * _ratio
    else:
        _score = 0
    return _score


def _calc_hidden_disposal_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100
    elif 0.1 > _ratio >= -0.1:
        _score = 90
    else:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, POINT_REPAIR_COUNT, \
        POINT_REPAIR_ALL_COUNT, STATUS_REPAIR_COUNT, WARNING_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    # 点外修完成数量
    POINT_REPAIR_COUNT = df_merge_with_dpid_right(
        pd_query(POINT_REPAIR_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 点外修总数量
    POINT_REPAIR_ALL_COUNT = df_merge_with_dpid_right(
        pd_query(POINT_REPAIR_ALL_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 状态修条数
    STATUS_REPAIR_COUNT = df_merge_with_dpid_right(
        pd_query(STATUS_REPAIR_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 一二级报警总条数
    warning_count = pd_query(
        WARNING_COUNT_SQL.format(*stats_month), db_name='db_mid')
    WARNING_COUNT = calc_warning_unit_count(warning_count, ZHANDUAN_DPID_DATA.copy())

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 作业计划兑现率
def _stats_job_plan_ratio(months_ago):
    """
    “点外修”完成数量/“点外修”总数量。100%得100分、95%得95分以此类推。90%以下得0分
    """
    return calc_child_index_type_divide_major(
        POINT_REPAIR_COUNT,
        POINT_REPAIR_ALL_COUNT,
        2,
        11,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_job_plan_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 隐患处置计划管理指数
def _stats_hidden_disposal_plan_ratio(months_ago):
    """
    “状态修”条数/1、2级报警总数。结果与各段平均数相比，超过10%得100分，正负10%得60分，低于10%得0分
    """
    return calc_child_index_type_divide(
        STATUS_REPAIR_COUNT,
        WARNING_COUNT,
        2,
        11,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_hidden_disposal_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【作业计划兑现率，隐患处置计划管理指数】
    child_index_func = [
        _stats_job_plan_ratio,
        _stats_hidden_disposal_plan_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b']]
    item_weight = [0.7, 0.3]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        11,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=11, child_index_list=[1, 2],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── plan_management index has been figured out!')


if __name__ == '__main__':
    pass
