# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification
   Author :       hwj
   date：          2019/10/16下午7:06
   Change Activity: 2019/10/16下午7:06
-------------------------------------------------
"""

import pandas as pd
from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_dianwaixiu import GLV
from app.data.major_risk_index.dianwu_dianwaixiu.common import calc_third_level_check_item_count, \
    calc_problem_check_item_basic_data, calc_key_problem_count, calc_overdue_problem_basic_data, \
    df_merge_with_dpid_right
from app.data.major_risk_index.dianwu_dianwaixiu.problem_exposure_sql import KAOHE_PROBLEM_SQL, CHECK_ITEM_SQL, \
    CHECK_KEY_PROBLEM_SQL, ALL_KEY_PROBLEM_SQL, ALL_PROBLEM_SQL
from app.data.major_risk_index.dianwu_dianwaixiu.problem_rectification_sql import OVERDUE_PROBLEM_NUMBER_SQL, \
    HAPPEN_PROBLEM_POINT_SQL, HIGH_QUALITY_PROBLEM_SQL, RECTIFICATION_DELETE_SQL
from app.data.major_risk_index.dianwu_dianwaixiu.stare_intensity_sql import ALL_CHECK_ITEM_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index, format_export_basic_data,
    write_export_basic_data_to_mongo, summizet_operation_set, calc_child_index_type_divide_major)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio > 0.2:
        _score = 100
    elif 0.2 >= _ratio >= -0.2:
        _score = 60
    else:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        ALL_PROBLEM, HIGH_QUALITY_PROBLEM, OVERDUE_PROBLEM_NUMBER, RECTIFICATION_DELETE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    CHECK_ITEM_IDS = diaoche[0]

    # 中高问题数
    HIGH_QUALITY_PROBLEM = df_merge_with_dpid_right(
        pd_query(HIGH_QUALITY_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 总问题数
    ALL_PROBLEM = df_merge_with_dpid_right(
        pd_query(ALL_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 超期问题
    OVERDUE_PROBLEM_NUMBER = pd_query(OVERDUE_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS))

    # 整改销号把关不严数
    RECTIFICATION_DELETE = pd_query(RECTIFICATION_DELETE_SQL.format(*stats_month, RISK_IDS))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题整改(整改时效) todo 整改销号把关不严
def _stats_rectification_overdue(months_ago):
    """
    超期未整改1条问题扣1分，扣完为止；整改销号把关不严每1条评价扣3分，扣完为止
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_overdue_problem_basic_data(
        OVERDUE_PROBLEM_NUMBER, RECTIFICATION_DELETE, _choose_dpid_data, DEPARTMENT_DATA)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 6, 1, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 6, 1, risk_type=RISK_TYPE)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_a_3',
        3,
        2,
        6,
        1,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 问题控制 todo 无计算方式
def _stats_repeatedly_index(months_ago):
    return problem_rectification.stats_repeatedly_index(
        RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 中高质量问题占比 todo 何为中高质量问题
def _stats_high_quality_problem_ratio(months_ago):
    """点外修中高质量问题占比/点外修总问题数, 结果与各段平均数相比，超过20%得100分，正负20%得60分，低于20%得0分"""
    return calc_child_index_type_divide_major(
        HIGH_QUALITY_PROBLEM,
        ALL_PROBLEM,
        2,
        6,
        7,
        months_ago,
        'COUNT',
        'SCORE_g',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【问题均衡度指数，关键问题查处率指数，问题考核率指数】
    child_index_func = [
        _stats_rectification_overdue,
        _stats_repeatedly_index,
        _stats_high_quality_problem_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'g']]
    item_weight = [0.2, 0.8, 0]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=6, child_index_list=[1, 3, 7],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
