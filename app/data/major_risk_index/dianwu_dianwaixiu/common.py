# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/10/10上午8:41
   Change Activity: 2019/10/10上午8:41
-------------------------------------------------
"""
import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df, df_merge_with_dpid
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def calc_warning_unit_count(df_count, zhanduan_dpid):
    """将报警总条数需要的数据关联到站段"""

    if df_count.empty:
        zhanduan_dpid['COUNT'] = 0
        data = zhanduan_dpid
    else:
        # 车间关联站段
        df_count['NAME'] = df_count.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_count.dropna(subset=['NAME'], inplace=True)
        df_count.drop(columns=['WORK_SHOP'], inplace=True)
        df_count = df_count.groupby([df_count.NAME])['COUNT'].sum().to_frame()
        data = pd.merge(
            df_count,
            zhanduan_dpid,
            left_index=True,
            right_on='NAME',
            how='right'
        )
        data.fillna(0, inplace=True)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return data


def calc_third_level_check_item_count(df_count, dp_id):
    """
    获取检查均衡度--各个站段的三级检查项目个数
    """
    data = pd.merge(
        df_count,
        dp_id,
        left_on='FK_DEPARTMENT_ID',
        right_on='TYPE2'
    )
    data.drop(['FK_DEPARTMENT_ID'], inplace=True, axis=1)
    data = pd.merge(
        df_count.rename(columns={'MAJOR_COUNT': 'TYPE3_COUNT'}),
        data,
        how='right',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID'
    )
    data.fillna(0, inplace=True)
    data['ALL_COUNT'] = data.apply(
        lambda row: row['MAJOR_COUNT'] + row['TYPE3_COUNT'], axis=1
    )
    data = data[['DEPARTMENT_ID', 'ALL_COUNT']]
    return data


def calc_check_item_basic_data(all_check_item_data, check_item_data, choose_dpid_data):
    """
    获取盯控力度指数--检查力度指数的基础数据
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """

    data = pd.merge(
        all_check_item_data,
        check_item_data,
        how='inner',
        on='DEPARTMENT_ID'
    )

    tmp = '本站段所有三级检查项目共{0:.0f}个,检查出问题的项目{1:.0f}个'
    data['CONTENT'] = data.apply(
        lambda row: tmp.format(row['ALL_COUNT'], row['COUNT']), axis=1
    )
    data['SCORE_b_3'] = data.apply(
        lambda row: max(100 - (row['ALL_COUNT'] - row['COUNT']), 0), axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=data.TYPE3,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = data.groupby(['TYPE3'])['SCORE_b_3'].sum().to_frame()
    return calc_df_data, df_rst


def calc_stare_timely_basic_data(stare_timely, choose_dpid_data):
    """
    获取盯控力度指数--盯控及时率的基础数据
    """
    stare_timely = stare_timely.groupby('TYPE3')['COUNT'].sum().to_frame()
    data = pd.merge(
        stare_timely,
        choose_dpid_data(3),
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID'
    )
    tmp = '作业盯控开始时间-车站登记时间小于60分钟的有{0:.0f}个'
    data['CONTENT'] = data.apply(
        lambda row: tmp.format(row['COUNT']), axis=1
    )
    data['SCORE_c_3'] = data.apply(
        lambda row: max(100 - row['COUNT'], 0), axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=data.DEPARTMENT_ID,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = data.groupby(['DEPARTMENT_ID'])['SCORE_c_3'].sum().to_frame()
    return calc_df_data, df_rst


def calc_problem_check_item_basic_data(all_check_item_data, check_item_data, choose_dpid_data):
    """
    获取盯控力度指数--检查力度指数的基础数据
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """

    data = pd.merge(
        all_check_item_data,
        check_item_data,
        how='inner',
        on='DEPARTMENT_ID'
    )

    tmp = '本站段日常修以及状态修三级检查项目共{0:.0f}个,检查出问题的项目{1:.0f}个'
    data['CONTENT'] = data.apply(
        lambda row: tmp.format(row['ALL_COUNT'], row['COUNT']), axis=1
    )
    data['SCORE_k_3'] = data.apply(
        lambda row: max(100 - (row['ALL_COUNT'] - row['COUNT']), 0), axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=data.TYPE3,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = data.groupby(['TYPE3'])['SCORE_k_3'].sum().to_frame()
    return calc_df_data, df_rst


def calc_key_problem_count(zhanduan_dpid_data, all_key_problem, department_data):
    """
    获取问题暴露指数-关键问题查处率-总问题数
    """
    count = all_key_problem.COUNT[0]
    data = zhanduan_dpid_data[['DEPARTMENT_ID']].copy()
    data['COUNT'] = count
    data.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    data = df_merge_with_dpid(
        data,
        department_data)
    return data


def calc_overdue_problem_basic_data(overdue_problem_number,
                                    rectification_delete,
                                    choose_dpid_data,
                                    department_data):
    """
    获取问题整改指数--问题整改的基础数据
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    data = df_merge_with_dpid(overdue_problem_number, department_data, how='right')
    data = df_merge_with_dpid(rectification_delete, data, how='right')
    data.fillna(0, inplace=True)
    data = data.groupby(['TYPE3'])['OVERDUE_COUNT', 'DELETE_COUNT'].sum()
    tmp = '本月问题超期发生条数{0:.0f}, 整改销号把关不严条数{1:.0f}'
    data['CONTENT'] = data.apply(
        lambda row: tmp.format(row['OVERDUE_COUNT'], row['DELETE_COUNT']), axis=1
    )
    data['SCORE_a_3'] = data.apply(
        lambda row: max(100 - row['OVERDUE_COUNT'] - row['DELETE_COUNT'] * 3, 0), axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = data[['SCORE_a_3']]
    return calc_df_data, df_rst


def df_merge_with_dpid_right(data, dpid_data, how='right'):
    data = pd.merge(
        data,
        dpid_data,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    data.fillna(0, inplace=True)
    return data
