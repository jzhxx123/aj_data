# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exprosure
   Author :       hwj
   date：          2019/9/30下午3:14
   Change Activity: 2019/9/30下午3:14
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_dianwaixiu import GLV
from app.data.major_risk_index.dianwu_dianwaixiu.common import calc_third_level_check_item_count, \
    calc_problem_check_item_basic_data, calc_key_problem_count, df_merge_with_dpid_right
from app.data.major_risk_index.dianwu_dianwaixiu.problem_exposure_sql import KAOHE_PROBLEM_SQL, CHECK_ITEM_SQL, \
    CHECK_KEY_PROBLEM_SQL, ALL_KEY_PROBLEM_SQL, ALL_PROBLEM_SQL
from app.data.major_risk_index.dianwu_dianwaixiu.stare_intensity_sql import ALL_CHECK_ITEM_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index, format_export_basic_data,
    write_export_basic_data_to_mongo, summizet_operation_set, calc_child_index_type_divide_major)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = row[column]
    if _ratio > 0.9:  # todo 未定义大于0.9的
        _score = 100
    elif 0.9 >= _ratio >= 0.8:
        _score = 100
    elif 0.8 > _ratio >= 0.7:
        _score = 80
    elif 0.7 > _ratio >= 0.6:
        _score = 60
    else:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, WORK_LOAD, \
        CHECK_RISK, CHECK_KEY_PROBLEM, ALL_KEY_PROBLEM, ASSESS_PROBLEM_COUNT, ALL_PROBLEM, \
        CHECK_ITEM, ALL_CHECK_ITEM
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    CHECK_ITEM_IDS = diaoche[0]

    # 检查出问题的项目
    CHECK_ITEM = df_merge_with_dpid_right(
        pd_query(CHECK_ITEM_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 问题均衡-各站段所有的三级检查项目
    all_check_item = pd_query(ALL_CHECK_ITEM_COUNT_SQL.format(CHECK_ITEM_IDS))
    ALL_CHECK_ITEM = calc_third_level_check_item_count(all_check_item, ZHANDUAN_DPID_DATA)

    # 查处关键问题数
    CHECK_KEY_PROBLEM = df_merge_with_dpid_right(
        pd_query(CHECK_KEY_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 关键问题总数
    all_key_problem = pd_query(ALL_KEY_PROBLEM_SQL.format(CHECK_ITEM_IDS))

    ALL_KEY_PROBLEM = calc_key_problem_count(ZHANDUAN_DPID_DATA, all_key_problem, DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid_right(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 总问题数
    ALL_PROBLEM = df_merge_with_dpid_right(
        pd_query(ALL_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """
    三级目录中，1个三级目录未发现问题，扣5分，扣完为止
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_problem_check_item_basic_data(ALL_CHECK_ITEM, CHECK_ITEM, _choose_dpid_data)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 11, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 11, risk_type=RISK_TYPE)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_k_3',
        3,
        2,
        5,
        11,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 关键问题查处率指数
def _stat_check_problem_ratio(months_ago):
    """
    关键问题数/关键问题总数。按百分比得分
    """
    return calc_child_index_type_divide_major(
        CHECK_KEY_PROBLEM,
        ALL_KEY_PROBLEM,
        2,
        5,
        12,
        months_ago,
        'COUNT',
        'SCORE_l',
        lambda x: x * 100,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        is_calc_score_base_major=False)


# 问题考核率指数
def _stats_check_problem_assess_radio(months_ago):
    """
    考核率=点外修考核问题数/点外修总问题数。
    考核率达到80%-90%得100分；70%-80%得80分；60%-70%得60分；低于60%得0分
    """
    return calc_child_index_type_divide_major(
        ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM,
        2,
        5,
        13,
        months_ago,
        'COUNT',
        'SCORE_m',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【问题均衡度指数，关键问题查处率指数，问题考核率指数】
    child_index_func = [
        _stats_problem_point_evenness,
        _stat_check_problem_ratio,
        _stats_check_problem_assess_radio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['k', 'l', 'm']]
    item_weight = [0.4, 0.4, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=5, child_index_list=[11, 12, 13],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
