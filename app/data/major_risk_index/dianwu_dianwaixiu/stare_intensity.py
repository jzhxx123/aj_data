# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     stare_intensity
   Author :       hwj
   date：          2019/10/12上午8:40
   Change Activity: 2019/10/12上午8:40
-------------------------------------------------
"""

from flask import current_app
import pandas as pd
from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_dianwaixiu import GLV
from app.data.major_risk_index.dianwu_dianwaixiu.common import calc_third_level_check_item_count, \
    calc_check_item_basic_data, calc_stare_timely_basic_data, df_merge_with_dpid_right
from app.data.major_risk_index.dianwu_dianwaixiu.plan_management_sql import POINT_REPAIR_COUNT_SQL
from app.data.major_risk_index.dianwu_dianwaixiu.stare_intensity_sql import CHECK_COUNT_SQL, ALL_CHECK_ITEM_COUNT_SQL, \
    STARE_TIMELY_SQL, COMPLETE_STARE_SQL, ALL_STARE_SQL, CHECK_ITEM_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index, calc_child_index_type_divide_major,
    format_export_basic_data, write_export_basic_data_to_mongo, summizet_operation_set)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_check_intensity_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100
    elif 0.1 > _ratio >= -0.1:
        _score = 60
    else:
        _score = 0
    return _score


def _calc_stare_complete_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif 0.2 > _ratio >= -0.2:
        _score = 60
    else:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, CHECK_COUNT, \
        POINT_REPAIR_COUNT, ALL_CHECK_ITEM, CHECK_ITEM, STARE_TIMELY, \
        COMPLETE_STARE_COUNT, ALL_STARE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    # 检查力度- 检查次数
    CHECK_COUNT = df_merge_with_dpid_right(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 检查力度-点外修完成数量
    POINT_REPAIR_COUNT = df_merge_with_dpid_right(
        pd_query(POINT_REPAIR_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 检查均衡-各站段所有的三级检查项目
    all_check_item = pd_query(ALL_CHECK_ITEM_COUNT_SQL.format(CHECK_ITEM_IDS))
    ALL_CHECK_ITEM = calc_third_level_check_item_count(all_check_item, ZHANDUAN_DPID_DATA)

    # 检查均衡-检查出问题的项目
    CHECK_ITEM = df_merge_with_dpid_right(
        pd_query(CHECK_ITEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 盯控及时率
    STARE_TIMELY = df_merge_with_dpid_right(
        pd_query(STARE_TIMELY_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA, how='right')

    # 盯控完整性指数-完整盯控作业条数
    COMPLETE_STARE_COUNT = df_merge_with_dpid_right(
        pd_query(COMPLETE_STARE_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA, how='right')

    # 盯控完整性指数-总作业条数
    ALL_STARE = df_merge_with_dpid_right(
        pd_query(ALL_STARE_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA, how='right')

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 检查力度指数
def _stats_check_intensity_ratio(months_ago):
    """
    “点外修”三级目录检查次数/“点外修”作业条数，结果与各段平均数相比，超过10%得100分，正负10%得60分，低于10%得0分
    """
    return calc_child_index_type_divide_major(
        CHECK_COUNT,
        POINT_REPAIR_COUNT,
        2,
        12,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_check_intensity_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 检查均衡度
def _stat_check_evenness_ratio(months_ago):
    """
    统计“点外修”涉及的三级目录，缺1项扣1分，扣完为止
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_check_item_basic_data(ALL_CHECK_ITEM, CHECK_ITEM, _choose_dpid_data)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 12, 2, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 12, 2, risk_type=RISK_TYPE)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_b_3',
        3,
        2,
        12,
        2,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 盯控及时率指数
def _stat_stare_timely_ratio(months_ago):
    """
    盯控及时率=作业盯控开始时间-车站登记时间。结果小于60分钟的扣1分，扣完为止
    """
    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_stare_timely_basic_data(STARE_TIMELY, _choose_dpid_data)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 12, 3, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 12, 3, risk_type=RISK_TYPE)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_c_3',
        3,
        2,
        12,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 盯控完整性指数
def _stat_stare_complete_ratio(months_ago):
    """盯控完整性=完整盯控作业条数/总作业条数，结果与各段平均数相比，超过20%得100分，正负20%得60分，低于20%得0分"""
    return calc_child_index_type_divide_major(
        COMPLETE_STARE_COUNT,
        ALL_STARE,
        2,
        12,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_stare_complete_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【检查力度, 检查均衡度, 盯控及时率指数, 盯控完整性指数】
    child_index_func = [
        _stats_check_intensity_ratio,
        _stat_check_evenness_ratio,
        _stat_stare_timely_ratio,
        _stat_stare_complete_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    # todo 比例不正确,0.8+0.2+0.2+0.8
    item_weight = [0.4, 0.1, 0.1, 0.4]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        12,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=12, child_index_list=[1, 2, 3, 4],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
