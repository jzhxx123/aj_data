# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/9/30下午2:24
   Change Activity: 2019/9/30下午2:24
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import (problem_exposure, plan_management, stare_intensity, problem_rectification, combine_child_index)
