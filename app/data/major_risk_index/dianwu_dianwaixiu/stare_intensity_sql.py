# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     stare_intensity_sql
   Author :       hwj
   date：          2019/10/12上午8:46
   Change Activity: 2019/10/12上午8:46
-------------------------------------------------
"""

# 检查力度-检查次数（现场检查）
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# # 所有三级检查项目
# ALL_CHECK_ITEM_COUNT_SQL = """SELECT
#     FK_DEPARTMENT_ID, COUNT(distinct c.PK_ID) as MAJOR_COUNT
# FROM
#     `t_check_item` as c
# WHERE
#     PARENT_ID IN (
#     SELECT
#     substring_index( substring_index( a.CHECK_ITEM_IDS, ',', b.help_topic_id + 1 ), ',',- 1 ) ids
#     FROM
#     t_risk_statistics_config AS a
#     INNER JOIN
#     mysql.help_topic as b on
#     b.help_topic_id <
# ( length( a.CHECK_ITEM_IDS ) - length( REPLACE ( a.CHECK_ITEM_IDS, ',', '' ))+ 1 )
#     WHERE PK_ID = 147 )
#     AND IS_DELETE = 0
#     GROUP BY FK_DEPARTMENT_ID
# """


# 所有三级检查项目
ALL_CHECK_ITEM_COUNT_SQL = """SELECT
    a.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) as MAJOR_COUNT  
FROM
    t_check_item AS a
    INNER JOIN t_check_item AS b ON a.PARENT_ID = b.PK_ID
    WHERE 
    a.PK_ID in ({0})
    or a.PARENT_ID in ({0})
    and a.HIERARCHY=3
    GROUP BY FK_DEPARTMENT_ID
"""


# 检查出问题的项目
CHECK_ITEM_SQL = """SELECT
        DISTINCT
        f.FK_DEPARTMENT_ID,
        count(DISTINCT a.FK_CHECK_ITEM_ID) as COUNT 
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND f.IS_DELETE = 0
            AND f.STATUS = 3
            AND f.TYPE = 3
        GROUP BY f.FK_DEPARTMENT_ID       
"""



# todo 车站登记时间
# 盯控及时率
STARE_TIMELY_SQL = """SELECT
    DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT PK_ID) as COUNT
FROM
    (
    SELECT
        PK_ID,
        DEPARTMENT_NAME,
        DEPARTMENT_ID,
        START_SUBMIT_DATE,
        END_SUBMIT_DATE,
        TIMESTAMPDIFF( MINUTE, START_SUBMIT_DATE, END_SUBMIT_DATE ) AS time 
    FROM
        `dw_t_daily_maintain_select_control` 
    WHERE
        WORK_DATE >= DATE_FORMAT('{0}', '%%Y-%%m-%%d') 
        AND WORK_DATE < DATE_FORMAT('{1}', '%%Y-%%m-%%d') 
        AND `STATUS` = 2 
    ) AS a 
WHERE
    a.time < 60
    GROUP BY DEPARTMENT_ID
"""

# todo 目前取盯控结束的数据
# 完整盯控作业条数
COMPLETE_STARE_SQL = """SELECT
    DEPARTMENT_ID as FK_DEPARTMENT_ID,
    COUNT( DISTINCT PK_ID ) AS COUNT 
FROM
    `dw_t_daily_maintain_select_control` 
WHERE
        WORK_DATE >= DATE_FORMAT('{0}', '%%Y-%%m-%%d') 
        AND WORK_DATE < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND `STATUS` = 2 
GROUP BY
    DEPARTMENT_ID
"""

# todo 目前取所有的数据
# 总作业条数
ALL_STARE_SQL = """SELECT
    DEPARTMENT_ID as FK_DEPARTMENT_ID,
    COUNT( DISTINCT PK_ID ) AS COUNT 
FROM
    `dw_t_daily_maintain_select_control` 
WHERE
        WORK_DATE >= DATE_FORMAT('{0}', '%%Y-%%m-%%d') 
        AND WORK_DATE < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
GROUP BY
    DEPARTMENT_ID
"""