# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     plan_management_sql
   Author :       hwj
   date：          2019/10/9上午10:04
   Change Activity: 2019/10/9上午10:04
-------------------------------------------------
"""

# 作业计划兑现率-点外修完成数量
POINT_REPAIR_COUNT_SQL = """SELECT
 a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
FROM
(SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID
FROM
    dw_t_concentrate_overhaul_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    and STATUS=2
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID
FROM
    dw_t_cooperate_work_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    and STATUS=2
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID  
FROM
    dw_t_daily_maintain_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    and STATUS=2
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID 
FROM
    dw_t_status_update_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    and STATUS=2
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID  
FROM
    dw_t_temp_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    and STATUS=2) AS a
    GROUP BY a.DEPARTMENT_ID   
"""

# 作业计划兑现率-点外修完总数量
POINT_REPAIR_ALL_COUNT_SQL = """SELECT
 a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(1) AS COUNT
FROM
(SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID
FROM
    dw_t_concentrate_overhaul_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID
FROM
    dw_t_cooperate_work_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID  
FROM
    dw_t_daily_maintain_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID 
FROM
    dw_t_status_update_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
UNION
SELECT
    DEPARTMENT_ID, DISPATCHINGBILL_ID  
FROM
    dw_t_temp_select_control 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    ) AS a
    GROUP BY a.DEPARTMENT_ID
"""

# 状态修条数
STATUS_REPAIR_COUNT_SQL = """SELECT
    DEPARTMENT_ID AS FK_DEPARTMENT_ID,
    COUNT(DISTINCT PK_ID) AS COUNT 
FROM
    `dw_t_status_update_select_control` 
WHERE
    WORK_DATE >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND WORK_DATE < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND ORDER_CODE LIKE '%%C%%'
    GROUP BY DEPARTMENT_ID 
"""

# 一二级报警条数
WARNING_COUNT_SQL = """SELECT
    WORK_SHOP,
    COUNT( DISTINCT PK_ID ) AS COUNT 
FROM
    `t_turnout_warning_statistics` 
WHERE
    WARNING_LEVEL IN ( '一级', '二级' ) 
    AND WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d' ) 
GROUP BY
    WORK_SHOP
"""


