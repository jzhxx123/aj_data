#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from . import (assess_intensity, check_evenness, check_intensity,
               evaluate_intensity, problem_exposure, problem_rectification,
               major_index)
from app.data.index.util import validate_exec_month
from flask import current_app
from app.data.util import pd_query


def _is_exist_riskname(risk_name):
    """判断该重点风险分析配置项是否存在

    Arguments:
        risk_name {str} -- 风险配置项名称

    Returns:
        bool --
    """
    sql = """SELECT
            CHECK_ITEM_IDS, RISK_IDS, POSITION
        FROM
            t_risk_statistics_config
        WHERE
            PK_ID = {} LIMIT 1;
    """.format(risk_name)
    record = pd_query(sql)
    if record.empty:
        return False
    return True


@validate_exec_month
def execute(months_ago, risk):
    risk_name = int(risk.split('-')[0])
    risk_type = risk[risk.find('-')+1:]
    current_app.logger.debug(f'calculate major_risk_index[{risk_name}]')
    if not _is_exist_riskname(risk_name):
        current_app.logger.debug(f'{risk_name} not exists.')
        return 'OK'
    index_func = [
        check_evenness,
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        problem_exposure,
        problem_rectification,
        major_index,
    ]
    for func in index_func:
        func.execute(months_ago, risk_name, risk_type)
    return 'OK'


if __name__ == '__main__':
    pass