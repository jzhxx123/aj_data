#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    desc: common funcs
"""
from app.data.util import pd_query
from app.data.index.util import (
    get_months_from_201712, get_custom_month)
from app.data.major_risk_index.util import (
    df_merge_with_dpid, calc_child_index_type_sum, append_major_column_to_df,
    write_export_basic_data_to_mongo, format_export_basic_data)
import pandas as pd
from app.data.major_risk_index.common_diff_risk_and_item.common import (
    get_check_address_standard_data)


# 根据文档补充risk id
def risk_ids_supplement(risk_ids: str):
    risk_ids = risk_ids.split(',')
    if '18' not in risk_ids:
        risk_ids.append('18')
    risk_ids = ','.join(risk_ids)
    return risk_ids


def export_basic_data_one_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        func_html_desc,
                                        title=None,
                                        risk_type=None, 
                                        special_dpids=[],):
    data = pd.DataFrame(data=data, columns=['FK_DEPARTMENT_ID', 'MONTH'])
    data['MONTH'] = data['MONTH'].apply(lambda x: func_html_desc(x))
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'MONTH']).size()
    data = data.unstack()
    data.fillna(0, inplace=True)
    columns = data.columns.tolist()
    if title is not None:
        title += '<br/>'
    else:
        title = ''
    data['CONTENT'] = ''
    for idx, row in data.iterrows():
        content = '当前月未查出关键隐患问题'
        if row.name not in special_dpids:
            content = [f'{col}: {int(row[col])}个' for col in columns]
            content = title + '<br/>'.join(content)
        data.loc[idx,'CONTENT'] = content
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    data_rst = format_export_basic_data(data, main_type, detail_type,
                                        hierarchy, months_ago, risk_type)
    write_export_basic_data_to_mongo(data_rst, months_ago, hierarchy,
                                     main_type, detail_type, risk_type)


# 事故隐患问题暴露度
def stats_problem_exposure_excellent(check_item_ids, zhanduan_dpid,
                                     hidden_key_problem_sql,
                                     hidden_key_problem_month_sql, department_data,
                                     months_ago, risk_type, choose_dpid_data,
                                     customizededuct=None, months=None):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。
    默认只记录前3，4，5，6个月未暴露的问题， 只扣最后连续月的分数
    a问题连续3，4，5未暴露，只扣5月的分数，
    b问题连续3，4未暴露，只扣4月的分数，
    总扣分等于a+b
    p.s:当月查出事故隐患项点不到1项的直接0分
    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    Returns:
        [type] -- [description]
    """
    if not customizededuct:
        # key,values(mon, deduct)
        customizededuct = {
            3: 1,
            4: 2,
            5: 3,
            6: 4
        }
    stats_months = get_months_from_201712(months_ago, months)
    # 当月检查项点
    this_months = get_custom_month(months_ago)
    this_months_problem_base_id = set(
            pd_query(hidden_key_problem_month_sql.format(
                    this_months[0], this_months[1], check_item_ids))['PID'].values)
    # 找出当前月没有检查出项点的部门
    zhanduan_all_dpids = set(zhanduan_dpid['DEPARTMENT_ID'].values.tolist())
    this_months_zhanduan_all_dpids = set([item.split('||')[0] for item in this_months_problem_base_id])
    # 当月未查出问题的部门
    this_month_zero_zhanduan_dpids = zhanduan_all_dpids - this_months_zhanduan_all_dpids
    # 初始化一个各站段的扣分字典,  存储每个月的扣分值，最后只去最后一个连续月扣分
    deduct_score = {k: dict()
                    for k in zhanduan_all_dpids}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if idx > 0:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                # 因为查出关键隐患的sql中没有过滤不需要的站段或者部门
                if dpid not in deduct_score.keys():
                    continue
                # 问题项
                fk_problem_base_id = each_problem.split('||')[1]
                # 动态初始化字典
                if not isinstance(deduct_score[dpid].get(fk_problem_base_id,None), dict):
                    deduct_score[dpid][fk_problem_base_id] = dict()
                deduct_score[dpid][fk_problem_base_id].update(
                    {
                        idx: deduct_score.get(dpid).get(fk_problem_base_id).get(
                                idx, 0) + customizededuct.get(idx, 0)
                    }
                )
                if idx in customizededuct.keys():
                    hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type,
        special_dpids=this_month_zero_zhanduan_dpids)
    # 将扣分字典展开成标准格式
    deduct_score_list = []
    for key, values in deduct_score.items():
        # 针对每项问题只扣最后一个月分数
        # 当存在扣分字典为空时
        # 例如 {'19B8C3534E035665E0539106C00A58FD':{}},默认给0分
        _score = 0
        if bool(values.keys()):
            for pk_id, de_values in values.items():
                if not bool(de_values):
                    continue
                idx = sorted(de_values.keys())[-1]
                _score += de_values[idx]
        if key in this_month_zero_zhanduan_dpids:
            _score = 100
        deduct_score_list.append([key, _score])
    df_hidden_prob = pd.DataFrame(
        data=deduct_score_list, columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def get_vitual_major_ids(zhanduan_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    # 货车站段
    # 货车运用车间 id为131
    FK_PROFESSION_DICTIONARY_ID = {
        "货车": 2135, 
        "动车": 2133,
        "客车": 2134,
        }
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(zhanduan_type, '')
    GET_VM_MAJORS_IDS_SQL = """
    select distinct TYPE3 from t_department where locate('{0}', FK_PROFESSION_DICTIONARY_ID)
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(set(major_ids['TYPE3'].values.tolist()))