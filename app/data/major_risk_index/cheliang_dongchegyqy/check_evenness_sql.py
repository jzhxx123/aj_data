#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import(
    EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, RISK_ABOVE_PROBLEM_POINT_COUNT_SQL)


# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
            b.TYPE = 9
            AND a.WORK_TYPE IS NOT NULL
            AND a.MAIN_TYPE = 1
            AND a.SOURCE_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        LEFT JOIN
    t_department_and_info AS d ON d.FK_DEPARTMENT_ID = c.FK_DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND d.MAIN_TYPE = 1
        AND d.SOURCE_ID IN ({2})
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""