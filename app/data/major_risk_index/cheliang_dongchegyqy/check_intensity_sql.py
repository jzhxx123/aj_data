#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import(
    ALL_PROBLEM_NUMBER_SQL, CHECK_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, ZUOYE_CHECK_PROBLEM_SQL,
    GUANLI_CHECK_PROBLEM_SQL, RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    YECHA_CHECK_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLME_SCORE_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL)

# 职工总人数（数据来源为“组织结构-部门维护-配置-其他指数配置”下的“动车配属”）
WORK_LOAD_2_SQL = """SELECT
    a.DEPARTMENT_ID, IFNULL(b.MANAGEMENT_INDEX, 0) AS COUNT, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME
    FROM t_department a 
    LEFT JOIN
    t_department_other_management as b on a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID AND b.TYPE_NAME = '动车配属'
    WHERE
    a.TYPE2 = '{0}'
    AND a.TYPE = 4;
"""

# 正式员工数
STAFF_NUMBER_SQL = """select
    a.DEPARTMENT_ID, IFNULL(b.MANAGEMENT_INDEX, 0) AS COUNT, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME
    FROM t_department a 
    LEFT JOIN 
    t_department_other_management AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID AND b.TYPE_NAME = '动车作业正式工数'
    WHERE
    a.TYPE2 = '{0}'
    AND a.TYPE = 4;
"""

# 外聘员工数
ZHANDUAN_STAFF_SQL = """SELECT
 a.DEPARTMENT_ID, IFNULL(b.MANAGEMENT_INDEX, 0) AS COUNT, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME
    FROM t_department a 
    LEFT JOIN
    t_department_other_management AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID AND b.TYPE_NAME = '动车作业外聘工数'
    WHERE
    a.TYPE2 = '{0}'
    AND a.TYPE = 4;
"""

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
            LEFT JOIN
        t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0
            AND b.MAIN_TYPE = 1
            AND b.SOURCE_ID IN ({0});
"""