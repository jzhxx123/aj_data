#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    desc: 各个业务处的重点风险分析指数开发
    author: Qiangsheng
    date: 2018/09/27
'''
from app.data.major_risk_index._global_var import GlobalVar
module = __package__
GLV = GlobalVar(module)

from . import (assess_intensity, check_evenness, check_intensity,
               problem_exposure, problem_rectification, combine_child_index, evaluate_intensity, common)
