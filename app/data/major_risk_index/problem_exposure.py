# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.index.common import (
    calc_child_index_type_sum, calc_extra_child_score_groupby_major,
    combine_child_index_func, df_merge_with_dpid, summizet_child_index,
    summizet_operation_set)
from app.data.major_risk_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.problem_exposure_sql import (
    CHECK_PROBLEM_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
    HIDDEN_KEY_PROBLEM_MONTH_SQL, HIDDEN_KEY_PROBLEM_SQL,
    OTHER_CHECK_PROBLEM_SQL, SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.index.util import (get_custom_month, get_months_from_201712,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    diaoche = get_query_condition_by_risktype(risk_name)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = diaoche[0]


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 6 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_value_per_person(series, weight, hierarchy):
    global STAFF_NUMBER
    data = pd.concat([series.to_frame(name='prob'), STAFF_NUMBER], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    return calc_extra_child_score_groupby_major(data, _choose_dpid_data(hierarchy),
                                    'ratio', _calc_score_by_formula, weight)


def _calc_prob_number_per_person(df_data, weight, hierarchy):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, weight, hierarchy)


def _calc_prob_score_per_person(df_data, weight, hierarchy):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby(
        [f'TYPE{hierarchy}'])['PROBLEM_SCORE'].sum()
    return _calc_value_per_person(prob_score, weight, hierarchy)


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        CHECK_PROBLEM_SQL.format(CHECK_ITEM_IDS, *stats_month))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    weight_item = [0.3, 0.3, 0.2, 0.2]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(func(data.copy(), weight, hierarchy))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=RISK_TYPE)
        rst_child_score.append(df_rst)
    return rst_child_score


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712(months_ago)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    }
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(HIDDEN_KEY_PROBLEM_SQL.format(CHECK_ITEM_IDS))['PID'].values)
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                HIDDEN_KEY_PROBLEM_MONTH_SQL.format(CHECK_ITEM_IDS, mon[0],
                                                    mon[1]))['PID'].values)
        if idx > 2:
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            exposure_problem = hidden_problem.intersection(
                month_hidden_problem)
            for each_problem in exposure_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
        hidden_problem = hidden_problem.difference(month_hidden_problem)
        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 1
                })
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_months = get_months_from_201712(months_ago)
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(DEPARTMENT_DATA[(DEPARTMENT_DATA['TYPE'] == 9)
                                       | (DEPARTMENT_DATA['TYPE'] == 10)][
                                           'DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    for idx, mon in enumerate(stats_months):
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            pd_query(
                EXPOSURE_PROBLEM_DEPARTMENT_SQL.format(
                    CHECK_ITEM_IDS, mon[0],
                    mon[1]))['FK_DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        if idx > 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx + 1
                })
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        # 一直到初始月份未暴露问题的班组
        if idx == (len(stats_months) - 1):
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx + 2
                })
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        3,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(SELF_CHECK_PROBLEM_SQL.format(
            CHECK_ITEM_IDS, *calc_month))['PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(
        SAFETY_PRODUCE_INFO_SQL.format(CHECK_ITEM_IDS, *calc_month))
    other_check_problem = set(
        pd_query(OTHER_CHECK_PROBLEM_SQL.format(
            CHECK_ITEM_IDS, *calc_month))['PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    }
    problem_risk_score = {
        '1': 5,
        '2': 3,
        '3': 1,
    }
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[1]
        problem_score = problem_risk_score.get(each_problem[2], 0)
        if each_problem[1] in deduct_score:
            deduct_score.update({
                each_problem[1]:
                deduct_score.get(each_problem[1]) + problem_score
            })
    # 未自查出安全生产信息问题
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            problem_score = problem_risk_score.get(
                row['PROBLEM_DPID_RISK'].split('||')[2],
                0) * (4 - int(row['MAIN_TYPE']))
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
                })
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        5,
        4,
        months_ago,
        'SCORE',
        'SCORE_d',
        lambda x: 30 if x > 30 else x,
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    item_weight = [0.65, 0.25, 0.1, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
