# -*- coding: utf-8 -*-
"""
客运劳安，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)
from app.data.major_risk_index.cw_keyun_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.cw_keyun_laoan import GLV
import pandas as pd
from app.data.util import (
    pd_query, get_coll_prefix, get_history_months)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number, COMMON_INDEX_ORGANIZATION)
from app.data.major_risk_index.cw_keyun_laoan.check_intensity_sql import(
    NOITEM_CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL,
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    XIANCHENG_YIBAN_LEVEL_PROBLEM_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    NOITEM_WATCH_MEDIA_BANZU_COUNT_SQL,
    GUANLI_CHECK_PROBLEM_SQL, ZUOYE_CHECK_PROBLEM_SQL,
    NOITEM_MEDIA_COST_TIME_SQL, NOITEM_YECHA_CHECK_SQL,
    KAOHE_GL_CHECK_PROBLEM_SQL, KAOHE_ZY_CHECK_PROBLEM_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL, ZUOYE_PROBLEM_CHECK_SCORE_SQL,
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL
)
from app.data.major_risk_index.cw_keyun_laoan.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, RISK_GANBU_ASSESS_RESPONSIBLE_SQL
)
from app.data.major_risk_index.cw_keyun_laoan.common_sql import (
    WORK_LOAD_SQL,
    WORKER_COUNT_SQL, CADRE_COUNT_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, PERSON_LOAD_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    ZUOYE_PROBLEM_COUNT_INFO, GUANLI_PROBLEM_COUNT_INFO,
    CADRE_COUNT_INFO, WORKER_COUNT_INFO, STAFF_NUMBER_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO, STAFF_NUMBER_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    KAOHE_GL_CHECK_PROBLEM_INFO, KAOHE_ZY_CHECK_PROBLEM_INFO,
    GANBU_ASSESS_RESPONSIBLE_INFO, ASSESS_RESPONSIBLE_INFO,
    ALL_PROBLEM_NUMBER_INFO, ZUOYE_PROBLEM_CHECK_SCORE_INFO)


# MAIN_TYPE=1为事故
# MAIN_TYPE=2为故障
BASE_UNIT_INFO_SQL = """
SELECT 
    a.OCCURRENCE_TIME, 
    c.TYPE3 as DEPARTMENT_ID,
    1 as COUNT,
    a.MAIN_TYPE
FROM
    t_safety_produce_info AS a
        INNER JOIN
    t_safety_produce_info_responsibility_department AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
		inner join
    t_department as c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
WHERE
    DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.IS_DELETE = 0
        AND c.TYPE3 in {2}
        AND a.MAIN_TYPE in (1, 2)
"""

# 外聘人员数 - 站段
EMPLOYED_OUTSIDE_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.TYPE3 in {1}
    AND b.is_delete = 0
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        STAFF_NUMBER, WORKER_COUNT_DATA, CHECK_ITEM_IDS, RISK_IDS
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    CALC_MONTH = END, START
    IDS = get_vitual_major_ids(risk_type)
    # 单位职工人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(IDS))
    # 非干部
    WORKER_COUNT_DATA = pd_query(WORKER_COUNT_SQL.format(IDS))


# ------------------------获取比值型相应基数------------------------ #
# 比值型指数（main_type-detail_type)
_DIVIDE_TYPE_CHILD_INDEX = {
    '1-2': [
        '{0}{1}月现场检查{2}次，干部职工数{3}',
        '虚拟单位前3个月现场检查均值{0}次(总现场检查{1}次), 次，干部职工数均值{2}(总干部职工{3})'
    ]
}


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, IDS)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.value = [NOITEM_CHECK_COUNT_SQL]

    # 总人数
    PERSON_LOAD = CommonCalcDataType(*PERSON_LOAD_INFO)
    PERSON_LOAD.value = [STAFF_NUMBER, EMPLOYED_OUTSIDE_PERSON_SQL.format('{0}', IDS)]
    # 单位职工数
    STAFF_NUMBER_DATA = CommonCalcDataType(*STAFF_NUMBER_INFO)
    STAFF_NUMBER_DATA.value = [STAFF_NUMBER]


    # 作业项问题数
    ZUOYE_PROBLEM_COUNT = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ZUOYE_PROBLEM_COUNT.value = [ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]


    # 管理项问题数
    GUANLI_PROBLEM_COUNT = CommonCalcDataType(*GUANLI_PROBLEM_COUNT_INFO)
    GUANLI_PROBLEM_COUNT.value = [GUANLI_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 干部数
    CADRE_COUNT = CommonCalcDataType(*CADRE_COUNT_INFO)
    CADRE_COUNT.value = [CADRE_COUNT_SQL.format(IDS)]
    
    # 作业人数
    WORKER_COUNT = CommonCalcDataType(*WORKER_COUNT_INFO)
    WORKER_COUNT.value = [WORKER_COUNT_DATA, EMPLOYED_OUTSIDE_PERSON_SQL.format('{0}', IDS), ]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.value = [PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 较大问题质量分
    # JIAODA_RISK_SCORE = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    # JIAODA_RISK_SCORE.value = [RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    XC_JIAODA_RISK_SCORE = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    XC_JIAODA_RISK_SCORE.version = 'v2'
    XC_JIAODA_RISK_SCORE.description = '现场检查一般及以上问题质量分（关联风险）'
    XC_JIAODA_RISK_SCORE.value = [XIANCHENG_YIBAN_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 夜查次数
    YECHA_COUNT = CommonCalcDataType(*YECHA_COUNT_INFO)
    YECHA_COUNT.value = [NOITEM_YECHA_CHECK_SQL]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value =[NOITEM_MEDIA_COST_TIME_SQL]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.value = [MEDIA_PROBLEM_NUMBER_SQL.format('{0}','{1}',RISK_IDS)]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.value = [MEDIA_PROBLME_SCORE_SQL.format('{0}','{1}',RISK_IDS)]

    # 考核作业问题数
    KAOHE_ZY_CHECK_PROBLEM = CommonCalcDataType(*KAOHE_ZY_CHECK_PROBLEM_INFO)
    KAOHE_ZY_CHECK_PROBLEM.value = [KAOHE_ZY_CHECK_PROBLEM_SQL.format('{0}','{1}',RISK_IDS)]

    # 考核管理项问题数
    KAOHE_GL_CHECK_PROBLEM = CommonCalcDataType(*KAOHE_GL_CHECK_PROBLEM_INFO)
    KAOHE_GL_CHECK_PROBLEM.value = [KAOHE_GL_CHECK_PROBLEM_SQL.format('{0}','{1}',RISK_IDS)]

    # 换算单位考核金额
    # 干部考核金额
    GANBU_ASSESS_RESPONSIBLE = CommonCalcDataType(*GANBU_ASSESS_RESPONSIBLE_INFO)
    GANBU_ASSESS_RESPONSIBLE.version = 'v2'
    GANBU_ASSESS_RESPONSIBLE.description = '干部月度考核总金额(关联风险)'
    GANBU_ASSESS_RESPONSIBLE.value = [RISK_GANBU_ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核总金额
    ASSESS_RESPONSIBLE = CommonCalcDataType(*ASSESS_RESPONSIBLE_INFO)
    ASSESS_RESPONSIBLE.version = 'v2'
    ASSESS_RESPONSIBLE.description = '月度考核总金额(关联风险)'
    ASSESS_RESPONSIBLE.value = [ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题数
    ABOVE_YIBAN_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ABOVE_YIBAN_PROBLEM_NUMBER.version = 'v2'
    ABOVE_YIBAN_PROBLEM_NUMBER.description = '一般及以上问题数'
    ABOVE_YIBAN_PROBLEM_NUMBER.value = [ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}','{1}',RISK_IDS)]

    # 一般以及以上作业项问题数
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.version = 'v2'
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.description = '一般以及以上作业项问题数'
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.value = [ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题质量分
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.version = 'v2'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.description = '一般及以上问题质量分'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.value = [ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上作业项问题质量分
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.version = 'v2'
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.description = '一般及以上作业项问题质量分'
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.value = [ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}','{1}', RISK_IDS)]

    # 作业项问题质量分
    ZUOYE_PROBLEM_CHECK_SCORE = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    ZUOYE_PROBLEM_CHECK_SCORE.value = [ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.value = [ALL_PROBLEM_NUMBER_SQL.format('{0}','{1}', RISK_IDS)]

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, PERSON_LOAD),),

        # 查出问题率
        '1-3': (IndexDetails(ZUOYE_PROBLEM_COUNT, WORKER_COUNT), IndexDetails(GUANLI_PROBLEM_COUNT, CADRE_COUNT)),


        # 换算单位人均质量分
        '1-5': (IndexDetails(PROBLEM_CHECK_SCORE, PERSON_LOAD),),

        # 一般及以上风险问题质量均分
        '1-6':(IndexDetails(ABOVE_YIBAN_PROBLEM_CHECK_SCORE, PERSON_LOAD), 
                IndexDetails(XC_JIAODA_RISK_SCORE,PERSON_LOAD)),

        # 夜查率
        '1-7':(IndexDetails(YECHA_COUNT, WORKER_COUNT),),
        # 监控调阅力度
        '1-10':(IndexDetails(MEDIA_COST_TIME, WORKER_COUNT), IndexDetails(MEDIA_PROBLEM_NUMBER,WORKER_COUNT), 
                IndexDetails(MEDIA_PROBLME_SCORE,WORKER_COUNT)),

        # 考核力度指数
        # 换算单位考核问题数
        '3-1':(IndexDetails(KAOHE_ZY_CHECK_PROBLEM, WORKER_COUNT), IndexDetails(KAOHE_GL_CHECK_PROBLEM, CADRE_COUNT)),

        # 换算单位考核金额
        '3-2':(IndexDetails(GANBU_ASSESS_RESPONSIBLE, CADRE_COUNT), IndexDetails(ASSESS_RESPONSIBLE, STAFF_NUMBER_DATA)),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1':(
            IndexDetails(ALL_PROBLEM_NUMBER, PERSON_LOAD),
            IndexDetails(PROBLEM_CHECK_SCORE, PERSON_LOAD),
            IndexDetails(ABOVE_YIBAN_PROBLEM_NUMBER, PERSON_LOAD),
            IndexDetails(ABOVE_YIBAN_PROBLEM_CHECK_SCORE, PERSON_LOAD),
            IndexDetails(ZUOYE_PROBLEM_COUNT, WORKER_COUNT),
            IndexDetails(ZUOYE_PROBLEM_CHECK_SCORE, WORKER_COUNT),
            IndexDetails(ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM, WORKER_COUNT),
            IndexDetails(ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE, WORKER_COUNT),
        ),
    }
    
    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_problem_ratio', CHILD_INDEX_SQL_DICT['1-3'])
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_score_per_person', CHILD_INDEX_SQL_DICT['1-5'])
    GLV.set_value('stats_yecha_ratio', CHILD_INDEX_SQL_DICT['1-7'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_check_problem_assess_radio', CHILD_INDEX_SQL_DICT['3-1'])
    GLV.set_value('stats_assess_money_per_person', CHILD_INDEX_SQL_DICT['3-2'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])
