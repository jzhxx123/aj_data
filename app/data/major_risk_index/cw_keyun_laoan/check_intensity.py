# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.cw_keyun_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.cw_keyun_laoan.check_intensity_sql import(
    NOITEM_CHECK_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    CHECK_INFO_SQL, NOITEM_WATCH_MEDIA_BANZU_COUNT_SQL,XIANCHENG_YIBAN_LEVEL_PROBLEM_SQL,
    WORK_BANZU_COUNT_SQL, GUANLI_CHECK_PROBLEM_SQL, ZUOYE_CHECK_PROBLEM_SQL,
    NOITEM_MEDIA_COST_TIME_SQL, NOITEM_YECHA_CHECK_SQL, BANZU_POINT_SQL,
    SPECIAL_REAL_BANZU_CHECKED_COUNT_SQLIST, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL)
from app.data.major_risk_index.cw_keyun_laoan.common_sql import (
    EXTERNAL_PERSON_SQL, WORK_LOAD_SQL,
    WORKER_COUNT_SQL, CADRE_COUNT_SQL)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query
from app.data.major_risk_index.cw_keyun_laoan.common import get_vitual_major_ids
from app.data.util import update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global PERSON_LOAD, CADRE_COUNT, WORKER_LOAD, CHECK_COUNT, \
        GUANLI_PROBLEM_COUNT, ZUOYE_PROBLEM_COUNT, PROBLEM_SCORE, \
        YECHA_COUNT, JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, \
        REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA, STAFF_NUMBER
    ids = get_vitual_major_ids(risk_type)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    global RISK_IDS
    RISK_IDS = risk_ids
    # 统计工作量【职工总人数】
    month = int(stats_month[1][5:7])
    # 单位职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, ids))
    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    # 干部职工总数（干部职工+外聘）
    PERSON_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    WORKER_LOAD = pd.concat([WORKER_COUNT, ZHANDUAN_STAFF], axis=0, sort=False)
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_CHECK_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(GUANLI_CHECK_PROBLEM_SQL.format(
            *stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_YECHA_CHECK_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 一般及以上风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)), DEPARTMENT_DATA)
    # 现场检查发现一般及以上风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_YIBAN_LEVEL_PROBLEM_SQL.format(
            *stats_month, risk_ids)),
        DEPARTMENT_DATA)

    REAL_CHECK_BANZU_DATA = pd.merge(
        pd_query(SPECIAL_REAL_BANZU_CHECKED_COUNT_SQLIST[0].format(ids)),
        pd_query(CHECK_INFO_SQL.format(*stats_month)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )
    REAL_CHECK_BANZU_DATA.drop(["PK_ID"], inplace=True, axis=1)
    # 删除重复部门
    REAL_CHECK_BANZU_DATA.drop_duplicates(
        subset='FK_DEPARTMENT_ID', keep='first', inplace=True)
    REAL_CHECK_BANZU_DATA['COUNT'] = 1
    BANZU_POINT_DATA = pd_query(BANZU_POINT_SQL)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = " + \
        "量化人员及干部检查（现场检查劳安风险）次数({4})/ 干部职工总数({5})</p>"
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    title = ['作业项问题数({0})/作业人数({1})', '管理项问题数({0})/管理人员数({1})']
    fraction_list = GLV.get_value('stats_check_problem_ratio', (None, None))
    return check_intensity.stats_check_problem_ratio_type_two_type_chewu(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORKER_LOAD,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.9, 0.1],
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    title = ['一般及以上安全风险问题质量分累计({0})/干部职工总数({1})',
             '现场检查发现一般及以上安全风险问题质量分累计({0})/干部职工总数({1})']
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = " + \
        "问题质量分累计({4})/ 站段干部职工总数({5})</p>"
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 夜查率
def _stats_yecha_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                        "现场检查劳安夜查次数({4})/作业人数({5})*100%</p>"
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        WORKER_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    title = ['监控调阅时长累计({0})/作业人数({1})',
             '监控调阅发现问题数({0})/作业人数({1})', 
             '监控调阅发现问题质量分累计({0})/作业人数({1})',
             '调阅班组数({0})/作业班组数({1})']
    monitor_watch_discovery_ratio_sqllist = [
        NOITEM_WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL
    ]
    stats_month = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_month, RISK_IDS)
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = list(fraction_list)
    fraction_list.append(None)
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORKER_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=NOITEM_MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        title=title,
        child_weight=[0.2, 0.3, 0.4, 0.1],
        fraction_list=tuple(fraction_list))


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person, _stats_score_per_person,
        _stats_yecha_ratio, _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']]
    item_weight = [0.25, 0.1, 0.15, 0.1, 0.15, 0.15, 0.1]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 6, 10, 7, 9],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
