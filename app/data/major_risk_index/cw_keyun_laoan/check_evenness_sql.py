# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT DISTINCT
    tcp.PK_ID AS CHECK_POINT_ID,
    tcp.ALL_NAME AS ADDRESS_NAME,
    tds.SOURCE_DEPARTMENT_ID,
    tds.PERSON_NUMBER 
FROM
    t_check_point AS tcp
    INNER JOIN
    t_department_and_main_production_site AS tds ON tcp.PK_ID = tds.FK_ADDRESS_ID 
WHERE
    tds.TYPE = 2
    AND tcp.TYPE = 1 
    AND tcp.IS_DELETE = 0
"""

# 重要检查点-专业下所有具备检查项目的部门-不关联项目
CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
    b.DEPARTMENT_ID as FK_DEPARTMENT_ID,
    COUNT( DISTINCT c.PK_ID ) AS PERSON_COUNT 
FROM
    t_department AS b
    INNER JOIN t_person AS c ON b.DEPARTMENT_ID = c.FK_DEPARTMENT_ID 
WHERE
    b.TYPE2 = '{0}'
    AND b.IS_DELETE = 0 
    AND c.IS_DELETE = 0 
GROUP BY
    b.DEPARTMENT_ID
"""

# 重要检查点受检次数-不关联项目
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID AS CHECK_POINT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 AND 2
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS PERSON_COUNT_2
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
    GROUP BY a.DEPARTMENT_ID;
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME
    FROM
        t_department
    WHERE
        TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND TYPE2 = '{0}'
"""

# 班组受检次数-不关联项目
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
    GROUP BY c.DEPARTMENT_ID;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE BETWEEN 9 AND 10
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY  BETWEEN 1 AND 2
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_H_SQL = """SELECT
        a.FK_DEPARTMENT_ID,  a.WORK_TYPE, a.WORK_TIME
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
        AND a.MAIN_TYPE = 1
            AND b.TYPE = 9 AND b.IS_DELETE = 0
            AND a.SOURCE_ID IN ({0})
"""

# 每天检查数-按时段
HOUR_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR,
        COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
    GROUP BY b.FK_DEPARTMENT_ID , START_HOUR, END_HOUR;
"""

# 基础问题库中较大及以上风险项点问题数
RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.PK_ID
"""

# 较大及以上问题项点数
RISK_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        MAX(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.fk_check_info_id = b.fk_check_info_id
            INNER JOIN
        t_check_problem_and_risk as c on a.PK_ID = c.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
    GROUP BY a.FK_PROBLEM_BASE_ID
"""