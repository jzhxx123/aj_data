#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-05-21
-------------------------------------------------
Change Activity:2019-05-21 14:49
-------------------------------------------------
"""
from app.data.util import pd_query

def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    BELONG_PROFESSION_ID = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = BELONG_PROFESSION_ID.get(major, 898)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())
