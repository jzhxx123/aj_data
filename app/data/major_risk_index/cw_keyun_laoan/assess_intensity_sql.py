from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, RISK_GANBU_ASSESS_RESPONSIBLE_SQL
)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import(
    KAOHE_ZY_CHECK_PROBLEM_SQL, KAOHE_GL_CHECK_PROBLEM_SQL
)