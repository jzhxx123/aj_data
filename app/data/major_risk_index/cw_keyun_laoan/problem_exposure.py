# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd
from app.data.major_risk_index.cw_keyun_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.cw_keyun_laoan.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.cw_keyun_laoan.problem_exposure_sql import(
    CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
    CHECKED_HIDDEN_PROBLEM_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL, 
    HIDDEN_PROBLEM_POINT_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.cw_keyun_laoan.common_sql import (
    WORK_LOAD_SQL,
    EXTERNAL_PERSON_SQL, WORKER_COUNT_SQL, CADRE_COUNT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query
from app.data.major_risk_index.cw_keyun_laoan.common import get_vitual_major_ids
from app.data.util import update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        PERSON_LOAD, WORKER_LOAD, CADRE_COUNT, CHECKED_HIDDEN_PROBLEM_POINT_DATA, \
        HIDDEN_PROBLEM_POINT_DATA
    ids = get_vitual_major_ids(risk_type)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(ids)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, ids))
    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL.format(ids)), DEPARTMENT_DATA)
    # 单位总人数
    PERSON_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 作业总人数
    WORKER_LOAD = pd.concat([WORKER_COUNT, ZHANDUAN_STAFF], axis=0, sort=False)
    WORKER_LOAD = WORKER_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORKER_LOAD = WORKER_LOAD.to_frame(name='PERSON_NUMBER')

    # 单位总人数
    PERSON_LOAD = PERSON_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    PERSON_LOAD = PERSON_LOAD.to_frame(name='PERSON_NUMBER')

    # 干部人数
    CADRE_COUNT = CADRE_COUNT.groupby(['TYPE3'])['COUNT'].sum()
    CADRE_COUNT = CADRE_COUNT.to_frame(name='PERSON_NUMBER')

    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)),
         DEPARTMENT_DATA
        )

    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)),
        DEPARTMENT_DATA)

# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    title = [
        '总问题数({0})/干部职工总数({1})', 
        '一般及以上问题数({0})/干部职工总数({1})', 
        '作业项问题数({0})/作业人数({1})',
        '一般及以上作业项问题数({0})/作业人数({1})'
    ]
    fraction_list = GLV.get_value('stats_total_problem_exposure', 
        (None, None, None, None, None, None, None, None))
    for fraction in fraction_list:
        if fraction:
            fraction.detail_type = 1
    return problem_exposure.stats_total_problem_exposure_type_chewu(
        RISK_IDS, CHECK_PROBLEM_SQL, PERSON_LOAD, WORKER_LOAD,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        title=title, fraction_list=[
            [fraction_list[0],fraction_list[1]],
            [fraction_list[2],fraction_list[3]],
            [fraction_list[4],fraction_list[5]],
            [fraction_list[6],fraction_list[7]],
        ])


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    customizededuct = {
            6: 2,
            9: 4
        }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct, months=10)


# 较严重隐患暴露度
def _stats_hidden_problem_exposure(months_ago):
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA,
        DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_ITEM_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return problem_exposure.stats_other_problem_exposure_excellent(
        CHECK_ITEM_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        problem_risk_score={'1': 3, '2': 1, '3': 0.1, })


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.5, 0.25, 0.15, 0.1, -1]
    update_major_maintype_weight(index_type=1, major=risk_type, main_type=5,
                                 child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
