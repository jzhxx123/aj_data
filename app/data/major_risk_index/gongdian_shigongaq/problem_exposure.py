# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd

from app.data.index.util import (get_custom_month, get_months_from_201712)
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shigongaq import GLV
from app.data.major_risk_index.gongdian_shigongaq.check_intensity_sql import WORK_BANZU_COUNT_SQL
from app.data.major_risk_index.gongdian_shigongaq.common import get_vitual_major_ids, get_ratio_df, \
    _calc_score_for_by_major_ratio_df, get_work_load_df
from app.data.major_risk_index.gongdian_shigongaq.common_sql import EXTERNAL_PERSON_SQL, WORK_LOAD_SQL, \
    DEPARTMENT_TICKET_COUNT_SQL, OTHER_WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_shigongaq.problem_exposure_sql import CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_SQL, \
    HIDDEN_KEY_PROBLEM_MONTH_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL, \
    SAFETY_PRODUCE_INFO_SQL, CHECKED_HIDDEN_PROBLEM_POINT_SQL, HIDDEN_PROBLEM_POINT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, export_basic_data_one_field_monthly,
    calc_child_index_type_sum)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE, CHECK_ITEM_IDS, RISK_IDS
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        WORK_LOAD, CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA, BANZU_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    stats_month = get_custom_month(months_ago)
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])

    # 统计工作量【职工总人数】
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        get_work_load_df(WORK_LOAD_SQL, OTHER_WORK_LOAD_SQL, major, month),
        DEPARTMENT_DATA)
    # 站段外聘人员
    zhanduan_staff = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, zhanduan_staff], axis=0, sort=False)
    WORK_LOAD = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    # 查出较严重隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 单位应检查问题项点
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)),
        DEPARTMENT_DATA)

    # 班组问题暴露度中所有班组(施工项目, 具有工作票)
    # 具备工作票的部门
    department_ticket_count = pd_query(DEPARTMENT_TICKET_COUNT_SQL.format(*stats_month), db_name='db_mid')
    # 工作班组信息
    work_banzu_info_data = pd_query(WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))
    BANZU_DATA = pd.merge(
        department_ticket_count, work_banzu_info_data[['FK_DEPARTMENT_ID']], how='inner', on='FK_DEPARTMENT_ID')

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    work = WORK_LOAD.reset_index().rename(columns={'TYPE3': 'DEPARTMENT_ID'})
    ratio_df_1 = get_ratio_df(
        work, 0.2, 0.18, 0.14, column='PERSON_NUMBER', is_stable=0)
    ratio_df_2 = get_ratio_df(work, 5, 4, 2.5, column='PERSON_NUMBER')
    ratio_df_dict = {
        0: [ratio_df_1, ratio_df_2]
    }
    calc_score_formula = {
        0: _calc_score_for_by_major_ratio_df
    }
    return problem_exposure.stats_total_problem_exposure(
        RISK_IDS, CHECK_PROBLEM_SQL, WORK_LOAD, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, ratio_df_dict=ratio_df_dict,
        calc_score_formula=calc_score_formula,
    )


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续4月无的扣2分/条，连续5个月无的扣4分/条，…扣月份-2分/条。得分=100-扣分。"""
    customizededuct = {
        4: 2,
        5: 4,
        6: 4
    }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct, months=7)


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    """较大、重大安全风险，且本单位基础问题库中有的),本单位未查出的按1分/项扣"""
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        calc_score_formula=lambda x: 0 if (100 - 1 * x) < 0 else round((100 - 1 * x), 2),
        title='本月问题个数: {1}个')


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_months = get_months_from_201712(months_ago, 4)
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(BANZU_DATA['FK_DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    # 用来记录每个班组每个月问题为空白的个数
    exposure_banzu_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            pd_query(
                EXPOSURE_PROBLEM_DEPARTMENT_SQL.format(
                    CHECK_ITEM_IDS, mon[0],
                    mon[1]))['FK_DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        if len(hidden_banzu) == 0:
            break
        if idx > 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx
                })
                exposure_banzu_num_monthly.append([dpid, idx])
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    # 导出中间计算数据
    export_basic_data_one_field_monthly(
        exposure_banzu_num_monthly,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        4,
        3,
        months_ago,
        lambda x: f'连续{x}个月未暴露问题的班组个数',
        title='暴露班组数:',
        risk_type=RISK_TYPE)
    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        4,
        months_ago,
        'SCORE',
        'SCORE_d',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        NA_value=True)
    return rst_child_score


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。"""
    return problem_exposure.stats_other_problem_exposure(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    '''
    child_index_func:
    指标名称 | 计算方法 | 缓存Key
    总体暴露度(65%) _stats_total_problem_exposure SCORE_a
    事故隐患问题暴露度(15%) _stats_problem_exposure SCORE_c
    班组问题暴露度(10%) _stats_banzu_problem_exposure SCORE_d
    他查问题暴露度(-1) _stats_other_problem_exposure SCORE_e
    较大隐患问题暴露度(10%) _stats_hidden_problem_exposure SCORE_b
    '''
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.4, 0.2, 0.1, 0.2, 0.1]
    update_major_maintype_weight(index_type=7, major=risk_type,
                                 main_type=5, child_index_list=[1, 2, 3, 4, 5], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
