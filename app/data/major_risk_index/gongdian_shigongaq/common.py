import datetime

import pandas as pd

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common.check_intensity import _calc_media_time_per_person_major, \
    _calc_media_problem_per_person_major, _calc_media_score_per_person_major, _calc_media_val_person_major, \
    _calc_score_by_formula_media_banzu
from app.data.major_risk_index.common.check_intensity_sql import MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, \
    MEDIA_PROBLME_SCORE_SQL
from app.data.major_risk_index.common.const import MainType, CheckIntensityDetailType
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import \
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
from app.data.major_risk_index.gongdian_shigongaq.check_intensity_sql import BANZU_POINT_SQL
from app.data.major_risk_index.gongdian_shigongaq.common_sql import DEPARTMENT_TICKET_COUNT_SQL
from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set, df_merge_with_dpid, \
    calc_extra_child_score_groupby_major, \
    calc_extra_child_score_groupby_major_third, write_cardinal_number_basic_data
from app.data.util import pd_query
from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_exposure(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_score_for_by_major_ratio(self_ratio, major_ratio):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    n1 = major_ratio[0][0]
    sn1 = major_ratio[0][1]
    n2 = major_ratio[1][0]
    sn2 = major_ratio[1][1]
    n3 = major_ratio[2][0]
    sn3 = major_ratio[2][1]
    c = self_ratio
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _calc_score_for_by_major_ratio_df(row):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    n1 = row['ratio_1']
    sn1 = row['score_1']
    n2 = row['ratio_2']
    sn2 = row['score_2']
    n3 = row['ratio_3']
    sn3 = row['score_3']
    c = row['ratio']
    for idx, item in enumerate([n1, n2, n3]):
        if c > item:
            level = idx + 1
            break
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _cacl_bd_total_person_num(row):
    """按照给定逻辑计算变电所人数
    """
    total_num = 0
    if row.SUBSTATION_GUARDIAN_ID.is_integer():
        id_str = str(row.WORK_LEADER_ID) + ',' + str(row.WORK_MAKEUP_PERSON_ID) + ',' + str(
            int(row.SUBSTATION_GUARDIAN_ID))
    else:
        id_str = str(row.WORK_LEADER_ID) + ',' + str(row.WORK_MAKEUP_PERSON_ID)
    total_num += len(set(id_str.split(',')))
    for field in [
        'NON_PROFESSIONAL_OTHER',
        'NON_PROFESSIONAL'
    ]:
        value = str(row[field])
        if len(value) > 0:
            total_num += len(value.split('、'))
    return total_num


def get_ticket_amount(truck_ticket_data, bd_ticket_data, department_data):
    """0.7*(工作票数/700)+0.3*(人时/18000)
    """
    bd_ticket_data['TOTAL_PERSON_NUM'] = bd_ticket_data.apply(
        lambda row: _cacl_bd_total_person_num(row), axis=1)
    bd_ticket_data.drop(
        columns=[
            'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
            'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
            'NON_PROFESSIONAL'
        ],
        inplace=True)
    ticket_data = pd.concat(
        [truck_ticket_data, bd_ticket_data], axis=0, sort=False)
    ticket_data['TIME_COUNT'] = ticket_data.apply(
        lambda row: int(row['TOTAL_PERSON_NUM']) * max(1, (row['HOURS'] + 24) if row['HOURS'] < 0 else row['HOURS']),
        axis=1)
    ticket_data['COUNT'] = ticket_data.apply(
        lambda row: 0.001 + row['TIME_COUNT'] / 60000, axis=1
    )

    ticket_data = ticket_data.groupby(['DEPART_ID'])['COUNT'].sum()
    ticket_department = pd.merge(
        ticket_data.to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return ticket_department


def _is_yecha(row):
    """判断是否是夜查，晚上10时到次日6时为夜查
    """
    if row['START_WORK_TIME'] >= 22 or row['END_WORK_TIME'] <= 6:
        return 1
    return 0


def _is_genban(row):
    """跟班率统计工作票上是否有跟班签字，接触网工作票叫做监控干部，
    变配电工作票叫做跟班干部，作业车工作票叫做添乘干部
    """
    if not row['GENBAN']:
        return 0
    if len(row['GENBAN'].strip()) > 0:
        return 1
    return 0


# 计算工作票总数， 跟班工作票数， 夜间工作票数， 夜间跟班工作票数
def stats_work_ticket(work_ticket, department_data):
    work_ticket['IS_YECHA'] = work_ticket.apply(
        lambda row: _is_yecha(row), axis=1)
    work_ticket['IS_GENBAN'] = work_ticket.apply(
        lambda row: _is_genban(row), axis=1)
    work_ticket.drop(
        columns=['START_WORK_TIME', 'END_WORK_TIME', 'GENBAN'], inplace=True)
    # 统计总票数
    total_tickets = pd.merge(
        work_ticket.groupby(['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 跟班总票数
    genban_tickets = pd.merge(
        work_ticket[work_ticket['IS_GENBAN'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查总票数
    yecha_tickets = pd.merge(
        work_ticket[work_ticket['IS_YECHA'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查跟班总票数
    yecha_genban_tickets = pd.merge(
        work_ticket[(work_ticket['IS_GENBAN'] == 1)
                    & (work_ticket['IS_YECHA'] == 1)].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return total_tickets, genban_tickets, yecha_tickets, yecha_genban_tickets


def _calc_risk_score_per_person_major(series, work_load, choose_dpid_data, hierarchy,
                                      idx, zhanduan_filter_list, title,
                                      calc_score_by_formula=_calc_score_by_formula,
                                      fraction=None, major_ratio=None):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data['ratio'] = data.apply(lambda row: row['score'] / row['PERSON_NUMBER'] if
    row['PERSON_NUMBER'] > 0 else 0, axis=1)
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(hierarchy), fraction,
                                         fraction.risk_type, MainType.check_intensity,
                                         CheckIntensityDetailType.stats_risk_score_per_person, fraction.months_ago,
                                         columns=['score', 'PERSON_NUMBER'])
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula, zhanduan_filter_list,
        numerator='score', denominator='PERSON_NUMBER', fraction=fraction, major_ratio=major_ratio)
    # 中间计算数据
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["score"], 2) if pd.notnull(row["score"]) else 0.00}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_problem_count_major(df_problem, work_load, choose_dpid_data, hierarchy,
                                 idx, zhanduan_filter_list, title,
                                 calc_score_by_formula=_calc_score_by_formula,
                                 major_ratio=None):
    """近三个月被评价人次分值({0})/近三个月该段干部所有评价人次总数({1})"""
    sc_problem_count = df_problem.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    sc_problem_count.fillna(0, inplace=True)
    return _calc_risk_score_per_person_major(sc_problem_count, work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             major_ratio=major_ratio)


def _calc_risk_score_major(
        df_jiaoda, work_load, choose_dpid_data, hierarchy, idx,
        zhanduan_filter_list, title,
        calc_score_by_formula=_calc_score_by_formula, major_ratio=None):
    """近三个月被评价人次数量({0})/近三个月该段干部所有评价人次总数({1})"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    risk_score.fillna(0, inplace=True)
    return _calc_risk_score_per_person_major(risk_score, work_load, choose_dpid_data,
                                             hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             major_ratio=major_ratio)


# 评价得分
def stats_score_per_dp(evaluate_people_count,
                       active_evaluate_score,
                       work_load,
                       months_ago,
                       risk_type,
                       child_weight=[0.5, 0.5],
                       choose_dpid_data=None,
                       calc_score_by_formula=_calc_score_by_formula,
                       major_ratio_list=None,
                       zhanduan_filter_list=[],
                       title=[
                           '近三个月被评价人次数量({0})/近三个月该段干部所有评价人次总数({1})',
                           '近三个月被评价人次分值({0})/近三个月该段干部所有评价人次总数({1})',
                       ]):
    """
    近三个月因施工安全风险（含作业安全风险、劳动安全风险和施工配合监管风险）被评价人次数量/该段干部总数所有评价人次总数（50%），
    被评价人次分值/该段干部总数所有评价人次总数（50%），与专业平均数比较换算得分
    """

    rst_child_score = []
    df_list = [evaluate_people_count, active_evaluate_score]
    # 保存中间计算过程数据
    calc_basic_data = []
    if not major_ratio_list:
        major_ratio_list = [None, None]
    for hierarchy in [3]:
        score = []
        child_func = [_calc_risk_score_major, _calc_xc_problem_count_major]
        for idx, ifunc in enumerate(child_func):
            if isinstance(calc_score_by_formula, dict):
                score_formula = calc_score_by_formula.get(idx)
            else:
                score_formula = _calc_score_by_formula
            major_ratio = major_ratio_list[idx]
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=score_formula,
                                             major_ratio=major_ratio,
                                             )
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 2, 11, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 2, 11, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_k_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            2,
            11,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula_exposure
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER')


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


# 总体暴露度
def stats_total_problem_exposure_type(
        check_item_ids, check_problem_sql, all_load, zuoye_load,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True,
        weight_item=[0.3, 0.3, 0.2, 0.2],
        weight_part=[0.4, 0.6]):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    if not title:
        title = [
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})']
    # 导出中间过程
    work_load = [all_load, zuoye_load]
    for i, data in enumerate(
            [base_data, risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load[i // 2], department_data, i,
                     title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in [3]:
        score = []
        for i, data in enumerate(
                [base_data, risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load[i // 2], department_data,
                         choose_dpid_data, weight, hierarchy,
                         is_calc_score_base_major=is_calc_score_base_major))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_work_load_oneday(time, flag=0):
    workload_a = 0
    zero = (datetime.datetime.strptime(time[:10] + '00:00:00',
                                       "%Y-%m-%d%H:%M:%S")).timestamp()
    six = (datetime.datetime.strptime(time[:10] + '06:00:00',
                                      "%Y-%m-%d%H:%M:%S")).timestamp()
    twentytw0 = (datetime.datetime.strptime(time[:10] + '22:00:00',
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
    twentyfour = (datetime.datetime.strptime(time[:10] + '23:59:59',
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    calc_value = (datetime.datetime.strptime(time,
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    if flag == 0:
        if zero <= calc_value <= six:
            workload_a = six - calc_value + twentyfour - twentytw0
        elif six < calc_value <= twentytw0:
            workload_a = twentyfour - twentytw0
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = twentyfour - calc_value
    else:
        if zero <= calc_value <= six:
            workload_a = calc_value - zero
        elif six < calc_value <= twentytw0:
            workload_a = six - zero
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = calc_value - twentytw0 + six - zero
    return workload_a


def _calc_work_load_otherday(start, end, zero, six, twentytw0, twentyfour):
    from datetime import date
    workload = 0
    distance = date(int(end[:4]), int(end[5:7]), int(end[8:10])) - \
               date(int(start[:4]), int(start[5:7]), int(start[8:10]))
    days = int(distance.days)
    if days > 1:
        workload = _calc_work_load_oneday(start) + \
                   (days - 1) * (six - zero + twentyfour - twentytw0) + \
                   _calc_work_load_oneday(end, 1)
    else:
        workload = _calc_work_load_oneday(start) + _calc_work_load_oneday(end, 1)
    return workload


def _calc_work_load_sameday(start, end, zero, six, twentytw0, twentyfour):
    workload = 0
    if zero <= start < end <= six or twentytw0 <= start < end <= twentyfour:
        workload = end - start
    elif start < six < end <= twentytw0:
        workload = six - start
    elif start <= six < twentytw0 < end <= twentyfour:
        workload = six - start + end - twentyfour
    elif six <= start < twentytw0 < end <= twentyfour:
        workload = end - twentytw0
    return workload


def _calc_start_time(row):
    start_times = str(row["WORK_START_TIME"])
    end_times = str(row['WORK_END_TIME'])
    work_load_list = []
    try:
        start = (datetime.datetime.strptime(start_times.replace(' ', ''),
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
        end = (datetime.datetime.strptime(end_times.replace(' ', ''),
                                          "%Y-%m-%d%H:%M:%S")).timestamp()
        flag = True if start_times[:10] == end_times[:10] else False

        six = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '06:00:00',
                                          "%Y-%m-%d%H:%M:%S")).timestamp()
        zero = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '00:00:00',
                                           "%Y-%m-%d%H:%M:%S")).timestamp()
        twentytw0 = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '22:00:00',
                                                "%Y-%m-%d%H:%M:%S")).timestamp()
        twentyfour = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '23:59:59',
                                                 "%Y-%m-%d%H:%M:%S")).timestamp()
        if flag:
            work_load_list.append(_calc_work_load_sameday(start, end, zero,
                                                          six, twentytw0, twentyfour))
        else:
            work_load_list.append(_calc_work_load_otherday(
                start_times.replace(' ', ''),
                end_times.replace(' ', ''),
                zero, six, twentytw0, twentyfour))
    except Exception as e:
        work_load_list.append(0)
    row["SECOND"] = sum(work_load_list)
    return row


def merge_ticket_data(night_truck_work_ticket_hour_sql, night_bd_work_ticket_hour_sql, stats_month):
    """
    合并不同工作票
    :param night_truck_work_ticket_hour_sql: 作业车工作票
    :param night_bd_work_ticket_hour_sql: 牵引工作票
    :param stats_month:
    :return:
    """
    night_ticket_hour = pd_query(night_truck_work_ticket_hour_sql.format(
        *stats_month), db_name='db_mid')

    night_bd_work_ticket_hour = pd_query(night_bd_work_ticket_hour_sql.format(
        *stats_month), db_name='db_mid')

    night_bd_work_ticket_hour['TOTAL_PERSON_NUM'] = night_bd_work_ticket_hour.apply(
        lambda row: _cacl_bd_total_person_num(row), axis=1)
    night_bd_work_ticket_hour.drop(
        columns=[
            'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
            'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
            'NON_PROFESSIONAL'
        ],
        inplace=True)

    ticket_data = pd.concat(
        [night_ticket_hour, night_bd_work_ticket_hour], axis=0, sort=False)

    return ticket_data


def calc_night_work_load(df_work_time, dpid_data):
    """
    计算夜间工作量
    :param df_work_time:
    :param dpid_data:
    :return:
    """
    df_work_time = df_work_time.apply(
        lambda row: _calc_start_time(row),
        axis=1)
    df_work_time.fillna(0)
    df_work_time["TIME_COUNT"] = (df_work_time["SECOND"] * df_work_time["TOTAL_PERSON_NUM"]) / 3600
    df_work_time['COUNT'] = df_work_time.apply(
        lambda row: 0.001 + row['TIME_COUNT'] / 60000, axis=1
    )
    data = pd.merge(
        df_work_time,
        dpid_data,
        how="inner",
        left_on="DEPART_ID",
        right_on="DEPARTMENT_ID"
    )
    return data


def get_check_address_standard_data(work_banzu_info_data,
                                    department_data,
                                    months_ago,
                                    check_item_ids,
                                    major, is_base_item=True):
    """[将覆盖率数据转成标准格式]
    类型：检查地点数（实际检查班组+实际重要检查点）/地点总数（班组数+重要检查点）
    班组跟重要检查点的工作项目要复合特定指数检查项目
    """
    # 实际检查班组数
    check_info_and_address = cache_client.get(
        'check_info_and_address', months_ago=months_ago)
    real_check_banzu_data = pd.merge(check_info_and_address, department_data, left_on='ADDRESS_FK_DEPARTMENT_ID',
                                     right_on='DEPARTMENT_ID')
    del check_info_and_address
    if is_base_item:
        real_check_banzu_data = real_check_banzu_data[(real_check_banzu_data['ADDRESS_TYPE'] == 1)
                                                      & (real_check_banzu_data['TYPE'] == 9)]
    real_check_banzu_data = real_check_banzu_data.groupby(
        'ADDRESS_FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_banzu_data = real_check_banzu_data.reset_index().rename(
        columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})
    real_check_banzu_data['COUNT'] = 1
    real_check_banzu_data = pd.merge(
        real_check_banzu_data,
        work_banzu_info_data,
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    # 删除重复班组id
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)

    banzu_point_data = pd_query(BANZU_POINT_SQL)
    banzu_point_data = pd.merge(
        banzu_point_data,
        work_banzu_info_data,
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID',
        how='inner'
    )

    filled_data = pd_query(BANZU_POINT_SQL)
    filled_data['COUNT'] = 0
    if real_check_banzu_data.empty:
        # 两个均为空时，补充检查班组数
        real_check_banzu_data = filled_data
    if banzu_point_data.empty:
        # 两个均为空时，补充符合某工作项班组数
        banzu_point_data = filled_data
    return real_check_banzu_data, banzu_point_data


def filter_department_data(df, department_ticket_count, dpid):
    data = df_merge_with_dpid(df, department_ticket_count)
    data = pd.merge(
        data,
        dpid,
        how='inner',
        on='DEPARTMENT_ID'
    )
    data = data[(data['ALL_NAME'].str.contains('供电段')) |
                (data['ALL_NAME'].str.contains('工电段') & (
                        (data['ALL_NAME'].str.contains('工电工队')) |
                        (data['ALL_NAME'].str.contains('工电工区')) |
                        (data['ALL_NAME'].str.contains('综合调度')) |
                        (data['ALL_NAME'].str.contains(r'.*接触网.*工区')) |
                        (data['ALL_NAME'].str.contains('供电维修车间')) |
                        (data['ALL_NAME'].str.contains('变电检修车间')) |
                        (data['ALL_NAME'].str.contains('水电维修车间'))
                )
                 )
                ]
    data.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    return data[['FK_DEPARTMENT_ID']]


# 监控调阅覆盖比例(调阅班组数/作业或施工班组数)
def _calc_media_banzu_val_workbanzu_major(choose_dpid_data, stats_month,
                                          department_data, work_load, check_item_ids,
                                          hierarchy, idx, monitor_watch_discovery_ratio_sqllist, title,
                                          zhanduan_filter_list=None,
                                          calc_score_by_formula=_calc_score_by_formula,
                                          fraction=None):
    """
    :param choose_dpid_data:
    :param stats_month:
    :param department_data:
    :param work_load:
    :param check_item_ids:
    :param hierarchy:
    :param idx:
    :param monitor_watch_discovery_ratio_sqllist: 监控调阅覆盖比例sql列表（一般包含查询调阅班组数sql，查询施工班组数sql）
    :return:
    """
    # 具备工作票的部门
    department_ticket_count = pd_query(DEPARTMENT_TICKET_COUNT_SQL.format(*stats_month), db_name='db_mid')
    # 作业班组数(总班组)
    work_banzu = pd_query(monitor_watch_discovery_ratio_sqllist[1].format(check_item_ids))
    # 具备项目且有工作票
    work_banzu = pd.merge(department_ticket_count, work_banzu[['FK_DEPARTMENT_ID']], on='FK_DEPARTMENT_ID')
    work_banzu_count = df_merge_with_dpid(work_banzu, department_data, how='right')

    # 调阅班组数
    watch_media_banzu = pd_query(monitor_watch_discovery_ratio_sqllist[0].format(
        *stats_month, check_item_ids))
    watch_media_banzu = pd.merge(
        watch_media_banzu,
        work_banzu[['FK_DEPARTMENT_ID']],
        how='inner',
        on='FK_DEPARTMENT_ID'
    )
    watch_media_banzu_count = df_merge_with_dpid(watch_media_banzu, department_data, how='right')

    watch_media_banzu_count.fillna(0, inplace=True)
    work_banzu_count.fillna(0, inplace=True)

    media_time = watch_media_banzu_count.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_banzu_count,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=_calc_score_by_formula_media_banzu,
                                        zhanduan_filter_list=zhanduan_filter_list,
                                        fraction=fraction)


# 监控调阅力度
def stats_media_intensity_major(department_data,
                                work_load,
                                months_ago,
                                risk_name,
                                risk_type,
                                risk_ids=None,
                                choose_dpid_data=None,
                                media_cost_time_sql=None,
                                media_problem_number_sql=None,
                                media_problem_score_sql=None,
                                monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
                                child_weight=[0.35, 0.35, 0.3, 0.00],
                                zhanduan_filter_list=[],
                                title=['监控调阅时长累计({0})/总人数({1})',
                                       '监控调阅发现问题数({0})/总人数({1})',
                                       '监控调阅发现问题质量分累计({0})/总人数({1})',
                                       '调阅班组数({0})/班组数({1})'],
                                calc_score_by_formula=_calc_score_by_formula,
                                fraction_list=[None, None, None, None]):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    if not risk_ids:
        diaoche = get_query_condition_by_risktype(risk_name)
        check_item_ids = diaoche[0]
    else:
        check_item_ids = risk_ids
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_cost_time_sql, media_problem_number_sql,
                  media_problem_score_sql, monitor_watch_discovery_ratio_sqllist
                  ]
    if media_cost_time_sql is None:
        media_func = [
            MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
            MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
        ]
    for hierarchy in [3]:
        score = []
        child_func = [
            _calc_media_time_per_person_major, _calc_media_problem_per_person_major,
            _calc_media_score_per_person_major, _calc_media_banzu_val_workbanzu_major
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load, check_item_ids,
                hierarchy, idx, media_func[idx], title=title, zhanduan_filter_list=zhanduan_filter_list,
                calc_score_by_formula=calc_score_by_formula, fraction=fraction_list[idx])
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def get_ratio_df(work_load, high, middle, low, column='COUNT', is_stable=1):
    ratio_df = work_load[['DEPARTMENT_ID', column]].copy()
    ratio_df['score_1'] = 100
    ratio_df['score_2'] = 90
    ratio_df['score_3'] = 60
    ratio_df['ratio_1'] = (ratio_df[column] ** is_stable) * high
    ratio_df['ratio_2'] = (ratio_df[column] ** is_stable) * middle
    ratio_df['ratio_3'] = (ratio_df[column] ** is_stable) * low
    ratio_df = ratio_df.drop(column, axis=1)
    return ratio_df


def get_work_load_df(
        work_load_sql, other_work_load_sql, major, month):
    df1 = pd_query(work_load_sql.format(major))
    df2 = pd_query(other_work_load_sql.format(month))
    df2 = df2.groupby(['FK_DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    df = pd.concat([df1, df2], sort=False)
    return df
