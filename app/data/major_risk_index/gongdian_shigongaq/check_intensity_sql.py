# 供电生产系统数据(db_mid)
# TYPE1表的（工作票人 * 作业时间）
WORK_TICKET_TYPE1_SQL = """SELECT 
        tu.DEPART_ID,
        TIMESTAMPDIFF(MINUTE, ty.WORK_START_TIME, ty.WORK_END_TIME)  / 60 AS WORK_PERSON_HOURS,
        ty.TOTAL_PERSON_NUM
    FROM 
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        ty.STATUS = 4
        AND DATE_FORMAT(ty.WORK_END_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(ty.WORK_END_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 供电生产系统数据(db_mid)
# 作业车工作票实体类（工作票人 * 作业时间）
# gd_t_work_ticket_for_truck的工作票时司机、副时间，工作人数为2
WORK_TICKET_TRUCK_SQL = """SELECT
        tu.DEPART_ID,
        TIMESTAMPDIFF(MINUTE, tk.WORK_START_TIME, tk.WORK_END_TIME) / 60 AS WORK_PERSON_HOURS,
        2 AS TOTAL_PERSON_NUM
    FROM 
        gd_t_work_ticket_for_truck AS tk
            INNER JOIN
        gd_t_unit AS tu ON tk.FK_UNIT_ID = tu.REALID
    WHERE
        tk.STATUS = 4
        AND DATE_FORMAT(tk.WORK_END_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(tk.WORK_END_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 供电生产系统数据(db_mid)
# 作业车工作票实体类（工作票人 * 作业时间）
# GD_BD_WORK_TICKET
WORK_TICKET_BD_SQL = """SELECT
        tu.DEPART_ID,
        TIMESTAMPDIFF(MINUTE, tk.REAL_DATE, tk.FINISH_JOB_DATE) / 60 AS WORK_PERSON_HOURS,
        IF(day(tk.REAL_DATE) = day(tk.FINISH_JOB_DATE), 
			-hour(tk.REAL_DATE) + hour(tk.FINISH_JOB_DATE), 
            - hour(tk.REAL_DATE) + 24 + hour(tk.FINISH_JOB_DATE)) as hours,
        tk.WORK_LEADER_ID,
        tk.WORK_MAKEUP_PERSON_ID,
        tk.SUBSTATION_GUARDIAN_ID,
        tk.NON_PROFESSIONAL_OTHER, tk.NON_PROFESSIONAL
    FROM 
		gd_bd_work_ticket as tk
            INNER JOIN
        gd_t_unit AS tu ON tk.UNIT_ID = tu.REALID
    WHERE
        tk.STATUS IN (4,5)
        AND DATE_FORMAT(tk.FINISH_JOB_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(tk.FINISH_JOB_DATE, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 供电生产系统数据（db_mid）
# 工作票人、作业时间
# 甲方系统查询使用下列条件
# 第一、二、三种工作票实体类及作业车工作票实体类使用工作票有效期
# START_WORK_DAY	工作票有效时期（开始日期）
# END_WORK_DAY	 工作票有效日期（结束日期）
# 牵引变电所工作票使用---TICKET_DATE	发票日期
WORK_TICKET_SQL = """SELECT
        tu.DEPART_ID, ty.MONITORING_LEADER_SIGN AS GENBAN,
        HOUR(ty.START_WORK_TIME) AS START_WORK_TIME,
        HOUR(ty.END_WORK_TIME) AS END_WORK_TIME
    FROM
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(ty.START_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(ty.END_WORK_DAY, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND ty.STATUS = 4
    UNION ALL SELECT
        tu.DEPART_ID, bd.EXECUTE_CADRE_SIGN AS GENBAN,
        HOUR(bd.REAL_DATE) AS START_WORK_TIME,
        HOUR(bd.FINISH_JOB_DATE) AS END_WORK_TIME
    FROM
        gd_bd_work_ticket AS bd
            INNER JOIN
        gd_t_unit AS tu ON bd.UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND bd.STATUS = 5
"""

# 拼接日期
TRUCK_WORK_TICKET_HOUR_SQL = """SELECT
        DISTINCT 
        tu.DEPART_ID,
        ty.PK_ID,
        ty.WORK_TICKET_CODE as TICKET_ID,
        1 AS TICKET_COUNT,
        (ty.TOTAL_PERSON_NUM + 1) as TOTAL_PERSON_NUM,
        WORK_START_TIME,
        WORK_END_TIME,
        timestampdiff(SECOND, WORK_START_TIME, WORK_END_TIME)
         / 3600 as HOURS
    FROM
        gd_t_work_ticket_type_1 AS ty
            INNER JOIN
        gd_t_unit AS tu ON ty.FK_UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(ty.END_WORK_DAY, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(ty.END_WORK_DAY, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND ty.STATUS = 4
"""

# 变电status=5完结, TICKET_DATE系统使用工作票发票日期
BD_WORK_TICKET_HOUR_SQL = """SELECT
        DISTINCT 
        tu.DEPART_ID,
        bd.PK_ID,
        bd.TICKET_ID,
        1 AS TICKET_COUNT,
        bd.WORK_LEADER_ID,
        bd.WORK_MAKEUP_PERSON_ID,
        bd.SUBSTATION_GUARDIAN_ID,
        bd.NON_PROFESSIONAL_OTHER, bd.NON_PROFESSIONAL,
        bd.REAL_DATE as WORK_START_TIME,
        bd.FINISH_JOB_DATE as WORK_END_TIME,
        (TIMESTAMPDIFF(second,bd.REAL_DATE,bd.FINISH_JOB_DATE) / 3600) AS HOURS
    FROM
        gd_bd_work_ticket AS bd
            INNER JOIN
        gd_t_unit AS tu ON bd.UNIT_ID = tu.REALID
    WHERE
        DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(bd.TICKET_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND bd.STATUS = 5
"""

# 现场检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 查处供电-施工问题数
SHIGONG_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 基础问题库中施工所有问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
        left join 
        t_problem_base_risk as b on a.PK_ID = b.FK_PROBLEM_BASE_ID
    WHERE
        a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""


# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 一般风险及以上问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """SELECT
      distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        AND a.RISK_LEVEL <= 3
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')        
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)  
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 3 AND 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND a.IS_EXTERNAL = 0
            AND a.TYPE = 3
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 AND 4
        AND a.TYPE = 3
        AND a.IS_EXTERNAL = 0
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数--问题数均为站段自查(e.TYPE=3)
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem as e on a.PK_ID= e.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND e.TYPE = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)  
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
"""


# 路局所有班组班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]
