# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     evaluate_intensity_sql
   Author :       hwj
   date：          2019/9/10下午2:50
   Change Activity: 2019/9/10下午2:50
-------------------------------------------------
"""

# 近三个月被评价人次数量(不是b.pk_id)
EVALUATE_PEOPLE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
            DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 近三个月被评价人次分值
ACTIVE_EVALUATE_SCORE_SQL = """
select aess.FK_DEPARTMENT_ID, sum(aess.COUNT) AS COUNT
from
(
SELECT
       distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.SCORE AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            ) as aess
    GROUP BY aess.FK_DEPARTMENT_ID
"""


# 三个月该段干部总数所有评价人次总数
ZHANDUAN_EVALUATE_PEOPLE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_department AS d on d.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""


# （主动）段机关干部评价记分条数
DUAN_CADRE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_check_evaluate_and_risk AS d
            ON d.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
            a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND b.IDENTITY = '干部'
            AND c.TYPE = 7
            AND d.FK_RISK_ID IN ({2})
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 评价记分总条数
EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
            DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 供电下所有风险
GONG_DIAN_ALL_RISK_ID = """
SELECT
    PK_ID
FROM
    `t_risk` 
WHERE
    FK_DEPARTMENT_ID = '{0}' 
    AND IS_DELETE =0
"""