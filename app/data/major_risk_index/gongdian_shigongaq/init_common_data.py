# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/8/17下午6:45
   Change Activity: 2019/8/17下午6:45
-------------------------------------------------
"""
from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shigongaq import GLV
from app.data.major_risk_index.gongdian_shigongaq.check_intensity_sql import TRUCK_WORK_TICKET_HOUR_SQL, \
    BD_WORK_TICKET_HOUR_SQL, WORK_TICKET_SQL
from app.data.major_risk_index.gongdian_shigongaq.common import get_vitual_major_ids, get_ticket_amount, \
    stats_work_ticket

from app.data.major_risk_index.gongdian_shigongaq.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """

    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(
        major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(
        major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(
        major, ids))
    # work_load = get_ticket_amount(
    #     pd_query(
    #         TRUCK_WORK_TICKET_HOUR_SQL.format(*stats_month), db_name='db_mid'),
    #     pd_query(
    #         BD_WORK_TICKET_HOUR_SQL.format(*stats_month), db_name='db_mid'),
    #     department_data)

    risktype_data = get_query_condition_by_risktype(risk_name)
    check_item_ids = risktype_data[0]
    check_risk_ids = risktype_data[1]

    ticket_data = stats_work_ticket(
        pd_query(WORK_TICKET_SQL.format(*stats_month), db_name='db_mid'),
        department_data)
    work_load = ticket_data[0]
    night_work_load = ticket_data[2]
    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'WORK_LOAD': work_load,
        'NIGHT_WORK_LOAD': night_work_load,
        'CHECK_ITEM_IDS': check_item_ids,
        'CHECK_RISK_IDS': check_risk_ids
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
