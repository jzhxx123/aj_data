# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     application_sql
   Author :       hwj
   date：          2019/11/5上午11:17
   Change Activity: 2019/11/5上午11:17
-------------------------------------------------
"""


# 报警总条数
# 微机监测系统中“报警信息分析统计”-“大数据”-“信号机”
WARNING_COUNT_SQL = """
    SELECT
    WORK_SHOP,
    COUNT( PK_ID ) AS COUNT 
FROM
    `t_signal_machine_warning` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d' )
    AND WARNING_LEVEL in ('一级','二级','三级')
GROUP BY
    WORK_SHOP
"""

# 信号机总数
TRACK_CIRCUIT_COUNT_SQL = """SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS COUNT 
FROM
    t_department_other_management 
WHERE
    TYPE_NAME = '信号机总数'
"""

# 故障通知”按键次数以及重复发生次数
# 微机监测系统“开关量”报警统计模块，筛选“故障通知”中包含“信号机或灯”字样出现次数
FAILURE_NOTIFICATION_COUNT_SQL = """SELECT
    WORK_SHOP,
    STATION,
    1 as BUTTON_COUNT,
    1 as REPEAT_COUNT
FROM
    `t_monitoring_switch_warning` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d' )
    AND (PROCESSING_RESULT LIKE '%%信号机%%'
    OR   PROCESSING_RESULT LIKE '%%灯%%')
"""
