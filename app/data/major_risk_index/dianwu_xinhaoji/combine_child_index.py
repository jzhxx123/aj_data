# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/11/6下午1:50
   Change Activity: 2019/11/6下午1:50
-------------------------------------------------
"""

from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_guidaodl.common import get_vitual_major_ids
from app.data.major_risk_index.dianwu_xinhaoji import (
    application, init_common_data, hidden_investigation_intensity,
    hidden_rectification_intensity)
from app.data.major_risk_index.dianwu_xinhaoji.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.util import update_major_maintype_weight


@validate_exec_month
def execute(months_ago):
    risk_name = 149
    risk_type = '电务-10'
    # init_monthly_index_map(months_ago, risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        application,
        hidden_investigation_intensity,
        hidden_rectification_intensity,
    ]:
        func.execute(months_ago, risk_name, risk_type)
    child_weight = [0.4, 0.3, 0.3]
    child_index_list = [8, 9, 10]
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(
        zhanduan_dpid_sql,
        chejian_dpid_sql,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_weight,
        child_index_list=child_index_list)

    update_major_maintype_weight(index_type=10, major=risk_type,
                                 child_index_list=child_index_list, child_index_weight=child_weight)


if __name__ == '__main__':
    pass
