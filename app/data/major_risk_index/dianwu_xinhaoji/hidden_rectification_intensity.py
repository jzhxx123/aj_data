# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_rectification_intensity
   Author :       hwj
   date：          2019/11/6下午1:49
   Change Activity: 2019/11/6下午1:49
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.hidden_rectification_intensity import stats_warning_eliminate_count_ratio, \
    stats_rectification_overdue, stats_warning_control_ratio, get_alarm_disposal_time_count_data
from app.data.major_risk_index.dianwu_xinhaoji import GLV
from app.data.major_risk_index.dianwu_xinhaoji.hidden_rectification_intensity_sql import REPEATED_WARNING_SQL, \
    ALARM_DISPOSAL_TIME_COUNT_SQL, WARNING_ELIMINATE_COUNT_SQL, OVERDUE_PROBLEM_NUMBER_SQL, OVERDUE_DAYS_NUMBER_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index,
    calc_child_index_type_divide)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.3:
        _score = 100
    elif 0.3 > _ratio >= -0.1:
        _score = 100 - 100 * (0.3 - _ratio)
    elif -0.1 > _ratio > -0.3:
        _score = 60 - 300 * (-0.1 - _ratio)
    else:
        _score = 0
    return _score


def _calc_rectification_score(problem_number, deduction_coefficient):
    val = 100 - deduction_coefficient * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        WARNING_ELIMINATE_COUNT, REPEATED_WARNING, PROCESSING_COUNT, ALL_EQUIPMENT, \
        OVERDUE_PROBLEM_NUMBER, OVERDUE_DAYS_NUMBER
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    check_risk_ids = GLV.get_value('CHECK_RISK_IDS')

    # 报警销号指数--报警数量
    WARNING_ELIMINATE_COUNT = pd_query(
        WARNING_ELIMINATE_COUNT_SQL.format(*stats_month), db_name='db_mid')
    # WARNING_ELIMINATE_COUNT = calc_monitoring_problem_count(warning_eliminate_count, ZHANDUAN_DPID_DATA.copy())

    # 重复报警数
    REPEATED_WARNING = pd_query(
        REPEATED_WARNING_SQL.format(*stats_month), db_name='db_mid')

    # 告警及时处置条数及总数
    alarm_disposal_time_count = pd_query(
        ALARM_DISPOSAL_TIME_COUNT_SQL.format(year, month), db_name='db_mid')
    PROCESSING_COUNT, ALL_EQUIPMENT = get_alarm_disposal_time_count_data(alarm_disposal_time_count, ZHANDUAN_DPID_DATA)

    # 超期问题数
    OVERDUE_PROBLEM_NUMBER = pd_query(OVERDUE_PROBLEM_NUMBER_SQL.format(year, month, check_risk_ids))

    # 问题超期消耗数
    OVERDUE_DAYS_NUMBER = pd_query(OVERDUE_DAYS_NUMBER_SQL.format(year, month, check_risk_ids))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 报警销号指数
def _stats_warning_eliminate_count_ratio(months_ago):
    """
    I级报警未销号每条扣20分
    """
    return stats_warning_eliminate_count_ratio(months_ago,
                                               WARNING_ELIMINATE_COUNT,
                                               ZHANDUAN_DPID_DATA,
                                               RISK_TYPE,
                                               _choose_dpid_data)


# 报警控制指数
def _stats_warning_control_ratio(months_ago):
    """
    同一站名、同一设备、同一报警在1-3天内重复发生，每发生一次扣10分；在4-7天内重复发生，每发生一次扣6分；在8-15天内重复发生，每发生一次扣3分
    """
    stats_warning_control_ratio(months_ago, REPEATED_WARNING,
                                ZHANDUAN_DPID_DATA, RISK_TYPE, _choose_dpid_data)


# 报警处置及时率指数
def _stats_alarm_disposal_time_ratio(months_ago):
    """
    及时率=告警及时处置条数/告警总条数
    """
    return calc_child_index_type_divide(
        PROCESSING_COUNT,
        ALL_EQUIPMENT,
        2,
        10,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE
    )


# 整改时效
def _stats_rectification_overdue(months_ago):
    """
    问题超期整改扣分：问题超期整改1条扣1分；问题整改超期1条扣2分
    """
    return stats_rectification_overdue(months_ago,
                                       OVERDUE_PROBLEM_NUMBER,
                                       OVERDUE_DAYS_NUMBER,
                                       DEPARTMENT_DATA,
                                       ZHANDUAN_DPID_DATA,
                                       RISK_TYPE,
                                       _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【道岔设备报警销号指数、道岔设备报警控制指数、返奖率】
    child_index_func = [
        _stats_warning_eliminate_count_ratio,
        _stats_warning_control_ratio,
        _stats_alarm_disposal_time_ratio,
        _stats_rectification_overdue
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    item_weight = [0.3, 0.3, 0.3, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        10,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=10, major=risk_type, main_type=10,
                                 child_index_list=[1, 2, 3, 4], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── hidden_rectification_intensity index has been figured out!')


if __name__ == '__main__':
    pass
