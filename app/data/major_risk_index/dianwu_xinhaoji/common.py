# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/11/5下午5:20
   Change Activity: 2019/11/5下午5:20
-------------------------------------------------
"""

import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def calc_monitoring_problem_count(df_monitoring, zhanduan_dpid):
    """
    微机监测问题数量
    """
    if df_monitoring.empty:
        zhanduan_dpid['COUNT'] = 0
        for i in range(1, 5):
            zhanduan_dpid['MIDDLE_{0}'.format(i)] = 0
        df_monitoring = zhanduan_dpid
    else:
        # 车间关联站段
        df_monitoring['zhanduan_name'] = df_monitoring.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_monitoring.dropna(subset=['zhanduan_name'], inplace=True)
        df_monitoring = df_monitoring.set_index('zhanduan_name')
        df_monitoring.drop(columns=['WORK_SHOP'], inplace=True)

        # 填充缺失数据并将str转换int
        df_monitoring.fillna(0, inplace=True)
        df_monitoring = df_monitoring.replace('', 0).astype(int)
        # 统计总问题数
        df_monitoring = df_monitoring.groupby([df_monitoring.index]).sum()
        df_monitoring['COUNT'] = df_monitoring.apply(
            lambda row: sum(row), axis=1
        )
        # 灯丝断丝
        df_monitoring['MIDDLE_1'] = df_monitoring.apply(
            lambda row: (row['CHANG_DIAN_DENG'] + row['FEI_CHANG_DIAN_DENG']), axis=1)
        # 灯转
        df_monitoring['MIDDLE_2'] = df_monitoring.apply(
            lambda row: (row['DENG_ZHUAN']), axis=1)
        # 点灯单元
        df_monitoring['MIDDLE_3'] = df_monitoring.apply(
            lambda row: (row['DIAN_DENG_DAN_YUAN']), axis=1)
        # 灯座
        df_monitoring['MIDDLE_4'] = df_monitoring.apply(
            lambda row: (row['DENG_ZUO']), axis=1)
        df_monitoring = pd.merge(
            df_monitoring,
            zhanduan_dpid,
            how='right',
            left_index=True,
            right_on='NAME'
        )
    df_monitoring.fillna(0, inplace=True)
    df_monitoring.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return df_monitoring


def get_check_problem_count(problem_number_sql, risk_ids, stats_months, department_data):
    """获取检查问题总数及干部检查问题数"""
    key_items_list = [2713, 2714, 2715, 2716]
    key_items_data_list = []
    key_col_list = []
    all_problem = pd_query(problem_number_sql.format(*stats_months, risk_ids))
    for idx, key in enumerate(key_items_list):
        col = 'MIDDLE_{}_PROBLEM'.format(idx + 1)
        check_problem = all_problem[all_problem.FK_KEY_ITEM_ID == key].copy()
        check_problem = check_problem.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().to_frame(col)
        key_col_list.append(col)
        key_items_data_list.append(check_problem)
    cadre_check_problem = pd.concat(key_items_data_list, axis=1, sort=False)
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem['ALL_PROBLEM'] = cadre_check_problem.apply(
        lambda row: sum(row), axis=1
    )
    key_col_list.append('ALL_PROBLEM')
    cadre_check_problem = pd.merge(
        cadre_check_problem,
        department_data,
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID')
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem = cadre_check_problem.groupby(
        'TYPE3')[key_col_list].sum()
    return cadre_check_problem


def calc_problem_exposure_basic_data(
        monitoring_problem_count,
        cadre_check_problem,
        choose_dpid_data):
    """
    获取隐患排查力度指数--问题暴露度指数的基础数据
    :param monitoring_problem_count: 微机监测问题数量
    :param cadre_check_problem: 干部检查问题数
    :param choose_dpid_data: 部门数据
    :return:
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    basic_data = pd.merge(
        monitoring_problem_count,
        cadre_check_problem,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    basic_data.fillna(0, inplace=True)

    for i in range(1, 5):
        basic_data['SCORE_{0}'.format(i)] = basic_data.apply(
            lambda row: max((row['MIDDLE_{0}'.format(i)] / row['COUNT']
                             - row['MIDDLE_{0}_PROBLEM'.format(i)] / row['ALL_PROBLEM']) * 100, 0)
            if row['COUNT'] > 0 and row['ALL_PROBLEM'] > 0 else 0, axis=1
        )

    tmp_list = [
        '灯丝断丝({0:.2f}) = (灯丝断丝报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-灯丝断丝问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '灯转({0:.2f}) = (灯转报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-灯转问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '点灯单元({0:.2f}) = (点灯单元报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-点灯单元问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '灯座({0:.2f}) = (灯座报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-灯座问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
    ]
    basic_data['CONTENT'] = basic_data.apply(
        lambda row: '<br/>'.join([content.format(
            row['SCORE_{0}'.format(idx + 1)],
            row['MIDDLE_{0}'.format(idx + 1)],
            row['MIDDLE_{0}_PROBLEM'.format(idx + 1)],
            row['COUNT'],
            row['ALL_PROBLEM']
        ) for idx, content in enumerate(tmp_list)]), axis=1
    )
    basic_data['SCORE_a_3'] = basic_data.apply(
        lambda row: max(100 - sum(row[col] for col in ['SCORE_{0}'.format(num) for num in range(1, 5)]), 0), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=basic_data['TYPE3'],
            data=basic_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_data.groupby(['TYPE3'])['SCORE_a_3'].sum().to_frame()
    return calc_df_data, df_rst
