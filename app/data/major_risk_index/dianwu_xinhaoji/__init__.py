# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/11/5上午10:51
   Change Activity: 2019/11/5上午10:51
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)


from . import (application, combine_child_index, hidden_investigation_intensity, hidden_rectification_intensity)
