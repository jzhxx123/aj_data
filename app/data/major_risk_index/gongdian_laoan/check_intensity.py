# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.gongdian_laoan import GLV
from app.data.major_risk_index.gongdian_laoan.check_intensity_sql import (
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLEM_SCORE_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, XIANGCHANG_PROBLEM_SCORE_SQL, ALL_PROBLEM_NUMBER_SQL,
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, TRUCK_WORK_TICKET_HOUR_SQL,
    BD_WORK_TICKET_HOUR_SQL, TICKET_COUNT_SQL)
from app.data.major_risk_index.gongdian_laoan.check_intensity_sql import (
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, )
from app.data.major_risk_index.gongdian_laoan.common import stats_risk_score_per_person, \
    merge_ticket_data, calc_night_work_load, get_check_count, get_media_intensity_basic_data, get_ratio_df, \
    _calc_score_for_by_major_ratio_df
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_media_banzu(row, column, major_column, detail_type=None):
    """
    监控调阅班组数，按100%计算
    :param row:
    :param column:
    :param major_column:
    :param detail_type:
    :return:
    """
    _score = min(100, 100 * row[column])
    return round(_score, 2)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME, RISK_IDS, check_item_ids, stats_month
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, GENBAN_CHECK, YECHA_CHECK, \
        TOTAL_TICKETS, GENBAN_TICKETS, YECHA_TICKETS, YECHA_GENBAN_TICKETS, \
        ALL_PROBLEM_NUMBER, XIANGCHANG_PROBLEM_SCORE, YECHA_WORK_LOAD, MEDIA_FUNC_DATA_DICT, \
        TICKET_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    check_info = GLV.get_value('CHECK_INFO')
    check_info_person = GLV.get_value('CHECK_INFO_PERSON')
    check_info_media_time = GLV.get_value('CHECK_INFO_MEDIA_TIME')
    check_info_department_address = GLV.get_value('CHECK_INFO_DEPARTMENT_ADDRESS')
    stats_month = get_custom_month(months_ago)

    # 统计工作量【施工工作总量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 获取现场检查总次数,夜查次数,跟班次数
    spot_check_count, ye_cha_check_count, gen_ban_check_count = get_check_count(check_info, check_info_person)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(spot_check_count, DEPARTMENT_DATA)
    # 劳安类问题数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA
    )

    # 现场劳安类问题数
    XIANGCHANG_PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(XIANGCHANG_PROBLEM_SCORE_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA
    )
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 跟班次数
    GENBAN_CHECK = df_merge_with_dpid(gen_ban_check_count, DEPARTMENT_DATA)

    # 工作票总数
    TICKET_COUNT = df_merge_with_dpid(
        pd_query(TICKET_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_CHECK = df_merge_with_dpid(ye_cha_check_count, DEPARTMENT_DATA)
    # 夜查工作量
    ticket_data = merge_ticket_data(TRUCK_WORK_TICKET_HOUR_SQL, BD_WORK_TICKET_HOUR_SQL, stats_month)
    YECHA_WORK_LOAD = calc_night_work_load(ticket_data, DEPARTMENT_DATA)

    # 监控调阅力度
    MEDIA_FUNC_DATA_DICT = get_media_intensity_basic_data(stats_month, check_item_ids, RISK_IDS, check_info,
                                                          check_info_person, check_info_media_time,
                                                          check_info_department_address,
                                                          DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
                        + '量化人员及干部检查次数({4})/ 施工工作总量({5})</p>', None]
    ratio_df = get_ratio_df(WORK_LOAD, 3000, 2800, 2500)
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_for_by_major_ratio_df,
        ratio_df=ratio_df)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = '
                        + '劳安项问题数({4})/ 施工工作总量({5})</p>', None]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        ALL_PROBLEM_NUMBER,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """60%）较大及以上劳安风险问题质量分累计/维修、施工、监管（配合）工作量。
    （20%）现场检查较大及以上险问题质量均分=较大和重大安全风险问题质量分累计（现场检查问题）/施工工作总量
    (20%）现场劳安问题质量分/工作量"""

    return stats_risk_score_per_person(
        JIAODA_RISK_SCORE,
        XIANGCHANG_PROBLEM_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = '
                        + '问题质量分累计({4})/ 工作量({5})</p>', None]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 夜查率
def _stats_yecha_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = '
                        + '夜查次数({4})/ 夜间工作量({5})*100%</p>', None]
    return check_intensity.stats_yecha_ratio_type_gd(
        YECHA_CHECK,
        YECHA_WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# 跟班率
def _stats_genban_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>跟班率({3}) = '
                        + '跟班次数({4})/ 工作量({5})*100%</p>', None]
    return check_intensity.stats_genban_ratio(
        GENBAN_CHECK,
        TICKET_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        calc_score_by_formula=_calc_score_by_formula
    )


# # 监控调阅力度
# def _stats_media_intensity(months_ago):
#     title = ['监控调阅时长累计({0})/施工工作总量({1})',
#              '监控调阅发现问题数({0})/施工工作总量({1})', '监控调阅发现问题质量分累计({0})/施工工作总量({1})',
#              '调阅班组数({0})/班组数({1})']
#     media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, check_item_ids)
#     monitor_watch_discovery_ratio_sqllist = [
#         MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month, check_item_ids),
#         MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(check_item_ids)
#     ]
#     return check_intensity.stats_media_intensity_major(
#         DEPARTMENT_DATA,
#         WORK_LOAD,
#         months_ago,
#         RISK_NAME,
#         RISK_TYPE,
#         risk_ids=RISK_IDS,
#         child_weight=[0.25, 0.25, 0.25, 0.25],
#         title=title,
#         choose_dpid_data=_choose_dpid_data,
#         media_cost_time_sql=media_cost_time_sql,
#         media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
#         media_problem_score_sql=MEDIA_PROBLEM_SCORE_SQL,
#         monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    title = ['监控调阅时长累计({0})/施工工作总量({1})',
             '监控调阅发现问题数({0})/施工工作总量({1})', '监控调阅发现问题质量分累计({0})/施工工作总量({1})',
             '调阅班组数({0})/班组数({1})']
    calc_score_by_formula_dict = {
        'media_cost_time': _calc_score_by_formula,
        'media_problem_number': _calc_score_by_formula,
        'media_problem_score': _calc_score_by_formula,
        'monitor_watch_discovery_ratio_list': _calc_score_by_formula_media_banzu,
    }
    return check_intensity.stats_media_intensity_excellent(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        title=title,
        choose_dpid_data=_choose_dpid_data,
        media_func_data_dict=MEDIA_FUNC_DATA_DICT,
        calc_score_by_formula_dict=calc_score_by_formula_dict
    )


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_score_per_person,
        _stats_risk_score_per_person,
        _stats_yecha_ratio,
        _stats_genban_ratio,
        _stats_media_intensity,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'm', 'l']]
    item_weight = [0.25, 0.05, 0.20, 0.1, 0.1, 0.05, 0.25]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 6, 10, 13, 12],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
