# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     assess_intensity_sql
   Author :       hwj
   date：          2019/8/16下午7:53
   Change Activity: 2019/8/16下午7:53
-------------------------------------------------
"""

# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND c.IS_DELETE = 0
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 月度考核总金额(关联问题)
ASSESS_RESPONSIBLE_SQL = """SELECT
    arps.FK_DEPARTMENT_ID, sum(arps.ACTUAL_MONEY) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""

# 月度返奖金额
AWARD_RETURN_SQL = """SELECT
        arps.FK_DEPARTMENT_ID, SUM(arps.ACTUAL_MONEY) AS COUNT
    FROM
(
SELECT
       distinct a.pk_id ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_RISK_ID IN ({2})
) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""

