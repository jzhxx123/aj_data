#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
"""
    author: Qiangsheng
    date: 2019/03/08
"""
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_laoan import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification, init_common_data)
from app.data.major_risk_index.gongdian_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_laoan.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 35
    risk_type = '供电-6'
    ids = get_vitual_major_ids("工电-1")
    major = get_major_dpid(risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    # init_monthly_index_map(months_ago, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(zhanduan_dpid_sql, chejian_dpid_sql,
                                          months_ago, risk_name, risk_type)

    update_major_maintype_weight(6, risk_type)


if __name__ == '__main__':
    pass
