#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    date: 2018/07/31
    desc: 考核力度指数
"""
from flask import current_app

from app.data.index.util import (get_custom_month, )
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.gongdian_laoan import GLV
from app.data.major_risk_index.gongdian_laoan.assess_intensity_sql import \
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_laoan.common import (
    get_vitual_major_ids, _calc_score_for_by_major_ratio)
from app.data.major_risk_index.gongdian_laoan.common_sql import WORK_LOAD_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")

    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    risk_ids = GLV.get_value('CHECK_RISK_IDS')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])

    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major, ids)),
        DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, risk_ids)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month, risk_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = '
                        + '考核问题数({4})/职工总人数 ({5})</p>', None]
    major_ratio = [(0.15, 100), (0.13, 90), (0.1, 60)]
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio
    )


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) = '
                        + '月度考核总金额({4})/ 总人数({5})</p>', None]
    major_ratio = [(25, 100), (23, 90), (20, 60)]
    return assess_intensity.stats_assess_money_per_person_type_one_major(
        ASSESS_RESPONSIBLE_MONEY,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio
    )


# 返奖率
def _stats_award_return_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>返奖率({3}) = '
                        + '月度返奖金额({4})/ 月度考核金额({5})</p>', None]
    return assess_intensity.stats_award_return_ratio(
        AWARD_RETURN_MONEY,
        ASSESS_RESPONSIBLE_MONEY,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.45, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=3,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
