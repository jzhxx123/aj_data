import datetime

import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.gongdian_laoan.check_intensity_sql import MEDIA_PROBLEM_NUMBER_SQL, \
    MEDIA_PROBLEM_SCORE_SQL, WORK_BANZU_COUNT_SQL
from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set, calc_extra_child_score_groupby_major_third, \
    calc_extra_child_score_groupby_major, df_merge_with_dpid
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_exposure(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_score_for_by_major_ratio_df(row):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    n1 = row['ratio_1']
    sn1 = row['score_1']
    n2 = row['ratio_2']
    sn2 = row['score_2']
    n3 = row['ratio_3']
    sn3 = row['score_3']
    c = row['ratio']
    for idx, item in enumerate([n1, n2, n3]):
        if c > item:
            level = idx + 1
            break
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _calc_score_for_by_major_ratio(self_ratio, major_ratio):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    n1 = major_ratio[0][0]
    sn1 = major_ratio[0][1]
    n2 = major_ratio[1][0]
    sn2 = major_ratio[1][1]
    n3 = major_ratio[2][0]
    sn3 = major_ratio[2][1]
    c = self_ratio
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + 10 / (n1 - n2) * (c - n2)
    elif level == 3:
        score = sn3 + 30 / (n2 - n3) * (c - n3)
    else:
        score = 60 / n3 * c
    score = max(0, score)
    score = min(100, score)
    return score


def _cacl_bd_total_person_num(row):
    """按照给定逻辑计算变电所人数
    """
    total_num = 0
    if row.SUBSTATION_GUARDIAN_ID.is_integer():
        id_str = str(row.WORK_LEADER_ID) + ',' + str(row.WORK_MAKEUP_PERSON_ID) + ',' + str(
            int(row.SUBSTATION_GUARDIAN_ID))
    else:
        id_str = str(row.WORK_LEADER_ID) + ',' + str(row.WORK_MAKEUP_PERSON_ID)
    total_num += len(set(id_str.split(',')))
    for field in [
        'NON_PROFESSIONAL_OTHER',
        'NON_PROFESSIONAL'
    ]:
        value = str(row[field])
        if len(value) > 0:
            total_num += len(value.split('、'))
    return total_num


def get_ticket_amount(truck_ticket_data, bd_ticket_data, department_data):
    """
    0.7*(工作票数/基准工作票数量)+0.3*(人时/基准人时)
    基准工作票数量=当月某段最大工作票数量
    基准人时=当月某段最大工作人时
    """
    bd_ticket_data['TOTAL_PERSON_NUM'] = bd_ticket_data.apply(
        lambda row: _cacl_bd_total_person_num(row), axis=1)
    bd_ticket_data.drop(
        columns=[
            'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
            'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
            'NON_PROFESSIONAL'
        ],
        inplace=True)
    ticket_data = pd.concat(
        [truck_ticket_data, bd_ticket_data], axis=0, sort=False)
    ticket_data['TIME_COUNT'] = ticket_data.apply(
        lambda row: int(row['TOTAL_PERSON_NUM']) * max(1, (row['HOURS'] + 24) if row['HOURS'] < 0 else row['HOURS']),
        axis=1)
    # ticket_data['COUNT'] = ticket_data.apply(
    #     lambda row: 0.001 + row['TIME_COUNT'] / 60000, axis=1
    # )

    ticket_data = pd.merge(
        ticket_data,
        department_data,
        how='inner',
        left_on='DEPART_ID',
        right_on='DEPARTMENT_ID')
    ticket_data = ticket_data.groupby(['TYPE3'])['TIME_COUNT', 'TICKET_COUNT'].sum()
    # 基准工作票数量及基准人时
    base_ticket_count = max(ticket_data['TICKET_COUNT'])
    base_time_count = max(ticket_data['TIME_COUNT'])
    ticket_data['COUNT'] = ticket_data.apply(
        lambda row: (
                (0.7 * row['TICKET_COUNT'] / base_ticket_count if base_ticket_count > 0 else 0)
                + (0.3 * row['TIME_COUNT'] / base_time_count if base_time_count > 0 else 0)), axis=1
    )
    ticket_department = pd.merge(
        ticket_data[['COUNT']],
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return ticket_department


def _is_yecha(row):
    """判断是否是夜查，晚上10时到次日6时为夜查
    """
    if row['START_WORK_TIME'] >= 22 or row['END_WORK_TIME'] <= 6:
        return 1
    return 0


def _is_genban(row):
    """跟班率统计工作票上是否有跟班签字，接触网工作票叫做监控干部，
    变配电工作票叫做跟班干部，作业车工作票叫做添乘干部
    """
    if not row['GENBAN']:
        return 0
    if len(row['GENBAN'].strip()) > 0:
        return 1
    return 0


# 计算工作票总数， 跟班工作票数， 夜间工作票数， 夜间跟班工作票数
def stats_work_ticket(work_ticket, department_data):
    work_ticket['IS_YECHA'] = work_ticket.apply(
        lambda row: _is_yecha(row), axis=1)
    work_ticket['IS_GENBAN'] = work_ticket.apply(
        lambda row: _is_genban(row), axis=1)
    work_ticket.drop(
        columns=['START_WORK_TIME', 'END_WORK_TIME', 'GENBAN'], inplace=True)
    # 统计总票数
    total_tickets = pd.merge(
        work_ticket.groupby(['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 跟班总票数
    genban_tickets = pd.merge(
        work_ticket[work_ticket['IS_GENBAN'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查总票数
    yecha_tickets = pd.merge(
        work_ticket[work_ticket['IS_GENBAN'] == 1].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    # 夜查跟班总票数
    yecha_genban_tickets = pd.merge(
        work_ticket[(work_ticket['IS_GENBAN'] == 1)
                    & (work_ticket['IS_YECHA'] == 1)].groupby(
            ['DEPART_ID']).size().to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return total_tickets, genban_tickets, yecha_tickets, yecha_genban_tickets


# def _calc_problem_work_load_score()


def _calc_risk_score_per_person_major(series, work_load, choose_dpid_data, hierarchy,
                                      idx, zhanduan_filter_list, title, calc_score_by_formula=_calc_score_by_formula):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data['ratio'] = data.apply(lambda row: row['score'] / row['PERSON_NUMBER'] if
    row['PERSON_NUMBER'] > 0 else 0, axis=1)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula, zhanduan_filter_list,
        numerator='score', denominator='PERSON_NUMBER')
    # 中间计算数据
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(
            f'{round(row["score"], 2)}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_risk_score_major(df_xianchang, work_load, choose_dpid_data, hierarchy,
                              idx, zhanduan_filter_list, title,
                              calc_score_by_formula=_calc_score_by_formula):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_major(sc_risk_score, work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula)


def _calc_xc_problem_count_major(df_problem, work_load, choose_dpid_data, hierarchy,
                                 idx, zhanduan_filter_list, title,
                                 calc_score_by_formula=_calc_score_by_formula):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_problem_count = df_problem.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_major(sc_problem_count, work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula)


def _calc_risk_score_major(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                           calc_score_by_formula=_calc_score_by_formula):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person_major(risk_score, work_load, choose_dpid_data,
                                             hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula)


# 较大风险问题质量均分
def stats_risk_score_per_person(df_jiaoda,
                                df_problem,
                                df_xianchang,
                                work_load,
                                months_ago,
                                risk_type,
                                child_weight=[0.6, 0.2, 0.2],
                                choose_dpid_data=None,
                                calc_score_by_formula=_calc_score_by_formula,
                                zhanduan_filter_list=[],
                                title=[
                                    '较大和重大安全风险问题质量分累计({0})/工作量({1})',
                                    '现场检查劳安问题质量分累计({0})/工作量({1})',
                                    '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
                                ]):
    """(较大和重大安全风险问题质量分累计*60% +
     （20%）现场劳安问题/工作量
     +现场检查发现较大和重大安全风险问题质量分累计*20%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_problem, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in [3]:
        score = []
        child_func = [_calc_risk_score_major, _calc_xc_problem_count_major, _calc_xc_risk_score_major]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx, zhanduan_filter_list, title,
                                             calc_score_by_formula=calc_score_by_formula,
                                             )
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False,
                           ratio_df=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula_exposure
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER', ratio_df=ratio_df)


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False,
                                 ratio_df=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(
        prob_number, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        ratio_df=ratio_df)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False,
                                ratio_df=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(
        prob_score, work_load, weight, hierarchy,
        choose_dpid_data, calc_score_formula,
        is_calc_score_base_major=is_calc_score_base_major,
        ratio_df=ratio_df)


# 总体暴露度
def stats_total_problem_exposure_type(
        check_item_ids, check_problem_sql, all_load, zuoye_load,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True, ratio_df_dict=None,
        weight_item=[0.3, 0.3, 0.2, 0.2],
        weight_part=[0.4, 0.6]):
    hierarchy = 3
    stats_month = get_custom_month(months_ago)
    func_key = ['总问题数', '一般及以上问题数']
    score_formula_dict = {
        '总问题数': _calc_score_for_by_major_ratio_df
    }
    if not title:
        title = [
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})']
    if not ratio_df_dict:
        ratio_df_dict = {}
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    rst_child_score = []

    data_dict = {
        "总问题数": base_data,
        '一般及以上问题数': risk_data
    }
    # 保存中间过程计算数据
    calc_basic_data = []
    # 导出中间过程
    work_load_dict = {
        "总问题数": all_load,
        '一般及以上问题数': zuoye_load
    }
    for i, key in enumerate(func_key):
        data = data_dict.get(key)
        work_load = work_load_dict.get(key)
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i,
                     title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    score = []
    for i, key in enumerate(func_key):
        data = data_dict.get(key)
        work_load = work_load_dict.get(key)
        calc_score_formula = score_formula_dict.get(key)
        ratio_df_list = ratio_df_dict.get(key)
        # 人均问题数，人均质量分
        for j, func in enumerate(
                [_calc_prob_number_per_person, _calc_prob_score_per_person]):
            weight = weight_item[i] * weight_part[j]
            if ratio_df_list:
                ratio_df = ratio_df_list[j]
            else:
                ratio_df = None
            sub_score = func(data.copy(), work_load, department_data,
                             choose_dpid_data, weight, hierarchy,
                             calc_score_formula=calc_score_formula,
                             is_calc_score_base_major=is_calc_score_base_major,
                             ratio_df=ratio_df)
            score.append(sub_score)
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_a_{hierarchy}'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        choose_dpid_data(hierarchy),
        column,
        hierarchy,
        2,
        5,
        1,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score


def _calc_work_load_oneday(time, flag=0):
    workload_a = 0
    zero = (datetime.datetime.strptime(time[:10] + '00:00:00',
                                       "%Y-%m-%d%H:%M:%S")).timestamp()
    six = (datetime.datetime.strptime(time[:10] + '06:00:00',
                                      "%Y-%m-%d%H:%M:%S")).timestamp()
    twentytw0 = (datetime.datetime.strptime(time[:10] + '22:00:00',
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
    twentyfour = (datetime.datetime.strptime(time[:10] + '23:59:59',
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    calc_value = (datetime.datetime.strptime(time,
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    if flag == 0:
        if zero <= calc_value <= six:
            workload_a = six - calc_value + twentyfour - twentytw0
        elif six < calc_value <= twentytw0:
            workload_a = twentyfour - twentytw0
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = twentyfour - calc_value
    else:
        if zero <= calc_value <= six:
            workload_a = calc_value - zero
        elif six < calc_value <= twentytw0:
            workload_a = six - zero
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = calc_value - twentytw0 + six - zero
    return workload_a


def _calc_work_load_otherday(start, end, zero, six, twentytw0, twentyfour):
    from datetime import date
    workload = 0
    distance = date(int(end[:4]), int(end[5:7]), int(end[8:10])) - \
               date(int(start[:4]), int(start[5:7]), int(start[8:10]))
    days = int(distance.days)
    if days > 1:
        workload = _calc_work_load_oneday(start) + \
                   (days - 1) * (six - zero + twentyfour - twentytw0) + \
                   _calc_work_load_oneday(end, 1)
    else:
        workload = _calc_work_load_oneday(start) + _calc_work_load_oneday(end, 1)
    return workload


def _calc_work_load_sameday(start, end, zero, six, twentytw0, twentyfour):
    workload = 0
    if zero <= start < end <= six or twentytw0 <= start < end <= twentyfour:
        workload = end - start
    elif start < six < end <= twentytw0:
        workload = six - start
    elif start <= six < twentytw0 < end <= twentyfour:
        workload = six - start + end - twentyfour
    elif six <= start < twentytw0 < end <= twentyfour:
        workload = end - twentytw0
    return workload


def _calc_start_time(row):
    start_times = str(row["WORK_START_TIME"])
    end_times = str(row['WORK_END_TIME'])
    work_load_list = []
    try:
        start = (datetime.datetime.strptime(start_times.replace(' ', ''),
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
        end = (datetime.datetime.strptime(end_times.replace(' ', ''),
                                          "%Y-%m-%d%H:%M:%S")).timestamp()
        flag = True if start_times[:10] == end_times[:10] else False

        six = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '06:00:00',
                                          "%Y-%m-%d%H:%M:%S")).timestamp()
        zero = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '00:00:00',
                                           "%Y-%m-%d%H:%M:%S")).timestamp()
        twentytw0 = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '22:00:00',
                                                "%Y-%m-%d%H:%M:%S")).timestamp()
        twentyfour = (datetime.datetime.strptime(start_times.replace(' ', '')[:10] + '23:59:59',
                                                 "%Y-%m-%d%H:%M:%S")).timestamp()
        if flag:
            work_load_list.append(_calc_work_load_sameday(start, end, zero,
                                                          six, twentytw0, twentyfour))
        else:
            work_load_list.append(_calc_work_load_otherday(
                start_times.replace(' ', ''),
                end_times.replace(' ', ''),
                zero, six, twentytw0, twentyfour))
    except Exception as e:
        work_load_list.append(0)
    row["SECOND"] = sum(work_load_list)
    return row


def merge_ticket_data(night_truck_work_ticket_hour_sql, night_bd_work_ticket_hour_sql, stats_month):
    """
    合并不同工作票
    :param night_truck_work_ticket_hour_sql: 作业车工作票
    :param night_bd_work_ticket_hour_sql: 牵引工作票
    :param stats_month:
    :return:
    """
    night_ticket_hour = pd_query(night_truck_work_ticket_hour_sql.format(
        *stats_month), db_name='db_mid')

    night_bd_work_ticket_hour = pd_query(night_bd_work_ticket_hour_sql.format(
        *stats_month), db_name='db_mid')

    night_bd_work_ticket_hour['TOTAL_PERSON_NUM'] = night_bd_work_ticket_hour.apply(
        lambda row: _cacl_bd_total_person_num(row), axis=1)
    night_bd_work_ticket_hour.drop(
        columns=[
            'WORK_LEADER_ID', 'WORK_MAKEUP_PERSON_ID',
            'SUBSTATION_GUARDIAN_ID', 'NON_PROFESSIONAL_OTHER',
            'NON_PROFESSIONAL'
        ],
        inplace=True)

    ticket_data = pd.concat(
        [night_ticket_hour, night_bd_work_ticket_hour], axis=0, sort=False)

    return ticket_data


def calc_night_work_load(df_work_time, dpid_data):
    """
    计算夜间工作量
    :param df_work_time:
    :param dpid_data:
    :return:
    """
    df_work_time = df_work_time.apply(
        lambda row: _calc_start_time(row),
        axis=1)
    df_work_time.fillna(0)
    df_work_time["TIME_COUNT"] = (df_work_time["SECOND"] * df_work_time["TOTAL_PERSON_NUM"]) / 3600
    df_work_time['COUNT'] = df_work_time.apply(
        lambda row: 0.001 + row['TIME_COUNT'] / 60000, axis=1
    )
    data = pd.merge(
        df_work_time,
        dpid_data,
        how="inner",
        left_on="DEPART_ID",
        right_on="DEPARTMENT_ID"
    )
    return data


def get_all_check_item_ids(all_check_item_data):
    """
    获取专业下所有检查项目ids
    :param all_check_item_data: 所有的检查项目
    :return:
    """
    ids_list = all_check_item_data['PK_ID'].values.tolist()
    check_item_ids = ','.join([str(ids) for ids in ids_list])
    return check_item_ids


def get_check_count(check_info, check_info_person):
    """
    获取检查力度中的现场检查次数,跟班次数,夜查次数
    :param check_info: 符号检查项目的所有检查信息
    :param check_info_person: 所有的检查次数查询sql
    :return:
    """
    basic_check_count = pd.merge(check_info, check_info_person, on='CHECK_INFO_ID')
    basic_check_count = basic_check_count[
        (basic_check_count.CHECK_TYPE < 400)
        & (~basic_check_count.CHECK_TYPE.isin([102, 103]))
        ]
    # 现场检查次数
    spot_check_count = basic_check_count[basic_check_count.CHECK_WAY.isin([1, 2])]
    # 夜查次数
    ye_cha_check_count = spot_check_count[spot_check_count.IS_YECHA == 1]
    # 跟班次数
    gen_ban_check_count = spot_check_count[spot_check_count.IS_GENBAN == 1]
    all_count = spot_check_count.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    ye_cha_check_count = ye_cha_check_count.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    gen_ban_check_count = gen_ban_check_count.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    return all_count, ye_cha_check_count, gen_ban_check_count


def get_media_intensity_basic_data(
        stats_month,
        check_item_ids,
        check_risk_ids,
        check_info,
        check_info_person,
        check_info_media_time,
        check_info_department_address,
        department_data):
    """
    获取监控调阅力度的基础数据
    :return:
    """
    check_info = check_info[
        (check_info.CHECK_TYPE < 400)
        & (~check_info.CHECK_TYPE.isin([102, 103]))
        & (check_info.CHECK_WAY == 3)]
    check_info_person = check_info_person[['CHECK_INFO_ID', 'FK_DEPARTMENT_ID']]

    # 监控调阅时长
    media_cost_time = pd.merge(check_info, check_info_media_time, on='CHECK_INFO_ID')
    media_cost_time = pd.merge(media_cost_time, check_info_person, on='CHECK_INFO_ID')
    media_cost_time = df_merge_with_dpid(media_cost_time, department_data, how='right')
    media_cost_time.fillna(0, inplace=True)
    # 监控调阅发现问题数
    media_problem_number = pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, check_risk_ids))
    media_problem_number = df_merge_with_dpid(media_problem_number, department_data, how='right')
    media_problem_number.fillna(0, inplace=True)
    # 监控调阅问题质量分
    media_problem_score = pd_query(MEDIA_PROBLEM_SCORE_SQL.format(*stats_month, check_risk_ids))
    media_problem_score = df_merge_with_dpid(media_problem_score, department_data, how='right')
    media_problem_score.fillna(0, inplace=True)
    # 作业班组数(具备检查项目的总班组数)
    work_department = pd_query(
        WORK_BANZU_COUNT_SQL.format(check_item_ids))
    work_department_count = df_merge_with_dpid(work_department, department_data, how='right')
    work_department_count.fillna(0, inplace=True)

    # 调阅班组数(查询去检查的所有班组与总班组取交集)
    watch_media_department = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    watch_media_department = watch_media_department[['FK_DEPARTMENT_ID', 'COUNT']]
    watch_media_department = watch_media_department.drop_duplicates('FK_DEPARTMENT_ID', keep='first')
    watch_media_department = pd.merge(
        watch_media_department,
        work_department[['FK_DEPARTMENT_ID']],
        how='inner',
        on='FK_DEPARTMENT_ID'
    )
    watch_media_department_count = df_merge_with_dpid(watch_media_department, department_data, how='right')
    watch_media_department_count.fillna(0, inplace=True)
    # 调阅班组,总班组列表
    monitor_watch_discovery_ratio_list = [watch_media_department_count, work_department_count]

    media_func_data_dict = {
        'media_cost_time': media_cost_time,
        'media_problem_number': media_problem_number,
        'media_problem_score': media_problem_score,
        'monitor_watch_discovery_ratio_list': monitor_watch_discovery_ratio_list,
    }
    return media_func_data_dict


def get_check_address_evenness_basic_data(
        check_info,
        check_info_department_address,
        check_info_point_address):
    """获取班组受检次数,地点受检次数"""
    check_info = check_info[check_info.CHECK_WAY.isin([1, 2])]
    banzu_department_checked_count = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    banzu_department_checked_count = banzu_department_checked_count.groupby(
        ['FK_DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    banzu_department_checked_count.rename(
        columns={'FK_DEPARTMENT_ID': 'DEPARTMENT_ID', 'COUNT': 'CHECK_COUNT'}, inplace=True)
    check_point_checked_count = pd.merge(check_info, check_info_point_address, on='CHECK_INFO_ID')
    check_point_checked_count = check_point_checked_count.groupby(
        ['CHECK_POINT_ID'])['COUNT'].sum().to_frame(name='CHECK_COUNT').reset_index()
    return banzu_department_checked_count, check_point_checked_count


def get_check_address_ratio_basic_data(
        banzu_point_sql,
        check_point_sql,
        check_info,
        check_info_department_address,
        check_info_point_address):
    """获取覆盖率基础数据"""
    # 总班组数
    banzu_point = pd_query(banzu_point_sql)
    # 总地点数
    check_point = pd_query(check_point_sql)
    # 检查班组数
    real_check_banzu = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    real_check_banzu = real_check_banzu[['FK_DEPARTMENT_ID', 'COUNT']]
    real_check_banzu = real_check_banzu.drop_duplicates('FK_DEPARTMENT_ID', keep='first')
    real_check_banzu = pd.merge(
        banzu_point[['FK_DEPARTMENT_ID']],
        real_check_banzu,
        on='FK_DEPARTMENT_ID')
    # 检查地点数
    real_check_point = pd.merge(check_info, check_info_point_address, on='CHECK_INFO_ID')
    real_check_point = real_check_point[['CHECK_POINT_ID', 'COUNT']]
    real_check_point = real_check_point.drop_duplicates('CHECK_POINT_ID', keep='first')
    real_check_point = pd.merge(
        check_point[['FK_DEPARTMENT_ID', 'CHECK_POINT_ID']],
        real_check_point,
        on='CHECK_POINT_ID')
    check_point = check_point.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    real_check_point = real_check_point.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()

    return banzu_point, check_point, real_check_banzu, real_check_point


def get_check_time_evenness_basic_data(check_info, check_info_person):
    """获取检查时间均衡度基础信息--每日检查数"""
    check_info = check_info[check_info.CHECK_WAY < 4]
    daily_check_count = pd.merge(check_info, check_info_person, on='CHECK_INFO_ID')
    daily_check_count.groupby(['FK_DEPARTMENT_ID', 'DAY'])['COUNT'].sum().reset_index()
    return daily_check_count


def get_ratio_df(work_load, high, middle, low, column='COUNT'):
    ratio_df = work_load[['DEPARTMENT_ID', column]].copy()
    ratio_df['score_1'] = 100
    ratio_df['score_2'] = 90
    ratio_df['score_3'] = 60
    ratio_df['ratio_1'] = ratio_df[column] * high
    ratio_df['ratio_2'] = ratio_df[column] * middle
    ratio_df['ratio_3'] = ratio_df[column] * low
    ratio_df = ratio_df.drop(column, axis=1)
    return ratio_df
