# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     evaluate_intensity_sql
   Author :       hwj
   date：          2019/9/7下午2:03
   Change Activity: 2019/9/7下午2:03
-------------------------------------------------
"""

# 评价记分总分数(指定风险)
EVALUATE_SCORE_PART_SQL = """SELECT
    mp.FK_DEPARTMENT_ID, SUM(mp.SCORE) AS COUNT
    FROM
(SELECT
   DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, a.SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.`CODE` NOT LIKE 'TX-%%'
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
) as mp 
GROUP BY mp.FK_DEPARTMENT_ID
"""


# 评价记分总分数
EVALUATE_SCORE_ALL_SQL = """SELECT
    mp.FK_DEPARTMENT_ID, SUM(mp.SCORE)  AS COUNT
    FROM
(SELECT
   DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, a.SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.`CODE` NOT LIKE 'TX-%%'
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as mp 
GROUP BY mp.FK_DEPARTMENT_ID
"""


# 站段劳安问题评价分(CHECK_TYPE = 2)
ACTIVE_EVALUATE_SCORE_SQL = """SELECT
    mp.FK_DEPARTMENT_ID, SUM(mp.SCORE) AS COUNT
    FROM
(SELECT
   DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, a.SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.CHECK_TYPE = 2
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.`CODE` NOT LIKE 'TX-%%'
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as mp 
GROUP BY mp.FK_DEPARTMENT_ID
"""


# 路局对站段的评价(CHECK_TYPE = 1)
LUJU_EVALUATE_COUNT_SQL = """SELECT
    mp.FK_DEPARTMENT_ID, SUM(mp.SCORE) AS COUNT
    FROM
(SELECT
   DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, a.SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.CHECK_TYPE = 1
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.`CODE` NOT LIKE 'TX-%%'
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as mp 
GROUP BY mp.FK_DEPARTMENT_ID
"""
