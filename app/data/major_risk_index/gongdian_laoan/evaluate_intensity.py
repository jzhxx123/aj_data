#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_laoan import GLV
from app.data.major_risk_index.gongdian_laoan.common import _calc_score_for_by_major_ratio
from app.data.major_risk_index.gongdian_laoan.evaluate_intensity_sql import EVALUATE_SCORE_PART_SQL, \
    EVALUATE_SCORE_ALL_SQL, LUJU_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid,
    summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global EVALUATE_SCORE_ALL, EVALUATE_SCORE_PART, CHEJIAN_DPID_DATA, \
        ZHANDUAN_DPID_DATA, LUJU_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    risk_ids = GLV.get_value('CHECK_RISK_IDS')
    stats_month = get_custom_month(months_ago)

    # 评价记分总分数(指定风险)
    EVALUATE_SCORE_PART = df_merge_with_dpid(
        pd_query(EVALUATE_SCORE_PART_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 评价记分总分数
    EVALUATE_SCORE_ALL = df_merge_with_dpid(
        pd_query(EVALUATE_SCORE_ALL_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 站段劳安问题评价分
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 路局对段劳安问题评价分
    LUJU_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(LUJU_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 主动评价记分占比
def _stats_evaluate_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价力度({3}) = '
                        + '评价记分（因劳安风险被评价）({4})/ 当月评价总分({5})*100%</p>', None]
    # 各个站段的分数
    major_ratio = [(0.3, 100), (0.25, 90), (0.15, 60)]
    return evaluate_intensity.stats_evaluate_ratio_type_gongdian(
        EVALUATE_SCORE_PART, EVALUATE_SCORE_ALL, months_ago, RISK_TYPE,
        _choose_dpid_data, customizecontent=customizecontent,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio
    )


def _stats_evaluate_difference(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>履职评价分数差（站段与路局）({3}) = '
                        + '履职评价分数（站段）({4}) - 履职评价分数（路局）({5})</p>', None]
    return evaluate_intensity.stats_evaluate_count_difference_type_gongdian(
        ACTIVE_EVALUATE_COUNT, LUJU_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data, customizecontent=customizecontent)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [_stats_evaluate_ratio, _stats_evaluate_difference]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['h', 'j']]
    item_weight = [0.6, 0.4]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=2,
                                 child_index_list=[8, 10],
                                 child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')


if __name__ == '__main__':
    pass
