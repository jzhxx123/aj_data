# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            INNER JOIN
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        mp.FK_DEPARTMENT_ID, SUM(mp.SCORE_STANDARD*mp.GRADATION_RATIO)*{3} AS SCORE,
        COUNT(mp.pk_id) AS COUNT
FROM
(SELECT
DISTINCT b.FK_DEPARTMENT_ID, a.SCORE_STANDARD, d.GRADATION_RATIO, a.pk_id
FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person_gradation_ratio AS d
            ON a.FK_PERSON_GRADATION_RATIO_ID = d.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    ) as mp
    GROUP BY mp.FK_DEPARTMENT_ID;
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_problem_base_risk as e on a.FK_PROBLEM_BASE_ID = e.FK_PROBLEM_BASE_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND e.FK_RISK_ID IN ({2})
        AND b.IS_DELETE = 0
"""

# 库内问题（实际中年问题）复查数
IMPORTANT_PROBLEM_RECHECK_COUNT_SQL = """SELECT
    mp.FK_DEPARTMENT_ID, SUM(mp.PROBLEM_REVIEW_NUMBER) AS COUNT
from 
(
SELECT
DISTINCT c.FK_DEPARTMENT_ID, a.PROBLEM_REVIEW_NUMBER, a.PK_ID   
FROM
    t_check_info AS a
        LEFT JOIN
    t_check_info_and_item AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
) as mp
GROUP BY mp.FK_DEPARTMENT_ID
"""


# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        DISTINCT b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_problem_base AS c ON c.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_problem_base AS d ON d.PK_ID = c.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_problem_base_risk AS e ON e.FK_PROBLEM_BASE_ID = d.PK_ID
    WHERE
        e.FK_RISK_ID in ({2})
        AND b.RESPONSIBILITY_DIVISION_NAME = '涉及局属单位责任'
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 以下涉及隐患库问题都(不包含已销号)
# 隐患库隐患数量
PERIL_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
    GROUP BY FK_DEPARTMENT_ID;
"""

# 延期隐患
PERIL_OVERDUE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
            AND COMPLETE_DATE < CURDATE()
    GROUP BY FK_DEPARTMENT_ID;
"""

# 长期整治
PERIL_PERIOD_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
            AND COMPLETE_DATE >= CURDATE()
            AND IS_PERIOD_RECTIFY = 1
    GROUP BY FK_DEPARTMENT_ID;
"""


# 检查出的的隐患
CHECKED_PERIL_ID_SQL = """SELECT
        DISTINCT c.FK_DEPARTMENT_ID, c.PK_ID AS PID
    FROM
        t_check_info_and_safety_peril_lib AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_peril_lib AS c ON a.FK_SAFETY_PERIL_LIB_ID = c.PK_ID     
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.RISK_TYPE_IDS in ({2})
    ;
"""

# 隐患库里的隐患
PERIL_ID_SQL = """SELECT
        FK_DEPARTMENT_ID, PK_ID AS PID
    FROM
        t_safety_peril_lib
    WHERE
        HIERARCHY = 3 AND `STATUS` < 5
        AND RISK_TYPE_IDS in ({0});
"""

# 长期整治没录入的
PERIL_RECTIFY_NO_ENTRY_SQL = """SELECT
        c.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_safety_peril_lib_and_person AS a
            INNER JOIN
        t_safety_peril_lib AS b ON a.FK_SAFETY_PERIL_LIB_ID = b.PK_ID
            INNER JOIN
        t_safety_peril_lib_and_department AS c
            ON a.FK_SAFETY_PERIL_AND_DEPARTMENT_ID = c.PK_ID
    WHERE
        b.IS_PERIOD_RECTIFY = 1
            AND b.HIERARCHY = 3
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_TYPE_IDS in ({2})
"""

# 警告性预警延期--CREATE_TIME:延长时间（开始延长）,HIERARCHY(路局层),TYPE=2(延长预警考核)
WARNING_DELAY_SQL = """
SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_warning_notification_assess
    WHERE
        HIERARCHY = 1
        AND
        TYPE = 2
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY FK_DEPARTMENT_ID
"""

# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where (b.TYPE2 = '{0}' or b.TYPE3 in {1})
    and b.is_delete = 0
    and a.is_delete = 0
"""