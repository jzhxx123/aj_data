#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_laoan import GLV
from app.data.major_risk_index.dianwu_laoan.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, BANZU_CONNECT_DEPARTMENT_SQL, CHECK_POINT_COUNT_SQL,
    CHECK_POINT_CONNECT_DEPARTMENT_SQL, DAILY_CHECK_BANZU_COUNT_H_SQL)
from app.data.major_risk_index.dianwu_laoan.common import get_check_address_evenness_basic_data, get_vitual_major_ids, \
    get_check_time_evenness_basic_data, df_merge_with_dpid
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

# 需要过滤不参与基数计算的站段列表
ZHANDUAN_FILTER_LIST = ['99990002001499A20007']


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, CHECK_ADDRESS_DATA, \
        BANZU_CONNECT_DEPARTMENT, BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_COUNT, \
        CHECK_POINT_CONNECT_DEPARTMENT, CHECK_POINT_CHECKED_COUNT, CHECK_BANZU_COUNT, \
        DAILY_CHECK_COUNT, HOUR_CHECK_COUNT, DAILY_CHECK_BANZU_COUNT, DAILY_CHECK_BANZU_COUNT_H

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')

    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    check_info = GLV.get_value('CHECK_INFO')
    check_info_department_address = GLV.get_value('CHECK_INFO_DEPARTMENT_ADDRESS')
    check_info_point_address = GLV.get_value('CHECK_INFO_POINT_ADDRESS')
    check_info_person = GLV.get_value('CHECK_INFO_PERSON')

    # 一般以上项点问题数(风险大类“劳动”的问题)
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数(风险大类“劳动”的问题)
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL), DEPARTMENT_DATA)

    # 检查时间均衡-每日检查数
    DAILY_CHECK_COUNT, HOUR_CHECK_COUNT = get_check_time_evenness_basic_data(check_info, check_info_person)
    # 检查时间均衡-每日班组数
    DAILY_CHECK_BANZU_COUNT = pd_query(DAILY_CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))
    # 检查时间均衡-每日班组数
    DAILY_CHECK_BANZU_COUNT_H = pd_query(DAILY_CHECK_BANZU_COUNT_H_SQL.format(CHECK_ITEM_IDS))

    # 班组受检次数, 重要检查点受检次数
    BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_CHECKED_COUNT = get_check_address_evenness_basic_data(
        check_info, check_info_department_address, check_info_point_address)

    # 检查班组数统计
    CHECK_BANZU_COUNT = pd_query(CHECK_BANZU_COUNT_SQL.format(major, ids))

    # 班组关联站段(选取部门全称)
    BANZU_CONNECT_DEPARTMENT = pd_query(BANZU_CONNECT_DEPARTMENT_SQL.format(major, ids))

    # 重要检查点
    CHECK_POINT_COUNT = pd_query(CHECK_POINT_COUNT_SQL.format(major, ids))

    # 重要检查点关联部门(选取部门全称)
    CHECK_POINT_CONNECT_DEPARTMENT = pd_query(CHECK_POINT_CONNECT_DEPARTMENT_SQL.format(major, ids))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = '
                        + '查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>', None]
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data, customizecontent=customizecontent)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    return check_evenness.stats_check_time_evenness_three_df(
        DAILY_CHECK_BANZU_COUNT, DAILY_CHECK_COUNT,
        DAILY_CHECK_BANZU_COUNT_H, HOUR_CHECK_COUNT,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """
    每个地点受检次数超过比较值600%以上的一处扣2分;受检次数低于比较值50%的一处扣2分，未检查的一处扣5分。
    （每个地点的比较值=该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)，受检地点的工作量越大，比较值越高，本单位现场检查）
    检查地点中，每处检查点、重要检查点人数依据车间班组配置的该点作业人数为准，若配置的主要生产场所点作业人数为0或空，则取整个班组作业人数。
    若一个检查点的关联单位为车间，同样去主要生产场所中配置的改地点作业人数，若配置的作业人数为0或空，则取该车间的直属人数，即人员部门为车间单位的人数
    """
    return check_evenness.stats_check_address_evenness_new(
        CHECK_BANZU_COUNT, CHECK_POINT_COUNT,
        BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_CHECKED_COUNT,
        CHECK_POINT_CONNECT_DEPARTMENT, BANZU_CONNECT_DEPARTMENT,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=5, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3], child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
