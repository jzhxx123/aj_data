#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2019/03/08
'''

from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_laoan import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification, init_common_data)
from app.data.major_risk_index.dianwu_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.dianwu_laoan.common_sql import (
    CHEJIAN_DPID_SQL, ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 30
    risk_type = '电务-5'
    ids = get_vitual_major_ids("工电-1")
    major = get_major_dpid(risk_type)
    # init_monthly_index_map(months_ago, risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    child_weight = [0.3, 0.1, 0.1, 0.15, 0.25, 0.1]
    zhanduan_dpid_sql = ZHANDUAN_DPID_SQL.format(major, ids)
    chejian_dpid_sql = CHEJIAN_DPID_SQL.format(major, ids)
    combine_child_index.merge_child_index(
        zhanduan_dpid_sql,
        chejian_dpid_sql,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_weight)

    update_major_maintype_weight(index_type=5, major=risk_type,
                                 child_index_list=[1, 2, 3, 4, 5, 6], child_index_weight=child_weight)


if __name__ == '__main__':
    pass
