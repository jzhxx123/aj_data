import pandas as pd

from app.data.index.util import get_months_from_201712
from app.data.major_risk_index.dianwu_laoan.check_intensity_sql import MEDIA_PROBLEM_NUMBER_SQL, WORK_BANZU_COUNT_SQL
from app.data.major_risk_index.gongdian_laoan.check_intensity_sql import MEDIA_PROBLEM_SCORE_SQL
from app.data.major_risk_index.util import export_basic_data_one_field_monthly, \
    calc_child_index_type_sum
from app.data.util import pd_query


def df_merge_with_dpid(data, dpid_data, how='right'):
    """将指数计算需要的数据跟部门单位连接后获取单位dpid
    Arguments:
        data {dataFrame} -- 指数数据
        dpid_data {dataFrame} -- 部门单位(参与计算的)

    Keyword Arguments:
        how {str} -- 连接方式 (default: {'inner'})

    Returns:
        dataFrame --各单位的数据
    """
    data = pd.merge(
        data,
        dpid_data,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.fillna(0, inplace=True)
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_work_hour_ctsc_person_num(row):
    """按照给定逻辑计算变电所人数
    """
    total_num = 0
    all_person = []
    for field in [
        'REALITY_ADMINISTRATOR', 'REALITY_PRINCIPAL', 'REALITY_TESTER',
        'INDOOR_PERSON', 'OUTDOOR_PERSON'
    ]:
        if len(str(row[field])) > 1:
            all_person.extend(str(row[field]).split(','))
    filter_word = ['', '/', '无', '空']
    for each in set(all_person):
        if each.strip() not in filter_word:
            total_num += 1
    return total_num


# def calc_work_load(work_hour, work_hour_ctsc, department_data):
#     """工作量= 工作人数×工作小时数"""
#     work_hour_ctsc['PERSON_COUNT'] = work_hour_ctsc.apply(
#         lambda row: _calc_work_hour_ctsc_person_num(row), axis=1)
#     work_hour_ctsc.drop(
#         columns=[
#             'REALITY_ADMINISTRATOR', 'REALITY_PRINCIPAL', 'REALITY_TESTER',
#             'INDOOR_PERSON', 'OUTDOOR_PERSON'
#         ],
#         inplace=True)
#     work_hour = pd.concat([work_hour, work_hour_ctsc], axis=0, sort=False)
#     work_hour.fillna(0, inplace=True)
#     work_hour['COUNT'] = work_hour.apply(
#         lambda row: int(row['PERSON_COUNT']) * max(1, (row['HOURS'] + 24) if row['HOURS'] < 0 else row['HOURS']),
#         axis=1)
#     work_hour = work_hour.groupby(['DEPARTMENT_ID'])['COUNT'].sum()
#     work_hour_department = pd.merge(
#         work_hour.to_frame(name='COUNT'),
#         department_data,
#         how='inner',
#         left_index=True,
#         right_on='DEPARTMENT_ID')
#     return work_hour_department


def calc_work_load(work_hour, work_hour_ctsc, department_data):
    """工作量= 工作人数×工作小时数"""

    work_hour_ctsc = pd.merge(
        work_hour_ctsc,
        department_data['DEPARTMENT_ID'].to_frame(),
        how="right", left_on='DEPARTMENT_ID', right_on='DEPARTMENT_ID')
    work_hour_ctsc.fillna(0, inplace=True)

    work_hour_ctsc['PERSON_COUNT'] = work_hour_ctsc.apply(
        lambda row: _calc_work_hour_ctsc_person_num(row), axis=1)

    work_hour_ctsc.drop(
        columns=[
            'REALITY_ADMINISTRATOR', 'REALITY_PRINCIPAL', 'REALITY_TESTER',
            'INDOOR_PERSON', 'OUTDOOR_PERSON',
        ],
        inplace=True)
    work_hour.replace('', 0, inplace=True)
    work_hour = pd.concat([work_hour, work_hour_ctsc], axis=0, sort=False)
    work_hour.fillna(0, inplace=True)
    work_hour['COUNT'] = work_hour.apply(
        lambda row: float(row['PERSON_COUNT']) * max(1, (row['HOURS'] + 24) if row['HOURS'] < 0 else row['HOURS']),
        axis=1)
    work_hour = work_hour.groupby(['DEPARTMENT_ID'])['COUNT'].sum()
    work_hour_department = pd.merge(
        work_hour.to_frame(name='COUNT'),
        department_data,
        how='inner',
        left_index=True,
        right_on='DEPARTMENT_ID')
    return work_hour_department


# 事故隐患问题暴露度
def stats_problem_exposure(check_item_ids, zhanduan_dpid,
                           hidden_key_problem_sql,
                           hidden_key_problem_month_sql, department_data,
                           months_ago, risk_type, choose_dpid_data):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712(months_ago)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(
        pd_query(hidden_key_problem_sql.format(check_item_ids))['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(
                hidden_key_problem_month_sql.format(
                    mon[0], mon[1], check_item_ids))['PID'].values)
        if 2 < idx < (len(stats_months) - 1):
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            # exposure_problem = hidden_problem.intersection(
            #     month_hidden_problem)
            # for each_problem in exposure_problem:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 1
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        department_data,
        choose_dpid_data(3),
        5,
        8,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数',
        risk_type=risk_type)
    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob,
        2,
        5,
        8,
        months_ago,
        'SCORE',
        'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def get_all_check_item_ids(all_check_item_data):
    """
    获取专业下所有检查项目ids
    :param all_check_item_data: 所有的检查项目
    :return:
    """
    ids_list = all_check_item_data['PK_ID'].values.tolist()
    check_item_ids = ','.join([str(ids) for ids in ids_list])
    return check_item_ids


def get_check_count(check_info, check_info_person):
    """
    获取检查力度中的现场检查次数,跟班次数,夜查次数
    :param check_info: 符号检查项目的所有检查信息
    :param check_info_person: 所有的检查次数查询sql
    :return:
    """
    basic_check_count = pd.merge(check_info, check_info_person, on='CHECK_INFO_ID')
    basic_check_count = basic_check_count[
        (basic_check_count.CHECK_TYPE < 400)
        & (~basic_check_count.CHECK_TYPE.isin([102, 103]))
        ]
    # 现场检查次数
    spot_check_count = basic_check_count[basic_check_count.CHECK_WAY.isin([1, 2])]
    all_count = spot_check_count.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    return all_count


def get_media_intensity_basic_data(
        stats_month,
        check_item_ids,
        check_risk_ids,
        check_info,
        check_info_person,
        check_info_media_time,
        check_info_department_address,
        department_data):
    """
    获取监控调阅力度的基础数据
    :return:
    """
    check_info = check_info[
        (check_info.CHECK_TYPE < 400)
        & (~check_info.CHECK_TYPE.isin([102, 103]))
        & (check_info.CHECK_WAY == 3)]
    check_info_person = check_info_person[['CHECK_INFO_ID', 'FK_DEPARTMENT_ID']]

    # 监控调阅时长
    media_cost_time = pd.merge(check_info, check_info_media_time, on='CHECK_INFO_ID')
    media_cost_time = pd.merge(media_cost_time, check_info_person, on='CHECK_INFO_ID')
    media_cost_time = df_merge_with_dpid(media_cost_time, department_data)
    # 监控调阅发现问题数
    media_problem_number = pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, check_risk_ids))
    media_problem_number = df_merge_with_dpid(media_problem_number, department_data)
    # 监控调阅问题质量分
    media_problem_score = pd_query(MEDIA_PROBLEM_SCORE_SQL.format(*stats_month, check_risk_ids))
    media_problem_score = df_merge_with_dpid(media_problem_score, department_data)
    # 作业班组数(具备检查项目的总班组数)
    work_department = pd_query(
        WORK_BANZU_COUNT_SQL.format(check_item_ids))
    work_department_count = df_merge_with_dpid(work_department, department_data)

    # 调阅班组数(查询去检查的所有班组与总班组取交集)
    watch_media_department = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    watch_media_department = watch_media_department[['FK_DEPARTMENT_ID', 'COUNT']]
    watch_media_department = watch_media_department.drop_duplicates('FK_DEPARTMENT_ID', keep='first')
    watch_media_department = pd.merge(
        watch_media_department,
        work_department[['FK_DEPARTMENT_ID']],
        how='inner',
        on='FK_DEPARTMENT_ID'
    )
    watch_media_department_count = df_merge_with_dpid(watch_media_department, department_data)
    # 调阅班组,总班组列表
    monitor_watch_discovery_ratio_list = [watch_media_department_count, work_department_count]

    media_func_data_dict = {
        'media_cost_time': media_cost_time,
        'media_problem_number': media_problem_number,
        'media_problem_score': media_problem_score,
        'monitor_watch_discovery_ratio_list': monitor_watch_discovery_ratio_list,
    }
    return media_func_data_dict


def get_check_address_evenness_basic_data(
        check_info,
        check_info_department_address,
        check_info_point_address):
    """获取班组受检次数,地点受检次数"""
    check_info = check_info[check_info.CHECK_WAY.isin([1, 2])]
    banzu_department_checked_count = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    banzu_department_checked_count = banzu_department_checked_count.groupby(
        ['FK_DEPARTMENT_ID'])['COUNT'].sum().reset_index()
    banzu_department_checked_count.rename(
        columns={'FK_DEPARTMENT_ID': 'DEPARTMENT_ID', 'COUNT': 'CHECK_COUNT'}, inplace=True)
    check_point_checked_count = pd.merge(check_info, check_info_point_address, on='CHECK_INFO_ID')
    check_point_checked_count = check_point_checked_count.groupby(
        ['CHECK_POINT_ID'])['COUNT'].sum().to_frame(name='CHECK_COUNT').reset_index()
    return banzu_department_checked_count, check_point_checked_count


def get_check_address_ratio_basic_data(
        banzu_point_sql,
        check_point_sql,
        check_info,
        check_info_department_address,
        check_info_point_address):
    """获取覆盖率基础数据"""
    # 总班组数
    banzu_point = pd_query(banzu_point_sql)
    # 总地点数
    check_point = pd_query(check_point_sql)
    # 检查班组数
    real_check_banzu = pd.merge(check_info, check_info_department_address, on='CHECK_INFO_ID')
    real_check_banzu = real_check_banzu[['FK_DEPARTMENT_ID', 'COUNT']]
    real_check_banzu = real_check_banzu.drop_duplicates('FK_DEPARTMENT_ID', keep='first')
    real_check_banzu = pd.merge(
        banzu_point[['FK_DEPARTMENT_ID']],
        real_check_banzu,
        on='FK_DEPARTMENT_ID')
    # 检查地点数
    real_check_point = pd.merge(check_info, check_info_point_address, on='CHECK_INFO_ID')
    real_check_point = real_check_point[['CHECK_POINT_ID', 'COUNT']]
    real_check_point = real_check_point.drop_duplicates('CHECK_POINT_ID', keep='first')
    real_check_point = pd.merge(
        check_point[['FK_DEPARTMENT_ID', 'CHECK_POINT_ID']],
        real_check_point,
        on='CHECK_POINT_ID')
    check_point = check_point.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    real_check_point = real_check_point.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()

    return banzu_point, check_point, real_check_banzu, real_check_point


def get_check_time_evenness_basic_data(check_info, check_info_person):
    """获取检查时间均衡度基础信息--每日检查数, 时段检查数"""

    check_info = check_info[(check_info.CHECK_WAY < 3)
                            & (check_info.CHECK_TYPE < 400)
                            & (~check_info.CHECK_TYPE.isin([102, 103]))]
    daily_check_count = pd.merge(check_info, check_info_person, on='CHECK_INFO_ID')
    hour_check_count = daily_check_count.copy()
    daily_check_count.groupby(['FK_DEPARTMENT_ID', 'DAY'])['COUNT'].sum().reset_index()
    hour_check_count.groupby(['FK_DEPARTMENT_ID', 'START_HOUR', 'END_HOUR'])['COUNT'].sum().reset_index()
    return daily_check_count, hour_check_count



