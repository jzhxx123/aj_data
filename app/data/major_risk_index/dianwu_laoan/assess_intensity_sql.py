# 月度考核总金额-劳安
ASSESS_RESPONSIBLE_SQL = """select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""

# 月度返奖金额
AWARD_RETURN_SQL = """SELECT
        arps.FK_DEPARTMENT_ID, SUM(arps.ACTUAL_MONEY) AS COUNT
    FROM
(
SELECT
       distinct a.pk_id ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_RISK_ID IN ({2})
) as arps
    GROUP BY arps.FK_DEPARTMENT_ID
"""
