# 一般以上项点问题数
# 风险大类“劳动”的问题
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        f.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND a.TYPE = 3
            and f.IS_DELETE = 0
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND f.RISK_TYPE_NAME = '劳动' 
    GROUP BY f.FK_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND a.RISK_TYPE_NAME = '劳动'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
    q.FK_DEPARTMENT_ID,
    MAX(q.WORK_TYPE) AS WORK_TYPE
FROM
(
SELECT 
    b.FK_DEPARTMENT_ID, b.WORK_TYPE AS WORK_TYPE
FROM
    t_department as a
    INNER JOIN
    t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    INNER JOIN
    t_check_item AS c ON b.SOURCE_ID = c.PK_ID  
WHERE
         a.TYPE BETWEEN 9 AND 10
        AND a.IS_DELETE = 0
        AND b.WORK_TYPE IS NOT NULL
        AND c.PK_ID IN ({0})
UNION
SELECT 
    b.FK_DEPARTMENT_ID, b.WORK_TYPE AS WORK_TYPE
FROM
    t_department as a
    INNER JOIN
    t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    INNER JOIN
    t_check_item AS c ON b.SOURCE_ID = c.PARENT_ID   
WHERE
         a.TYPE BETWEEN 9 AND 10
        AND a.IS_DELETE = 0
        AND b.WORK_TYPE IS NOT NULL
        AND c.PK_ID IN ({0})
) as q
GROUP BY q.FK_DEPARTMENT_ID
"""

# 时段作业班组数
DAILY_CHECK_BANZU_COUNT_H_SQL = """SELECT 
    b.FK_DEPARTMENT_ID, b.WORK_TYPE, b.WORK_TIME
FROM
    t_department as a
    INNER JOIN
    t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    INNER JOIN
    t_check_item AS c ON b.SOURCE_ID = c.PK_ID  
WHERE
         a.TYPE = 9
        AND a.IS_DELETE = 0
        AND b.WORK_TYPE IS NOT NULL
        AND c.PK_ID IN ({0})
UNION
SELECT 
    b.FK_DEPARTMENT_ID, b.WORK_TYPE, b.WORK_TIME
FROM
    t_department as a
    INNER JOIN
    t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    INNER JOIN
    t_check_item AS c ON b.SOURCE_ID = c.PARENT_ID   
WHERE
         a.TYPE = 9
        AND a.IS_DELETE = 0
        AND b.WORK_TYPE IS NOT NULL
        AND c.PK_ID IN ({0})
"""


# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT DISTINCT
    tcp.PK_ID AS CHECK_POINT_ID,
    tcp.ALL_NAME AS ADDRESS_NAME,
    tds.SOURCE_DEPARTMENT_ID,
    tds.PERSON_NUMBER 
FROM
    t_check_point AS tcp
    INNER JOIN
    t_department_and_main_production_site AS tds ON tcp.PK_ID = tds.FK_ADDRESS_ID 
WHERE
    tds.TYPE = 2
    AND tcp.TYPE = 1 
    AND tcp.IS_DELETE = 0
"""

# 重要检查点-专业下所有具备检查项目的部门-不关联项目
CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
    b.DEPARTMENT_ID as FK_DEPARTMENT_ID,
    COUNT( DISTINCT c.PK_ID ) AS PERSON_COUNT 
FROM
    t_department AS b
    INNER JOIN t_person AS c ON b.DEPARTMENT_ID = c.FK_DEPARTMENT_ID 
WHERE
    (b.TYPE2 = '{0}' or b.TYPE3 in {1}) 
    AND b.IS_DELETE = 0 
    AND c.IS_DELETE = 0 
GROUP BY
    b.DEPARTMENT_ID
"""


# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS PERSON_COUNT_2
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND (a.TYPE2 = '{0}' or a.TYPE3 in {1})
    GROUP BY a.DEPARTMENT_ID;
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME
    FROM
        t_department
    WHERE
        TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND (TYPE2 = '{0}' or TYPE3 in {1})
"""
