# 基数选择:选择专业内连续3个月无责任事故、故障（机务、电务不统计D21事故）的单位、
# 月份（3个月）的均值指数作为专业基数参考。（多次比较得出基数）。若找不出相应比较单位，
# 找出选择3个月故障率（无责任事故）（每个月）最低的单位均数上浮20%作为专业基数。
# 电务专业：机务优先选择以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
BASE_UNIT_SQL = """SELECT DISTINCT
    a.FK_DEPARTMENT_ID
FROM
    t_safety_produce_info_responsibility_unit AS a
        LEFT JOIN
    t_safety_produce_info AS b ON b.PK_ID = a.FK_SAFETY_PRODUCE_INFO_ID
WHERE
    a.RESPONSIBILITY_IDENTIFIED < 11
        AND b.MAIN_TYPE < 3
        AND ((b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {})
        OR (b.SURE_YEAR = {} AND b.SURE_MONTH = {}))
"""

# 检查次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.CHECK_WAY BETWEEN 1 AND 2
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 较大及以上风险问题数(量化人员及干部)
RISK_LEVEL_PROBLEM_SQL = """SELECT
    a.FK_DEPARTMENT_ID, SUM(b.PROBLEM_SCORE) AS COUNT
FROM
    t_check_problem_and_responsible_department AS a
        LEFT JOIN
    t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
WHERE
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
            or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
        AND b.TYPE = 3
        AND b.RISK_LEVEL <= 2
GROUP BY a.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """SELECT
    a.FK_DEPARTMENT_ID, SUM(b.PROBLEM_SCORE) AS COUNT
FROM
    t_check_problem_and_responsible_department AS a
        LEFT JOIN
    t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
        LEFT JOIN
    t_check_info AS c  ON a.FK_CHECK_INFO_ID = c.PK_ID
WHERE
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
            or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
        AND b.TYPE = 3
        AND b.RISK_LEVEL <= 2
        AND c.CHECK_WAY BETWEEN 1 AND 2
GROUP BY a.FK_DEPARTMENT_ID;
"""

# 问题质量分累计(量化人员及干部)
PROBLEM_CHECK_SCORE_SQL = """SELECT
    a.FK_DEPARTMENT_ID, SUM(b.PROBLEM_SCORE) AS COUNT
FROM
    t_check_problem_and_responsible_department AS a
        LEFT JOIN
    t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
WHERE
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
            or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
        AND b.TYPE = 3
GROUP BY a.FK_DEPARTMENT_ID;
"""

# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
        FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 管理项问题数
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
        FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND b.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY a.FK_DEPARTMENT_ID;
"""


# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_YECHA = 1
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 实际重要检查地点数
REAL_CHECK_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 实际检查班组数
REAL_CHECK_BANZU_SQL = """SELECT DISTINCT
        a.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.HIERARCHY = 5
        AND c.IS_DELETE = 0
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0
        AND HIERARCHY = 2
    GROUP BY FK_DEPARTMENT_ID;
"""
# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0;
"""

# 监控调阅时长
MEDIA_COST_TIME_SQL = """SELECT
        c.FK_DEPARTMENT_ID, SUM(a.COST_TIME) AS TIME
    FROM
        t_check_info_and_media AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.CHECK_WAY = 3
    GROUP BY c.FK_DEPARTMENT_ID;
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND c.CHECK_WAY BETWEEN 3 AND 4
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """SELECT
        a.FK_DEPARTMENT_ID, SUM(b.PROBLEM_SCORE) AS SCORE
    FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND c.CHECK_WAY BETWEEN 3 AND 4
    GROUP BY a.FK_DEPARTMENT_ID;
"""
