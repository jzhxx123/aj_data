# 月度返奖金额
AWARD_RETURN_SQL = """SELECT
        b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
"""


# 考核作业项问题数
KAOHE_ZY_CHECK_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
    t_check_problem_and_responsible_department AS a
        LEFT JOIN
    t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
            AND b.IS_ASSESS = 1
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 管理项考核问题数 - 管理项+设备质量
KAOHE_GL_CHECK_PROBLEM_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS NUMBER
    FROM
    t_check_problem_and_responsible_department AS a
        LEFT JOIN
    t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND (b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
                or b.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
                or b.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
            AND b.TYPE = 3
            AND b.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
            AND b.IS_ASSESS = 1
    GROUP BY a.FK_DEPARTMENT_ID;
"""