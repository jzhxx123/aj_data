#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
import pandas as pd
from flask import current_app

from app.data.index.common import (
    calc_extra_child_score_groupby_major,
    combine_and_format_basic_data_to_mongo, combine_child_index_func,
    df_merge_with_dpid, summizet_child_index, summizet_operation_set)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.chewu_laoan import GLV
from app.data.major_risk_index.chewu_laoan.assess_intensity_sql import \
    KAOHE_GL_CHECK_PROBLEM_SQL, KAOHE_ZY_CHECK_PROBLEM_SQL

from app.data.major_risk_index.chewu_laoan.common_sql import (
    CADRE_COUNT_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL,
    WORK_LOAD_SQL, WORKER_COUNT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.common.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, FEIGANBU_ASSESS_RESPONSIBLE_SQL,
    GANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.chewu_laoan.common import get_vitual_major_ids, get_major_radio, \
    _calc_score_for_by_major_ratio


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global PROBLEM_COUNT, ZHANDUAN_DPID_DATA, WORKER_LOAD, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, \
        PERSON_LOAD, ASSESS_ZUOYE_PROBLEM_COUNT, ASSESS_GUANLI_PROBLEM_COUNT, \
        CADRE_COUNT, WORKER_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER,\
        WORKER_ASSESS_RESPONSIBLE_MONEY
    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))

    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL), DEPARTMENT_DATA)

    # 单位总人数
    PERSON_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL), DEPARTMENT_DATA)
    WORKER_LOAD = pd.concat([WORKER_COUNT, ZHANDUAN_STAFF], axis=0, sort=False)
    # 考核作业项问题数
    ASSESS_ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_ZY_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 考核管理项问题数
    ASSESS_GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            KAOHE_GL_CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额（干部）
    CADRE_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(GANBU_ASSESS_RESPONSIBLE_SQL.format(year, month)),
        DEPARTMENT_DATA)
    # 月度考核总金额（非干部）
    WORKER_ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(FEIGANBU_ASSESS_RESPONSIBLE_SQL.format(year, month)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    major_ratio = get_major_radio('换算单位考核问题数')

    return assess_intensity.stats_check_problem_assess_radio_type_two_type_chewu(
        ASSESS_ZUOYE_PROBLEM_COUNT,
        ASSESS_GUANLI_PROBLEM_COUNT,
        WORKER_LOAD,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        child_weight=[0.9, 0.1],
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    fraction_list = GLV.get_value('stats_assess_money_per_person', (None, None))
    return assess_intensity.stats_assess_money_per_person_type_two(
        CADRE_COUNT, CADRE_ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER,
        WORKER_ASSESS_RESPONSIBLE_MONEY, months_ago, RISK_TYPE,
        _choose_dpid_data, fraction_list=fraction_list, child_weight=[0.1, 0.9])


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100 if detail_type != 3 else 100 * (1 - _ratio)
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100 if detail_type == 3 else 100 * (1 + _ratio)
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b']]
    item_weight = [0.5, 0.5]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=2, major=risk_type, main_type=3, child_index_list=[1, 2],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
