# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
from flask import current_app

from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.chewu_laoan.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.chewu_laoan.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL, HAPPEN_PROBLEM_POINT_SQL,
    RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL)
from app.data.index.util import (get_query_condition_by_risktype)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.chewu_laoan.common import get_vitual_major_ids


SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        PROBLEM_POINT_INFO_DATA, WORKER_COUNT

    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL)
    WORKER_COUNT = pd_query(WORK_LOAD_SQL)


# 整改时效
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_ITEM_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 整改成效
def _stats_rectification_effect(months_ago):
    return problem_rectification.stats_rectification_effect(
        CHECK_ITEM_IDS, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 问题反复
def _stats_repeatedly_index(months_ago):
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 15,
    }
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_repeatedly_index,
        _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'f']]
    item_weight = [0.4, 0.6, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=2, major=risk_type, main_type=6, child_index_list=[1, 3, 6],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
