#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         19-6-27下午3:32
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    BELONG_PROFESSION_ID = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = BELONG_PROFESSION_ID.get(major, 898)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def get_major_radio(detail_type_name):
    """
    获取子指数专业基数
    :param detail_type_name: 子指数名称
    :return:
    """
    major_ratio_dict = {
        '换算单位检查频次': [(1.9, 100), (1.6, 90), (1.0, 60)],
        '查处问题率': [(0.11, 100), (0.08, 90), (0.05, 60)],
        '换算单位人均质量分': [(0.35, 100), (0.30, 90), (0.18, 60)],
        '换算单位考核问题数': [(0.08, 100), (0.06, 90), (0.04, 60)],
    }
    major_ratio = major_ratio_dict.get(detail_type_name)
    return major_ratio


def _calc_score_for_by_major_ratio(self_ratio, major_ratio):
    """记分规则
    N:基础线	基数对应分值Sn*（基础分）	C（各站段实际比值）	逻辑公式
    1.1(N1)	100	C>=N1	    S=S(n1)
    0.9(N2)	90	N1>C>=N2	S=Sn2+(C-N2)*(Sn1-Sn2)/(N1-N2)
    0.5(N3)	60	N2>C>=N3	S=Sn3+(C-N3)*((Sn2-Sn3)/(N2-N3))
                N3>C	    S=Sn3+(C-N3)*(((Sn2-Sn3)/(N2-N3))*2)
    Arguments:
        self_ratio {float} -- 站段本身的值，即公式中的C
        major {str} -- 专业名称
    """
    # 所属档次，最低为4
    level = 4
    for idx, item in enumerate(major_ratio):
        if self_ratio > item[0]:
            level = idx + 1
            break
    n1 = major_ratio[0][0]
    sn1 = major_ratio[0][1]
    n2 = major_ratio[1][0]
    sn2 = major_ratio[1][1]
    n3 = major_ratio[2][0]
    sn3 = major_ratio[2][1]
    c = self_ratio
    if level == 1:
        score = sn1
    elif level == 2:
        score = sn2 + (c - n2) * (sn1 - sn2) / (n1 - n2)
    elif level == 3:
        score = sn3 + (c - n3) * ((sn2 - sn3) / (n2 - n3))
    else:
        score = sn3 + (c - n3) * (((sn2 - sn3) / (n2 - n3)) * 2)
    score = max(0, score)
    score = min(100, score)
    return score
