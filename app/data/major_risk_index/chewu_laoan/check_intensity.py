# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.chewu_laoan import GLV
from app.data.major_risk_index.chewu_laoan.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_COUNT_SQL, GUANLI_CHECK_PROBLEM_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    PROBLEM_CHECK_SCORE_SQL, REAL_CHECK_BANZU_SQL, RISK_LEVEL_PROBLEM_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL, ZUOYE_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.chewu_laoan.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL, WORKER_COUNT_SQL, CADRE_COUNT_SQL)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.chewu_laoan.common import get_vitual_major_ids, get_major_radio, \
    _calc_score_for_by_major_ratio


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global PERSON_LOAD, CADRE_COUNT, WOKER_COUNT, WORKER_LOAD, CHECK_COUNT, \
        GUANLI_PROBLEM_COUNT, ZUOYE_PROBLEM_COUNT, PROBLEM_SCORE, \
        YECHA_COUNT, JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE
    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 统计工作量【职工总人数】
    month = int(stats_month[1][5:7])
    # 干部职工总人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 干部数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(CADRE_COUNT_SQL), DEPARTMENT_DATA)
    # 非干部
    WORKER_COUNT = df_merge_with_dpid(
        pd_query(WORKER_COUNT_SQL), DEPARTMENT_DATA)
    # 单位总人数（干部职工+外聘）
    PERSON_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    WORKER_LOAD = pd.concat([WORKER_COUNT, ZHANDUAN_STAFF], axis=0, sort=False)
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(GUANLI_CHECK_PROBLEM_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    major_ratio = get_major_radio('换算单位检查频次')
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    major_ratio = get_major_radio('查处问题率')
    return check_intensity.stats_check_problem_ratio_type_two_type_chewu(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORKER_LOAD,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        child_weight=[0.9, 0.1],
        choose_dpid_data=_choose_dpid_data,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    fraction_list = GLV.get_value('stats_risk_score_per_person', (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 人均质量分
def _stats_score_per_person(months_ago):
    major_ratio = get_major_radio('换算单位人均质量分')
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        PERSON_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        major_ratio=major_ratio,
        calc_score_by_formula=_calc_score_for_by_major_ratio)


# 夜查率
def _stats_yecha_ratio(months_ago):
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        WORKER_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = list(fraction_list)
    fraction_list.append(None)
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORKER_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=MEDIA_COST_TIME_SQL,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        fraction_list=tuple(fraction_list))


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio(
        REAL_CHECK_BANZU_SQL,
        BANZU_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person, _stats_score_per_person,
        _stats_yecha_ratio, _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'j', 'g', 'i']]
    item_weight = [0.15, 0.15, 0.2, 0.2, 0.15, 0.1, 0.05]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd', 'f', 'h', 'i']]
    # item_weight = [0.25, 0.2, 0.08, 0.25, 0.9, 0.13]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 1, months_ago,
    #                      item_name, item_weight, [4])
    update_major_maintype_weight(index_type=2, major=risk_type, main_type=1, child_index_list=[2, 3, 5, 6, 10, 7, 9],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
