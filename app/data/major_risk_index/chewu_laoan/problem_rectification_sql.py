# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND (c.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or c.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or c.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or c.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or c.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or c.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
            or c.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND a.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
        -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (a.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%'
            or a.PROBLEM_DIVIDE_NAMES LIKE '%%劳安%%')
        AND b.IS_DELETE = 0
"""

# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_problem_base AS c ON c.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_problem_base AS d ON d.PK_ID = c.FK_PROBLEM_BASE_ID
    WHERE
        b.RESPONSIBILITY_DIVISION_NAME = '局属单位责任'
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (d.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or d.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or d.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or d.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or d.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or d.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%')
"""

# 重复发生问题信息
REPEATE_HAPPEN_PROBLEM_SQL = """
SELECT 
    MAX(a.FK_PROBLEM_BASE_ID) AS FK_PROBLEM_BASE_ID,
    MAX(a.RISK_LEVEL) AS RISK_LEVEL,
    MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
    1 AS COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <=3
        AND (a.CHECK_ITEM_NAME LIKE '%%车务-接发列车（普）-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-接发列车（高）-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-行车劳安%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-调车-劳动安全%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-货装劳安（货）%%'
            or a.CHECK_ITEM_NAME LIKE '%%车务-客运劳安%%')
GROUP BY a.PK_ID
"""

# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
    and b.is_delete = 0
    and a.is_delete = 0
"""
