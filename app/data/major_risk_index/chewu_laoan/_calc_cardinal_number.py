# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     _calc_cardinal_number
   Author :       hwj
   date：          2019/11/21下午5:10
   Change Activity: 2019/11/21下午5:10
-------------------------------------------------
"""

from app.data.index.util import get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_laoan import GLV
from app.data.major_risk_index.chewu_laoan.check_intensity_sql import (
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    YECHA_CHECK_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL)
from app.data.major_risk_index.chewu_laoan.common import get_vitual_major_ids
from app.data.major_risk_index.chewu_laoan.common_sql import (
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL,
    DEPARTMENT_SQL, CADRE_COUNT_SQL, WORKER_COUNT_SQL)
from app.data.major_risk_index.common.assess_intensity_sql import FEIGANBU_ASSESS_RESPONSIBLE_SQL, \
    GANBU_ASSESS_RESPONSIBLE_SQL
from app.data.major_risk_index.common.cardinal_number_common import calc_cardinal_number
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import BASE_UNIT_INFO_SQL
from app.data.major_risk_index.common.const import (
    IndexDivider, CommonCalcDataType, PROBLEM_SCORE_INFO,
    JIAODA_RISK_SCORE_INFO, YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO,
    MEDIA_PROBLEM_NUMBER_INFO, MEDIA_PROBLME_SCORE_INFO, GANBU_ASSESS_RESPONSIBLE_INFO,
    FEIGANBU_ASSESS_RESPONSIBLE_INFO, ALL_PROBLEM_NUMBER_INFO, ZUOYE_PROBLEM_COUNT_INFO,
    ZUOYE_PROBLEM_CHECK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO, WORKER_COUNT_INFO, STAFF_NUMBER_INFO, CADRE_COUNT_INFO, PERSON_LOAD_INFO)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import ABOVE_YIBAN_PROBLEM_NUMBER_SQL, \
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL, ALL_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL, \
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL, ZUOYE_PROBLEM_CHECK_SCORE_SQL, ZUOYE_CHECK_PROBLEM_SQL
from app.data.util import pd_query


# 外聘人员数 - 站段
EMPLOYED_OUTSIDE_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
    AND b.is_delete = 0
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, CALC_MONTH, \
        CHECK_ITEM_IDS, RISK_IDS, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, STAFF_NUMBER, WORKER_COUNT_DATA
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    risk_config = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_config[0]
    RISK_IDS = risk_config[1]
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    month = int(STATS_MONTH[1][5:7])
    CALC_MONTH = end, start
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    # 职工总人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL)
    # 非干部
    WORKER_COUNT_DATA = pd_query(WORKER_COUNT_SQL)


# ------------------------获取比值型相应基数------------------------ #

def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    major_dpid = get_major_dpid(risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, major_dpid)

    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 干部数
    cadre_count = CommonCalcDataType(*CADRE_COUNT_INFO)
    cadre_count.value = [CADRE_COUNT_SQL]

    # 作业人数
    worker_count = CommonCalcDataType(*WORKER_COUNT_INFO)
    worker_count.value = [WORKER_COUNT_DATA, EMPLOYED_OUTSIDE_PERSON_SQL]

    # 总人数
    person_load = CommonCalcDataType(*PERSON_LOAD_INFO)
    person_load.value = [STAFF_NUMBER, EMPLOYED_OUTSIDE_PERSON_SQL]

    # 单位职工数
    staff_number_data = CommonCalcDataType(*STAFF_NUMBER_INFO)
    staff_number_data.value = [STAFF_NUMBER]

    # ---------检查力度-----------
    # 问题质量分
    problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    problem_check_score.value = [
        PROBLEM_CHECK_SCORE_SQL]

    # 较大问题质量分
    jiaoda_risk_score = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    jiaoda_risk_score.value = [RISK_LEVEL_PROBLEM_SQL]

    # 现场检查较大问题质量分
    xc_jiaoda_risk_score = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    xc_jiaoda_risk_score.value = [XIANCHENG_RISK_LEVEL_PROBLEM_SQL]

    # 夜查次数
    yecha_count = CommonCalcDataType(*YECHA_COUNT_INFO)
    yecha_count.value = [YECHA_CHECK_SQL]

    # 监控调阅时长
    media_cost_time = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    media_cost_time.value = [MEDIA_COST_TIME_SQL]

    # 监控调阅发现问题数
    media_problem_number = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    media_problem_number.value = [MEDIA_PROBLEM_NUMBER_SQL]

    # 监控调阅质量分
    media_problme_score = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    media_problme_score.value = [MEDIA_PROBLME_SCORE_SQL]

    # ---------考核力度-----------

    # 干部考核金额
    ganbu_assess_responsible = CommonCalcDataType(*GANBU_ASSESS_RESPONSIBLE_INFO)
    ganbu_assess_responsible.value = [GANBU_ASSESS_RESPONSIBLE_SQL]

    # 非干部考核金额
    feiganbu_assess_responsible = CommonCalcDataType(*FEIGANBU_ASSESS_RESPONSIBLE_INFO)
    feiganbu_assess_responsible.value = [FEIGANBU_ASSESS_RESPONSIBLE_SQL]

    # ---------问题暴露度-----------
    # 一般及以上问题数
    above_yiban_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    above_yiban_problem_number.version = 'v2'
    above_yiban_problem_number.description = '一般及以上问题数'
    above_yiban_problem_number.value = [
        ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题质量分
    above_yiban_problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    above_yiban_problem_check_score.version = 'v2'
    above_yiban_problem_check_score.description = '一般及以上问题质量分'
    above_yiban_problem_check_score.value = [
        ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 总问题数
    all_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    all_problem_number.value = [
        ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般以及以上作业项问题数
    above_yiban_zuoye_check_problem = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    above_yiban_zuoye_check_problem.version = 'v2'
    above_yiban_zuoye_check_problem.description = '一般以及以上作业项问题数'
    above_yiban_zuoye_check_problem.value = [ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上作业项问题质量分
    above_yiban_zuoye_problem_check_score = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    above_yiban_zuoye_problem_check_score.version = 'v2'
    above_yiban_zuoye_problem_check_score.description = '一般及以上作业项问题质量分'
    above_yiban_zuoye_problem_check_score.value = [
        ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 作业项问题数
    zuoye_problem_count = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    zuoye_problem_count.value = [ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 作业项问题质量分
    zuoye_problem_check_score = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    zuoye_problem_check_score.value = [ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 参与基数计算的sql字典
    child_index_sql_dict = {

        # 较大风险问题质量均分
        '1-6': (IndexDetails(jiaoda_risk_score, person_load),
                IndexDetails(xc_jiaoda_risk_score, person_load)),

        # 夜查率
        '1-7': (IndexDetails(yecha_count, worker_count),),

        # 监控调阅力度
        '1-10': (IndexDetails(media_cost_time, worker_count),
                 IndexDetails(media_problem_number, worker_count),
                 IndexDetails(media_problme_score, worker_count)),
        # 考核力度指数
        # 换算单位考核金额
        '3-2': (
            IndexDetails(ganbu_assess_responsible, cadre_count),
            IndexDetails(feiganbu_assess_responsible, staff_number_data)),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
            IndexDetails(all_problem_number, worker_count),
            IndexDetails(problem_check_score, worker_count),
            IndexDetails(above_yiban_problem_number, worker_count),
            IndexDetails(above_yiban_problem_check_score, worker_count),
            IndexDetails(zuoye_problem_count, person_load),
            IndexDetails(zuoye_problem_check_score, person_load),
            IndexDetails(above_yiban_zuoye_check_problem, person_load),
            IndexDetails(above_yiban_zuoye_problem_check_score, person_load),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_risk_score_per_person', child_index_sql_dict['1-6'])
    GLV.set_value('stats_yecha_ratio', child_index_sql_dict['1-7'])
    GLV.set_value('stats_media_intensity', child_index_sql_dict['1-10'])
    GLV.set_value('stats_assess_money_per_person', child_index_sql_dict['3-2'])
    GLV.set_value('stats_total_problem_exposure', child_index_sql_dict['5-1'])
