# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/12/11上午9:45
   Change Activity: 2019/12/11上午9:45
-------------------------------------------------
"""
from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_diaochengyth import GLV
from app.data.major_risk_index.chewu_diaochengyth.common import get_vitual_major_ids
from app.data.major_risk_index.chewu_diaochengyth.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, \
    MANAGEMENT_INDEX_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids()
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(
        major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(
        major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(
        major, ids))
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    check_risk_ids = diaoche[1]
    # 调乘工作量
    # todo
    work_load = df_merge_with_dpid(pd_query(MANAGEMENT_INDEX_SQL.format(11)), department_data)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'WORK_LOAD': work_load,
        'CHECK_ITEM_IDS': check_item_ids,
        'CHECK_RISK_IDS': check_risk_ids
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)

