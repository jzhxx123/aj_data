# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/12/11上午9:44
   Change Activity: 2019/12/11上午9:44
   desc: 车务调乘一体化,均使用检查项目
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import combine_child_index
