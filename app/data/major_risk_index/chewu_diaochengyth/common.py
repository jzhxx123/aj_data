# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/12/11上午10:31
   Change Activity: 2019/12/11上午10:31
-------------------------------------------------
"""
from datetime import datetime

import pandas as pd

from app.data.major_risk_index.util import append_major_column_to_df, add_avg_score_by_major, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set, write_cardinal_number_basic_data, \
    add_avg_number_by_major_third


def get_vitual_major_ids():
    """
    获取参与计算的站段id
    六盘水车务段,达州车务段,贵阳车务段,贵阳南车站,内江车务段,成都北车站,凯里车务段,兴隆场车站,遵义车站,贵阳车站
    """
    ids_tuple = (
        '19B8C3534E0B5665E0539106C00A58FD',
        '19B8C3534E215665E0539106C00A58FD',
        '19B8C3534E205665E0539106C00A58FD',
        '19B8C3534E4B5665E0539106C00A58FD',
        '19B8C3534E105665E0539106C00A58FD',
        '19B8C3534E0C5665E0539106C00A58FD',
        '19B8C3534E2A5665E0539106C00A58FD',
        '19B8C3534E265665E0539106C00A58FD',
        '19B8C3534E0E5665E0539106C00A58FD',
        '19B8C3534E1C5665E0539106C00A58FD',
    )
    return ids_tuple


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 6
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_work_load_oneday(time, flag=0):
    workload_a = 0
    zero = (datetime.strptime(time[:10] + ' 00:00:00', "%Y-%m-%d %H:%M:%S")).timestamp()
    six = (datetime.strptime(time[:10] + ' 06:00:00', "%Y-%m-%d %H:%M:%S")).timestamp()
    twentytw0 = (datetime.strptime(time[:10] + ' 22:00:00', "%Y-%m-%d %H:%M:%S")).timestamp()
    twentyfour = (datetime.strptime(time[:10] + ' 23:59:59', "%Y-%m-%d %H:%M:%S")).timestamp() + 1
    calc_value = (datetime.strptime(time, "%Y-%m-%d %H:%M:%S")).timestamp()
    if flag == 0:
        if zero <= calc_value <= six:
            workload_a = six - calc_value + twentyfour - twentytw0
        elif six < calc_value <= twentytw0:
            workload_a = twentyfour - twentytw0
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = twentyfour - calc_value
    else:
        if zero <= calc_value <= six:
            workload_a = calc_value - zero
        elif six < calc_value <= twentytw0:
            workload_a = six - zero
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = calc_value - twentytw0 + six - zero
    return workload_a


def _calc_work_load_otherday(start, end, zero, six, twentytw0, twentyfour):
    from datetime import date
    workload = 0
    distance = date(int(end[:4]), int(end[5:7]), int(end[8:10])) - \
               date(int(start[:4]), int(start[5:7]), int(start[8:10]))
    days = int(distance.days)
    if days > 1:
        workload = _calc_work_load_oneday(start) + \
                   (days - 1) * (six - zero + twentyfour - twentytw0) + \
                   _calc_work_load_oneday(end, 1)
    else:
        workload = _calc_work_load_oneday(start) + _calc_work_load_oneday(end, 1)
    return workload


def _calc_work_load_sameday(start, end, zero, six, twentytw0, twentyfour):
    """检查时间和结束时间在同一天
    A : 00.00-6.00
    B:  22:00-23.59,
    """
    workload = 0
    # 在A或者B内
    if zero <= start < end <= six or twentytw0 <= start < end <= twentyfour:
        workload = end - start
    # 只含A
    elif start < six < end <= twentytw0:
        workload = six - start
    # 含A和B
    elif start <= six < twentytw0 < end <= twentyfour:
        workload = six - start + end - twentyfour
    # 只含B
    elif six <= start < twentytw0 < end <= twentyfour:
        workload = end - twentytw0
    return workload


def _calc_start_time(row):
    start = row["START_CHECK_TIME"].to_pydatetime().timestamp()
    end = row["END_CHECK_TIME"].to_pydatetime().timestamp()
    start_time = datetime.strftime(row["START_CHECK_TIME"], "%Y-%m-%d %H:%M:%S")
    end_time = datetime.strftime(row["END_CHECK_TIME"], "%Y-%m-%d %H:%M:%S")
    try:
        flag = start_time[:10] == end_time[:10]

        six = (datetime.strptime(start_time[:10] + ' 06:00:00',
                                 "%Y-%m-%d %H:%M:%S")).timestamp()
        zero = (datetime.strptime(start_time[:10] + ' 00:00:00',
                                  "%Y-%m-%d %H:%M:%S")).timestamp()
        twentytw0 = (datetime.strptime(start_time[:10] + ' 22:00:00',
                                       "%Y-%m-%d %H:%M:%S")).timestamp()
        twentyfour = (datetime.strptime(start_time[:10] + ' 23:59:59',
                                        "%Y-%m-%d %H:%M:%S")).timestamp() + 1
        if flag:
            work_load = _calc_work_load_sameday(start, end, zero,
                                                six, twentytw0, twentyfour)
        else:
            work_load = _calc_work_load_otherday(
                start_time,
                end_time,
                zero, six, twentytw0, twentyfour)
    except Exception as e:
        work_load = 0
    minute = work_load / 60
    return minute


def df_merge_with_depart_id(data, dpid_data, how='right'):
    """将指数计算需要的数据跟部门单位连接后获取单位dpid

    Arguments:
        data {dataFrame} -- 指数数据
        dpid_data {dataFrame} -- 部门单位(参与计算的)

    Keyword Arguments:
        how {str} -- 连接方式 (default: {'inner'})

    Returns:
        dataFrame --各单位的数据
    """
    data = pd.merge(
        data,
        dpid_data,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    data.fillna(0, inplace=True)
    return data


def calc_ye_cha_count(data):
    """计算夜间检查次数"""
    data['TIME'] = data.apply(
        lambda row: _calc_start_time(row), axis=1
    )
    data = data[data['TIME'] >= 30]
    data = data[['FK_DEPARTMENT_ID', 'COUNT']]
    return data


# 问题均衡度
def stats_problem_point_evenness(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        work_load, months_ago,
        risk_type, choose_dpid_data,
        department_data,
        zhanduan_filter_list=[],
        fraction=None):
    rst_index_score = []
    df_numerator = generally_above_problem_point_count
    df_denominator = generally_above_problem_point_in_problem_base_count
    ser_numerator = df_numerator.groupby(
        ['TYPE3'])['COUNT'].sum()
    ser_denominator = df_denominator.groupby(
        ['TYPE3'])['COUNT'].sum()
    work_load = work_load.groupby(['TYPE3'])['COUNT'].sum()
    df_calc = pd.concat(
        [
            ser_numerator.to_frame(name='numerator'),
            ser_denominator.to_frame(name='denominator'),
            work_load.to_frame(name='work_load')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['denominator'], inplace=True)
    df_calc.fillna(0, inplace=True)
    # 分母为0时ratio赋予0
    # 问题均衡度
    df_calc['point_evenness'] = df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100)
        if row['denominator'] > 0 else 0, axis=1)
    df_calc['ratio'] = df_calc.apply(
        lambda row: (row['point_evenness'] / row["work_load"])
        if row["work_load"] > 0 else 0, axis=1)

    # 将df_calc的分子分母存储到基数基础数据库中
    if fraction:
        write_cardinal_number_basic_data(df_calc,
                                         choose_dpid_data(3), fraction, risk_type, 13, 3,
                                         months_ago)

    column = 'SCORE_c_3'
    data = add_avg_number_by_major_third(df_calc, choose_dpid_data(3), column,
                                         zhanduan_filter_list=zhanduan_filter_list,
                                         numerator='point_evenness', denominator='work_load',
                                         fraction=fraction)
    data['SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'ratio', 'AVG_NUMBER', 1),
        axis=1)
    # 增加平均分一列
    data = add_avg_score_by_major(data, 'SCORE')
    data.fillna(0, inplace=True)
    data['group_sort'] = data['SCORE'].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    df_calc = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[column])
    content_tmp = '<p>得分：{0:.2f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.6f}</p><p>问题查处均衡度({3:.6f}) = ' \
                  + '查出一般以上问题项点数({4:.0f})/ (基础问题库中一般以上问题项点数({5:.0f})* 工作量({7:.3f}))*100%</p>'

    data['CONTENT'] = data.apply(lambda row:
                                 content_tmp.format(row['SCORE'], row['group_sort'],
                                                    row['AVG_NUMBER'],
                                                    row['ratio'], row['numerator'],
                                                    row['denominator'], row['AVG_SCORE'],
                                                    row['work_load']), axis=1)
    calc_basic_data_rst = format_export_basic_data(
        data.copy(), 13, 3, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 13, 3, risk_type=risk_type)

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(df_calc, choose_dpid_data(3), column,
                           3, 2, 13, 3,
                           months_ago, risk_type)
    rst_index_score.append(df_calc[[column]])
    return rst_index_score


def calc_check_key_problem_basic_data(key_problem, zhanduan_dpid):
    """
    获取关键问题查处力度基础数据
    :param key_problem: 查出的关键问题
    :param zhanduan_dpid:站段
    :param dpid:部门
    :return:
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    key_problem = key_problem.groupby('TYPE3')['COUNT'].sum().reset_index()
    basic_data = pd.merge(
        key_problem,
        zhanduan_dpid,
        left_on='TYPE3',
        right_on='DEPARTMENT_ID',
        how='right'
    )
    basic_data.fillna(0, inplace=True)
    tmp = """本月发现关键问题数: {0:.0f}"""
    basic_data['CONTENT'] = basic_data.apply(
        lambda row: tmp.format(row['COUNT']), axis=1)
    basic_data['SCORE_e_3'] = basic_data.apply(
        lambda row: 0 if row['COUNT'] > 0 else 69, axis=1)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=basic_data['TYPE3'],
            data=basic_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_data.groupby(['TYPE3'])['SCORE_e_3'].sum().to_frame()
    return calc_df_data, df_rst
