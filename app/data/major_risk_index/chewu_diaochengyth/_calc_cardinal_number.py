#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     _calc_cardinal_number
   Author :       hwj
   date：          2019/12/13下午3:55
   Change Activity: 2019/12/13下午3:55
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_diaochengyth import GLV
from app.data.major_risk_index.chewu_diaochengyth.check_intensity_sql import GUANLI_CHECK_PROBLEM_SQL, \
    ZUOYE_CHECK_PROBLEM_SQL, YECHA_CHECK_SQL, SITE_CHECK_COUNT_SQL, MONITOR_CHECK_COUNT_SQL
from app.data.major_risk_index.chewu_diaochengyth.check_quality_sql import PROBLEM_CHECK_SCORE_SQL, \
    HIGH_PROBLEM_CHECK_SCORE_SQL, GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, \
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL
from app.data.major_risk_index.chewu_diaochengyth.common import get_vitual_major_ids, calc_ye_cha_count, \
    df_merge_with_depart_id
from app.data.major_risk_index.chewu_diaochengyth.common_sql import (
    ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.common.cardinal_number_common import calc_cardinal_number
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import BASE_UNIT_INFO_SQL
from app.data.major_risk_index.common.const import (
    IndexDivider, CommonCalcDataType, CHECK_COUNT_INFO,
    WORKER_LOAD_INFO,
    PERSON_LOAD_INFO, GUANLI_PROBLEM_COUNT_INFO, ZUOYE_PROBLEM_COUNT_INFO, YECHA_COUNT_INFO, PROBLEM_SCORE_INFO,
    PROBLEM_POINT_EVENNESS_INFO, MONITOR_CHECK_COUNT_INFO)
from app.data.util import pd_query


# 调乘工作量--因为assdf是年和月
MANAGEMENT_INDEX_SQL = """
SELECT
FK_DEPARTMENT_ID,
MANAGEMENT_INDEX AS COUNT 
FROM
t_department_integrated_management 
WHERE
FK_TYPE_ID = 2553
AND MONTH={1} 
"""


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, CALC_MONTH, \
        CHECK_ITEM_IDS, RISK_IDS, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, STAFF_NUMBER, WORKER_COUNT_DATA
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    risk_config = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_config[0]
    RISK_IDS = risk_config[1]
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    month = int(STATS_MONTH[1][5:7])
    CALC_MONTH = end, start
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids()
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    # 职工总人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(major))


# ------------------------获取比值型相应基数------------------------ #
def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    major_dpid = get_major_dpid(risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, major_dpid)

    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 调乘工作量
    worker_load_count = CommonCalcDataType(*WORKER_LOAD_INFO)
    worker_load_count.version = 'v2'
    worker_load_count.func_version = 'assess_df'
    worker_load_count.description = '调乘工作量'
    # todo 因为8,9,10无数据,暂时取11月
    # worker_load_count.value = [MANAGEMENT_INDEX_SQL]
    worker_load_count.value = [MANAGEMENT_INDEX_SQL.format(2019, 11)]

    # 总人数
    person_load = CommonCalcDataType(*PERSON_LOAD_INFO)
    person_load.value = [STAFF_NUMBER, EXTERNAL_PERSON_SQL]

    # ---------检查力度-----------
    # 现场检查次数
    check_count = CommonCalcDataType(*CHECK_COUNT_INFO)
    check_count.value = [SITE_CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 现场检查次数
    monitor_check_count = CommonCalcDataType(*MONITOR_CHECK_COUNT_INFO)
    monitor_check_count.value = [MONITOR_CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 管理项问题数
    guanli_problem_count = CommonCalcDataType(*GUANLI_PROBLEM_COUNT_INFO)
    guanli_problem_count.version = 'v2'
    guanli_problem_count.description = '检查管理项问题数（项目）'
    guanli_problem_count.value = [GUANLI_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 作业项问题数
    zuoye_problem_count = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    zuoye_problem_count.version = 'v2'
    zuoye_problem_count.description = '检查作业项问题数（项目）'
    zuoye_problem_count.value = [ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 夜查次数
    yecha_count = CommonCalcDataType(*YECHA_COUNT_INFO)
    yecha_count.value = [YECHA_CHECK_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]
    yecha_count.func_version = 'customized_df'
    yecha_count.func_value = _get_ye_cha_count

    # 检查质量指数
    # 问题质量分
    problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    problem_check_score.version = 'v2'
    problem_check_score.description = '检查出问题质量分（项目）'
    problem_check_score.value = [PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 高质量问题分
    high_problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    high_problem_check_score.version = 'v3'
    high_problem_check_score.description = '高质量问题（A、B、F1、E1）质量分累计（项目）'
    high_problem_check_score.value = [HIGH_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题均衡度
    problem_point_evenness = CommonCalcDataType(*PROBLEM_POINT_EVENNESS_INFO)
    problem_point_evenness.value = [
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS),
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(CHECK_ITEM_IDS)]
    problem_point_evenness.func_value = _calc_problem_point_evenness

    # 参与基数计算的sql字典
    child_index_sql_dict = {
        # 检查力度
        # 换算单位检查频次
        '1-2': (IndexDetails(check_count, worker_load_count),
                IndexDetails(monitor_check_count, worker_load_count)),

        # 查处问题率
        '1-3': (IndexDetails(zuoye_problem_count, worker_load_count),
                IndexDetails(guanli_problem_count, worker_load_count)),

        # 夜查率
        '1-7': (IndexDetails(yecha_count, worker_load_count),),

        # 检查质量指数
        # 质量均分
        '13-1': (IndexDetails(problem_check_score, worker_load_count),),
        # 高质量问题分
        '13-2': (IndexDetails(high_problem_check_score, worker_load_count),),
        # 问题均衡度
        '13-3': (IndexDetails(problem_point_evenness, worker_load_count),),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('_stats_check_per_person', child_index_sql_dict['1-2'])
    GLV.set_value('_stats_check_problem_ratio', child_index_sql_dict['1-3'])
    GLV.set_value('_stats_yecha_ratio', child_index_sql_dict['1-7'])
    GLV.set_value('_stats_score_per_person', child_index_sql_dict['13-1'])
    GLV.set_value('_stats_high_score_per_person', child_index_sql_dict['13-2'])
    GLV.set_value('_stats_problem_point_evenness', child_index_sql_dict['13-3'])


def _calc_problem_point_evenness(
        _sqllist_numerator,
        zhanduan_dpid_data,
        department_data,
        mon_ago, column, dpids):
    stats_month = get_custom_month(mon_ago)

    # 一般以上项点问题数
    problem_point_count = df_merge_with_depart_id(
        pd_query(_sqllist_numerator[0].format(*stats_month)),
        department_data)
    # 基础问题库中一般及以上风险项点问题数
    base_problem_point_count = df_merge_with_depart_id(
        pd_query(_sqllist_numerator[1]), department_data)

    problem_point_count = problem_point_count.groupby('TYPE3')['COUNT'].sum().to_frame(name='problem_count')
    base_problem_point_count = base_problem_point_count.groupby('TYPE3')['COUNT'].sum().to_frame(name='base_count')
    data = pd.merge(
        problem_point_count,
        base_problem_point_count,
        left_index=True,
        right_index=True,
    )
    # 问题均衡度
    data['COUNT'] = data.apply(
        lambda row: row['problem_count'] / row['base_count'] * 100 if row['base_count'] > 0 else 0, axis=1
    )
    data = data.reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data


def _get_ye_cha_count(
        _sqllist_numerator,
        zhanduan_dpid_data,
        department_data,
        mon_ago, column, dpids):
    stats_month = get_custom_month(mon_ago)
    data = df_merge_with_depart_id(
        calc_ye_cha_count(pd_query(_sqllist_numerator[0].format(*stats_month))),
        department_data)
    data = data.groupby('TYPE3')['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data
