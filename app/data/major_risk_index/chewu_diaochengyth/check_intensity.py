# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/12/11上午11:56
   Change Activity: 2019/12/11上午11:56
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.chewu_diaochengyth import GLV
from app.data.major_risk_index.chewu_diaochengyth.check_intensity_sql import *
from app.data.major_risk_index.chewu_diaochengyth.common import df_merge_with_depart_id, calc_ye_cha_count
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.util import combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH, CHECK_ITEM_IDS, CHECK_RISK_IDS
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        WORK_LOAD, SITE_CHECK_COUNT, MONITOR_CHECK_COUNT, ZUOYE_PROBLEM_COUNT, \
        GUANLI_PROBLEM_COUNT, YECHA_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    stats_month = get_custom_month(months_ago)

    # 调乘工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 现场检查次数
    SITE_CHECK_COUNT = df_merge_with_depart_id(
        pd_query(SITE_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 现场检查次数
    MONITOR_CHECK_COUNT = df_merge_with_depart_id(
        pd_query(MONITOR_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 作业项问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_depart_id(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 管理项问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_depart_id(
        pd_query(GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_depart_id(
        calc_ye_cha_count(pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS))),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    """
    现场检查占比70%：现场检查（包含添乘检查和现场检查）调乘一体化作业检查次数/工作量*70%
    调阅检查占比30%：调阅检查（检查方式为监控调阅检查）调乘一体化检查次数/工作量*30%
    """
    fraction_list = GLV.get_value("_stats_check_per_person", (None, None))
    return check_intensity.stats_check_per_person_by_two_type(
        SITE_CHECK_COUNT,
        MONITOR_CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list
    )


# 查处问题考核率
def _stats_check_problem_ratio(months_ago):
    fraction_list = GLV.get_value("_stats_check_problem_ratio", (None, None))
    return check_intensity.stats_check_problem_ratio_type_two_type_major(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 夜查率
def _stats_yecha_ratio(months_ago):
    """现场检查接发列车夜查次数（22:00-6:00，时间段内的检查不少于30分钟）/∑站段管内各站“夜间（22:00-6:00）多方向接发列车工作量"""
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                       "夜查次数(22:00-6:00，时间段内的检查不少于30分钟)({4})/ 调乘工作量({5})*100%</p>"
    fraction = GLV.get_value("_stats_yecha_ratio", (None,))[0]
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent,
        fraction=fraction)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_yecha_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'g']
    ]
    item_weight = [0.38, 0.5, 0.12]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=10, major=risk_type, main_type=1, child_index_list=[2, 3, 7],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
