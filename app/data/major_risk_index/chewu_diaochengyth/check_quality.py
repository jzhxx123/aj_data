#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_quality
   Author :       hwj
   date：          2019/12/12下午3:06
   Change Activity: 2019/12/12下午3:06
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.chewu_diaochengyth import GLV
from app.data.major_risk_index.chewu_diaochengyth.check_quality_sql import *
from app.data.major_risk_index.chewu_diaochengyth.common import df_merge_with_depart_id, stats_problem_point_evenness, \
    calc_check_key_problem_basic_data
from app.data.major_risk_index.common import check_quality
from app.data.major_risk_index.util import combine_child_index_func, summizet_child_index, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH, CHECK_ITEM_IDS, CHECK_RISK_IDS
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        WORK_LOAD, PROBLEM_SCORE, HIGH_PROBLEM_SCORE, GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, KEY_PROBLEM_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    stats_month = get_custom_month(months_ago)

    # 调乘工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # # 累计质量分
    PROBLEM_SCORE = df_merge_with_depart_id(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # # 累计质量分
    HIGH_PROBLEM_SCORE = df_merge_with_depart_id(
        pd_query(HIGH_PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_depart_id(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_depart_id(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 关键问题数
    KEY_PROBLEM_COUNT = df_merge_with_depart_id(
        pd_query(CHECK_KEY_PROBLEM_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均质量分
def _stats_score_per_person(months_ago):
    """问题质量分累计/装卸车工作量"""
    fraction = GLV.get_value("_stats_score_per_person", (None,))[0]
    return check_quality.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction,
    )


# 高质量问题质量分
def _stats_high_score_per_person(months_ago):
    """高质量问题分累计/装卸车工作量"""
    fraction = GLV.get_value("_stats_high_score_per_person", (None,))[0]

    return check_quality.stats_high_score_per_person_major(
        HIGH_PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction
    )


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """一般以上问题项点数/基础问题库中一般以上问题项点数"""
    fraction = GLV.get_value("_stats_problem_point_evenness", (None,))[0]

    return stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, WORK_LOAD, months_ago,
        RISK_TYPE, _choose_dpid_data, DEPARTMENT_DATA, fraction=fraction)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return check_quality.stats_other_problem_exposure(
        CHECK_ITEM_IDS, months_ago, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        SELF_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL,
        OTHER_CHECK_PROBLEM_SQL,
        _choose_dpid_data,
        RISK_TYPE)


def _stats_check_key_problem(months_ago):
    """当月未查处任何符合的检查项目间断瞭望、错误操作项关键问题（A、B类），
    总指数中扣分41分（单独扣分，即该指数前的两个分指数合成后的得分减去d该指数扣分数）"""

    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_check_key_problem_basic_data(KEY_PROBLEM_COUNT, ZHANDUAN_DPID_DATA)

    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 13, 5, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 13, 5, risk_type=RISK_TYPE)

    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_e_3',
        3,
        2,
        13,
        5,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_score_per_person,
        _stats_high_score_per_person,
        _stats_problem_point_evenness,
        _stats_other_problem_exposure,
        _stats_check_key_problem
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    item_name = [
        f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']
    ]
    item_weight = [0.33, 0.33, 0.17, 0.17, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        13,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=10, major=risk_type, main_type=13, child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
