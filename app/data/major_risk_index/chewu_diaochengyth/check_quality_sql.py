#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_quality_sql
   Author :       hwj
   date：          2019/12/12下午3:10
   Change Activity: 2019/12/12下午3:10
-------------------------------------------------
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 高质量问题质量分
HIGH_PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND a.LEVEL IN ('A','B','F1','E1')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 一般及以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.FK_CHECK_ITEM_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""


# 他查问题-检查部门不是本站段的视为他查
OTHER_CHECK_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(a.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RISK_LEVEL) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS d
            ON a.PK_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_department AS e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE
        c.TYPE3 != e.TYPE3
            AND a.RISK_LEVEL BETWEEN 1 AND 2
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
            AND a.FK_CHECK_ITEM_ID IN ({0})
"""

# 安全生产信息问题
# 安全信息管联多个部门,需判断基础问题是否属于该站段
SAFETY_PRODUCE_INFO_SQL = """SELECT
        DISTINCT 
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                d.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_department AS d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN 
        t_problem_base AS e ON b.FK_PROBLEM_BASE_ID = e.PK_ID 
    WHERE
        a.RANK BETWEEN 1 AND 2
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
                AND e.FK_DEPARTMENT_ID = d.TYPE3
                AND e.FK_CHECK_ITEM_ID IN ({0})
"""

# 自查问题-检查部门是本站段的视为自查
SELF_CHECK_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(a.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RISK_LEVEL) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS d
            ON a.PK_ID = d.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_department AS e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    WHERE
        c.TYPE3 = e.TYPE3
            AND a.RISK_LEVEL BETWEEN 1 AND 2
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
            AND a.FK_CHECK_ITEM_ID IN ({0})
"""

# 发现关键问题数
CHECK_KEY_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID    
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND a.LEVEL IN ('A', 'B') 
    GROUP BY b.FK_DEPARTMENT_ID
"""