# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql
   Author :       hwj
   date：          2019/7/13下午5:23
   Change Activity: 2019/7/13下午5:23
-------------------------------------------------
"""

# 检查次数（现场检查）
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        and (a.IS_KECHE = 1 or a.IS_GAOTIE=1 or a.IS_DONGCHE =1)
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID  
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_EXTERNAL = 0            
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
            AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 管理项问题数
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID  
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
            AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# # 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 较大和重大安全风险问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """SELECT
      distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""


# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """SELECT
        distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 AND 2
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND TIMESTAMPDIFF(MINUTE,
            a.START_CHECK_TIME,
            a.END_CHECK_TIME) >= 30            
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)  
            AND c.FK_CHECK_ITEM_ID IN ({2})
            and (a.IS_KECHE = 1 or a.IS_GAOTIE=1 or a.IS_DONGCHE =1)
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND (b.IS_KECHE = 1 or b.IS_GAOTIE=1 or b.IS_DONGCHE =1)
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 3 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND a.IS_EXTERNAL = 0
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 and 4
        AND a.IS_EXTERNAL = 0
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
        OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY BETWEEN 3 and 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103) 
        AND (a.IS_KECHE = 1 or a.IS_GAOTIE=1 or a.IS_DONGCHE =1) 
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct a.FK_DEPARTMENT_ID as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
    t_check_item as c on  (a.SOURCE_ID = c.PARENT_ID or a.SOURCE_ID =c.pk_id)
        INNER JOIN
    t_department_type_cw as d on d.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MAIN_TYPE = 1
        AND b.IS_DELETE = 0
        AND c.PK_ID  in ({0})
        and d.ACCEPT_DEPARTURE_NUMBER > 0
        and d.PASSENGER_DISTRICT =1
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]


# 接发列车数(夜间)
YECHA_JIEFALC_COUNT_SQL = """
SELECT
NODE,
COUNT( 1 ) AS CW_COUNT,
CFCC 
FROM
t_td_data 
WHERE
DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )
AND ((DDSJ <= DATE_ADD(DATE(DDSJ),
            INTERVAL 86399 SECOND)
            AND DDSJ >= DATE_ADD(DATE(DDSJ),
            INTERVAL 22 HOUR))
            OR (DDSJ >= DATE(DDSJ)
            AND DDSJ <= DATE_ADD(DATE(DDSJ),
            INTERVAL 6 HOUR)))
GROUP BY
NODE,
CFCC
"""


# 实际检查班组数
REAL_CHECK_BANZU_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY BETWEEN 1 and 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)  
        AND (a.IS_KECHE = 1 or a.IS_GAOTIE=1 or a.IS_DONGCHE =1)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.IS_DELETE = 0
"""