#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2019/03/08
'''

from app.data.major_risk_index.chewu_gaodongkejflc import (
    check_evenness, check_intensity, problem_exposure, init_common_data, GLV)
from app.data.major_risk_index.chewu_gaodongkejflc.common import get_vitual_major_ids
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common.common_sql import (CHEJIAN_DPID_SQL,
                                                         ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 17
    risk_type = '车务-7'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        check_evenness,
        problem_exposure
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    child_index_weight = [0.4, 0.2, 0.4]
    child_index_list = [1, 4, 5]
    ids = get_vitual_major_ids('客运-1')
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight,
        child_index_list=child_index_list,
        vitual_major_ids=ids)

    update_major_maintype_weight(index_type=7, major=risk_type, child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )

    # 清除本模块的共享数据
    GLV.rm_all_values()


if __name__ == '__main__':
    pass
