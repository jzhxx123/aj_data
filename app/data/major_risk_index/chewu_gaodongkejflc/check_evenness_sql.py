# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness_sql
   Author :       hwj
   date：          2019/7/15上午9:13
   Change Activity: 2019/7/15上午9:13
-------------------------------------------------
"""
# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND (a.PROBLEM_DIVIDE_NAMES LIKE '%%动%%'
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%客%%' 
            OR a.PROBLEM_DIVIDE_NAMES LIKE '%%高%%') 
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""


# 每天检查数-按时段
HOUR_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR,
        COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_check_info_and_item as c on a.pk_id = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND c.FK_CHECK_ITEM_ID in ({2})
        AND (a.IS_KECHE = 1 or a.IS_GAOTIE=1 or a.IS_DONGCHE =1)
    GROUP BY b.FK_DEPARTMENT_ID , START_HOUR, END_HOUR;
"""


# 车站受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
    DISTINCT 
    b.PK_ID, 
    a.FK_DEPARTMENT_ID,
    FK_CHECK_POINT_ID
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID			
    WHERE
        a.TYPE = 1
        AND b.CHECK_WAY BETWEEN 1 and 3
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND 
        d.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND (b.IS_KECHE = 1 OR b.IS_GAOTIE=1 OR b.IS_DONGCHE =1)
"""

# 检查地点-车站地点受检次数
POINT_CHECK_COUNT_SQL = """SELECT
    DISTINCT  
    b.PK_ID,
    FK_CHECK_POINT_ID
    FROM
    t_check_info_and_address AS a
    INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    INNER JOIN
    t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
    a.TYPE = 2
        AND b.CHECK_WAY NOT BETWEEN 4 AND 6
        AND b.CHECK_WAY BETWEEN 1 and 3
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND	SUBMIT_TIME >= DATE_FORMAT('{0}','%%Y-%%m-%%d')
        AND SUBMIT_TIME < DATE_FORMAT('{1}','%%Y-%%m-%%d')
        AND (b.IS_KECHE = 1 OR b.IS_GAOTIE=1 OR b.IS_DONGCHE =1)
"""

# 检查地点-检查地点关联部门
CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
    SOURCE_DEPARTMENT_ID as FK_DEPARTMENT_ID, FK_ADDRESS_ID as FK_CHECK_POINT_ID
FROM
    t_department_and_main_production_site 
WHERE
    TYPE = 2
"""


# 检查地点-车站管理部门
STATION_CONNECT_DEPARTMENT_SQL = """SELECT
    DISTINCT a.FK_DEPARTMENT_ID, NODE 
FROM
    t_department_and_info AS a
    inner join 
    t_check_item as ti on (a.SOURCE_ID = ti.PARENT_ID or a.SOURCE_ID =ti.pk_id)
    INNER JOIN (
    SELECT
        c.FK_DEPARTMENT_ID,
        c.NODE 
    FROM
        (
        SELECT
            a.FK_DEPARTMENT_ID,
            substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE 
        FROM
            t_department_type_cw a
            JOIN mysql.help_topic b ON b.help_topic_id < ( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 ) 
            INNER JOIN 
            t_department as dp on a.FK_DEPARTMENT_ID=dp.DEPARTMENT_ID
        WHERE
            a.PASSENGER_DISTRICT = 1
            and a.ACCEPT_DEPARTURE_NUMBER > 0
            and dp.IS_DELETE=0 
        ) AS c 
    WHERE
        c.NODE != ''
    GROUP BY
        c.NODE,
        c.FK_DEPARTMENT_ID
    ) as b
    on b.FK_DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.MAIN_TYPE = 1
        AND ti.PK_ID in ({0}) 
"""


# 车站接发列车数-时段
STATION_JIEFALC_COUNT_HOUR_SQL = """
SELECT
    a.NODE,
    a.CFCC,
    ( DATE_FORMAT( DDSJ, '%%H' ) DIV 2 ) + 1 AS WORK_HOUR,
    COUNT( 1 ) AS CW_COUNT 
FROM
    (
    SELECT DISTINCT
        NODE,
        CFCC,
        CFSJ,
        DDSJ 
    FROM
        `t_td_data` 
    WHERE
        DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    ) AS a 
GROUP BY
    a.NODE,a.CFCC, WORK_HOUR"""


# 车站接发列车数- 每日
STATION_JIEFALC_COUNT_DAILY_SQL = """
SELECT
    a.NODE,
    a.CFCC,
    DAY(DDSJ)  AS WORK_DAY,
    COUNT( 1 ) AS DAY_COUNT 
FROM
    (
    SELECT DISTINCT
        NODE,
        CFCC,
        CFSJ,
        DDSJ 
    FROM
        `t_td_data` 
    WHERE
        DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    ) AS a 
GROUP BY
    a.NODE,a.CFCC, WORK_DAY
"""