# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/7/15上午8:43
   Change Activity: 2019/7/15上午8:43
-------------------------------------------------
"""
# -*- coding: utf-8 -*-
import calendar

import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype, get_month_day
from app.data.major_risk_index.chewu_jiefalc.check_intensity_sql import MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, \
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    BANZU_POINT_SQL)
from app.data.major_risk_index.util import summizet_operation_set, \
    write_export_basic_data_to_mongo, df_merge_with_dpid, calc_child_index_type_sum, append_major_column_to_df, \
    export_basic_data_tow_field_monthly_two, format_export_basic_data, calc_extra_child_score_groupby_major, \
    add_avg_score_by_major
from app.data.util import get_history_months, pd_query
from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)
HIERARCHY = [3]


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    belong_profession_id = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = belong_profession_id.get(major, 898)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
        AND 
        FK_PROFESSION_DICTIONARY_ID NOT LIKE '%%2168%%'
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 6
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def calc_score_by_formula_address(row, column, major_column, node):
    if row[node] == 0:
        return 0
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


def df_merge_with_dp_id(data, dp_id, how='right'):
    """以部门数据为准, 得到完整的部门结果"""
    data = pd.merge(
        data,
        dp_id,
        how=how,
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    data.fillna(0, inplace=True)
    return data


# 问题均衡度
def stats_problem_point_evenness(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        work_load, months_ago,
        risk_type, choose_dpid_data,
        zhanduan_filter_list=None):
    rst_index_score = []
    df_numerator = generally_above_problem_point_count
    df_denominator = generally_above_problem_point_in_problem_base_count
    ser_numerator = df_numerator.groupby(
        ['TYPE3'])['COUNT'].sum()
    ser_denominator = df_denominator.groupby(
        ['TYPE3'])['COUNT'].sum()
    work_load = work_load.groupby(['TYPE3'])['COUNT'].sum()
    df_calc = pd.concat(
        [
            ser_numerator.to_frame(name='numerator'),
            ser_denominator.to_frame(name='denominator'),
            work_load.to_frame(name='work_load')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['denominator'], inplace=True)
    df_calc.fillna(0, inplace=True)
    # 分母为0时ratio赋予0
    df_calc['ratio'] = df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100 / row["work_load"])
        if row["work_load"] > 0 and row['denominator'] > 0 else 0, axis=1)

    # 问题均衡度
    filted_df_calc = df_calc[~df_calc.index.isin(zhanduan_filter_list)]
    radio = filted_df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100)
        if row['denominator'] > 0 else 0, axis=1)

    major_avg_score = sum(radio) / sum(filted_df_calc['work_load'])

    df_calc['AVG_NUMBER'] = major_avg_score
    data = append_major_column_to_df(choose_dpid_data(3), df_calc)

    data['SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'ratio', 'AVG_NUMBER', 1),
        axis=1)
    # 增加平均分一列
    data = add_avg_score_by_major(data, 'SCORE')
    data.fillna(0, inplace=True)
    data['group_sort'] = data['SCORE'].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    column = 'SCORE_a_3'
    df_calc = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[column])
    content_tmp = '<p>得分：{0:.2f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.6f}</p><p>问题查处均衡度({3:.6f}) = ' \
                  + '查出一般以上问题项点数({4:.0f})/ (基础问题库中一般以上问题项点数({5:.0f})* 工作量({7:.3f}))*100%</p>'

    data['CONTENT'] = data.apply(lambda row:
                                 content_tmp.format(row['SCORE'], row['group_sort'],
                                                    row['AVG_NUMBER'],
                                                    row['ratio'], row['numerator'],
                                                    row['denominator'], row['AVG_SCORE'],
                                                    row['work_load']), axis=1)
    calc_basic_data_rst = format_export_basic_data(
        data.copy(), 4, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 4, 1, risk_type=risk_type)

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(df_calc, choose_dpid_data(3), column,
                           3, 2, 4, 1,
                           months_ago, risk_type)
    rst_index_score.append(df_calc[[column]])

    return rst_index_score


def _export_calc_address_evenness_data(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        work_load = row['WORK_COUNT']
        point_name = row['ALL_NAME']
        dpid = row['TYPE3']
        check_desc = (f'{point_name}(比较值：{avg_check_count:.2f}, '
                      f'受检次数：{real_check_count:.0f}, '
                      f'基准值：{base_check_count:.4f}, '
                      f'工作量：{work_load:.3f})')
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})

        if isinstance(point_name, int):
            cnt.update({'总检查地点数：': 0})
            calc_data.update({dpid: cnt})
            continue
        if avg_check_count == 0:
            if int(real_check_count) == 0:
                cnt.update({
                    '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
                })
            calc_data.update({dpid: cnt})
            continue
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= 4:
                cnt.update({
                    '受检次数超过比较值400%以上地点：':
                        cnt.get('受检次数超过比较值400%以上地点：', '') + '<br/>' + check_desc
                })
            elif _ratio <= -0.5:
                cnt.update({
                    '受检次数低于比较值50%地点：':
                        cnt.get('受检次数低于比较值50%地点：', '') + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


# 检查地点均衡度
def stats_check_address_evenness(check_item_ids, dpid_work_load, zhanduan_work_count,
                                 station_connect_department_sql,
                                 check_point_connect_department_sql,
                                 banzu_department_checked_count_sql,
                                 point_check_count_sql,
                                 department_data, zhanduan_dpid_data,
                                 months_ago, risk_type, choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    dpid_work_load.rename(columns={
        "COUNT": "WORK_COUNT",
    }, inplace=True)
    zhanduan_work_count = zhanduan_work_count[['DEPARTMENT_ID', 'COUNT']].copy()
    zhanduan_work_count.rename(columns={'DEPARTMENT_ID': 'TYPE3', 'COUNT': 'WORK_COUNT'}, inplace=True)

    # 地点
    station_connect_department = pd_query(station_connect_department_sql.format(check_item_ids))
    check_point_connect_department = pd_query(check_point_connect_department_sql)
    point_check_count = pd_query(point_check_count_sql.format(*stats_month, check_item_ids))

    point_data = pd.merge(
        station_connect_department,
        check_point_connect_department,
        how='left',
        on='FK_DEPARTMENT_ID',
    )
    point_data = pd.merge(
        point_data,
        point_check_count,
        how='left',
        on='FK_CHECK_POINT_ID'
    )
    point_data.dropna(inplace=True)

    # 班组受检次数
    banzu_check = pd_query(
        banzu_department_checked_count_sql.format(*stats_month, check_item_ids))
    banzu_check = pd.merge(
        banzu_check,
        station_connect_department,
        how='right',
        on='FK_DEPARTMENT_ID',
    )

    # 只取班组
    # data = pd.concat([point_data, banzu_check], sort='False')
    data = banzu_check
    data.drop_duplicates(['FK_DEPARTMENT_ID', 'PK_ID'], keep='first', inplace=True)
    station_count = data.groupby(['FK_DEPARTMENT_ID']).size().to_frame(name='COUNT').reset_index()

    data_check_banzu = pd.merge(
        station_count,
        dpid_work_load,
        how='left',
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.fillna(0, inplace=True)
    zhanduan_count = data_check_banzu.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='zhanduan_count')
    zhanduan_work_count = zhanduan_work_count.groupby(['TYPE3'])['WORK_COUNT'].sum().to_frame(
        name='zhanduan_work_count')
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_work_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu.drop(['DEPARTMENT_ID', 'NAME', "TYPE"], inplace=True, axis=1)
    data = pd.merge(
        data_check_banzu,
        zhanduan_dpid_data,
        how='right',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID')
    data.drop(['TYPE3'], inplace=True, axis=1)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    data.fillna(0, inplace=True)

    # 计算基准值
    data['BASE_CHECK_COUNT'] = data.apply(lambda row: (row['zhanduan_count'] / row['zhanduan_work_count'])
    if row['zhanduan_work_count'] > 0 else 0, axis=1)
    # 计算比较值=地点工作量*基准值
    data['AVG_CHECK_COUNT'] = data.apply(lambda row: row['WORK_COUNT'] * row['BASE_CHECK_COUNT'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: calc_score_by_formula_address(row, 'COUNT', 'AVG_CHECK_COUNT', 'NODE'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data, problem_risk_score={1: 4, 2: 2, 3: 0.1, },
        columns_list=[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                      (2, 3), (3, 1), (3, 2), (3, 3)]):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(safety_produce_info_sql.format(check_item_ids, *calc_month))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        risk_level = int(each_problem[1])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data, zhanduan_dpid,
                                                          5, 5, 3, months_ago, first_title, second_title,
                                                          columns_list=columns_list)
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(lambda row:
                                                 row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 5, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5,
                                     5, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type)
    return rst_child_score


def _calc_check_problem_val_person(series, choose_dpid_data, work_load,
                                   hierarchy, idx):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    # data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    data['ratio'] = data.apply(lambda row: row['prob'] / row['PERSON_NUMBER']
    if row['PERSON_NUMBER'] > 0 else 0, axis=1)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula)
    # 中间计算数据
    title = ['作业项问题数({0})/工作量({1})', '管理项问题数({0})/工作量({1})']
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_zuoye_check_problem_ratio(df_data, choose_dpid_data, work_load,
                                    hierarchy, idx):
    """作业项问题查处率"""
    zuoye_problem = df_data.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(zuoye_problem, choose_dpid_data,
                                          work_load, hierarchy, idx)


def _calc_guanli_check_problem_ratio(df_data, choose_dpid_data, work_load,
                                     hierarchy, idx):
    """管理项问题查处率"""
    guanli_problem = df_data.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_check_problem_val_person(guanli_problem, choose_dpid_data,
                                          work_load, hierarchy, idx)


# 查处问题率
def stats_check_problem_ratio_type_two(first_df,
                                       second_df,
                                       work_load,
                                       months_ago,
                                       risk_type,
                                       choose_dpid_data=None,
                                       child_weight=[0.8, 0.2]):
    """(作业项问题数*80% + 管理项问题数*20%)/工作量
    """
    rst_child_score = []
    # 保存中间计算过程数据
    calc_basic_data = []
    df_list = [first_df, second_df]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_zuoye_check_problem_ratio, _calc_guanli_check_problem_ratio
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], choose_dpid_data,
                                             work_load, hierarchy, idx)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 3, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 3, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_c_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            3,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_risk_score_per_person(series, work_load, choose_dpid_data, hierarchy,
                                idx):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='score'), work_load], axis=1, sort=False)
    data['ratio'] = data.apply(lambda row: row['score'] / row['PERSON_NUMBER']
    if row['PERSON_NUMBER'] > 0 else 0, axis=1)
    data.fillna(0, inplace=True)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula)
    # 中间计算数据
    title = [
        '较大和重大安全风险问题质量分累计({0})/工作量({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/工作量({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["score"], 2)}', round(row['PERSON_NUMBER'], 2)),
        axis=1)
    data.drop(
        columns=['score', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


def _calc_xc_risk_score(df_xianchang, work_load, choose_dpid_data, hierarchy,
                        idx):
    """现场检查发现较大和重大安全风险问题质量分累计"""
    sc_risk_score = df_xianchang.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(sc_risk_score, work_load,
                                       choose_dpid_data, hierarchy, idx)


def _calc_risk_score(df_jiaoda, work_load, choose_dpid_data, hierarchy, idx):
    """较大和重大安全风险问题质量分累计"""
    risk_score = df_jiaoda.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    return _calc_risk_score_per_person(risk_score, work_load, choose_dpid_data,
                                       hierarchy, idx)


# 较大风险问题质量均分
def stats_risk_score_per_person(df_jiaoda,
                                df_xianchang,
                                work_load,
                                months_ago,
                                risk_type,
                                child_weight=[0.6, 0.4],
                                choose_dpid_data=None):
    """(较大和重大安全风险问题质量分累计*60% + 现场检查发现较大和
    重大安全风险问题质量分累计*40%)/调车工作量
    较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险
    现场检查发现较大和重大安全风险问题质量分累计:
    数据源:安全问题管控->安全问题管理->问题查询->车务->**站段->∑问题对应质量加分
    筛选项:安全问题基础筛选项、风险等级:重大安全风险|较大安全风险、检查方式:现场检查|添乘检查
    """
    rst_child_score = []
    df_list = [df_jiaoda, df_xianchang]
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [_calc_risk_score, _calc_xc_risk_score]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(df_list[idx], work_load,
                                             choose_dpid_data, hierarchy, idx)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 6, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 6, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_f_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            6,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _calc_media_val_person(choose_dpid_data, series, work_load, hierarchy,
                           idx, title=['监控调阅时长累计({0})/总人数({1})',
                                       '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
                                       '调阅班组数({0})/班组数({1})']):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data.apply(lambda row: row['media'] / row['PERSON_NUMBER']
    if row['PERSON_NUMBER'] > 0 else 0, axis=1)
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major(
        data, choose_dpid_data(hierarchy), 'ratio', _calc_score_by_formula)
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(f'{round(row["media"], 4)}', round(row['PERSON_NUMBER'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅人均时长
def _calc_media_time_per_person(choose_dpid_data, stats_month, department_data,
                                work_load, check_item_ids, hierarchy, idx,
                                media_cost_time_sql, title):
    media_time = df_merge_with_dpid(
        pd_query(media_cost_time_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['TIME'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title)


# 监控调阅人均问题个数
def _calc_media_problem_per_person(choose_dpid_data, stats_month,
                                   department_data, work_load, check_item_ids,
                                   hierarchy, idx, media_problem_number_sql, title):
    media_time = df_merge_with_dpid(
        pd_query(
            media_problem_number_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title)


# 监控调阅人均质量分
def _calc_media_score_per_person(choose_dpid_data, stats_month,
                                 department_data, work_load, check_item_ids,
                                 hierarchy, idx, media_problem_score_sql, title):
    media_time = df_merge_with_dpid(
        pd_query(media_problem_score_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_load,
                                  hierarchy, idx, title=title)


# 监控调阅覆盖比例(调阅班组数/作业或施工班组数)
def _calc_media_banzu_val_workbanzu(choose_dpid_data, stats_month,
                                    department_data, work_load, check_item_ids,
                                    hierarchy, idx, monitor_watch_discovery_ratio_sqllist, title):
    """
    :param choose_dpid_data:
    :param stats_month:
    :param department_data:
    :param work_load:
    :param check_item_ids:
    :param hierarchy:
    :param idx:
    :param media_problem_score_sql:
    :param monitor_watch_discovery_ratio_sqllist: 监控调阅覆盖比例sql列表（一般包含查询调阅班组数sql，查询施工班组数sql）
    :param watch_media_banzu_count:
    :return:
    """
    # 调阅班组数
    watch_media_banzu_count = df_merge_with_dpid(
        pd_query(monitor_watch_discovery_ratio_sqllist[0].format(*stats_month, check_item_ids)),
        department_data)
    # 作业班组数
    work_banzu_count = df_merge_with_dpid(
        pd_query(monitor_watch_discovery_ratio_sqllist[1].format(check_item_ids)),
        department_data)
    media_time = watch_media_banzu_count.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(choose_dpid_data, media_time, work_banzu_count,
                                  hierarchy, idx, title=title)


# 监控调阅力度
def stats_media_intensity(department_data,
                          work_load,
                          months_ago,
                          risk_name,
                          risk_type,
                          choose_dpid_data=None,
                          media_cost_time_sql=None,
                          media_problem_number_sql=None,
                          media_problem_score_sql=None,
                          monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
                          child_weight=[0.35, 0.35, 0.3, 0.00],
                          title=['监控调阅时长累计({0})/总人数({1})',
                                 '监控调阅发现问题数({0})/总人数({1})', '监控调阅发现问题质量分累计({0})/总人数({1})',
                                 '调阅班组数({0})/班组数({1})']):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_cost_time_sql, media_problem_number_sql,
                  media_problem_score_sql, monitor_watch_discovery_ratio_sqllist
                  ]
    if media_cost_time_sql is None:
        media_func = [
            MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
            MEDIA_PROBLME_SCORE_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
        ]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person, _calc_media_problem_per_person,
            _calc_media_score_per_person, _calc_media_banzu_val_workbanzu
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load,
                check_item_ids, hierarchy, idx, media_func[idx], title=title)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def _cal_deduct_score_by_hour(row, columns):
    """计算逻辑：
    基准值（站段级）=Σ时段检查次数/Σ时段作业班组数
    应检查值=该时段作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣2分/日，低于50%的扣4分/日，
    低于100%的扣8分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/时段和次数/时段]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算基准值:
    total_check_count = sum([row[hour] for hour in columns])
    total_banzu_count = sum([row[f'banzu_count_{hour}'] for hour in columns])
    avg_check_count = total_check_count / total_banzu_count

    # 保存中间计算过程
    unfinished_hour = {
        '检查数低于比较值20%的时段': [],
        '检查数低于比较值50%的时段': [],
        '检查数低于比较值100%的时段': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for hour in columns:
        hour_banzu_count = row[f'banzu_count_{hour}']  # 该时段班组数
        hour_check_count = row[hour]  # 该时段检查数
        base_check_count = hour_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (hour_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            hour_deduction = 0
        else:
            if ratio <= -1:
                hour_deduction = -3
                deduct_type = '检查数低于比较值100%的时段'
            elif ratio <= -0.5:
                hour_deduction = -4
                deduct_type = '检查数低于比较值50%的时段'
            elif ratio <= -0.2:
                hour_deduction = -2
                deduct_type = '检查数低于比较值20%的时段'
            deduct_hour = unfinished_hour.get(deduct_type)
            hour_title = f'{(hour - 1) * 2} - {hour * 2}时'
            deduct_hour.append(f'{hour_title}[基准值：{avg_check_count:.1f},' +
                               f'班组数：{hour_banzu_count:.1f}，' +
                               f'比较值：{base_check_count:.1f}' +
                               f'实际检查值：{hour_check_count:.1f}]')
            unfinished_hour.update({deduct_type: deduct_hour})
        score.append(hour_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_hour.items()])
    return total_score, rst_calc_data


def _calc_banzu_count_by_hour(data):
    """班组数的统计是按照不同班组不同的工作时间来计算，且倒班班组数记为：倒班班组/倒班数

    增加描述：统计班组时，若存在多个工作制，24小时工作制>自定义（两小时一间隔）>日勤，
    （24小时-每天算几分之一班组[优先最大班制]，自定义-按时段计算，日勤-工作日上班, 上班时间）,
    上班时间为（8:00-12:00， 2:00-5:30）
    WORK_TIME: 24小时制度（2:二班倒、3:三班倒，1：四班倒）  自定义（两小时一个间隔）
    4、00:00 ~ 02:00   5、02:00~04:00   6、04:00~06:00 7、06:00~08:00
    8、08:00~10:00  9、10:00~12:00  10、12:00~14:00  11、14:00~16:00
    12、16:00~18:00  13、18:00~20:00   14、20:00~22:00  15、22:00~00:00
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, WORK_TYPE, WORK_TIME]
    """
    banzu_count = []
    for dpid, group in data.groupby(['FK_DEPARTMENT_ID']):
        # 计算每个班组在哪些时段有过工作
        work_type_list = group['WORK_TYPE'].values
        if 2 in work_type_list:
            work_time = group[group['WORK_TYPE'] == 2]['WORK_TIME'].values
            work_time = 1 / max([int(x) for x in work_time])
            work_unit = [[dpid, hour, work_time] for hour in range(1, 13)]
        else:
            # 保存日勤制和自定义工作的工作时段集合
            hour_list = []
            for each in group.values:
                if 3 == each[1]:
                    hour_list.extend([int(x) - 3 for x in each[2].split(',')])
                if 1 == each[1]:
                    hour_list.extend([5, 6, 8, 9])
                    if 3 not in work_type_list:
                        break
            work_unit = [[dpid, hour, 1] for hour in set(hour_list)]
        banzu_count.extend(work_unit)
    banzu_count_df = pd.DataFrame(
        data=banzu_count, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return banzu_count_df


def _cal_check_banzu_evenness_score(row, columns, basedata=None):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 保存中间计算过程
    calc_basic_data = {
        '日均班组检查数低于基数20%': 0,
        '日均班组检查数低于基数50%': 0,
        '日均班组检查数低于基数100%': 0,
    }
    score = [100]
    for day in columns:
        # 取某天的某个专业的班组数
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        # 取某天的某个专业的平均检查数
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        if basedata is not None:
            major_avg = float(basedata['AVG_NUMBER'][basedata['DAY'] == day])
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
            calc_basic_data.update({
                '日均班组检查数低于基数100%':
                    calc_basic_data.get('日均班组检查数低于基数100%') + 1
            })
        elif ratio <= -0.5:
            daily_deduction = -2
            calc_basic_data.update({
                '日均班组检查数低于基数50%':
                    calc_basic_data.get('日均班组检查数低于基数50%') + 1
            })
        elif ratio <= -0.2:
            daily_deduction = -1
            calc_basic_data.update({
                '日均班组检查数低于基数20%':
                    calc_basic_data.get('日均班组检查数低于基数20%') + 1
            })
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {v}天' for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _calc_hour_code(hour):
    """1-12 分别代表0-23时， 每2个小时一个时段，左闭又开,ex: [4: 6)
    Arguments:
        hour {int} -- 0 - 24时
    Returns:
        [int] -- 1- 12代表12个时段
    """
    code = 1 + (hour // 2)
    return code


def _calc_between_hours(start_hour, end_hour):
    """计算开始结束时间之间的时段间隔
    Arguments:
        start_hour {int} -- 0 - 23
        end_hour {int} -- 0 -23
    Returns:
        [list[int]] --
    """
    start_hour = _calc_hour_code(start_hour)
    end_hour = _calc_hour_code(end_hour)
    # 跨天
    if start_hour > end_hour:
        return list(range(start_hour, 13)) + list(range(1, end_hour + 1))
    else:
        return list(range(start_hour, end_hour + 1))


def _handle_cross_hour(data, year, month):
    """1.检查时段组成部分，根据每次检查的跨越时段，从0点开始，每两个小时算一个时段，
    计算从检查开始到检查结束的区间，其中每个时段都算在这个时段内的一次检查。
    即：如果从11:00至15:00的检查，
    那么就记为10:00~12:00、12:00~14:00、14:00~16:00各一次检查
    Arguments:
        data {dataFrame} --
            columns=[FK_DEPARTMENT_ID, START_HOUR, END_HOUR, COUNT]
    """
    if month == 1:
        last_month = 12
        last_year = year - 1
    else:
        last_month = month - 1
        last_year = year
    hour_data = []
    for idx, row in data.iterrows():
        start_day = int(row['START_HOUR'][5:7])
        end_day = int(row['END_HOUR'][5:7])
        start_hour = int(row['START_HOUR'][11:])
        end_hour = int(row['END_HOUR'][11:])
        for hour in _calc_between_hours(start_hour, end_hour):
            hour_data.append([row['FK_DEPARTMENT_ID'], hour, row['COUNT']])
        if end_day > start_day:
            # 开始结束日期不在同一天，而且不跨月
            # 再加上中间间隔的天数
            interval_days = (end_day - start_day)
        elif end_day < start_day:
            # 开始时间结束日期不在同一天，而且跨月
            # 再加上中间间隔的天数
            this_month_days = calendar.monthrange(last_year, last_month)[1]
            interval_days = (this_month_days - start_day) + end_day
        else:
            continue
        if start_hour > end_hour:
            interval_days -= 1
        # 计算中间间隔天数的时段
        for hour in range(1, 13):
            hour_data.append(
                [row['FK_DEPARTMENT_ID'], hour, interval_days * row['COUNT']])
    hour_data_df = pd.DataFrame(
        data=hour_data, columns=['FK_DEPARTMENT_ID', 'WORK_HOUR', 'COUNT'])
    return hour_data_df


# 判断是否是周六或者周日
def _is_weekends(year, month, day):
    if day >= current_app.config.get('UPDATE_DAY'):
        if month == 1:
            month = 12
            year -= 1
        else:
            month -= 1
    if calendar.weekday(year, month, day) > 4:
        return True
    else:
        return False


def _get_daily_check_banzu_count(year, month, work_type_1, work_type_2,
                                 work_type_3, day):
    work_banzu = (work_type_1 + work_type_2 + work_type_3)
    if _is_weekends(year, month, int(day)):
        work_banzu -= work_type_1
    return work_banzu


# 检查时间均衡度---日期加时间段
def stats_check_time_evenness_three(
        check_item_ids, daily_check_banzu_count_sql,
        daily_check_count_sql, daily_check_banzu_count_h_sql, hour_check_count_sql,
        department_data, zhanduan_dpid_data, months_ago, risk_type,
        choose_dpid_data, basedata=None, item_weight=[0.5, 0.5]):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    calc_basic_data = []
    for hierarchy in [3]:
        # 检查时段
        banzu_count = _calc_banzu_count_by_hour(
            pd_query(daily_check_banzu_count_h_sql.format(check_item_ids)))
        hour_data = df_merge_with_dpid(banzu_count, department_data)
        if hour_data.empty:
            continue
        # WORK_TYPE(1, 2, 3)代表3种班制
        hour_xdata = hour_data.groupby([f'TYPE{hierarchy}', 'WORK_HOUR'])[
            'COUNT'].sum()
        hour_xdata = hour_xdata.unstack()
        hour_xdata = hour_xdata.fillna(0)
        # 重命名列名
        hour_xdata.rename(
            columns={hour: f'banzu_count_{hour}'
                     for hour in range(1, 13)},
            inplace=True)
        # 每日班组实际检查数
        # 如果检查的时段，2个小时算一个时段
        hour_data_check = df_merge_with_dpid(
            _handle_cross_hour(
                pd_query(hour_check_count_sql.format(
                    *stats_month, check_item_ids)), year,
                month), department_data)
        hour_xdata_check = hour_data_check.groupby([f'TYPE{hierarchy}',
                                                    'WORK_HOUR'])['COUNT'].sum()
        hour_xdata_check = hour_xdata_check.unstack()
        hour_xdata_check = hour_xdata_check.fillna(0)
        columns = hour_xdata_check.columns.values
        hour_xdata = pd.merge(
            hour_xdata, hour_xdata_check, how='left', left_index=True, right_index=True)

        hour_xdata['SCORE_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[0], axis=1)

        hour_xdata['CONTENT_HOUR'] = hour_xdata.apply(
            lambda row: _cal_deduct_score_by_hour(row, columns)[1], axis=1)
        hour_xdata = append_major_column_to_df(zhanduan_dpid_data, hour_xdata)
        hour_xdata.drop(['MAJOR'], inplace=True, axis=1)
        str_tmp = "检查数低于比较值20%的时段: <br/> 检查数低于比较值50%的时段: <br/> 检查数低于比较值100%的时段: <br/>"
        hour_xdata['SCORE_HOUR'].fillna(0, inplace=True)
        hour_xdata['CONTENT_HOUR'].fillna(str_tmp, inplace=True)

        # 检查日期
        # 每日检查班组数
        day_data = df_merge_with_dpid(
            pd_query(daily_check_banzu_count_sql.format(check_item_ids)),
            department_data)
        if day_data.empty:
            continue
        day_xdata = day_data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        day_xdata = day_xdata.unstack()
        for x in [1, 2, 3]:
            if x not in day_xdata.columns.values.tolist():
                day_xdata[x] = 0
        day_xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        day_xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        day_xdata = day_xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        day_columns = xdata_check.columns.values
        day_xdata = pd.merge(
            day_xdata, xdata_check, how='left', left_index=True, right_index=True)
        day_xdata = append_major_column_to_df(zhanduan_dpid_data, day_xdata)
        for day in day_columns:
            new_column = f'daily_check_banzu_count_{day}'
            day_xdata[new_column] = day_xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        major_banzu_count = day_xdata.groupby(['MAJOR']).sum()
        day_xdata = pd.merge(
            day_xdata,
            major_banzu_count,
            how='left',
            left_on='MAJOR',
            right_index=True)
        day_xdata['SCORE_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(
                row, day_columns, basedata)[0],
            axis=1)
        day_xdata['CONTENT_DAY'] = day_xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(
                row, day_columns, basedata)[1],
            axis=1)

        # 聚合检查日期和检查时间
        column = f'SCORE_b_{hierarchy}'
        xdata = pd.merge(hour_xdata, day_xdata, how="right", left_on="DEPARTMENT_ID", right_on="DEPARTMENT_ID")
        xdata['CONTENT'] = xdata.apply(lambda row: '<br/>'.join([row['CONTENT_HOUR'], row['CONTENT_DAY']]), axis=1)
        xdata[column] = xdata.apply(
            lambda row: int(row['SCORE_HOUR'] * item_weight[0]) + int(row['SCORE_DAY'] * item_weight[1]), axis=1)

        # xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        calc_basic_data_rst = format_export_basic_data(xdata.copy(), 4, 2, 3,
                                                       months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                         2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(xdata, choose_dpid_data(hierarchy), column,
                               hierarchy, 2, 4, 2, months_ago, risk_type=risk_type)
        rst_index_score.append(xdata)

    return rst_index_score


# 计算接发列车工作量及车站接法列车总人数
def calc_jiefalc_work_load(jiefalc_count, jiefalc_people_count,
                           a_station_count, b_station_count,
                           a_dpid_station_count, b_dpid_station_count,
                           department_data, months_ago):
    """
    接发列车工作量计算
    :param jiefalc_count: 接法列车数
    :param jiefalc_people_count: 接法列车人数
    :param months_ago: 月份
    :param department_data: 部门
    :param a_station_count: A站段接发列车站点(关联车站个数，但接发列车人数不为0。操作方式条件257)
    :param b_station_count: B站段接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件1346)
    :param a_dpid_station_count: A部门接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件257)
    :param b_dpid_station_count: B部门接发列车站点(关联车站个数，但接发列车人数不为0。不选择操作方式条件1346)
    :return: 接法列车工作量,车站接发列车总人数, 高动客接发列车合成工作量0.1的部门id列表
    """
    data = df_merge_with_dpid(jiefalc_count, jiefalc_people_count, how='right')
    data.fillna(0, inplace=True)

    # 选取2,5,7ctc操作方式的车站
    # data = data[(data['NUMBER'] > 0) & (data['CTC_OPERATION_WAY'].isin([2, 5, 7]))]
    data = data[data['NUMBER'] > 0]

    # 拼接车站数
    data = pd.merge(a_station_count, data, on='TYPE3', how='right')
    data = pd.merge(b_station_count, data, on='TYPE3', how='right')
    data = pd.merge(a_dpid_station_count, data, on='DEPARTMENT_ID', how='right')
    data = pd.merge(b_dpid_station_count, data, on='DEPARTMENT_ID', how='right')
    data.fillna(0, inplace=True)

    data.drop_duplicates(subset=['DEPARTMENT_ID'], keep='first', inplace=True)

    month_day = get_month_day(months_ago)
    data["AVERAGE_JIEFA_COUNT"] = data["NUMBER"] * data['CW_COUNT'] / month_day

    # 站段工作量计算
    work_load = data.groupby('TYPE3')['AVERAGE_JIEFA_COUNT'].sum().reset_index()
    station_count = pd.merge(a_station_count, b_station_count, how='outer', on='TYPE3')
    station_count.fillna(0, inplace=True)
    work_load = pd.merge(
        station_count,
        work_load,
        on='TYPE3',
    )
    work_load['COUNT'] = work_load.apply(
        lambda row:
        (row['A_NODE_COUNT'] + row['B_NODE_COUNT'] * 0.2) * 0.7 / 100 +
        row['AVERAGE_JIEFA_COUNT'] * 0.3 / 3000, axis=1
    )
    work_load.rename(columns={'TYPE3': 'DEPARTMENT_ID'}, inplace=True)
    work_load = pd.merge(
        work_load[['DEPARTMENT_ID', 'COUNT']],
        department_data,
        on='DEPARTMENT_ID'
    )
    # 高动客接发列车合成工作量 0.1及以下的 不纳入基数计算
    filter_department_id_list = work_load[work_load['COUNT'] <= 0.1].DEPARTMENT_ID.tolist()
    # 每个部门工作量
    data['COUNT'] = data.apply(
        lambda row:
        (row['A_DPID_NODE_COUNT'] + row['B_DPID_NODE_COUNT'] * 0.2) * 0.7 / 100 +
        row['AVERAGE_JIEFA_COUNT'] * 0.3 / 3000, axis=1
    )
    # 高动客不过滤操作方式
    # data = data[data['CTC_OPERATION_WAY'].isin([2, 5, 7])]
    data = data[['DEPARTMENT_ID', 'COUNT', 'NODE', "NUMBER",
                 'A_DPID_NODE_COUNT', 'B_DPID_NODE_COUNT', 'AVERAGE_JIEFA_COUNT']]

    # 统计工作量【接发列车工作量】
    dpid_work_load = pd.merge(
        data,
        department_data,
        how='inner',
        left_on='DEPARTMENT_ID',
        right_on='DEPARTMENT_ID'
    )
    # work_load.drop_duplicates(subset=['NODE'], keep='first', inplace=True)
    return work_load, jiefalc_people_count, dpid_work_load, station_count, filter_department_id_list


def calc_jiefalc_count(jiefalc_count, node_ctc):
    """"计算高动客接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    # 统计ctc为2车站车次, CTC为5,7且为字母旅客列车车次(CFCC)
    ctc_and_word_data = data[(data.CTC == 2) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]\d{1,3}[0-8]$') & data.CTC.isin([5, 7])) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]999[0-8]$') & data.CTC.isin([5, 7]))]
    # 编号为1001-7598及7601-8998的旅客列车
    number_cfcc_data = data[data.CFCC.str.contains(r'^\d{4}$') & data.CTC.isin([5, 7])].copy()
    number_cfcc_data['CFCC'] = number_cfcc_data.CFCC.astype(int)

    number_cfcc_data = number_cfcc_data[((1001 <= number_cfcc_data.CFCC) & (7598 >= number_cfcc_data.CFCC)) | (
            (7601 <= number_cfcc_data.CFCC) & (8998 >= number_cfcc_data.CFCC))]

    data = pd.concat([ctc_and_word_data, number_cfcc_data])

    data = data.groupby(['FK_DEPARTMENT_ID'])['CW_COUNT'].sum().reset_index()
    return data


def calc_hour_jiefalc_count(jiefalc_count, node_ctc):
    """"计算时段高动客接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    # 统计ctc为2车站车次, CTC为5,7且为字母旅客列车车次(CFCC)
    ctc_and_word_data = data[(data.CTC == 2) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]\d{1,3}[0-8]$') & data.CTC.isin([5, 7])) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]999[0-8]$') & data.CTC.isin([5, 7]))]
    # 编号为1001-7598及7601-8998的旅客列车
    number_cfcc_data = data[data.CFCC.str.contains(r'^\d{4}$') & data.CTC.isin([5, 7])].copy()

    number_cfcc_data['CFCC'] = number_cfcc_data.CFCC.astype(int)

    number_cfcc_data = number_cfcc_data[((1001 <= number_cfcc_data.CFCC) & (7598 >= number_cfcc_data.CFCC)) | (
            (7601 <= number_cfcc_data.CFCC) & (8998 >= number_cfcc_data.CFCC))]

    data = pd.concat([ctc_and_word_data, number_cfcc_data])

    data = data.groupby(['FK_DEPARTMENT_ID', 'WORK_HOUR'])['CW_COUNT'].sum().reset_index()
    return data


def calc_day_jiefalc_count(jiefalc_count, node_ctc):
    """"计算每日高动客接发列车数"""
    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='left',
        right_on='NODE',
        left_on='NODE'
    )
    data.fillna(0, inplace=True)
    # 统计ctc为2车站车次, CTC为5,7且为字母旅客列车车次(CFCC)
    ctc_and_word_data = data[(data.CTC == 2) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]\d{1,3}[0-8]$') & data.CTC.isin([5, 7])) |
                             (data.CFCC.str.contains(r'^[GDCZTKLY]999[0-8]$') & data.CTC.isin([5, 7]))]
    # 编号为1001-7598及7601-8998的旅客列车
    number_cfcc_data = data[data.CFCC.str.contains(r'^\d{4}$') & data.CTC.isin([5, 7])].copy()

    number_cfcc_data['CFCC'] = number_cfcc_data.CFCC.astype(int)

    number_cfcc_data = number_cfcc_data[((1001 <= number_cfcc_data.CFCC) & (7598 >= number_cfcc_data.CFCC)) | (
            (7601 <= number_cfcc_data.CFCC) & (8998 >= number_cfcc_data.CFCC))]

    data = pd.concat([ctc_and_word_data, number_cfcc_data])

    data = data.groupby(['FK_DEPARTMENT_ID', 'WORK_DAY'])['DAY_COUNT'].sum().reset_index()
    return data


def get_chewu_check_address_standard_data(work_banzu_info_data,
                                          real_check_banzu_data,
                                          department_data,
                                          months_ago,
                                          check_item_ids,
                                          major, is_base_item=True):
    """[将覆盖率数据转成标准格式]
    类型：检查地点数（实际检查班组+实际重要检查点）/地点总数（班组数+重要检查点）
    班组跟重要检查点的工作项目要复合特定指数检查项目
    """
    # 实际检查班组数
    real_check_banzu_data = pd.merge(
        real_check_banzu_data,
        work_banzu_info_data[['FK_DEPARTMENT_ID']],
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    # 删除重复班组id
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    banzu_point_data = work_banzu_info_data
    filled_data = pd_query(BANZU_POINT_SQL.format(major))
    filled_data['COUNT'] = 0
    if real_check_banzu_data.empty:
        # 两个均为空时，补充检查班组数
        real_check_banzu_data = filled_data
    if banzu_point_data.empty:
        # 两个均为空时，补充符合某工作项班组数
        banzu_point_data = filled_data
    return real_check_banzu_data, banzu_point_data


def get_different_level_problem_data(problem_data, depart_data):
    """获取A,B,F1类问题"""
    a_problem = df_merge_with_dp_id(problem_data[problem_data.LEVEL == 'A'], depart_data)
    b_problem = df_merge_with_dp_id(problem_data[problem_data.LEVEL == 'B'], depart_data)
    c_problem = df_merge_with_dp_id(problem_data[problem_data.LEVEL == 'C'], depart_data)
    f1_problem = df_merge_with_dp_id(problem_data[problem_data.LEVEL == 'F1'], depart_data)
    a_problem = a_problem.groupby('TYPE3')['COUNT'].sum().to_frame('A')
    b_problem = b_problem.groupby('TYPE3')['COUNT'].sum().to_frame('B')
    c_problem = c_problem.groupby('TYPE3')['COUNT'].sum().to_frame('C')
    f1_problem = f1_problem.groupby('TYPE3')['COUNT'].sum().to_frame('F1')
    return a_problem, b_problem, c_problem, f1_problem


def stats_hidden_problem_exposure_level(problem_data,
                                        months_ago,
                                        risk_type,
                                        choose_dpid_data,
                                        depart_data
                                        ):
    """
    本单位查处高动客问题A类问题1个+3分、B类问题1个+2分、F1类问题1个+2分，最高+100分
    """
    rst_child_score = []
    # A类,B类,F1类问题数
    a_problem, b_problem, c_problem, f1_problem = get_different_level_problem_data(problem_data, depart_data)
    basic_df = pd.concat([a_problem, b_problem, c_problem, f1_problem], axis=1)
    basic_df.fillna(0, inplace=True)
    tmp = """
    A类问题数: {0:.0f}个<br/>
    B类问题数: {1:.0f}个<br/>
    C类问题数: {2:.0f}个<br/>
    F1类问题数: {3:.0f}个<br/>
    """
    basic_df['CONTENT'] = basic_df.apply(
        lambda row: tmp.format(row['A'], row['B'], row['C'], row['F1']), axis=1
    )
    basic_df['SCORE_b_3'] = basic_df.apply(
        lambda row: min(row['A'] * 6 + row['B'] * 3 + row['F1'] * 4 + row['C'], 100), axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=basic_df.index,
            data=basic_df.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_df[['SCORE_b_3']].copy()
    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 2, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 2, risk_type=risk_type)
    # 导出得分
    summizet_operation_set(
        df_rst,
        choose_dpid_data(3),
        'SCORE_b_3',
        3,
        2,
        5,
        2,
        months_ago,
        risk_type=risk_type)
    rst_child_score.append(df_rst)
    return rst_child_score
