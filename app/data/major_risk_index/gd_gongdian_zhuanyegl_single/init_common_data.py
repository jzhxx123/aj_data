# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/10/11上午10:30
   Change Activity: 2019/10/11上午10:30
-------------------------------------------------
"""

from app.data.major_risk_index.gd_gongdian_zhuanyegl_single.common import get_vitual_major_ids
from app.data.major_risk_index.gd_gongdian_zhuanyegl_single import GLV
from app.data.major_risk_index.gd_gongdian_zhuanyegl_single.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    ids = get_vitual_major_ids(risk_type)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(ids))
    department_data = pd_query(DEPARTMENT_SQL.format(ids))

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)