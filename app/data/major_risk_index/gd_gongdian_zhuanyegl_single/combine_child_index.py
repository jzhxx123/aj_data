# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/10/11上午11:08
   Change Activity: 2019/10/11上午11:08
-------------------------------------------------
"""
from app.data.major_risk_index.gd_gongdian_zhuanyegl_single.common import get_vitual_major_ids
from app.data.major_risk_index.gd_gongdian_zhuanyegl_single.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.major_risk_index.gd_gongdian_zhuanyegl_single import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification, init_common_data)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.util import update_major_maintype_weight


@validate_exec_month
def execute(months_ago):
    risk_name = 5
    risk_type = '工电-13'
    ids = get_vitual_major_ids(risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        func.execute(months_ago, risk_name, risk_type)
    child_index_weight = [0.3, 0.1, 0.1, 0.2, 0.1, 0.2]
    combine_child_index.merge_child_index(ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL,
                                          months_ago, risk_name, risk_type,
                                          child_index_weight=child_index_weight,
                                          vitual_major_ids=ids)
    update_major_maintype_weight(index_type=13, major=risk_type,
                                 child_index_weight=child_index_weight)


if __name__ == '__main__':
    pass
