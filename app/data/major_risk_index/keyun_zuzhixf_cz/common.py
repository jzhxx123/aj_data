#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-05-21
-------------------------------------------------
Change Activity:2019-05-21 14:49
-------------------------------------------------
"""
from app.data.util import pd_query
import datetime
import calendar
import pandas as pd
from app.data.major_risk_index.keyun_zuzhixf_cz.common_sql import (
    CW_VISITOR_RECEVIVED_COUNT_SQL, CW_RELAVITE_STATION_SQL)
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.index.util import (get_custom_month)


def get_vitual_major_ids(zhanduan_type):
    """
    客运组织 - 车务段内容
    :param risk_type:
    :return:
    """
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
    a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
FROM
    t_department AS a
        LEFT JOIN
    t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
WHERE
    a.TYPE = 4 AND a.IS_DELETE = 0
        AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
        AND a.SHORT_NAME != ''
       and a.NAME like '%%{0}%%'
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(zhanduan_type))
    if zhanduan_type == '车站':
        # 甲方要求三大车站 成都车站、贵阳车站、重庆车站
        return ('19B8C3534E125665E0539106C00A58FD', '19B8C3534E205665E0539106C00A58FD',
                '19B8C3534E1E5665E0539106C00A58FD')
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def add_workshop_department_info(check_banzu_count_data, department_data):
    """[summary]
    根据班组的检查项目，给它所属的未配置的车间添加进去
    Arguments:
        check_banzu_count_data {[type]} -- [description]
        department_data {[type]} -- [description]
    """
    check_banzu_count_data = pd.merge(
        check_banzu_count_data,
        department_data,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')
    workshop_dpids = check_banzu_count_data['TYPE4'].values.tolist()
    check_banzu_count_data = check_banzu_count_data[['FK_DEPARTMENT_ID']]
    for dpid in workshop_dpids:
        check_banzu_count_data = check_banzu_count_data.append(
                    [{"FK_DEPARTMENT_ID": dpid}], ignore_index=True)
    check_banzu_count_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    return check_banzu_count_data


def calc_workload(months_ago, department_data):
    """sumary_line
    计算工作量
    Keyword arguments:
    argument -- description
    Return: return_description
    """
    stats_month = get_custom_month(months_ago)
    d_time =  datetime.datetime.strptime(
                str(stats_month[1]), "%Y-%m-%d")
    d_time = d_time.date()
    days_num = calendar.monthrange(d_time.year, d_time.month)[1] #获取一个月有多少天
    start = stats_month[0].replace('-', '')
    end = stats_month[1].replace('-', '')
    work_load = pd.merge(
        pd_query(CW_VISITOR_RECEVIVED_COUNT_SQL.format(
            int(start), int(end)), db_name='db_mid'),
        pd_query(CW_RELAVITE_STATION_SQL),
        on='UNIT',
        how='inner'
    )
    work_load = work_load.groupby(["FK_DEPARTMENT_ID"])["COUNT"].sum().reset_index()
    work_load['COUNT'] = work_load['COUNT'] / days_num * 0.01

    # 客站工作量
    return df_merge_with_dpid(
        work_load,
        department_data
    )
    