# -*- coding: utf-8 -*-
"""
客运-客运组织消防（车站）指数，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)

from app.data.major_risk_index.keyun_zuzhixf_cz import GLV
import pandas as pd
from app.data.util import (
    pd_query, get_coll_prefix, get_history_months)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number, COMMON_INDEX_ORGANIZATION)
from app.data.major_risk_index.common.common_sql import (
    BASE_UNIT_INFO_SQL, EMPLOYED_OUTSIDE_PERSON_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.keyun_zuzhixf.assess_intensity_sql import(
    GANBU_ASSESS_RESPONSIBLE_SQL,
    FEIGANBU_ASSESS_RESPONSIBLE_SQL)
from app.data.major_risk_index.keyun_zuzhixf_cz.common import (
    get_vitual_major_ids, calc_workload)
from app.data.major_risk_index.common.common_sql import(
    WORKER_COUNT_SQL)
from app.data.major_risk_index.keyun_zuzhixf_cz.common_sql import (
    CADRE_COUNT_SQL, WORK_LOAD_SQL, QUANTIZATION_PERSON_SQL)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import (
    CHECK_COUNT_SQL, NORISK_ZUOYE_CHECK_PROBLEM_SQL, NORISK_GUANLI_CHECK_PROBLEM_SQL,
    NORISK_LEVEL_PROBLEM_SQL, NORISK_PROBLEM_CHECK_SCORE_SQL,
    NORISK_XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_CHECK_SQL,
    NORISK_ABOVE_YIBAN_PROBLEM_NUMBER_SQL, NORISK_ALL_PROBLEM_NUMBER_SQL,
    NORISK_ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL, NORISK_ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL,
    NORISK_ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL, NORISK_ZUOYE_PROBLEM_CHECK_SCORE_SQL,
    MEDIA_COST_TIME_SQL, NORISK_MEDIA_PROBLEM_NUMBER_SQL, NORISK_MEDIA_PROBLME_SCORE_SQL,
    NORISK_KAOHE_GL_CHECK_PROBLEM_SQL, NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.keyun_zuzhixf.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_SCORE_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, IndexDivider, STAFF_NUMBER_INFO,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    SPECIAL_CONFIG_STAFF_INFO,
    WORKER_COUNT_INFO, ZUOYE_PROBLEM_COUNT_INFO, 
    GUANLI_PROBLEM_COUNT_INFO, CADRE_COUNT_INFO, 
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    YECHA_COUNT_INFO, MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    KAOHE_GL_CHECK_PROBLEM_INFO, KAOHE_ZY_CHECK_PROBLEM_INFO,
    GANBU_ASSESS_RESPONSIBLE_INFO, ASSESS_RESPONSIBLE_INFO,
    ALL_PROBLEM_NUMBER_INFO, ZUOYE_PROBLEM_CHECK_SCORE_INFO,
    ACTIVE_EVALUATE_SCORE_INFO, QUANTIZATION_PERSON_INFO,
    FEIGANBU_ASSESS_RESPONSIBLE_INFO,
    AWARD_RETURN_MONEY_PROBLEM_INFO, WORKER_LOAD_INFO)


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        STAFF_NUMBER, CHECK_ITEM_IDS, RISK_IDS,\
        WORK_LOAD_DATA, DIAOCHE_POSITION, KEYUN_STAFF_NUMBER, \
        KEYUN_CADRE_NUMBER
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    DIAOCHE_POSITION = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    CALC_MONTH = END, START
    IDS = get_vitual_major_ids("车站")
    # 单位职工人数
    STAFF_NUMBER = GLV.get_value("STAFF_NUMBER")[['DEPARTMENT_ID', 'COUNT']]
    # 工作量
    WORK_LOAD_DATA = GLV.get_value('CWDUAN_WORK_LOAD').copy()
    WORK_LOAD_DATA = WORK_LOAD_DATA[['DEPARTMENT_ID','COUNT']]
    WORK_LOAD_DATA.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    # 客运人数
    KEYUN_STAFF_NUMBER = GLV.get_value("KEYUN_STAFF_NUMBER")[['DEPARTMENT_ID', 'COUNT']]
    # 管理人数
    KEYUN_CADRE_NUMBER = GLV.get_value("KEYUN_CADRE_NUMBER")[['DEPARTMENT_ID', 'COUNT']]


# ------------------------获取比值型相应基数------------------------ #


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    major_dpid = get_major_dpid('车务-x')
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, major_dpid)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 公共部分
    
    # 职工总人数
    STAFF_NUMBER_COUNT = CommonCalcDataType(*SPECIAL_CONFIG_STAFF_INFO)
    STAFF_NUMBER_COUNT.value = [STAFF_NUMBER]

    # 客运人数
    WORKER_COUNT = CommonCalcDataType(*SPECIAL_CONFIG_STAFF_INFO)
    WORKER_COUNT.version = 'v2'
    WORKER_COUNT.description = '客运人数（系统管理-部门管理-专业特殊属性[客运人数]）'
    WORKER_COUNT.value = [KEYUN_STAFF_NUMBER]

    # 管理人数
    CADRE_COUNT = CommonCalcDataType(*SPECIAL_CONFIG_STAFF_INFO)
    CADRE_COUNT.version = 'v3'
    CADRE_COUNT.description = '管理人数（系统管理-部门管理-专业特殊属性[管理人数]）'
    CADRE_COUNT.value = [KEYUN_CADRE_NUMBER]

    # 工作量
    WORK_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    WORK_LOAD_COUNT.value = []
    WORK_LOAD_COUNT.func_version = 'customized_df'
    WORK_LOAD_COUNT.func_value = _calc_work_load
    WORK_LOAD_COUNT.description = '车站工作量'

    # 量化人员数
    QUANTIZATION_PERSON_COUNT = CommonCalcDataType(*QUANTIZATION_PERSON_INFO,)
    QUANTIZATION_PERSON_COUNT.value = [QUANTIZATION_PERSON_SQL.format('{0}', '{1}', DIAOCHE_POSITION)]

    # # 干部数
    # CADRE_COUNT = CommonCalcDataType(*CADRE_COUNT_INFO)
    # CADRE_COUNT.value = [CADRE_COUNT_SQL.format(IDS)]

    # # 作业人数
    # WORKER_COUNT = CommonCalcDataType(*WORKER_COUNT_INFO)
    # WORKER_COUNT.value = [WORKER_COUNT_SQL.format(IDS)]
    # WORKER_COUNT.func_version = 'single_df'


    # 检查力度指数
    
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总数（关联项目)'
    CHECK_COUNT.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.version = 'v3'
    PROBLEM_CHECK_SCORE.description = '检查出问题质量分（关联项目）'
    PROBLEM_CHECK_SCORE.value = [
        NORISK_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 管理项问题数
    GUANLI_PROBLEM_COUNT = CommonCalcDataType(*GUANLI_PROBLEM_COUNT_INFO)
    GUANLI_PROBLEM_COUNT.version = 'v2'
    GUANLI_PROBLEM_COUNT.description = '检查管理项问题数（关联项目）'
    GUANLI_PROBLEM_COUNT.value = [NORISK_GUANLI_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 作业项问题数
    ZUOYE_PROBLEM_COUNT = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ZUOYE_PROBLEM_COUNT.version = 'v3'
    ZUOYE_PROBLEM_COUNT.description = '检查作业项问题数（关联项目）'
    ZUOYE_PROBLEM_COUNT.value = [NORISK_ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 较大问题质量分
    JIAODA_RISK_SCORE = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    JIAODA_RISK_SCORE.version = 'v2'
    JIAODA_RISK_SCORE.description = '较大以及以上风险问题质量分（关联项目）'
    JIAODA_RISK_SCORE.value = [
        NORISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 现场检查较大问题质量分
    XC_JIAODA_RISK_SCORE = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    XC_JIAODA_RISK_SCORE.version = 'v2'
    XC_JIAODA_RISK_SCORE.description = '现场检查较大重大问题质量分（关联项目）'
    XC_JIAODA_RISK_SCORE.value = [
        NORISK_XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 夜查次数
    YECHA_COUNT = CommonCalcDataType(*YECHA_COUNT_INFO)
    YECHA_COUNT.value = [YECHA_CHECK_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.version = 'v2'
    MEDIA_PROBLEM_NUMBER.description = '监控调阅发现问题数（关联项目）'
    MEDIA_PROBLEM_NUMBER.value = [
        NORISK_MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.version = 'v2'
    MEDIA_PROBLME_SCORE.description = '监控调阅质量分（关联项目）'
    MEDIA_PROBLME_SCORE.value = [
        NORISK_MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 考核力度指数
    # 考核作业问题数
    KAOHE_ZY_CHECK_PROBLEM = CommonCalcDataType(*KAOHE_ZY_CHECK_PROBLEM_INFO)
    KAOHE_ZY_CHECK_PROBLEM.version = 'v2'
    KAOHE_ZY_CHECK_PROBLEM.description = '考核作业问题数（关联项目）'
    KAOHE_ZY_CHECK_PROBLEM.value = [NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 考核管理项问题数
    KAOHE_GL_CHECK_PROBLEM = CommonCalcDataType(*KAOHE_GL_CHECK_PROBLEM_INFO)
    KAOHE_GL_CHECK_PROBLEM.version = 'v2'
    KAOHE_GL_CHECK_PROBLEM.description = '考核管理问题数（关联项目）'
    KAOHE_GL_CHECK_PROBLEM.value = [NORISK_KAOHE_GL_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 干部考核金额
    GANBU_ASSESS_RESPONSIBLE = CommonCalcDataType(*GANBU_ASSESS_RESPONSIBLE_INFO)
    GANBU_ASSESS_RESPONSIBLE.value = [GANBU_ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 非干部考核总金额
    FEIGANBU_ASSESS_RESPONSIBLE = CommonCalcDataType(*FEIGANBU_ASSESS_RESPONSIBLE_INFO)
    FEIGANBU_ASSESS_RESPONSIBLE.value = [
        FEIGANBU_ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题暴露度

    # 一般及以上问题数
    ABOVE_YIBAN_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ABOVE_YIBAN_PROBLEM_NUMBER.version = 'v4'
    ABOVE_YIBAN_PROBLEM_NUMBER.description = '一般及以上问题数（关联项目）'
    ABOVE_YIBAN_PROBLEM_NUMBER.value = [
        NORISK_ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般及以上问题质量分
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.version = 'v4'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.description = '一般及以上问题质量分（关联项目）'
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE.value = [
        NORISK_ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.version = 'v3'
    ALL_PROBLEM_NUMBER.description = '所有问题数（关联项目）'
    ALL_PROBLEM_NUMBER.value = [
        NORISK_ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般以及以上作业项问题数
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.version = 'v4'
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.description = '一般以及以上作业项问题数（关联项目）'
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM.value = [
        NORISK_ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 一般及以上作业项问题质量分
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.version = 'v4'
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.description = '一般及以上作业项问题质量分（关联项目）'
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE.value = [
        NORISK_ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 作业项问题质量分
    ZUOYE_PROBLEM_CHECK_SCORE = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    ZUOYE_PROBLEM_CHECK_SCORE.version = 'v3'
    ZUOYE_PROBLEM_CHECK_SCORE.description = '作业项问题质量分（关联项目）'
    ZUOYE_PROBLEM_CHECK_SCORE.value = [NORISK_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 评价力度

    # 干部主动评价记分
    ACTIVE_EVALUATE_SCORE = CommonCalcDataType(*ACTIVE_EVALUATE_SCORE_INFO)
    ACTIVE_EVALUATE_SCORE.value = [
        ACTIVE_EVALUATE_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]
    

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, WORK_LOAD_COUNT),),

        # 问题查出率
        '1-3': (IndexDetails(ZUOYE_PROBLEM_COUNT, WORK_LOAD_COUNT), 
                IndexDetails(GUANLI_PROBLEM_COUNT, WORK_LOAD_COUNT)),

        # 质量均分
        '1-5': (IndexDetails(PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),),

        # 较大风险问题质量均分
        '1-6': (IndexDetails(JIAODA_RISK_SCORE, WORK_LOAD_COUNT), IndexDetails(XC_JIAODA_RISK_SCORE, WORK_LOAD_COUNT)),

        # 夜查率
        '1-7': (IndexDetails(YECHA_COUNT, WORK_LOAD_COUNT),),

        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, WORK_LOAD_COUNT), 
                IndexDetails(MEDIA_PROBLEM_NUMBER, WORK_LOAD_COUNT),
                IndexDetails(MEDIA_PROBLME_SCORE, WORK_LOAD_COUNT)),
        # 干部主动评价记分
        '2-3': (IndexDetails(ACTIVE_EVALUATE_SCORE, QUANTIZATION_PERSON_COUNT),),
        # 考核力度指数
        # 换算单位考核问题数
        '3-1': (IndexDetails(KAOHE_ZY_CHECK_PROBLEM, WORKER_COUNT), IndexDetails(KAOHE_GL_CHECK_PROBLEM, CADRE_COUNT)),

        # 换算单位考核金额
        '3-2': (IndexDetails(GANBU_ASSESS_RESPONSIBLE, CADRE_COUNT), IndexDetails(FEIGANBU_ASSESS_RESPONSIBLE, WORKER_COUNT)),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
            IndexDetails(ALL_PROBLEM_NUMBER, WORK_LOAD_COUNT),
            IndexDetails(PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_NUMBER, WORK_LOAD_COUNT),
            IndexDetails(ABOVE_YIBAN_PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),
            IndexDetails(ZUOYE_PROBLEM_COUNT, STAFF_NUMBER_COUNT),
            IndexDetails(ZUOYE_PROBLEM_CHECK_SCORE, STAFF_NUMBER_COUNT),
            IndexDetails(ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM, STAFF_NUMBER_COUNT),
            IndexDetails(ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE, STAFF_NUMBER_COUNT),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('stats_check_problem_ratio', CHILD_INDEX_SQL_DICT['1-3'])
    GLV.set_value('stats_score_per_person', CHILD_INDEX_SQL_DICT['1-5'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_yecha_ratio', CHILD_INDEX_SQL_DICT['1-7'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_active_score_per_person', CHILD_INDEX_SQL_DICT['2-3'])
    GLV.set_value('stats_check_problem_assess_radio',
                  CHILD_INDEX_SQL_DICT['3-1'])
    GLV.set_value('stats_assess_money_per_person', CHILD_INDEX_SQL_DICT['3-2'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])


def _calc_work_load(
    _sqllist_numerator, 
    zhanduan_dpid_data, 
    department_data, 
    mon_ago, column, dpids):
    """[summary]
    
    Arguments:
        _sqllist_numerator {[type]} -- [description]
        zhanduan_dpid_data {[type]} -- [description]
        department_data {[type]} -- [description]
        mon_ago {[type]} -- [description]
        column {[type]} -- [description]
        dpids {[type]} -- [description]
    """
    data = calc_workload(mon_ago, department_data)
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data