#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness_sql
   Author :       hwj
   date：          2019/12/25下午2:05
   Change Activity: 2019/12/25下午2:05
-------------------------------------------------
"""

# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        COUNT(DISTINCT f.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID   
    WHERE
            e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
            AND a.RISK_LEVEL <= 3
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_problem_base AS a
        left join 
        t_problem_base_risk as b on a.PK_ID = b.FK_PROBLEM_BASE_ID
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 风险包含劳动安全的id
TIME_CHECK_RISK_IDS_SQL = """SELECT
        PK_ID 
    FROM
        t_risk 
    WHERE
        ALL_NAME LIKE '%%劳动安全%%' 
        AND FK_DEPARTMENT_ID = '1ACE7D1C80B44456E0539106C00A2E70KSC' 
    AND IS_DELETE =0
"""


# 每日检查人次
# 均衡度---检查次数以排查风险含有劳动安全的
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT c.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_risk AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
        a.END_CHECK_TIME  BETWEEN '{0}' AND '{1}'
        AND b.FK_RISK_ID IN ({2})
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103) 
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""


# 检查班组数统计
# “主要风险类型”含“调车”或“防溜”的班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        MAX(d.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
        INNER JOIN
        (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_risk as a
    INNER JOIN
    t_risk as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on (c.CHILD_ID =b.SOURCE_ID 
                or c.PK_ID=b.SOURCE_ID
                or c.PARENT_ID=b.SOURCE_ID)
    INNER JOIN
        t_department_and_info as d on d.FK_DEPARTMENT_ID=b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE between 9 and 10
        and b.MAIN_TYPE = 2
            AND a.IS_DELETE = 0
            GROUP BY a.DEPARTMENT_ID
"""


# 重要检查点--PERSON_NUMBER作业人数,
# 此项查出地点为主要生产场所及其关联的部门
CHECK_POINT_PERSON_COUNT_SQL = """SELECT DISTINCT
    tcp.PK_ID AS CHECK_POINT_ID,
    tcp.ALL_NAME AS ADDRESS_NAME,
    tds.SOURCE_DEPARTMENT_ID,
    tds.PERSON_NUMBER 
FROM
    t_check_point AS tcp
    INNER JOIN
    t_department_and_main_production_site AS tds ON tcp.PK_ID = tds.FK_ADDRESS_ID 
WHERE
    tds.TYPE = 2 
    AND tcp.TYPE = 1
    AND tcp.IS_DELETE = 0
    AND tcp.FK_DEPARTMENT_ID IN (
    SELECT
        a.DEPARTMENT_ID
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
    )
"""

# 重要检查点-专业下所有具备检查风险的部门
CHECK_POINT_CONNECT_DEPARTMENT_SQL = """SELECT
    a.FK_DEPARTMENT_ID,
    COUNT( DISTINCT c.PK_ID ) AS PERSON_COUNT 
FROM
    t_department_and_info AS a
    INNER JOIN t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    INNER JOIN t_person AS c ON b.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
    inner join
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_risk as a
    INNER JOIN
    t_risk as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({1}) 
    ) AS d on (d.CHILD_ID =a.SOURCE_ID or d.PK_ID=a.SOURCE_ID or d.PARENT_ID=a.SOURCE_ID) 
WHERE
    MAIN_TYPE = 2 
    AND b.TYPE2 = '{0}'
    AND b.IS_DELETE = 0 
    AND c.IS_DELETE = 0 
GROUP BY
    a.FK_DEPARTMENT_ID
"""

# 重要检查点受检次数
POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID as CHECK_POINT_ID, COUNT(DISTINCT a.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
                    LEFT JOIN
        t_check_info_and_risk as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND d.FK_RISK_ID IN ({2})
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数及其部门人数统计
CHECK_BANZU_PERSON_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS PERSON_COUNT_2
    FROM
        t_department AS a
            INNER JOIN
        t_person AS b ON b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY >= 5
            AND a.IS_DELETE = 0
            AND b.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
    GROUP BY a.DEPARTMENT_ID;
"""

# 班组关联站段
BANZU_CONNECT_DEPARTMENT_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME AS ADDRESS_NAME
    FROM
        t_department
    WHERE
        TYPE BETWEEN 9 AND 10 AND IS_DELETE = 0
        AND TYPE2 = '{0}'
"""

# 班组受检次数
BANZU_CHECKED_COUNT_SQL = """SELECT
        c.DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS CHECK_COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_check_info_and_risk as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND b.CHECK_WAY BETWEEN 1 AND 2
            AND c.TYPE BETWEEN 9 AND 10
            AND c.HIERARCHY >= 5
            AND c.IS_DELETE = 0
            AND d.FK_RISK_ID IN ({2})
    GROUP BY c.DEPARTMENT_ID;
"""