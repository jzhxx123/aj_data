#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification_sql
   Author :       hwj
   date：          2019/12/25下午3:44
   Change Activity: 2019/12/25下午3:44
-------------------------------------------------
"""

# 超期问题数(超期未整改)
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_risk AS c ON c.FK_CHECK_PROBLEM_ID = a.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 1 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""
