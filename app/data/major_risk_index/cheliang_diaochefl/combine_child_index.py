#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/12/25上午10:51
   Change Activity: 2019/12/25上午10:51
-------------------------------------------------
"""
from app.data.major_risk_index.cheliang_diaochefl import (
    check_intensity, check_evenness, problem_rectification, init_common_data, _calc_cardinal_number)
from app.data.major_risk_index.cheliang_diaochefl.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.util import update_major_maintype_weight
from app.utils.decorator import record_func_runtime


@validate_exec_month
def execute(months_ago):
    risk_name = 41
    risk_type = '车辆-11'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    _calc_cardinal_number.get_cardinal_number(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        check_evenness,
        problem_rectification
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    child_index_list = [1, 4, 6]
    child_index_weight = [0.5, 0.3, 0.2]
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight,
        child_index_list=child_index_list,
    )

    update_major_maintype_weight(index_type=11, major=risk_type,
                                 child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )


if __name__ == '__main__':
    pass
