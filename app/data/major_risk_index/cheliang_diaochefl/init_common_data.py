#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/12/25上午9:18
   Change Activity: 2019/12/25上午9:18
-------------------------------------------------
"""
from app.data.index.util import get_query_condition_by_risktype
from app.data.major_risk_index.cheliang_diaochefl import GLV
from app.data.major_risk_index.cheliang_diaochefl.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, OVERHAUL_PERSON_SQL, ALL_CLASSIFY_ID_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)

    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(major))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(major))
    department_data = pd_query(DEPARTMENT_SQL.format(major))
    # 工作量
    all_classify_id = pd_query(ALL_CLASSIFY_ID_SQL)
    all_classify_id = ','.join([str(i) for i in all_classify_id.PK_ID])
    work_load = df_merge_with_dpid(
        pd_query(OVERHAUL_PERSON_SQL.format(major, all_classify_id)),
        department_data)
    risktype_data = get_query_condition_by_risktype(risk_name)
    check_item_ids = risktype_data[0]
    check_risk_ids = risktype_data[1]

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        "WORK_LOAD": work_load,
        "CHECK_ITEM_IDS": check_item_ids,
        "CHECK_RISK_IDS": check_risk_ids,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
