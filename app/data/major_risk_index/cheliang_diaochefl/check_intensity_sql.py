#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql
   Author :       hwj
   date：          2019/12/25上午10:08
   Change Activity: 2019/12/25上午10:08
-------------------------------------------------
"""

# 现场检查次数
# 检查方式为“只要现场检查”
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
            INNER JOIN
        t_check_info_and_risk as d on  d.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY = 1
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND a.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.FK_RISK_ID IN ({3})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
         COUNT(DISTINCT a.PK_ID) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID  
    WHERE
        e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 管理项问题数
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
         COUNT(DISTINCT a.PK_ID) AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID  
    WHERE
        e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """SELECT
    distinct
     b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
     f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
"""


# 较大和重大安全风险问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """SELECT
      distinct 
      b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, 
      f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        a.RISK_LEVEL <= 2
        and e.CHECK_WAY BETWEEN 1 and 4
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """SELECT
        distinct b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
         f.CHECK_SCORE AS COUNT, a.PK_ID
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID 
    WHERE
        e.CHECK_WAY BETWEEN 1 AND 2
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND a.RISK_LEVEL <= 2
        AND d.FK_RISK_ID IN ({2})
        AND e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
"""


# 监控调阅时长
MEDIA_COST_TIME_SQL = """select
    mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as COUNT
    from
     (select distinct a.PK_ID, a.COST_TIME, d.FK_DEPARTMENT_ID
    from 
    t_check_info_and_media as a
    left join 
    t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
    left join
    t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    left join
    t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and c.FK_CHECK_ITEM_ID in ({2})
    AND b.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
    )as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数(风险)
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
         COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
            AND e.CHECK_WAY BETWEEN 3 AND 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)  
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分(风险)
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as COUNT 
from (
SELECT
        DISTINCT
        (a.PK_ID),b.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, f.CHECK_SCORE AS SCORE
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
    WHERE
        e.CHECK_WAY BETWEEN 3 AND 4
        AND d.FK_RISK_ID IN ({2})
        AND e.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数(所有去检查的班组)
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem as e on a.PK_ID= e.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)  
        AND a.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 作业班组数(所有具备检查项目的班组)
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct
    (if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID,
     1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
"""


# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]


# 检查地点数(班组)
CHECK_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem as e on a.PK_ID= e.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY BETWEEN 1 and 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)  
        AND a.SUBMIT_TIME  BETWEEN '{0}' AND '{1}'
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE = 9
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 地点总数(班组)
# “部门维护-配置-主要项目配置”中“主要风险类型”
BANZU_COUNT_SQL = """
SELECT 
    distinct
    (if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID,
     1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
        INNER JOIN
    (SELECT a.PK_ID AS CHILD_ID, b.PK_ID, b.PARENT_ID  FROM
    t_check_item as a
    INNER JOIN
    t_check_item as b on a.PARENT_ID=b.PK_ID
    WHERE
    a.pk_id in ({0})
    ) AS c on (c.CHILD_ID =a.SOURCE_ID or c.PK_ID=a.SOURCE_ID or c.PARENT_ID=a.SOURCE_ID)
WHERE
    a.MAIN_TYPE = 2
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
"""