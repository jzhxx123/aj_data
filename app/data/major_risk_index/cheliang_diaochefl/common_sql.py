#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/12/25上午9:18
   Change Activity: 2019/12/25上午9:18
-------------------------------------------------
"""

# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != "";
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}';
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4
    AND b.TYPE2 = '{1}';
"""


# 所有的检修车间属性id
ALL_CLASSIFY_ID_SQL = """
SELECT
    DISTINCT c.PK_ID 
FROM
    `t_department_classify_config`  as a 
    INNER JOIN
    `t_department_classify_config` AS b ON a.PK_ID = b.PARENT_ID
        INNER JOIN
    `t_department_classify_config`  as c on 
    (b.PK_ID = c.PARENT_ID or b.PK_ID = c.PK_ID or a.PK_ID=c.PK_ID)
WHERE
    a.PK_ID = 2
"""


# 工作量-检修车间职工数
OVERHAUL_PERSON_SQL = """
SELECT
    b.FK_DEPARTMENT_ID, 1 as COUNT
FROM
    `t_department` as a
    INNER JOIN
    t_person as b on a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
WHERE
    a.IS_DELETE=0
    and b.IS_DELETE=0
    and TYPE2='{0}'
    and FK_DEPARTMENT_CLASSIFY_CONFIG_ID IN ({1})
"""