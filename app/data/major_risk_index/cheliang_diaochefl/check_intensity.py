#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/12/25上午10:07
   Change Activity: 2019/12/25上午10:07
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 )
from app.data.major_risk_index.cheliang_diaochefl import GLV
from app.data.major_risk_index.cheliang_diaochefl.check_intensity_sql import (
    CHECK_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    WORK_BANZU_COUNT_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    ZUOYE_CHECK_PROBLEM_SQL, GUANLI_CHECK_PROBLEM_SQL, CHECK_BANZU_COUNT_SQL)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongdian_shigongaq.common import get_check_address_standard_data
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_media_banzu(row, column, major_column, detail_type=None):
    """
    监控调阅班组数，按100%计算
    :param row:
    :param column:
    :param major_column:
    :param detail_type:
    :return:
    """
    _score = min(100, 100 * row[column])
    return round(_score, 2)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME, CHECK_RISK_IDS, CHECK_ITEM_IDS
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, ZUOYE_PROBLEM_COUNT, GUANLI_PROBLEM_COUNT, \
        ALL_PROBLEM_NUMBER, PROBLEM_SCORE, YIBAN_RISK_SCORE, YECHA_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, REAL_CHECK_BANZU_DATA, \
        BANZU_POINT_DATA, DEPARTMENT_DATA, JIAODA_RISK_SCORE, \
        XC_JIAODA_RISK_SCORE, MEDIA_COST_TIME, MEDIA_PROBLEM_NUMBER, \
        MEDIA_PROBLME_SCORE, WORK_BANZU_COUNT, WATCH_MEDIA_DEPARTMENT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    # 统计工作量【施工工作总量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(
            *stats_month, CHECK_ITEM_IDS, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 作业项检查问题数
    ZUOYE_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ZUOYE_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    # 管理项检查问题数
    GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(
            GUANLI_CHECK_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA)

    work_banzu_info_data = pd_query(WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA = get_check_address_standard_data(
        work_banzu_info_data[['FK_DEPARTMENT_ID']],
        DEPARTMENT_DATA, months_ago, CHECK_ITEM_IDS, major, is_base_item=True)

    # 监控调阅时长
    MEDIA_COST_TIME = df_merge_with_dpid(
        pd_query(MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = df_merge_with_dpid(
        pd_query(MEDIA_PROBLME_SCORE_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 站段作业班组数(具备检查项目的班组)
    WORK_BANZU_COUNT = df_merge_with_dpid(
        work_banzu_info_data,
        DEPARTMENT_DATA)

    # 调阅班组数
    watch_media_department = pd_query(
        CHECK_BANZU_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS))
    WATCH_MEDIA_DEPARTMENT = WORK_BANZU_COUNT[
        WORK_BANZU_COUNT.DEPARTMENT_ID.isin(
            watch_media_department.FK_DEPARTMENT_ID)].copy()

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value("stats_check_per_person", (None,))[0]
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
                        + '现场检查作业次数({4})/ 施工工作总量({5})</p>', None]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction,
    )


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    fraction_list = GLV.get_value("stats_check_problem_ratio", (None, None))
    return check_intensity.stats_check_problem_ratio_type_two_type_major(
        ZUOYE_PROBLEM_COUNT,
        GUANLI_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction_list=fraction_list,
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value("stats_score_per_person", (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
        fraction=fraction

    )


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    child_weight = [0.7, 0.3]
    fraction_list = GLV.get_value("stats_risk_score_per_person", (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        child_weight=child_weight,
        calc_score_by_formula=_calc_score_by_formula,
        fraction_list=fraction_list
    )


# 监控调阅力度
def _stats_media_intensity(months_ago):
    title = ['监控调阅时长累计({0})/工作量({1})',
             '监控调阅发现问题数({0})/工作量({1})',
             '监控调阅发现问题质量分累计({0})/工作量({1})',
             '调阅班组数({0})/班组数({1})']

    # 子指数执行需要的数据字典
    media_func_data_dict = {
        'media_cost_time': MEDIA_COST_TIME,
        'media_problem_number': MEDIA_PROBLEM_NUMBER,
        'media_problem_score': MEDIA_PROBLME_SCORE,
        'monitor_watch_discovery_ratio_list': [
            WATCH_MEDIA_DEPARTMENT, WORK_BANZU_COUNT],
    }
    calc_score_by_formula_dict = {
        'media_cost_time': _calc_score_by_formula,
        'media_problem_number': _calc_score_by_formula,
        'media_problem_score': _calc_score_by_formula,
        'monitor_watch_discovery_ratio_list': _calc_score_by_formula_media_banzu,
    }
    fraction_list = GLV.get_value(
        'stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0],
        fraction_list[1],
        fraction_list[2],
        None
    )
    return check_intensity.stats_media_intensity_excellent(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        title=title,
        choose_dpid_data=_choose_dpid_data,
        media_func_data_dict=media_func_data_dict,
        calc_score_by_formula_dict=calc_score_by_formula_dict,
        fraction_list=fraction_list
    )


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_score_per_person,
        _stats_risk_score_per_person,
        _stats_media_intensity,
        _stats_check_address_ratio,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'i', 'j']]
    item_weight = [0.30, 0.10, 0.25, 0.15, 0.15, 0.05]
    update_major_maintype_weight(index_type=11, major=risk_type,
                                 main_type=1,
                                 child_index_list=[2, 3, 5, 6, 9, 10],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
