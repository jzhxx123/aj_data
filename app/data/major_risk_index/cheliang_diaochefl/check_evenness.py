#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness
   Author :       hwj
   date：          2019/12/25下午2:05
   Change Activity: 2019/12/25下午2:05
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.common import get_major_dpid, get_check_address_basic_data
from app.data.major_risk_index.cheliang_diaochefl import GLV
from app.data.major_risk_index.cheliang_diaochefl.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    CHECK_BANZU_PERSON_COUNT_SQL,
    BANZU_CONNECT_DEPARTMENT_SQL,
    BANZU_CHECKED_COUNT_SQL,
    CHECK_POINT_PERSON_COUNT_SQL,
    CHECK_POINT_CONNECT_DEPARTMENT_SQL,
    POINT_CHECKED_COUNT_SQL, TIME_CHECK_RISK_IDS_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, CHECK_ADDRESS_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS, LAO_AN_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    """每日检查次数低于每日比较值20%的扣1分/日，低于50%的扣2分/日，低于100%的扣3分/日"""
    # 检查次数以排查风险含有劳动安全的
    LAO_AN_RISK_IDS = ','.join(
        [str(i) for i in pd_query(TIME_CHECK_RISK_IDS_SQL).PK_ID])

    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_RISK_IDS)), DEPARTMENT_DATA)

    check_banzu_person_count_sql = CHECK_BANZU_PERSON_COUNT_SQL.format(major)
    banzu_connect_department_sql = BANZU_CONNECT_DEPARTMENT_SQL.format(major)
    banzu_checked_count_sql = BANZU_CHECKED_COUNT_SQL.format(
        *stats_month, LAO_AN_RISK_IDS)
    check_point_person_count_sql = CHECK_POINT_PERSON_COUNT_SQL.format(major)
    check_point_connect_department_sql = \
        CHECK_POINT_CONNECT_DEPARTMENT_SQL.format(major, CHECK_RISK_IDS)
    point_checked_count_sql = POINT_CHECKED_COUNT_SQL.format(
        *stats_month, LAO_AN_RISK_IDS)

    CHECK_ADDRESS_DATA = get_check_address_basic_data(
        months_ago, major, CHECK_ITEM_IDS,
        check_banzu_person_count_sql=check_banzu_person_count_sql,
        banzu_connect_department_sql=banzu_connect_department_sql,
        banzu_checked_count_sql=banzu_checked_count_sql,
        check_point_person_count_sql=check_point_person_count_sql,
        check_point_connect_department_sql=check_point_connect_department_sql,
        point_checked_count_sql=point_checked_count_sql
    )


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """一般及以上问题项点数/基础问题库中一般及以上问题项点数"""
    return check_evenness.stats_problem_point_evenness_major(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):

    # 主要风险类型”含“调车”或“防溜”的班组数
    daily_check_banzu_count_sql = DAILY_CHECK_BANZU_COUNT_SQL.format(
        CHECK_RISK_IDS)
    return check_evenness.stats_check_time_evenness(
        LAO_AN_RISK_IDS, daily_check_banzu_count_sql, DAILY_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    check_banzu_count = CHECK_ADDRESS_DATA.get('check_banzu_count')
    check_point_count = CHECK_ADDRESS_DATA.get('check_point_count')
    banzu_department_checked_count = CHECK_ADDRESS_DATA.get('banzu_department_checked_count')
    check_point_checked_count = CHECK_ADDRESS_DATA.get('check_point_checked_count')
    check_point_connect_department = CHECK_ADDRESS_DATA.get('check_point_connect_department')
    banzu_connect_department = CHECK_ADDRESS_DATA.get('banzu_connect_department')

    return check_evenness.stats_check_address_evenness_new(
        check_banzu_count, check_point_count,
        banzu_department_checked_count, check_point_checked_count,
        check_point_connect_department, banzu_connect_department,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness,
        _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=11, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
