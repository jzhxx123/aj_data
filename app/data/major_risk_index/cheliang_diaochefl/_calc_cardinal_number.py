#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     _calc_cardinal_number
   Author :       hwj
   date：          2019/12/25下午5:08
   Change Activity: 2019/12/25下午5:08
-------------------------------------------------
"""

from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)
from app.data.major_risk_index.cheliang_diaochefl import GLV
from app.data.major_risk_index.cheliang_diaochefl.check_intensity_sql import (
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    CHECK_COUNT_SQL, MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    GUANLI_CHECK_PROBLEM_SQL, ZUOYE_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.cheliang_diaochefl.common_sql import (
    OVERHAUL_PERSON_SQL, ALL_CLASSIFY_ID_SQL)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import (
    BASE_UNIT_INFO_SQL)
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    WORKER_LOAD_INFO, GUANLI_PROBLEM_COUNT_INFO,
    ZUOYE_PROBLEM_COUNT_INFO)
from app.data.util import (pd_query)


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH, \
        CHECK_ITEM_IDS, RISK_IDS, MAJOR, \
        YEJIAN_WORK_BANZU_COUNT, WORK_LOAD_DATA, OVERHAUL_PERSON
    MAJOR = get_major_dpid(risk_type)
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    CALC_MONTH = end, start
    # 工作量(检修车间人数)
    all_classify_id = pd_query(ALL_CLASSIFY_ID_SQL)
    all_classify_id = ','.join([str(i) for i in all_classify_id.PK_ID])
    OVERHAUL_PERSON = pd_query(
        OVERHAUL_PERSON_SQL.format(MAJOR, all_classify_id))


# ------------------------获取比值型相应基数------------------------ #
def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, MAJOR)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 公共部分
    # 工作量
    work_load_count = CommonCalcDataType(*WORKER_LOAD_INFO)
    work_load_count.value = [OVERHAUL_PERSON]
    work_load_count.func_version = 'single_df'
    work_load_count.description = '调车防溜工作量'

    # 检查力度指数
    # 检查次数
    check_count = CommonCalcDataType(*CHECK_COUNT_INFO)
    check_count.version = 'v2'
    check_count.description = '现场检查总数（关联项目,风险)'
    check_count.value = [
        CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS, RISK_IDS)]

    # 管理项问题数
    guanli_problem_count = CommonCalcDataType(*GUANLI_PROBLEM_COUNT_INFO)

    guanli_problem_count.value = [
        GUANLI_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 作业项问题数
    zuoye_problem_count = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    zuoye_problem_count.value = [
        ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 问题质量分
    problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    problem_check_score.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 较大问题质量分
    jiaoda_risk_score = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    jiaoda_risk_score.value = [
        RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    xc_jiaoda_risk_score = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    xc_jiaoda_risk_score.value = [
        XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅时长
    media_cost_time = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    media_cost_time.version = 'v2'
    media_cost_time.description = '监控调阅时长(关联项目)'
    media_cost_time.value = [
        MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    media_problem_number = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    media_problem_number.value = [
        MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅质量分
    media_problme_score = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    media_problme_score.value = [
        MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 参与基数计算的sql字典
    child_index_sql_dict = {
        # 换算单位检查频次
        '1-2': (IndexDetails(check_count, work_load_count),),

        # 查处问题率
        '1-3': (IndexDetails(zuoye_problem_count, work_load_count),
                IndexDetails(guanli_problem_count, work_load_count)),

        # 质量均分
        '1-5': (IndexDetails(problem_check_score, work_load_count),),

        # 较大风险问题质量均分
        '1-6': (IndexDetails(jiaoda_risk_score, work_load_count),
                IndexDetails(xc_jiaoda_risk_score, work_load_count)),

        # 监控调阅力度
        '1-10': (IndexDetails(media_cost_time, work_load_count),
                 IndexDetails(media_problem_number, work_load_count),
                 IndexDetails(media_problme_score, work_load_count)),

    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', child_index_sql_dict['1-2'])
    GLV.set_value('stats_check_problem_ratio', child_index_sql_dict['1-3'])
    GLV.set_value('stats_score_per_person', child_index_sql_dict['1-5'])
    GLV.set_value('stats_risk_score_per_person', child_index_sql_dict['1-6'])
    GLV.set_value('stats_media_intensity', child_index_sql_dict['1-10'])
