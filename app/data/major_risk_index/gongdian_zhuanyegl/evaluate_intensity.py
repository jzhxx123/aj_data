#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     evaluate_intensity_sql
   Author :       hwj
   date：          2019/8/26下午6:42
   Change Activity: 2019/8/26下午6:42
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.evaluate_intensity_sql import (
    ACCUMULATIVE_EVALUATE_SCORE_SQL, LUJU_EVALUATE_SCORE_SQL,
    ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL, PERSON_ID_CARD_SQL, ZHANDUAN_EVALUATE_SCORE_SQL)
from app.data.major_risk_index.gongdian_zhuanyegl import GLV
from app.data.major_risk_index.gongdian_zhuanyegl.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_zhuanyegl.common_sql import CADRE_COUNT_SQL
from app.data.major_risk_index.gongdian_zhuanyegl.evaluate_intensity_sql import EVALUATE_COUNT_SQL, \
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, DUAN_CADRE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL, ACTIVE_EVALUATE_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid,
    summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common import evaluate_intensity


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global RISK_IDS
    global DEPARTMENT_DATA, CHEJIAN_DPID_DATA, ZHANDUAN_DPID_DATA, TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_KEZHI_COUNT, \
        ACTIVE_EVALUATE_COUNT, DUAN_CADRE_COUNT, CADRE_COUNT, ACTIVE_EVALUATE_SCORE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    RISK_IDS = diaoche[1]

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 主动评价记分条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(
            ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(pd_query(CADRE_COUNT_SQL.format(major, ids)), DEPARTMENT_DATA)

    # 干部主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    """主动评价记分条数/评价记分总条数"""
    return evaluate_intensity.stats_active_ratio(
        ACTIVE_EVALUATE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    """科职干部评价记分条数/评价记分总条数"""
    return evaluate_intensity.stats_gradation_ratio(
        ACTIVE_EVALUATE_KEZHI_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    """段机关干部评价记分条数/评价记分总条数"""
    return evaluate_intensity.stats_duan_ratio(
        DUAN_CADRE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 干部人均主动评价记分条数
def _stats_count_per_person(months_ago):
    """主动评价记分条数/干部总人数"""
    return evaluate_intensity.stats_count_per_person(
        ACTIVE_EVALUATE_COUNT, CADRE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 干部人均主动评价记分分数
def _stats_score_per_person(months_ago):
    """主动评价记分分数/干部总人数"""
    return evaluate_intensity.stats_score_per_person_major(
        ACTIVE_EVALUATE_SCORE,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 评价集中度
def _stats_concentartion_ratio_of_evaluation(months_ago):
    """造成单人处置方式升级的1人+N分"""
    return evaluate_intensity.stats_concentartion_ratio_of_evaluation_two(
        ACCUMULATIVE_EVALUATE_SCORE_SQL,
        LUJU_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
        PERSON_ID_CARD_SQL,
        DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        risk_ids=RISK_IDS)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_active_ratio,
        _stats_gradation_ratio,
        _stats_duan_ratio,
        _stats_count_per_person,
        _stats_score_per_person,
        _stats_concentartion_ratio_of_evaluation,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'd', 'e', 'b', 'c', 'g']]
    item_weight = [0.2, 0.2, 0.2, 0.1, 0.1, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=3, major=risk_type, main_type=2, child_index_list=[1, 4, 5, 2, 3, 7],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
