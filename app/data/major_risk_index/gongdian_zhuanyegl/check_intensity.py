# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql.py
   Author :       hwj
   date：          2019/8/26下午2:39
   Change Activity: 2019/8/26下午2:39
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.gongdian_zhuanyegl import GLV
from app.data.major_risk_index.gongdian_zhuanyegl.check_intensity_sql import CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, \
    PROBLEM_CHECK_SCORE_SQL, YIBAN_RISK_PROBLEM_NUMBER_SQL, REAL_CHECK_BANZU_SQL, REAL_CHECK_POINT_SQL, BANZU_POINT_SQL, \
    CHECK_POINT_SQL
from app.data.major_risk_index.gongdian_zhuanyegl.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_zhuanyegl.common_sql import WORK_LOAD_SQL, EXTERNAL_PERSON_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YIBAN_RISK_PROBLEM_NUMBER, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, ALL_PROBLEM_NUMBER, \
        DEPARTMENT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    # 统计工作量【职工总人数】
    # 正式职工人数
    staff_number = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(major, ids)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    zhanduan_staff = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([staff_number, zhanduan_staff], axis=0, sort=False)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 一般及以上风险问题数
    YIBAN_RISK_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(
            YIBAN_RISK_PROBLEM_NUMBER_SQL.format(
                *stats_month, risk_ids)), DEPARTMENT_DATA)

    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
               + '现场检查作业次数({4})/ 总人数({5})</p>', None]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        calc_score_by_formula=_calc_score_by_formula)


# 问题平均质量分
def _stats_score_per_check_problem(months_ago):
    return check_intensity.stats_score_per_check_problem(
        PROBLEM_SCORE,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 人均质量分
def _stats_score_per_person(months_ago):
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = '
               + '问题质量分累计({4})/ 总人数({5})</p>', None]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        calc_score_by_formula=_calc_score_by_formula)


# 一般以上风险占比
def _stats_yiban_risk_ratio(months_ago):
    return check_intensity.stats_yiban_risk_ratio_type_one(
        YIBAN_RISK_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
               + '检查地点数({4})/ 地点总数({5})*100%</p>', None]
    return check_intensity.stats_check_address_ratio_include_check_point(
        REAL_CHECK_BANZU_SQL,
        REAL_CHECK_POINT_SQL,
        BANZU_POINT_SQL,
        CHECK_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_score_per_check_problem,
        _stats_score_per_person,
        _stats_yiban_risk_ratio,
        _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'k', 'e', 'h', 'i']]
    item_weight = [0.30, 0.20, 0.20, 0.20, 0.10]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=1, child_index_list=[2, 11, 5, 8, 9],
                                 child_index_weight=item_weight
                                 )
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
