# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification_sql
   Author :       hwj
   date：          2019/8/26下午7:16
   Change Activity: 2019/8/26下午7:16
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.problem_rectification_sql import RESPONSIBE_SAFETY_PRODUCE_INFO_SQL
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import REPEATE_HAPPEN_PROBLEM_SQL
from app.data.major_risk_index.gongdian_zhuanyegl import GLV
from app.data.major_risk_index.gongdian_zhuanyegl.common import get_vitual_major_ids
from app.data.major_risk_index.gongdian_zhuanyegl.common_sql import WORK_LOAD_SQL, EXTERNAL_PERSON_SQL
from app.data.major_risk_index.gongdian_zhuanyegl.problem_rectification_sql import HAPPEN_PROBLEM_POINT_SQL, \
    OVERDUE_PROBLEM_NUMBER_SQL, CHECK_EVALUATE_SZ_SCORE_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL, WARNING_DELAY_SQL, \
    MAJOR_PROBLEM_POINT_INFO_SQL
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query, update_major_maintype_weight

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, \
        WORKER_COUNT, PROBLEM_POINT_INFO_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 统计工作量【职工总人数】
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(major, ids))
    # 正式职工人数
    staff_number = df_merge_with_dpid(WORKER_COUNT, DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    zhanduan_staff = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([staff_number, zhanduan_staff], axis=0, sort=False)
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major, ids))


# 整改时效
def _stats_rectification_overdue(months_ago):
    """问题整改超期1条扣0.2分"""
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 整改履责
def _stats_check_evaluate(months_ago):
    """ZG-1、2、3、4、5分别扣评价条款基础分*8*职务系数"""
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=8,
        calc_func=lambda x: min(100, 40 + x))


# 问题控制
def _stats_repeatedly_index(months_ago):
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20}
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


# 整改复查
def _stats_rectification_review(months_ago):
    return problem_rectification.stats_rectification_review(
        CHECK_ITEM_IDS, WORK_LOAD, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula)


# 整改成效
def _stats_repeatedly_effect(months_ago):
    """警告性预警一次扣10分；(暂未加入)
    事故主要、全部责任的1个扣20分、重要扣15分、次要的扣10分（含追究责任）；
    D21与故障：主要、全部责任的1个扣6分、重要扣4分、次要的扣2分
    综合信息主要、全部责任的1个扣3分、重要扣2分、次要的扣1分。
    直接在总分中扣，改项最多扣40分（总分不能低于0)。只统计“涉及局属单位责任”的事故
    """
    return problem_rectification.stats_rectification_effect_two(
        CHECK_RISK_IDS, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_check_evaluate,
        _stats_repeatedly_index,
        _stats_rectification_review,
        _stats_repeatedly_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'f']]
    item_weight = [0.25, 0.25, 0.25, 0.25, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=3, major=risk_type, main_type=6, child_index_list=[1, 2, 3, 4, 6],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
