# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common.py
   Author :       hwj
   date：          2019/8/26下午4:37
   Change Activity: 2019/8/26下午4:37
-------------------------------------------------
"""

import pandas as pd
from app.data.index.util import get_custom_month
from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, df_merge_with_dpid, calc_extra_child_score_groupby_major, \
    calc_extra_child_score_groupby_major_third, summizet_operation_set, export_basic_data_tow_field_monthly_two, \
    calc_child_index_type_sum
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 50
        _score = 0 if _score < 0 else _score
    return _score


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER')


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major)


def stats_total_problem_exposure(check_item_ids, check_problem_sql, work_load,
                                 department_data, months_ago, risk_type,
                                 choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]

    weight_item = [0.5, 0.5]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
        '一般及以上作业项问题数({0})/人数({1})'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load, department_data, i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in [3]:
        score = []
        for i, data in enumerate(
                [base_data, risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load, department_data,
                         choose_dpid_data, weight, hierarchy))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data, problem_risk_score={'1': 4, '2': 2, '3': 0.1, },
        columns_list=[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                      (2, 3), (3, 1), (3, 2), (3, 3)]):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(safety_produce_info_sql.format(check_item_ids, *calc_month))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        risk_level = int(each_problem[1])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data, zhanduan_dpid,
                                                          5, 5, 3, months_ago, first_title, second_title,
                                                          columns_list=columns_list)
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(lambda row:
                                                 row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 5, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5,
                                     5, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type)
    return rst_child_score
