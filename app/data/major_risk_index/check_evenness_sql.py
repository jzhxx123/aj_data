# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT PROBLEM_POINT) AS COUNT
    FROM
        t_check_problem
    WHERE
        DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND RISK_LEVEL <= 3
            AND FK_CHECK_ITEM_ID IN ({2})
    GROUP BY EXECUTE_DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PROBLEM_POINT) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 3 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
            AND FK_CHECK_ITEM_ID IN ({0})
    GROUP BY FK_DEPARTMENT_ID;
"""

# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT
        PK_ID AS CHECK_POINT_ID, FK_DEPARTMENT_ID
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0;
"""

# 重要检查点受检次数
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID AS DEPARTMENT_ID
    FROM
        t_address_and_map AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 1
            AND a.FK_DEPARTMENT_ID IS NOT NULL
            AND b.TYPE BETWEEN 9 AND 10;
"""

# 班组受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_department AS c ON a.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 1
            AND c.TYPE BETWEEN 9 AND 10
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE BETWEEN 9 AND 10
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""
