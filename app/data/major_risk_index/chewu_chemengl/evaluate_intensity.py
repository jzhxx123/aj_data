#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.common.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, ANALYSIS_CENTER_ASSESS_SQL,
    DUAN_CADRE_COUNT_SQL, EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_ALL_SQL)
from app.data.util import pd_query
from app.data.major_risk_index.dianwu_laoan.evaluate_intensity_sql import (
    ACCUMULATIVE_EVALUATE_SCORE_SQL, LUJU_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
    ZHANDUAN_EVALUATE_SCORE_SQL, PERSON_ID_CARD_SQL
)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, \
        ACTIVE_EVALUATE_KEZHI_COUNT, ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        ANALYSIS_CENTER_ASSESS_SCORE, DUAN_CADRE_COUNT, RISK_IDS,\
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, ACTIVE_EVALUATE_SCORE_ALL
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]
    RISK_IDS = risk_ids
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 干部主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(
            ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 集中评价度数据
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    ACTIVE_EVALUATE_SCORE_ALL = pd_query(
        ACTIVE_EVALUATE_SCORE_ALL_SQL.format(year, month, risk_ids))


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    # 各个站段的分数
    return evaluate_intensity.stats_active_ratio(
        ACTIVE_EVALUATE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 干部人均主动评价记分条数
def _stats_count_per_person(months_ago):
    return evaluate_intensity.stats_count_per_person(
        ACTIVE_EVALUATE_COUNT, CADRE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 人均评价记分
def _stats_score_per_person(months_ago):
    return evaluate_intensity.stats_score_per_person(
        ACTIVE_EVALUATE_SCORE,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    return evaluate_intensity.stats_gradation_ratio(
        ACTIVE_EVALUATE_KEZHI_COUNT, TOTAL_EVALUATE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    return evaluate_intensity.stats_duan_ratio(
        DUAN_CADRE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 分析中心得分
def _stats_analysis_center_assess(months_ago):
    return evaluate_intensity.stats_analysis_center_assess(
        ANALYSIS_CENTER_ASSESS_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 评价集中度
def _stats_concentartion_ratio_of_evaluation(months_ago):
    return evaluate_intensity.stats_concentartion_ratio_of_evaluation(
        ACCUMULATIVE_EVALUATE_SCORE_SQL, LUJU_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
        PERSON_ID_CARD_SQL, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago,
        RISK_TYPE, _choose_dpid_data, RISK_IDS)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_score_per_person, _stats_active_ratio, _stats_count_per_person,
        _stats_gradation_ratio, _stats_duan_ratio,
        _stats_analysis_center_assess, _stats_concentartion_ratio_of_evaluation
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f', 'g']]
    item_weight = [0.4, 0.13, 0.15, 0.07, 0.05, 0.1, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd']]
    # item_weight = [0.4, 0.4, 0.2]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 2, months_ago,
    #                      item_name, item_weight, [4])
    # current_app.logger.debug(
    #     '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
