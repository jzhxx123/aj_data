#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.check_evenness_sql import (
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    return check_evenness.stats_check_time_evenness(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_evenness.stats_check_address_evenness(
        CHECK_ITEM_IDS, CHECK_BANZU_COUNT_SQL,
        BANZU_DEPARTMENT_CHECKED_COUNT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.3, 0.3]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
