#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.gongwu_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.gongwu_laoan.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, CHECK_POINT_CONNECT_DEPARTMENT_SQL,
    BANZU_CONNECT_DEPARTMENT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_POINT_COUNT_SQL,
    CHECK_POINT_CHECKED_COUNT_SQL, CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL)
from app.data.major_risk_index.gongwu_laoan.common import get_all_check_item_ids
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common.common import get_major_dpid



def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_fun(row, column, major_column, count_column, df_type3_count):
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio >= -0.15:
        _score = 90 + _ratio * 66
    elif _ratio >= -0.3:
        _score = 80 + (_ratio + 0.15) * 160
    else:
        _score = 71 + _ratio * 50
    return _score / df_type3_count[row[count_column]]


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, \
        CHECK_POINT_CONNECT_DEPARTMENT, \
        BANZU_CONNECT_DEPARTMENT, BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_COUNT, \
        CHECK_POINT_CONNECT_DEPARTMENT, CHECK_POINT_CHECKED_COUNT, CHECK_BANZU_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')

    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, RISK_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = get_all_check_item_ids(major)
    RISK_IDS = diaoche[1]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS)), DEPARTMENT_DATA)

    # 检查班组数统计
    CHECK_BANZU_COUNT = pd_query(CHECK_BANZU_COUNT_SQL.format(major))

    # 班组关联站段(选取部门全称)
    BANZU_CONNECT_DEPARTMENT = pd_query(BANZU_CONNECT_DEPARTMENT_SQL.format(major))

    # 班组受检次数--不关联项目
    BANZU_DEPARTMENT_CHECKED_COUNT = pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQL.format(*stats_month, major, CHECK_ITEM_IDS))

    # 重要检查点
    CHECK_POINT_COUNT = pd_query(CHECK_POINT_COUNT_SQL.format(major))

    # 重要检查点关联部门(选取部门全称)
    CHECK_POINT_CONNECT_DEPARTMENT = pd_query(CHECK_POINT_CONNECT_DEPARTMENT_SQL.format(major))

    # 重要检查点受检次数--不关联项目
    CHECK_POINT_CHECKED_COUNT = pd_query(CHECK_POINT_CHECKED_COUNT_SQL.format(*stats_month, major, CHECK_ITEM_IDS))
# 问题均衡度


def _stats_problem_point_evenness(months_ago):
    """
    查出一般以上问题项点数/基础问题库中一般以上问题项点数（符合风险类型为人身伤害风险、其它归类-劳安的问题）。
    得分=占比*100
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = " +
                        "查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>", None]
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data,
        customizecontent=customizecontent)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    """
    检查日期（按每日来考虑工作量）：每日检查人次数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，低于100%的扣3分/日，得分=100-扣分
    :param months_ago:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    major = get_major_dpid('工务-1')
    daily_check_count_sql = DAILY_CHECK_COUNT_SQL.format(*stats_month, major, CHECK_ITEM_IDS)
    return check_evenness.stats_check_time_evenness(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, daily_check_count_sql,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """
    受检地点超过平均值400%以上的一处扣2分;受检地点低于基数50%的一处扣2分，未检查的一处扣5分
    :param months_ago:
    :return:
    """
    return check_evenness.stats_check_address_evenness_new(
        CHECK_BANZU_COUNT, CHECK_POINT_COUNT,
        BANZU_DEPARTMENT_CHECKED_COUNT, CHECK_POINT_CHECKED_COUNT,
        CHECK_POINT_CONNECT_DEPARTMENT, BANZU_CONNECT_DEPARTMENT,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    update_major_maintype_weight(index_type=2, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
