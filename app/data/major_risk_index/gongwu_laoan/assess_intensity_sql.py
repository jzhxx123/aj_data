#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
包含路外问题

"""
# 考核作业项问题数
KAOHE_PROBLEM_SQL = """SELECT
        MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER join
        t_person as c on a.CHECK_PERSON_ID_CARD = c.ID_CARD
            inner join
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2}) 
    GROUP BY a.PK_ID;
"""

# 月度考核总金额(关联问题)
ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            INNER JOIN
        t_check_info as e on d.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 月度返奖金额
AWARD_RETURN_SQL = """
select ars.FK_DEPARTMENT_ID, SUM(ars.COUNT) as COUNT
from (
SELECT
        distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            inner JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID 
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            INNER JOIN
        t_check_info as e on d.FK_CHECK_INFO_ID = e.PK_ID
            INNER JOIN
        t_person AS tp ON a.ID_CARD = tp.ID_CARD
    WHERE
        b.STATUS = 3
        AND a.IS_RETURN = 1
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        AND tp.IDENTITY <> '干部'
        ) as ars
    GROUP BY ars.FK_DEPARTMENT_ID
"""
