from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    REAL_CHECK_BANZU_SQLIST, CHECK_INFO_SQL,
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL,RISK_LEVEL_PROBLEM_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    MEDIA_COST_TIME_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
)

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_check_problem_and_responsible_department as d on b.pk_id = d.FK_CHECK_PROBLEM_ID
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_check_problem_and_risk as a on a.fk_check_problem_id = b.pk_id
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND a.fk_risk_id IN ({2})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""


# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """
SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0 AND HIERARCHY = 2
        AND TYPE = 1
    GROUP BY FK_DEPARTMENT_ID;
"""

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
"""

# 检查次数（现场检查）
# 检查信息里默认不要102，103等
CHECK_COUNT_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT, a.PK_ID
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 获取专业及路局的所有检查项目
ALL_CHECK_ITEM_IDS_SQL = """SELECT
    b.PK_ID 
    FROM
    `t_department` AS a
    INNER JOIN t_check_item AS b ON ( a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID ) 
    WHERE
    (
    a.TYPE2 = '{0}' 
    OR a.DEPARTMENT_ID IN ( SELECT DEPARTMENT_ID FROM t_department WHERE HIERARCHY = 1 AND IS_DELETE = 0 )
    ) 
    AND a.IS_DELETE = 0 
    AND b.IS_DELETE =0
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        1 AS COUNT,
        a.PK_ID
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_YECHA = 1
"""

# 实际重要检查地点数（含检查项目）
REAL_CHECK_POINT_SQL = """SELECT
max(c.FK_DEPARTMENT_ID), 1 AS COUNT,
max(b.PK_ID) as PK_ID
FROM
t_check_info_and_address AS a
    INNER JOIN
t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
    INNER JOIN
t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
WHERE
a.TYPE = 2
AND c.HIERARCHY = 2
AND c.IS_DELETE = 0
AND c.TYPE = 1
AND b.CHECK_WAY BETWEEN 1 AND 2
AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
AND b.CHECK_TYPE NOT IN (102, 103)
AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
GROUP BY a.FK_CHECK_POINT_ID
"""

CHECK_INFO_AND_ITEM_SQL = """SELECT 
    a.PK_ID, b.FK_CHECK_ITEM_ID
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
	DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    """
