#!/usr/bin/python3
# -*- coding: utf-8 -*-
from app.data.major_risk_index.common.evaluate_intensity_sql import \
    ACTIVE_EVALUATE_SCORE_SQL

# (施工、天窗修、点外修作业人数）工作量里面的人数，是派工单中的上单作业人数
# gw_t_ps_dailyplan表中的tyoe字段表示作业类型
# 天窗修：{0:"请点“,(5,8):"一级维修",(6,9):"二级维修"}
# 施工：{"一级施工","二级施工",(3,4):"三级施工"}
# 点外修:{(1,2):"点外作业"}
SHIGONG_COUNT_SQL = """
SELECT
        a.DEPARTID AS FK_DEPARTMENT_ID, 
        SUM( a.ON_RAILWAY_NUMBER ) AS COUNT 
    FROM
        gw_t_ps_dailyplan AS a 
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND 
        b.DISPATCH_ORDER_STATUS = 1 
    GROUP BY
    a.DEPARTID 
"""