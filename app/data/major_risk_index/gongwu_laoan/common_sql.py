# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
         --   AND a.TYPE3 not in {1}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
         --   AND a.TYPE3 not in {1}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
        --    AND a.TYPE3 not in {1}
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 施工劳动安全劳时
# gw_t_ps_dailyplan表中的tyoe字段表示作业类型
# 天窗修：{0:"请点“,(5,8):"一级维修",(6,9):"二级维修"}
# 施工：{"一级施工","二级施工",(3,4):"三级施工"}
# 点外修:{(1,2):"点外作业"}
# 但是此处不计算作业类型
TOTAL_LABORTIME_SQL = """
SELECT
    gw.DEPARTID AS FK_DEPARTMENT_ID,
    SUM( gw.ON_RAILWAY_NUMBER * gw.COUNT ) / 3600 AS COUNT 
FROM
    (
    SELECT
        max(a.DEPARTID) as DEPARTID,
        max(a.ON_RAILWAY_NUMBER) as ON_RAILWAY_NUMBER,
        ( UNIX_TIMESTAMP( max(b.END_TIMES) ) - UNIX_TIMESTAMP( max(b.START_TIMES) ) ) AS COUNT 
    FROM
        gw_t_ps_dailyplan AS a
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) <= DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )  
        AND 
        b.DISPATCH_ORDER_STATUS = 1   
        group by a.pk_id ) AS gw 
GROUP BY
    gw.DEPARTID
"""

# 派工单数量
ORDER_COUNT_SQL = """
SELECT
    max(b.DEPARTID) AS FK_DEPARTMENT_ID,
    1 AS COUNT 
FROM
    `gw_t_ps_dailyplan_complete` AS a
    INNER JOIN 
    gw_t_ps_dailyplan AS b ON a.DAILY_PLAN_ID = b.PK_ID 
WHERE
    DATE_FORMAT( a.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
    AND 
    DATE_FORMAT( a.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
    AND 
    a.DISPATCH_ORDER_STATUS = 1 
GROUP BY
    b.pk_id
"""

# 工作时间分布
WORK_TIME_RECORD_SQL = """
SELECT
        MAX(a.DEPARTID) as DEPARTID,
        MAX(a.ON_RAILWAY_NUMBER) as ON_RAILWAY_NUMBER,
        MAX(b.END_TIMES) as END_TIMES,
        MAX(b.START_TIMES) as START_TIMES
    FROM
        gw_t_ps_dailyplan AS a
        LEFT JOIN 
        gw_t_ps_dailyplan_complete AS b ON a.PK_ID = b.DAILY_PLAN_ID 
    WHERE
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
        AND 
        DATE_FORMAT( b.END_TIMES, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
        AND 
        b.DISPATCH_ORDER_STATUS = 1
        group by a.PK_ID
"""
