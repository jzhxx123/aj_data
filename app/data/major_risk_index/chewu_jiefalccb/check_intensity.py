# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/8/14上午9:16
   Change Activity: 2019/8/14上午9:16
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_jiefalccb import GLV
from app.data.major_risk_index.chewu_jiefalccb.check_intensity_sql import CHECK_COUNT_SQL,\
    PROBLEM_CHECK_SCORE_SQL, YECHA_CHECK_SQL, RISK_LEVEL_PROBLEM_SQL, \
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YECHA_JIEFALC_COUNT_SQL
from app.data.major_risk_index.chewu_jiefalccb.common import calc_jiefalc_count, stats_media_intensity_major
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.check_intensity_sql import REAL_CHECK_BANZU_SQL, BANZU_POINT_SQL, \
    ALL_PROBLEM_NUMBER_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index, \
    calc_child_index_type_divide_major
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_COUNT,\
        PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, JIEFALC_PEOPLE_COUNT, NODE_CTC
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 接发列车数
    JIEFALC_COUNT = GLV.get_value('JIEFALC_COUNT')
    # 接发列车作业人数
    JIEFALC_PEOPLE_COUNT = GLV.get_value('JIEFALC_PEOPLE_COUNT')
    # 接发列车工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    NODE_CTC = GLV.get_value('NODE_CTC')
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, check_item_ids)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    """现场检查接发列车作业检查次数/多方向接发列车工作量"""
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    """多方向接发列车问题数/多方向接发列车工作量
    """
    return check_intensity.stats_check_problem_ratio_type_one_major(
        PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """较大和重大安全风险问题（作业项+专业管理项+设备质量）质量分累计/多方向接发列车工作量"""
    return calc_child_index_type_divide_major(
        JIAODA_RISK_SCORE,
        WORK_LOAD,
        2,
        1,
        6,
        months_ago,
        'COUNT',
        'SCORE_f',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    """接发列车（作业项+专业管理项+设备质量）问题质量分累计/多方向接发列车工作量"""
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
    )


# 夜查率
def _stats_yecha_ratio(months_ago):
    """现场检查接发列车夜查次数（22:00-6:00，时间段内的检查不少于30分钟）/∑站段管内各站“夜间（22:00-6:00）多方向接发列车工作量"""
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>夜查率({3}) = " + \
                       "夜查次数({4})/ 多方向接发列车夜间工作量({5})*100%</p>"
    stats_month = get_custom_month(months_ago)
    # 夜间接法列车数
    yecha_jiefalc_cfcc_count = pd_query(YECHA_JIEFALC_COUNT_SQL.format(*stats_month), db_name='db_mid')
    yecha_jiefalc_count = calc_jiefalc_count(yecha_jiefalc_cfcc_count, NODE_CTC)

    data = pd.merge(
        yecha_jiefalc_count,
        WORK_LOAD[['DEPARTMENT_ID', 'NODE', 'ALL_NODE_NUMBER']],
        how='right',
        left_on='NODE',
        right_on='NODE'
    )
    data.fillna(0, inplace=True)

    data["COUNT"] = data["ALL_NODE_NUMBER"] * data['CW_COUNT']
    data.drop_duplicates(subset=['NODE'], keep='first', inplace=True)
    data.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
    data = data[['FK_DEPARTMENT_ID', 'COUNT']]

    yecha_work_load = df_merge_with_dpid(
        data,
        DEPARTMENT_DATA)
    return check_intensity.stats_yecha_ratio_major(
        YECHA_COUNT,
        yecha_work_load,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    """
    监控调阅发现接发列车防错办问题数/多方向接发列车工作量(50%)
    监控调阅问题质量单位均分/多方向接发列车工作量(50%)
    """
    return stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """检查接发列车班组数/有接发列车作业班组数"""
    return check_intensity.stats_check_address_ratio(
        REAL_CHECK_BANZU_SQL,
        BANZU_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_risk_score_per_person,
        _stats_score_per_person, _stats_yecha_ratio,
        _stats_check_address_ratio, _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'e', 'f', 'g', 'i', 'j']
    ]
    item_weight = [0.15, 0.15, 0.20, 0.20, 0.10, 0.05, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=9, major=risk_type, main_type=1, child_index_list=[2, 3, 5, 6, 7, 9, 10],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
