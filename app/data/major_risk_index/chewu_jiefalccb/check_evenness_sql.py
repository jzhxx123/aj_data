# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness_sql
   Author :       hwj
   date：          2019/8/14上午11:34
   Change Activity: 2019/8/14上午11:34
-------------------------------------------------
"""
# 一般以上项点问题数--均为站段自查(a.TYPE=3)
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """SELECT
        b.TYPE3 AS FK_DEPARTMENT_ID,
        COUNT(DISTINCT a.PROBLEM_POINT) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_department AS b ON a.EXECUTE_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.TYPE3
"""

# 基础问题库中一般及以上风险项点问题数--均为站段自查(b.TYPE=3)
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PROBLEM_POINT) AS COUNT
    FROM
        t_problem_base AS a
            LEFT JOIN   
        t_check_problem  AS b on b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 车站受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID AS DEPARTMENT_ID, COUNT(DISTINCT b.PK_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            INNER JOIN
        t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        a.TYPE = 1
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY NOT BETWEEN 4 AND 6
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 车站接发列车数-时段
STATION_JIEFALC_COUNT_HOUR_SQL = """
SELECT
NODE,
( DATE_FORMAT( DDSJ, '%%H' ) DIV 2 ) + 1 AS WORK_HOUR,
COUNT( 1 ) AS HOUR_COUNT 
FROM
t_td_data 
WHERE
DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )
GROUP BY
NODE,
WORK_HOUR
"""

# 车站接发列车数- 每日
STATION_JIEFALC_COUNT_DAILY_SQL = """
select 
NODE,
DAY(DDSJ)  AS WORK_DAY,
COUNT( 1 ) AS DAY_COUNT 
FROM
t_td_data 
WHERE
DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )
GROUP BY
NODE,
WORK_DAY
"""