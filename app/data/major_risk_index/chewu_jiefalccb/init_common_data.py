# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/8/14上午9:11
   Change Activity: 2019/8/14上午9:11
-------------------------------------------------
"""
from app.data.index.util import get_custom_month
from app.data.major_risk_index.chewu_jiefalccb import GLV
from app.data.major_risk_index.chewu_jiefalccb.common import get_vitual_major_ids, calc_jiefalc_count, \
    calc_jiefalc_work_load
from app.data.major_risk_index.chewu_jiefalccb.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, \
    JIEFALC_COUNT_SQL, JIEFALC_CTC_WAY_SQL, JIEFALC_PEOPLE_COUNT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids('客运-1')
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(
        major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(
        major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(
        major, ids))

    # 接发列车数
    jiefalc_and_checi_count = pd_query(JIEFALC_COUNT_SQL.format(*stats_month), db_name='db_mid')
    node_ctc = pd_query(JIEFALC_CTC_WAY_SQL)
    jiefalc_count = calc_jiefalc_count(jiefalc_and_checi_count, node_ctc)

    # 接发列车工作人数
    jiefalc_people_count = df_merge_with_dpid(
        pd_query(JIEFALC_PEOPLE_COUNT_SQL),
        department_data)

    # 接法列车工作量,车站接发列车总人数
    work_load, jiefalc_people_count = calc_jiefalc_work_load(jiefalc_count, jiefalc_people_count, department_data)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'JIEFALC_COUNT': jiefalc_count,
        'JIEFALC_PEOPLE_COUNT': jiefalc_people_count,
        'NODE_CTC': node_ctc,
        'WORK_LOAD': work_load
    }
    # 设置对应的全局变量
    GLV.set_all_values(values)
