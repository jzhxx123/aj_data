# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/8/14上午9:12
   Change Activity: 2019/8/14上午9:12
-------------------------------------------------
"""
# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
            AND a.TYPE3 not in {1}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
            AND a.TYPE3 not in {1}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
            AND a.TYPE3 not in {1}
"""

# 接发列车
JIEFALC_COUNT_SQL = """
SELECT
NODE,
COUNT( 1 ) AS CW_COUNT
FROM
t_td_data 
WHERE
DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' ) 
GROUP BY
NODE
"""
# 接发列车作业人数--接发列车人数
JIEFALC_PEOPLE_COUNT_SQL = """
SELECT
a.FK_DEPARTMENT_ID,
a.ACCEPT_DEPARTURE_NUMBER AS NUMBER,
substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
CTC_OPERATION_WAY
FROM
t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 )
"""

# 接发列车(多方向,ctc为2,5,7)
JIEFALC_CTC_WAY_SQL = """
SELECT
c.CTC,
c.NODE,
c.IS_MULTI_DIRECTION 
FROM
(
SELECT
    a.CTC_OPERATION_WAY AS CTC,
    substring_index( substring_index( a.ASSOCIATED_STATION, ',', b.help_topic_id + 1 ), ',',- 1 ) NODE,
    IS_MULTI_DIRECTION
FROM
    t_department_type_cw a
JOIN mysql.help_topic b ON b.help_topic_id < 
( length( a.ASSOCIATED_STATION ) - length( REPLACE ( a.ASSOCIATED_STATION, ',', '' ))+ 1 ) 
) AS c
WHERE c.IS_MULTI_DIRECTION =1 and c.ctc in (2,5,7)
GROUP BY 	
c.NODE,
c.CTC,
c.IS_MULTI_DIRECTION
"""
