# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/8/14上午9:11
   Change Activity: 2019/8/14上午9:11
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_jiefalccb.check_intensity_sql import \
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL

from app.data.major_risk_index.util import append_major_column_to_df, format_export_basic_data, \
    write_export_basic_data_to_mongo, summizet_operation_set, df_merge_with_dpid, \
    calc_extra_child_score_groupby_major_third, calc_child_index_type_sum, add_avg_score_by_major, \
    export_basic_data_tow_field_monthly_two
from app.data.util import pd_query, get_history_months

HIERARCHY = [3]


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    BELONG_PROFESSION_ID = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = BELONG_PROFESSION_ID.get(major, 898)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


# 计算接发列车工作量及车站接法列车总人数
def calc_jiefalc_work_load(jiefalc_count, jiefalc_people_count, department_data):
    """
    接发列车工作量计算
    :param jiefalc_count: 多方向,且ctc为2,5,7的车站接法列车数
    :param jiefalc_people_count: 接法列车人数
    :param department_data: 部门
    :return: 接法列车工作量,车站接发列车总人数
    """
    data = pd.merge(
        jiefalc_count,
        jiefalc_people_count,
        how='left',
        left_on='NODE',
        right_on='NODE'
    )
    data.fillna(0, inplace=True)

    # 选取2,5,7ctc操作方式的车站
    data = data[data['CTC_OPERATION_WAY'].isin([2, 5, 7])]

    jiefalc_people_count = data.groupby(['NODE'])['NUMBER'].sum().to_frame(name='ALL_NODE_NUMBER')

    data = pd.merge(
        data,
        jiefalc_people_count,
        how='left',
        left_on='NODE',
        right_index=True
    )
    data["COUNT"] = data["ALL_NODE_NUMBER"] * data['CW_COUNT']
    data = data[['DEPARTMENT_ID', 'COUNT', 'NODE', "ALL_NODE_NUMBER"]]

    # 统计工作量【接发列车工作量】
    work_load = pd.merge(
        data,
        department_data,
        how='inner',
        left_on='DEPARTMENT_ID',
        right_on='DEPARTMENT_ID'
    )
    work_load.drop_duplicates(subset=['NODE'], keep='first', inplace=True)
    return work_load, jiefalc_people_count


def calc_jiefalc_count(jiefalc_count, node_ctc):
    """
    计算多方向车站的接发列车数
    :param jiefalc_count: 车站接发列车数
    :param node_ctc: ctc为2,5,7,多方向的车站
    :return:
    """

    data = pd.merge(
        jiefalc_count,
        node_ctc,
        how='right',
        right_on='NODE',
        left_on='NODE'
    )

    data = data.groupby(['NODE'])['CW_COUNT'].sum().reset_index()
    return data


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_media_val_person_major(choose_dpid_data, series, work_load, hierarchy,
                                 idx, title=['监控调阅时长累计({0})/总人数({1})',
                                             '监控调阅发现问题数({0})/总人数({1})',
                                             '监控调阅发现问题质量分累计({0})/总人数({1})',
                                             '调阅班组数({0})/班组数({1})'],
                                 zhanduan_filter_list=None,
                                 calc_score_by_formula=_calc_score_by_formula):
    work_load = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load = work_load.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='media', denominator='PERSON_NUMBER', zhanduan_filter_list=zhanduan_filter_list,
    )
    # 中间计算数据
    data.fillna(0, inplace=True)
    if len(data) > 0:
        data[f'middle_{idx}'] = data.apply(
            lambda row: title[idx].format(
                f'{round(row["media"], 4)}', round(row['PERSON_NUMBER'], 4)),
            axis=1)
    else:
        data[f'middle_{idx}'] = ''
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅人均问题个数
def _calc_media_problem_per_person_major(choose_dpid_data, stats_month,
                                         department_data, work_load, check_item_ids,
                                         hierarchy, idx, media_problem_number_sql, title,
                                         zhanduan_filter_list=None,
                                         calc_score_by_formula=_calc_score_by_formula):
    media_time = df_merge_with_dpid(
        pd_query(
            media_problem_number_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        zhanduan_filter_list=zhanduan_filter_list)


# 监控调阅人均质量分
def _calc_media_score_per_person_major(choose_dpid_data, stats_month,
                                       department_data, work_load, check_item_ids,
                                       hierarchy, idx, media_problem_score_sql, title,
                                       zhanduan_filter_list=None,
                                       calc_score_by_formula=_calc_score_by_formula,
                                       ):
    media_time = df_merge_with_dpid(
        pd_query(media_problem_score_sql.format(*stats_month, check_item_ids)),
        department_data)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person_major(choose_dpid_data, media_time, work_load,
                                        hierarchy, idx, title=title,
                                        calc_score_by_formula=calc_score_by_formula,
                                        zhanduan_filter_list=zhanduan_filter_list)


# 监控调阅力度
def stats_media_intensity_major(department_data,
                                work_load,
                                months_ago,
                                risk_name,
                                risk_type,
                                choose_dpid_data=None,
                                media_problem_number_sql=None,
                                media_problem_score_sql=None,
                                child_weight=[0.5, 0.5],
                                zhanduan_filter_list=[],
                                title=[
                                    '监控调阅发现接发列车防错办问题数({0})/多方向接发列车工作量({1})',
                                    '监控调阅问题质量单位均分({0})/多方向接发列车工作量({1})',
                                ],
                                calc_score_by_formula=_calc_score_by_formula):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    # 保存中间计算过程数据
    calc_basic_data = []
    media_func = [media_problem_number_sql, media_problem_score_sql,
                  ]
    if media_problem_number_sql is None:
        media_func = [
            MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL
        ]
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_problem_per_person_major, _calc_media_score_per_person_major,
        ]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(
                choose_dpid_data, stats_month, department_data, work_load, check_item_ids,
                hierarchy, idx, media_func[idx], title=title, zhanduan_filter_list=zhanduan_filter_list,
                calc_score_by_formula=calc_score_by_formula)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        print('SCORE', score)
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join(
                [row[col] for col in columns if pd.notnull(row[col])]), axis=1)
        calc_df_data = append_major_column_to_df(
            choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(
            calc_df_data, 1, 10, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 1, 10, risk_type=risk_type)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            1,
            10,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


# 问题均衡度
def stats_problem_point_evenness(
        generally_above_problem_point_count,
        generally_above_problem_point_in_problem_base_count,
        work_load, months_ago,
        risk_type, choose_dpid_data):
    rst_index_score = []
    df_numerator = generally_above_problem_point_count
    df_denominator = generally_above_problem_point_in_problem_base_count
    ser_numerator = df_numerator.groupby(
        ['TYPE3'])['COUNT'].sum()
    ser_denominator = df_denominator.groupby(
        ['TYPE3'])['COUNT'].sum()
    work_load = work_load.groupby(['TYPE3'])['COUNT'].sum()
    df_calc = pd.concat(
        [
            ser_numerator.to_frame(name='numerator'),
            ser_denominator.to_frame(name='denominator'),
            work_load.to_frame(name='work_load')
        ],
        axis=1,
        sort=False)
    df_calc.dropna(subset=['denominator'], inplace=True)
    df_calc.fillna(0, inplace=True)
    # 分母为0时ratio赋予0
    df_calc['ratio'] = df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100 / row["work_load"])
        if row["work_load"] > 0 and row['denominator'] > 0 else 0, axis=1)

    # 问题均衡度
    radio = df_calc.apply(
        lambda row: (row['numerator'] / row['denominator'] * 100)
        if row['denominator'] > 0 else 0, axis=1)

    major_avg_score = sum(radio) / sum(df_calc['work_load'])

    df_calc['AVG_NUMBER'] = major_avg_score
    data = append_major_column_to_df(choose_dpid_data(3), df_calc)

    data['SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'ratio', 'AVG_NUMBER', 1),
        axis=1)
    # 增加平均分一列
    data = add_avg_score_by_major(data, 'SCORE')
    data.fillna(0, inplace=True)
    data['group_sort'] = data['SCORE'].groupby(data['MAJOR']).rank(
        ascending=0, method='first')
    column = 'SCORE_a_3'
    df_calc = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data['SCORE'].values,
        columns=[column])
    content_tmp = '<p>得分：{0:.2f}</p><p>专业平均得分：{6:.2f}</p><p>排名: {1:.0f}</p><p>专业基数: {2:.6f}</p><p>问题查处均衡度({3:.6f}) = ' \
                  + '查出一般以上问题项点数({4:.0f})/ (基础问题库中一般以上问题项点数({5:.0f})* 工作量({7:.0f}))*100%</p>'

    data['CONTENT'] = data.apply(lambda row:
                                 content_tmp.format(row['SCORE'], row['group_sort'],
                                                    row['AVG_NUMBER'],
                                                    row['ratio'], row['numerator'],
                                                    row['denominator'], row['AVG_SCORE'],
                                                    row['work_load']), axis=1)
    calc_basic_data_rst = format_export_basic_data(
        data.copy(), 4, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 4, 1, risk_type=risk_type)

    # 将最终的各单位的得分进行排名入库等一系列操作
    summizet_operation_set(df_calc, choose_dpid_data(3), column,
                           3, 2, 4, 1,
                           months_ago, risk_type)
    rst_index_score.append(df_calc[[column]])

    return rst_index_score


def calc_score_by_formula_address(row, column, major_column):
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = ((row['COUNT'] / row['WORK_COUNT']) /
                  (row['zhanduan_count'] / row['zhanduan_work_count']) - row[major_column]) / row[major_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


def _export_calc_address_evenness_data(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        work_load = row['WORK_COUNT']
        point_name = row['NODE']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if avg_check_count == 0:
            continue
        check_desc = (f'{point_name}站(比较值：{avg_check_count:.2f}, '
                      f'受检次数：{real_check_count:.0f}, '
                      f'基准值：{base_check_count:.4f}, '
                      f'工作量：{work_load:.0f})')
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= 4:
                cnt.update({
                    '受检次数超过比较值400%以上地点：':
                        cnt.get('受检次数超过比较值400%以上地点：', '') + '<br/>' + check_desc
                })
            elif _ratio <= -0.5:
                cnt.update({
                    '受检次数低于比较值50%地点：':
                        cnt.get('受检次数低于比较值50%地点：', '') + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


# 检查地点均衡度
def stats_check_address_evenness(check_item_ids, work_load,
                                 check_banzu_count_sql,
                                 check_point_count_sql,
                                 banzu_department_checked_count_sql,
                                 check_point_checked_count_sql,
                                 department_data, zhanduan_dpid_data,
                                 months_ago, risk_type, choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    work_load.rename(columns={
        "COUNT": "WORK_COUNT",
    }, inplace=True)
    # 班组受检次数
    banzu_check = pd_query(
        banzu_department_checked_count_sql.format(*stats_month,
                                                  check_item_ids))
    point_check = pd_query(
        check_point_checked_count_sql.format(*stats_month,
                                             check_item_ids))
    data_check_banzu = pd.merge(
        work_load,
        banzu_check,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.fillna(0, inplace=True)
    zhanduan_count = data_check_banzu.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='zhanduan_count')
    zhanduan_work_count = data_check_banzu.groupby(['TYPE3'])['WORK_COUNT'].sum().to_frame(name='zhanduan_work_count')
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_work_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu.drop(['DEPARTMENT_ID', 'NAME', "TYPE"], inplace=True, axis=1)
    data = pd.merge(
        data_check_banzu,
        zhanduan_dpid_data,
        how='left',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID')

    # 计算基准值
    data['BASE_CHECK_COUNT'] = data.apply(
        lambda row: (row['zhanduan_count'] / row['zhanduan_work_count'])
        if row['zhanduan_work_count'] > 0 else 0, axis=1)
    # 计算比较值=地点工作量*基准值
    data['AVG_CHECK_COUNT'] = data.apply(lambda row: row['WORK_COUNT'] * row['BASE_CHECK_COUNT'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: calc_score_by_formula_address(row, 'COUNT', 'AVG_CHECK_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


# 他查问题暴露度
def stats_other_problem_exposure(
        check_item_ids, self_check_problem_sql, other_check_problem_sql,
        safety_produce_info_sql, zhanduan_dpid, department_data, months_ago,
        risk_type, choose_dpid_data, problem_risk_score={1: 4, 2: 2, 3: 0.1, },
        columns_list=[(1, 1), (1, 2), (1, 3), (2, 1), (2, 2),
                      (2, 3), (3, 1), (3, 2), (3, 3)]):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣2分，严重风险扣4分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(safety_produce_info_sql.format(check_item_ids, *calc_month))
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(check_item_ids, *calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    first_title = {1: "事故", 2: "故障", 3: "综合信息", }
    second_title = {1: '严重风险', 2: '较大风险', 3: '一般风险'}
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        risk_level = int(each_problem[1])
        problem_score = problem_risk_score.get(risk_level, 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, risk_level])

    other_problem_data = pd.DataFrame(data=calc_problems, columns=['FK_DEPARTMENT_ID', 'RISK_LEVEL'])
    other_problem_data['RISK_LEVEL'] = other_problem_data['RISK_LEVEL'].apply(lambda x: second_title.get(int(x)))
    other_problem_data = df_merge_with_dpid(other_problem_data, department_data)
    other_problem_data = other_problem_data.groupby(['TYPE3', 'RISK_LEVEL']).size()
    other_problem_data = other_problem_data.unstack()
    other_problem_data.fillna(0, inplace=True)
    columns = other_problem_data.columns.tolist()
    title = '未自查出问题<br/>'
    other_problem_data['CONTENT'] = other_problem_data.apply(
        lambda row: title + ';'
            .join(f'{col}: {int(row[col])}个' for col in columns),
        axis=1)
    other_problem_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=other_problem_data.index,
            data=other_problem_data.loc[:, 'CONTENT'].values,
            columns=['middle_2']))
    other_problem_data['middle_2'].fillna('未自查出问题<br/>严重风险:0个;较大风险:0个;一般风险:0个', inplace=True)

    # 未自查出安全生产信息问题
    safety_basic_data = []
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            risk_level = int(row['PROBLEM_DPID_RISK'].split('||')[2])
            main_type = int(row['MAIN_TYPE'])
            problem_score = problem_risk_score.get(risk_level, 0) * (4 - main_type)
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, risk_level])
                safety_basic_data.append([problem_dpid, main_type, risk_level])
    # 导出中间计算过程

    safety_data = export_basic_data_tow_field_monthly_two(safety_basic_data, department_data, zhanduan_dpid,
                                                          5, 5, 3, months_ago, first_title, second_title,
                                                          columns_list=columns_list)
    calc_df_data = pd.merge(
        safety_data,
        other_problem_data,
        left_index=True,
        right_on="DEPARTMENT_ID",
        how="left"
    )
    calc_df_data["CONTENT"] = calc_df_data.apply(lambda row:
                                                 row["middle_2"] + "<br/><br/>安全生产信息问题<br/>" + row["middle_1"], axis=1)
    calc_df_data.set_index("DEPARTMENT_ID", inplace=True)
    calc_df_data = append_major_column_to_df(
        zhanduan_dpid,
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))

    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 5, 3,
                                                   months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5,
                                     5, risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, 2, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, choose_dpid_data, risk_type=risk_type)
    return rst_child_score