# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/8/14上午9:10
   Change Activity: 2019/8/14上午9:10
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import (check_intensity,
               combine_child_index, init_common_data, check_evenness, problem_exposure)
