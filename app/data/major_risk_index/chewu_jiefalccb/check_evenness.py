# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness
   Author :       hwj
   date：          2019/8/14上午11:34
   Change Activity: 2019/8/14上午11:34
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_jiefalccb import GLV
from app.data.major_risk_index.chewu_jiefalccb.check_evenness_sql import GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, \
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, STATION_JIEFALC_COUNT_HOUR_SQL, \
    STATION_JIEFALC_COUNT_DAILY_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQL
from app.data.major_risk_index.chewu_jiefalccb.common import stats_problem_point_evenness, stats_check_address_evenness
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.check_evenness_sql import DAILY_CHECK_COUNT_SQL, HOUR_CHECK_COUNT_SQL, \
    CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL, CHECK_POINT_CHECKED_COUNT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, WORK_LOAD
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    # 接发列车工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """一般以上问题项点数/基础问题库中一般以上问题项点数"""
    return stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, WORK_LOAD, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    """检查时段（2小时一个时段）（50%）：低于基数20%的扣2分/时段，低于50%的扣4分/时段，低于100%的扣8分/时段,
                                    基数=时段检查总人次/时段接发列车工作量
       检查日期（按每日来考虑工作量）（50%）：日检查数/日接发列车工作量,基数=日检查总人次/日接发列车工作量
    """
    # 统计时段工作量
    stats_month = get_custom_month(months_ago)
    station_jiefalc_count_hour = pd_query(STATION_JIEFALC_COUNT_HOUR_SQL.format(*stats_month), db_name='db_mid')
    hour_work_load = pd.merge(
        WORK_LOAD,
        station_jiefalc_count_hour,
        how='inner',
        right_on='NODE',
        left_on='NODE')
    hour_work_load['HOUR_WORK_LOAD'] = hour_work_load['ALL_NODE_NUMBER'] * hour_work_load['HOUR_COUNT']
    hour_work_load = hour_work_load[['DEPARTMENT_ID', 'HOUR_WORK_LOAD', 'WORK_HOUR']]
    hour_work_load.rename(columns={"DEPARTMENT_ID": "FK_DEPARTMENT_ID"}, inplace=True)

    # 统计每日工作量
    stats_month = get_custom_month(months_ago)
    station_jiefalc_daliy_hour = pd_query(STATION_JIEFALC_COUNT_DAILY_SQL.format(*stats_month), db_name='db_mid')
    daily_work_load = pd.merge(
        WORK_LOAD,
        station_jiefalc_daliy_hour,
        how='inner',
        right_on='NODE',
        left_on='NODE')
    daily_work_load['DAILY_WORK_LOAD'] = daily_work_load['ALL_NODE_NUMBER'] * daily_work_load['DAY_COUNT']
    daily_work_load = daily_work_load[['DEPARTMENT_ID', 'DAILY_WORK_LOAD', 'WORK_DAY']]
    daily_work_load.rename(columns={"DEPARTMENT_ID": "FK_DEPARTMENT_ID"}, inplace=True)
    hour_content = '{0}[基准值：{1:.1f},工作量：{2:.1f},比较值：{3:.1f},实际检查值：{4:.1f}]'
    day_content = '{0}[基准值：{1:.3f},工作量：{2:.0f},比较值：{3:.3f},实际检查值：{4:.0f}]'
    return check_evenness.stats_check_time_evenness_three_wordload(
        CHECK_ITEM_IDS, daily_work_load, DAILY_CHECK_COUNT_SQL,
        hour_work_load, HOUR_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, hour_content=hour_content, day_content=day_content)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """受检地点受检次数/受检地点接发列车工作量 与 站段接发列车检查总次数/站段接发列车总工作量比较"""
    return stats_check_address_evenness(
        CHECK_ITEM_IDS, WORK_LOAD.copy(),
        CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL,
        BANZU_DEPARTMENT_CHECKED_COUNT_SQL,
        CHECK_POINT_CHECKED_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness,
        _stats_check_time_evenness,
        _stats_check_address_evenness,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.3, 0.3]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=9, major=risk_type, main_type=4, child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
