# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql
   Author :       hwj
   date：          2019/8/14上午9:24
   Change Activity: 2019/8/14上午9:24
-------------------------------------------------
"""
# 现场检查检查次数( a.CHECK_WAY = 1)
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY = 1
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 问题质量分累计--问题数均为站段自查(a.TYPE=3)
PROBLEM_CHECK_SCORE_SQL = """SELECT
        a.EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.TYPE = 3
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""


# 较大和重大安全风险问题质量分累计--问题数均为站段自查(a.TYPE=3)
RISK_LEVEL_PROBLEM_SQL = """SELECT
        EXECUTE_DEPARTMENT_ID AS FK_DEPARTMENT_ID, SUM(b.CHECK_SCORE) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
    WHERE
        c.CHECK_WAY NOT BETWEEN 4 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        AND a.TYPE = 3
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.EXECUTE_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计--问题数均为站段自查(a.TYPE=3)
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.COUNT) as COUNT
from
(
SELECT
        distinct(a.pk_id),d.FK_DEPARTMENT_ID, b.CHECK_SCORE AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            LEFT JOIN
        t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY BETWEEN 1 AND 2
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.IS_EXTERNAL = 0
        AND a.RISK_LEVEL <= 2
        AND a.TYPE = 3
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
            LEFT JOIN
        t_statistics_check_info AS d on d.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')

            AND ((d.START_CHECK_TIME <= DATE_ADD(DATE(d.START_CHECK_TIME),
            INTERVAL 86399 SECOND)
            AND d.START_CHECK_TIME >= DATE_ADD(DATE(d.START_CHECK_TIME),
            INTERVAL 22 HOUR))
            OR (d.START_CHECK_TIME >= DATE(d.START_CHECK_TIME)
            AND d.START_CHECK_TIME <= DATE_ADD(DATE(d.START_CHECK_TIME),
            INTERVAL 6 HOUR)))
            AND TIMESTAMPDIFF(MINUTE,
            d.START_CHECK_TIME,
            d.END_CHECK_TIME) >= 30            
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 监控调阅时长--问题数均为站段自查(e.TYPE=3)
MEDIA_COST_TIME_SQL = """
select mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from (
select distinct(a.PK_ID), a.COST_TIME, d.FK_DEPARTMENT_ID from 
t_check_info_and_media as a
left join 
t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID
left join
t_check_problem as e on a.FK_CHECK_INFO_ID = e.FK_CHECK_INFO_ID 
left join
 t_check_info_and_item as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
left join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
where
 b.CHECK_WAY = 3
and e.TYPE=3
and c.FK_CHECK_ITEM_ID in ({2})
and 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
AND 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
)as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLEM_NUMBER_SQL = """SELECT
        c.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS NUMBER
    FROM
        t_check_problem as a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            left join
        t_check_info_and_person as c on a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY = 3
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.IS_EXTERNAL = 0
            AND a.TYPE = 3
            AND a.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY c.FK_DEPARTMENT_ID;
"""

# 监控调阅质量分--问题数均为站段自查(a.TYPE=3)
MEDIA_PROBLME_SCORE_SQL = """select mps.FK_DEPARTMENT_ID,sum(mps.SCORE) as SCORE 
from (
SELECT
        DISTINCT
        (a.PK_ID),d.FK_DEPARTMENT_ID AS FK_DEPARTMENT_ID, b.CHECK_SCORE AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        c.CHECK_WAY = 3
        AND a.TYPE = 3
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                ) as mps
    GROUP BY mps.FK_DEPARTMENT_ID;
"""

# 调阅班组数--问题数均为站段自查(e.TYPE=3)
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct b.FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem as e on a.PK_ID= e.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND e.TYPE = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(TYPE !=9, FK_PARENT_ID, DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department 
WHERE
         TYPE BETWEEN 9 AND 10
        AND IS_DELETE = 0
        AND MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]


# 接发列车数(夜间)
YECHA_JIEFALC_COUNT_SQL = """
SELECT
NODE,
COUNT( 1 ) AS CW_COUNT
FROM
t_td_data 
WHERE
DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) >= DATE_FORMAT( '{0}', '%%Y-%%m-%%d' ) 
AND DATE_FORMAT( DDSJ, '%%Y-%%m-%%d' ) < DATE_FORMAT( '{1}', '%%Y-%%m-%%d' )
AND ((DDSJ <= DATE_ADD(DATE(DDSJ),
            INTERVAL 86399 SECOND)
            AND DDSJ >= DATE_ADD(DATE(DDSJ),
            INTERVAL 22 HOUR))
            OR (DDSJ >= DATE(DDSJ)
            AND DDSJ <= DATE_ADD(DATE(DDSJ),
            INTERVAL 6 HOUR)))
GROUP BY
NODE
"""