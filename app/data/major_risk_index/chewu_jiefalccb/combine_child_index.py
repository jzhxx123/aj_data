# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/8/14上午9:29
   Change Activity: 2019/8/14上午9:29
-------------------------------------------------
"""
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.chewu_jiefalccb import (init_common_data, check_intensity,
                                                       GLV, check_evenness, problem_exposure)
from app.data.major_risk_index.chewu_jiefalccb.common import get_vitual_major_ids
from app.data.major_risk_index.chewu_jiefalccb.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.major_risk_index.common import combine_child_index
from app.data.util import update_major_maintype_weight


@validate_exec_month
def execute(months_ago):
    risk_name = 16
    risk_type = '车务-9'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        check_evenness,
        problem_exposure
    ]:
        func.execute(months_ago, risk_name, risk_type)
    child_index_weight = [0.4, 0.3, 0.3]
    child_index_list = [1, 4, 5]
    ids = get_vitual_major_ids('客运-1')
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight,
        child_index_list=child_index_list,
        vitual_major_ids=ids)

    update_major_maintype_weight(index_type=9, major=risk_type, child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )

    # 清除本模块的共享数据
    GLV.rm_all_values()


if __name__ == '__main__':
    pass
