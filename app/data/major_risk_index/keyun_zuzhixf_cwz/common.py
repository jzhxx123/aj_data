#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-05-21
-------------------------------------------------
Change Activity:2019-05-21 14:49
-------------------------------------------------
"""
from app.data.util import pd_query
import pandas as pd
import datetime
import calendar
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.major_risk_index.keyun_zuzhixf_cwz.common_sql import (
    CW_VISITOR_RECEVIVED_COUNT_SQL, CW_RELAVITE_STATION_SQL, CWZ_STATIONS_NUMBERS_SQL)
from app.data.index.util import (get_custom_month)


def get_vitual_major_ids(zhanduan_type):
    """
    客运组织 - 车务段内容
    :param risk_type:
    :return:
    """
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
    a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
FROM
    t_department AS a
        LEFT JOIN
    t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
WHERE
    a.TYPE = 4 AND a.IS_DELETE = 0
        AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
        AND a.SHORT_NAME != ''
       and a.NAME like '%%{0}%%'
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(zhanduan_type))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def add_workshop_department_info(check_banzu_count_data, department_data):
    """[summary]
    根据班组的检查项目，给它所属的未配置的车间添加进去,
    某班组被检查，它所属车间也算检查一次
    Arguments:
        check_banzu_count_data {[type]} -- [description]
        department_data {[type]} -- [description]
    """
    check_banzu_count_data = pd.merge(
        check_banzu_count_data,
        department_data,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner')
    workshop_dpids = check_banzu_count_data['TYPE4'].values.tolist()
    check_banzu_count_data = check_banzu_count_data[['FK_DEPARTMENT_ID']]
    for dpid in workshop_dpids:
        check_banzu_count_data = check_banzu_count_data.append(
                    [{"FK_DEPARTMENT_ID": dpid}], ignore_index=True)
    check_banzu_count_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    return check_banzu_count_data


def calc_station_cofficients(month_count, days):
    """sumary_line
    计算换算客运办理站数
    Keyword arguments:
    argument -- description
    Return: return_description
    """
    day_count = month_count / days
    station_cofficient = 1
    if day_count >=7000:
        station_cofficient = 5
    elif day_count >= 3000:
        station_cofficient = 3
    elif day_count >= 1000:
        station_cofficient = 2
    return station_cofficient


def calc_work_load(months_ago, department_data):
    """sumary_line
    计算工作量
    Keyword arguments:
    argument -- description
    Return: return_description
    """
    stats_month = get_custom_month(months_ago)
    d_time =  datetime.datetime.strptime(
                str(stats_month[1]), "%Y-%m-%d")
    d_time = d_time.date()
    days_num = calendar.monthrange(d_time.year, d_time.month)[1] #获取一个月有多少天
    start = stats_month[0].replace('-', '')
    end = stats_month[1].replace('-', '')
    work_load = pd.merge(
        pd_query(CW_VISITOR_RECEVIVED_COUNT_SQL.format(
            int(start), int(end)), db_name='db_mid'),
        pd_query(CW_RELAVITE_STATION_SQL),
        on='UNIT',
        how='inner'
    )
    work_load = work_load.groupby(["FK_DEPARTMENT_ID"])["COUNT"].sum().reset_index()
    # work_load['COUNT'] = work_load['COUNT'] / 1800000

    cwz_stations_numbers = pd_query(CWZ_STATIONS_NUMBERS_SQL)
    work_load = pd.merge(
        work_load,
        cwz_stations_numbers,
        on='FK_DEPARTMENT_ID',
        how='outer'
    )
    work_load.fillna({"COUNT": 0, "STATION_COUNT": 0}, inplace=True)
    work_load['STATION_COFFICIENT'] = work_load.apply(
        lambda row: calc_station_cofficients(
            row['COUNT'], days_num), axis=1)
    work_load['COUNT'] = work_load.apply(
        lambda row: round(row['COUNT'] / 1800000 * 0.4 + row['STATION_COUNT'] * 1 / 40 * 0.6, 4), axis=1)
    work_load.drop(['STATION_COUNT'], inplace=True, axis=1)
     # 车务段工作量
    return df_merge_with_dpid(
        work_load,
        department_data
    )
