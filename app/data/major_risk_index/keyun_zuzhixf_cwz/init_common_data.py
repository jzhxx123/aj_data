#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.keyun_zuzhixf_cwz import GLV
from app.data.major_risk_index.keyun_zuzhixf_cwz.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL,
    CW_VISITOR_RECEVIVED_COUNT_SQL, CW_RELAVITE_STATION_SQL, CWZ_STATIONS_NUMBERS_SQL)
from app.data.major_risk_index.keyun_zuzhixf.check_intensity_sql import (
    CW_SPECIAL_CONFIG_SQL
)
from app.data.major_risk_index.keyun_zuzhixf_cwz.common import get_vitual_major_ids, calc_work_load
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.index.util import (get_custom_month)
import pandas as pd
import datetime
import calendar
        

def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    ids = get_vitual_major_ids("车务段")

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(ids))
    CWDUAN_WORK_LOAD = calc_work_load(months_ago, DEPARTMENT_DATA)

    
    cw_special_config_data = pd_query(CW_SPECIAL_CONFIG_SQL)
    # 车务段 - 客运职工数
    KEYUN_STAFF_NUMBER = df_merge_with_dpid(
        cw_special_config_data[['FK_DEPARTMENT_ID','PASSENGER_NUMBER']],
        DEPARTMENT_DATA
    ).rename(columns={"PASSENGER_NUMBER": "COUNT"})

    # 车务段 - 客运职工干部数
    KEYUN_CADRE_NUMBER = df_merge_with_dpid(
        cw_special_config_data[['FK_DEPARTMENT_ID','MANAGE_NUMBER']],
        DEPARTMENT_DATA
    ).rename(columns={"MANAGE_NUMBER": "COUNT"})

    # 车务段 - 正式职工数
    STAFF_NUMBER = pd.merge(
        pd.concat([
            KEYUN_STAFF_NUMBER[['DEPARTMENT_ID', 'COUNT']], 
            KEYUN_CADRE_NUMBER[['DEPARTMENT_ID', 'COUNT']]], ignore_index=True), 
        DEPARTMENT_DATA, 
        on='DEPARTMENT_ID',
        how='inner'
    )
    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "CWDUAN_WORK_LOAD": CWDUAN_WORK_LOAD,
        "STAFF_NUMBER": STAFF_NUMBER,
        "KEYUN_STAFF_NUMBER": KEYUN_STAFF_NUMBER,
        "KEYUN_CADRE_NUMBER": KEYUN_CADRE_NUMBER,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
