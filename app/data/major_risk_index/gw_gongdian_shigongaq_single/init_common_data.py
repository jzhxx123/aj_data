#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gw_gongdian_shigongaq_single import GLV
from app.data.major_risk_index.gw_gongdian_shigongaq_single.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL,
    TOTAL_LABORTIME_SQL, ORDER_COUNT_SQL)
from app.data.major_risk_index.gw_gongdian_shigongaq_single.common import(
    get_vitual_major_ids, calc_total_workload)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month)


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-3")
    
    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(ids))

    # 施工总人时
    SHIGONG_LABORTIME = pd_query(TOTAL_LABORTIME_SQL.format(*stats_month), db_name='db_mid')

    # 施工工作量
    SHIGONG_WORK_LOAD = calc_total_workload(
        SHIGONG_LABORTIME,
        pd_query(ORDER_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "SHIGONG_WORK_LOAD": SHIGONG_WORK_LOAD,
        "SHIGONG_LABORTIME": SHIGONG_LABORTIME,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
