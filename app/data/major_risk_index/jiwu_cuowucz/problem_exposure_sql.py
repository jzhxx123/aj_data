#!/usr/bin/python3
# -*- coding: utf-8 -*-

from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import(
    CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL, HIDDEN_KEY_PROBLEM_SQL
)

# 查出的隐患问题项点(本单位基础问题库中有的)
CHECKED_HIDDEN_PROBLEM_POINT_SQL = """SELECT DISTINCT
    c.FK_DEPARTMENT_ID, b.PK_ID as FK_PROBLEM_BASE_ID
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
        INNER JOIN
    t_check_problem_and_risk AS d ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    b.IS_HIDDEN_KEY_PROBLEM = 1
    AND a.RISK_LEVEL <= 2
        AND b.`STATUS` = 3
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 隐患问题项点
HIDDEN_PROBLEM_POINT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.PK_ID as FK_PROBLEM_BASE_ID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_problem_base_risk AS c ON c.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.STATUS = 3
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND c.FK_RISK_ID in ({0});
"""

# 他查问题
# 筛选项:责任部门、是否路外:否、
# 检查方式:问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、
# 月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:是
OTHER_CHECK_PROBLEM_SQL = """SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            LEFT JOIN
        t_check_problem_and_risk AS f ON f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 1
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND f.FK_RISK_ID IN ({0})
    AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
        < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 安全生产信息问题
SAFETY_PRODUCE_INFO_SQL = """SELECT
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            LEFT JOIN
        t_problem_base_risk AS f on f.FK_PROBLEM_BASE_ID = d.PK_ID
    WHERE
        a.RANK BETWEEN 1 AND 3
        AND f.FK_RISK_ID in ({0})
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 自查问题
# 筛选项:检查部门、是否路外:否、
# 问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:否
SELF_CHECK_PROBLEM_SQL = """SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem_and_responsible_department AS a
            LEFT JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_department AS e ON a.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            LEFT JOIN
        t_check_problem_and_risk AS f ON f.FK_CHECK_PROBLEM_ID = b.PK_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 3
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
    AND f.FK_RISK_ID IN ({0})
    AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(c.END_CHECK_TIME, '%%Y-%%m-%%d')
        < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""
