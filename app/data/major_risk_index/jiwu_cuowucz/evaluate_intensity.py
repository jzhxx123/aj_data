#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import current_app
from app.data.major_risk_index.jiwu_cuowucz import GLV
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_cuowucz.common_sql import (
    CADRE_COUNT_SQL)
from app.data.major_risk_index.jiwu_cuowucz.evaluate_intensity_sql import (
    COMMON_EVALUATE_COUNT_SQL, EVALUATE_COUNT_SQL, EVALUATE_SCORE_ALL_SQL, 
    COMMON_EVALUATE_SCORE_ALL_SQL, LUJU_EVALUATE_COUNT_SQL, 
    COMMON_LUJU_EVALUATE_COUNT_SQL, LUJU_EVALUATE_SCORE_SQL, 
    COMMON_LUJU_EVALUATE_SCORE_SQL)

from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        EVALUATE_SCORE_ALL, \
        LUJU_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, COMMON_EVALUATE_COUNT, \
        COMMON_EVALUATE_SCORE_ALL, COMMON_LUJU_EVALUATE_COUNT, LUJU_EVALUATE_SCORE, \
        COMMON_LUJU_EVALUATE_SCORE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]

    # 站段干部对劳安问题评价条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 站段非干部对劳安问题评价条数
    COMMON_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(COMMON_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 站段干部对劳安问题的评价分数
    EVALUATE_SCORE_ALL = df_merge_with_dpid(
        pd_query(EVALUATE_SCORE_ALL_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 站段非干部对劳安问题的评价分数
    COMMON_EVALUATE_SCORE_ALL = df_merge_with_dpid(
        pd_query(COMMON_EVALUATE_SCORE_ALL_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 路局对劳安问题评价条数(干部)
    LUJU_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(LUJU_EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 路局对劳安问题评价条数(非干部)
    COMMON_LUJU_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(COMMON_LUJU_EVALUATE_COUNT_SQL.format(
            *stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 路局对劳安问题评价分数(干部)
    LUJU_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(LUJU_EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 路局对劳安问题评价分数(非干部)
    COMMON_LUJU_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(COMMON_LUJU_EVALUATE_SCORE_SQL.format(
            *stats_month, risk_ids)),
        DEPARTMENT_DATA)


def _calc_evaluate_count_difference_type(differce_count):
    differce_count = int(differce_count)
    if 7 > differce_count > 0:
        return 40 + differce_count * 10
    elif differce_count == 0:
        return 40
    elif differce_count < 0:
        return 0
    else:
        return 100


# '履职评价条数差（站段与路局）'
def _stats_evaluate_difference_count(months_ago):
    return evaluate_intensity.stats_evaluate_difference_count(
        ACTIVE_EVALUATE_COUNT, COMMON_EVALUATE_COUNT, LUJU_EVALUATE_COUNT,
        COMMON_LUJU_EVALUATE_COUNT, months_ago, RISK_TYPE, _choose_dpid_data,
        calc_func=_calc_evaluate_count_difference_type)


# '履职评价分数差（站段与路局）'
def _stats_evaluate_difference_score(months_ago):
    return evaluate_intensity.stats_evaluate_difference_score(
        EVALUATE_SCORE_ALL, COMMON_EVALUATE_SCORE_ALL, LUJU_EVALUATE_SCORE,
        COMMON_LUJU_EVALUATE_SCORE, months_ago, RISK_TYPE, _choose_dpid_data,
        calc_func=_calc_evaluate_count_difference_type
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_evaluate_difference_count,
        _stats_evaluate_difference_score
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['i', 'j']]
    item_weight = [0.4, 0.6]
    child_index_list = [9, 10]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=4, major=risk_type, main_type=2,
                                 child_index_list=child_index_list, child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')


if __name__ == '__main__':
    pass
