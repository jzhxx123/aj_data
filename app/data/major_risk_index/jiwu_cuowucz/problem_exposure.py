# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app
from app.data.major_risk_index.jiwu_cuowucz import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_exposure

from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_cuowucz.problem_exposure_sql import(
    CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL, HIDDEN_KEY_PROBLEM_SQL,
    CHECKED_HIDDEN_PROBLEM_POINT_SQL, HIDDEN_PROBLEM_POINT_SQL,
    OTHER_CHECK_PROBLEM_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, 
    export_basic_data_one_field_monthly, write_cardinal_number_basic_data,
    calc_child_index_type_sum)
from app.data.util import pd_query, update_major_maintype_weight


HIERARCHY = [3]


def _calc_score_by_formula_type_jiwu(row,
                                     column,
                                     major_column,
                                     detail_type=None):
    """【机务】该比值≥20%，得分为100分；10%≤该比值〈20%，得分90分；
        5%≤该比值〈10%，得分80分；该比值〈5%，得分70分。
    """
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0.0:
        _score = 90 + _ratio*50
    elif _ratio >= -0.15:
        _score = 80 + (_ratio+0.15)*60
    elif _ratio >= -0.3:
        _score = 80 + (_ratio+0.15)*200
    else:
        _score = 50 + (_ratio+0.3)*300
    _score = max(60, _score)
    _score = min(100, _score)
    return _score


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER,\
            CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)

    # 基本工作量【职工总人数】
    data = GLV.get_value('APPLY_WORKSHOP_COUNT')
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    
    # 查出较严重隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
    pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)), 
    DEPARTMENT_DATA)

    # 单位应检查问题项点
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)), 
        DEPARTMENT_DATA)


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    fraction_list = GLV.get_value('stats_total_problem_exposure', (None, None))
    for fraction in fraction_list:
        fraction.detail_type = 1
    fraction_list = (
        (fraction_list[0], fraction_list[1]),
    )
    return problem_exposure.stats_total_problem_exposure_type_jiwu(
        RISK_IDS, CHECK_PROBLEM_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, 
        calc_score_formula=_calc_score_by_formula_type_jiwu,
        title=['总问题数({0})/基本工作量({1})'],
        fraction_list=fraction_list)


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        calc_score_formula=lambda x: 0 if (100 - 10 * x) < 0 else round((100 - 10 * x), 2))


# 事故隐患问题暴露
def _stats_problem_exposure(months_ago):
    customizededuct = {
        3: 1,
        4: 2
    }
    return problem_exposure.stats_problem_exposure_excellent(
        RISK_IDS, ZHANDUAN_DPID_DATA,
        HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        customizededuct, months=5)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """
    单位自查未发现错误操纵问题项点的问题，但上级部门检查发现该类问题项点，一条扣10分。得分=100-扣分。
    :param months_ago:
    :return:
    """
    self_check_problem_sql = SELF_CHECK_PROBLEM_SQL
    other_check_problem_sql = OTHER_CHECK_PROBLEM_SQL
    zhanduan_dpid = ZHANDUAN_DPID_DATA
    choose_dpid_data = _choose_dpid_data
    department_data = DEPARTMENT_DATA
    risk_type = RISK_TYPE

    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(self_check_problem_sql.format(
            RISK_IDS, *calc_month))['PROBLEM_DPID_RISK'].values)
    other_check_problem = set(
        pd_query(other_check_problem_sql.format(
            RISK_IDS, *calc_month))['PROBLEM_DPID_RISK'].values)

    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in zhanduan_dpid.loc[:, 'DEPARTMENT_ID'].values}

    # 保存中间计算过程涉及到的数据
    calc_problems = []
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[2]
        problem_score = 10
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, each_problem[1]])

    export_basic_data_one_field_monthly(
        calc_problems,
        department_data,
        choose_dpid_data(3),
        5,
        5,
        3,
        months_ago,
        lambda x: '上级查出自查未发现问题',
        title='他查问题个数',
        risk_type=risk_type)
    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, department_data)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob,
        2,
        5,
        5,
        months_ago,
        'SCORE',
        'SCORE_e',
        lambda x: 30 if x > 30 else x,
        choose_dpid_data,
        risk_type=risk_type)
    return rst_child_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_hidden_problem_exposure,
        _stats_problem_exposure,
        _stats_other_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'e']]
    item_weight = [0.7, 0.15, 0.15, -1]
    child_index_list = [1, 2, 3, 5]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=4, major=risk_type, main_type=5,
                                 child_index_list=child_index_list, child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
