# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''
from flask import current_app
from app.data.major_risk_index.jiwu_cuowucz import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index, 
                                   format_export_basic_data,
                                   write_export_basic_data_to_mongo,
                                   calc_child_index_type_sum)
from app.data.major_risk_index.jiwu_cuowucz.problem_rectification_sql import (
    REPEAT_PROBLEM_SQL, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, OVERDUE_PROBLEM_NUMBER_SQL,
    CHECK_EVALUATE_SZ_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL,
    WARINING_NOTIFICATION_COUNT_SQL,
    REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL)
from app.data.major_risk_index.jiwu_cuowucz.common_sql import (
    WORK_LOAD_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_query_condition_by_risktype,
                                 get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common import problem_rectification
import pandas as pd
from app.data.major_risk_index.util import (
    export_basic_data_one_field_monthly, df_merge_with_dpid, calc_child_index_type_sum,
    data_complete_by_condition)
SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_check_evaluate_score(row, code_dict):
    score = 0
    for code in code_dict:
        score += int(row[code]) * int(code_dict[code])
    return min(100, score)


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        if int(row['RESP_LEVEL']) == 1:
            _score = int(row['COUNT_A']) * 20
        elif int(row['RESP_LEVEL']) == 2:
            _score = int(row['COUNT_A']) * 15
        else:
            _score = int(row['COUNT_A']) * 10
    return _score


def _calc_rectification_effect_type_d(row):
    """[计算根据级别的预警通知书]
    因错误操纵问题下发警告预警通知书一次扣10分。
    Arguments:
        row {[type]} -- [description]
    """
    _score = 10 * int(row['COUNT_D'])
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA, WARINING_NOTIFICATION_COUNT_DATA,\
    WORKER_COUNT, MAJOR_PROBLEM_POINT_INFO_DATA

    major = get_major_dpid(risk_type)
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    # 部门正式职工人数
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(major))
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(*stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1,1],
        DEPARTMENT_DATA)
    
    WARINING_NOTIFICATION_COUNT_DATA = data_complete_by_condition(
        pd_query(WARINING_NOTIFICATION_COUNT_SQL.format(*stats_month)),
        ZHANDUAN_DPID_DATA,
        ['TYPE', 'RANK'],
        [1,1],
        DEPARTMENT_DATA
    )

    # 专业问题项点信息
    MAJOR_PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major))


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):

    calc_month = get_custom_month(months_ago)
    check_evaluate_sz_number = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(*calc_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 获取每个站段各个code的数量
    # code_list = ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-5"]
    code_dict = {"ZG-1": 35,
                 "ZG-2": 25,
                 "ZG-3": 30,
                 "ZG-4": 20,
                 "ZG-5": 20}
    data = ZHANDUAN_DPID_DATA
    for code in code_dict:
        code_data = check_evaluate_sz_number[check_evaluate_sz_number["CODE"] == code]
        code_data = code_data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
        code_data.rename(columns={"COUNT": code}, inplace=True)
        data = pd.merge(
            data,
            code_data.loc[:, [code, "TYPE3"]],
            how="left",
            left_on="DEPARTMENT_ID",
            right_on="TYPE3")
        data.drop(["TYPE3"], inplace=True, axis=1)
    data["TYPE3"] = data["DEPARTMENT_ID"]
    data.fillna(0, inplace=True)

    data['CONTENT'] = data.apply(
        lambda row: '<br/>'.join([f'本月{col}履职评价发生条数：{int(row[col])}条' for col in code_dict]), axis=1
    )

    data['SCORE'] = data.apply(lambda row: _calc_check_evaluate_score(row, code_dict), axis=1)
    # 中间过程
    data_rst = format_export_basic_data(data, 6, 2, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     6, 2, risk_type=RISK_TYPE)
    # 最终结果
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        NA_value=True)
    return rst_index_score


def _stats_repeatedly_index(months_ago):
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20
    }
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, REPEAT_PROBLEM_SQL, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago,
    #     RISK_TYPE, _choose_dpid_data,
    #     problem_ctl_threshold_dict=problem_ctl_threshold_dict)
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, 
        CHECK_RISK_IDS, _choose_dpid_data, WORKER_COUNT,
        REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


# 整改成效
def _stats_rectification_effect(months_ago):
    """A.因错误操纵问题下发警告预警通知书一次扣10分；
       B.D15类事故主要、全部责任的1个扣20分，重要扣15分，次要的扣10分（含追究责任）。
       得分=100-扣分。
    """
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,
        'D': WARINING_NOTIFICATION_COUNT_DATA
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a,
        'D': _calc_rectification_effect_type_d
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        df_dict,rectification_type_calc_func_dict=rectification_type_calc_func_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index, _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'f']]
    item_weight = [0.2, 0.3, 0.5, -1]
    child_index_list = [1, 2, 3, 6]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=4, major=risk_type, main_type=6,
                                 child_index_list=child_index_list, child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
