#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

import pandas as pd
from flask import current_app
from app.data.major_risk_index.jiwu_cuowucz import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_cuowucz.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL, RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_POINT_CHECKED_COUNT_SQL,
    DAILY_CHECK_COUNT_SQL, RISK_ABOVE_PROBLEM_POINT_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST)
from app.data.major_risk_index.jiwu_cuowucz.common_sql import (
    APPLY_WORKSHOP_COUNT_SQL,)
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index,
    append_major_column_to_df, df_merge_with_dpid, format_export_basic_data,
    summizet_operation_set, write_export_basic_data_to_mongo
)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.jiwu_jianduanlw.common import retrieve_all_dp_configid


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _cal_check_banzu_evenness_score(row, columns, basedata=None):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 保存中间计算过程
    calc_basic_data = {
        '日均班组检查数低于基数20%': 0,
        '日均班组检查数低于基数50%': 0,
        '日均班组检查数低于基数100%': 0,
    }
    score = [100]
    for day in columns:
        # 取某天的某个专业的班组数
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        # 取某天的某个专业的平均检查数
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        if basedata is not None:
            major_avg = float(basedata['AVG_NUMBER'][basedata['DAY'] == day])
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
            calc_basic_data.update({
                '日均班组检查数低于基数100%':
                calc_basic_data.get('日均班组检查数低于基数100%') + 1
            })
        elif ratio <= -0.5:
            daily_deduction = -2
            calc_basic_data.update({
                '日均班组检查数低于基数50%':
                calc_basic_data.get('日均班组检查数低于基数50%') + 1
            })
        elif ratio <= -0.2:
            daily_deduction = -1
            calc_basic_data.update({
                '日均班组检查数低于基数20%':
                calc_basic_data.get('日均班组检查数低于基数20%') + 1
            })
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {v}天' for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, APPLY_WORKSHOP_COUNT,\
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, RISK_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]


    # 基本工作量（运用车间人数)
    APPLY_WORKSHOP_COUNT = GLV.get_value('APPLY_WORKSHOP_COUNT')

    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS)), DEPARTMENT_DATA)
    
    CHECK_BANZU_COUNT_DATA = pd_query(CHECK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = pd.merge(
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[0].format(major)),
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[1].format(*stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA.drop(
        ["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = BANZU_DEPARTMENT_CHECKED_COUNT_DATA.groupby(
        ['DEPARTMENT_ID'])['COUNT'].sum().reset_index()


def _calc_time_evenness_compare_value(months_ago):
    """
    计算时间均衡度自定义基数值 每日检查次数/基本工作量
    :param check_item_ids:
    :param check_banzu_count_sql:
    :param banzu_department_checked_count_sql:
    :param department_data:
    :param ZHANDUAN_DPID_DATA:
    :param months_ago:
    :param RISK_TYPE:
    :param _choose_dpid_data:
    :return:
    """
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    daily_check_count = df_merge_with_dpid(
        pd_query(DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 统计每日检查数
    daily_check_count = daily_check_count.groupby(['DAY'])['COUNT'].sum().reset_index()
    # 统计基本工作量
    work_load = sum(APPLY_WORKSHOP_COUNT['COUNT'])
    # 每日基数
    daily_check_count['AVG_NUMBER'] = daily_check_count['COUNT'] / work_load
    return daily_check_count


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = " + \
                        "查出较大及以上问题项点数({4})/ 基础问题库中较大及以上问题项点数({5})</p>"
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data, customizecontent=customizecontent)


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    basedata = _calc_time_evenness_compare_value(months_ago)
    work_load = APPLY_WORKSHOP_COUNT
    daily_check_count_sql = DAILY_CHECK_COUNT_SQL
    check_item_ids = CHECK_ITEM_IDS
    department_data = DEPARTMENT_DATA
    zhanduan_dpid_data = ZHANDUAN_DPID_DATA
    risk_type = RISK_TYPE
    choose_dpid_data = _choose_dpid_data

    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        if work_load.empty:
            continue
        xdata = pd.DataFrame(work_load, columns=['TYPE3', 'COUNT'])
        xdata = xdata.groupby([f'TYPE{hierarchy}']).size().to_frame(name='COUNT')
        # xdata = xdata.unstack()
        for x in range(1, 32):
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[x for x in range(1, 32)], inplace=True)
        origin_columns = xdata.columns.values
        for col in origin_columns:
            new_column = 'daily_check_banzu_count_{}'.format(col)
            xdata[new_column] = xdata.apply(
                lambda row: row['COUNT'],
                axis=1)
        xdata = xdata.drop(columns=origin_columns)
        xdata = xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                daily_check_count_sql.format(*stats_month, check_item_ids)),
            department_data)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(zhanduan_dpid_data, xdata)
        major_banzu_count = xdata.groupby(['MAJOR']).sum()
        xdata = pd.merge(
            xdata,
            major_banzu_count,
            how='left',
            left_on='MAJOR',
            right_index=True)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(row, columns, basedata)[0],
            axis=1)
        xdata['CONTENT'] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(row, columns, basedata)[1],
            axis=1)
        calc_basic_data_rst = format_export_basic_data(
            xdata.copy(), 4, 2, 3, months_ago, risk_type=risk_type)
        write_export_basic_data_to_mongo(
            calc_basic_data_rst, months_ago, 3, 4, 2, risk_type=risk_type)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=risk_type)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_evenness.stats_check_address_evenness_work_load_ex(
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,
        APPLY_WORKSHOP_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, 
        months_ago, RISK_TYPE, _choose_dpid_data)
    # return check_evenness.stats_check_address_evenness_laoan(
    #     CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL,
    #     BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_POINT_CHECKED_COUNT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
    #     check_item_ids=CHECK_ITEM_IDS, risk_ids=RISK_IDS)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.2, 0.4, 0.4]
    child_index_list = [1, 2, 3]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=4, major=risk_type, main_type=4,
                                 child_index_list=child_index_list, child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
