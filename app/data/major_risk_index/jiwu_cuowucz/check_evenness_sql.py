from app.data.major_risk_index.common.check_evenness_sql import (
    DAILY_CHECK_COUNT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST)

# 基础问题库中较大及以上风险项点问题数
RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.PK_ID
"""

# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT
        PK_ID AS CHECK_POINT_ID, FK_DEPARTMENT_ID
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0 AND HIERARCHY = 2
        and (
        name like '%%调车点%%' or
        name like '%%出入库点%%' or
        name like '%%检查段区%%')
"""

# 重要检查点受检次数
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
            left join
        t_check_info_and_item as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY between 1 and 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID
    FROM
        t_department AS a
            LEFT JOIN
        t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 9
        AND b.main_type = 1
            AND b.SOURCE_ID IN ({0})
            AND a.IS_DELETE = 0;
"""

# 班组受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """SELECT
        c.TYPE5 AS DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            INNER JOIN
        t_check_info_and_item AS d ON a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
            INNER JOIN
        t_department_and_info AS e ON a.FK_DEPARTMENT_ID = e.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.HIERARCHY BETWEEN 5 AND 6
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY  BETWEEN 1 AND 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND e.SOURCE_ID IN ({2})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.TYPE5;
"""
