from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    PROBLEM_CHECK_SCORE_SQL, YECHA_CHECK_SQL, MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, CHECK_COUNT_SQL,
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ALL_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL, 
)
# 分析中心检查力度（问题数）
ANALYSIS_CENTER_PROBLEM_COUNT_SQL = """
SELECT
        MAX(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 AS NUMBER
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk AS e ON e.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        c.CHECK_WAY between 3 and 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.status = 1
        AND e.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.all_name like "%%安全分析中心%%"
    GROUP BY a.PK_ID
"""
#c.CHECK_WAY NOT BETWEEN 4 AND 6
        # AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        # AND a.IS_EXTERNAL = 0

# 分析中心检查力度（质量分）
ANALYSIS_CENTER_PROBLEM_SCORE_SQL = """
SELECT
        MAX(f.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS NUMBER
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS f ON a.FK_CHECK_INFO_ID = f.FK_CHECK_INFO_ID
            INNER JOIN
        t_department as d on f.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk AS e ON e.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        c.CHECK_WAY between 3 and 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.status = 1
        AND e.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.all_name like "%%安全分析中心%%"
    GROUP BY a.pk_id;
"""

ANALYSIS_CENTER_CHECK_INTENSITY_SQLLIST = [ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL]
