#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2019/03/08
'''
from app.utils.decorator import record_func_runtime
from app.data.major_risk_index.jiwu_cuowucz import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification, init_common_data, GLV,
    _calc_cardinal_number)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common.common_sql import (CHEJIAN_DPID_SQL,
                                                         ZHANDUAN_DPID_SQL)
from app.data.util import update_major_maintype_weight, init_monthly_index_map


@validate_exec_month
def execute(months_ago):
    risk_name = 20
    risk_type = '机务-4'
    init_common_data.init_func(months_ago, risk_name, risk_type)
    _calc_cardinal_number.get_cardinal_number(months_ago, risk_name, risk_type)
    # init_monthly_index_map(months_ago, risk_type)
    for func in [
            assess_intensity,
            check_evenness,
            check_intensity,
            problem_exposure,
            problem_rectification,
            evaluate_intensity
    ]:
        _func = record_func_runtime(func.execute)
        _func(months_ago, risk_name, risk_type)
    child_index_weight = [0.3, 0.1, 0.15, 0.1, 0.2, 0.15]
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight)
    update_major_maintype_weight(index_type=4, major=risk_type,
                                 child_index_list=[1, 2, 3, 4, 5, 6], 
                                 child_index_weight=child_index_weight)
    # 删除本模块数据
    GLV.rm_all_values()


if __name__ == '__main__':
    pass
