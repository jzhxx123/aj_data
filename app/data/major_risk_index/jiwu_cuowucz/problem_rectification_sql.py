from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    WARINING_NOTIFICATION_COUNT_SQL,
    REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL)

# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
            INNER JOIN 
        t_check_problem_and_risk AS d on d.FK_CHECK_PROBLEM_ID = c.PK_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.FK_SAFETY_PRODUCE_INFO_ID = c.FK_SAFETY_PRODUCE_INFO_ID
    WHERE
        c.FK_RISK_ID in ({2})
        AND b.CODE = 'D15'
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 反复发生的同一项点问题数
REPEAT_PROBLEM_SQL = """SELECT
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_safety_assess_month_responsible_detail AS d
            ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
            LEFT JOIN 
        t_check_problem_and_risk as e on a.pk_id = e.FK_CHECK_PROBLEM_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND e.fk_risk_id IN ({2})
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_NUMBER_SQL = """SELECT
        MAX(a.RESPONSIBE_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        MAX(a.CODE) as CODE, 
        1 AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.fk_risk_id in ({2})
            GROUP BY a.PK_ID
"""

# 责任安全信息
DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND locate('D15', b.CODE)
"""