# -*- coding: utf-8 -*-
'''
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
'''

import pandas as pd
from flask import current_app

from app.data.index.common import (
    calc_child_index_type_sum, combine_child_index_func, df_merge_with_dpid,
    summizet_child_index, summizet_operation_set)
from app.data.major_risk_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, HAPPEN_PROBLEM_POINT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL)
from app.data.index.util import (get_custom_month, get_year_month,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]


def _calc_rectification_score(problem_number):
    val = 100 - 0.2 * problem_number
    val = 0 if val < 0 else round(val, 2)
    return val


# 问题整改
def _stats_rectification_effect(months_ago):
    year_mon, last_month = get_year_month(months_ago)
    # 超期问题数
    data = df_merge_with_dpid(
        pd_query(
            OVERDUE_PROBLEM_NUMBER_SQL.format(year_mon // 100, year_mon % 100,
                                              CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_rectification_score,
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_index_score


# 履职评价指数
def _stats_check_evaluate(months_ago):
    calc_month = get_custom_month(months_ago)
    data = df_merge_with_dpid(
        pd_query(
            CHECK_EVALUATE_SZ_SCORE_SQL.format(*calc_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        6,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_index_score


def _get_appoint_month_happen_problem(months_ago, hierarchy):
    """获取前第{-months_ago}月的发生问题项点数

    Arguments:
        months_ago {int} -- [description]

    Returns:
        set -- 反复问题
    """
    i_month_data = df_merge_with_dpid(
        pd_query(
            HAPPEN_PROBLEM_POINT_SQL.format(*get_custom_month(months_ago),
                                            CHECK_ITEM_IDS)), DEPARTMENT_DATA)
    if i_month_data.empty is True:
        return set()
    i_month_data = i_month_data.groupby(
        [f'TYPE{hierarchy}', 'PK_ID', 'RISK_LEVEL']).size()
    repeatedly_problem = set()
    for idx in i_month_data.index:
        risk_level = idx[2]
        problem_number = int(i_month_data[idx])
        if risk_level == 1 and problem_number < 3:
            continue
        elif risk_level == 2 and problem_number < 6:
            continue
        elif risk_level == 3 and problem_number < 20:
            continue
        else:
            repeatedly_problem.add(f'{idx[0]}||{idx[1]}||{idx[2]}')
    return repeatedly_problem


def _calc_problem_score(risk_level, i_month):
    """根据风险等级和月份进行扣分

    Arguments:
        risk_level {str} -- 风险等级
        i_month {str} -- 前第{-i_month}月

    Returns:
        float/int -- 得分
    """
    _score = {
        '1': 1,
        '2': 0.5,
        '3': 0.2,
    }
    problem_score = _score.get(risk_level, 0) * (4 + int(i_month))
    return problem_score


def _calc_repeatedly_index(months_ago, hierarchy):
    repeatedly_problem = []
    base_repeatedly_problem = _get_appoint_month_happen_problem(
        months_ago, hierarchy)
    for i_month in range(-1, -4, -1):
        i_month_repeatedly_problem = _get_appoint_month_happen_problem(
            months_ago + i_month, hierarchy)
        # 获取每个月反复的问题项点
        common_problem = base_repeatedly_problem.intersection(
            i_month_repeatedly_problem)
        repeatedly_problem.extend(
            [[x.split('||')[0], x.split('||')[2], i_month]
             for x in common_problem])
    if repeatedly_problem == []:
        return []
    df_prob = pd.DataFrame(
        data=repeatedly_problem, columns=['DEP_DPID', 'RISK_LEVEL', 'I_MONTH'])
    df_prob['SCORE'] = df_prob.apply(
        lambda row: _calc_problem_score(row['RISK_LEVEL'], row['I_MONTH']),
        axis=1)
    # 将站段分组求和
    df_prob = df_prob.groupby(['DEP_DPID'])['SCORE'].sum()
    df_prob = df_prob.to_frame(name='SCORE')
    column = f'SCORE_c_{hierarchy}'
    df_prob[column] = df_prob['SCORE'].apply(
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2))

    # 计算排名、入库
    summizet_operation_set(
        df_prob,
        _choose_dpid_data(hierarchy),
        column,
        hierarchy,
        2,
        6,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    return df_prob[[column]]


# 反复指数
def _stats_repeatedly_index(months_ago):
    """以站段为统计单位同一问题项点是否发生，严重风险的问题3月内重复发生一个扣a分，
    较大风险的（当月累计发生3次以上算重复发生）问题3月内重复发生一项扣a分，
    一般风险的（当月累计发生10次以上算重复发生）问题3月内重复发生一项扣a分。
    低风险问题不计算

    Arguments:
        months_ago {int} -- [description]
    """
    rst_index_score = []
    for hierarchy in HIERARCHY:
        rst_score = _calc_repeatedly_index(months_ago, hierarchy)
        if len(rst_score) != 0:
            rst_index_score.append(rst_score)
    return rst_index_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_effect, _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.1, 0.2, 0.7]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
