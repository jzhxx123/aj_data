# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure
   Author :       hwj
   date：          2019/10/11下午5:01
   Change Activity: 2019/10/11下午5:01
-------------------------------------------------
"""

from flask import current_app
import pandas as pd
from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import BANZU_POINT_SQL
from app.data.major_risk_index.gd_gongdian_shigongjg_single import GLV
from app.data.major_risk_index.gd_gongdian_shigongjg_single.common import get_vitual_major_ids
from app.data.major_risk_index.gd_gongdian_shigongjg_single.common_sql import WORK_LOAD_SQL, EXTERNAL_PERSON_SQL
from app.data.major_risk_index.gongdian_shigongjg.problem_exposure_sql import HIDDEN_PROBLEM_POINT_SQL, \
    CHECKED_HIDDEN_PROBLEM_POINT_SQL, CHECK_PROBLEM_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL, \
    SAFETY_PRODUCE_INFO_SQL, HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_vitual_major_ids(risk_type)
    global RISK_TYPE, CHECK_ITEM_IDS, RISK_IDS
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        HIDDEN_PROBLEM_POINT_DATA, CHECKED_HIDDEN_PROBLEM_POINT_DATA, WORK_LOAD
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    risk_stats_conf = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_stats_conf[0]
    RISK_IDS = risk_stats_conf[1]
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])

    # 统计工作量【职工总人数】
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)),
        DEPARTMENT_DATA)
    # 站段外聘人员
    zhanduan_staff = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, zhanduan_staff], axis=0, sort=False)
    WORK_LOAD = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(RISK_IDS)),
        DEPARTMENT_DATA
    )

    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA
    )

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    return problem_exposure.stats_total_problem_exposure(
        RISK_IDS, CHECK_PROBLEM_SQL, WORK_LOAD, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    title = '本月问题个数: {1}个'
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA,
        HIDDEN_PROBLEM_POINT_DATA, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, title=title)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    return problem_exposure.stats_problem_exposure(
        RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分
    """
    return problem_exposure.stats_banzu_problem_exposure(
        RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return problem_exposure.stats_other_problem_exposure(
        RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_hidden_problem_exposure,
        _stats_problem_exposure,
        _stats_banzu_problem_exposure,
        _stats_other_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.4, 0.2, 0.1, 0.2, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=15, major=risk_type, main_type=5, child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
