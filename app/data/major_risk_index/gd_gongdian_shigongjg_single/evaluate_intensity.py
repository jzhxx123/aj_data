# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     evaluate_intensity
   Author :       hwj
   date：          2019/10/11下午5:01
   Change Activity: 2019/10/11下午5:01
-------------------------------------------------
"""

from dateutil.relativedelta import relativedelta
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.evaluate_intensity_sql import ACCUMULATIVE_EVALUATE_SCORE_SQL, \
    LUJU_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_SQL, PERSON_ID_CARD_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL
from app.data.major_risk_index.gd_gongdian_shigongjg_single import GLV
from app.data.major_risk_index.gongdian_shigongjg.evaluate_intensity_sql import EVALUATE_COUNT_SQL, \
    DUAN_CADRE_COUNT_SQL, EVALUATE_PEOPLE_COUNT_SQL, ZHANDUAN_EVALUATE_PEOPLE_COUNT_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight
from app.utils.common_func import get_today


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_custom_month_3(months_ago):
    """获取近3个月的时间范围

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        tuple(str) -- 前-N个月的三个月的时间统计范围
    """
    update_day = current_app.config.get('UPDATE_DAY')
    delta = -3
    today = get_today()
    if today.day >= update_day:
        delta = 0
    start_month = today + relativedelta(months=(months_ago + delta))
    end_month = today + relativedelta(months=months_ago)
    start_date = '{}-{:0>2}-{}'.format(start_month.year, start_month.month,
                                       update_day)
    end_date = '{}-{:0>2}-{}'.format(end_month.year, end_month.month,
                                     update_day)
    return start_date, end_date


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid('工务-1')
    global RISK_TYPE
    RISK_TYPE = risk_type
    global TOTAL_EVALUATE_COUNT, EVALUATE_PEOPLE_COUNT, ZHANDUAN_EVALUATE_PEOPLE_COUNT, \
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DUAN_CADRE_COUNT, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, RISK_IDS
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    stats_month_3 = _get_custom_month_3(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]
    RISK_IDS = risk_ids

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, risk_ids)), DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month, risk_ids)), DEPARTMENT_DATA)

    # 被评价人次数量
    EVALUATE_PEOPLE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_PEOPLE_COUNT_SQL.format(*stats_month_3, risk_ids)), DEPARTMENT_DATA)

    # 站段下评价人次总数
    ZHANDUAN_EVALUATE_PEOPLE_COUNT = df_merge_with_dpid(
        pd_query(ZHANDUAN_EVALUATE_PEOPLE_COUNT_SQL.format(*stats_month_3)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type):
    if row[major_column] == 0:
        return 100
    _ratio = (row[column] - row[major_column]) / row[major_column]
    _score = 100 * _ratio + 70
    _score = max(0, _score)
    _score = min(100, _score)
    return _score


# 评价得分
def _stats_score_per_dp(months_ago):
    """三个月被评价人次数量/近三个月该段所有评价人次总数"""
    return evaluate_intensity.stats_score_per_dp(
        EVALUATE_PEOPLE_COUNT,
        ZHANDUAN_EVALUATE_PEOPLE_COUNT,
        months_ago,
        _calc_score_by_formula,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
    )


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    """（主动）因施工配合监管风险段机关干部评价记分条数/评价记分总条数*100%，与专业平均数比较换算得分"""
    return evaluate_intensity.stats_duan_ratio(
        DUAN_CADRE_COUNT,
        TOTAL_EVALUATE_COUNT,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data)


# 评价集中度
def _stats_concentartion_ratio_of_evaluation(months_ago):
    """施工配合监管风险站段评价造成单人处置方式升级的1人+N分"""
    return evaluate_intensity.stats_concentartion_ratio_of_evaluation_two(
        ACCUMULATIVE_EVALUATE_SCORE_SQL,
        LUJU_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
        PERSON_ID_CARD_SQL,
        DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        RISK_IDS)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_score_per_dp,
        _stats_duan_ratio,
        _stats_concentartion_ratio_of_evaluation
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['e', 'g', 'k']]
    item_weight = [0.2, 0.3, 0.5]

    update_major_maintype_weight(index_type=15, major=risk_type, main_type=2,
                                 child_index_list=[5, 7, 11],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')


if __name__ == '__main__':
    pass
