#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from app.data.index.common import df_merge_with_dpid

# 如果数据有缺失，按站段补全数据
def data_complete_by_condition(raw_df, filled_data, columns, default, merged_data):
    """[补全数据，填充默认值]
    1.数据全部缺失补全站段
    2.数据部分缺失，找出缺失站段补全
    3.数据完整不处理
    Arguments:
        raw_df {[type]} -- [description]
        filled_data {[type]} -- [description]
        default {[type]} -- [description]
    """
    if raw_df.empty is True:
        raw_df = filled_data[['DEPARTMENT_ID']].copy()
        raw_df.loc[:,'COUNT'] = 0
        for idx, column in enumerate(columns):
            raw_df.loc[:, column] = default[idx]
        raw_df.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
        raw_df = df_merge_with_dpid(raw_df, merged_data)
    else:
        raw_df = df_merge_with_dpid(raw_df, merged_data)
        raw_type3_set = set(raw_df['TYPE3'].values.tolist())
        real_type3_set = set(filled_data['DEPARTMENT_ID'].values.tolist())
        subset = real_type3_set - raw_type3_set
        if subset:
            init_data = [[data] for data in subset]
            sub_df = pd.DataFrame(data=init_data, columns=['FK_DEPARTMENT_ID'])
            sub_df.loc[:,'COUNT'] = 0
            sub_df = df_merge_with_dpid(sub_df, merged_data)
            for idx, column in enumerate(columns):
                sub_df.loc[:,column] = default[idx]
            raw_df = raw_df.append(sub_df, ignore_index=True)
        else:
            pass
    return raw_df