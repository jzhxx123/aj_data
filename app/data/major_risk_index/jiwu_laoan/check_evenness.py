#!/usr/bin/python3
# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.jiwu_laoan.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL, DAILY_CHECK_BANZU_COUNT_H_SQL,
    HOUR_CHECK_COUNT_SQL,CHECK_INFO_AND_ITEM_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL,
    BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST, GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL
    )
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_laoan.common_sql import (
    WORK_LOAD_SQL,
    EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, WORK_LOAD,\
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    # 统计工作量【职工总人数】
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS)), DEPARTMENT_DATA)
    
    CHECK_BANZU_COUNT_DATA = pd_query(CHECK_BANZU_COUNT_SQL.format(RISK_IDS))

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = pd.merge(
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[0].format(major)),
        pd_query(CHECK_INFO_AND_ITEM_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA.drop(
        ["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = BANZU_DEPARTMENT_CHECKED_COUNT_DATA.groupby(
        ['DEPARTMENT_ID'])['COUNT'].sum().reset_index()


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 检查时间均衡度-日期均衡度 + 时段均衡度
def _stats_check_time_evenness(months_ago):
    return check_evenness.stats_check_time_evenness_three(
        CHECK_ITEM_IDS, DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
        DAILY_CHECK_BANZU_COUNT_H_SQL, HOUR_CHECK_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_evenness.stats_check_address_evenness_work_load_ex(
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,
        WORK_LOAD, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, 
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    update_major_maintype_weight(index_type=5, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
