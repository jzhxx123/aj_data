#!/usr/bin/python3
# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app
from app.data.major_risk_index.jiwu_laoan import GLV
from app.data.major_risk_index.util import (
    calc_extra_child_score_groupby_major_third,
    write_cardinal_number_basic_data)
from app.data.index.common import (
    combine_and_format_basic_data_to_mongo, combine_child_index_func,
    df_merge_with_dpid, summizet_child_index, summizet_operation_set)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.jiwu_laoan.common_sql import (
    EXTERNAL_PERSON_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.jiwu_laoan.assess_intensity_sql import (
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL, LEVEL_AWARD_RETURN_SQL)
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, STAFF_NUMBER

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    risk_ids = diaoche[1]
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, risk_ids)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(LEVEL_AWARD_RETURN_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 换算单位考核问题数
def _stats_check_problem_assess_radio_type_one(months_ago):
    fraction = GLV.get_value('stats_check_problem_assess_radio', (None,))[0]
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        STAFF_NUMBER,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 换算单位考核金额
def _stats_assess_money_per_person(months_ago):
    fraction = GLV.get_value('stats_assess_money_per_person', (None,))[0]
    return assess_intensity.stats_assess_money_per_person_type_one_major(
        ASSESS_RESPONSIBLE_MONEY,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


def _calc_score_by_formula(row, column, major_column, detail_type, major_ratio_dict={}):
    _score = 60
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100 if detail_type != 3 else 100 * (1 - _ratio)
    elif _ratio >= -0.1:
        _score = 90
    else:
        _score = 100 if detail_type == 3 else 100 * (1 + _ratio)
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


# 返奖率
def _stats_award_return_ratio(months_ago):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    if AWARD_RETURN_MONEY.empty:
        return None
    fraction_list = GLV.get_value('stats_award_return_ratio', 
        (None, None, None))
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = AWARD_RETURN_MONEY[AWARD_RETURN_MONEY['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby(['DEPARTMENT_ID']).size()
        prob_number = idata.groupby(['DEPARTMENT_ID']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        if fraction_list[i]:
            write_cardinal_number_basic_data(idata, ZHANDUAN_DPID_DATA,
            fraction_list[i], fraction_list[i].risk_type, 3, 3,fraction_list[i].months_ago,
            columns=['award','prob'])
        rst_child_data = calc_extra_child_score_groupby_major_third(
            idata.copy(),
            _choose_dpid_data(3),
            'ratio',
            _calc_score_by_formula,
            weight=child_weight[i],
            detail_type=3,
            numerator='award', denominator='prob',
            fraction=fraction_list[i])
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 问题个数（{2}）'
            .format(title[i], row['award'], row['prob']),
            axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data,
        _choose_dpid_data(3),
        months_ago,
        3,
        3,
        3,
        risk_type=RISK_TYPE)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        column,
        3,
        2,
        3,
        3,
        months_ago,
        risk_type=RISK_TYPE)
    return [df_rst]


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio_type_one, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.45, 0.15]
    update_major_maintype_weight(index_type=5, major=risk_type, main_type=3,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
