from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, REPEAT_PROBLEM_SQL, OVERDUE_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import(
    CHECK_INFO_AND_ITEM_COUNT_SQL)
# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE_STANDARD*d.GRADATION_RATIO)*{3} AS SCORE,
        COUNT(1) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person_gradation_ratio AS d
            ON a.FK_PERSON_GRADATION_RATIO_ID = d.PK_ID 
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 库内问题（实际中年问题）复查数
IMPORTANT_PROBLEM_RECHECK_COUNT_SQL = """SELECT
    b.FK_DEPARTMENT_ID, SUM(a.PROBLEM_REVIEW_NUMBER) AS COUNT
FROM
    t_check_info AS a
        LEFT JOIN
    t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN
    t_check_info_and_item as c on a.pk_id = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID in ({2})
GROUP BY b.FK_DEPARTMENT_ID
"""

# 以下涉及隐患库问题都(不包含已销号)
# 隐患库隐患数量
PERIL_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
    GROUP BY FK_DEPARTMENT_ID;
"""

# 延期隐患
PERIL_OVERDUE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
            AND COMPLETE_DATE < CURDATE()
    GROUP BY FK_DEPARTMENT_ID;
"""

# 长期整治
PERIL_PERIOD_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib
    WHERE
        `STATUS` < 5
            AND COMPLETE_DATE >= CURDATE()
            AND IS_PERIOD_RECTIFY = 1
    GROUP BY FK_DEPARTMENT_ID;
"""

# 检查出的的隐患
CHECKED_PERIL_ID_SQL = """SELECT
        c.FK_DEPARTMENT_ID, a.FK_SAFETY_PERIL_LIB_ID AS PID
    FROM
        t_check_info_and_safety_peril_lib AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.ID_CARD = c.ID_CARD
            LEFT JOIN
        t_check_info_and_item as d on b.pk_id = d.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.FK_CHECK_ITEM_ID in (876,880,879,878)
"""

# 隐患库里的隐患
PERIL_ID_SQL = """SELECT
        FK_DEPARTMENT_ID, PK_ID AS PID
    FROM
        t_safety_peril_lib
    WHERE
        HIERARCHY = 3 AND `STATUS` < 5;
"""

# 长期整治没录入的
PERIL_RECTIFY_NO_ENTRY_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_peril_lib_and_person AS a
            INNER JOIN
        t_safety_peril_lib AS b ON a.FK_SAFETY_PERIL_LIB_ID = b.PK_ID
            INNER JOIN
        t_safety_peril_lib_and_department AS c
            ON a.FK_SAFETY_PERIL_AND_DEPARTMENT_ID = c.PK_ID
    WHERE
        b.IS_PERIOD_RECTIFY
            AND b.HIERARCHY = 3
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.RECTIFY_DATE, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 检查隐患的人员
CHECKED_PERIL_ID_AND_PERSON_SQL = """
SELECT
        c.FK_DEPARTMENT_ID, a.FK_SAFETY_PERIL_LIB_ID AS PID, c.FK_CHECK_INFO_ID
    FROM
        t_check_info_and_safety_peril_lib AS a
            INNER JOIN
        t_check_info_and_person AS c ON a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
"""