# -*- coding: utf-8 -*-

from flask import current_app
import pandas as pd
from app.data.major_risk_index.jiwu_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.jiwu_laoan.common_sql import (
    WORK_LOAD_SQL,
    EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.jiwu_laoan.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, NOITEM_YECHA_CHECK_SQL,
    NOITEM_CHECK_COUNT_SQL, NOITEM_MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    BANZU_POINT_SQL, REAL_CHECK_BANZU_SQL,
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST
)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, PROBLEM_COUNT, \
        YIBAN_RISK_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    global RISK_IDS
    risk_ids = diaoche[1]
    RISK_IDS = risk_ids
    # 统计工作量【职工总人数】
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_CHECK_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(NOITEM_YECHA_CHECK_SQL.format(*stats_month)),
        DEPARTMENT_DATA)

    # 一般及以上问题数
    YIBAN_RISK_COUNT = df_merge_with_dpid(
        pd_query(ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format(*stats_month, risk_ids)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


# 人均检查频次
def _stats_check_per_person(months_ago):
    fraction = GLV.get_value('stats_check_per_person', (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 问题平均质量分
def _stats_score_per_check_problem(months_ago):
    fraction = GLV.get_value('stats_score_per_check_problem', (None,))[0]
    return check_intensity.stats_score_per_check_problem(
        PROBLEM_SCORE,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    fraction = GLV.get_value('stats_score_per_person', (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    stats_month = get_custom_month(months_ago)
    media_problem_number_sql = MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)
    media_problem_score_sql = MEDIA_PROBLME_SCORE_SQL.format(*stats_month, RISK_IDS)
    fraction_list = GLV.get_value('stats_media_intensity', (None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1], 
        fraction_list[2], None
    )
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=NOITEM_MEDIA_COST_TIME_SQL,
        media_problem_number_sql=media_problem_number_sql,
        media_problem_score_sql=media_problem_score_sql,
        monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        child_weight=[0.35, 0.35, 0.20, 0.10],
        fraction_list=fraction_list)


# 夜查率
def _stats_yecha_ratio(months_ago):
    fraction = GLV.get_value('stats_yecha_ratio', (None,))[0]
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        CHECK_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 一般及以上风险问题占比
def _stats_yiban_risk_ratio_type_one(months_ago):
    """
    :param months_ago:
    :return:
    """
    fraction = GLV.get_value('stats_yiban_risk_ratio', (None,))[0]
    return check_intensity.stats_yiban_risk_ratio_type_one(
        YIBAN_RISK_COUNT,
        PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        calc_score_by_formula=_calc_score_by_formula,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    return check_intensity.stats_check_address_ratio(
        REAL_CHECK_BANZU_SQL,
        BANZU_POINT_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        RISK_NAME,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_yecha_ratio,
        _stats_score_per_person, _stats_check_address_ratio,
        _stats_media_intensity, _stats_score_per_check_problem,
        _stats_yiban_risk_ratio_type_one
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'k', 'e', 'i', 'j', 'g', 'h']]
    item_weight = [0.26, 0.05, 0.24, 0.06, 0.20, 0.05, 0.14]
    update_major_maintype_weight(index_type=5, major=risk_type, main_type=1,
                                 child_index_list=[2, 11, 5, 9, 10, 7, 8],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
