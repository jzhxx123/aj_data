from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import (
    EX_CHECKED_HIDDEN_PROBLEM_POINT_SQL, EX_HIDDEN_PROBLEM_POINT_SQL, CHECK_PROBLEM_SQL,
    EXPOSURE_PROBLEM_DEPARTMENT_SQL,
    SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL, SAFETY_PRODUCE_INFO_SQL,
    HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL)

