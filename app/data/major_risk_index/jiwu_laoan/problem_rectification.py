# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app
from app.data.major_risk_index.jiwu_laoan import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_laoan.common_sql import (
    WORK_LOAD_SQL, EXTERNAL_PERSON_SQL)
from app.data.major_risk_index.jiwu_laoan.problem_rectification_sql import(
    CHECK_EVALUATE_SZ_SCORE_SQL,
    IMPORTANT_PROBLEM_RECHECK_COUNT_SQL, PERIL_COUNT_SQL, PERIL_OVERDUE_COUNT_SQL,
    PERIL_PERIOD_COUNT_SQL, CHECKED_PERIL_ID_AND_PERSON_SQL, PERIL_ID_SQL, PERIL_RECTIFY_NO_ENTRY_SQL,
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, REPEAT_PROBLEM_SQL, OVERDUE_PROBLEM_NUMBER_SQL,
    CHECK_INFO_AND_ITEM_COUNT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid,
    summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.jiwu_laoan.common import(
    data_complete_by_condition
)

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, WORKER_COUNT,\
        DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA, WARNING_DELAY_DATA, \
        PERIL_COUNT_DATA, PERIL_OVERDUE_COUNT_DATA, PERIL_PERIOD_COUNT_DATA,\
        CHECKED_PERIL_ID_DATA, PERIL_ID_DATA, PERIL_RECTIFY_NO_ENTRY_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month, major))
    # 单位总人数
    data = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    WORKER_COUNT = data
    WORK_LOAD = data.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')

    PERIL_COUNT_DATA = data_complete_by_condition(
        pd_query(PERIL_COUNT_SQL), ZHANDUAN_DPID_DATA, [], [], DEPARTMENT_DATA)
    PERIL_OVERDUE_COUNT_DATA = data_complete_by_condition(
        pd_query(PERIL_OVERDUE_COUNT_SQL), ZHANDUAN_DPID_DATA, [], [], DEPARTMENT_DATA)
    PERIL_PERIOD_COUNT_DATA = data_complete_by_condition(
        pd_query(PERIL_PERIOD_COUNT_SQL), ZHANDUAN_DPID_DATA, [], [], DEPARTMENT_DATA)
    # 检查出隐患整治
    CHECKED_PERIL_ID_DATA = pd.merge(
        pd_query(CHECKED_PERIL_ID_AND_PERSON_SQL),
        pd_query(CHECK_INFO_AND_ITEM_COUNT_SQL.format(
            *stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )
    CHECKED_PERIL_ID_DATA.drop(
        ['FK_CHECK_INFO_ID', 'PK_ID'], inplace=True, axis=1)
    PERIL_ID_DATA = pd_query(PERIL_ID_SQL)
    PERIL_RECTIFY_NO_ENTRY_DATA = data_complete_by_condition(
        pd_query(PERIL_RECTIFY_NO_ENTRY_SQL.format(*stats_month)),
        ZHANDUAN_DPID_DATA, [], [], DEPARTMENT_DATA)
    PERIL_RECTIFY_NO_ENTRY_DATA.rename(columns={
        'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)

    # 防控责任详细信息filled_data, columns, default, merged_data
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )

    WARNING_DELAY_DATA = data_complete_by_condition(
        pd_query(WARNING_DELAY_SQL.format(*stats_month)),
        ZHANDUAN_DPID_DATA,
        [], [],
        DEPARTMENT_DATA)


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        _score = int(row['COUNT_A']) * (25 - 5 * int(row['RESP_LEVEL']))
    elif int(row['MAIN_TYPE']) == 2:
        _score = int(row['COUNT_A']) * (8 - 2 * int(row['RESP_LEVEL']))
    else:
        _score = int(row['COUNT_A']) * (4 - 1 * int(row['RESP_LEVEL']))
    return _score

# 问题控制


def _stats_repeatedly_index(months_ago):
    return problem_rectification.stats_repeatedly_index_type_jiwu(
        CHECK_RISK_IDS, REPEAT_PROBLEM_SQL, DEPARTMENT_DATA, months_ago,
        RISK_TYPE, _choose_dpid_data)


# 整改时效
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS,
        OVERDUE_PROBLEM_NUMBER_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        deduction_coefficient=0.2)


# 整改履职
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS,
        CHECK_EVALUATE_SZ_SCORE_SQL,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        deduction_coefficient=8,
        calc_func=lambda x: min(round(40 + x, 2), 100))


# 整改复查
def _stats_rectification_review(months_ago):
    return problem_rectification.stats_rectification_review(
        CHECK_ITEM_IDS, WORKER_COUNT,
        IMPORTANT_PROBLEM_RECHECK_COUNT_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 隐患整治
def _stats_peril_renovation(months_ago):
    return problem_rectification.stats_peril_renovation_excellent(
        PERIL_COUNT_DATA, PERIL_OVERDUE_COUNT_DATA, PERIL_PERIOD_COUNT_DATA,
        CHECKED_PERIL_ID_DATA, PERIL_ID_DATA, PERIL_RECTIFY_NO_ENTRY_DATA,
        WORK_LOAD, DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data, CHECK_RISK_IDS)


# 整改成效
def _stats_recification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,
        'B': WARNING_DELAY_DATA,
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, df_dict,
        rectification_type_calc_func_dict=rectification_type_calc_func_dict
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_repeatedly_index, _stats_peril_renovation,
        _stats_recification_effect,
        _stats_rectification_overdue,
        _stats_rectification_review, _stats_check_evaluate
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f']]
    item_weight = [0.10, 0.20, 0.30, 0.15, 0.25, -1]
    update_major_maintype_weight(index_type=5, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3, 4, 5, 6],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
