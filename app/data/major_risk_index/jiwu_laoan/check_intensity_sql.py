from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, NOITEM_YECHA_CHECK_SQL,
    NOITEM_CHECK_COUNT_SQL, NOITEM_MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    ZUOYE_PROBLEM_CHECK_SCORE_SQL, ZUOYE_CHECK_PROBLEM_SQL,
    ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL
)

# 实际检查班组数
REAL_CHECK_BANZU_SQL = """SELECT DISTINCT
        a.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 AND 2
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.IS_DELETE = 0;
"""

# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct( if(d.TYPE !=9, d.FK_PARENT_ID,b.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE !=''
"""

# 施工或作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct (if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
         b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.MEDIA_TYPE !=''
"""

# 监控调阅覆盖（调阅班组数/作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]

# 较大及以上风险问题数(量化人员及干部)
# PROBLEM_DIVIDE_IDS in (4) 其它归类问题为劳安
RISK_LEVEL_PROBLEM_SQL = """
SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.RISK_LEVEL <=3
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""
