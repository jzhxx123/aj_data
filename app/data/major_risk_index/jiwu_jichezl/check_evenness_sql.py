#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql
Description:
Author:    
date:         2019-06-05
-------------------------------------------------
Change Activity:2019-06-05 16:13
-------------------------------------------------
"""
from app.data.major_risk_index.common.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL,
    )
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST
)

# 每日检查数(现场检查)
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY NOT BETWEEN 4 AND 6
        AND a.CHECK_WAY BETWEEN 1 AND 2
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """
SELECT
        max(a.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS COUNT
    FROM
        t_problem_base as a
        inner join 
        t_problem_base_risk as b on a.pk_id = b.fk_problem_base_id
    WHERE
        a.RISK_LEVEL <= 3 
        AND a.IS_DELETE = 0
        AND a.STATUS = 3
        AND a.TYPE = 3
        AND b.FK_RISK_ID IN ({0})
    GROUP BY a.pk_id;
"""