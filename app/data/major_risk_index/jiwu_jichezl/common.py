#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   common
Description:
Author:    
date:         2019-06-05
-------------------------------------------------
Change Activity:2019-06-05 11:05
-------------------------------------------------
"""
from app.data.util import pd_query


def retrieve_all_dp_configid(top_ids):
    """
    获取所有部门配置id
    检修及整备车间(包含车间班组分类配置-机务-检整车间-检修车间、整备车间、大部件检修车间(parent id = 6)，
    以及车间班组分类配置-机务-解体组装车间(parent id = 281)
    """
    # 查询所属的子check_item的PK_ID
    load_child_check_item_sql = "SELECT PK_ID FROM t_department_classify_config WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret
