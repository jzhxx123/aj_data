# -*- coding: utf-8 -*-
'''
    机车故障扣分
'''
from flask import current_app
from app.data.major_risk_index.jiwu_jichezl import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.common.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.index.util import (get_query_condition_by_risktype,
                                 get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.util import (
    df_merge_with_dpid, data_complete_by_condition,
    format_export_basic_data, write_export_basic_data_to_mongo,
    calc_child_index_type_sum)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common import problem_rectification
import pandas as pd

SCORE = []
HIERARCHY = [3]

# 机车故障定责率
LOCOMOTIVE_MALFUNCTION_LIABILITY_RATIO = 0.7

# 机车故障统计数
LOCOMOTIVE_MALFUNCTION_COUNT_SQL = """
SELECT
        a.FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
        AND b.MAIN_TYPE = 2
        AND b.STATUS NOT BETWEEN 7 AND 8
"""

# 机车故障定责统计数
LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_SQL = """
SELECT
        a.FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
        AND b.MAIN_TYPE = 2
        AND b.STATUS NOT BETWEEN 2 AND 3
        AND b.STATUS NOT BETWEEN 7 AND 8
"""

# 每段每月机车故障指标件数
ALLOWED_LOCOMOTIVE_MALFUNCTION_DICT = {
    # 成都机务段
    '19B8C3534E095665E0539106C00A58FD': 5,
    # 贵阳机务段
    '19B8C3534E035665E0539106C00A58FD': 5,
    # 重庆机务段
    '19B8C3534E285665E0539106C00A58FD': 3,
    # 西昌机务段
    '19B9D8D920DD589FE0539106C00A1189': 2
}


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        LOCOMOTIVE_MALFUNCTION_COUNT_DATA,\
        LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]

    # 机车故障数
    LOCOMOTIVE_MALFUNCTION_COUNT_DATA = data_complete_by_condition(
        pd_query(LOCOMOTIVE_MALFUNCTION_COUNT_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        [],
        [],
        DEPARTMENT_DATA)

    # 机车故障定责数
    LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_DATA = data_complete_by_condition(
        pd_query(LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        [],
        [],
        DEPARTMENT_DATA)


def _stats_locomotive_malfunction_deduct(months_ago):
    """[机车故障扣分计算]

    Arguments:
        months_ago {[type]} -- [description]
    """
    # 聚合各站段的机车故障数
    locomotive_malfunction_count_data = LOCOMOTIVE_MALFUNCTION_COUNT_DATA.groupby(
        ['TYPE3'])['COUNT'].sum().reset_index()
    # 这里删掉全局变量
    # del LOCOMOTIVE_MALFUNCTION_COUNT_DATA

    # 聚合各站段的机车故障定责数
    liability_locomotive_malfunction_count_data = LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_DATA.groupby(
        ['TYPE3'])['COUNT'].sum().reset_index()
    # del LIABILITY_LOCOMOTIVE_MALFUNCTION_COUNT_DATA
    liability_locomotive_malfunction_count_data.rename(
        columns={
            "COUNT": 'COUNT_LIABILITY'
        }, inplace=True)

    # 合并两个df
    locomotive_malfunction_count_data = pd.merge(
        locomotive_malfunction_count_data,
        liability_locomotive_malfunction_count_data,
        right_on='TYPE3',
        left_on='TYPE3',
        how='inner'
    )
    # del liability_locomotive_malfunction_count_data
    locomotive_malfunction_count_data['CONTENT'] = '暂无数据'
    locomotive_malfunction_count_data['SCORE'] = 0
    for idx, row in locomotive_malfunction_count_data.iterrows():
        _score = 0
        allowed_count = ALLOWED_LOCOMOTIVE_MALFUNCTION_DICT.get(row['TYPE3'])
        _ratio = 1.0
        if int(row['COUNT']) != 0:
            _ratio = int(row['COUNT_LIABILITY']) / int(row['COUNT'])
        _ratio = round(_ratio, 1)
        if int(row['COUNT']) > allowed_count:
            # 每超出一件扣0.3分
            _score = 0.3 * (int(row['COUNT']) - allowed_count)
        if _ratio >= LOCOMOTIVE_MALFUNCTION_LIABILITY_RATIO:
            # 是否大于规定定责率
            _score += 0.2 * int(row['COUNT_LIABILITY'])
        else:
            _score += 0.5 * int(row['COUNT_LIABILITY'])
        locomotive_malfunction_count_data.loc[idx, 'CONTENT'] = f"机车故障定责件数({row['COUNT_LIABILITY']}) " + \
                                                               f"/ 发生机车故障件数({row['COUNT']})"
        locomotive_malfunction_count_data.loc[idx, 'SCORE'] = _score
    locomotive_malfunction_count_data.rename(
        columns={'TYPE3': 'FK_DEPARTMENT_ID'}, inplace=True)
    # 保留 fk_department_id,score 两列
    locomotive_malfunction_count_data = locomotive_malfunction_count_data[[
        'FK_DEPARTMENT_ID', 'SCORE', 'CONTENT']]
    # 导出中间过程数据
    rst = pd.merge(
        _choose_dpid_data(3),
        locomotive_malfunction_count_data,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_rst = format_export_basic_data(rst, 7, 2,
                                        3, months_ago, RISK_TYPE)
    write_export_basic_data_to_mongo(data_rst, months_ago, 3,
                                     7, 2, RISK_TYPE)
    locomotive_malfunction_count_data = df_merge_with_dpid(
        locomotive_malfunction_count_data, DEPARTMENT_DATA)

    rst_child_score = calc_child_index_type_sum(
        locomotive_malfunction_count_data,
        2,
        7,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: x,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        NA_value=True)
    return rst_child_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_locomotive_malfunction_deduct
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['b']]
    item_weight = [-1]
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=7,
                                 child_index_list=[2], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        7,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── locomotive_malfunction_deduction index has been figured out!')


if __name__ == '__main__':
    pass
