#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql
Description:
Author:    
date:         2019-06-05
-------------------------------------------------
Change Activity:2019-06-05 15:02
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import(
    PROBLEM_CHECK_SCORE_SQL, MEDIA_COST_TIME_SQL, CHECK_COUNT_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, ABOVE_RISK_PROBLEM_NUMBER_SQL,
    ALL_PROBLEM_NUMBER_SQL,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL
)

# 实际检查班组数(现场检查检整车间（即所有检修、整备及解体组装车间）班组数)
REAL_CHECK_BANZU_SQL = """SELECT DISTINCT
        a.FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND b.CHECK_WAY BETWEEN 1 AND 2 
        AND (
            c.all_name like '%%解体组装车间%%'
            or c.all_name like '%%检修车间%%'
            or c.all_name like '%%整备车间%%'
            or c.all_name like '%%大部件检修车间%%'
        )
        AND c.HIERARCHY = 5
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY BETWEEN 1 and 2
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0
            AND (
            a.all_name like '%%解体组装车间%%'
            or a.all_name like '%%检修车间%%'
            or a.all_name like '%%整备车间%%'
            or a.all_name like '%%大部件检修车间%%'
        )
"""
