#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_exposure_sql
Description:
Author:    
date:         2019-06-05
-------------------------------------------------
Change Activity:2019-06-05 16:53
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_exposure_sql import (
    ANALYSIS_CHECK_PROBLEM_SQL, CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL,
    EX_CHECKED_HIDDEN_PROBLEM_POINT_SQL, EX_HIDDEN_PROBLEM_POINT_SQL,
    HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL)
