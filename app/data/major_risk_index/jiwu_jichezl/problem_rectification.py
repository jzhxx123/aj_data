# -*- coding: utf-8 -*-

from flask import current_app
from app.data.major_risk_index.jiwu_jichezl import GLV
from app.data.index.common import (combine_child_index_func,
                                   summizet_child_index)
from app.data.major_risk_index.jiwu_jichezl.common_sql import (
    WORK_LOAD_SQL)
from app.data.major_risk_index.jiwu_jichezl.problem_rectification_sql import (
    OVERDUE_PROBLEM_NUMBER_SQL,
    CHECK_EVALUATE_SZ_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL,
    REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_SQL)
from app.data.index.util import (get_query_condition_by_risktype,
                                 get_custom_month)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.util import df_merge_with_dpid, data_complete_by_condition
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common import problem_rectification
SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA,\
        CHECK_EVALUATE_SZ_NUMBER_DATA, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,\
        MAJOR_PROBLEM_POINT_INFO_DATA, WORKER_COUNT

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]

    # 部门正式职工人数
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(major))

    CHECK_EVALUATE_SZ_NUMBER_DATA = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )

    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA)
    
    # 专业问题项点信息
    MAJOR_PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major))


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1 or int(row['MAIN_TYPE'] == 2):
        if int(row['RESP_LEVEL']) == 1:
            _score = int(row['COUNT_A']) * 20
        elif int(row['RESP_LEVEL']) == 2:
            _score = int(row['COUNT_A']) * 15
        else:
            _score = int(row['COUNT_A']) * 10
    else:
        _score = 0
    return _score


# 问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, deduction_coefficient=2)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    code_dict = {"ZG-1": 30,
                 "ZG-2": 25,
                 "ZG-3": 30,
                 "ZG-4": 20,
                 "ZG-5": 15
                 }
    return problem_rectification.stats_check_evaluate_by_codetype(
        CHECK_EVALUATE_SZ_NUMBER_DATA, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        calc_func=lambda x: min(100, x),
        code_dict=code_dict)


def _stats_repeatedly_index(months_ago):
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20
    }
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago,
    #     RISK_TYPE, _choose_dpid_data,
    #     problem_ctl_threshold_dict=problem_ctl_threshold_dict)
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, 
        CHECK_RISK_IDS, _choose_dpid_data, WORKER_COUNT,
        REPEATE_HAPPEN_PROBLEM_SQL, MAJOR_PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)

def _stats_rectification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data,
        df_dict, rectification_type_calc_func_dict=rectification_type_calc_func_dict)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index, _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'f']]
    item_weight = [0.2, 0.3, 0.5, -1]
    update_major_maintype_weight(index_type=6, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3, 6], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')

if __name__ == '__main__':
    pass
