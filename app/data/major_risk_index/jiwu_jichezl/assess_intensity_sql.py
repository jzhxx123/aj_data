#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   assess_intensity_sql
Description:
Author:    
date:         2019-06-05
-------------------------------------------------
Change Activity:2019-06-05 18:07
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import(
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL,
    ACTUAL_RETURN_MONTY_PROBLEM_SQl, RETURN_MONTY_PROBLEM_SQL)