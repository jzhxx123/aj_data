#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.jiwu_jichezl import GLV
from app.data.major_risk_index.jiwu_jichezl.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL, WORK_LOAD_SQL,
    WORKSHOP_INFO_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month)
import pandas as pd
from app.data.major_risk_index.jiwu_jichezl.common import retrieve_all_dp_configid


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # todo 之后都需要将这些部门配置信息，做成配置文件类型
    JIWU_APPLY_WORKSHOP_PARENTID = '8, 29, 30, 281'
    apply_workshop_ids = retrieve_all_dp_configid(JIWU_APPLY_WORKSHOP_PARENTID)

    # 运用车间信息
    apply_workshop_info = pd_query(WORKSHOP_INFO_SQL.format(apply_workshop_ids))

    # 所有运用车间以及一下班组信息
    apply_workshop_info = pd.merge(
        apply_workshop_info,
        DEPARTMENT_DATA,
        left_on='FK_DEPARTMENT_ID',
        right_on='TYPE4',
        how='inner'
    )
    apply_workshop_info.drop(['FK_DEPARTMENT_ID'], inplace=True, axis=1)

    # 职工总人数
    work_load = pd_query(WORK_LOAD_SQL.format(major))
    # 基本工作量（运用车间人数)
    WORKSHOP_COUNT = pd.merge(
        apply_workshop_info,
        work_load, 
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='DEPARTMENT_ID'
    )
    WORKSHOP_COUNT.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "CHECK_WORKSHOP_COUNT": WORKSHOP_COUNT,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
