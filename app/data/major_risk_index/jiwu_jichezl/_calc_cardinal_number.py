# -*- coding: utf-8 -*-
"""
机务机车质量指数，计算专业基数
基数选择：选择专业内连续3个月无责任事故、故障的单位、月份（3个月）的均值指数
（即将符合条件的所有单位“合成”一个单位采取相同计算公式得到的结果）作为专业基数参考。
（多次比较得出基数）。若找不出相应比较单位，找出选择3个月故障率（无责任事故）
（每个月）最低的单位均数上浮20%作为专业基数。以后调整为去年、前年、3年前当月良好单位的均值作为专业基数参考。
"""
from flask import current_app
from app import mongo
from app.data.index.util import (
    get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype)

from app.data.major_risk_index.jiwu_jichezl import GLV
import pandas as pd
from app.data.util import (
    pd_query)
from app.data.major_risk_index.common.cardinal_number_common import (
    calc_cardinal_number)
from app.data.major_risk_index.common.common_sql import (
    BASE_UNIT_INFO_SQL
)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_jichezl.assess_intensity_sql import(
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL, ACTUAL_RETURN_MONTY_PROBLEM_SQl, 
    RETURN_MONTY_PROBLEM_SQL
)
from app.data.major_risk_index.jiwu_jichezl.common_sql import (
    WORK_LOAD_SQL)
from app.data.major_risk_index.jiwu_jichezl.check_intensity_sql import (
    CHECK_COUNT_SQL,
    RISK_LEVEL_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    ALL_PROBLEM_NUMBER_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, ABOVE_RISK_PROBLEM_NUMBER_SQL,
    ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL,
    )
from app.data.major_risk_index.common.const import (
    CHECK_COUNT_INFO, IndexDivider,
    CommonCalcDataType, PROBLEM_SCORE_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    MEDIA_COST_TIME_INFO,
    MEDIA_PROBLME_SCORE_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    ASSESS_RESPONSIBLE_INFO,
    ALL_PROBLEM_NUMBER_INFO, ASSESS_PROBLEM_INFO,
    ANALYSIS_CENTER_PROBLEM_COUNT_INFO,
    ANALYSIS_CENTER_PROBLEM_SCORE_INFO,
    AWARD_RETURN_MONEY_PROBLEM_INFO, REAL_AWARD_RETURN_MONEY_PROBLEM_INFO,
    WORKER_LOAD_INFO)


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, IDS, CALC_MONTH,\
        STAFF_NUMBER, CHECK_ITEM_IDS, RISK_IDS,\
        WORK_LOAD_DATA, DIAOCHE_POSITION
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]
    START = stats_months_list[0][1]
    END = stats_months_list[-1][0]
    DIAOCHE_POSITION = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])
    CALC_MONTH = END, START
    # 单位职工人数
    IDS = get_major_dpid(risk_type)
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(IDS))
    # 工作量
    WORK_LOAD_DATA = GLV.get_value('CHECK_WORKSHOP_COUNT').copy()
    WORK_LOAD_DATA = WORK_LOAD_DATA[['DEPARTMENT_ID','COUNT']]
    WORK_LOAD_DATA.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)


# ------------------------获取比值型相应基数------------------------ #


def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, IDS)
    zhanduan_dpid_data = GLV.get_value('ZHANDUAN_DPID_DATA')
    department_data = GLV.get_value('DEPARTMENT_DATA')
    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 公共部分

    # 工作量
    WORK_LOAD_COUNT = CommonCalcDataType(*WORKER_LOAD_INFO)
    WORK_LOAD_COUNT.value = [WORK_LOAD_DATA]
    WORK_LOAD_COUNT.func_version = 'single_df'
    WORK_LOAD_COUNT.description = '运用车间人数'


    # 检查力度指数
    
    # 检查次数
    CHECK_COUNT = CommonCalcDataType(*CHECK_COUNT_INFO)
    CHECK_COUNT.version = 'v2'
    CHECK_COUNT.description = '现场检查总数（关联项目)'
    CHECK_COUNT.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 问题质量分
    PROBLEM_CHECK_SCORE = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    PROBLEM_CHECK_SCORE.value = [
        PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]


    # 较大问题质量分
    JIAODA_RISK_SCORE = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    JIAODA_RISK_SCORE.value = [
        RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    XC_JIAODA_RISK_SCORE = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    XC_JIAODA_RISK_SCORE.value = [
        XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅时长
    MEDIA_COST_TIME = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    MEDIA_COST_TIME.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    MEDIA_PROBLEM_NUMBER = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    MEDIA_PROBLEM_NUMBER.value = [
        MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅质量分
    MEDIA_PROBLME_SCORE = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    MEDIA_PROBLME_SCORE.value = [
        MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]


    # 分析中心检查发现问题数
    ANALYSIS_CENTER_PROBLEM_COUNT = CommonCalcDataType(*ANALYSIS_CENTER_PROBLEM_COUNT_INFO)
    ANALYSIS_CENTER_PROBLEM_COUNT.value = [
        ANALYSIS_CENTER_PROBLEM_COUNT_SQL.format('{0}', '{1}', RISK_IDS)]

    # 分析中心检查发现质量分（关联风险）
    ANALYSIS_CENTER_PROBLEM_SCORE = CommonCalcDataType(*ANALYSIS_CENTER_PROBLEM_SCORE_INFO)
    ANALYSIS_CENTER_PROBLEM_SCORE.value = [
        ANALYSIS_CENTER_PROBLEM_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核力度指数
    # 考核问题数
    ASSESS_PROBLEM_COUNT = CommonCalcDataType(*ASSESS_PROBLEM_INFO)
    ASSESS_PROBLEM_COUNT.value = [
        KAOHE_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核总金额
    ASSESS_RESPONSIBLE = CommonCalcDataType(*ASSESS_RESPONSIBLE_INFO)
    ASSESS_RESPONSIBLE.version = 'v2'
    ASSESS_RESPONSIBLE.description = '月度考核总金额(关联风险)'
    ASSESS_RESPONSIBLE.value = [
        ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 达到返奖时限错误操纵问题问题个数
    AWARD_RETURN_MONEY_PROBLEM = CommonCalcDataType(
        *AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_PROBLEM.version = 'v4'
    AWARD_RETURN_MONEY_PROBLEM.description = '达到返奖时限错误操纵问题问题个数(关联风险)'
    AWARD_RETURN_MONEY_PROBLEM.value = [RETURN_MONTY_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]
    # 实际返奖错误操纵问题个数
    AWARD_RETURN_MONEY_ACTUAL = CommonCalcDataType(
        *REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    AWARD_RETURN_MONEY_ACTUAL.version = 'v4'
    AWARD_RETURN_MONEY_ACTUAL.description = '实际返奖错误操纵问题个数(关联风险)'
    AWARD_RETURN_MONEY_ACTUAL.value = [ACTUAL_RETURN_MONTY_PROBLEM_SQl.format('{0}', '{1}', RISK_IDS)]

    # 问题暴露度

    # 较大及以上问题数
    ABOVE_RISK_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ABOVE_RISK_PROBLEM_NUMBER.version = 'v3'
    ABOVE_RISK_PROBLEM_NUMBER.description = '较大及以上问题数'
    ABOVE_RISK_PROBLEM_NUMBER.value = [
        ABOVE_RISK_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]


    # 总问题数
    ALL_PROBLEM_NUMBER = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    ALL_PROBLEM_NUMBER.value = [
        ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]
    

    # 参与基数计算的sql字典
    CHILD_INDEX_SQL_DICT = {
        # 换算单位检查频次
        '1-2': (IndexDetails(CHECK_COUNT, WORK_LOAD_COUNT),),

        # 均质量分
        '1-5': (IndexDetails(PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),),

        # 较大风险问题质量均分
        "1-6": (IndexDetails(JIAODA_RISK_SCORE, WORK_LOAD_COUNT), 
                IndexDetails(XC_JIAODA_RISK_SCORE, WORK_LOAD_COUNT)),


        # 监控调阅力度
        '1-10': (IndexDetails(MEDIA_COST_TIME, WORK_LOAD_COUNT), 
                IndexDetails(MEDIA_PROBLEM_NUMBER, WORK_LOAD_COUNT),
                IndexDetails(MEDIA_PROBLME_SCORE, WORK_LOAD_COUNT),
                IndexDetails(ANALYSIS_CENTER_PROBLEM_COUNT, WORK_LOAD_COUNT),
                IndexDetails(ANALYSIS_CENTER_PROBLEM_SCORE, WORK_LOAD_COUNT),),
        # 考核力度指数
        # 换算单位考核问题数
        '3-1': (IndexDetails(ASSESS_PROBLEM_COUNT, WORK_LOAD_COUNT),),

        # 换算单位考核金额
        '3-2': (IndexDetails(ASSESS_RESPONSIBLE, WORK_LOAD_COUNT),),

        # 返奖率
        "3-3": (IndexDetails(AWARD_RETURN_MONEY_ACTUAL, AWARD_RETURN_MONEY_PROBLEM),),

        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
                IndexDetails(ALL_PROBLEM_NUMBER, WORK_LOAD_COUNT),
                IndexDetails(PROBLEM_CHECK_SCORE, WORK_LOAD_COUNT),
                IndexDetails(ABOVE_RISK_PROBLEM_NUMBER, WORK_LOAD_COUNT),
                IndexDetails(JIAODA_RISK_SCORE, WORK_LOAD_COUNT),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         zhanduan_dpid_data,
                         department_data,
                         CHILD_INDEX_SQL_DICT,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', CHILD_INDEX_SQL_DICT['1-2'])
    GLV.set_value('stats_score_per_person', CHILD_INDEX_SQL_DICT['1-5'])
    GLV.set_value('stats_risk_score_per_person', CHILD_INDEX_SQL_DICT['1-6'])
    GLV.set_value('stats_media_intensity', CHILD_INDEX_SQL_DICT['1-10'])
    GLV.set_value('stats_check_problem_assess_radio',
                  CHILD_INDEX_SQL_DICT['3-1'])
    GLV.set_value('stats_assess_money_per_person', CHILD_INDEX_SQL_DICT['3-2'])
    GLV.set_value('stats_award_return_ratio', CHILD_INDEX_SQL_DICT['3-3'])
    GLV.set_value('stats_total_problem_exposure', CHILD_INDEX_SQL_DICT['5-1'])
