#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure_sql
   Author :       hwj
   date：          2020/1/2下午3:32
   Change Activity: 2020/1/2下午3:32
-------------------------------------------------
"""
# 检查问题
NORISK_CHECK_PROBLEM_SQL = """SELECT
        MAX(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        MAX(b.LEVEL) as LEVEL,
        MAX(a.RISK_LEVEL) as RISK_LEVEL, 
        MAX(b.CHECK_SCORE) as CHECK_SCORE,
        1 AS COUNT 
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        group by a.PK_ID
"""


# 所有被查出的问题项点
# 问题项点,检查人的部门
CHECK_PROBLEM_BASE_COUNT_SQL = """
SELECT DISTINCT
        a.FK_PROBLEM_BASE_ID,
        c.TYPE3 as CHECK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        and a.FK_CHECK_ITEM_ID IN ({2})
"""

# 本专业所有站段问题项点
# type=3 站段级
PROBLEM_BASE_COUNT_SQL = """
SELECT DISTINCT
    a.PK_ID AS FK_PROBLEM_BASE_ID,
    b.TYPE3 AS FK_DEPARTMENT_ID
    FROM
    t_problem_base as a
        INNER JOIN
    t_department as b on b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
WHERE
    b.TYPE2 ='{0}'		
    and a.IS_DELETE=0
    and b.IS_DELETE=0
    and a.TYPE=3
    and a.FK_CHECK_ITEM_ID IN ({1})
"""