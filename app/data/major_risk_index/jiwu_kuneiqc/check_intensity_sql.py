#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity_sql
   Author :       hwj
   date：          2019/12/28下午4:15
   Change Activity: 2019/12/28下午4:15
-------------------------------------------------
"""

from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL, NORISK_PROBLEM_CHECK_SCORE_SQL,
    YECHA_CHECK_SQL)


# 监控调阅检查时间
MEDIA_COST_TIME_SQL = """
select d.FK_DEPARTMENT_ID, sum(a.COST_TIME) as COUNT 
from
t_check_info_and_media as a
inner join 
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_item as c on b.PK_ID = c.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf on a.FK_CHECK_INFO_ID = qf.PK_ID
inner join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数(关联项目)
NORISK_MEDIA_PROBLEM_NUMBER_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 as COUNT
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.FK_CHECK_ITEM_ID in ({2})
            group by a.pk_id
"""

# 监控调阅质量分(关联项目)
NORISK_MEDIA_PROBLME_SCORE_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS COUNT
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID
"""