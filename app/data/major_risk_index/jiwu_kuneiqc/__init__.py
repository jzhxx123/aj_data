#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/12/28上午8:39
   Change Activity: 2019/12/28上午8:39
-------------------------------------------------
"""
from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import combine_child_index
