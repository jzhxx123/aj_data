#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness_sql
   Author :       hwj
   date：          2019/12/28下午5:06
   Change Activity: 2019/12/28下午5:06
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL, NO_RISK_ABOVE_PROBLEM_POINT_COUNT_SQL)
from app.data.major_risk_index.common.check_evenness_sql import DAILY_CHECK_COUNT_SQL

# 检查时间
HOUR_CHECK_COUNT_SQL = """SELECT
        DISTINCT
        b.PK_ID,
        b.FK_DEPARTMENT_ID,
        a.START_CHECK_TIME,
        a.END_CHECK_TIME,
        1 AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_check_info_and_item as c on a.pk_id = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND c.FK_CHECK_ITEM_ID in ({2})
"""

# 整备场,检修库地点数
POINT_COUNT_SQL = """
SELECT
    a.FK_DEPARTMENT_ID,
    a.PK_ID AS POINT_ID,
    a.ALL_NAME,
    b.CHECK_ITEM_IDS
FROM
    `t_check_point` AS a
    INNER JOIN t_check_address_score_config AS b ON b.FK_CHECK_POINT_ID = a.PK_ID 
WHERE
    a.TYPE = 1 
    AND a.IS_DELETE = 0 
    AND ( PARENT_ID = 13244 OR PARENT_ID = 13242 ) 
    AND b.TYPE=2
"""

# 地点受检次数
POINT_CHECK_COUNT_SQL = """
SELECT 
    b.FK_CHECK_POINT_ID as POINT_ID,
    COUNT(DISTINCT a.PK_ID) as COUNT 
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item as d on a.PK_ID = d.FK_CHECK_INFO_ID
WHERE
    b.TYPE = 2
        AND a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102 , 103)
        AND d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY b.FK_CHECK_POINT_ID
"""

PREPARE_WORKSHOP_SQL = """
SELECT 
    DEPARTMENT_ID,
    ALL_NAME
FROM t_department  as a
WHERE 
 TYPE = 8
 AND ALL_NAME like '%%整备车间%%' 
 AND IS_DELETE = 0
 and TYPE2= '{0}'
"""
