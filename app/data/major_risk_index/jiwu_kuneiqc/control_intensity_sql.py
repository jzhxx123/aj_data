#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     control_intensity_sql
   Author :       hwj
   date：          2020/1/7上午10:48
   Change Activity: 2020/1/7上午10:48
-------------------------------------------------
"""

CHECK_PROBLEM_AND_POINT_SQL = """
    SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    e.ID_CARD,
    b.LEVEL,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    c.TYPE,
    c.FK_CHECK_POINT_ID,
    f.FK_DEPARTMENT_ID AS TYPE3
     FROM 
    t_check_info as a 
        inner join
    t_check_problem as b on a.PK_ID = b.FK_CHECK_INFO_ID
        inner join
    t_check_info_and_address as c on c.FK_CHECK_INFO_ID = a.PK_ID
        inner join
    t_person as e on e.ID_CARD = b.CHECK_PERSON_ID_CARD
        inner join
    t_check_point as f on c.FK_CHECK_POINT_ID = f.PK_ID
    where 
    b.FK_CHECK_ITEM_ID IN ({2})
    AND 
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    AND c.type = 2
"""


# 检查部门及检查出的问题(问题归属到车间)
CHECK_PROBLEM_AND_DEPART_SQL = """
    SELECT
    DISTINCT  
    a.PK_ID AS FK_CHECK_INFO_ID,
    e.ID_CARD,
    b.LEVEL,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    c.TYPE,
    f.TYPE4 as CHECK_DEPARTMENT_ID,
    f.TYPE3
     FROM 
    t_check_info as a 
        inner join
    t_check_problem as b on a.PK_ID = b.FK_CHECK_INFO_ID
        inner join
    t_check_info_and_address as c on c.FK_CHECK_INFO_ID = a.PK_ID
        inner join
    t_check_problem_and_risk as d on b.PK_ID = d.FK_CHECK_PROBLEM_ID
        inner join
    t_person as e on e.ID_CARD = b.CHECK_PERSON_ID_CARD
        inner join
    t_department as f on e.FK_DEPARTMENT_ID = f.DEPARTMENT_ID
    where 
    b.FK_CHECK_ITEM_ID IN ({2})
    AND 
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    AND c.type = 1
"""

# 检查点
CHECK_POINT_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID, a.PK_ID as FK_CHECK_POINT_ID,
    a.PARENT_ID, a.ALL_NAME, b.TYPE3
FROM
    t_check_point AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
        a.IS_DELETE = 0
        AND b.IS_DELETE = 0
        AND b.TYPE2 = '{0}'
        AND ( a.PARENT_ID = 13244 OR a.PARENT_ID = 13242 ) 
"""

# 检修车间、解体组装车间、整备车间
# todo (FK_DEPARTMENT_CLASSIFY_CONFIG_ID西昌没配置, 目前使用like)
WORKSHOP_INFO_SQL = """
SELECT 
    DEPARTMENT_ID as CHECK_DEPARTMENT_ID,
    TYPE3,TYPE4,TYPE5,ALL_NAME
FROM t_department  as a
WHERE 
 TYPE = 8
 AND (
    ALL_NAME like '%%整备车间%%' 
    or ALL_NAME like '%%检修车间%%' 
    or ALL_NAME like '%%解体组装车间%%')
 AND IS_DELETE = 0
 and TYPE2= '{0}'
"""
