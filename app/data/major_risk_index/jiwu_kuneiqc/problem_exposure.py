#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure
   Author :       hwj
   date：          2020/1/2下午3:30
   Change Activity: 2020/1/2下午3:30
-------------------------------------------------
"""
from flask import current_app
import pandas as pd
from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.jiwu_kuneiqc.common import (
    get_problem_exposure_basic_data, get_middle_problem_score_basic_data, get_middle_problem_other_check)
from app.data.major_risk_index.util import (
    df_merge_with_dpid, combine_child_index_func,
    summizet_child_index, append_major_column_to_df, format_export_basic_data, write_export_basic_data_to_mongo,
    summizet_operation_set)
from app.data.major_risk_index.jiwu_kuneiqc.problem_exposure_sql import (
    NORISK_CHECK_PROBLEM_SQL, CHECK_PROBLEM_BASE_COUNT_SQL,
    PROBLEM_BASE_COUNT_SQL)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 50
        _score = 0 if _score < 0 else _score
    return _score


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        CHECK_PROBLEM, TOW_COUNT, PROBLEM_SCORE, MIDDLE_PROBLEM_COUNT, \
        MIDDLE_PROBLEM_SCORE, HIGH_PROBLEM_COUNT, HIGH_PROBLEM_SCORE, \
        CHECK_PROBLEM_BASE_COUNT, PROBLEM_BASE_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    # 牵车次数
    TOW_COUNT = GLV.get_value('TOW_COUNT')
    # 所有问题数
    check_problem = pd_query(NORISK_CHECK_PROBLEM_SQL.format(
        *stats_month, CHECK_ITEM_IDS))
    basic_data = get_problem_exposure_basic_data(check_problem, DEPARTMENT_DATA)
    CHECK_PROBLEM = basic_data.get('CHECK_PROBLEM')
    PROBLEM_SCORE = basic_data.get('PROBLEM_SCORE')
    MIDDLE_PROBLEM_COUNT = basic_data.get('MIDDLE_PROBLEM_COUNT')
    MIDDLE_PROBLEM_SCORE = basic_data.get('MIDDLE_PROBLEM_SCORE')
    HIGH_PROBLEM_COUNT = basic_data.get('HIGH_PROBLEM_COUNT')
    HIGH_PROBLEM_SCORE = basic_data.get('HIGH_PROBLEM_SCORE')

    # 所有检查出的基础问题数
    CHECK_PROBLEM_BASE_COUNT = pd_query(CHECK_PROBLEM_BASE_COUNT_SQL.format(
        *stats_month, CHECK_ITEM_IDS))

    # 所有基础问题数
    PROBLEM_BASE_COUNT = pd_query(PROBLEM_BASE_COUNT_SQL.format(
        major, CHECK_ITEM_IDS))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题数
def _stats_problem_count_ratio(months_ago):
    """牵车作业风险问题数/基本工作量。与专业基数比较，按照下表计算得分"""
    return problem_exposure.stats_problem_count_ratio(
        CHECK_PROBLEM,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 质量分占比
def _stats_problem_score_ratio(months_ago):
    """牵车作业风险问题质量分/基本工作量。与专业基数比较"""
    return problem_exposure.stats_problem_score_ratio(
        PROBLEM_SCORE,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 中高质量问题数占比
def _stats_middle_problem_count_ratio(months_ago):
    """
    中高（高:A、B、E1、E2、F1、F2，中：C、E3、F3）质量牵车作业风险问题数/基本工作量,
    与专业基数比较
    """
    return problem_exposure.stats_middle_problem_count_ratio(
        MIDDLE_PROBLEM_COUNT,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 中高质量问题质量分占比
def _stats_middle_problem_score_ratio(months_ago):
    """
    A:中高（高:A、B、E1、E2、F1、F2，中：C、E3、F3）质量牵车作业风险问题质量分/基本工作量,
    与专业基数比较
    B: 本专业内,其它单位查出的牵车问题项点(本单位基础问题库中有的),
       本单位未查出的按10分/项扣。分值=100-扣分
    """
    rst_child_score = []
    child_func = [
        get_middle_problem_score_basic_data,
        get_middle_problem_other_check
    ]
    child_data_list = [MIDDLE_PROBLEM_SCORE, CHECK_PROBLEM_BASE_COUNT]
    worl_load = [TOW_COUNT, PROBLEM_BASE_COUNT]
    child_weight = [0.5, 0.5]
    title = [
        '<p>中高质量问题质量分占比：{0:.1f}</p><p>专业平均得分：{6:.2f}</p><p>'
        + '排名: {1:.0f}</p><p>专业基数: {2:.4f}</p><p>'
        + ' 中高质量问题质量分占比({3:.4f}) = '
        + '中高质量问题质量分({4:.1f})/ 工作量({5:.1f})*100%</p>',
        '其他单位查出本单位未查出问题项点: {0:.0f}个',
    ]
    # 保存中间过程计算数据
    calc_basic_data = []
    score = []
    for idx, func in enumerate(child_func):
        rst_data, rst_basic_data = func(
            child_data_list[idx], worl_load[idx], _choose_dpid_data, idx,
            title=title,
            fraction=None,
            calc_score_by_formula=_calc_score_by_formula)
        calc_basic_data.append(rst_basic_data)
        score.append(rst_data * child_weight[idx])

    # 保存中间计算过程
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 17, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 17, risk_type=RISK_TYPE)

    # 合并计算子指数
    data = pd.concat(score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = 'SCORE_q_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        column,
        3,
        2,
        5,
        17,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 高质量问题数占比
def _stats_high_problem_count_ratio(months_ago):
    """
    高质量（高:A、B、E1、E2、F1、F2）牵车作业风险问题数/基本工作量,
    与专业基数比较
    """
    return problem_exposure.stats_high_problem_count_ratio(
        CHECK_PROBLEM,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 高质量问题质量分占比
def _stats_high_problem_score_ratio(months_ago):
    """
    高质量（高:A、B、E1、E2、F1、F2）牵车作业风险问题质量分/基本工作量,
    与专业基数比较
    """
    return problem_exposure.stats_high_problem_score_ratio(
        HIGH_PROBLEM_SCORE,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_count_ratio,
        _stats_problem_score_ratio,
        _stats_middle_problem_count_ratio,
        _stats_middle_problem_score_ratio,
        _stats_high_problem_count_ratio,
        _stats_high_problem_score_ratio,
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['n', 'o', 'p', 'q', 'r', 's']]
    item_weight = [0.1, 0.1, 0.15, 0.15, 0.25, 0.25]
    update_major_maintype_weight(
        index_type=7,
        major=risk_type,
        main_type=5,
        child_index_list=['n', 'o', 'p', 'q', 'r', 's'],
        child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
