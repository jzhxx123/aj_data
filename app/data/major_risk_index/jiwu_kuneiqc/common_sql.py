#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/12/28上午8:41
   Change Activity: 2019/12/28上午8:41
-------------------------------------------------
"""

# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != "";
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}';
"""


# 整备车间及其下所有部门
PREPARE_DPID_SQL = """
SELECT
    a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 29 AS CONFIG_ID
FROM
    t_department as a
    INNER JOIN
    t_department as b on a.TYPE4 = b.DEPARTMENT_ID
WHERE
    b.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = 29
    AND a.IS_DELETE = 0 
    AND b.IS_DELETE = 0 
    AND a.TYPE2 = '{0}'
"""


# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 牵车次数 todo 暂无数据
TOW_COUNT_SQL = """
SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS COUNT 
FROM
    `t_department_other_management` 
WHERE
    FK_TYPE_ID = {0}  
"""