#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/12/28下午3:56
   Change Activity: 2019/12/28下午3:56
-------------------------------------------------
"""
from app.data.index.util import get_query_condition_by_risktype
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.jiwu_kuneiqc.common_sql import (
    ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, TOW_COUNT_SQL, PREPARE_DPID_SQL)
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)

    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(major))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(major))
    department_data = pd_query(DEPARTMENT_SQL.format(major))
    prepare_data = pd_query(PREPARE_DPID_SQL.format(major))
    # 牵车次数id
    fk_type_id = 2639
    tow_count = df_merge_with_dpid(
        pd_query(TOW_COUNT_SQL.format(fk_type_id)),
        department_data)
    check_item_ids = get_query_condition_by_risktype(risk_name)[0]

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        "PREPARE_DATA": prepare_data,
        "TOW_COUNT": tow_count,
        'CHECK_ITEM_IDS': check_item_ids
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
