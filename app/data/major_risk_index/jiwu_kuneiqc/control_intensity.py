#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     control_intensity
   Author :       hwj
   date：          2020/1/7上午10:09
   Change Activity: 2020/1/7上午10:09
-------------------------------------------------
"""
from flask import current_app

from app.data.major_risk_index.common import control_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.jiwu_kuneiqc.control_intensity_sql import (
    CHECK_PROBLEM_AND_DEPART_SQL, CHECK_PROBLEM_AND_POINT_SQL,
    CHECK_POINT_SQL, WORKSHOP_INFO_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global YEAR, MONTH, LAST_MONTH
    global DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, CHECK_POINT_DATA, \
        WORKSHOP_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value("ZHANDUAN_DPID_DATA")
    CHEJIAN_DPID_DATA = GLV.get_value("CHEJIAN_DPID_DATA")
    DEPARTMENT_DATA = GLV.get_value("DEPARTMENT_DATA")
    global CHECK_ITEM_IDS, RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    # workshop_config_id = [7, 8, 281]
    # “检修库”、“整备场”
    CHECK_POINT_DATA = pd_query(CHECK_POINT_SQL.format(major))
    # 检修车间、解体组装车间、整备车间
    WORKSHOP_DATA = pd_query(WORKSHOP_INFO_SQL.format(major))
    current_app.logger.debug('|   └── extract data from mysql have done!')


def _stats_control_quality(months_ago):
    customizededuct = {
        "check_point_rst": {1: 0.5, 2: 1, 3: 2},
        "banzu_rst": {1: 5, 2: 10, 3: 15, 4: 20, 5: 25, 6: 30},
        "level_banzu_rst": {4: 3, 5: 4, 6: 5},
    }
    func_list = [
        'check_point_rst',
        'banzu_rst',
        'level_banzu_rst'
    ]
    sql = {
        "check_point_rst": CHECK_PROBLEM_AND_POINT_SQL,
        "banzu_rst": CHECK_PROBLEM_AND_DEPART_SQL,
        "level_banzu_rst": CHECK_PROBLEM_AND_DEPART_SQL,
    }
    data = {
        "check_point_rst": CHECK_POINT_DATA,
        "banzu_rst": WORKSHOP_DATA,
        "level_banzu_rst": WORKSHOP_DATA,
    }
    months = {
        'check_point_rst': 3,
        'banzu_rst': 3,
        'level_banzu_rst': 6
    }
    type_dict = {
        1: '车间',
        2: '检查点',
    }
    title = {
        'check_point_rst': "检修库、整备场问题卡控",
        'banzu_rst': '检修车间、解体组装车间、整备车间问题卡控',
        'level_banzu_rst': '检修车间、解体组装车间、整备车间AB类问题卡控'
    }

    return control_intensity.stats_control_quality_diff(
        CHECK_ITEM_IDS,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        _choose_dpid_data,
        sql_dict=sql,
        data_dict=data,
        customizededuct=customizededuct,
        func_list=func_list,
        months=months,
        type_dict=type_dict,
        title=title
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_control_quality
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a']]
    item_weight = [1]
    child_index_list = [1]
    update_major_maintype_weight(index_type=int(risk_type.split('-')[1]),
                                 major=risk_type,
                                 main_type=14,
                                 child_index_list=child_index_list,
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        14,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')


if __name__ == '__main__':
    pass
