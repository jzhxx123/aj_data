#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification
   Author :       hwj
   date：          2020/1/6上午10:06
   Change Activity: 2020/1/6上午10:06
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app import mongo
from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.jiwu_kuneiqc.problem_rectification_sql import (
    NORISK_OVERDUE_PROBLEM_NUMBER_SQL, NORISK_ALL_PROBLEM_NUMBER_SQL,
    TOTAL_PROBLEM_NUMBER_SQL, DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL
)
from app.data.major_risk_index.util import (
    data_complete_by_condition, get_major_index_data_bymon,
    df_merge_with_dpid, combine_child_index_func,
    summizet_child_index)
from app.data.util import (
    pd_query, get_history_months, update_major_maintype_weight)

SCORE = []
HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_repeatedly_index_ratio_score(
        row, column, major_column, detail_type, last_repeate_index_ratio, ):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    # 如果问题数/总问题数 无这个部门， 该值给0
    _last_ratio = (
            (last_repeate_index_ratio[1].get(row['DEPARTMENT_ID'], 0)
             - last_repeate_index_ratio[0]) / last_repeate_index_ratio[0]) \
        if last_repeate_index_ratio[0] > 0 else 0
    _ratio_changed_ratio = (
            (_ratio - _last_ratio) / _last_ratio) if _last_ratio > 0 else 0
    if _ratio_changed_ratio >= 0.1:
        _score = 100 - 100 * _ratio_changed_ratio
    else:
        _score = 100
    return _score


def get_last_month_alpha(months_ago, risk_type, main_type, detail_type):
    """
    获取某指数上个的a值
    a=（段调车风险问题数/段问题总数-专业基数）/专业基数。
    """
    # 获取专业基数
    risk_type_list = risk_type.split('-')
    mon = get_history_months(months_ago)[0]
    problem_data = list(mongo.db['monthly_major_index_basic_data'].find(
        {"MON": mon, "MAIN_TYPE": main_type,
         "DETAIL_TYPE": detail_type,
         "MAJOR": risk_type_list[0], "INDEX_TYPE": int(risk_type_list[1])},
        {"_id": 0, "DEPARTMENT_ID": 1, "AVG_QUOTIENT": 1, "QUOTIENT": 1}
    ))
    celic_dict = dict()
    if problem_data:
        for row in problem_data:
            celic_dict[row['DEPARTMENT_ID']] = row.get('QUOTIENT', 0)
        avg_quotient = problem_data[0].get('AVG_QUOTIENT', 0)
    else:
        avg_quotient = 0
    return avg_quotient, celic_dict


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        PROBLEM_COUNT, TOTAL_PROBLEM_COUNT, \
        DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA, MAJOR_INDEX_RANK_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value("ZHANDUAN_DPID_DATA")
    CHEJIAN_DPID_DATA = GLV.get_value("CHEJIAN_DPID_DATA")
    DEPARTMENT_DATA = GLV.get_value("DEPARTMENT_DATA")
    stats_months = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = GLV.get_value("CHECK_ITEM_IDS")

    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(NORISK_ALL_PROBLEM_NUMBER_SQL.format(
            *stats_months, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA
    )

    TOTAL_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(TOTAL_PROBLEM_NUMBER_SQL.format(*stats_months)),
        DEPARTMENT_DATA
    )
    #
    DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA = data_complete_by_condition(
        pd_query(DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(
            *stats_months)),
        ZHANDUAN_DPID_DATA,
        ['MAIN_TYPE', 'RESPONSIBILITY_IDENTIFIED'],
        [1, 1],
        DEPARTMENT_DATA
    )

    # 过去4个月重点指数排名数据
    MAJOR_INDEX_RANK_DATA = []
    for i in range(1, 5):
        MAJOR_INDEX_RANK_DATA.extend(
            get_major_index_data_bymon(months_ago - i, risk_type)
        )
    MAJOR_INDEX_RANK_DATA = pd.DataFrame(MAJOR_INDEX_RANK_DATA)
    # 避免空数据，做一些初始化操作
    if MAJOR_INDEX_RANK_DATA.empty:
        a = [months_ago - i for i in range(1, 5)]
    else:
        a = set([months_ago - i for i in range(1, 5)]) \
            - set(MAJOR_INDEX_RANK_DATA['MON_AGO'].values.tolist())
    for i in a:
        for _, j in ZHANDUAN_DPID_DATA.iterrows():
            MAJOR_INDEX_RANK_DATA = MAJOR_INDEX_RANK_DATA.append(
                [{
                    'DEPARTMENT_ID': j['DEPARTMENT_ID'],
                    'MON_AGO': i,
                    'RANK': 0,
                    "DEPARTMENT_NAME": j['NAME']
                }], ignore_index=True
            )


def _calc_rectification_effect_type_a(row):
    """[整改成效中计算a类型分数]
    main_type_title = {
        1: '事故', 2: '故障', 3: '综合信息'
    }
    resp_level_title = {1: '主要、全部责任', 2: '重要责任', 3: '次要责任'}
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if int(row['MAIN_TYPE']) == 1:
        _score = int(row['COUNT_A']) * (10 * 2 ** (3 - int(row['RESP_LEVEL'])))
    return _score


def _calc_rectification_effect_type_e(row):
    """[风险指数排名扣分]
    风险指数连续两个月排末位，扣5分，连续三个月排末位，扣10分；连续四个月排末位，扣20分
    Arguments:
        row {[type]} -- [description]
    """
    _score = 0
    if row['COUNT_E'] > 1:
        _score = 10 * (row['COUNT_E'] - 1)
    return _score


# 整改时效
def _stats_rectification_overdue(months_ago):
    """
    牵车作业风险问题超期整改扣分：牵车作业风险问题整改超期1条扣0.2分
    :param months_ago:
    :return:
    """
    return problem_rectification.stats_rectification_overdue(
        CHECK_ITEM_IDS, NORISK_OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 问题控制
def _stats_repeatedly_index_ratio(months_ago):
    """
    段牵车风险问题数/段问题总数，与专业基数比较。
    即a=（段牵车风险问题数/段问题总数-专业基数）/专业基数。
    a较前一月高10%，扣10分(按照百分比进行扣分换算，每1%扣1分)
    """
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>" \
                       + " 专业基数: {2}</p><p>问题控制率({3}) = " \
                       + "段牵车风险问题数({4})/ 段问题总数({5})*100%</p>"
    return problem_rectification.stats_repeatedly_index_ratio(
        PROBLEM_COUNT, TOTAL_PROBLEM_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data,
        customizecontent=customizecontent,
        calc_alpha_func=get_last_month_alpha,
        calc_score_by_formula=_calc_repeatedly_index_ratio_score,
    )


# 整改成效
def _stats_rectification_effect(months_ago):
    df_dict = {
        'A': DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_DATA,
        'E': MAJOR_INDEX_RANK_DATA,
    }
    rectification_type_calc_func_dict = {
        'A': _calc_rectification_effect_type_a,
        'E': _calc_rectification_effect_type_e
    }
    return problem_rectification.stats_rectification_effect_excellent(
        DEPARTMENT_DATA, months_ago,
        RISK_TYPE, _choose_dpid_data, df_dict,
        rectification_type_calc_func_dict=rectification_type_calc_func_dict,
        calc_score_by_formula=lambda x: 60 if x > 60 else x)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue,
        _stats_repeatedly_index_ratio,
        _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'f']]
    item_weight = [0.3, 0.7, -1]
    update_major_maintype_weight(index_type=int(risk_type.split('-')[1]),
                                 major=risk_type,
                                 main_type=6,
                                 child_index_list=['a', 'c', 'f'],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
