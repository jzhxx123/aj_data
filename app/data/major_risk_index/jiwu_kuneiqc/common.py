#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/12/28上午8:41
   Change Activity: 2019/12/28上午8:41
-------------------------------------------------
"""
from datetime import timedelta

import pandas as pd
from dateutil import rrule
from interval import Interval

from app.data.index.util import get_month_day
from app.data.major_risk_index.util import (
    calc_extra_child_score_groupby_major_third, df_merge_with_dpid, write_cardinal_number_basic_data,
    calc_extra_child_basic_data)


def get_day_key_time_count(start_hour, end_hour, z_list):
    t = Interval(start_hour, end_hour)
    for z in z_list:
        # 判断数值区间是否重叠
        if t.overlaps(z):
            # count += 1
            return True
    return False


def get_key_hour_count(hour_check_count, prepare_workshop, months_ago):
    """
    获取每日检查中包含了关键时段检查的次数
    """
    calc_basic_data = {}
    other_i = [Interval(8, 10), Interval(13, 15)]
    prepare_i = [Interval(22, 24)]
    for idx, row in hour_check_count.iterrows():
        z_list = other_i
        dpid = row['TYPE3']
        dp_id_data = calc_basic_data.get(dpid, {})
        start_date = row['START_CHECK_TIME']
        end_date = row['END_CHECK_TIME']
        start_day = start_date.day
        end_day = end_date.day
        start_hour = start_date.hour
        end_hour = end_date.hour
        if row['TYPE4'] in prepare_workshop.DEPARTMENT_ID:
            z_list = prepare_i
        if start_day == end_day:
            # 同一天
            overlap = get_day_key_time_count(start_hour, end_hour, z_list)
            date = start_day
            if overlap:
                count = dp_id_data.get(date, 0)
                count += 1
                dp_id_data.update({date: count})
        else:
            # 不同天
            diff = rrule.rrule(
                rrule.DAILY, dtstart=start_date, until=end_date).count()
            for day in range(diff + 1):
                # 第一天
                if day == 0:
                    overlap = get_day_key_time_count(start_hour, 24, z_list)
                # 最后一天
                elif day == diff:
                    overlap = get_day_key_time_count(0, end_hour, z_list)
                # 中间天
                else:
                    overlap = True
                if not overlap:
                    continue
                date = (start_date + timedelta(day)).day
                count = dp_id_data.get(date, 0)
                count += 1
                dp_id_data.update({date: count})
        calc_basic_data.update({dpid: dp_id_data})
    data = pd.DataFrame(calc_basic_data).unstack().unstack()
    columns = data.columns.values
    sys_mon_day = get_month_day(months_ago)
    # 填充无关键时段检查的日期
    for i in range(1, sys_mon_day + 1):
        if i not in columns:
            data[i] = 0
    data.rename(columns={t: 'day_{0}'.format(t) for t in data.columns.values},
                inplace=True)
    return data


def _cal_key_hour_evenness_basic_data(row, columns):
    """获取关键时段均衡度的文本描述和得分"""
    day_basic_data = {
        '比较值低于30%的日期': 0,
        '比较值低于20%的日期': 0,
        '比较值低于10%的日期': 0,
    }
    calc_basic_data = {
        '比较值低于30%的日期': [],
        '比较值低于20%的日期': [],
        '比较值低于10%的日期': []
    }
    score = [100]
    for day in columns:
        key_count = row[f'day_{day}']
        day_count = row[day]
        ratio = (key_count / day_count) if day_count > 0 else 0
        if ratio < 0.1:
            daily_deduction = - 2
            deduct_type = '比较值低于10%的日期'
        elif ratio < 0.2:
            daily_deduction = - 1
            deduct_type = '比较值低于20%的日期'
        elif ratio < 0.3:
            daily_deduction = - 0.2
            deduct_type = '比较值低于30%的日期'
        else:
            daily_deduction = 0
            deduct_type = None
        if not deduct_type:
            continue
        day_content = \
            '{0}号[关键时段检查次数：{1:.0f},每日检查次数：{2:.0f}, 比较值:{3:.3f}]'
        deduct_day = calc_basic_data.get(deduct_type)
        score.append(daily_deduction)
        deduct_day.append(day_content.format(day, key_count, day_count, ratio))
        calc_basic_data.update({deduct_type: deduct_day})
        day_basic_data.update(
            {deduct_type: day_basic_data.get(deduct_type) + 1})
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/><br/>'.join(
        [f'{k}: {day_basic_data.get(k)}天<br/>{"<br/>".join(v)}'
         for k, v in calc_basic_data.items()])
    return total_score, rst_calc_data


def get_problem_exposure_basic_data(all_problem, dp_id_df):
    """
    获取问题暴露度中不同质量的问题数
    :param all_problem: 总问题数
    :param dp_id_df: 部门
    :return: 不同等级的问题数及分数
    """
    # 总问题质量分
    problem_score = all_problem.copy().drop(columns='COUNT', axis=1).rename(
        columns={'CHECK_SCORE': 'COUNT'})

    # 中高质量问题数及分数
    middle_problem_count = all_problem[
        all_problem.LEVEL.isin(
            ['A', 'B', 'E1', 'E2', 'F1', 'F2', 'C', 'E3', 'F3'])].copy()
    middle_problem_score = middle_problem_count.copy().drop(
        columns='COUNT', axis=1).rename(columns={'CHECK_SCORE': 'COUNT'})
    # 高质量问题数及分数
    high_problem_count = all_problem[
        all_problem.LEVEL.isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])].copy()
    high_problem_score = high_problem_count.copy().drop(
        columns='COUNT', axis=1).rename(columns={'CHECK_SCORE': 'COUNT'})
    all_problem = df_merge_with_dpid(
        all_problem, dp_id_df, how='right').fillna(0)
    problem_score = df_merge_with_dpid(
        problem_score, dp_id_df, how='right').fillna(0)
    middle_problem_count = df_merge_with_dpid(
        middle_problem_count, dp_id_df, how='right').fillna(0)
    middle_problem_score = df_merge_with_dpid(
        middle_problem_score, dp_id_df, how='right').fillna(0)
    high_problem_count = df_merge_with_dpid(
        high_problem_count, dp_id_df, how='right').fillna(0)
    high_problem_score = df_merge_with_dpid(
        high_problem_score, dp_id_df, how='right').fillna(0)
    basic_data = {
        'CHECK_PROBLEM': all_problem,
        'PROBLEM_SCORE': problem_score,
        'MIDDLE_PROBLEM_COUNT': middle_problem_count,
        'MIDDLE_PROBLEM_SCORE': middle_problem_score,
        'HIGH_PROBLEM_COUNT': high_problem_count,
        'HIGH_PROBLEM_SCORE': high_problem_score,
    }
    return basic_data


def get_middle_problem_score_basic_data(
        middle_problem_score,
        tow_count,
        choose_dpid_data,
        idx,
        title=None,
        fraction=None,
        zhanduan_filter_list=None,
        calc_score_by_formula=None

):
    # A
    problem_score = middle_problem_score.groupby('TYPE3')['COUNT'].sum()
    tow_count = tow_count.groupby('TYPE3')['COUNT'].sum()
    data = pd.concat(
        [
            problem_score.to_frame(name='numerator'),
            tow_count.to_frame(name='denominator')
        ], axis=1, sort=False
    )
    data['ratio'] = data.apply(
        lambda row: (row['numerator'] / row['denominator'])
        if row['denominator'] > 0 else 0, axis=1
    )
    if fraction:
        write_cardinal_number_basic_data(data, choose_dpid_data(3), fraction,
                                         fraction.risk_type, 5,
                                         17, fraction.months_ago,
                                         columns=['prob', 'PERSON_NUMBER'])

    # 计算结果
    rst_data, data = calc_extra_child_basic_data(
        data, choose_dpid_data(3), 'ratio', calc_score_by_formula,
        zhanduan_filter_list=zhanduan_filter_list,
        fraction=fraction
    )
    # 中间计算数据
    data.fillna(0, inplace=True)
    cnt_col = f'middle_{idx}'
    if len(data) > 0:
        data[cnt_col] = data.apply(
            lambda row: title[idx].format(
                row['SCORE'], row['group_sort'], row['AVG_NUMBER'], row['ratio'],
                row["numerator"], row['denominator'], row['AVG_SCORE']),
            axis=1)
    else:
        data[cnt_col] = ''
    data = data.set_index('DEPARTMENT_ID')[[cnt_col]]
    return rst_data, data


def get_middle_problem_other_check(
        check_problem_base_count,
        problem_base_count,
        choose_dpid_data,
        idx,
        title=None,
        fraction=None,
        zhanduan_filter_list=None,
        calc_score_by_formula=None,
):
    # B
    # 站段下所有检查出来的问题
    data = pd.merge(
        check_problem_base_count,
        problem_base_count,
        on='FK_PROBLEM_BASE_ID'
    )
    # 其他单位查出问题
    other_data = data[~(data.CHECK_DEPARTMENT_ID == data.FK_DEPARTMENT_ID)]
    # 本单位查出问题
    self_data = data[data.CHECK_DEPARTMENT_ID == data.FK_DEPARTMENT_ID]
    # 其他单位查出问题,本单位未查出
    other_check = other_data[
        ~other_data.FK_PROBLEM_BASE_ID.isin(
            self_data.FK_PROBLEM_BASE_ID)]
    other_check = other_check.groupby(
        'FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    basic_data = df_merge_with_dpid(
        other_check, choose_dpid_data(3), how='right')
    basic_data.fillna(0, inplace=True)
    tmp = title[idx]
    cnt_col = f'middle_{idx}'
    basic_data[cnt_col] = basic_data.apply(
        lambda row: tmp.format(row['COUNT']), axis=1)
    basic_data['SCORE'] = basic_data.apply(
        lambda row: max(100 - row['COUNT'] * 10, 0), axis=1)
    rst_data = basic_data.set_index('DEPARTMENT_ID')[['SCORE']]
    data = basic_data.set_index('DEPARTMENT_ID')[[cnt_col]]
    return rst_data, data
