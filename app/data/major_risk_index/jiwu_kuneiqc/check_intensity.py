#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/12/28下午3:55
   Change Activity: 2019/12/28下午3:55
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.util import (
    df_merge_with_dpid,
    combine_child_index_func,
    summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
from app.data.major_risk_index.jiwu_kuneiqc.check_intensity_sql import (
    CHECK_COUNT_SQL, NORISK_PROBLEM_CHECK_SCORE_SQL, YECHA_CHECK_SQL,
    MEDIA_COST_TIME_SQL,
    NORISK_MEDIA_PROBLEM_NUMBER_SQL, NORISK_MEDIA_PROBLME_SCORE_SQL
)


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH, CHECK_ITEM_IDS, RISK_IDS
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, TOW_COUNT, MEDIA_COST_TIME, \
        NORISK_MEDIA_PROBLEM_NUMBER, NORISK_MEDIA_PROBLME_SCORE
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    # 牵车次数
    TOW_COUNT = GLV.get_value('TOW_COUNT')
    stats_month = get_custom_month(months_ago)

    # 检查次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(NORISK_PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 监控调阅时长
    MEDIA_COST_TIME = df_merge_with_dpid(
        pd_query(MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 监控调阅发现问题数
    NORISK_MEDIA_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(NORISK_MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month,
                                                        CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 监控调阅质量分
    NORISK_MEDIA_PROBLME_SCORE = df_merge_with_dpid(
        pd_query(NORISK_MEDIA_PROBLME_SCORE_SQL.format(*stats_month,
                                                       CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    # fraction = GLV.get_value("stats_check_per_person")[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 人均质量分
def _stats_score_per_person(months_ago):
    # fraction = GLV.get_value("stats_score_per_person")[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_score_by_formula,
    )


# 夜查率
def _stats_yecha_ratio(months_ago):
    customizecontent = "<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: " \
        + "{2}</p><p>夜查率({3}) = 夜查次数({4})/ 工作量({5})*100%</p>"
    return check_intensity.stats_yecha_ratio(
        YECHA_COUNT,
        TOW_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=lambda x: min(100, x * 100),
        is_calc_score_base_major=False,
        customizecontent=customizecontent)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    # 需要执行的子指数名称列表
    media_func_key_list = ['media_cost_time',
                           'media_problem_number',
                           'media_problem_score',
                           ]
    # 子指数执行需要的数据字典
    media_func_data_dict = {
        'media_cost_time': MEDIA_COST_TIME,
        'media_problem_number': NORISK_MEDIA_PROBLEM_NUMBER,
        'media_problem_score': NORISK_MEDIA_PROBLME_SCORE,
    }
    # 子指数分数计算函数
    calc_score_by_formula_dict = {
        'media_cost_time': _calc_score_by_formula,
        'media_problem_number': _calc_score_by_formula,
        'media_problem_score': _calc_score_by_formula,
    }
    return check_intensity.stats_media_intensity_excellent(
        DEPARTMENT_DATA,
        TOW_COUNT,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        child_weight=[0.3, 0.35, 0.35],
        media_func_key_list=media_func_key_list,
        media_func_data_dict=media_func_data_dict,
        calc_score_by_formula_dict=calc_score_by_formula_dict,
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_score_per_person,
        _stats_yecha_ratio,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'e', 'j', 'g']]
    item_weight = [0.35, 0.25, 0.3, 0.1]
    update_major_maintype_weight(index_type=7, major=risk_type, main_type=1,
                                 child_index_list=['b', 'e', 'j', 'g'],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
