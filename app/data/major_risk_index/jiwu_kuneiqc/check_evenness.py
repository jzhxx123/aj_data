#! /usr/bin/env python3
# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness
   Author :       hwj
   date：          2019/12/28下午5:05
   Change Activity: 2019/12/28下午5:05
-------------------------------------------------
"""
import pandas as pd
from flask import current_app
from app.data.index.util import (
    get_custom_month, get_month_day)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.jiwu_kuneiqc.check_evenness_sql import *
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.jiwu_kuneiqc import GLV
from app.data.major_risk_index.jiwu_kuneiqc.common import (
    get_key_hour_count, _cal_key_hour_evenness_basic_data)
from app.data.major_risk_index.util import (
    df_merge_with_dpid, summizet_child_index, combine_child_index_func,
    format_export_basic_data,
    write_export_basic_data_to_mongo, summizet_operation_set,
    append_major_column_to_df)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, \
        CHECK_POINT_DATA, WORK_LOAD_DATA, DAILY_CHECK_COUNT, TOW_COUNT, \
        HOUR_CHECK_COUNT, POINT_COUNT, POINT_CHECK_COUNT, PREPARE_WORKSHOP

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    # 牵车次数
    TOW_COUNT = GLV.get_value('TOW_COUNT')
    global CHECK_ITEM_IDS, CHECK_POINT_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')

    # 较大以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            NO_RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中较大及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 每日检查数
    DAILY_CHECK_COUNT = df_merge_with_dpid(
        pd_query(
            DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 时段检查数
    HOUR_CHECK_COUNT = df_merge_with_dpid(
        pd_query(
            HOUR_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 地点数
    POINT_COUNT = df_merge_with_dpid(pd_query(POINT_COUNT_SQL), DEPARTMENT_DATA)

    # 地点受检数
    POINT_CHECK_COUNT = pd_query(
        POINT_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS))

    # 整备车间
    PREPARE_WORKSHOP = pd_query(PREPARE_WORKSHOP_SQL.format(major))

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


def _cal_check_time_evenness_score_new(row, columns, basedata=None,
                                       day_content=None,
                                       deduct_dict=None):
    """
    基数（站段级）=段全月牵车检查次数/基本工作量
    比较值=每日牵车检查次数/每日基本工作量
    结果=（比较值-基数）/ 基数
    低于基数40%的扣0.5分/日，低于60%的扣1分/日，
    低于100%的扣2分/日，得分=100-扣分。]
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算站段基准值:
    if deduct_dict is None:
        deduct_dict = {
            0.4: 0.5,
            0.6: 1,
            1: 2
        }
    _key_list = list(deduct_dict.keys())
    # 默认升序排列符合渲染格式
    _key_list.sort()
    total_check_count = row['depart_check']
    work_load = row['work_load']
    # 基数
    avg_check_count = (total_check_count / work_load) if work_load > 0 else 0

    # 保存中间计算过程
    # 这里沿用升序（0.2，0.5，1）
    day_basic_data = {
        f'比较值低于基数{_key_list[0] * 100}%的日期': 0,
        f'比较值低于基数{_key_list[1] * 100}%的日期': 0,
        f'比较值低于基数{_key_list[2] * 100}%的日期': 0
    }
    calc_basic_data = {
        f'比较值低于基数{_key_list[0] * 100}%的日期': [],
        f'比较值低于基数{_key_list[1] * 100}%的日期': [],
        f'比较值低于基数{_key_list[2] * 100}%的日期': []
    }
    score = [100]
    for day in columns:
        # 每日工作量及每日检查数
        day_work_load = row['day_work_load']
        day_check_count = row[day]
        # 比较值
        base_check_count = (
                day_check_count / day_work_load) if day_work_load > 0 else 0
        if base_check_count == 0:
            continue
        ratio = (base_check_count - avg_check_count) / avg_check_count
        # 这里沿用降序的字典键默认（1，0.5，0.2）
        if ratio <= -_key_list[2]:
            daily_deduction = - deduct_dict[_key_list[2]]
            deduct_type = f'比较值低于基数{_key_list[2] * 100}%的日期'
        elif ratio <= -_key_list[1]:
            daily_deduction = - deduct_dict[_key_list[1]]
            deduct_type = f'比较值低于基数{_key_list[1] * 100}%的日期'
        elif ratio <= -_key_list[0]:
            daily_deduction = - deduct_dict[_key_list[0]]
            deduct_type = f'比较值低于基数{_key_list[0] * 100}%的日期'
        else:
            daily_deduction = 0
            deduct_type = None
        score.append(daily_deduction)

        deduct_day = calc_basic_data.get(deduct_type)
        if not deduct_type:
            continue
        if not day_content:
            day_content = \
                '{0}号[基数：{1:.3f},工作量：{2:.0f},比较值：{3:.3f},实际检查值：{4:.0f}]'
        deduct_day.append(
            day_content.format(day, avg_check_count, day_work_load,
                               base_check_count, day_check_count))
        calc_basic_data.update({deduct_type: deduct_day})
        day_basic_data.update(
            {deduct_type: day_basic_data.get(deduct_type) + 1})

    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: {day_basic_data.get(k)}天<br/>{"<br/>".join(v)}' for k, v in
         calc_basic_data.items()])
    return total_score, rst_calc_data


# 检查日期均衡度
def _stats_check_time_evenness(months_ago):
    """
    每日牵车检查次数/每日基本工作量（段总工作量/当月天数），
    低于基数40%的扣0.5分/日，低于基数60%的扣1分/日，低于100%的扣2分/日，得分=100-扣分。
    基数=段全月牵车检查次数/基本工作量
    """
    rst_index_score = []
    # 系统月天数
    day = get_month_day(months_ago)
    # 站段工作量
    day_tow_count = TOW_COUNT.groupby('TYPE3')['COUNT'].sum().to_frame(
        name='work_load')
    # 每日工作量
    day_tow_count['day_work_load'] = day_tow_count.apply(
        lambda row: row['work_load'] / day, axis=1
    )
    # 每日检查次数
    day_check = DAILY_CHECK_COUNT.groupby(['TYPE3', 'DAY'])[
        'COUNT'].sum().unstack()
    # 站段检查次数
    depart_check = DAILY_CHECK_COUNT.groupby('TYPE3')['COUNT'].sum().to_frame(
        name='depart_check')

    data = pd.merge(day_tow_count, day_check, right_on='TYPE3', left_index=True)
    data = pd.merge(depart_check, data, right_on='TYPE3', left_index=True)
    day_columns = day_check.columns.values
    column = 'SCORE_b_3'
    data['CONTENT'] = data.apply(
        lambda row: _cal_check_time_evenness_score_new(
            row, day_columns)[1],
        axis=1)

    data[column] = data.apply(
        lambda row: _cal_check_time_evenness_score_new(
            row, day_columns)[0],
        axis=1)
    data = append_major_column_to_df(ZHANDUAN_DPID_DATA, data)
    calc_basic_data_rst = format_export_basic_data(data.copy(), 4, 2, 3,
                                                   months_ago,
                                                   risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                     2, risk_type=RISK_TYPE)
    data.drop(['MAJOR'], inplace=True, axis=1)
    data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, column].values,
        columns=[column])
    summizet_operation_set(data, _choose_dpid_data(3), column,
                           3, 2, 4, 2, months_ago, risk_type=RISK_TYPE)
    rst_index_score.append(data)
    return rst_index_score


# 关键时段检查均衡度
def _stats_key_hour_check_evenness(months_ago):
    """
    关键时段（8时-10时）、（13:00-15:00）、（整备车间22:00-0:00）：
    每日段关键时段牵车检查次数/段每日牵车检查次数
    低于30%的扣0.2分/日，低于20%的扣1分/日，低于10%的扣2分/日，得分=100-扣分
    """
    rst_index_score = []
    # 获取关键检查数及每日检查数
    key_hour_df = get_key_hour_count(
        HOUR_CHECK_COUNT, PREPARE_WORKSHOP, months_ago)
    day_check_df = DAILY_CHECK_COUNT.groupby(['TYPE3', 'DAY'])[
        'COUNT'].sum().unstack()
    data = pd.concat([key_hour_df, day_check_df], axis=1, sort=False)
    data = append_major_column_to_df(ZHANDUAN_DPID_DATA, data)
    # 获取中间过程数据及得分
    columns = day_check_df.columns.values
    data['CONTENT'] = data.apply(
        lambda row: _cal_key_hour_evenness_basic_data(row, columns)[1], axis=1
    )
    data['SCORE_f_3'] = data.apply(
        lambda row: _cal_key_hour_evenness_basic_data(row, columns)[0], axis=1
    )
    # 中间过程导入数据库
    calc_basic_data_rst = format_export_basic_data(
        data.copy(), 4, 6, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 4, 6, risk_type=RISK_TYPE)
    data.drop(['MAJOR'], inplace=True, axis=1)
    column = 'SCORE_f_3'
    xdata = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, column].values,
        columns=[column])
    # 最终得分入库
    summizet_operation_set(
        xdata,
        _choose_dpid_data(3),
        column,
        3,
        2,
        4,
        6,
        months_ago,
        risk_type=RISK_TYPE)
    rst_index_score.append(xdata)
    return rst_index_score


def point_item_config_check(row, check_item_set):
    """获取具备检查项目的地点"""
    point_item = set(row['CHECK_ITEM_IDS'].split(','))
    diff = point_item.intersection(check_item_set)
    if diff:
        return True
    return False


def _calc_check_address_evenness_basic_data(basic_data):
    """获取检查地点均衡度的基础数据（CONTENT, SCORE_c_3）"""
    # 计算比较值
    all_check_count = sum(basic_data['COUNT'])
    all_point_count = len(basic_data)
    base_ratio = (
            all_check_count / all_point_count) if all_point_count > 0 else 1

    address_count_dict = {
        '总检查地点数:': 0,
        '受检次数超过比较值500%以上地点:': 0,
        '受检次数低于比较值20%以上地点:': 0,
        '未检查地点:': 0
    }
    calc_basic_data = {}
    dpid_address_count = {}
    for idx, row in basic_data.iterrows():
        dpid = row['TYPE3']
        dp_id_data = calc_basic_data.get(dpid, {})
        score = dp_id_data.get('SCORE_c_3', 100)
        content = dp_id_data.get('CONTENT', {})
        if dpid not in dpid_address_count:
            dpid_address_count.update({dpid: address_count_dict.copy()})
        address_count = dpid_address_count.get(dpid)
        address_count.update(
            {'总检查地点数:': address_count.get('总检查地点数:', 0) + 1})
        check_count = row['COUNT']
        tmp = '{0}(比较值：{1:.0f}, 受检次数：{2:.0f})'.format(row['ALL_NAME'],
                                                      base_ratio, check_count)
        ratio = (check_count - base_ratio) / base_ratio
        if check_count == 0:
            deduct_type = '未检查地点:'
            score += -10
        elif ratio > 5:
            score += -1
            deduct_type = '受检次数超过比较值500%以上地点:'
        elif ratio < -0.2:
            score += -1
            deduct_type = '受检次数低于比较值20%以上地点:'
        else:
            deduct_type = None
        if deduct_type:
            content.update({deduct_type: content.get(
                deduct_type, '') + '<br/>' + tmp})
            address_count.update(
                {deduct_type: address_count.get(deduct_type, 0) + 1})
        dp_id_data.update({'CONTENT': content, 'SCORE_c_3': score})
        dpid_address_count.update({dpid: address_count})
        calc_basic_data.update({dpid: dp_id_data})
    for dpid in calc_basic_data:
        """拼接每个站段不同key的CONTENT"""
        dpid_basic = calc_basic_data.get(dpid)
        content = ';<br/>'.join(
            [f'总检查地点数: {dpid_address_count.get(dpid).get("总检查地点数:")}个'] +
            [f'{k} {dpid_address_count.get(dpid).get(k)}个{v}' for k, v in
             dpid_basic.get('CONTENT').items()]
        )
        dpid_basic.update({'CONTENT': content})
        calc_basic_data.update({dpid: dpid_basic})
    return calc_basic_data


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """
    检查地点范围为：检查项目为“机务-生产作业-牵车作业”且重要生产场所为“检修库”、“整备场”
    每个地点受检次数超过比较值500%以上的一处扣1分;
    受检次数低于比较值20%的一处扣1分，未检查的一处扣10分
    """
    rst_index_score = []
    check_item_set = set(CHECK_ITEM_IDS.split(','))
    # 获取重要检查点及检查次数
    point_count = POINT_COUNT[POINT_COUNT.apply(
        lambda row: point_item_config_check(row, check_item_set), axis=1
    )].copy()
    basic_data = pd.merge(
        point_count,
        POINT_CHECK_COUNT,
        on='POINT_ID',
        how='left',
    )
    basic_data.fillna(0, inplace=True)

    # 获取中间过程数据
    calc_basic_data = _calc_check_address_evenness_basic_data(basic_data)
    data = pd.DataFrame(calc_basic_data).unstack().unstack()
    column = 'SCORE_c_3'
    if data.empty:
        data = ZHANDUAN_DPID_DATA.copy()
        data['CONTENT'] = '总检查地点数: 0个'
        data[column] = 100
    else:
        data = append_major_column_to_df(ZHANDUAN_DPID_DATA, data)
        data[column].fillna(100, inplace=True)
        data['CONTENT'].fillna('总检查地点数: 0个', inplace=True)
    # 数据库导入
    calc_basic_data_rst = format_export_basic_data(data.copy(), 4, 3, 3,
                                                   months_ago,
                                                   risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                     3, risk_type=RISK_TYPE)
    data.drop(['MAJOR'], inplace=True, axis=1)
    data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, column].values,
        columns=[column])
    summizet_operation_set(data, _choose_dpid_data(3), column,
                           3, 2, 4, 3, months_ago, risk_type=RISK_TYPE)
    rst_index_score.append(data)
    return rst_index_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness,
        _stats_check_time_evenness,
        _stats_key_hour_check_evenness,
        _stats_check_address_evenness
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'f']]
    item_weight = [0.15, 0.25, 0.3, 0.3]
    update_major_maintype_weight(
        index_type=int(risk_type.split('-')[1]),
        major=risk_type,
        main_type=4,
        child_index_list=['a', 'b', 'c', 'f'],
        child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
