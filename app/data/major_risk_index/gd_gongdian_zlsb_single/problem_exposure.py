# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exprosure
   Author :       hwj
   date：          2019/10/11下午4:14
   Change Activity: 2019/10/11下午4:14
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gd_gongdian_zlsb_single import GLV
from app.data.major_risk_index.gongdian_zlsb.check_intensity_sql import WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_zlsb.common import stats_total_problem_exposure_type
from app.data.major_risk_index.gongdian_zlsb.problem_exposure_sql import CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_SQL, \
    HIDDEN_KEY_PROBLEM_MONTH_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL, \
    SAFETY_PRODUCE_INFO_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE, CHECK_ITEM_IDS, CHECK_RISK_IDS
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, TOTAL_TICKETS
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)

    risk_stats_conf = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_stats_conf[0]
    CHECK_RISK_IDS = risk_stats_conf[1]
    staff_positions = ','.join(
        [f'"{postion}"' for postion in risk_stats_conf[2].split(',')])

    # # 统计工作量【职工总人数】
    # data = df_merge_with_dpid(
    #     pd_query(WORK_LOAD_SQL.format(staff_positions, major)),
    #     DEPARTMENT_DATA)

    # 单位出车趟次(作业车工作票实体类)
    work_load = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    STAFF_NUMBER = work_load.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """自轮设备-运用总问题数（质量分）/本单位出车趟次（采集接触网生产管理系统自轮设备工作票数量）"""
    return stats_total_problem_exposure_type(
        CHECK_RISK_IDS, CHECK_PROBLEM_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, weight_item=[0.5, 0.5])


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续3月无的扣1分/条，连续4个月无的扣2分/条，…扣月份-2分/条。得分=100-扣分。
    """
    return problem_exposure.stats_problem_exposure_three(
        CHECK_RISK_IDS, ZHANDUAN_DPID_DATA,
        HIDDEN_KEY_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣3/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """
    一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。
    """
    return problem_exposure.stats_other_problem_exposure(
        CHECK_RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score={"1": 5, "2": 3, "3": 1})


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'd', 'e']]
    item_weight = [0.65, 0.25, 0.1, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=14, major=risk_type, main_type=5, child_index_list=[1, 3, 4, 5],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
