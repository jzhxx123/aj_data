# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/10/11下午4:14
   Change Activity: 2019/10/11下午4:14
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gd_gongdian_zlsb_single import GLV
from app.data.major_risk_index.gongdian_zlsb.check_intensity_sql import CHECK_COUNT_SQL, CHECK_PROBLEM_COUNT_SQL, \
    ALL_PROBLEM_COUNT_SQL, PROBLEM_CHECK_SCORE_SQL, YIBAN_RISK_LEVEL_PROBLEM_SCORE_SQL, CHECK_POINT_SQL, \
    ALL_CHECK_POINT_SQL, WORK_TICKET_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, \
    MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, WORK_LOAD_SQL
from app.data.major_risk_index.gongdian_zlsb.common import stats_work_ticket
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index, \
    calc_child_index_type_divide_major
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, CHECK_PROBLEM_COUNT, ALL_PROBLEM_COUNT, \
        YIBAN_RISK_LEVEL_PROBLEM_SCORE, CHECK_POINT, ALL_CHECK_POINT, \
        PROBLEM_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, CHECK_ITEM_IDS, CHECK_RISK_IDS, \
        TOTAL_TICKETS, GENBAN_TICKETS, YECHA_TICKETS, YECHA_GENBAN_TICKETS
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    CHECK_RISK_IDS = diaoche[1]

    # 添乘检查+现场检查次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 查处问题数
    CHECK_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_COUNT_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 问题总数
    ALL_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_COUNT_SQL.format(CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 一般及以上风险问题质量均分
    YIBAN_RISK_LEVEL_PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(YIBAN_RISK_LEVEL_PROBLEM_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 现场检查或添乘检查（自轮设备-运用）地点数
    CHECK_POINT = df_merge_with_dpid(
        pd_query(CHECK_POINT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)
    # 地点总数
    ALL_CHECK_POINT = df_merge_with_dpid(
        pd_query(ALL_CHECK_POINT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 工作票总数, 跟班票数， 夜查票数， 跟班夜查票数
    TOTAL_TICKETS, GENBAN_TICKETS, YECHA_TICKETS, YECHA_GENBAN_TICKETS = stats_work_ticket(
        pd_query(WORK_TICKET_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    # 单位出车趟次(作业车工作票实体类)
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 添乘检查频次:35%
def _stats_check_per_tickets(months_ago):
    """
    （添乘检查（检查项目为“自轮设备-运用”）+现场检查（检查项目为“自轮设备-运用”，且勾选跟班的）/本单位出车趟
    次（采集接触网生产管理系统自轮设备工作票数量）"""
    return calc_child_index_type_divide_major(
        CHECK_COUNT,
        WORK_LOAD,
        2,
        1,
        14,
        months_ago,
        'COUNT',
        'SCORE_n',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 查处问题率:10%
def _stats_check_problem_ratio(months_ago):
    """查处“自轮设备-运用”问题数/自轮设备总问题数"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = '
               + '查处自轮设备问题数({4})/ 自轮设备总问题数({5})</p>', None]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        CHECK_PROBLEM_COUNT,
        ALL_PROBLEM_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content
    )


# 人均质量分:10%
def _stats_score_per_person(months_ago):
    """自轮设备-运用问题质量分累计/本单位出车趟次"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = '
               + '自轮设备问题质量分累计({4})/ 出车趟次({5})</p>', None]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content
    )


# 较大及以上风险问题质量均分:10%
def _stats_yiban_risk_score_per_person(months_ago):
    """一般及及以上问题质量分累计/本单位出车趟次"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>一般风险问题质量均分({3}) = '
               + '一般风险及以上问题质量分累计({4})/出车趟次({5})</p>', None]
    return calc_child_index_type_divide_major(
        YIBAN_RISK_LEVEL_PROBLEM_SCORE,
        WORK_LOAD,
        2,
        1,
        15,
        months_ago,
        'COUNT',
        'SCORE_o',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        customizecontent=content)


# 覆盖率:5%
def _stats_check_address_ratio(months_ago):
    """现场检查或添乘检查（自轮设备-运用）地点数/地点总数（重要检查点中对应有作业车的工区）"""
    return calc_child_index_type_divide_major(
        CHECK_POINT,
        ALL_CHECK_POINT,
        2,
        1,
        16,
        months_ago,
        'COUNT',
        'SCORE_p',
        lambda x: x * 100,
        _choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type=RISK_TYPE)


# 换算单位检查频次:15%
def _stats_check_per_person(months_ago):
    """现场检查次数/地点总数"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
               + '现场检查作业次数({4})/ 地点总数({5})</p>', None]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        ALL_CHECK_POINT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content)


# 监控调阅力度:15%
def _stats_media_intensity(months_ago):
    """
    监控调阅人均时长（35%）=监控调阅时长累计/本单位出车趟次
    监控调阅人均问题个数（35%）=监控调阅发现自轮设备-运用问题数/本单位出车趟次
    监控调阅人均质量分（20%）=监控调阅发现自轮设备-运用问题质量分累计/本单位出车趟次
    监控调阅覆盖比例(10%)=调阅检查班组数/具备调阅条件班组数
    """
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    title = ['监控调阅时长累计({0})/出车趟次({1})',
             '监控调阅发现问题数({0})/出车趟次({1})',
             '监控调阅发现问题质量分累计({0})/出车趟次({1})',
             '调阅班组数({0})/班组数({1})']
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        risk_ids=CHECK_RISK_IDS,
        choose_dpid_data=_choose_dpid_data,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        monitor_watch_discovery_ratio_sqllist=MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST,
        title=title,
        child_weight=[0.35, 0.35, 0.2, 0.1]
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_tickets,
        _stats_score_per_person,
        _stats_check_problem_ratio,
        _stats_yiban_risk_score_per_person,
        _stats_check_address_ratio,
        _stats_check_per_person,
        _stats_media_intensity
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [f'SCORE_{x}' for x in ['b', 'c', 'e', 'p', 'j', 'n', 'o']]
    item_weight = [0.15, 0.1, 0.1, 0.05, 0.15, 0.35, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=9, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 5, 16, 10, 14, 15],
                                 child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
