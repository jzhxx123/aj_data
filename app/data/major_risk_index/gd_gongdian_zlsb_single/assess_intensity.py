# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     assess_intensity
   Author :       hwj
   date：          2019/10/11下午4:06
   Change Activity: 2019/10/11下午4:06
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common import assess_intensity
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gd_gongdian_zlsb_single import GLV
from app.data.major_risk_index.gongdian_zlsb.assess_intensity_sql import WORK_LOAD_SQL, KAOHE_PROBLEM_SQL, \
    ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    risk_stats_conf = get_query_condition_by_risktype(risk_name)
    check_item_ids = risk_stats_conf[0]
    check_risk_ids = risk_stats_conf[1]

    # 缺少了轨道车司机
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in risk_stats_conf[2].split(',')])

    # 司乘人员总数(作业车司机+轨道车司机+指导司机)
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL),
        DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_risk_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, check_risk_ids)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month, check_risk_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均考核问题数
def _stats_check_problem_assess_radio(months_ago):
    """考核问题数/司乘人员总数（采集信息系统中人员组织结构岗位中作业车司机+轨道车司机+指导司机)"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核问题数({3}) = '
               + '考核问题数({4})/司乘人员总数 ({5})</p>', None]
    return assess_intensity.stats_check_problem_assess_radio_type_one_major(
        ASSESS_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    """月度考核总金额（月考核表中）/司乘人员总数"""
    content = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均考核金额({3}) = '
               + '月度考核总金额({4})/ 司乘人员总数({5})</p>', None]
    return assess_intensity.stats_assess_money_per_person_type_one_major(
        ASSESS_RESPONSIBLE_MONEY,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content)


# 返奖率
def _stats_award_return_ratio(months_ago):
    """月度返奖金额/月度考核金额"""
    return assess_intensity.stats_award_return_ratio(
        AWARD_RETURN_MONEY,
        ASSESS_RESPONSIBLE_MONEY,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.45, 0.15]

    update_major_maintype_weight(index_type=14, major=risk_type, main_type=3,
                                 child_index_list=[1, 2, 3], child_index_weight=item_weight)

    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        3,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
