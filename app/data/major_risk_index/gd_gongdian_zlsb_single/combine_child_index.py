# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     combine_child_index
   Author :       hwj
   date：          2019/10/11下午4:17
   Change Activity: 2019/10/11下午4:17
-------------------------------------------------
"""
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.gd_gongdian_zlsb_single import assess_intensity, check_evenness, check_intensity, \
    problem_exposure, problem_rectification, evaluate_intensity, init_common_data
from app.data.major_risk_index.gd_gongdian_zlsb_single.common import get_vitual_major_ids
from app.data.major_risk_index.gd_gongdian_zlsb_single.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL
from app.data.util import update_major_maintype_weight


@validate_exec_month
def execute(months_ago):
    risk_name = 55
    risk_type = '工电-14'
    ids = get_vitual_major_ids(risk_type)
    init_common_data.init_func(months_ago, risk_name, risk_type)
    for func in [
        check_intensity,
        evaluate_intensity,
        assess_intensity,
        check_evenness,
        problem_exposure,
        problem_rectification,
    ]:
        func.execute(months_ago, risk_name, risk_type)

    child_index_list = [1, 2, 3, 4, 5, 6]
    child_index_weight = [0.45, 0.1, 0.15, 0.05, 0.15, 0.1]

    combine_child_index.merge_child_index(ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL,
                                          months_ago, risk_name, risk_type,
                                          child_index_list=child_index_list,
                                          child_index_weight=child_index_weight,
                                          vitual_major_ids=ids)

    update_major_maintype_weight(index_type=14, major=risk_type, child_index_list=child_index_list,
                                 child_index_weight=child_index_weight
                                 )


if __name__ == '__main__':
    pass