
# 检查问题(关联风险, 项目)
CHECK_PROBLEM_SQL = """SELECT
        MAX(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        MAX(b.LEVEL) as LEVEL,
        MAX(a.RISK_LEVEL) as RISK_LEVEL, 
        MAX(b.CHECK_SCORE) as CHECK_SCORE
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as e on a.pk_id = e.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND e.fk_risk_id IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        group by a.PK_ID
"""

# 查出的隐患问题项点(本单位基础问题库中有的)(关联风险, 项目)
EX_CHECKED_HIDDEN_PROBLEM_POINT_SQL = """SELECT DISTINCT
    c.FK_DEPARTMENT_ID, b.PK_ID as FK_PROBLEM_BASE_ID
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
        INNER JOIN
    t_check_problem_and_risk AS d ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    b.IS_HIDDEN_KEY_PROBLEM = 1
    AND a.RISK_LEVEL <= 2
        AND b.`STATUS` = 3
        AND d.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 所以这部分筛选先放进程序代码里筛选(关联风险, 项目)
EXPOSURE_PROBLEM_DEPARTMENT_SQL = """SELECT
        DISTINCT a.FK_DEPARTMENT_ID
    FROM
        t_check_problem_and_responsible_department AS a
            INNER JOIN
        t_check_problem AS b ON a.FK_CHECK_PROBLEM_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        c.FK_RISK_ID IN ({0})
        AND b.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 每月发生事故隐患问题(关联风险, 项目)
HIDDEN_KEY_PROBLEM_MONTH_SQL = """SELECT DISTINCT
        CONCAT(c.DEPARTMENT_ID, '||', b.PK_ID) AS PID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON b.PK_ID = a.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            inner join
        t_problem_base_risk as d on b.pk_id = d.FK_PROBLEM_BASE_ID
    WHERE
        b.IS_HIDDEN_KEY_PROBLEM = 1
            AND b.`STATUS` = 3
            AND b.is_delete = 0
            AND d.fk_risk_id IN ({2})
            AND a.FK_CHECK_ITEM_ID IN ({3})
            AND c.SHORT_NAME != ''
            AND c.IS_DELETE = 0
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 基础问题库里的事故隐患问题
# 不过滤问题层级，基础问题的问题所属部门默认站段级别(关联风险, 项目)
HIDDEN_KEY_PROBLEM_SQL = """SELECT DISTINCT
        CONCAT(b.DEPARTMENT_ID, '||', a.PK_ID) AS PID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
            inner join
        t_problem_base_risk as c on a.pk_id = c.FK_PROBLEM_BASE_ID
    WHERE
         a.IS_HIDDEN_KEY_PROBLEM = 1
            AND c.fk_risk_id IN ({0})
            AND a.FK_CHECK_ITEM_ID IN ({1})
            AND a.`STATUS` = 3
            AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.IS_DELETE = 0
"""

# 隐患问题项点(关联风险, 项目)
EX_HIDDEN_PROBLEM_POINT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.PK_ID as FK_PROBLEM_BASE_ID
    FROM
        t_problem_base AS a
            INNER JOIN
        t_problem_base_risk AS c ON c.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.STATUS = 3
            AND a.IS_HIDDEN_KEY_PROBLEM = 1
            AND c.FK_RISK_ID in ({0})
            AND a.FK_CHECK_ITEM_ID IN ({1})
"""

# 他查问题
# 筛选项:责任部门、是否路外:否、
# 检查方式:问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、
# 月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:是(关联风险, 项目)
OTHER_CHECK_PROBLEM_SQL = """
SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS b 
            LEFT JOIN
        t_check_info AS c ON b.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_check_problem_and_responsible_department as f on b.PK_ID = f.FK_CHECK_PROBLEM_ID
            LEFT JOIN
        t_department AS e ON f.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            LEFT JOIN
        t_check_problem_and_risk as g on b.pk_id = g.FK_CHECK_PROBLEM_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 1
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
            AND g.FK_RISK_ID IN ({0})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 安全生产信息问题(关联风险, 项目)
SAFETY_PRODUCE_INFO_SQL = """SELECT
        CONCAT(b.FK_PROBLEM_BASE_ID,
                '||',
                e.TYPE3,
                '||',
                a.RANK) AS PROBLEM_DPID_RISK,
        a.MAIN_TYPE
    FROM
        t_safety_produce_info AS a
            INNER JOIN
        t_safety_produce_info_problem_base AS b
            ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_department AS c
            ON c.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            INNER JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            INNER JOIN
        t_department AS e ON c.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            inner join
        t_problem_base_risk as f on d.pk_id = f.fk_problem_base_id
    WHERE
        a.RANK BETWEEN 1 AND 3
            AND f.fk_risk_id IN ({0})
            AND d.FK_CHECK_ITEM_ID in ({3})
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""

# 自查问题
# 筛选项:检查部门、是否路外:否、
# 问题类型:包含调车项中的一项、
# 时间标准:检查时间结束时间、月份采用:系统月、
# 检查方式:不要“转录”“职工检查”“复查调阅”、路局检查:否(关联风险, 项目)
SELF_CHECK_PROBLEM_SQL = """
SELECT
        CONCAT(d.PK_ID,
            '||',
            b.RISK_LEVEL,
            '||',
            e.TYPE3,
            '||',
            d.IS_HIDDEN_KEY_PROBLEM) AS PROBLEM_DPID_RISK
    FROM
        t_check_problem AS b 
            LEFT JOIN
        t_check_info AS c ON b.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS d ON b.FK_PROBLEM_BASE_ID = d.PK_ID
            LEFT JOIN
        t_person as f on b.CHECK_PERSON_ID_CARD = f.ID_CARD
            left join
        t_department AS e ON f.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
            left join
        t_check_problem_and_risk as g on b.pk_id = g.FK_CHECK_PROBLEM_ID
    WHERE b.RISK_LEVEL <= 3
    AND b.IS_EXTERNAL = 0
    AND b.TYPE = 3
    AND c.CHECK_WAY NOT BETWEEN 5 AND 6
            AND g.fk_risk_id IN ({0})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{2}', '%%Y-%%m-%%d')
"""