from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL,
    MEDIA_COST_TIME_SQL,
    RISK_WORK_BANZU_COUNT_SQL, YECHA_CHECK_SQL)
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL

# 所有问题数(关联风险，关联项目)
ALL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""

# 问题质量分累计(关联风险，关联项目)
PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            group by b.pk_id
"""

# 较大及以上风险问题质量分累计(关联风险，关联项目)
RISK_LEVEL_PROBLEM_SQL = """
select rlp.FK_DEPARTMENT_ID, sum(rlp.CHECK_SCORE) as COUNT
from
(
SELECT
        distinct(b.pk_id), a.FK_DEPARTMENT_ID, d.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join 
        t_person as a on b.CHECK_PERSON_ID_CARD = a.ID_CARD
        left join
        t_check_problem_and_risk as c
        ON  b.PK_ID = c.FK_CHECK_PROBLEM_ID
        left join
        t_problem_base as d 
        on b.FK_PROBLEM_BASE_ID = d.pk_id
        inner join
        t_check_info as e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            ) as rlp
    GROUP BY rlp.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计(关联风险，关联项目)
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_person as d ON b.CHECK_PERSON_ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_check_problem_and_risk as a on a.fk_check_problem_id = b.pk_id
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND a.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 作业项问题数(关联风险，关联项目)
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.FK_CHECK_ITEM_ID IN ({3})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID
"""

# 一般以及以上问题数（关联风险，关联项目)
ABOVE_YIBAN_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <= 3
    GROUP BY a.PK_ID;
"""

# 一般问题质量分累计（关联风险，关联项目)
ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND b.FK_CHECK_ITEM_ID IN ({3})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 管理项问题数（关联风险，关联项目)
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.FK_CHECK_ITEM_ID IN ({3})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY a.PK_ID
"""

# 监控调阅发现问题数（关联风险，关联项目)
MEDIA_PROBLEM_NUMBER_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
        INNER join
    t_check_problem_and_risk as d on a.PK_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID in ({2})
            AND a.FK_CHECK_ITEM_ID IN ({3})
            group by a.pk_id
"""

# 监控调阅质量分（关联风险，关联项目)
MEDIA_PROBLME_SCORE_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            left join
        t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND f.FK_RISK_ID in ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID
"""