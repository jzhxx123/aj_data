from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL)


# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE3 in {0}
    and b.is_delete = 0
    and a.is_delete = 0
"""

# 重复发生问题信息（关联风险，项目）
REPEATE_HAPPEN_PROBLEM_SQL = """
SELECT 
    MAX(a.FK_PROBLEM_BASE_ID) AS FK_PROBLEM_BASE_ID,
    MAX(a.RISK_LEVEL) AS RISK_LEVEL,
    MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
    1 AS COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_problem_and_risk AS b ON a.PK_ID = b.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_RISK_ID IN ({2})
        AND a.FK_CHECK_ITEM_ID IN ({3})
        AND a.RISK_LEVEL <=3
GROUP BY a.PK_ID
""" 

# 反复发生的同一项点问题数（关联风险，项目）
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_problem_base_risk as e on a.FK_PROBLEM_BASE_ID = e.FK_PROBLEM_BASE_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND e.FK_RISK_ID IN ({2})
        AND b.FK_CHECK_ITEM_ID IN ({3})
        AND b.IS_DELETE = 0
"""

# 超期问题数（关联风险，项目）
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_risk AS c ON c.FK_CHECK_PROBLEM_ID = a.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_RISK_ID IN ({2})
            AND d.FK_CHECK_ITEM_ID IN ({3})
    GROUP BY b.FK_DEPARTMENT_ID;
"""
