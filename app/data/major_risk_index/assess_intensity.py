#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''
from flask import current_app

from app.data.major_risk_index.assess_intensity_sql import (
    ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL)
from app.data.major_risk_index.check_intensity_sql import KAOHE_PROBLEM_SQL
from app.data.index.common import (calc_child_index_type_divide,
                                   combine_child_index_func,
                                   df_merge_with_dpid, summizet_child_index)
from app.data.major_risk_index.common_sql import (
    CHECK_PROBLEM_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL,
    ZHANDUAN_DPID_SQL)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    WORK_LOAD = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)

    # 检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        pd_query(ASSESS_RESPONSIBLE_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    # 月度返奖金额
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month, check_item_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]

    if _ratio > 0.1 or _ratio >= -0.1:
        _score = 90
    else:
        deduction = _ratio * 100
        if detail_type == 3:
            deduction *= -1
        _score = 100 + deduction
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    return calc_child_index_type_divide(
        ASSESS_PROBLEM_COUNT,
        PROBLEM_COUNT,
        2,
        3,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    return calc_child_index_type_divide(
        ASSESS_RESPONSIBLE_MONEY,
        WORK_LOAD,
        2,
        3,
        2,
        months_ago,
        'COUNT',
        'SCORE_b',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


# 返奖率
def _stats_award_return_ratio(months_ago):
    return calc_child_index_type_divide(
        AWARD_RETURN_MONEY,
        ASSESS_RESPONSIBLE_MONEY,
        2,
        3,
        3,
        months_ago,
        'COUNT',
        'SCORE_c',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.4, 0.45, 0.15]
    summizet_child_index(child_score, _choose_dpid_data, 2, 3, months_ago,
                         item_name, item_weight, risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


if __name__ == '__main__':
    pass
