# 反复发生的同一项点问题数
REPEAT_PROBLEM_SQL = """SELECT
   CONCAT(d.FK_DEPARTMENT_ID, '||', c.ID_CARD, '||', b.PK_ID) AS DIP
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_responsibility_person AS c
        ON c.FK_CHECK_PROBLEM_ID = a.PK_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS d
        ON c.FK_RESPONSIBLE_DEPARTMENT_ID = d.PK_ID
    WHERE
        b.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_problem_base AS c ON c.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_problem_base AS d ON d.PK_ID = c.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_problem_base_risk AS e ON e.FK_PROBLEM_BASE_ID = d.PK_ID
    WHERE
        e.FK_RISK_ID in ({2})
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""
