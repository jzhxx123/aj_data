#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2019/03/08
'''

from app.data.major_risk_index.jiwu_diaoche import (
    assess_intensity, check_evenness, check_intensity, evaluate_intensity,
    problem_exposure, problem_rectification)
from app.data.major_risk_index.common import combine_child_index
from app.data.index.util import validate_exec_month
from app.data.major_risk_index.common.common_sql import (CHEJIAN_DPID_SQL,
                                                         ZHANDUAN_DPID_SQL)


@validate_exec_month
def execute(months_ago):
    risk_name = 22
    risk_type = '机务-3'
    for func in [
            assess_intensity, check_evenness, check_intensity,
            problem_exposure, problem_rectification, evaluate_intensity
    ]:
        func.execute(months_ago, risk_name, risk_type)
    child_index_weight = [0.3, 0.1, 0.15, 0.1, 0.2, 0.15]
    combine_child_index.merge_child_index(
        ZHANDUAN_DPID_SQL,
        CHEJIAN_DPID_SQL,
        months_ago,
        risk_name,
        risk_type,
        child_index_weight=child_index_weight)


if __name__ == '__main__':
    pass
