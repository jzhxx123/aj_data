# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import problem_exposure

from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.problem_exposure_sql import (
    ANALYSIS_CHECK_PROBLEM_SQL, CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    diaoche = get_query_condition_by_risktype(risk_name)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    # 统计工作量【职工总人数】
    data = df_merge_with_dpid(
        pd_query(WORK_LOAD_SQL.format(diaoche_position, major)),
        DEPARTMENT_DATA)
    STAFF_NUMBER = data.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = diaoche[0]


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    return problem_exposure.stats_total_problem_exposure_type_jiwu(
        CHECK_ITEM_IDS, CHECK_PROBLEM_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def _stats_analysis_problem_exposure(months_ago):
    stats_month = get_custom_month(months_ago)
    prob_data = ANALYSIS_CHECK_PROBLEM_SQL.format(*stats_month, '漏检')
    return problem_exposure.stats_analysis_problem_exposure_type_jiwu(
        prob_data, STAFF_NUMBER, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return problem_exposure.stats_other_problem_exposure(
        CHECK_ITEM_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_analysis_problem_exposure,
        _stats_other_problem_exposure,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'g', 'e']]
    item_weight = [0.7, 0.3, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
