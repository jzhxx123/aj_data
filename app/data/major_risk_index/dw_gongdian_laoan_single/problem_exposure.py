# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import \
    BANZU_POINT_SQL
from app.data.major_risk_index.dianwu_laoan.common import (
    stats_problem_exposure, df_merge_with_dpid)
from app.data.major_risk_index.dianwu_laoan.problem_exposure_sql import (
    CHECK_PROBLEM_SQL, CHECKED_HIDDEN_PROBLEM_POINT_SQL,
    EXPOSURE_PROBLEM_DEPARTMENT_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL, HIDDEN_PROBLEM_POINT_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.major_risk_index.dw_gongdian_laoan_single import GLV
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    # 统计工作量【劳时】
    work_load = GLV.get_value('WORK_LOAD')
    STAFF_NUMBER = work_load.groupby(['TYPE3'])['COUNT'].sum()
    STAFF_NUMBER = STAFF_NUMBER.to_frame(name='PERSON_NUMBER')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    # 查出较严重隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 单位应检查问题项点
    HIDDEN_PROBLEM_POINT_DATA = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    return problem_exposure.stats_total_problem_exposure_fliter(
        CHECK_RISK_IDS, CHECK_PROBLEM_SQL, STAFF_NUMBER, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        title=[
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})', '作业项问题数({0})/工作量({1})',
            '一般及以上作业项问题数({0})/工作量({1})'
        ])


# 较严重隐患问题暴露度
def _stats_hidden_problem_exposure(months_ago):
    title = '本月问题个数: {1}个'
    return problem_exposure.stats_hidden_problem_exposure_excellent(
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA,
        DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, title=title)


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    customizededuct = {
        3: 1,
        4: 2,
        5: 3
    }
    return problem_exposure.stats_problem_exposure_excellent(
        CHECK_RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    return problem_exposure.stats_other_problem_exposure(
        CHECK_RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure, _stats_problem_exposure,
        _stats_banzu_problem_exposure, _stats_other_problem_exposure,
        _stats_hidden_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.4, 0.2, 0.2, 0.2, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=5,
                                 child_index_list=[1, 2, 8, 4, 5], child_index_weight=item_weight)

    current_app.logger.debug(
        '├── └── [dianwu_laoan]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
