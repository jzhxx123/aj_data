#!/usr/bin/python3
# -*- coding: utf-8 -*-
from flask import current_app

from app.data.index.util import (get_custom_month)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.dianwu_laoan.common import (get_vitual_major_ids, df_merge_with_dpid)
from app.data.major_risk_index.dianwu_laoan.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, ACTIVE_EVALUATE_SCORE_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL, ANALYSIS_CENTER_ASSESS_SQL,
    DUAN_CADRE_COUNT_SQL, EVALUATE_COUNT_SQL, ACCUMULATIVE_EVALUATE_SCORE_SQL,
    LUJU_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
    ZHANDUAN_EVALUATE_SCORE_SQL, PERSON_ID_CARD_SQL, GANBU_ACTIVE_EVALUATE_COUNT_SQL, GANBU_ACTIVE_EVALUATE_SCORE_SQL)
from app.data.major_risk_index.dw_gongdian_laoan_single import GLV
from app.data.major_risk_index.dw_gongdian_laoan_single.common_sql import (
    WORK_LOAD_SQL, CADRE_COUNT_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

# 需要过滤不参与基数计算的站段列表
ZHANDUAN_FILTER_LIST = ['99990002001499A20007']


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_vitual_major_ids(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global TOTAL_EVALUATE_COUNT, ACTIVE_EVALUATE_COUNT, \
        ACTIVE_EVALUATE_KEZHI_COUNT, ACTIVE_EVALUATE_SCORE, \
        CADRE_COUNT, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, GANBU_ACTIVE_EVALUATE_SCORE, \
        ANALYSIS_CENTER_ASSESS_SCORE, DUAN_CADRE_COUNT, GANBU_ACTIVE_EVALUATE_COUNT, \
        DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, RISK_IDS
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')

    # 总人数
    WORK_LOAD = df_merge_with_dpid(pd_query(WORK_LOAD_SQL.format(major)), DEPARTMENT_DATA)

    # 干部总人数
    CADRE_COUNT = df_merge_with_dpid(pd_query(CADRE_COUNT_SQL.format(major)), DEPARTMENT_DATA)

    # 评价记分总条数
    TOTAL_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 主动评价记分总条数
    ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 干部主动评价记分条数
    GANBU_ACTIVE_EVALUATE_COUNT = df_merge_with_dpid(
        pd_query(GANBU_ACTIVE_EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 主动评价记分总分数
    ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 干部主动评价记分分数
    GANBU_ACTIVE_EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(GANBU_ACTIVE_EVALUATE_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 科职及以上干部主动评价记分条数
    ACTIVE_EVALUATE_KEZHI_COUNT = df_merge_with_dpid(
        pd_query(ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # （主动）段机关干部评价记分条数
    DUAN_CADRE_COUNT = df_merge_with_dpid(
        pd_query(DUAN_CADRE_COUNT_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 主动评价记分占比
def _stats_active_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>主动评价记分占比({3}) = '
                        + '主动评价记分条数({4})/ 评价记分总条数({5})*100%</p>', None]
    # 各个站段的分数
    return evaluate_intensity.stats_active_ratio(
        ACTIVE_EVALUATE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data, zhanduan_filter_list=ZHANDUAN_FILTER_LIST, customizecontent=customizecontent)


# 干部人均主动评价记分条数
def _stats_count_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分条数({3}) = '
                        + '干部主动评价记分条数({4})/ 干部总人数({5})</p>', None]
    return evaluate_intensity.stats_count_per_person(
        GANBU_ACTIVE_EVALUATE_COUNT, CADRE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data, zhanduan_filter_list=ZHANDUAN_FILTER_LIST, customizecontent=customizecontent)


# 干部人均主动评价记分分数
def _stats_score_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>干部人均主动评价记分分数({3}) = '
                        + '主动评价记分分数({4})/ 干部总人数({5})</p>', None]
    return evaluate_intensity.stats_score_per_person_major(
        GANBU_ACTIVE_EVALUATE_SCORE,
        CADRE_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        zhanduan_filter_list=ZHANDUAN_FILTER_LIST,
        customizecontent=customizecontent)


# 评价职务占比
def _stats_gradation_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>评价职务占比({3}) = '
                        + '（主动）科职干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>', None]
    return evaluate_intensity.stats_gradation_ratio(
        ACTIVE_EVALUATE_KEZHI_COUNT, TOTAL_EVALUATE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data, zhanduan_filter_list=ZHANDUAN_FILTER_LIST, customizecontent=customizecontent)


# 段机关干部占比
def _stats_duan_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>段机关干部占比({3}) = '
                        + '（主动）段机关干部评价记分条数({4})/ 评价记分总条数({5})*100%</p>', None]
    return evaluate_intensity.stats_duan_ratio(
        DUAN_CADRE_COUNT, TOTAL_EVALUATE_COUNT, months_ago, RISK_TYPE,
        _choose_dpid_data, zhanduan_filter_list=ZHANDUAN_FILTER_LIST, customizecontent=customizecontent)


# 分析中心得分
def _stats_analysis_center_assess(months_ago):
    return evaluate_intensity.stats_analysis_center_assess(
        ANALYSIS_CENTER_ASSESS_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, score=0)


# 评价集中度
def _stats_concentartion_ratio_of_evaluation(months_ago):
    return evaluate_intensity.stats_concentartion_ratio_of_evaluation_two(
        ACCUMULATIVE_EVALUATE_SCORE_SQL, LUJU_EVALUATE_SCORE_SQL,
        ZHANDUAN_EVALUATE_SCORE_SQL, ZHANDUAN_EVALUATE_SCORE_NOT_JL2_SQL,
        PERSON_ID_CARD_SQL, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago,
        RISK_TYPE, _choose_dpid_data, RISK_IDS)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [
        _stats_score_per_person, _stats_active_ratio, _stats_count_per_person,
        _stats_gradation_ratio, _stats_duan_ratio,
        _stats_analysis_center_assess, _stats_concentartion_ratio_of_evaluation
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f', 'g']]
    item_weight = [0.4, 0.13, 0.15, 0.07, 0.05, 0.1, 0.1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)
    # # 车间
    # item_name = [f'SCORE_{x}' for x in ['b', 'c', 'd']]
    # item_weight = [0.4, 0.4, 0.2]
    # summizet_child_index(child_score, _choose_dpid_data, 2, 2, months_ago,
    #                      item_name, item_weight, [4])

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=2,
                                 child_index_list=[1, 2, 3, 4, 5, 6, 7], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
