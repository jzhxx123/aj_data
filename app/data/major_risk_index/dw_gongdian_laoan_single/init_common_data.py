# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/9/18上午11:21
   Change Activity: 2019/9/18上午11:21
-------------------------------------------------
"""
from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.init_common_data_sql import CHECK_INFO_ASSOCIATE_ITEM_SQL, \
    CHECK_INFO_MEDIA_TIME_SQL, CHECK_INFO_ASSOCIATE_PERSON_SQL, CHECK_INFO_DEPARTMENT_ADDRESS_SQL, \
    CHECK_INFO_POINT_ADDRESS_SQL
from app.data.major_risk_index.dianwu_laoan.common import get_vitual_major_ids, calc_work_load, get_all_check_item_ids
from app.data.major_risk_index.dw_gongdian_laoan_single import GLV
from app.data.major_risk_index.dw_gongdian_laoan_single.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, \
    DEPARTMENT_SQL, WORK_HOUR_SQL, WORK_HOUR_CTSC_SQL, ALL_CHECK_ITEM_IDS_SQL
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """

    print(__package__)
    major = get_major_dpid('电务-5')
    ids = get_vitual_major_ids("工电-1")
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(ids))
    department_data = pd_query(DEPARTMENT_SQL.format(ids))
    risktype_data = get_query_condition_by_risktype(risk_name)
    all_check_item_ids = pd_query(ALL_CHECK_ITEM_IDS_SQL.format(major))
    check_item_ids = get_all_check_item_ids(all_check_item_ids)

    check_risk_ids = risktype_data[1]
    work_load = calc_work_load(
        pd_query(WORK_HOUR_SQL.format(*stats_month), db_name='db_mid'),
        pd_query(WORK_HOUR_CTSC_SQL.format(*stats_month), db_name='db_mid'),
        department_data)

    # 子数计算基础数据
    check_info = pd_query(CHECK_INFO_ASSOCIATE_ITEM_SQL.format(*stats_month, check_item_ids))
    check_info_media_time = pd_query(CHECK_INFO_MEDIA_TIME_SQL.format(*stats_month, check_item_ids))
    check_info_person = pd_query(CHECK_INFO_ASSOCIATE_PERSON_SQL.format(*stats_month))
    check_info_department_address = pd_query(CHECK_INFO_DEPARTMENT_ADDRESS_SQL.format(*stats_month))
    check_info_point_address = pd_query(CHECK_INFO_POINT_ADDRESS_SQL.format(*stats_month))

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'WORK_LOAD': work_load,
        'CHECK_ITEM_IDS': check_item_ids,
        'CHECK_RISK_IDS': check_risk_ids,
        'CHECK_INFO': check_info,
        'CHECK_INFO_PERSON': check_info_person,
        'CHECK_INFO_MEDIA_TIME': check_info_media_time,
        'CHECK_INFO_DEPARTMENT_ADDRESS': check_info_department_address,
        'CHECK_INFO_POINT_ADDRESS': check_info_point_address
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
