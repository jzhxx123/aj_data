# -*- coding: utf-8 -*-

from flask import current_app

from app.data.index.util import get_custom_month
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.dianwu_laoan.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_POINT_SQL, CHECK_PROBLEM_SQL,
    RISK_LEVEL_PROBLEM_SQL, KAOHE_PROBLEM_SQL, PROBLEM_CHECK_SCORE_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, YIBAN_RISK_PROBLEM_NUMBER_SQL)
from app.data.major_risk_index.dianwu_laoan.common import get_check_count, get_check_address_ratio_basic_data, \
    get_media_intensity_basic_data, df_merge_with_dpid
from app.data.major_risk_index.dw_gongdian_laoan_single import GLV
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global WORK_LOAD, CHECK_COUNT, PROBLEM_SCORE, YECHA_COUNT, \
        JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, ASSESS_PROBLEM_COUNT, \
        YIBAN_RISK_PROBLEM_NUMBER, ALL_PROBLEM_NUMBER, RISK_IDS, \
        BANZU_POINT, CHECK_POINT, REAL_CHECK_BANZU, REAL_CHECK_POINT, MEDIA_FUNC_DATA_DICT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    check_info = GLV.get_value('CHECK_INFO')
    check_info_person = GLV.get_value('CHECK_INFO_PERSON')
    check_info_media_time = GLV.get_value('CHECK_INFO_MEDIA_TIME')
    check_info_department_address = GLV.get_value('CHECK_INFO_DEPARTMENT_ADDRESS')
    check_info_point_address = GLV.get_value('CHECK_INFO_POINT_ADDRESS')
    stats_month = get_custom_month(months_ago)
    # 统计工作量【劳时】
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 获取现场检查总次数,夜查次数,跟班次数
    spot_check_count = get_check_count(check_info, check_info_person)
    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(spot_check_count, DEPARTMENT_DATA)
    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)
    # 问题总数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 一般风险问题数
    YIBAN_RISK_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(YIBAN_RISK_PROBLEM_NUMBER_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)
    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, RISK_IDS)), DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(*stats_month, RISK_IDS)),
        DEPARTMENT_DATA)

    # 覆盖率
    basic_address_data = get_check_address_ratio_basic_data(
        BANZU_POINT_SQL, CHECK_POINT_SQL, check_info,
        check_info_department_address, check_info_point_address)
    BANZU_POINT, CHECK_POINT, REAL_CHECK_BANZU, REAL_CHECK_POINT = basic_address_data

    # 监控调阅力度
    MEDIA_FUNC_DATA_DICT = get_media_intensity_basic_data(stats_month, check_item_ids, RISK_IDS, check_info,
                                                          check_info_person, check_info_media_time,
                                                          check_info_department_address,
                                                          DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = '
                        + '量化人员及干部检查次数({4})/ 劳时({5})</p>', None]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 查处问题率
def _stats_check_problem_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = '
                        + '劳安问题数({4})/ 劳时({5})</p>', None]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        ALL_PROBLEM_NUMBER,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题考核率({3}) = '
                        + '考核问题数({4})/ 发现问题数({5})</p>', None]
    return check_intensity.stats_check_problem_assess_radio_type_one(
        ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    title = ['较大和重大安全风险问题质量分累计({0})/劳时({1})',
             '现场检查发现较大和重大安全风险问题质量分累计({0})/劳时({1})']
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        title=title)


# 质量均分
def _stats_score_per_person(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>质量均分({3}) = '
                        + '问题质量分累计({4})/ 劳时({5})</p>', None]
    return check_intensity.stats_score_per_person_filter(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 一般以上风险占比--部分专业基数
def _stats_yiban_risk_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>一般及以上风险问题占比({3}) = '
                        + '一般及以上风险问题数({4})/ 问题总数({5})*100%</p>', None]
    return check_intensity.stats_yiban_risk_ratio_type_one(
        YIBAN_RISK_PROBLEM_NUMBER,
        ALL_PROBLEM_NUMBER,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>覆盖率({3}) = '
                        + '检查地点数({4})/ 地点总数({5})*100%</p>', None]
    return check_intensity.stats_check_address_ratio_include_check_point_excellent(
        REAL_CHECK_BANZU,
        REAL_CHECK_POINT,
        BANZU_POINT,
        CHECK_POINT,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=customizecontent)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    title = ['监控调阅时长累计({0})/工作量({1})',
             '监控调阅发现问题数({0})/工作量({1})',
             '监控调阅发现问题质量分累计({0})/工作量({1})',
             '调阅班组数({0})/班组数({1})']

    return check_intensity.stats_media_intensity_excellent(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        title=title,
        choose_dpid_data=_choose_dpid_data,
        media_func_data_dict=MEDIA_FUNC_DATA_DICT,
    )


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person, _stats_check_problem_ratio,
        _stats_check_problem_assess_radio, _stats_score_per_person,
        _stats_risk_score_per_person, _stats_media_intensity,
        _stats_yiban_risk_ratio, _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'd', 'e', 'f', 'h', 'i', 'j']
    ]
    item_weight = [0.25, 0.2, 0.1, 0.1, 0.1, 0.05, 0.05, 0.15]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=1,
                                 child_index_list=[2, 3, 4, 17, 6, 8, 9, 10], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
