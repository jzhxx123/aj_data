# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, if(b.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND a.SHORT_NAME != ""
            AND a.TYPE3 in {0}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        if(c.NAME !='',"工电","工电") AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND a.TYPE3 in {0}
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND b.TYPE3 in {0}
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""


# 电务信号三级施工 T_CONSTRUCT_THREE_SELECT_CONTROL
WORK_HOUR_CTSC_SQL = """SELECT
    a.DEPARTMENT_ID,
    a.REALITY_ADMINISTRATOR,
    a.REALITY_PRINCIPAL,
    a.REALITY_TESTER,
    a.INDOOR_PERSON,
    a.OUTDOOR_PERSON,
    TIMESTAMPDIFF(MINUTE,CONCAT(b.WORK_DATE,' ', b.WORK_START_TIME),
    CONCAT(b.END_WORK_DATE, ' ', b.WORK_END_TIME))/ 60 as HOURS
FROM
    dw_t_construct_three_select_control AS a
        INNER JOIN
    dw_t_dispatching_bill as b on a.DISPATCHINGBILL_ID=b.PK_ID
WHERE
    DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.STATUS = 2;
"""

# 电务生产人数工时
WORK_HOUR_SQL = """SELECT
        DEPARTMENT_ID,
        WORKER_COUNT AS PERSON_COUNT,
        REALITY_TIME / 60 AS HOURS
    FROM
        dw_t_daily_maintain_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION ALL SELECT
        DEPARTMENT_ID,
        INPLACE_WORKER_COUNT AS PERSON_COUNT,
        (INDOOR_WORK_TIME + OUT_WORK_TIME) / 60 AS HOURS
    FROM
        dw_t_concentrate_overhaul_select_control
    WHERE
        DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND STATUS = 2
    UNION ALL SELECT
        a.DEPARTMENT_ID,
        a.PERSON_CONTENT AS PERSON_COUNT,
        TIMESTAMPDIFF(MINUTE,CONCAT(b.WORK_DATE,' ', b.WORK_START_TIME),
        CONCAT(b.END_WORK_DATE, ' ', b.WORK_END_TIME))/ 60 as HOURS
    FROM
        dw_t_cooperate_work_select_control as a 
        INNER JOIN
        dw_t_dispatching_bill as b on a.DISPATCHINGBILL_ID=b.PK_ID
    WHERE
        DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.STATUS = 2
    UNION ALL SELECT
        a.DEPARTMENT_ID,
        a.PERSON_UP_COUNT AS PERSON_COUNT,
        TIMESTAMPDIFF(MINUTE,CONCAT(b.WORK_DATE,' ', b.WORK_START_TIME),
        CONCAT(b.END_WORK_DATE, ' ', b.WORK_END_TIME))/ 60 as HOURS
    FROM
        dw_t_status_update_select_control as a
        INNER JOIN
        dw_t_dispatching_bill as b on a.DISPATCHINGBILL_ID=b.PK_ID
    WHERE
        DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
         < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.STATUS = 2
    UNION ALL SELECT
        a.DEPARTMENT_ID,
        a.PERSON_CONTENT AS PERSON_COUNT,
        TIMESTAMPDIFF(MINUTE,CONCAT(b.WORK_DATE,' ', b.WORK_START_TIME),
        CONCAT(b.END_WORK_DATE, ' ', b.WORK_END_TIME))/ 60 as HOURS
    FROM
        dw_t_temp_select_control as a
                INNER JOIN
        dw_t_dispatching_bill as b on a.DISPATCHINGBILL_ID=b.PK_ID
    WHERE
        DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.WORK_DATE, '%%Y-%%m-%%d')
        < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.STATUS = 2;
"""

# 获取电务专业及路局的所有检查项目
ALL_CHECK_ITEM_IDS_SQL = """SELECT
    b.PK_ID 
    FROM
    `t_department` AS a
    INNER JOIN t_check_item AS b ON ( a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID ) 
    WHERE
    (
    a.TYPE2 = '{0}' 
    OR a.DEPARTMENT_ID IN ( SELECT DEPARTMENT_ID FROM t_department WHERE HIERARCHY = 1 AND IS_DELETE = 0 )
    ) 
    AND a.IS_DELETE = 0 
    AND b.IS_DELETE =0
"""