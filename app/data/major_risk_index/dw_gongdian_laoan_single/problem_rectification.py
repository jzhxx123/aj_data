# -*- coding: utf-8 -*-
"""""
    问题整改效果指数。从超期整改、履职评价、反复发生方面分析。
"""
from flask import current_app

from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import REPEATE_HAPPEN_PROBLEM_SQL
from app.data.major_risk_index.dianwu_laoan.common import get_vitual_major_ids, df_merge_with_dpid
from app.data.major_risk_index.dianwu_laoan.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, CHECKED_PERIL_ID_SQL,
    HAPPEN_PROBLEM_POINT_SQL, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL, PERIL_COUNT_SQL, PERIL_ID_SQL,
    PERIL_OVERDUE_COUNT_SQL, PERIL_PERIOD_COUNT_SQL,
    PERIL_RECTIFY_NO_ENTRY_SQL, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, MAJOR_PROBLEM_POINT_INFO_SQL)
from app.data.major_risk_index.dw_gongdian_laoan_single import GLV
from app.data.major_risk_index.dw_gongdian_laoan_single.common_sql import (
    WORK_LOAD_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


# 需要过滤不参与基数计算的站段列表


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    ids = get_vitual_major_ids(risk_type)
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, WORK_LOAD, \
        STAFF_NUMBER, WORKER_COUNT, PROBLEM_POINT_INFO_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = GLV.get_value('CHECK_ITEM_IDS')
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    # 正式职工人数
    WORKER_COUNT = pd_query(WORK_LOAD_SQL.format(ids))
    PROBLEM_POINT_INFO_DATA = pd_query(MAJOR_PROBLEM_POINT_INFO_SQL.format(major, ids))
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(WORKER_COUNT, DEPARTMENT_DATA)

    WORK_LOAD = STAFF_NUMBER.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')
    current_app.logger.debug('|   └── extract data from mysql have done!')


# 超期问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    return problem_rectification.stats_check_evaluate(
        CHECK_RISK_IDS, CHECK_EVALUATE_SZ_SCORE_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 问题控制
def _stats_repeatedly_index(months_ago):
    # return problem_rectification.stats_repeatedly_index(
    #     CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
    #     ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)
    problem_ctl_threshold_dict = {
        1: 2,
        2: 5,
        3: 20}
    return problem_rectification.stats_repeatedly_index_excellent(
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, CHECK_RISK_IDS, _choose_dpid_data,
        WORKER_COUNT, REPEATE_HAPPEN_PROBLEM_SQL, PROBLEM_POINT_INFO_DATA,
        problem_ctl_threshold_dict=problem_ctl_threshold_dict)


# 整改复查
def _stats_rectification_review(months_ago):
    customizecontent = ['<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>整改复查({3}) = '
                        + '库内问题复查数({4})/ 总人数({5})</p>', None]
    return problem_rectification.stats_rectification_review(
        CHECK_RISK_IDS, STAFF_NUMBER, IMPORTANT_PROBLEM_RECHECK_COUNT_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data,
        customizecontent=customizecontent)


# 隐患整治
def _stats_peril_renovation(months_ago):
    return problem_rectification.stats_peril_renovation_two(
        PERIL_COUNT_SQL, PERIL_OVERDUE_COUNT_SQL, PERIL_PERIOD_COUNT_SQL,
        CHECKED_PERIL_ID_SQL, PERIL_ID_SQL, PERIL_RECTIFY_NO_ENTRY_SQL,
        WORK_LOAD, DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data, CHECK_RISK_IDS)


# 整改成效
def _stats_rectification_effect(months_ago):
    return problem_rectification.stats_rectification_effect_two(
        CHECK_ITEM_IDS, RESPONSIBE_SAFETY_PRODUCE_INFO_SQL, WARNING_DELAY_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index,
        _stats_rectification_review,
        _stats_peril_renovation,
        _stats_rectification_effect
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e', 'f']]
    item_weight = [0.1, 0.2, 0.3, 0.15, 0.25, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3, 4, 5, 6], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
