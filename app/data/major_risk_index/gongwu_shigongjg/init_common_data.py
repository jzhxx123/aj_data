#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gongwu_shigongjg import GLV
from app.data.major_risk_index.gongwu_shigongjg.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    CHECK_COUNT_SQL
)
from app.data.major_risk_index.util import (df_merge_with_dpid)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month, get_query_condition_by_risktype)


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    diaoche = get_query_condition_by_risktype(risk_name)
    check_item_ids = diaoche[0]
    
    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # 施工监管总人时 TODO 暂时用检查次数代替
    SHIGONGJG_LABORTIME = pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids))

    # 施工监管工作量 TODO 暂时用检查次数代替
    SHIGONGJG_WORK_LOAD = df_merge_with_dpid(
    pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids)),
    DEPARTMENT_DATA)

    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "SHIGONGJG_WORK_LOAD": SHIGONGJG_WORK_LOAD,
        "SHIGONGJG_LABORTIME": SHIGONGJG_LABORTIME,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
