# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
"""

# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0 
        AND b.TYPE2 = '{0}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    a.FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4 
"""

# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 量化人员数量,去除各项指标（包括基础指标和细化指标）都为零的
QUANTIZATION_PERSON_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_quantization_base_quota AS a
            INNER JOIN
        t_person AS b ON b.ID_CARD = a.ID_CARD
    WHERE
        a.YEAR = {0} AND a.MONTH = {1}
            AND a.`STATUS` = 1
            AND b.POSITION IN ({2})
            AND (a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_base_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(CHECK_TIMES_TOTAL, 0)
                    + IFNULL(PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_NUMBER_TOTAL, 0)
                    + IFNULL(WORK_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_PROBLEM_NUMBER_TOTAL, 0)
                    + IFNULL(CHECK_NOTIFICATION_TIMES_TOTAL, 0)
                    + IFNULL(MIN_QUALITY_GRADES_TOTAL, 0)
                    + IFNULL(HIDDEN_DANGER_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(RISK_RECHECK_TIMES_TOTAL, 0)
                    + IFNULL(IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL, 0) 
                    ) > 0
                )
            OR a.ID_CARD IN (SELECT
                ID_CARD
            FROM
                t_quantization_refinement_quota
            WHERE
                YEAR = {0} AND MONTH = {1}
                    AND `STATUS` = 1
                    AND (IFNULL(NUMBER_TOTAL, 0)
                    + IFNULL(MONITOR_TIME_TOTAL, 0)
                    ) > 0
                )
            )
    GROUP BY a.FK_DEPARTMENT_ID
"""
