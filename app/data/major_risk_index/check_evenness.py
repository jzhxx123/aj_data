#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    date: 2018/07/31
    desc: 考核力度指数
'''

import calendar

import pandas as pd
from flask import current_app

from app.data.major_risk_index.check_evenness_sql import (
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, CHECK_BANZU_COUNT_SQL,
    CHECK_POINT_CHECKED_COUNT_SQL, CHECK_POINT_COUNT_SQL,
    DAILY_CHECK_BANZU_COUNT_SQL, DAILY_CHECK_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_divide,
    calc_child_index_type_sum, combine_child_index_func, df_merge_with_dpid,
    summizet_child_index, summizet_operation_set)
from app.data.major_risk_index.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.util import pd_query


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


# 计算检查点扣分
def _calc_score_by_formula(row, column, major_column):
    if row[column] == 0:
        return -5
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


# 判断是否是周六或者周日
def _is_weekends(year, month, day):
    if day >= current_app.config.get('UPDATE_DAY'):
        if month == 1:
            month = 12
            year -= 1
        else:
            month -= 1
    if calendar.weekday(year, month, day) > 4:
        return True
    else:
        return False


def _get_daily_check_banzu_count(year, month, work_type_1, work_type_2,
                                 work_type_3, day):
    work_banzu = (work_type_1 + work_type_2 + work_type_3)
    if _is_weekends(year, month, int(day)):
        work_banzu -= work_type_1
    return work_banzu


def _cal_check_banzu_evenness_score(row, columns):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]

    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    score = [100]
    for day in columns:
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
        elif ratio <= -0.5:
            daily_deduction = -2
        elif ratio <= -0.2:
            daily_deduction = -1
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    return total_score


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT

    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_ITEM_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_ITEM_IDS)), DEPARTMENT_DATA)


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    rst_index_score = calc_child_index_type_divide(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT,
        2,
        4,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        lambda x: min(100, x * 100),
        _choose_dpid_data,
        is_calc_score_base_major=False,
        risk_type=RISK_TYPE)
    return rst_index_score


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    rst_index_score = []
    for hierarchy in [3]:
        # 每日检查班组数
        data = df_merge_with_dpid(
            pd_query(DAILY_CHECK_BANZU_COUNT_SQL), DEPARTMENT_DATA)
        if data.empty:
            continue
        xdata = data.groupby([f'TYPE{hierarchy}', 'WORK_TYPE']).size()
        xdata = xdata.unstack()
        for x in [1, 2, 3]:
            if x not in xdata.columns.values.tolist():
                xdata[x] = 0
        xdata.dropna(how='all', subset=[1, 2, 3], inplace=True)
        xdata.rename(
            columns={
                1: 'COUNT_1',
                2: 'COUNT_2',
                3: 'COUNT_3'
            }, inplace=True)
        xdata = xdata.fillna(0)
        # 每日检查数
        data_check = df_merge_with_dpid(
            pd_query(
                DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
            DEPARTMENT_DATA)
        if data_check.empty:
            continue
        xdata_check = data_check.groupby([f'TYPE{hierarchy}',
                                          'DAY'])['COUNT'].sum()
        xdata_check = xdata_check.unstack()
        xdata_check = xdata_check.fillna(0)

        columns = xdata_check.columns.values
        xdata = pd.merge(
            xdata, xdata_check, how='left', left_index=True, right_index=True)
        xdata = append_major_column_to_df(ZHANDUAN_DPID_DATA, xdata)
        for day in columns:
            new_column = f'daily_check_banzu_count_{day}'
            xdata[new_column] = xdata.apply(
                lambda row: _get_daily_check_banzu_count(
                    year, month,
                    row['COUNT_1'],
                    row['COUNT_2'],
                    row['COUNT_3'],
                    day),
                axis=1)
        major_banzu_count = xdata.groupby(['MAJOR']).sum()
        xdata = pd.merge(
            xdata,
            major_banzu_count,
            how='left',
            left_on='MAJOR',
            right_index=True)
        column = f'SCORE_b_{hierarchy}'
        xdata[column] = xdata.apply(
            lambda row: _cal_check_banzu_evenness_score(row, columns), axis=1)
        xdata.drop(['MAJOR'], inplace=True, axis=1)
        xdata = pd.DataFrame(
            index=xdata['DEPARTMENT_ID'],
            data=xdata.loc[:, column].values,
            columns=[column])
        summizet_operation_set(
            xdata,
            _choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            4,
            2,
            months_ago,
            risk_type=RISK_TYPE)
        rst_index_score.append(xdata)
    return rst_index_score


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    stats_month = get_custom_month(months_ago)
    # 重要检查点受检次数
    data_check_point = pd.merge(
        pd_query(CHECK_POINT_COUNT_SQL),
        pd_query(CHECK_POINT_CHECKED_COUNT_SQL.format(*stats_month)),
        how='left',
        left_on="CHECK_POINT_ID",
        right_on="FK_CHECK_POINT_ID")
    data_check_point.drop(
        ["CHECK_POINT_ID", "FK_CHECK_POINT_ID"], inplace=True, axis=1)
    data_check_point.fillna(0, inplace=True)
    data_check_point = df_merge_with_dpid(
        data_check_point, DEPARTMENT_DATA, how='left')
    data_check_point.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)
    # 班组受检次数
    data_check_banzu = pd.merge(
        pd_query(CHECK_BANZU_COUNT_SQL),
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQL.format(*stats_month)),
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='FK_DEPARTMENT_ID')
    data_check_banzu.drop(["DEPARTMENT_ID"], inplace=True, axis=1)
    data_check_banzu.fillna(0, inplace=True)
    data_check_banzu = df_merge_with_dpid(
        data_check_banzu, DEPARTMENT_DATA, how='left')
    data_check_banzu.drop(
        ["DEPARTMENT_ID", "NAME", "TYPE"], inplace=True, axis=1)

    # 合并
    data = pd.concat([data_check_banzu, data_check_point], axis=0, sort=False)
    data = pd.merge(
        ZHANDUAN_DPID_DATA,
        data,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='TYPE3')
    data.drop(
        [
            "DEPARTMENT_ID",
        ], inplace=True, axis=1)
    # 本月各专业的平均分
    major_avg_score = data[data['COUNT'] > 0].groupby(
        ['MAJOR'])['COUNT'].mean()
    data = pd.merge(
        data,
        major_avg_score.to_frame(name='AVG_COUNT'),
        how='left',
        left_on='MAJOR',
        right_index=True)
    data = data.fillna(0)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'COUNT', 'AVG_COUNT'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_index_score


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
