#!/usr/bin/python3
# -*- coding: utf-8 -*-


from flask import current_app
from app.data.major_risk_index.gongwu_shigong import GLV
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.check_evenness_sql import (
    CHECK_BANZU_COUNT_SQL,
    DAILY_CHECK_COUNT_SQL)
from app.data.major_risk_index.common_diff_risk_and_item.check_evenness_sql import (
    RISK_ABOVE_PROBLEM_POINT_COUNT_SQL, BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST,
    EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.gongwu_shigong.check_evenness_sql import (
    DAILY_CHECK_HOUR_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.util import pd_query, update_major_maintype_weight
import pandas as pd


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _cal_check_banzu_evenness_score(row, columns):
    """[每日检查数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    score = [100]
    for day in columns:
        major_check_banzu_count = row[f'daily_check_banzu_count_{day}_y']
        if major_check_banzu_count == 0:
            continue
        major_avg = row[f'{day}_y'] / major_check_banzu_count
        # 线上由于数据不全失败
        if major_avg == 0:
            continue
        zhanduan_check_banzu_count = row[f'daily_check_banzu_count_{day}_x']
        if zhanduan_check_banzu_count == 0:
            continue
        zhanduan_avg = row[f'{day}_x'] / zhanduan_check_banzu_count
        ratio = (zhanduan_avg - major_avg) / major_avg
        if ratio <= -1:
            daily_deduction = -3
        elif ratio <= -0.5:
            daily_deduction = -2
        elif ratio <= -0.2:
            daily_deduction = -1
        else:
            daily_deduction = 0
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    return total_score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, WORK_LOAD, \
        CHECK_BANZU_COUNT_DATA, DAILY_CHECK_COUNT_DATA, \
        BANZU_DEPARTMENT_CHECKED_COUNT_DATA, DAILY_CHECK_HOUR_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, RISK_IDS
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    RISK_IDS = diaoche[1]

    # 一般以上项点问题数(符合施工、天窗修、点外修检查项目的问题)
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            RISK_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数(符合施工、天窗修、点外修检查项目的问题)
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                RISK_IDS)), DEPARTMENT_DATA)

    # 统计工作量【人时+派单数】
    WORK_LOAD = GLV.get_value('SHIGONG_WORK_LOAD')

    CHECK_BANZU_COUNT_DATA = pd_query(CHECK_BANZU_COUNT_SQL.format(RISK_IDS))

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = pd.merge(
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[0].format(major)),
        pd_query(BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST[1].format(*stats_month, CHECK_ITEM_IDS)),
        how='inner',
        left_on='FK_CHECK_INFO_ID',
        right_on='PK_ID'
    )

    BANZU_DEPARTMENT_CHECKED_COUNT_DATA.drop(
        ["PK_ID", "FK_CHECK_INFO_ID"], inplace=True, axis=1)
    BANZU_DEPARTMENT_CHECKED_COUNT_DATA = BANZU_DEPARTMENT_CHECKED_COUNT_DATA.groupby(
        ['DEPARTMENT_ID'])['COUNT'].sum().reset_index()

    DAILY_CHECK_COUNT_DATA = df_merge_with_dpid(
        pd_query(DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    DAILY_CHECK_HOUR_DATA = df_merge_with_dpid(
        pd_query(DAILY_CHECK_HOUR_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)


def _calc_time_evenness_compare_value(months_ago):
    """
    计算检查时间均衡度额基数
    基数=检查总人次/汇总每日人时
    :param months_ago:
    :return:
    """
    daily_check_count = DAILY_CHECK_COUNT_DATA
    # 统计每日检查数
    daily_check_count = daily_check_count.groupby(['DAY'])['COUNT'].sum().reset_index()
    # 汇总每日人时
    labortime_sum = DAILY_CHECK_HOUR_DATA
    labortime_sum = labortime_sum['HOURS'].sum()
    check_sum = daily_check_count['COUNT'].sum()
    # 每日基数
    daily_check_count['AVG_NUMBER'] = check_sum / labortime_sum
    return daily_check_count


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """
    查出一般以上问题项点数/基础问题库中一般以上问题项点数（符合施工、天窗修、点外修检查项目的问题）。得分=占比*100
    :param months_ago:
    :return:
    """
    customizecontent = ["<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>问题查处均衡度({3}) = " +
                        "查出一般以上问题项点数({4})/ 基础问题库中一般以上问题项点数({5})</p>", None]
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data,
        customizecontent=customizecontent)


# 每日班组数改为当日人时
# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    """
    检查日期（按每日来考虑工作量）：每日检查人次数/当日工作班组数，基数=检查总人次/汇总每日作业班组数。
    低于基数20%的扣1分/日，低于50%的扣2分/日，低于100%的扣3分/日，得分=100-扣分
    :param months_ago:
    :return:
    """
    base_data = _calc_time_evenness_compare_value(months_ago)
    return check_evenness.stats_check_time_evenness_labortime_excellent(
        DAILY_CHECK_HOUR_DATA, DAILY_CHECK_COUNT_DATA,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, basedata=base_data)


# 基数的计算:每个班组当月工作量,工作量=作业人数*作业时间、占比60%,次数=派工单数量、占比40%
# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    return check_evenness.stats_check_address_evenness_work_load_ex(
        CHECK_BANZU_COUNT_DATA, BANZU_DEPARTMENT_CHECKED_COUNT_DATA,
        WORK_LOAD, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, 
        months_ago, RISK_TYPE, _choose_dpid_data,
        is_deduct=False)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness, _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    update_major_maintype_weight(index_type=7, major=risk_type, main_type=4,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
