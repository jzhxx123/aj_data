# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/9/23上午11:56
   Change Activity: 2019/9/23上午11:56
-------------------------------------------------
"""

import pandas as pd

from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_score_by_formula(row, column, major_column):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.3:
        _score = 100
    elif 0.3 > _ratio > 0.2:
        _score = 90
    elif 0.2 >= _ratio > -0.1:
        _score = 60
    elif -0.3 >= _ratio:
        _score = 0
    return _score


def calc_monitoring_problem_count(df_monitoring, zhanduan_dpid):
    """
    微机监测问题数量
    """
    if df_monitoring.empty:
        zhanduan_dpid['COUNT'] = 0
        zhanduan_dpid['MIDDLE_1'] = 0
        zhanduan_dpid['MIDDLE_2'] = 0
        zhanduan_dpid['MIDDLE_3'] = 0
        df_monitoring = zhanduan_dpid
    else:
        # 车间关联站段
        df_monitoring['zhanduan_name'] = df_monitoring.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_monitoring.dropna(subset=['zhanduan_name'], inplace=True)
        df_monitoring = df_monitoring.set_index('zhanduan_name')
        df_monitoring.drop(columns=['WORK_SHOP'], inplace=True)

        # 填充缺失数据并将str转换int
        df_monitoring.fillna(0, inplace=True)
        df_monitoring = df_monitoring.replace('', 0).astype(int)
        # df_monitoring[df_monitoring > 0] = 1
        # 统计总问题数
        df_monitoring = df_monitoring.groupby([df_monitoring.index]).sum()
        df_monitoring['COUNT'] = df_monitoring.apply(
            lambda row: sum(row), axis=1
        )
        df_monitoring = pd.merge(
            df_monitoring,
            zhanduan_dpid,
            how='right',
            left_index=True,
            right_on='NAME'
        )
        df_monitoring['MIDDLE_1'] = df_monitoring.apply(
            lambda row: row['CHEZHAN'] + row['XINHAO'], axis=1
        )
        df_monitoring['MIDDLE_2'] = df_monitoring.apply(
            lambda row: row['KADAOZHA'] + row['KASHA'] + row['QITAYIWU'], axis=1
        )
        df_monitoring['MIDDLE_3'] = df_monitoring.apply(
            lambda row: (row['JIANGUIFEIBIAN'] + row['JIANGUISHANGQIAO'] + row['JIANGUIPAXING_CUANDONG']
                         + row['JIANGUIBIANXING'] + row['GENBULUOSIGUOJIN'] + row['GUNLUN']
                         + row['DINGTIEDINGGUIYAO'] + row['KUANGJIAKUANGDONG'] + row['QITA']), axis=1
        )
    df_monitoring.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return df_monitoring


def calc_problem_exposure_basic_data(
        monitoring_problem_count,
        cadre_check_problem,
        choose_dpid_data):
    """
    获取隐患排查力度指数--问题暴露度指数的基础数据
    :param monitoring_problem_count: 微机监测问题数量
    :param cadre_check_problem: 干部检查问题数
    :param choose_dpid_data: 部门数据
    :return:
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    basic_data = pd.merge(
        monitoring_problem_count,
        cadre_check_problem,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    basic_data.fillna(0, inplace=True)

    basic_data['SCORE_1'] = basic_data.apply(
        lambda row: max((row['MIDDLE_1'] / row['COUNT'] - row['MIDDLE_1_PROBLEM'] / row['ALL_PROBLEM']) * 100, 0)
        if row['COUNT'] > 0 and row['ALL_PROBLEM'] > 0 else 0, axis=1
    )
    basic_data['SCORE_2'] = basic_data.apply(
        lambda row: max((row['MIDDLE_2'] / row['COUNT'] - row['MIDDLE_2_PROBLEM'] / row['ALL_PROBLEM']) * 100, 0)
        if row['COUNT'] > 0 and row['ALL_PROBLEM'] > 0 else 0, axis=1
    )
    basic_data['SCORE_3'] = basic_data.apply(
        lambda row: max((row['MIDDLE_3'] / row['COUNT'] - row['MIDDLE_3_PROBLEM'] / row['ALL_PROBLEM']) * 100, 0)
        if row['COUNT'] > 0 and row['ALL_PROBLEM'] > 0 else 0, axis=1
    )

    tmp = """
    滑床板清扫不良({8:.2f}) = (滑床板清扫不良报警总数({1:.0f}) / 道岔报警总数({0:.0f})
     - 滑床板清扫不良问题数({4:.0f}) / 干部检查问题总数({7:.0f}))*100<br/>
    异物卡阻报警({9:.2f})  = (异物卡阻报警总数({2:.0f}) / 道岔报警总数({0:.0f})
     - 异物卡阻问题数({5:.0f}) / 干部检查问题总数({7:.0f}))*100<br/>
    工务结合部（转辙设备）({10:.2f})  = (工务结合部报警总数({3:.0f}) / 道岔报警总数({0:.0f})
     - 工务结合部问题数({6:.0f}) / 干部检查问题总数({7:.0f}))*100
    """
    basic_data['CONTENT'] = basic_data.apply(
        lambda row: tmp.format(
            row['COUNT'], row['MIDDLE_1'], row['MIDDLE_2'], row['MIDDLE_3'],
            row['MIDDLE_1_PROBLEM'], row['MIDDLE_2_PROBLEM'], row['MIDDLE_3_PROBLEM'],
            row['ALL_PROBLEM'], row['SCORE_1'], row['SCORE_2'], row['SCORE_3'],), axis=1
    )
    basic_data['SCORE_a_3'] = basic_data.apply(
        lambda row: max(100 - row['SCORE_1'] - row['SCORE_2'] - row['SCORE_3'], 0)
        , axis=1
    )
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=basic_data['TYPE3'],
            data=basic_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_data.groupby(['TYPE3'])['SCORE_a_3'].sum().to_frame()
    return calc_df_data, df_rst
