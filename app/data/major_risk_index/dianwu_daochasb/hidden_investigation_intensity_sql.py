# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     investigation_intensity_sql
   Author :       hwj
   date：          2019/9/23下午6:26
   Change Activity: 2019/9/23下午6:26
-------------------------------------------------
"""

#  微机监测问题数量
# 微机监测系统中“报警信息分析统计”-“大数据”-“道岔报警”
MONITORING_PROBLEM_COUNT_SQL = """SELECT
    WORK_SHOP,
    CHEZHAN,
    XINHAO,
    KADAOZHA,
    KASHA,
    QITAYIWU,
    JIANGUIFEIBIAN,
    JIANGUISHANGQIAO,
    JIANGUIPAXING_CUANDONG,
    JIANGUIBIANXING,
    GENBULUOSIGUOJIN,
    GUNLUN,
    DINGTIEDINGGUIYAO,
    KUANGJIAKUANGDONG,
    QITA
FROM
    `t_turnout_warning_statistics` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d') 
"""


# 检查问题
PROBLEM_NUMBER_SQL = """SELECT
    DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, f.KEY_ITEMS, g.FK_KEY_ITEM_ID,
    1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
            INNER JOIN 
        t_problem_base_key_item as g on f.PK_ID=g.FK_PROBLEM_BASE_ID    
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.FK_RISK_ID IN ({2})
            AND b.IDENTITY= '干部'
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
"""


# 安全薄弱车间-车间及其报警条数
# 微机监测系统中“报警信息分析统计”-“大数据”-“道岔报警”，每段按车间统计报警条数，取报警条数前3
WEAK_SECURITY_DEPARTMENT_SQL = """SELECT
    UNIT_NAME,
    WORK_SHOP,
    COUNT(CAST(REPEAT_TIMES as signed)) AS WARN_COUNT 
FROM
    `t_turnout_warning_statistics` 
WHERE
        WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d')  
and WARNING_LEVEL in ('一级','二级','三级')
GROUP BY
    UNIT_NAME,WORK_SHOP
"""


# 所有车间
ALL_DEPARTMENT_SQL = """SELECT
    DEPARTMENT_ID AS FK_DEPARTMENT_ID,
    1 AS WORK_SHOP_COUNT 
FROM
    `t_department` 
WHERE
    TYPE = 8 
    AND IS_DELETE = 0 
    AND SHORT_NAME != '' 
"""

# 车间检查次数
CADRE_CHECK_COUNT_SQL = """SELECT
        d.TYPE4 as FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
            INNER JOIN
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY d.TYPE4;
"""

# 检查总次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 报警信息分析统计
# 微机监测系统中“报警信息分析统计”-“统计表”
# 站段名称,已调阅, 及时分析
ALARM_INFORMATION_ANALYSIS_SQL = """SELECT
    WORK_SHOP AS NAME,
    UNACCESSED_QUANTITY,
    ACCESSED_QUANTITY,
    TIMELY_ANALYSIS
FROM
    `t_statistics`
WHERE 
    MONTH( CREATE_TIME )  = {1}
    AND YEAR(CREATE_TIME) = {0}
   AND DAY(CREATE_TIME) >= 25
   AND DAY(CREATE_TIME) <= 31
"""


# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 总问题数
ALL_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_EXTERNAL = 0
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""
