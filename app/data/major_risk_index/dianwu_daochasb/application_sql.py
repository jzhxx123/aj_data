# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     application_sql
   Author :       hwj
   date：          2019/9/23上午9:59
   Change Activity: 2019/9/23上午9:59
-------------------------------------------------
"""

# 报警总条数
# 微机监测系统中“报警信息分析统计”-“大数据”-“道岔报警”
WARNING_COUNT_SQL = """
    SELECT
    WORK_SHOP,
    COUNT(CAST(REPEAT_TIMES as signed)) as COUNT
FROM
    `t_turnout_warning_statistics` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d' )
    AND WARNING_LEVEL in ('一级','二级','三级')
GROUP BY
    WORK_SHOP
"""

# 道岔牵引点总数
TRACTION_POINT_COUNT_SQL = """SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS COUNT 
FROM
    t_department_other_management 
WHERE
    TYPE_NAME = '道岔牵引点数'
"""


# 故障通知”按键次数以及重复发生次数
# 微机监测系统“开关量”报警统计模块，处理结果筛选'#','道岔','缺口'
FAILURE_NOTIFICATION_COUNT_SQL = """SELECT
    WORK_SHOP,
    STATION,
    1 as BUTTON_COUNT,
    1 as REPEAT_COUNT
FROM
    `t_monitoring_switch_warning` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d' )
    AND (PROCESSING_RESULT like '%%#%%'
    OR PROCESSING_RESULT like '%%道岔%%'
    OR PROCESSING_RESULT like '%%缺口%%')
"""
