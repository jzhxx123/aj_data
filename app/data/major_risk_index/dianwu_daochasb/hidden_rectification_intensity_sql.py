# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_rectification_intensity_sql
   Author :       hwj
   date：          2019/9/25下午4:41
   Change Activity: 2019/9/25下午4:41
-------------------------------------------------
"""

# 未销号报警
# 微机监测系统中“报警信息分析统计”-“销号表”。其中在表格中筛选“ZD6/ZY4/ZY6/ZYJ7”
# 因为数量过多,选择待修表
WARNING_ELIMINATE_COUNT_SQL = """SELECT
    WORK_SHOP,
    WARNING_LEVEL
FROM
    `t_to_be_repaired_table` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d')  
    AND WARNING_LEVEL = '一级'
    AND (EQUIPMENT_TYPE like '%%ZD6%%'
    OR EQUIPMENT_TYPE like '%%ZY4%%'
    OR EQUIPMENT_TYPE like '%%ZY6%%'
    OR EQUIPMENT_TYPE like '%%ZYJ7%%')
"""

# 重复报警
# 微机监测系统中“报警信息分析统计”-“销号表”。其中在表格中筛选“ZD6/ZY4/ZY6/ZYJ7”
REPEATED_WARNING_SQL = """SELECT
     WORKSHOP as WORK_SHOP,
     STATION,
     EQUIPMENT_NAME,
     WARNING_REASON AS WARNING_PROJECT,
     WARNING_TIME,
     WARNING_LEVEL
FROM
    `t_elimination`
    WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d') 
    AND WARNING_LEVEL in ('一级', '二级', '三级')
    AND (EQUIPMENT_TYPE like '%%ZD6%%' 
    or EQUIPMENT_TYPE like '%%ZY4%%'
    or EQUIPMENT_TYPE like '%%ZY6%%'
    or EQUIPMENT_TYPE like '%%ZYJ7%%')
"""


# 报警处置条数
# 微机监测系统中“报警信息分析统计”-“统计表”
ALARM_DISPOSAL_TIME_COUNT_SQL = """SELECT
    WORK_SHOP,
    UNACCESSED_QUANTITY,
    ACCESSED_QUANTITY,
    TIMELY_PROCESSING
FROM
    `t_statistics`
WHERE 
    MONTH( CREATE_TIME )  = {1}
    AND YEAR(CREATE_TIME) = {0}
   AND DAY(CREATE_TIME) >= 25
   AND DAY(CREATE_TIME) <= 31
"""


# 问题销号超期数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS OVERDUE_PROBLEM
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            INNER JOIN
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
            AND a.TYPE = 4
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 问题整改超期数
OVERDUE_DAYS_NUMBER_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT c.PK_ID) AS OVERDUE_DAYS_NUMBER
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON  a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            INNER JOIN
        t_check_problem_and_risk as d on a.FK_CHECK_PROBLEM_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND a.OVERDUE_DAYS >= 7
            AND b.MONTH = {1}
            AND d.FK_RISK_ID IN ({2})
            AND a.TYPE = 2
    GROUP BY b.FK_DEPARTMENT_ID
"""