# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/9/23上午11:56
   Change Activity: 2019/9/23上午11:56
-------------------------------------------------
"""

# 部门--去除维修段
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
            AND b.SHORT_NAME != ''
            AND a.TYPE3 != '99990002001499A20007'
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, '电务' AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND (b.TYPE2 = '{0}' or a.TYPE3 in {1})
            AND a.SHORT_NAME != ""
            AND a.TYPE3 != '99990002001499A20007'
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        '电务' AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND (c.DEPARTMENT_ID = '{0}' or a.TYPE3 in {1})
            AND a.TYPE3 != '99990002001499A20007'
"""