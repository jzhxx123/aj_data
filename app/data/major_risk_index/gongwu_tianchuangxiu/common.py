#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from app.data.util import pd_query
import datetime

def calc_total_workload(labortime_data, order_count, dpid_data):
    """
    按照要求计算工务施工安全的工作量=施工、天窗修、点外修的作业人时+次数（其中人时=作业人数*作业时间、占比60%,次数=派工单数量、占比40%
    :return:
    """
    # print("labortime_data:", labortime_data)
    labortime_data['COUNT'] = labortime_data['COUNT'] * 0.6
    order_count['COUNT'] = order_count['COUNT'] * 0.4
    work_load = labortime_data.append(order_count)
    work_load = work_load.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    data = pd.merge(
        work_load,
        dpid_data,
        how='inner',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    FK_PROFESSION_DICTIONARY_ID = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(major,2140)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def _calc_work_load_oneday(time, flag=0):
    workload_a = 0
    zero = (datetime.datetime.strptime(time[:10] + '00:00:00',
                                       "%Y-%m-%d%H:%M:%S")).timestamp()
    six = (datetime.datetime.strptime(time[:10] + '06:00:00',
                                      "%Y-%m-%d%H:%M:%S")).timestamp()
    twentytw0 = (datetime.datetime.strptime(time[:10] + '22:00:00',
                                            "%Y-%m-%d%H:%M:%S")).timestamp()
    twentyfour = (datetime.datetime.strptime(time[:10] + '23:59:59',
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    calc_value = (datetime.datetime.strptime(time,
                                             "%Y-%m-%d%H:%M:%S")).timestamp()
    if flag ==0:
        if zero <= calc_value <= six:
            workload_a = six - calc_value + twentyfour - twentytw0
        elif six < calc_value <= twentytw0:
            workload_a = twentyfour - twentytw0
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = twentyfour - calc_value
    else:
        if zero <= calc_value <= six:
            workload_a = calc_value - zero
        elif six < calc_value <= twentytw0:
            workload_a = six - zero
        elif twentytw0 < calc_value <= twentyfour:
            workload_a = calc_value - twentytw0 + six - zero
    return workload_a


def _calc_work_load_otherday(start, end, zero, six, twentytw0, twentyfour):
    from datetime import date
    workload = 0
    distance = date(int(end[:4]), int(end[5:7]), int(end[8:10])) - \
               date(int(start[:4]), int(start[5:7]), int(start[8:10]))
    days = int(distance.days)
    if days > 1:
        workload = _calc_work_load_oneday(start) + \
                   (days - 1)*(six - zero + twentyfour - twentytw0) + \
                   _calc_work_load_oneday(end, 1)
    else:
        workload = _calc_work_load_oneday(start) + _calc_work_load_oneday(end, 1)
    return workload


def _calc_work_load_sameday(start, end, zero, six, twentytw0, twentyfour):
    workload = 0
    if zero <= start < end <= six or twentytw0 <= start < end <= twentyfour:
        workload = end - start
    elif start < six < end <= twentytw0:
        workload = six - start
    elif start <= six < twentytw0 < end <= twentyfour:
        workload = six - start + end - twentyfour
    elif six <= start < twentytw0 < end <= twentyfour:
        workload = end - twentytw0
    return workload


def _calc_start_time(row):
    start_times_list = row["START_TIMES"].split(',')
    end_times_list = row['END_TIMES'].split(',')
    work_load_list = []
    for idx in range(len(start_times_list)):
        try:
            start = (datetime.datetime.strptime(start_times_list[idx].replace(' ', ''),
                                                "%Y-%m-%d%H:%M:%S")).timestamp()
            end = (datetime.datetime.strptime(end_times_list[idx].replace(' ', ''),
                                              "%Y-%m-%d%H:%M:%S")).timestamp()
            flag = True if start_times_list[idx][:10] == end_times_list[idx][:10] else False

            six = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '06:00:00',
                                              "%Y-%m-%d%H:%M:%S")).timestamp()
            zero = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '00:00:00',
                                               "%Y-%m-%d%H:%M:%S")).timestamp()
            twentytw0 = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '22:00:00',
                                                    "%Y-%m-%d%H:%M:%S")).timestamp()
            twentyfour = (datetime.datetime.strptime(start_times_list[idx].replace(' ', '')[:10] + '23:59:59',
                                                     "%Y-%m-%d%H:%M:%S")).timestamp()
            if flag:
                work_load_list.append(_calc_work_load_sameday(start, end, zero,
                                                              six, twentytw0, twentyfour))
            else:
                work_load_list.append(_calc_work_load_otherday(
                    start_times_list[idx].replace(' ', ''),
                    end_times_list[idx].replace(' ', ''),
                    zero, six, twentytw0, twentyfour))
        except Exception as e:
            work_load_list.append(0)
            continue
    row["COUNT"] = sum(work_load_list)

    return row


def calc_night_work_load(df_work_time, dpid_data):
    """
    计算夜间工作量
    :param df_work_time:
    :param dpid_data:
    :return:
    """
    df_work_time = df_work_time.apply(
        lambda row: _calc_start_time(row),
        axis=1)
    df_work_time.fillna(0)
    df_work_time["COUNT"] = (df_work_time["COUNT"] * df_work_time["ON_RAILWAY_NUMBER"]) / 3600
    data = pd.merge(
        df_work_time,
        dpid_data,
        how="inner",
        left_on="DEPARTID",
        right_on="DEPARTMENT_ID"
    )
    data.drop(["DEPARTID", "ON_RAILWAY_NUMBER", "END_TIMES", "START_TIMES"], inplace=True, axis=1)
    return data
