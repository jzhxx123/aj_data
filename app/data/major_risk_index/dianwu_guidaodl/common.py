# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/10/17下午2:09
   Change Activity: 2019/10/17下午2:09
-------------------------------------------------
"""
import pandas as pd

from app.data.index.common import df_merge_with_dpid
from app.data.major_risk_index.common.common import get_work_shop_department
from app.data.major_risk_index.util import append_major_column_to_df
from app.data.util import pd_query


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    fk_profession_dictionary_id = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = fk_profession_dictionary_id.get(major, 2140)
    get_vm_majors_ids_sql = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def calc_monitoring_problem_count(df_monitoring, zhanduan_dpid):
    """
    微机监测问题数量
    """
    if df_monitoring.empty:
        zhanduan_dpid['COUNT'] = 0
        for i in range(1, 9):
            zhanduan_dpid['MIDDLE_{0}'.format(i)] = 0
        df_monitoring = zhanduan_dpid
    else:
        # 车间关联站段
        df_monitoring['zhanduan_name'] = df_monitoring.apply(
            lambda row: get_work_shop_department(row.WORK_SHOP), axis=1
        )
        df_monitoring.dropna(subset=['zhanduan_name'], inplace=True)
        df_monitoring = df_monitoring.set_index('zhanduan_name')
        df_monitoring.drop(columns=['WORK_SHOP'], inplace=True)

        # 填充缺失数据并将str转换int
        df_monitoring.fillna(0, inplace=True)
        df_monitoring = df_monitoring.replace('', 0).astype(int)
        # df_monitoring[df_monitoring > 0] = 1

        # 统计总问题数
        df_monitoring = df_monitoring.groupby([df_monitoring.index]).sum()
        df_monitoring['COUNT'] = df_monitoring.apply(
            lambda row: sum(row), axis=1
        )
        # 电子元器件
        df_monitoring['MIDDLE_1'] = df_monitoring.apply(
            lambda row: (row['FA_SONG_HE'] + row['JIE_SHOU_HE'] + row['SHUAI_HAO_HE']
                         + row['BU_CHANG_DIAN_RONG'] + row['XI_DUI'] + row['BIAN_ZU_QI']
                         + row['FANG_LEI_BU_CHANG_QI'] + row['BIAN_YA_QI'] + row['JI_DIAN_QI']
                         + row['JI_ZHOU_SHE_BEI'] + row['QI_TA']), axis=1)
        # 工务结合部
        df_monitoring['MIDDLE_2'] = df_monitoring.apply(
            lambda row: (row['JUE_YUAN_BU_LIANG'] + row['KOU_JIAN_DI_YU_WEI_BAN'] + row['GUI_JU_GAN_DUAN_LU']
                         + row['DI_MAO_LA_GAN_DUAN_LU'] + row['GANG_GUI_DUAN_GUI'] + row['FEI_BIAN_DUAN_LU']
                         + row['GANG_GUI_LIN_PIAN_DUAN_LU'] + row['OTHER']), axis=1)
        # 外单位作业影响
        df_monitoring['MIDDLE_3'] = df_monitoring.apply(
            lambda row: (row['GONG_WU_ZUO_YE'] + row['GONG_DIAN_ZUO_YE'] + row['ANOTHER']), axis=1)
        # 各部连接线断接触不良
        df_monitoring['MIDDLE_4'] = df_monitoring.apply(
            lambda row: (row['LENG_YA_CHA_ZHEN_SONG_DONG']
                         + row['XIANG_LIAN_XIAN_DUAN_XIAN_HUO_JIE_CHU_BU_LIANG']
                         + row['DAO_JIE_XIAN_DUAN_XIAN_HUO_JIE_CHU_BU_LIANG']
                         + row['DIAO_XIE_DAN_YUAN_LIAN_JIE_XIAN_XIU_SHI'] + row['QITA4']), axis=1)
        # 区段压不死
        df_monitoring['MIDDLE_5'] = df_monitoring.apply(
            lambda row: (row['PAI_LIE_DIAO_CHE_JIN_LU']
                         + row['WEI_PAI_LIE_DIAO_CHE_JIN_LU'] + row['LIE_CHE_JIN_LU']), axis=1)
        # 电缆不良、道床漏泄、金属异物短路
        df_monitoring['MIDDLE_6'] = df_monitoring['DIAN_LAN_BU_LIANG']
        df_monitoring['MIDDLE_7'] = df_monitoring['DAO_CHUANG_LOU_XIE']
        df_monitoring['MIDDLE_8'] = df_monitoring['JIN_SHU_YI_WI_DUAN_LU']

        df_monitoring = pd.merge(
            df_monitoring,
            zhanduan_dpid,
            how='right',
            left_index=True,
            right_on='NAME'
        )
    df_monitoring.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    return df_monitoring


def get_check_problem_count(problem_number_sql, risk_ids, stats_months, department_data):
    """获取检查问题总数及干部检查问题数"""
    # todo 区段压不死 无id  暂时取999999
    key_items_list = [2706, 2707, 2708, 2709, 999999, 2710, 2711, 2712]
    key_items_data_list = []
    key_col_list = []
    all_problem = pd_query(problem_number_sql.format(*stats_months, risk_ids))
    for idx, key in enumerate(key_items_list):
        col = 'MIDDLE_{}_PROBLEM'.format(idx + 1)
        check_problem = all_problem[all_problem.FK_KEY_ITEM_ID == key].copy()
        check_problem = check_problem.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().to_frame(col)
        key_col_list.append(col)
        key_items_data_list.append(check_problem)
    cadre_check_problem = pd.concat(key_items_data_list, axis=1, sort=False)
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem['ALL_PROBLEM'] = cadre_check_problem.apply(
        lambda row: sum(row), axis=1
    )
    key_col_list.append('ALL_PROBLEM')
    cadre_check_problem = pd.merge(
        cadre_check_problem,
        department_data,
        how='right',
        left_index=True,
        right_on='DEPARTMENT_ID')
    cadre_check_problem.fillna(0, inplace=True)
    cadre_check_problem = cadre_check_problem.groupby(
        'TYPE3')[key_col_list].sum()
    return cadre_check_problem


def calc_problem_exposure_basic_data(
        monitoring_problem_count,
        cadre_check_problem,
        choose_dpid_data):
    """
    获取隐患排查力度指数--问题暴露度指数的基础数据
    :param monitoring_problem_count: 微机监测问题数量
    :param cadre_check_problem: 干部检查问题数
    :param choose_dpid_data: 部门数据
    :return:
    calc_df_data: 中间过程数据
    df_rst: 得分数据
    """
    basic_data = pd.merge(
        monitoring_problem_count,
        cadre_check_problem,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    basic_data.fillna(0, inplace=True)
    for i in range(1, 9):
        basic_data['SCORE_{0}'.format(i)] = basic_data.apply(
            lambda row: max((row['MIDDLE_{0}'.format(i)] / row['COUNT']
                             - row['MIDDLE_{0}_PROBLEM'.format(i)] / row['ALL_PROBLEM']) * 100, 0)
            if row['COUNT'] > 0 and row['ALL_PROBLEM'] > 0 else 0, axis=1
        )

    tmp_list = [
        '电子元器件({0:.2f}) = (电子元器件报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-电子元器件问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '工务结合部（轨道电路）({0:.2f}) = (工务结合部报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-工务结合部问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '外单位作业影响({0:.2f}) = (外单位作业影响报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-外单位作业影响问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '各部连接线断接触不良({0:.2f}) = (各部连接线断接触不良报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-各部连接线断接触不良问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '区段压不死({0:.2f}) = (区段压不死报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-区段压不死问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '电缆不良({0:.2f}) = (电缆不良报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-电缆不良问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '道床漏泄({0:.2f}) = (道床漏泄报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-道床漏泄问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
        '金属异物短路({0:.2f}) = (金属异物短路报警总数({1:.0f}) / 报警总数({3:.0f})'
        '-金属异物短路问题数({2:.0f}) / 干部检查问题总数({4:.0f}))*100',
    ]
    basic_data['CONTENT'] = basic_data.apply(
        lambda row: '<br/>'.join([content.format(
            row['SCORE_{0}'.format(idx + 1)],
            row['MIDDLE_{0}'.format(idx + 1)],
            row['MIDDLE_{0}_PROBLEM'.format(idx + 1)],
            row['COUNT'],
            row['ALL_PROBLEM']
        ) for idx, content in enumerate(tmp_list)]), axis=1
    )
    basic_data['SCORE_a_3'] = basic_data.apply(
        lambda row: max(100 - sum(row[col] for col in ['SCORE_{0}'.format(num) for num in range(1, 9)]), 0), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=basic_data['TYPE3'],
            data=basic_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    df_rst = basic_data.groupby(['TYPE3'])['SCORE_a_3'].sum().to_frame()
    return calc_df_data, df_rst
