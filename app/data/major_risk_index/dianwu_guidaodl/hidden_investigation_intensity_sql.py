# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_investigation_intensity_sql
   Author :       hwj
   date：          2019/10/21上午10:29
   Change Activity: 2019/10/21上午10:29
-------------------------------------------------
"""

#  微机监测问题数量
MONITORING_PROBLEM_COUNT_SQL = """SELECT
    WORK_SHOP,
    FA_SONG_HE,
    JIE_SHOU_HE,
    SHUAI_HAO_HE,
    BU_CHANG_DIAN_RONG,
    XI_DUI,
    BIAN_ZU_QI,
    FANG_LEI_BU_CHANG_QI,
    BIAN_YA_QI,
    JI_DIAN_QI,
    JI_ZHOU_SHE_BEI,
    QI_TA,
    JUE_YUAN_BU_LIANG,
    KOU_JIAN_DI_YU_WEI_BAN,
    GUI_JU_GAN_DUAN_LU,
    DI_MAO_LA_GAN_DUAN_LU,
    GANG_GUI_DUAN_GUI,
    FEI_BIAN_DUAN_LU,
    GANG_GUI_LIN_PIAN_DUAN_LU,
    OTHER,
    GONG_WU_ZUO_YE,
    GONG_DIAN_ZUO_YE,
    ANOTHER,
    LENG_YA_CHA_ZHEN_SONG_DONG,
    XIANG_LIAN_XIAN_DUAN_XIAN_HUO_JIE_CHU_BU_LIANG,
    DAO_JIE_XIAN_DUAN_XIAN_HUO_JIE_CHU_BU_LIANG,
    DIAO_XIE_DAN_YUAN_LIAN_JIE_XIAN_XIU_SHI,
    QITA4,
    PAI_LIE_DIAO_CHE_JIN_LU,
    WEI_PAI_LIE_DIAO_CHE_JIN_LU,
    LIE_CHE_JIN_LU,
    DIAN_LAN_BU_LIANG,
    DAO_CHUANG_LOU_XIE,
    JIN_SHU_YI_WI_DUAN_LU
FROM
    `t_track_circuit_warning` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d') 
"""

# 检查问题
PROBLEM_NUMBER_SQL = """SELECT
    DISTINCT  b.FK_DEPARTMENT_ID, a.PK_ID, f.KEY_ITEMS, g.FK_KEY_ITEM_ID,
    1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
            INNER JOIN
        t_problem_base as f on a.FK_PROBLEM_BASE_ID = f.PK_ID
            INNER JOIN 
        t_problem_base_key_item as g on f.PK_ID=g.FK_PROBLEM_BASE_ID    
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.FK_RISK_ID IN ({2})
            AND b.IDENTITY= '干部'
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
"""

# 安全薄弱车间-车间及其报警条数
# 微机监测系统中“报警信息分析统计”-“大数据”-“轨道”，每段按车间统计报警条数，取报警条数前五
WEAK_SECURITY_DEPARTMENT_SQL = """SELECT
    UNIT_NAME,
    WORK_SHOP,
    COUNT(CAST(REPEAT_TIMES as signed)) AS WARN_COUNT 
FROM
    `t_track_circuit_warning` 
WHERE
    WARNING_TIME >= DATE_FORMAT( '{0}', '%%Y/%%m/%%d' ) 
    AND WARNING_TIME < DATE_FORMAT( '{1}', '%%Y/%%m/%%d')  
and WARNING_LEVEL in ('一级','二级','三级')
GROUP BY
    UNIT_NAME,WORK_SHOP
"""


# 所有车间
ALL_DEPARTMENT_SQL = """SELECT
    DEPARTMENT_ID AS FK_DEPARTMENT_ID,
    1 AS WORK_SHOP_COUNT 
FROM
    `t_department` 
WHERE
    TYPE = 8 
    AND IS_DELETE = 0 
    AND SHORT_NAME != '' 
"""


# 车间检查次数
CADRE_CHECK_COUNT_SQL = """SELECT
        d.TYPE4 as FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
            INNER JOIN
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY d.TYPE4;
"""

# 检查总次数
CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct b.PK_ID) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 4
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 报警信息分析统计
# 站段名称,未调阅, 已调阅, 及时分析数
ALARM_INFORMATION_ANALYSIS_SQL = """SELECT
    WORK_SHOP,
    UNACCESSED_QUANTITY,
    ACCESSED_QUANTITY,
    TIMELY_ANALYSIS
FROM
    `t_statistics`
WHERE 
    MONTH( CREATE_TIME )  = {1}
    AND YEAR(CREATE_TIME) = {0}
   AND DAY(CREATE_TIME) >= 25
   AND DAY(CREATE_TIME) <= 31
"""


# 考核问题数
KAOHE_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 3
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""


# 总问题数
ALL_PROBLEM_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct a.PK_ID) AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk as d on d.FK_CHECK_PROBLEM_ID=a.PK_ID
            INNER JOIN
        t_check_info as e on e.PK_ID=a.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.TYPE = 3
            AND a.IS_EXTERNAL = 0
            AND d.FK_RISK_ID IN ({2})
            AND e.CHECK_WAY BETWEEN 1 and 4
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
    GROUP BY b.FK_DEPARTMENT_ID;
"""
