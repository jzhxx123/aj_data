# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/10/17下午2:09
   Change Activity: 2019/10/17下午2:09
-------------------------------------------------
"""
from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)


from . import (application, combine_child_index, hidden_investigation_intensity, hidden_rectification_intensity)
