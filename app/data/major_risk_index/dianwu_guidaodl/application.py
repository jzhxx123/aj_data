# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     application
   Author :       hwj
   date：          2019/10/21上午8:44
   Change Activity: 2019/10/21上午8:44
-------------------------------------------------
"""

from flask import current_app

from app.data.index.util import (get_custom_month, )
from app.data.major_risk_index.common.application import stats_driving_influence_radio, calc_warning_unit_count, \
    calc_failure_notification_count
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_guidaodl import GLV
from app.data.major_risk_index.dianwu_guidaodl.application_sql import WARNING_COUNT_SQL, \
    FAILURE_NOTIFICATION_COUNT_SQL, TRACK_CIRCUIT_COUNT_SQL
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.3:
        _score = 0
    elif 0.3 > _ratio >= 0.1:
        _score = 60 + 3 * (0.1 - _ratio) * 100
    elif 0.1 >= _ratio > -0.3:
        _score = 100 + (-0.3 - _ratio) * 100
    else:
        _score = 100
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        WARNING_COUNT, ASSESS_GUANLI_PROBLEM_COUNT, FAILURE_NOTIFICATION_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    check_risk_ids = GLV.get_value('CHECK_RISK_IDS')

    # 报警总条数
    warning_count = pd_query(
        WARNING_COUNT_SQL.format(*stats_month), db_name='db_mid')
    WARNING_COUNT = calc_warning_unit_count(warning_count, ZHANDUAN_DPID_DATA.copy())

    # 轨道电路区段总数
    ASSESS_GUANLI_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(TRACK_CIRCUIT_COUNT_SQL),
        DEPARTMENT_DATA)

    # 故障通知”按键次数以及重复发生次数
    failure_notification_count = pd_query(FAILURE_NOTIFICATION_COUNT_SQL.format(*stats_month), db_name='db_mid')
    FAILURE_NOTIFICATION_COUNT = calc_failure_notification_count(failure_notification_count, ZHANDUAN_DPID_DATA.copy())

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 报警率指数
def _stats_warning_radio(months_ago):
    """
    报警总条数/道岔牵引点总数，低于基数30%得100分、专业基数正负10%得60分、高于基数30%为0分
    基数：为当月各段报警率的平均值
    """
    content = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>报警率指数({3}) = ' \
              + '报警总条数({4})/ 轨道电路区段总数({5})*100%</p>'
    return calc_child_index_type_divide(
        WARNING_COUNT,
        ASSESS_GUANLI_PROBLEM_COUNT,
        2,
        8,
        1,
        months_ago,
        'COUNT',
        'SCORE_a',
        _calc_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
        customizecontent=content
    )


# 行车影响指数
def _stats_driving_influence_radio(months_ago):
    """
    每一次“故障通知”按键记录扣1分，扣满20分为止。
    同一车站存在多次“故障通知”按键记录时，每次加扣0.5分"""
    # 获取中间过程,得分数据
    return stats_driving_influence_radio(months_ago,
                                         FAILURE_NOTIFICATION_COUNT,
                                         _choose_dpid_data,
                                         RISK_TYPE)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【报警率指数、行车影响指数】
    child_index_func = [
        _stats_warning_radio,
        _stats_driving_influence_radio,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b']]
    item_weight = [0.8, 0.2]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        8,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=9, major=risk_type, main_type=8,
                                 child_index_list=[1, 2], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── application index has been figured out!')


if __name__ == '__main__':
    pass
