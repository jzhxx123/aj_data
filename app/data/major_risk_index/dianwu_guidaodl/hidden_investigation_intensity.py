# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     hidden_investigation_intensity
   Author :       hwj
   date：          2019/10/21上午10:03
   Change Activity: 2019/10/21上午10:03
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import (get_custom_month,
                                 )
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.hidden_investigation_intensity import stat_check_evenness_radio, \
    get_weak_security_department_count, alarm_information_analysis_ratio, get_all_department_count
from app.data.major_risk_index.dianwu_guidaodl import GLV
from app.data.major_risk_index.dianwu_guidaodl.common import calc_monitoring_problem_count, \
    calc_problem_exposure_basic_data, get_check_problem_count
from app.data.major_risk_index.dianwu_guidaodl.hidden_investigation_intensity_sql import (
    MONITORING_PROBLEM_COUNT_SQL, PROBLEM_NUMBER_SQL, WEAK_SECURITY_DEPARTMENT_SQL, CADRE_CHECK_COUNT_SQL,
    CHECK_COUNT_SQL, ALL_DEPARTMENT_SQL, KAOHE_PROBLEM_SQL, ALARM_INFORMATION_ANALYSIS_SQL, ALL_PROBLEM_SQL)
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index, calc_child_index_type_divide_major,
    format_export_basic_data, write_export_basic_data_to_mongo, summizet_operation_set)
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_assess_score_by_formula(row, column, major_column, detail_type=None):
    _score = 0
    if row[major_column] == 0:
        return 60
    _ratio = row[column]
    if _ratio >= 0.9:
        _score = 100 - 100 * (_ratio - 0.9)
    elif 0.9 > _ratio >= 0.6:
        _score = 100 - 100 * (0.9 - _ratio) * 4 / 3
    elif 0.6 > _ratio >= 0.4:
        _score = 60 - 200 * (0.6 - _ratio)
    else:
        _ratio = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        MONITORING_PROBLEM_COUNT, CADRE_CHECK_PROBLEM, WEAK_CHECK_COUNT, CHECK_COUNT, \
        ASSESS_PROBLEM_COUNT, ALL_PROBLEM_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    check_item_ids = GLV.get_value('CHECK_ITEM_IDS')
    check_risk_ids = GLV.get_value('CHECK_RISK_IDS')

    # 问题暴露度指数--微机监测问题数量
    monitoring_problem_count = pd_query(
        MONITORING_PROBLEM_COUNT_SQL.format(*stats_month), db_name='db_mid')
    MONITORING_PROBLEM_COUNT = calc_monitoring_problem_count(monitoring_problem_count, ZHANDUAN_DPID_DATA.copy())

    # 问题暴露度指数--干部检查问题数及总问题数
    CADRE_CHECK_PROBLEM = get_check_problem_count(
        PROBLEM_NUMBER_SQL, check_risk_ids, stats_month, DEPARTMENT_DATA)

    # 检查均衡-安全薄弱部门干部检查次数
    all_department = pd_query(WEAK_SECURITY_DEPARTMENT_SQL.format(*stats_month), db_name='db_mid')
    cadre_check_count = pd_query(CADRE_CHECK_COUNT_SQL.format(*stats_month, check_item_ids))
    WEAK_CHECK_COUNT = get_weak_security_department_count(all_department, cadre_check_count,
                                                          DEPARTMENT_DATA.copy())

    # 检查均衡-检查次数
    check_count = pd_query(CHECK_COUNT_SQL.format(*stats_month, check_item_ids))
    CHECK_COUNT = get_all_department_count(
        all_department, check_count, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA)

    # ALARM_INFORMATION_ANALYSIS = pd_query(ALARM_INFORMATION_ANALYSIS_SQL.format(*stats_month), db_name='db_mid')

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month, check_risk_ids)),
        DEPARTMENT_DATA)

    # 总问题数
    ALL_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_SQL.format(*stats_month, check_risk_ids)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题暴露度指数
def _stats_problem_exposure_radio(months_ago):
    """微机监测问题数量占比/干部检查问题占比, 即微机监测问题数量/干部检查问题数量"""

    rst_child_score = []
    # 获取中间过程数据及分数
    calc_df_data, df_rst = calc_problem_exposure_basic_data(MONITORING_PROBLEM_COUNT,
                                                            CADRE_CHECK_PROBLEM,
                                                            _choose_dpid_data)
    # 保存中间计算过程到mongo
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 9, 1, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 9, 1, risk_type=RISK_TYPE)
    # 导出得分
    summizet_operation_set(
        df_rst,
        _choose_dpid_data(3),
        'SCORE_a_3',
        3,
        2,
        9,
        1,
        months_ago,
        risk_type=RISK_TYPE)
    rst_child_score.append(df_rst)
    return rst_child_score


# 检查均衡度
def _stat_check_evenness_radio(months_ago):
    """
    安全薄弱车间干部检查次数/全段道岔设备检查总次数
    基数 = 道岔设备检查总次数/车间总数量
    """
    return stat_check_evenness_radio(months_ago,
                                     WEAK_CHECK_COUNT,
                                     CHECK_COUNT,
                                     _choose_dpid_data,
                                     RISK_TYPE)


# # 报警分析指数
# def _alarm_information_analysis_ratio(months_ago):
#     """
#     分析覆盖率指数(50%):已调阅数量/道岔设备总数
#     分析及时性指数(50%):及时分析条数/已调阅数量
#     """
#     return alarm_information_analysis_ratio(
#         ALARM_INFORMATION_ANALYSIS,
#         ZHANDUAN_DPID_DATA,
#         _choose_dpid_data,
#         months_ago,
#         RISK_TYPE
#     )


# 问题考核率
def _stats_check_problem_assess_radio(months_ago):
    return calc_child_index_type_divide_major(
        ASSESS_PROBLEM_COUNT,
        ALL_PROBLEM_COUNT,
        2,
        9,
        4,
        months_ago,
        'COUNT',
        'SCORE_d',
        _calc_assess_score_by_formula,
        _choose_dpid_data,
        risk_type=RISK_TYPE,
    )


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别表示【问题暴露度指数、检查均衡度、 问题考核率】
    child_index_func = [
        _stats_problem_exposure_radio,
        _stat_check_evenness_radio,
        _stats_check_problem_assess_radio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'd']]
    item_weight = [0.4, 0.3, 0.3]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        9,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    update_major_maintype_weight(index_type=9, major=risk_type, main_type=9,
                                 child_index_list=[1, 2, 4], child_index_weight=item_weight)
    current_app.logger.debug(
        '├── └── hidden_investigation_intensity index has been figured out!')


if __name__ == '__main__':
    pass
