#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 5:17 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ALL_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL,
    XIANCHENG_RISK_LEVEL_PROBLEM_SQL, MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL,
    MEDIA_PROBLME_SCORE_SQL, REAL_CHECK_BANZU_SQLIST, REAL_CHECK_POINT_SQL, CHECK_POINT_SQL,
    CHECK_COUNT_SQL, YECHA_CHECK_SQL, ABOVE_YIBAN_PROBLEM_NUMBER_SQL,
    ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL)