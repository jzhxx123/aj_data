#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 5:18 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import (
    CHECK_EVALUATE_SZ_SCORE_SQL, HAPPEN_PROBLEM_POINT_SQL,
    OVERDUE_PROBLEM_NUMBER_SQL)