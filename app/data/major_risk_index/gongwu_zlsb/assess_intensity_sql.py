#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   ass_intensity_sql
Description:
Author:    
date:         2019/11/5
-------------------------------------------------
Change Activity:2019/11/5 5:13 下午
-------------------------------------------------
"""
from app.data.major_risk_index.common_diff_risk_and_item.assess_intensity_sql import \
    KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL, AWARD_RETURN_SQL
