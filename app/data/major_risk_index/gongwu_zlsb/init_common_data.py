#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
一些常用数据初始化
"""
from app.data.util import pd_query
from app.data.major_risk_index.gongwu_zlsb import GLV
from app.data.major_risk_index.gongwu_zlsb.common_sql import (
    CHEJIAN_DPID_SQL, DEPARTMENT_SQL, ZHANDUAN_DPID_SQL,
    WORK_LOAD_SQL)
from app.data.major_risk_index.gongwu_zlsb.common import(
    get_vitual_major_ids, calc_total_workload)
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.index.util import (get_custom_month)
import pandas as pd


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-3")

    stats_month = get_custom_month(months_ago)
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major))

    # 防洪工作量
    # ZLSB_WORK_LOAD = pd_query(WORK_LOAD_SQL.format(major))
    # 19B8C3534E065665E0539106C00A58FD, 遂宁工务段, 工务
    # 19B8C3534E085665E0539106C00A58FD, 涪陵工务段, 工务
    # 19B8C3534E0D5665E0539106C00A58FD, 内江工务段, 工务
    # 19B8C3534E0F5665E0539106C00A58FD, 达州工务段, 工务
    # 19B8C3534E135665E0539106C00A58FD, 成都工务段, 工务
    # 19B8C3534E145665E0539106C00A58FD, 成都工务大机段, 工务
    # 19B8C3534E1D5665E0539106C00A58FD, 贵阳工务段, 工务
    # 19B8C3534E255665E0539106C00A58FD, 成都工务大修段, 工务
    # 19B8C3534E275665E0539106C00A58FD, 重庆工务段, 工务
    # 19B8C3534E2D5665E0539106C00A58FD, 绵阳工务段, 工务
    # 19B8C3534E395665E0539106C00A58FD, 凯里工务段, 工务
    # 19B8C3534E505665E0539106C00A58FD, 贵阳高铁工务段, 工务
    # 19B9D8D920D8589FE0539106C00A1189, 成都高铁工务段, 工务
    # 99990002001499A10010, 重庆工电段, 工务
    # 99990002001499A10012, 宜宾工电段, 工务
    # 99990002001499A10013, 六盘水工电段, 工务
    # 99990002001499A10014, 成都桥路大修段, 工务
    # 99990002001499A10015, 西昌工电段, 工务
    zlsb_work_load_list = [
        # 遂宁工务段, 涪陵工务段
        ['19B8C3534E065665E0539106C00A58FD', 15], ['19B8C3534E085665E0539106C00A58FD', 11],
        # 内江工务段, 达州工务段
        ['19B8C3534E0D5665E0539106C00A58FD', 12], ['19B8C3534E0F5665E0539106C00A58FD', 13],
        # 成都工务段, 成都工务大机段
        ['19B8C3534E135665E0539106C00A58FD', 15], ['19B8C3534E145665E0539106C00A58FD', 281],
        # 贵阳工务段, 成都工务大修段
        ['19B8C3534E1D5665E0539106C00A58FD', 18], ['19B8C3534E255665E0539106C00A58FD', 63],
        # 重庆工务段, 绵阳工务段
        ['19B8C3534E275665E0539106C00A58FD', 13], ['19B8C3534E2D5665E0539106C00A58FD', 15],
        # 凯里工务段, 贵阳高铁工务段
        ['19B8C3534E395665E0539106C00A58FD', 11], ['19B8C3534E505665E0539106C00A58FD', 18],
        # 成都高铁工务段, 重庆工电段
        ['19B9D8D920D8589FE0539106C00A1189', 14], ['99990002001499A10010', 13],
        # 宜宾工电段, 六盘水工电段
        ['99990002001499A10012', 9], ['99990002001499A10013', 15],
        # 成都桥路大修段, 西昌工电段
        ['99990002001499A10014', 2], ['99990002001499A10015', 14],
    ]
    ZLSB_WORK_LOAD = pd.DataFrame(data=zlsb_work_load_list, columns=['FK_DEPARTMENT_ID','COUNT'])


    values = {
        "ZHANDUAN_DPID_DATA": ZHANDUAN_DPID_DATA,
        "CHEJIAN_DPID_DATA": CHEJIAN_DPID_DATA,
        "DEPARTMENT_DATA": DEPARTMENT_DATA,
        "ZLSB_WORK_LOAD": ZLSB_WORK_LOAD,
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
