#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import pandas as pd
from app.data.util import pd_query
import datetime
from app.data.index.common import df_merge_with_dpid

def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    FK_PROFESSION_DICTIONARY_ID = {"工电": 2140}
    major = risk_type.split('-')[0]
    profession_dictionary_id = FK_PROFESSION_DICTIONARY_ID.get(major,2140)
    GET_VM_MAJORS_IDS_SQL = """
        SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        FK_PROFESSION_DICTIONARY_ID in ({0})
    """
    major_ids = pd_query(GET_VM_MAJORS_IDS_SQL.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


# 如果数据有缺失，按站段补全数据
def data_complete_by_condition(raw_df, filled_data, columns, default, merged_data):
    """[补全数据，填充默认值]
    1.数据全部缺失补全站段
    2.数据部分缺失，找出缺失站段补全
    3.数据完整不处理
    Arguments:
        raw_df {[type]} -- [description]
        filled_data {[type]} -- [description]
        default {[type]} -- [description]
    """

    if raw_df.empty is True:
        raw_df = filled_data[['DEPARTMENT_ID']]
        raw_df.loc[:,'COUNT'] = 0
        for idx, column in enumerate(columns):
            raw_df.loc[:, column] = default[idx]
        raw_df.rename(columns={'DEPARTMENT_ID': 'FK_DEPARTMENT_ID'}, inplace=True)
        raw_df = df_merge_with_dpid(raw_df, merged_data)
    else:
        data = df_merge_with_dpid(raw_df, merged_data)
        raw_type3_set = set(data['TYPE3'].values.tolist())
        real_type3_set = set(filled_data['DEPARTMENT_ID'].values.tolist())
        subset = real_type3_set - raw_type3_set
        if subset:
            init_data = [[data] for data in subset]
            sub_df = pd.DataFrame(data=init_data, columns=['FK_DEPARTMENT_ID'])
            sub_df.loc[:,'COUNT'] = 0
            sub_df = df_merge_with_dpid(sub_df, merged_data)
            for idx, column in enumerate(columns):
                sub_df.loc[:,column] = default[idx]
            raw_df.append(sub_df)
        else:
            pass
    return raw_df


def calc_total_workload(labortime_data, order_count, dpid_data):
    """
    按照要求计算工务施工安全的工作量=施工、天窗修、点外修的作业人时+次数（其中人时=作业人数*作业时间、占比60%,次数=派工单数量、占比40%
    :return:
    """
    labortime_data['COUNT'] = labortime_data['COUNT'] * 0.6
    order_count['COUNT'] = order_count['COUNT'] * 0.4
    work_load = labortime_data.append(order_count)
    work_load = work_load.groupby('FK_DEPARTMENT_ID')['COUNT'].sum().reset_index()
    data = pd.merge(
        work_load,
        dpid_data,
        how='inner',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    data.drop(["FK_DEPARTMENT_ID"], inplace=True, axis=1)
    return data