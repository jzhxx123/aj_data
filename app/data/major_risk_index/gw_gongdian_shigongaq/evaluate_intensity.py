#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    评价力度指数
"""
from flask import current_app
from app.data.major_risk_index.gw_gongdian_shigongaq import GLV
from app.data.major_risk_index.util import (
    combine_child_index_func, df_merge_with_dpid, summizet_child_index)
from app.data.index.util import (get_custom_month,
                                 get_query_condition_by_risktype)
from app.data.major_risk_index.common import evaluate_intensity
from app.data.major_risk_index.gongwu_shigongaq.evaluate_intensity_sql import \
    EVALUATE_SCORE_SQL, SHIGONG_COUNT_SQL
from app.data.util import pd_query
from app.data.major_risk_index.gw_gongdian_shigongaq.common import fill_nan_withzero
from app.data.util import update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_func(row, column, major_column, detail_type=None):
    """
    Arguments:
        row {[type]} -- [description]
        column {[type]} -- [description]
        major_column {[type]} -- [description]
    
    Keyword Arguments:
        detail_type {[type]} -- [description] (default: {None})
    """
    _score = 0
    if row[major_column] == 0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.15:
        return 100
    elif _ratio >= 0:
        return 90 + _ratio * 50
    elif _ratio >= -0.15:
        return 90 + _ratio * 66
    elif _ratio >= -0.3:
        return 80 + (_ratio + 0.15) * 160
    else:
        return 71 + _ratio * 50


def _get_sql_data(months_ago, risk_name, risk_type):
    global RISK_TYPE
    RISK_TYPE = risk_type
    global EVALUATE_SCORE, \
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, SHIGONG_COUNT
    # 虚拟专业站段id
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    risk_ids = diaoche[1]


    # (施工、天窗修、点外修作业人数）工作量里面的人数，是派工单中的上单作业人数
    SHIGONG_COUNT = df_merge_with_dpid(
        pd_query(SHIGONG_COUNT_SQL.format(*stats_month), db_name='db_mid'),
        DEPARTMENT_DATA)
    # 干部评价记分总分数
    EVALUATE_SCORE = df_merge_with_dpid(
        pd_query(EVALUATE_SCORE_SQL.format(*stats_month, risk_ids)),
        DEPARTMENT_DATA)
    
    EVALUATE_SCORE = fill_nan_withzero(EVALUATE_SCORE, DEPARTMENT_DATA)


# 人均评价记分
def _stats_active_score_per_person(months_ago):
    fraction = GLV.get_value("stats_active_score_per_person", (None,))[0]
    return evaluate_intensity.stats_score_per_person_major(
        EVALUATE_SCORE,
        SHIGONG_COUNT,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        calc_score_by_formula=_calc_func,
        fraction=fraction)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    child_index_func = [_stats_active_score_per_person]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    # 站段
    item_name = [f'SCORE_{x}' for x in ['c']]
    item_weight = [1]
    update_major_maintype_weight(index_type=2, major=risk_type, main_type=2,
                                 child_index_list=[3], child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        2,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    current_app.logger.debug(
        '├── └── evaluate_intensity index has been figured out!')

if __name__ == '__main__':
    pass
