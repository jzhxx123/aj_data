#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2019/03/08
'''

from app.data.major_risk_index.cheliang_diaochefd import (
    assess_intensity, check_evenness, check_intensity, problem_rectification,
    problem_exposure)
from app.data.major_risk_index.common import combine_child_index
from app.data.major_risk_index.cheliang_diaochefd.common_sql import (
    CHEJIAN_DPID_SQL, ZHANDUAN_DPID_SQL)

from app.data.index.util import validate_exec_month


@validate_exec_month
def execute(months_ago):
    risk_name = 41
    risk_type = '车辆-11'
    for func in [
            check_intensity, assess_intensity, check_evenness,
            problem_exposure, problem_rectification
    ]:
        func.execute(months_ago, risk_name, risk_type)

    child_index_name = [1, 3, 4, 5, 6]
    child_index_weight = [0.3, 0.2, 0.1, 0.2, 0.2]
    combine_child_index.merge_child_index(ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL,
                                          months_ago, risk_name, risk_type,
                                          child_index_name, child_index_weight)


if __name__ == '__main__':
    pass
