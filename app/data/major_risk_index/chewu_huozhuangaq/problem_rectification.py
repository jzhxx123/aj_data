# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_rectification
   Author :       hwj
   date：          2019/8/13上午8:22
   Change Activity: 2019/8/13上午8:22
-------------------------------------------------
"""
from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.problem_rectification_sql import OVERDUE_PROBLEM_NUMBER_SQL, \
    HAPPEN_PROBLEM_POINT_SQL
from app.data.major_risk_index.common import problem_rectification
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common_diff_risk_and_item.problem_rectification_sql import CHECK_EVALUATE_SZ_SCORE_SQL, \
    CHECK_EVALUATE_SZ_NUMBER_SQL
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, DEPARTMENT_DATA, CHEJIAN_DPID_DATA, \
        CHECK_EVALUATE_SZ_NUMBER_DATA

    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    risktype_data = get_query_condition_by_risktype(risk_name)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = risktype_data[0]
    CHECK_RISK_IDS = risktype_data[1]
    stats_month = get_custom_month(months_ago)

    # 评价履职类型统计
    CHECK_EVALUATE_SZ_NUMBER_DATA = df_merge_with_dpid(
        pd_query(CHECK_EVALUATE_SZ_NUMBER_SQL.format(
            *stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA
    )

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 超期问题整改
def _stats_rectification_overdue(months_ago):
    return problem_rectification.stats_rectification_overdue(
        CHECK_RISK_IDS, OVERDUE_PROBLEM_NUMBER_SQL, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data)


# 履职评价指数
def _stats_check_evaluate(months_ago):
    code_dict = {"ZG-1": 60,
                 "ZG-2": 50,
                 "ZG-3": 40,
                 "ZG-4": 30,
                 "ZG-5": 20}
    return problem_rectification.stats_check_evaluate_by_codetype(
        CHECK_EVALUATE_SZ_NUMBER_DATA, DEPARTMENT_DATA, ZHANDUAN_DPID_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, code_dict=code_dict)


# 问题控制
def _stats_repeatedly_index(months_ago):
    return problem_rectification.stats_repeatedly_index(
        CHECK_RISK_IDS, HAPPEN_PROBLEM_POINT_SQL, DEPARTMENT_DATA,
        ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 存放所有子指数项的分数
    child_index_func = [
        _stats_rectification_overdue, _stats_check_evaluate,
        _stats_repeatedly_index
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.1, 0.2, 0.7]
    update_major_maintype_weight(index_type=8, major=risk_type, main_type=6,
                                 child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight)
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        6,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)
    current_app.logger.debug(
        '├── └── problem_rectification index has been figured out!')


if __name__ == '__main__':
    pass
