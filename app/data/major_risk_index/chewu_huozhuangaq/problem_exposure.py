# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     problem_exposure
   Author :       hwj
   date：          2019/8/12下午5:00
   Change Activity: 2019/8/12下午5:00
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_query_condition_by_risktype, get_custom_month, get_months_from_201712_two
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.common import stats_total_problem_exposure_type_chewu
from app.data.major_risk_index.chewu_huozhuangaq.problem_exposure_sql import CHECK_PROBLEM_SQL, HIDDEN_KEY_PROBLEM_SQL, \
    HIDDEN_KEY_PROBLEM_MONTH_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL, \
    SAFETY_PRODUCE_INFO_SQL
from app.data.major_risk_index.common import problem_exposure
from app.data.major_risk_index.common.check_intensity_sql import BANZU_POINT_SQL
from app.data.major_risk_index.common.common import get_major_dpid

from app.data.major_risk_index.util import combine_child_index_func, summizet_child_index, \
    export_basic_data_one_field_monthly, df_merge_with_dpid, calc_child_index_type_sum
from app.data.util import update_major_maintype_weight, pd_query

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, WORK_LOAD, \
        CHECKED_HIDDEN_PROBLEM_POINT_DATA, HIDDEN_PROBLEM_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    diaoche = get_query_condition_by_risktype(risk_name)
    stats_month = get_custom_month(months_ago)
    diaoche_position = ','.join(
        [f'"{postion}"' for postion in diaoche[2].split(',')])

    WORK_LOAD = GLV.get_value('WORK_LOAD')
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = diaoche[0]
    CHECK_RISK_IDS = diaoche[1]


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """（数量占40%，质量分占60%）：总问题数（质量分）/工作量与本专业基数比较（50%），一般及以上数与本专业基数比较（50%）。"""
    fraction_list = GLV.get_value("stats_total_problem_exposure", (None, None, None, None))
    fraction_list = (
        (fraction_list[0], fraction_list[1]),
        (fraction_list[2], fraction_list[3])
    )
    work_load = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='PERSON_NUMBER')
    return stats_total_problem_exposure_type_chewu(
        CHECK_RISK_IDS, CHECK_PROBLEM_SQL, work_load, work_load, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, weight_item=[0.5, 0.5],
        fraction_list=fraction_list)


# # todo 事故扣分
# # 事故隐患问题暴露度
# def _stats_problem_exposure(months_ago):
#     """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。
#
#     Arguments:
#         months_ago {int} -- 前第-N个月（N为负数)
#
#     Returns:
#         [type] -- [description]
#     """
#     stats_months = get_months_from_201712_two(months_ago)
#     # 初始化一个各站段的扣分字典
#     deduct_score = {k: 0 for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values}
#     # hidden_problem 为尚未暴露的隐患问题问题
#     hidden_problem = set(
#         pd_query(HIDDEN_KEY_PROBLEM_SQL.format(CHECK_RISK_IDS))['PID'].values)
#     # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
#     hidden_problem_num_monthly = []
#     for idx, mon in enumerate(stats_months):
#         # month_hidden_problem 为该月暴露出的隐患问题
#         month_hidden_problem = set(
#             pd_query(
#                 HIDDEN_KEY_PROBLEM_MONTH_SQL.format(
#                     mon[0], mon[1], CHECK_RISK_IDS))['PID'].values)
#         if 3 < idx <= (len(stats_months) - 1):
#             # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
#             for each_problem in hidden_problem:
#                 dpid = each_problem.split('||')[0]
#                 deduct_score.update({
#                     dpid: deduct_score.get(dpid, 0) + (idx - 3) * 2
#                 })
#                 hidden_problem_num_monthly.append([dpid, idx])
#         hidden_problem = hidden_problem.difference(month_hidden_problem)
#     # 导出中间过程
#     export_basic_data_one_field_monthly(
#         hidden_problem_num_monthly,
#         DEPARTMENT_DATA,
#         _choose_dpid_data(3),
#         5,
#         3,
#         3,
#         months_ago,
#         lambda x: f'连续{x}个月无',
#         title='事故隐患个数',
#         risk_type=RISK_TYPE)
#     df_hidden_prob = pd.DataFrame(
#         data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
#     df_hidden_prob = df_merge_with_dpid(df_hidden_prob, DEPARTMENT_DATA)
#     rst_child_score = calc_child_index_type_sum(
#         df_hidden_prob,
#         2,
#         5,
#         3,
#         months_ago,
#         'SCORE',
#         'SCORE_c',
#         lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
#         _choose_dpid_data,
#         risk_type=RISK_TYPE)
#     return rst_child_score


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """
    连续4月无的扣2分/条，连续5个月无的扣4分/条，…扣月份-2分/条。得分=100-扣分
    :param months_ago:
    :return:
    """
    customizededuct = {
        4: 2,
        5: 4
    }
    return problem_exposure.stats_problem_exposure_excellent(
        CHECK_RISK_IDS, ZHANDUAN_DPID_DATA, HIDDEN_KEY_PROBLEM_SQL,
        HIDDEN_KEY_PROBLEM_MONTH_SQL, DEPARTMENT_DATA, months_ago, RISK_TYPE,
        _choose_dpid_data, customizededuct=customizededuct)


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    return problem_exposure.stats_banzu_problem_exposure(
        CHECK_RISK_IDS, BANZU_POINT_SQL, EXPOSURE_PROBLEM_DEPARTMENT_SQL,
        DEPARTMENT_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """从他查问题分析1个月全段未自查出该项问题，最高扣30分。较大风险扣1分，严重风险扣3分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题"""
    return problem_exposure.stats_other_problem_exposure(
        CHECK_RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score={'1': 5, '2': 3, '3': 1})


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，事故隐患问题暴露度，班组问题暴露度，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_problem_exposure,
        _stats_banzu_problem_exposure,
        _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'c', 'd', 'e']]
    item_weight = [0.65, 0.25, 0.10, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=5, child_index_list=[1, 3, 4, 5],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── [diaoche]problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
