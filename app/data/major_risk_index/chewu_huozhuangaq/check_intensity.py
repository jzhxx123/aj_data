# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_intensity
   Author :       hwj
   date：          2019/8/9上午9:24
   Change Activity: 2019/8/9上午9:24
-------------------------------------------------
"""

import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.check_intensity_sql import ALL_PROBLEM_NUMBER_SQL, \
    VIDEO_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL, RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL, \
    MEDIA_COST_TIME_SQL, MEDIA_PROBLME_SCORE_SQL, MEDIA_PROBLEM_NUMBER_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST, \
    CHECK_COUNT_SQL, WORK_BANZU_COUNT_SQL
from app.data.major_risk_index.chewu_huozhuangaq.common import get_huo_zhuang_check_address_standard_data
from app.data.major_risk_index.common import check_intensity
from app.data.major_risk_index.common.check_intensity_sql import REAL_CHECK_BANZU_SQL, BANZU_POINT_SQL
from app.data.major_risk_index.common_diff_risk_and_item.common import get_check_address_standard_data
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 60 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _get_sql_data(months_ago, risk_name, risk_type):
    major = _get_major_dpid(risk_type)
    global RISK_TYPE, RISK_NAME
    RISK_TYPE = risk_type
    RISK_NAME = risk_name
    global YEAR, MONTH, LAST_MONTH, CHECK_ITEM_IDS, CHECK_RISK_IDS
    global WORK_LOAD, CHECK_COUNT, PROBLEM_COUNT, VIDEO_PROBLEM_COUNT \
        , PROBLEM_SCORE, JIAODA_RISK_SCORE, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, XC_JIAODA_RISK_SCORE, WORK_LOAD, REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    diaoche = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = diaoche[0]
    CHECK_RISK_IDS = diaoche[1]
    # 装卸车工作量
    WORK_LOAD = GLV.get_value('WORK_LOAD')
    # 量化人员及干部检查次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    # 现场检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 视频检查问题数
    VIDEO_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(VIDEO_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)

    # 较大和重大安全风险问题质量分累计
    JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month, CHECK_RISK_IDS)),
        DEPARTMENT_DATA)
    # 现场检查发现较大和重大安全风险问题质量分累计
    XC_JIAODA_RISK_SCORE = df_merge_with_dpid(
        pd_query(
            XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format(
                *stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA)

    # 工作班组信息
    work_banzu_info_data = pd_query(WORK_BANZU_COUNT_SQL.format(CHECK_ITEM_IDS))

    REAL_CHECK_BANZU_DATA, BANZU_POINT_DATA = get_huo_zhuang_check_address_standard_data(
        work_banzu_info_data, DEPARTMENT_DATA, months_ago, CHECK_ITEM_IDS, major)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 人均检查频次
def _stats_check_per_person(months_ago):
    """量化人员及干部检查/装卸车工作量"""
    content = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算单位检查频次({3}) = ' \
              + '量化人员及干部检查次数({4})/ 装卸车工作量({5})</p>'
    fraction = GLV.get_value("stats_check_per_person", (None,))[0]
    return check_intensity.stats_check_per_person_major(
        CHECK_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        fraction=fraction)


# 现场查处问题率
def _stats_check_problem_ratio(months_ago):
    """现场检查货装问题数/装卸车工作量
    """
    content = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>查出问题率({3}) = ' \
              + '现场检查货装问题数({4})/ 装卸车工作量({5})</p>'
    fraction = GLV.get_value("stats_check_problem_ratio", (None,))[0]
    return check_intensity.stats_check_problem_ratio_type_one_major(
        PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        fraction=fraction)


# 视频查处问题率
def _stats_check_video_problem_ratio(months_ago):
    """视频检查货装问题数/装卸车工作量
    """
    fraction = GLV.get_value("stats_check_video_problem_ratio", (None,))[0]
    return check_intensity.stats_check_problem_video_ratio_type_one_major(
        VIDEO_PROBLEM_COUNT,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        fraction=fraction)


# 人均质量分
def _stats_score_per_person(months_ago):
    """问题质量分累计/装卸车工作量"""
    content = '<p>得分：{0}</p><p>专业平均得分：{6}</p><p>排名: {1}</p><p>专业基数: {2}</p><p>换算人均问题质量分({3}) = ' \
              + '问题质量分累计({4})/ 装卸车工作量({5})</p>'
    fraction = GLV.get_value("stats_score_per_person", (None,))[0]
    return check_intensity.stats_score_per_person_major(
        PROBLEM_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data,
        customizecontent=content,
        fraction=fraction
    )


# 较大风险问题质量均分
def _stats_risk_score_per_person(months_ago):
    """（70%）较大和重大安全风险问题质量分累计/装卸车工作量。
       （30%）较大和重大安全风险问题质量分累计（现场检查问题）/装卸车工作量"""
    title = [
        '较大和重大安全风险问题质量分累计({0})/装卸车工作量({1})',
        '现场检查发现较大和重大安全风险问题质量分累计({0})/装卸车工作量({1})'
    ]
    fraction_list = GLV.get_value("stats_risk_score_per_person", (None, None))
    return check_intensity.stats_risk_score_per_person_major(
        JIAODA_RISK_SCORE,
        XC_JIAODA_RISK_SCORE,
        WORK_LOAD,
        months_ago,
        RISK_TYPE,
        child_weight=[0.7, 0.3],
        choose_dpid_data=_choose_dpid_data,
        title=title,
        fraction_list=fraction_list
    )


# 监控调阅力度
def _stats_media_intensity(months_ago):
    """监控调阅时长累计/装卸车工作量(20%)
    监控调阅发现问题数/装卸车工作量(30%)
    监控调阅发现接发列车问题质量分累计/装卸车工作量(40%)
    调阅班组数/班组数(10%)"""
    title = ['监控调阅时长累计({0})/装卸车工作量({1})',
             '监控调阅发现问题数({0})/装卸车工作量({1})',
             '监控调阅发现问题质量分累计({0})/装卸车工作量({1})',
             '调阅班组数({0})/班组数({1})']
    stats_month = get_custom_month(months_ago)
    media_cost_time_sql = MEDIA_COST_TIME_SQL.format(*stats_month, CHECK_ITEM_IDS)
    monitor_watch_discovery_ratio_sqllist = [
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month, CHECK_ITEM_IDS),
        MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1].format(CHECK_ITEM_IDS)
    ]
    fraction_list = GLV.get_value("stats_media_intensity", (None, None, None, None, None))
    fraction_list = (
        fraction_list[0], fraction_list[1],
        fraction_list[2], None)
    return check_intensity.stats_media_intensity_major(
        DEPARTMENT_DATA,
        WORK_LOAD,
        months_ago,
        RISK_NAME,
        RISK_TYPE,
        risk_ids=CHECK_RISK_IDS,
        media_cost_time_sql=media_cost_time_sql,
        media_problem_score_sql=MEDIA_PROBLME_SCORE_SQL,
        media_problem_number_sql=MEDIA_PROBLEM_NUMBER_SQL,
        monitor_watch_discovery_ratio_sqllist=monitor_watch_discovery_ratio_sqllist,
        title=title,
        child_weight=[0.25, 0.25, 0.25, 0.25],
        choose_dpid_data=_choose_dpid_data,
        fraction_list=fraction_list)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    """检查地点数（货装班组）/货装班组总数（货装人数为0的不纳入）"""
    return check_intensity.stats_check_address_ratio_excellent(
        REAL_CHECK_BANZU_DATA,
        BANZU_POINT_DATA,
        DEPARTMENT_DATA,
        months_ago,
        RISK_TYPE,
        choose_dpid_data=_choose_dpid_data)


def _get_major_dpid(risk_type):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(risk_type.split('-')[0])


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 计算基数
    # _get_base_index(months_ago)
    child_index_func = [
        _stats_check_per_person,
        _stats_check_problem_ratio,
        _stats_check_video_problem_ratio,
        _stats_score_per_person,
        _stats_risk_score_per_person,
        _stats_media_intensity,
        _stats_check_address_ratio
    ]

    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    item_name = [
        f'SCORE_{x}' for x in ['b', 'c', 'r', 'e', 'f', 'j', 'i']
    ]
    item_weight = [0.25, 0.05, 0.10, 0.25, 0.10, 0.15, 0.10]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        1,
        months_ago,
        item_name,
        item_weight, [3],
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=1, child_index_list=[2, 3, 18, 5, 6, 10, 9],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


if __name__ == '__main__':
    pass
