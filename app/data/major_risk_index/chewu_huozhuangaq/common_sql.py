# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common_sql
   Author :       hwj
   date：          2019/8/9上午9:34
   Change Activity: 2019/8/9上午9:34
-------------------------------------------------
"""
# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, a.TYPE3, a.TYPE4, a.TYPE5, a.NAME, a.TYPE
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND b.SHORT_NAME != ''
            AND a.TYPE3 not in {1}
"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '{0}'
            AND a.SHORT_NAME != ""
            AND a.TYPE3 not in {1}
"""

# 车间
CHEJIAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID,
        a.NAME,
        a.TYPE3 AS PARENT_ID,
        c.NAME AS MAJOR
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
            INNER JOIN
        t_department AS c ON a.TYPE2 = c.DEPARTMENT_ID
    WHERE
        a.TYPE = 8 AND a.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND c.DEPARTMENT_ID = '{0}'
            AND a.TYPE3 not in {1}
"""


# 职工总人数
WORK_LOAD_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
        and b.IS_DELETE = 0
        AND b.TYPE2 = '{0}'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 外聘人员数 - 站段
EXTERNAL_PERSON_SQL = """SELECT
    FK_DEPARTMENT_ID AS TYPE3, a.NUMBER AS COUNT
FROM
    t_department_external_person_number AS a
        LEFT JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MONTH = {0} AND b.TYPE = 4;
"""

# 装卸车数
# 装车数Report_name为ZX1,entry001的数据
# 卸车数Report_name为ZX1,entry025的数据
# 地铁卸空数--Report_name为ZX1,entry074的数据
LOAD_AND_UNLOAD_COUNT_SQL = """SELECT
    UNIT, SUM(ENTRY001 + ENTRY025 + ENTRY074) AS CW_COUNT   
    FROM 
        t_data_date
    WHERE
        REPORT_NAME = 'ZX1'
        AND D18_DATE >= {0}
        AND D18_DATE < {1}
    GROUP BY UNIT
"""

# 装卸车数关联部门id
UNIT_ASSOCIATE_DEPARTMENT_SQL = """SELECT
        FK_DEPARTMENT_ID,
        PASSENGER_STATION as UNIT 
    FROM
        t_department_type_cw
"""

# 自装占比及换算货场数
MANAGEMENT_INDEX_SQL = """SELECT a.FK_DEPARTMENT_ID,SELF_MANAGEMENT, GOOD_MANAGEMENT FROM
    ( SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS SELF_MANAGEMENT 
    FROM
        t_department_other_management 
    WHERE
    FK_TYPE_ID = 2504 
    ) as a
    INNER JOIN
    (
    SELECT
    FK_DEPARTMENT_ID,
    MANAGEMENT_INDEX AS GOOD_MANAGEMENT
    FROM
        t_department_other_management 
    WHERE
    FK_TYPE_ID = 2505 
    ) as b
    on a.FK_DEPARTMENT_ID = b.FK_DEPARTMENT_ID
"""