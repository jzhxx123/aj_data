# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     _calc_cardinal_number
   Author :       hwj
   date：          2019/11/27上午9:55
   Change Activity: 2019/11/27上午9:55
-------------------------------------------------
"""
from app.data.index.util import get_custom_month, get_months_from_201712_two, get_query_condition_by_risktype
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.assess_intensity_sql import KAOHE_PROBLEM_SQL, ASSESS_RESPONSIBLE_SQL
from app.data.major_risk_index.chewu_huozhuangaq.check_intensity_sql import (
    CHECK_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, VIDEO_PROBLEM_NUMBER_SQL, PROBLEM_CHECK_SCORE_SQL,
    RISK_LEVEL_PROBLEM_SQL, XIANCHENG_RISK_LEVEL_PROBLEM_SQL,
    MEDIA_COST_TIME_SQL, MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL)
from app.data.major_risk_index.chewu_huozhuangaq.common import get_vitual_major_ids, calc_huozhuang_count
from app.data.major_risk_index.chewu_huozhuangaq.common_sql import (
    ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, LOAD_AND_UNLOAD_COUNT_SQL,
    UNIT_ASSOCIATE_DEPARTMENT_SQL, MANAGEMENT_INDEX_SQL, EXTERNAL_PERSON_SQL, WORK_LOAD_SQL)
from app.data.major_risk_index.common.cardinal_number_common import calc_cardinal_number
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.common.common_sql import BASE_UNIT_INFO_SQL
from app.data.major_risk_index.common.const import (
    IndexDivider, CommonCalcDataType, CHECK_COUNT_INFO,
    SITE_PROBLEM_INFO, VIDEO_PROBLEM_INFO, PROBLEM_SCORE_INFO,
    JIAODA_RISK_SCORE_INFO, XC_JIAODA_RISK_SCORE_INFO,
    MEDIA_COST_TIME_INFO, MEDIA_PROBLEM_NUMBER_INFO,
    MEDIA_PROBLME_SCORE_INFO, ASSESS_PROBLEM_INFO,
    ASSESS_RESPONSIBLE_INFO, AWARD_RETURN_MONEY_PROBLEM_INFO,
    REAL_AWARD_RETURN_MONEY_PROBLEM_INFO,
    ALL_PROBLEM_NUMBER_INFO, ZUOYE_PROBLEM_COUNT_INFO,
    ZUOYE_PROBLEM_CHECK_SCORE_INFO, WORKER_LOAD_INFO,
    PERSON_LOAD_INFO)
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    ABOVE_YIBAN_PROBLEM_NUMBER_SQL, ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL,
    ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL, ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL)
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


class IndexDetails(IndexDivider):
    """[summary]
    实例化本地指数配置
    Arguments:
        IndexDivider {[type]} -- [description]
    """
    months_ago = -1
    risk_type = None
    __slots__ = ('numerator', 'denominator', 'detail_type')

    def __init__(self, numerator, denominator):
        super().__init__(numerator, denominator)


# 月度返奖明细
AWARD_RETURN_PROBLEM_SQL = """SELECT
    distinct  b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            LEFT JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
            AND c.FK_RISK_ID IN ({2})
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND a.LEVEL in {3} 
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 月度返奖明细
REAL_AWARD_RETURN_PROBLEM_SQL = """SELECT
    distinct  b.FK_DEPARTMENT_ID, COUNT(DISTINCT a.PK_ID) AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            LEFT JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
    WHERE
        b.STATUS = 3
            AND c.FK_RISK_ID IN ({2})
            AND b.YEAR = {0} AND b.MONTH = {1}
            AND a.LEVEL in {3} 
            AND a.IS_RETURN = 1
            and a.ACTUAL_MONEY > 0
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 获取一些全局数据
def _get_data(months_ago, risk_name, risk_type):
    global STATS_MONTH, CALC_MONTH, \
        CHECK_ITEM_IDS, RISK_IDS, ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, \
        DEPARTMENT_DATA, STAFF_NUMBER, WORKER_COUNT_DATA
    STATS_MONTH = get_custom_month(months_ago)
    stats_months_list = get_months_from_201712_two(months_ago, months=4)
    risk_config = get_query_condition_by_risktype(risk_name)
    CHECK_ITEM_IDS = risk_config[0]
    RISK_IDS = risk_config[1]
    start = stats_months_list[0][1]
    end = stats_months_list[-1][0]
    month = int(STATS_MONTH[1][5:7])
    CALC_MONTH = end, start
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids('客运-1')
    ZHANDUAN_DPID_DATA = pd_query(ZHANDUAN_DPID_SQL.format(major, ids))
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL.format(major, ids))
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL.format(major, ids))
    # 职工总人数
    STAFF_NUMBER = pd_query(WORK_LOAD_SQL.format(major))


# ------------------------获取比值型相应基数------------------------ #
def get_cardinal_number(months_ago, risk_name, risk_type):
    """[summary]
    获取基数
    Arguments:
        months_ago {[type]} -- [description]
        risk_type {[type]} -- [description]
    """
    _get_data(months_ago, risk_name, risk_type)
    major_dpid = get_major_dpid(risk_type)
    base_unit_info_sql = BASE_UNIT_INFO_SQL.format(*CALC_MONTH, major_dpid)

    IndexDetails.months_ago = months_ago
    IndexDetails.risk_type = risk_type

    # 装卸车工作量
    worker_load_count = CommonCalcDataType(*WORKER_LOAD_INFO)
    worker_load_count.description = '装卸车工作量'
    worker_load_count.value = [LOAD_AND_UNLOAD_COUNT_SQL, UNIT_ASSOCIATE_DEPARTMENT_SQL, MANAGEMENT_INDEX_SQL]
    worker_load_count.func_version = 'customized_df'
    worker_load_count.func_value = _calc_total_work_load

    # 总人数
    person_load = CommonCalcDataType(*PERSON_LOAD_INFO)
    person_load.value = [STAFF_NUMBER, EXTERNAL_PERSON_SQL]

    # ---------检查力度-----------
    # 现场检查次数
    check_count = CommonCalcDataType(*CHECK_COUNT_INFO)
    check_count.value = [CHECK_COUNT_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 现场检查问题数
    site_problem = CommonCalcDataType(*SITE_PROBLEM_INFO)
    site_problem.value = [ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 视频检查问题数
    video_problem = CommonCalcDataType(*VIDEO_PROBLEM_INFO)
    video_problem.value = [VIDEO_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 问题质量分
    problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    problem_check_score.value = [PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 较大问题质量分
    jiaoda_risk_score = CommonCalcDataType(*JIAODA_RISK_SCORE_INFO)
    jiaoda_risk_score.value = [RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 现场检查较大问题质量分
    xc_jiaoda_risk_score = CommonCalcDataType(*XC_JIAODA_RISK_SCORE_INFO)
    xc_jiaoda_risk_score.value = [XIANCHENG_RISK_LEVEL_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅时长
    media_cost_time = CommonCalcDataType(*MEDIA_COST_TIME_INFO)
    media_cost_time.value = [MEDIA_COST_TIME_SQL.format('{0}', '{1}', CHECK_ITEM_IDS)]

    # 监控调阅发现问题数
    media_problem_number = CommonCalcDataType(*MEDIA_PROBLEM_NUMBER_INFO)
    media_problem_number.value = [MEDIA_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 监控调阅质量分
    media_problme_score = CommonCalcDataType(*MEDIA_PROBLME_SCORE_INFO)
    media_problme_score.value = [MEDIA_PROBLME_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # ---------考核力度-----------
    # 考核问题数
    assess_problem_count = CommonCalcDataType(*ASSESS_PROBLEM_INFO)
    assess_problem_count.value = [
        KAOHE_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 考核总金额
    assess_responsible = CommonCalcDataType(*ASSESS_RESPONSIBLE_INFO)
    assess_responsible.version = 'v2'
    assess_responsible.description = '月度考核总金额(关联风险)'
    assess_responsible.value = [ASSESS_RESPONSIBLE_SQL.format('{0}', '{1}', RISK_IDS)]

    # A, B, E1, E2问题
    award_return_money_problem_abe1e2 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_abe1e2.value = [
        AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'E1', 'E2'))]
    # A, B, E1, E2问题返奖数
    award_return_money_abe1e2 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_abe1e2.value = [
        REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('A', 'B', 'E1', 'E2'))]

    # C, E3问题
    award_return_money_problem_ce3 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_ce3.value = [
        AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('C', 'E3'))]
    award_return_money_problem_ce3.version = 'v2'
    award_return_money_problem_ce3.description = '返奖问题个数(C, E3)'
    # C, E3问题返奖数
    award_return_money_ce3 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_ce3.value = [
        REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('C', 'E3'))]
    award_return_money_ce3.version = 'v2'
    award_return_money_ce3.description = '实际返奖问题个数(C, E3)'

    # D, E4问题
    award_return_money_problem_de4 = CommonCalcDataType(*AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_problem_de4.value = [
        AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('D', 'E4'))]
    award_return_money_problem_de4.version = 'v3'
    award_return_money_problem_de4.description = '返奖问题个数(D, E4)'
    # D, E4问题返奖数
    award_return_money_de4 = CommonCalcDataType(*REAL_AWARD_RETURN_MONEY_PROBLEM_INFO)
    award_return_money_de4.value = [
        REAL_AWARD_RETURN_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS, ('D', 'E4'))]
    award_return_money_de4.version = 'v3'
    award_return_money_de4.description = '实际返奖问题个数(D, E4)'

    # ---------问题暴露度-----------
    # 一般及以上问题数
    above_yiban_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    above_yiban_problem_number.version = 'v2'
    above_yiban_problem_number.description = '一般及以上问题数'
    above_yiban_problem_number.value = [
        ABOVE_YIBAN_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上问题质量分
    above_yiban_problem_check_score = CommonCalcDataType(*PROBLEM_SCORE_INFO)
    above_yiban_problem_check_score.version = 'v2'
    above_yiban_problem_check_score.description = '一般及以上问题质量分'
    above_yiban_problem_check_score.value = [
        ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 总问题数
    all_problem_number = CommonCalcDataType(*ALL_PROBLEM_NUMBER_INFO)
    all_problem_number.value = [
        ALL_PROBLEM_NUMBER_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般以及以上作业项问题数
    above_yiban_zuoye_check_problem = CommonCalcDataType(*ZUOYE_PROBLEM_COUNT_INFO)
    above_yiban_zuoye_check_problem.version = 'v2'
    above_yiban_zuoye_check_problem.description = '一般以及以上作业项问题数'
    above_yiban_zuoye_check_problem.value = [ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL.format('{0}', '{1}', RISK_IDS)]

    # 一般及以上作业项问题质量分
    above_yiban_zuoye_problem_check_score = CommonCalcDataType(*ZUOYE_PROBLEM_CHECK_SCORE_INFO)
    above_yiban_zuoye_problem_check_score.version = 'v2'
    above_yiban_zuoye_problem_check_score.description = '一般及以上作业项问题质量分'
    above_yiban_zuoye_problem_check_score.value = [
        ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL.format('{0}', '{1}', RISK_IDS)]

    # 参与基数计算的sql字典
    child_index_sql_dict = {
        # 换算单位检查频次
        '1-2': (IndexDetails(check_count, worker_load_count),),

        # 现场检查问题查处率
        '1-3': (IndexDetails(site_problem, worker_load_count),),

        # 质量均分
        '1-5': (IndexDetails(problem_check_score, worker_load_count),),

        # 较大风险问题质量均分
        '1-6': (IndexDetails(jiaoda_risk_score, worker_load_count),
                IndexDetails(xc_jiaoda_risk_score, worker_load_count)),

        # 监控调阅力度
        '1-10': (IndexDetails(media_cost_time, worker_load_count),
                 IndexDetails(media_problem_number, worker_load_count),
                 IndexDetails(media_problme_score, worker_load_count)),

        # 视频查处问题率
        '1-18': (IndexDetails(video_problem, worker_load_count),),


        # 考核力度指数
        # 换算单位考核问题数
        '3-1': (
            IndexDetails(assess_problem_count, person_load),),
        # 换算单位考核金额
        '3-2': (
            IndexDetails(assess_responsible, person_load),),
        # 返奖率
        '3-3': (IndexDetails(award_return_money_abe1e2, award_return_money_problem_abe1e2),
                IndexDetails(award_return_money_ce3, award_return_money_problem_ce3),
                IndexDetails(award_return_money_de4, award_return_money_problem_de4)),
        # 问题暴露度指数
        # 普遍性暴露
        '5-1': (
            IndexDetails(all_problem_number, worker_load_count),
            IndexDetails(problem_check_score, worker_load_count),
            IndexDetails(above_yiban_problem_number, worker_load_count),
            IndexDetails(above_yiban_problem_check_score, worker_load_count),
        ),
    }

    calc_cardinal_number(months_ago,
                         risk_type,
                         ZHANDUAN_DPID_DATA,
                         DEPARTMENT_DATA,
                         child_index_sql_dict,
                         base_unit_info_sql, __package__)

    # 设置对应的全局变量
    GLV.set_value('stats_check_per_person', child_index_sql_dict['1-2'])
    GLV.set_value('stats_check_problem_ratio', child_index_sql_dict['1-3'])
    GLV.set_value('stats_check_video_problem_ratio', child_index_sql_dict['1-18'])
    GLV.set_value('stats_risk_score_per_person', child_index_sql_dict['1-6'])
    GLV.set_value('stats_score_per_person', child_index_sql_dict['1-5'])
    GLV.set_value('stats_media_intensity', child_index_sql_dict['1-10'])
    GLV.set_value('stats_check_problem_assess_radio', child_index_sql_dict['3-1'])
    GLV.set_value('stats_assess_money_per_person', child_index_sql_dict['3-2'])
    GLV.set_value('stats_award_return_ratio', child_index_sql_dict['3-3'])
    GLV.set_value('stats_total_problem_exposure', child_index_sql_dict['5-1'])


def _calc_total_work_load(
        _sqllist_numerator,
        zhanduan_dpid_data,
        department_data,
        mon_ago, column, dpids):
    stats_month = get_custom_month(mon_ago)
    begin_date = int(stats_month[0][:4] + stats_month[0][5:7] + stats_month[0][8:10])
    end_date = int(stats_month[1][:4] + stats_month[1][5:7] + stats_month[1][8:10])
    # 装卸车数
    load_and_unload_count = pd_query(_sqllist_numerator[0].format(begin_date, end_date), db_name='db_mid')
    # 装卸车数关联部门
    unit_associate_department = df_merge_with_dpid(
        pd_query(_sqllist_numerator[1]),
        department_data
    )
    # 自装占比及换算货场数
    management = pd_query(MANAGEMENT_INDEX_SQL)
    data = calc_huozhuang_count(load_and_unload_count, unit_associate_department, department_data, management)
    data = data.groupby(['TYPE3'])['COUNT'].sum().reset_index()
    data.rename(columns={'TYPE3': 'DEPARTMENT_ID', 'COUNT': column}, inplace=True)
    data = data[data['DEPARTMENT_ID'].isin(dpids)]
    return data
