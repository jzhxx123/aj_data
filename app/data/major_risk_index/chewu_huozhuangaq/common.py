# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     common
   Author :       hwj
   date：          2019/8/9上午9:29
   Change Activity: 2019/8/9上午9:29
-------------------------------------------------
"""
import pandas as pd

from app.data.index.util import get_custom_month
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import BANZU_POINT_SQL, CHECK_POINT_SQL
from app.data.major_risk_index.util import write_export_basic_data_to_mongo, calc_child_index_type_sum, \
    df_merge_with_dpid, append_major_column_to_df, format_export_basic_data, calc_extra_child_score_groupby_major, \
    calc_extra_child_score_groupby_major_third, summizet_operation_set
from app.data.util import pd_query, get_history_months
from app.data.workshop_health_index.cache.cache import get_cache_client

cache_client = get_cache_client(__package__)
HIERARCHY = [3]


def get_vitual_major_ids(risk_type):
    """
    获取类似工电段的虚拟专业ids（主工电段组成）
    :param risk_type:
    :return:
    """
    belong_profession_id = {"客运": 898}
    major = risk_type.split('-')[0]
    profession_dictionary_id = belong_profession_id.get(major, 898)
    get_vm_majors_ids_sql = """
    SELECT 
        DEPARTMENT_ID
    FROM
        t_department
    WHERE
        TYPE = 4
        AND 
        SHORT_NAME != ""
        AND 
        BELONG_PROFESSION_ID in ({0})
    """
    major_ids = pd_query(get_vm_majors_ids_sql.format(profession_dictionary_id))
    return tuple(major_ids['DEPARTMENT_ID'].values.tolist())


def calc_huozhuang_count(load_and_unload_count, unit_associate_department, department_data, management):
    """"计算换算装卸车数量(即工作量)
    """
    data = pd.merge(
        load_and_unload_count,
        unit_associate_department,
        how='inner',
        left_on='UNIT',
        right_on='UNIT'
    )
    data.drop_duplicates(subset={'UNIT', 'TYPE3'}, inplace=True)

    data = data.groupby(['TYPE3'])['CW_COUNT'].sum().reset_index()
    data = pd.merge(
        data,
        management,
        how='inner',
        left_on='TYPE3',
        right_on='FK_DEPARTMENT_ID'
    )
    data['CONVERT_COUNT'] = data.apply(
        lambda row: (0.7 * row['CW_COUNT'] * row['SELF_MANAGEMENT'] + 30 * row['CW_COUNT']) / 100, axis=1
    )
    data['COUNT'] = data.apply(
        lambda row: 0.5 * row['GOOD_MANAGEMENT'] / 30 + 0.5 * row['CONVERT_COUNT'] / 40000, axis=1
    )
    data.drop('TYPE3', inplace=True, axis=1)
    data = df_merge_with_dpid(data, department_data)
    return data


def _cal_check_banzu_evenness_score(row, columns):
    """计算逻辑：
    基准值（站段级）=检查总人次/当月实际天数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数20%的扣1分/日，低于50%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 计算基准值:
    total_check_count = sum([row[day] for day in columns])
    total_banzu_count = sum([row[f'banzu_count_{day}'] for day in columns])
    avg_check_count = total_check_count / total_banzu_count

    # 保存中间计算过程
    unfinished_day = {
        '检查数低于比较值基数20%的日期': [],
        '检查数低于比较值基数50%的日期': [],
        '检查数低于比较值基数100%的日期': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for day in columns:
        day_banzu_count = row[f'banzu_count_{day}']  # 日班组数
        day_check_count = row[day]  # 日检查数
        base_check_count = day_banzu_count * avg_check_count
        if base_check_count == 0:
            continue
        ratio = (day_check_count - base_check_count) / base_check_count
        if ratio >= -0.2:
            daily_deduction = 0
        else:
            if ratio <= -1:
                daily_deduction = -3
                deduct_type = '检查数低于比较值基数100%的日期'
            elif ratio <= -0.5:
                daily_deduction = -2
                deduct_type = '检查数低于比较值基数50%的日期'
            elif ratio <= -0.2:
                daily_deduction = -1
                deduct_type = '检查数低于比较值基数20%的日期'
            deduct_day = unfinished_day.get(deduct_type)
            deduct_day.append(f'{day}号[基准值：{avg_check_count:.1f},' +
                              f'班组数：{day_banzu_count:.1f}, ' +
                              f'比较值：{base_check_count:.1f}，' +
                              f'实际检查值：{day_check_count:.1f}]')
            unfinished_day.update({deduct_type: deduct_day})
        score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_day.items()])
    return total_score, rst_calc_data


# 计算检查点扣分
def _calc_score_by_formula(row, column, zhanduan_column, all_name):
    if row[all_name] == 0:
        return 0
    if row[column] == 0:
        return -5
    if row[zhanduan_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[zhanduan_column]) / row[zhanduan_column]
        if _ratio >= 4:
            return -2
        elif _ratio <= -0.5:
            return -2
        else:
            return 0


def _calc_score_by_formula_exposure(row, column, major_column, detail_type=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 60 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_avg_check_count_by_zhanduan(data):
    """基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    地点工作人数=该地点工作的货装人数
    应检查值=地点作业人数*基准值
    """
    # 计算站段总工作人数(作业人数PERSON_NUMBER为0时补充为部门人数PERSON_COUNT)
    data['PERSON_NUMBER'] = data.apply(
        lambda row: row['PERSON_NUMBER'] if row['PERSON_NUMBER'] > 0 else row['PERSON_COUNT'], axis=1)
    zhanduan_person_number = data.groupby(['TYPE3'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        zhanduan_person_number.to_frame(name='SUM_PERSON_NUMBER'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 每个地点工作人数(不同班组聚合)
    address_data = data.groupby(['ADDRESS_NAME'])['PERSON_NUMBER'].sum()
    data = pd.merge(
        data,
        address_data.to_frame(name='ADDRESS_PERSON_NUMBER'),
        how='left',
        left_on='ADDRESS_NAME',
        right_index=True)
    data.drop_duplicates(subset=['ADDRESS_NAME'], keep='first', inplace=True)

    # 计算站段总检查次数
    zhanduan_check_count = data.groupby(['TYPE3'])['CHECK_COUNT'].sum()
    data = pd.merge(
        data,
        zhanduan_check_count.to_frame(name='SUM_CHECK_COUNT'),
        how='left',
        left_on='TYPE3',
        right_index=True)
    # 计算基准值（站段级）=检查人次*地点/Σ应检查地点工作人数
    data['BASE_CHECK_COUNT'] = data.apply(
        lambda row: row['SUM_CHECK_COUNT'] / row['SUM_PERSON_NUMBER'], axis=1)
    # 计算应检查值=地点作业人数*基准值
    # data['AVG_CHECK_COUNT'] = data.apply(
    #     lambda row: row['BASE_CHECK_COUNT'] * row['PERSON_COUNT'], axis=1)
    # 比较值= 该点工作量(人数)*∑每个地点受检次数/∑每个地点工作量(人数)
    data['AVG_CHECK_COUNT'] = data.apply(
        lambda row: row['ADDRESS_PERSON_NUMBER'] * row['SUM_CHECK_COUNT'] / row['SUM_PERSON_NUMBER'], axis=1)
    return data


def _export_calc_address_evenness_data(data, months_ago, risk_type):
    """将检查地点数据中间统计结果导出

    Arguments:
        data {pandas.DataFrame} -- 统计数据
        months_ago {int} -- 前第 -N 个月
    """
    calc_data = {}
    major_data = {}
    for idx, row in data.iterrows():
        avg_check_count = row['AVG_CHECK_COUNT']
        real_check_count = row['COUNT']
        base_check_count = row['BASE_CHECK_COUNT']
        work_load = row['WORK_COUNT']
        point_name = row['ALL_NAME']
        dpid = row['TYPE3']
        # 保存各部门的专业
        if dpid not in major_data:
            major_data.update({dpid: row['MAJOR']})
        cnt = {}
        if dpid in calc_data:
            cnt = calc_data.get(dpid)
        cnt.update({'总检查地点数：': cnt.get('总检查地点数：', 0) + 1})
        if isinstance(point_name, int):
            cnt.update({'总检查地点数：': 0})
            calc_data.update({dpid: cnt})
            continue
        if avg_check_count == 0:
            continue
        check_desc = (f'{point_name}站(比较值：{avg_check_count:.2f}, '
                      f'受检次数：{real_check_count:.0f}, '
                      f'基准值：{base_check_count:.4f}, '
                      f'工作量：{work_load:.0f})')
        if int(real_check_count) == 0:
            cnt.update({
                '未检查地点：': cnt.get('未检查地点：', '') + '<br/>' + check_desc
            })
        else:
            _ratio = (real_check_count - avg_check_count) / avg_check_count
            if _ratio >= 4:
                cnt.update({
                    '受检次数超过比较值400%以上地点：':
                        cnt.get('受检次数超过比较值400%以上地点：', '') + '<br/>' + check_desc
                })
            elif _ratio <= -0.5:
                cnt.update({
                    '受检次数低于比较值50%地点：':
                        cnt.get('受检次数低于比较值50%地点：', '') + '<br/>' + check_desc
                })
        calc_data.update({dpid: cnt})
    rst_data = []
    mon = get_history_months(months_ago)[0]
    for dpid, cnt in calc_data.items():
        rst_data.append({
            'TYPE':
                2,
            'INDEX_TYPE':
                int(risk_type.split('-')[1]),
            'MAIN_TYPE':
                4,
            'DETAIL_TYPE':
                3,
            'MON':
                mon,
            'DEPARTMENT_ID':
                dpid,
            'HIERARCHY':
                3,
            'MAJOR':
                major_data[dpid],
            'CONTENT':
                ';<br/>'.join([f'{x}{y}' for x, y in cnt.items()]),
        })
    write_export_basic_data_to_mongo(
        rst_data, months_ago, 3, 4, 3, risk_type=risk_type)


# 检查地点均衡度
def stats_check_address_evenness(check_item_ids, work_load,
                                 banzu_department_checked_count_sql,
                                 department_data, zhanduan_dpid_data,
                                 months_ago, risk_type, choose_dpid_data):
    stats_month = get_custom_month(months_ago)
    work_load.rename(columns={
        "COUNT": "WORK_COUNT",
    }, inplace=True)
    # 班组受检次数
    banzu_check = pd_query(
        banzu_department_checked_count_sql.format(*stats_month,
                                                  check_item_ids))
    data_check_banzu = pd.merge(
        work_load,
        banzu_check,
        how='left',
        left_on='DEPARTMENT_ID',
        right_on='DEPARTMENT_ID')
    data_check_banzu.fillna(0, inplace=True)
    zhanduan_count = data_check_banzu.groupby(['TYPE3'])['COUNT'].sum().to_frame(name='zhanduan_count')
    zhanduan_work_count = data_check_banzu.groupby(['TYPE3'])['WORK_COUNT'].sum().to_frame(name='zhanduan_work_count')
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu = pd.merge(
        data_check_banzu,
        zhanduan_work_count,
        how='left',
        left_on='TYPE3',
        right_index=True
    )
    data_check_banzu.drop(['DEPARTMENT_ID', 'NAME', "TYPE"], inplace=True, axis=1)
    data = pd.merge(
        data_check_banzu,
        zhanduan_dpid_data,
        how='right',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID')
    data.drop(['TYPE3'], inplace=True, axis=1)
    data.rename(columns={'DEPARTMENT_ID': 'TYPE3'}, inplace=True)
    data.fillna(0, inplace=True)

    # 计算基准值
    data['BASE_CHECK_COUNT'] = data.apply(lambda row: (row['zhanduan_count'] / row['zhanduan_work_count'])
    if row['zhanduan_work_count'] > 0 else 0, axis=1)
    # 计算比较值=地点工作量*基准值
    data['AVG_CHECK_COUNT'] = data.apply(lambda row: row['WORK_COUNT'] * row['BASE_CHECK_COUNT'], axis=1)

    data = data.fillna(0)
    # 导出中间计算过程
    _export_calc_address_evenness_data(data.copy(), months_ago, risk_type)
    data['DEDUCT_SCORE'] = data.apply(
        lambda row: _calc_score_by_formula(row, 'COUNT', 'AVG_CHECK_COUNT', 'ALL_NAME'), axis=1)
    rst_index_score = calc_child_index_type_sum(
        data,
        2,
        4,
        3,
        months_ago,
        'DEDUCT_SCORE',
        'SCORE_c',
        lambda x: 0 if 0 > (x + 100) else (x + 100),
        choose_dpid_data,
        risk_type=risk_type)
    return rst_index_score


def _calc_value_per_person(series,
                           work_load,
                           weight,
                           hierarchy,
                           choose_dpid_data,
                           calc_score_formula=None,
                           is_calc_score_base_major=False,
                           fraction=None):
    data = pd.concat(
        [series.to_frame(name='prob'), work_load], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    if calc_score_formula is None:
        calc_score_formula = _calc_score_by_formula_exposure
    if not is_calc_score_base_major:
        return calc_extra_child_score_groupby_major(
            data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight)
    return calc_extra_child_score_groupby_major_third(
        data, choose_dpid_data(hierarchy), 'ratio', calc_score_formula, weight=weight,
        numerator='prob', denominator='PERSON_NUMBER', fraction=fraction)


def _calc_basic_prob_number_per_person(df_data, work_load, department_data, i,
                                       title):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby(['TYPE3']).size()
    data = pd.concat(
        [prob_number.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, work_load, department_data, i,
                                      title):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby(['TYPE3'])['CHECK_SCORE'].sum()
    data = pd.concat(
        [prob_score.to_frame(name='prob'), work_load], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(
            f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_prob_number_per_person(df_data,
                                 work_load,
                                 department_data,
                                 choose_dpid_data,
                                 weight,
                                 hierarchy,
                                 calc_score_formula=None,
                                 is_calc_score_base_major=False,
                                 fraction=None):
    prob_number = df_merge_with_dpid(df_data, department_data)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major,
                                  fraction=fraction)


def _calc_prob_score_per_person(df_data,
                                work_load,
                                department_data,
                                choose_dpid_data,
                                weight,
                                hierarchy,
                                calc_score_formula=None,
                                is_calc_score_base_major=False,
                                fraction=None):
    prob_score = df_merge_with_dpid(df_data, department_data)
    prob_score = prob_score.groupby([f'TYPE{hierarchy}'])['CHECK_SCORE'].sum()
    return _calc_value_per_person(prob_score, work_load, weight, hierarchy,
                                  choose_dpid_data, calc_score_formula,
                                  is_calc_score_base_major=is_calc_score_base_major,
                                  fraction=fraction)


# 总体暴露度
def stats_total_problem_exposure_type_chewu(
        check_item_ids, check_problem_sql, all_load, zuoye_load,
        department_data, months_ago, risk_type, choose_dpid_data,
        title=None, is_calc_score_base_major=True,
        weight_item=[0.3, 0.3, 0.2, 0.2],
        weight_part=[0.4, 0.6],
        fraction_list=None):
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(
        check_problem_sql.format(*stats_month, check_item_ids))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    if not title:
        title = [
            '总问题数({0})/工作量({1})', '一般及以上问题数({0})/工作量({1})']
    # 导出中间过程
    work_load = [all_load, zuoye_load]
    for i, data in enumerate(
            [base_data, risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), work_load[i // 2], department_data, i,
                     title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=risk_type)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=risk_type)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate(
                    [_calc_prob_number_per_person, _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(
                    func(data.copy(), work_load[i // 2], department_data,
                         choose_dpid_data, weight, hierarchy, fraction=fraction_list[i][j],
                         is_calc_score_base_major=is_calc_score_base_major))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(
            df_rst,
            choose_dpid_data(hierarchy),
            column,
            hierarchy,
            2,
            5,
            1,
            months_ago,
            risk_type=risk_type)
        rst_child_score.append(df_rst)
    return rst_child_score


def get_huo_zhuang_check_address_standard_data(work_banzu_info_data,
                                               department_data,
                                               months_ago,
                                               check_item_ids,
                                               major):
    """[将覆盖率数据转成标准格式]
    类型：检查地点数（实际检查班组+实际重要检查点）/地点总数（班组数+重要检查点）
    班组跟重要检查点的工作项目要复合特定指数检查项目
    """
    # 实际检查班组数
    check_info_and_address = cache_client.get(
        'check_info_and_address', months_ago=months_ago)
    real_check_banzu_data = pd.merge(check_info_and_address, department_data, left_on='ADDRESS_FK_DEPARTMENT_ID',
                                     right_on='DEPARTMENT_ID')
    del check_info_and_address
    real_check_banzu_data = real_check_banzu_data.groupby(
        'ADDRESS_FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_banzu_data = real_check_banzu_data.reset_index().rename(
        columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})
    real_check_banzu_data['COUNT'] = 1
    real_check_banzu_data = pd.merge(
        real_check_banzu_data,
        work_banzu_info_data[['FK_DEPARTMENT_ID']],
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    # 删除重复班组id
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    banzu_point_data = work_banzu_info_data
    filled_data = pd_query(BANZU_POINT_SQL.format(major))
    filled_data['COUNT'] = 0
    if real_check_banzu_data.empty:
        # 两个均为空时，补充检查班组数
        real_check_banzu_data = filled_data
    if banzu_point_data.empty:
        # 两个均为空时，补充符合某工作项班组数
        banzu_point_data = filled_data
    return real_check_banzu_data, banzu_point_data
