# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     check_evenness
   Author :       hwj
   date：          2019/8/9下午4:28
   Change Activity: 2019/8/9下午4:28
-------------------------------------------------
"""
import pandas as pd
from flask import current_app

from app.data.index.util import get_custom_month, get_query_condition_by_risktype, get_month_day
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.check_evenness_sql import DAILY_CHECK_COUNT_SQL, HUOZHUANG_NUMBER_SQL, \
    BANZU_DEPARTMENT_CHECKED_COUNT_SQL, GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL, \
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL
from app.data.major_risk_index.chewu_huozhuangaq.common import stats_check_address_evenness
from app.data.major_risk_index.common import check_evenness
from app.data.major_risk_index.common.check_evenness_sql import CHECK_BANZU_COUNT_SQL, CHECK_POINT_COUNT_SQL, \
    CHECK_POINT_CHECKED_COUNT_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid, combine_child_index_func, summizet_child_index, \
    append_major_column_to_df, format_export_basic_data, write_export_basic_data_to_mongo, summizet_operation_set
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column):
    if row[column] == 0:
        return -1
    if row[major_column] == 0:
        return -2
    else:
        _ratio = (row[column] - row[major_column]) / row[major_column]
        if _ratio <= -1:
            return -3
        elif _ratio <= -0.6:
            return -2
        elif _ratio <= -0.3:
            return -1
        else:
            return 0


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_major_dpid(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT, WORK_LOAD, \
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, DAILY_CHECK_COUNT_DATA
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    global CHECK_ITEM_IDS, CHECK_RISK_IDS
    CHECK_ITEM_IDS = get_query_condition_by_risktype(risk_name)[0]
    CHECK_RISK_IDS = get_query_condition_by_risktype(risk_name)[1]

    # 统计工作量【接发列车工作量】
    WORK_LOAD = GLV.get_value('WORK_LOAD')

    # 一般以上项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(
                *stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA)

    # 基础问题库中一般及以上风险项点问题数
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT = df_merge_with_dpid(
        pd_query(
            GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL.format(
                CHECK_RISK_IDS)), DEPARTMENT_DATA)
    # 每日检查数
    DAILY_CHECK_COUNT_DATA = df_merge_with_dpid(
        pd_query(DAILY_CHECK_COUNT_SQL.format(*stats_month, CHECK_ITEM_IDS)),
        DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 问题均衡度
def _stats_problem_point_evenness(months_ago):
    """查出一般以上问题项点数/基础问题库中一般以上问题项点数"""
    return check_evenness.stats_problem_point_evenness(
        GENERALLY_ABOVE_PROBLEM_POINT_COUNT,
        GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT, months_ago,
        RISK_TYPE, _choose_dpid_data)


def _cal_check_banzu_evenness_score(row, columns, months_days):
    """计算逻辑：
    基准值（站段级）=Σ日检查次数/当月天数
    应检查值=当日作业班组数*基准值
    结果=（实际受检-应受检）/ 应受检
    低于基数30%的扣1分/日，低于60%的扣2分/日，
    低于100%的扣3分/日，得分=100-扣分。]
    并且返回中间计算结果的统计
    Arguments:
        row {[pandas.core.series.Series]} -- [站段一个月检查班组数/日和次数/日]]
        columns {[list]} -- [所有日期列]]
    """
    # 基准值:
    avg_check_count = row['AVG_NUMBER']  # 日均数
    # 检查总次数
    total_check_count = row['TOTAL_CHECK_COUNT']
    # 保存中间计算过程
    unfinished_day = {
        '检查数低于比较值基数30%的日期': [],
        '检查数低于比较值基数60%的日期': [],
        '检查数低于比较值基数100%的日期': [],
    }
    score = [100]
    # 按天依次计算每天的检查是否达标和对应扣分
    for day in columns:
        day_check_count = row[day]  # 日检查数

        ratio = (day_check_count - avg_check_count) / avg_check_count
        if ratio <= -0.3:
            if ratio <= -1:
                daily_deduction = -3
                deduct_type = '检查数低于比较值基数100%的日期'
            elif ratio <= -0.6:
                daily_deduction = -2
                deduct_type = '检查数低于比较值基数60%的日期'
            else:
                daily_deduction = -1
                deduct_type = '检查数低于比较值基数30%的日期'
            deduct_day = unfinished_day.get(deduct_type)
            deduct_day.append(f'{day}号[基准值：{avg_check_count:.2f},' +
                              f'实际检查值：{day_check_count:.0f},' +
                              f'检查总次数：{total_check_count:.0f},' +
                              f'当月实际天数：{months_days:.0f}]')
            unfinished_day.update({deduct_type: deduct_day})
            score.append(daily_deduction)
    total_score = sum(score)
    total_score = 0 if total_score < 0 else total_score
    rst_calc_data = '<br/>'.join(
        [f'{k}: <br/>{"<br/>".join(v)}' for k, v in unfinished_day.items()])
    return total_score, rst_calc_data


# 检查时间均衡度
def _stats_check_time_evenness(months_ago):
    """基数=检查总人次/当月实际天数。低于基数30%的扣1分/日，低于60%的扣2分/日，低于100%的扣3分/日，得分=100-扣分"""
    rst_index_score = []
    months_days = get_month_day(months_ago)
    day_count = DAILY_CHECK_COUNT_DATA.groupby(['TYPE3', 'DAY'])['COUNT'].sum().unstack()
    columns = day_count.columns.values
    day_count['TOTAL_CHECK_COUNT'] = day_count.apply(lambda row: sum(row), axis=1)
    day_count['AVG_NUMBER'] = day_count.apply(lambda row: row['TOTAL_CHECK_COUNT']/months_days, axis=1)
    column = 'SCORE_b_3'
    day_count[column] = day_count.apply(
        lambda row: _cal_check_banzu_evenness_score(row, columns, months_days)[0], axis=1)
    day_count['CONTENT'] = day_count.apply(
        lambda row: _cal_check_banzu_evenness_score(row, columns, months_days)[1], axis=1)
    data = append_major_column_to_df(ZHANDUAN_DPID_DATA, day_count)
    calc_basic_data_rst = format_export_basic_data(data.copy(), 4, 2, 3,
                                                   months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 4,
                                     2, risk_type=RISK_TYPE)

    data = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, column].values,
        columns=[column])
    summizet_operation_set(data, _choose_dpid_data(3), column,
                           3, 2, 4, 2, months_ago, risk_type=RISK_TYPE)
    rst_index_score.append(data)
    return rst_index_score


# 检查地点均衡度
def _stats_check_address_evenness(months_ago):
    """受检地点超过平均值400%以上的一处扣2分;受检地点低于基数50%的一处扣2分，未检查的一处扣5分
    工作量=系统管理-组织结构-专业特殊属性-货装人数
    """
    work_load = df_merge_with_dpid(
        pd_query(HUOZHUANG_NUMBER_SQL.format(CHECK_ITEM_IDS)), DEPARTMENT_DATA)
    return stats_check_address_evenness(
        CHECK_ITEM_IDS, work_load,
        BANZU_DEPARTMENT_CHECKED_COUNT_SQL,
        DEPARTMENT_DATA, ZHANDUAN_DPID_DATA, months_ago, RISK_TYPE, _choose_dpid_data)


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)

    # 分别代表【问题均衡度、检查时间均衡度、地点均衡度】
    child_index_func = [
        _stats_problem_point_evenness,
        _stats_check_time_evenness,
        _stats_check_address_evenness
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c']]
    item_weight = [0.15, 0.5, 0.35]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        4,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(index_type=8, major=risk_type, main_type=4, child_index_list=[1, 2, 3],
                                 child_index_weight=item_weight
                                 )

    current_app.logger.debug(
        '├── └── check_evenness index has been figured out!')


if __name__ == '__main__':
    pass
