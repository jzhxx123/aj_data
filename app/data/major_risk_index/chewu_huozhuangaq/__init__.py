# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     __init__.py
   Author :       hwj
   date：          2019/8/9上午9:23
   Change Activity: 2019/8/9上午9:23
-------------------------------------------------
"""

from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import (check_intensity, check_evenness, problem_exposure, assess_intensity,
               problem_rectification,
               combine_child_index, init_common_data)
