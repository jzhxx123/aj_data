# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/8/9上午9:26
   Change Activity: 2019/8/9上午9:26
-------------------------------------------------
"""
import pandas as pd
from app.data.index.util import get_custom_month
from app.data.major_risk_index.chewu_huozhuangaq import GLV
from app.data.major_risk_index.chewu_huozhuangaq.common import get_vitual_major_ids, calc_huozhuang_count
from app.data.major_risk_index.chewu_huozhuangaq.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, \
    LOAD_AND_UNLOAD_COUNT_SQL, UNIT_ASSOCIATE_DEPARTMENT_SQL, \
    MANAGEMENT_INDEX_SQL
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """
    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids('客运-1')
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(
        major, ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(
        major, ids))
    department_data = pd_query(DEPARTMENT_SQL.format(
        major, ids))

    begin_date = int(stats_month[0][:4] + stats_month[0][5:7] + stats_month[0][8:10])
    end_date = int(stats_month[1][:4] + stats_month[1][5:7] + stats_month[1][8:10])
    # 装卸车数
    load_and_unload_count = pd_query(LOAD_AND_UNLOAD_COUNT_SQL.format(begin_date, end_date), db_name='db_mid')
    # 装卸车数关联部门
    unit_associate_department = df_merge_with_dpid(
        pd_query(UNIT_ASSOCIATE_DEPARTMENT_SQL),
        department_data
        )
    # 自装占比及换算货场数
    management = pd_query(MANAGEMENT_INDEX_SQL)

    work_load = calc_huozhuang_count(load_and_unload_count, unit_associate_department, department_data, management)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'WORK_LOAD': work_load
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
