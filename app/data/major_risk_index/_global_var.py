#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
全局变量，类变量存储常用df
"""
import logging
logging.basicConfig(level = logging.INFO,format = '%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

class GlobalVar:
    
    global_data = dict()


    def __init__(self, module):
        self.module = module
        logger.debug(f'|   └── {module} data init begin!')


    def set_all_values(self, values):
        GlobalVar.global_data[self.module] = values
        logger.debug(f'|   └── {self.module} data init finish!')


    def get_value(self, key, default=None):
        value = None
        try:
            value = GlobalVar.global_data.get(self.module)[key]
        except Exception as e:
            value = default
            logger.error(f'|   └── {self.module} get {key} error! \n' \
                + f' detail reason : {e}')
        return value

    def set_value(self, key, value):
        GlobalVar.global_data[self.module][key] = value

    def rm_value(self, key):
        GlobalVar.global_data[self.module].pop(key)
        logger.debug(f'|   └── {self.module} clear {key} data!')
    
    def rm_all_values(self):
        GlobalVar.global_data.pop(self.module)
        logger.debug(f'|   └── {self.module} clear this module data')
