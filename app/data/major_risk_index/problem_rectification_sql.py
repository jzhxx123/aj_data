# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT 
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE_STANDARD)*0.2 AS SCORE
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.FK_CHECK_ITEM_ID IN ({2})
"""