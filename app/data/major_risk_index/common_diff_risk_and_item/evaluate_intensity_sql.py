#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   evaluate_intensity_sql
Description:
Author:    
date:         2019-06-25
-------------------------------------------------
Change Activity:2019-06-25 18:58
-------------------------------------------------
"""
# 干部人数
CADRE_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, COUNT(distinct(a.ID_CARD)) AS COUNT
    FROM
        t_person AS a
            INNER JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.IS_DELETE = 0
            AND a.POSITION IN ({0})
            AND b.TYPE2 = '{1}'
            AND a.IDENTITY = '干部'
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 干部评价记分总条数
# 筛选项:时间:系统月、附标:不含JL-2-1条目、
# 风险类型:“车务-调车-%”“车务-防溜-%”“车务-调乘一体化-%”
EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 干部主动评价记分总条数
# 筛选项:时间:系统月、附标: 主动评价不含事故故障倒查和JL-2-1条目及系统自动评价、
# 风险类型:“车务-调车-%”“车务-防溜-%”“车务-调乘一体化-%”。”、
# 检查类型:站段检查、是否自动评价:否
ACTIVE_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# 干部主动评价记分总分数
ACTIVE_EVALUATE_SCORE_SQL = """
select aess.FK_DEPARTMENT_ID, sum(aess.COUNT) AS COUNT
from
(
SELECT
       distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.SCORE AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
            ) as aess
    GROUP BY aess.FK_DEPARTMENT_ID
"""

# 科职主动评价记分条数(科职干部包含正科、副科职干部)
ACTIVE_KEZHI_EVALUATE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 2 AND 3
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID
"""

# （主动）段机关干部评价记分条数
DUAN_CADRE_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_check_evaluate_and_risk AS d
            ON d.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
        a.CODE_ADDITION != 'JL-2-1'
            AND a.`CODE` NOT LIKE 'SZ-%%'
            AND a.CHECK_TYPE = 2
            AND a.EVALUATE_WAY > 0
            AND a.FK_PERSON_GRADATION_RATIO_ID BETWEEN 1 AND 4
            AND b.IDENTITY = '干部'
            AND c.TYPE = 7
            AND d.FK_RISK_ID IN ({2})
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
                AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                    <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID
"""


# 分析中心得分
ANALYSIS_CENTER_ASSESS_SQL = """SELECT
        a.FK_DEPARTMENT_ID, b.GRADES_TYPE, b.ACTUAL_SCORE
    FROM
        t_analysis_center_assess AS a
            inner JOIN
        t_analysis_center_assess_detail AS b
            ON a.PK_ID = b.FK_ANALYSIS_CENTER_ASSESS_ID
    WHERE
        a.YEAR = {0}
            AND a.MONTH = {1}
            AND a.STATUS IN (1, 3)
            AND a.TYPE = 3
            AND b.ACTUAL_SCORE > 0
    ORDER BY a.FK_DEPARTMENT_ID;
"""