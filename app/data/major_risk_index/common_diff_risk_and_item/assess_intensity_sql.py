#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   assess_intensity_sql
Description:
Author:    
date:         2019-06-25
-------------------------------------------------
Change Activity:2019-06-25 18:31
-------------------------------------------------
"""
# 考核作业项问题数
KAOHE_PROBLEM_SQL = """SELECT
        MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER join
        t_person as c on a.CHECK_PERSON_ID_CARD = c.ID_CARD
            inner join
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_EXTERNAL = 0
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2}) 
    GROUP BY a.PK_ID;
"""

# 考核问题数（基本问题库）
KAOHE_PROBLEM_BASE_SQL = """
    SELECT 
    max(d.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_info as e on a.FK_CHECK_INFO_ID = e.PK_ID
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_risk AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department as d on a.pk_id = d.FK_CHECK_PROBLEM_ID
WHERE
    (b.IS_PROFESSION_ASSESS = 1
        OR b.IS_UNIT_ASSESS = 1)
    AND e.CHECK_WAY NOT BETWEEN 5 AND 6
    AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND e.CHECK_TYPE NOT IN (102, 103)
    AND a.IS_EXTERNAL = 0
    AND
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    AND c.FK_RISK_ID in ({2})
    GROUP BY a.PK_ID
"""

# 月度考核总金额(关联问题)
ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            INNER JOIN
        t_check_info as e on d.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND d.IS_EXTERNAL = 0
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 月度考核总金额(关联项目)
NORISK_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            INNER JOIN
        t_check_info as e on c.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.IS_EXTERNAL = 0
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 月度返奖金额
AWARD_RETURN_SQL = """
select ars.FK_DEPARTMENT_ID, SUM(ars.COUNT) as COUNT
from (
SELECT
        distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            inner JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID 
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            INNER JOIN
        t_check_info as e on d.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND a.IS_RETURN = 1
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND d.IS_EXTERNAL = 0
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as ars
    GROUP BY ars.FK_DEPARTMENT_ID
"""

# 月度返奖金额(非干部)
NOGANBU_AWARD_RETURN_SQL = """
select nars.FK_DEPARTMENT_ID, sum(nars.count) AS COUNT
from
(
SELECT
       distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as f on a.FK_CHECK_PROBLEM_ID = f.PK_ID
            INNER JOIN
        t_check_info as e on f.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND a.IS_RETURN = 1
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY <> '干部'
        AND c.FK_RISK_ID IN ({2})
        AND f.IS_EXTERNAL = 0
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as nars
    GROUP BY nars.FK_DEPARTMENT_ID
"""

# 月度返奖金额(非干部)
GANBU_AWARD_RETURN_SQL = """
select nars.FK_DEPARTMENT_ID, sum(nars.count) AS COUNT
from
(
SELECT
       distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as f on a.FK_CHECK_PROBLEM_ID = f.PK_ID
            INNER JOIN
        t_check_info as e on f.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
        AND a.IS_RETURN = 1
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.IDENTITY = '干部'
        AND c.FK_RISK_ID IN ({2})
        AND f.IS_EXTERNAL = 0
        AND e.CHECK_WAY NOT BETWEEN 5 AND 6
        AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND e.CHECK_TYPE NOT IN (102, 103)
        ) as nars
    GROUP BY nars.FK_DEPARTMENT_ID
"""

# 月度返奖金额(关联项目)
NORISK_AWARD_RETURN_SQL = """
select ars.FK_DEPARTMENT_ID, SUM(ars.COUNT) as COUNT
from (
SELECT
        distinct(a.pk_id), b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            inner JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID 
            inner join
        t_check_info as d on c.FK_CHECK_INFO_ID = d.PK_ID
    WHERE
        b.STATUS = 3
        AND a.IS_RETURN = 1
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND c.IS_EXTERNAL = 0
        AND d.CHECK_WAY NOT BETWEEN 5 AND 6
        AND d.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND d.CHECK_TYPE NOT IN (102, 103)
        ) as ars
    GROUP BY ars.FK_DEPARTMENT_ID
"""

# 月度返奖金额(关联项目)
LEVEL_AWARD_RETURN_SQL = """
SELECT
        distinct  a.PK_ID, b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY, a.LEVEL, a.IS_RETURN
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
            inner join
        t_check_info as d on c.FK_CHECK_INFO_ID = d.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND c.IS_EXTERNAL = 0
            AND d.CHECK_WAY NOT BETWEEN 5 AND 6
            AND d.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND d.CHECK_TYPE NOT IN (102, 103)
"""

# 干部月度考核总金额(关联项目)
GANBU_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            inner JOIN
        t_person AS tp on a.ID_CARD = tp.ID_CARD
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            inner join 
        t_check_info as f on d.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND tp.IDENTITY = '干部'
        AND d.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""


# 干部月度考核总金额(关联项目)
FEIGANBU_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            inner JOIN
        t_person AS tp on a.ID_CARD = tp.ID_CARD
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            inner join 
        t_check_info as f on d.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND d.FK_CHECK_ITEM_ID IN ({2})
        AND tp.IDENTITY  <> '干部'
        AND d.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 实际返奖错误操纵问题个数
ACTUAL_RETURN_MONTY_PROBLEM_SQl = """
SELECT
        MAX(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            INNER JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
            INNER JOIN
        t_check_problem_and_risk e on a.FK_CHECK_PROBLEM_ID = e.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            inner join
        t_check_info as f on c.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0} AND b.MONTH = {1}
        AND a.IS_RETURN = 1
        AND a.ACTUAL_MONEY > 0
        AND e.FK_RISK_ID in ({2})
        AND c.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        group by a.FK_CHECK_PROBLEM_ID
"""

# 达到返奖时限错误操纵问题问题个数
RETURN_MONTY_PROBLEM_SQL = """
SELECT
        MAX(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            INNER JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            INNER JOIN
        t_person AS d ON a.ID_CARD = d.ID_CARD
            INNER JOIN
        t_check_problem_and_risk e on a.FK_CHECK_PROBLEM_ID = e.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.PK_ID
            inner join
        t_check_info as f on c.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0} AND b.MONTH = {1}
        AND a.MONEY > 0
        AND e.FK_RISK_ID in ({2})
        AND c.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        group by a.FK_CHECK_PROBLEM_ID
"""

# 月度返奖问题明细(关联项目)
AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
            inner join 
        t_check_info as e on c.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND a.LEVEL in {3}
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND c.IS_EXTERNAL = 0
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
"""

# 月度实际返奖问题明细(关联项目)
REAL_AWARD_RETURN_PROBLEM_SQL = """
SELECT
        b.FK_DEPARTMENT_ID, 1 AS COUNT
   FROM
        t_safety_award_responsible_return_detail AS a
            LEFT JOIN
        t_safety_award_responsible_return AS b 
            ON a.FK_SAFETY_AWARD_RESPONSIBLE_RETURN_ID = b.PK_ID
            LEFT JOIN
        t_check_problem as c on a.FK_CHECK_PROBLEM_ID = c.pk_id
            inner join
        t_check_info as e on c.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        b.STATUS = 3
            AND b.YEAR = {0} AND b.MONTH = {1} 
            AND a.IS_RETURN = 1
            AND a.ACTUAL_MONEY > 0
            AND a.LEVEL in {3}
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND c.IS_EXTERNAL = 0
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103)
"""

# 干部月度考核总金额(关联问题)
RISK_GANBU_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            inner JOIN
        t_person AS tp on a.ID_CARD = tp.ID_CARD
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            inner join 
        t_check_info as f on d.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND tp.IDENTITY = '干部'
        AND d.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""

# 干部月度考核总金额(关联问题)
RISK_FEIGANBU_ASSESS_RESPONSIBLE_SQL = """
select arps.FK_DEPARTMENT_ID, sum(arps.COUNT) as COUNT
from
(
SELECT
       distinct(a.pk_id) ,b.FK_DEPARTMENT_ID, a.ACTUAL_MONEY AS COUNT
    FROM
        t_safety_assess_month_responsible_detail AS a
            LEFT JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            inner JOIN
        t_person AS tp on a.ID_CARD = tp.ID_CARD
            LEFT JOIN
        t_check_problem_and_risk as c on a.FK_CHECK_PROBLEM_ID = c.FK_CHECK_PROBLEM_ID
            inner join 
        t_check_problem as d on a.FK_CHECK_PROBLEM_ID = d.PK_ID
            inner join 
        t_check_info as f on d.FK_CHECK_INFO_ID = f.PK_ID
    WHERE
        b.STATUS = 3
        AND b.YEAR = {0}
        AND b.MONTH = {1}
        AND c.FK_RISK_ID IN ({2})
        AND tp.IDENTITY  <> '干部'
        AND d.IS_EXTERNAL = 0
        AND f.CHECK_WAY NOT BETWEEN 5 AND 6
        AND f.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND f.CHECK_TYPE NOT IN (102, 103)
        ) as arps
    GROUP BY arps.FK_DEPARTMENT_ID;
"""