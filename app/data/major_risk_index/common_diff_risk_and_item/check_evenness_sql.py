#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_evenness_sql
Description:
1. 2019-11-14 检查问题，问题质量分相关，
    关联检查信息t_check_info,check_way 改为 not between 5 and 6
Author:    
date:         2019-06-24
-------------------------------------------------
Change Activity:2019-06-24 16:47
-------------------------------------------------
"""

# 一般以上项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base_risk as c on a.FK_PROBLEM_BASE_ID = c.FK_PROBLEM_BASE_ID
        inner join
        t_check_info AS cif on a.FK_CHECK_INFO_ID = cif.PK_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND c.fk_risk_id IN ({2})
            AND cif.CHECK_WAY NOT BETWEEN 5 AND 6
            AND cif.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND cif.CHECK_TYPE NOT IN (102, 103) 
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
"""

# 一般以上项点问题数(关联项目)
NORISK_GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL = """select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base as c on a.FK_PROBLEM_BASE_ID = c.PK_ID
        inner join
        t_check_info AS cif on a.FK_CHECK_INFO_ID = cif.PK_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 3
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND cif.CHECK_WAY NOT BETWEEN 5 AND 6
            AND cif.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND cif.CHECK_TYPE NOT IN (102, 103) 
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
"""

# 较大及以上问题项点数
RISK_ABOVE_PROBLEM_POINT_COUNT_SQL = """
select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base_risk as c on a.FK_PROBLEM_BASE_ID = c.FK_PROBLEM_BASE_ID
        inner join
        t_check_info AS cif on a.FK_CHECK_INFO_ID = cif.PK_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
            AND cif.CHECK_WAY NOT BETWEEN 5 AND 6
            AND cif.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND cif.CHECK_TYPE NOT IN (102, 103) 
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
"""

# 较大及以上问题项点数(关联项目)
NO_RISK_ABOVE_PROBLEM_POINT_COUNT_SQL = """
select
distinct
d.TYPE3 as FK_DEPARTMENT_ID,
cf.FK_PROBLEM_BASE_ID,
 1 as COUNT from
(
SELECT
        distinct a.CHECK_PERSON_ID_CARD, a.FK_PROBLEM_BASE_ID
    FROM
        t_check_problem AS a
        inner join
        t_problem_base as c on a.FK_PROBLEM_BASE_ID = c.PK_ID
        inner join
        t_check_info AS cif on a.FK_CHECK_INFO_ID = cif.PK_ID
        WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.RISK_LEVEL <= 2
            AND c.FK_CHECK_ITEM_ID IN ({2})
            AND cif.CHECK_WAY NOT BETWEEN 5 AND 6
            AND cif.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND cif.CHECK_TYPE NOT IN (102, 103) 
           ) as cf
        inner join
        t_person as b on cf.CHECK_PERSON_ID_CARD = b.ID_CARD
        inner join
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
"""

# 基础问题库中一般及以上风险项点问题数
GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PK_ID) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 3 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
            AND FK_CHECK_ITEM_ID IN ({0})
    GROUP BY FK_DEPARTMENT_ID;
"""

# 基础问题库中较大及以上风险项点问题数
RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        FK_DEPARTMENT_ID, COUNT(DISTINCT PK_ID) AS COUNT
    FROM
        t_problem_base
    WHERE
        RISK_LEVEL <= 2 AND IS_DELETE = 0
            AND STATUS = 3
            AND TYPE = 3
            AND FK_CHECK_ITEM_ID IN ({0})
    GROUP BY FK_DEPARTMENT_ID
"""

# 基础问题库中较大及以上风险项点问题数
EX_RISK_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 2 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.PK_ID
"""

# 基础问题库中一般及以上风险项点问题数(关联风险)
EX_GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL = """SELECT
        MAX(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID,
        1 AS COUNT
    FROM
        t_problem_base as a
        INNER JOIN 
        t_problem_base_risk AS b ON b.FK_PROBLEM_BASE_ID = a.PK_ID
    WHERE
        a.RISK_LEVEL <= 3 AND a.IS_DELETE = 0
            AND a.STATUS = 3
            AND a.TYPE = 3
            AND b.FK_RISK_ID IN ({0})
    GROUP BY a.PK_ID
"""

# 每日检查数
DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103) 
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 检查班组数统计
CHECK_BANZU_COUNT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID
    FROM
        t_department AS a
            INNER JOIN
        t_department_and_info AS b ON a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
    WHERE
        a.TYPE between 9 and 10
            AND b.SOURCE_ID IN ({0})
            AND a.IS_DELETE = 0;
"""

# 重要检查点
CHECK_POINT_COUNT_SQL = """SELECT
        PK_ID AS CHECK_POINT_ID, FK_DEPARTMENT_ID
    FROM
        t_check_point
    WHERE
        IS_DELETE = 0 AND HIERARCHY = 2;
"""

# 班组受检次数
BANZU_DEPARTMENT_CHECKED_COUNT_SQL = """
SELECT
        max(a.FK_DEPARTMENT_ID) AS DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
            inner join 
        t_check_info_and_item as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        a.TYPE = 1
        AND c.TYPE BETWEEN 9 AND 10
        AND c.HIERARCHY >= 5
        AND c.IS_DELETE = 0
        AND b.CHECK_WAY  BETWEEN 1 AND 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        and d.FK_CHECK_ITEM_ID in ({2})
    GROUP BY b.PK_ID;
"""

# 重要检查点受检次数
CHECK_POINT_CHECKED_COUNT_SQL = """SELECT
        a.FK_CHECK_POINT_ID, COUNT(distinct(b.pk_id)) AS COUNT
    FROM
        t_check_info_and_address AS a
            LEFT JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            LEFT JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
            left join
        t_check_info_and_item as d on d.FK_CHECK_INFO_ID = b.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            <= DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND d.FK_CHECK_ITEM_ID in ({2})
        AND b.CHECK_WAY  BETWEEN 1 AND 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
    GROUP BY a.FK_CHECK_POINT_ID;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_SQL = """SELECT
        a.FK_DEPARTMENT_ID, MAX(a.WORK_TYPE) AS WORK_TYPE
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE = 9
            AND a.SOURCE_ID IN ({0})
    GROUP BY a.FK_DEPARTMENT_ID;
"""

# 班组受检点
BANZU_CHECKED_COUNT_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID AS DEPARTMENT_ID, a.FK_CHECK_INFO_ID, 1 as COUNT
FROM
    t_check_info_and_address AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.type = 1
        AND b.TYPE2 = '{0}'
        AND b.TYPE BETWEEN 9 AND 10
        AND b.HIERARCHY >= 5
        AND b.IS_DELETE = 0
"""

# 特殊专业下班组受检点
SPECIAL_BANZU_CHECKED_COUNT_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID AS DEPARTMENT_ID, a.FK_CHECK_INFO_ID, 1 as COUNT
FROM
    t_check_info_and_address AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.type = 1
        AND b.TYPE3 in {0}
        AND b.TYPE BETWEEN 9 AND 10
        AND b.HIERARCHY >= 5
        AND b.IS_DELETE = 0
"""

# 检查信息关联检查项目
CHECK_INFO_AND_ITEM_COUNT_SQL = """
SELECT 
    a.PK_ID
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
    b.FK_CHECK_ITEM_ID IN ({2})
    AND a.CHECK_WAY  BETWEEN 1 AND 2
    AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND a.CHECK_TYPE NOT IN (102, 103)
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST = [
    BANZU_CHECKED_COUNT_SQL,
    CHECK_INFO_AND_ITEM_COUNT_SQL
]

SPECIAL_BANZU_DEPARTMENT_CHECKED_COUNT_SQLIST = [
    SPECIAL_BANZU_CHECKED_COUNT_SQL,
    CHECK_INFO_AND_ITEM_COUNT_SQL
]

# 每日检查数(不关联项目)
NOITEM_DAILY_CHECK_COUNT_SQL = """SELECT
    c.FK_DEPARTMENT_ID,
    DAY(a.END_CHECK_TIME) AS DAY,
    COUNT(DISTINCT a.PK_ID) AS COUNT
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_person AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
        >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY  BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
GROUP BY c.FK_DEPARTMENT_ID , DAY(a.END_CHECK_TIME)
"""

# 每天检查数-按时段
HOUR_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID,
        DATE_FORMAT(a.START_CHECK_TIME, '%%Y-%%m-%%d %%H') AS START_HOUR,
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d %%H') AS END_HOUR,
        COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            LEFT JOIN
        t_check_info_and_item as c on a.pk_id = c.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.END_CHECK_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND c.FK_CHECK_ITEM_ID in ({2})
    GROUP BY b.FK_DEPARTMENT_ID , START_HOUR, END_HOUR;
"""

# 每天作业班组数
DAILY_CHECK_BANZU_COUNT_H_SQL = """SELECT
        a.FK_DEPARTMENT_ID,  a.WORK_TYPE, a.WORK_TIME
    FROM
        t_department_and_info AS a
            LEFT JOIN
        t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    WHERE
        a.WORK_TYPE IS NOT NULL
            AND b.TYPE = 9 AND b.IS_DELETE = 0
            AND a.SOURCE_ID IN ({0})
"""