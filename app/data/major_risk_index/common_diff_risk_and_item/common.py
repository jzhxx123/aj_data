#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
"""[数据转换成标准格式的公共函数]
"""
import pandas as pd
from app.data.index.util import get_custom_month
from app.data.util import pd_query
from app.data.major_risk_index.common_diff_risk_and_item.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_POINT_SQL)
from app.data.workshop_health_index.cache.cache import get_cache_client
cache_client = get_cache_client(__package__)

# ————————————————————检查力度—————————————————— #


# ——————————覆盖率 -------------------- #


def get_check_address_standard_data(work_banzu_info_data,
                                    department_data,
                                    months_ago,
                                    check_item_ids,
                                    major, is_base_item=True):
    """[将覆盖率数据转成标准格式]
    类型：检查地点数（实际检查班组+实际重要检查点）/地点总数（班组数+重要检查点）
    班组跟重要检查点的工作项目要复合特定指数检查项目
    """
    # 实际检查班组数
    check_info_and_address = cache_client.get(
        'check_info_and_address', months_ago=months_ago)
    real_check_banzu_data = pd.merge(check_info_and_address, department_data, left_on='ADDRESS_FK_DEPARTMENT_ID',
                                     right_on='DEPARTMENT_ID')
    del check_info_and_address
    if is_base_item:
        real_check_banzu_data = real_check_banzu_data[(real_check_banzu_data['ADDRESS_TYPE'] == 1)
                                                      & (real_check_banzu_data['TYPE'] == 9)]
    real_check_banzu_data = real_check_banzu_data.groupby(
        'ADDRESS_FK_DEPARTMENT_ID').size().to_frame(name='COUNT')
    real_check_banzu_data = real_check_banzu_data.reset_index().rename(
        columns={'ADDRESS_FK_DEPARTMENT_ID': 'FK_DEPARTMENT_ID'})
    real_check_banzu_data['COUNT'] = 1
    real_check_banzu_data = pd.merge(
        real_check_banzu_data,
        work_banzu_info_data,
        how='inner',
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID'
    )
    # 删除重复班组id
    real_check_banzu_data.drop_duplicates(subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)
    # 实际重要检查点数
    real_check_point_data = cache_client.get(
        'real_check_point_relative_banzu', months_ago=months_ago)
    # 筛选项目
    check_item_ids_list = check_item_ids.split(',')
    check_item_ids_list = [int(item) for item in check_item_ids_list]
    if is_base_item:
        real_check_point_data = real_check_point_data[real_check_point_data.FK_CHECK_ITEM_ID.isin(
            check_item_ids_list)]
    real_check_point_data.drop(
        ["FK_CHECK_ITEM_ID", "FK_CHECK_INFO_ID", 'FK_CHECK_POINT_ID'], inplace=True, axis=1)
    real_check_point_data.drop_duplicates(
        subset=['FK_DEPARTMENT_ID'], keep='first', inplace=True)

    real_check_point_data = pd.merge(
        real_check_point_data,
        work_banzu_info_data,
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID',
        how='inner'
    )

    banzu_point_data = pd_query(BANZU_POINT_SQL.format(major))
    banzu_point_data = pd.merge(
        banzu_point_data,
        work_banzu_info_data,
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID',
        how='inner'
    )

    check_point_data = pd_query(CHECK_POINT_SQL)
    check_point_data = pd.merge(
        check_point_data,
        work_banzu_info_data,
        right_on='FK_DEPARTMENT_ID',
        left_on='FK_DEPARTMENT_ID',
        how='inner'
    )
    filled_data = pd_query(BANZU_POINT_SQL.format(major))
    filled_data['COUNT'] = 0
    if real_check_point_data.empty and real_check_banzu_data.empty:
        # 两个均为空时，补充检查班组数
        real_check_banzu_data = filled_data
    if banzu_point_data.empty and check_point_data.empty:
        # 两个均为空时，补充符合某工作项班组数
        banzu_point_data = filled_data
    return real_check_banzu_data, real_check_point_data, banzu_point_data, check_point_data


# ----------------------- 检查均衡度指数 ------------------- #

# ---------检查地点均衡度----------------#

def get_check_address_evenness_data(major, months_ago, check_item_ids):
    """[获取检查地点均衡度标准数据格式]
    新的检查均衡度，比较值默认以人员数为工作量
    """
    from app.data.major_risk_index.common.check_evenness_sql import(
        CHECK_BANZU_PERSON_COUNT_SQL, BANZU_CONNECT_DEPARTMENT_SQL,
        BANZU_CHECKED_COUNT_SQL, CHECK_POINT_PERSON_COUNT_SQL,
        CHECK_POINT_CONNECT_DEPARTMENT_SQL, POINT_CHECKED_COUNT_SQL
    )
    stats_month = get_custom_month(months_ago)
    # 检查班组数统计
    check_banzu_person_count_data = pd_query(
        CHECK_BANZU_PERSON_COUNT_SQL.format(major))

    # 班组关联站段(选取部门全称)
    banzu_connect_department_data = pd_query(
        BANZU_CONNECT_DEPARTMENT_SQL.format(major))

    # 班组受检次数
    banzu_checked_count_data = pd_query(
        BANZU_CHECKED_COUNT_SQL.format(*stats_month, check_item_ids))

    # 重要检查点
    check_point_person_count_data = pd_query(
        CHECK_POINT_PERSON_COUNT_SQL.format(major))

    # 重要检查点关联部门(选取部门全称)
    check_point_connect_department_data = pd_query(
        CHECK_POINT_CONNECT_DEPARTMENT_SQL.format(major, check_item_ids))

    # 重要检查点受检次数
    point_checked_count_data = pd_query(
        POINT_CHECKED_COUNT_SQL.format(*stats_month, check_item_ids))

    check_address_evenness_data = (check_banzu_person_count_data, check_point_person_count_data,
                                   banzu_checked_count_data, point_checked_count_data, 
                                   check_point_connect_department_data, banzu_connect_department_data)
    return check_address_evenness_data


def recursive_count_from_down_to_up(data, dpid_data, column='COUNT'):
    """[summary]
    自下向上递归，将下级的次数归给上级
    例如sql统计某车间检查1次，该车间下的班组检查3次，
    实际情况为该车间检查1+3=4次，班组3次
    Arguments:
        data {[type]} -- [description]
    """
    # 部门
    DEPARTMENT_SQL = """
    SELECT
        a.DEPARTMENT_ID, 
        a.FK_PARENT_ID
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
    """
    _dpid_data = pd.merge(
        pd_query(DEPARTMENT_SQL),
        dpid_data,
        on='DEPARTMENT_ID',
        how='inner'
    )

    data['REAL_COUNT'] = 0
    for idx, row in data.iterrows():
        all_sub_dpids = get_all_sub_dpids(row['DEPARTMENT_ID'], _dpid_data)
        # 将自己的次数+子部门的次数
        data.loc[idx,'REAL_COUNT'] = sum(
            data[data['DEPARTMENT_ID'].isin(all_sub_dpids)][column].values.tolist())
    data.drop([column], inplace=True, axis=1)
    data.rename(columns={'REAL_COUNT': column}, inplace=True)
    return data


def get_all_sub_dpids(dpid, dpid_data,
                      super_column='FK_PARENT_ID',
                      sub_column='DEPARTMENT_ID'):
    """[summary]
    求该部门的所有子部门
    返回dpid+所有子部门id(默认)，
    拓展计算所有类型自己以及子集单位id
    Arguments:
        dpid {[summary]} -- [str]} -- [description]
    """
    ret = [dpid]
    parent_ids = [dpid]
    while len(parent_ids) > 0:
        df = dpid_data[dpid_data[super_column].isin(parent_ids)]
        if len(df) > 0:
            ids_list = df[sub_column].tolist()
            parent_ids = [str(val) for val in ids_list]
            ret.extend(parent_ids)
        else:
            break
    # 去除重复的PK_ID
    total_set = set(ret)
    return list(total_set)


def get_all_superior_ids(sub_ids, all_data):
    """[summary]
    获取子集的所有父级id
    向上递归
    Arguments:
        sub_ids {[type]} -- [description]
        all_data {[type]} -- [description]
    """
    ret = sub_ids
    parent_ids = sub_ids
    while True:
        df = all_data[all_data['PK_ID'].isin(parent_ids.split(','))]
        if len(df) > 0:
            ids_list = df['PARENT_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break
    
    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    ret = ','.join(total_set)
    return ret
