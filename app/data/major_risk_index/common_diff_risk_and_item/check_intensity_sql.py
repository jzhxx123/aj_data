#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   check_intensity_sql
Description:
1. 2019-11-14 检查问题，问题质量分相关，
    关联检查信息t_check_info,check_way 改为 not between 5 and 6
Author:    
date:         2019-06-24
-------------------------------------------------
Change Activity:2019-06-24 16:03
-------------------------------------------------
"""
# SELECT
#         b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.FK_DEPARTMENT_ID, a.PK_ID, b.ID_CARD) AS COUNT
#     FROM
#         t_check_info AS a
#             INNER JOIN
#         t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
#             INNER JOIN
#         t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
#     WHERE
#         a.CHECK_WAY BETWEEN 1 AND 2
#         AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
#             >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
#         AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
#             < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
#         AND c.FK_CHECK_ITEM_ID IN ({2})
#     GROUP BY b.FK_DEPARTMENT_ID;
# 检查次数（现场检查）
# 检查信息里默认不要102，103等
CHECK_COUNT_SQL = """
select d.FK_DEPARTMENT_ID, COUNT(1) as COUNT from
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_item as c on b.PK_ID = c.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY BETWEEN 1 AND 2
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf
inner join
t_check_info_and_person as d on qf.PK_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 检查次数（现场检查）不关联任何检查项目
NOITEM_CHECK_COUNT_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        a.CHECK_WAY BETWEEN 1 AND 2
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# -------------------------查处问题率--------------------------#

# 所有问题数
ALL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""

# 所有问题数(关联项目)
NORISK_ALL_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID;
"""

# 作业项问题数
ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID
"""

# 作业项问题数（关联项目）
NORISK_ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID
"""

# 作业项问题信息
ZUOYE_CHECK_PROBLEM_INFO_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        max(a.LEVEL) AS LEVEL,
        1 AS COUNT
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID
"""

# 管理项问题数
GUANLI_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY a.PK_ID
"""

# 管理项问题数(关联项目)
NORISK_GUANLI_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4')
    GROUP BY a.PK_ID
"""

# 考核作业项问题数
KAOHE_ZY_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID;
"""

# 考核作业项问题数(关联项目)
NORISK_KAOHE_ZY_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
    GROUP BY a.PK_ID;
"""

# 管理项, 设备项考核问题数 - 管理项只包含F
KAOHE_GL_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_ASSESS = 1
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('F1', 'F2', 'F3', 'F4','E1', 'E2', 'E3', 'E4')
    GROUP BY a.PK_ID;
"""

# 管理项, 设备项考核问题数 - 管理项只包含F(关联项目)
NORISK_KAOHE_GL_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.IS_ASSESS = 1
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('F1', 'F2', 'F3', 'F4','E1', 'E2', 'E3', 'E4')
    GROUP BY a.PK_ID;
"""

# 问题质量分累计
PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            group by b.pk_id
"""

# 问题质量分累计(关联项目)
NORISK_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            group by b.pk_id
"""

# 夜查次数
YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.FK_DEPARTMENT_ID,a.PK_ID,b.ID_CARD) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_check_info_and_item AS c ON c.FK_CHECK_INFO_ID = a.PK_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 夜查次数（无检查项目）
NOITEM_YECHA_CHECK_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(DISTINCT b.FK_DEPARTMENT_ID,a.PK_ID,b.ID_CARD) AS COUNT
    FROM
        t_check_info AS a
            INNER JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND a.IS_YECHA = 1
            AND a.CHECK_WAY BETWEEN 1 AND 2
            AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND a.CHECK_TYPE NOT IN (102, 103)
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 较大及以上风险问题质量分累计
RISK_LEVEL_PROBLEM_SQL = """
select rlp.FK_DEPARTMENT_ID, sum(rlp.CHECK_SCORE) as COUNT
from
(
SELECT
        distinct(b.pk_id), a.FK_DEPARTMENT_ID, d.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join 
        t_person as a on b.CHECK_PERSON_ID_CARD = a.ID_CARD
        left join
        t_check_problem_and_risk as c
        ON  b.PK_ID = c.FK_CHECK_PROBLEM_ID
        left join
        t_problem_base as d 
        on b.FK_PROBLEM_BASE_ID = d.pk_id
        inner join
        t_check_info as e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            ) as rlp
    GROUP BY rlp.FK_DEPARTMENT_ID;
"""

# 较大及以上风险问题质量分累计(关联项目)
NORISK_LEVEL_PROBLEM_SQL = """
select rlp.FK_DEPARTMENT_ID, sum(rlp.CHECK_SCORE) as COUNT
from
(
SELECT
        distinct(b.pk_id), a.FK_DEPARTMENT_ID, d.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join 
        t_person as a on b.CHECK_PERSON_ID_CARD = a.ID_CARD
        left join
        t_problem_base as d 
        on b.FK_PROBLEM_BASE_ID = d.pk_id
        inner join
        t_check_info as e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            ) as rlp
    GROUP BY rlp.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计
XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_person as d ON b.CHECK_PERSON_ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_check_problem_and_risk as a on a.fk_check_problem_id = b.pk_id
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND a.fk_risk_id IN ({2})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 现场检查发现较大和重大安全风险问题质量分累计(关联项目)
NORISK_XIANCHENG_RISK_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_person as d ON b.CHECK_PERSON_ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 2
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND b.FK_CHECK_ITEM_ID IN ({2})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 分析中心检查力度（问题数）
ANALYSIS_CENTER_PROBLEM_COUNT_SQL = """
SELECT
        MAX(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 AS NUMBER
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_check_info_and_person AS b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
            INNER JOIN
        t_department as d on b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk AS e ON e.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        c.CHECK_WAY between 3 and 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.IS_EXTERNAL = 0
        AND e.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.all_name like "%%安全分析中心%%"
    GROUP BY a.PK_ID
"""

# 分析中心检查力度（质量分）
ANALYSIS_CENTER_PROBLEM_SCORE_SQL = """
SELECT
    MAX(f.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS NUMBER
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS f ON a.FK_CHECK_INFO_ID = f.FK_CHECK_INFO_ID
            INNER JOIN
        t_department as d on f.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
            INNER JOIN
        t_check_problem_and_risk AS e ON e.FK_CHECK_PROBLEM_ID = a.PK_ID
    WHERE
        c.CHECK_WAY between 3 and 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.IS_EXTERNAL = 0
        AND e.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.all_name like "%%安全分析中心%%"
    GROUP BY a.pk_id;
"""

ANALYSIS_CENTER_CHECK_INTENSITY_SQLLIST = [ANALYSIS_CENTER_PROBLEM_COUNT_SQL, ANALYSIS_CENTER_PROBLEM_SCORE_SQL]


# -------------------- 监控调阅力度 -------------------- #

# 监控调阅检查时间
MEDIA_COST_TIME_SQL = """
select d.FK_DEPARTMENT_ID, sum(a.COST_TIME) as TIME from
t_check_info_and_media as a
inner join 
(
    select distinct b.PK_ID
    from 
    t_check_info as b
    inner join
    t_check_info_and_item as c on b.PK_ID = c.FK_CHECK_INFO_ID
    where
    b.CHECK_WAY = 3
    AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
    AND b.CHECK_TYPE NOT IN (102, 103)
    and b.status = 1
    and c.FK_CHECK_ITEM_ID in ({2})
    and 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND 
    DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
) as qf on a.FK_CHECK_INFO_ID = qf.PK_ID
inner join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
group by d.FK_DEPARTMENT_ID
"""

# 监控调阅检查时间
NOITEM_MEDIA_COST_TIME_SQL = """
select mdc.FK_DEPARTMENT_ID, sum(mdc.COST_TIME) as TIME from (
select a.COST_TIME, d.FK_DEPARTMENT_ID from 
t_check_info_and_media as a
left join 
t_check_info as b on a.FK_CHECK_INFO_ID = b.PK_ID 
left join
t_check_info_and_person as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
where
 b.CHECK_WAY = 3
 AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
AND b.CHECK_TYPE NOT IN (102, 103)
and 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
AND 
DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
)as mdc
group by mdc.FK_DEPARTMENT_ID
"""

# 监控调阅发现问题数
MEDIA_PROBLEM_NUMBER_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
        INNER join
    t_check_problem_and_risk as d on a.PK_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID in ({2})
            group by a.pk_id
"""

# 监控调阅发现问题数(关联项目)
NORISK_MEDIA_PROBLEM_NUMBER_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.FK_CHECK_ITEM_ID in ({2})
            group by a.pk_id
"""

# 监控调阅发现问题信息
MEDIA_PROBLEM_NUMBER_INFO_SQL = """
SELECT  
MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
MAX(a.LEVEL) AS LEVEL,
1 as NUMBER
    FROM
    t_check_problem AS a
        INNER JOIN 
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        INNER JOIN 
    t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
        INNER join
    t_check_problem_and_risk as d on a.PK_ID = d.FK_CHECK_PROBLEM_ID
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY BETWEEN 3 AND 4 
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID in ({2})
            group by a.pk_id
"""


# 监控调阅质量分
MEDIA_PROBLME_SCORE_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            left join
        t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND f.FK_RISK_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID
"""

# 监控调阅质量分(关联项目)
NORISK_MEDIA_PROBLME_SCORE_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, max(b.CHECK_SCORE) AS SCORE
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID
"""

# 监控调阅问题得分信息
MEDIA_PROBLME_SCORE_INFO_SQL = """
select 
    MAX(d.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
    max(b.CHECK_SCORE) AS SCORE, 
    max(a.LEVEL) as LEVEL
    FROM
        t_check_problem AS a
            LEFT JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            LEFT JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            left join 
        t_person as d on a.CHECK_PERSON_ID_CARD = d.ID_CARD
            left join
        t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
    WHERE
        c.CHECK_WAY BETWEEN 3 AND 4
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND f.FK_RISK_ID in ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY a.PK_ID
"""


# 调阅班组数
WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct( if(d.TYPE !=9, d.FK_PARENT_ID,b.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_item AS c ON a.PK_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_CHECK_ITEM_ID IN ({2})
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
"""

# 调阅班组数（不包含检查项目）
NOITEM_WATCH_MEDIA_BANZU_COUNT_SQL = """
SELECT 
    distinct( if(d.TYPE !=9, d.FK_PARENT_ID,b.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS NUMBER
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_address AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
        INNER JOIN
    t_department AS d ON b.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CHECK_WAY = 3
        AND a.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND a.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.TYPE BETWEEN 9 AND 10
        AND d.IS_DELETE = 0
        AND d.MEDIA_TYPE != ''
"""


# 具有调阅作业班组数
WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct (if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
         b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.MEDIA_TYPE !=''
"""

# 施工或作业班组数(MAIN_TYPE=1表示工作项目)
RISK_WORK_BANZU_COUNT_SQL = """
SELECT 
    distinct(if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID))as FK_DEPARTMENT_ID, 1 AS COUNT
FROM
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND a.source_id in ({0})
"""

# 监控调阅覆盖（调阅班组数/具有调阅作业班组数）计算
MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, WORK_BANZU_COUNT_SQL]

# 监控调阅覆盖（调阅班组数/某种风险配置作业班组数）计算
RISK_MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST = [WATCH_MEDIA_BANZU_COUNT_SQL, RISK_WORK_BANZU_COUNT_SQL]


# -------------------- 覆盖率 -------------------- #

# 班组受检点
BANZU_CHECKED_COUNT_SQL = """
SELECT 
    if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, a.FK_CHECK_INFO_ID, 1 as COUNT
FROM
    t_check_info_and_address AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.type = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.TYPE2 = '{0}'
"""

# 特殊专业班组受检点
VIRTUAL_BANZU_CHECKED_COUNT_SQL = """
SELECT 
    if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, a.FK_CHECK_INFO_ID, 1 as COUNT
FROM
    t_check_info_and_address AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.type = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND b.TYPE3 in {0}
"""

# 检查信息关联检查项目
CHECK_INFO_AND_ITEM_COUNT_SQL = """
SELECT 
    distinct a.PK_ID
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_item AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
    b.FK_CHECK_ITEM_ID IN ({2})
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 检查信息不关联任何
CHECK_INFO_SQL = """
SELECT 
    a.PK_ID
FROM
    t_check_info AS a
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
    AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

REAL_CHECK_BANZU_SQLIST = [
    BANZU_CHECKED_COUNT_SQL,
    CHECK_INFO_AND_ITEM_COUNT_SQL
]

# 总地点数
# 重要检查点实体
CHECK_POINT_SQL = """
select tdf.FK_DEPARTMENT_ID, tdf.COUNT
from (
SELECT
       distinct SOURCE_DEPARTMENT_ID as FK_DEPARTMENT_ID, 1 as COUNT, a.PK_ID
    FROM 
        t_check_point as a
        inner join
        t_department_and_main_production_site AS b on a.PK_ID = b.FK_ADDRESS_ID
    WHERE
        a.IS_DELETE = 0
        AND a.HIERARCHY = 2
        AND b.type=2
        AND a.type = 1
) as tdf
"""

# 班组
BANZU_POINT_SQL = """SELECT
        a.DEPARTMENT_ID AS FK_DEPARTMENT_ID, 1 AS COUNT
    FROM
        t_department AS a
    WHERE
        a.TYPE BETWEEN 9 AND 10
            AND a.HIERARCHY = 5
            AND a.IS_DELETE = 0
            AND a.TYPE2 = '{0}'
"""

# 实际重要检查地点数
REAL_CHECK_POINT_SQL = """
select dmp.source_department_id as FK_DEPARTMENT_ID, 1 as COUNT
from
(
SELECT
        distinct c.PK_ID
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
            inner join
        t_check_info_and_item as d on a.FK_CHECK_INFO_ID = d.FK_CHECK_INFO_ID
    WHERE
        a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND c.TYPE = 1
        AND b.CHECK_WAY between 1 and 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND d.fk_check_item_id in ({2})
    ) as cdp
    inner join
    t_department_and_main_production_site AS dmp on cdp.PK_ID = dmp.FK_ADDRESS_ID
    WHERE
        dmp.type=2
"""

# 实际重要检查地点数
NOITEM_REAL_CHECK_POINT_SQL = """SELECT
        c.FK_DEPARTMENT_ID, COUNT(DISTINCT a.FK_CHECK_POINT_ID) AS COUNT
    FROM
        t_check_info_and_address AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_point AS c ON a.FK_CHECK_POINT_ID = c.PK_ID
    WHERE
        a.TYPE = 2
        AND c.HIERARCHY = 2
        AND c.IS_DELETE = 0
        AND c.TYPE = 1
        AND b.CHECK_WAY between 1 and 2
        AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND b.CHECK_TYPE NOT IN (102, 103)
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY c.FK_DEPARTMENT_ID
"""

# 工作班组信息
WORK_BANZU_INFO_SQL = """
SELECT 
    distinct(if(b.TYPE !=9, b.FK_PARENT_ID, a.FK_DEPARTMENT_ID) ) as FK_DEPARTMENT_ID
    from
    t_department_and_info AS a
        INNER JOIN
    t_department AS b ON a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.MAIN_TYPE = 1
        AND b.TYPE BETWEEN 9 AND 10
        AND b.IS_DELETE = 0
        AND a.source_id in ({1})
        AND b.TYPE2 = '{0}'
"""

# 一般以及以上问题数（风险）
ABOVE_YIBAN_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <= 3
    GROUP BY a.PK_ID;
"""

# 一般以及以上问题数（项目）
NORISK_ABOVE_YIBAN_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND a.FK_CHECK_ITEM_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <= 3
    GROUP BY a.PK_ID;
"""

# 一般作业项问题数
ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND d.FK_RISK_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
            AND a.RISK_LEVEL <= 3
    GROUP BY a.PK_ID
"""

# 一般作业项问题（关联项目）
NORISK_ABOVE_YIBAN_ZUOYE_CHECK_PROBLEM_SQL = """SELECT
        max(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID, 
        1 AS NUMBER
    FROM
        t_check_problem as a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_person AS c ON a.CHECK_PERSON_ID_CARD = c.ID_CARD
    WHERE
        DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.CHECK_WAY NOT BETWEEN 5 AND 6
            AND b.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND b.CHECK_TYPE NOT IN (102, 103)
            AND a.FK_CHECK_ITEM_ID IN ({2})
            AND a.LEVEL IN ('A', 'B', 'C', 'D')
            AND a.RISK_LEVEL <= 3
    GROUP BY a.PK_ID
"""

# 一般问题质量分累计
ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 一般问题质量分累计（关联项目）
NORISK_ABOVE_YIBAN_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 作业问题质量分
ZUOYE_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
            inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
            group by b.pk_id
"""

# 作业问题质量分(关联项目)
NORISK_ZUOYE_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
            group by b.pk_id
"""

# 一般以上作业质量分
ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
            inner join
        t_check_problem_and_risk as d on b.pk_id = d.fk_check_problem_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND d.fk_risk_id IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 一般以上作业质量分(关联项目)
NORISK_ABOVE_YIBAN_ZUOYE_PROBLEM_CHECK_SCORE_SQL = """
select max(a.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, MAX(c.CHECK_SCORE) as COUNT 
    from 
        t_check_problem AS b
            inner join
        t_person as a ON b.CHECK_PERSON_ID_CARD = a.ID_CARD
            inner join
        t_problem_base as c on b.FK_PROBLEM_BASE_ID = c.pk_id
        inner join
        t_check_info AS e on b.FK_CHECK_INFO_ID = e.PK_ID
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.FK_CHECK_ITEM_ID IN ({2})
            AND e.CHECK_WAY NOT BETWEEN 5 AND 6
            AND e.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND e.CHECK_TYPE NOT IN (102, 103) 
            AND b.LEVEL IN ('A', 'B', 'C', 'D')
            AND b.RISK_LEVEL <= 3
            group by b.pk_id
"""

# 较大以及以上问题数（风险）
ABOVE_RISK_PROBLEM_NUMBER_SQL = """SELECT
        max(b.FK_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 1 as COUNT
    FROM
        t_check_problem AS a
            INNER JOIN
        t_check_info AS c ON a.FK_CHECK_INFO_ID = c.PK_ID
            INNER JOIN
        t_person as b on a.CHECK_PERSON_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_problem_and_risk as d on a.pk_id = d.fk_check_problem_id
    WHERE
        c.CHECK_WAY NOT BETWEEN 5 AND 6
        AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
        AND c.CHECK_TYPE NOT IN (102, 103)
        AND d.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND a.RISK_LEVEL <= 2
    GROUP BY a.PK_ID;
"""

# 现场检查发现一般及以上安全风险问题质量分累计
XIANCHENG_YIBAN_LEVEL_PROBLEM_SQL = """
select xrp.FK_DEPARTMENT_ID, sum(xrp.CHECK_SCORE)AS COUNT
from (
SELECT distinct(b.pk_id),d.FK_DEPARTMENT_ID, e.CHECK_SCORE 
    FROM
        t_check_problem AS b 
        left join
        t_person as d ON b.CHECK_PERSON_ID_CARD = d.ID_CARD
            LEFT JOIN
        t_check_info AS c  ON b.FK_CHECK_INFO_ID = c.PK_ID
        left join
        t_check_problem_and_risk as a on a.fk_check_problem_id = b.pk_id
        left join
        t_problem_base as e on b.FK_PROBLEM_BASE_ID = e.pk_id
    WHERE
        DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(b.SUBMIT_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND b.RISK_LEVEL <= 3
            AND c.CHECK_WAY BETWEEN 1 AND 2
            AND c.CHECK_TYPE NOT BETWEEN 400 AND 499
            AND c.CHECK_TYPE NOT IN (102, 103)
            AND a.fk_risk_id IN ({2})
            ) as xrp
    GROUP BY xrp.FK_DEPARTMENT_ID;
"""

# 获取专业及路局的所有检查项目
ALL_CHECK_ITEM_IDS_SQL = """SELECT
    b.PK_ID 
    FROM
    t_department AS a
    INNER JOIN t_check_item AS b ON ( a.DEPARTMENT_ID = b.FK_DEPARTMENT_ID ) 
    WHERE
    (
    a.TYPE2 = '{0}' 
    OR a.DEPARTMENT_ID IN ( SELECT DEPARTMENT_ID FROM t_department WHERE HIERARCHY = 1 AND IS_DELETE = 0 )
    ) 
    AND a.IS_DELETE = 0 
    AND b.IS_DELETE =0
"""
