#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   problem_rectification_sql
Description:
Author:    
date:         2019-06-25
-------------------------------------------------
Change Activity:2019-06-25 19:11
-------------------------------------------------
"""
# 超期问题数
OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_risk AS c ON c.FK_CHECK_PROBLEM_ID = a.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 超期问题数(关联项目)
NORISK_OVERDUE_PROBLEM_NUMBER_SQL = """SELECT
        b.FK_DEPARTMENT_ID, COUNT(distinct(a.pk_id)) AS COUNT
    FROM
        t_safety_assess_month_problem_detail AS a
            INNER JOIN
        t_safety_assess_month AS b ON a.FK_SAFETY_ASSESS_MONTH_ID = b.PK_ID
            INNER JOIN
        t_check_problem AS c ON c.PK_ID = a.FK_CHECK_PROBLEM_ID
    WHERE
        b.`STATUS` = 3 AND b.YEAR = {0}
            AND b.MONTH = {1}
            AND c.FK_CHECK_ITEM_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_SCORE_SQL = """SELECT
        b.FK_DEPARTMENT_ID, SUM(a.SCORE_STANDARD*d.GRADATION_RATIO)*{3} AS SCORE,
        COUNT(a.pk_id) AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_person AS b ON a.RESPONSIBE_ID_CARD = b.ID_CARD
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
            INNER JOIN
        t_person_gradation_ratio AS d
            ON a.FK_PERSON_GRADATION_RATIO_ID = d.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.FK_RISK_ID IN ({2})
    GROUP BY b.FK_DEPARTMENT_ID;
"""

# 反复发生的同一项点问题数
REPEAT_PROBLEM_SQL = """SELECT
   CONCAT(d.FK_DEPARTMENT_ID, '||', c.ID_CARD, '||', b.PK_ID) AS DIP
FROM
    t_check_problem AS a
        INNER JOIN
    t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
        INNER JOIN
    t_check_problem_and_responsibility_person AS c
        ON c.FK_CHECK_PROBLEM_ID = a.PK_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS d
        ON c.FK_RESPONSIBLE_DEPARTMENT_ID = d.PK_ID
        INNER JOIN
    t_problem_base_risk as e on a.fk_problem_base_id = e.fk_problem_base_id
    WHERE
        e.FK_RISK_ID IN ({2})
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 责任安全信息
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_problem_base AS c ON c.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_problem_base AS d ON d.PK_ID = c.FK_PROBLEM_BASE_ID
            INNER JOIN
        t_problem_base_risk AS e ON e.FK_PROBLEM_BASE_ID = d.PK_ID
    WHERE
        e.FK_RISK_ID in ({2})
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 反复发生的同一项点问题数
HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
            INNER JOIN
        t_problem_base_risk as e on a.FK_PROBLEM_BASE_ID = e.FK_PROBLEM_BASE_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND e.FK_RISK_ID IN ({2})
        AND b.IS_DELETE = 0
"""

# 反复发生的同一项点问题数（关联项目）
NORISK_HAPPEN_PROBLEM_POINT_SQL = """SELECT
        DISTINCT 
        c.FK_DEPARTMENT_ID, b.PK_ID, b.RISK_LEVEL, b.ASSESS_MONEY,  b.LEVEL, a.IS_EXTERNAL,
        a.PK_ID AS FK_CHECK_PROBLEM_ID
    FROM
        t_check_problem AS a
            INNER JOIN
        t_problem_base AS b ON a.FK_PROBLEM_BASE_ID = b.PK_ID
            INNER JOIN
        t_check_problem_and_responsible_department AS c
            ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
    WHERE
        b.RISK_LEVEL BETWEEN 1 AND 3
        -- AND b.LEVEL NOT IN ('E1', 'E2', 'E3', 'E4')
       -- AND d.ACTUAL_MONEY > 0
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_CHECK_ITEM_ID IN ({2})
        AND b.IS_DELETE = 0
"""

# 事故防控责任信息
PRECONTROL_RESPONSIBLE_UNIT_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID, 
    c.MAIN_TYPE,
    1 as COUNT
FROM
    t_safety_produce_info_responsibility_unit AS a
        INNER JOIN
    t_safety_produce_info_responsibility_unit_and_risk AS b ON a.pk_id = b.FK_RESPONSIBILITY_UNIT_ID
        INNER JOIN
    t_safety_produce_info as c ON a.FK_SAFETY_PRODUCE_INFO_ID = c.PK_ID
WHERE
    b.FK_RISK_ID IN ({2}) 
    AND a.type = 2
    AND c.MAIN_TYPE = 1
        AND DATE_FORMAT(a.RECTIFY_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.RECTIFY_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
"""

# 发警告预警通知书
# STATUS=2表示待整改状态
# HIERARCHY in(1,2)表示路局以及专业
WARINING_NOTIFICATION_COUNT_SQL = """
SELECT 
    FK_DUTY_DEPARTMENT_ID as FK_DEPARTMENT_ID, 
    RANK, 
    TYPE, 
    1 as COUNT
FROM
    t_warning_notification
WHERE
    DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND HIERARCHY in (1, 2)
"""

# 履职评价（ZG-1、2、3、4、5）数
CHECK_EVALUATE_SZ_NUMBER_SQL = """SELECT
        MAX(a.RESPONSIBE_DEPARTMENT_ID) as FK_DEPARTMENT_ID, 
        MAX(a.CODE) as CODE, 
        1 AS COUNT
    FROM
        t_check_evaluate_info AS a
            INNER JOIN
        t_check_evaluate_and_risk AS c
            ON c.FK_CHECK_EVALUATE_INFO_ID = a.PK_ID
    WHERE
        a.CODE IN ('ZG-1' , 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
            AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
                < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
            AND c.fk_risk_id in ({2})
            GROUP BY a.PK_ID
"""

# 责任安全信息
DETAIL_RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = """SELECT
        b.MAIN_TYPE, a.FK_DEPARTMENT_ID, a.RESPONSIBILITY_IDENTIFIED, 1 as COUNT
    FROM
        t_safety_produce_info_responsibility_unit AS a
            INNER JOIN
        t_safety_produce_info AS b ON a.FK_SAFETY_PRODUCE_INFO_ID = b.PK_ID
            INNER JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS c ON a.pk_id = c.FK_RESPONSIBILITY_UNIT_ID
    WHERE
        DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(b.OCCURRENCE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND c.FK_RISK_ID IN ({2})
"""

# 警告性预警延期--CREATE_TIME:延长时间（开始延长）,HIERARCHY(路局层),TYPE=2(延长预警考核)
WARNING_DELAY_SQL = """
SELECT
        FK_DEPARTMENT_ID, COUNT(1) AS COUNT
    FROM
        t_warning_notification_assess
    WHERE
        HIERARCHY = 1
        AND
        TYPE = 2
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(CREATE_TIME, '%%Y-%%m-%%d')
            < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
    GROUP BY FK_DEPARTMENT_ID
"""

# 重复发生问题信息
REPEATE_HAPPEN_PROBLEM_SQL = """
SELECT 
    MAX(a.FK_PROBLEM_BASE_ID) AS FK_PROBLEM_BASE_ID,
    MAX(a.RISK_LEVEL) AS RISK_LEVEL,
    MAX(c.FK_DEPARTMENT_ID) AS FK_DEPARTMENT_ID,
    1 AS COUNT
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_problem_and_risk AS b ON a.PK_ID = b.FK_CHECK_PROBLEM_ID
        INNER JOIN
    t_check_problem_and_responsible_department AS c ON a.PK_ID = c.FK_CHECK_PROBLEM_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') < DATE_FORMAT('{1}', '%%Y-%%m-%%d')
        AND b.FK_RISK_ID IN ({2})
        AND a.RISK_LEVEL <=3
GROUP BY a.PK_ID
""" 

# 某个专业的所有问题项点问题
MAJOR_PROBLEM_POINT_INFO_SQL = """
    select a.PK_ID as FK_PROBLEM_BASE_ID, a.PROBLEM_POINT from
    t_problem_base as a
    inner join
    t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
    where b.TYPE2 = '{0}'
    and b.is_delete = 0
    and a.is_delete = 0
"""