#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         2019/5/13


from app.data.major_risk_index._global_var import GlobalVar

module = __package__
GLV = GlobalVar(module)

from . import check_intensity, problem_exposure, \
    assess_intensity, problem_rectification, combine_child_index
