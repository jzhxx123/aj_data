# -*- coding: utf-8 -*-
"""
-------------------------------------------------
   File Name：     init_common_data
   Author :       hwj
   date：          2019/9/18下午3:09
   Change Activity: 2019/9/18下午3:09
-------------------------------------------------
"""
from app.data.index.util import get_custom_month, get_query_condition_by_risktype
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_shigongaq.common import cacl_work_person_count
from app.data.major_risk_index.dianwu_shigongaq.common_sql import CONSTRUCTION_PERSON_SQL, \
    COOPERATE_PERSON_COUNT_SQL, SPECIAL_DEPARTMENT_CONSTRUCTION_PERSON_SQL
from app.data.major_risk_index.dw_gongdian_shigongaq_single import GLV
from app.data.major_risk_index.dw_gongdian_shigongaq_single.common import get_vitual_major_ids
from app.data.major_risk_index.dw_gongdian_shigongaq_single.common_sql import ZHANDUAN_DPID_SQL, CHEJIAN_DPID_SQL, \
    DEPARTMENT_SQL
from app.data.major_risk_index.util import df_merge_with_dpid
from app.data.util import pd_query


def init_func(months_ago, risk_name, risk_type):
    """[初始化常用数据]

    Arguments:
        months_ago {[int]} -- [description]
        risk_name {[int} -- [description]
        risk_type {[str]} -- [description]
    """

    print(__package__)
    major = get_major_dpid(risk_type)
    ids = get_vitual_major_ids("工电-1")
    stats_month = get_custom_month(months_ago)
    zhanduan_dpid_data = pd_query(ZHANDUAN_DPID_SQL.format(ids))
    chejian_dpid_data = pd_query(CHEJIAN_DPID_SQL.format(ids))
    department_data = pd_query(DEPARTMENT_SQL.format(ids))
    risktype_data = get_query_condition_by_risktype(risk_name)
    check_item_ids = risktype_data[0]
    check_risk_ids = risktype_data[1]
    month = int(stats_month[1][5:7])

    # 施工劳时
    # work_time = pd_query(WORK_LABOR_TIME_SQL.format(*stats_month), db_name='db_mid')
    # work_person = pd_query(WORK_PERSON_SQL.format(*stats_month), db_name='db_mid')
    # work_load = df_merge_with_dpid(
    #     calc_work_load_count(work_person, work_time)[0], department_data
    # )

    # 三级施工人数
    construction_person = pd_query(CONSTRUCTION_PERSON_SQL.format(*stats_month), db_name='db_mid')
    # 施工配合人数
    cooperate_person = pd_query(COOPERATE_PERSON_COUNT_SQL.format(*stats_month), db_name='db_mid')
    # 特殊站段施工人数
    special_department_person = pd_query(
        SPECIAL_DEPARTMENT_CONSTRUCTION_PERSON_SQL.format(month))
    work_load = df_merge_with_dpid(
        cacl_work_person_count(construction_person, cooperate_person, special_department_person),
        department_data,
        how='right'
    )
    work_load.fillna(0, inplace=True)

    values = {
        "ZHANDUAN_DPID_DATA": zhanduan_dpid_data,
        "CHEJIAN_DPID_DATA": chejian_dpid_data,
        "DEPARTMENT_DATA": department_data,
        'WORK_LOAD': work_load,
        'CHECK_ITEM_IDS': check_item_ids,
        'CHECK_RISK_IDS': check_risk_ids
    }

    # 设置对应的全局变量
    GLV.set_all_values(values)
