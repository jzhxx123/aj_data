#!/usr/bin/python3
# -*- coding: utf-8 -*-
# Author:       huangweijian
# Date:         2019/5/14
# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.index.util import (get_custom_month, )
from app.data.major_risk_index.common.common import get_major_dpid
from app.data.major_risk_index.dianwu_shigongaq.problem_exposure_sql import (
    NORMAL_CONSTRUCTION_COUNT_SQL, ALL_PROBLEM_NUMBER_SQL, ALL_BASE_PROBLEM_SQL,
    CHECKED_HIDDEN_PROBLEM_POINT_SQL, HIDDEN_PROBLEM_POINT_SQL, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SPECIAL_DEPARTMENT_CONSTRUCTION_SQL)
from app.data.major_risk_index.dw_gongdian_shigongaq_single import GLV
from app.data.major_risk_index.dw_gongdian_shigongaq_single.common import (
    get_vitual_major_ids, stats_other_problem_exposure)
from app.data.major_risk_index.util import (
    append_major_column_to_df,
    combine_child_index_func, df_merge_with_dpid, format_export_basic_data,
    summizet_child_index,
    write_export_basic_data_to_mongo, calc_child_index_type_sum)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago, risk_name, risk_type):
    major = get_vitual_major_ids(risk_type)
    global RISK_TYPE
    RISK_TYPE = risk_type
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, \
        ALL_BASE_PROBLEM, ALL_PROBLEM_NUMBER, MONTH_CONSTRUCTION_COUNT, \
        CHECKED_HIDDEN_PROBLEM_POINT, HIDDEN_PROBLEM_POINT, DF_CONSTRUCTION_COUNT
    ZHANDUAN_DPID_DATA = GLV.get_value('ZHANDUAN_DPID_DATA')
    CHEJIAN_DPID_DATA = GLV.get_value('CHEJIAN_DPID_DATA')
    DEPARTMENT_DATA = GLV.get_value('DEPARTMENT_DATA')
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])

    global CHECK_RISK_IDS
    CHECK_RISK_IDS = GLV.get_value('CHECK_RISK_IDS')
    # 特殊站段施工次数
    special_construction_count = pd_query(SPECIAL_DEPARTMENT_CONSTRUCTION_SQL.format(month))
    # 普通部门施工次数
    normal_construction_count = pd_query(
        NORMAL_CONSTRUCTION_COUNT_SQL.format(*stats_month, major), db_name='db_mid')
    # 当月施工次数
    DF_CONSTRUCTION_COUNT = pd.concat([
        normal_construction_count,
        special_construction_count
    ])
    MONTH_CONSTRUCTION_COUNT = df_merge_with_dpid(
        DF_CONSTRUCTION_COUNT,
        DEPARTMENT_DATA
    )

    # 基础问题库中施工类问题总数
    ALL_BASE_PROBLEM = df_merge_with_dpid(
        pd_query(ALL_BASE_PROBLEM_SQL.format(CHECK_RISK_IDS)), DEPARTMENT_DATA
    )

    # 查处施工类问题数
    ALL_PROBLEM_NUMBER = df_merge_with_dpid(
        pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA
    )

    # 查处施工事故关键隐患问题
    CHECKED_HIDDEN_PROBLEM_POINT = df_merge_with_dpid(
        pd_query(CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, CHECK_RISK_IDS)), DEPARTMENT_DATA
    )

    # 施工事故关键隐患问题总数
    HIDDEN_PROBLEM_POINT = df_merge_with_dpid(
        pd_query(HIDDEN_PROBLEM_POINT_SQL.format(CHECK_RISK_IDS)), DEPARTMENT_DATA
    )


def _calc_basic_prob_number_per_person(df_data, i, title):
    chachu_prob_number = df_merge_with_dpid(df_data[0], DEPARTMENT_DATA)
    base_prob_number = df_merge_with_dpid(df_data[1], DEPARTMENT_DATA)
    construction_number = df_merge_with_dpid(df_data[2], DEPARTMENT_DATA)

    chachu_prob_number = chachu_prob_number.groupby(['TYPE3'])['COUNT'].sum()
    base_prob_number = base_prob_number.groupby(['TYPE3'])['COUNT'].sum()
    construction_number = construction_number.groupby(['TYPE3'])['COUNT'].sum()

    data = pd.concat(
        [chachu_prob_number.to_frame(name='CHACHU_PROB'),
         base_prob_number.to_frame(name='BASE_PROB'),
         construction_number.to_frame(name='CONSTRUCTION')],
        axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['AVG_NUMBER'] = data['CONSTRUCTION'].mean()
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['CHACHU_PROB'], row['BASE_PROB'], row['CONSTRUCTION'], row['AVG_NUMBER']), axis=1)
    data.drop(columns=['CHACHU_PROB', 'BASE_PROB', 'CONSTRUCTION', 'AVG_NUMBER'], inplace=True, axis=1)
    return data


def _calc_hidden_prob_number_per_person(df_data, i, title):
    checked_pdata_number = df_merge_with_dpid(df_data[0], DEPARTMENT_DATA)
    pdata_number = df_merge_with_dpid(df_data[1], DEPARTMENT_DATA)
    construction_number = df_merge_with_dpid(df_data[2], DEPARTMENT_DATA)

    checked_pdata_number = checked_pdata_number.groupby(['TYPE3'])['COUNT'].sum()
    pdata_number = pdata_number.groupby(['TYPE3'])['COUNT'].sum()
    construction_number = construction_number.groupby(['TYPE3'])['COUNT'].sum()

    data = pd.concat(
        [checked_pdata_number.to_frame(name='CHACHU_PROB'),
         pdata_number.to_frame(name='HIDDEN_PROB'),
         construction_number.to_frame(name='CONSTRUCTION')],
        axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['AVG_NUMBER'] = data['CONSTRUCTION'].mean()
    data.fillna(0, inplace=True)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['CHACHU_PROB'], row['HIDDEN_PROB'], row['CONSTRUCTION'], row['AVG_NUMBER']), axis=1)
    data.drop(columns=['CHACHU_PROB', 'HIDDEN_PROB', 'CONSTRUCTION', 'AVG_NUMBER'], inplace=True, axis=1)
    return data


# 总体暴露度
def _stats_total_problem_exposure(months_ago):
    """
    A=查处施工类问题数/基础问题库中施工类问题总数
    B=单位本月施工次数/专业施工平均次数
    得分=A/0.3*100/B
    """
    stats_month = get_custom_month(months_ago)
    major = get_major_dpid(RISK_TYPE)
    # 当月施工次数
    df_construction_count = DF_CONSTRUCTION_COUNT
    # 基础问题库中施工类问题总数
    df_base_prob = pd_query(ALL_BASE_PROBLEM_SQL.format(CHECK_RISK_IDS))
    # 查处施工类问题数
    df_chachu_prob = pd_query(ALL_PROBLEM_NUMBER_SQL.format(*stats_month, CHECK_RISK_IDS))

    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '查处施工类问题数: {0:.0f}个<br/>基础问题库中施工类问题总数: {1:.0f}个<br/>当月施工次数: {2:.0f}次,<br/>专业施工平均次数: {3:.2f}次'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [[df_chachu_prob, df_base_prob, df_construction_count]]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            # _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), i, title[i]))

    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 1, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 1, risk_type=RISK_TYPE)

    # 计算子字数
    df_chachu_prob_num = ALL_PROBLEM_NUMBER.groupby(['TYPE3'])['COUNT'].sum()
    df_base_prob_num = ALL_BASE_PROBLEM.groupby(['TYPE3'])['COUNT'].sum()
    df_construction_prob_num = MONTH_CONSTRUCTION_COUNT.groupby(['TYPE3'])['COUNT'].sum()
    data = pd.concat([df_chachu_prob_num.to_frame(name='CHACHU_PROB'),
                      df_base_prob_num.to_frame(name='BASE_PROB'),
                      df_construction_prob_num.to_frame(name='CONSTRUCTION')], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['AVG_NUMBER'] = data['CONSTRUCTION'].mean()
    data['SCORE'] = data.apply(
        lambda row: (row['CHACHU_PROB'] / row['BASE_PROB'] / 0.3 * 100 / (row['CONSTRUCTION'] / row['AVG_NUMBER']))
        if (row['CONSTRUCTION'] / row['AVG_NUMBER']) != 0 else 0, axis=1)
    data.drop(columns=['CHACHU_PROB', 'BASE_PROB', 'CONSTRUCTION', 'AVG_NUMBER'], inplace=True)
    data = data.reset_index()
    data.rename(columns={'index': 'TYPE3'}, inplace=True)

    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        1,
        months_ago,
        'SCORE',
        'SCORE_a',
        lambda x: min(x, 100),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


# 较严重隐患暴露
def _stats_hidden_problem_exposure(months_ago):
    """
    A=查处施工事故关键隐患问题/施工事故关键隐患问题总数。
    B=单位本月施工次数/专业施工平均次数
    得分=A/0.1*100/B
    """
    major = get_major_dpid(RISK_TYPE)
    stats_month = get_custom_month(months_ago)
    checked_pdata = pd_query(
        CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month, CHECK_RISK_IDS))
    pdata = pd_query(HIDDEN_PROBLEM_POINT_SQL.format(CHECK_RISK_IDS))
    df_construction_count = DF_CONSTRUCTION_COUNT

    # 导出中间计算过程数据

    calc_basic_data = []
    title = [
        '查处施工事故关键隐患问题数: {0:.0f}个<br/>施工事故关键隐患问题总数: {1:.0f}个<br/>当月施工次数: {2:.0f}次<br/>专业施工平均次数: {3:.2f}次'
    ]
    for i, data in enumerate(
            [[checked_pdata, pdata, df_construction_count]]):
        for j, func in enumerate([
            _calc_hidden_prob_number_per_person
        ]):
            calc_basic_data.append(
                func(data.copy(), i, title[i])
            )

    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(
        calc_df_data, 5, 2, 3, months_ago, risk_type=RISK_TYPE)
    write_export_basic_data_to_mongo(
        calc_basic_data_rst, months_ago, 3, 5, 2, risk_type=RISK_TYPE)

    # COUNT,TYPE3需要更改
    hidden_problem_point_num = HIDDEN_PROBLEM_POINT.groupby(['TYPE3'])['COUNT'].sum()
    checked_hidden_problem_point_num = CHECKED_HIDDEN_PROBLEM_POINT.groupby(['TYPE3'])['COUNT'].sum()
    df_construction_prob_num = MONTH_CONSTRUCTION_COUNT.groupby(['TYPE3'])['COUNT'].sum()

    data = pd.concat(
        [checked_hidden_problem_point_num.to_frame(name='CHACHU_PROB'),
         hidden_problem_point_num.to_frame(name='HIDDEN_PROB'),
         df_construction_prob_num.to_frame(name='CONSTRUCTION')], axis=1, sort=False)
    data.fillna(0, inplace=True)
    data['AVG_NUMBER'] = data['CONSTRUCTION'].mean()
    data['SCORE'] = data.apply(
        lambda row: (row['CHACHU_PROB'] / row['HIDDEN_PROB'] / 0.1 * 100 / (row['CONSTRUCTION'] / row['AVG_NUMBER']))
        if (row['CONSTRUCTION'] / row['AVG_NUMBER']) != 0 else 0, axis=1)
    data.drop(columns=['CHACHU_PROB', 'HIDDEN_PROB', 'CONSTRUCTION', 'AVG_NUMBER'], inplace=True)
    data = data.reset_index()
    data.rename(columns={'index': 'TYPE3'}, inplace=True)

    rst_child_score = calc_child_index_type_sum(
        data,
        2,
        5,
        2,
        months_ago,
        'SCORE',
        'SCORE_b',
        lambda x: min(x, 100),
        _choose_dpid_data,
        risk_type=RISK_TYPE)
    return rst_child_score


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """
    从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣0.1分，较大风险扣2分，严重风险扣4分；
    problem_risk_score(问题危险等级权重): {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    :param months_ago:
    :return:
    """
    return stats_other_problem_exposure(
        CHECK_RISK_IDS, SELF_CHECK_PROBLEM_SQL, OTHER_CHECK_PROBLEM_SQL,
        SAFETY_PRODUCE_INFO_SQL, ZHANDUAN_DPID_DATA, DEPARTMENT_DATA,
        months_ago, RISK_TYPE, _choose_dpid_data, problem_risk_score={'1': 4, '2': 2, '3': 0.1, })


def execute(months_ago, risk_name, risk_type):
    # 部门按站段聚合
    _get_sql_data(months_ago, risk_name, risk_type)
    # 分别表示【总体暴露度，较严重隐患暴露，他查问题暴露度】
    child_index_func = [
        _stats_total_problem_exposure,
        _stats_hidden_problem_exposure,
        _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'e']]
    item_weight = [0.7, 0.3, -1]
    summizet_child_index(
        child_score,
        _choose_dpid_data,
        2,
        5,
        months_ago,
        item_name,
        item_weight,
        risk_type=RISK_TYPE)

    update_major_maintype_weight(9, risk_type, main_type=5, child_index_list=[1, 2, 5],
                                 child_index_weight=[0.7, 0.3, -1])

    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


if __name__ == '__main__':
    pass
