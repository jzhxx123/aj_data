#!/usr/bin/python3
# -*- coding: utf-8 -*-

import json

import pandas as pd
from flask import current_app
from pymongo import ASCENDING, HASHED, IndexModel

from app import mongo
from app.data.util import (pd_query, write_bulk_mongo)
from app.utils.data_update_models import UpdateMonth
from app.utils.decorator import init_wrapper
from app.utils.common_func import get_today
from dateutil.relativedelta import relativedelta

sql_dep = "SELECT DEPARTMENT_ID, HIERARCHY, TYPE1, TYPE2, TYPE3, TYPE4, \
                TYPE5, TYPE6, TYPE7 FROM t_department"


def get_map_data(mon_point):
    main_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME,
            b.FK_DEPARTMENT_ID
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
        WHERE DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59', '%%Y-%%m-%%d %%H:%%i:%%s');
    """.format(mon_point[0], mon_point[1])
    data = pd_query(main_sql)
    if data.empty:
        return
    data['MON'] = mon

    sql_dep = "SELECT DEPARTMENT_ID, TYPE3, TYPE4, \
                TYPE5 FROM t_department"

    dep = pd_query(sql_dep)
    data = pd.merge(
        data,
        dep,
        how='left',
        left_on="FK_DEPARTMENT_ID",
        right_on="DEPARTMENT_ID")
    del data['DEPARTMENT_ID']
    del data['OCCURRENCE_TIME']
    result = []
    # pandas 0.23.3 版本 TYPE4 值为空是会报错: index implies
    for x in range(3, 6):
        xdata = data.drop_duplicates(
            ['PK_ID', f'TYPE{x}'], keep='first')
        xdata = xdata[pd.notnull(xdata[f'TYPE{x}'])]
        xdata = xdata.groupby([f'TYPE{x}', 'MON']).size()
        for index in xdata.index:
            value = xdata.loc[index]
            result.append({
                'DPID': index[0],
                'MON': index[1],
                'COUNT': int(value),
            })
    current_app.logger.debug('complete: format data.')
    coll_name = '{}map_safety_produce_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_index([("DPID", HASHED)], background=True)


def resp_divide(val):
    '''责任区分'''
    if val is None:
        return 0
    elif val == '涉及局属单位责任' or val == '局属单位责任':  # 责任
        return 1
    elif val == '待定':
        return 3
    else:  # 非责任
        return 2


def resp_type_divide(val):
    val_dict = {
        1: '全部责任',
        2: '主要责任',
        3: '同等主要责任',
        4: '重要责任',
        5: '同等重要责任',
        6: '次要责任',
        7: '同等次要责任',
        8: '追究主要责任',
        9: '追究重要责任',
        10: '追究次要责任',
        11: '非责任'
    }
    return val_dict.get(val)


def belong_divide(val):
    '''生产信息所属辖区分布'''
    if val == 0:
        return 0
    elif val == 1:
        return '四川省'
    elif val == 2:
        return '重庆市'
    elif val == 3:
        return '贵州省'
    else:
        return '云南省'


def main_type_divide(val):
    '''安全生产信息大类分布'''
    if val == 0:
        return 0
    elif val == 1:
        return '事故'
    elif val == 2:
        return '故障'
    else:
        return '综合'


def detail_type_divide(val):
    if val == 0:
        return 0
    elif val == 1:
        return '行车'
    elif val == 2:
        return '劳安'
    else:
        return '路外'


def get_dpid_data(mon_point):
    sql_safe_produce_info = """SELECT
        a.PK_ID,
        a.OCCURRENCE_TIME,
        a.MAIN_TYPE,
        a.DETAIL_TYPE,
        a.CATEGORY,
        a.OVERVIEW,
        a.STATUS,
        a.REASON,
        a.PROFESSION,
        a.RESPONSIBILITY_DIVISION_NAME,
        # a.RESPONSIBILITY_UNIT,
        a.PONDERANCE_NUMBER,
        a.IS_MATERIALS,
        a.RANK,
        a.CODE,
        a.NAME,
        a.ADMINISTRATIVE_DIVISION,
        a.ACCIDENT_IDENTIFIED_NUMBER,
        a.LINE_NAME,
        b.DEPARTMENT_NAME as RESPONSIBILITY_UNIT,
        c.DEPARTMENT_ID,
        c.TYPE3,
        c.TYPE4,
        c.TYPE5,
        d.RISK_NAME,
        f.PROBLEM_POINT AS DIRECT_REASON,
        g.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS d
                    ON d.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59', '%%Y-%%m-%%d %%H:%%i:%%s');
    """.format(mon_point[0], mon_point[1])
    data = pd_query(sql_safe_produce_info)
    if data.empty:
        return
    data = data.fillna(0)
    data['DATE'] = data['OCCURRENCE_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = mon

    data['CLOCK'] = data['OCCURRENCE_TIME'].apply(
        lambda x: str(x)[11:16].replace('-', ''))
    data['RANK'] = data['RANK'].apply(int)
    data['NAME'] = data.apply(lambda x: str(
        x['NAME']).replace(str(x['CODE']), '').replace('.', ''), axis=1)
    data['MAIN_TYPE'] = data['MAIN_TYPE'].apply(int)
    data['DETAIL_TYPE'] = data['DETAIL_TYPE'].apply(int)
    data['RESPONSIBILITY_NAME'] = data['RESPONSIBILITY_DIVISION_NAME'].apply(
        resp_divide)
    data['RESPONSIBILITY_IDENTIFIED_NAME'] = data[
        'RESPONSIBILITY_IDENTIFIED'].apply(resp_type_divide)
    data['BELONG'] = data['ADMINISTRATIVE_DIVISION'].apply(belong_divide)
    data['MAIN_CLASS'] = data['MAIN_TYPE'].apply(main_type_divide)
    data['DETAIL_CLASS'] = data['DETAIL_TYPE'].apply(detail_type_divide)
    del data['ADMINISTRATIVE_DIVISION']
    del data['OCCURRENCE_TIME']
    del data['RESPONSIBILITY_DIVISION_NAME']
    data.rename(columns={'DEPARTMENT_ID': 'DPID'}, inplace=True)
    major_sql = "SELECT DEPARTMENT_ID, TYPE2, NAME FROM t_department;"
    major_data = pd_query(major_sql)
    data['DPID'] = data['DPID'].apply(str)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'TYPE2': major_data['TYPE2']
        }),
        how='left',
        left_on='DPID',
        right_on='DPID')
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'MAJOR': major_data['NAME']
        }),
        how='left',
        left_on='TYPE2',
        right_on='DPID')
    del data['TYPE2']
    del data['DPID_x']
    del data['DPID_y']
    # majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    # 2018-08-17: 取消专业过滤，写入所有安全信息以便大屏显示
    # data = data.loc[data['MAJOR'].isin(majors)]
    result = []
    for index in data.index:
        result.append(json.loads(data.loc[index].to_json()))
    coll_name = '{}detail_safety_produce_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)
    mongo.db[coll_name].create_indexes([
        IndexModel([('TYPE3', ASCENDING), ('MON', ASCENDING)]),
        IndexModel([('TYPE4', ASCENDING), ('MON', ASCENDING)]),
        IndexModel([('TYPE5', ASCENDING), ('MON', ASCENDING)])
    ])


def get_global_data(mon_point):
    global_data_sql = """
    SELECT
        a.PK_ID,
        a.OCCURRENCE_TIME,
        a.PONDERANCE_NUMBER,
        c.CHECK_ITEM_NAME,
        c.PROBLEM_CLASSIFY_NAME,
        c.PROBLEM_POINT,
        d.FK_DEPARTMENT_ID AS DPID
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_problem_base AS b
            ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_problem_base AS c ON b.FK_PROBLEM_BASE_ID = c.PK_ID
            LEFT JOIN
        t_safety_produce_info_refer_department AS d
            ON a.PK_ID = d.FK_SAFETY_PRODUCE_INFO_ID
            WHERE DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.OCCURRENCE_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59', '%%Y-%%m-%%d %%H:%%i:%%s');
    """.format(mon_point[0], mon_point[1])
    data = pd_query(global_data_sql)
    if data.empty:
        return
    data['DATE'] = data['OCCURRENCE_TIME'].apply(
        lambda x: int(str(x)[:10].replace('-', '')))
    data['MON'] = mon
    del data['OCCURRENCE_TIME']
    major_sql = "SELECT DEPARTMENT_ID, TYPE2, NAME FROM t_department;"
    major_data = pd_query(major_sql)
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'TYPE2': major_data['TYPE2']
        }),
        how='left',
        left_on='DPID',
        right_on='DPID')
    data = pd.merge(
        data,
        pd.DataFrame({
            'DPID': major_data['DEPARTMENT_ID'],
            'MAJOR': major_data['NAME']
        }),
        how='left',
        left_on='TYPE2',
        right_on='DPID')
    del data['TYPE2']
    del data['DPID_x']
    del data['DPID_y']
    majors = ['供电', '车辆', '工务', '机务', '电务', '车务']
    data = data.loc[data['MAJOR'].isin(majors)]
    # data = data.drop_duplicates(['PK_ID', 'MAJOR'])
    data.drop_duplicates(inplace=True)
    result = []
    for index in data.index:
        result.append(json.loads(data.loc[index].to_json()))
    coll_name = '{}global_safety_produce_info'.format(prefix)
    if prefix == 'daily_':
        mongo.db[coll_name].drop()
    else:
        mongo.db[coll_name].delete_many({'MON': mon})
    write_bulk_mongo(coll_name, result)


def devide_type(ONEtype):
    if 'A' in ONEtype:
        return 'A'
    elif 'B' in ONEtype:
        return 'B'
    elif 'C' in ONEtype:
        return 'C'
    elif 'D' in ONEtype:
        return 'D'
    else:
        return 0


def get_chewu_own_name(own):
    if own is not None:
        _id = "type_" + str(own)
        owns = {"type_1": "运输", "type_2": "客运", "type_3": "货运", "type_4": "其他"}
        return owns[_id]
    else:
        return


def handle(mon_point):
    # 获取各个部门的安全生产信息（主屏）
    get_map_data(mon_point)
    # 获取各个部门的安全生产信息详情数据(副屏)
    get_dpid_data(mon_point)
    # 获取全局安全生产信息数据
    get_global_data(mon_point)


@init_wrapper
def execute(months: UpdateMonth):
    # months *= -1
    # global months_ago
    # months_ago = months
    # mon_point = get_previous_month(months)
    global prefix, mon
    prefix = months.prefix
    mon = months.year_month
    mon_point = (months.start_date, months.end_date)
    handle(mon_point)
    # 执行一次真正的daily
    if(months.start_date < months.end_date):
        prefix = 'daily_'
        t2 = get_today()
        t1 = t2 - relativedelta(days=1)
        mon_point = (t1, t2)
        handle(mon_point)
    return 'OK'


if __name__ == '__main__':
    current_app.logger.debug('Done!')
