#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''月初更新-将montly_xx表里的一年前的数据迁移到对应的history_xx表里
'''

from flask import current_app

from app import mongo
from app.data.util import paste_monthly_to_history
from app.utils.common_func import get_today


def migration():
    all_collection = [
        x[8:] for x in mongo.db.list_collection_names() if x[:8] == 'monthly_'
    ]
    r = list(map(paste_monthly_to_history, all_collection))
    return r


def delete_daily():
    if get_today().day != current_app.config.get(
                'UPDATE_DAY'):
        return
    all_collection = [
        x for x in mongo.db.list_collection_names() if x[:6] == 'daily_'
    ]
    r = list(map(mongo.db.drop_collection, all_collection))
    return r


def execute(months):
    # 将monthly_相关表里超过一年的数据取出，并写入history_对应表
    migration()
    # 删除daily_相关表
    delete_daily()
    return 'OK'


if __name__ == '__main__':
    pass
