#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   util
Description:
Author:    
date:         2019-06-21
-------------------------------------------------
Change Activity:2019-06-21 14:08
-------------------------------------------------
"""
import pandas as pd
from app.data.index.common import (
    df_merge_with_dpid, append_major_column_to_df)

def _combine_second_filed_to_str(row, first_title, second_title):
    """将涉及到的2个维度属性数据拼接成一个描述性字符串
    """
    rst = {}
    for idx in row.index:
        first_key = first_title[int(idx[0])]
        val = f'{second_title[idx[1]]}{int(row.loc[idx])}个;'
        rst.update({first_key: rst.get(first_key, '') + val})
    return '<br/>'.join([f'{k}:{v}' for k, v in rst.items()])


def export_basic_data_dicttype(data,
                               dpid_data,
                               main_type,
                               detail_type,
                               hierarchy,
                               months_ago,
                               func_html_desc,
                               risk_type=None):

    data = data.groupby(['TYPE3'])['COUNT'].sum()
    data = append_major_column_to_df(dpid_data, data.to_frame(name='CONTENT'))
    data.fillna(0, inplace=True)
    data['CONTENT'] = data['CONTENT'].apply(lambda x: func_html_desc(x))
    return data


def export_basic_data_tow_field_monthly(data,
                                        dpid_data,
                                        major_data,
                                        main_type,
                                        detail_type,
                                        hierarchy,
                                        months_ago,
                                        first_title,
                                        second_title,
                                        risk_type=None):
    data = pd.DataFrame(
        data=data, columns=['FK_DEPARTMENT_ID', 'FIRST', 'SECOND'])
    data = df_merge_with_dpid(data, dpid_data)
    data = data.groupby(['TYPE3', 'SECOND', 'FIRST']).size()
    data = data.unstack().unstack()
    data.fillna(0, inplace=True)
    data['CONTENT'] = data.apply(
        lambda row: _combine_second_filed_to_str(row, first_title, second_title),
        axis=1)
    data = append_major_column_to_df(
        major_data,
        pd.DataFrame(
            index=data.index,
            data=data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    return data
