# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app

from app.data.health_index.common_sql import (CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
                                              EXTERNAL_PERSON_SQL,
                                              WORK_LOAD_SQL, ZHANDUAN_DPID_SQL)
from app.data.health_index.check_intensity_sql import BANZU_POINT_SQL
from app.data.health_index.problem_exposure_sql import (
    CHECK_PROBLEM_SQL, CHECKED_HIDDEN_PROBLEM_POINT_SQL,
    EXPOSURE_PROBLEM_DEPARTMENT_SQL, HIDDEN_KEY_PROBLEM_MONTH_SQL,
    HIDDEN_KEY_PROBLEM_SQL, HIDDEN_PROBLEM_POINT_SQL, OTHER_CHECK_PROBLEM_SQL,
    SAFETY_PRODUCE_INFO_SQL, SELF_CHECK_PROBLEM_SQL)
from app.data.index.common import (
    append_major_column_to_df, calc_child_index_type_sum,
    calc_extra_child_score_groupby_major_two, combine_child_index_func,
    df_merge_with_dpid, export_basic_data_one_field_monthly,
    format_export_basic_data, get_zhanduan_deparment, summizet_child_index,
    summizet_operation_set, write_export_basic_data_to_mongo)
from app.data.index.util import (get_custom_month, get_months_from_201712,
                                 validate_exec_month)
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _get_sql_data(months_ago):
    global ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, WORK_LOAD
    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    month = int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    WORK_LOAD = WORK_LOAD.groupby(['TYPE3'])['COUNT'].sum()
    WORK_LOAD = WORK_LOAD.to_frame(name='PERSON_NUMBER')


def _calc_score_by_formula(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = _ratio * 50 + 90
    elif _ratio >= -0.15:
        _score = (_ratio + 0.15) * 6 + 80
    elif _ratio >= -0.3:
        _score = (_ratio + 0.15) * 200 + 80
    else:
        _score = (_ratio + 0.3) * 300 + 60
        _score = 0 if _score < 0 else _score
    return _score


def _calc_value_per_person(series, weight, hierarchy):
    global WORK_LOAD
    data = pd.concat(
        [series.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data['ratio'] = data['prob'] / data['PERSON_NUMBER']
    return calc_extra_child_score_groupby_major_two(data,
                                                _choose_dpid_data(hierarchy),
                                                'ratio',
                                                _calc_score_by_formula, weight=weight,
                                                numerator='prob', denominator='PERSON_NUMBER')


def _calc_prob_number_per_person(df_data, weight, hierarchy):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby([f'TYPE{hierarchy}']).size()
    return _calc_value_per_person(prob_number, weight, hierarchy)


def _calc_prob_score_per_person(df_data, weight, hierarchy):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby(
        [f'TYPE{hierarchy}'])['PROBLEM_SCORE'].sum()
    return _calc_value_per_person(prob_score, weight, hierarchy)


def _calc_basic_prob_number_per_person(df_data, i, title):
    prob_number = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_number = prob_number.groupby(['TYPE3']).size()
    global WORK_LOAD
    data = pd.concat(
        [prob_number.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data[f'number_{i}'] = data.apply(
        lambda row: title.format(row['prob'], row['PERSON_NUMBER']), axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


def _calc_basic_prob_score_per_person(df_data, i, title):
    prob_score = df_merge_with_dpid(df_data, DEPARTMENT_DATA)
    prob_score = prob_score.groupby(['TYPE3'])['PROBLEM_SCORE'].sum()
    global WORK_LOAD
    data = pd.concat(
        [prob_score.to_frame(name='prob'), WORK_LOAD], axis=1, sort=False)
    data[f'score_{i}'] = data.apply(
        lambda row: title.format(f'质量分:{round(row["prob"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(columns=['prob', 'PERSON_NUMBER'], inplace=True, axis=1)
    return data


# 普遍性暴露
def _stats_total_problem_exposure(months_ago):
    """（数量占40%，质量分占60%）：
    总问题数（质量分）/人数与本专业基数比较（20%），
    一般及以上问题数（质量分）/人数与本专业基数比较（40%），
    作业项问题数（质量分）/人数与本专业作业项基数比较（20%），
    一般及以上作业项问题数（质量分）/人数与本专业作业项基数比较（20%）。
    """
    stats_month = get_custom_month(months_ago)
    # 问题
    base_data = pd_query(CHECK_PROBLEM_SQL.format(*stats_month))
    # 一般及以上风险问题
    risk_data = base_data[base_data['RISK_LEVEL'] < 4]
    # 作业项问题
    level_data = base_data[base_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 作业项问题（一般及以上风险）
    level_risk_data = base_data[(base_data['LEVEL'].isin(['A', 'B', 'C', 'D']))
                                & (base_data['RISK_LEVEL'] < 4)]

    weight_item = [0.2, 0.4, 0.2, 0.2]
    weight_part = [0.4, 0.6]
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = [
        '总问题数({0})/人数({1})', '一般及以上问题数({0})/人数({1})', '作业项问题数({0})/人数({1})',
        '一般及以上作业项问题数({0})/人数({1})'
    ]
    # 导出中间过程
    for i, data in enumerate(
            [base_data, risk_data, level_data, level_risk_data]):
        for j, func in enumerate([
            _calc_basic_prob_number_per_person,
            _calc_basic_prob_score_per_person
        ]):
            calc_basic_data.append(func(data.copy(), i, title[i]))
    calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
    calc_df_data.fillna('', inplace=True)
    columns = calc_df_data.columns.tolist()
    calc_df_data['CONTENT'] = calc_df_data.apply(
        lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
    calc_df_data = append_major_column_to_df(
        _choose_dpid_data(3),
        pd.DataFrame(
            index=calc_df_data.index,
            data=calc_df_data.loc[:, 'CONTENT'].values,
            columns=['CONTENT']))
    calc_basic_data_rst = format_export_basic_data(calc_df_data, 5, 1, 3,
                                                   months_ago, index_type=-1)
    write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 5, 1, index_type=-1)
    # 计算子指数
    for hierarchy in HIERARCHY:
        score = []
        for i, data in enumerate(
                [base_data, risk_data, level_data, level_risk_data]):
            # 人均问题数，人均质量分
            for j, func in enumerate([_calc_prob_number_per_person,
                                      _calc_prob_score_per_person]):
                weight = weight_item[i] * weight_part[j]
                score.append(func(data.copy(), weight, hierarchy))
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_a_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(df_rst, _choose_dpid_data(hierarchy), column,
                               hierarchy, -1, 5, 1, months_ago)
        rst_child_score.append(df_rst)
    return rst_child_score


# 较严重隐患暴露指数
def _stats_hidden_problem_exposure(months_ago):
    """较严重隐患暴露:本专业内,其它单位查出的问题项点(本单位基础问题库中有的),
    本单位未查出的按1分/项扣。分值=100-扣
    """
    stats_month = get_custom_month(months_ago)
    checked_pdata = pd_query(
        CHECKED_HIDDEN_PROBLEM_POINT_SQL.format(*stats_month))
    pdata = pd_query(HIDDEN_PROBLEM_POINT_SQL)
    deduct_score = {}
    # 记录本月问题扣分次数
    deduct_number_month = []
    for idx, row in checked_pdata.iterrows():
        major_data = pdata[(pdata['MAJOR'] == row['MAJOR'])
                           & (pdata['PK_ID'] == row['PK_ID'])]
        for m_idx, m_row in major_data.iterrows():
            if checked_pdata[
                (checked_pdata['DEPARTMENT_ID'] == m_row['DEPARTMENT_ID'])
                & (checked_pdata['PK_ID'] == m_row['PK_ID'])
            ].empty:
                deduct_score.update({
                    m_row['DEPARTMENT_ID']:
                        deduct_score.get(m_row['DEPARTMENT_ID'], 0) + 1
                })
                deduct_number_month.append([m_row['DEPARTMENT_ID'], 1])
    df_pdata = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_pdata = df_merge_with_dpid(df_pdata, DEPARTMENT_DATA)
    # 生成中间计算过程数据
    export_basic_data_one_field_monthly(deduct_number_month, DEPARTMENT_DATA,
                                        _choose_dpid_data(3), 5, 2, 3,
                                        months_ago, lambda x: '本月个数', index_type=-1)

    rst_child_score = calc_child_index_type_sum(
        df_pdata, -1, 5, 2, months_ago, 'SCORE', 'SCORE_b',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data)
    return rst_child_score


# 事故隐患问题暴露度
def _stats_problem_exposure(months_ago):
    """连续3月无的扣1分/条，连续4个月无的扣1分/条，…扣月份-2分/条。得分=100-扣分。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    stats_months = get_months_from_201712(months_ago)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    }
    # hidden_problem 为尚未暴露的隐患问题问题
    hidden_problem = set(pd_query(HIDDEN_KEY_PROBLEM_SQL)['PID'].values)
    # 用来记录连续3个月，4个月，5个月，6个月未暴露的问题个数
    hidden_problem_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # month_hidden_problem 为该月暴露出的隐患问题
        month_hidden_problem = set(
            pd_query(HIDDEN_KEY_PROBLEM_MONTH_SQL.format(
                mon[0], mon[1]))['PID'].values)
        if idx > 2 and idx < (len(stats_months) - 1):
            # exposure_problem 为连续{idx}月未暴露但是第{idx+1}个月暴露的隐患问题
            # exposure_problem = hidden_problem.intersection(
            #     month_hidden_problem)
            # for each_problem in exposure_problem:
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 2
                })
                hidden_problem_num_monthly.append([dpid, idx])
        # 一直到初始月份未暴露的隐患问题
        if idx == (len(stats_months) - 1):
            for each_problem in hidden_problem:
                dpid = each_problem.split('||')[0]
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx - 1
                })
                hidden_problem_num_monthly.append([dpid, idx])
        hidden_problem = hidden_problem.difference(month_hidden_problem)
    # 导出中间过程
    export_basic_data_one_field_monthly(
        hidden_problem_num_monthly,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        3,
        3,
        months_ago,
        lambda x: f'连续{x}个月无',
        title='事故隐患个数', index_type=-1)

    df_hidden_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_hidden_prob = df_merge_with_dpid(df_hidden_prob, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        df_hidden_prob, -1, 5, 3, months_ago, 'SCORE', 'SCORE_c',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data)
    return rst_child_score


# 班组问题暴露度
def _stats_banzu_problem_exposure(months_ago):
    """问题为空白的班组（一个月的扣1分/个，连续2月无的扣2/个，
    连续3个月无的扣/个）得分=100-扣分

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)
    """
    stats_months = get_months_from_201712(months_ago)
    # hidden_banzu为问题为空白的班组
    hidden_banzu = set(pd_query(BANZU_POINT_SQL)['FK_DEPARTMENT_ID'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {k: 0 for k in hidden_banzu}
    # 用来记录每个班组每个月问题为空白的个数
    exposure_banzu_num_monthly = []
    for idx, mon in enumerate(stats_months):
        # i_month_exposure_banzu 为该月问题暴露出的班组
        i_month_exposure_banzu = set(
            pd_query(EXPOSURE_PROBLEM_DEPARTMENT_SQL.format(
                mon[0], mon[1]))['FK_DEPARTMENT_ID'].values)
        # i_month_exposure_banzu 为连续{idx}月未暴露但是第{idx+1}个月暴露问题的班组
        i_month_exposure_banzu = hidden_banzu.intersection(
            i_month_exposure_banzu)
        hidden_banzu = hidden_banzu.difference(i_month_exposure_banzu)
        if idx > 0:
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx + 1
                })
                exposure_banzu_num_monthly.append([dpid, mon[1]])
        # 一直到初始月份未暴露问题的班组
        if idx == (len(stats_months) - 1):
            for dpid in i_month_exposure_banzu:
                deduct_score.update({
                    dpid: deduct_score.get(dpid, 0) + idx + 2
                })
                exposure_banzu_num_monthly.append([dpid, mon[1]])
    data = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    data = data[data['SCORE'] > 0]
    data = df_merge_with_dpid(data, DEPARTMENT_DATA)
    # 导出中间计算数据
    export_basic_data_one_field_monthly(
        exposure_banzu_num_monthly,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        4,
        3,
        months_ago,
        lambda x: f'{x[:4]}年{x[5:7]}月',
        title='问题为空白的班组', index_type=-1)

    rst_child_score = calc_child_index_type_sum(
        data, -1, 5, 4, months_ago, 'SCORE', 'SCORE_d',
        lambda x: 0 if (100 - x) < 0 else round((100 - x), 2),
        _choose_dpid_data)
    return rst_child_score


# 他查问题暴露度
def _stats_other_problem_exposure(months_ago):
    """从他查问题（包括生产信息暴露问题和直接原因）分析1个月未自查出该项问题，最高扣30分。
    一般风险问题一条扣1分，较大风险扣3分，严重风险扣5分；
    事故的暴露问题和直接原因在上述基础上*3；故障的*2，综合信息同问题。

    Arguments:
        months_ago {int} -- 前第-N个月（N为负数)

    Returns:
        [type] -- [description]
    """
    calc_month = get_custom_month(months_ago)
    self_check_problem = set(
        pd_query(SELF_CHECK_PROBLEM_SQL.format(*calc_month))[
            'PROBLEM_DPID_RISK'].values)
    safety_produce_info = pd_query(SAFETY_PRODUCE_INFO_SQL.format(*calc_month))
    other_check_problem = set(
        pd_query(OTHER_CHECK_PROBLEM_SQL.format(*calc_month))[
            'PROBLEM_DPID_RISK'].values)
    # 初始化一个各站段的扣分字典
    deduct_score = {
        k: 0
        for k in ZHANDUAN_DPID_DATA.loc[:, 'DEPARTMENT_ID'].values
    }
    problem_risk_score = {
        '1': 4,
        '2': 2,
        '3': 0.1,
    }
    # 保存中间计算过程涉及到的数据
    calc_problems = []
    # 未自查出他查问题（检查问题）
    other_not_self_problem = other_check_problem.difference(self_check_problem)
    for each_problem in other_not_self_problem:
        each_problem = each_problem.split('||')
        problem_dpid = each_problem[1]
        problem_score = problem_risk_score.get(each_problem[2], 0)
        if problem_dpid in deduct_score:
            deduct_score.update({
                problem_dpid:
                    deduct_score.get(problem_dpid) + problem_score
            })
            calc_problems.append([problem_dpid, each_problem[2]])
    # 未自查出安全生产信息问题
    for idx, row in safety_produce_info.iterrows():
        if row['PROBLEM_DPID_RISK'] not in self_check_problem:
            problem_dpid = row['PROBLEM_DPID_RISK'].split('||')[1]
            problem_score = problem_risk_score.get(
                row['PROBLEM_DPID_RISK'].split('||')[2],
                0) * (4 - int(row['MAIN_TYPE']))
            if problem_dpid in deduct_score:
                deduct_score.update({
                    problem_dpid:
                        deduct_score.get(problem_dpid) + problem_score
                })
                calc_problems.append([problem_dpid, each_problem[2]])
    # 导出中间计算过程
    first_title = {'1': '严重风险', '2': '较大风险', '3': '一般风险'}
    export_basic_data_one_field_monthly(
        calc_problems,
        DEPARTMENT_DATA,
        _choose_dpid_data(3),
        5,
        5,
        3,
        months_ago,
        lambda x: first_title.get(x),
        title='他查问题个数', index_type=-1)

    df_other_prob = pd.DataFrame(
        data=list(deduct_score.items()), columns=['FK_DEPARTMENT_ID', 'SCORE'])
    df_other_prob = df_merge_with_dpid(df_other_prob, DEPARTMENT_DATA)
    rst_child_score = calc_child_index_type_sum(
        df_other_prob, -1, 5, 5, months_ago, 'SCORE', 'SCORE_e',
        lambda x: 30 if x > 30 else x, _choose_dpid_data)
    return rst_child_score


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)
    # 分别表示【总体暴露度，较严重隐患暴露，事故隐患问题暴露度，班组问题暴露度，他查问题扣分】
    child_index_func = [
        _stats_total_problem_exposure, _stats_hidden_problem_exposure,
        _stats_problem_exposure, _stats_banzu_problem_exposure,
        _stats_other_problem_exposure
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd', 'e']]
    item_weight = [0.50, 0.15, 0.25, 0.1, -1]
    update_major_maintype_weight(index_type=-1, main_type=5,
                                 child_index_list=[1, 2, 3, 4, 5],
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, -1, 5, months_ago,
                         item_name, item_weight)
    current_app.logger.debug(
        '├── └── problem_exposure index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
