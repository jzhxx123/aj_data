#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
    date: 2018/07/31
    desc: 考核力度指数
"""
import pandas as pd
from flask import current_app
from app.data.health_index.assess_intensity_sql import (
    ASSESS_QUANTIFY_SQL, ASSESS_RESPONSIBLE_SQL, ASSESS_REVISE_SQL,
    ASSESS_RROBLEM_SQL, AWARD_RETURN_SQL)
from app.data.health_index.check_intensity_sql import KAOHE_PROBLEM_SQL
from app.data.health_index.common_sql import (
    CHECK_PROBLEM_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL, EXTERNAL_PERSON_SQL,
    WORK_LOAD_SQL, ZHANDUAN_DPID_SQL, CHECK_PROBLEM_ONOUTWAY_SQL)
from app.data.index.common import (
    calc_child_index_type_divide,
    calc_child_index_type_divide_major, add_avg_number_by_major,
    calc_extra_child_score_groupby_major_two,
    combine_and_format_basic_data_to_mongo, combine_child_index_func,
    df_merge_with_dpid, get_zhanduan_deparment, summizet_child_index,
    summizet_operation_set)
from app.data.index.util import get_custom_month, validate_exec_month
from app.data.util import pd_query, update_major_maintype_weight


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _summizet_assess_money(year, month):
    data = pd.concat(
        [
            pd_query(each_sql.format(year, month)) for each_sql in [
            ASSESS_REVISE_SQL, ASSESS_RROBLEM_SQL, ASSESS_QUANTIFY_SQL,
            ASSESS_RESPONSIBLE_SQL
        ]
        ],
        axis=0)
    return data


def _get_sql_data(months_ago):
    global ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, ZHANDUAN_DPID_DATA, \
        CHEJIAN_DPID_DATA, ASSESS_RESPONSIBLE_MONEY, AWARD_RETURN_MONEY, \
        WORK_LOAD, STAFF_NUMBER, PROBLEM_COUNT_NOPUOTWAY

    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])

    # 统计工作量
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    # 检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 问题总数（剔除路外问题）
    PROBLEM_COUNT_NOPUOTWAY = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_ONOUTWAY_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 月度考核总金额
    ASSESS_RESPONSIBLE_MONEY = df_merge_with_dpid(
        _summizet_assess_money(year, month), DEPARTMENT_DATA)
    # 月度返奖明细
    AWARD_RETURN_MONEY = df_merge_with_dpid(
        pd_query(AWARD_RETURN_SQL.format(year, month)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


def _calc_score_by_formula(row, column, major_column, detail_type, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 0
    if row[column] == 0.0:
        return 0
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.1:
        _score = 100
    elif _ratio >= -0.1:
        _score = 90
    else:
        deduction = _ratio * 100
        if detail_type == 3:
            deduction *= -1
        _score = 100 + deduction
    _score = 0 if _score < 0 else _score
    _score = 100 if _score > 100 else _score
    return _score


def _calc_score_by_formula_asses_ratio(row, column, major_column, detail_type):
    _score = 0
    _ratio = row[column] / row[major_column]
    if _ratio >= 0.9:
        _score = (_ratio - 0.9) * 100
    return _score


def _calc_extra_child_score_groupby_major(df,
                                         dpid_data,
                                         column,
                                         func_calc_score,
                                         weight=None,
                                         detail_type=None,
                                         base_column="AVG_NUMBER"):
    """基于各专业均数，计算各个部门得分
    Arguments:
        df {DataFrame}} -- 各部门raw data
        dpid_data {DataFrame} -- 单位
        column {str} -- 列名
        func_calc_score {func} -- 计算公式
    Keyword Arguments:
        weight {float} -- 权重系数 (default: {None})
        detail_type {str} -- [子指数小类](default: {None})
    Returns:
        dataFrame -- 基于专业的最终部门得分
    """
    data = add_avg_number_by_major(df, dpid_data, column)
    data['SCORE'] = data.apply(
        lambda row: func_calc_score(row, column, base_column, detail_type),
        axis=1)
    if weight is not None:
        data['SCORE'] = data['SCORE'].apply(lambda x: x * weight)
    df_rst = pd.DataFrame(
        index=data['DEPARTMENT_ID'],
        data=data.loc[:, 'SCORE'].values,
        columns=['SCORE'])
    return df_rst


# 换算人均考核问题数
def _stats_check_problem_assess_radio(months_ago):
    return calc_child_index_type_divide_major(
        ASSESS_PROBLEM_COUNT, STAFF_NUMBER, -1, 3, 1, months_ago, 'COUNT',
        'SCORE_a', _calc_score_by_formula, _choose_dpid_data)


# 月人均考核金额
def _stats_assess_money_per_person(months_ago):
    return calc_child_index_type_divide_major(
        ASSESS_RESPONSIBLE_MONEY, STAFF_NUMBER, -1, 3, 2, months_ago, 'COUNT',
        'SCORE_b', _calc_score_by_formula, _choose_dpid_data)


# 返奖率
def _stats_award_return_ratio(months_ago):
    """原方法差值部分：（月度返奖金额÷月度考核金额-专业基数）÷专业基数
    修改为：
    高质量差值：（问题级别为A、B、E1、E2）：
    中质量差值：（问题级别为C、E3）：
    低质量差值：（问题级别D、E4）：

    通用公式（月返奖个数（返奖金额不为0）÷问题个数-专业基数）÷专业基数
    最后综合差值为：高质量差值*34%+中质量差值*33%+低质量差值*33%
    """
    if AWARD_RETURN_MONEY.empty:
        return None
    high_level = ['A', 'B', 'E1', 'E2']
    middle_level = ['C', 'E3']
    low_level = ['D', 'E4']
    child_weight = [0.34, 0.33, 0.33]
    # 保存计算结果
    rst_child_score = []
    # 保存中间过程计算数据
    calc_basic_data = []
    title = ['高质量差值', '中质量差值', '低质量差值']
    for i, ilevel in enumerate([high_level, middle_level, low_level]):
        idata = AWARD_RETURN_MONEY[AWARD_RETURN_MONEY['LEVEL'].isin(ilevel)]
        if idata.empty:
            continue
        award_number = idata[(idata['ACTUAL_MONEY'] > 0)
                             & (idata['IS_RETURN'] == 1)]
        award_number = award_number.groupby(['DEPARTMENT_ID']).size()
        prob_number = idata.groupby(['DEPARTMENT_ID']).size()
        idata = pd.concat(
            [
                award_number.to_frame(name='award'),
                prob_number.to_frame(name='prob')
            ],
            axis=1,
            sort=False)
        idata['ratio'] = idata['award'] / idata['prob']
        rst_child_data = calc_extra_child_score_groupby_major_two(
            idata.copy(),
            _choose_dpid_data(3),
            'ratio',
            _calc_score_by_formula,
            weight=child_weight[i],
            detail_type=3, numerator='award',denominator='prob')
        rst_child_score.append(rst_child_data)
        idata[f'middle_{i}'] = idata.apply(
            lambda row: '{0}<br/>月返奖个数（金额大于0）({1}) / 问题个数（{2}）'
                .format(title[i], row['award'], row['prob']),
            axis=1)
        idata.drop(columns=['prob', 'award', 'ratio'], inplace=True, axis=1)
        calc_basic_data.append(idata)
    # 合并保存中间过程计算结果到mongo
    combine_and_format_basic_data_to_mongo(
        calc_basic_data, _choose_dpid_data(3), months_ago, 3, 3, 3, index_type=-1)
    data = pd.concat(rst_child_score, axis=1, sort=False)
    series_rst = data.apply(lambda row: sum(row), axis=1)
    column = f'SCORE_c_3'
    df_rst = series_rst.to_frame(name=column)
    summizet_operation_set(df_rst, _choose_dpid_data(3), column, 3, -1, 3, 3,
                           months_ago)
    return [df_rst]


# 考核率
def _stats_problem_assess_radio(months_ago):
    return calc_child_index_type_divide(
        ASSESS_PROBLEM_COUNT, PROBLEM_COUNT_NOPUOTWAY, -1, 3, 4, months_ago, 'COUNT',
        'SCORE_d', _calc_score_by_formula_asses_ratio, _choose_dpid_data)


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 分别表示【问题考核率、月人均考核金额、返奖率】
    child_index_func = [
        _stats_check_problem_assess_radio, _stats_assess_money_per_person,
        _stats_award_return_ratio, _stats_problem_assess_radio
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 加权计算
    item_name = [f'SCORE_{x}' for x in ['a', 'b', 'c', 'd']]
    item_weight = [0.25, 0.25, 0.5, -1]
    update_major_maintype_weight(index_type=-1, major=None, main_type=3,
                                 child_index_list=[1, 2, 3, 4],
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, -1, 3, months_ago,
                         item_name, item_weight)
    current_app.logger.debug(
        '├── └── assess_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
