# -*- coding: utf-8 -*-

import pandas as pd
from flask import current_app
from math import fabs
from app.data.health_index.check_intensity_sql import (
    BANZU_POINT_SQL, CHECK_POINT_SQL, KAOHE_PROBLEM_SQL, MEDIA_COST_TIME_SQL,
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_PROBLME_SCORE_SQL, REAL_CHECK_BANZU_SQL,
    REAL_CHECK_POINT_SQL, YECHA_CHECK_SQL, MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST)
from app.data.health_index.common_sql import (
    CHECK_COUNT_SQL, CHECK_PROBLEM_SQL, CHEJIAN_DPID_SQL, DEPARTMENT_SQL,
    EXTERNAL_PERSON_SQL, PROBLEM_CHECK_SCORE_SQL, QUANTIZATION_PERSON_SQL,
    RISK_LEVEL_PROBLEM_SQL, WORK_LOAD_SQL, XIANCHANG_CHECK_COUNT_SQL,
    ZHANDUAN_DPID_SQL, JIAODA_RISK_LEVEL_PROBLEM_SQL)
from app.data.index.common import (
    append_major_column_to_df,
    calc_child_index_type_divide_major,
    calc_extra_child_score_groupby_major_two, combine_child_index_func,
    df_merge_with_dpid, format_export_basic_data, summizet_child_index,
    summizet_operation_set, write_export_basic_data_to_mongo,
    get_zhanduan_deparment, calc_check_count_per_person)
from app.data.index.util import get_custom_month, validate_exec_month
from app.data.util import pd_query, update_major_maintype_weight

HIERARCHY = [3]


def _choose_dpid_data(hierarchy):
    dpid_data = {
        3: ZHANDUAN_DPID_DATA,
        4: CHEJIAN_DPID_DATA,
    }
    return dpid_data.get(hierarchy)


def _calc_score_by_formula(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column]) / row[major_column]
    if _ratio >= 0.2:
        _score = 100
    elif _ratio >= 0:
        _score = 90 + _ratio * 50
    elif _ratio > -0.15:
        _score = 80 + (_ratio + 0.15) * 60
    elif _ratio > -0.3:
        _score = 80 + (_ratio + 0.15) * 200
    else:
        _score = 50 + (_ratio + 0.3) * 300
    if _score < 0:
        _score = 0
    return _score


def _calc_score_by_formula_banzu(row, column, major_column, detail_type=None, major_ratio_dict=None):
    _score = 60
    if row[major_column] == 0:
        return 60
    _ratio = (row[column] - row[major_column])
    _score = _ratio * 100
    if _score < 0:
        _score = 0
    return min(100, _score)


def _get_sql_data(months_ago):
    global WORK_LOAD, QUANTIZATION_PERSON, CHECK_COUNT, PROBLEM_COUNT, \
        ASSESS_PROBLEM_COUNT, PROBLEM_SCORE, YECHA_COUNT, YIBAN_RISK_COUNT, \
        ZHANDUAN_DPID_DATA, CHEJIAN_DPID_DATA, DEPARTMENT_DATA, STAFF_NUMBER, \
        XIANCHANG_CHECK_COUNT, JIAODA_RISK_COUNT
    ZHANDUAN_DPID_DATA = get_zhanduan_deparment(ZHANDUAN_DPID_SQL)
    CHEJIAN_DPID_DATA = pd_query(CHEJIAN_DPID_SQL)
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    stats_month = get_custom_month(months_ago)
    year, month = int(stats_month[1][:4]), int(stats_month[1][5:7])
    # 正式职工人数
    STAFF_NUMBER = df_merge_with_dpid(pd_query(WORK_LOAD_SQL), DEPARTMENT_DATA)
    # 外聘人员 （外聘人员不需要统计下属单位，会有冗余）
    # 站段外聘人员
    ZHANDUAN_STAFF = pd_query(EXTERNAL_PERSON_SQL.format(month))
    # 单位总人数
    WORK_LOAD = pd.concat([STAFF_NUMBER, ZHANDUAN_STAFF], axis=0, sort=False)
    # 量化人员数
    QUANTIZATION_PERSON = df_merge_with_dpid(
        pd_query(QUANTIZATION_PERSON_SQL.format(year, month)), DEPARTMENT_DATA)

    # 检查总次数
    CHECK_COUNT = df_merge_with_dpid(
        pd_query(CHECK_COUNT_SQL.format(*stats_month)), DEPARTMENT_DATA)
    # return
    # 检查总次数（现场检查）
    XIANCHANG_CHECK_COUNT = df_merge_with_dpid(
        pd_query(XIANCHANG_CHECK_COUNT_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 检查问题数
    PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(CHECK_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)
    # 考核问题数
    ASSESS_PROBLEM_COUNT = df_merge_with_dpid(
        pd_query(KAOHE_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)
    # 累计质量分
    PROBLEM_SCORE = df_merge_with_dpid(
        pd_query(PROBLEM_CHECK_SCORE_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    # 夜查次数
    YECHA_COUNT = df_merge_with_dpid(
        pd_query(YECHA_CHECK_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 一般及以上问题数
    YIBAN_RISK_COUNT = df_merge_with_dpid(
        pd_query(RISK_LEVEL_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    # 较大及以上问题数
    JIAODA_RISK_COUNT = df_merge_with_dpid(
        pd_query(JIAODA_RISK_LEVEL_PROBLEM_SQL.format(*stats_month)), DEPARTMENT_DATA)

    current_app.logger.debug('|   └── extract data from mysql have done!')


# 统计干部量化率
def _stats_quantization_ratio(months_ago):
    '''
    量化率：量化人员数/正式职工总数。值越高检查力度越大。
    '''
    return calc_child_index_type_divide_major(
        QUANTIZATION_PERSON, STAFF_NUMBER, -1, 1, 1, months_ago, 'COUNT',
        'SCORE_a', _calc_score_by_formula, _choose_dpid_data)


# 人均检查频次
def _stats_check_per_person(months_ago):
    return calc_child_index_type_divide_major(
        CHECK_COUNT, WORK_LOAD, -1, 1, 2, months_ago, 'COUNT', 'SCORE_b',
        _calc_score_by_formula, _choose_dpid_data)


def _stats_check_problem_ratio(months_ago):
    return calc_child_index_type_divide_major(
        CHECK_COUNT, PROBLEM_COUNT, -1, 1, 3, months_ago, 'COUNT', 'SCORE_c',
        _calc_score_by_formula, _choose_dpid_data)


# 查处问题考核率
def _stats_check_problem_assess_radio(months_ago):
    return calc_child_index_type_divide_major(
        ASSESS_PROBLEM_COUNT, PROBLEM_COUNT, -1, 1, 4, months_ago, 'COUNT',
        'SCORE_d', _calc_score_by_formula, _choose_dpid_data)


# 问题平均质量分
def _stats_score_per_check_problem(months_ago):
    return calc_child_index_type_divide_major(
        PROBLEM_SCORE, PROBLEM_COUNT, -1, 1, 5, months_ago, 'COUNT', 'SCORE_e',
        _calc_score_by_formula, _choose_dpid_data)


# 人均质量分
def _stats_score_per_person(months_ago):
    return calc_child_index_type_divide_major(
        PROBLEM_SCORE, WORK_LOAD, -1, 1, 6, months_ago, 'COUNT', 'SCORE_f',
        _calc_score_by_formula, _choose_dpid_data)


# 夜查率
def _stats_yecha_ratio(months_ago):
    return calc_child_index_type_divide_major(
        YECHA_COUNT, XIANCHANG_CHECK_COUNT, -1, 1, 7, months_ago, 'COUNT',
        'SCORE_g', _calc_score_by_formula, _choose_dpid_data)


def _calc_media_val_person(series, hierarchy, idx, work_load, calc_score_by_formula=_calc_score_by_formula):
    work_load_cp = work_load.groupby([f'TYPE{hierarchy}'])['COUNT'].sum()
    work_load_cp = work_load_cp.to_frame(name='PERSON_NUMBER')
    data = pd.concat(
        [series.to_frame(name='media'), work_load_cp], axis=1, sort=False)
    data['ratio'] = data['media'] / data['PERSON_NUMBER']
    # 计算结果
    rst_data = calc_extra_child_score_groupby_major_two(
        data, _choose_dpid_data(hierarchy), 'ratio', calc_score_by_formula,
        numerator='media', denominator='PERSON_NUMBER')
    # 中间计算数据
    title = [
        '监控调阅时长累计({0})/总人数({1})', '监控调阅发现问题数({0})/总人数({1})',
        '监控调阅发现问题质量分累计({0})/总人数({1})', '调阅检查班组数({0})/具备调阅条件班组数({1})'
    ]
    data[f'middle_{idx}'] = data.apply(
        lambda row: title[idx].format(f'{round(row["media"], 2)}', row['PERSON_NUMBER']),
        axis=1)
    data.drop(
        columns=['media', 'ratio', 'PERSON_NUMBER'], inplace=True, axis=1)
    return rst_data, data


# 监控调阅人均时长
def _calc_media_time_per_person(stats_month, hierarchy, idx):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_COST_TIME_SQL.format(*stats_month)), DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['TIME'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, WORK_LOAD)


# 监控调阅人均问题个数
def _calc_media_problem_per_person(stats_month, hierarchy, idx):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, WORK_LOAD)


# 监控调阅人均质量分
def _calc_media_score_per_person(stats_month, hierarchy, idx):
    media_time = df_merge_with_dpid(
        pd_query(MEDIA_PROBLME_SCORE_SQL.format(*stats_month)),
        DEPARTMENT_DATA)
    media_time = media_time.groupby([f'TYPE{hierarchy}'])['SCORE'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, WORK_LOAD)


# 监控调阅人均质量分
def _calc_media_banzu_val_workbanzu(stats_month, hierarchy, idx):
    # 调阅班组数
    watch_media_banzu_count = df_merge_with_dpid(
        pd_query(MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[0].format(*stats_month)),
        DEPARTMENT_DATA)
    # 作业班组数
    work_banzu_count = df_merge_with_dpid(
        pd_query(MONITOR_WATCH_DISCOVERY_RATIO_SQLLIST[1]),
        DEPARTMENT_DATA)
    media_time = watch_media_banzu_count.groupby(
        [f'TYPE{hierarchy}'])['NUMBER'].sum()
    return _calc_media_val_person(media_time, hierarchy, idx, work_banzu_count,
                                  calc_score_by_formula=_calc_score_by_formula_banzu)


# 监控调阅力度
def _stats_media_intensity(months_ago):
    rst_child_score = []
    stats_month = get_custom_month(months_ago)
    # 保存中间计算过程数据
    calc_basic_data = []
    for hierarchy in HIERARCHY:
        score = []
        child_func = [
            _calc_media_time_per_person, _calc_media_problem_per_person,
            _calc_media_score_per_person, _calc_media_banzu_val_workbanzu
        ]
        child_weight = [0.35, 0.35, 0.2, 0.1]
        for idx, ifunc in enumerate(child_func):
            rst_func, rst_basic_data = ifunc(stats_month, hierarchy, idx)
            calc_basic_data.append(rst_basic_data)
            if rst_func is not None:
                score.append(rst_func * child_weight[idx])
        # 保存导出中间计算数据到mongo
        calc_df_data = pd.concat(calc_basic_data, axis=1, sort=False)
        calc_df_data.fillna('', inplace=True)
        columns = calc_df_data.columns.tolist()
        calc_df_data['CONTENT'] = calc_df_data.apply(
            lambda row: '<br/>'.join([row[col] for col in columns]), axis=1)
        calc_df_data = append_major_column_to_df(
            _choose_dpid_data(3),
            pd.DataFrame(
                index=calc_df_data.index,
                data=calc_df_data.loc[:, 'CONTENT'].values,
                columns=['CONTENT']))
        calc_basic_data_rst = format_export_basic_data(calc_df_data, 1, 10, 3,
                                                       months_ago, index_type=-1)
        write_export_basic_data_to_mongo(calc_basic_data_rst, months_ago, 3, 1,
                                         10, index_type=-1)
        # 合并计算子指数
        data = pd.concat(score, axis=1, sort=False)
        series_rst = data.apply(lambda row: sum(row), axis=1)
        column = f'SCORE_j_{hierarchy}'
        df_rst = series_rst.to_frame(name=column)
        summizet_operation_set(df_rst, _choose_dpid_data(hierarchy), column,
                               hierarchy, -1, 1, 10, months_ago)
        rst_child_score.append(df_rst)
    return rst_child_score


# 较大及以上风险问题占比
def _stats_jiaoda_risk_ratio(months_ago):
    """
    计算较大风险问题占比（总人数）
    """
    return calc_child_index_type_divide_major(
        JIAODA_RISK_COUNT, WORK_LOAD, -1, 1, 8, months_ago, 'COUNT',
        'SCORE_h', _calc_score_by_formula, _choose_dpid_data)


# 覆盖率
def _stats_check_address_ratio(months_ago):
    stats_month = get_custom_month(months_ago)
    # 检查地点数
    data_real = pd.concat(
        [
            pd_query(REAL_CHECK_POINT_SQL.format(*stats_month)),
            pd_query(REAL_CHECK_BANZU_SQL.format(*stats_month)),
        ],
        axis=0,
        sort=False)
    data_real = df_merge_with_dpid(data_real, DEPARTMENT_DATA)
    # 地点总数
    data_total = pd.concat(
        [
            pd_query(CHECK_POINT_SQL),
            pd_query(BANZU_POINT_SQL),
        ],
        axis=0,
        sort=False)
    data_total = df_merge_with_dpid(data_total, DEPARTMENT_DATA)
    return calc_child_index_type_divide_major(
        data_real,
        data_total,
        -1,
        1,
        9,
        months_ago,
        'COUNT',
        'SCORE_i',
        lambda x: min(100, x * 100),
        _choose_dpid_data,
        is_calc_score_base_major=False)


def handle(months_ago):
    # 部门按站段聚合
    _get_sql_data(months_ago)

    # 计算基数
    child_index_func = [
        _stats_check_per_person,
        _stats_score_per_check_problem,
        _stats_score_per_person,
        _stats_yecha_ratio,
        _stats_jiaoda_risk_ratio,
        _stats_check_address_ratio,
        _stats_media_intensity,
    ]
    # 存放所有子指数项的分数
    child_score = combine_child_index_func(child_index_func, months_ago)
    # 站段
    item_name = [
        f'SCORE_{x}' for x in ['b', 'e', 'f', 'g', 'h', 'i', 'j']
    ]
    item_weight = [0.18, 0.00, 0.3, 0.05, 0.21, 0.06, 0.20]

    update_major_maintype_weight(index_type=-1, main_type=1, major=None,
                                 child_index_list=[2, 5, 6, 7, 8, 9, 10],
                                 child_index_weight=item_weight)
    summizet_child_index(child_score, _choose_dpid_data, -1, 1, months_ago,
                         item_name, item_weight, [3])
    current_app.logger.debug(
        '├── └── check_intensity index has been figured out!')


@validate_exec_month
def execute(months_ago):
    handle(months_ago)


if __name__ == '__main__':
    pass
