import openpyxl
from app.data.util import pd_query
from app.report.dianwu.station_daily_report_data import get_data


def export_major_score_table_excel(file_path, times, dp_id):
    workbook = openpyxl.load_workbook('app/report/template/电务站段.xlsx')
    worksheet = workbook.worksheets[0]
    result = get_data(times, worksheet, dp_id)
    workbook.save(file_path)
    return result



