from app.report.dianwu.station_semiannual_report_data import get_data
from docxtpl import DocxTemplate
from app import mongo
import os
import datetime
from app.report.analysis_report_manager import SemiannualAnalysisReport
import app.report.dianwu.station_semiannual_report_data as report_data


class DianwuStationSemiannualAnalysisReport(SemiannualAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(DianwuStationSemiannualAnalysisReport, self).__init__(hierarchy_type='STATION', major='电务',
                                                                    station_id=station_id)

    def generate_report_data(self, year, half):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param half: int 月
        :return:
        """
        data = get_data(year, half, self.station_id)
        mongo.db['safety_analysis_semiannual_report'].delete_one(
            {
                "year": year,
                "half": half,
                "hierarchy": "STATION",
                "station_id": self.station_id,
                "major": self.major
            })
        mongo.db['safety_analysis_semiannual_report'].insert_one(data)
        return data

    def generate_report(self, year, half):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param half: {int} 月份
        :return:
        """
        data = self.load_report_data(year, half)
        tpl = DocxTemplate('app/report/template/dianwu_station_semiannual_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path, image_path = self.get_report_paths()
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']


