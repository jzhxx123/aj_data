# 履职评价信息
CHECK_EVALUATE_SITUATION_SQL = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME AS DEPARTMENT_ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE3 = '{2}'"""


# 检查信息
CHECK_INFO_SQL = """SELECT i.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,i.IS_YECHA,e.`NAME` AS SHOP1,
c.TYPE,g.`NAME` AS SHOP2
from t_check_info i
left join t_check_info_and_person b on b.fk_check_info_id = i.pk_id
LEFT JOIN t_check_info_and_address a on a.FK_CHECK_INFO_ID=i.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE4
LEFT JOIN t_department f on f.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
LEFT JOIN t_department g on g.DEPARTMENT_ID=f.type4
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}'"""


# 问题信息
CHECK_PROBLEM_SQL = """SELECT p.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,p.PROBLEM_CLASSITY_NAME,p.`LEVEL`,
d.CHECK_SCORE,i.IS_YECHA,p.RISK_LEVEL,e.`NAME`,p.CHECK_ITEM_NAME,p.PROBLEM_POINT
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_problem_base d on p.FK_PROBLEM_BASE_ID = d.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}'"""


# 违章问题信息
CHECK_PROBLEM_WZ_SQL = """SELECT e.`NAME`,a.BIRTHDAY,a.POSITION
from t_check_problem p 
LEFT JOIN t_check_problem_and_responsibility_person b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_person a on a.id_card=b.ID_card
LEFT JOIN t_department c on p.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
where p.SOLVE_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}' AND p.`LEVEL` IN ('A', 'B', 'C', 'D')"""


# 重点人员预警库
CHECK_IIILEGE_SQL = """SELECT c.ID_CARD,a.`YEAR`,a.`MONTH`,a.DEDUCT_SCORE,c.POSITION,c.BIRTHDAY,d.ALL_NAME,e.NAME
from t_warning_key_person_library a
LEFT JOIN t_person c on a.ID_CARD = c.ID_CARD
LEFT JOIN t_department d on a.fk_unit_id = d.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
where a.`YEAR`={0} and a.`MONTH`={1} 
and d.TYPE3 = '{2}'"""


# 预警信息
warning_info_sql = """SELECT a.COPY_DEPARTMENT_NAMES as NAME 
FROM t_warning_notification a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.COPY_DEPARTMENT_IDS
WHERE a.CREATE_TIME BETWEEN '{0} 00:00:00' AND '{1} 23:59:59'
AND b.TYPE3='{0}' and a.HIERARCHY=3"""


# 车间信息
ZHANDUAN_PERSON_SQL = """SELECT DISTINCT d.NAME,d.DEPARTMENT_ID
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0 AND d.`NAME` like '%%车间'
"""

# 计划检查+实际检查信息
SAFEFY_CENTER_ANALYSIS_SQL = """
SELECT b.ALL_NAME,a.MONITOR_REVIEW_TIME,a.MONITOR_TIME,a.MONITOR_PROBLEM,a.MONITOR_KEY_PERIOD_NUMBER,a.MONITOR_PROBLEM_SCORE,
a.NON_MANAGEMENT_EVALUATE_REVIEW_NUMBER,a.SECCO_MANAGEMENT_QUANTITY,a.GENERAL_AND_PROFESSIONAL_QUANTITY,a.MONITOR_COVER_ADDRESS,
a.EVALUATE_NOT_SERIOUS_NUMBER
from t_analysis_center_quantization_quota a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
where b.TYPE3 = '{2}'
and a.TYPE = 3 and a.`YEAR` = {0} 
and a.`MONTH` = {1}"""