from app.data.util import pd_query
import pandas as pd
from app import mongo
from app.report.draw_picture import *
from app.report.dianwu.major_monthly_report_sql import *
from docxtpl import InlineImage
from docx.shared import Mm
from dateutil.relativedelta import relativedelta
import datetime


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, month):
    global end_date
    end_date = datetime.date(year, month, 24)
    last_month_end = end_date - relativedelta(months=1)
    start_date = end_date - relativedelta(months=1, days=-1)
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    last_month_end = str(last_month_end)[:10]
    # 履职评价
    evaluate_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date))
    last_evaluate_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date[:4] + '-01-01', end_date))
    last_month_eva = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date[:4] + '-01-01', last_month_end))
    # 问题信息
    check_problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 人员信息
    person_data = pd_query(CHECK_PERSON_NUMBER)
    # 履职风险
    evaluate_intensity_data = pd_query(CHECK_EVALUATE_INTENSITY_SQL.format(start_date, end_date))
    # 考核金额
    assess_intensity_data = pd_query(CHECK_ASSESS_INTENSITY_SQL.format(year, start_month))
    # 检查信息
    check_info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 监控设备调阅时长
    dy_time_data = pd_query(jk_time_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 安全分析中心量化指标
    center_data = pd_query(SAFEFY_CENTER_ANALYSIS_SQL.format(year, start_month))
    # 预警信息
    warning_data = pd_query(warning_info_sql.format(start_date, end_date))
    # 违章大王信息
    iiilege_data = pd_query(CHECK_IIILEGE_SQL.format(year, month))

    wz_pro_data = pd_query(CHECK_PROBLEM_WZ_SQL.format(start_date, end_date))

    first = get_fitst(evaluate_data, last_evaluate_data, last_month_eva)
    second = get_second(check_problem_data)
    third = get_third(iiilege_data, wz_pro_data, year)
    fourth = get_fourth(center_data, warning_data, evaluate_data)
    fifth = get_fifth(check_problem_data, person_data, evaluate_intensity_data, assess_intensity_data, check_info_data,
                      dy_time_data)
    file_name = f'{start_date}至{end_date}电务系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '电务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second": second,
        'third': third,
        "fourth": fourth,
        'fifth': fifth,
    }
    return result


def calc_ded_score(data):
    """
    计算扣分
    :param data:
    :return:
    """
    score_data = data[data['SCORE'] != 0]
    # 扣分人次
    ded_score_count = len(score_data)
    # 总扣分数
    ded_score = int(score_data['SCORE'].sum())
    new_data = score_data.groupby(['RESPONSIBE_PERSON_NAME', 'RESPONSIBE_ID_CARD']).sum().sort_values(by='SCORE',
                                                                                                      ascending=False).reset_index()
    if new_data.empty is not True:
        names = new_data['RESPONSIBE_PERSON_NAME'].tolist()
        id_cards = new_data['RESPONSIBE_ID_CARD'].tolist()
        scores = new_data['SCORE'].tolist()
        # 最高分信息
        max_name = names[0]
        max_id_card = id_cards[0]
        max_score = scores[0]
        max_reason = data[data['RESPONSIBE_ID_CARD'] == max_id_card]['EVALUATE_CONTENT'].tolist()
        # 最低分信息
        min_name = names[-1]
        min_id_card = id_cards[-1]
        min_score = scores[-1]
        min_reason = data[data['RESPONSIBE_ID_CARD'] == min_id_card]['EVALUATE_CONTENT'].tolist()
        return {
            'ded_score_count': ded_score_count,
            'ded_score': ded_score,
            'max_name': max_name,
            'max_score': max_score,
            'max_reason': [x.strip() for x in max_reason],
            'min_name': min_name,
            'min_score': min_score,
            'min_reason': [x.strip() for x in min_reason]
        }
    else:
        return {
            'ded_score_count': 0
        }


# ------------------------------------------------------------------第一部分--------------------------------------------
def get_fitst(evaluate_data, last_evaluate_data, last_month_eva):
    """
    干部评价总体情况
    :param evaluate_data:
    :param last_evaluate_data:
    :param last_month_eva:
    :return:
    """
    # 顶部信息及表格
    first_top = get_first_top(evaluate_data, last_evaluate_data, last_month_eva)
    # 安全效果评价扣分
    safety_ded = get_safety_ded(evaluate_data)
    # 典型故障及事故评价扣分
    typical_accident = get_typical_accident(evaluate_data)
    # 重点工作推进评价扣分
    main_work = get_main_work(evaluate_data)
    # 音视频评价扣分
    mv_ded = get_mv_ded(evaluate_data)
    # 安管系统运用评价
    safety_manager = get_safety_manager(evaluate_data)
    return {
        'first_top': first_top,
        'safety_ded': safety_ded,
        'typical_accident': typical_accident,
        'main_work': main_work,
        'mv_ded': mv_ded,
        'safety_manager': safety_manager
    }


def get_safety_manager(data):
    data = data[data['EVALUATE_TYPE'].isin([1, 2])]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_mv_ded(data):
    """
    音视频评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_TYPE'] == 3]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_main_work(data):
    """
    重点工作推进评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('重点工作推进评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_typical_accident(data):
    """
    典型故障及事故评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('典型故障及事故评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_safety_ded(data):
    """
    安全效果评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('安全效果评价')]
    data = data[data['MAIN_TYPE'].isin([1, 2])]
    # 总问题数
    problem_count = len(data)
    info = calc_ded_score(data)
    return {
        'problem_count': problem_count,
        'info': info,
    }


def get_first_top(data, last_data, last_month_eva):
    """
    顶部信息及表格
    :param data:
    :param last_data:
    :param last_month_eva:
    :return:
    """
    # 扣分人次
    ded_person_count = len(data)
    # 总计扣分
    ded_score = int(data['SCORE'].sum())
    # 累计扣分人次
    sum_person = len(last_data)
    # 累计总扣分
    sum_score = int(last_data['SCORE'].sum())
    # 扣分分类
    ded_type = _get_score_table(last_month_eva, last_data)
    return {
        'ded_person_count': ded_person_count,
        'ded_score': ded_score,
        'ded_type': ded_type,
        'sum_person': sum_person,
        'sum_score': sum_score
    }


def _get_score_table(data, last_data):
    """
    计分统计表
    :param data:
    :return:
    """
    data = data.groupby(['RESPONSIBE_PERSON_NAME', 'RESPONSIBE_ID_CARD']).sum().sort_values(by='SCORE',
                                                                                            ascending=False).reset_index()
    last_data = last_data.groupby(['RESPONSIBE_PERSON_NAME', 'RESPONSIBE_ID_CARD']).sum().sort_values(by='SCORE',
                                                                                                      ascending=False).reset_index()
    list1 = []
    list2 = []
    list3 = []
    # 各分段计分
    for i in range(6):
        if data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)].empty:
            list1.append(0)
        else:
            list1.append(len(data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)]))
    if data[data['SCORE'] >= 12].empty:
        list1.append(0)
    else:
        list1.append(len(data[data['SCORE'] >= 12]))

    for i in range(6):
        if last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)].empty:
            list2.append(0)
        else:
            list2.append(len(last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)]))
    if last_data[last_data['SCORE'] >= 12].empty:
        list2.append(0)
    else:
        list2.append(len(last_data[last_data['SCORE'] >= 12]))
    for i in range(len(list1)):
        list3.append(list2[i] - list1[i])
    return {
        'list1': list1,
        'list2': list2,
        'list3': list3
    }


# ----------------------------------------------------------------第二部分----------------------------------------------
def get_second(data):
    """
    音视频运用情况分析
    :param data:
    :return:
    """
    new_data = data[data['CHECK_WAY'] == 3]
    types = ['A', 'B', 'C', 'D', 'E', 'F']
    num = []
    for i in types:
        num.append(len(new_data[new_data['LEVEL'].str.contains(i)]))
    return {
        'type': types,
        'num': num
    }


# -------------------------------------------------------------------第三部分-------------------------------------------
def get_third(iiilege_data, wz_pro_data, year):
    # 绘图1
    all_count = []
    for j in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        new_data = wz_pro_data[wz_pro_data['NAME'] == j]
        new_data = new_data[new_data['BIRTHDAY'] != ' ']
        new_data = new_data.dropna(subset=['BIRTHDAY'])
        new_data['AGE'] = new_data['BIRTHDAY'].apply(
            lambda x: (datetime.datetime.now().date() -
                       datetime.datetime.strptime(x[:4] + '-' + x[4:6] + '-' + x[6:], '%Y-%m-%d').date()).days // 365)
        new_data1 = new_data.groupby('AGE').count().reset_index()
        count1 = [0, 0, 0, 0, 0]
        for i in range(len(new_data1)):
            if new_data1.iloc[i]['AGE'] <= 30:
                count1[0] += new_data1.iloc[i]['POSITION']
            elif 30 < new_data1.iloc[i]['AGE'] <= 40:
                count1[1] += new_data1.iloc[i]['POSITION']
            elif 40 < new_data1.iloc[i]['AGE'] <= 50:
                count1[2] += new_data1.iloc[i]['POSITION']
            elif 50 < new_data1.iloc[i]['AGE'] <= 60:
                count1[3] += new_data1.iloc[i]['POSITION']
            else:
                count1[4] += new_data1.iloc[i]['POSITION']
        all_count.append(count1)
    name1 = ['小于30', '30~40', '40~50', '50~60', '其他']
    images1 = bar_picture1(all_count, name1, ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段'],
                           '违章人员年龄分布情况', 'major_' + end_date, '电务', 'monthly')

    # 绘图2
    all_list = []
    for j in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        new_data = iiilege_data[iiilege_data['NAME'] == j].drop_duplicates(subset=['ID_CARD'])
        all_list.append(len(new_data))
    images2 = bar_picture([all_list], ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段'], [''],
                          '违章大王人数统计', 'major_' + end_date, '电务', 'monthly')
    return {
        'images1': images1,
        'images2': images2
    }


# -------------------------------------------------------------------第四部分-------------------------------------------
def get_fourth(center_data, warning_data, evaluate_data):
    """
    分析中心主要工作质量分析
    :param center_data:
    :param warning_data:
    :param evaluate_data:
    :return:
    """
    list1 = []
    for i in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        data1 = center_data[center_data['ALL_NAME'].str.contains(i)]
        data2 = evaluate_data[evaluate_data['NAME'].str.contains(i)]
        data3 = warning_data[warning_data['NAME'].str.contains(i)]
        list1.append(calc_index(i, data1, data2, data3))
    return {
        'list1': list1
    }


def calc_index(name, data, data1, data2):
    """
    计算小指数
    :param data:
    :return:
    """
    # 音视频调阅时长
    monitor_time = int(data['MONITOR_TIME'].sum())
    # 复查时长
    monitor_review_time = int(data['MONITOR_REVIEW_TIME'].sum())
    # 调阅发现问题数
    monitor_problem = int(data['MONITOR_PROBLEM'].sum())
    # 关键时段调阅次数
    key_number = int(data['MONITOR_KEY_PERIOD_NUMBER'].sum())
    # 调阅问题质量分
    monitor_problem_score = int(data['MONITOR_PROBLEM_SCORE'].sum())
    # 科职管理人员评价人数
    secco = len(data1[data1['GRADATION'].isin(['正科职管理人员', '副科职管理人员'])]['CHECK_PERSON_ID_CARD'].unique())
    # 评价记分人数
    count = len(data1['CHECK_PERSON_ID_CARD'].unique())
    # 阶段评价人次
    jd_number = len(data1[data1['EVALUATE_WAY'] == 3])
    # 预警次数
    warning_count = len(data2)
    # 音视频调阅不认真评价人数
    not_serious = len(data1[data1['SITUATION'].str.contains('监控调阅检查不认真')]['CHECK_PERSON_ID_CARD'].unique())
    # 现场检查不认真评价人
    xc_not_serious = len(data1[data1['SITUATION'].str.contains('现场监控不认真')]['CHECK_PERSON_ID_CARD'].unique())
    return {
        'name': name,
        'monitor_time': monitor_time,
        'monitor_review_time': monitor_review_time,
        'monitor_problem': monitor_problem,
        'key_number': key_number,
        'monitor_problem_score': monitor_problem_score,
        'secco': secco,
        'count': count,
        'warning_count': warning_count,
        "not_serious": not_serious,
        'jd_number': jd_number,
        'xc_not_serious': xc_not_serious
    }


# ----------------------------------------------------------------------第五部分----------------------------------------
def get_fifth(check_problem_data, person_data, evaluate_intensity_data, assess_intensity_data, check_info_data,
              dy_time_data):
    """
    大数据统计分析
    :param check_problem_data:
    :param person_data:
    :param evaluate_intensity_data:
    :param assess_intensity_data:
    :param check_info_data:
    :param dy_time_data:
    :return:
    """
    # 检查力度指数
    check_intensity = get_check_intensity(check_problem_data, person_data, check_info_data, dy_time_data, end_date)
    # 评价力度指数
    evaluate_intensity = get_evaluate_intensity(evaluate_intensity_data, person_data, end_date)
    # 考核力度指数
    assess_intensity = get_assess_intensity(assess_intensity_data, person_data, end_date)
    # 问题暴露指数
    problem_exposure = get_problem_exposure(end_date)
    # 问题整改指数
    problem_rectification = get_problem_rectification(end_date)
    # 检查均衡度指数
    check_evenness = get_check_evenness(end_date)
    return {
        'check_intensity': check_intensity,
        'evaluate_intensity': evaluate_intensity,
        'assess_intensity': assess_intensity,
        'check_evenness': check_evenness,
        'problem_exposure': problem_exposure,
        'problem_rectification': problem_rectification
    }


def get_problem_rectification(end_date):
    """
    问题整改力度
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 6,
         'MAJOR': '电务',
         'MON': date,
         'TYPE': 5
         },
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1,
         'SCORE': 1,
         'DETAIL_TYPE': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1,
            'SCORE': 0,
            'DETAIL_TYPE': 1
        })
    data = pd.DataFrame(document)
    content = {
        1: '更改时效',
        2: '整改履职',
        3: '问题控制',
        4: '整改复查',
        5: '隐患整治',
        6: '整改成效'
    }
    station_list = []
    for i in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        all_list = []
        new_data = data[data['DEPARTMENT_NAME'] == i]
        for j, k in new_data.iterrows():
            if k['DETAIL_TYPE'] not in [1, 2, 3, 4, 5, 6]:
                continue
            info_list = [content[k['DETAIL_TYPE']], k['SCORE'], k['RANK']]
            all_list.append(info_list)
        dic = {
            'name': i,
            'all_list': all_list
        }
        station_list.append(dic)
    return {
        'station_list': station_list
    }


def get_problem_exposure(end_date):
    """
    问题暴露指数
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 5,
         'MAJOR': '电务',
         'MON': date,
         'TYPE': 5
         },
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1,
         'SCORE': 1,
         'DETAIL_TYPE': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1,
            'SCORE': 0,
            'DETAIL_TYPE': 1
        })
    data = pd.DataFrame(document)
    content = {
        1: '普遍性暴漏度',
        2: '较严重隐患暴漏',
        3: '事故隐患问题暴漏度',
        4: '班组问题暴漏度',
        5: '他查问题暴漏度'
    }
    station_list = []
    for i in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        all_list = []
        new_data = data[data['DEPARTMENT_NAME'] == i]
        for j, k in new_data.iterrows():
            if k['DETAIL_TYPE'] not in [1, 2, 3, 4, 5]:
                continue
            info_list = [content[k['DETAIL_TYPE']], k['SCORE'], k['RANK']]
            all_list.append(info_list)
        dic = {
            'name': i,
            'all_list': all_list
        }
        station_list.append(dic)
    return {
        'station_list': station_list
    }


def get_check_evenness(end_date):
    """
    检查均衡度指数
    :param data:
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 4,
         'MAJOR': '电务',
         'MON': date,
         'TYPE': 5
         },
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1,
         'SCORE': 1,
         'DETAIL_TYPE': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1,
            'SCORE': 0,
            'DETAIL_TYPE': 1
        })
    data = pd.DataFrame(document)
    content = {
        1: '问题查处均衡度',
        2: '检查日期均衡度',
        3: '检查地点均衡度',
        4: '检查时段均衡度',
    }
    station_list = []
    for i in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        all_list = []
        new_data = data[data['DEPARTMENT_NAME'] == i]
        for j, k in new_data.iterrows():
            if k['DETAIL_TYPE'] not in [1, 2, 3, 4]:
                continue
            info_list = [content[k['DETAIL_TYPE']], k['SCORE'], k['RANK']]
            all_list.append(info_list)
        dic = {
            'name': i,
            'all_list': all_list
        }
        station_list.append(dic)
    return {
        'station_list': station_list
    }


def get_assess_intensity(data, person_data, end_date):
    """
    考核力度指数
    :param data:
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 3,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 2,
         'TYPE': 5},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    all_list = []
    for i in document:
        station_name = i['DEPARTMENT_NAME']
        if station_name in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
            dic = {
                'name': station_name,
                'rank': i['RANK'],
                'station': calc_assess_intensity_problem(data, person_data, station_name)
            }
            all_list.append(dic)
        else:
            continue
    for i in all_list:
        i['rank1'] = sorted(all_list, key=lambda x: x['station']['gb_ratio'], reverse=True).index(i) + 1
        i['rank2'] = sorted(all_list, key=lambda x: x['station']['worker_avg'], reverse=True).index(i) + 1
        i['rank3'] = sorted(all_list, key=lambda x: x['station']['gb_person'], reverse=True).index(i) + 1
    return {
        'all_list': all_list
    }


def calc_assess_intensity_problem_station(data, person_data):
    """
    站段计算
    :param data:
    :param person_data:
    :param station_name:
    :return:
    """
    # 职工人数
    worker_person = len(person_data[person_data['IDENTITY'] != '干部'])
    # 总考核金额
    all_money = int(data['ACTUAL_MONEY'].sum())
    # 干部考核金额
    manger = int(data[data['IDENTITY'] == '干部']['ACTUAL_MONEY'].sum())
    # 干部考核金额占比
    ratio = round(ded_zero(manger, all_money), 2)
    # 职工考核金额
    worker_money = int(data[data['IDENTITY'] != '干部']['ACTUAL_MONEY'].sum())
    # 职工人均
    worker_avg = round(ded_zero(worker_money, worker_person), 2)
    # 考核干部人次
    gb_person = len(data)
    return {
        'ratio': ratio,
        'worker_avg': worker_avg,
        'gb_person': gb_person
    }


def calc_assess_intensity_problem(data, person_data, station_name):
    """
    站段计算
    :param data:
    :param person_data:
    :param station_name:
    :return:
    """
    # 职工人数
    worker_person = len(person_data[person_data['IDENTITY'] != '干部'])
    new_data = data[data['ALL_NAME'].str.contains(station_name)]
    # 总金额
    all_money = int(new_data['ACTUAL_MONEY'].sum())
    # 干部考核金额
    gb_money = int(new_data[new_data['IDENTITY'] == '干部']['ACTUAL_MONEY'].sum())
    # 职工考核金额
    worker_money = int(new_data[new_data['IDENTITY'] != '干部']['ACTUAL_MONEY'].sum())
    # 干部占比
    gb_ratio = round(ded_zero(gb_money, all_money), 2)
    # 职工人均
    worker_avg = round(ded_zero(worker_money, worker_person), 2)
    # 考核干部人次
    gb_person = len(new_data)
    return {
        'all_money': all_money,
        'gb_money': gb_money,
        'worker_money': worker_money,
        'gb_ratio': gb_ratio,
        'worker_avg': worker_avg,
        'gb_person': gb_person
    }


def get_evaluate_intensity(data, person_data, end_date):
    """
    评价力度指数
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 2,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 0,
         'TYPE': 5},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    all_list = []
    for i in document:
        station_name = i['DEPARTMENT_NAME']
        if station_name in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
            dic = {
                'name': station_name,
                'rank': i['RANK'],
                'station': calc_evaluate_intensity_problem(data, person_data, station_name)
            }
            all_list.append(dic)
        else:
            continue
    for i in all_list:
        i['rank1'] = sorted(all_list, key=lambda x: x['station']['ratio'], reverse=True).index(i) + 1
        i['rank2'] = sorted(all_list, key=lambda x: x['station']['gb_avg'], reverse=True).index(i) + 1
        i['rank3'] = sorted(all_list, key=lambda x: x['station']['ratio1'], reverse=True).index(i) + 1
    return {
        'all_station': all_list,
    }


def calc_evaluate_intensity_problem(data, person_data, station_name):
    """
    站段计算
    :param data:
    :param person_data:
    :param station_name:
    :return:
    """
    new_data = data[data['NAME'] == station_name]
    person = person_data[(person_data['NAME'] == station_name) & (person_data['IDENTITY'] == '干部')]
    # 主动评价
    zd = len(new_data[new_data['EVALUATE_WAY'] > 0]['CHECK_PERSON_ID_CARD'].unique())
    # 主动评价占比
    ratio = round(ded_zero(zd, len(new_data['CHECK_PERSON_ID_CARD'].unique())), 2)
    # 干部主动评价
    gb_zd = len(
        new_data[(new_data['EVALUATE_WAY'] > 0) & (new_data['IDENTITY'] == '干部')]['CHECK_PERSON_ID_CARD'].unique())
    # 干部人均主动评价
    gb_avg = round(ded_zero(gb_zd, len(person)), 4)
    # 评价各级职务
    job = len(new_data['GRADATION'].unique())
    # 占比
    ratio1 = round(ded_zero(job, 5), 2)
    dic = {
        'zd': zd,
        'ratio': ratio,
        'gb_avg': gb_avg,
        'job': job,
        'ratio1': ratio1
    }
    return dic


def get_check_intensity(data, person_data, check_info_data, dy_time_data, end_date):
    """
    检查力度指数
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 1,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 0,
         'TYPE': 5},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '无',
            'RANK': 1
        })
    check_info_data = check_info_data.drop_duplicates(subset=['PK_ID'])
    major_infos = calc_check_intensity_problem(data, person_data, check_info_data, dy_time_data)
    all_list = []
    for i in document:
        station_name = i['DEPARTMENT_NAME']
        if station_name in ['达州电务段', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
            dic = {
                'name': station_name,
                'rank': i['RANK'],
                'station': calc_check_intensity_problem_station(data, person_data, check_info_data, dy_time_data,
                                                                station_name)
            }
            all_list.append(dic)
        else:
            continue
    return {
        'all_list': all_list,
        'major': major_infos,
    }


def calc_check_intensity_problem_station(data, person_data, check_info_data, dy_time_data, station_name):
    """
    计算站段所有问题数据
    :param data:
    :param person_data:
    :param center_data:
    :param station_name:
    :return:
    """
    data = data[data['NAME'].str.contains(station_name)]
    person_data = person_data[person_data['NAME'].str.contains(station_name)]
    info_data = check_info_data[check_info_data['NAME'].str.contains(station_name)]
    dy_data = dy_time_data[dy_time_data['NAME'].str.contains(station_name)]
    # 检查总次数
    check_count = len(info_data)
    person_count = len(person_data)
    ratio1 = round(ded_zero(check_count, person_count), 2)
    # 现场两违检查
    xc = len(data[data['CHECK_WAY'] == 1])
    ratio2 = round(ded_zero(xc, person_count), 2)
    # 检查发现问题
    find_problem = len(data['PROBLEM_NUMBER'])
    ratio3 = round(ded_zero(find_problem, person_count), 2)
    # 作业类问题
    work = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    ratio4 = round(ded_zero(work, person_count), 2)
    # 设备类问题
    shebei = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    ratio5 = round(ded_zero(shebei, person_count), 2)
    # 管理类问题
    manage = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    ratio6 = round(ded_zero(manage, person_count), 2)
    # 高质量问题
    high = len(data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    avg_high = round(ded_zero(high, person_count), 2)
    # 两违问题
    two_problem = len(data[data['LEVEL'].isin(['A', 'B'])])
    avg_two = round(ded_zero(two_problem, person_count), 2)
    # 一般及以上风险问题
    gener = len(data[data['RISK_LEVEL'].isin([1, 2, 3])])
    ratio7 = round(ded_zero(gener, person_count), 2)
    # 检查问题质量分
    check_score = int(data['CHECK_SCORE'].sum())
    ratio8 = round(ded_zero(check_score, person_count), 2)
    # 夜间检查率
    yecha = len(info_data[info_data['IS_YECHA'] == 1])
    ratio9 = round(ded_zero(yecha, check_count), 2)
    # 监控调阅人均时长
    dy_info = info_data[info_data['CHECK_WAY'] == 3]
    monitor_time = int(dy_data['COST_TIME'].sum())
    ratio10 = round(ded_zero(monitor_time, person_count), 4)
    # 监控调阅人均问题数
    monitor_problem = int(dy_info['PROBLEM_NUMBER'].sum())
    ratio11 = round(ded_zero(monitor_problem, person_count), 2)
    # 监控调阅覆盖比例
    address = 0
    ratio12 = 0
    # 监控调阅人均质量分
    score = int(data[data['CHECK_WAY'] == 3]['CHECK_SCORE'].sum())
    ratio13 = round(ded_zero(score, person_count), 2)
    return {
        'check_count': check_count,
        'xc': xc,
        'find_problem': find_problem,
        'work': work,
        'shebei': shebei,
        'manage': manage,
        'gener': gener,
        'check_score': check_score,
        'yecha': yecha,
        'monitor_time': monitor_time,
        'monitor_problem': monitor_problem,
        'address': address,
        'score': score,
        'high': high,
        'avg_high': avg_high,
        'two_problem': two_problem,
        'avg_two': avg_two,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'ratio6': ratio6,
        'ratio7': ratio7,
        'ratio8': ratio8,
        'ratio9': ratio9,
        'ratio10': ratio10,
        'ratio11': ratio11,
        'ratio12': ratio12,
        'ratio13': ratio13,
    }


def calc_check_intensity_problem(data, person_data, check_info_data, dy_time_data):
    """
    计算专业所有问题数据
    :param data:
    :param person_data:
    :return:
    """
    # 检查总次数
    check_count = len(check_info_data)
    person_count = len(person_data)
    ratio1 = round(ded_zero(check_count, person_count), 2)
    # 现场两违检查
    xc = len(data[data['CHECK_WAY'] == 1])
    ratio2 = round(ded_zero(xc, person_count), 2)
    # 检查发现问题
    find_problem = len(data['PROBLEM_NUMBER'])
    ratio3 = round(ded_zero(find_problem, person_count), 2)
    # 作业类问题
    work = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    ratio4 = round(ded_zero(work, person_count), 2)
    # 设备类问题
    shebei = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    ratio5 = round(ded_zero(shebei, person_count), 2)
    # 管理类问题
    manage = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    ratio6 = round(ded_zero(manage, person_count), 2)
    # 高质量问题
    high = len(data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    avg_high = round(ded_zero(high, person_count), 2)
    # 两违问题
    two_problem = len(data[data['LEVEL'].isin(['A', 'B'])])
    avg_two = round(ded_zero(two_problem, person_count), 2)
    # 一般及以上风险问题
    gener = len(data[data['RISK_LEVEL'].isin([1, 2, 3])])
    ratio7 = round(ded_zero(gener, person_count), 2)
    # 检查问题质量分
    check_score = int(data['CHECK_SCORE'].sum())
    ratio8 = round(ded_zero(check_score, person_count), 2)
    # 夜间检查率
    yecha = len(check_info_data[check_info_data['IS_YECHA'] == 1])
    ratio9 = round(ded_zero(yecha, check_count), 2)
    # 监控调阅人均时长
    dy_info = check_info_data[check_info_data['CHECK_WAY'] == 3]
    monitor_time = int(dy_time_data['COST_TIME'].sum())
    ratio10 = round(ded_zero(monitor_time, person_count), 2)
    # 监控调阅人均问题数
    monitor_problem = int(dy_info['PROBLEM_NUMBER'].sum())
    ratio11 = round(ded_zero(monitor_problem, person_count), 2)
    # 监控调阅覆盖比例
    address = 0
    ratio12 = 0
    # 监控调阅人均质量分
    score = int(data[data['CHECK_WAY'] == 3]['CHECK_SCORE'].sum())
    ratio13 = round(ded_zero(score, person_count), 2)
    return {
        'avg_high': avg_high,
        'avg_two': avg_two,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'ratio6': ratio6,
        'ratio7': ratio7,
        'ratio8': ratio8,
        'ratio9': ratio9,
        'ratio10': ratio10,
        'ratio11': ratio11,
        'ratio12': ratio12,
        'ratio13': ratio13,
    }


# ---------------------------------------------绘图----------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第一张
    data['third']['picture1'] = InlineImage(template, data['third']['images1'], width=Mm(180), height=Mm(150))
    data['third']['picture2'] = InlineImage(template, data['third']['images2'], width=Mm(180), height=Mm(150))
