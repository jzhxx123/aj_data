# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B34456E0539106C00A2E70KSC'"""


# 问题信息
check_problem_sql = """SELECT p.PK_ID,p.STATUS,b.NAME,c.CHECK_WAY,
p.`LEVEL`,p.RISK_LEVEL,d.IDENTITY,p.PROBLEM_POINT,p.DESCRIPTION
from t_check_problem p 
LEFT JOIN t_check_info c on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department g on g.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_department a on g.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where p.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B34456E0539106C00A2E70KSC'"""


# 电务处检查信息
dwc_check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,a.ALL_NAME,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE4 = '999900020014000315DC0006' and c.CHECK_WAY = 1 and e.IDENTITY = '干部'"""


# 电务处问题信息
dwc_check_problem_sql = """SELECT c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,f.GRADATION,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.IDENTITY,g.`STATUS`,p.PROBLEM_POINT
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department g on g.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_person_level e on e.LEVEL = d.LEVEL
LEFT JOIN t_person_gradation_ratio f on f.pk_id=e.FK_PERSON_GRADATION_RATIO_ID
LEFT JOIN t_department a on p.EXECUTE_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE4 = '999900020014000315DC0006' and c.CHECK_WAY = 1 and d.IDENTITY = '干部'"""


# 履职查询
CHECK_EVALUATE_SQL = """SELECT
	a.*,
	b.SITUATION,
	e.NAME 
FROM
	t_check_evaluate_info AS a
	LEFT JOIN t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
	LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department d ON d.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department e ON e.DEPARTMENT_ID = d.TYPE3 
WHERE
	a.CREATE_TIME >= '{0}' 
	AND a.CREATE_TIME <= '{1} 23:59:59' 
	AND c.TYPE2 = '1ACE7D1C80B34456E0539106C00A2E70KSC'"""


# 履职复查
CHECK_EVALUATE_REVIEW_SQL = """SELECT c.ALL_NAME,b.EVALUATE_TYPE,b.EVALUATE_WAY
FROM t_check_evaluate_review_person b 
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_REVIEW_DEPARTMENT_ID
WHERE b.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
AND c.TYPE2 = '1ACE7D1C80B34456E0539106C00A2E70KSC'
"""


# 监控调阅时长
CHECK_MV_TIME_OR_PROBLEM_SQL = """SELECT
        e.`NAME`,SUM(a.COST_TIME) AS TIME
    FROM
        t_check_info_and_media AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
            LEFT JOIN
		t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
		    LEFT JOIN 
		t_department e on e.DEPARTMENT_ID=d.TYPE3
    WHERE
        b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
            AND b.CHECK_WAY in (3,4)
			AND d.TYPE2= '1ACE7D1C80B34456E0539106C00A2E70KSC'
			GROUP BY e.`NAME`"""


CHECK_RISK_NUMBER_SQL = """select `ALL_NAME` from t_risk where ALL_NAME like '%%电务%%'"""