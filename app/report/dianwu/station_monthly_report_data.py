from app.data.util import pd_query
import datetime
import pandas as pd
from app.report.draw_picture import *
from app.report.dianwu.station_monthly_report_sql import *
from docxtpl import InlineImage
from docx.shared import Mm
from dateutil.relativedelta import relativedelta
from app.new_big.util import get_data_from_mongo_by_find


def get_data(year, month, station_id):
    global station_name, times
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    times = int(year) * 100 + int(month)
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    # 检查信息
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(CHECK_INFO_SQL.format(last_month_start, last_month_end, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 问题信息
    pro_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date, station_id))
    # 履职评价
    eva_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date, station_id))
    last_eva = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(last_month_start, last_month_end, station_id))
    year_eva = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date[:4] + '-01-01', end_date, station_id))
    # 车间信息
    shop_data = pd_query(ZHANDUAN_PERSON_SQL.format(station_id))
    # 违章大王信息
    iiilege_data = pd_query(CHECK_IIILEGE_SQL.format(year, month, station_id))
    wz_pro_data = pd_query(CHECK_PROBLEM_WZ_SQL.format(start_date, end_date, station_id))
    # 安全分析中心量化指标
    center_data = pd_query(SAFEFY_CENTER_ANALYSIS_SQL.format(year, start_month, station_id))
    # 预警信息
    warning_data = pd_query(warning_info_sql.format(start_date, end_date, station_id))

    first = get_first(start_date, end_date, last_month_start, last_month_end, station_id)
    second = get_second(info_data, last_info, pro_data)
    third = get_third(eva_data, last_eva, year_eva)
    fourth = get_four(info_data, shop_data)
    fifth = get_five(pro_data)
    six = get_six(iiilege_data, wz_pro_data)
    seven = get_seven(center_data, warning_data, eva_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '电务',
        "station_id": station_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second": second,
        'third': third,
        "four": fourth,
        'five': fifth,
        'six': six,
        'seven': seven,
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# ------------------------------------------------------------------------第一部分--------------------------------------
def get_first(start_date, end_date, last_month_start, last_month_end, station_id):
    """
    段安全生产基本情况分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :param station_id:
    :return:
    """
    start_date = int(''.join(start_date.split('-')))
    end_date = int(''.join(end_date.split('-')))
    last_month_start = int(''.join(last_month_start.split('-')))
    last_month_end = int(''.join(last_month_end.split('-')))
    data = pd.DataFrame(calc_safety_produce_data(start_date, end_date, station_id))
    last_data = pd.DataFrame(calc_safety_produce_data(last_month_start, last_month_end, station_id))
    # 事故故障件数
    acc = len(data['MAIN_CLASS'].isin(['事故', '故障']))
    last_acc = len(last_data['MAIN_CLASS'].isin(['事故', '故障']))
    ratio = calculate_month_and_ring_ratio([acc, last_acc])
    # 道岔、轨道、车载、电源
    count = []
    for i in ['道岔', '轨道电路', '车载', '电源']:
        new_data = data[data['RISK_NAME'].str.contains(i)]
        count.append(len(new_data))
    return {
        'acc': acc,
        'ratio': ratio,
        'count': count
    }


def calc_safety_produce_data(now_date, end_date, dp_id):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param now_date:
    :param end_date:
    :return:
    """
    keys = {
        "match": {
            "DATE": {
                '$lte': end_date,
                '$gte': now_date
            },
            "TYPE3": dp_id
        },
        "project": {
            "_id": 0,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        documents = [{
            'RISK_NAME': '无',
            "MAIN_CLASS": '无'
        }]
    return documents


# ------------------------------------------------------------------------第二部分--------------------------------------
def get_second(info_data, last_info, pro_data):
    """
    检查基本情况统计分析
    :param info_data:
    :param last_info:
    :param pro_data:
    :return:
    """
    # 检查基本情况
    check_base_situation = get_check_base_situation(info_data, last_info)
    # 问题统计分析
    problem_analysis = get_problem_analysis(pro_data)
    # 问题“三性”分析
    problem_three_analysis = get_problem_three_analysis(pro_data)
    return {
        'check_base_situation': check_base_situation,
        'problem_analysis': problem_analysis,
        'problem_three_analysis': problem_three_analysis
    }


def get_problem_three_analysis(pro_data):
    """
    问题“三性”分析
    :param pro_data:
    :return:
    """
    problem = pro_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PROBLEM_POINT', ascending=False).reset_index()
    if problem.empty is False:
        problem_name = problem['PROBLEM_CLASSITY_NAME'][0]
    else:
        problem_name = '无'
    name_list = problem['PROBLEM_CLASSITY_NAME'].tolist()
    problem_count = problem['PROBLEM_POINT'].tolist()
    problem_type = pro_data.groupby('LEVEL').count().sort_values(by='PROBLEM_POINT', ascending=False).reset_index()
    level_type = problem_type['LEVEL'].tolist()
    level_count = problem_type['PROBLEM_POINT'].tolist()
    if problem_type.empty is False:
        problem_type_name = problem_type['LEVEL'][0]
    else:
        problem_type_name = '无'
    # 绘图
    images = hentail_pie_picture(level_count, problem_count, level_type, name_list, '问题分项统计',
                                 str(times) + '月份' + station_name, '电务', 'monthly')
    return {
        'problem_name': problem_name,
        'problem_type_name': problem_type_name,
        'images': images
    }


def get_problem_analysis(pro_data):
    """
    问题统计分析
    :param pro_data:
    :return:
    """
    problem = pro_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PROBLEM_POINT').reset_index()
    problem_name = problem['PROBLEM_CLASSITY_NAME'].tolist()
    problem_count = [int(i) for i in problem['PROBLEM_POINT'].tolist()]
    images = bar_picture([problem_count], [str(times)[4:] + '月'], problem_name, station_name + '问题归类统计', str(times) + '月份', '电务',
                         'monthly')
    return {
        'problem_name': problem_name,
        'images': images
    }


def get_check_base_situation(info_data, last_info):
    """
    检查基本情况
    :param info_data:
    :param last_info:
    :return:
    """
    # 检查次数
    check_count = len(info_data)
    # 现场检查
    xc_check = len(info_data[info_data['CHECK_WAY'] == 1])
    last_xc = len(last_info[last_info['CHECK_WAY'] == 1])
    xc_ratio = calculate_month_and_ring_ratio([xc_check, last_xc])
    # 音视频调阅检查
    mv_check = len(info_data[info_data['CHECK_WAY'].isin([3, 4])])
    last_mv = len(last_info[last_info['CHECK_WAY'].isin([3, 4])])
    mv_ratio = calculate_month_and_ring_ratio([mv_check, last_mv])
    # 添乘检查
    tc_check = len(info_data[info_data['CHECK_WAY'] == 2])
    last_tc = len(last_info[last_info['CHECK_WAY'] == 2])
    tc_ratio = calculate_month_and_ring_ratio([tc_check, last_tc])
    # 绘图
    count1 = []
    count2 = []
    for i in ['机关', '车间']:
        if i == '机关':
            new_data = info_data[info_data['TYPE'] < 8]
        else:
            new_data = info_data[info_data['TYPE'] >= 8]
        xc = len(new_data[new_data['CHECK_WAY'] == 1])
        mv = len(new_data[new_data['CHECK_WAY'].isin([3, 4])])
        tc = len(new_data[new_data['CHECK_WAY'] == 2])
        count1.append([xc, mv, tc])
    for i in ['机关', '车间']:
        if i == '机关':
            new_data = last_info[last_info['TYPE'] < 8]
        else:
            new_data = last_info[last_info['TYPE'] >= 8]
        xc = len(new_data[new_data['CHECK_WAY'] == 1])
        mv = len(new_data[new_data['CHECK_WAY'].isin([3, 4])])
        tc = len(new_data[new_data['CHECK_WAY'] == 2])
        count2.append([xc, mv, tc])
    xc_count = [count2[0][0], count2[1][0], last_xc, count1[0][0], count1[1][0], xc_check]
    tc_count = [count2[0][2], count2[1][2], last_tc, count1[0][2], count1[1][2], tc_check]
    mv_count = [count2[0][1], count2[1][1], last_mv, count1[0][1], count1[1][1], mv_check]
    timess = ['机关干部' + str(times - 1)[4:] + '月份', '车间干部' + str(times - 1)[4:] + '月份', str(times - 1)[4:] +
              '月份总计', '机关干部' + str(times)[4:] + '月份', '车间干部' + str(times)[4:] + '月份', str(times)[4:] + '月份总计']
    images = bar_picture1([tc_count, xc_count, mv_count], timess, ['添乘', '现场', '音视频'],
                          station_name + '月基本检查情况统计', str(times) + '月份', '电务', 'monthly')
    return {
        'check_count': check_count,
        'xc_check': xc_check,
        'xc_ratio': xc_ratio,
        'tc_check': tc_check,
        'tc_ratio': tc_ratio,
        'mv_check': mv_check,
        'mv_ratio': mv_ratio,
        'images': images
    }


# ------------------------------------------------------------------------第三部分--------------------------------------
def get_third(eva_data, last_eva, year_eva):
    """
    干部履职情况简要统计分析"
    :param eva_data:
    :param last_eva:
    :param year_eva:
    :return:
    """
    # 干部履职问题类型统计表
    evaluate_problem_table = _get_evaluate_problem_table(eva_data, last_eva)
    # 干部履职突出问题记分类型统计表
    issue_problem_table = _get_issue_problem_table(eva_data)
    # 累计计分统计
    score_table = _get_score_table(year_eva)
    # 分析
    if len(evaluate_problem_table['ratio']) != 0:
        up = evaluate_problem_table['ratio'].index(max(evaluate_problem_table['ratio']))
        up_name = evaluate_problem_table['all_list'][0][up]
        score = issue_problem_table['problem_ratio'].index(max(issue_problem_table['problem_ratio']))
        up_score = issue_problem_table['list1'][score]
    else:
        up_name = '无'
        up_score = 0
    return {
        'evaluate_problem_table': evaluate_problem_table,
        'issue_problem_table': issue_problem_table,
        'score_table': score_table,
        'up_name': up_name,
        'up_score': up_score
    }


def _get_evaluate_problem_table(eva_data, last_eva):
    """
    干部履职问题类型统计表
    :param eva_data:
    :param last_eva:
    :return:
    """
    all_list = []
    data = eva_data.groupby('ITEM_NAME').count().reset_index()
    for j, k in data.iterrows():
        last_data = last_eva[last_eva['ITEM_NAME'] == k['ITEM_NAME']]
        # 上月条数
        last_count = len(last_data)

        name = k['ITEM_NAME']
        # 条数
        count = int(k['GRADATION'])
        # 占比
        diff = round(ded_zero(count, len(eva_data)) * 100, 2)
        all_list.append([name, count, diff, last_count])
    # 环比
    ratio = []
    for i in all_list:
        ratio.append(calculate_month_and_ring_ratio([i[1], i[3]])['month_percent'])
    return {
        'all_list': all_list,
        'ratio': ratio
    }


def _get_issue_problem_table(eva_data):
    """
    干部履职突出问题记分类型统计表
    :param eva_data:
    :return:
    """
    list1 = ['专业管理重点落实不到位。', '日常重点安全工作落实不到位。', '倒查重点问题，检查质量低下。', '监控调阅检查不认真。', '问题整改督促不力。']
    problem_number = []
    problem_ratio = []
    for i in list1:
        data = eva_data[eva_data['SITUATION'] == i]
        problem_number.append(len(data))
        problem_ratio.append(round(ded_zero(len(data), len(eva_data)) * 100, 2))
    return {
        'list1': list1,
        'problem_number': problem_number,
        'problem_ratio': problem_ratio
    }


def _get_score_table(year_eva):
    """
    计分统计表
    :param year_eva:
    :return:
    """
    score = [0, 0, 0, 0, 0, 0, 0]
    data = year_eva.groupby('RESPONSIBE_ID_CARD').sum().reset_index()
    for j in data['SCORE']:
        if j < 2:
            score[0] += 1
        elif j < 4:
            score[1] += 1
        elif j < 6:
            score[2] += 1
        elif j < 8:
            score[3] += 1
        elif j < 10:
            score[4] += 1
        elif j < 12:
            score[5] += 1
        else:
            score[6] += 1
    return {
        'score': score
    }


# ------------------------------------------------------------------------第四部分--------------------------------------
def get_four(info_data, shop_data):
    """
    近期安全管理持续薄弱科室、车间情况分析
    :param info_data:
    :param shop_data:
    :return:
    """
    all_shop = []
    for i, j in shop_data.iterrows():
        data = info_data[info_data['SHOP1'] == j['NAME']]
        # 上级检查
        top_check = data[data['TYPE'] < 8]
        # 自身检查
        self_check = data[(data['TYPE'] >= 8) & (data['SHOP1'] == data['SHOP2'])]
        # 上级检查次数
        count = len(top_check)
        # 上级检查发现问题
        number = int(data['PROBLEM_NUMBER'].sum())
        # 车间自身检查次数
        station_count = len(self_check)
        # 车间自检发现问题
        station_num = int(self_check['PROBLEM_NUMBER'].sum())
        # 绘图
        images = bars_picture([[count], [station_count], [number], [station_num]], [''],
                             ['上级检查次数', '车间检查次数', '上级发现问题数', '车间发现问题数'], j['NAME'] + '检查数据分析',
                             str(times) + '月份', '电务', 'monthly')
        all_shop.append({
            'name': j['NAME'],
            'count': count,
            'number': number,
            'station_count': station_count,
            'station_num': station_num,
            'images': images
        })
    return {
        'all_shop': all_shop
    }


# ------------------------------------------------------------------------第五部分--------------------------------------
def get_five(data):
    """
    音视频运用情况分析
    :param data:
    :return:
    """
    new_data = data[data['CHECK_WAY'] == 3]
    types = ['A', 'B', 'C', 'D', 'E', 'F']
    num = []
    for i in types:
        num.append(len(new_data[new_data['LEVEL'].str.contains(i)]))
    return {
        'num': num
    }


# ------------------------------------------------------------------------第六部分--------------------------------------
def get_six(iiilege_data, wz_pro_data):
    # 绘图1
    all_count = []
    new_data = wz_pro_data
    new_data = new_data[new_data['BIRTHDAY'] != ' ']
    new_data = new_data.dropna(subset=['BIRTHDAY'])
    new_data['AGE'] = new_data['BIRTHDAY'].apply(
        lambda x: (datetime.datetime.now().date() -
                   datetime.datetime.strptime(x[:4] + '-' + x[4:6] + '-' + x[6:], '%Y-%m-%d').date()).days // 365)
    new_data1 = new_data.groupby('AGE').count().reset_index()
    count1 = [0, 0, 0, 0, 0]
    for i in range(len(new_data1)):
        if new_data1.iloc[i]['AGE'] <= 30:
            count1[0] += new_data1.iloc[i]['POSITION']
        elif 30 < new_data1.iloc[i]['AGE'] <= 40:
            count1[1] += new_data1.iloc[i]['POSITION']
        elif 40 < new_data1.iloc[i]['AGE'] <= 50:
            count1[2] += new_data1.iloc[i]['POSITION']
        elif 50 < new_data1.iloc[i]['AGE'] <= 60:
            count1[3] += new_data1.iloc[i]['POSITION']
        else:
            count1[4] += new_data1.iloc[i]['POSITION']
    all_count.append(count1)
    name1 = ['小于30', '30~40', '40~50', '50~60', '其他']
    images1 = bar_picture1(all_count, name1, [station_name], '违章人员年龄分布情况', 'major_' + str(times), '电务', 'monthly')

    # 绘图2
    all_list = []
    new_data = iiilege_data.drop_duplicates(subset=['ID_CARD'])
    all_list.append(len(new_data))
    images2 = bar_picture([all_list], [station_name], [''],
                          '违章大王人数统计', 'major_' + str(times), '电务', 'monthly')
    return {
        'images1': images1,
        'images2': images2
    }


# ------------------------------------------------------------------------第七部分--------------------------------------
def get_seven(center_data, warning_data, eva_data):
    """
    分析中心主要工作质量分析
    :param center_data:
    :param warning_data:
    :param eva_data:
    :return:
    """
    data = center_data
    data2 = warning_data
    data1 = eva_data
    # 音视频调阅时长
    monitor_time = float(data['MONITOR_TIME'].sum())
    # 复查时长
    monitor_review_time = float(data['MONITOR_REVIEW_TIME'].sum())
    # 调阅发现问题数
    monitor_problem = int(data['MONITOR_PROBLEM'].sum())
    # 关键时段调阅次数
    key_number = int(data['MONITOR_KEY_PERIOD_NUMBER'].sum())
    # 调阅问题质量分
    monitor_problem_score = float(data['MONITOR_PROBLEM_SCORE'].sum())
    # 科职管理人员评价人数
    secco = len(data1[data1['GRADATION'].isin(['正科职管理人员', '副科职管理人员'])]['CHECK_PERSON_ID_CARD'].unique())
    # 评价记分人数
    count = len(data1['CHECK_PERSON_ID_CARD'].unique())
    # 阶段评价人次
    jd_number = len(data1[data1['EVALUATE_WAY'] == 3])
    # 预警次数
    warning_count = len(data2)
    # 音视频调阅不认真评价人数
    not_serious = len(data1[data1['SITUATION'].str.contains('监控调阅检查不认真')]['CHECK_PERSON_ID_CARD'].unique())
    # 现场检查不认真评价人
    xc_not_serious = len(data1[data1['SITUATION'].str.contains('现场监控不认真')]['CHECK_PERSON_ID_CARD'].unique())
    return {
        'monitor_time': monitor_time,
        'monitor_review_time': monitor_review_time,
        'monitor_problem': monitor_problem,
        'key_number': key_number,
        'monitor_problem_score': monitor_problem_score,
        'secco': secco,
        'count': count,
        'warning_count': warning_count,
        "not_serious": not_serious,
        'jd_number': jd_number,
        'xc_not_serious': xc_not_serious
    }


# -----------------------------------------------第八部分---------------------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第一张
    # data['first']['picture'] = InlineImage(template, data['first']['images'], width=Mm(120))
    data['second']['check_base_situation']['picture'] = InlineImage(template, data['second']['check_base_situation'][
        'images'], width=Mm(150), height=Mm(120))
    data['second']['problem_analysis']['picture'] = InlineImage(template, data['second']['problem_analysis']['images'],
                                                                width=Mm(150), height=Mm(120))
    data['second']['problem_three_analysis']['picture'] = InlineImage(template,
                                                                      data['second']['problem_three_analysis'][
                                                                          'images'], width=Mm(150), height=Mm(120))
    data['four']['picture'] = [InlineImage(template, i['images'], width=Mm(150), height=Mm(120)) for i in data['four']['all_shop']]
    data['six']['picture1'] = InlineImage(template, data['six']['images1'], width=Mm(150), height=Mm(120))
    data['six']['picture2'] = InlineImage(template, data['six']['images2'], width=Mm(150), height=Mm(120))
