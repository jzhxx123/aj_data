from docxtpl import DocxTemplate
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from app import mongo
from app.report.analysis_report_manager import WeeklyAnalysisReport, restore_numpy_value
from app.report.dianwu.major_weekly_report_data import get_fitst, get_second, \
    get_third, get_fourth, get_safety_tip


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_month = start_date[5:7]
    start_day = start_date[8:]
    end_month = end_date[5:7]
    end_day = end_date[8:]
    year = start_date[:4]
    first = get_fitst(start_date, end_date, last_week_start, last_week_end)
    second = get_second(start_date, end_date)
    third = get_third(start_date, end_date)
    fourth = get_fourth(start_date, end_date)
    sixth = get_safety_tip(start_date, end_date)
    file_name = f'{start_date}至{end_date}电务系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        'start_month': start_month,
        'start_day': start_day,
        'end_month': end_month,
        'end_day': end_day,
        'year': year,
        "major": '电务',
        "hierarchy": "MAJOR",
        "created_at": dt.now(),
        'file_name': file_name,
        "first": first,
        "second": second,
        'third': third,
        "fourth": fourth,
        "sixth": sixth,
    }
    return result


class DianwuMajorWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    电务的专业级的周分析报告类。
    """

    def __init__(self):
        # 电务周报，周五至周四
        super(DianwuMajorWeeklyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='电务', start_weekday=4)

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date)
        restore_numpy_value(data)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data
