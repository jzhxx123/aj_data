from app.report.dianwu.major_quarterly_report_data import get_data
from app.report.analysis_report_manager import QuarterlyAnalysisReport
from app.report.dianwu import major_quarterly_report_data as report_data
from app import mongo
from docxtpl import DocxTemplate
import os


class DianwuMajorQuarterlyAnalysisReport(QuarterlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self):
        super(DianwuMajorQuarterlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='电务')

    def generate_report_data(self, year, quarter):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param quarter: int 季度
        :return:
        """
        data = get_data(year, quarter)
        mongo.db['safety_analysis_quarterly_report'].delete_one(
            {
                "year": year,
                "quarter": quarter,
                "major": self.major,
                "hierarchy": "MAJOR",
            })
        mongo.db['safety_analysis_quarterly_report'].insert_one(data)
        return data

    def generate_report(self, year, quarter):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param quarter: {int} 季度
        :return:
        """
        data = self.load_report_data(year, quarter)
        tpl = DocxTemplate('app/report/template/dianwu_major_quarterly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path, image_path = self.get_report_paths()
        file_path = os.path.join(dir_path, data['file_name'])
        print(file_path)
        tpl.save(file_path)
        return dir_path, data['file_name']