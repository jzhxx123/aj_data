from app.data.util import pd_query
import re
from app.report.dianwu.major_weekly_report_sql import *


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 2) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# ---------------------------------------------------------------第一部分-----------------------------------------------
def get_fitst(start_date, end_date, last_week_start, last_week_end):
    """
    基本检查情况
    :param start_date:
    :param end_date:
    :param last_week_start:
    :param last_week_end:
    :return:
    """
    global problem_data, info_data
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date))
    last_info = pd_query(check_info_sql.format(last_week_start, last_week_end))
    # 检查问题
    problem_data = pd_query(check_problem_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem_data = pd_query(check_problem_sql.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 基本检查
    base_value = _first_base_count(info_data, last_info, problem_data, last_problem_data)
    # 设备监控调阅
    shebei = get_shebei(info_data, last_info, problem_data)
    # 各段检查及设备监控调阅情况明细
    station_check_table = _station_check_table(info_data, problem_data)
    return {
        "base_value": base_value,
        "shebei": shebei,
        'station_check_table': station_check_table
    }


def _first_base_count(info_data, last_info, problem_data, last_problem_data):
    # 现场检查次数
    xc = len(info_data[info_data['CHECK_WAY'] == 1])
    ratio1 = calculate_month_and_ring_ratio([xc, len(last_info[last_info['CHECK_WAY'] == 1])])
    # 发现问题数
    find_pro = int(info_data['PROBLEM_NUMBER'].sum())
    ratio2 = calculate_month_and_ring_ratio([find_pro, int(last_info['PROBLEM_NUMBER'].sum())])
    # 问题分类统计
    count = []
    for i in ['作业', "E", 'F', 'G']:
        if i == '作业':
            data = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
        else:
            data = problem_data[problem_data['LEVEL'].str.contains(i)]
        count.append(len(data))
    ratio = [round(ded_zero(i, find_pro) * 100, 2) for i in count]
    # 性质严重问题
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    ratio3 = calculate_month_and_ring_ratio([main_pro, len(last_problem_data[last_problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])])
    diff = round(ded_zero(main_pro, find_pro) * 100, 2)
    return {
        'xc': xc,
        'ratio1': ratio1,
        'find_pro': find_pro,
        'ratio2': ratio2,
        'count': count,
        'ratio': ratio,
        'main_pro': main_pro,
        'ratio3': ratio3,
        'diff': diff
    }


def _get_weekly_diff(now, last):
    if last == 0:
        return 0
    else:
        return round((now - last) / last * 100, 1)


def _all_value(problem_data):
    count = len(problem_data)
    all_list = []
    for i in ['作业', '管理', '设备', '外部环境']:
        data = problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        ratio = round(len(data) / count * 100, 1)
        all_list.append({
            'data': len(data),
            'ratio': ratio
        })
    return {
        'all_list': all_list
    }


def _main_problem(problem_data, last_pro):
    all_count = []
    for i in ['A', 'B', 'E1', 'E2', 'F1', 'F2']:
        count = len(problem_data[problem_data['LEVEL'].isin([i])])
        all_count.append(count)
    last_all_type = len(last_pro[last_pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    all_type = sum(all_count)
    type_more_less = all_type - last_all_type
    count = len(problem_data)
    ratio = round(all_type / count * 100, 1)
    return {
        'all_count': all_count,
        'all_type': all_type,
        'type_more_less': type_more_less,
        'ratio': ratio
    }


def get_shebei(info_data, last_info, problem_data):
    # 设备问题+检查次数
    shebei = info_data[info_data['CHECK_WAY'].isin([3, 4])]
    last_shebei = last_info[last_info['CHECK_WAY'].isin([3, 4])]
    info = len(shebei)
    ratio1 = calculate_month_and_ring_ratio([info, len(last_shebei)])
    pro = int(shebei['PROBLEM_NUMBER'].sum())
    ratio2 = calculate_month_and_ring_ratio([pro, int(last_shebei['PROBLEM_NUMBER'].sum())])
    # 性质严重问题
    main_pro = len(problem_data[(problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])) & (problem_data['CHECK_WAY'].isin([3, 4]))])
    ratio3 = round(ded_zero(main_pro, pro), 2)
    return {
        'info': info,
        'ratio1': ratio1,
        'pro': pro,
        'ratio2': ratio2,
        'main_pro': main_pro,
        'ratio3': ratio3
    }


def _station_check_table(info_data, problem_data):
    station_list = ['成都电务段', '重庆电务段', '达州电务段', '贵阳电务段', '成都电务维修段']
    all_list = []
    for i in station_list:
        new_info = info_data[info_data['NAME'].str.contains(i)]
        new_pro = problem_data[problem_data['NAME'].str.contains(i)]
        # 下现场检查
        xc_info_data = new_info[new_info['CHECK_WAY'] == 1]
        xc_pro_data = new_pro[new_pro['CHECK_WAY'] == 1]
        xc_count = len(xc_info_data)
        xc_pro = int(xc_info_data['PROBLEM_NUMBER'].sum())
        xc_main = len(xc_pro_data[xc_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        # 设备监控调阅
        shebei_info_data = new_pro[new_pro['CHECK_WAY'].isin([3, 4])]
        shebei_pro_data = new_pro[new_pro['CHECK_WAY'].isin([3, 4])]
        shebei_count = len(shebei_info_data)
        shebei_pro = len(shebei_pro_data)
        shebei_main = len(shebei_pro_data[shebei_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        all_list.append([i, xc_count, xc_pro, xc_main, shebei_count, shebei_pro, shebei_main])
    return {
        'all_list': all_list
    }


# --------------------------------------------------------------------------第二部分------------------------------------
def get_second(start_date, end_date):
    # 履职检查信息
    data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date))
    # 履职复查信息
    review_data = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date))
    # 1.评价基本情况
    evaluate_base = _get_evaluate_base(data, review_data)
    # 2.评价记分情况
    evaluate_score_problem = _get_evaluate_score_problem(data)
    # 评价扣分具体情况
    eva_situation = _get_eva_situation(data)
    return {
        'evaluate_base': evaluate_base,
        'evaluate_score_problem': evaluate_score_problem,
        'eva_situation': eva_situation
    }


def _get_eva_situation(data):
    """
    评价扣分具体情况
    :param end_date:
    :param data:
    :return:
    """
    new_data = data.groupby(['CODE', 'SITUATION']).count().sort_values(by='NAME').reset_index()
    situations = []
    for i, k in new_data.iterrows():
        situations.append(k['CODE'] + k['SITUATION'] + '{0}人次'.format(k['NAME']))
    return {
        'situations': situations
    }


def _get_evaluate_base(data, review_data):
    """
    评价基本情况
    :param data:
    :return:
    """
    # 共计逐条评价+复查
    one_count = len(data[data['EVALUATE_WAY'] == 1])
    one_count_review = len(review_data[review_data['EVALUATE_WAY'] == 1])
    # 定期评价+复查
    regular_count = len(data[data['EVALUATE_WAY'] == 2])
    regular_count_review = len(review_data[review_data['EVALUATE_WAY'] == 2])
    # 设备监控调阅复查
    shebei = len(review_data[review_data['EVALUATE_WAY'] == 3])
    # 本周评价总计分
    score_count = round(float(data['SCORE'].sum()), 4)
    # 计分最多
    try:
        score_data = data.groupby(['RESPONSIBE_PERSON_NAME', 'RESPONSIBE_ID_CARD']).sum().sort_values(
            by='SCORE', ascending=False).reset_index()
        id_card = score_data['RESPONSIBE_ID_CARD'][0]
        score_name = score_data['RESPONSIBE_PERSON_NAME'][0]
        most_score = int(score_data['SCORE'][0])
        score_dp = data[data['RESPONSIBE_ID_CARD'] == id_card]['NAME'].tolist()[0]
    except:
        score_name = '无'
        most_score = 0
        score_dp = '无'
    return {
        'one_count': one_count,
        'one_count_review': one_count_review,
        'regular_count': regular_count,
        'regular_count_review': regular_count_review,
        'shebei': shebei,
        'score_count': score_count,
        'most_score': most_score,
        'score_name': score_name,
        'score_dp': score_dp
    }


def _get_evaluate_score_problem(data):
    station_evaluate_number = len(data)
    # 各机务段被计分人次
    station_list = ['成都电务段', '重庆电务段', '达州电务段', '贵阳电务段', '成都电务维修段']
    l1 = []
    for i in station_list:
        l1.append(len(data[data['NAME'] == i]))
    # 定期评价
    dq_evaluate = len(data[data['EVALUATE_WAY'] == 2])
    # 检查信息、问题逐条评价
    check_problem_infos = data[data['EVALUATE_WAY'].isin([1])]
    check_problem_info = len(check_problem_infos[check_problem_infos['EVALUATE_TYPE'].isin([1, 2])])
    # 阶段评价
    level_evaluate = len(data[data['EVALUATE_WAY'] == 3])
    # 按职务分
    ju_leader = len(data[data['GRADATION'].str.contains('局管领导人员')])
    zk = len(data[data['GRADATION'].str.contains('正科')])
    fk = len(data[data['GRADATION'].str.contains('副科')])
    manage_skill = len(data[data['GRADATION'].str.contains('一般管理和专业技术人员')])
    not_manage = len(data[data['GRADATION'].str.contains('非管理')])
    # 按分数分
    all_score = round(float(data['SCORE'].sum()), 4)
    luju = round(float(data[data['CHECK_TYPE'] == 1]['SCORE'].sum()), 4)
    zhanduan = round(float(data[data['CHECK_TYPE'] == 2]['SCORE'].sum()), 4)
    # 最高扣分
    new_data = data.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    id_card = new_data['RESPONSIBE_ID_CARD'][0]
    max_data = data[data['RESPONSIBE_ID_CARD'] == id_card].reset_index()
    max_station = max_data['NAME'][0]
    max_score = round(float(max_data['SCORE'].sum()), 4)
    max_name = max_data['RESPONSIBE_PERSON_NAME'][0]
    reason = []
    for i, k in max_data.iterrows():
        reason.append('因{0},扣{1}分。'.format(k['SITUATION'], k['SCORE']))
    return {
        'major_evaluate_number': len(data[data['CHECK_TYPE'] == 1]),
        'station_evaluate_number': station_evaluate_number,
        'l1': l1,
        'dq_evaluate': dq_evaluate,
        'check_problem_info': check_problem_info,
        'level_evaluate': level_evaluate,
        'ju_leader': ju_leader,
        'zk': zk,
        'fk': fk,
        'manage_skill': manage_skill,
        'not_manage': not_manage,
        'all_score': all_score,
        'luju': luju,
        'zhanduan': zhanduan,
        'max_score': max_score,
        'max_name': max_name,
        'reason': reason,
        'max_station': max_station
    }


# ---------------------------------------------------------第三部分---------------------------------------------------------------------
def get_third(start_date, end_date):
    """
    干部检查典型问题
    :param start_date:
    :param end_date:
    :return:
    """
    # 电务处检查信息、问题
    dwc_info = pd_query(dwc_check_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    dwc_pro = pd_query(dwc_check_problem_sql.format(start_date, end_date))
    # 检查概况
    check_situation = get_check_situation(info_data, problem_data, dwc_info, dwc_pro)
    # 各级干部检查发现典型问题
    find_problem = get_find_problem(problem_data)
    return {
        'check_situation': check_situation,
        'find_problem': find_problem,
    }


def get_check_situation(info_data, problem_data, dwc_info, dwc_problem):
    """
    检查概况
    :param info_data:
    :param problem_data:
    :param dwc_info:
    :param dwc_problem:
    :return:
    """
    check_info = info_data[(info_data['CHECK_WAY'] == 1) & (info_data['IDENTITY'] == '干部')]
    check_pro = problem_data[(problem_data['CHECK_WAY'] == 1) & (problem_data['IDENTITY'] == '干部')]
    # 检查次数
    check_count = len(check_info)
    # 发现问题数
    find_pro = len(check_pro)
    # 已整改
    zg = len(check_pro[check_pro['STATUS'].isin([4, 5])])
    # 待销号+已销号
    zg_d = len(check_pro[check_pro['STATUS'].isin([4])])
    zg_y = len(check_pro[check_pro['STATUS'].isin([5])])
    # 整改中
    zg_z = len(check_pro[check_pro['STATUS'].isin([3])])
    # 电务处
    dwc_check = len(dwc_info)
    dwc_pro = len(dwc_problem)
    dwc_zg = len(dwc_problem[dwc_problem['STATUS'].isin([4, 5])])
    dwc_d = len(dwc_problem[dwc_problem['STATUS'].isin([4])])
    dwc_y = len(dwc_problem[dwc_problem['STATUS'].isin([5])])
    dwc_z = len(dwc_problem[dwc_problem['STATUS'].isin([3])])
    return {
        'check_count': check_count,
        'find_pro': find_pro,
        'zg': zg,
        'zg_d': zg_d,
        'zg_y': zg_y,
        'zg_z': zg_z,
        'dwc_check': dwc_check,
        'dwc_pro': dwc_pro,
        'dwc_zg': dwc_zg,
        'dwc_d': dwc_d,
        'dwc_y': dwc_y,
        'dwc_z': dwc_z
    }


def get_find_problem(data):
    # 发现AB类问题
    a_data = data[(data['LEVEL'].isin(['A', 'B'])) & (data['CHECK_WAY'].isin([1, 2])) & (data['IDENTITY'] == '干部')]
    all_list = []
    for j, k in a_data.iterrows():
        all_list.append({
            'type': k['LEVEL'],
            'content': k['DESCRIPTION'],
        })
    return {
        'all_list': all_list[:3]
    }


# ----------------------------------------------------------------第四部分------------------------------------------------------------------------
def get_fourth(start_date, end_date):
    data = pd_query(CHECK_MV_TIME_OR_PROBLEM_SQL.format(start_date, end_date))
    # 检查概况
    fourth_one = get_check_base(info_data, data)
    # 各级干部检查发现典型问题
    fourth_two = get_check_find_problem(problem_data)
    return {
        'fourth_one': fourth_one,
        'fourth_two': fourth_two
    }


def get_check_base(info_data, data):
    """
    检查概况
    :param data:
    :return:
    """
    all_time = round(float(data['TIME'].sum()), 4)
    all_problem = info_data[info_data['CHECK_WAY'].isin([3, 4])]
    station_list = ['成都电务段', '重庆电务段', '贵阳电务段', '达州电务段', '成都电务维修段']
    time = []
    problem = []
    for i in station_list:
        new_data = data[data['NAME'] == i]
        pro_data = all_problem[all_problem['NAME'] == i]
        time.append(round(float(new_data['TIME'].sum()), 4))
        problem.append(int(pro_data['PROBLEM_NUMBER'].sum()))
    return {
        'all_time': all_time,
        'all_problem': int(all_problem['PROBLEM_NUMBER'].sum()),
        'station_list': station_list,
        'time': time,
        'problem': problem
    }


def get_check_find_problem(data):
    """
    各级干部检查发现典型问题
    :param data:
    :return:
    """
    # 发现AB类问题
    a_data = data[(data['LEVEL'].isin(['A'])) & (data['CHECK_WAY'].isin([3, 4])) & (data['IDENTITY'] == '干部')]
    all_list = []
    for j, k in a_data.iterrows():
        all_list.append({
            'type': k['LEVEL'],
            'content': k['DESCRIPTION'],
        })
    if len(all_list) < 3:
        b_data = data[(data['LEVEL'].isin(['B'])) & (data['CHECK_WAY'].isin([3, 4])) & (data['IDENTITY'] == '干部')]
        for j, k in b_data.iterrows():
            all_list.append({
                'type': k['LEVEL'],
                'content': k['DESCRIPTION'],
            })
    return {
        'all_list': all_list[:3]
    }


# -------------------------------------------------------------第六部分-----------------------------------------------------------------
def get_safety_tip(start_date, end_date):
    year = start_date[:4]
    month = start_date[5:7]
    day = int(start_date[8:])
    # 总风险数
    risk_data = pd_query(CHECK_RISK_NUMBER_SQL)
    risk_data['NAME'] = risk_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    risk_count = len(risk_data['NAME'].unique())
    # 本周检查风险数
    risk_names = info_data.dropna(subset=['RISK_NAME'])['RISK_NAME'].unique().tolist()
    risk_names.pop(risk_names.index(''))
    week_risk_name = get_split_risk_name(risk_names)
    # 至今日期
    start_date = get_calc_day_or_month(year, month, day)
    today_risk_data = pd_query(check_info_sql.format(start_date, end_date)).dropna(subset=['RISK_NAME']).drop_duplicates(subset=['PK_ID']).reset_index()
    today_risk_names = today_risk_data['RISK_NAME'].unique().tolist()
    today_risk_name = get_split_risk_name(today_risk_names)
    # 未检查风险数
    count = risk_count - len(today_risk_name)
    # 未检查风险名
    not_check_risk_name = [i for i in risk_data['NAME'].unique().tolist() if i not in today_risk_name]
    return {
        'risk_count': risk_count,
        'week_risk': len(week_risk_name),
        'count': count,
        'not_check_risk_name': not_check_risk_name
    }


def get_split_risk_name(risk_name):
    """
    处理风险长字符串
    :param risk_name:
    :return:
    """
    for i in range(len(risk_name)):
        risk_name[i] = risk_name[i].split(',')
        for j in range(len(risk_name[i])):
            if risk_name[i][j] == '':
                continue
            else:
                risk_name[i][j] = risk_name[i][j].split('-')[1]
        risk_name[i] = list(set(risk_name[i]))
    risk_name = [y for x in risk_name for y in x]
    return list(set(risk_name))


def get_calc_day_or_month(year, month, day):
    if day >= 25:
        start_date = year + '-' + month + '-' + '25'
    else:
        start_date = year + '-' + month[0] + str(int(month[1]) - 1) + '-' + '25'
    return start_date