from app.report.dianwu.station_monthly_report_data import get_data
from docxtpl import DocxTemplate
from app import mongo
import os
import app.report.analysis_report_manager as manager
import app.report.dianwu.station_monthly_report_data as report_data


class DianwuStationMonthlyAnalysisReport(manager.MonthlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(DianwuStationMonthlyAnalysisReport, self).__init__(hierarchy_type='STATION', major='电务',
                                                                 station_id=station_id)

    def generate_report_data(self, year, month):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param month: int 月
        :return:
        """
        data = get_data(year, month, self.station_id)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "month": month,
                "hierarchy": "STATION",
                "station_id": self.station_id,
                "major": self.major
            })
        mongo.db['safety_analysis_monthly_report'].insert_one(data)
        return data

    def generate_report(self, year, month):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param month: {int} 月份
        :return:
        """
        data = self.load_report_data(year, month)
        tpl = DocxTemplate('app/report/template/dianwu_station_monthly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']


