import numpy as np
from datetime import datetime as dt
from app.report.analysis_report_manager import DailyAnalysisReport
from app.data.util import pd_query
from app.report.common_sql_two import *
import threading

locals_data = threading.local()

_MAIN_PROBLEM = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4']
}

MAJOR = "1ACE7D1C80B34456E0539106C00A2E70KSC"


def get_data(year, month, day):
    report_date = dt(year, month, day)
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    times = (params[0][0], params[0][1])
    _TOP10_RISK = list(pd_query(RISK_MAIN_TYPE_SQL.format(MAJOR)).dropna()['NAME'])
    locals_data.TOP10_RISK = _TOP10_RISK
    first = get_first(times, month, day)
    second = get_second(times)
    third = get_third(times)
    four = get_four(times)
    return {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "MAJOR",
        'major': '电务',
        "file_name": '{0}电务系统安全分析日报.xlsx',
        "created_at": dt.now(),
        'first': first,
        'second': second,
        'third': third,
        'four': four
    }


def get_first(times, month, day):
    # 安全情况
    first_one = get_first_one(times, month, day)
    # 事故故障情况
    first_two = get_first_two(times, month, day)
    return {
        'first_one': first_one,
        'first_two': first_two
    }


def get_first_one(times, month, day):
    """
    安全生产情况
    :param times:
    :param month:
    :param day:
    :param worksheet:
    :return:
    """
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format('2017-10-01', times[1], MAJOR))
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
        if len(accident) == 0:
            dic[i] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic[i] = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).
                         iloc[0]['OCCURRENCE_TIME'].date()).split(' ')[0]
    content = '截至{0}月{1}日：全段实现无责任一般A类事故{2}天;无责任一般B类事故{3}天;无责任一般C类事故{4}天;' \
              '无责任一般D类事故{5}天。全段实现安全生产{6}天。'.format(month, day, dic['A'], dic['B'], dic['C'], dic['D'], dic['A'])
    return content


def get_first_two(times, month, day):
    infos = []
    without_accident = pd_query(without_accident_sql.format(times[0], times[1], MAJOR))
    if len(without_accident) == 0:
        content = "{0}月{1}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障0件。其中信号故障0件、机车故障0件、动车组故障0件。" \
            .format(month, day)
        info = {
            'acc': '无',
            'fault': '无'
        }
        infos.append(info)
    else:
        data = without_accident
        acc = len(data[data['MAIN_TYPE'] == 1])
        fault = len(data[data['MAIN_TYPE'] == 2])
        comp_info = len(data[data['MAIN_TYPE'] == 3])
        # 涉及工务段的综合信息
        comp_infos = len(data[(data['MAIN_TYPE'] == 3) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)])
        # 行车、劳动、路外
        safety_list = []
        for i in [1, 2, 3]:
            new_data = data[data['DETAIL_TYPE'] == i]
            safety_list.append(len(new_data[new_data['MAIN_TYPE'] == 1]))
        # 设备故障
        shebei = data[data['NAME'].str.contains('故障')]
        all_shebei = []
        for i in ['信号故障', '机车故障', '动车组故障']:
            acc_data = shebei[(shebei['NAME'].str.contains(i)) & (shebei['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
            all_shebei.append(len(acc_data))
        content = "{0}月{1}日，发生事故{2}件、故障{3}件。" \
                  "\n1.行车安全：发生{4}件事故。" \
                  "\n2.劳动安全：发生{5}件事故。" \
                  "\n3.路外安全：发生{6}件相撞事故。" \
                  "\n4.设备故障{7}件。其中信号故障{8}件、机车故障{9}件、动车组故障{10}件。" .\
            format(month, day, acc, fault, safety_list[0], safety_list[1], safety_list[2], len(shebei), all_shebei[0],
                   all_shebei[1], all_shebei[2])
    return content


def get_second(times):
    """
    监督检查基本情况
    :param times:
    :param worksheet:
    :return:
    """
    # 检查信息
    check_info = pd_query(check_info_sql.format(times[0], times[1], MAJOR))
    # 检查问题
    check_problem = pd_query(check_problem_sql.format(times[0], times[1], MAJOR)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 分析中心日报
    center_daily = pd_query(analysis_center_daily.format(times[0], times[1], MAJOR))

    # 全局总体检查情况
    second_one = get_second_one(check_info, check_problem)
    # 客运处检查情况
    second_two = get_second_two(check_problem)
    # 分析中心检查
    second_three = get_second_three(center_daily)
    return {
        'second_one': second_one,
        'second_two': second_two,
        'second_three': second_three
    }


def get_second_one(check_info, check_problem):
    dic = {}
    # 检查基本情况
    xc = len(check_info[check_info['CHECK_WAY'].isin([1, 2])])
    find_problem = int(check_info['PROBLEM_NUMBER'].sum())
    main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    work_point = []
    # 问题三项
    for i in ['作业', '设备', '管理']:
        data = check_problem[check_problem['LEVEL'].isin(_MAIN_PROBLEM[i])]
        work_point.append(len(data))
    dic['one'] = '电务系统下现场检查{0}人次，发现问题{1}个。性质严重问题{2}个，其中作业项{3}个、' \
                 '设备设施项{4}个、管理项{5}个。'. \
        format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2])
    # 典型突出问题
    content = []
    for i, j in check_problem[check_problem['LEVEL'].isin(['A', 'B', 'E1', 'F1'])].iterrows():
        content.append(j['DESCRIPTION'])
    dic['two'] = '\n'.join(list(set(content)))
    # 重点风险管控
    checked = []
    uncheck = []
    rk_data = check_info.dropna(subset=['RISK_NAME'])
    for rk in locals_data.TOP10_RISK:
        if rk_data[rk_data['RISK_NAME'].str.contains(rk)].shape[0] > 0:
            checked.append(rk)
        else:
            uncheck.append(rk)
    checked_con = '，'.join(checked)
    uncheck_con = '，'.join(uncheck)
    line31 = '今日检查风险{}大类，分别是：{}\n'.format(len(checked), checked_con)
    line32 = '今日未检查风险{}大类，分别是：{}'.format(len(uncheck), uncheck_con)
    dic['three'] = line31 + line32
    return dic


def get_second_two(check_problem):
    """
    各分析中心典型突出问题
    :param check_problem:
    :return:
    """
    content = []
    for i in ['达州电务段', '电务部', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        data = check_problem[(check_problem['STATION'] == i) & (check_problem['LEVEL'].isin(['A', 'B', 'E1', 'F1']))]
        content.append('\n'.join(data['DESCRIPTION'].unique().tolist()) if len(data['DESCRIPTION']) != 0 else '无')
    return content


def get_second_three(center_daily):
    """
    分析中心检查情况
    :param center_daily:
    :return:
    """
    content = {}
    # 音视频调阅时长
    cost_time = float('%.4f' % center_daily[center_daily['FLAG'] == 1]['DAILY_COMPLETE'].sum())
    # 履职评价条数
    eva_count1 = int(center_daily[center_daily['FLAG'].isin([3])]['DAILY_COMPLETE'].sum())
    eva_count2 = int(center_daily[center_daily['FLAG'].isin([4])]['DAILY_COMPLETE'].sum())
    # 履职复查条数
    re_count1 = int(center_daily[center_daily['FLAG'].isin([5])]['DAILY_COMPLETE'].sum())
    re_count2 = int(center_daily[center_daily['FLAG'].isin([6])]['DAILY_COMPLETE'].sum())
    # 阶段评价
    jd = int(center_daily[center_daily['FLAG'].isin([7])]['DAILY_COMPLETE'].sum())
    # 音视频复查发现问题
    re_pro = int(center_daily[center_daily['FLAG'].isin([2])]['DAILY_PROBLEM_NUMBER'].sum())
    # 履职评价发现问题
    eva_pro = int(center_daily[center_daily['FLAG'].isin([3, 4])]['DAILY_PROBLEM_NUMBER'].sum())
    content['one'] = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，干部履职评价{1}条，' \
                     '干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，阶段评价{5}人次。 问题查处情况：音视频调阅' \
                     '（复查）发现问题{6}个，干部履职评价（复查）发现{7}个问题。'.format(cost_time, eva_count1, eva_count2,
                                                               re_count1, re_count2, jd, re_pro, eva_pro)
    # 评价分析工作存在的主要问题
    center_daily = center_daily.dropna(subset=['CENTER_PROBLEM'])
    content['three'] = '\n'.join(list(center_daily['CENTER_PROBLEM'].unique())) \
        if len(center_daily['CENTER_PROBLEM']) != 0 else '无'
    return content


def get_third(times):
    """
    重点安全信息追踪
    :param times:
    :return:
    """
    safety_info = pd_query(main_info_sql.format(times[0], times[1], MAJOR))
    # 总公司追踪
    info = safety_info[safety_info['HIERARCHY'].isin([1])]
    infos = info['CONTENT'].unique().tolist()[:3]
    if len(infos) == 0:
        infos = ['无', '无', '无']
    # 路局追踪
    road_info = safety_info[safety_info['HIERARCHY'] == 2]
    road_infos = road_info['CONTENT'].unique().tolist()[:3]
    if len(road_infos) == 0:
        road_infos = ['无', '无', '无']
    # 专业追踪
    major_info = safety_info[safety_info['HIERARCHY'] == 3]
    major_infos = major_info['CONTENT'].unique().tolist()[:3]
    if len(major_infos) == 0:
        major_infos = ['无', '无', '无']
    return {
        'infos': infos,
        'major_infos': major_infos,
        'road_infos': road_infos,
    }


def get_four(times):
    """
    风险预警
    :param times:
    :return:
    """
    warning_data = pd_query(warning_notification_sql.format(times[0], times[1], MAJOR))
    warning_data = warning_data[warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7])]
    # 总预警
    luju_count = len(warning_data[warning_data['HIERARCHY'] == 1])
    # 分析室预警
    fxs_count = len(warning_data[warning_data['HIERARCHY'] == 2])
    # 站段分析中心预警
    zd_count = len(warning_data[warning_data['HIERARCHY'] == 3])
    content = f"总预警{luju_count}条，分析室{fxs_count}条，站段分析中心{zd_count}条"
    # 站段预警内容
    station_content = []
    for i in ['达州电务段', '电务部', '贵阳电务段', '重庆电务段', '成都电务段', '成都电务维修段']:
        data = warning_data[(warning_data['STATION'] == i) & (warning_data['HIERARCHY'] == 3)]
        station_content.append('\n'.join(data['CONTENT'].unique().tolist()) if len(data['CONTENT']) != 0 else '无')
    return {
        'content': content,
        'station_content': station_content,
    }
