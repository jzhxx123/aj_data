from app.report.dianwu.station_weekly_report_data import get_data
from app.report.analysis_report_manager import WeeklyAnalysisReport
from docxtpl import DocxTemplate
from app import mongo
import os
import app.report.analysis_report_manager as manager
import app.report.dianwu.station_weekly_report_data as report_data


class DianwuStationWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    机务的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(DianwuStationWeeklyAnalysisReport, self).__init__(hierarchy_type='STATION', major='电务',
                                                                station_id=station_id)

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date, self.station_id)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "STATION",
                "major": self.major,
                "station_id": self.station_id
            })
        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data

    def generate_report(self, year, month):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param month: {int} 月份
        :return:
        """
        data = self.load_report_data(year, month)
        tpl = DocxTemplate('app/report/template/dianwu_station_weekly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']
