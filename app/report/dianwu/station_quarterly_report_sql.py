# 发现问题信息
FIND_PROBLEM_SQL = """SELECT a.MAIN_TYPE,c.DEPARTMENT_NAME,b.RISK_NAME,a.OCCURRENCE_TIME from t_safety_produce_info a
LEFT JOIN t_safety_produce_info_responsibility_unit_and_risk b on a.PK_ID=b.FK_SAFETY_PRODUCE_INFO_ID
LEFT JOIN t_safety_produce_info_responsibility_unit c on b.FK_RESPONSIBILITY_UNIT_ID=c.PK_ID
where a.OCCURRENCE_TIME BETWEEN '{0}' and '{1}'
and c.FK_DEPARTMENT_ID = '{2}'
"""

CHECK_INFO_PROBLEM_SQL = """select a.DEPARTMENT_ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,b.`LEVEL`,b.IS_RED_LINE,
b.PROBLEM_CLASSITY_NAME,b.RISK_NAMES,b.PROBLEM_POINT,b.CHECK_ITEM_NAME
from t_check_info a,t_check_problem b
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and a.PK_ID=b.FK_CHECK_INFO_ID and EXISTS
(SELECT c.DEPARTMENT_ID from t_department c where c.type3 = '{2}' and b.EXECUTE_DEPARTMENT_ID=c.department_id)"""

CHECK_EVALUATE_SCORE_PRODUCT_SQL = """SELECT
        a.RESPONSIBE_PERSON_NAME,
        a.EVALUATE_WAY,
        a.CHECK_TYPE,
        a.GRADATION,
        a.`LEVEL`,
        a.SCORE,
         b.ITEM_NAME,
         b.SITUATION,
        c.ALL_NAME,
        d.IS_EVALUATE
FROM t_check_evaluate_info a
LEFT JOIN t_check_evaluate_config b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
LEFT JOIN t_department c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_check_info d on a.FK_CHECK_OR_PROBLEM_ID = d.PK_ID 
WHERE a.CREATE_TIME >= '{0}' AND a.CREATE_TIME <= '{1}'
and c.TYPE3 = '{2}'
"""

CHECK_CHEJIAN_SQL = """SELECT PROBLEM_NUMBER,CHECK_ITEM_NAMES,DEPARTMENT_ALL_NAME,CHECK_TYPE from t_check_info 
where SUBMIT_TIME BETWEEN '{0}' and '{1}'
and CHECK_ITEM_NAMES like '电务%%' and DEPARTMENT_ALL_NAME like '{2}%%'"""

CHECK_MV_SITUATION_SQL = """select b.`LEVEL`,b.CHECK_ITEM_NAME
from t_check_info a,t_check_problem b
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and a.PK_ID=b.FK_CHECK_INFO_ID and EXISTS
(SELECT c.DEPARTMENT_ID from t_department c where c.type3 = '{2}' and b.EXECUTE_DEPARTMENT_ID=c.department_id)"""

CHECK_IIILEGE_SQL = """SELECT a.`YEAR`,a.`MONTH`,a.DEDUCT_SCORE,c.POSITION,c.BIRTHDAY from t_warning_key_person a
LEFT JOIN t_warning_key_person_config b on a.FK_CONFIG_ID = b.PK_ID
LEFT JOIN t_person c on a.ID_CARD = c.ID_CARD
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
where a.`YEAR`={0} and `MONTH` >={1} and `MONTH` <={2}
and d.TYPE3 = '{3}'"""

SAFEFY_CENTER_ANALYSIS_SQL = """SELECT a.MONITOR_REVIEW_TIME,a.MONITOR_TIME,a.MONITOR_PROBLEM,a.MONITOR_KEY_PERIOD_NUMBER,a.MONITOR_PROBLEM_SCORE,
a.NON_MANAGEMENT_EVALUATE_REVIEW_NUMBER,a.SECCO_MANAGEMENT_QUANTITY,a.GENERAL_AND_PROFESSIONAL_QUANTITY,
a.EVALUATE_NOT_SERIOUS_NUMBER
from t_analysis_center_quantization_quota a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
where b.TYPE3 = '{3}'
and a.TYPE = 3 and a.`YEAR` = {0} 
and a.`MONTH` >= {1} and a.`MONTH` <= {2}"""