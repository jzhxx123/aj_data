from app.data.util import pd_query
from app import mongo
from app.report.analysis_report_manager import AnnualAnalysisReport
import datetime
from app.report.draw_picture import *
from app.report.dianwu.major_annual_report_sql import *
from docxtpl import InlineImage
from docx.shared import Mm


def get_data(year):
    date = AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month-11
    first = get_fitst(start_date, end_date, last_month_start, last_month_end)
    second = get_second(start_date, end_date)
    third = get_third(year, start_month, end_month)
    fourth = get_fourth(year, start_month, end_month)
    fifth = get_fifth(start_date, end_date, year, start_month, end_month)
    file_name = f'{start_date}至{end_date}电务系统安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '电务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second": second,
        'third': third,
        "fourth": fourth,
        'fifth': fifth,
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calc_ded_score(data):
    """
    计算扣分
    :param data:
    :return:
    """
    score_data = data[data['SCORE'] != 0]
    # 扣分人次
    ded_score_count = len(score_data)
    # 总扣分数
    ded_score = int(score_data['SCORE'].sum())
    new_data = score_data.groupby('RESPONSIBE_PERSON_NAME').sum().sort_values(by='SCORE', ascending=False).reset_index()
    if new_data.empty is not True:
        # 最高分
        max_name = new_data['RESPONSIBE_PERSON_NAME'].tolist()[0]
        max_score = int(new_data['SCORE'].tolist()[0])
        max_reason = score_data[score_data['RESPONSIBE_PERSON_NAME'] == max_name]['SITUATION'].tolist()
        # 最低分
        min_name = new_data.iloc[-1]['RESPONSIBE_PERSON_NAME']
        min_score = int(new_data.iloc[-1]['SCORE'])
        min_reason = score_data[score_data['RESPONSIBE_PERSON_NAME'] == min_name]['SITUATION'].tolist()
        return {
            'ded_score_count': ded_score_count,
            'ded_score': ded_score,
            'max_name': max_name,
            'max_score': max_score,
            'max_reason': [x.strip() for x in max_reason],
            'min_name': min_name,
            'min_score': min_score,
            'min_reason': [x.strip() for x in min_reason]
        }
    else:
        return {
            'ded_score_count': 0
        }


# ------------------------------------------------------------------第一部分--------------------------------------------
def get_fitst(start_date, end_date, last_month_start, last_month_end):
    """
    干部评价总体情况
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :return:
    """
    global evaluate_data
    evaluate_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date))
    last_evaluate_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(last_month_start, last_month_end))
    # 顶部信息及表格
    first_top = get_first_top(evaluate_data, last_evaluate_data)
    # 安全效果评价扣分
    safety_ded = get_safety_ded(evaluate_data)
    # 典型故障及事故评价扣分
    typical_accident = get_typical_accident(evaluate_data)
    # 重点工作推进评价扣分
    main_work = get_main_work(evaluate_data)
    # 音视频评价扣分
    mv_ded = get_mv_ded(evaluate_data)
    # 安管系统运用评价
    safety_manager = get_safety_manager(evaluate_data)
    return {
        'first_top': first_top,
        'safety_ded': safety_ded,
        'typical_accident': typical_accident,
        'main_work': main_work,
        'mv_ded': mv_ded,
        'safety_manager': safety_manager
    }


def get_safety_manager(data):
    data = data[data['EVALUATE_CONTENT'].str.contains('安管系统运用评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_mv_ded(data):
    """
    音视频评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('音视频评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_main_work(data):
    """
    重点工作推进评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('重点工作推进评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_typical_accident(data):
    """
    典型故障及事故评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('典型故障及事故评价')]
    info = calc_ded_score(data)
    return {
        'info': info
    }


def get_safety_ded(data):
    """
    安全效果评价扣分
    :param data:
    :return:
    """
    data = data[data['EVALUATE_CONTENT'].str.contains('安全效果评价')]
    data = data[data['MAIN_TYPE'].isin([1, 2])]
    # 总问题数
    problem_count = len(data)
    info = calc_ded_score(data)
    return {
        'problem_count': problem_count,
        'info': info,
    }


def get_first_top(data, last_data):
    """
    顶部信息及表格
    :param data:
    :param last_data:
    :return:
    """
    # 扣分人次
    ded_person_count = len(data[~data['SCORE'].isin([0])])
    # 总计扣分
    ded_score = int(data['SCORE'].sum())
    # 扣分分类
    ded_type = _get_score_table(data, last_data)
    return {
        'ded_person_count': ded_person_count,
        'ded_score': ded_score,
        'ded_type': ded_type,
    }


def _get_score_table(data, last_data):
    """
    计分统计表
    :param data:
    :return:
    """
    data = data.groupby('RESPONSIBE_PERSON_NAME').sum().sort_values(by='SCORE', ascending=False).reset_index()
    list1 = []
    list2 = []
    list3 = []
    # 各分段计分
    for i in range(6):
        if data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)].empty:
            list1.append(0)
        else:
            list1.append(len(data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)]))
    if data[data['SCORE'] >= 12].empty:
        list1.append(0)
    else:
        list1.append(len(data[data['SCORE'] >= 12]))

    for i in range(6):
        if last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)].empty:
            list2.append(0)
        else:
            list2.append(len(last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)]))
    if last_data[last_data['SCORE'] >= 12].empty:
        list2.append(0)
    else:
        list2.append(len(last_data[last_data['SCORE'] >= 12]))
    for i in range(len(list1)):
        list3.append(list1[i] - list2[i])
    return {
        'list1': list1,
        'list3': list3
    }


# ----------------------------------------------------------------第二部分----------------------------------------------
def get_second(start_date, end_date):
    """
    音视频运用情况分析
    :param start_date:
    :param end_date:
    :return:
    """
    data = pd_query(CHECK_MV_SITUATION_SQL.format(start_date, end_date))
    new_data = data[data['CHECK_ITEM_NAME'].str.contains('音视频')]
    types = ['A', 'B', 'C', 'D', 'E', 'F']
    num = []
    for i in types:
        num.append(len(new_data[new_data['LEVEL'].str.contains(i)]))
    return {
        'type': types,
        'num': num
    }


# -------------------------------------------------------------------第三部分-------------------------------------------
def get_third(year, start_month, end_month):
    iiilege_data = pd_query(CHECK_IIILEGE_SQL.format(year, start_month, end_month))
    iiilege_data = iiilege_data.dropna()
    iiilege_data['AGE'] = iiilege_data['BIRTHDAY'].apply(
        lambda x: (datetime.datetime.now().date() -
                   datetime.datetime.strptime(x[:4] + '-' + x[4:6] + '-' + x[6:], '%Y-%m-%d').date()).days // 365)
    # 绘图1
    new_data = iiilege_data.groupby('POSITION').count().reset_index()
    count = new_data['AGE'].tolist()
    name = new_data['POSITION'].tolist()
    images1 = bar_picture([count], name, name, '责任岗位统计', 'major_' + str(year), '电务', 'quarterly')
    # 绘图2
    new_data1 = iiilege_data.groupby('AGE').count().reset_index()
    count1 = [0, 0, 0, 0, 0]
    for i in range(len(new_data1)):
        if 20 < new_data1.iloc[i]['AGE'] <= 30:
            count1[0] += new_data1.iloc[i]['POSITION']
        elif 30 < new_data1.iloc[i]['AGE'] <= 40:
            count1[1] += new_data1.iloc[i]['POSITION']
        elif 40 < new_data1.iloc[i]['AGE'] <= 50:
            count1[2] += new_data1.iloc[i]['POSITION']
        elif 50 < new_data1.iloc[i]['AGE'] <= 60:
            count1[3] += new_data1.iloc[i]['POSITION']
        else:
            count1[4] += new_data1.iloc[i]['POSITION']
    name1 = ['20~30', '30~40', '40~50', '50~60', '其他']
    images2 = bar_picture([count1], name1, ['人次'], '年龄责任层次统计', 'major_' + str(year), '电务', 'quarterly')
    return {
        'images1': images1,
        'images2': images2
    }


# -------------------------------------------------------------------第四部分-------------------------------------------
def get_fourth(year, start_month, end_month):
    """
    分析中心主要工作质量分析
    :param year:
    :param start_month:
    :param end_month:
    :return:
    """
    global center_data
    center_data = pd_query(SAFEFY_CENTER_ANALYSIS_SQL.format(year, start_month, end_month))
    list1 = []
    for i in range(len(center_data)):
        list1.append(calc_index(center_data.loc[i]))
    return {
        'list1': list1
    }


def calc_index(data):
    """
    计算小指数
    :param data:
    :return:
    """
    data = data.fillna(0)
    # 站段名
    name = data['ALL_NAME']
    # 音视频调阅时长
    monitor_time = int(data['MONITOR_TIME'].sum())
    # 复查时长
    monitor_review_time = int(data['MONITOR_REVIEW_TIME'].sum())
    # 调阅发现问题数
    monitor_problem = int(data['MONITOR_PROBLEM'].sum())
    # 关键时段调阅次数
    key_number = int(data['MONITOR_KEY_PERIOD_NUMBER'].sum())
    # 调阅问题质量分
    monitor_problem_score = int(data['MONITOR_PROBLEM_SCORE'].sum())
    # 科职管理人员评价人数
    secco = int(data['SECCO_MANAGEMENT_QUANTITY'].sum())
    # 评价记分人数
    non = int(data['NON_MANAGEMENT_EVALUATE_REVIEW_NUMBER'].sum())
    general = int(data['GENERAL_AND_PROFESSIONAL_QUANTITY'].sum())
    count = secco + non + general
    # 阶段评价人次
    jd_number = len(
        evaluate_data[(evaluate_data['EVALUATE_WAY'] == 3) & (evaluate_data['ALL_NAME'].str.contains(name))])
    # 预警次数

    # 音视频调阅不认真评价人数
    not_serious = int(data['EVALUATE_NOT_SERIOUS_NUMBER'].sum())
    # 现场检查不认真评价人
    return {
        'name': name,
        'monitor_time': monitor_time,
        'monitor_review_time': monitor_review_time,
        'monitor_problem': monitor_problem,
        'key_number': key_number,
        'monitor_problem_score': monitor_problem_score,
        'secco': secco,
        'count': count,
        "not_serious": not_serious,
        'jd_number': jd_number
    }


# ----------------------------------------------------------------------第五部分----------------------------------------
def get_fifth(start_date, end_date, year, start_month, end_month):
    """
    大数据统计分析
    :param start_date:
    :param end_date:
    :param year:
    :param start_month:
    :return:
    """
    check_intensity_data = pd_query(CHECK_INTENSITY_SQL.format(start_date, end_date))
    person_data = pd_query(CHECK_PERSON_NUMBER)
    evaluate_intensity_data = pd_query(CHECK_EVALUATE_INTENSITY_SQL.format(start_date, end_date))
    assess_intensity_data = pd_query(CHECK_ASSESS_INTENSITY_SQL.format(year, start_month, end_month))
    check_evenness_data = pd_query(CHECK_EVENNESS_SQL.format(start_date, end_date))
    # 检查力度指数
    check_intensity = get_check_intensity(check_intensity_data, person_data, end_date)
    # 评价力度指数
    evaluate_intensity = get_evaluate_intensity(evaluate_intensity_data, person_data, end_date)
    # 考核力度指数
    assess_intensity = get_assess_intensity(assess_intensity_data, end_date)
    # 问题暴露指数
    # 问题整改指数
    # 检查均衡度指数
    check_evenness = get_check_evenness(check_evenness_data, end_date)
    return {
        'check_intensity': check_intensity,
        'evaluate_intensity': evaluate_intensity,
        'assess_intensity': assess_intensity,
        'check_evenness': check_evenness,
    }


def get_check_evenness(data, end_date):
    """
    检查均衡度指数
    :param data:
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 4,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 3},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    station_name = sorted(document, key=lambda k: k['RANK'], reverse=True)[0]['DEPARTMENT_NAME']
    station_data = data[data['DEPARTMENT_ALL_NAME'].str.contains(station_name)]
    new_data = station_data[station_data['IDENTITY'] == '干部']
    all_count = len(station_data)
    ganbu = len(new_data)
    count = all_count - ganbu
    document1 = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 4,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 1},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document1) == 0:
        document1.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    station_name1 = sorted(document1, key=lambda k: k['RANK'], reverse=True)[0]['DEPARTMENT_NAME']
    station_data1 = data[data['DEPARTMENT_ALL_NAME'].str.contains(station_name1)]
    new_data1 = station_data1[station_data1['IDENTITY'] == '干部']
    risk_name = new_data1[new_data1['PROBLEM_NUMBER'] == 0]['RISK_NAME'].tolist()
    name_list = []
    if len(risk_name) > 0:
        for name in risk_name:
            try:
                name_list.append(name.split(',')[0].split('-')[1])
            except IndexError:
                name_list.append(name)
    return {
        'count': count,
        'station_name': station_name,
        'risk_name': name_list,
        'station_name1': station_name1
    }


def get_assess_intensity(data, end_date):
    """
    考核力度指数
    :param data:
    :param end_date:
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 3,
         'MAJOR': '电务',
         'MON': date,
         'DETAIL_TYPE': 2},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    # 排序
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    station_name = sorted(document, key=lambda k: k['RANK'], reverse=True)[0]['DEPARTMENT_NAME']
    station = calc_assess_intensity_problem(data, station_name)
    one_station = calc_assess_intensity_problem_station(data, station_name)
    sort_list1 = station['list1']
    sort_list1.sort()
    index1 = sort_list1.index(one_station['list1'])
    sort_list2 = station['list2']
    sort_list2.sort()
    index2 = sort_list2.index(one_station['list2'])
    sort_list3 = station['list3']
    sort_list3.sort()
    index3 = sort_list3.index(one_station['list3'])
    if index1 == index2 == index3:
        c = 3
    elif index1 == index2 or index1 == index3 or index2 == index3:
        c = 2
    else:
        c = 0
    return {
        'station_name': station_name,
        'one_station': one_station,
        'index1': index1,
        'index2': index2,
        'index3': index3,
        'c': c
    }


def calc_assess_intensity_problem_station(data, station_name):
    """
    站段计算
    :param data:
    :param station_name:
    :return:
    """
    new_data = data[data['ALL_NAME'].str.contains(station_name)]
    # 总考核金额
    all_money = int(new_data['ACTUAL_MONEY'].sum())
    # 干部考核金额
    manger = int(new_data[new_data['IDENTITY'] == '干部']['ACTUAL_MONEY'].sum())
    # 职工考核金额
    zhigong = int(new_data[~new_data['IDENTITY'].isin(['干部'])]['ACTUAL_MONEY'].sum())
    list1 = round(ded_zero(manger, all_money), 2)
    if len(new_data[~new_data['IDENTITY'].isin(['干部'])]) == 0:
        list2 = 0
    else:
        list2 = round(ded_zero(manger, len(new_data[~new_data['IDENTITY'].isin(['干部'])])), 2)
    list3 = len(new_data[new_data['IDENTITY'].isin(['干部'])])
    return {
        'list1': list1,
        'list2': list2,
        'list3': list3,
        'all_money': all_money,
        'manger': manger,
        'zhigong': zhigong
    }


def calc_assess_intensity_problem(data, station_name):
    """
    站段计算
    :param data:
    :return:
    """
    list1 = []
    list2 = []
    list3 = []
    for i in station_name:
        new_data = data[data['ALL_NAME'].str.contains(i)]
        all_money = int(new_data['ACTUAL_MONEY'].sum())
        list1.append(round(ded_zero(int(new_data[new_data['IDENTITY'] == '干部']['ACTUAL_MONEY'].sum()), all_money), 2))
        if len(new_data[~new_data['IDENTITY'].isin(['干部'])]) == 0:
            list2.append(0)
        else:
            list2.append(round(ded_zero(int(new_data[~new_data['IDENTITY'].isin(['干部'])]['ACTUAL_MONEY'].sum()), len(
                new_data[~new_data['IDENTITY'].isin(['干部'])])), 2))
        list3.append(len(new_data[new_data['IDENTITY'].isin(['干部'])]))
    return {
        'list1': list1,
        'list2': list2,
        'list3': list3
    }


def get_evaluate_intensity(data, person_data, end_date):
    """
    评价力度指数
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 2,
         'MAJOR': '电务',
         'MON': date},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    station_name = sorted(document, key=lambda k: k['RANK'], reverse=True)[0]['DEPARTMENT_NAME']
    all_station = calc_evaluate_intensity_problem(data, person_data, station_name)
    return {
        'all_station': all_station,
        'station_name': station_name,
    }


def calc_evaluate_intensity_problem(data, person_data, station_name):
    """
    站段计算
    :param data:
    :param person_data:
    :param station_name:
    :return:
    """
    station_list = ['成都电务段', '成都电务维修段', '贵阳电务段', '重庆电务段', '达州电务段']
    all_list = []
    for i in station_list:
        new_data = data[(data['ALL_NAME'].str.contains(i)) & (data['EVALUATE_WAY'].isin([1, 2, 3]))]
        person = person_data[person_data['ALL_NAME'].str.contains(i)]
        identity_data = new_data[new_data['IDENTITY'] == '干部']
        dic = {
            'name': i,
            'zd': len(new_data),
            'zd_ratio': round(ded_zero(len(new_data), len(data[data['ALL_NAME'].str.contains(i)])) * 100, 1),
            'avg_zd': round(ded_zero(int(identity_data['SCORE'].sum()), len(identity_data)), 1),
            'all_job': len(new_data.groupby('LEVEL')),
            'job_ratio': round(ded_zero(len(new_data.groupby('LEVEL')), len(person.groupby('LEVEL'))) * 100, 1),
            'low_level': identity_data['LEVEL'].value_counts().index[-1] if identity_data.empty is not True else '无'
        }
        all_list.append(dic)
    sort1 = sorted(all_list, key=lambda x: x['zd_ratio'], reverse=True)
    sort2 = sorted(all_list, key=lambda x: x['avg_zd'], reverse=True)
    sort3 = sorted(all_list, key=lambda x: x['job_ratio'], reverse=True)
    index1 = 1
    index2 = 1
    index3 = 1
    for i in sort1:
        if i['name'] == station_name:
            break
        else:
            index1 += 1
    for i in sort2:
        if i['name'] == station_name:
            break
        else:
            index2 += 1
    for i in sort3:
        if i['name'] == station_name:
            break
        else:
            index3 += 1
    return {
        'all_list': all_list,
        'index1': index1,
        'index2': index2,
        'index3': index3,
    }


def get_check_intensity(data, person_data, end_date):
    """
    检查力度指数
    :return:
    """
    date = int(''.join(str(end_date).split('-')[:2]))
    document = list(mongo.db['monthly_detail_major_index'].find(
        {'MAIN_TYPE': 1,
         'MAJOR': '电务',
         'MON': date},
        {'_id': 0,
         'DEPARTMENT_NAME': 1,
         'RANK': 1
         }))
    if len(document) == 0:
        document.append({
            'DEPARTMENT_NAME': '1',
            'RANK': 1
        })
    station_name = sorted(document, key=lambda k: k['RANK'], reverse=True)[0]['DEPARTMENT_NAME']
    station = calc_check_intensity_problem_station(data, person_data, center_data, station_name)
    major = calc_check_intensity_problem(data, person_data, center_data)
    return {
        'station_name': station_name,
        'station': station,
        'major': major,
    }


def calc_check_intensity_problem_station(data, person_data, center_data, station_name):
    """
    计算站段所有问题数据
    :param data:
    :param person_data:
    :return:
    """
    data = data[data['DEPARTMENT_ALL_NAME'].str.contains(station_name)]
    person_data = person_data[person_data['ALL_NAME'].str.contains(station_name)]
    center_data = center_data[center_data['ALL_NAME'] == station_name]
    # 检查总次数
    check_count = len(data)
    person_count = len(person_data)
    ratio1 = round(ded_zero(check_count, person_count), 2)
    # 现场两违检查
    xc = len(data[data['CHECK_WAY'] == 1])
    ratio2 = round(ded_zero(xc, person_count), 2)
    # 检查发现问题
    find_problem = int(data['PROBLEM_NUMBER'].sum())
    ratio3 = round(ded_zero(find_problem, person_count), 2)
    # 作业类问题
    work = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    ratio4 = round(ded_zero(work, person_count), 2)
    # 设备类问题
    shebei = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    ratio5 = round(ded_zero(shebei, person_count), 2)
    # 管理类问题
    manage = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    ratio6 = round(ded_zero(manage, person_count), 2)
    # 高质量问题
    high = len(data[data['LEVEL'].isin(['A', 'B', 'F1'])])
    avg_high = round(ded_zero(high, person_count), 2)
    # 两违问题
    two_problem = len(data[data['LEVEL'].isin(['A', 'B'])])
    avg_two = round(ded_zero(two_problem, person_count), 2)
    # 一般及以上风险问题
    gener = len(data[data['RISK_LEVEL'] <= 3])
    ratio7 = round(ded_zero(gener, person_count), 2)
    # 检查问题质量分
    check_score = int(data['CHECK_SCORE'].sum())
    ratio8 = round(ded_zero(check_score, person_count), 2)
    # 夜间检查率
    yecha = len(data[data['IS_YECHA'] == 1])
    ratio9 = round(ded_zero(yecha, person_count), 2)
    # 监控调阅人均时长
    monitor_time = int(center_data['MONITOR_TIME'].sum())
    ratio10 = round(ded_zero(monitor_time, person_count), 2)
    # 监控调阅人均问题数
    monitor_problem = int(center_data['MONITOR_PROBLEM'].sum())
    ratio11 = round(ded_zero(monitor_problem, person_count), 2)
    # 监控调阅覆盖比例
    address = int(center_data['MONITOR_COVER_ADDRESS'].sum())
    ratio12 = round(ded_zero(address, len(data.groupby('DEPARTMENT_ALL_NAME').count().reset_index())), 2)
    # 监控调阅人均质量分
    score = int(center_data['MONITOR_PROBLEM_SCORE'].sum())
    ratio13 = round(ded_zero(score, person_count), 2)
    return {
        'check_count': check_count,
        'xc': xc,
        'find_problem': find_problem,
        'work': work,
        'shebei': shebei,
        'manage': manage,
        'gener': gener,
        'check_score': check_score,
        'yecha': yecha,
        'monitor_time': monitor_time,
        'monitor_problem': monitor_problem,
        'address': address,
        'score': score,
        'high': high,
        'avg_high': avg_high,
        'two_problem': two_problem,
        'avg_two': avg_two,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'ratio6': ratio6,
        'ratio7': ratio7,
        'ratio8': ratio8,
        'ratio9': ratio9,
        'ratio10': ratio10,
        'ratio11': ratio11,
        'ratio12': ratio12,
        'ratio13': ratio13,
    }


def calc_check_intensity_problem(data, person_data, center_data):
    """
    计算专业所有问题数据
    :param data:
    :param person_data:
    :return:
    """
    # 检查总次数
    check_count = len(data)
    person_count = len(person_data)
    ratio1 = round(ded_zero(check_count, person_count), 2)
    # 现场两违检查
    xc = len(data[data['CHECK_WAY'] == 1])
    ratio2 = round(ded_zero(xc, person_count), 2)
    # 检查发现问题
    find_problem = int(data['PROBLEM_NUMBER'].sum())
    ratio3 = round(ded_zero(find_problem, person_count), 2)
    # 作业类问题
    work = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    ratio4 = round(ded_zero(work, person_count), 2)
    # 设备类问题
    shebei = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    ratio5 = round(ded_zero(shebei, person_count), 2)
    # 管理类问题
    manage = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    ratio6 = round(ded_zero(manage, person_count), 2)
    # 高质量问题
    high = len(data[data['LEVEL'].isin(['A', 'B', 'F1'])])
    avg_high = round(ded_zero(high, person_count), 2)
    # 两违问题
    two_problem = len(data[data['LEVEL'].isin(['A', 'B'])])
    avg_two = round(ded_zero(two_problem, person_count), 2)
    # 一般及以上风险问题
    gener = len(data[data['RISK_LEVEL'] <= 3])
    ratio7 = round(ded_zero(gener, person_count), 2)
    # 检查问题质量分
    check_score = int(data['CHECK_SCORE'].sum())
    ratio8 = round(ded_zero(check_score, person_count), 2)
    # 夜间检查率
    yecha = len(data[data['IS_YECHA'] == 1])
    ratio9 = round(ded_zero(yecha, person_count), 2)
    # 监控调阅人均时长
    monitor_time = int(center_data['MONITOR_TIME'].sum())
    ratio10 = round(ded_zero(monitor_time, person_count), 2)
    # 监控调阅人均问题数
    monitor_problem = int(center_data['MONITOR_PROBLEM'].sum())
    ratio11 = round(ded_zero(monitor_problem, person_count), 2)
    # 监控调阅覆盖比例
    address = int(center_data['MONITOR_COVER_ADDRESS'].sum())
    ratio12 = round(ded_zero(address, len(data.groupby('DEPARTMENT_ALL_NAME').count().reset_index())), 2)
    # 监控调阅人均质量分
    score = int(center_data['MONITOR_PROBLEM_SCORE'].sum())
    ratio13 = round(ded_zero(score, person_count), 2)
    return {
        'avg_high': avg_high,
        'avg_two': avg_two,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'ratio6': ratio6,
        'ratio7': ratio7,
        'ratio8': ratio8,
        'ratio9': ratio9,
        'ratio10': ratio10,
        'ratio11': ratio11,
        'ratio12': ratio12,
        'ratio13': ratio13,
    }


# ---------------------------------------------绘图----------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第一张
    data['third']['picture1'] = InlineImage(template, data['third']['images1'], width=Mm(120))
    data['third']['picture2'] = InlineImage(template, data['third']['images2'], width=Mm(120))

