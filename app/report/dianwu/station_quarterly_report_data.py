from app.data.util import pd_query
from dateutil.relativedelta import relativedelta
import datetime
from app.report.draw_picture import *
from app.report.analysis_report_manager import QuarterlyAnalysisReport
from app.report.dianwu.station_quarterly_report_sql import *
from docxtpl import InlineImage
from docx.shared import Mm


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, quarter, station_id):
    date = QuarterlyAnalysisReport.get_quarter_intervals(year, quarter)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    times = str(year * 100 + month)
    start_month = end_month - 2
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    first = get_safe_produce_base_doc(start_date, end_date, last_month_start, last_month_end, station_id, year,
                                      station_name, times)
    second = get_check_base_doc(start_date, end_date, last_month_start, last_month_end, station_id, station_name, times, month)
    third = get_manage_evaluate_problem(start_date, end_date, last_month_start, last_month_end, station_id)
    fourth = get_safety_low_department_analysis(start_date, end_date, station_name, month, times)
    fifth = get_mv_situation(start_date, end_date, station_id)
    sixth = get_sixth(year, start_month, end_month, station_id, station_name, times)
    seventh = get_safety_center_work(year, start_month, end_month, station_id)
    file_name = f'{start_date}至{end_date}{station_name}安全管理季度分析.docx'
    result = {
        "year": year,
        "quarter": quarter,
        "start_month": start_month,
        "end_month": end_month,
        "major": '电务',
        "hierarchy": "STATION",
        'station_id': station_id,
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second": second,
        'third': third,
        "fourth": fourth,
        'fifth': fifth,
        'sixth': sixth,
        'seventh': seventh,
    }
    return result


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# ------------------------------------------------------------------------第一部分--------------------------------------
def get_safe_produce_base_doc(start_date, end_date, last_month_start, last_month_end, station_id, year, station_name, times):
    """
    段安全生产基本情况分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :param station_id:
    :param year:
    :param station_name:
    :return:
    """
    data = pd_query(FIND_PROBLEM_SQL.format(start_date, end_date, station_id))
    last_data = pd_query(FIND_PROBLEM_SQL.format(last_month_start, last_month_end, station_id))
    year_data = pd_query(
        FIND_PROBLEM_SQL.format(datetime.datetime.strptime(str(year) + '-01-01', '%Y-%m-%d').date(), end_date,
                                station_id))
    # 图表
    year_data['NEW_DATE'] = year_data['OCCURRENCE_TIME'].apply(lambda x: int(str(x.date()).split('-')[1]))
    new_data = year_data[year_data['MAIN_TYPE'] == 2].groupby('NEW_DATE').count().sort_values(
        by='NEW_DATE').reset_index()
    months = new_data['NEW_DATE'].tolist()
    count = new_data['OCCURRENCE_TIME'].tolist()
    for i in range(len(months)):
        months[i] = str(months[i]) + '月'
    images = bar_picture([count], months, ['当月故障件数'], station_name + '安全信息数量统计表', times+'月份', '电务',
                         'quarterly')
    num = []
    a = 0
    b = 0
    for i in ['道岔设备', '轨道电路', '车载设备', '电源设备']:
        accident_data = data[data['RISK_NAME'].str.contains(i)]
        dic = {
            'name': i,
            'count': len(accident_data)
        }
        a = a + len(accident_data)
        b = b + len(last_data[last_data['RISK_NAME'].str.contains(i)])
        num.append(dic)
    sorted(num, key=lambda x: x['count'])
    result = a - b
    return {
        'images': images,
        'num': num,
        'result': result
    }


# ------------------------------------------------------------------------第二部分--------------------------------------
def get_check_base_doc(start_date, end_date, last_month_start, last_month_end, station_id, station_name, times, month):
    """
    检查基本情况统计分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :param station_id:
    :param station_name:
    :param month:
    :return:
    """
    last_month = str(int(str(last_month_start).split('-')[1]))
    data = pd_query(CHECK_INFO_PROBLEM_SQL.format(start_date, end_date, station_id))
    last_data = pd_query(CHECK_INFO_PROBLEM_SQL.format(last_month_start, last_month_end, station_id))
    # 检查基本情况
    check_base_situation = get_check_base_situation(data, last_data, month, last_month, station_name, times)
    # 问题统计分析
    problem_analysis = get_problem_analysis(data, month, station_name, times)
    # 问题“三性”分析
    problem_three_analysis = get_problem_three_analysis(data, station_name, times)
    return {
        'check_base_situation': check_base_situation,
        'problem_analysis': problem_analysis,
        'problem_three_analysis': problem_three_analysis
    }


def get_problem_three_analysis(data, station_name, times):
    """
    问题“三性”分析
    :param data:
    :param month:
    :param station_name:
    :return:
    """
    problem = data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PROBLEM_POINT',
                                                                        ascending=False).reset_index()
    problem_name = problem.loc[0]['PROBLEM_CLASSITY_NAME']
    name_list = problem['PROBLEM_CLASSITY_NAME'].tolist()
    problem_count = problem['PROBLEM_POINT'].tolist()
    problem_type = data.groupby('LEVEL').count().sort_values(by='PROBLEM_POINT', ascending=False).reset_index()
    level_type = problem_type['LEVEL'].tolist()
    level_count = problem_type['PROBLEM_POINT'].tolist()
    problem_type_name = problem_type.loc[0]['LEVEL']
    # 绘图
    images = hentail_pie_picture(level_count, problem_count, level_type, name_list, '问题分项统计',
                                times + '月份' + station_name, '电务', 'quarterly')
    return {
        'problem_name': problem_name,
        'problem_type_name': problem_type_name,
        'images': images
    }


def get_problem_analysis(data, month, station_name, times):
    """
    问题统计分析
    :param data:
    :param month:
    :param station_name:
    :return:
    """
    problem = data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PROBLEM_POINT').reset_index()
    problem_name = []
    problem_count = []
    for i in range(len(problem)):
        problem_name.append(problem.loc[i]['PROBLEM_CLASSITY_NAME'])
        problem_count.append([int(problem.loc[i]['PROBLEM_POINT'])])
    images = bar_picture(problem_count, [str(month) + '月'], problem_name, station_name + '问题归类统计', times+'月份', '电务',
                         'quarterly')
    return {
        'problem_name': problem_name,
        'images': images
    }


def get_check_base_situation(data, last_data, month, last_month, station_name, timess):
    """
    检查基本情况
    :param data:
    :param last_data:
    :param month:
    :param last_month:
    :param station_name:
    :return:
    """
    # 检查次数
    check_count = len(data)
    # 现场检查
    xc_check = len(data[data['CHECK_WAY'] == 1])
    last_xc = len(last_data[last_data['CHECK_WAY'] == 1])
    xc_ratio = calculate_month_and_ring_ratio([xc_check, last_xc])
    # 音视频调阅检查
    mv_check = len(data[data['CHECK_ITEM_NAME'].str.contains('音视频')])
    last_mv = len(last_data[last_data['CHECK_ITEM_NAME'].str.contains('音视频')])
    mv_ratio = calculate_month_and_ring_ratio([mv_check, last_mv])
    # 添乘检查
    tc_check = len(data[data['CHECK_WAY'] == 2])
    last_tc = len(last_data[last_data['CHECK_WAY'] == 2])
    tc_ratio = calculate_month_and_ring_ratio([tc_check, last_tc])
    # 绘图
    count1 = []
    count2 = []
    for i in ['机关', '车间']:
        new_data = data[data['DEPARTMENT_ALL_NAME'].str.contains(i)]
        xc = len(new_data[new_data['CHECK_WAY'] == 1])
        mv = len(new_data[new_data['CHECK_ITEM_NAME'].str.contains('音视频')])
        tc = len(new_data[new_data['CHECK_WAY'] == 2])
        count1.append([xc, mv, tc])
    for i in ['机关', '车间']:
        new_data = last_data[last_data['DEPARTMENT_ALL_NAME'].str.contains(i)]
        xc = len(new_data[new_data['CHECK_WAY'] == 1])
        mv = len(new_data[new_data['CHECK_ITEM_NAME'].str.contains('音视频')])
        tc = len(new_data[new_data['CHECK_WAY'] == 2])
        count2.append([xc, mv, tc])
    xc_count = [count2[0][0], count2[1][0], last_xc, count1[0][0], count1[1][0], xc_check]
    tc_count = [count2[0][2], count2[1][2], last_tc, count1[0][2], count1[1][2], tc_check]
    mv_count = [count2[0][1], count2[1][1], last_mv, count1[0][1], count1[1][1], mv_check]
    times = ['机关干部' + last_month + '月份', '车间干部' + last_month + '月份', last_month + '月份总计', '机关干部' + str(month) + '月份',
             '车间干部' + str(month) + '月份', str(month) + '月份总计']
    images = bar_picture1([tc_count, xc_count, mv_count], times, ['添乘', '现场', '音视频'],
                          station_name + '基本检查情况统计', timess+'月份', '电务', 'quarterly')
    return {
        'check_count': check_count,
        'xc_check': xc_check,
        'xc_ratio': xc_ratio,
        'tc_check': tc_check,
        'tc_ratio': tc_ratio,
        'mv_check': mv_check,
        'mv_ratio': mv_ratio,
        'images': images
    }


# ------------------------------------------------------------------------第三部分--------------------------------------
def get_manage_evaluate_problem(start_date, end_date, last_month_start, last_month_end, station_id):
    """
    干部履职情况简要统计分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :param station_id:
    :return:
    """
    data = pd_query(CHECK_EVALUATE_SCORE_PRODUCT_SQL.format(start_date, end_date, station_id))
    last_data = pd_query(CHECK_EVALUATE_SCORE_PRODUCT_SQL.format(last_month_start, last_month_end, station_id))
    year_data = pd_query(CHECK_EVALUATE_SCORE_PRODUCT_SQL.format(str(start_date[:4]) + '-01-01', end_date, station_id))
    # 干部履职问题类型统计表
    evaluate_problem_table = _get_evaluate_problem_table(data, last_data)
    # 干部履职突出问题记分类型统计表
    issue_problem_table = _get_issue_problem_table(data)
    # 累计计分统计
    score_table = _get_score_table(data, year_data)
    # 分析
    up = evaluate_problem_table['diff_list'].index(min(evaluate_problem_table['diff_list']))
    up_name = evaluate_problem_table['list1'][up]
    score = issue_problem_table['problem_ratio'].index(max(issue_problem_table['problem_ratio']))
    up_score = issue_problem_table['list1'][score]
    return {
        'evaluate_problem_table': evaluate_problem_table,
        'issue_problem_table': issue_problem_table,
        'score_table': score_table,
        'up_name': up_name,
        'up_score': up_score
    }


def _get_evaluate_problem_table(data, last_data):
    """
    干部履职问题类型统计表
    :param data:
    :param last_data:
    :return:
    """
    list1 = ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
             '履职评价管理', '事故故障追溯', '弄虚作假']
    data_list = []
    diff_list = []
    ratio_list = []
    all_count = len(data)
    for i in list1:
        if data[data['ITEM_NAME'] == i].empty:
            data_list.append(0)
            ratio_list.append(0)
            diff_list.append(0)
        else:
            data_list.append(len(data[data['ITEM_NAME'] == i]))
            ratio_list.append(round(ded_zero(len(data[data['ITEM_NAME'] == i]), all_count * 100), 1))
            if last_data[last_data['ITEM_NAME'] == i].empty:
                diff_list.append(
                    calculate_month_and_ring_ratio([len(data[data['ITEM_NAME'] == i]), 0])['month_percent'])
            else:
                diff_list.append(calculate_month_and_ring_ratio(
                    [len(data[data['ITEM_NAME'] == i]), len(last_data[last_data['ITEM_NAME'] == i])])[
                                     'month_percent'])
    return {
        'list1': list1,
        'data_list': data_list,
        'ratio_list': ratio_list,
        'diff_list': diff_list
    }


def _get_issue_problem_table(data):
    """
    干部履职突出问题记分类型统计表
    :param data:
    :return:
    """
    list1 = ['专业管理重点落实不到位。', '日常重点安全工作落实不到位。', '倒查重点问题，检查质量低下。', '监控调阅检查不认真。', '问题整改督促不力。']
    problem_number = []
    problem_ratio = []
    for i in list1:
        if data[data['SITUATION'] == i].empty:
            problem_number.append(0)
            problem_ratio.append(0)
        else:
            problem_number.append(len(data[data['SITUATION'] == i]))
            problem_ratio.append(round(ded_zero(len(data[data['SITUATION'] == i]), len(data)) * 100, 1))
    return {
        'list1': list1,
        'problem_number': problem_number,
        'problem_ratio': problem_ratio
    }


def _get_score_table(data, last_data):
    """
    计分统计表
    :param data:
    :return:
    """
    score_list = []
    for i in [data, last_data]:
        score = [0, 0, 0, 0, 0, 0, 0]
        for j in i['SCORE']:
            if j < 2:
                score[0] += 1
            elif j < 4:
                score[1] += 1
            elif j < 6:
                score[2] += 1
            elif j < 8:
                score[3] += 1
            elif j < 10:
                score[4] += 1
            elif j < 12:
                score[5] += 1
            else:
                score[6] += 1
        score_list.append(score)
    return {
        'score_list': score_list
    }


# ------------------------------------------------------------------------第四部分--------------------------------------
def get_safety_low_department_analysis(start_date, end_date, station_name, month, times):
    """
    近期安全管理持续薄弱科室、车间情况分析
    :param start_date:
    :param end_date:
    :param station_name:
    :param month:
    :return:
    """
    data = pd_query(CHECK_CHEJIAN_SQL.format(start_date, end_date, station_name))
    # 上级检查次数
    count = len(data)
    # 上级检查发现问题
    number = int(data['PROBLEM_NUMBER'].sum())
    data['STATION_NAME'] = data[data['DEPARTMENT_ALL_NAME'].str.contains('车间')]['DEPARTMENT_ALL_NAME'].apply(
        lambda x: x.split('-')[1])
    new_data = data.groupby('STATION_NAME').count().sort_values(by='PROBLEM_NUMBER').reset_index()
    name = new_data.loc[0]['STATION_NAME']
    # 车间自身检查次数
    station_count = int(new_data.loc[0]['PROBLEM_NUMBER'])
    station_data = data[data['DEPARTMENT_ALL_NAME'].str.contains(name)]
    # 车间自检发现问题
    station_num = int(station_data['PROBLEM_NUMBER'].sum())
    # 故障问题数
    shigu = len(station_data[station_data['CHECK_TYPE'] == 202])
    # 绘图
    images = bar_picture([[count], [station_count], [number], [station_num]], [str(month) + '月'],
                         ['上级检查次数', '车间检查次数', '上级发现问题数', '车间发现问题数', '故障问题数'], '检查数据分析',
                         times + '月份' + station_name, '电务', 'quarterly')
    return {
        'count': count,
        'number': number,
        'name': name,
        'station_count': station_count,
        'station_num': station_num,
        'shigu': shigu,
        'images': images
    }


# ------------------------------------------------------------------------第五部分--------------------------------------
def get_mv_situation(start_date, end_date, station_id):
    """
    音视频运用情况分析
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    data = pd_query(CHECK_MV_SITUATION_SQL.format(start_date, end_date, station_id))
    new_data = data[data['CHECK_ITEM_NAME'].str.contains('音视频')]
    type = ['A', 'B', 'C', 'D', 'E', 'F']
    num = []
    for i in type:
        num.append(len(new_data[new_data['LEVEL'].str.contains(i)]))
    return {
        'type': type,
        'num': num
    }


# ------------------------------------------------------------------------第六部分--------------------------------------
def get_sixth(year, start_month, end_month, station_id, station_name, times):
    # 责任统计数据
    iiilege_data = pd_query(CHECK_IIILEGE_SQL.format(year, start_month, end_month, station_id))
    iiilege_data = iiilege_data.dropna()
    iiilege_data['AGE'] = iiilege_data['BIRTHDAY'].apply(
        lambda x: (datetime.datetime.now().date() -
                   datetime.datetime.strptime(x[:4] + '-' + x[4:6] + '-' + x[6:], '%Y-%m-%d').date()).days // 365)
    # 绘图1
    new_data = iiilege_data.groupby('POSITION').count().reset_index()
    count = new_data['AGE'].tolist()
    name = new_data['POSITION'].tolist()
    images1 = bar_picture([count], name, ['问题数'], station_name + '责任岗位统计', times + '月份', '电务',
                          'quarterly')
    # 绘图2
    new_data1 = iiilege_data.groupby('AGE').count().reset_index()
    count1 = [0, 0, 0, 0, 0]
    for i in range(len(new_data1)):
        if 20 < new_data1.iloc[i]['AGE'] <= 30:
            count1[0] += new_data1.iloc[i]['POSITION']
        elif 30 < new_data1.iloc[i]['AGE'] <= 40:
            count1[1] += new_data1.iloc[i]['POSITION']
        elif 40 < new_data1.iloc[i]['AGE'] <= 50:
            count1[2] += new_data1.iloc[i]['POSITION']
        elif 50 < new_data1.iloc[i]['AGE'] <= 60:
            count1[3] += new_data1.iloc[i]['POSITION']
        else:
            count1[4] += new_data1.iloc[i]['POSITION']
    name1 = ['20~30', '30~40', '40~50', '50~60', '其他']
    images2 = bar_picture([count1], name1, ['人次'], station_name + '责任层次统计', times + '月份', '电务',
                          'quarterly')
    return {
        'images1': images1,
        'images2': images2
    }


# ------------------------------------------------------------------------第七部分--------------------------------------
def get_safety_center_work(year, start_month, end_month, station_id):
    """
    分析中心主要工作质量分析
    :param year:
    :param start_month:
    :param end_month:
    :param station_id:
    :return:
    """
    data = pd_query(SAFEFY_CENTER_ANALYSIS_SQL.format(year, start_month, end_month, station_id))
    # 音视频调阅时长
    monitor_time = int(data['MONITOR_TIME'].sum())
    # 复查时长
    monitor_review_time = int(data['MONITOR_REVIEW_TIME'].sum())
    # 调阅发现问题数
    monitor_problem = int(data['MONITOR_PROBLEM'].sum())
    # 关键时段调阅次数
    key_number = int(data['MONITOR_KEY_PERIOD_NUMBER'].sum())
    # 调阅问题质量分
    monitor_problem_score = int(data['MONITOR_PROBLEM_SCORE'].sum())
    # 科职管理人员评价人数
    secco = int(data['SECCO_MANAGEMENT_QUANTITY'].sum())
    # 评价记分人数
    non = int(data['NON_MANAGEMENT_EVALUATE_REVIEW_NUMBER'].sum())
    general = int(data['GENERAL_AND_PROFESSIONAL_QUANTITY'].sum())
    count = secco + non + general
    # 阶段评价人次
    # 预警次数
    # 音视频调阅不认真评价人数
    not_serious = int(data['EVALUATE_NOT_SERIOUS_NUMBER'].sum())
    # 现场检查不认真评价人

    return {
        'monitor_time': monitor_time,
        'monitor_review_time': monitor_review_time,
        'monitor_problem': monitor_problem,
        'key_number': key_number,
        'monitor_problem_score': monitor_problem_score,
        'secco': secco,
        'count': count,
        "not_serious": not_serious,
    }


# -----------------------------------------------第八部分---------------------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第一张
    data['first']['picture'] = InlineImage(template, data['first']['images'], width=Mm(120))
    data['second']['check_base_situation']['picture'] = InlineImage(template, data['second']['check_base_situation'][
        'images'], width=Mm(120))
    data['second']['problem_analysis']['picture'] = InlineImage(template, data['second']['problem_analysis']['images'],
                                                                width=Mm(120))
    data['second']['problem_three_analysis']['picture'] = InlineImage(template,
                                                                      data['second']['problem_three_analysis'][
                                                                          'images'], width=Mm(120))
    data['fourth']['picture'] = InlineImage(template, data['fourth']['images'], width=Mm(120))
    data['sixth']['picture1'] = InlineImage(template, data['sixth']['images1'], width=Mm(120))
    data['sixth']['picture2'] = InlineImage(template, data['sixth']['images2'], width=Mm(120))
