#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    履职报告报表
'''
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from docxtpl import DocxTemplate
from flask import current_app, request
import xlwt

from app import mongo
# from .util import get_department_structrue
# from line_profiler import LineProfiler
from app.data.util import (get_previous_month,
                           pd_query)
from app.data.report.util import get_department_structrue
from app.utils.common_func import get_date, get_today
from app.report.util import get_report_list


def get_evaluate_report_list(start_time, end_time):
    '''获取履职报告列表'''
    title = '{year}年{month}月中国铁路成都局干部履职报告'
    TYPE = 'evaluate'
    reports = get_report_list(start_time, end_time, title, TYPE)
    return reports


def get_other_list(idcard, station,
                   mon, name=None, page=1, number=20):
    """根据权限获取可查看人员的报告列表

    Arguments:
        idcard {str} -- 身份证ID
        station {str} -- 站段
        mon {int} -- 报告月份

    Keyword Arguments:
        name {str} -- 搜索的用户姓名 (default: {None})
        page {int} -- 分页查询当前页数 (default: {1})
        number {int} -- 每页查询数量 (default: {20})

    Returns:
        list -- 可查看人员的得分排名情况
    """
    global anjian_cards
    # 安监室权限最高的干部人员身份证id
    # anjian_cards = [
    #     '511026197303046417', '362502198110296851', '510125198403242311',
    #     '513825198711020615', '510625197612110019', '511024198011173133',
    #     '511024198611225216', '511025198407258818', '510106198611111819',
    #     '510681198312120317', '510781197503118739'
    # ]
    # niubi_cards = [
    #     '510106197908081417', '510125197409290010', '510802198209070513',
    #     '51012519840611581X', '510125198112131216', '511303198403160010',
    #     '513401197008231616', '513001197803090857', '530111198203024454',
    #     '520111198312165414', '510129198801173548', '511026197108194713',
    #     '51100219740501153X', '51102719761025487X', '622826198610041178',
    #     '510802198108253513', '510622197305220315', '510106198310261418',
    #     '511137197610211413', '510283198008120399', '620202198912260223',
    #     '51100219760618121X', '510781198801271816', '420105197302122036',
    #     '362502198110296851', '510781197503118739', '510106198611111819',
    #     '510625197612110019', '510125198403242311', '511025198407258818',
    #     '511024198611225216', '510681198312120317', '513825198711020615',
    #     '511024198011173133', '511026197303046417', '513401196310271614',
    #     '510106198409031014', '51102619740921022X', '513401197605041618',
    #     '320107197502013423', '510106197607211417'
    # ]

    niubi_cards = [
        "513401196905131610", "510106197503052132",
        "432524198203044537", "510106198409031014",
        "51102619740921022X", "320107197502013423",
        "510106197607211417", "513401196310271614",
        "511026197303046417", "510625197612110019",
    ]
    token = request.cookies.get('TOKEN').split('#')
    job_role = int(token[1])
    # 安监监察大队 及 安监-新政领导的人员权限判断 2019-09-26
    is_sec_mon = False
    if has_safety_mon_perm(idcard):
        is_sec_mon = True
    # End of 安监监察大队的人员权限判断
    if idcard in niubi_cards or is_sec_mon:
        if len(station) == 0:
            station = ''
        all_index, total = get_department_reports(
            idcard, station, mon, name, page, number, 0)
        departments = get_department_structrue()
    elif job_role == 0:
        major = token[-1].split('-')[0]
        if len(station) == 0:
            station = token[-2]
        all_index, total = get_department_reports(
            idcard, station, mon, name, page, number, job_role)
        departments = get_department_structrue(major, station)
    else:
        departments = {}
        all_index, total = get_department_reports(
            idcard, station, mon, name, page, number, job_role)
    result = {"data": all_index, "filter": departments, "total": total}
    return result


def validate_id_card(id_card):
    """检验是否具有履职报告下载权限"""
    # 权限人员
    id_card_dict= {
        "513401196905131610": "罗冠军", "510106197503052132": "张波",
        "432524198203044537": "肖斌", "510106198409031014": "程颉",
        "51102619740921022X": "周一平", "320107197502013423": "刘琪慧",
        "510106197607211417": "李健", "513401196310271614": "何庆平",
        "511026197303046417": "宁勇", "510625197612110019": "官小波",
    }
    person = mongo.db['base_person'].find_one({"ID_CARD": id_card}, {"_id": 0})

    if id_card_dict.get(id_card) or person["MAJOR"] == "局领导":
        return True
    if has_safety_mon_perm(id_card):
        return True
    return False


def has_safety_mon_perm(id_card):
    person = mongo.db['base_person'].find_one({"ID_CARD": id_card}, {"_id": 0})
    if person['MAJOR'] == '安监' and str(person['ALL_NAME']).startswith('安全监察大队'):
        return True
    if person['MAJOR'] == '安监' and str(person['ALL_NAME']).startswith('安监室-行政领导'):
        return True
    return False


def validate_evaluate_id_card(table_type):
    """干部履职统计权限"""
    # 干部履职排名-同类干部履职分析，干部履职总体分析，权限，人事处（有身份证号的），局领导，安监室（3人）
    # 干部履职排名-领导班子分析,局领导,管小波,  段内干部明细--官小波
    # 人事处id_card
    id_list = [
        "513401196905131610", "510106197503052132", "432524198203044537", "510106198409031014",
        "51102619740921022X", "320107197502013423", "510106197607211417"]
    anjian_card = ["513401196310271614", "511026197303046417", "510625197612110019"]
    token = request.cookies.get('TOKEN')
    if not token:
        return False
    permission_id_card = token.split('#')[0]
    person = mongo.db['base_person'].find_one({"ID_CARD": permission_id_card}, {"_id": 0})
    # 干部履职排名-同类干部履职分析，干部履职总体分析
    if table_type == ('first' or "four"):
        if permission_id_card in id_list + anjian_card or person.get("MAJOR") == "局领导":
            return True
        if has_safety_mon_perm(permission_id_card):
            return True
        return False
    # 段内干部明细
    elif table_type == "third":
        if permission_id_card == "510625197612110019":
            return True
        if has_safety_mon_perm(permission_id_card):
            return True
        return False
    # 领导班子
    elif table_type == "fifth":
        if permission_id_card == "510625197612110019" or person.get("MAJOR") == "局领导":
            return True
        if has_safety_mon_perm(permission_id_card):
            return True
        return False
    return True


def get_department_reports(idcard, station,
                           mon, name=None, page=1, number=20, job_role=1):
    '''获取领导下的部门相关人员报告'''
    # mon = int(date[:7].replace('/', ''))
    match = {"MON": mon, "TYPE": 0, "ID_CARD": {"$ne": idcard}}
    start = (page - 1) * number
    if len(station) != 0:
        match['ALL_NAME'] = {"$regex": station}
    if name is not None and name != '':
        match['NAME'] = name
    for prefix in ['monthly_', 'history_']:
        coll_name = f'{prefix}detail_evaluate_index'
        if page == 1 and (name is None or name == ''):
            _self = list(mongo.db[coll_name].find(
                {'ID_CARD': idcard, 'MON': mon}))
            number = number - len(_self)
        else:
            _self = []
        if job_role == 0:
            other_person = list(
                mongo.db[coll_name].find(match).skip(start).limit(number))
        else:
            other_person = []
        other_person = other_person + _self
        total = mongo.db[coll_name].find(match).count()
        if other_person:
            break
    other_reports = []
    other_data = pd.DataFrame(other_person)
    if other_data.empty:
        return [], 0
    other_data = other_data.drop_duplicates('ID_CARD')
    for index in other_data.index:
        other = other_data.loc[index]
        if other['JOB'] == 'a':
            job = ''
        else:
            job = other['JOB']
        level = get_level(other['LEVEL'], other['GRADE'])
        level_rank = f"{int(other['level_rank'])}/{int(other['level_count'])}"
        job_rank = f'''{int(other['job_rank'])}/{int(other['job_count'])}'''
        content = {
            "ID_CARD": other['ID_CARD'],
            'department': other['DEPARTMENT'],
            'duty': job,
            'duty_rank': job_rank,
            'level': level,
            'level_rank': level_rank,
            'name': other['NAME'],
            'score': round(other['score'], 1)
        }
        if other['ID_CARD'] == idcard:
            other_reports.insert(0, content)
        else:
            other_reports.append(content)
    return other_reports, total


def get_check_info_doc(card, start, end):
    '''获取个人当月检查信息数据'''
    sql = """SELECT
                a.PK_ID,
                a.CHECK_WAY,
                a.CHECK_ITEM_NAMES,
                a.RISK_NAME,
                a.CHECK_ADDRESS_NAMES
            FROM
                t_check_info AS a
                LEFT JOIN t_check_info_and_person AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
            WHERE
                b.ID_CARD = '{}'
                AND a.SUBMIT_TIME >= '{}'
                AND a.SUBMIT_TIME < '{}'"""
    data = pd_query(sql.format(card, start, end))
    return data


def get_check_problem_doc(card, month):
    '''获取个人当月检查问题数据'''
    prefixs = ['history_', 'monthly_', 'daily']
    pro_doc = []
    for prefix in prefixs:
        problem_coll_name = f'{prefix}detail_check_problem'
        proDoc = list(mongo.db[problem_coll_name].find(
            {
                "CHECK_ID_CARD": card,
                "MON": month
            }, {
                "_id": 0,
                "PK_ID": 1,
                "RISK_LEVEL": 1,
                "RISK_NAMES": 1,
                "RISK_NAME": 1
            }))
        pro_doc = pro_doc + proDoc
    return pro_doc


def get_person_work_data(card):
    '''获取个人分工检查项目信息'''
    sql = '''SELECT
        b.CHECK_ITEM_NAME, b.TYPE
    FROM
        t_personal_division AS a
            LEFT JOIN
        t_personal_division_check_item AS b \
                    ON b.FK_PERSONAL_DIVISION_ID = a.PK_ID
    WHERE
        a.ID_CARD = '{}' and a.STATUS = 4
    '''.format(card)
    data = pd_query(sql)
    data = data[~data['CHECK_ITEM_NAME'].isnull()]
    return data


def get_diff_per(score, aver):
    """计算本人得分和平均分差值和百分比

    Arguments:
        _index {dict} -- 安全履职得分信息
        column {str} -- 平均分的列名
    """
    if aver > 0:
        diff = round(score - aver, 1)
        per = round(diff / aver * 100, 1)
        if diff >= 0:
            status_1 = '高'
            status_2 = '多'
        else:
            status_1 = '低'
            status_2 = '少'
        rst = f'比平均分{status_1}{abs(diff)}分，{status_2}{abs(per)}%'
    else:
        rst = f'比平均分多{score}分'
    return rst


def get_level(level, grade):

    if grade != 0:
        rst = f'''{level}{int(grade)}档'''
    else:
        rst = f'''{level}?档'''
    return rst


def get_base_content(index, month):
    """拼接履职分析报告第一部分整体情况

    Arguments:
        index {dict} -- 安全履职得分信息

    Returns:
        [list] -- 第一部分履职报告的两个自然段
    """
    content = []
    job_rank = ''
    for _index in index:  # 因为有多个职务分类情况存在，所以取得的index是list
        name = _index['NAME']
        depart = _index['ALL_NAME']
        position = _index.get('POSITION', '一般员工')
        score = round(_index['score'], 1)
        major = _index['MAJOR']
        level = get_level(_index['LEVEL'], _index['GRADE'])
        level_rank = (f'''{int(_index['level_rank'])}名'''
                      f'''/{int(_index['level_count'])}名''')
        level_aver = round(_index['level_aver'], 1)
        level_rst = get_diff_per(score, level_aver)
        posi_rank = int(_index['position_rank'])
        posi_count = int(_index['position_count'])
        posi_aver = round(_index['position_aver'], 1)
        department = _index['DEPARTMENT_CLASS']
        if department == 0:
            department = '?'
        posi_rst = get_diff_per(score, posi_aver)
        posi_rst = (f'在{department}类科室{position}中排{posi_rank}名/'
                    f'{posi_count}名(平均分为{posi_aver}分，{posi_rst})。')
        if _index['JOB'] != 'a':
            job_aver = round(_index['job_aver'], 1)
            job_rst = get_diff_per(score, job_aver)
            job_rst = (f'''在{_index['JOB']}类干部中排{int(_index['job_rank'])}'''
                       f'''名/{int(_index['job_count'])}名({_index['JOB']}'''
                       f'''类干部平均分为{job_aver}分，{job_rst})，''')
            job_rank = job_rank + job_rst
    # 判断传入是年份还是月份
    if 2017 < month < 9999:
        date = f'{month}年份'
    else:
        date = f'{month%100}月份'
    first = (f'{name}同志为{depart}{position}，{date}，安全履职得分为'
             f'{score}分。在{major}专业{level}干部中排{level_rank}({level}干部安全'
             f'为{level_aver}分，{level_rst})，{job_rank}{posi_rst}')
    content.append(first)
    # 第二部分内容
    dalao = ['正局级', '副局级', '正处级', '副处级']
    _index = index[0]
    if _index['LEVEL'] not in dalao:
        station_level_rank = (f'''{int(_index['station_level_rank'])}名'''
                              f'''/{int(_index['station_level_count'])}名''')
        station_level_aver = round(_index['station_level_aver'], 1)
        station_level_rst = get_diff_per(score, station_level_aver)
        station_rank = int(_index['station_rank'])
        station_count = int(_index['station_count'])
        station_aver = round(_index['station_aver'], 1)
        station_rst = get_diff_per(score, station_aver)
        second = (f'在本单位{level}干部中排{station_level_rank}({level}干部'
                  f'安全履职平均分为{station_level_aver}, {station_level_rst})。'
                  f'在全段干部中排{station_rank}名/{station_count}名（平均分为'
                  f'{station_aver}，{station_rst}）。')
        content.append(second)
    result = '\n'.join(content)
    return result


def get_info_content(index, card, month):
    # 判断传入是年份还是月份
    if month % 100 != 0:
        end_time = dt(month // 100, month % 100, 25)
        start_time = end_time - relativedelta(months=1)
        date = '本月'
    else:
        end_time = dt(month, 12, 25)
        start_time = dt(month, 1, 25)
        date = '本年'
    data = get_check_info_doc(card, start_time, end_time)
    if data.empty:
        return {'base_status': f'{date}共检查0次，安全检查得分为0分。',
                'check_table': {}}
    # data = pd.DataFrame(check_doc)
    # data = data.drop_duplicates(['PK_ID'])
    count = len(data)
    xian = len(data[data['CHECK_WAY'] == 1])  # 现场检查
    tian = len(data[data['CHECK_WAY'] == 2])  # 添乘检查
    jian = len(data[data['CHECK_WAY'] == 3])  # 监控调阅检查
    level = get_level(index['LEVEL'], index['GRADE'])
    score = round(index['check_score'], 1)
    aver = round(index['check_aver'], 1)
    diff = round(score - aver, 1)
    if diff >= 0:
        status = '高'
    else:
        status = '低'
    check_status = (f'{date}共检查{count}次，安全检查得分为{score}（{level}平均得分'
                    f'{aver}，{status}于平均分{abs(diff)}分），其中现场检查{xian}'
                    f'次，添乘检查{tian}次，监控调阅检查{jian}次。')
    # 计算检查情况内容
    # 检查内容计数
    item_data = data[['PK_ID', 'CHECK_ITEM_NAMES']]
    item_data = item_data.set_index(['PK_ID'])['CHECK_ITEM_NAMES'].str.split(
        ',', expand=True).stack().reset_index(
            level=1, drop=True).reset_index(name='CHECK_ITEM')
    item_data = item_data.groupby('CHECK_ITEM').size()
    item_name = list(item_data.index)
    item_count = list(item_data)
    # 排查风险计数
    risk_data = data[['PK_ID', 'RISK_NAME']]
    risk_data = risk_data.set_index(['PK_ID'])[
        'RISK_NAME'].str.split(',', expand=True).stack().reset_index(
            level=1, drop=True).reset_index(name='RISK_NAME')
    risk_data = risk_data[risk_data['RISK_NAME'] != '']
    risk_data = risk_data.groupby('RISK_NAME').size()
    risk_name = list(risk_data.index)
    risk_count = list(risk_data)
    # 检查地点计数
    address_data = data[['PK_ID', 'CHECK_ADDRESS_NAMES']]
    address_data = address_data.set_index(['PK_ID'])[
        'CHECK_ADDRESS_NAMES'].str.split(',', expand=True).stack().reset_index(
            level=1, drop=True).reset_index(name='CHECK_ADDRESS')
    address_data = address_data[~address_data['CHECK_ADDRESS'].isnull()]
    address_data = address_data.groupby('CHECK_ADDRESS').size()
    address_name = list(address_data.index)
    address_count = list(address_data)
    # 未检查项目
    work_data = get_person_work_data(card)
    if work_data.empty:
        no_items = []
    else:
        work_doc = list(work_data['CHECK_ITEM_NAME'])
        no_items = [item for item in work_doc if item not in item_name]
        # 最终结果
    rst = {'base_status': check_status,
           'check_table': {
               'check_content': {'name': item_name, 'count': item_count},
               'risk': {'name': risk_name, 'count': risk_count},
               'address': {'name': address_name, 'count': address_count},
               'no_item': no_items
           }}
    return rst


def get_problem_content(index, card, month):
    """拼接安全履职报告第三部分：发现问题情况

    Arguments:
        index {dict} -- 安全履职得分信息
        card {str} -- 个人身份证
        month {int or dict} -- 月份信息

    Returns:
        str -- 报告第三段
    """
    # 判断传入是年份还是月份
    if isinstance(month, int):
        date = '本月'
    else:
        date = '本年'
    score = index['problem_score']
    level = get_level(index['LEVEL'], index['GRADE'])
    aver = round(index['problem_aver'], 1)
    diff = round(score - aver, 1)
    if diff >= 0:
        status = '高'
    else:
        status = '低'
    quality = index.get('problem_quality_score', 0)
    assess = index.get('problem_assess_score', 0)
    pro_doc = get_check_problem_doc(card, month)
    data = pd.DataFrame(pro_doc)
    data = data.drop_duplicates(['PK_ID', 'RISK_NAMES'])
    total = len(data)
    zhong = len(data[data['RISK_LEVEL'] == 1])
    jiao = len(data[data['RISK_LEVEL'] == 2])
    yi = len(data[data['RISK_LEVEL'] == 3])
    di = len(data[data['RISK_LEVEL'] == 4])
    data = data.groupby('RISK_NAMES').size().reset_index()
    data['count'] = data[0]
    content = f''
    risks = ''
    for index in data.index:
        risk = data.at[index, 'RISK_NAMES']
        count = data.at[index, 'count']
        risks = risks + f'{risk}{count}个，'
    risks = risks[:-1]
    content = (f'{date}共发现{total}个问题，安全问题得分为{score}分（{level}平均'
               f'得分{aver}，{status}于平均分{abs(diff)}分），其中问题质量得分{quality}，问题考核'
               f'得分{assess}，发现重大安全风险问题{zhong}个，较大安全风险问题{jiao}个'
               f'，一般安全风险问题{yi}个，低安全风险问题{di}个。本月排查出{risks}。')
    return content


def get_evaluate_content(index, month):
    """报告第四部分：履职积分情况

    Arguments:
        index {dict} -- 个人安全得分信息

    Returns:
        str -- 履职记分情况内容
    """
    # 判断传入是年份还是月份
    if month > 9999:
        date = '本月'
    else:
        date = '本年'
    level = get_level(index['LEVEL'], index['GRADE'])
    points = index.get('POINTS', 0)
    count = index.get('COUNT', 0)
    score = index.get('evaluate_score', 0)
    aver = round(index.get('evaluate_aver', 0), 1)
    diff = round(score - aver, 1)
    if diff >= 0:
        status = '高'
    else:
        status = '低'
    content = (f'{date}被履职评价{count}条，共计{points}分，安全履职得分{score}分（'
               f'{level}平均得分{aver}，{status}于平均分{abs(diff)}分）。')
    return content


def get_talk_info(card, mon_point):
    '''获取安全谈心信息'''
    SQL = f'''SELECT
        ID_CARD,
        APPRAISAL_RESULT AS result
    FROM
        t_talk_safety_heart
    WHERE
        IS_DELETE = 0
            AND ID_CARD = '{card}'
            AND TALK_DATE >= '{mon_point[0]}'
            AND TALK_DATE <= '{mon_point[1]}';'''
    data = pd_query(SQL)
    data = data.fillna(0)
    return data


def get_talk_content(card, month, TYPE):
    if TYPE == 'month':
        mon = dt(month // 100, month % 100, 1)
        today = get_date(get_today())
        today = dt(today // 100, today % 100, 1)
        delta = round((mon - today).days / 30)
        if get_today().day == current_app.config.get('UPDATE_DAY', 25):
            delta -= 1
        mon_point = get_previous_month(delta)
        date = "本月"
    else:
        mon_point = [dt(month, 1, 1), dt(month + 1, 1, 1)]
        date = "本年"
    data = get_talk_info(card, mon_point)
    if len(data) == 0:
        return '无谈心任务。'
    else:
        if len(data) > 1:
            return f'{date}安全谈心被评为优。'
        else:
            x = data.at[0, 'result']
            if x == 1:
                return f'{date}安全谈心被评为差。'
            elif x == 2:
                return f'{date}安全谈心被评为良。'
            elif x == 3:
                return f'{date}安全谈心被评为优。'


def get_position_status(now_index, card, month):
    now_month = dt(month // 100, month % 100, 1)
    last_month = now_month - relativedelta(months=1)
    lastMonth = int(f'{last_month.year}{last_month.month:0>2}')
    for prefix in ['history_', 'monthly_']:
        coll_name = f'{prefix}detail_evaluate_index'
        last_index = list(mongo.db[coll_name].find({
            'ID_CARD': card,
            'MON': lastMonth
        }, {"_id": 0}))
    levels = ['级别', '岗位', '部门']
    nowIndex = now_index[0]
    now_positions = [nowIndex['LEVEL'],
                     nowIndex['POSITION'], nowIndex['ALL_NAME']]
    now_job = set([index['JOB'] for index in now_index])
    if len(last_index) == 0:
        return ''
    lastIndex = last_index[0]
    last_positions = [lastIndex['LEVEL'],
                      lastIndex['POSITION'], lastIndex['ALL_NAME']]
    last_job = set([index['JOB'] for index in last_index])
    if now_positions == last_positions and now_job == last_job:
        result = ''
    else:
        now_job = list(now_job)
        last_job = list(last_job)
        rst = []
        level_rst = []
        for idx, name in enumerate(now_positions):
            if name != last_positions[idx]:
                rst.append(last_positions[idx] + '变为了' + name)
                level_rst.append(levels[idx])
        for idx, name in enumerate(now_job):
            if name != last_job[idx]:
                rst.append(last_job[idx] + '变为了' + name)
                level_rst.append('职务分类')
        rst = ','.join(rst)
        level_rst = ','.join(level_rst)
        result = f'{month%100}月{level_rst}发生了变化（{rst}）'
    return result


def get_assess_status(card, month):
    sql = f'''SELECT
            b.ID_CARD,
            b.PERSON_NAME,
            b.TOTAL_MONEY,
            b.ACTUAL_MONEY,
            b.REMARK
        FROM
            t_safety_assess_month AS a
                LEFT JOIN
            t_safety_assess_month_quantify_detail AS b
                ON b.FK_SAFETY_ASSESS_MONTH_ID = a.PK_ID
        WHERE
            a.YEAR = {month//100} AND a.month = {month%100}
                AND b.ID_CARD='{card}' '''
    data = pd_query(sql)
    if data.empty:
        return ''
    else:
        content = []
        for index in data.index:
            content.append(data.at[index, 'REMARK'])
        remark = '、'.join(content)
        return f'{month%100}月因{remark}进行了核减'


def get_other_content(index, card, month, TYPE):
    result = []
    if TYPE == 'year':
        months = [int(f'{month}{mon:0>2}') for mon in range(1, 13)]
    else:
        months = [month]
    position = []
    assess = []
    for mon in months:
        posi = get_position_status(index, card, mon)
        if posi != '':
            position.append(posi)
        asse = get_assess_status(card, mon)
        if asse != '':
            assess.append(asse)
    position_rst = ';'.join(position)
    assess_rst = ';'.join(assess)
    if len(position_rst) >= 10:
        result.append('岗位变化情况：' + position_rst)
    if len(assess_rst) >= 10:
        result.append('量化核减情况: ' + assess_rst)
    result = '\n'.join(result)
    return result


def get_detail_report_content(index, card, month, TYPE):
    '''获取月份履职报告'''
    if TYPE == 'year':
        mon = {'$gt': month * 100, '$lt': (month + 1) * 100}
        date = '本年'
    else:
        mon = month
        date = '本月'
    # 第一部分内容：整体情况
    first = get_base_content(index, month)
    _index = index[0]
    # 第二部分内容：安全检查情况
    second = get_info_content(_index, card, month)
    # 第三部分内容：发现问题情况
    if _index.get('problem_score') == 0:
        third = f'{date}共发现0个问题，安全得分为0分。'
    else:
        third = get_problem_content(_index, card, mon)
    # 第四部分内容：履职积分情况
    fourth = get_evaluate_content(_index, month)
    # 第五部分内容：安全谈心情况
    fifth = get_talk_content(card, month, TYPE)
    # 第六部分内容：其他情况
    sixth = get_other_content(index, card, month, TYPE)
    data = [first, second, third, fourth, fifth, sixth]
    return data


def get_report(card, month, TYPE):
    '''获取履职报告内容'''

    if TYPE == 'month':
        match = {'ID_CARD': card, 'MON': month, 'TYPE': 0}
        date = f'{month%100}月份'
    else:
        month = month // 100
        date = f'{month}年'
        match = {'ID_CARD': card, 'YEAR': month, 'TYPE': 1}
    for prefix in ['history_', 'monthly_']:
        coll_name = f'{prefix}detail_evaluate_index'
        evaluate_index = list(mongo.db[coll_name].find(match, {"_id": 0}))
    if len(evaluate_index) == 0:
        name_data = pd_query(
            f"select PERSON_NAME FROM t_person WHERE ID_CARD = '{card}'")
        name = name_data.at[0, 'PERSON_NAME']
        result = {'info': f'{name}同志无当前日期履职报告', 'data': []}
    else:
        name = evaluate_index[0]['NAME']
        info = f'{name}{date}安全履职分析报告'
        data = get_detail_report_content(evaluate_index, card, month, TYPE)
        result = {'info': info, 'data': data}
    return result


def get_download_reports(idcard, major, station, mon):
    '''获取领导下的部门相关人员报告'''
    match = {"MON": mon, "TYPE": 0}
    if major != '':
        match['MAJOR'] = major
    if station != '':
        match['ALL_NAME'] = {"$regex": station}
    for prefix in ['monthly_', 'history_']:
        coll_name = f'{prefix}detail_evaluate_index'
        other_person = list(mongo.db[coll_name].find(match))
        if other_person:
            break
    other_reports = []
    other_data = pd.DataFrame(other_person)
    if other_data.empty:
        return []
    other_data = other_data.drop_duplicates('ID_CARD')
    for index in other_data.index:
        value = other_data.loc[index]
        grade = int(value['GRADE'])
        if grade == 0:
            grade = '?'
        # content = {
        #     'major': value['MAJOR'],
        #     'department': value['DEPARTMENT'],
        #     'name': value['NAME'],
        #     'level': value['LEVEL'],
        #     'grade': value['GRADE'],
        #     'score': round(value['score'], 1)
        # }
        content = [value['MAJOR'], value['ALL_NAME'], value['NAME'],
                   value['LEVEL'], grade, round(value['score'], 1)]
        if value['ID_CARD'] == idcard:
            other_reports.insert(0, content)
        else:
            other_reports.append(content)
    return other_reports


def save_evaluate(id_card, month, major, station, file_path):
    '''生成履职得分统计表'''
    wb = xlwt.Workbook(encoding='ascill')
    if major == '':
        sheet_name = '全局'
    else:
        sheet_name = major
        if station != '':
            sheet_name = sheet_name + '_' + station
    wt = wb.add_sheet(sheet_name)
    row = 0
    titles = ['序号', '专业', '部门', '姓名', '级别', '档数', '履职得分']
    for idx, title in enumerate(titles):
        wt.write(row, idx, title)
    contents = get_download_reports(id_card, major, station, month)
    if contents:
        for idx, content in enumerate(contents):
            row += 1
            wt.write(row, 0, idx + 1)
            for index, val in enumerate(content):
                wt.write(row, int(index) + 1, val)
    wb.save(file_path)
    return True


def get_evaluate_data(id_card, month, mon, name, evaluate_type):
    prefix_dict = {"history": 'history_', 'month': 'monthly_'}
    prefix = prefix_dict.get(evaluate_type, 0)
    match = {'ID_CARD': id_card, 'MON': month, 'TYPE': 0}
    coll_name = f'{prefix}detail_evaluate_index'
    data = list(mongo.db[coll_name].find(match, {"_id": 0}))
    data = get_detail_report_content(data, id_card, month, evaluate_type)

    first = data[0]
    # first.resplace('')

    second = data[1]
    if second.get("check_table"):
        check_content = second.get("check_table", {}).get("check_content", {})
        risk = second.get("check_table", {}).get("risk", {})
        address = second.get("check_table", {}).get("address", {})
        no_item = second.get("check_table", {}).get("no_item", {})

        max_lenth = max(len(check_content.get("name", [])),
                        len(risk.get("name", [])),
                        len(address.get("name", [])),
                        len(no_item),
                        )
        check_table = []
        second = {"base_status": second["base_status"],
                  "check_table": check_table
                  }

        item_dict = {
            "check_content": check_content,
            "risk": risk,
            "address": address,
            "no_item": no_item,
        }
        for i in range(max_lenth):
            row_dict = {}
            for item_name, item in item_dict.items():
                # no_item为列表
                if item_name == 'no_item':
                    if item and i < len(item):
                        row_dict[item_name] = {"name": item[i]}

                else:
                    if i < len(item.get("name", [])):
                        row_dict[item_name] = {"name": item["name"][i],
                                               "count": item["count"][i]}
            check_table.append(row_dict)
    else:
        second = {
            "base_status": second.get("base_status"),
            "check_table": second.get("check_table")
        }

    data_dict = {
                 "name": name, "mon": mon,
                 "first": data[0], "second": second,
                 "third": data[2], "fourth": data[3],
                 "fifth": data[4], "sixth": data[5]}

    return data_dict


def export_evaluate_word(id_card, month, mon, name, evaluate_type, file_path):
    """生成履职报告数据"""
    tpl = DocxTemplate('app/report/evaluate/evaluate_report_template.docx')
    result = get_evaluate_data(id_card, month, mon, name, evaluate_type)
    tpl.render(result)
    tpl.save(file_path)
    return True
    # return result


if __name__ == '__main__':
    pass
