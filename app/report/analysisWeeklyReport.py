from datetime import datetime as dt

from dateutil.relativedelta import relativedelta
from docxtpl import DocxTemplate

from app import mongo
from app.report.analysisWeeklyReport_wb import (
    get_four_typical_problem_analysis, get_second_safe_big_data)

from app.report.analysisWeeklyReport_cyf import get_safety_basic_situation
from app.report.util import get_weekly_report_list


def get_analysis_weekly_list(start_time, end_time):
    title = '{start}至{end}集团公司周安全分析报告'
    TYPE = 'weekly_analysis'
    reports = get_weekly_report_list(start_time, end_time, title, TYPE)
    return reports


def save_weekly_report(start_date, end_date, file_path):
    """集团公司安全情况月度分析报告
    Arguments:
        month {int} -- 年份月份信息
    Returns:
        str -- 是否保存成功
    """
    tpl = DocxTemplate(
        'app/report/assess/analysis_weekly_report_template.docx')
    # 第一部分
    result = get_weekly_analysis_data(start_date, end_date)
    # result = get_data(start_date, end_date)
    tpl.render(result)
    tpl.save(file_path)
    return True


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    first_basic_situation = get_safety_basic_situation(start_date, end_date)
    second_first = get_second_safe_big_data(start_date, end_date,
                                            last_week_start, last_week_end)
    fourth = get_four_typical_problem_analysis(start_date, end_date)
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "first_basic_situation": first_basic_situation,
        "second_safe_big_data_analysis": second_first,
        "fourth_typical_problem_analysis": fourth
    }
    return result


def get_weekly_analysis_data(start_date, end_date):
    """获取分析报告数据入口

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- 当月报告所有数据
    """
    result = mongo.db.detail_analysis_weekly_report.find_one(
        {
            'start_date': start_date,
            'end_date': end_date
        }, {'_id': 0})
    if result is None:
        result = get_data(start_date, end_date)
        mongo.db.detail_analysis_weekly_report.insert_one(result)
    return result
