# 机务某站段的所有数据
CHECK_PROBLEM_INFO_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,a.IS_YECHA,b.RISK_LEVEL,b.IS_ASSESS,\
b.`LEVEL` ,b.PROBLEM_CLASSITY_NAME,c.ALL_NAME,b.RISK_NAMES,b.PROBLEM_SCORE,a.CHECK_TYPE
from t_check_info a
LEFT JOIN t_check_problem b on a.PK_ID = b.FK_CHECK_INFO_ID 
LEFT JOIN t_department c on b.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}' 
and c.TYPE3 = '{2}'"""
# 检查覆盖率的数据
CHECK_COVER_RATIO_SQL = """SELECT a.DEPARTMENT_ALL_NAME,d.TYPE from t_check_info a
LEFT JOIN t_check_problem b on a.PK_ID = b.FK_CHECK_INFO_ID
LEFT JOIN t_department c on b.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_check_info_and_address d on a.PK_ID=d.FK_CHECK_INFO_ID
where c.TYPE3 = '{2}'
and a.SUBMIT_TIME>='{0}' and a.SUBMIT_TIME<='{1}'"""
# 所有站段名
ALL_STATION_NAME_SQL = """select ALL_NAME FROM t_department where TYPE3 = '{0}'"""
# 量化干部录入问题查询
ALL_MANAGER_INTO_PROBLEM_SQL = """SELECT b.FK_CHECK_OR_PROBLEM_ID,c.ALL_NAME,d.DEPARTMENT_ALL_NAME,d.PROBLEM_NUMBER,
d.CHECK_WAY,d.IS_YECHA,a.IS_ASSESS,b.LEVEL,d.CHECK_TYPE
from t_check_problem a
LEFT JOIN t_check_evaluate_info b on a.ID_CARD = b.CHECK_PERSON_ID_CARD
LEFT JOIN t_department c on a.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_check_info d on a.FK_CHECK_INFO_ID = d.PK_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and c.TYPE3 = '{2}'"""
# 上级部门录入查询
TOP_DEPARTMENT_SQL = """SELECT PROBLEM_CLASSITY_NAME,`LEVEL` from t_check_problem where EXECUTE_DEPARTMENT_NAME like '%%机务%%' 
and SUBMIT_TIME BETWEEN '{0}' and '{1}'"""
# 检查通知书
CHECK_BOOK_SQL = """SELECT a.CHECK_NOTIFICATION_TIMES from t_quantization_base_quota a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
where b.TYPE3 = '{3}'
and a.`YEAR` ={0} and a.`MONTH` BETWEEN {1} and {2}
"""

CHECK_EVALUATE_PROBLEM_SITUATION = """SELECT
        a.GRADATION,
        a.RESPONSIBE_PERSON_NAME,
        a.SCORE,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME
FROM t_check_evaluate_info a
LEFT JOIN t_check_evaluate_config b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
LEFT JOIN t_department c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
WHERE a.CREATE_TIME >= '{0}' AND a.CREATE_TIME <= '{1}'
and c.TYPE3 = '{2}'"""

CHECK_SITUATION_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,b.`LEVEL`,
b.PROBLEM_CLASSITY_NAME,b.RISK_NAMES
from t_check_info a
LEFT JOIN t_check_problem b on a.PK_ID = b.FK_CHECK_INFO_ID 
LEFT JOIN t_department c on b.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}' 
and c.TYPE3 = '{2}'
"""

CHECK_MAIN_PROBLEM_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.CHECK_WAY,a.IS_DONGCHE,a.IS_KECHE,c.`NAME`,a.CHECK_ADDRESS_NAMES
from t_check_info a
LEFT JOIN t_check_info_and_item b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_item c on c.PK_ID= b.FK_CHECK_ITEM_ID
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and d.TYPE3='{2}'"""

CHECK_SAFETY_ISSUE_SQL = """SELECT b.RISK_NAMES,c.ALL_NAME from t_check_info a
LEFT JOIN t_check_problem b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_department c on b.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
where c.TYPE3='{2}'
and a.SUBMIT_TIME BETWEEN '{0}' and '{1}'"""

CHECK_SAFETY_DEPARTMENT = """SELECT a.TYPE,b.ALL_NAME,a.RISK_TYPE_NAMES from t_safety_peril_lib a,t_department b 
WHERE a.SUBMIT_TIME  BETWEEN '{0}' and '{1}'
and a.RESPONSIBILITY_DEPARTMENT_IDS = b.DEPARTMENT_ID and b.TYPE3 = '{2}'"""

CHECK_IIIEGE_SQL = """SELECT a.PERSON_NAME,b.POSITION,a.DEDUCT_SCORE,c.ALL_NAME,a.VIOLATION_NUMBER,b.BIRTHDAY
from t_warning_key_person a
LEFT JOIN t_person b on a.ID_CARD = b.ID_CARD
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
where a.WARN_START_DATE BETWEEN '{0}' and '{1}'
and c.TYPE3 = '{2}'"""

CHECK_PERSON_SQL = """SELECT a.BIRTHDAY,b.ALL_NAME FROM `t_person` a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID=b.DEPARTMENT_ID
where b.TYPE3 = '{0}';"""

CHECK_SAFETY_ANALYSIS_CENTER_SQL = """SELECT a.EVALUATE_WAY,a.EVALUATE_TYPE,a.IS_REVIEW,c.CHECK_WAY,c.IS_EVALUATE from t_check_evaluate_check_person a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
LEFT JOIN t_check_info c on a.FK_CHECK_OR_PROBLEM_ID = c.PK_ID
where a.CREATE_TIME BETWEEN '{0}' and '{1}'
and b.TYPE3='{2}'"""

CHECK_RISK_PROBLEM_SQL = """
SELECT b.ALL_NAME,a.PROBLEM_POINT,a.RISK_NAMES,a.PROBLEM_CLASSITY_NAME,a.RISK_CONSEQUENCE_NAMES FROM `t_check_problem` a
LEFT JOIN t_department b on a.EXECUTE_DEPARTMENT_ID = b.DEPARTMENT_ID
where b.TYPE3='{2}' 
and a.SUBMIT_TIME BETWEEN '{0}' and '{1}'"""

without_accident_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
            where c.TYPE3 = '{}'
        """