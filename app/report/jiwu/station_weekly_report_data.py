from app.data.util import pd_query
import re
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from app.utils.common_func import get_department_name_by_dpid
import datetime
from app.report.jiwu.station_weekly_report_sql import *


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(start_date, end_date, station_id):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')  # 2019-04-27
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    station_name = get_department_name_by_dpid(station_id)
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_month = int(start_date[6:7])
    start_day = int(start_date[8:])
    end_month = int(end_date[6:7])
    end_day = end_date[8:]
    year = int(start_date[:4])
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_week_start, last_week_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_week_start, last_week_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 履职评价
    eva_data = pd_query(check_evaluate_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职复查
    eva_re_data = pd_query(check_evaluate_review_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 违章人员信息
    wz_data = pd_query(DUTY_PERSON_SQL.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()

    first = get_first(info_data, last_info, problem_data, last_problem)
    third = get_third(wz_data)
    fourth = get_fourth(problem_data, end_date)
    fifth = get_fifth(eva_data, eva_re_data)
    sixth = get_sixth(problem_data, last_problem)
    file_name = f'{start_date}至{end_date}{station_name}安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "hierarchy": "STATION",
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        'major': '机务',
        'station_id': station_id,
        'station_name': station_name,
        'created_at': dt.now(),
        'file_name': file_name,
        'first': first,
        'third': third,
        'four': fourth,
        'five': fifth,
        'six': sixth
    }
    return result


_PROBLEM_TYPE = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4'],
    '外部环境': ['G1', 'G2', 'G3', 'G4'],
    '反恐防暴': ['K1', 'K2', 'K3', 'K4'],
    '高质量': ['A', 'B', 'F1', 'F2', 'E1', 'E2']
}


def _get_ring_diff(now, last):
    """
    计算环比
    :param now:
    :param last:
    :return:
    """
    if last == 0:
        return {
            'diff': now,
            'percent': now * 100
        }
    else:
        return {
            'diff': now - last,
            'percent': round((now - last) / last * 100, 1)
        }


# ---------------------------------------------------------------第一部分-------------------------------------------------------------------------
def get_first(info_data, last_info, problem_data, last_problem):
    """
    基本检查情况
    :param info_data:
    :param last_info:
    :param problem_data:
    :param last_problem:
    :return:
    """
    # 检查信息
    check_info = get_check_info(info_data, last_info)
    # 干部检查信息
    gb_check_info = get_gb_check_info(info_data, last_info)
    # 现场检查问题情况
    problem_situation = get_problem_situation(problem_data, last_problem)
    # 调阅检查问题情况
    dy_problem = get_dy_problem_situation(problem_data, last_problem)
    # 现场+设备调阅
    table = _station_check_table(info_data, problem_data)
    return {
        'check_info': check_info,
        'gb_check_info': gb_check_info,
        'problem_situation': problem_situation,
        'table': table,
        'dy_problem': dy_problem
    }


def get_check_info(info_data, last_info):
    # 现场检查
    xc = len(info_data[info_data['CHECK_WAY'].isin([1, 2])])
    last_xc = len(last_info[last_info['CHECK_WAY'].isin([1, 2])])
    ratio1 = _get_ring_diff(xc, last_xc)
    # 职能科室检查
    znks = len(info_data[(info_data['CHECK_WAY'].isin([1, 2])) & (info_data['NAME'].str.contains('科'))])
    # 运用车间检查
    yycj = len(info_data[(info_data['CHECK_WAY'].isin([1, 2])) & (info_data['NAME'].str.contains('车间'))])
    # 设备调阅
    shebei = len(info_data[info_data['CHECK_WAY'].isin([3, 4])])
    last_shebei = len(last_info[last_info['CHECK_WAY'].isin([3, 4])])
    ratio2 = _get_ring_diff(shebei, last_shebei)
    # 职能科室检查
    znks_dy = len(info_data[(info_data['CHECK_WAY'].isin([3, 4])) & (info_data['NAME'].str.contains('科'))])
    # 运用车间检查
    yycj_dy = len(info_data[(info_data['CHECK_WAY'].isin([3, 4])) & (info_data['NAME'].str.contains('车间'))])
    return {
        'xc': xc,
        'shebei': shebei,
        'ratio1': ratio1,
        'znks': znks,
        'yycj': yycj,
        'znks_dy': znks_dy,
        'yycj_dy': yycj_dy,
        'ratio2': ratio2
    }


def get_gb_check_info(info_data, last_info):
    info_data = info_data[info_data['IDENTITY'] == '干部']
    last_info = last_info[last_info['IDENTITY'] == '干部']
    # 现场检查
    xc = len(info_data[info_data['CHECK_WAY'].isin([1, 2])])
    last_xc = len(last_info[last_info['CHECK_WAY'].isin([1, 2])])
    ratio1 = _get_ring_diff(xc, last_xc)
    # 设备调阅
    shebei = len(info_data[info_data['CHECK_WAY'].isin([3, 4])])
    last_shebei = len(last_info[last_info['CHECK_WAY'].isin([3, 4])])
    ratio2 = _get_ring_diff(shebei, last_shebei)
    return {
        'xc': xc,
        'shebei': shebei,
        'ratio1': ratio1,
        'ratio2': ratio2
    }


def get_problem_situation(problem_data, last_problem):
    # 下现场检查
    problem_data = problem_data[problem_data['CHECK_WAY'].isin([1, 2])]
    last_problem = last_problem[last_problem['CHECK_WAY'].isin([1, 2])]
    # 发现问题数
    counts = len(problem_data)
    last_count = len(last_problem)
    ratio = _get_ring_diff(counts, last_count)
    # 各类问题
    all_types = []
    for i in ['作业', '管理', '设备', '外部环境']:
        data = problem_data[problem_data['LEVEL'].isin(_PROBLEM_TYPE[i])]
        dic = {
            'count': len(data),
            'ratio': round(ded_zero(len(data), counts) * 100, 2)
        }
        all_types.append(dic)
    # 红线问题
    luju_red = problem_data[problem_data['IS_RED_LINE'].isin([1])]
    zd_red = problem_data[problem_data['IS_RED_LINE'].isin([2])]
    # 性质严重问题
    main_pro = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    last_main = last_problem[last_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    diff = round(ded_zero(len(main_pro), counts) * 100, 2)
    ratio4 = _get_ring_diff(len(main_pro), len(last_main))
    # 各类严重问题
    main_types = []
    for i in ['作业', '管理', '设备']:
        data = main_pro[main_pro['LEVEL'].isin(_PROBLEM_TYPE[i])]
        main_types.append(len(data))
    return {
        'count': counts,
        'ratio': ratio,
        'all_types': all_types,
        'luju_red': len(luju_red),
        'zd_red': len(zd_red),
        'main_problem': len(main_pro),
        'ratio4': ratio4,
        'main_types': main_types,
        'diff': diff
    }


def get_dy_problem_situation(problem_data, last_problem):
    # 调阅检查
    problem_data = problem_data[problem_data['CHECK_WAY'].isin([3, 4])]
    last_problem = last_problem[last_problem['CHECK_WAY'].isin([3, 4])]
    # 发现问题数
    counts = len(problem_data)
    last_count = len(last_problem)
    ratio = _get_ring_diff(counts, last_count)
    # 性质严重问题
    main_pro = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    diff = round(ded_zero(len(main_pro), counts) * 100, 2)
    return {
        'count': counts,
        'ratio': ratio,
        'main_problem': len(main_pro),
        'diff': diff
    }


def _station_check_table(info_data, problem_data):
    all_list = []
    for i in info_data['NAME'].unique().tolist():
        new_info = info_data[info_data['NAME'] == i]
        new_pro = problem_data[problem_data['NAME'] == i]
        # 下现场检查
        xc_info_data = new_info[new_info['CHECK_WAY'].isin([1, 2])]
        xc_pro_data = new_pro[new_pro['CHECK_WAY'].isin([1, 2])]
        xc_count = len(xc_info_data)
        xc_pro = int(xc_info_data['PROBLEM_NUMBER'].sum())
        xc_main = len(xc_pro_data[xc_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        # 设备监控调阅
        shebei_info_data = new_info[new_info['CHECK_WAY'].isin([3, 4])]
        shebei_pro_data = new_pro[new_pro['CHECK_WAY'].isin([3, 4])]
        shebei_count = len(shebei_info_data)
        shebei_pro = int(shebei_info_data['PROBLEM_NUMBER'].sum())
        shebei_main = len(shebei_pro_data[shebei_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        all_list.append([i, xc_count, xc_pro, xc_main, shebei_count, shebei_pro, shebei_main])
    return {
        'all_list': all_list
    }
# ---------------------------------------------------------------第二部分--------------------------------------------------------------------


# ---------------------------------------------------------------第三部分----------------------------------------------------------------------
def get_third(wz_data):
    # 违章大王 0  严重违章大王1
    count = [0, 0]
    for i in wz_data['ID_CARD'].unique().tolist():
        data = wz_data[wz_data['ID_CARD'] == i]
        wz_data = data[data['LEVEL'].isin(['A', 'B', 'C'])]
        main_wz = data[data['LEVEL'].isin(['A', 'B'])]
        if len(wz_data) >= 3:
            count[0] += 1
        if len(main_wz) >= 3:
            count[1] += 1
    return {
        'count': count
    }


# --------------------------------------------------------------------第四部分--------------------------------------------------------------------
def get_fourth(problem_data, end_date):
    # 总问题数
    pro = len(problem_data)
    # 段自查
    zd = len(problem_data[problem_data['TYPE'] == 3])
    # 路局检查
    luju = len(problem_data[problem_data['TYPE'].isin([1, 2])])
    # 已整改消号问题
    yxh = len(problem_data[(problem_data['STATUS'] == 3) & (problem_data['SOLVE_TIME'] < datetime.datetime.strptime(end_date, '%Y-%m-%d').date())])
    # 未整改
    wzg = len(problem_data[problem_data['STATUS'] == 0])
    # 未销号问题
    wxh = len(problem_data[problem_data['SOLVE_TIME'] > datetime.datetime.strptime(end_date, '%Y-%m-%d').date()])
    return {
        'pro': pro,
        'zd': zd,
        'luju': luju,
        'yxh': yxh,
        'wzg': wzg,
        'wxh': wxh
    }


# ---------------------------------------------------------------第五部分---------------------------------------------------------------------
def get_fifth(eva_data, eva_re_data):
    """
    干部履职评价情况
    :param eva_data:
    :param eva_re_data:
    :return:
    """
    # 评价基本情况
    eva_situation = get_eva_situation(eva_data, eva_re_data)
    # 评价记分情况
    score_situation = get_score_situation(eva_data)
    # 存在主要问题
    exist_pro = get_exist_pro(eva_data)
    return {
        'eva_situation': eva_situation,
        'score_situation': score_situation,
        'exist_pro': exist_pro
    }


def get_eva_situation(eva_data, eva_re_data):
    # 逐条评价
    zt = len(eva_data[eva_data['EVALUATE_WAY'] == 1])
    zt_re = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 1])
    # 定期
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    dq_re = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    # 设备监控调阅复查
    shebei = len(eva_data[eva_data['CHECK_WAY'].isin([3, 4])])
    # 总记分
    score = round(float((eva_data['SCORE'].sum())), 4)
    # 记分最多人
    if eva_data.empty is True:
        max_data = eva_data
        max_score = 0
    else:
        max_data = eva_data.groupby(['RESPONSIBE_ID_CARD', 'ALL_NAME', 'RESPONSIBE_PERSON_NAME']).sum().sort_values(by='SCORE', ascending=False).reset_index()
        max_score = list(max_data['SCORE'])[0]
    max_list = []
    for i, j in max_data[max_data['SCORE'] == max_score].iterrows():
        dic = {
            'name': j['RESPONSIBE_PERSON_NAME'],
            'dp': j['ALL_NAME']
        }
        max_list.append(dic)
    return {
        'zt': zt,
        'zt_re': zt_re,
        'dq': dq,
        'dq_re': dq_re,
        'shebei': shebei,
        'score': score,
        'max_list': max_list
    }


def get_score_situation(eva_data):
    # 分析室和分析中心评价
    luju = len(eva_data[eva_data['CHECK_TYPE'].isin([1, 2])])
    zd = len(eva_data[eva_data['CHECK_TYPE'] == 3])
    # 被评价
    pj_count = []
    for i in ['科', '运用车间', '检修车间', '整备车间']:
        data = eva_data[eva_data['SHOP'].str.contains(i)]
        pj_count.append(len(data))
    # 定期、逐条、阶段
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2])
    zt = len(eva_data[eva_data['EVALUATE_TYPE'].isin([1, 2])])
    jd = len(eva_data[eva_data['EVALUATE_WAY'] == 3])
    # 职务分类
    zhiwu_count = []
    for i in ['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        data = eva_data[eva_data['GRADATION'] == i]
        zhiwu_count.append(len(data))
    # 计分内容 录入信息、问题明显错误。
    all_reason = []
    for i, j in eva_data.groupby('SHOP').count().sort_values(by='GRADATION', ascending=False).reset_index().iterrows():
        if j['SITUATION'] != '录入信息、问题明显错误。':
            dic = {
                'name': j['SHOP'],
                'count': int(j['GRADATION'])
            }
            all_reason.append(dic)
        else:
            continue
    # 具体记分内容
    all_table = []
    for i, j in eva_data.iterrows():
        dic = {
            'name': j['RESPONSIBE_PERSON_NAME'],
            'dp': j['SHOP'],
            'type': j['CHECK_TYPE'],
            'code': j['CODE'],
            'codes': j['CODE_ADDITION']
        }
        all_table.append(dic)
    return {
        'luju': luju,
        'zd': zd,
        'pj_count': pj_count,
        'dq': dq,
        'zt': zt,
        'jd': jd,
        'zhiwu_count': zhiwu_count,
        'all_reason': all_reason,
        'all_table': all_table
    }


def get_exist_pro(eva_data):
    """
    存在主要问题
    :param eva_data:
    :return:
    """
    all_content = []
    for i, k in eva_data.groupby('ITEM_NAME').count().sort_values('SHOP', ascending=False).reset_index().iterrows():
        dic = {
            'name': k['ITEM_NAME'],
            'content': int(k['SHOP'])
        }
        all_content.append(dic)
    return {
        'all_content': all_content[:3]
    }


# ---------------------------------------------------------------第六部分-----------------------------------------------------------------
def get_sixth(problem_data, last_problem):
    # 动车典型问题
    dc_data = problem_data[problem_data['SHOP'] == '动车运用车间']
    last_dc = last_problem[last_problem['SHOP'] == '动车运用车间']
    dc_pro = get_dc(dc_data, last_dc)
    # 普速列车问题
    name = list(problem_data['SHOP'].unique())
    if '动车运用车间' in name:
        name.remove('动车运用车间')
    else:
        pass
    pslc_data = problem_data[problem_data['SHOP'].isin(name)].reset_index()
    last_pslc = last_problem[last_problem['SHOP'].isin(name)].reset_index()
    pslc_pro = get_pslc(pslc_data, last_pslc)
    return {
        'dc_pro': dc_pro,
        'pslc_pro': pslc_pro
    }


def get_dc(dc_data, last_dc):
    # 作业层面
    work = get_work_dc(dc_data, last_dc)
    # 管理层面
    manager = get_manager_dc(dc_data, last_dc)
    return {
        'work': work,
        'manager': manager
    }


def get_work_dc(dc_data, last_dc):
    work_data = dc_data[dc_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    last_work = last_dc[last_dc['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 环比
    ratio1 = _get_ring_diff(len(work_data), len(last_work))
    # 单类问题数
    type_count = []
    for i in ['A', 'B', 'C', 'D']:
        type_count.append(len(work_data[work_data['LEVEL'] == i]))
    # 间断瞭望
    jdlw_data = dc_data[
        (dc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (dc_data['PROBLEM_POINT'].str.contains('间歇打盹'))]
    last_jdlw = last_dc[
        (last_dc['PROBLEM_POINT'].str.contains('间断瞭望')) | (last_dc['PROBLEM_POINT'].str.contains('间歇打盹'))]
    jdlw_count = len(jdlw_data)
    ratio2 = _get_ring_diff(jdlw_count, len(last_jdlw))
    # 典型间断瞭望问题
    content = jdlw_data[jdlw_data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
    all_list = []
    for i in ['列车超速运行', '动车组司机所内及非营业车站违反开关门规定', '重要作业步骤漏项']:
        data = dc_data[dc_data['PROBLEM_POINT'].str.contains(i)]
        last_data = last_dc[last_dc['PROBLEM_POINT'].str.contains(i)]
        dic = {
            'name': i,
            'count': len(data),
            'ratio': _get_ring_diff(len(data), len(last_data)),
            'content': data[data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
        }
        all_list.append(dic)
    return {
        'work_count': len(work_data),
        'ratio1': ratio1,
        'type_count': type_count,
        'jdlw_count': jdlw_count,
        'ratio2': ratio2,
        'content': content,
        'all_list': all_list
    }


def get_manager_dc(dc_data, last_dc):
    m_data = dc_data[dc_data['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])]
    last_m = last_dc[last_dc['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])]
    # 环比
    ratio1 = _get_ring_diff(len(m_data), len(last_m))
    # 单类问题数
    type_count = []
    for i in ['F1', 'F2', 'F3', 'F4']:
        type_count.append(len(m_data[m_data['LEVEL'] == i]))
    return {
        'm_count': len(m_data),
        'ratio1': ratio1,
        'type_count': type_count,
    }


def get_pslc(pslc_data, last_pslc):
    # 间断瞭望
    jdlw = get_pslc_jdlw(pslc_data, last_pslc)
    # 劳动安全隐患
    ldaq = get_pslc_ldaq(pslc_data, last_pslc)
    # 乘务员错误操纵问题
    cwcz = get_pslc_cwcz(pslc_data, last_pslc)
    # 调车安全隐患
    dcaq = get_pslc_dcaq(pslc_data, last_pslc)
    # 机车漏检漏修问题突出
    ljlx = get_pslc_ljlx(pslc_data, last_pslc)
    return {
        'jdlw': jdlw,
        'ldaq': ldaq,
        'cwcz': cwcz,
        'ljlx': ljlx,
        'dcaq': dcaq
    }


def get_pslc_jdlw(pslc_data, last_pslc):
    """
    普速列车间断瞭望
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    jdlw_data = pslc_data[
        (pslc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (pslc_data['PROBLEM_POINT'].str.contains('间歇打盹'))]
    last_jdlw = last_pslc[
        (last_pslc['PROBLEM_POINT'].str.contains('间断瞭望')) | (last_pslc['PROBLEM_POINT'].str.contains('间歇打盹'))]
    jdlw_count = len(jdlw_data)
    ratio2 = _get_ring_diff(jdlw_count, len(last_jdlw))
    # 典型间断瞭望问题
    content = jdlw_data[jdlw_data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
    return {
        'jdlw_count': jdlw_count,
        'ratio2': ratio2,
        'content': content
    }


def get_pslc_ldaq(pslc_data, last_pslc):
    """
    普速列车劳动安全
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    pslc_data = pslc_data.dropna(subset=['KEY_ITEM'])
    last_pslc = last_pslc.dropna(subset=['KEY_ITEM'])
    ldaq_data = pslc_data[pslc_data['KEY_ITEM'].str.contains('劳动安全')]
    last_ldaq = last_pslc[last_pslc['KEY_ITEM'].str.contains('劳动安全')]
    ratio = _get_ring_diff(len(ldaq_data), len(last_ldaq))
    return {
        'ldaq_count': len(ldaq_data),
        'ratio': ratio,
    }


def get_pslc_cwcz(pslc_data, last_pslc):
    """
    普速列车乘务员错误操作
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    cwcz_data = pslc_data[
        (pslc_data['RISK_NAMES'].str.contains('乘务员')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    last_cwcz = last_pslc[
        (last_pslc['RISK_NAMES'].str.contains('乘务员')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    ratio = _get_ring_diff(len(cwcz_data), len(last_cwcz))
    # 严重问题
    main_data = cwcz_data[cwcz_data['LEVEL'].isin(['A', 'B'])]
    last_main = last_cwcz[last_cwcz['LEVEL'].isin(['A', 'B'])]
    ratio1 = _get_ring_diff(len(main_data), len(last_main))
    # 分类
    count = []
    for i in ['操纵不当造成监控装置', '错误关闭劈相机', '错误使用车位键调整距离', '机车试验不当造成机车移动', '未进行制动机简略试验']:
        data = main_data[main_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'cwcz_count': len(cwcz_data),
        'ratio': ratio,
        'main_count': len(main_data),
        'ratio1': ratio1,
        'count': count
    }


def get_pslc_dcaq(pslc_data, last_pslc):
    """
    普速列车调车安全隐患
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    dcaq_data = pslc_data[pslc_data['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    last_dcaq = last_pslc[last_pslc['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    ratio = _get_ring_diff(len(dcaq_data), len(last_dcaq))
    # 严重问题
    main_data = dcaq_data[dcaq_data['LEVEL'].isin(['A', 'B'])]
    content = []
    for i, j in main_data.groupby('PROBLEM_POINT').count().sort_values(by='LEVEL',
                                                                       ascending=False).reset_index().iterrows():
        dic = {
            'name': j['PROBLEM_POINT'],
            'count': int(j['LEVEL'])
        }
        content.append(dic)
    return {
        'dcaq_count': len(dcaq_data),
        'ratio': ratio,
        'main_count': len(main_data),
        'content': content
    }


def get_pslc_ljlx(pslc_data, last_pslc):
    """
    普速列车漏检漏修问题
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    ljlx_data = pslc_data[
        (pslc_data['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写') | (pslc_data['RISK_NAMES'].str.contains('行车设备质量'))]
    ljlx_data = ljlx_data[(ljlx_data['SHOP'].str.contains('检修车间')) | (ljlx_data['SHOP'].str.contains('整备车间')) | (
        ljlx_data['SHOP'].str.contains('解体组装'))]
    ljlx_data = ljlx_data[ljlx_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    last_ljlx = last_pslc[
        (last_pslc['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写') | (last_pslc['RISK_NAMES'].str.contains('行车设备质量'))]
    last_ljlx = last_ljlx[(last_ljlx['SHOP'].str.contains('检修车间')) | (last_ljlx['SHOP'].str.contains('整备车间')) | (
        last_ljlx['SHOP'].str.contains('解体组装'))]
    last_ljlx = last_ljlx[last_ljlx['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    ratio = _get_ring_diff(len(ljlx_data), len(last_ljlx))
    return {
        'ljlx_count': len(ljlx_data),
        'ratio': ratio,
    }