from datetime import datetime as dt
from app.data.util import pd_query
from app.report.jiwu.station_daily_report_sql import *


code = {
    0: 'B',
    1: 'C',
    2: 'D',
    3: 'E',
    4: 'F'
}


def get_data(times, work_sheet, dp_id, workbook):
    start_date = times[0]
    end_date = times[1]
    station_name = pd_query("select NAME from t_department where department_id = '{0}'".format(dp_id))['NAME'][0]
    workshots = pd_query("select NAME,DEPARTMENT_ID from t_department where fk_parent_id = '{0}'".format(dp_id))
    work_sheet['A1'] = '{0}安全日报表'.format(station_name)
    # 检查信息
    info_data = pd_query(check_info_sql.format(times[0], times[1], dp_id)).dropna(subset=['NAME'])
    # 检查问题信息
    problem_data = pd_query(check_problem_sql.format(times[0], times[1], dp_id)).dropna(subset=['NAME'])
    # 音视频检查
    mv_data = pd_query(check_mv_sql.format(times[0], times[1], dp_id)).dropna(subset=['NAME'])
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format(times[1], dp_id))
    # 日安全责任事故
    daily_without = pd_query(withouts_accident_sql.format(times[0], times[1], dp_id))
    # 安全预警
    safety_notification = pd_query(warning_notification_sql.format(times[0], times[1], dp_id)).dropna(subset=['NAME'])
    # 重要检查信息
    main_info = pd_query(main_info_sql.format(times[0], times[1], dp_id))
    main_info['NAME'] = main_info['SURVEY_DEPARTMENT_NAMES'].apply(lambda x: x.split('-')[1])
    # 整改情况
    zg_data = pd_query(zg_sql.format(times[0], times[1], dp_id)).dropna(subset=['NAME'])
    first = get_first(info_data, problem_data, mv_data, main_info, safety_notification, zg_data, work_sheet, workshots, workbook)
    second = get_second(without_accident, work_sheet)
    third = get_third(info_data, problem_data, mv_data, safety_notification, main_info, zg_data, work_sheet)
    return {
        'start_date': start_date,
        'title': '{0}安全日报表'.format(station_name),
        'end_date': end_date,
        'first': first,
        'second': second,
        'third': third
    }


def get_first(info_data, problem_data, mv_data, main_info, safety_notification, zg_data, work_sheet, workshots, workbook):
    """
    检查信息

    :param info_data:
    :param problem_data:
    :param mv_data:
    :param main_info:
    :param safety_notification:
    :param zg_data:
    :param work_sheet:
    :param workshots:
    :return:
    """
    all_list = []
    index1 = 0
    # 分车间取数据
    work_sheets = work_sheet
    for i in workshots['NAME']:
        safety_data = safety_notification[safety_notification['NAME'].str.contains(i)]
        info = info_data[info_data['NAME'].str.contains(i)]
        problem = problem_data[problem_data['NAME'].str.contains(i)]
        main = main_info[main_info['NAME'].str.contains(i)]
        zg = zg_data[zg_data['NAME'].str.contains(i)]
        mv = mv_data[mv_data['NAME'].str.contains(i)]
        eva_pro_data = problem[problem['IS_EVALUATE'] == 1]
        # 记分评价人次
        eva_number = len(problem[problem['IS_EVALUATE'] == 1])
        # 记分评价情况
        eva_sit = '\n'.join(eva_pro_data['DESCRIPTION'].tolist()) if len(eva_pro_data['DESCRIPTION']) != 0 else '无'
        # 到期未整改问题数
        dq_wzg = len(zg[zg['TYPE'] == 1])
        # 超期未销号问题数
        cq_wxh = len(zg[zg['TYPE'] == 3])
        persons_list = []
        index2 = 0
        for j in ['干部', '工班长']:
            # safety_person = safety_data[safety_data['POSITION'].str.contains(j)]
            safety_person = safety_data
            info_person = info[info['POSITION'].str.contains(j)]
            problem_person = problem[problem['POSITION'].str.contains(j)]
            main_person = main[main['POSITION'].str.contains(j)]
            mv_person = mv[mv['POSITION'].str.contains(j)]
            res = cal_person_data(info_person, problem_person, mv_person, safety_person, main_person)
            persons_list.append(res)
            # 分干部类型填入数据
            index = 4
            if index1 % 2 == 0:
                if index2 % 2 == 0:
                    for k in range(20):
                        work_sheets['C{0}'.format(index)] = res[k]
                        index += 1
                    work_sheets['C28'] = persons_list[0][20]
                    work_sheets['C29'] = persons_list[0][21]
                    work_sheets['C31'] = persons_list[0][22]
                    work_sheets['C32'] = persons_list[0][23]
                    work_sheets['C33'] = persons_list[0][24]
                    work_sheets['C34'] = persons_list[0][25]
                else:
                    for k in range(20):
                        work_sheets['D{0}'.format(index)] = res[k]
                        index += 1
                    work_sheets['D28'] = persons_list[0][20]
                    work_sheets['D29'] = persons_list[0][21]
                    work_sheets['D31'] = persons_list[0][22]
                    work_sheets['D32'] = persons_list[0][23]
                    work_sheets['D33'] = persons_list[0][24]
                    work_sheets['D34'] = persons_list[0][25]
            else:
                if index2 % 2 == 0:
                    for k in range(20):
                        work_sheets['F{0}'.format(index)] = res[k]
                        index += 1
                    work_sheets['F28'] = persons_list[0][20]
                    work_sheets['F29'] = persons_list[0][21]
                    work_sheets['F31'] = persons_list[0][22]
                    work_sheets['F32'] = persons_list[0][23]
                    work_sheets['F33'] = persons_list[0][24]
                    work_sheets['F34'] = persons_list[0][25]
                else:
                    for k in range(20):
                        work_sheets['G{0}'.format(index)] = res[k]
                        index += 1
                    work_sheets['G28'] = persons_list[0][20]
                    work_sheets['G29'] = persons_list[0][21]
                    work_sheets['G31'] = persons_list[0][22]
                    work_sheets['G32'] = persons_list[0][23]
                    work_sheets['G33'] = persons_list[0][24]
                    work_sheets['G34'] = persons_list[0][25]
            index2 += 1
        all_list.append([eva_number, eva_sit, dq_wzg, cq_wxh, persons_list])
        # 分车间科室序号填入数据
        if index1 % 2 == 0:
            work_sheets['C2'] = i
            work_sheets['C24'] = eva_number
            work_sheets['C25'] = eva_sit
            work_sheets['C26'] = dq_wzg
            work_sheets['C27'] = cq_wxh
        else:
            work_sheets['F2'] = i
            work_sheets['F24'] = eva_number
            work_sheets['F25'] = eva_sit
            work_sheets['F26'] = dq_wzg
            work_sheets['F27'] = cq_wxh
        index1 += 1
        if index1 % 2 == 0 and index1 != 0:
            work_sheets = workbook.copy_worksheet(work_sheet)
    return all_list


def get_second(without_accident, work_sheet):
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
        if len(accident) == 0:
            dic['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    content = '截至今日：全段实现无责任一般A类事故{0}天;无责任一般B类事故{1}天;无责任一般C类事故{2}天;' \
              '无责任一般D类事故{3}天。全段实现安全生产{4}天。'.format(dic['A'], dic['B'], dic['C'], dic['D'], dic['A'])
    work_sheet['B30'] = content
    return content


def get_third(info_data, problem_data, mv_data, safety_notification, main_info, zg_data, work_sheet):
    """
    领导检查信息
    :param info_data:
    :param problem_data:
    :param mv_data:
    :param safety_notification:
    :param main_info:
    :param zg_data:
    :param work_sheet:
    :return:
    """
    safety_data = safety_notification[safety_notification['NAME'].str.contains('领导')]
    info = info_data[info_data['NAME'].str.contains('领导')]
    problem = problem_data[problem_data['NAME'].str.contains('领导')].dropna(subset=['PROBLEM_CLASSITY_NAME'])
    main = main_info[main_info['NAME'].str.contains('领导')]
    zg = zg_data[zg_data['NAME'].str.contains('领导')]
    mv = mv_data[mv_data['NAME'].str.contains('领导')]
    eva_pro_data = problem[problem['IS_EVALUATE'] == 1]
    high = problem[problem['LEVEL'].isin(['A', 'B', 'E1', 'E2'])]
    if high.empty is not True:
        pro_point = high['PROBLEM_POINT'].value_counts().index[0]
        desc = '\n'.join(high[high['PROBLEM_POINT'] == pro_point]['DESCRIPTION'].tolist())
    else:
        desc = '无'
    # 现场检查
    xc = len(info[info['CHECK_WAY'] == 1])
    # 添乘检查
    tc = len(info[info['CHECK_WAY'] == 2])
    # 异地检查
    yd = len(info[info['POINT'].str.contains('异地')])
    # 动客车检查
    dkc = len(info[(info['IS_DONGCHE'] == 1) | (info['IS_KECHE'] == 1)])
    # 调车机（点）检查
    dcj = len(info[info['POINT'].str.contains('调车机')])
    # 夜查
    yecha = len(info[info['IS_YECHA'] == 1])
    # 音视频调阅次数
    mv_count = len(mv[mv['TYPE'] == 4])
    # 音视频调阅时长
    mv_time = int(mv[mv['TYPE'] == 4]['COST_TIME'].sum())
    # 音视频复检次数
    mv_re_count = len(mv[(mv['TYPE'] == 4) & (mv['CHECK_WAY'] == 4)])
    # 音视频复检时长
    mv_re_time = int(mv[(mv['TYPE'] == 4) & (mv['CHECK_WAY'] == 4)]['COST_TIME'].sum())
    # LKJ（EOAS）数据分析次数
    lkj = 0
    # LKJ（EOAS）数据分析复检次数
    lkj_re = 0
    # JK430数据分析次数
    jk430 = 0
    # JK430数据分析复检次数
    jk430_re = 0
    # 问题总数
    pro_number = len(problem)
    # 作业问题数
    work_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    # 设备问题数
    shebei_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    # 管理问题数
    manager_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    # 高质量问题数
    high_pro = len(high)
    # A类问题情况
    a_pro = '\n'.join(problem[problem['LEVEL'] == 'A']['DESCRIPTION'].tolist()) if len(problem[problem['LEVEL'] == 'A']
                                                                                       ['DESCRIPTION']) != 0 else '无'
    # 记分评价人次
    eva_number = len(problem[problem['IS_EVALUATE'] == 1])
    # 记分评价情况
    eva_sit = '\n'.join(eva_pro_data['DESCRIPTION'].tolist()) if len(eva_pro_data['DESCRIPTION']) != 0 else '无'
    # 到期未整改问题数
    dq_wzg = len(zg[zg['TYPE'] == 1])
    # 超期未销号问题数
    cq_wxh = len(zg[zg['TYPE'] == 3])
    # 机务系统日风险预警
    sys_risk_warning = len(safety_data[safety_data['HIERARCHY'] == 2])
    # 段日风险预警
    station_risk_warning = len(safety_data[safety_data['HIERARCHY'] == 3])
    # 日安全生产信息
    safety_produce_info = 0
    # 重点追踪信息情况
    main_infos = '\n'.join(main['CONTENT'].tolist()) if len(main['CONTENT']) != 0 else '无'
    # 路局红线问题情况
    red_sit = '\n'.join(problem[problem['IS_RED_LINE'] == 1]['DESCRIPTION'].tolist())
    # 问题项点出现次数最多的高质量问题情况
    high_sit = desc
    data_list = [xc, tc, yd, dkc, dcj, yecha, mv_count, mv_time, mv_re_count, mv_re_time, lkj, lkj_re, jk430, jk430_re,
                 pro_number, work_pro, shebei_pro, manager_pro, high_pro, a_pro, eva_number, eva_sit, dq_wzg, cq_wxh,
                 sys_risk_warning, station_risk_warning, safety_produce_info, main_infos, red_sit, high_sit]
    index = 4
    for i in data_list:
        if index == 30:
            index += 1
            continue
        else:
            work_sheet['B{0}'.format(index)] = i
            index += 1
    return data_list


def cal_person_data(info, problem, mv, safety_data, main):
    high = problem[problem['LEVEL'].isin(['A', 'B', 'E1', 'E2'])]
    if high.empty is not True:
        pro_point = high['PROBLEM_POINT'].value_counts().index[0]
        desc = '\n'.join(high[high['PROBLEM_POINT'] == pro_point]['DESCRIPTION'].tolist())
    else:
        desc = '无'
    # 现场检查
    xc = len(info[info['CHECK_WAY'] == 1])
    # 添乘检查
    tc = len(info[info['CHECK_WAY'] == 2])
    # 异地检查
    yd = len(info[info['POINT'].str.contains('异地')])
    # 动客车检查
    dkc = len(info[(info['IS_DONGCHE'] == 1) | (info['IS_KECHE'] == 1)])
    # 调车机（点）检查
    dcj = len(info[info['POINT'].str.contains('调车机')])
    # 夜查
    yecha = len(info[info['IS_YECHA'] == 1])
    # 音视频调阅次数
    mv_count = len(mv[mv['TYPE'] == 4])
    # 音视频调阅时长
    mv_time = int(mv[mv['TYPE'] == 4]['COST_TIME'].sum())
    # 音视频复检次数
    mv_re_count = len(mv[(mv['TYPE'] == 4) & (mv['CHECK_WAY'] == 4)])
    # 音视频复检时长
    mv_re_time = int(mv[(mv['TYPE'] == 4) & (mv['CHECK_WAY'] == 4)]['COST_TIME'].sum())
    # LKJ（EOAS）数据分析次数
    lkj = 0
    # LKJ（EOAS）数据分析复检次数
    lkj_re = 0
    # JK430数据分析次数
    jk430 = 0
    # JK430数据分析复检次数
    jk430_re = 0
    # 问题总数
    pro_number = len(problem)
    # 作业问题数
    work_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    # 设备问题数
    shebei_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    # 管理问题数
    manager_pro = len(problem[problem['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    # 高质量问题数
    high_pro = len(high)
    # A类问题情况
    a_pro = '\n'.join(problem[problem['LEVEL'] == 'A']['DESCRIPTION'].tolist()) if len(problem[problem['LEVEL'] == 'A']
                                                                                       ['DESCRIPTION']) != 0 else '无'
    # 机务系统日风险预警
    sys_risk_warning = len(safety_data[safety_data['HIERARCHY'] == 2])
    # 段日风险预警
    station_risk_warning = len(safety_data[safety_data['HIERARCHY'] == 3])
    # 日安全生产信息
    safety_produce_info = 0
    # 重点追踪信息情况
    main_infos = '\n'.join(main['CONTENT'].tolist()) if len(main['CONTENT']) != 0 else '无'
    # 路局红线问题情况
    red_sit = '\n'.join(problem[problem['IS_RED_LINE'] == 1]['DESCRIPTION'].tolist()) if \
        len(problem[problem['IS_RED_LINE'] == 1]['DESCRIPTION'].tolist()) != 0 else '无'
    # 问题项点出现次数最多的高质量问题情况
    high_sit = desc
    data_list = [xc, tc, yd, dkc, dcj, yecha, mv_count, mv_time, mv_re_count, mv_re_time, lkj, lkj_re, jk430, jk430_re,
                 pro_number, work_pro, shebei_pro, manager_pro, high_pro, a_pro, sys_risk_warning, station_risk_warning,
                 safety_produce_info, main_infos, red_sit, high_sit]
    return data_list