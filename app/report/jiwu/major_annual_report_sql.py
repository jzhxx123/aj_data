# 检查信息
CHECK_INFO_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.ID_CARD,a.CHECK_WAY,a.CHECK_ITEM_NAMES,a.SUBMIT_TIME from t_check_info a
where a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
and a.DEPARTMENT_ALL_NAME like '%%机务段%%'
"""
# 职工人数
CHECK_PERSON_NUMBER = """
        SELECT a.PERSON_id,b.ALL_NAME  FROM `t_person` a
        INNER JOIN t_department b on b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
        where b.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
        """
# 问题质量分
CHECK_PROBLEM_FREQ_SCORE_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.CHECK_WAY,a.PROBLEM_NUMBER,c.CHECK_SCORE,a.IS_YECHA,a.CHECK_ADDRESS_NAMES,
b.PROBLEM_CLASSITY_NAME,b.IS_RED_LINE,b.RISK_LEVEL
from t_check_info a
LEFT JOIN t_check_problem b on b.FK_CHECK_INFO_ID = a.PK_ID
LEFT JOIN t_problem_base c on b.FK_PROBLEM_BASE_ID = c.PK_ID
where a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
and a.DEPARTMENT_ALL_NAME like '%%机务段%%'
"""

CHECK_CWY_SQL = """SELECT a.DEPARTMENT_ALL_NAME,a.CHECK_WAY,a.PROBLEM_NUMBER,a.IS_YECHA,a.CHECK_ADDRESS_NAMES,
c.MONITOR_POST_NAMES,b.PROBLEM_CLASSITY_NAME,b.IS_RED_LINE,b.RISK_LEVEL
from t_check_info a
LEFT JOIN t_check_problem b on b.FK_CHECK_INFO_ID = a.PK_ID
LEFT JOIN t_check_info_and_media c on c.FK_CHECK_INFO_ID = a.PK_ID
where a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
and a.DEPARTMENT_ALL_NAME like '%%机务段%%'"""

CHECK_EVALUATE_SQL = """SELECT
        a.SCORE,
        a.EVALUATE_TYPE,
        a.EVALUATE_WAY,
        a.CHECK_TYPE,
        a.RESPONSIBE_PERSON_NAME,
        b.ITEM_NAME,
        c.ALL_NAME,
        e.`LEVEL`,
        e.IDENTITY,
        e.ID_CARD
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD       
    WHERE
        a.CREATE_TIME >= '{0}'
            AND a.CREATE_TIME <= '{1}'
and c.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'"""

CHECK_EVALUATE_REVIEW_SQL = """select p.EVALUATE_WAY,p.EVALUATE_TYPE,p.IS_REVIEW,i.CHECK_WAY,d.ALL_NAME,i.PROBLEM_NUMBER,p.CHECK_PERSON_ID_CARD
from t_check_evaluate_check_person p,t_department d,t_check_info i 
where p.FK_DEPARTMENT_ID = d.DEPARTMENT_ID and p.CREATE_TIME
BETWEEN '{0}' and '{1}' and p.FK_CHECK_OR_PROBLEM_ID = i.PK_ID
and d.TYPE2='1ACE7D1C80B14456E0539106C00A2E70KSC'"""