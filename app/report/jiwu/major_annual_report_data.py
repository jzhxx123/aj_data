from app.data.util import pd_query
import pandas as pd
import datetime
from app.report.analysis_report_manager import AnnualAnalysisReport
from app import mongo
from app.report.draw_picture import *
from app.report.jiwu.major_annual_report_sql import *
from docx.shared import Mm
from docxtpl import InlineImage


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year):
    date = AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_year_start, last_year_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 11
    ded_month = int(end_month) - int(start_month)
    first = get_first(start_date, end_date, last_year_start, last_year_end, ded_month, year)
    second = get_second(last_year_start, last_year_end)
    third = get_third(start_date, end_date, last_year_start, last_year_end)
    fourth = get_fourth(start_date, end_date, year)
    file_name = f'{start_date}至{end_date}机务系统安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '机务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'first': first,
        'second': second,
        'third': third,
        'fourth': fourth
    }
    return result


def insert_images(result, tpl):
    result['first']['first_three']['picture'] = InlineImage(tpl, result['first']['first_three']['images'], width=Mm(120))
    result['fourth']['job_type']['picture'] = InlineImage(tpl, result['fourth']['job_type']['images'], width=Mm(120))
    result['fourth']['project']['picture'] = InlineImage(tpl, result['fourth']['project']['images'], width=Mm(120))
    return result


def calc_year_ratio(year_list):
    """
    计算同比
    :param year_list:
    :return:
    """
    year, last_year = year_list
    if last_year == 0:
        diff = year
        percent = year * 100
    else:
        diff = year - last_year
        percent = round((year - last_year) / last_year * 100, 1)
    return {
        'diff': diff,
        'percent': percent
    }


def _get_health_index1(start_date, end_date):
    """
    安全综合指数得分及排名
    :param start_date:
    :param end_date:
    :return:
    """
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MAJOR': '机务',
            'MON': {
                "$gte": int(start_date.split('-')[0] + start_date.split('-')[1]),
                "$lte": int(end_date.split('-')[0] + end_date.split('-')[1])
            }
        }, {
            "_id": 0,
            'DEPARTMENT_NAME': 1,
            "SCORE": 1,
            'RANK': 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    if data.empty is True:
        data = pd.DataFrame({
            'DEPARTMENT_NAME': ['1', '2', '3'],
            "SCORE": [1, 2, 3],
            'RANK': [1, 2, 3]
        })
    else:
        data = data
    return data


def calc_freq_rank(info_data, num):
    """
    计算检查排名
    :param info_data:
    :param num:
    :return:
    """
    all_list = []
    for i in info_data['NEW_NAME'].value_counts().index:
        data = info_data[info_data['NEW_NAME'] == i]
        # 检查人员数
        person = int(data['ID_CARD'].value_counts().count())
        # 添乘检查数
        tc = len(data[data['CHECK_WAY'] == num])
        # 人均现场检查数
        score = round(ded_zero(tc, person), 1)
        dic = {
            'name': i,
            'score': score
        }
        all_list.append(dic)
    all_list = sorted(all_list, key=lambda x: x['score'], reverse=True)
    return {
        'all_list': all_list
    }


def calc_two_freq_score(problem_data, person_data):
    """
    计算车间受检质量和车间自检质量
    :param problem_data:
    :param person_data:
    :return:
    """
    all_list = []
    for i in problem_data['NEW_NAME'].value_counts().index:
        data2 = problem_data[problem_data['NEW_NAME'] == i]
        data3 = person_data[person_data['NEW_NAME'] == i]
        # 车间受检质量
        # 总人数
        person = int(data3['PERSON_id'].value_counts().count())
        # 人均问题数
        problem_num = int(data2['PROBLEM_NUMBER'].sum())
        if person == 0:
            avg_problem = 0
        else:
            avg_problem = round(ded_zero(problem_num, person), 1)
        # 人均问题质量分
        score = int(data2['ALL_SCORE'].sum())
        if person == 0:
            avg_score = 0
        else:
            avg_score = round(ded_zero(score, person), 1)
        # 车间自检质量
        # 自查问题数占比
        zicha_problem = int(data2[data2['DEPARTMENT_ALL_NAME'] == data2['CHECK_ADDRESS_NAMES']]['PROBLEM_NUMBER'].sum())
        zicha_ratio = round(ded_zero(zicha_problem, problem_num) * 100, 1)
        # 自查问题质量分占比
        zicha_score = int(data2[data2['DEPARTMENT_ALL_NAME'] == data2['CHECK_ADDRESS_NAMES']]['ALL_SCORE'].sum())
        zicha_score_ratio = round(ded_zero(zicha_score, score) * 100, 1)
        dic = {
            'name': i,
            'station_name': data2['CHECK_ADDRESS_NAMES'].iloc[0],
            'avg_problem': avg_problem,
            'avg_score': avg_score,
            'zicha_ratio': zicha_ratio,
            'zicha_score_ratio': zicha_score_ratio,
        }
        all_list.append(dic)
    l1 = sorted(all_list, key=lambda x: x['avg_problem'], reverse=True)
    l2 = sorted(all_list, key=lambda x: x['zicha_ratio'], reverse=True)
    return {
        'l1': l1,
        'l2': l2
    }


def calc_zc_problem(data, last_problem_data):
    """
    计算自查问题
    :param data:
    :param last_problem_data:
    :return:
    """
    zicha_problem = []
    station_list = [[], []]
    station_name = list(set(data['NEW_NAME'].tolist()))
    index = 0
    for i in [data, last_problem_data]:
        zicha_problem.append(int(i['PROBLEM_NUMBER'].sum()))
        for j in station_name:
            new_data = i[i['NEW_NAME'] == j]
            num = int(new_data['PROBLEM_NUMBER'].sum())
            station_list[index].append(num)
        index = index + 1
    ratio = calc_year_ratio(zicha_problem)
    ratio_list = []
    for i in range(len(station_name)):
        dic = {
            'name': station_name[i],
            'num': station_list[0][i],
            'ratio': calc_year_ratio([station_list[0][i], station_list[1][i]])
        }
        ratio_list.append(dic)
    l1 = sorted(ratio_list, key=lambda x: abs(x['ratio']['percent']), reverse=True)
    l2 = sorted(ratio_list, key=lambda x: x['num'], reverse=True)
    return {
        'station_name': station_name,
        'zicha_problem': zicha_problem[0],
        'ratio': ratio,
        'l1': l1,
        'l2': l2
    }


# ------------------------------------------------------------------第一部分--------------------------------------------
def get_first(start_date, end_date, last_year_start, last_year_end, ded_month, year):
    global problem_data, person_data
    person_data = pd_query(CHECK_PERSON_NUMBER)
    person_data['NEW_NAME'] = person_data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date))
    info_data['NEW_NAME'] = info_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    problem_data = pd_query(CHECK_PROBLEM_FREQ_SCORE_SQL.format(start_date, end_date))
    problem_data['NEW_NAME'] = problem_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    problem_data['ALL_SCORE'] = problem_data.apply(lambda x: x['PROBLEM_NUMBER'] * x['CHECK_SCORE'], axis=1)
    last_info_data = pd_query(CHECK_INFO_SQL.format(last_year_start, last_year_end))
    # (一)总体检查情况
    all_check_info = get_all_check_info(info_data, last_info_data)
    # (二)综合检查力度分析
    check_intensity = get_check_intensity(start_date, end_date)
    # (三)检查频次分析
    check_freq = get_check_freq(info_data, ded_month, person_data, year)
    # (四)定点检查质量分析
    # (五)添乘检查质量分析
    tc_check_freq = get_tc_check_freq(info_data, problem_data)
    # (六)监控调阅质量分析
    shebei_freq = get_shebei_freq(info_data, problem_data, person_data)
    # (七)车间检查质量分析
    chejian_freq = get_chejian_freq(problem_data, person_data)
    return {
        'first_one': all_check_info,
        'first_two': check_intensity,
        'first_three': check_freq,
        'first_five': tc_check_freq,
        'first_six': shebei_freq,
        'first_seven': chejian_freq,
    }


def get_all_check_info(info_data, last_info_data):
    """
    总体检查情况
    :param info_data:
    :param last_info_data:
    :return:
    """
    # 现场检查
    xc = []
    # 设备监控调阅
    shebei = []
    for i in [info_data, last_info_data]:
        xc.append(len(i[i['CHECK_WAY'] == 1]))
        shebei.append(len(i[i['CHECK_WAY'] == 3]))
    # 现场检查同比
    ratio_xc = calc_year_ratio(xc)
    # 设备监控调阅同比
    ratio_shebei = calc_year_ratio(shebei)
    # 添乘检查
    tc = len(info_data[info_data['CHECK_WAY'] == 2])
    # 定点检查
    # dd = len(info_data[info_data['CHECK_WAY']])
    # 音视频调阅
    mv = len(info_data[(info_data['CHECK_WAY'] == 3) & (info_data['CHECK_ITEM_NAMES'].str.contains('音视频调阅'))])
    # 数据分析
    data_analysis = len(info_data[(info_data['CHECK_WAY'] == 3) & (info_data['CHECK_ITEM_NAMES'].str.contains('数据分析'))])
    return {
        'xc': xc[0],
        'shebei': shebei[0],
        'ratio_xc': ratio_xc,
        'ratio_shebei': ratio_shebei,
        'tc': tc,
        # 'dd': dd,
        'mv': mv,
        'data_analysis': data_analysis,
    }


def get_check_intensity(start_date, end_date):
    """
    检查安全综合指数
    :return:
    """
    data = _get_health_index1(start_date, end_date)
    rank = [1, 2, 3]
    # 排名站段名
    name = []
    # 排名站段出现次数
    count = []
    for i in rank:
        new_data = data[data['RANK'] == i]
        name.append(new_data['DEPARTMENT_NAME'].value_counts().index[0])
        count.append(int(new_data['DEPARTMENT_NAME'].value_counts().iloc[0]))
    min_data = data[data['RANK'] == max(data['RANK'].tolist())]
    # 排名最后站段
    min_name = min_data['DEPARTMENT_NAME'].value_counts().index[0]
    min_count = int(min_data['DEPARTMENT_NAME'].value_counts().iloc[0])
    return {
        'name': name,
        'count': count,
        'min_name': min_name,
        'min_count': min_count
    }


def get_check_freq(info_data, ded_month, person_data, year):
    """
    检查频次分析
    :param info_data:
    :param ded_month:
    :param person_data:
    :param year:
    :return:
    """
    # 职工总人数
    person = int(person_data['PERSON_id'].value_counts().count())
    # 现场检查次数
    xc = len(info_data[info_data['CHECK_WAY'] == 1])
    # 检查人员总数
    check_person = int(info_data['ID_CARD'].value_counts().count())
    num1 = round(ded_zero(xc, person) / ded_month, 1)
    num2 = round(ded_zero(xc, check_person) / ded_month, 1)
    # 换算单位检查频次
    calc_check_freq = get_calc_check_freq(info_data, person_data)
    # 人均检查频次
    avg_person_check_freq = calc_freq_rank(info_data, 1)
    # 绘图
    info_data['NEW_DATE'] = info_data['SUBMIT_TIME'].apply(lambda x: ''.join(str(x.date()).split('-')[:2]))
    station_name = [i for i in info_data['NEW_NAME'].value_counts().index]
    count = []
    times = [i for i in info_data['NEW_DATE'].value_counts().index]
    times.sort()
    for i in station_name[:-2]:
        data = info_data[info_data['NEW_NAME'] == i].groupby('NEW_DATE').count().sort_values(
            by='NEW_DATE').reset_index()
        count.append(data['SUBMIT_TIME'].tolist())
    images = line_picture(times, count, station_name, str(year) + '机务现场检查次数趋势图', 'major_', '机务', 'annual')
    return {
        'num1': num1,
        'num2': num2,
        'calc_check_freq': calc_check_freq,
        'avg_person_check_freq': avg_person_check_freq,
        'images': images,
    }


def get_calc_check_freq(info_data, person_data):
    """
    换算单位检查频次
    :param info_data:
    :param person_data:
    :return:
    """
    all_list = []
    for i in info_data['NEW_NAME'].value_counts().index:
        data = info_data[info_data['NEW_NAME'] == i]
        new_person = person_data[person_data['NEW_NAME'] == i]
        # 现场检查次数
        xc = len(data[data['CHECK_WAY'] == 1])
        # 职工总人数
        person = int(new_person['PERSON_id'].value_counts().count())
        # 换算单位检查频次
        score = round(ded_zero(xc, person), 1)
        dic = {
            'name': i,
            'score': score
        }
        all_list.append(dic)
    all_list = sorted(all_list, key=lambda x: x['score'], reverse=True)
    return {
        'all_list': all_list
    }


def get_tc_check_freq(info_data, problem_data):
    """
    添乘检查
    :param info_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_WAY'] == 2]
    rank = calc_freq_rank(info_data, 2)
    tc_list = []
    for i in problem_data['NEW_NAME'].value_counts().index:
        data = problem_data[problem_data['NEW_NAME'] == i]
        # tc检查次数
        tc = len(data)
        find_problem_tc = len(data[data['PROBLEM_NUMBER'] != 0])
        tc_ratio = round(ded_zero(find_problem_tc, tc) * 100, 1)
        # 发现问题质量分
        score = int(max(data[data['PROBLEM_NUMBER'] != 0]['ALL_SCORE'].tolist()))
        # 夜班tc检查比率
        yecha = len(data[data['IS_YECHA'] == 1])
        dic1 = {
            'name': i,
            'tc_ratio': tc_ratio,
            'score': score,
            'yecha_ratio': round(ded_zero(yecha, tc) * 100, 1)
        }
        tc_list.append(dic1)
    l1 = sorted(tc_list, key=lambda x: x['tc_ratio'], reverse=True)
    l2 = sorted(tc_list, key=lambda x: x['score'], reverse=True)
    l3 = sorted(tc_list, key=lambda x: x['yecha_ratio'], reverse=True)
    return {
        'rank': rank,
        'l1': l1,
        'l2': l2,
        'l3': l3
    }


def get_shebei_freq(info_data, problem_data, person_data):
    """
    监控设备调阅
    :param info_data:
    :param problem_data:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_WAY'] == 3]
    rank = calc_freq_rank(info_data, 3)
    tc_list = []
    for i in problem_data['NEW_NAME'].value_counts().index:
        data = problem_data[problem_data['NEW_NAME'] == i]
        # tc检查次数
        sb = len(data)
        find_problem_sb = len(data[data['PROBLEM_NUMBER'] != 0])
        sb_ratio = round(ded_zero(find_problem_sb, sb) * 100, 1)
        # 监控调阅人均问题数
        all_problem = int(data['PROBLEM_NUMBER'].sum())
        person = int(person_data[person_data['NEW_NAME'] == i]['PERSON_id'].value_counts().count())
        avg_problem = round(ded_zero(all_problem, person), 1)
        # 调阅人均质量分
        score = int(data['ALL_SCORE'].sum())
        avg_score = round(ded_zero(score, person), 1)
        dic1 = {
            'name': i,
            'sb_ratio': sb_ratio,
            'avg_problem': avg_problem,
            'avg_score': avg_score
        }
        tc_list.append(dic1)
    l1 = sorted(tc_list, key=lambda x: x['sb_ratio'], reverse=True)
    l2 = sorted(tc_list, key=lambda x: x['avg_problem'], reverse=True)
    l3 = sorted(tc_list, key=lambda x: x['avg_score'], reverse=True)
    return {
        'rank': rank,
        'l1': l1,
        'l2': l2,
        'l3': l3
    }


def get_chejian_freq(problem_data, person_data):
    """
    车间检查质量分析
    :param info_data:
    :param problem_data:
    :param person_data:
    :return:
    """
    # 动车运用车间
    dc = get_dc(problem_data, person_data)
    # 客运车间
    ky = get_ky(problem_data, person_data)
    # 货运车间
    hy = get_hy(problem_data, person_data)
    # 客运机车整备车间
    kyjc = get_kyjc(problem_data, person_data)
    # 货运机车整备车间
    hyjc = get_hyjc(problem_data, person_data)
    # 检修车间
    jx = get_jx(problem_data, person_data)
    return {
        'dc': dc,
        'ky': ky,
        'hy': hy,
        'kyjc': kyjc,
        'hyjc': hyjc,
        'jx': jx,
    }


def get_dc(problem_data, person_data):
    """
    动车运用车间计算
    :param info_data:
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_ADDRESS_NAMES'].str.contains('动车运用车间')]
    person_data = person_data[person_data['ALL_NAME'].str.contains('动车运用车间')]
    result = calc_two_freq_score(problem_data, person_data)
    return result


def get_ky(problem_data, person_data):
    """
    客运车间计算
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[(problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都运用车间')) |
                                (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆北运用车间')) |
                                (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳运用车间'))
                                ]
    person_data = person_data[(person_data['ALL_NAME'].str.contains('成都运用车间')) |
                              (person_data['ALL_NAME'].str.contains('重庆北运用车间')) |
                              (person_data['ALL_NAME'].str.contains('贵阳运用车间'))
                              ]
    result = calc_two_freq_score(problem_data, person_data)
    return result


def get_hy(problem_data, person_data):
    """
    货运车间
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[(problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都北一运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都北二运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('西昌运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('峨眉运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆西运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('内江运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳南运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳西运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('六盘水运用车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('遵义运用车间'))
                                ]
    person_data = person_data[(person_data['ALL_NAME'].str.contains('成都北一运用车间'))
                              | (person_data['ALL_NAME'].str.contains('成都北二运用车间'))
                              | (person_data['ALL_NAME'].str.contains('西昌运用车间'))
                              | (person_data['ALL_NAME'].str.contains('峨眉运用车间'))
                              | (person_data['ALL_NAME'].str.contains('重庆西运用车间'))
                              | (person_data['ALL_NAME'].str.contains('内江运用车间'))
                              | (person_data['ALL_NAME'].str.contains('贵阳南运用车间'))
                              | (person_data['ALL_NAME'].str.contains('贵阳西运用车间'))
                              | (person_data['ALL_NAME'].str.contains('六盘水运用车间'))
                              | (person_data['ALL_NAME'].str.contains('遵义运用车间'))
                              ]
    result = calc_two_freq_score(problem_data, person_data)
    return result


def get_kyjc(problem_data, person_data):
    """
    客运机车运用车间
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[(problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都整备车间')) |
                                (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆北整备车间')) |
                                (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳整备车间'))
                                ]
    person_data = person_data[(person_data['ALL_NAME'].str.contains('成都东整备车间')) |
                              (person_data['ALL_NAME'].str.contains('重庆北整备车间')) |
                              (person_data['ALL_NAME'].str.contains('贵阳整备车间'))
                              ]
    result = calc_two_freq_score(problem_data, person_data)
    return result


def get_hyjc(problem_data, person_data):
    """
    货运机车整备车间
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[(problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都北整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('西昌整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆西整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆内燃整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳西整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳南整备车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('六盘水整备车间'))
                                ]
    person_data = person_data[(person_data['ALL_NAME'].str.contains('成都北整备车间'))
                              | (person_data['ALL_NAME'].str.contains('西昌整备车间'))
                              | (person_data['ALL_NAME'].str.contains('重庆西整备车间'))
                              | (person_data['ALL_NAME'].str.contains('重庆内燃整备车间'))
                              | (person_data['ALL_NAME'].str.contains('贵阳西整备车间'))
                              | (person_data['ALL_NAME'].str.contains('贵阳南整备车间'))
                              | (person_data['ALL_NAME'].str.contains('六盘水整备车间'))
                              ]
    result = calc_two_freq_score(problem_data, person_data)
    return result


def get_jx(problem_data, person_data):
    """
    检修车间
    :param problem_data:
    :param person_data:
    :return:
    """
    problem_data = problem_data[(problem_data['CHECK_ADDRESS_NAMES'].str.contains('成都检修车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('峨眉检修车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('解体组装车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆西检修车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('重庆大部件检修车间'))
                                | (problem_data['CHECK_ADDRESS_NAMES'].str.contains('贵阳检修车间'))
                                ]
    person_data = person_data[(person_data['ALL_NAME'].str.contains('成都检修车间'))
                              | (person_data['ALL_NAME'].str.contains('峨眉检修车间'))
                              | (person_data['ALL_NAME'].str.contains('解体组装车间'))
                              | (person_data['ALL_NAME'].str.contains('重庆西检修车间'))
                              | (person_data['ALL_NAME'].str.contains('重庆大部件检修车间'))
                              | (person_data['ALL_NAME'].str.contains('贵阳检修车间'))
                              ]
    result = calc_two_freq_score(problem_data, person_data)
    return result


# ----------------------------------------------------------------第二部分----------------------------------------------
def get_second(last_year_start, last_year_end):
    """
    检查问题分析
    :param last_year_start:
    :param last_year_end:
    :return:
    """
    last_problem_data = pd_query(CHECK_PROBLEM_FREQ_SCORE_SQL.format(last_year_start, last_year_end))
    last_problem_data['NEW_NAME'] = last_problem_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    # 问题基本情况
    problem_base = get_problem_base(problem_data, last_problem_data)
    # 各机务段自查问题数对比分析
    second_two = get_second_two(problem_data, last_problem_data)
    # 各机务段自查问题质量分对比分析
    freq_score = get_freq_score(problem_data, person_data)
    return {
        'problem_base': problem_base,
        'second_two': second_two,
        'freq_score': freq_score
    }


def get_problem_base(data, last_problem_data):
    """
    问题基本情况
    :param data:
    :param last_problem_data:
    :return:
    """
    # 发现问题
    find_problem = int(data['PROBLEM_NUMBER'].sum())
    # 同比
    last_find = int(last_problem_data['PROBLEM_NUMBER'].sum())
    ratio = calc_year_ratio([find_problem, last_find])
    problem_list = ['作业', '设备', '管理', '路外', '反恐']
    data = data.dropna()
    count = []
    for i in problem_list:
        count.append(int(data[data['PROBLEM_CLASSITY_NAME'].str.contains(i)]['PROBLEM_NUMBER'].sum()))
    # 性质严重问题
    serious = int(data[data['RISK_LEVEL'].isin([1, 2])]['PROBLEM_NUMBER'].sum())
    s_ratio = round(ded_zero(serious, find_problem) * 100, 1)
    # 红线问题
    red = int(data[data['IS_RED_LINE'].isin([1, 2])]['PROBLEM_NUMBER'].sum())
    return {
        'find_problem': find_problem,
        'ratio': ratio,
        'count': count,
        'serious': serious,
        's_ratio': s_ratio,
        'red': red
    }


def get_second_two(data, last_problem_data):
    """
    各机务段自查问题数对比分析
    :param problem_data:
    :param last_problem_data:
    :return:
    """
    data = data[(data['DEPARTMENT_ALL_NAME'] == data['CHECK_ADDRESS_NAMES']) & (data['PROBLEM_NUMBER'] != 0)]
    last_problem_data = last_problem_data[
        (last_problem_data['DEPARTMENT_ALL_NAME'] == last_problem_data['CHECK_ADDRESS_NAMES']) & (
                last_problem_data['PROBLEM_NUMBER'] != 0)]
    # 自查问题数
    zicha = get_zicha(data, last_problem_data)
    # 自查作业项问题数
    zc_work = get_zc_work(data, last_problem_data)
    # 自查管理项问题数
    zc_manager = get_zc_manager(data, last_problem_data)
    # 自查设备项问题数
    zc_shebei = get_zc_shebei(data, last_problem_data)
    # 自查严重问题数
    zc_main_problem = get_zc_main_problem(data, last_problem_data)
    return {
        'zicha': zicha,
        'zc_work': zc_work,
        'zc_manager': zc_manager,
        'zc_shebei': zc_shebei,
        'zc_main_problem': zc_main_problem
    }


def get_zicha(data, last_problem_data):
    """
    自查问题数
    :param data:
    :param last_problem_data:
    :return:
    """
    return calc_zc_problem(data, last_problem_data)


def get_zc_work(data, last_problem_data):
    """
    自查作业项问题
    :param data:
    :param last_problem_data:
    :return:
    """
    data = data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')]
    last_problem_data = last_problem_data[last_problem_data['PROBLEM_CLASSITY_NAME'].str.contains('作业')]
    return calc_zc_problem(data, last_problem_data)


def get_zc_manager(data, last_problem_data):
    """
    自查管理项问题数
    :param data:
    :param last_problem_data:
    :return:
    """
    data = data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')]
    last_problem_data = last_problem_data[last_problem_data['PROBLEM_CLASSITY_NAME'].str.contains('管理')]
    return calc_zc_problem(data, last_problem_data)


def get_zc_shebei(data, last_problem_data):
    """
    自查设备项问题数
    :param data:
    :param last_problem_data:
    :return:
    """
    data = data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')]
    last_problem_data = last_problem_data[last_problem_data['PROBLEM_CLASSITY_NAME'].str.contains('设备')]
    return calc_zc_problem(data, last_problem_data)


def get_zc_main_problem(data, last_problem_data):
    """
    自查严重问题数
    :param data:
    :param last_problem_data:
    :return:
    """
    data = data[data['RISK_LEVEL'].isin([1, 2])]
    last_problem_data = last_problem_data[last_problem_data['RISK_LEVEL'].isin([1, 2])]
    return calc_zc_problem(data, last_problem_data)


def get_freq_score(problem_data, person_data):
    """
    各机务段自查问题质量分对比分析
    :param last_problem_data:
    :param person_data:
    :return:
    """
    station_list = ['成都机务段', '西昌机务段', '重庆机务段', '贵阳机务段']
    all_list = []
    for i in station_list:
        data = problem_data[problem_data['NEW_NAME'] == i]
        person = person_data[person_data['NEW_NAME'] == i]
        # 问题总质量分
        score = int(data['ALL_SCORE'].sum())
        # 问题数
        problem_count = int(data['PROBLEM_NUMBER'].sum())
        # 问题平均质量分
        avg_score = round(ded_zero(score, problem_count), 1)
        # 换算单位人均质量分
        avg_person = round(ded_zero(score, len(person)), 1)
        dic = {
            'name': i,
            'score': score,
            'avg_score': avg_score,
            'avg_person': avg_person
        }
        all_list.append(dic)
    l1 = sorted(all_list, key=lambda x: x['avg_score'], reverse=True)
    l2 = sorted(all_list, key=lambda x: x['avg_person'], reverse=True)
    return {
        'all_list': all_list,
        'l1': l1,
        'l2': l2
    }


# ---------------------------------------------------------第三部分-------------------------------------------------------------
def get_third(start_date, end_date, last_year_start, last_year_end):
    data = pd_query(CHECK_CWY_SQL.format(start_date, end_date))
    last_data = pd_query(CHECK_CWY_SQL.format(last_year_start, last_year_end))
    all_list = []
    for i in [data, last_data]:
        # 问题总数
        count = int(i['PROBLEM_NUMBER'].sum())
        new_data = i[(i['MONITOR_POST_NAMES'].str.contains('机车乘务员')) & (i['PROBLEM_CLASSITY_NAME'].str.contains('作业'))]
        # 问题个数
        problem_count = int(new_data['PROBLEM_NUMBER'].sum())
        # 占比
        ratio = round(ded_zero(problem_count, count) * 100, 1)
        # 严重问题
        serious = int(new_data[new_data['RISK_LEVEL'].isin([1, 2])]['PROBLEM_NUMBER'].sum())
        dic = {
            'problem_count': problem_count,
            'ratio': ratio,
            'serious': serious
        }
        all_list.append(dic)
    problem_ratio = calc_year_ratio([all_list[0]['problem_count'], all_list[1]['problem_count']])
    serious_ratio = calc_year_ratio([all_list[0]['serious'], all_list[1]['serious']])
    return {
        'all_list': all_list[0],
        'problem_ratio': problem_ratio,
        'serious_ratio': serious_ratio
    }


# -----------------------------------------------------------第四部分---------------------------------------------------------------
def get_fourth(start_date, end_date, year):
    """
    分析中心作用发挥分析
    :param start_date:
    :param end_date:
    :return:
    """
    evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date))
    evaluate_data['NEW_NAME'] = evaluate_data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    review_data = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date))
    # 履职评价概述
    evaluate_overview = get_evaluate_overview(evaluate_data, review_data)
    # 按职务分
    job_type = get_job_type(evaluate_data, year)
    # 按单位分
    position = get_position(evaluate_data)
    # 按项目分
    project = get_project(evaluate_data, year)
    # 音视频调阅
    mv = get_mv(review_data)

    return {
        'evaluate_overview': evaluate_overview,
        'job_type': job_type,
        'position': position,
        'project': project,
        'mv': mv
    }


def get_evaluate_overview(evaluate_data, review_data):
    """
    履职评价概述
    :param evaluate_data:
    :param review_data:
    :return:
    """
    # 履职评价概述
    # 1.
    info_problem = len(
        evaluate_data[(evaluate_data['EVALUATE_TYPE'].isin([1, 2])) & (evaluate_data['EVALUATE_WAY'] == 1)])
    zt_re = len(review_data[(review_data['IS_REVIEW'] == 1) & (review_data['EVALUATE_WAY'] == 1)])
    if len(review_data[review_data['EVALUATE_WAY'] == 1]) == 0:
        zt_ratio = zt_re
    else:
        zt_ratio = round(ded_zero(zt_re, len(review_data[review_data['EVALUATE_WAY'] == 1])) * 100, 1)
    dq = len(evaluate_data[evaluate_data['EVALUATE_WAY'] == 2])
    dq_re = len(review_data[(review_data['IS_REVIEW'] == 1) & (review_data['EVALUATE_WAY'] == 2)])
    if len(review_data[review_data['EVALUATE_WAY'] == 2]) == 0:
        dq_ratio = len(review_data[(review_data['IS_REVIEW'] == 1) & (review_data['EVALUATE_WAY'] == 2)])
    else:
        dq_ratio = round(ded_zero(dq_re, len(review_data[review_data['EVALUATE_WAY'] == 2])) * 100, 1)
    # 2.计算问题累计分数
    dp_data = evaluate_data[evaluate_data['IDENTITY'].str.contains('干部')]
    dp_person = len(dp_data)
    dp_p = len(dp_data['ID_CARD'].value_counts())
    dp_score = int(dp_data['SCORE'].sum())
    dp_count = [0, 0, 0, 0, 0, 0, 0]
    up_8_name = []
    up_6_name = []
    for i in dp_data['ID_CARD'].value_counts().index:
        data = dp_data[dp_data['ID_CARD'] == i]
        s = int(data['SCORE'].sum())
        if s < 1:
            dp_count[0] = dp_count[0] + 1
        elif s < 2:
            dp_count[1] = dp_count[1] + 1
        elif s < 3:
            dp_count[2] = dp_count[2] + 1
        elif s < 4:
            dp_count[3] = dp_count[3] + 1
        elif s < 6:
            dp_count[4] = dp_count[4] + 1
        elif s < 8:
            up_6_name.append({
                'name': data['ALL_NAME'].tolist()[0],
                'person_name': data['RESPONSIBE_PERSON_NAME'].tolist()[0],
                'score': s})
            dp_count[5] = dp_count[5] + 1
        else:
            up_8_name.append({
                'name': data['ALL_NAME'].tolist()[0],
                'person_name': data['RESPONSIBE_PERSON_NAME'].tolist()[0],
                'score': s
            })
            dp_count[6] = dp_count[6] + 1
    luju = len(dp_data[dp_data['CHECK_TYPE'] == 1])
    luju_ratio = round(ded_zero(luju, dp_person) * 100, 1)
    return {
        'info_problem': info_problem,
        'zt_re': zt_re,
        'zt_ratio': zt_ratio,
        'dq': dq,
        'dq_re': dq_re,
        'dq_ratio': dq_ratio,
        'dp_person': dp_person,
        'dp_p': dp_p,
        'dp_score': dp_score,
        'dp_count': dp_count,
        'luju': luju,
        'luju_ratio': luju_ratio,
        'up_8_name': up_8_name,
        'up_6_name': up_6_name
    }


def get_job_type(evaluate_data, year):
    """
    按职务分类
    :param evaluate_data:
    :param year:
    :return:
    """
    data = evaluate_data[evaluate_data['IDENTITY'] == '干部']
    # 正处级
    zc = len(data[data['LEVEL'].str.contains('正处级')])
    # 副处级
    fc = len(data[data['LEVEL'].str.contains('副处级')])
    # 正科级
    zk = len(data[data['LEVEL'].str.contains('正科级')])
    # 副科级
    fk = len(data[data['LEVEL'].str.contains('副科级')])
    # 一般干部
    general = len(data[data['LEVEL'].str.contains('一般管理')])
    # 绘图
    data = [zc, fc, zk, fk, general]
    labels = ['正处级', '副处级', '正科级', '副科级', '一般干部']
    images = pie_picture(data, labels, '机务系统评价干部级别分布图', 'major_' + str(year), '机务', 'annual')
    return {
        'zc': zc,
        'fc': fc,
        'zk': zk,
        'fk': fk,
        'general': general,
        'images': images
    }


def get_position(evaluate_data):
    """
    按单位分
    :param evaluate_data:
    :return:
    """
    count = []
    station_name = [i for i in evaluate_data['NEW_NAME'].value_counts().index]
    for i in station_name:
        count.append(len(evaluate_data[evaluate_data['NEW_NAME'] == i]))
    return {
        'station_name': station_name,
        'count': count
    }


def get_project(evaluate_data, year):
    """
    按项目分类
    :param evaluate_data:
    :return:
    """
    project_list = ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实',
                    '音视频运用管理', '事故故障追溯', '履职评价管理', '弄虚作假']
    count = []
    for i in project_list:
        data = evaluate_data[evaluate_data['ITEM_NAME'].str.contains(i)]
        count.append(len(data))
    # 绘图
    images = barh_picture(project_list, [count], ['问题数'], '评价项目分布图', 'major_' + str(year), '机务', 'annual')
    return {
        'project_list': project_list,
        'count': count,
        'images': images
    }


def get_mv(review_data):
    """
    音视频调阅
    :param review_data:
    :return:
    """
    data = review_data[review_data['EVALUATE_TYPE'] == 3]
    # 音视频调阅
    mv = len(data)
    # 机务分析室
    jw = len(data[data['ALL_NAME'].str.contains('机务分析室')])
    jw_num = int(data[data['ALL_NAME'].str.contains('机务分析室')]['PROBLEM_NUMBER'].sum())
    # 安全分析中心
    safety_center = len(data[data['ALL_NAME'].str.contains('安全分析中心')])
    safety_num = int(data[data['ALL_NAME'].str.contains('安全分析中心')]['PROBLEM_NUMBER'].sum())
    # 评价人数
    person = len(data['CHECK_PERSON_ID_CARD'].value_counts())
    return {
        'mv': mv,
        'jw': jw,
        'jw_num': jw_num,
        'safety_center': safety_center,
        'safety_num': safety_num,
        'person': person
    }
