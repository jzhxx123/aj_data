from app.data.util import pd_query
from app.report.jiwu.major_monthly_report_sql import *
import datetime
from dateutil.relativedelta import relativedelta
import threading

locals_data = threading.local()

def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def get_data(year, month):
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    locals_data.year = int(year)
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    # 站段名
    station_name_data = pd_query(STATION_LIST_SQL)['NAME']
    # 责任事故信息
    duty_data = pd_query(without_accident_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_month_start, last_month_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_month_start, last_month_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 履职评价
    eva_data = pd_query(check_evaluate_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # last_eva = pd_query(check_evaluate_sql.format(last_month_start, last_month_end))
    accrued_eva = pd_query(check_evaluate_sql.format(str(year-1)+'-12-25', end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职评价工作量
    eva_re_data = pd_query(check_evaluate_review_sql.format(start_date, end_date))
    last_eva_re = pd_query(check_evaluate_review_sql.format(last_month_start, last_month_end))
    # 履职复查
    eva_re = pd_query(eva_review_sql.format(start_date, end_date))
    last_re = pd_query(eva_review_sql.format(last_month_start, last_month_end))
    # 违章大王信息
    iiilege_data = pd_query(CHECK_IIILEGE_SQL.format(year, month))

    top = get_top(duty_data)
    first = get_first(duty_data)
    second = get_second(info_data, problem_data, eva_data, eva_re_data, last_eva_re, accrued_eva, eva_re, last_re)
    third = get_third(info_data, last_info, problem_data, last_problem)
    four = get_four(iiilege_data)
    five = get_five(problem_data, last_problem)
    jws = get_jws(problem_data)
    file_name = f'{start_date}至{end_date}机务系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '机务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'top': top,
        "first": first,
        "second": second,
        'third': third,
        'four': four,
        'five': five,
        'jws': jws
    }
    return result


def get_top(duty_data):
    """
    顶部安全事故信息
    :param duty_data:
    :return:
    """
    duty = duty_data[(duty_data['MAIN_TYPE'] == 1) & (duty_data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
    return {
        'duty_count': len(duty)
    }


# ---------------------------------------------------------------------第一部分-----------------------------------------------------------------------------
def get_first(duty_data):
    """
    机务系统责任交通事故统计
    :param duty_data:
    :return:
    """
    data = duty_data[(duty_data['MAIN_TYPE'] == 1) & (duty_data['FK_RESPONSIBILITY_DIVISION_ID'] == 667) &
                     (duty_data['DETAIL_TYPE'] == 1)]
    content = data['OVERVIEW'].unique().tolist()
    return {
        'count': len(data),
        "content": content,
    }


# ---------------------------------------------------------------------第二部分-----------------------------------------------------------------------------
def get_second(info_data, problem_data, eva_data, eva_re_data, last_eva_re, accrued_eva, eva_re, last_re):
    """
    干部履职评价情况
    :param info_data:
    :param last_info:
    :param problem_data:
    :param last_problem:
    :param eva_data:
    :param last_eva:
    :param eva_re_data:
    :param last_eva_re:
    :return:
    """
    # 基本情况
    eva_base_sit = get_eva_base_situation(info_data, problem_data, eva_data, eva_re_data, last_eva_re, eva_re, last_re)
    # 记分情况
    scored_sit = get_scored_situation(eva_data)
    # 表格信息
    table = get_second_table(eva_data)
    # 累计记分情况
    accrued_score = get_accrued_score(accrued_eva)
    # 干部履职
    gb_eva = get_gb_eva(eva_data)
    return {
        'eva_base_sit': eva_base_sit,
        'scored_sit': scored_sit,
        'table': table,
        'accrued_score': accrued_score,
        'gb_eva': gb_eva
    }


def get_eva_base_situation(info_data, problem_data, eva_data, eva_re_data, last_eva_re, eva_re, last_re):
    """
    评价、复查基本情况
    :param info_data:
    :param problem_data:
    :param eva_data:
    :param eva_re_data:
    :param last_eva_re:
    :return:
    """
    # 检查信息数
    info_count = len(info_data)
    # 发现问题数
    find_pro = len(problem_data)
    # 逐条评价+复查
    zt_and_re = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 1]) + len(eva_re[eva_re['EVALUATE_WAY'] == 1])
    last_zt_re = len(last_eva_re[last_eva_re['EVALUATE_WAY'] == 1]) + len(last_re[last_re['EVALUATE_WAY'] == 1])
    ratio1 = round(ded_zero(zt_and_re, info_count + find_pro) * 100, 2)
    ratios1 = calculate_month_and_ring_ratio([zt_and_re, last_zt_re])['month_percent']
    # 分析室逐条评价
    fxs_zt = len(eva_data[(eva_data['EVALUATE_WAY'] == 1) & (eva_data['CHECK_TYPE'] == 1)])
    # 站段分析中心逐条评价
    fxzx_zt = len(eva_data[(eva_data['EVALUATE_WAY'] == 1) & (eva_data['CHECK_TYPE'] == 2)])
    # 逐条复查
    zt_re = len(eva_re[eva_re['EVALUATE_WAY'] == 1])
    last_zt_re = len(last_re[last_re['EVALUATE_WAY'] == 1])
    ratio2 = round(ded_zero(zt_re, len(eva_re_data)) * 100, 2)
    ratios2 = round(ded_zero(zt_re, len(eva_re_data)) * 100, 2) - round(ded_zero(last_zt_re, len(last_eva_re)) * 100, 2)
    # 定期评价人
    dq = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    dq_re = len(eva_re[eva_re['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    last_dq_re = len(last_re[last_re['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    ratio3 = round(ded_zero(dq_re, dq) * 100, 2)
    ratios3 = calculate_month_and_ring_ratio([dq_re, last_dq_re])['month_percent']
    return {
        'info_count': info_count,
        'find_pro': find_pro,
        'zt_and_re': zt_and_re,
        'ratio1': ratio1,
        'ratios1': ratios1,
        'ratio2': ratio2,
        'ratios2': ratios2,
        'fxs_zt': fxs_zt,
        'fxzx_zt': fxzx_zt,
        'zt_re': zt_re,
        'dq': dq,
        'dq_re': dq_re,
        'ratio3': ratio3,
        'ratios3': ratios3,
    }


def get_scored_situation(eva_data):
    """
    记分情况
    :param eva_data:
    :return:
    """
    # 干部记分人次
    gb_data = eva_data[eva_data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
    count = len(gb_data)
    person = len(gb_data['RESPONSIBE_ID_CARD'].unique())
    # 合计记分
    score = round(float(gb_data['SCORE'].sum()), 4)
    # 单月记分人次
    counts = [0, 0]
    for i, j in gb_data.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE',
                                                                        ascending=False).reset_index().iterrows():
        if j['SCORE'] >= 3:
            counts[0] += 1
        elif j['SCORE'] >= 2:
            counts[1] += 1
    # 职务分布
    gradations = []
    for i in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员']:
        gradations.append(len(eva_data[eva_data['GRADATION'] == i]))
    # 单位分布
    stations = []
    for i in ['重庆机务段', '成都机务段', '贵阳机务段', '西昌机务段', '机务部']:
        stations.append(len(eva_data[eva_data['STATION'] == i]))
    # 检查类型
    luju = len(eva_data[eva_data['CHECK_TYPE'] == 1])
    zd_data = eva_data[eva_data['CHECK_TYPE'] == 2]
    zds = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
        zds.append(len(zd_data[zd_data['STATION'] == i]))
    # 评价类型分布
    zt = len(eva_data[eva_data['EVALUATE_WAY'] == 1])
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2])
    jd = len(eva_data[eva_data['EVALUATE_WAY'] == 3])
    return {
        'all_count': len(eva_data),
        'all_person': len(eva_data['RESPONSIBE_ID_CARD'].unique()),
        'count': count,
        'person': person,
        'score': score,
        'counts': counts,
        'gradations': gradations,
        'stations': stations,
        'luju': luju,
        'zd': len(zd_data),
        'zds': zds,
        'zt': zt,
        'dq': dq,
        'jd': jd
    }


def get_second_table(eva_data):
    # 记分项目统计表
    score_items_name = []
    score_items_count = []
    score_items_ratio = []
    eva_data['ITEM'] = eva_data['ITEM_NAME'].apply(lambda x: x.strip().replace('。', ''))
    eva_data['SIT'] = eva_data['SITUATION'].apply(lambda x: x.strip().replace('。', ''))
    for i, j in eva_data.groupby('ITEM').count().sort_values('GRADATION', ascending=False).reset_index()[:7].iterrows():
        name = j['ITEM']
        count = int(j['GRADATION'])
        score_items_name.append(name)
        score_items_count.append(count)
    for i in score_items_count:
        score_items_ratio.append(round(ded_zero(i, sum(score_items_count)) * 100, 2))
    # 突出问题统计表
    issue_pro_name = []
    issue_pro_count = []
    issue_pro_ratio = []
    for i, j in eva_data.groupby('SIT').count().sort_values('GRADATION', ascending=False).reset_index()[:6].iterrows():
        name = j['SIT']
        count = int(j['GRADATION'])
        issue_pro_name.append(name)
        issue_pro_count.append(count)
    for i in issue_pro_count:
        issue_pro_ratio.append(round(ded_zero(i, len(eva_data['RESPONSIBE_ID_CARD'].unique())) * 100, 2))
    # 各段履职问题分析
    stations = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段', '机务部']:
        data = eva_data[eva_data['STATION'] == i]
        counts = []
        for j in ['考核责任落实', '量化指标完成', '检查信息录入', '重点工作落实', '问题闭环管理', '监督检查质量', '事故故障追溯']:
            counts.append(len(data[data['ITEM'] == j]))
        stations.append(counts)
    # 合计分
    station_score = [0, 0, 0, 0, 0, 0, 0]
    for i in stations:
        for j in range(len(i)):
            station_score[j] += i[j]
    # 记分集中项目
    jfxm = []
    for i in range(3):
        jf_count = []
        for j in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
            data = eva_data[(eva_data['STATION'] == j) & (eva_data['ITEM'].str.contains(score_items_name[i]))]
            jf_count.append(len(data))
        dic = {
            'name': score_items_name[i],
            'count': score_items_count[i],
            'ratio': round(ded_zero(score_items_count[i], sum(score_items_count)) * 100, 2),
            'station': jf_count
        }
        jfxm.append(dic)
    return {
        'score_items_name': score_items_name,
        'score_items_count': score_items_count,
        'score_items_ratio': score_items_ratio,
        'issue_pro_name': issue_pro_name,
        'issue_pro_count': issue_pro_count,
        'issue_pro_ratio': issue_pro_ratio,
        'stations': stations,
        'station_score': station_score,
        'jfxm': jfxm
    }


def get_accrued_score(accrued_eva):
    accrued_eva = accrued_eva[(accrued_eva['FK_PERSON_GRADATION_RATIO_ID'] != 5) & (accrued_eva['YEAR'] == locals_data.year)]
    # 记分人次
    counts = len(accrued_eva)
    # 记分人数
    person = len(accrued_eva['RESPONSIBE_ID_CARD'].unique())
    # 总记分
    score = int(accrued_eva['SCORE'].sum())
    count = [0, 0, 0, 0, 0, 0, 0]
    persons = []
    for i, j in accrued_eva.groupby('RESPONSIBE_ID_CARD').sum().sort_values('SCORE', ascending=False).reset_index().iterrows():
        if j['SCORE'] >= 8:
            count[0] += 1
        elif j['SCORE'] >= 6:
            count[1] += 1
        elif j['SCORE'] >= 4:
            count[2] += 1
        elif j['SCORE'] >= 3:
            count[3] += 1
        elif j['SCORE'] >= 2:
            count[4] += 1
        elif j['SCORE'] >= 1:
            count[5] += 1
        else:
            count[6] += 1
        if j['SCORE'] >= 8:
            data = accrued_eva[accrued_eva['RESPONSIBE_ID_CARD'] == j['RESPONSIBE_ID_CARD']]
            dic = {
                'name': list(data['RESPONSIBE_PERSON_NAME'])[0],
                'station': list(data['ALL_NAME'])[0],
                'score': round(float(data['SCORE'].sum()), 4)
            }
            persons.append(dic)
        else:
            continue
    return {
        'counts': counts,
        'person': person,
        'score': score,
        'count': count,
        'persons': persons
    }


def get_gb_eva(eva_data):
    eva_data = eva_data[eva_data['FK_PERSON_GRADATION_RATIO_ID'] != 5]
    # 局管领导人员
    ju = eva_data[eva_data['GRADATION'] == '局管领导人员']
    ju_count = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
        data = ju[ju['STATION'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        ju_count.append(dic)
    # 存在问题
    ju_exist_pro = []
    for i, j in ju.groupby('ITEM_NAME').count().sort_values('GRADATION', ascending=False).reset_index()[:3].iterrows():
        dic = {
            'name': j['ITEM_NAME'],
            'count': int(j['GRADATION'])
        }
        ju_exist_pro.append(dic)
    # 正科
    zk = eva_data[eva_data['GRADATION'] == '正科职管理人员']
    zk_count = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段', '机务部']:
        data = zk[zk['STATION'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        zk_count.append(dic)
    # 存在问题
    zk_exist_pro = []
    for i, j in zk.groupby('ITEM_NAME').count().sort_values('GRADATION', ascending=False).reset_index()[:3].iterrows():
        dic = {
            'name': j['ITEM_NAME'],
            'count': int(j['GRADATION'])
        }
        zk_exist_pro.append(dic)
    # 副科及以下
    fk = eva_data[eva_data['FK_PERSON_GRADATION_RATIO_ID'].isin([3, 4])]
    fk_count = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段', '机务部']:
        data = fk[fk['STATION'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        fk_count.append(dic)
    # 存在问题
    fk_exist_pro = []
    for i, j in fk.groupby('ITEM_NAME').count().sort_values('GRADATION', ascending=False).reset_index()[:3].iterrows():
        dic = {
            'name': j['ITEM_NAME'],
            'count': int(j['GRADATION'])
        }
        fk_exist_pro.append(dic)
    return {
        'ju': len(ju),
        'ju_count': ju_count,
        'ju_exist_pro': ju_exist_pro,
        'zk': len(zk),
        'zk_count': zk_count,
        'zk_exist_pro': zk_exist_pro,
        'fk': len(fk),
        'fk_count': fk_count,
        'fk_exist_pro': fk_exist_pro
    }


def get_third(info_data, last_info, problem_data, last_problem):
    # 总体情况
    overall = get_third_overall(info_data, last_info, problem_data, last_problem)
    # 机务处检查情况
    jwc = get_third_jwc(info_data, problem_data)
    return {
        'overall': overall,
        'jwc': jwc
    }


def get_third_overall(info_data, last_info, problem_data, last_problem):
    # 检查次数
    check_count = len(info_data)
    ratio1 = calculate_month_and_ring_ratio([check_count, len(last_info)])
    # 发现问题
    find_pro = len(problem_data)
    ratio2 = calculate_month_and_ring_ratio([find_pro, len(last_problem)])
    # 问题分类
    work = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    ratio3 = round(ded_zero(work, find_pro) * 100, 2)
    all_pro = []
    for i in ['E', 'F', 'G', 'K']:
        data = problem_data[problem_data['LEVEL'].str.contains(i)]
        dic = {
            'count': len(data),
            'ratio': round(ded_zero(len(data), find_pro) * 100, 2)
        }
        all_pro.append(dic)
    # 性质严重问题
    main_data = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    main_ratio = round(ded_zero(len(main_data), len(problem_data)) * 100, 2)
    main_count = []
    for i in [['A', 'B'], ['E1', 'E2'], ['F1', 'F2']]:
        data = main_data[main_data['LEVEL'].isin(i)]
        dic = {
            'count': len(data),
            'ratio': round(ded_zero(len(data), len(main_data)) * 100, 2)
        }
        main_count.append(dic)
    # 红线问题
    red_data = problem_data[problem_data['IS_RED_LINE'].isin([1, 2])]
    red_count = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
        data = red_data[red_data['NAME'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        red_count.append(dic)
    return {
        'check_count': check_count,
        'ratio1': ratio1,
        'find_pro': find_pro,
        'ratio2': ratio2,
        'work': work,
        'ratio3': ratio3,
        'all_pro': all_pro,
        'main': len(main_data),
        'main_ratio': main_ratio,
        'main_count': main_count,
        'red': len(red_data),
        'red_count': red_count
    }


def get_third_jwc(info_data, problem_data):
    info_data = info_data[info_data['NAME'] == '机务部']
    problem_data = problem_data[problem_data['NAME'] == '机务部']
    # 下现场检查
    xxc = len(info_data[info_data['CHECK_WAY'].isin([1, 2])])
    # 现场
    xc = len(info_data[info_data['CHECK_WAY'] == 1])
    # 添乘
    tc = len(info_data[info_data['CHECK_WAY'] == 2])
    # 设备监控调阅
    shebei = len(info_data[info_data['CHECK_WAY'].isin([3, 4])])
    # 发现问题
    pro = len(problem_data)
    # 作业
    work = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    # 设备
    sb = len(problem_data[problem_data['LEVEL'].str.contains('E')])
    # 管理
    manager = len(problem_data[problem_data['LEVEL'].str.contains('F')])
    return {
        'xxc': xxc,
        'xc': xc,
        'tc': tc,
        'shebei': shebei,
        'pro': pro,
        'work': work,
        'sb': sb,
        'manager': manager
    }


def get_four(iiilege_data):
    iiilege_data = iiilege_data.drop_duplicates(subset=['ID_CARD'])
    # 违章大王人数
    person = len(iiilege_data)
    # 职务分类
    jobs = []
    for i, j in iiilege_data.dropna(subset=['JOB']).groupby('JOB').count().reset_index().iterrows():
        dic = {
            'job': j['JOB'],
            'count': int(j['ID_CARD'])
        }
        jobs.append(dic)
    # 按段分
    stations = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
        data = iiilege_data[iiilege_data['NAME'] == i]
        stations.append(len(data))
    return {
        'person': person,
        'jobs': jobs,
        'stations': stations
    }


def get_five(problem_data, last_problem):
    # 前5项点
    problem_point = []
    for i, j in problem_data.groupby('PROBLEM_POINT').count().sort_values('LEVEL').reset_index()[:5].iterrows():
        dic = {
            'name': j['PROBLEM_POINT'],
            'count': int(j['LEVEL'])
        }
        problem_point.append(dic)
    # 间断瞭望
    jdlw = get_jdlw(problem_data, last_problem)
    # 劳动安全隐患
    ldaq = get_ldaq(problem_data, last_problem)
    # 乘务员错误操纵问题
    cwcz = get_cwcz(problem_data, last_problem)
    # 防溜风险突出
    flfx = get_flfx(problem_data)
    # 机车漏检漏修问题突出
    ljlx = get_ljlx(problem_data, last_problem)
    return {
        'problem_point': problem_point,
        'jdlw': jdlw,
        'ldaq': ldaq,
        'cwcz': cwcz,
        'ljlx': ljlx,
        'flfx': flfx
    }


def get_jdlw(pslc_data, last_pslc):
    """
    普速列车间断瞭望
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    jdlw_data = pslc_data[
        (pslc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (pslc_data['PROBLEM_POINT'].str.contains('间歇打盹'))]
    last_jdlw = last_pslc[
        (last_pslc['PROBLEM_POINT'].str.contains('间断瞭望')) | (last_pslc['PROBLEM_POINT'].str.contains('间歇打盹'))]
    jdlw_count = len(jdlw_data)
    ratio2 = calculate_month_and_ring_ratio([jdlw_count, len(last_jdlw)])
    # 按段分
    jdlw_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = jdlw_data[jdlw_data['NAME'] == i]
        jdlw_stations.append(len(data))
    # 调车作业（机车出入库、出入所）间断瞭望
    dcaq_data = jdlw_data[jdlw_data['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    last_dcaq = last_jdlw[last_jdlw['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    ratio = calculate_month_and_ring_ratio([len(dcaq_data), len(last_dcaq)])
    dcaq_content = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        dcaq_content.append(len(dcaq_data[dcaq_data['NAME'] == i]))
    # 动车组司机间断瞭望
    dcyy = jdlw_data[jdlw_data['SHOP'] == '动车运用车间']
    last_dcyy = last_jdlw[last_jdlw['SHOP'] == '动车运用车间']
    ratio1 = calculate_month_and_ring_ratio([len(dcyy), len(last_dcyy)])
    dcyy_content = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        dcyy_content.append(len(dcaq_data[dcaq_data['NAME'] == i]))
    return {
        'jdlw_count': jdlw_count,
        'ratio2': ratio2,
        'jdlw_stations': jdlw_stations,
        'ratio': ratio,
        'dcaq': len(dcaq_data),
        'dcaq_content': dcaq_content,
        'dcyy': len(dcyy),
        'dcyy_content': dcyy_content,
        'ratio1': ratio1
    }


def get_ldaq(pslc_data, last_pslc):
    """
    普速列车劳动安全
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    pslc_data = pslc_data.dropna(subset=['RISK_NAMES'])
    last_pslc = pslc_data.dropna(subset=['RISK_NAMES'])
    ldaq_data = pslc_data[pslc_data['RISK_NAMES'].str.contains('机务-劳动安全')]
    last_ldaq = last_pslc[last_pslc['RISK_NAMES'].str.contains('机务-劳动安全')]
    # 严重问题
    main_ldaq = ldaq_data[ldaq_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    main_last_ldaq = last_ldaq[last_ldaq['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    ratio1 = calculate_month_and_ring_ratio([len(main_ldaq), len(main_last_ldaq)])
    ratio = calculate_month_and_ring_ratio([len(ldaq_data), len(last_ldaq)])
    ldaq_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = ldaq_data[ldaq_data['NAME'] == i]
        ldaq_stations.append(len(data))
    return {
        'ldaq_count': len(ldaq_data),
        'main_ldaq': len(main_ldaq),
        'ratio': ratio,
        'ratio1': ratio1,
        'ldaq_stations': ldaq_stations
    }


def get_cwcz(pslc_data, last_pslc):
    """
    普速列车乘务员错误操作
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    cwcz_data = pslc_data[(pslc_data['RISK_NAMES'].str.contains('乘务员')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    last_cwcz = last_pslc[(last_pslc['RISK_NAMES'].str.contains('乘务员')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    ratio = calculate_month_and_ring_ratio([len(cwcz_data), len(last_cwcz)])
    # 严重问题
    main_data = cwcz_data[cwcz_data['LEVEL'].isin(['A', 'B'])]
    last_main = last_cwcz[last_cwcz['LEVEL'].isin(['A', 'B'])]
    ratio1 = calculate_month_and_ring_ratio([len(main_data), len(last_main)])
    # 分类
    count = []
    for i in ['操纵不当造成监控装置', '错误关闭劈相机', '错误使用车位键调整距离', '机车试验不当造成机车移动', '未进行制动机简略试验']:
        data = main_data[main_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'cwcz_count': len(cwcz_data),
        'ratio': ratio,
        'main_count': len(main_data),
        'ratio1': ratio1,
        'count': count
    }


def get_flfx(pslc_data):
    """
    普速列车调车安全隐患
    :param pslc_data:
    :return:
    """
    count = []
    # 试验不当发生移动
    pslc_data = pslc_data.dropna(subset=['PROBLEM_POINT'])
    for i in ['试验不当发生移动', '起车不当发生移动', '防溜措施执行不到位且未发生移动', '防溜措施执行不到位发生移动']:
        data = pslc_data[pslc_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'count': count
    }


def get_ljlx(pslc_data, last_pslc):
    """
    普速列车漏检漏修问题
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    data = pslc_data[(pslc_data['SHOP'].str.contains('检修车间')) | (pslc_data['SHOP'].str.contains('整备车间'))
                          | (pslc_data['SHOP'].str.contains('解体组装'))]
    ljlx_data = data[(data['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写')]
    ljlx_datas = ljlx_data[ljlx_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    last_data = last_pslc[(last_pslc['SHOP'].str.contains('检修车间')) | (last_pslc['SHOP'].str.contains('整备车间'))
                          | (last_pslc['SHOP'].str.contains('解体组装'))]
    last_ljlx = last_data[(last_data['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写')]
    last_ljlxs = last_ljlx[last_ljlx['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    ratio = calculate_month_and_ring_ratio([len(ljlx_datas), len(last_ljlxs)])
    # 按段分类
    ljlx_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = ljlx_datas[ljlx_datas['NAME'] == i]
        ljlx_stations.append(len(data))
    # 行车设备质量问题
    xcsb_data = data[(data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])) & (data['RISK_NAMES'].str.contains('行车设备质量'))]
    last_xcsb = last_data[(last_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])) & (last_data['RISK_NAMES'].str.contains('行车设备质量'))]
    ratio1 = calculate_month_and_ring_ratio([len(xcsb_data), len(last_xcsb)])
    # 按段分
    xcsb_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = xcsb_data[xcsb_data['NAME'] == i]
        xcsb_stations.append(len(data))
    return {
        'ljlx_count': len(ljlx_data),
        'ratio': ratio,
        'ljlx_stations': ljlx_stations,
        'xcsb_count': len(xcsb_data),
        'ratio1': ratio1,
        'xcsb_stations': xcsb_stations
    }


def get_jws(problem_data):
    problem_data = problem_data[(problem_data['TYPE'] == 1) & (problem_data['DEPARTMENT_ALL_NAME'].str.contains('分析室'))]
    data = problem_data[problem_data['CHECK_ITEM_NAME'].str.contains('分析中心检查')]
    all_station = []
    for i in ['重庆机务段', '成都机务段', '西昌机务段', '贵阳机务段']:
        new_data = data[data['NAME'] == i]
        dic = {
            'name': i,
            'content': new_data['DESCRIPTION'].tolist()
        }
        all_station.append(dic)
    return {
        'all_station': all_station
    }

