import datetime
from datetime import datetime as dt
from app.data.util import pd_query
import pandas as pd
from app.report.jiwu.station_semiannual_report_sql import *
from app.report.analysis_report_manager import SemiannualAnalysisReport


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return round(num1 / num2, 1)


def age_interval(num):
    """
    年龄区间
    :param num:
    :return:
    """
    if num == 0:
        return '20岁-30岁'
    elif num == 1:
        return '30岁-40岁'
    elif num == 2:
        return '40岁-50岁'
    else:
        return '50岁-60岁'


def get_data(year, half, station_id):
    date = SemiannualAnalysisReport.get_semiannual_intervals(year, half)
    global last_month_start, last_month_end, last_year_start, last_year_end, station_name, start_month, end_month
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    last_year_start, last_year_end = date[2]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 5
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    global data, last_month_data, last_year_data, all_station_data, cover_data, manager_into_problem_data, \
        last_month_manager_into_problem_data, top_department_data, last_top_data
    # 获取当前月份的数据
    data = pd_query(CHECK_PROBLEM_INFO_SQL.format(start_date, end_date, station_id))
    # 获取上个月的数据
    last_month_data = pd_query(CHECK_PROBLEM_INFO_SQL.format(last_month_start, last_month_end, station_id))
    # 获取去年这个月的数据
    last_year_data = pd_query(CHECK_PROBLEM_INFO_SQL.format(last_year_start, last_year_end, station_id))
    # 覆盖信息
    all_station_data = pd_query(ALL_STATION_NAME_SQL.format(station_id))
    cover_data = pd_query(CHECK_COVER_RATIO_SQL.format(start_date, end_date, station_id))
    # 量化干部录入问题的数据
    manager_into_problem_data = pd_query(ALL_MANAGER_INTO_PROBLEM_SQL.format(start_date, end_date, station_id))
    last_month_manager_into_problem_data = pd_query(
        ALL_MANAGER_INTO_PROBLEM_SQL.format(last_month_start, last_month_end, station_id))
    # 上级干部录入问题数据
    top_department_data = pd_query(TOP_DEPARTMENT_SQL.format(start_date, end_date))
    last_top_data = pd_query(TOP_DEPARTMENT_SQL.format(last_month_start, last_month_end, station_id))

    first = get_first(station_id)
    second = get_evaluate_situation_base_doc(station_id, year)
    third = get_manage_evaluate_problem(start_date, end_date, station_id)
    fourth = get_safety_low_department_analysis(start_date, end_date, station_id)
    sixth = get_safety_tip(start_date, end_date, station_id)
    seventh = get_safety_center_work(start_date, end_date, station_id)
    nineth = get_risk_problem_analysis(start_date, end_date, station_id)
    file_name = f'{start_date}至{end_date}{station_name}安全管理半年度分析.docx'
    result = {
        "year": year,
        'half': half,
        'times': '上半年' if half == 1 else '下半年',
        "start_month": start_month,
        "end_month": end_month,
        "major": '机务',
        "hierarchy": "STATION",
        'station_id': station_id,
        'station_name': station_name,
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'first': first,
        "second_evaluate_analysis": second,
        'third_manage_evaluate_problem': third,
        "fourth_safety_low_department_analysis": fourth,
        "sixth_safety_warning": sixth,
        'seventh_safety_center_work': seventh,
        'nineth_risk_problem_analysis': nineth,
    }
    return result


def calculate_year_and_ring_ratio(counts):
    """
    计算环比和同比
    :param counts:
    :return:
    """
    now_count, year_count, month_count = counts
    year_ratio = now_count - year_count
    if year_count == 0:
        year_percent = year_ratio * 100
    else:
        year_percent = round(year_ratio / year_count, 1) * 100
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'year_diff': year_ratio,
        'year_percent': year_percent,
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# ----------------------------------------------------------------------------------第一点-----------------------------------------------------------------
def get_first(station_id):
    without_accident = pd_query(without_accident_sql.format(station_id))
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    all_times = []
    for i in ['B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
        if len(accident) == 0:
            time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            time = str(dt.now().date() - accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
        all_times.append(time)
    for i in ['死亡', '重伤', '轻伤']:
        hurt = data[data['NAME'].str.contains(i)]
        if len(hurt) == 0:
            hurt_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            hurt_time = str(dt.now().date() - hurt.sort_values(by='OT', ascending=False)['OT'].tolist()[0].date()).split(' ')[0]
        all_times.append(hurt_time)
    return {
        'all_times': all_times
    }


# -------------------------------------------------------第二点---------------------------------------------------------
def get_evaluate_situation_base_doc(station_id, year):
    """
    获取所有检查信息数据
    :param station_id:
    :param year:
    :return:
    """
    # 检查基本情况
    check_basic_situation = _get_check_basic_situation(data, last_month_data, last_year_data, all_station_data,
                                                       cover_data, manager_into_problem_data,
                                                       last_month_manager_into_problem_data,
                                                       top_department_data, last_top_data, station_id, year)
    # 问题统计分析
    problem_statistics_analysis = _get_problem_statistics_analysis(data)
    return {
        'check_basic_situation': check_basic_situation,
        'problem_statistics_analysis': problem_statistics_analysis,
    }


def _get_check_basic_situation(data, last_month_data, last_year_data, all_station_data, cover_data,
                               manager_into_problem_data, last_month_manager_into_problem_data, top_department_data,
                               last_top_data, station_id, year):
    """
    检查基本情况的各个小部分
    :param data:
    :param last_month_data:
    :param last_year_data:
    :param all_station_data:
    :param cover_data:
    :param manager_into_problem_data:
    :param last_month_manager_into_problem_data:
    :param top_department_data:
    :param last_top_data:
    :param station_id:
    :param year:
    :return:
    """
    # 全段总体情况
    all_station_situation = _get_all_station_situation(data, last_month_data, last_year_data, all_station_data,
                                                       cover_data)
    # 量化干部录入情况
    into_cadre_enter_situation = _get_manage_into_problem_situation(manager_into_problem_data,
                                                                    last_month_manager_into_problem_data)
    # 其他管理人员（工班长及分析组）录入情况
    qt_manager = get_qt_manager(manager_into_problem_data, last_month_manager_into_problem_data)
    # 上级部门录入情况
    up_department_into_info_situation = _get_up_department_into_info_situation(top_department_data, last_top_data)
    # 检查通知书录入情况
    check_book = get_check_book(station_id, year)
    return {
        'all_station_situation': all_station_situation,
        'into_cadre_enter_situation': into_cadre_enter_situation,
        'qt_manager': qt_manager,
        'up_department_into_info_situation': up_department_into_info_situation,
        'check_book': check_book,
    }


def get_check_book(station_id, year):
    """
    检查通知书录入情况
    :return:
    """
    data = pd_query(CHECK_BOOK_SQL.format(year, start_month, end_month, station_id))
    count = int(data['CHECK_NOTIFICATION_TIMES'].sum())
    return {
        'count': count
    }


def get_qt_manager(data, last_data):
    """
    其他管理人员（工班长及分析组）录入情况
    :param data:
    :param last_data:
    :return:
    """
    data = data[data['LEVEL'] == '其他']
    last_data = last_data[last_data['LEVEL'] == '其他']
    count = len(data)
    last_count = len(last_data)
    ratio = calculate_month_and_ring_ratio([count, last_count])
    # 分组
    data['NEW_NAME'] = data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    new_data = data.groupby('NEW_NAME').count().reset_index()
    chejian = {}
    for i in range(len(new_data)):
        chejian[new_data.loc[i]['NEW_NAME']] = int(new_data.loc[i]['ALL_NAME'])
    return {
        'count': count,
        'chejian': chejian,
        'ratio': ratio
    }


def _get_up_department_into_info_situation(top_department_data, last_top_data):
    # 录入问题数+环比
    problem_number = len(top_department_data)
    last_problem_number = len(last_top_data)
    problem_ratio = calculate_month_and_ring_ratio([problem_number, last_problem_number])
    # 各个等级的问题
    all_type_problem = _get_problem_table(top_department_data)
    return {
        'problem_number': problem_number,
        'problem_ratio': problem_ratio,
        'all_type_problem': all_type_problem,
    }


def _get_manage_into_problem_situation(manager_into_problem_data, last_month_manager_into_problem_data):
    """
    量化干部录入情况
    :param manager_into_problem_data:
    :param last_month_manager_into_problem_data:
    :return:
    """
    manage_all_info = manager_into_problem_data[manager_into_problem_data['FK_CHECK_OR_PROBLEM_ID'].isin(['NaN'])]
    last_manage_all_info = last_month_manager_into_problem_data[
        last_month_manager_into_problem_data['FK_CHECK_OR_PROBLEM_ID'].isin(['NaN'])]
    manage_into_info_number = len(manage_all_info['FK_CHECK_OR_PROBLEM_ID'])
    last_into_info_number = len(last_manage_all_info['FK_CHECK_OR_PROBLEM_ID'])
    into_info_ratio = calculate_month_and_ring_ratio([manage_into_info_number, last_into_info_number])
    # 发现问题的信息数
    find_problem_info = manage_into_info_number - len(manage_all_info[manage_all_info['PROBLEM_NUMBER'].isin([0])])
    last_find_problem_info = last_into_info_number - len(
        last_manage_all_info[last_manage_all_info['PROBLEM_NUMBER'].isin([0])])
    find_problem_info_ratio = calculate_month_and_ring_ratio([find_problem_info, last_find_problem_info])
    # 发现问题率
    find_problem_ratio = round(ded_zero(find_problem_info, manage_into_info_number) * 100, 1)
    # 夜查信息 + 环比
    yecha_info = len(manage_all_info[manage_all_info['IS_YECHA'].isin([1])])
    last_month_yecha_info = len(last_manage_all_info[last_manage_all_info['IS_YECHA'].isin([1])])
    yecha_info_ratio = calculate_month_and_ring_ratio([yecha_info, last_month_yecha_info])
    # 夜查率
    yecha_ratio = round(ded_zero(yecha_info, manage_into_info_number) * 100, 1)
    # 监控设备回放
    freq = len(manager_into_problem_data[manager_into_problem_data['CHECK_TYPE'] == 104])
    freq_ratio = calculate_month_and_ring_ratio(
        [freq, len(last_month_manager_into_problem_data[last_month_manager_into_problem_data['CHECK_TYPE'] == 104])])
    # 检查发现问题 + 环比
    check_find_problem = int(manage_all_info['PROBLEM_NUMBER'].sum())
    last_month_check_find_problem = int(last_manage_all_info['PROBLEM_NUMBER'].sum())
    check_find_problem_ratio = calculate_month_and_ring_ratio([check_find_problem, last_month_check_find_problem])
    # 考核数 +环比
    assess_problem_number = len(manage_all_info[manage_all_info['IS_ASSESS'].isin([1])])
    last_month_assess_problem_number = len(last_manage_all_info[last_manage_all_info['IS_ASSESS'].isin([1])])
    assess_problem_ratio = calculate_month_and_ring_ratio(
        [assess_problem_number, last_month_assess_problem_number])
    # 考核率
    assess_ratio = round(ded_zero(assess_problem_number, manage_into_info_number) * 100, 1)

    return {
        'manage_into_info_number': manage_into_info_number,
        'into_info_ratio': into_info_ratio,
        'find_problem_info': find_problem_info,
        'find_problem_info_ratio': find_problem_info_ratio,
        'find_problem_ratio': find_problem_ratio,
        'yecha_info': yecha_info,
        'yecha_info_ratio': yecha_info_ratio,
        'yecha_ratio': yecha_ratio,
        'check_find_problem': check_find_problem,
        'check_find_problem_ratio': check_find_problem_ratio,
        'assess_problem_number': assess_problem_number,
        'assess_problem_ratio': assess_problem_ratio,
        'assess_ratio': assess_ratio,
        'freq': freq,
        'freq_ratio': freq_ratio
    }


def _get_station_cover(all_station_data, cover_data):
    """
    检查点覆盖查询
    :param all_station_data:
    :param cover_data:
    :return:
    """
    all_station = all_station_data.groupby('ALL_NAME').count().reset_index()
    all_station_number = int(all_station['ALL_NAME'].count())
    cover_station = cover_data.groupby('DEPARTMENT_ALL_NAME').count().reset_index()
    cover_station_number = int(cover_station['DEPARTMENT_ALL_NAME'].count())
    not_cover = all_station_number - cover_station_number
    main_cover_station = cover_data[cover_data['TYPE'].isin([2])].groupby('DEPARTMENT_ALL_NAME').count().reset_index()
    main_cover_station_number = int(main_cover_station['DEPARTMENT_ALL_NAME'].count())
    not_cover_main = all_station_number - main_cover_station_number
    return {
        'cover_station': cover_station_number,
        'not_cover': not_cover,
        'main_cover_station_number': main_cover_station_number,
        'not_cover_main': not_cover_main,
    }


def _get_all_station_situation(data, last_month_data, last_year_data, all_station_data, cover_data):
    """
    计算全段总体情况（录入信息数、夜查率、考核率、发现问题数）
    :param data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 录入信息数
    input_info_number = len(data)
    last_month_input_info_number = len(last_month_data)
    last_year_input_info_number = len(last_year_data)
    # 录入信息同比、环比
    input_info_ratio = calculate_year_and_ring_ratio(
        [input_info_number, last_year_input_info_number, last_month_input_info_number])
    # 录入发现问题的检查信息+环比
    find_problem_number = input_info_number - len(data[data['PROBLEM_NUMBER'].isin([0])])
    last_month_find_problem = last_month_input_info_number - len(
        last_month_data[last_month_data['PROBLEM_NUMBER'].isin([0])])
    last_year_find_problem = last_year_input_info_number - len(
        last_year_data[last_year_data['PROBLEM_NUMBER'].isin([0])])
    find_problem_number_ratio = calculate_year_and_ring_ratio(
        [find_problem_number, last_year_find_problem, last_month_find_problem])
    # 发现问题率
    find_problem_ratio = round(find_problem_number / input_info_number, 3) * 100
    # 夜查信息 + 环比、同比
    yecha_info = len(data[data['IS_YECHA'].isin([1])])
    last_month_yecha_info = len(last_month_data[last_month_data['IS_YECHA'].isin([1])])
    last_year_yecha_info = len(last_year_data[last_year_data['IS_YECHA'].isin([1])])
    yecha_info_ratio = calculate_year_and_ring_ratio([yecha_info, last_year_yecha_info, last_month_yecha_info])
    # 夜查率
    yecha_ratio = round(yecha_info / input_info_number, 1) * 100
    # 监控设备回放
    freq = len(data[data['CHECK_TYPE'] == 104])
    freq_ratio = calculate_month_and_ring_ratio([freq, len(last_month_data[last_month_data['CHECK_TYPE'] == 104])])
    # 检查发现问题 + 环比
    check_find_problem = int(data['PROBLEM_NUMBER'].sum())
    last_month_check_find_problem = int(last_month_data['PROBLEM_NUMBER'].sum())
    check_find_problem_ratio = calculate_month_and_ring_ratio([check_find_problem, last_month_check_find_problem])
    # 考核数 +环比、同比
    assess_problem_number = len(data[data['IS_ASSESS'].isin([1])])
    last_month_assess_problem_number = len(last_month_data[last_month_data['IS_ASSESS'].isin([1])])
    last_year_assess_problem_number = len(last_year_data[last_year_data['IS_ASSESS'].isin([1])])
    assess_problem_ratio = calculate_year_and_ring_ratio(
        [assess_problem_number, last_year_assess_problem_number, last_month_assess_problem_number])
    # 考核率
    assess_ratio = round(assess_problem_number / input_info_number, 1) * 100
    # 按问题分项划分
    problem_table = _get_problem_table(data)
    # 夜查率
    yecha_ratio_info = _get_all_ratio_info(data, 'IS_YECHA', False)
    # 问题率
    problem_ratio_info = _get_all_ratio_info(data, 'PROBLEM_NUMBER', True)
    # 考核率
    assess_ratio_info = _get_all_ratio_info(data, 'IS_ASSESS', False)
    # 检查点覆盖情况
    station_cover_info = _get_station_cover(all_station_data, cover_data)
    return {
        'input_info_number': input_info_number,
        'input_info_ratio': input_info_ratio,
        'find_problem_number': find_problem_number,
        'find_problem_number_ratio': find_problem_number_ratio,
        'find_problem_ratio': find_problem_ratio,
        'yecha_info': yecha_info,
        'yecha_info_ratio': yecha_info_ratio,
        'yecha_ratio': yecha_ratio,
        'check_find_problem': check_find_problem,
        'check_find_problem_ratio': check_find_problem_ratio,
        'assess_problem_number': assess_problem_number,
        'assess_problem_ratio': assess_problem_ratio,
        'assess_ratio': assess_ratio,
        'freq': freq,
        'freq_ratio': freq_ratio,
        'problem_table': problem_table,
        'yecha_ratio_info': yecha_ratio_info,
        'problem_ratio_info': problem_ratio_info,
        'assess_ratio_info': assess_ratio_info,
        'station_cover_info': station_cover_info,
    }


def _get_problem_table(data):
    """
    获取问题分项表格数据
    :param data:
    :return:
    """
    # 作业项
    work_point = calc_problem_table(data, '作业', ['A', 'B', 'C', 'D'])
    # 设备设施
    shebei_point = calc_problem_table(data, '设备', ['E1', 'E2', 'E3', 'E4'])
    # 管理
    manager_point = calc_problem_table(data, '管理', ['F1', 'F2', 'F3', 'F4'])
    # 反恐防暴
    cs_point = calc_problem_table(data, '反恐防暴', ['K1', 'K2', 'K3', 'K4'])
    num = work_point['a'] + work_point['b'] + shebei_point['a'] + shebei_point['b'] + manager_point['a'] + \
          manager_point['b'] + cs_point['a'] + cs_point['b']
    ratio = round(
        num / (work_point['count'] + shebei_point['count'] + manager_point['count'] + cs_point['count']) * 100, 1)
    return {
        'work_point': work_point,
        'shebei_point': shebei_point,
        'manager_point': manager_point,
        'cs_point': cs_point,
        'num': num,
        'ratio': ratio
    }


def calc_problem_table(data, str1, list1):
    """
    计算各个分项问题的各个种类数量
    :param data:
    :param str1:
    :param list1:
    :return:
    """
    problem_data = data[data['PROBLEM_CLASSITY_NAME'].str.contains(str1)]
    a = len(problem_data[problem_data['LEVEL'].str.contains(list1[0])])
    b = len(problem_data[problem_data['LEVEL'].str.contains(list1[1])])
    c = len(problem_data[problem_data['LEVEL'].str.contains(list1[2])])
    d = len(problem_data[problem_data['LEVEL'].str.contains(list1[3])])
    count = a + b + c + d
    return {
        'a': a,
        'b': b,
        "c": c,
        'd': d,
        'count': count,
    }


def _get_all_ratio_info(data, str1, bool):
    """
    所有信息计算(问题查询除外)
    :param data:
    :return:
    """
    input_info_number = len(data)
    if bool is False:
        problem_info = data[data[str1].isin([1])]
    else:
        problem_info = data[~data[str1].isin([0])]
    # 职能科室
    section = calc_all_department(problem_info, input_info_number, '科')
    # 运用车间
    use_workship = calc_all_department(problem_info, input_info_number, '运用车间')
    # 检整车间 = 检修车间+整备车间+设备车间
    check_workship = calc_check_workship(problem_info, input_info_number, ['检修车间', '整备车间', '设备车间'])
    return {
        'section': section,
        'use_workship': use_workship,
        'check_workship': check_workship,
    }


def calc_all_department(problem_info, input_info_number, str1):
    """
    计算除检整车间以外的其他车间检查率
    :param problem_info:
    :param input_info_number:
    :param str1:
    :return:
    """
    job_name = problem_info[problem_info['DEPARTMENT_ALL_NAME'].str.contains(str1)]
    fenzu_job_name = job_name.groupby('DEPARTMENT_ALL_NAME').count().sort_values(by='CHECK_WAY',
                                                                                 ascending=False).reset_index()
    max_dp = fenzu_job_name.iloc[0]['DEPARTMENT_ALL_NAME'].split('-')[1]
    max_dp_count = fenzu_job_name.iloc[0]['CHECK_WAY']
    max_ratio = int(max_dp_count) / input_info_number * 100

    min_dp = fenzu_job_name.iloc[-1]['DEPARTMENT_ALL_NAME'].split('-')[1]
    min_dp_count = fenzu_job_name.iloc[-1]['CHECK_WAY']
    min_ratio = int(min_dp_count) / input_info_number * 100
    if str1 == '运用车间':
        job_name1 = problem_info[problem_info['DEPARTMENT_ALL_NAME'].str.contains('货运车间')]
        if not job_name1.empty:
            fenzu_job_name1 = job_name1.groupby('DEPARTMENT_ALL_NAME').count().sort_values(by='CHECK_WAY',
                                                                                           ascending=False).reset_index()
            min_dp1 = fenzu_job_name1.iloc[-1]['DEPARTMENT_ALL_NAME']
            min_dp_count1 = fenzu_job_name1.iloc[-1]['CHECK_WAY'].split('-')[1]
            min_ratio1 = int(min_dp_count1) / input_info_number * 100
        else:
            min_dp1 = 0
            min_ratio1 = 0
        return {
            'max_dp': max_dp,
            'max_ratio': format(max_ratio, '.2f'),
            'min_dp': min_dp,
            'min_ratio': format(min_ratio, '.2f'),
            'min_dp1': min_dp1,
            'min_ratio1': format(min_ratio1, '.2f'),
        }
    else:
        return {
            'max_dp': max_dp,
            'max_ratio': format(max_ratio, '.2f'),
            'min_dp': min_dp,
            'min_ratio': format(min_ratio, '.2f'),
        }


def calc_check_workship(problem_info, input_info_number, str1):
    """
    求检整车间的最大和最小率
    :param problem_info:
    :param input_info_number:
    :param str1:
    :return:
    """
    job_name1 = problem_info[problem_info['DEPARTMENT_ALL_NAME'].str.contains(str1[0])]
    job_name2 = problem_info[problem_info['DEPARTMENT_ALL_NAME'].str.contains(str1[1])]
    job_name3 = problem_info[problem_info['DEPARTMENT_ALL_NAME'].str.contains(str1[2])]
    job_name = pd.concat([job_name1, job_name2, job_name3])
    fenzu_job_name = job_name.groupby('DEPARTMENT_ALL_NAME').count().sort_values(by='CHECK_WAY',
                                                                                 ascending=False).reset_index()
    max_dp = fenzu_job_name.iloc[0]['DEPARTMENT_ALL_NAME'].split('-')[1]
    max_dp_count = fenzu_job_name.iloc[0]['CHECK_WAY']
    max_ratio = round(int(max_dp_count) / input_info_number, 2) * 100
    min_dp = fenzu_job_name.iloc[-1]['DEPARTMENT_ALL_NAME'].split('-')[1]
    min_dp_count = fenzu_job_name.iloc[-1]['CHECK_WAY']
    min_ratio = round(int(min_dp_count) / input_info_number, 2) * 100
    return {
        'max_dp': max_dp,
        'max_ratio': format(max_ratio, '.2f'),
        'min_dp': min_dp,
        'min_ratio': format(min_ratio, '.2f'),
    }


def _get_problem_statistics_analysis(data):
    """
    问题统计分析
    :param data:
    :return:
    """
    # 责任部门数
    duty_department_number = len(data.groupby('DEPARTMENT_ALL_NAME'))
    # 总问题数
    all_problem = int(data['PROBLEM_NUMBER'].sum())
    # 作业项问题
    work_point = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    # 管理问题
    manager_point = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')])
    # 设备设施
    shebei_point = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')]) + len(
        data[data['PROBLEM_CLASSITY_NAME'].str.contains('其它设施设备')])
    # 占比
    work_ratio = work_point / all_problem * 100
    manager_ratio = manager_point / all_problem * 100
    shebei_ratio = shebei_point / all_problem * 100
    # 高质量问题(A、B、E1、E2、F1、F2）
    height_point = len(data[data['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    # 分析表格
    # 添乘检查
    add_check = calc_all_type_check(data, 2)
    # 现场检查
    scene_check = calc_all_type_check(data, 1)
    # 设备监控调阅
    shebei_check = calc_all_type_check(data, 3)
    # 按检查方式划分
    check_way_type = _get_check_way_type(data)
    # 按风险大类划分
    risk_type = _get_risk_type(data)
    return {
        'duty_department_number': duty_department_number,
        'work_point': work_point,
        'manager_point': manager_point,
        'shebei_point': shebei_point,
        'work_ratio': format(work_ratio, '.2f'),
        'manager_ratio': format(manager_ratio, '.2f'),
        'shebei_ratio': format(shebei_ratio, '.2f'),
        'height_point': height_point,
        'add_check': add_check,
        'scene_check': scene_check,
        'shebei_check': shebei_check,
        'check_way_type': check_way_type,
        'risk_type': risk_type,
    }


def _get_risk_type(data):
    """
    按风险大类分
    :param data:
    :return:
    """
    list1 = ['行车', '劳动', '路外环境', '特种设备', '食品卫生', '规章管理']
    list2 = []
    list3 = []
    # 重大安全风险
    big_main_risk = data[data['RISK_LEVEL'] == 1]
    big_main_risk_type = calc_risk_small_type(big_main_risk, list1)
    # 较大
    more_risk = data[data['RISK_LEVEL'] == 2]
    more_risk_type = calc_risk_small_type(more_risk, list1)
    # 一般
    common_risk = data[data['RISK_LEVEL'] == 3]
    common_risk_type = calc_risk_small_type(common_risk, list1)
    # 低安全
    low_risk = data[data['RISK_LEVEL'] == 4]
    low_risk_type = calc_risk_small_type(low_risk, list1)
    for i in range(len(list1)):
        list2.append(
            big_main_risk_type['count_list'][i] + more_risk_type['count_list'][i] + common_risk_type['count_list'][i] +
            low_risk_type['count_list'][i])
        list3.append(
            big_main_risk_type['score_list'][i] + more_risk_type['score_list'][i] + common_risk_type['score_list'][i] +
            low_risk_type['score_list'][i])
    return {
        'big_main_risk_type': big_main_risk_type,
        'more_risk_type': more_risk_type,
        'common_risk_type': common_risk_type,
        'low_risk_type': low_risk_type,
        'list1': list1,
        'list2': list2,
        'list3': list3,
    }


def calc_risk_small_type(data, list1):
    data = data.dropna(subset=['RISK_NAMES'])
    count_list = []
    score_list = []
    for i in range(len(list1)):
        x = data[data['RISK_NAMES'].str.contains(list1[i])]
        if x.empty is True:
            count_list.append(0)
            score_list.append(0)
        else:
            count_list.append(len(x))
            score_list.append(int(x['PROBLEM_SCORE'].sum()))
    all_count = sum(count_list)
    all_score = sum(score_list)
    return {
        'count_list': count_list,
        'score_list': score_list,
        'all_score': all_score,
        'all_count': all_count
    }


def _get_check_way_type(data):
    """
    检查方式分类查询
    :param data:
    :return:
    """
    # 总问题数
    all_problem_count = int(data['PROBLEM_NUMBER'].sum())
    # 现场抽查
    spot_check = int(data[data['CHECK_WAY'] == 1]['PROBLEM_NUMBER'].sum())
    # 添乘检查
    add_checkup = int(data[data['CHECK_WAY'] == 2]['PROBLEM_NUMBER'].sum())
    # 设备监控调阅
    shebei_check = int(data[data['CHECK_WAY'] == 3]['PROBLEM_NUMBER'].sum())
    # 占比
    spot_ratio = spot_check / all_problem_count * 100
    add_ratio = add_checkup / all_problem_count * 100
    shebei_ratio = shebei_check / all_problem_count * 100
    return {
        'spot_check': spot_check,
        'add_checkup': add_checkup,
        'shebei_check': shebei_check,
        'spot_ratio': spot_ratio,
        'add_ratio': add_ratio,
        'shebei_ratio': shebei_ratio,
    }


def calc_all_type_check(data, num):
    """
    计算各个问题类型
    :param data:
    :param num:
    :return:
    """
    check_data = data[data['CHECK_WAY'].isin([num])]
    a = len(check_data[check_data['LEVEL'].str.contains('A')])
    b = len(check_data[check_data['LEVEL'].str.contains('B')])
    c = len(check_data[check_data['LEVEL'].str.contains('C')])
    d = len(check_data[check_data['LEVEL'].str.contains('D')])
    e1 = len(check_data[check_data['LEVEL'].str.contains('E1')])
    e2 = len(check_data[check_data['LEVEL'].str.contains('E2')])
    e3 = len(check_data[check_data['LEVEL'].str.contains('E3')])
    e4 = len(check_data[check_data['LEVEL'].str.contains('E4')])
    f2 = len(check_data[check_data['LEVEL'].str.contains('F2')])
    f3 = len(check_data[check_data['LEVEL'].str.contains('F3')])
    f4 = len(check_data[check_data['LEVEL'].str.contains('F4')])
    k3 = len(check_data[check_data['LEVEL'].str.contains('K3')])
    k4 = len(check_data[check_data['LEVEL'].str.contains('K4')])
    count1 = a + b + c + d
    count2 = e1 + e2 + e3 + e4
    count3 = f2 + f3 + f4
    count4 = k3 + k4
    all_count = count1 + count2 + count3 + count4
    return {
        'a': a,
        'b': b,
        'c': c,
        'd': d,
        'e1': e1,
        'e2': e2,
        'e3': e3,
        'e4': e4,
        'f2': f2,
        'f3': f3,
        "f4": f4,
        'k3': k3,
        'k4': k4,
        'count1': count1,
        'count2': count2,
        'count3': count3,
        'count4': count4,
        'all_count': all_count
    }


# ----------------------------------------------------------------------------------第三点-----------------------------------------------------------------
def get_manage_evaluate_problem(start_date, end_date, station_id):
    """
    干部履职情况简要统计分析
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    # 履职信息问题data
    evaluate_info_data = pd_query(CHECK_EVALUATE_PROBLEM_SITUATION.format(start_date, end_date, station_id))
    last_evaluate_data = pd_query(CHECK_EVALUATE_PROBLEM_SITUATION.format(last_month_start, last_month_end, station_id))
    # 监督检查情况data
    supervision_check_data = pd_query(CHECK_SITUATION_SQL.format(start_date, end_date, station_id))
    supervision_check_data['NEW_NAME'] = supervision_check_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[1])
    check_main_problem_data = pd_query(CHECK_MAIN_PROBLEM_SQL.format(start_date, end_date, station_id))
    check_main_problem_data['NEW_NAME'] = check_main_problem_data['DEPARTMENT_ALL_NAME'].apply(
        lambda x: x.split('-')[1])
    # 干部评价总体情况分析
    cadre_evaluate_situation_analysis = _get_cadre_evaluate_situation_analysis(evaluate_info_data, last_evaluate_data)
    # 监督检查质量情况分析
    supervision_check_analysis = _get_supervision_check_analysis(supervision_check_data, check_main_problem_data)

    return {
        'cadre_evaluate_situation_analysis': cadre_evaluate_situation_analysis,
        'supervision_check_analysis': supervision_check_analysis,
    }


def _get_supervision_check_analysis(data, main_data):
    """
    监督检查质量情况分析
    :return:
    """
    # 段领导
    station_manager = _get_station_manager(data)
    # 职能科室
    func_keshi = _get_func_qt(data, main_data, '科')
    # 运用车间
    use_chejian = _get_func_qt(data, main_data, '运用车间')
    # 整备车间
    svc_chejian = _get_func_qt(data, main_data, '整备车间')
    return {
        'station_manager': station_manager,
        'func_keshi': func_keshi,
        'use_chejian': use_chejian,
        'svc_chejian': svc_chejian,
    }


def _get_func_qt(data, main_data, str1):
    """
    职能科室+运用车间+整备车间
    :param data:
    :return:
    """
    child = {}
    j = 0
    for i in data[data['DEPARTMENT_ALL_NAME'].str.contains(str1)]['NEW_NAME'].value_counts().index:
        j = j + 1
        child[i] = _get_func_qt_t(data[data['NEW_NAME'] == i], j)
        if j == 10:
            break
    # 音视频调阅（复查）不均衡
    mv_data = main_data[(main_data['CHECK_WAY'] == 4) & (main_data['NAME'].str.contains('音视频'))]
    # 添乘检查不均衡
    tc_data = main_data[main_data['CHECK_WAY'] == 2]
    dic = {
        'mv_count': len(mv_data),
        'mv_keche': len(mv_data[mv_data['IS_KECHE'] == 1]),
        'mv_dongche': len(mv_data[mv_data['IS_DONGCHE'] == 1]),
        'tc': len(tc_data),
        'tc_keche': len(tc_data[tc_data['IS_KECHE'] == 1]),
        'tc_dongche': len(tc_data[tc_data['IS_DONGCHE'] == 1])
    }
    return {
        'child': child,
        'dic': dic
    }


def _get_func_qt_t(data, j):
    """
    子车间
    :param data:
    :param str1:
    :return:
    """
    problem_list = ['作业', '设备', '管理', '外部环境', '反恐防暴']
    code_list = ['A', 'B', 'C', 'D']
    risk = []
    code_type = []
    xc_check = len(data[data['CHECK_WAY'].isin([1])])
    tc_check = len(data[data['CHECK_WAY'].isin([2])])
    shebei_check = len(data[data['CHECK_WAY'].isin([3])])
    review_check = len(data[data['CHECK_WAY'].isin([4])])
    problem_count = int(data['PROBLEM_NUMBER'].sum())
    for i in problem_list:
        risk.append(int(data[data['PROBLEM_CLASSITY_NAME'].str.contains(i)]['PROBLEM_NUMBER'].sum()))
    new = data[data['PROBLEM_CLASSITY_NAME'] == '生产作业']
    for i in code_list:
        code_type.append(int(new[new['LEVEL'] == i]['PROBLEM_NUMBER'].sum()))
    return {
        'j': j,
        'xc_check': xc_check,
        'tc_check': tc_check,
        'shebei_check': shebei_check,
        'review_check': review_check,
        'problem_count': problem_count,
        'risk': risk,
        'code_type': code_type,
    }


def _get_station_manager(data):
    """
    段领导
    :param data:
    :return:
    """
    problem_list = ['作业', '设备', '管理', '外部环境', '反恐防暴']
    code_list = ['A', 'B', 'C', 'D']
    risk = []
    code_type = []
    data = data[data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    xc_check = len(data[data['CHECK_WAY'].isin([1])])
    tc_check = len(data[data['CHECK_WAY'].isin([2])])
    shebei_check = len(data[data['CHECK_WAY'].isin([3])])
    review_check = len(data[data['CHECK_WAY'].isin([4])])
    problem_count = int(data['PROBLEM_NUMBER'].sum())
    for i in problem_list:
        if data[data['PROBLEM_CLASSITY_NAME'].str.contains(i)].empty:
            risk.append(0)
        else:
            risk.append(int(data[data['PROBLEM_CLASSITY_NAME'].str.contains(i)]['PROBLEM_NUMBER'].sum()))
    new = data[data['PROBLEM_CLASSITY_NAME'] == '生产作业']
    for i in code_list:
        if new[new['LEVEL'] == i].empty:
            code_type.append(0)
        else:
            code_type.append(int(new[new['LEVEL'] == i]['PROBLEM_NUMBER'].sum()))
    return {
        'xc_check': xc_check,
        'tc_check': tc_check,
        'shebei_check': shebei_check,
        'review_check': review_check,
        'problem_count': problem_count,
        'risk': risk,
        'code_type': code_type,
    }


def _get_cadre_evaluate_situation_analysis(evaluate_info_data, last_evaluate_data):
    """
    干部评价总体情况分析
    :return:
    """
    # 干部履职问题类型统计表
    evaluate_problem_table = _get_evaluate_problem_table(evaluate_info_data, last_evaluate_data)
    # 干部履职突出问题记分类型统计表
    issue_problem_table = _get_issue_problem_table(evaluate_info_data)
    # 计分统计表
    score_table = _get_score_table(evaluate_info_data, last_evaluate_data)
    # 累计计分情况
    score_situation = _get_score_situation(evaluate_info_data)
    calcaluate_point = _get_calcaluate_point(evaluate_info_data)
    # 正科职干部履职评价分析
    zk_analysis = _get_zk_analysis(evaluate_info_data)
    # 副科职及以下干部履职评价分析
    fk_analysis = _get_fk_analysis(evaluate_info_data)
    return {
        'evaluate_problem_table': evaluate_problem_table,
        'issue_problem_table': issue_problem_table,
        'score_table': score_table,
        'score_situation': score_situation,
        'calcaluate_point': calcaluate_point,
        'zk_analysis': zk_analysis,
        'fk_analysis': fk_analysis,
    }


def _get_fk_analysis(evaluate_info_data):
    """
    副科职及以下干部履职评价分析
    :param evaluate_info_data:
    :return:
    """
    fk_person = evaluate_info_data[evaluate_info_data['GRADATION'] != '正科职管理人员']
    number = len(fk_person)
    if fk_person.empty:
        station_name = 0
        station_number = 0
    else:
        fz_gp = fk_person.groupby('ALL_NAME').count().reset_index()
        station_name = fz_gp['ALL_NAME'].tolist()
        station_number = fz_gp['ITEM_NAME'].tolist()
    main_problem = fk_person.groupby('ITEM_NAME').count().sort_values(by='SCORE', ascending=False).reset_index()
    list1 = []
    list2 = []
    for i in range(4):
        list1.append(main_problem.loc[i]['ITEM_NAME'])
        list2.append(int(main_problem.loc[i]['SCORE']))

    return {
        'number': number,
        'station_name': station_name,
        'station_number': station_number,
        'list1': list1,
        'list2': list2,
    }


def _get_zk_analysis(evaluate_info_data):
    """
    正科职干部履职评价分析
    :param evaluate_info_data:
    :return:
    """
    zk_person = evaluate_info_data[evaluate_info_data['GRADATION'] == '正科职管理人员']
    number = len(zk_person)
    if zk_person.empty:
        station_name = 0
        station_number = 0
    else:
        fz_gp = zk_person.groupby('ALL_NAME').count().reset_index()
        station_name = fz_gp['ALL_NAME'].tolist()
        station_number = fz_gp['ITEM_NAME'].tolist()
    main_problem = zk_person.groupby('ITEM_NAME').count().sort_values(by='SCORE', ascending=False).reset_index()
    list1 = main_problem['ITEM_NAME'].tolist()
    list2 = main_problem['SCORE'].tolist()
    return {
        'number': number,
        'station_name': station_name,
        'station_number': station_number,
        'list1': list1[:3],
        'list2': list2[:3],
    }


def _get_calcaluate_point(evaluate_info_data):
    """
    累计计分情况
    :param evaluate_info_data:
    :return:
    """
    person_count = len(evaluate_info_data)
    all_number = len(evaluate_info_data.groupby('RESPONSIBE_PERSON_NAME'))
    score_count = int(evaluate_info_data['SCORE'].sum())
    score_up_8 = len(evaluate_info_data[evaluate_info_data['SCORE'] >= 8])
    score_6_8 = len(evaluate_info_data[(evaluate_info_data['SCORE'] >= 6) & (evaluate_info_data['SCORE'] < 8)])
    score_5_6 = len(evaluate_info_data[(evaluate_info_data['SCORE'] >= 5) & (evaluate_info_data['SCORE'] < 6)])
    score_4_5 = len(evaluate_info_data[(evaluate_info_data['SCORE'] >= 4) & (evaluate_info_data['SCORE'] < 5)])
    score_3_4 = len(evaluate_info_data[(evaluate_info_data['SCORE'] >= 3) & (evaluate_info_data['SCORE'] < 4)])
    score_2_3 = len(evaluate_info_data[(evaluate_info_data['SCORE'] >= 2) & (evaluate_info_data['SCORE'] < 3)])
    if evaluate_info_data[evaluate_info_data['SCORE'] >= 8].empty and evaluate_info_data[
        (evaluate_info_data['SCORE'] >= 6) & (evaluate_info_data['SCORE'] < 8)].empty:
        station_name = 0
        station_score = 0
        person_name = 0
    else:
        score_up_6 = pd.concat([evaluate_info_data[evaluate_info_data['SCORE'] >= 8], evaluate_info_data[
            (evaluate_info_data['SCORE'] >= 6) & (evaluate_info_data['SCORE'] < 8)]]).reset_index()
        station_name = score_up_6['ALL_NAME'].tolist()
        station_score = score_up_6['SCORE'].tolist()
        person_name = score_up_6['RESPONSIBE_PERSON_NAME'].tolist()
    return {
        'person_count': person_count,
        'all_number': all_number,
        'score_count': score_count,
        'score_up_8': score_up_8,
        'score_6_8': score_6_8,
        'score_5_6': score_5_6,
        'score_4_5': score_4_5,
        'score_3_4': score_3_4,
        'score_2_3': score_2_3,
        'station_name': station_name,
        'station_score': station_score,
        'person_name': person_name,
    }


def _get_score_situation(data):
    """
    计分情况分析
    :param data:
    :return:
    """
    sort_data = data.groupby('ITEM_NAME').sum().sort_values(by='SCORE', ascending=False).reset_index()
    # count_data = data.groupby('ITEM_NAME').count().sort_values(by='SCORE',ascending=False).reset_index()
    list_name = []
    list_number = []
    list_ratio = []
    for i in range(3):
        list_name.append(sort_data.loc[i]['ITEM_NAME'])
        list_number.append(len(data[data['ITEM_NAME'] == sort_data.loc[i]['ITEM_NAME']]))
        if len(data[data['ITEM_NAME'] == sort_data.loc[i]['ITEM_NAME']]) == 0:
            list_ratio[i] = 0
        else:
            list_ratio.append(round(len(data[data['ITEM_NAME'] == sort_data.loc[i]['ITEM_NAME']]) / len(data) * 100, 1))
    station1 = calc_evaluate_name_type(data, list_name[0])
    station2 = calc_evaluate_name_type(data, list_name[1])
    station3 = calc_evaluate_name_type(data, list_name[2])
    return {
        'list_name': list_name,
        'list_number': list_number,
        'list_ratio': list_ratio,
        'station1': station1,
        'station2': station2,
        'station3': station3,
    }


def calc_evaluate_name_type(data, name):
    list1_name = []
    list1_number = []
    list2_name = []
    list2_number = []
    station_data = data[data['ITEM_NAME'] == name].groupby('ALL_NAME').count().reset_index()
    keshi = station_data[station_data['ALL_NAME'].str.contains('科')].reset_index()
    for i in range(len(keshi)):
        list1_name.append(keshi.loc[i]['ALL_NAME'])
        list1_number.append(int(keshi.loc[i]['SITUATION']))
    chejian = station_data[station_data['ALL_NAME'].str.contains('车间')].reset_index()
    for i in range(len(chejian)):
        list2_name.append(chejian.loc[i]['ALL_NAME'])
        list2_number.append(int(chejian.loc[i]['SITUATION']))
    return {
        'list1_name': list1_name,
        'list1_number': list1_number,
        'list2_name': list2_name,
        'list2_number': list2_number,
    }


def _get_score_table(data, last_data):
    """
    计分统计表
    :param data:
    :return:
    """
    list1 = []
    list2 = []
    list3 = []
    for i in range(6):
        if data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)].empty:
            list1.append(0)
        else:
            list1.append(len(data[(data['SCORE'] >= i * 2) & (data['SCORE'] < (i + 1) * 2)]))
    if data[data['SCORE'] >= 12].empty:
        list1.append(0)
    else:
        list1.append(len(data[data['SCORE'] >= 12]))

    for i in range(6):
        if last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)].empty:
            list2.append(0)
        else:
            list2.append(len(last_data[(last_data['SCORE'] >= i * 2) & (last_data['SCORE'] < (i + 1) * 2)]))
    if last_data[last_data['SCORE'] >= 12].empty:
        list2.append(0)
    else:
        list2.append(len(last_data[last_data['SCORE'] >= 12]))
    for i in range(len(list1)):
        list3.append(list1[i] - list2[i])
    return {
        'list1': list1,
        'list3': list3
    }


def _get_issue_problem_table(data):
    """
    干部履职突出问题记分类型统计表
    :param data:
    :return:
    """
    list1 = ['专业管理重点落实不到位。', '日常重点安全工作落实不到位。', '倒查重点问题，检查质量低下。', '监控调阅检查不认真。', '问题整改督促不力。']
    problem_number = []
    problem_ratio = []
    for i in list1:
        if data[data['SITUATION'] == i].empty:
            problem_number.append(0)
            problem_ratio.append(0)
        else:
            problem_number.append(len(data[data['SITUATION'] == i]))
            problem_ratio.append(round(len(data[data['SITUATION'] == i]) / len(data) * 100, 1))
    return {
        'problem_number': problem_number,
        'problem_ratio': problem_ratio
    }


def _get_evaluate_problem_table(data, last_data):
    """
    干部履职问题类型统计表
    :param data:
    :param last_data:
    :return:
    """
    list1 = ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
             '履职评价管理', '事故故障追溯', '弄虚作假']
    data_list = []
    diff_list = []
    ratio_list = []
    all_count = len(data)

    for i in list1:
        if data[data['ITEM_NAME'] == i].empty:
            data_list.append(0)
            ratio_list.append(0)
            diff_list.append(0)
        else:
            data_list.append(len(data[data['ITEM_NAME'] == i]))
            ratio_list.append(round(len(data[data['ITEM_NAME'] == i]) / all_count * 100, 1))
            if last_data[last_data['ITEM_NAME'] == i].empty:
                diff_list.append(
                    calculate_month_and_ring_ratio([len(data[data['ITEM_NAME'] == i]), 0])['month_percent'])
            else:
                diff_list.append(calculate_month_and_ring_ratio(
                    [len(data[data['ITEM_NAME'] == i]), len(last_data[last_data['ITEM_NAME'] == i])])['month_percent'])
    return {
        'data_list': data_list,
        'ratio_list': ratio_list,
        'diff_list': diff_list
    }


# ----------------------------------------------------------------------------------第四点-----------------------------------------------------------------
def get_safety_low_department_analysis(start_date, end_date, station_id):
    """
    近期安全管理持续薄弱科室、车间情况分析
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    # 问题检查data
    safety_issue_data = pd_query(CHECK_SAFETY_ISSUE_SQL.format(start_date, end_date, station_id))
    # 严重问题部门data
    safety_department_data = pd_query(CHECK_SAFETY_DEPARTMENT.format(start_date, end_date, station_id))
    # 部门名称
    department_data = safety_department_data[safety_department_data['TYPE'] == 2]
    department_name = department_data.groupby('ALL_NAME').count().reset_index()
    department_name_list1 = department_name['ALL_NAME'].tolist()
    department_name_list2 = department_name['ALL_NAME'].apply(lambda x: x.split('-')[1]).tolist()
    department_risk_number = department_name['TYPE'].tolist()
    # 风险名称
    risk_name = []
    for i in range(len(department_name)):
        risk_name.append(
            department_data[department_data['ALL_NAME'] == department_name_list1[i]]['RISK_TYPE_NAMES'].tolist())
    # 检查次数
    check_number = []
    for i in range(len(department_name)):
        check_number.append(len(safety_issue_data[(safety_issue_data['RISK_NAMES'].isin(risk_name[i])) & (
                safety_issue_data['ALL_NAME'] == department_name_list1[i])]))
    return {
        'department_name_list2': department_name_list2,
        'department_risk_number': department_risk_number,
        'check_number': check_number,
    }


# ----------------------------------------------------------------------------------第五点-----------------------------------------------------------------


# ----------------------------------------------------------------------------------第六点-----------------------------------------------------------------
def get_safety_tip(start_date, end_date, station_id):
    """
    “违章大王”情况分析
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    # 违章人员data
    iiilege = pd_query(CHECK_IIIEGE_SQL.format(start_date, end_date, station_id))
    iiilege['NEW_NAME'] = iiilege['ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 所有人员信息
    person = pd_query(CHECK_PERSON_SQL.format(station_id))
    person['NEW_NAME'] = person['ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 违章信息
    iiilege_info = get_iiilege_info(iiilege, person, start_date)
    # 违章大王人数
    ilege_number = len(iiilege)
    # 机车乘务员
    crew = len(iiilege[iiilege['POSITION'].str.contains('机车')])
    # 整备职工
    workers = len(iiilege[iiilege['POSITION'].str.contains('整备工')]) + len(
        iiilege[iiilege['POSITION'].str.contains('检修工')])
    # 按车间分类
    all_chejian = iiilege[iiilege['DEDUCT_SCORE'] >= 12]
    new_chejian = all_chejian.groupby('NEW_NAME').count().reset_index()
    chejian = new_chejian[new_chejian['NEW_NAME'].str.contains('车间')].reset_index(drop=True)
    chejian_list = []
    person_list = []
    for i in range(len(chejian)):
        chejian_list.append(chejian.loc[i]['NEW_NAME'])
        person_list.append(int(chejian.loc[i]['DEDUCT_SCORE']))
    return {
        'ilege_number': ilege_number,
        'chejian_list': chejian_list,
        'person_list': person_list,
        'crew': crew,
        'workers': workers,
        'iiilege_info': iiilege_info
    }


def get_iiilege_info(iiilege, person, start_date):
    year = start_date.split('-')[0]
    person = person.dropna()
    person = person[person['BIRTHDAY'] != ' ']
    person['AGE'] = person['BIRTHDAY'].apply(lambda x: int(year) - int(x[:4]))
    iiilege['AGE'] = iiilege['BIRTHDAY'].apply(lambda x: int(year) - int(x[:4]))
    # 按责任岗位划分
    crew_data = iiilege[iiilege['POSITION'].str.contains('机车')]
    # 机车司机
    crew = len(crew_data)
    driver = len(crew_data[crew_data['POSITION'].str.contains('机车司机')])
    t_driver = len(crew_data[crew_data['POSITION'].str.contains('副司机')])
    # 其他违章人员
    new_data = iiilege[~iiilege['POSITION'].str.contains('机车')].groupby('POSITION').count().reset_index()
    name = new_data['POSITION'].tolist()
    count = [int(i) for i in new_data['ALL_NAME'].tolist()]
    # 按年龄责任次数
    all_count = len(iiilege)
    age_level = [0, 0, 0, 0]
    for i in iiilege['AGE'].tolist():
        if 20 < i < 30:
            age_level[0] += 1
        elif 30 < i < 40:
            age_level[1] += 1
        elif 40 < i < 50:
            age_level[2] += 1
        elif 50 < i < 60:
            age_level[3] += 1
    ratio = [round(ded_zero(i, all_count) * 100, 1) for i in age_level]
    # 单位人数
    person_number = [0, 0, 0, 0]
    for i in person['AGE'].tolist():
        if 20 < i < 30:
            person_number[0] += 1
        elif 30 < i < 40:
            person_number[1] += 1
        elif 40 < i < 50:
            person_number[2] += 1
        elif 50 < i < 60:
            person_number[3] += 1
    # 人均占比
    avg_ratio = [round(ded_zero(age_level[i], person_number[i]) * 100, 2) for i in range(len(age_level))]
    # 按运用车间和检整车间分
    yy_workshop = get_chejian(iiilege[iiilege['NEW_NAME'].str.contains('运用车间')])
    fix_workshop = get_chejian(iiilege[(iiilege['NEW_NAME'].str.contains('检修车间')) | (iiilege['NEW_NAME'].str.contains('整备车间'))])
    return {
        'crew': crew,
        'driver': driver,
        't_driver': t_driver,
        'name': name,
        'count': count,
        'age_level': age_level,
        'ratio': ratio,
        'person_number': person_number,
        'avg_ratio': avg_ratio,
        'yy_workshop': yy_workshop,
        'fix_workshop': fix_workshop
    }


def get_chejian(data):
    """
    计算车间
    :param iiilege:
    :return:
    """
    all_list = []
    for i in data['NEW_NAME'].value_counts().index:
        new_data = data[data['NEW_NAME'] == i]
        age_level = [0, 0, 0, 0]
        for j in new_data['AGE'].tolist():
            if 20 < j < 30:
                age_level[0] += 1
            elif 30 < j < 40:
                age_level[1] += 1
            elif 40 < j < 50:
                age_level[2] += 1
            elif 50 < j < 60:
                age_level[3] += 1
        dic = {
            'name': i,
            'count': max(age_level),
            'interval': age_interval(age_level.index(max(age_level)))
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


# ----------------------------------------------------------------------------------第七点-----------------------------------------------------------------
def get_safety_center_work(start_data, end_data, station_id):
    """
    分析中心主要工作质量分析
    :param station_id:
    :return:
    """
    satety_center_data = pd_query(CHECK_SAFETY_ANALYSIS_CENTER_SQL.format(start_data, end_data, station_id))
    # 信息、问题逐条评价
    problem_info_data = satety_center_data[satety_center_data['EVALUATE_TYPE'].isin([1, 2])]
    problem_info_number = len(problem_info_data[problem_info_data['EVALUATE_WAY'].isin([1])])
    # 逐条复查
    zt_review = len(satety_center_data[(satety_center_data['EVALUATE_WAY'].isin([1])) & (
        satety_center_data['IS_REVIEW'].isin([1]))])
    # 监控调阅逐条评价
    jk_evaluate = len(
        satety_center_data[(satety_center_data['CHECK_WAY'] == 3) & (satety_center_data['EVALUATE_WAY'] == 1)])
    # 监控调阅复查
    jk_review = len(satety_center_data[(satety_center_data['CHECK_WAY'] == 3) & (satety_center_data['IS_REVIEW'] == 1)])
    # 定期评价
    dq_evaluate = len(satety_center_data[satety_center_data['EVALUATE_WAY'] == 2])
    # 定期评价复查
    dq_review = len(
        satety_center_data[(satety_center_data['EVALUATE_WAY'] == 2) & (satety_center_data['IS_REVIEW'] == 1)])
    # 评价记分
    score = len(satety_center_data[satety_center_data['IS_EVALUATE'] == 1])
    return {
        'problem_info_number': problem_info_number,
        'zt_review': zt_review,
        'jk_evaluate': jk_evaluate,
        'jk_review': jk_review,
        'dq_evaluate': dq_evaluate,
        'dq_review': dq_review,
        'score': score,
    }


# ----------------------------------------------第九点---------------------------------------------------
def get_risk_problem_analysis(start_data, end_data, station_id):
    """
    安全风险分析预警
    :param start_data:
    :param end_data:
    :param station_id:
    :return:
    """
    risk_problem_data = pd_query(CHECK_RISK_PROBLEM_SQL.format(start_data, end_data, station_id))
    risk_problem_data['CHEJIAN'] = risk_problem_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 间断瞭望及盹睡问题突出
    jdlw = _get_jdlw_all_data(risk_problem_data)
    # 劳动安全风险居高不下
    ldaq = _get_ldaq_all_data(risk_problem_data)
    # 错误操作问题突出
    error_caozuo = _get_error_caozuo(risk_problem_data)
    # 溜逸风险
    liuyi_risk = _get_liuyi_risk(risk_problem_data)
    # 调车风险
    diaoche = _get_diaoche(risk_problem_data)
    # 机车质量
    xingche = _get_xingche(risk_problem_data)
    return {
        'jdlw': jdlw,
        'ldaq': ldaq,
        'error_caozuo': error_caozuo,
        'diaoche': diaoche,
        'xingche': xingche,
        'liuyi_risk': liuyi_risk,
    }


def _get_liuyi_risk(risk_problem_data):
    """
    溜逸风险
    :param risk_problem_data:
    :return:
    """
    data = risk_problem_data[risk_problem_data['RISK_CONSEQUENCE_NAMES'] == '溜逸']
    count = len(data)
    problem = {}
    problem_data = data.groupby('PROBLEM_POINT').count().reset_index()
    for i in range(len(problem_data)):
        problem[problem_data.loc[i]['PROBLEM_POINT']] = int(problem_data.loc[i]['RISK_CONSEQUENCE_NAMES'])
    return {
        'count': count,
        'problem': problem
    }


def _get_xingche(risk_problem_data):
    """
    行车设备质量
    :param risk_problem_data:
    :return:
    """
    xingche_data = risk_problem_data[risk_problem_data['RISK_NAMES'].str.contains('行车设备质量')]
    problem_num = len(xingche_data)
    # 漏检漏修违法修问题突出
    loujian = len(xingche_data[xingche_data['PROBLEM_POINT'].str.contains('漏检漏修')])
    # 机车关键部件质量隐患
    # 走行部质量
    zx = len(xingche_data[xingche_data['RISK_NAMES'].str.contains('走行部质量')])
    # 制动系统质量
    zd = len(xingche_data[xingche_data['RISK_NAMES'].str.contains('制动系统质量')])
    # 车顶部件质量
    cd = len(xingche_data[xingche_data['RISK_NAMES'].str.contains('车顶部件质量')])
    return {
        'problem_num': problem_num,
        'loujian': loujian,
        'zx': zx,
        'zd': zd,
        'cd': cd
    }


def _get_diaoche(risk_problem_data):
    """
    调车风险
    :param risk_problem_data:
    :return:
    """
    diaoche_data = risk_problem_data[risk_problem_data['RISK_NAMES'].str.contains('调车')]
    diaoche_number = len(diaoche_data)
    # 问题
    diaoche_problem = diaoche_data.groupby('PROBLEM_POINT').count().sort_values(by='ALL_NAME').reset_index()
    problem_name = [i for i in diaoche_problem['PROBLEM_POINT']]
    problem_count = [i for i in diaoche_problem['ALL_NAME']]
    return {
        'diaoche_number': diaoche_number,
        'problem_name': problem_name,
        'problem_count': problem_count,
    }


def _get_error_caozuo(risk_problem_data):
    """
    错误操作风险
    :param risk_problem_data:
    :return:
    """
    caozuo = risk_problem_data[risk_problem_data['RISK_NAMES'].str.contains('错误操作')]
    # 手柄未及时回零
    error_data = caozuo[(caozuo['PROBLEM_POINT'].str.contains('手柄未')) & (caozuo['PROBLEM_POINT'].str.contains('回零'))]
    error_count = len(error_data)
    zero_data = error_data.groupby('PROBLEM_POINT').count().reset_index()
    zero_name = [i for i in zero_data['PROBLEM_POINT']]
    zer_count = [i for i in zero_data['ALL_NAME']]
    # 过分相未达速
    gfx_count = len(caozuo[caozuo['PROBLEM_POINT'].str.contains('过分相未')])
    # 制动机使用问题突出
    zhidong_problem = get_zhidong_problem(caozuo)
    # LKJ操作问题突出，控制参数设置错误、车位调整错误等时有发生
    lkj_count = len(caozuo[caozuo['PROBLEM_POINT'].str.contains('LKJ')])
    return {
        'error_count': error_count,
        'zero_name': zero_name,
        'zero_count': zer_count,
        'gfx_count': gfx_count,
        'lkj_count': lkj_count,
        'zhidong_problem': zhidong_problem,
    }


def get_zhidong_problem(data):
    """
    制动机使用问题突出
    :param risk_problem_data:
    :return:
    """
    # 充风不足
    cf_count = len(data[data['PROBLEM_POINT'].str.contains('充风不足')])
    # 低速缓解列车
    ds_count = len(data[data['PROBLEM_POINT'].str.contains('低速缓解')])
    # 排风不止
    pf_count = len(data[data['PROBLEM_POINT'].str.contains('排风不止')])
    # 货物列车
    hw_count = len(data[data['PROBLEM_POINT'].str.contains('二段制动')])
    # 紧急制动缓解机车单阀
    jj_count = len(data[data['PROBLEM_POINT'].str.contains('缓解机车单阀')])
    # 小闸调速
    jz_count = len(data[data['PROBLEM_POINT'].str.contains('小闸调速')])
    return {
        'count': cf_count + ds_count + pf_count + hw_count,
        'jj_count': jj_count,
        'jz_count': jz_count
    }


def _get_ldaq_all_data(risk_problem_data):
    """
    劳动安全风险居高不下
    :param risk_problem_data:
    :return:
    """
    ldaq = risk_problem_data[risk_problem_data['RISK_NAMES'].str.contains('劳动安全')]
    # 总数量
    ldaq_count = len(ldaq)
    # 不按规定穿戴、使用劳动保护用品
    not_accident_safety = len(ldaq[ldaq['PROBLEM_POINT'].str.contains('不安规定穿戴')])
    # 是不按规定设置防护
    not_set_safety = len(ldaq[ldaq['PROBLEM_POINT'].str.contains('设置')])
    nss = ldaq[ldaq['PROBLEM_POINT'].str.contains('设置')]
    zuoye = len(nss[nss['PROBLEM_CLASSITY_NAME'].str.contains('作业')]['PROBLEM_CLASSITY_NAME'])
    # 反手关门、反手下车、单手下车问题
    slr_door = len(ldaq[ldaq['PROBLEM_POINT'].str.contains('反手')])
    # 抢越线路、飞乘飞降、横跨地沟、翻越车钩等
    rob_route = len(ldaq[ldaq['PROBLEM_POINT'].str.contains('抢越线路')])
    # 其他劳安隐患

    return {
        'ldaq_count': ldaq_count,
        'not_accident_safety': not_accident_safety,
        'not_set_safety': not_set_safety,
        'zuoye': zuoye,
        'slr_door': slr_door,
        'rob_route': rob_route,
    }


def _get_jdlw_all_data(risk_problem_data):
    """间断瞭望"""
    jdlw = risk_problem_data[risk_problem_data['RISK_NAMES'].str.contains('间断瞭望')]
    # 总数量
    jdlw_count = len(jdlw)
    # 运行中打盹
    nap_data = jdlw[jdlw['PROBLEM_POINT'].str.contains('运行中间歇打盹。')]
    nap_count = len(nap_data)
    chejian = nap_data.groupby('CHEJIAN').count().sort_values(by='ALL_NAME', ascending=False).reset_index()
    chejian_name = [i for i in chejian['CHEJIAN']]
    chejian_count = [int(i) for i in chejian['ALL_NAME']]
    # 运行中间断瞭望
    jdlw_data = jdlw[jdlw['PROBLEM_POINT'].str.contains('运行中间断瞭望')]
    jdlw_yunxin_count = len(jdlw_data)
    return {
        'jdlw_count': jdlw_count,
        'nap_count': nap_count,
        'chejian_name': chejian_name,
        'chejian_count': chejian_count,
        'jdlw_yunxin_count': jdlw_yunxin_count,
    }
