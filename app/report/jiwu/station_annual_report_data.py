from app.data.util import pd_query
import pandas as pd
from app.report.analysis_report_manager import AnnualAnalysisReport
from app import mongo
from app.report.draw_picture import *
import datetime
from docx.shared import Mm
from docxtpl import InlineImage
from app.report.jiwu.station_annual_report_sql import *


def get_data(year, dp_id):
    date = AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_year_start, last_year_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    times = str(year * 100 + month)
    start_month = end_month - 5
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(dp_id)).iloc[0][
            'all_name']
    ded_month = int(end_month) - int(start_month)
    first = get_first(start_date, end_date, last_year_start, last_year_end, ded_month, station_name, dp_id, year)
    second = get_second(last_year_start, last_year_end, station_name, dp_id)
    third = get_third(start_date, end_date, last_year_start, last_year_end, dp_id, station_name, year)
    fourth = get_fourth(start_date, end_date, last_year_start, last_year_end, station_name, dp_id, ded_month, year)
    fifth = get_fifth(start_date, end_date, last_year_start, last_year_end, dp_id)
    file_name = f'{start_date}至{end_date}{station_name}安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '机务',
        "hierarchy": "STATION",
        'station_id': dp_id,
        'station_name': station_name,
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'first': first,
        'second': second,
        'third': third,
        'fourth': fourth,
        'fifth': fifth
    }
    return result


def insert_images(result, tpl):
    result['first']['first_two']['picture'] = InlineImage(tpl, result['first']['first_two']['images'], width=Mm(120))
    result['first']['first_three']['picture'] = InlineImage(tpl, result['first']['first_three']['draw_image'],
                                                            width=Mm(120))
    result['first']['first_six']['picture'] = InlineImage(tpl, result['first']['first_six']['images'], width=Mm(120))
    result['third']['record_score']['evaluate_score_project']['picture'] = InlineImage(tpl,
                                                                                       result['third']['record_score']
                                                                                       ['evaluate_score_project']
                                                                                       ['images'], width=Mm(120))
    result['third']['record_score']['dp_score']['picture'] = InlineImage(tpl,
                                                                         result['third']['record_score']['dp_score']
                                                                         ['images'], width=Mm(120))
    result['third']['record_score']['chejian_dp']['picture'] = InlineImage(tpl,
                                                                           result['third']['record_score']['chejian_dp']
                                                                           ['images'], width=Mm(120))
    result['fourth']['picture'] = InlineImage(tpl, result['fourth']['images'], width=Mm(120))
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calc_year_ratio(year_list):
    """
    计算同比
    :param year_list:
    :return:
    """
    year, last_year = year_list
    if last_year == 0:
        diff = year
        percent = year
    else:
        diff = year - last_year
        percent = round((year - last_year) / last_year * 100, 1)
    return {
        'diff': diff,
        'percent': percent
    }


def _get_health_index1(start_date, end_date, dp_id):
    """
    安全综合指数得分及排名
    :param start_date:
    :param end_date:
    :param dp_id:
    :return:
    """
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MAJOR': '机务',
            'MON': {
                "$gte": int(start_date.split('-')[0] + start_date.split('-')[1]),
                "$lt": int(end_date.split('-')[0] + end_date.split('-')[1])
            },
            'DEPARTMENT_ID': dp_id
        }, {
            "_id": 0,
            'DEPARTMENT_NAME': 1,
            "SCORE": 1,
            'RANK': 1,
            'MON': 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    if data.empty is True:
        data = pd.DataFrame({
            'DEPARTMENT_NAME': ['1', '2', '3', '4'],
            "SCORE": [1, 2, 3, 4],
            'RANK': [1, 2, 3, 4],
            'MON': [1, 2, 3, 4]
        })
    return data


def calc_freq_rank(info_data, num, station_name):
    """
    计算检查排名
    :param info_data:
    :param num:
    :param station_name:
    :return:
    """
    all_list = []
    for i in info_data['NEW_NAME'].value_counts().index:
        data = info_data[info_data['NEW_NAME'] == i]
        # 检查人员数
        person = int(data['ID_CARD'].value_counts().count())
        # 添乘检查数
        tc = len(data[data['CHECK_WAY'] == num])
        # 人均现场检查数
        score = round(ded_zero(tc, person), 1)
        dic = {
            'name': i,
            'score': score
        }
        all_list.append(dic)
    all_list = sorted(all_list, key=lambda x: x['score'], reverse=True)
    s = 0
    for i in all_list:
        if i['name'] == station_name:
            break
        s = s + 1
    return {
        's': s + 1
    }


def calc_ratio(num1, num2):
    if num2 == 0:
        diff = num1
    else:
        diff = round(num1 / num2, 1)
    return diff


# --------------------------------------------------------------第一部分-----------------------------------------------------------
def get_first(start_date, end_date, last_year_start, last_year_end, ded_month, station_name, dp_id, year):
    global problem_data, person_data
    person_data = pd_query(CHECK_PERSON_NUMBER.format(dp_id))
    person_data['NEW_NAME'] = person_data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date, station_name))
    info_data['NEW_NAME'] = info_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    problem_data = pd_query(CHECK_PROBLEM_FREQ_SCORE_SQL.format(start_date, end_date, dp_id))
    problem_data['NEW_NAME'] = problem_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[1])
    last_info_data = pd_query(CHECK_INFO_SQL.format(last_year_start, last_year_end, station_name))
    # (一)总体检查情况
    all_check_info = get_all_check_info(info_data, last_info_data, problem_data, station_name)
    # (二)综合检查力度分析
    check_intensity = get_check_intensity(start_date, end_date, dp_id, year, station_name)
    # (三)检查频次分析
    check_freq = get_check_freq(info_data, ded_month, year, station_name)
    # (四)现场检查质量分析
    xc_check_freq = get_xc_check_freq(problem_data, station_name, person_data, ded_month)
    # (五)添乘检查质量分析
    tc_check_freq = get_tc_check_freq(problem_data, station_name, ded_month)
    # (六)监控调阅质量分析
    shebei_freq = get_shebei_freq(info_data, problem_data, station_name, person_data, start_date, end_date,
                                  dp_id, year)
    return {
        'first_one': all_check_info,
        'first_two': check_intensity,
        'first_three': check_freq,
        'first_four': xc_check_freq,
        'first_five': tc_check_freq,
        'first_six': shebei_freq,
    }


def get_all_check_info(info_data, last_info_data, problem_data, station_name):
    """
    总体检查情况
    :param info_data:
    :param last_info_data:
    :param problem_data:
    :param station_name:
    :return:
    """
    info_data = info_data[info_data['DEPARTMENT_ALL_NAME'].str.contains(station_name)]
    last_info_data = last_info_data[last_info_data['DEPARTMENT_ALL_NAME'].str.contains(station_name)]
    # 现场检查
    xc = []
    # 设备监控调阅
    shebei = []
    # 发现问题
    find_problem = []
    for i in [info_data, last_info_data]:
        xc.append(len(i[i['CHECK_WAY'] == 1]))
        shebei.append(len(i[i['CHECK_WAY'] == 3]))
        find_problem.append(int(i['PROBLEM_NUMBER'].sum()))
    # 现场检查同比
    ratio_xc = calc_year_ratio(xc)
    # 设备监控调阅同比
    ratio_shebei = calc_year_ratio(shebei)
    # 添乘检查
    tc = len(info_data[info_data['CHECK_WAY'] == 2])
    # 定点检查
    # dd = len(info_data[info_data['CHECK_WAY']])
    # 音视频调阅
    mv = len(info_data[(info_data['CHECK_WAY'] == 3) & (info_data['CHECK_ITEM_NAMES'].str.contains('音视频调阅'))])
    # 数据分析
    data_analysis = len(info_data[(info_data['CHECK_WAY'] == 3) & (info_data['CHECK_ITEM_NAMES'].str.contains('数据分析'))])
    # 发现问题占比
    ratio_find = calc_year_ratio(find_problem)
    # 各种问题
    problem1 = []
    problem2 = []
    problem_data = problem_data.dropna()
    for i in ['作业', '设备', '管理', '路外', '反恐防暴']:
        problem1.append(len(problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]))
    for i in ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'G1', 'G2', 'G3', 'K1', 'K3', 'K4']:
        problem2.append(len(problem_data[problem_data['LEVEL'] == i]))
    return {
        'xc': xc[0],
        'shebei': shebei[0],
        'ratio_xc': ratio_xc,
        'ratio_shebei': ratio_shebei,
        'tc': tc,
        # 'dd': dd,
        'mv': mv,
        'data_analysis': data_analysis,
        'problem1': problem1,
        'ratio_find': ratio_find,
        'find_problem': find_problem[0],
        'problem2': problem2
    }


def get_check_intensity(start_date, end_date, dp_id, year, station_name):
    """
    检查安全综合指数
    :param start_date:
    :param end_date:
    :param dp_id:
    :param year:
    :param station_name:
    :return:
    """
    data = _get_health_index1(start_date, end_date, dp_id)
    rank = [1, 2, 3, 4]
    # 排名站段出现次数
    count = []
    for i in rank:
        new_data = data[data['RANK'] == i]
        count.append(len(new_data))
    score = data.sort_values(by='MON').reset_index()['SCORE'].tolist()
    mon = data.sort_values(by='MON').reset_index()['MON'].tolist()
    month = []
    for i in mon:
        month.append(str(i) + '月')
    line = line_picture(month, [score], ['分数'], str(year) + station_name + '检查力度趋势图', 'station_', '机务', 'annual')
    return {
        'count': count,
        'images': line,
    }


def get_check_freq(info_data, ded_month, year, station_name):
    """
    检查频次分析
    :param info_data:
    :param ded_month:
    :param year:
    :param station_name:
    :return:
    """
    # 绘制图形
    draw_image = get_draw_image(info_data, year, station_name)
    # 现场检查次数
    xc = len(info_data[info_data['CHECK_WAY'] == 1])
    # 检查人员总数
    check_person = int(info_data['ID_CARD'].value_counts().count())
    num2 = round(ded_zero(xc, check_person) / ded_month, 1)
    # 人均检查频次
    avg_person_check_freq = calc_freq_rank(info_data, 1, station_name)
    return {
        'draw_image': draw_image,
        'num2': num2,
        'avg_person_check_freq': avg_person_check_freq
    }


def get_draw_image(info_data, year, station_name):
    """
    绘图
    :param info_data:
    :param year:
    :param station_name:
    :return:
    """
    info_data['NEW_DATE'] = info_data['SUBMIT_TIME'].apply(lambda x: ''.join(str(x.date()).split('-')[:2]))
    tc = info_data[info_data['CHECK_WAY'] == 2].groupby('NEW_DATE').count().sort_values(by='NEW_DATE').reset_index()
    times = tc['NEW_DATE'].tolist()
    values1 = tc['CHECK_WAY'].tolist()
    xc = info_data[info_data['CHECK_WAY'] == 1].groupby('NEW_DATE').count().sort_values(by='NEW_DATE').reset_index()
    values2 = xc['CHECK_WAY'].tolist()
    values3 = []
    for i in range(len(values1)):
        values3.append(values1[i] + values2[i])
    line_data = [values1, values2]
    bar_data = [values3]
    images = line_and_bar_picture(times, line_data, bar_data, str(year) + station_name + '检查频次图', 'station_', '机务', 'annual')
    return images


def get_tc_check_freq(problem_data, station_name, ded_month):
    """
    添乘检查
    :param problem_data:
    :param station_name:
    :param ded_month:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_WAY'] == 2]
    # tc检查次数
    tc = len(problem_data)
    find_problem_tc = len(problem_data[problem_data['PROBLEM_NUMBER'] != 0])
    tc_ratio = round(ded_zero(find_problem_tc, tc) * 100, 1)
    # 发现问题质量分
    score = round(ded_zero(int(problem_data[problem_data['PROBLEM_NUMBER'] != 0]['CHECK_SCORE'].sum()), tc), 1)
    # 夜班tc检查比率
    yecha = len(problem_data[problem_data['IS_YECHA'] == 1])
    yecha_ratio = round(ded_zero(yecha, tc) * 100, 1)
    data = problem_data[
        (problem_data['CHECK_WAY'] == 2) & (problem_data['DEPARTMENT_ALL_NAME'].str.contains(station_name))]
    # tc检查次数
    d_tc = len(data)
    avg_d_tc = round(ded_zero(d_tc, ded_month), 1)
    d_find_problem_tc = len(data[data['PROBLEM_NUMBER'] != 0])
    avg_d_find_problem_tc = round(ded_zero(d_find_problem_tc, ded_month), 1)
    d_tc_ratio = round(ded_zero(d_find_problem_tc, d_tc) * 100, 1)
    # 发现问题质量分
    d_score = round(ded_zero(int(data[data['PROBLEM_NUMBER'] != 0]['CHECK_SCORE'].sum()), d_tc), 1)
    # 夜班tc检查比率
    d_yecha = len(data[data['IS_YECHA'] == 1])
    d_yecha_ratio = round(ded_zero(d_yecha, d_tc) * 100, 1)
    return {
        'd_tc': avg_d_tc,
        'd_find_problem_tc': avg_d_find_problem_tc,
        'd_tc_ratio': d_tc_ratio,
        'd_score': d_score,
        'd_yecha_ratio': d_yecha_ratio,
        'up1': round(d_tc_ratio - tc_ratio, 1),
        'up2': round(ded_zero((d_score - score), score) * 100, 1),
        'up3': round(d_yecha_ratio - yecha_ratio, 1)
    }


def get_xc_check_freq(problem_data, station_name, person_data, ded_month):
    """
    现场检查
    :param problem_data:
    :param station_name:
    :param person_data:
    :param ded_month:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_WAY'] == 1]
    person = len(person_data['PERSON_id'].value_counts())
    # xc检查次数
    xc = len(problem_data)
    find_problem_data = problem_data[problem_data['PROBLEM_NUMBER'] != 0]
    find_problem_xc = len(find_problem_data)
    xc_ratio = round(ded_zero(find_problem_xc, xc) * 100, 1)
    # 发现问题质量分
    score = round(ded_zero(int(find_problem_data['CHECK_SCORE'].sum()), find_problem_xc), 1)
    # 夜班xc检查比率
    yecha = len(problem_data[problem_data['IS_YECHA'] == 1])
    yecha_ratio = round(ded_zero(yecha, xc) * 100, 1)
    # 换算单位人均质量分
    avg_score = round(ded_zero(int(find_problem_data['CHECK_SCORE'].sum()), person), 1)

    data = problem_data[
        (problem_data['CHECK_WAY'] == 1) & (problem_data['DEPARTMENT_ALL_NAME'].str.contains(station_name))]
    d_person = len(person_data[person_data['ALL_NAME'].str.contains(station_name)]['PERSON_id'].value_counts())
    # xc检查次数
    d_xc = len(data)
    avg_xc = round(ded_zero(d_xc, ded_month), 1)
    d_find_problem_xc = len(data[data['PROBLEM_NUMBER'] != 0])
    avg_d_find_problem_xc = round(ded_zero(d_find_problem_xc, ded_month), 1)
    d_xc_ratio = round(ded_zero(d_find_problem_xc, d_xc) * 100, 1)
    # 发现问题质量分
    d_score = round(ded_zero(int(data[data['PROBLEM_NUMBER'] != 0]['CHECK_SCORE'].sum()), d_find_problem_xc), 1)
    # 夜班tc检查比率
    d_yecha = len(data[data['IS_YECHA'] == 1])
    d_yecha_ratio = round(ded_zero(d_yecha, d_xc) * 100, 1)
    # 换算单位人均质量分
    d_avg_score = round(ded_zero(int(data[data['PROBLEM_NUMBER'] != 0]['CHECK_SCORE'].sum()), d_person), 1)
    return {
        'd_xc': avg_xc,
        'd_find_problem_xc': avg_d_find_problem_xc,
        'd_xc_ratio': d_xc_ratio,
        'd_score': d_score,
        'd_yecha_ratio': d_yecha_ratio,
        'd_avg_score': d_avg_score,
        'up1': round(d_xc_ratio - xc_ratio, 1),
        'up2': round(ded_zero((d_score - score), score) * 100, 1),
        'up3': round(d_yecha_ratio - yecha_ratio, 1),
        'up4': round(ded_zero((d_avg_score - avg_score), avg_score) * 100, 1)
    }


def get_shebei_freq(info_data, problem_data, station_name, person_data, start_date, end_date, dp_id, year):
    """
    监控设备调阅
    :param info_data:
    :param problem_data:
    :param station_name:
    :param person_data:
    :param start_date:
    :param end_date:
    :param dp_id:
    :param year:
    :return:
    """
    problem_data = problem_data[problem_data['CHECK_WAY'] == 3]
    rank = calc_freq_rank(info_data, 3, station_name)
    person = len(person_data['PERSON_id'].value_counts())
    # xc检查次数
    shebei = len(problem_data)
    find_problem_data = problem_data[problem_data['PROBLEM_NUMBER'] != 0]
    find_problem_xc = len(find_problem_data)
    shebei_ratio = round(ded_zero(find_problem_xc, shebei) * 100, 1)
    # 人均发现问题数
    avg_count = round(ded_zero(len(find_problem_data), person), 1)
    # 换算单位人均质量分
    avg_score = round(ded_zero(int(find_problem_data['CHECK_SCORE'].sum()), person), 1)
    data = problem_data[
        (problem_data['CHECK_WAY'] == 3) & (problem_data['DEPARTMENT_ALL_NAME'].str.contains(station_name))]
    d_person = len(person_data[person_data['ALL_NAME'].str.contains(station_name)]['PERSON_id'].value_counts())
    # xc检查次数
    d_shebei = len(data)
    d_find_problem_shebei = len(data[data['PROBLEM_NUMBER'] != 0])
    d_shebei_ratio = round(ded_zero(d_find_problem_shebei, d_shebei) * 100, 1)
    # 人均发现问题数
    d_avg_count = round(ded_zero(len(data[data['PROBLEM_NUMBER'] != 0]), d_person), 1)
    # 换算单位人均质量分
    d_avg_score = round(ded_zero(int(data[data['PROBLEM_NUMBER'] != 0]['CHECK_SCORE'].sum()), d_person), 1)
    # 监控调阅力度指数
    shebei_intensity = get_shebei_intensity(start_date, end_date, dp_id)
    shebei_data = shebei_intensity.sort_values(by='MON').reset_index()
    months = shebei_data['MON'].tolist()
    months = [str(i) for i in months]
    score = shebei_data['SCORE'].tolist()
    # 绘图
    draw_images = line_picture(months, [score], ['设备'], str(year) + station_name + '监控调阅力度趋势图', 'station_', '机务', 'annual')
    return {
        'rank': rank,
        'd_shebei_ratio': d_shebei_ratio,
        'd_avg_score': d_avg_score,
        'd_avg_count': d_avg_count,
        'up1': round(d_shebei_ratio - shebei_ratio, 1),
        'up2': round(ded_zero((d_avg_count - avg_count), avg_count) * 100, 1),
        'up3': round(ded_zero((d_avg_score - avg_score), avg_score) * 100, 1),
        'images': draw_images
    }


def get_shebei_intensity(start_date, end_date, dp_id):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'detail_health_index'
        doc = list(mongo.db[coll_name].find({
            'MAJOR': '机务',
            'MON': {
                "$gte": int(start_date.split('-')[0] + start_date.split('-')[1]),
                "$lt": int(end_date.split('-')[0] + end_date.split('-')[1])
            },
            'DEPARTMENT_ID': dp_id,
            'MAIN_TYPE': 1,
            'DETAIL_TYPE': 10
        }, {
            "_id": 0,
            'DEPARTMENT_NAME': 1,
            "SCORE": 1,
            'RANK': 1,
            'MON': 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    if data.empty is True:
        data = pd.DataFrame({
            'DEPARTMENT_NAME': ['1', '2', '3', '4'],
            "SCORE": [1, 2, 3, 4],
            'RANK': [1, 2, 3, 4],
            'MON': [1, 2, 3, 4]
        })
    return data


# -------------------------------------------------------第二部分--------------------------------------------------------------
def get_second(last_year_start, last_year_end, station_name, dp_id):
    """
    车间检查质量分析
    :param last_year_start:
    :param last_year_end:
    :param station_name:
    :param dp_id:
    :return:
    """
    data = problem_data[problem_data['DEPARTMENT_ALL_NAME'].str.contains(station_name)]
    last_data = pd_query(CHECK_PROBLEM_FREQ_SCORE_SQL.format(last_year_start, last_year_end, dp_id))
    last_data['NEW_NAME'] = last_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 运用车间检查质量分析
    second_two = get_second_two(data, last_data)
    # 重要检查点检查情况
    main_check = get_main_check(data)
    # 检整车间检查质量分析
    chejian_freq = get_chejian_freq(data, last_data)
    return {
        'second_two': second_two,
        'main_check': main_check,
        'chejian_freq': chejian_freq,
    }


def get_second_two(data, last_data):
    """
    运用车间检查质量分析
    :param data:
    :param last_data:
    :return:
    """
    data = data[data['NEW_NAME'].str.contains('运用车间')]
    last_data = last_data[last_data['NEW_NAME'].str.contains('运用车间')]
    list1 = []
    for x in data['NEW_NAME'].value_counts().index:
        new_data = data[data['NEW_NAME'] == x]
        last_new_data = last_data[last_data['NEW_NAME'] == x]
        # 现场检查
        xc = []
        # 设备监控调阅
        shebei = []
        # 发现问题
        find_problem = []
        for i in [new_data, last_new_data]:
            xc.append(len(i[i['CHECK_WAY'] == 1]))
            shebei.append(len(i[i['CHECK_WAY'] == 3]))
            find_problem.append(int(i['PROBLEM_NUMBER'].sum()))
        # 现场检查同比
        ratio_xc = calc_year_ratio(xc)
        # 设备监控调阅同比
        ratio_shebei = calc_year_ratio(shebei)
        # 添乘检查
        tc = len(new_data[new_data['CHECK_WAY'] == 2])
        # 定点检查
        # dd = len(info_data[info_data['CHECK_WAY']])
        # 音视频调阅
        mv = len(new_data[(new_data['CHECK_WAY'] == 3) & (new_data['CHECK_ITEM_NAMES'].str.contains('音视频调阅'))])
        # 数据分析
        data_analysis = len(
            new_data[(new_data['CHECK_WAY'] == 3) & (new_data['CHECK_ITEM_NAMES'].str.contains('数据分析'))])
        # 发现问题占比
        ratio_find = calc_year_ratio(find_problem)
        # 各种问题
        problem1 = []
        problem2 = []
        problem_data = new_data.dropna()
        for i in ['作业', '设备', '管理', '路外', '反恐防暴']:
            problem1.append(len(problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]))
        for i in ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'G1', 'G2', 'G3', 'K1', 'K3',
                  'K4']:
            problem2.append(len(problem_data[problem_data['LEVEL'] == i]))
        dic = {
            'name': x,
            'xc': xc[0],
            'shebei': shebei[0],
            'ratio_xc': ratio_xc,
            'ratio_shebei': ratio_shebei,
            'tc': tc,
            # 'dd': dd,
            'mv': mv,
            'data_analysis': data_analysis,
            'problem1': problem1,
            'ratio_find': ratio_find,
            'find_problem': find_problem[0],
            'problem2': problem2
        }
        list1.append(dic)
    return {
        'list1': list1
    }


def get_main_check(data):
    """
    重要检查点检查情况
    :param data:
    :return:
    """
    data = data[data['NEW_NAME'].str.contains('运用车间')]
    address = ['调车点', '公寓', '油库']
    for x in data['NEW_NAME'].value_counts().index:
        new_data = data[data['NEW_NAME'] == x]
        for i in address:
            d_data = new_data[new_data['CHECK_ADDRESS_NAMES'].str.contains(i)]
            # name =
    return 1


def get_chejian_freq(data, last_data):
    """
    检整车间检查质量分析
    :param data:
    :param last_data:
    :return:
    """
    top = get_top(data, last_data)
    # 漏检漏修检查情况
    fix = get_fix(data, last_data)
    # 劳动安全检查情况
    laodong = get_fix1(data, last_data, '劳动安全检查', ['A', 'B', 'C', 'D'])
    # 设备质量检查情况
    shebei = get_fix1(data, last_data, '设备质量', ['E1', 'E2', 'E3', 'E4'])
    return {
        'top': top,
        'fix': fix,
        'laodong': laodong,
        'shebei': shebei
    }


def get_top(data, last_data):
    """
    顶部信息
    :param data:
    :param last_data:
    :return:
    """
    data = data[(data['NEW_NAME'].str.contains('整备车间'))
                | (data['NEW_NAME'].str.contains('检修车间'))]
    last_data = last_data[(last_data['NEW_NAME'].str.contains('整备车间'))
                          | (last_data['NEW_NAME'].str.contains('检修车间'))]
    station = []
    for x in data['NEW_NAME'].value_counts().index:
        xc = []
        shebei = []
        mv = []
        shuju = []
        for i in [data, last_data]:
            new_data = i[i['NEW_NAME'] == x]
            xc.append(len(new_data[new_data['CHECK_WAY'] == 1]))
            shebei.append(len(new_data[new_data['CHECK_WAY'] == 3]))
            mv.append(
                len(new_data[(new_data['CHECK_WAY'] == 3) & (new_data['CHECK_ITEM_NAMES'].str.contains('音视频调阅'))]))
            shuju.append(
                len(new_data[(new_data['CHECK_WAY'] == 3) & (new_data['CHECK_ITEM_NAMES'].str.contains('数据分析'))]))
        ratio_xc = calc_year_ratio(xc)
        ratio_shebei = calc_year_ratio(shebei)
        dic = {
            'name': x,
            'xc': xc[0],
            'shebei': shebei[0],
            'mv': mv[0],
            'shuju': shuju[0],
            'ratio_xc': ratio_xc,
            'ratio_shebei': ratio_shebei
        }
        station.append(dic)
    return {
        'station': station
    }


def get_fix(data, last_data):
    """
    漏检漏修检查情况
    :param data:
    :param last_data:
    :return:
    """
    data = data[data['PROBLEM_NUMBER'] != 0]
    last_data = last_data[last_data['PROBLEM_NUMBER'] != 0]
    count = []
    type_count = []
    for i in [data, last_data]:
        new_data = i[(i['PROBLEM_POINT'].str.contains('漏检'))
                     | (i['PROBLEM_POINT'].str.contains('漏修'))]
        count.append(len(new_data))
    ratio = calc_year_ratio(count)
    s_data = data[(data['PROBLEM_POINT'].str.contains('漏检'))
                  | (data['PROBLEM_POINT'].str.contains('漏修'))]
    for i in ['A', 'B', 'C', 'D']:
        type_count.append(len(s_data[s_data['LEVEL'] == i]))
    problem_list = []
    name_list = list(set(s_data['NEW_NAME'].tolist()))
    for i in s_data['PROBLEM_POINT'].value_counts().index:
        count = [i]
        for j in name_list:
            count.append(len(s_data[(s_data['PROBLEM_POINT'] == i) & (s_data['NEW_NAME'] == j)]))
        problem_list.append(count)
    return {
        'count': count[0],
        'ratio': ratio,
        'type_count': type_count,
        'name_list': name_list,
        'problem_list': problem_list
    }


def get_fix1(data, last_data, str1, type_list):
    """
    漏检漏修
    :param data:
    :param last_data:
    :param str1:
    :param type_list:
    :return:
    """
    data = data[data['PROBLEM_NUMBER'] != 0]
    last_data = last_data[last_data['PROBLEM_NUMBER'] != 0]
    count = []
    type_count = []
    for i in [data, last_data]:
        new_data = i[i['CHECK_ITEM_NAMES'].str.contains(str1)]
        count.append(len(new_data))
    ratio = calc_year_ratio(count)
    s_data = data[data['CHECK_ITEM_NAMES'].str.contains(str1)]
    for i in type_list:
        type_count.append(len(s_data[s_data['LEVEL'] == i]))
    problem_list = []
    name_list = list(set(s_data['NEW_NAME'].tolist()))[:6]
    k = 0
    for i in s_data['PROBLEM_POINT'].value_counts().index:
        if k == 8:
            break
        count = [i]
        for j in name_list:
            count.append(len(s_data[(s_data['PROBLEM_POINT'] == i) & (s_data['NEW_NAME'] == j)]))
        problem_list.append(count)
        k = k + 1
    return {
        'count': count[0],
        'ratio': ratio,
        'type_count': type_count,
        'name_list': name_list,
        'problem_list': problem_list
    }


# ---------------------------------------------------------------第三部分------------------------------------------------------
def get_third(start_date, end_date, last_year_start, last_year_end, dp_id, station_name, year):
    """
    干部履职评价情况
    :param start_date:
    :param end_date:
    :param last_year_start:
    :param last_year_end:
    :param dp_id:
    :param station_name:
    :param year:
    :return:
    """
    evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date, dp_id))
    evaluate_data['NEW_NAME'] = evaluate_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    last_evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(last_year_start, last_year_end, dp_id))
    last_evaluate_data['NEW_NAME'] = last_evaluate_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    review_data = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date, dp_id))
    # 基本情况
    third_one = get_basis_situation(evaluate_data, last_evaluate_data, review_data)
    # 记分情况
    record_score = get_record_score(evaluate_data, last_evaluate_data, dp_id, station_name, year)
    # 履职问责情况
    third_three = get_evaluate_duty(evaluate_data)
    return {
        'one': third_one,
        'record_score': record_score,
        'third_three': third_three,
    }


def get_basis_situation(evaluate_data, last_evaluate_data, review_data):
    """
    基本情况
    :param evaluate_data:
    :param last_evaluate_data:
    :param review_data:
    :return:
    """
    # 检查问题信息数
    info_count = []
    # 逐条信息
    zt_i = []
    # 逐条问题
    zt_p = []
    # 设备监控
    jk = []
    for i in [evaluate_data, last_evaluate_data]:
        data = i[i['EVALUATE_TYPE'].isin([1, 2])]
        info_count.append(len(data))
        zt_i.append(len(data[(data['EVALUATE_WAY'] == 1) & (data['EVALUATE_TYPE'] == 1)]))
        zt_p.append(len(data[(data['EVALUATE_WAY'] == 1) & (data['EVALUATE_TYPE'] == 2)]))
        jk.append(len(data[data['SITUATION'].str.contains('监控调阅')]))
    ratio = calc_year_ratio(info_count)
    # 逐条复查
    zt_review_data = review_data[(review_data['EVALUATE_WAY'] == 1) & (review_data['IS_REVIEW'] == 1)]
    zt_review = len(zt_review_data)
    zt_review_i = len(zt_review_data['EVALUATE_TYPE'] == 1)
    i_ratio = round(calc_ratio(zt_review_i, len(review_data['EVALUATE_TYPE'] == 1)) * 100, 1)
    zt_review_p = len(zt_review_data['EVALUATE_TYPE'] == 2)
    p_ratio = round(calc_ratio(zt_review_p, len(review_data['EVALUATE_TYPE'] == 2)) * 100, 1)
    # 定期评价
    dq = len(review_data[review_data['EVALUATE_WAY'] == 2])
    dq_re = len(review_data[(review_data['EVALUATE_WAY'] == 2) & (review_data['IS_REVIEW'] == 1)])
    dq_ratio = round(calc_ratio(dq_re, dq) * 100, 1)
    return {
        'info_count': info_count[0],
        'zt_i': zt_i[0],
        'zt_p': zt_p[0],
        'jk': jk[0],
        'ratio': ratio,
        'zt_review': zt_review,
        'zt_review_i': zt_review_i,
        'i_ratio': i_ratio,
        'zt_review_p': zt_review_p,
        'p_ratio': p_ratio,
        'dq': dq,
        'dq_re': dq_re,
        'dq_ratio': dq_ratio
    }


def get_record_score(evaluate_data, last_evaluate_data, dp_id, station_name, year):
    """
    计分情况
    :param evaluate_data:
    :param last_evaluate_data:
    :return:
    """
    top = _get_top(evaluate_data, last_evaluate_data, dp_id)
    # 履职记分项目情况
    evaluate_score_project = _get_evaluate_project(evaluate_data, station_name, year)
    # 职务层次履职记分情况
    dp_score = _get_dp_score(evaluate_data, last_evaluate_data, station_name, year)
    # 车间部门履职记分情况
    chejian_dp = _get_chejian_dp(evaluate_data, last_evaluate_data, station_name, year)
    return {
        'top': top,
        'evaluate_score_project': evaluate_score_project,
        'dp_score': dp_score,
        'chejian_dp': chejian_dp,
    }


def _get_top(evaluate_data, last_evaluate_data, dp_id):
    """
    top信息
    :param evaluate_data:
    :param last_evaluate_data:
    :return:
    """
    # 履职计分人数
    person = []
    score = []
    compony = []
    all_score = []
    for i in [evaluate_data, last_evaluate_data]:
        data = i[i['SCORE'] != 0]
        person.append(len(data[data['CHECK_PERSON_DEPARTMENT_ID'] == dp_id]))
        score.append(int(data[data['CHECK_PERSON_DEPARTMENT_ID'] == dp_id]['SCORE'].sum()))
        compony.append(len(data[data['CHECK_PERSON_DEPARTMENT_ID'] == '19B8F0430DF157AEE0539106C00AE532KSC']))
        all_score.append(data['SCORE'].sum())
    return {
        'person': person[0],
        'score': score[0],
        'compony': compony[0],
        'all_score': all_score[0],
        'ratio1': calc_year_ratio(person),
        'ratio2': calc_year_ratio(score),
        'ratio3': calc_year_ratio(compony),
        'ratio4': calc_year_ratio(all_score)
    }


def _get_evaluate_project(evaluate_data, station_name, year):
    """
    履职计分项目
    :param evaluate_data:
    :return:
    """
    count = []
    score = []
    name = []
    for i in evaluate_data['SITUATION'].value_counts().index:
        name.append(i.strip().replace('。', ''))
        data = evaluate_data[evaluate_data['SITUATION'] == i]
        count.append(len(data))
        score.append(int(data['SCORE'].sum()))
    x_data = [score, count]
    label_name = ['分值', '次数']
    images = barh_picture(name, x_data, label_name, str(year) + station_name + '履职计分情况', 'staion_', '机务', 'annual')
    return {
        'name': name,
        'count': count,
        'score': score,
        'images': images
    }


def _get_dp_score(evaluate_data, last_data, station_name, year):
    """
    干部履职计分
    :param evaluate_data:
    :return:
    """
    dp = ['正处', '副处', '正科', '副科', '一般管理及专业技术人员', '非管理技术人员']
    all_list = []
    for i in [evaluate_data, last_data]:
        data = i[i['IDENTITY'] == '干部']
        score = []
        for j in dp:
            score.append(len(data[data['GRADATION'].str.contains(j)]))
        all_list.append(score)
    ratio = []
    for i in range(len(all_list[0])):
        ratio.append(calc_year_ratio([all_list[0][i], all_list[1][i]]))
    images = hollow_pie_picture(dp, all_list[0], str(year) + station_name + '干部履职记分级别分布图', 'station_', '机务', 'annual')
    return {
        'all_list': all_list[0],
        'ratio': ratio,
        'images': images
    }


def _get_chejian_dp(evaluate_data, last_evaluate_data, station_name, year):
    """
    车间部门履职记分情况
    :param evaluate_data:
    :return:
    """
    all_list = []
    name = [i for i in evaluate_data['NEW_NAME'].value_counts().index]
    for i in [evaluate_data, last_evaluate_data]:
        count = []
        for j in evaluate_data['NEW_NAME'].value_counts().index:
            data = i[i['NEW_NAME'] == j]
            count.append(len(data))
        all_list.append(count)
    ratio = []
    for i in range(len(all_list[0])):
        ratio.append(calc_year_ratio([all_list[0][i], all_list[1][i]]))
    images = barh_picture(name, [all_list[0]], ['分数'], str(year) + station_name + '各部门履职记分情况', 'station_', '机务', 'annual')
    return {
        'name': name,
        'all_list': all_list[0],
        'ratio': ratio,
        'images': images
    }


def get_evaluate_duty(evaluate_data):
    """
    履职问责情况
    :param evaluate_data:
    :return:
    """
    data = evaluate_data.groupby('ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    count = [0, 0, 0, 0, 0, 0]
    for i in data['ID_CARD'].tolist():
        score = int(data[data['ID_CARD'] == i]['SCORE'])
        if score >= 12:
            count[0] = count[0] + 1
        elif 10 <= score < 12:
            count[1] = count[1] + 1
        elif 8 <= score < 10:
            count[2] = count[2] + 1
        elif 6 <= score < 8:
            count[3] = count[3] + 1
        elif 4 <= score < 6:
            count[4] = count[4] + 1
        elif 2 <= score < 4:
            count[5] = count[5] + 1
    return {
        'count': count
    }


# ------------------------------------------------------第四部分---------------------------------------------------------
def get_fourth(start_date, end_date, last_year_start, last_year_end, station_name, dp_id, ded_month, year):
    assess_data = pd_query(CHECK_ASSESS_SQL.format(start_date, end_date, station_name))
    assess_data['NEW_DATE'] = assess_data['SUBMIT_TIME'].apply(lambda x: int(''.join(str(x.date()).split('-')[:2])))
    last_assess_data = pd_query(CHECK_ASSESS_SQL.format(last_year_start, last_year_end, station_name))
    return_data = pd_query(CHECK_RETURN_MONEY_SQL.format(start_date, end_date, dp_id))
    return_data['NEW_DATE'] = return_data['SUBMIT_TIME'].apply(lambda x: int(''.join(str(x.date()).split('-')[:2])))
    last_return_data = pd_query(CHECK_RETURN_MONEY_SQL.format(last_year_start, last_year_end, dp_id))
    # 考核金额
    assess_money = get_assess_moneny(assess_data, last_assess_data)
    # 返奖金额
    return_money = get_return_money(return_data, last_return_data, ded_month, assess_money['s_date'])
    # 绘图
    monthes = assess_money['s_date']
    money1 = assess_money['money']
    money2 = return_money['money']
    images = bar_picture([money1, money2], monthes, ['月考核', '月返奖'], str(year) + station_name + '考核返奖情况', 'station_', '机务',
                         'annual')
    return {
        'assess_money': assess_money,
        'return_money': return_money,
        'images': images
    }


def get_assess_moneny(assess_data, last_assess_data):
    """
    考核金额
    :param assess_data:
    :param last_assess_data:
    :return:
    """
    # 考核人数
    all_list = [[], [], [], [], [], []]
    for i in [assess_data, last_assess_data]:
        all_list[0].append(len(i))
        all_list[1].append(int(i['ASSESS_MONEY'].sum()))
        all_list[2].append(len(i[i['IDENTITY'] == '干部']))
        all_list[3].append(int(i[i['IDENTITY'] == '干部']['ASSESS_MONEY'].sum()))
        all_list[4].append(len(i[i['IDENTITY'] == '工人']))
        all_list[5].append(int(i[i['IDENTITY'] == '工人']['ASSESS_MONEY'].sum()))
    ratio = []
    for i in range(6):
        ratio.append(calc_year_ratio([all_list[i][0], all_list[i][1]]))
    # 超期整改
    zg_time = assess_data['RECTIFY_TIME'].tolist()
    zg_fin = assess_data['RECTIFY_DATE'].tolist()
    a = 0
    for i in range(len(zg_time)):
        if datetime.datetime.strptime(str(zg_time[i]), '%Y-%m-%d %H:%M:%S').date() > datetime.datetime.strptime(
                str(zg_fin[i]), '%Y-%m-%d').date():
            a = a + 1
    # 月考核金额
    new_data = assess_data.groupby('NEW_DATE').sum().sort_values(by='ASSESS_MONEY').reset_index()
    s_date = new_data['NEW_DATE'].tolist()
    months = new_data['NEW_DATE'].tolist()
    money = new_data['ASSESS_MONEY'].tolist()
    return {
        'all_list': all_list,
        'ratio': ratio,
        'a': a,
        'months': months,
        'money': money,
        's_date': s_date
    }


def get_return_money(return_data, last_return_data, ded_month, s_date):
    """
    返奖金额
    :param return_data:
    :param last_return_data:
    :return:
    """
    all_list = [[], [], []]
    for i in [return_data, last_return_data]:
        all_list[0].append(len(i))
        all_list[1].append(int(i['MONEY'].sum()))
        all_list[2].append(round(ded_zero(len(i), ded_month), 1))
    ratio = []
    for i in range(3):
        ratio.append(calc_year_ratio([all_list[i][0], all_list[i][1]]))
    # 月返奖金额
    moneys = []
    for i in s_date:
        new_data = return_data[return_data['NEW_DATE'] == i]
        if new_data.empty is True:
            money = 0
        else:
            money = int(new_data['MONEY'].sum())
        moneys.append(money)
    return {
        'all_list': all_list,
        'ratio': ratio,
        'money': moneys
    }


# -------------------------------------------------第五部分-----------------------------------------------------------
def get_fifth(start_date, end_date, last_year_start, last_year_end, dp_id):
    """
    谈心帮教情况
    :return:
    """
    talk_data = pd_query(CHECK_TALK_HEART_SQL.format(start_date, end_date, dp_id))
    last_talk_data = pd_query(CHECK_TALK_HEART_SQL.format(last_year_start, last_year_end, dp_id))
    # 谈心情况
    talk_situation = get_talk_situation(talk_data, last_talk_data)
    return {
        'talk_situation': talk_situation
    }


def get_talk_situation(talk_data, last_talk_data):
    """
    谈心情况分析
    :param talk_data:
    :param last_talk_data:
    :return:
    """
    count = []
    for i in [talk_data, last_talk_data]:
        count.append(len(i))
    num = []
    ratio = calc_year_ratio(count)
    for i in range(4):
        num.append(len(talk_data[talk_data['TYPE'] == i]))
    return {
        'count': count[0],
        'ratio': ratio,
        'num': num
    }
