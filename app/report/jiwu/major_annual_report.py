from app.report.jiwu.major_annual_report_data import get_data
from app import mongo
from app.report.analysis_report_manager import AnnualAnalysisReport
from app.report.jiwu import major_annual_report_data as report_data
from docxtpl import DocxTemplate
import os


class JiwuMajorAnnualAnalysisReport(AnnualAnalysisReport):
    """
    电务的专业级的年分析报告类。
    """

    def __init__(self):
        super(JiwuMajorAnnualAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='机务')

    def generate_report_data(self, year):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :return:
        """
        data = get_data(year)
        mongo.db['safety_analysis_annual_report'].delete_one(
            {
                "year": year,
                "major": self.major,
                "hierarchy": "MAJOR",
            })
        mongo.db['safety_analysis_annual_report'].insert_one(data)
        return data

    def generate_report(self, year):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :return:
        """
        data = self.load_report_data(year)
        tpl = DocxTemplate('app/report/template/jiwu_major_annual_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path, image_path = self.get_report_paths()
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']