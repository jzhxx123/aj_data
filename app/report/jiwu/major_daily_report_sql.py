# 安全生产信息
without_accident_sql = """SELECT
            a.*,
            d.NAME AS STATION
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
        WHERE a.OCCURRENCE_TIME between '{0}' and '{1}'
            AND c.TYPE2='1ACE7D1C80B14456E0539106C00A2E70KSC'
        """

# 检查信息
check_info_sql = """SELECT a.*,f.TYPE3 AS ST,d.NAME AS STATION,b.TYPE
FROM `t_check_info` a 
LEFT JOIN t_check_info_and_address b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
LEFT JOIN t_check_info_and_person e on e.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID = e.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=f.TYPE3
WHERE SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND f.TYPE2='1ACE7D1C80B14456E0539106C00A2E70KSC'
"""

# 问题信息
check_problem_sql = """SELECT p.PK_ID,i.CHECK_WAY,p.`LEVEL`,f.TYPE3,p.PROBLEM_POINT,
p.RISK_LEVEL,e.`NAME` AS STATION,p.PROBLEM_SCORE,p.TYPE,b.STATUS,p.DESCRIPTION,p.PROBLEM_DIVIDE_IDS
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_check_info_and_person d on d.FK_CHECK_INFO_ID=i.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID = d.FK_DEPARTMENT_ID
where i.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and c.TYPE2='1ACE7D1C80B14456E0539106C00A2E70KSC'
"""

# 安全预警通知书
warning_notification_sql = """SELECT
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						a.CONTENT,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.NAME AS STATION
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')
					and b.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'"""


# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION,
		g.CHECK_WAY
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
            LEFT JOIN
        t_check_info g on g.PK_ID=a.FK_CHECK_OR_PROBLEM_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND  c.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
"""

# 音视频检查信息
CHECK_MV_COST_TIME_SQL = """SELECT 
        a.PK_ID,
        b.CHECK_PERSON_NAMES,
        b.DEPARTMENT_ALL_NAME,
        a.COST_TIME,
        b.CHECK_ITEM_NAMES,
        b.CHECK_WAY,
        d.ALL_NAME,
        e.`NAME` AS STATION,
        a.MONITOR_POST_NAMES,
        a.RETRIVAL_TYPE_NAME
FROM
    t_check_info_and_media AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        LEFT JOIN
    t_check_info_and_person c on c.FK_CHECK_INFO_ID=b.PK_ID
        LEFT JOIN
    t_department d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
        LEFT JOIN 
    t_department e on e.DEPARTMENT_ID=d.TYPE3
WHERE
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
    AND d.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
"""