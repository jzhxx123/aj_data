# 安全生产信息
CHECK_SAFETY_INFO_SQL = """SELECT
        a.*,
        c.DEPARTMENT_ID,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED,
        g.TYPE,
        c.`NAME` as STATION
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID  
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1} 23:59:59')
            and c.TYPE3='{2}'"""


# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY,f.NAME AS SHOP,c.IS_YECHA,
e.ID_CARD
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department f on f.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'"""


# 问题信息
check_problem_sql = """SELECT p.*,c.CHECK_WAY,b.NAME,e.NAME AS SHOP,c.PROBLEM_NUMBER,d.`STATUS`,c.CHECK_PERSON_NAMES,
f.RISK_TYPE_NAME,f.CHECK_SCORE
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department d on d.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department e on e.DEPARTMENT_ID=a.TYPE4
LEFT JOIN t_problem_base f on f.PK_ID=p.FK_PROBLEM_BASE_ID
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'"""


# 车间总数
SHOP_NUMBER_SQL = """SELECT count(*) AS COUNT
from t_department 
WHERE FK_PARENT_ID = '{0}' 
AND IS_DELETE=0 
AND LENGTH(SHORT_NAME)>0
AND TYPE=8"""


# 检查地点
CHECK_ADDRESS_SQL = """SELECT a.PK_ID,b.TYPE,d.`NAME`,f.`NAME` as POINT,c.TYPE AS LEIXING
from t_check_info a
LEFT JOIN t_check_info_and_address b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_info_and_person e on e.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=e.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE4
LEFT JOIN t_check_point f on f.PK_ID=b.FK_CHECK_POINT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
AND c.TYPE3 = '{2}' AND c.TYPE = 8 AND LENGTH(c.SHORT_NAME)>0 AND c.IS_DELETE=0"""


# 重要检查点信息
MAIN_CHECK_POINT_SQL = """
SELECT count(*) AS COUNT from t_check_point a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
where b.type3='{0}'"""


# 量化信息完成情况
t_quanttization_sql = """SELECT
        a.ID_CARD, a.PERSON_NAME, dp.ALL_NAME, 
        a.CHECK_TIMES_TOTAL,
        a.CHECK_NOTIFICATION_TIMES_TOTAL,
        a.MONITOR_NUMBER_TOTAL,
        a.PROBLEM_NUMBER_TOTAL,
        a.WORK_PROBLEM_NUMBER_TOTAL,
        a.MONITOR_PROBLEM_NUMBER_TOTAL,
        a.HIDDEN_DANGER_RECHECK_TIMES_TOTAL,
        a.RISK_RECHECK_TIMES_TOTAL,
        a.IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL,
        a.MIN_QUALITY_GRADES_TOTAL,
        a.MANAGE_PROBLEM_NUMBER_TOTAL,
        a.DEVICE_PROBLEM_NUMBER_TOTAL,
        a.ENVIRONMENT_PROBLEM_NUMBER_TOTAL,
        a.OUT_WORK_PROBLEM_NUMBER_TOTAL,
        b.REALITY_NUMBER,
        b.CHECK_NOTIFICATION_REALITY_NUMBER,
        b.MEDIA_REALITY_TIME,
        b.REALITY_PROBLEM_NUMBER,
        b.REALITY_WORK_ITEM_PROBLEM_NUMBER,
        b.MEDIA_REALITY_PROBLEM_NUMBER,
        b.HIDDEN_DANGER_RECHECK_REALITY_NUMBER,
        b.RISK_RECHECK_REALITY_NUMBER,
        b.IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER,
        b.REALITY_MIN_QUALITY_GRADES,
        b.REALITY_MANAGE_PROBLEM_NUMBER,
        b.REALITY_DEVICE_PROBLEM_NUMBER,
        b.REALITY_ENVIRONMENT_PROBLEM_NUMBER,
        b.REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER,
        dp.TYPE3 AS STATION_ID, dp.TYPE4 AS SECTION_ID,
        sp.`NAME` AS SHOP
    FROM t_quantization_base_quota a
        LEFT JOIN t_quantify_assess_real_time b ON a.ID_CARD = b.ID_CARD AND a.MONTH = b.MONTH AND a.YEAR = b.YEAR
        LEFT JOIN t_department dp ON a.FK_DEPARTMENT_ID = dp.DEPARTMENT_ID
        LEFT JOIN t_department sp ON sp.DEPARTMENT_ID=dp.TYPE4
    WHERE dp.TYPE3 = '{2}'
    AND a.YEAR = {0} AND a.MONTH = {1}"""


# 检查通知书
check_notification_sql = """
SELECT * from t_check_notification a 
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.ISSUE_TIME BETWEEN '{0}' and '{1} 23:59:59'
AND b.TYPE3='{2}'"""


# 问题责任人信息
PROBLEM_DUTY_PERSON_SQL = """
SELECT d.PK_ID,d.POSITION,d.JOB,d.BIRTHDAY,g.`NAME`,b.LEVEL
from t_check_info a
LEFT JOIN t_check_problem b on a.pk_id=b.fk_check_info_id
LEFT JOIN t_check_problem_and_responsibility_person c on c.FK_CHECK_PROBLEM_ID=b.PK_ID
LEFT JOIN t_person d on d.ID_CARD=c.ID_CARD
LEFT JOIN t_department e on d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
LEFT JOIN t_department g on g.DEPARTMENT_ID=e.TYPE4
where a.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and e.TYPE3 = '{2}'"""


# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        e.JOB,
        e.IDENTITY,
        f.NAME AS SHOP,
        d.CHECK_WAY
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE4
            LEFT JOIN
        t_check_info d on d.PK_ID=a.FK_CHECK_OR_PROBLEM_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE3 = '{2}'
"""

# 履职复查信息查询语句
check_evaluate_review_sql = """SELECT
a.*,c.`NAME`,d.CHECK_WAY
FROM
t_check_evaluate_review_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE4
LEFT JOIN t_check_info d on d.PK_ID=a.FK_CHECK_OR_PROBLEM_ID
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE3 = '{2}'
"""


# 重点人员预警库
CHECK_IIILEGE_SQL = """SELECT c.ID_CARD,a.`YEAR`,a.`MONTH`,a.DEDUCT_SCORE,c.POSITION,c.BIRTHDAY,d.ALL_NAME,e.NAME,c.JOB
from t_warning_key_person_library a
LEFT JOIN t_person c on a.ID_CARD = c.ID_CARD
LEFT JOIN t_department d on a.fk_unit_id = d.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE4
where a.`YEAR`={0} and a.`MONTH`={1} 
and d.TYPE3 = '{2}'"""