from app import mongo
from app.report.jiwu.major_monthly_report_data import get_data
import app.report.analysis_report_manager as manager


class JiwuMajorMonthlyAnalysisReport(manager.MonthlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self):
        super(JiwuMajorMonthlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='机务',)

    def generate_report_data(self, year, month):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param month: int 月
        :return:
        """
        data = get_data(year, month)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "month": month,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_monthly_report'].insert_one(data)
        return data