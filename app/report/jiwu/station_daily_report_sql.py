without_accident_sql = """SELECT
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
            WHERE a.OCCURRENCE_TIME <= '{0}' and c.TYPE3='{1}'
        """

withouts_accident_sql = """SELECT
            a.PK_ID,
            a.MAIN_TYPE,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.RESPONSIBILITY_DIVISION_NAME,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR,
            e.CHEWU_OWN AS TYPE
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
                LEFT JOIN
            t_check_problem AS e ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME between '{0}' and '{1}' and c.TYPE3='{2}'
        """

# 重要检查点
check_info_sql = """
SELECT a.CHECK_WAY,e.NAME,a.IS_KECHE,a.IS_DONGCHE,c.`NAME` as POINT,a.IS_YECHA,f.POSITION
FROM `t_check_info` a
LEFT JOIN t_check_info_and_address b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_point c on c.PK_ID=b.FK_CHECK_POINT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE4
LEFT JOIN t_person f on f.ID_CARD=a.ID_CARD
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE3='{2}'"""

check_problem_sql = """SELECT b.PROBLEM_POINT,b.`LEVEL`,a.IS_GAOTIE,a.IS_DONGCHE,e.`NAME`,b.PROBLEM_SCORE,b.DESCRIPTION,
b.TYPE,b.IS_EVALUATE,f.POSITION,b.PROBLEM_CLASSITY_NAME,b.IS_RED_LINE
FROM `t_check_info` a
LEFT JOIN t_check_problem b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_check_info_and_person c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE4
LEFT JOIN t_person f on f.ID_CARD=a.ID_CARD
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE3 = '{2}'
"""

check_mv_sql = """SELECT a.PK_ID,a.CHECK_WAY,b.TYPE,e.`NAME`,b.COST_TIME,f.POSITION
FROM `t_check_info` a
LEFT JOIN t_check_info_and_media b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_check_info_and_person c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE4
LEFT JOIN t_person f on f.ID_CARD=a.ID_CARD
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE3 = '{2}'
"""

warning_notification_sql = """SELECT
a.HIERARCHY,
c.`NAME`
FROM
t_warning_notification AS a
INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
INNER JOIN t_department AS c ON b.TYPE4 = c.DEPARTMENT_ID
WHERE
a.CREATE_TIME BETWEEN Date('{0}')
AND Date('{1}')
AND b.TYPE3 = '{2}'"""


main_info_sql = """select a.HIERARCHY,a.CONTENT,a.SURVEY_DEPARTMENT_NAMES,c.POSITION 
FROM t_key_information_tracking a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_VEST_DEPARTMENT_ID
LEFT JOIN t_person c on c.ID_CARD = a.TRACKING_PERSON_ID_CARDS
WHERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND b.TYPE3='{2}'"""


# 超期问题
zg_sql = """SELECT b.TYPE,d.POSITION,e.`NAME` from t_check_problem a
LEFT JOIN t_safety_assess_month_problem_detail b on b.FK_CHECK_PROBLEM_ID=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = c.TYPE4
LEFT JOIN t_person d on d.ID_CARD=b.ID_CARD
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND c.TYPE3='{2}'"""