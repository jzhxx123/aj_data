# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY,f.NAME AS SHOP
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department f on f.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'"""


# 问题信息
check_problem_sql = """SELECT p.PK_ID,c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,e.NAME AS SHOP,p.RISK_NAMES,p.KEY_ITEM,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.`STATUS`,p.PROBLEM_POINT,p.IS_RED_LINE,c.CHECK_PERSON_NAMES,p.DESCRIPTION,p.WORK_PROCEDURE
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department d on d.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department e on e.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'"""


# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION,
		g.CHECK_WAY
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
            LEFT JOIN
        t_check_info g on g.PK_ID=a.FK_CHECK_OR_PROBLEM_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
"""

# 履职复查信息查询语句
check_evaluate_review_sql = """SELECT
a.*,c.`NAME`
FROM
t_check_evaluate_review_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE2 = '1ACE7D1C80B14456E0539106C00A2E70KSC'
"""
