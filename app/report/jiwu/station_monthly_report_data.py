import datetime
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
from app.data.util import pd_query
import pandas as pd
from app.report.jiwu.station_monthly_report_sql import *

_PROBLEM_TYPE = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4'],
    '外部环境': ['G1', 'G2', 'G3', 'G4'],
    '反恐防暴': ['K1', 'K2', 'K3', 'K4'],
    '高质量': ['A', 'B', 'F1', 'F2', 'E1', 'E2']
}


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, month, station_id):
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    last_year_start = str(start_date - relativedelta(months=12))[:10]
    last_year_end = str(end_date - relativedelta(months=12))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    # 安全生产信息
    produce_data = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date, station_id))
    year_produce = pd_query(CHECK_SAFETY_INFO_SQL.format('2017-10-01', end_date, station_id))
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date, station_id))
    last_info = pd_query(check_info_sql.format(last_month_start, last_month_end, station_id))
    year_info = pd_query(check_info_sql.format(last_year_start, last_year_end, station_id))
    # 问题信息
    pro_data = pd_query(check_problem_sql.format(start_date, end_date, station_id))
    last_pro = pd_query(check_problem_sql.format(last_month_start, last_month_end, station_id))
    # 检查地点
    check_address = pd_query(CHECK_ADDRESS_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 量化人员信息
    qt_data = pd_query(t_quanttization_sql.format(start_date, end_date, station_id))
    # 检查通知书
    check_notification = pd_query(check_notification_sql.format(start_date, end_date, station_id))
    # 问题责任人
    problem_person = pd_query(PROBLEM_DUTY_PERSON_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 履职评价
    eva_data = pd_query(check_evaluate_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_eva = pd_query(check_evaluate_sql.format(last_month_start, last_month_end, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    year_eva = pd_query(check_evaluate_sql.format(start_date[:4]+'-01-01', end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职复查
    re_eva = pd_query(check_evaluate_review_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 重点人员
    iiilege = pd_query(CHECK_IIILEGE_SQL.format(year, month, station_id))

    first = get_first(produce_data, year_produce)
    second = get_second(info_data, last_info, year_info, pro_data, last_pro, station_id, check_address, qt_data,
                        check_notification, problem_person)
    third = get_third(eva_data, last_eva, year_eva, info_data, pro_data)
    four = get_four(iiilege)
    five = get_five(eva_data, re_eva)
    six = get_six(pro_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '机务',
        "station_id": station_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'first': first,
        'second': second,
        'third': third,
        'four': four,
        'five': five,
        'six': six
    }
    return result


def calculate_year_and_ring_ratio(counts):
    """
    计算环比和同比
    :param counts:
    :return:
    """
    now_count, year_count, month_count = counts
    year_ratio = now_count - year_count
    if year_count == 0:
        year_percent = year_ratio * 100
    else:
        year_percent = round(year_ratio / year_count, 1) * 100
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'year_diff': year_ratio,
        'year_percent': year_percent,
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def calc_tongbi(count1, count2):
    if count2 == 0:
        return {
            'month_diff': count1,
            'month_percent': count1 * 100
        }
    else:
        return {
            'month_diff': count1 - count2,
            'month_percent': round((count1 - count2) / count2 * 100, 2)
        }


# ----------------------------------------------------------------------------------第一点-----------------------------------------------------------------
def get_first(produce_data, year_produce):
    """
    段安全生产基本情况分析
    :param produce_data:
    :return:
    """
    duty_acc = len(
        produce_data[(produce_data['MAIN_TYPE'] == 1) & (produce_data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)])
    # 无责任
    data = year_produce[year_produce['FK_RESPONSIBILITY_DIVISION_ID'] == 667]
    dic = {}
    # 无责任ABCD类事故天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[data['CODE'].str.contains(i)]
        if len(accident) == 0:
            dic['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                    'OCCURRENCE_TIME'].date()).split(' ')[0]
    # 死亡、重伤、轻伤无责任事故天数
    times = []
    for i in [['ROAD_IN_DEAD', 'ROAD_OUT_DEAD'], ['ROAD_IN_SERIOUS_INJURY', 'ROAD_OUT_SERIOUS_INJURY'],
              ['ROAD_IN_MINOR_INJURY', 'ROAD_OUT_MINOR_INJURY']]:
        data1 = data[data[i[0]] == 1]
        data2 = data[data[i[1]] == 1]
        if len(data1) == 0:
            time1 = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            time1 = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                'OCCURRENCE_TIME'].date()).split(' ')[0]
        if len(data2) == 0:
            time2 = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            time2 = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                'OCCURRENCE_TIME'].date()).split(' ')[0]
        if int(time1) <= int(time2):
            times.append(time1)
        else:
            times.append(time2)
    return {
        'duty_acc': duty_acc,
        'dic': dic,
        'times': times
    }


# --------------------------------------------------------------第二点---------------------------------------------------
def get_second(info_data, last_info, year_info, pro_data, last_pro, station_id, check_address, qt_data,
               check_notification, problem_person):
    """
    检查基本情况统计分析
    :param info_data:
    :param last_info:
    :param year_info:
    :param pro_data:
    :param last_pro:
    :param station_id:
    :param check_address:
    :param qt_data:
    :param check_notification:
    :param problem_person:
    :return:
    """
    # 检查基本情况
    check_base = get_check_base(info_data, year_info, last_info, pro_data, last_pro, station_id, check_address)
    # 纳入量化干部录入情况
    qt_gb = get_qt_gb(info_data, last_info, qt_data)
    # 其他管理人员（工班长及分析组）录入情况
    qt_person = get_qt_person(pro_data, last_pro, qt_data)
    # 上级部门录入情况
    top_dp = get_top_dp(pro_data, last_pro)
    # 检查通知书录入情况
    check_notifications = get_check_notification(check_notification)
    # 问题统计分析
    problem_analysis = get_problem_analysis(pro_data, problem_person)
    # 按年龄划分责任
    age_duty = get_age_duty(problem_person)
    # 按风险大类分
    risk_type_name = get_risk_type_name(pro_data)
    return {
        'check_base': check_base,
        'qt_gb': qt_gb,
        'qt_person': qt_person,
        'top_dp': top_dp,
        'check_notifications': check_notifications,
        'problem_analysis': problem_analysis,
        'age_duty': age_duty,
        'risk_type_name': risk_type_name
    }


def get_check_base(info_data, year_info, last_info, pro_data, last_pro, station_id, check_address):
    # 车间数
    cj_number = int(pd_query(SHOP_NUMBER_SQL.format(station_id))['COUNT'])
    # 重要检查点数
    main_point = int(pd_query(MAIN_CHECK_POINT_SQL.format(station_id))['COUNT'])
    # 全段总体情况
    all_station_sit = get_all_station_sit(info_data, year_info, last_info)
    # 按问题划分分析
    pro_point = get_pro_point(pro_data, last_pro)
    # 夜查， 问题， 考核率
    yc = calc_ring(info_data, 'IS_YECHA', 1)
    pro = calc_ring(pro_data, 'TYPE', 3)
    kh = calc_ring(pro_data, 'IS_ASSESS', 1)
    # 检查点覆盖
    check_address = get_check_adress(cj_number, main_point, check_address)
    return {
        'ast': all_station_sit,
        'pro_point': pro_point,
        'yc': yc,
        'pro': pro,
        'kh': kh,
        'check_address': check_address
    }


def get_all_station_sit(info_data, year_info, last_info):
    all_data = []
    for i in [info_data, year_info, last_info]:
        # 录入信息条数
        count = len(i)
        # 发现问题得信息条数
        pro_info = len(i[i['PROBLEM_NUMBER'] != 0])
        # 发现问题率
        ratio = round(ded_zero(pro_info, count) * 100, 2)
        # 夜查信息条数
        yc = len(i[i['IS_YECHA'] == 1])
        # 夜查率
        yc_ratio = round(ded_zero(yc, count) * 100, 2)
        # 监控设备回访
        shebei = len(i[i['CHECK_WAY'].isin([3, 4])])
        # 发现问题数
        pro = int(i['PROBLEM_NUMBER'].sum())
        all_data.append([count, pro_info, ratio, yc, yc_ratio, shebei, pro])
    # 环比同比计算
    ratios = []
    for i in range(len(all_data[0])):
        if i not in [2, 4]:
            ratios.append(calculate_year_and_ring_ratio([all_data[0][i], all_data[1][i], all_data[2][i]]))
        else:
            continue
    return {
        'all_data': all_data[0],
        'ratios': ratios
    }


def get_pro_point(pro_data, last_pro):
    # 本月和上月各类问题信息
    all_count = []
    for i in [pro_data, last_pro]:
        count = []
        for j in ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'K1', 'K2', 'K3', 'K4']:
            data = i[i['LEVEL'] == j]
            count.append(len(data))
        all_count.append(count)
    # 严重问题数
    main_pro = sum(all_count[0][0:2]) + sum(all_count[0][4:6]) + sum(all_count[0][8:10])
    ratio = round(ded_zero(main_pro, sum(all_count[0])) * 100, 2)
    return {
        'all_count': all_count,
        'main_pro': main_pro,
        'ratio': ratio
    }


def calc_ring(data, strs, num):
    """
    计算夜查率、问题发现率、考核率
    :param data:
    :param strs:
    :param num:
    :return:
    """
    # 总次数
    count = len(data[data[strs] == num])
    # 职能科室
    znks = calc_shop_ring(data, '科', strs, num)
    # 运用车间
    yycj = calc_shop_ring(data, '运用车间', strs, num)
    # 整备车间
    zbcj = calc_shop_ring(data, '整备车间', strs, num)
    return {
        'count': count,
        'znks': znks,
        'yycj': yycj,
        'zbcj': zbcj
    }


def calc_shop_ring(data, strs, str1, num):
    """
    计算职能科室、运用车间、检整车间
    :param data:
    :param strs:
    :param str1:
    :param num:
    :return:
    """
    all_shops = []
    shop = data[data['SHOP'].str.contains(strs)]
    for i in shop['SHOP'].unique().tolist():
        shop_data = shop[shop['SHOP'] == i]
        # 总数
        count = len(shop_data)
        # 夜查次数
        yc = len(shop_data[shop_data[str1] == num])
        # 夜查率
        yc_ratio = round(ded_zero(yc, count) * 100, 2)
        dic = {
            'name': i,
            'ratio': yc_ratio
        }
        all_shops.append(dic)
    all_shops = sorted(all_shops, key=lambda x: x['ratio'], reverse=True)
    return all_shops


def get_check_adress(cj_number, main_point, check_address):
    # 检查覆盖车间数
    cj = len(check_address[check_address['TYPE'] == 1]['NAME'].unique())
    # 未覆盖
    wfg_cj = cj_number - cj
    # 检查覆盖重要检查点数
    main = len(check_address[check_address['TYPE'] == 2]['POINT'].unique())
    # 未覆盖
    wfg_main = main_point - main
    return {
        'cj': cj,
        'wfg_cj': wfg_cj,
        'main': main,
        'wfg_main': wfg_main
    }


def get_qt_gb(info_data, last_info, qt_data):
    # 量化人员ID_CARD
    qt_id_card = qt_data['ID_CARD'].unique().tolist()
    info = info_data[info_data['ID_CARD'].isin(qt_id_card)]
    last_info = last_info[last_info['ID_CARD'].isin(qt_id_card)]
    # 录入信息
    all_data = []
    for i in [info, last_info]:
        # 录入信息条数
        count = len(i)
        # 发现问题得信息条数
        pro_info = len(i[i['PROBLEM_NUMBER'] != 0])
        # 发现问题率
        ratio = round(ded_zero(pro_info, count) * 100, 2)
        # 夜查信息条数
        yc = len(i[i['IS_YECHA'] == 1])
        # 夜查率
        yc_ratio = round(ded_zero(yc, count) * 100, 2)
        # 监控设备回访
        shebei = len(i[i['CHECK_WAY'].isin([3, 4])])
        # 发现问题数
        pro = int(i['PROBLEM_NUMBER'].sum())
        all_data.append([count, pro_info, ratio, yc, yc_ratio, shebei, pro])
    # 环比同比计算
    ratios = []
    for i in range(len(all_data[0])):
        if i not in [2, 4]:
            ratios.append(calc_tongbi(all_data[0][i], all_data[1][i]))
        else:
            continue
    return {
        'all_data': all_data[0],
        'ratios': ratios
    }


def get_qt_person(pro_data, last_pro, qt_data):
    # 量化人员ID_CARD
    qt_id_card = qt_data['ID_CARD'].unique().tolist()
    pro = pro_data[~pro_data['ID_CARD'].isin(qt_id_card)]
    last_pro = last_pro[~last_pro['ID_CARD'].isin(qt_id_card)]
    # 录入问题数
    count = len(pro)
    ratio = calc_tongbi(count, len(last_pro))
    # 各车间发现问题数
    all_shops = []
    for i, j in pro.groupby('SHOP').count().reset_index().iterrows():
        dic = {
            'name': j['SHOP'],
            'count': int(j['LEVEL'])
        }
        all_shops.append(dic)
    return {
        'count': count,
        'ratio': ratio,
        'all_shops': all_shops
    }


def get_top_dp(pro_data, last_pro):
    # 上级部门
    pro_data = pro_data[pro_data['TYPE'].isin([1, 2])]
    last_pro = last_pro[last_pro['TYPE'].isin([1, 2])]
    # 录入问题数
    count = len(pro_data)
    ratios = calc_tongbi(count, len(last_pro))
    # 本月和上月问题数
    all_count = []
    for i in [pro_data, last_pro]:
        count = []
        for j in ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'K1', 'K2', 'K3', 'K4']:
            data = i[i['LEVEL'] == j]
            count.append(len(data))
        all_count.append(count)
    # 严重问题数
    main_pro = sum(all_count[0][0:2]) + sum(all_count[0][4:6]) + sum(all_count[0][8:10])
    ratio = round(ded_zero(main_pro, sum(all_count[0])) * 100, 2)
    return {
        'count': count,
        'ratios': ratios,
        'all_count': all_count,
        'main_pro': main_pro,
        'ratio': ratio
    }


def get_check_notification(check_notification):
    count = len(check_notification)
    return count


def get_problem_analysis(pro_data, problem_person):
    # 责任部门
    duty_dp = len(pro_data['SHOP'].unique())
    # 总问题数
    count = len(pro_data)
    all_pro = []
    for i in ['作业', '管理', '设备', '高质量']:
        data = pro_data[pro_data['LEVEL'].isin(_PROBLEM_TYPE[i])]
        all_pro.append({
            'count': len(data),
            'ratio': round(ded_zero(len(data), count) * 100, 2)
        })
    # 问题分项
    all_type = []
    for i in ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F2', 'F3', 'F4', 'K3', 'K4']:
        data = pro_data[pro_data['LEVEL'] == i]
        # 添乘+现场+设备
        all_way = []
        for j in [2, 1, 3]:
            all_way.append(len(data[data['CHECK_WAY'] == j]))
        dic = {
            'all_way': all_way,
            'count': len(data)
        }
        all_type.append(dic)
    # 按检查方式分类
    check_way = []
    check_way_ratio = []
    for j in [1, 2, 3]:
        data = pro_data[pro_data['CHECK_WAY'] == j]
        check_way.append(len(data))
        check_way_ratio.append(round(ded_zero(len(data), count) * 100, 2))
    # 按责任岗位分类
    all_position = []
    for j, k in problem_person.groupby('POSITION').count().sort_values('LEVEL',
                                                                       ascending=False).reset_index().iterrows():
        dic = {
            'name': k['POSITION'],
            'count': int(k['LEVEL'])
        }
        all_position.append(dic)
    return {
        'duty_dp': duty_dp,
        'all_pro': all_pro,
        'all_type': all_type,
        'count': count,
        'check_way': check_way,
        'check_way_ratio': check_way_ratio,
        'all_position': all_position
    }


def get_age_duty(problem_person):
    year = datetime.datetime.now().year
    person = problem_person[problem_person['BIRTHDAY'] != ' ']
    person['AGE'] = person['BIRTHDAY'].apply(lambda x: int(year) - int(x[:4]))
    person['AGE_TYPE'] = person['AGE'].apply(
        lambda x: 1 if 20 <= x < 30 else (2 if 30 <= x < 40 else (3 if 40 <= x < 50 else 4)))
    # 按年龄段分类
    age = []
    for i, j in person.groupby('AGE_TYPE').count().sort_values('AGE_TYPE').reset_index().iterrows():
        age.append(int(j['LEVEL']))
    # 占比
    ratio = []
    for i in age:
        ratio.append(round(ded_zero(i, sum(age)) * 100, 2))
    index = age.index(max(age))
    index2 = ratio.index(max(ratio))
    # 按车间分
    all_yycj = []
    # 运用车间
    yycj = person[person['NAME'].str.contains('运用车间')]
    for i in yycj['NAME'].unique().tolist():
        data = yycj[yycj['NAME'] == i]
        max_data = data.groupby('AGE_TYPE').count().sort_values('LEVEL', ascending=False).reset_index()
        dic = {
            'name': i,
            'max_name': _AGE_TYPE[max_data['AGE_TYPE'][0]],
            'count': int(max_data['LEVEL'][0])
        }
        all_yycj.append(dic)
    # 整被车间
    all_zbcj = []
    zbcj = person[person['NAME'].str.contains('整备车间')]
    for i in zbcj['NAME'].unique().tolist():
        data = zbcj[zbcj['NAME'] == i]
        max_data = data.groupby('AGE_TYPE').count().sort_values('LEVEL', ascending=False).reset_index()
        dic = {
            'name': i,
            'max_name': _AGE_TYPE[max_data['AGE_TYPE'][0]],
            'count': int(max_data['LEVEL'][0])
        }
        all_zbcj.append(dic)
    return {
        'age': age,
        'ratio': ratio,
        'index': index,
        'index2': index2,
        'all_yycj': all_yycj,
        'all_zbcj': all_zbcj
    }


_AGE_TYPE = {
    1: '20岁至30岁',
    2: '30岁至40岁',
    3: '40岁至50岁',
    4: '50岁至60岁',
}


def get_risk_type_name(pro_data):
    # 按风险大类分类
    all_number = []
    all_score = []
    name = []
    for i in pro_data['RISK_TYPE_NAME'].unique().tolist():
        risk_data = pro_data[pro_data['RISK_TYPE_NAME'] == i]
        # 按风险等级分类
        name.append(i)
        number = []
        score = []
        for j in [4, 3, 2, 1]:
            data = risk_data[risk_data['RISK_LEVEL'] == j]
            # 个数
            number.append(len(data))
            # 分数
            score.append(float(data['CHECK_SCORE'].sum()))
        all_number.append(number)
        all_score.append(score)
    # 总个数
    total_number = [sum(i) for i in all_number]
    total_score = [sum(i) for i in all_score]
    return {
        'all_number': all_number,
        'all_score': all_score,
        'name': name,
        'total_number': total_number,
        'total_score': total_score
    }


def get_third(eva_data, last_eva, year_eva, info_data, pro_data):
    """
    干部履职情况简要分析
    :param eva_data:
    :param last_eva:
    :param year_eva:
    :param info_data:
    :param pro_data:
    :return:
    """
    # 3个表格
    table1 = get_third_table(eva_data, last_eva)
    # 累计记分情况
    cum_score_sit = get_cum_score_sit(year_eva)
    # 正科干部履职
    zk = get_third_gb(eva_data, ['正科职管理人员'])
    # 副科及以下干部履职
    fk = get_third_gb(eva_data, ['副科职管理人员', '一般管理和专业技术人员'])
    # 监督质量检查
    jdzl_check = get_jdzl_check(info_data, pro_data)
    # 职能科室
    znks = get_jdzl_check_ks(info_data, pro_data, '科')
    # 运用车间
    yycj = get_jdzl_check_ks(info_data, pro_data, '运用车间')
    # 整备车间
    zbcj = get_jdzl_check_ks(info_data, pro_data, '整备车间')
    return {
        'table': table1,
        'cum_score_sit': cum_score_sit,
        'zk': zk,
        'fk': fk,
        'jdzl_check': jdzl_check,
        'znks': znks,
        'yycj': yycj,
        'zbcj': zbcj
    }


def get_third_table(eva_data, last_eva):
    # 干部履职问题类型统计表
    all_item = []
    for i in ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实',
              '音视频运用管理', '履职评价管理', '事故故障追溯', '弄虚作假']:
        data = eva_data[eva_data['ITEM_NAME'] == i]
        last_data = last_eva[last_eva['ITEM_NAME'] == i]
        # 按车间科室分类
        shops = []
        for j in data['SHOP'].unique():
            shop_data = data[data['SHOP'] == j]
            shops.append({
                'name': j,
                'count': len(shop_data)
            })
        dic = {
            'name': i,
            # 条数
            'count': len(data),
            # 占比
            'ring': round(ded_zero(len(data), len(eva_data)) * 100, 2),
            # 环比
            'ratio': calc_tongbi(len(data), len(last_data)),
            'shops': shops
        }
        all_item.append(dic)
    # 干部履职突出问题记分类型统计表
    all_situation = []
    for i in ['专业管理重点落实不到位', '日常重点安全工作落实不到位', '倒查重点问题检查质量低下', '监控调阅检查不认真', '问题整改督促不力']:
        data = eva_data[eva_data['SITUATION'].str.contains(i)]
        dic = {
            # 条数
            'count': len(data),
            # 占比
            'ring': round(ded_zero(len(data), len(eva_data)) * 100, 2)
        }
        all_situation.append(dic)
    # 履职记分集中情况
    top_three = sorted(all_item, key=lambda x: x['count'], reverse=True)[:3]
    return {
        'all_item': all_item,
        'all_situation': all_situation,
        'top_three': top_three
    }


def get_cum_score_sit(year_eva):
    # 总人员累计记分
    total_score = eva_score_calc(year_eva)
    # 干部累计记分
    gb = eva_score_calc(year_eva[year_eva['IDENTITY'] == '干部'])
    gb_person = len(year_eva[year_eva['IDENTITY'] == '干部'])
    gb_score = float(year_eva[year_eva['IDENTITY'] == '干部']['SCORE'].sum())
    return {
        'total_score': total_score['count'],
        'gb': gb,
        'gb_person': gb_person,
        'gb_score': gb_score
    }


def eva_score_calc(data):
    """
    计算个分段累计记分人数
    :param data:
    :return:
    """
    count = [0, 0, 0, 0, 0, 0, 0]
    person = []
    for i, j in data.groupby(['RESPONSIBE_ID_CARD', 'SHOP', 'RESPONSIBE_PERSON_NAME']).sum().reset_index().iterrows():
        if j['SCORE'] < 2:
            count[0] += 1
        elif j['SCORE'] < 4:
            count[1] += 1
        elif j['SCORE'] < 6:
            count[2] += 1
        elif j['SCORE'] < 8:
            count[3] += 1
            person.append({
                'name': j['RESPONSIBE_PERSON_NAME'],
                'dp': j['SHOP'],
                'score': float(j['SCORE'])
            })
        elif j['SCORE'] < 10:
            count[4] += 1
            person.append({
                'name': j['RESPONSIBE_PERSON_NAME'],
                'dp': j['SHOP'],
                'score': float(j['SCORE'])
            })
        elif j['SCORE'] < 12:
            count[5] += 1
            person.append({
                'name': j['RESPONSIBE_PERSON_NAME'],
                'dp': j['SHOP'],
                'score': float(j['SCORE'])
            })
        else:
            count[6] += 1
            person.append({
                'name': j['RESPONSIBE_PERSON_NAME'],
                'dp': j['SHOP'],
                'score': float(j['SCORE'])
            })
    return {
        'count': count,
        'person': person
            }


def get_third_gb(eva_data, list1):
    """
    干部履职分类
    :param eva_data:
    :param list1:
    :return:
    """
    data = eva_data[eva_data['GRADATION'].isin(list1)]
    # 人次
    count = len(data)
    # 按车间分
    shops = []
    for i, j in data.groupby('SHOP').count().reset_index().iterrows():
        shops.append({
            'name': j['SHOP'],
            'count': int(j['GRADATION'])
        })
    # 按记分项目分
    item = []
    for i, j in data.groupby('ITEM_NAME').count().sort_values('GRADATION', ascending=False).reset_index()[:3].iterrows():
        item.append({
            'name': j['ITEM_NAME'],
            'count': int(j['GRADATION'])
        })
    return {
        'count': count,
        'shops': shops,
        'item': item
    }


def get_jdzl_check(info_data, pro_data):
    # 下现场检查
    xxc = info_data[info_data['CHECK_WAY'].isin([1, 2])]
    # 现场
    xc = info_data[info_data['CHECK_WAY'] == 1]
    # 添乘
    tc = info_data[info_data['CHECK_WAY'] == 2]
    # 设备
    shebei = info_data[info_data['CHECK_WAY'] == 3]
    # 设备复查
    shebei_re = info_data[info_data['CHECK_WAY'] == 4]
    # 发现问题
    find_pro = len(pro_data)
    # 作业项
    work = []
    for i in ['A', 'B', 'C', 'D']:
        work.append(len(pro_data[pro_data['LEVEL'] == i]))
    # 管理。。。。
    pro_type = []
    for i in ['E', 'F', 'F', 'K']:
        data = pro_data[pro_data['LEVEL'].str.contains(i)]
        pro_type.append(len(data))
    return {
        'xxc': len(xxc),
        'xc': len(xc),
        'tc': len(tc),
        'shebei': len(shebei),
        'shebei_re': len(shebei_re),
        'find_pro': find_pro,
        'work': work,
        'pro_type': pro_type
    }


def get_jdzl_check_ks(info_data, pro_data, str1):
    info_data = info_data[info_data['SHOP'].str.contains(str1)]
    pro_data = pro_data[pro_data['SHOP'].str.contains(str1)]
    all_shops = []
    for i in info_data['SHOP'].unique():
        info = info_data[info_data['SHOP'] == i]
        pro = pro_data[pro_data['SHOP'] == i]
        # 下现场检查
        xxc = info[info['CHECK_WAY'].isin([1, 2])]
        # 现场
        xc = info[info['CHECK_WAY'] == 1]
        # 添乘
        tc = info[info['CHECK_WAY'] == 2]
        # 设备
        shebei = info[info['CHECK_WAY'] == 3]
        # 设备复查
        shebei_re = info[info['CHECK_WAY'] == 4]
        # 发现问题
        find_pro = len(pro)
        # 作业项
        work = []
        for j in ['A', 'B', 'C', 'D']:
            work.append(len(pro[pro['LEVEL'] == j]))
        # 管理。。。。
        pro_type = []
        for j in ['E', 'F', 'F', 'K']:
            data = pro[pro['LEVEL'].str.contains(j)]
            pro_type.append(len(data))
        dic = {
            'name': i,
            'xxc': len(xxc),
            'xc': len(xc),
            'tc': len(tc),
            'shebei': len(shebei),
            'shebei_re': len(shebei_re),
            'find_pro': find_pro,
            'work': work,
            'pro_type': pro_type
        }
        all_shops.append(dic)
    return {
        'all_shops': all_shops
    }


def get_four(iiilege):
    # 违章大王
    count = len(iiilege['ID_CARD'].unique())
    # 按岗位分
    jobs = []
    for i, j in iiilege.groupby('JOB').count().reset_index().iterrows():
        dic = {
            'job': j['JOB'],
            'count': int(j['ID_CARD'])
        }
        jobs.append(dic)
    # 按车间分
    shops = []
    for i, j in iiilege.groupby('NAME').count().reset_index().iterrows():
        dic = {
            'job': j['NAME'],
            'count': int(j['ID_CARD'])
        }
        shops.append(dic)
    return {
        'count': count,
        'jobs': jobs,
        'shops': shops
    }


def get_five(eva_data, re_eva):
    # 检查信息、问题逐条评价
    zt = len(eva_data[(eva_data['EVALUATE_TYPE'].isin([1, 2])) & (eva_data['EVALUATE_WAY'] == 1)])
    # 逐条复查
    zt_re = len(re_eva[re_eva['EVALUATE_WAY'] == 1])
    # 监控调阅逐条评价
    shebei_zt = len(eva_data[(eva_data['EVALUATE_WAY'] == 1) & (eva_data['CHECK_WAY'].isin([3, 4]))])
    # 监控设备调阅复查
    shebei_re = len(re_eva[re_eva['CHECK_WAY'].isin([3, 4])])
    # 定期
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2])
    # 定期复查
    dq_re = len(re_eva[re_eva['EVALUATE_WAY'] == 2])
    # 评价记分人次
    count = len(eva_data)
    return {
        'zt': zt,
        'zt_re': zt_re,
        'shebei_zt': shebei_zt,
        'shebei_re': shebei_re,
        'dq': dq,
        'dq_re': dq_re,
        'count': count
    }


def get_six(problem_data):
    # 间断瞭望
    jdlw = get_jdlw(problem_data)
    # 劳动安全隐患
    ldaq = get_ldaq(problem_data)
    # 乘务员错误操纵问题
    cwcz = get_cwcz(problem_data)
    # 防溜风险突出
    flfx = get_flfx(problem_data)
    # 机车漏检漏修问题突出
    ljlx = get_ljlx(problem_data)
    return {
        'jdlw': jdlw,
        'ldaq': ldaq,
        'cwcz': cwcz,
        'ljlx': ljlx,
        'flfx': flfx
    }


def get_jdlw(pslc_data):
    """
    普速列车间断瞭望
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    jdlw_data = pslc_data[
        (pslc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (pslc_data['PROBLEM_POINT'].str.contains('间歇盹睡'))]
    jdlw_count = len(jdlw_data)
    # 间断瞭望
    dcaq_data = jdlw_data[jdlw_data['PROBLEM_POINT'].str.contains('间断瞭望')]
    dcaq_shops = []
    for i, j in dcaq_data.groupby('SHOP').count().reset_index().iterrows():
        dic = {
            'name': j['SHOP'],
            'count': int(j['PROBLEM_POINT'])
        }
        dcaq_shops.append(dic)
    # 动车组司机间断瞭望
    dcyy = jdlw_data[jdlw_data['PROBLEM_POINT'].str.contains('间歇盹睡')]
    dcyy_shops = []
    for i, j in dcyy.groupby('SHOP').count().reset_index().iterrows():
        dic = {
            'name': j['SHOP'],
            'count': int(j['PROBLEM_POINT'])
        }
        dcyy_shops.append(dic)
    return {
        'jdlw_count': jdlw_count,
        'dcaq': len(dcaq_data),
        'dcaq_shops': dcaq_shops,
        'dcyy': len(dcyy),
        'dcyy_shops': dcyy_shops
    }


def get_ldaq(pslc_data):
    """
    普速列车劳动安全
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    pslc_data = pslc_data.dropna(subset=['KEY_ITEM'])
    ldaq_data = pslc_data[pslc_data['KEY_ITEM'].str.contains('劳动安全')]
    ldaq_stations = []
    for i in ldaq_data['SHOP'].unique():
        data = ldaq_data[ldaq_data['SHOP'] == i]
        ldaq_stations.append({
            'name': i,
            'count': len(data)
        })
    all_count = []
    for i in ['防护不到位', '反手关门', '未按规定穿戴', '其他劳安隐患']:
        data = ldaq_data[ldaq_data['PROBLEM_POINT'].str.contains(i)]
        all_count.append(len(data))
    return {
        'ldaq_count': len(ldaq_data),
        'ldaq_stations': ldaq_stations,
        'all_count': all_count
    }


def get_cwcz(pslc_data):
    """
    普速列车乘务员错误操作
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    cwcz = len(pslc_data[pslc_data['RISK_NAMES'].str.contains('乘务员')])
    cwcz_data = pslc_data[
        (pslc_data['RISK_NAMES'].str.contains('乘务员')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    # 分类
    count = []
    for i in ['操纵不当造成监控装置', '错误关闭劈相机', '错误使用车位键调整距离', '机车试验不当造成机车移动', '未进行制动机简略试验']:
        data = cwcz_data[cwcz_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'cwcz': cwcz,
        'cwcz_count': len(cwcz_data),
        'count': count
    }


def get_flfx(pslc_data):
    """
    普速列车调车安全隐患
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    count = []
    # 试验不当发生移动
    pslc_data = pslc_data.dropna(subset=['PROBLEM_POINT'])
    for i in ['试验不当发生移动', '起车不当发生移动', '防溜措施执行不到位且未发生移动', '防溜措施执行不到位发生移动']:
        data = pslc_data[pslc_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'count': count
    }


def get_ljlx(pslc_data):
    """
    普速列车漏检漏修问题
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    ljlx_data = pslc_data[
        (pslc_data['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写') | (pslc_data['RISK_NAMES'].str.contains('行车设备质量'))]
    ljlx_data = ljlx_data[(ljlx_data['SHOP'].str.contains('检修车间')) | (ljlx_data['SHOP'].str.contains('整备车间')) | (
        ljlx_data['SHOP'].str.contains('解体组装'))]
    ljlx_datas = ljlx_data[ljlx_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 按段分类
    ljlx_stations = []
    for i in ljlx_datas['SHOP'].unique():
        data = ljlx_datas[ljlx_datas['SHOP'] == i]
        ljlx_stations.append({
            'name': i,
            'count': len(data)
        })
    # 行车设备质量问题
    xcsb_data = ljlx_data[ljlx_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])]
    # 按段分
    xcsb_stations = []
    for i in xcsb_data['SHOP'].unique():
        data = xcsb_data[xcsb_data['SHOP'] == i]
        xcsb_stations.append({
            'name': i,
            'count': len(data)
        })
    return {
        'ljlx_count': len(ljlx_data),
        'ljlx_stations': ljlx_stations,
        'xcsb_count': len(xcsb_data),
        'xcsb_stations': xcsb_stations
    }