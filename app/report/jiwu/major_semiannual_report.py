
from app import mongo
from app.report.jiwu.major_semiannual_report_data import get_data
from app.report.analysis_report_manager import SemiannualAnalysisReport


class JiwuMajorSemiannualAnalysisReport(SemiannualAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self):
        super(JiwuMajorSemiannualAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='机务',)

    def generate_report_data(self, year, half):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param half: int 月
        :return:
        """
        data = get_data(year, half)
        mongo.db['safety_analysis_semiannual_report'].delete_one(
            {
                "year": year,
                "half": half,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_semiannual_report'].insert_one(data)
        return data