
from app import mongo
from app.report.jiwu.station_monthly_report_data import get_data
import app.report.analysis_report_manager as manager


class JiwuStationMonthlyAnalysisReport(manager.MonthlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(JiwuStationMonthlyAnalysisReport, self).__init__(hierarchy_type='STATION', major='机务',
                                                               station_id=station_id)

    def generate_report_data(self, year, month):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param month: int 月
        :return:
        """
        data = get_data(year, month, self.station_id)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "month": month,
                "hierarchy": "STATION",
                "major": self.major,
                "station_id": self.station_id
            })
        mongo.db['safety_analysis_monthly_report'].insert_one(data)
        return data
