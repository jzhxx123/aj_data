from app.data.util import pd_query
from app.report.jiwu.major_semiannual_report_sql import *
import datetime
from app.report.analysis_report_manager import SemiannualAnalysisReport


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, half):
    date = SemiannualAnalysisReport.get_semiannual_intervals(year, half)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 5
    top = top_safety_produce_analysis(start_date, end_date)
    first = get_traffic_liability_accidents(start_date, end_date)
    second = get_evaluate_situation_base_doc(start_date, end_date, last_month_start, last_month_end)
    third = get_check_quilt(start_date, end_date, last_month_start, last_month_end)
    seventh = get_safety_tip(start_date, end_date)
    eighth = get_risk_problem_analysis(start_date, end_date, last_month_start, last_month_end)
    file_name = f'{start_date}至{end_date}机务系统安全管理半年度分析.docx'
    result = {
        "year": year,
        'half': half,
        'times': '上半年' if half == 1 else '下半年',
        "start_month": start_month,
        "end_month": end_month,
        "major": '机务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'top': top,
        "first": first,
        "second": second,
        'third': third,
        'seventh': seventh,
        'eighth': eighth,
    }
    return result


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def station_cft(data, str2, num):
    """
    分类站段
    :param data: 所有站段数据
    :param str1: 站段列名
    :param str2: 排序列明
    :param num: 1.求count 其他.求sum
    :return: 返回分类后的data
    """
    if num == 1:
        new_data = data.groupby('NAME').count().sort_values(by=str2, ascending=False).reset_index()
    else:
        new_data = data.groupby('NAME').sum().sort_values(by=str2, ascending=False).reset_index()
    station = {}
    for i, k in new_data.iterrows():
        station[k['NAME']] = int(k[str2])
    return station


def top_safety_produce_analysis(start_data, end_data):
    """
    顶部安全分析信息
    :param start_data:
    :param end_data:
    :return:
    """
    data = pd_query(CHECK_SAFETY_PRODUCE_SQL.format(start_data, end_data))
    accident_num = len(data[(data['MAIN_TYPE'] == 1)])
    return {
        'accident_num': accident_num
    }


# ---------------------------------------------------------------------第一部分-----------------------------------------------------------------------------
def get_traffic_liability_accidents(start_data, end_data):
    """
    机务系统责任交通事故统计
    :param start_date: 开始时间
    :param end_date: 结束时间
    :return:
    """
    data = pd_query(CHECK_SAFETY_PRODUCE_SQL.format(start_data, end_data))
    accident_num = len(data[(data['MAIN_TYPE'] == 1) & (data['DETAIL_TYPE'] == 1)])
    problem_list = [i for i in data['DESCRIPTION']]
    return {
        'accident_num': accident_num,
        "problem_list": problem_list,
    }


# ---------------------------------------------------------------------第二部分-----------------------------------------------------------------------------
def get_evaluate_situation_base_doc(start_date, end_date, last_start, last_end):
    """
    干部履职评价情况
    :param start_date:
    :param end_date:
    :return:
    """
    evaluate_data = pd_query(CHECK_EVALUATE_BASE_SQL.format(start_date, end_date))
    last_evaluate_data = pd_query(CHECK_EVALUATE_BASE_SQL.format(last_start, last_end))
    score_product_data = pd_query(CHECK_EVALUATE_SCORE_PRODUCT_SQL.format(start_date, end_date))
    # 评价情况分析
    evaluate_score = _get_evaluate_score(evaluate_data, last_evaluate_data, score_product_data)
    return {
        'evaluate_score': evaluate_score,
    }


def _get_evaluate_score(evaluate_data, last_evaluate_data, score_product_data):
    """
    评价情况分析
    :param evaluate_data:
    :return:
    """
    # 评价、复查基本情况
    base_situation = _get_base_situation(evaluate_data)
    # 上月复查基本情况
    last_base_situation = _get_base_situation(last_evaluate_data)
    # 与上月占比比较
    ratio1 = round(base_situation['zt_ratio'] - last_base_situation['zt_ratio'], 1)
    ratio2 = round(base_situation['review_ratio'] - last_base_situation['review_ratio'], 1)
    ratio3 = round(base_situation['dq_ratio'] - last_base_situation['dq_ratio'], 1)
    # 记分情况
    score_situation = get_score_data(score_product_data)
    # 累计计分情况
    add_score_situation = get_add_score_situation(score_product_data)
    # 正科职干部履职评价分析
    zk_evaluate = _get_zk_evaluate(score_product_data)
    # 副科职及以下干部履职评价分析
    fk_evaluate = _get_fk_evaluate(score_product_data)
    return {
        'base_situation': base_situation,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'score_situation': score_situation,
        'add_score_situation': add_score_situation,
        'zk_evaluate': zk_evaluate,
        'fk_evaluate': fk_evaluate,
    }


def _get_fk_evaluate(score_product_data):
    """
    副科职及以下干部履职评价分析
    :param score_product_data:
    :return:
    """
    fk_person = score_product_data[score_product_data['GRADATION'] != '正科职管理人员']
    number = len(fk_person)
    if fk_person.empty:
        station_name = 0
        station_number = 0
    else:
        fz_gp = fk_person.groupby('NAME').count().reset_index()
        station_name = fz_gp['NAME'].tolist()
        station_number = fz_gp['ITEM_NAME'].tolist()
    main_problem = fk_person.groupby('ITEM_NAME').count().sort_values(by='SCORE', ascending=False).reset_index()
    list1 = []
    list2 = []
    for i, k in main_problem.iterrows():
        list1.append(k['ITEM_NAME'])
        list2.append(int(k['SCORE']))

    return {
        'number': number,
        'station_name': station_name,
        'station_number': station_number,
        'list1': list1[:4],
        'list2': list2[:4],
    }


def _get_zk_evaluate(score_product_data):
    """
    正科职干部履职评价分析
    :param score_product_data:
    :return:
    """
    zk_person = score_product_data[score_product_data['GRADATION'] == '正科职管理人员']
    number = len(zk_person)
    if zk_person.empty:
        station_name = 0
        station_number = 0
    else:
        fz_gp = zk_person.groupby('NAME').count().sort_values(by='SCORE', ascending=False).reset_index()
        station_name = fz_gp['NAME'].tolist()
        station_number = fz_gp['NAME'].tolist()
    main_problem = zk_person.groupby('ITEM_NAME').count().sort_values(by='SCORE', ascending=False).reset_index()
    list1 = []
    list2 = []
    for i, k in main_problem.iterrows():
        list1.append(k['ITEM_NAME'])
        list2.append(int(k['SCORE']))
    return {
        'number': number,
        'station_name': station_name,
        'station_number': station_number,
        'list1': list1[:3],
        'list2': list2[:3],
    }


def get_add_score_situation(score_product_data):
    """
    累计计分情况
    :param score_product_data:
    :return:
    """
    all_person_count = len(score_product_data)
    all_score = int(score_product_data['SCORE'].sum())
    all_person_data = score_product_data.groupby('RESPONSIBE_PERSON_NAME').sum().reset_index()
    all_person = len(all_person_data)
    number_up_8 = len(all_person_data[all_person_data['SCORE'] >= 8])
    number_up_6_8 = len(all_person_data[(all_person_data['SCORE'] >= 6) & (all_person_data['SCORE'] < 8)])
    number_up_5_6 = len(all_person_data[(all_person_data['SCORE'] >= 5) & (all_person_data['SCORE'] < 6)])
    number_up_4_5 = len(all_person_data[(all_person_data['SCORE'] >= 4) & (all_person_data['SCORE'] < 5)])
    number_up_3_4 = len(all_person_data[(all_person_data['SCORE'] >= 3) & (all_person_data['SCORE'] < 4)])
    number_up_2_3 = len(all_person_data[(all_person_data['SCORE'] >= 2) & (all_person_data['SCORE'] < 3)])
    up_6_data = all_person_data[all_person_data['SCORE'] >= 6].reset_index(drop=True)
    station = []
    score = []
    name = []
    for i, k in up_6_data.iterrows():
        score.append(k['SCORE'])
        name.append(k['RESPONSIBE_PERSON_NAME'])
        n = score_product_data[score_product_data['RESPONSIBE_PERSON_NAME'] == k['RESPONSIBE_PERSON_NAME']]
        n1 = n['ALL_NAME'].tolist()[0]
        station.append(n1)
    return {
        'all_person_count': all_person_count,
        'all_score': all_score,
        'all_person': all_person,
        'number_up_8': number_up_8,
        'number_up_6_8': number_up_6_8,
        'number_up_5_6': number_up_5_6,
        'number_up_4_5': number_up_4_5,
        'number_up_3_4': number_up_3_4,
        'number_up_2_3': number_up_2_3,
        'station': station,
        'name': name,
        'score': score
    }


def get_score_data(score_product_data):
    """
    记分情况
    :param score_product_data:
    :return:
    """
    # 干部履职评价记分人数
    person_number = len(score_product_data)
    # 实际被评价干部人数
    actual_person = len(score_product_data[score_product_data['IS_EVALUATE'] == 1])
    # 合计评价记分
    score_count = int(score_product_data['SCORE'].sum())
    # 单月记分3分及以上
    month_up_3 = len(score_product_data[score_product_data['SCORE'] >= 3])
    # 单月记分2分及以上
    month_up_2 = len(score_product_data[score_product_data['SCORE'] >= 2])
    # 记分项目及类型简要分析
    score_project_type = get_score_project_type(score_product_data)
    # 职务分布
    duty_dbt = get_duty_dbt(score_product_data)
    # 单位分布
    unit_dbt = get_unit_dbt(score_product_data)
    # 检查类型
    check_type = get_check_type(score_product_data)
    # 评价类型分布
    evaluate_type_dbt = get_evaluate_type_dbt(score_product_data)
    return {
        'person_number': person_number,
        'actual_person': actual_person,
        'score_count': score_count,
        'month_up_3': month_up_3,
        'month_up_2': month_up_2,
        'score_project_type': score_project_type,
        'duty_dbt': duty_dbt,
        'unit_dbt': unit_dbt,
        'check_type': check_type,
        'evaluate_type_dbt': evaluate_type_dbt
    }


def get_check_type(score_product_data):
    """
    检查类型
    :param score_product_data:
    :return:
    """
    station_name = []
    station_person_number = []
    luju = len(score_product_data[score_product_data['CHECK_TYPE'] == 1])
    zhanduan_data = score_product_data[score_product_data['CHECK_TYPE'] == 2]
    zhanduan = len(zhanduan_data)
    zd_data = zhanduan_data.groupby('NAME').count().reset_index()
    for i, k in zd_data.iterrows():
        station_name.append(k['NAME'])
        station_person_number.append(int(k['ALL_NAME']))
    return {
        'station_name': station_name,
        'station_person_number': station_person_number,
        'luju': luju,
        'zhanduan': zhanduan,
    }


def get_evaluate_type_dbt(score_product_data):
    """
    评价类型分布
    :param score_product_data:
    :return:
    """
    problem_list = ['考核责任落实', '量化指标完成', '检查信息录入', '重点工作落实', '问题闭环管理', '监督检查质量', '事故故障追溯']
    zt = len(score_product_data[score_product_data['EVALUATE_WAY'] == 1])
    jd = len(score_product_data[score_product_data['EVALUATE_WAY'] == 3])
    dq = len(score_product_data[score_product_data['EVALUATE_WAY'] == 2])
    cd_data = score_product_data[score_product_data['NAME'] == '成都机务段']
    gy_data = score_product_data[score_product_data['NAME'] == '贵阳机务段']
    cq_data = score_product_data[score_product_data['NAME'] == '重庆机务段']
    xc_data = score_product_data[score_product_data['NAME'] == '西昌机务段']
    cd = calc_dep_evaluate_problem(cd_data, problem_list)
    gy = calc_dep_evaluate_problem(gy_data, problem_list)
    cq = calc_dep_evaluate_problem(cq_data, problem_list)
    xc = calc_dep_evaluate_problem(xc_data, problem_list)
    return {
        'zt': zt,
        'jd': jd,
        'dq': dq,
        'cd': cd,
        'gy': gy,
        'cq': cq,
        'xc': xc,
    }


def calc_dep_evaluate_problem(data, problem_list):
    number = []
    for i in problem_list:
        number.append(len(data[data['ITEM_NAME'] == i]))
    return {'number': number}


def get_duty_dbt(score_product_data):
    """
    职务分布
    :param score_product_data:
    :return:
    """
    zf_chu = len(score_product_data[score_product_data['LEVEL'].isin(['正处级', '副处级'])])
    zk = len(score_product_data[score_product_data['LEVEL'] == '正科级'])
    fk = len(score_product_data[score_product_data['LEVEL'] == '副科级'])
    # 一般管理和专业技术人员
    qt = len(score_product_data[score_product_data['GRADATION'] == '一般管理和专业技术人员'])
    return {
        'zf_chu': zf_chu,
        'zk': zk,
        'fk': fk,
        'qt': qt
    }


def get_unit_dbt(score_product_data):
    """
    单位分布
    :param score_product_data:
    :return:
    """
    unit = {}
    unit_data = score_product_data.groupby('NAME').count().reset_index()
    for i, k in unit_data.iterrows():
        unit[k['NAME']] = int(k['LEVEL'])
    return unit


def get_score_project_type(score_product_data):
    """
    记分项目及类型简要分析
    :param score_product_data:
    :return:
    """
    # 机务干部履职评价记分项目统计表
    score_project_table = get_score_project_table(score_product_data)
    # 机务干部履职突出问题记分类型统计表
    issue_problem_table = get_issue_problem_table(score_product_data)
    # 干部履职评价记分
    evaluate_type = get_evaluate_type(score_product_data, score_project_table)
    return {
        'score_project_table': score_project_table,
        'issue_problem_table': issue_problem_table,
        'evaluate_type': evaluate_type
    }


def get_evaluate_type(score_product_data, score_project_table):
    """
    干部履职评价记分
    :param score_product_data:
    :param score_project_table:
    :return:
    """
    name = score_project_table['name']
    number = score_project_table['number']
    ratio = score_project_table['ratio']
    station_info = []
    for i in name:
        score_data = score_product_data[score_product_data['ITEM_NAME'] == i].groupby('NAME').count().reset_index()
        station_info.append(calc_station_person_count(score_data))
    return {
        'name': name,
        'number': number,
        'ratio': ratio,
        'station_info': station_info
    }


def calc_station_person_count(score_data):
    """
    计算各个计分项目的各个站段人数
    :param score_data:
    :return:
    """
    name = []
    number = []
    for i, k in score_data.iterrows():
        name.append(k['NAME'])
        number.append(int(k['ALL_NAME']))
    return {
        'name': name,
        'number': number
    }


def get_score_project_table(score_product_data):
    """
    机务干部履职评价记分项目统计表
    :param score_product_data:
    :return:
    """
    # 总人数
    all_person = len(score_product_data)
    data = score_product_data.groupby('ITEM_NAME').count().sort_values(by='ALL_NAME').reset_index()
    name = []
    number = []
    ratio = []
    for i, k in data.iterrows():
        name.append(k['ITEM_NAME'])
        number.append(int(k['ALL_NAME']))
        ratio.append(round(int(k['ALL_NAME']) / all_person * 100, 1))
    return {
        'name': name[:7],
        'number': number[:7],
        'ratio': ratio[:7]
    }


def get_issue_problem_table(score_product_data):
    """
    机务干部履职突出问题记分类型统计表
    :param score_product_data:
    :return:
    """
    all_person = len(score_product_data)
    data = score_product_data.groupby('SITUATION').count().sort_values(by='ALL_NAME').reset_index()
    name = []
    number = []
    ratio = []
    for i, k in data.iterrows():
        name.append(k['SITUATION'])
        number.append(int(k['ALL_NAME']))
        ratio.append(round(int(k['ALL_NAME']) / all_person * 100, 1))
    return {
        'name': name[:3],
        'number': number[:3],
        'ratio': ratio[:3]
    }


def _get_base_situation(evaluate_data):
    """
    评价、复查基本情况
    :param evaluate_data:
    :return:
    """
    # 总检查数
    info_count = len(evaluate_data)
    # 总问题数
    problem_count = int(evaluate_data['PROBLEM_NUMBER'].sum())
    # 逐条检查总数
    all_zt = len(evaluate_data[evaluate_data['EVALUATE_WAY'] == 1])
    # 系统总信息数 = 检查信息+发现问题
    sys_count = info_count + problem_count
    # 占比
    if sys_count == 0:
        zt_ratio = all_zt * 100
    else:
        zt_ratio = round(all_zt / sys_count * 100, 1)
    # 局机务分析室逐条评价
    ju_zt = len(evaluate_data[(evaluate_data['EVALUATE_WAY'] == 1) & (evaluate_data['CHECK_TYPE'] == 1)])
    # 段分析中心逐条评价
    duan_zt = len(evaluate_data[(evaluate_data['EVALUATE_WAY'] == 1) & (evaluate_data['CHECK_TYPE'] == 2)])
    # 逐条评价复查
    review_zt = len(evaluate_data[(evaluate_data['EVALUATE_WAY'] == 1) & (evaluate_data['IS_REVIEW'] == 1)])
    # 占比
    if all_zt == 0:
        review_ratio = review_zt * 100
    else:
        review_ratio = round(review_zt / all_zt * 100, 1)
    # 定期评价
    all_dq = len(evaluate_data[evaluate_data['EVALUATE_WAY'] == 2])
    # 定期评价复查
    review_dq = len(evaluate_data[(evaluate_data['EVALUATE_WAY'] == 2) & (evaluate_data['IS_REVIEW'] == 1)])
    # 占比
    if all_dq == 0:
        dq_ratio = review_dq * 100
    else:
        dq_ratio = round(review_dq / all_dq * 100, 1)
    return {
        'info_count': info_count,
        'problem_count': problem_count,
        'all_zt': all_zt,
        'sys_count': sys_count,
        'zt_ratio': zt_ratio,
        'ju_zt': ju_zt,
        'duan_zt': duan_zt,
        'review_zt': review_zt,
        'review_ratio': review_ratio,
        'all_dq': all_dq,
        'review_dq': review_dq,
        'dq_ratio': dq_ratio
    }


# ---------------------------------------------------------------------第三部分-----------------------------------------------------------------------------
def get_check_quilt(start_date, end_date, last_month_start, last_month_end):
    """
    监督检查质量情况分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :return:
    """
    check_data = pd_query(CHECK_QUILT_SQL.format(start_date, end_date))
    check_data['NEW_NAME'] = check_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    last_data = pd_query(CHECK_QUILT_SQL.format(last_month_start, last_month_end))
    last_data['NEW_NAME'] = last_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    # 总体情况
    overall = get_overall(check_data, last_data)
    # 机务处检查情况
    jiwuchu = get_jiwuchu(check_data)
    return {
        'overall': overall,
        'jiwuchu': jiwuchu,
    }


def get_jiwuchu(data):
    """
    机务处检查情况
    :param data:
    :return:
    """
    # 机务处现场检查
    xc = len(data[data['CHECK_WAY'].isin([1, 2])])
    # 各点检查
    gd = len(data[data['CHECK_WAY'] == 1])
    # 添乘检查
    tc = len(data[data['CHECK_WAY'] == 2])
    # 设备监控调阅
    shebei = len(data[data['CHECK_WAY'] == 3])
    # 发现问题
    problem_num = int(data['PROBLEM_NUMBER'].sum())
    # 作业项
    zuoye_data = data[data['PROBLEM_CLASSITY_NAME'] == '生产作业']
    zuoye_point = len(zuoye_data)
    zuoye_type = calc_zuoye_type(zuoye_data)
    # 设备质量
    shebei_point = len(data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')])
    # 管理项
    manager_point = len(data[data['PROBLEM_CLASSITY_NAME'] == '安全管理'])
    return {
        'xc': xc,
        'gd': gd,
        'tc': tc,
        'shebei': shebei,
        'problem_num': problem_num,
        'zuoye_point': zuoye_point,
        'zuoye_type': zuoye_type,
        'shebei_point': shebei_point,
        'manager_point': manager_point
    }


def calc_zuoye_type(zuoye_data):
    """
    A,B,C,D类问题
    :param zuoye_data:
    :return:
    """
    a = len(zuoye_data[zuoye_data['LEVEL'] == 'A'])
    b = len(zuoye_data[zuoye_data['LEVEL'] == 'B'])
    c = len(zuoye_data[zuoye_data['LEVEL'] == 'C'])
    d = len(zuoye_data[zuoye_data['LEVEL'] == 'D'])
    return {
        'a': a,
        'b': b,
        'c': c,
        'd': d
    }


def get_overall(data, last_data):
    """
    总体情况
    :param data:
    :param last_data:
    :return:
    """
    # 检查次数
    all_check_count = len(data)
    last_check_count = len(last_data)
    # 问题总数
    find_problem = int(data['PROBLEM_NUMBER'].sum())
    last_find_problem = int(last_data['PROBLEM_NUMBER'].sum())
    # 环比
    check_ratio = calculate_month_and_ring_ratio([all_check_count, last_check_count])
    find_ratio = calculate_month_and_ring_ratio([find_problem, last_find_problem])
    # 问题项
    problem_list = ['生产作业', '安全管理', '反恐防暴', '外部环境']
    new_data = data.groupby('PROBLEM_CLASSITY_NAME').sum().sort_values(by='PROBLEM_NUMBER').reset_index()
    problem_point = get_problem_point(data, new_data, problem_list)
    # 设备设施问题
    shebei_data = data[data['PROBLEM_CLASSITY_NAME'].str.contains('设备')]
    shebei_num = int(shebei_data['PROBLEM_NUMBER'].sum())
    shebei_ratio = round(shebei_num / find_problem * 100, 1)
    # 性质严重问题
    main_problem = data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    main_count = len(main_problem)
    main_list = []
    for i in ['作业', '设备', '管理']:
        new_data = main_problem[main_problem['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        dic = {
            'count': len(new_data),
            'ratio': round(ded_zero(len(new_data), main_count) * 100, 1)
        }
        main_list.append(dic)
    # 红线问题
    red_name = []
    station_num = []
    red_data = data[data['IS_RED_LINE'].isin([1, 2])]
    red_num = int(red_data['PROBLEM_NUMBER'].sum())
    fenzu = red_data.groupby('NEW_NAME').sum().sort_values(by='PROBLEM_NUMBER', ascending=False).reset_index()
    for i, k in fenzu.iterrows():
        red_name.append(k['NEW_NAME'])
        station_num.append(int(k['PROBLEM_NUMBER']))
    return {
        'all_check_count': all_check_count,
        'find_problem': find_problem,
        'check_ratio': check_ratio,
        'find_ratio': find_ratio,
        'problem_point': problem_point,
        'shebei_num': shebei_num,
        'shebei_ratio': shebei_ratio,
        'red_num': red_num,
        'main_count': main_count,
        'main_list': main_list,
        'red_name': red_name,
        'station_num': station_num
    }


def get_problem_point(data, new_data, problem_list):
    """
    问题项
    :param data:
    :param problem_list:
    :return:
    """
    all_count = int(data['PROBLEM_NUMBER'].sum())
    num = []
    ratio = []
    for i in range(len(problem_list)):
        num.append(int(new_data[new_data['PROBLEM_CLASSITY_NAME'] == problem_list[i]]['PROBLEM_NUMBER'].sum()))
        ratio.append(round(num[i] / all_count * 100, 1))
    return {
        'num': num,
        'ratio': ratio
    }


# ---------------------------------------------------------------------第四部分-----------------------------------------------------------------------------


# ---------------------------------------------------------------------第五部分-----------------------------------------------------------------------------


# ---------------------------------------------------------------------第六部分-----------------------------------------------------------------------------


# ---------------------------------------------------------------------第七部分-----------------------------------------------------------------------------
def get_safety_tip(start_date, end_date):
    """
    “违章大王”情况分析
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    # 违章人员data
    iiilege = pd_query(CHECK_IIIEGE_SQL.format(start_date, end_date))
    # 违章大王人数
    iiilege = iiilege[iiilege['DEDUCT_SCORE'] >= 12]
    ilege_number = len(iiilege)
    # 按职务分类
    job = iiilege.groupby('JOB').count().reset_index()
    job_name = []
    job_person = []
    for i, k in job.iterrows():
        job_name.append(k['JOB'])
        job_person.append(int(k['DEDUCT_SCORE']))
    # 按站段分类
    new_station = iiilege.groupby('NAME').count().reset_index()
    zhanduan_list = []
    person_list = []
    for i, k in new_station.iterrows():
        zhanduan_list.append(k['NAME'])
        person_list.append(int(k['DEDUCT_SCORE']))
    return {
        'ilege_number': ilege_number,
        'zhanduan_list': zhanduan_list,
        'person_list': person_list,
        'job_name': job_name,
        'job_person': job_person
    }


# ---------------------------------------------------------------------第八部分-----------------------------------------
def get_risk_problem_analysis(start_date, end_date, last_month_start, last_month_end):
    data = pd_query(CHECK_RISK_PROBLEM_SQL.format(start_date, end_date))
    last_data = pd_query(CHECK_RISK_PROBLEM_SQL.format(last_month_start, last_month_end))
    # 列出一般、较大、严重三个风险等级问题环比增加的风险。
    # 列出问题数排前五位的项点。
    sort_problem_point = get_sort_problem_point(data)
    # 间断瞭望风险仍然突出
    risk_jdlw = get_risk_jdlw(data, last_data)
    # 劳动安全风险突出
    risk_laoan = get_risk_laoan(data, last_data)
    # 乘务员操作风险
    risk_cwy = get_risk_cwy(data)
    # 防溜风险突出
    risk_fangliu = get_risk_fangliu(data)
    # 机车质量风险突出
    risk_jiche = get_risk_jiche(data, last_data)
    return {
        'sort_problem_point': sort_problem_point,
        'risk_jdlw': risk_jdlw,
        'risk_laoan': risk_laoan,
        'risk_cwy': risk_cwy,
        'risk_fangliu': risk_fangliu,
        'risk_jiche': risk_jiche,
    }


def get_risk_jiche(data, last_data):
    """
    机车质量风险突出
    :param data:
    :return:
    """
    # 机车漏检漏修问题
    data = data[data['RISK_NAMES'].notnull()]
    last_data = last_data[last_data['RISK_NAMES'].notnull()]
    missing_fix_data = data[data['RISK_NAMES'].str.contains('漏修、漏探、漏试、漏分析、漏填写')]
    last_missing_data = last_data[last_data['RISK_NAMES'].str.contains('漏修、漏探、漏试、漏分析、漏填写')]
    # 总数
    count1 = len(missing_fix_data[(missing_fix_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('检修车间')) |
                                  (missing_fix_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('整备车间'))])
    last_count1 = len(last_missing_data[(last_missing_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('检修车间')) |
                                        (last_missing_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('整备车间'))])
    station1 = station_cft(missing_fix_data, 'RISK_NAMES', 1)
    ratio1 = calculate_month_and_ring_ratio([count1, last_count1])
    # 机车质量问题
    jiche_quality_data = data[data['RISK_NAMES'].str.contains('行车设备质量')]
    last_jiche_data = last_data[last_data['RISK_NAMES'].str.contains('行车设备质量')]
    # 总数
    count2 = len(jiche_quality_data[(jiche_quality_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('检修车间')) |
                                    (jiche_quality_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('整备车间'))])
    last_count2 = len(last_jiche_data[(last_jiche_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('检修车间')) |
                                      (last_jiche_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('整备车间'))])
    station2 = station_cft(missing_fix_data, 'RISK_NAMES', 1)
    ratio2 = calculate_month_and_ring_ratio([count2, last_count2])
    return {
        'count1': count1,
        'station1': station1,
        'ratio1': ratio1,
        'count2': count2,
        'station2': station2,
        'ratio2': ratio2
    }


def get_risk_fangliu(data):
    """
    防溜风险突出
    :param data:
    :return:
    """
    problem_list = ['机车试验不当造成机车移动', '起车不当，造成列车向后移动', '防溜措施执行不到位发生移动造成其他后果',
                    '防溜措施执行不到位且未发生移动']
    d_problem = {}
    d_problem['d1'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[0])])
    d_problem['d2'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[1])])
    d_problem['d3'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[2])])
    d_problem['d4'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[3])])
    return {
        'd_problem': d_problem
    }


def get_risk_cwy(data):
    """
    乘务员操作风险
    :param data:
    :return:
    """
    data = data[data['RISK_NAMES'].notnull()]
    new_data = data[~data['PROBLEM_POINT'].str.contains('动车运用车间')]
    # 操纵不当造成监控装置常用制动或紧急制动动作
    caozuo_error = len(new_data[new_data['PROBLEM_POINT'].str.contains('操纵不当造成监控装置常用制动或紧急制动动作')])
    # 过分相操作违规
    guofen_error = len(new_data[new_data['PROBLEM_POINT'].str.contains('电力机车（含动车组）过分相未达速')])
    # 调速手柄未回零
    tiaosu_error = len((new_data[new_data['PROBLEM_POINT'].str.contains('调速手柄未回零(过分相时，且未断开主断路器)')]))
    # 制动机使用违规
    problem_list = ['货物列车低速缓解', '运行中充风不足加载、再制动。', '排风不止追加减压', '紧急制动车未停稳，移动自阀、单阀手柄。',
                    '列车实施紧急制动时，缓解机车单阀（或机车小闸在缓解位）。', '升弓未确认升弓状态。',
                    '未确认降弓状态。', '升弓前未确认总风压力、未呼唤、未确认各手柄位置等。']
    # 典型问题
    d_problem = {}
    d_problem['d1'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[0])])
    d_problem['d2'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[1])])
    d_problem['d3'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[2])])
    d_problem['d4'] = len(data[(data['PROBLEM_POINT'].str.contains(problem_list[3])) | (
        len(data[data['PROBLEM_POINT'].str.contains(problem_list[4])]))])
    d_problem['d5'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[5])])
    d_problem['d6'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[6])])
    d_problem['d7'] = len(data[data['PROBLEM_POINT'].str.contains(problem_list[7])])
    return {
        'caozuo_error': caozuo_error,
        'guofen_error': guofen_error,
        'tiaosu_error': tiaosu_error,
        'd_problem': d_problem
    }


def get_risk_laoan(data, last_data):
    """
    劳动安全风险突出
    :param data:
    :param last_data:
    :return:
    """
    data = data[data['RISK_NAMES'].notnull()]
    last_data = last_data[last_data['RISK_NAMES'].notnull()]
    ldaq = data[data['RISK_NAMES'].str.contains('劳动安全')]
    last_ldaq = last_data[last_data['RISK_NAMES'].str.contains('劳动安全')]
    count = len(ldaq)
    last_count = len(last_ldaq)
    # 环比
    ratio = calculate_month_and_ring_ratio([count, last_count])
    # 按段分
    station = station_cft(ldaq, 'RISK_NAMES', 1)
    # 防护不到位
    return {
        'count': count,
        'ratio': ratio,
        'station': station,
    }


def get_risk_jdlw(data, last_data):
    """
    间断瞭望风险仍然突出
    :param data:
    :param last_data:
    :return:
    """
    data = data[(data['PROBLEM_POINT'].str.contains('运行中间断瞭望')) | (data['PROBLEM_POINT'].str.contains('运行中间歇打盹'))]
    last_data = last_data[
        (last_data['PROBLEM_POINT'].str.contains('运行中间断瞭望')) | (last_data['PROBLEM_POINT'].str.contains('运行中间歇打盹'))]
    # 总问题数
    count = len(data)
    last_count = len(last_data)
    # 占比
    ratio = calculate_month_and_ring_ratio([count, last_count])
    # 按站段分类
    station_data = station_cft(data, 'RISK_NAMES', 1)
    # 调车作业
    diaoche_work = get_diaoche_work(data, last_data)
    # 动车组司机间断瞭望
    dongche = get_dongche(data, last_data)
    # 单司机值乘或双人值乘同时间断瞭望
    one_or_more = get_one_or_more(data)
    # 部分运用车间查处间断瞭望力度不够
    chejian = get_chejian(data)
    return {
        'count': count,
        'ratio': ratio,
        'station_data': station_data,
        'diaoche_work': diaoche_work,
        'dongche': dongche,
        'one_or_more': one_or_more,
        'chejian': chejian,
    }


def get_one_or_more(data):
    """
    单司机值乘或双人值乘同时间断瞭望
    :param data:
    :return:
    """
    data = data[data['RISK_NAMES'].str.contains('间断瞭望')]
    all_count = len(data)
    dan_count = len(data[data['PROBLEM_POINT'].str.contains('单司机值乘')])
    duo_count = len(data[data['PROBLEM_POINT'].str.contains('双人值乘')])
    ratio = round((dan_count + duo_count) / all_count * 100, 1)
    return {
        'all_count': all_count,
        'dan_count': dan_count,
        'duo_count': duo_count,
        'ratio': ratio
    }


def get_chejian(data):
    """
    部分运用车间查处间断瞭望力度不够
    :param data:
    :return:
    """
    new_data = data.groupby('NAME').count().sort_values(by='RISK_NAMES', ascending=True).reset_index()
    station = {}
    for i in range(3):
        station['{0}'.format(i)] = new_data.loc[i]['NAME']
    return station


def get_dongche(data, last_data):
    """
    动车组司机间断瞭望
    :param data:
    :return:
    """
    data = data[data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('动车运用车间')]
    last_data = last_data[last_data['RESPONSIBILITY_DEPARTMENT_NAME'].str.contains('动车运用车间')]
    # 发现数量
    find_count = len(data)
    last_find = len(last_data)
    # 环比
    ratio = calculate_month_and_ring_ratio([find_count, last_find])
    # 分站段
    station = station_cft(data, 'RISK_NAMES', 1)
    return {
        'find_count': find_count,
        'ratio': ratio,
        'station': station
    }


def get_diaoche_work(data, last_data):
    """
    调车作业
    :param data:
    :param last_data:
    :return:
    """
    new_data = data[(data['PROBLEM_POINT'].str.contains('运行中间断瞭望')) & (data['PROBLEM_POINT'].str.contains('运行中间歇打盹'))]
    last_new_data = last_data[
        (last_data['PROBLEM_POINT'].str.contains('运行中间断瞭望')) & (last_data['PROBLEM_POINT'].str.contains('运行中间歇打盹'))]
    count = len(new_data)
    ring = calculate_month_and_ring_ratio([count, len(last_new_data)])
    all_list = []
    for i in data['NAME'].value_counts().index:
        n_data = new_data[new_data['NAME'] == i]
        last_n_data = last_new_data[last_new_data['NAME'] == i]
        dic = {
            'name': i,
            'count': len(n_data),
            'ring': calculate_month_and_ring_ratio([len(n_data), len(last_n_data)])['month_percent']
        }
        all_list.append(dic)
    l = sorted(all_list, key=lambda x: x['ring'])
    return {
        'count': count,
        'ring': ring,
        'all_list': all_list,
        'l': l
    }


def get_sort_problem_point(data):
    """
    列出问题数排前五位的项点
    :param data:
    :return:
    """
    problem_data = data.groupby('PROBLEM_POINT').count().sort_values(by='ALL_NAME', ascending=False).reset_index()
    problem_list = []
    for i, k in problem_data.iterrows():
        problem_list.append(k['PROBLEM_POINT'])
    return {
        'problem_list': problem_list[:5]
    }
