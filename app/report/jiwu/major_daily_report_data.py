import numpy as np
from datetime import datetime as dt
from app.report.analysis_report_manager import DailyAnalysisReport
from app.data.util import pd_query
from app.report.common_sql_two import *

code = {
    0: 'B',
    1: 'C',
    2: 'D',
    3: 'E',
    4: 'F'
}

MAJOR = "1ACE7D1C80B14456E0539106C00A2E70KSC"


def get_data(year, month, day):
    report_date = dt(year, month, day)
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    times = (params[0][0], params[0][1])
    # 检查信息
    info_data = pd_query(check_info_sql.format(times[0], times[1], MAJOR))
    # 检查问题信息
    problem_data = pd_query(check_problem_sql.format(times[0], times[1], MAJOR))
    # 音视频检查
    mv_data = pd_query(CHECK_MV_COST_TIME_SQL.format(times[0], times[1], MAJOR)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format('2017-10-01', times[1], MAJOR))
    # 日安全责任事故
    daily_without = pd_query(without_accident_sql.format(times[0], times[1], MAJOR))
    # 安全预警
    safety_notification = pd_query(warning_notification_sql.format(times[0], times[1], MAJOR))
    # 履职评价
    eva_data = pd_query(check_evaluate_sql.format(times[0], times[1], MAJOR))
    # 检查地点
    point_data = pd_query(check_point_address_sql.format(times[0], times[1], MAJOR)).drop_duplicates(subset=['PK_ID']).reset_index()

    first = get_first(info_data, problem_data, eva_data, mv_data, daily_without, point_data)
    second = get_second(without_accident)
    third = get_third(safety_notification)
    return {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "MAJOR",
        'major': '机务',
        "file_name": '{0}机务系统安全分析日报.xlsx',
        "created_at": dt.now(),
        'first': first,
        'second': second,
        'third': third
    }


def get_first(info_data, problem_data, eva_data, mv_data, daily_without, point_data):
    """
    检查信息
    :param info_data:
    :param problem_data:
    :param eva_data:
    :param mv_data:
    :param daily_without:
    :return:
    """
    all_list = []
    for i in ['成都机务段', '重庆机务段', '贵阳机务段', '西昌机务段', '机务部']:
        data = daily_without[daily_without['STATION'].str.contains(i)]
        info = info_data[info_data['STATION'].str.contains(i)]
        problem = problem_data[problem_data['STATION'].str.contains(i)]
        mv = mv_data[mv_data['STATION'].str.contains(i)]
        eva = eva_data[eva_data['STATION'].str.contains(i)]
        point = point_data[point_data['STATION'].str.contains(i)]
        # 重要检查点检查
        check_point = point

        code_type = {}
        # 计算ABCD 4类问题无责任天数
        for j in ['A', 'B', 'C', 'D']:
            accident = data[(data['CODE'].str.contains(j)) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
            if len(accident) == 0:
                code_type['{0}'.format(j)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
            else:
                code_type['{0}'.format(j)] = \
                    str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).
                        iloc[0]['OCCURRENCE_TIME'].date()).split(' ')[0]
        content = '机务系统实现无责任一般A类事故{0}天;无责任一般B类事故{1}天;无责任一般C类事故{2}天;' \
                  '无责任一般D类事故{3}天。全段实现安全生产{4}天。'.format(code_type['A'], code_type['B'],
                                                       code_type['C'], code_type['D'], code_type['A'])
        # 高质量问题
        high = problem[problem['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])].dropna(subset=['PROBLEM_POINT'])
        # 高铁动车高质量问题
        dg = high[high['PROBLEM_DIVIDE_IDS'].isin([1, 2])]
        # A类问题情况
        a_data = problem[problem['LEVEL'] == 'A']
        all_a = []
        for x, y in a_data.reset_index().iterrows():
            a_content = '%s' % (x + 1) + '.' + y['PROBLEM_POINT'] + ' 时间：' + str(y['SUBMIT_TIME']) + ' ' + '-'.join(y['DUTY_DEPT'].split('-')[:2])
            all_a.append(a_content)
        a_desc = '\n'.join(all_a) if len(all_a) != 0 else '无'
        # 问题项点出现最多的高质量问题情况
        problem_point = list(high.groupby('PROBLEM_POINT').count().sort_values('LEVEL', ascending=False).reset_index()['PROBLEM_POINT'])
        if len(problem_point) == 0:
            high_desc = '无'
        else:
            high_desc = '\n'.join(high[high['PROBLEM_POINT'].str.contains(problem_point[0])]['DESCRIPTION'].unique().tolist())
        dic = {
            # 现场检查
            'xc': len(info[info['CHECK_WAY'] == 1]),
            # 添乘检查
            'tc': len(info[info['CHECK_WAY'] == 2]),
            # 异地点检查
            'ydd': len(check_point[check_point['CHECK_ITEM_NAMES'].str.contains('异地点')]),
            # 动客车检查
            'dkc': len(info[(info['IS_DONGCHE'] == 1) | (info['IS_KECHE'] == 1) | (info['IS_GAOTIE'] == 1)]),
            # 动车停放点检查
            'dctfd': len(check_point[check_point['CHECK_ADDRESS_NAMES'].str.contains('动车停放点')]),
            # 调车机（点）检查
            'dcj': len(check_point[(check_point['CHECK_ADDRESS_NAMES'].str.contains('调车点')) |
                                   (check_point['CHECK_ADDRESS_NAMES'].str.contains('调乘一体化'))]),
            # 夜查
            'yc': len(info[info['IS_YECHA'] == 1]),
            # 音视频调阅次数
            'dy_count': len(info[info['CHECK_WAY'] == 3]),
            # 音视频调阅时长
            'dy_time': round(float(mv[mv['CHECK_WAY'] == 3]['COST_TIME'].sum()), 4),
            # 音视频复检次数
            're_count': len(info[info['CHECK_WAY'] == 4]),
            # 音视频复检时长
            're_time': round(float(mv[mv['CHECK_WAY'] == 4]['COST_TIME'].sum()), 4),
            # 数据分析次数
            'data_count': len(mv[mv['RETRIVAL_TYPE_NAME'].str.contains('数据分析')]),
            # 数据分析复检次数
            'data_re':  len(mv[mv['RETRIVAL_TYPE_NAME'].str.contains('数据分析复查')]),
            # 问题总数
            'pro_count': len(problem),
            # 高质量问题数
            'main_pro': len(high),
            # A类问题情况
            'a_desc': a_desc,
            # 动车、高铁高质量问题数
            'dcgt_count': len(dg),
            # 动车、高铁高质量问题情况
            'dcgt_desc': '\n'.join(list(dg['DESCRIPTION'].unique())) if len(dg['DESCRIPTION']) != 0 else '无',
            # 记分评价人次
            'eva_count': len(eva),
            # 路局记分评价条数
            'luju_eva': len(eva[eva['CHECK_TYPE'] == 1]),
            # 日安全生产情况
            # 'content': content,
            # 问题项点出现最多的高质量问题情况
            'high_desc': high_desc
               }
        all_list.append(dic)
    return {
        'all_list': all_list,
    }


def get_second(without_accident):
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
        if len(accident) == 0:
            dic['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0]['OCCURRENCE_TIME'].date()).split(' ')[0]
    content = '截至今日：机务系统实现无责任一般A类事故{0}天;无责任一般B类事故{1}天;无责任一般C类事故{2}天;' \
              '无责任一般D类事故{3}天。全段实现安全生产{4}天。'.format(dic['A'], dic['B'], dic['C'], dic['D'], dic['A'])
    return content


def get_third(warning_data):
    """
    风险预警
    :param times:
    :return:
    """
    warning_data = warning_data[warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7])]
    # 上级
    road_data = warning_data[warning_data['HIERARCHY'] == 1].reset_index().head(3)
    company_warn_list = []
    if len(road_data) == 0:
        company_warn_list = ['无']
    else:
        for index in range(0, len(road_data)):
            company_warn_list.append("""{0}.{1}""".format(
                index + 1,
                road_data['CONTENT'][index])
            )
    return {
        'company_warn_list': '\n'.join(company_warn_list)
    }