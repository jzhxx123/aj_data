from app.data.util import pd_query
import re
import pandas as pd
from app.report.jiwu.major_weekly_report_sql import *
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta


def _get_ring_diff(now, last):
    """
    计算环比
    :param now:
    :param last:
    :return:
    """
    if last == 0:
        return {
            'diff': now,
            'percent': now * 100
        }
    else:
        return {
            'diff': now - last,
            'percent': round((now - last) / last * 100, 1)
        }


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')  # 2019-04-27
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_month = start_date[5:7]
    start_day = start_date[8:]
    end_month = end_date[5:7]
    end_day = end_date[8:]
    year = start_date[:4]
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职评价
    eva_data = pd_query(check_evaluate_sql.format(start_date, end_date))
    # 履职复查
    eva_re_data = pd_query(check_evaluate_review_sql.format(start_date, end_date))

    first = get_first(info_data, last_info, problem_data, last_problem)
    second = get_second(eva_data, eva_re_data)
    third = get_third(problem_data, last_problem)
    # four = get_four(info_data, problem_data)
    # six = get_six(eva_data, eva_re_data)
    file_name = f'{start_date}至{end_date}机务系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "hierarchy": "MAJOR",
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        'major': '机务',
        'created_at': dt.now(),
        'file_name': file_name,
        "first": first,
        'second': second,
        'third': third,
        # 'four': four,
        # 'six': six
    }
    return result


# --------------------------------------------------第一部分-----------------------------------------------------------------------------------------------------
def get_first(info_data, last_info, problem_data, last_problem):
    # 检查信息
    check_info = get_check_info(info_data, last_info)
    # 问题情况
    problem_situation = get_problem_situation(problem_data, last_problem)
    # 现场+设备调阅
    table = _station_check_table(info_data, problem_data)
    return {
        'check_info': check_info,
        'problem_situation': problem_situation,
        'table': table
    }


def get_check_info(info_data, last_info):
    info_data = info_data
    # 总检查次数
    count = len(info_data)
    last_count = len(last_info)
    ratio = _get_ring_diff(count, last_count)
    # 现场检查
    xc = len(info_data[info_data['CHECK_WAY'].isin([1, 2])])
    # 设备调阅
    shebei = len(info_data[info_data['CHECK_WAY'].isin([3, 4])])
    return {
        'count': count,
        'ratio': ratio,
        'xc': xc,
        'shebei': shebei
    }


def get_problem_situation(problem_data, last_problem):
    problem_data = problem_data
    last_problem = last_problem
    # 发现问题数
    counts = len(problem_data)
    last_count = len(last_problem)
    ratio = _get_ring_diff(counts, last_count)
    # 作业问题
    work_problem = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    last_work = len(last_problem[last_problem['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    ratio1 = _get_ring_diff(work_problem, last_work)
    # 设备、管理
    all_count = []
    for i in ['F', 'E']:
        data = problem_data[problem_data['LEVEL'].str.contains(i)]
        last_data = last_problem[last_problem['LEVEL'].str.contains(i)]
        count = len(data)
        ratios = _get_ring_diff(count, len(last_data))
        all_count.append([count, ratios])
    # 外部环境和反恐防暴项问题
    env_data = problem_data[(problem_data['LEVEL'].str.contains('G')) | (problem_data['LEVEL'].str.contains('K'))]
    last_env = last_problem[(last_problem['LEVEL'].str.contains('G')) | (last_problem['LEVEL'].str.contains('K'))]
    ratio2 = _get_ring_diff(len(env_data), len(last_env))
    # 红线问题
    red_pro = problem_data[problem_data['IS_RED_LINE'].isin([1, 2])]
    last_red = last_problem[last_problem['IS_RED_LINE'].isin([1, 2])]
    ratio3 = _get_ring_diff(len(red_pro), len(last_red))
    # 性质严重问题
    main_pro = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    last_main = last_problem[last_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    ratio4 = _get_ring_diff(len(main_pro), len(last_main))
    # 问题分项
    type_count = []
    for i in ['A', 'B', 'F1', 'F2', 'E1', 'E2']:
        type_count.append(len(problem_data[problem_data['LEVEL'] == i]))
    return {
        'count': counts,
        'ratio': ratio,
        'work_problem': work_problem,
        'ratio1': ratio1,
        'all_count': all_count,
        'env_problem': len(env_data),
        'ratio2': ratio2,
        'red_problem': len(red_pro),
        'ratio3': ratio3,
        'main_problem': len(main_pro),
        'ratio4': ratio4,
        'type_count': type_count
    }


def _station_check_table(info_data, problem_data):
    station_list = ['成都机务段', '重庆机务段', '西昌机务段', '贵阳机务段']
    all_list = []
    for i in station_list:
        new_info = info_data[info_data['NAME'].str.contains(i)]
        new_pro = problem_data[problem_data['NAME'].str.contains(i)]
        # 下现场检查
        xc_info_data = new_info[new_info['CHECK_WAY'].isin([1, 2])]
        xc_pro_data = new_pro[new_pro['CHECK_WAY'].isin([1, 2])]
        xc_count = len(xc_info_data)
        xc_pro = int(xc_info_data['PROBLEM_NUMBER'].sum())
        xc_main = len(xc_pro_data[xc_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        # 设备监控调阅
        shebei_info_data = new_info[new_info['CHECK_WAY'].isin([3, 4])]
        shebei_pro_data = new_pro[new_pro['CHECK_WAY'].isin([3, 4])]
        shebei_count = len(shebei_info_data)
        shebei_pro = int(shebei_info_data['PROBLEM_NUMBER'].sum())
        shebei_main = len(shebei_pro_data[shebei_pro_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        all_list.append([i, xc_count, xc_pro, xc_main, shebei_count, shebei_pro, shebei_main])
    return {
        'all_list': all_list
    }


# ---------------------------------------------------------第二部分-----------------------------------------------------
def get_second(eva_data, eva_re_data):
    # 评价基本情况
    eva_situation = get_eva_situation(eva_data, eva_re_data)
    # 评价记分情况
    score_situation = get_score_situation(eva_data)
    # 存在主要问题
    exist_pro = get_exist_pro(eva_data)
    return {
        'eva_situation': eva_situation,
        'score_situation': score_situation,
        'exist_pro': exist_pro
    }


def get_eva_situation(eva_data, eva_re_data):
    # 逐条评价
    zt = len(eva_data[eva_data['EVALUATE_WAY'] == 1])
    zt_re = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 1])
    # 定期
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    dq_re = len(eva_re_data[eva_re_data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    # 设备监控调阅复查
    shebei = len(eva_data[eva_data['CHECK_WAY'].isin([3, 4])])
    # 总记分
    score = round(float((eva_data['SCORE'].sum())), 4)
    # 记分最多人
    if eva_data.empty is True:
        max_data = eva_data
        max_score = 0
    else:
        max_data = eva_data.groupby(['RESPONSIBE_ID_CARD', 'ALL_NAME', 'RESPONSIBE_PERSON_NAME']).sum().sort_values(by='SCORE', ascending=False).reset_index()
        max_score = list(max_data['SCORE'])[0]
    max_list = []
    for i, j in max_data[max_data['SCORE'] == max_score].iterrows():
        dic = {
            'name': j['RESPONSIBE_PERSON_NAME'],
            'dp': j['ALL_NAME']
        }
        max_list.append(dic)
    return {
        'zt': zt,
        'zt_re': zt_re,
        'dq': dq,
        'dq_re': dq_re,
        'shebei': shebei,
        'score': score,
        'max_list': max_list
    }


def get_score_situation(eva_data):
    # 分析室和分析中心评价
    fxs = len(eva_data[eva_data['CHECK_TYPE'] == 1])
    fxzx = len(eva_data[eva_data['CHECK_TYPE'] == 2])
    # 被评价
    pj_count = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = eva_data[eva_data['STATION'] == i]
        pj_count.append(len(data))
    # 定期、逐条、阶段
    dq = len(eva_data[eva_data['EVALUATE_WAY'] == 2])
    zt = len(eva_data[eva_data['EVALUATE_WAY'] == 1])
    jd = len(eva_data[eva_data['EVALUATE_WAY'] == 3])
    zd = len(eva_data[eva_data['EVALUATE_WAY'] == 0])
    # 职务分类
    zhiwu_count = []
    for i in ['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        data = eva_data[eva_data['GRADATION'] == i]
        zhiwu_count.append(len(data))
    # 计分内容 录入信息、问题明显错误。
    dic = dict()
    for i, j in eva_data.groupby('SITUATION').count().sort_values(by='GRADATION', ascending=False).reset_index().iterrows():
        if j['SITUATION'] != '录入信息、问题明显错误。':
            if not dic.get(int(j['GRADATION']), None):
                dic[int(j['GRADATION'])] = []
            dic[int(j['GRADATION'])].append(j['SITUATION'])
        else:
            continue
    all_reason = [{
        'name': ''.join(j),
        'count': i
    } for i, j in dic.items()]
    # 具体记分内容
    all_table = []
    for i, j in eva_data.iterrows():
        dic = {
            'name': j['RESPONSIBE_PERSON_NAME'],
            'dp': j['STATION'],
            'type': j['CHECK_TYPE'],
            'code': j['CODE'],
            'codes': j['CODE_ADDITION']
        }
        all_table.append(dic)
    return {
        'fxs': fxs,
        'fxzx': fxzx,
        'pj_count': pj_count,
        'dq': dq,
        'zd': zd,
        'zt': zt,
        'jd': jd,
        'zhiwu_count': zhiwu_count,
        'all_reason': all_reason,
        'all_table': all_table
    }


def get_exist_pro(eva_data):
    """
    存在主要问题
    :param eva_data:
    :return:
    """
    all_content = []
    for i, k in eva_data.groupby('ITEM_NAME').count().sort_values('SITUATION', ascending=False).reset_index().iterrows():
        dic = {
            'name': k['ITEM_NAME'],
            'content': int(k['SITUATION'])
        }
        all_content.append(dic)
    return {
        'all_content': all_content[:3]
    }


def get_third(problem_data, last_problem):
    # 红线问题
    red_data = problem_data[problem_data['IS_RED_LINE'].isin([1, 2])]
    all_count = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = red_data[red_data['NAME'] == i]
        dic = {
            'name': i,
            'count': len(data),
            'person': data['CHECK_PERSON_NAMES'].tolist(),
            'content': data['DESCRIPTION'].tolist()
        }
        all_count.append(dic)
    # 动车典型问题
    dc_data = problem_data[problem_data['SHOP'] == '动车运用车间']
    last_dc = last_problem[last_problem['SHOP'] == '动车运用车间']
    dc_pro = get_dc(dc_data, last_dc)
    # 普速列车问题
    name = list(problem_data['SHOP'].unique())
    if '动车运用车间' in name:
        name.remove('动车运用车间')
    else:
        pass
    pslc_data = problem_data[problem_data['SHOP'].isin(name)].reset_index()
    last_pslc = last_problem[last_problem['SHOP'].isin(name)].reset_index()
    pslc_pro = get_pslc(pslc_data, last_pslc)
    return {
        'red_count': len(red_data),
        'all_count': all_count,
        'dc_pro': dc_pro,
        'pslc_pro': pslc_pro
    }


def get_dc(dc_data, last_dc):
    # 作业层面
    work = get_work_dc(dc_data, last_dc)
    # 管理层面
    manager = get_manager_dc(dc_data, last_dc)
    return {
        'work': work,
        'manager': manager
    }


def get_work_dc(dc_data, last_dc):
    work_data = dc_data[dc_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    last_work = last_dc[last_dc['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    # 环比
    ratio1 = _get_ring_diff(len(work_data), len(last_work))
    # 单类问题数
    type_count = []
    for i in ['A', 'B', 'C', 'D']:
        type_count.append(len(work_data[work_data['LEVEL'] == i]))
    # 间断瞭望
    jdlw_data = dc_data[(dc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (dc_data['PROBLEM_POINT'].str.contains('间歇打盹'))]
    last_jdlw = last_dc[(last_dc['PROBLEM_POINT'].str.contains('间断瞭望')) | (last_dc['PROBLEM_POINT'].str.contains('间歇打盹'))]
    jdlw_count = len(jdlw_data)
    ratio2 = _get_ring_diff(jdlw_count, len(last_jdlw))
    # 典型间断瞭望问题
    content = jdlw_data[jdlw_data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
    all_list = []
    for i in ['列车超速运行', '动车组司机所内及非营业车站违反开关门规定', '重要作业步骤漏项']:
        data = dc_data[dc_data['PROBLEM_POINT'].str.contains(i)]
        last_data = last_dc[last_dc['PROBLEM_POINT'].str.contains(i)]
        dic = {
            'name': i,
            'count': len(data),
            'ratio': _get_ring_diff(len(data), len(last_data)),
            'content': data[data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
        }
        all_list.append(dic)
    return {
        'work_count': len(work_data),
        'ratio1': ratio1,
        'type_count': type_count,
        'jdlw_count': jdlw_count,
        'ratio2': ratio2,
        'content': content,
        'all_list': all_list
    }


def get_manager_dc(dc_data, last_dc):
    m_data = dc_data[dc_data['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])]
    last_m = last_dc[last_dc['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])]
    # 环比
    ratio1 = _get_ring_diff(len(m_data), len(last_m))
    # 单类问题数
    type_count = []
    for i in ['F1', 'F2', 'F3', 'F4']:
        type_count.append(len(m_data[m_data['LEVEL'] == i]))
    return {
        'm_count': len(m_data),
        'ratio1': ratio1,
        'type_count': type_count,
    }


def get_pslc(pslc_data, last_pslc):
    # 间断瞭望
    jdlw = get_pslc_jdlw(pslc_data, last_pslc)
    # 劳动安全隐患
    ldaq = get_pslc_ldaq(pslc_data, last_pslc)
    # 乘务员错误操纵问题
    cwcz = get_pslc_cwcz(pslc_data, last_pslc)
    # 调车安全隐患
    dcaq = get_pslc_dcaq(pslc_data, last_pslc)
    # 机车漏检漏修问题突出
    ljlx = get_pslc_ljlx(pslc_data, last_pslc)
    return {
        'jdlw': jdlw,
        'ldaq': ldaq,
        'cwcz': cwcz,
        'ljlx': ljlx,
        'dcaq': dcaq
    }


def get_pslc_jdlw(pslc_data, last_pslc):
    """
    普速列车间断瞭望
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    jdlw_data = pslc_data[
        (pslc_data['PROBLEM_POINT'].str.contains('间断瞭望')) | (pslc_data['PROBLEM_POINT'].str.contains('间歇打盹'))]
    last_jdlw = last_pslc[
        (last_pslc['PROBLEM_POINT'].str.contains('间断瞭望')) | (last_pslc['PROBLEM_POINT'].str.contains('间歇打盹'))]
    jdlw_count = len(jdlw_data)
    ratio2 = _get_ring_diff(jdlw_count, len(last_jdlw))
    # 按段分
    jdlw_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = jdlw_data[jdlw_data['NAME'] == i]
        jdlw_stations.append(len(data))
    # 典型间断瞭望问题
    content = jdlw_data[jdlw_data['LEVEL'].isin(['A', 'B'])]['DESCRIPTION'].tolist()
    return {
        'jdlw_count': jdlw_count,
        'ratio2': ratio2,
        'jdlw_stations': jdlw_stations,
        'content': content
    }


def get_pslc_ldaq(pslc_data, last_pslc):
    """
    普速列车劳动安全
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    pslc_data = pslc_data.dropna(subset=['RISK_NAMES'])
    last_pslc = last_pslc.dropna(subset=['RISK_NAMES'])
    ldaq_data = pslc_data[pslc_data['RISK_NAMES'].str.contains('机务-劳动安全')]
    last_ldaq = last_pslc[last_pslc['RISK_NAMES'].str.contains('机务-劳动安全')]
    ratio = _get_ring_diff(len(ldaq_data), len(last_ldaq))
    ldaq_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = ldaq_data[ldaq_data['NAME'] == i]
        ldaq_stations.append(len(data))
    return {
        'ldaq_count': len(ldaq_data),
        'ratio': ratio,
        'ldaq_stations': ldaq_stations
    }


def get_pslc_cwcz(pslc_data, last_pslc):
    """
    普速列车乘务员错误操作
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    cwcz_data = pslc_data[(pslc_data['RISK_NAMES'].str.contains('错误操作')) & (pslc_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    last_cwcz = last_pslc[(last_pslc['RISK_NAMES'].str.contains('错误操作')) & (last_pslc['LEVEL'].isin(['A', 'B', 'C', 'D']))]
    ratio = _get_ring_diff(len(cwcz_data), len(last_cwcz))
    # 严重问题
    main_data = cwcz_data[cwcz_data['LEVEL'].isin(['A', 'B'])]
    last_main = last_cwcz[last_cwcz['LEVEL'].isin(['A', 'B'])]
    ratio1 = _get_ring_diff(len(main_data), len(last_main))
    # 分类
    count = []
    for i in ['操纵不当造成监控装置', '错误关闭劈相机', '错误使用车位键调整距离', '机车试验不当造成机车移动', '未进行制动机简略试验']:
        data = main_data[main_data['PROBLEM_POINT'].str.contains(i)]
        count.append(len(data))
    return {
        'cwcz_count': len(cwcz_data),
        'ratio': ratio,
        'main_count': len(main_data),
        'ratio1': ratio1,
        'count': count
    }


def get_pslc_dcaq(pslc_data, last_pslc):
    """
    普速列车调车安全隐患
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    dcaq_data = pslc_data[pslc_data['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    last_dcaq = last_pslc[last_pslc['WORK_PROCEDURE'].isin(['出入段（所）与挂车', '调车作业'])]
    ratio = _get_ring_diff(len(dcaq_data), len(last_dcaq))
    # 严重问题
    main_data = dcaq_data[dcaq_data['LEVEL'].isin(['A', 'B'])]
    content = []
    for i, j in main_data.groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index().iterrows():
        dic = {
            'name': j['PROBLEM_POINT'],
            'count': int(j['LEVEL'])
        }
        content.append(dic)
    return {
        'dcaq_count': len(dcaq_data),
        'ratio': ratio,
        'main_count': len(main_data),
        'content': content
    }


def get_pslc_ljlx(pslc_data, last_pslc):
    """
    普速列车漏检漏修问题
    :param pslc_data:
    :param last_pslc:
    :return:
    """
    ljlx_data = pslc_data[(pslc_data['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写') | (pslc_data['RISK_NAMES'].str.contains('行车设备质量'))]
    ljlx_data = ljlx_data[(ljlx_data['SHOP'].str.contains('检修车间')) | (ljlx_data['SHOP'].str.contains('整备车间')) | (ljlx_data['SHOP'].str.contains('解体组装'))]
    ljlx_data = ljlx_data[ljlx_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    last_ljlx = last_pslc[(last_pslc['RISK_NAMES'] == '机务-作业风险-漏检、漏修、漏探、漏试、漏分析、漏填写') | (last_pslc['RISK_NAMES'].str.contains('行车设备质量'))]
    last_ljlx = last_ljlx[(last_ljlx['SHOP'].str.contains('检修车间')) | (last_ljlx['SHOP'].str.contains('整备车间')) | (last_ljlx['SHOP'].str.contains('解体组装'))]
    last_ljlx = last_ljlx[last_ljlx['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    ratio = _get_ring_diff(len(ljlx_data), len(last_ljlx))
    # 按段分类
    ljlx_stations = []
    for i in ['成都机务段', '贵阳机务段', '重庆机务段', '西昌机务段']:
        data = ljlx_data[ljlx_data['NAME'] == i]
        ljlx_stations.append(len(data))
    return {
        'ljlx_count': len(ljlx_data),
        'ratio': ratio,
        'ljlx_stations': ljlx_stations
    }


def get_four(info_data, problem_data):
    # 监控设备调阅+复查
    shebei_data = info_data[info_data['CHECK_WAY'].isin([3, 4])]
    count = len(shebei_data)
    # 覆盖车间
    yycj = len(shebei_data[shebei_data['SHOP'].str.contains('运用车间')]['SHOP'].unique())
    jxzb = len(shebei_data[shebei_data['SHOP'].str.contains('检修车间')]['SHOP'].unique())
    # 发现问题
    find_pro_data = problem_data[problem_data['CHECK_WAY'].isin([3, 4])]
    find_pro = len(find_pro_data)
    # 问题分类统计
    counts = []
    for i in ['A', 'B', 'C', 'D']:
        data = find_pro_data[find_pro_data['LEVEL'] == i]
        counts.append(len(data))
    # 设备项、管理项问题
    sanx = []
    for i in ['E', 'F']:
        data = find_pro_data[find_pro_data['LEVEL'].str.contains(i)]
        sanx.append(len(data))
    return {
        'count': count,
        'yycj': yycj,
        'jxzb': jxzb,
        'find_pro': find_pro,
        'counts': counts,
        'sanx': sanx
    }


def get_six(eva_data, eva_re_data):
    all_list = []
    for i in ['西昌机务段', '重庆机务段', '贵阳机务段', '成都机务段']:
        data = eva_data[eva_data['STATION'] == i]
        re_data = eva_re_data[eva_re_data['NAME'] == i]
        # 检查信息、问题逐条评价
        zt = len(data[(data['EVALUATE_WAY'] == 1) & (data['EVALUATE_TYPE'].isin([1, 2]))])
        ratio = round(ded_zero(zt, len(data)) * 100, 1)
        # 逐条复查
        zt_re = len(re_data[re_data['EVALUATE_WAY'] == 1])
        ratio1 = round(ded_zero(zt_re, zt) * 100, 1)
        # 音视频复查
        mv_re = len(re_data[re_data['EVALUATE_TYPE'] == 3])
        # 监控设备调阅及复查人次
        shebei = len(data[data['CHECK_WAY'] == 3])
        shebei_re = len(data[data['CHECK_WAY'] == 4])
        # 定期及复查
        dq = len(data[data['EVALUATE_WAY'] == 2]) + len(re_data[re_data['EVALUATE_WAY'] == 2])
        # 阶段评价人次
        jd = len(data[data['EVALUATE_WAY'] == 3])
        # 记分人次
        person = len(data)
        all_list.append([i, zt, ratio, zt_re, ratio1, mv_re, shebei, shebei_re, dq, jd, person])
    return {
        'all_list': all_list
    }
