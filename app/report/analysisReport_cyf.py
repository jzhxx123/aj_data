#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from app import mongo
import pandas as pd
from datetime import datetime
from flask import jsonify, current_app
from app.data.util import pd_query
from app.report.analysisMonthlyReport_sql import WARNING_NOTIFICATION_AND_TASK_PROCESS_SQL

# 安全预警分析
def get_safety_tip(start_date=None, end_date=None):
	if start_date is None:
		raise(Exception("get 预警总体情况 arg start_date error: {0}".format({start_date})))
	if end_date is None:
		end_date = datetime.now().strftime('%Y-%m-%d')
	# 获取预警次数
	"""
        STATUS: 通知书状态 eg. 0: 待核实
        HIERARCHY: 层级 1: 路局 2: 专业 3: 站段
        CREATE_TIME: 预警时间
        RANK: 等级
        warning_type: 预警类型 eg. 1.安全事故警告预警 2.差异化精准警告预警 3.劳动安全专项警
        department_type: 部门类型：1:局，2：专业，3：处，4：段，5:局科室，6：处科室，7：段
        BELONG_PROFESSION_NAME: 所属专业系统
	"""
	dFrames = pd_query("""
					SELECT
						a.PK_ID,
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.BELONG_PROFESSION_NAME
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
				""".format(start_date, end_date))
	dFrames = dFrames.drop_duplicates(subset=['PK_ID'])
	del dFrames['PK_ID']
	dFrames["HAS_APPLY_UUID"] = dFrames["APPLY_UUID"].str.len()
	rows_count, cols_count = dFrames.shape
	# 路局 或 专业系统 层面
	luju_dFrames = dFrames[((dFrames["HIERARCHY"] == 1)) | (dFrames["HIERARCHY"] == 2)]
	# 降序排列取前三 (预警次数最多的三个站段)
	statistics1 = luju_dFrames.groupby("duty_department_name")
	# 站段数
	statistics1_top3 = statistics1.size().sort_values(ascending=False).head(3)
	station_name = '、'.join(statistics1_top3.keys())

	# 站段层面 警告预警
	zhanduan_dFrames = dFrames[(dFrames["HIERARCHY"] == 3)]

	# 集团公司 差异化精准警告预警
	difference_precise_warning = dFrames[((dFrames["HIERARCHY"] == 1) & dFrames["warning_type"] == 2)]
	# 站段数
	dpw_station_count = difference_precise_warning.groupby("duty_department_name").size().shape[0]
	# 所属 专业 系统
	yunshu_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "运输"].groupby("duty_department_name").size()
	keyun_sys =  difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "客运"].groupby("duty_department_name").size()
	huoyun_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "货运"].groupby("duty_department_name").size()
	jiwu_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "机务"].groupby("duty_department_name").size()
	gongwu_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "工务"].groupby("duty_department_name").size()
	gongdian_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "供电"].groupby("duty_department_name").size()
	dianwu_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "电务"].groupby("duty_department_name").size()
	cheliang_sys = difference_precise_warning[difference_precise_warning["BELONG_PROFESSION_NAME"] == "车辆"].groupby("duty_department_name").size()

	# 日常安全工作提示 构成：分析日报 + 系统通知
	data_analysis_daily = pd_query("""
			SELECT
				PK_ID
			FROM
				t_analysis_center_daily
			WHERE
				DATE BETWEEN Date( '{0}' ) AND Date( '{1}' )
				AND LENGTH(RISK_WARNING) > 4
	""".format(start_date, end_date))
	# TYPE 3 安全提示、  STATUS 2 已发布、 HIERARCHY 1 路局层面
	data_sys_notification = pd_query("""
			SELECT
				PK_ID
			FROM
				t_system_notify
			WHERE
				HIERARCHY = 1
				AND TYPE = 3
				AND STATUS = 2
				AND CREATE_TIME BETWEEN '{0}'
				AND  '{1} 23:59:59'
	""".format(start_date, end_date))
	# 日常安全工作提示最终值
	daily_safety_tip = data_analysis_daily.shape[0] + data_sys_notification.shape[0]

	# 重点问题预警
	dFrames_important_problem = pd_query("""
			SELECT
				b.HIERARCHY as hierarchy
			FROM
				t_important_problem_prompt AS a
				INNER JOIN t_important_problem_config AS b ON a.FK_IMPORTANT_PROBLEM_CONFIG_ID = b.PK_ID
			WHERE
				a.CREATE_TIME BETWEEN '{0}'
				AND '{1} 23:59:59'
	""".format(start_date, end_date))
	# 业务处 提示
	ywc_important_problem = dFrames_important_problem[dFrames_important_problem["hierarchy"] == 2].shape[0]
	# 站段提示
	station_important_problem = dFrames_important_problem[dFrames_important_problem["hierarchy"] == 3].shape[0]

	#风险预警提示
	dFrames_warning_risk = pd_query("""
			SELECT
				b.HIERARCHY as hierarchy
			FROM
				t_warning_risk AS a
				INNER JOIN t_warning_risk_prompt_config as b ON a.FK_CONFIG_ID = b.PK_ID
			WHERE
				a.CREATE_TIME BETWEEN '{0}'
				AND '{1} 23:59:59'
	""".format(start_date, end_date))
	# 业务处 提示
	ywc_risk_warn = dFrames_warning_risk[dFrames_warning_risk["hierarchy"] == 2].shape[0]
	# 站段提示
	station_risk_warn = dFrames_warning_risk[dFrames_warning_risk["hierarchy"] == 3].shape[0]

	# 生成风险提示通知书 status != 5 过滤状态为：取消预警
	risk_notification = pd_query("""
			SELECT
				PK_ID
			FROM
				t_warning_risk_notification
			WHERE
				CREATE_TIME BETWEEN '{0}'
				AND '{1} 23:59:59'
				AND STATUS != 5
	""".format(start_date, end_date)).shape[0]

	# 人员提示预警
	date_list = get_yearCombineMonth_list(end_date, end_date)
	condition = ''
	for year_month in date_list:
		ym_date = datetime.strptime(year_month, '%Y-%m')
		condition += '( YEAR = {0} AND MONTH = {1} ) OR'.format(ym_date.year, ym_date.month)
	condition = condition[:-3]
	# 初始化人员提示预警统计变量
	person_warning_count = 0
	if(len(condition) == 0):
		raise(Exception("人员提示预警 args:日期存在异常"))
	else:
		dFrames_person_warning = pd_query("""
				SELECT
					PK_ID, ID_CARD
				FROM
					t_warning_key_person
				WHERE {0}
		""".format(condition))
		# 人数去重（ID_CARD）
		person_warning_count = len(set(dFrames_person_warning['ID_CARD'].values.tolist()))
	# 单位预警情况罗列
	tmp = dFrames[dFrames["HIERARCHY"] == 1].reset_index()
	warning_type_map = {
		1: "安全事故警告预警",
		2: "差异化精准警告预警",
		3: "劳动安全专项警告预警",
		4: "其他警告预警"
	}
	rank_map = {
		1: "I级",
		2: "II级",
		3: "III级",
		4: "IV"
	}
	company_warn_list = []
	for index in range(0, len(tmp)):
		tmp_date = tmp["CREATE_TIME"][index]
		company_warn_list.append("""{0}月{1}日对{2}开展{3}""".format(
			tmp_date.month,
			tmp_date.day,
			tmp["duty_department_name"][index],
			rank_map.get(tmp["RANK"][index]) + warning_type_map.get(tmp["warning_type"][index])
		))
	return {
		"whole_warning": {
			"luju_hierarchy": {
				"top3": station_name,
				"group_hierarchy": luju_dFrames[luju_dFrames["HIERARCHY"] == 1].shape[0],
				"profession_hierarchy": luju_dFrames[luju_dFrames["HIERARCHY"] == 2].shape[0],
				"station_count": statistics1.size().shape[0],
				"I": luju_dFrames[luju_dFrames["RANK"] == 1].shape[0],
				"II": luju_dFrames[luju_dFrames["RANK"] == 2].shape[0],
				"III": luju_dFrames[luju_dFrames["RANK"] == 3].shape[0],
			},
			"station_hierarchy": {
				"I": zhanduan_dFrames[zhanduan_dFrames["RANK"] == 1].shape[0],
				"II": zhanduan_dFrames[zhanduan_dFrames["RANK"] == 2].shape[0],
				"III": zhanduan_dFrames[zhanduan_dFrames["RANK"] == 3].shape[0]
			},
			"dpw": {
				"dpw_station_count": dpw_station_count,
				"difference_precise_warning": difference_precise_warning.shape[0],
				"yunshu_sys": {
					"station_num": yunshu_sys.shape[0],
					"count": int(yunshu_sys.sum()),
				},
				"keyun_sys": {
					"station_num": keyun_sys.shape[0],
					"count": int(keyun_sys.sum()),
				},
				"huoyun_sys": {
					"station_num": huoyun_sys.shape[0],
					"count": int(huoyun_sys.sum()),
				},
				"jiwu_sys": {
					"station_num": jiwu_sys.shape[0],
					"count": int(jiwu_sys.sum()),
				},
				"gongwu_sys": {
					"station_num": gongwu_sys.shape[0],
					"count": int(gongwu_sys.sum()),
				},
				"gongdian_sys": {
					"station_num": gongdian_sys.shape[0],
					"count": int(gongdian_sys.sum()),
				},
				"cheliang_sys": {
					"station_num": cheliang_sys.shape[0],
					"count": int(cheliang_sys.sum()),
				},
				"dianwu_sys": {
					"station_num": dianwu_sys.shape[0],
					"count": int(dianwu_sys.sum()),
				},
			},
			"safety_warning": {
				"daily_safety_tip": daily_safety_tip,
				"important_problem": {
					"ywc": ywc_important_problem,
					"station": station_important_problem,
				},
				"risk_warning": {
					"ywc": ywc_risk_warn,
					"station": station_risk_warn,
				},
				"risk_notification": risk_notification,
				"person_warning_count": person_warning_count,
			}
		},
		"company_warn_list": company_warn_list,
		"reform_situation": {
			"daizhenggai": list(dFrames[
								(dFrames["STATUS"] == 2) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			"leader_exam": list(dFrames[
								(dFrames["STATUS"] == 3) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			"ywc_yanshou": list(dFrames[
								(dFrames["STATUS"] == 4) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			"ywc_lingdao_yanshou": list(dFrames[
								(dFrames["STATUS"] == 5) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			"anjianshi_heshi": list(dFrames[
								(dFrames["STATUS"] == 6) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			"anjianshi_lingdao_heshi": list(dFrames[
								(dFrames["STATUS"] == 7) &
								(dFrames["HIERARCHY"].isin([1, 2]))
								]["duty_department_name"]),
			# "cancel_warning_station_count": dFrames[dFrames["STATUS"] == 9].groupby("duty_department_name").size().shape[0],
			"cancel_warning_station_count": _get_warning_notification_task(start_date, end_date)[0],
			"postpone_warning_station_count": _get_warning_notification_task(start_date, end_date)[1],
			# "commit_canncel_apply_station_count": dFrames[(dFrames["HIERARCHY"] == 1) & (dFrames["HAS_APPLY_UUID"] > 0)].shape[0]
			"commit_canncel_apply_station_count": dFrames[
				(dFrames['HIERARCHY'].isin([1, 2])) &
				(dFrames["STATUS"].isin([2, 3, 4, 5, 6]))
			].shape[0]
		}
	}


# 各级干部重点工作落实质量分析 第三大部分 第三部分
def get_check_evaluate_info(start_date, end_date):
	dFrames = pd_query("""
					SELECT
						a.PK_ID,
						b.NAME as responsible_department,
						a.RESPONSIBE_PERSON_NAME as person_name,
						a.RESPONSIBE_ID_CARD as id_card,
						a.GRADATION,
						a.YEAR,
						a.MONTH,
						a.CREATE_TIME,
						a.EVALUATE_WAY,
						a.CODE_ADDITION,
						a.CODE,
						a.EVALUATE_CONTENT,
						a.SCORE,
						a.CHECK_TYPE
					FROM
						t_check_evaluate_info as a
						INNER JOIN t_department as b ON a.RESPONSIBE_DEPARTMENT_ID = b.DEPARTMENT_ID
					WHERE
						a.CREATE_TIME BETWEEN "{0}"
						AND "{1} 23:59:59"
	""".format(start_date, end_date))
	# 重点工作   gclsbl:贯彻落实不力
	# 去重
	dFrames = dFrames.drop_duplicates(subset=['PK_ID'])
	del dFrames['PK_ID']
	gclsbl_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-1") |
						(dFrames["CODE_ADDITION"] == "ZD-5-2") |
						(dFrames["CODE_ADDITION"] == "ZD-8-1") |
						(dFrames["CODE_ADDITION"] == "ZD-8-2") ]
	gclsbl_info = []
	for i,row in gclsbl_dFrame.iterrows():
		gclsbl_info.append(_serializeProblemZG(row))

	# 重点工作 jcdcbg:检察督促不够
	jcdcbg_dFrame = dFrames[  (dFrames["CODE_ADDITION"] == "ZD-5-3") |
							  (dFrames["CODE_ADDITION"] == "ZD-8-3") ]
	jcdcbg_info = []
	for i,row in jcdcbg_dFrame.iterrows():
		jcdcbg_info.append(_serializeProblemZG(row))

	# 整改质量分析
	zg_dFrame = dFrames[dFrames["CODE"].str.contains("ZG")]
	zg_5_dFrame = dFrames[dFrames["CODE"] == "ZG-5"]
	zg_4_dFrame = dFrames[dFrames["CODE"] == "ZG-4"]
	zg_3_dFrame = dFrames[dFrames["CODE"] == "ZG-3"]
	zg_2_dFrame = dFrames[dFrames["CODE"] == "ZG-2"]
	zg_1_dFrame = dFrames[dFrames["CODE"] == "ZG-1"]

	# 问题整改不彻底
	zgbcd_dFrame = dFrames[ (dFrames["CODE"] == "ZG-1") |
						(dFrames["CODE"] == "ZG-2") |
						(dFrames["CODE"] == "ZG-3") ]
	zgbcd_info = []
	for i,row in zgbcd_dFrame.iterrows():
		zgbcd_info.append(_serializeProblemZG(row))

	# 问题整改不认真
	zgbrz_dFrame = dFrames[ dFrames["CODE"] != "ZG-5"]
	zgbrz_info = []
	for i,row in zgbrz_dFrame.iterrows():
		zgbrz_info.append(_serializeProblemZG(row))

	# 整改不把关
	zgbbg_dFrame = zg_5_dFrame
	zgbbg_info = []
	for i,row in zgbbg_dFrame.iterrows():
		zgbbg_info.append(_serializeProblemZG(row))

	# 考核质量分析
	kh_1_dFrame = dFrames[dFrames["CODE"] == 'KH-1']
	kh_2_dFrame = dFrames[dFrames["CODE"] == 'KH-2']

	# 路局评价
	luju_evaluate = dFrames[dFrames["CHECK_TYPE"] == 1]
	# 站段评价
	station_evaluate = dFrames[dFrames["CHECK_TYPE"] == 2]

	kh_1_info = []
	for i, row in kh_1_dFrame.iterrows():
		kh_1_info.append(_serializeProblemZG(row))

	kh_2_info = []
	for i, row in kh_2_dFrame.iterrows():
		kh_2_info.append(_serializeProblemZG(row))

	return {
		"gclsbl": {
			"item_count": gclsbl_dFrame.shape[0],
			"people_count": gclsbl_dFrame.groupby("id_card").size().shape[0],
			"list": gclsbl_info
		},
		"jcdcbg": {
			"item_count": jcdcbg_dFrame.shape[0],
			"people_count": jcdcbg_dFrame.groupby("id_card").size().shape[0],
			"list": jcdcbg_info
		},
		"zg": {
			"total": {
				"item_count": zg_dFrame.shape[0],
				"people_count": zg_dFrame.groupby("id_card").size().shape[0],
			},
			"zg_1": {
				"item_count": zg_1_dFrame.shape[0],
				"people_count": zg_1_dFrame.groupby("id_card").size().shape[0],
			},
			"zg_2": {
				"item_count": zg_2_dFrame.shape[0],
				"people_count": zg_2_dFrame.groupby("id_card").size().shape[0],
			},
			"zg_3": {
				"item_count": zg_3_dFrame.shape[0],
				"people_count": zg_3_dFrame.groupby("id_card").size().shape[0],
			},
			"zg_4": {
				"item_count": zg_4_dFrame.shape[0],
				"people_count": zg_4_dFrame.groupby("id_card").size().shape[0],
			},
			"zg_5": {
				"item_count": zg_5_dFrame.shape[0],
				"people_count": zg_5_dFrame.groupby("id_card").size().shape[0],
			}
		},
		"zgbcd": {
			"item_count": zgbcd_dFrame.shape[0],
			"people_count": zgbcd_dFrame.groupby("id_card").size().shape[0],
			"list": jcdcbg_info
		},
		"zgbrz": {
			"item_count": zgbrz_dFrame.shape[0],
			"people_count": zgbrz_dFrame.groupby("id_card").size().shape[0],
			"list": zgbrz_info
		},
		"zgbbg": {
			"item_count": zgbbg_dFrame.shape[0],
			"people_count": zgbbg_dFrame.groupby("id_card").size().shape[0],
			"list": zgbbg_info
		},
		"kh_1": {
			"item_count": kh_1_dFrame.shape[0],
			"people_count": kh_1_dFrame.groupby("id_card").size().shape[0],
			"list": kh_1_info
		},
		"kh_2": {
			"item_count": kh_2_dFrame.shape[0],
			"people_count": kh_2_dFrame.groupby("id_card").size().shape[0],
			"list": kh_2_info
		},
		"luju_evaluate": {
			"item_count": luju_evaluate.shape[0],
			"people_count": luju_evaluate.groupby("id_card").size().shape[0],
		},
		"station_evaluate": {
			"item_count": station_evaluate.shape[0],
			"people_count": station_evaluate.groupby("id_card").size().shape[0],
		}
	}

# 各级干部检查质量分析 第三大部分 第二部分
def get_cadre_check_quality_analyse(start_date, end_date):
	dFrames = pd_query("""
					SELECT
						a.PK_ID,
						b.ALL_NAME AS responsible_department,
						c.IDENTITY,
						a.RESPONSIBE_PERSON_NAME AS person_name,
						a.RESPONSIBE_ID_CARD AS id_card,
						a.GRADATION,
						a.FK_PERSON_GRADATION_RATIO_ID,
						a.YEAR,
						a.MONTH,
						a.CREATE_TIME,
						a.EVALUATE_WAY,
						a.CODE_ADDITION,
						a.CODE,
						a.EVALUATE_CONTENT,
						a.SCORE,
						a.CHECK_TYPE
					FROM
						t_check_evaluate_info AS a
						INNER JOIN t_department AS b ON a.RESPONSIBE_DEPARTMENT_ID = b.DEPARTMENT_ID
						INNER JOIN t_person AS c ON a.RESPONSIBE_ID_CARD = c.ID_CARD
					WHERE
						a.CREATE_TIME BETWEEN "{0}"
						AND "{1} 23:59:59"
						AND c.IS_DELETE = 0
	""".format(start_date, end_date))
	# 筛选干部，去重
	dFrames = dFrames.drop_duplicates(subset=['PK_ID'])
	del dFrames['PK_ID']
	dFrames = dFrames[dFrames['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
	# 职责履行有差距
	reponsible_difference_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-3") | (dFrames["CODE_ADDITION"] == "ZL-6-4")]
	zzlxycj_info = []
	for i,row in reponsible_difference_dFrame.iterrows():
		zzlxycj_info.append(_serializeProblemZG(row))

	# 风险防控有差距
	risk_difference_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZL-1-1")
								    |(dFrames["CODE_ADDITION"] == "ZD-8-3")
									|(dFrames["CODE_ADDITION"] == "ZL-6-4")]
	fxfkycj_info = []
	for i,row in risk_difference_dFrame.iterrows():
		fxfkycj_info.append(_serializeProblemZG(row))

	# 设备管控有差距
	equipment_difference_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZL-1-1")
									|(dFrames["CODE_ADDITION"] == "ZL-3-2")]
	sbgkycj_info = []
	for i,row in equipment_difference_dFrame.iterrows():
		sbgkycj_info.append(_serializeProblemZG(row))

	# 针对性检查有差距
	pertinence_difference_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZL-1-1")
									|(dFrames["CODE_ADDITION"] == "ZL-7-1")]
	zdxjcycj_info = []
	for i,row in pertinence_difference_dFrame.iterrows():
		zdxjcycj_info.append(_serializeProblemZG(row))

	# 关键查处有差距
	crux_difference_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-3")
									|(dFrames["CODE_ADDITION"] == "ZL-7-1")]
	gjccycj_info = []
	for i,row in crux_difference_dFrame.iterrows():
		gjccycj_info.append(_serializeProblemZG(row))

	# 作业现场管控有差距
	work_place_difference_dFrame =  dFrames[(dFrames["CODE_ADDITION"] == "ZL-4-1")
									|(dFrames["CODE_ADDITION"] == "ZL-4-2")]
	zyxcgkycj_info = []
	for i,row in work_place_difference_dFrame.iterrows():
		zyxcgkycj_info.append(_serializeProblemZG(row))

	# 专业管理能力欠缺
	major_ability_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-1")
								  |(dFrames["CODE_ADDITION"] == "ZD-5-2")
								  |(dFrames["CODE_ADDITION"] == "ZD-5-3")]
	zyglnlqq_info = []
	for i,row in major_ability_dFrame.iterrows():
		zyglnlqq_info.append(_serializeProblemZG(row))

	# 专业管理松懈
	major_relax_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-1")
								|(dFrames["CODE_ADDITION"] == "ZD-5-2")
								|(dFrames["CODE_ADDITION"] == "ZD-5-3")]
	zyglsx_info = []
	for i,row in major_relax_dFrame.iterrows():
		zyglsx_info.append(_serializeProblemZG(row))

	# 专业检查指导不力
	major_check_dFrame = dFrames[(dFrames["CODE_ADDITION"] == "ZD-5-1")
								|(dFrames["CODE_ADDITION"] == "ZD-5-2")
								|(dFrames["CODE_ADDITION"] == "ZD-5-3")]
	zyjczdbl_info = []
	for i,row in major_check_dFrame.iterrows():
		zyjczdbl_info.append(_serializeProblemZG(row))

	# 音视频调阅缺乏针对性、有效性
	audio_video1 = dFrames[dFrames["CODE_ADDITION"] == "ZL-5-2"]
	yspdyqfzdx_info = []
	for i,row in audio_video1.iterrows():
		yspdyqfzdx_info.append(_serializeProblemZG(row))

	# 音视频调阅分析不认真
	audio_video2 = dFrames[dFrames["CODE_ADDITION"] == "ZL-5-1"]
	yspdyfxbrz_info = []
	for i,row in audio_video2.iterrows():
		yspdyfxbrz_info.append(_serializeProblemZG(row))

	# 音视频调阅工作开展不力、插件音视频管理薄弱
	office_info, workshop_info = get_fxs_check_result(start_date, end_date)
	anjian, station = analyse_center_stastics(start_date, end_date)
	return {
		# 职责履行有差距
		"zzlxycj": {
			"item_count": len(zzlxycj_info),
			"people_count": reponsible_difference_dFrame.groupby("id_card").size().shape[0],
			"list": zzlxycj_info,
			"ZD_5_3": {
				"item_count": reponsible_difference_dFrame[
					reponsible_difference_dFrame["CODE_ADDITION"] == 'ZD-5-3'].shape[0],
				"people_count": reponsible_difference_dFrame[
					reponsible_difference_dFrame["CODE_ADDITION"] == 'ZD-5-3'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_6_4": {
				"item_count": reponsible_difference_dFrame[
					reponsible_difference_dFrame["CODE_ADDITION"] == 'ZL-6-4'].shape[0],
				"people_count": reponsible_difference_dFrame[
					reponsible_difference_dFrame["CODE_ADDITION"] == 'ZL-6-4'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 风险防控有差距
		"fxfkycj": {
			"item_count": len(fxfkycj_info),
			"people_count": risk_difference_dFrame.groupby("id_card").size().shape[0],
			"list": fxfkycj_info,
			"ZL_6_4": {
				"item_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZL-6-4'].shape[0],
				"people_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZL-6-4'].groupby(
						"id_card").size().shape[0]
			},
			"ZD_8_3": {
				"item_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZD-8-3'].shape[0],
				"people_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZD-8-3'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_1_1": {
				"item_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].shape[0],
				"people_count": risk_difference_dFrame[
					risk_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 设备管控有差距
		"sbgkycj": {
			"item_count": len(sbgkycj_info),
			"people_count": equipment_difference_dFrame.groupby("id_card").size().shape[0],
			"list": sbgkycj_info,
			"ZL_3_2": {
				"item_count": equipment_difference_dFrame[
					equipment_difference_dFrame["CODE_ADDITION"] == 'ZL-3-2'].shape[0],
				"people_count": equipment_difference_dFrame[
					equipment_difference_dFrame["CODE_ADDITION"] == 'ZL-3-2'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_1_1": {
				"item_count": equipment_difference_dFrame[
					equipment_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].shape[0],
				"people_count": equipment_difference_dFrame[
					equipment_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 针对性检查有差距
		"zdxjcycj": {
			"item_count": len(zdxjcycj_info),
			"people_count": pertinence_difference_dFrame.groupby("id_card").size().shape[0],
			"list": zdxjcycj_info,
			"ZL_1_1": {
				"item_count": pertinence_difference_dFrame[
					pertinence_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].shape[0],
				"people_count": pertinence_difference_dFrame[
					pertinence_difference_dFrame["CODE_ADDITION"] == 'ZL-1-1'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_7_1": {
				"item_count": pertinence_difference_dFrame[
					pertinence_difference_dFrame["CODE_ADDITION"] == 'ZL-7-1'].shape[0],
				"people_count": pertinence_difference_dFrame[
					pertinence_difference_dFrame["CODE_ADDITION"] == 'ZL-7-1'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 关键查处有差距
		"gjccycj": {
			"item_count": len(gjccycj_info),
			"people_count": crux_difference_dFrame.groupby("id_card").size().shape[0],
			"list": gjccycj_info,
			"ZD_5_3": {
				"item_count": crux_difference_dFrame[
					crux_difference_dFrame["CODE_ADDITION"] == 'ZD-5-3'].shape[0],
				"people_count": crux_difference_dFrame[
					crux_difference_dFrame["CODE_ADDITION"] == 'ZD-5-3'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_7_1": {
				"item_count": crux_difference_dFrame[
					crux_difference_dFrame["CODE_ADDITION"] == 'ZL-7-1'].shape[0],
				"people_count": crux_difference_dFrame[
					crux_difference_dFrame["CODE_ADDITION"] == 'ZL-7-1'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 作业现场管控有差距
		"zyxcgkycj": {
			"item_count": len(zyxcgkycj_info),
			"people_count": work_place_difference_dFrame.groupby("id_card").size().shape[0],
			"list": zyxcgkycj_info,
			"ZL_4_1": {
				"item_count": work_place_difference_dFrame[
					work_place_difference_dFrame["CODE_ADDITION"] == 'ZL-4-1'].shape[0],
				"people_count": work_place_difference_dFrame[
					work_place_difference_dFrame["CODE_ADDITION"] == 'ZL-4-1'].groupby(
						"id_card").size().shape[0]
			},
			"ZL_4_2": {
				"item_count": work_place_difference_dFrame[
					work_place_difference_dFrame["CODE_ADDITION"] == 'ZL-4-2'].shape[0],
				"people_count": work_place_difference_dFrame[
					work_place_difference_dFrame["CODE_ADDITION"] == 'ZL-4-2'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 专业管理能力欠缺
		"zyglnlqq": {
			"item_count": len(zyglnlqq_info),
			"people_count": major_ability_dFrame.groupby("id_card").size().shape[0],
			"list": zyglnlqq_info,
			"ZD_5_1": {
				"item_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-1'].shape[0],
				"people_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-1'].groupby(
						"id_card").size().shape[0]
			},
			"ZD_5_2": {
				"item_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-2'].shape[0],
				"people_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-2'].groupby(
						"id_card").size().shape[0]
			},
			"ZD_5_3": {
				"item_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-3'].shape[0],
				"people_count": major_ability_dFrame[
					major_ability_dFrame["CODE_ADDITION"] == 'ZD-5-3'].groupby(
						"id_card").size().shape[0]
			}
		},
		# 音视频调阅缺乏针对性、有效性
		"yspdyqfzdx": {
			"item_count": len(yspdyqfzdx_info),
			"people_count": audio_video1.groupby("id_card").size().shape[0],
			"list": yspdyqfzdx_info
		},
		# 音视频调阅分析不认真
		"yspdyfxbrz": {
			"item_count": len(yspdyfxbrz_info),
			"people_count": audio_video2.groupby("id_card").size().shape[0],
			"list": yspdyfxbrz_info
		},
		# 音视频调阅工作开展不力
		"yspdygzkzbl": office_info,
		# 车间音视频管理薄弱
		"cjyspglbr": workshop_info,
		"anjian": anjian,
		"station": station,
	}


# 分析室查处 责任部门为职能科室一级和车间一级的E类和F类问题 4.3、 4.4、 5.1、 5.2、
def get_fxs_check_result(start_date, end_date):
	dFrame = pd_query("""
					SELECT
						a.PK_ID,
						a.FK_CHECK_INFO_ID,
						LEFT(a.RISK_NAMES, 2) as system,
						f.NAME as responsible_station,
						e.NAME as responsible_department,
						d.RESPONSIBILITY_PERSON_NAMES,
						a.LEVEL,
						a.DESCRIPTION,
						e.TYPE as department_type
					FROM
						t_check_problem AS a
						INNER JOIN t_check_info as b ON a.FK_CHECK_INFO_ID = b.PK_ID
						INNER JOIN t_check_info_and_person AS c ON a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
						INNER JOIN t_check_problem_and_responsible_department as d ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
						INNER JOIN t_department as e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
						INNER JOIN t_department as f ON e.TYPE3 = f.DEPARTMENT_ID
					WHERE
						a.SUBMIT_TIME BETWEEN "{0}"
						AND "{1} 23:59:59"
						AND c.ALL_NAME LIKE "%%安全监察大队%%"
						AND a.FK_CHECK_ITEM_ID = 17699
						AND e.TYPE in (7,8)
	""".format(start_date, end_date))

	if not dFrame.empty:
		dFrame = dFrame.drop_duplicates(["FK_CHECK_INFO_ID", "PK_ID"])
	office_info = []
	# 查处的责任部门为职能科室一级
	for i,row in dFrame[dFrame["department_type"] == 7].iterrows():
		office_info.append({
			"system": row["system"],
			"responsible_station": row["responsible_station"],
			"responsible_department": row["responsible_department"],
			"responsible_names": row["RESPONSIBILITY_PERSON_NAMES"],
			"problem_type": row["LEVEL"],
			"content": row["DESCRIPTION"]
		})
	workshop_info = []
	# 查处的责任部门为职能车间一级
	for i,row in dFrame[dFrame["department_type"] == 8].iterrows():
		workshop_info.append({
			"system": row["system"],
			"responsible_station": row["responsible_station"],
			"responsible_department": row["responsible_department"],
			"responsible_names": row["RESPONSIBILITY_PERSON_NAMES"],
			"problem_type": row["LEVEL"],
			"content": row["DESCRIPTION"]
		})
	return (office_info, workshop_info)

# 干部履职分析评价质量方面 2.5.1： 安全监察大队专业分析室 、 2.5.2： 站段安全分析中心
def analyse_center_stastics(start_date, end_date):
	resumption_dFrame = pd_query("""
					SELECT
						a.PK_ID,
						a.RESPONSIBE_PERSON_NAME as person_name,
						a.GRADATION,
						a.YEAR,
						a.MONTH,
						a.EVALUATE_WAY,
						a.CODE_ADDITION,
						a.SCORE,
						a.EVALUATE_CONTENT,
						b.NAME,
						d.NAME as responsible_department,
						b.ALL_NAME,
						b.BUSINESS_CLASSIFY,
						a.CREATE_TIME
					FROM
						t_check_evaluate_info AS a
						INNER JOIN t_department as b ON a.RESPONSIBE_DEPARTMENT_ID = b.DEPARTMENT_ID
						INNER JOIN t_person as c ON a.RESPONSIBE_ID_CARD = c.ID_CARD
						INNER JOIN t_department as d ON b.TYPE3 = d.DEPARTMENT_ID
					WHERE
						a.CREATE_TIME BETWEEN "{0}"
						AND "{1} 23:59:59"
						AND c.IDENTITY = "干部"
						AND b.BUSINESS_CLASSIFY LIKE "%%分析中心%%"
	""".format(start_date, end_date))
	resumption_dFrame = resumption_dFrame.drop_duplicates(subset=['PK_ID'])
	del resumption_dFrame['PK_ID']
	# 安全监察大队专业分析室履职评价情况统计表
	anjian_df = resumption_dFrame[resumption_dFrame["ALL_NAME"].str.contains("安全监察大队")]
	anjian_resumption_info = []
	for i,row in anjian_df.iterrows():
		anjian_resumption_info.append(_serializeProblemZG(row))

	station_df = resumption_dFrame[resumption_dFrame["ALL_NAME"].str.contains(r"[^(安全监察大队)]")]
	station_resumption_info = []
	for i,row in station_df.iterrows():
		station_resumption_info.append(_serializeProblemZG(row))

	station_all_analyse_evaluate = []
	station_gp = station_df.groupby("responsible_department").size()
	for station,count in station_gp.iteritems():
		# 各个站段安全分析中心被评价次数统计
		station_all_analyse_evaluate.append({
			"name": station,
			"count": count
		})

	anjian_df["responsible_department"] = anjian_df["responsible_department"].str.slice(7)
	anjian_gp = anjian_df.groupby("responsible_department").size()
	anjian_all_analyse_evaluate = []
	for station,count in anjian_gp.iteritems():
		# 安全监察大队 各个分析室被评价次数
		anjian_all_analyse_evaluate.append({
			"name": station,
			"count": count,
		})

	# 安全监察大队和站段安全分析中心：存在问题统计表
	dFrame2 = pd_query("""
					SELECT
						a.PK_ID,
						c.BUSINESS_CLASSIFY,
						d.NAME as major,
						f.NAME,
						a.RESPONSIBILITY_DEPARTMENT_NAME,
						e.CHECK_WAY,
						a.LEVEL,
						a.DESCRIPTION
					FROM
						t_check_problem AS a
						INNER JOIN t_check_problem_and_responsible_department as b ON a.PK_ID = b.FK_CHECK_PROBLEM_ID
						INNER JOIN t_department as c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
						INNER JOIN t_department as d ON c.TYPE2 = d.DEPARTMENT_ID
						INNER JOIN t_check_info as e ON e.PK_ID = a.FK_CHECK_INFO_ID
						INNER JOIN t_department as f ON c.TYPE3 = f.DEPARTMENT_ID
					WHERE
						a.SUBMIT_TIME BETWEEN "{0}"
						AND "{1} 23:59:59"
						AND c.BUSINESS_CLASSIFY LIKE "%%分析中心%%"
	""".format(start_date, end_date))
	dFrame2 = dFrame2.drop_duplicates(subset=['PK_ID'])
	del dFrame2['PK_ID']
	anjian_problem = []
	anjian_problem_df = dFrame2[dFrame2["RESPONSIBILITY_DEPARTMENT_NAME"].str.contains("安全监察大队")]

	anjian_problem_df["RESPONSIBILITY_DEPARTMENT_NAME"] = anjian_problem_df["RESPONSIBILITY_DEPARTMENT_NAME"].str.slice(7)
	for i,row in anjian_problem_df.iterrows():
		anjian_problem.append({
			"system": row["RESPONSIBILITY_DEPARTMENT_NAME"],
			"check_way": PROBLEM_CHECK_WAY[row["CHECK_WAY"]],
			"level": row["LEVEL"],
			"content": row["DESCRIPTION"]
		})
	anjian_problem_gp_list = []
	for station, count in anjian_problem_df.groupby("RESPONSIBILITY_DEPARTMENT_NAME").size().iteritems():
		anjian_problem_gp_list.append({
			"name": station,
			"count": count,
		})

	station_problem = []
	station_problem_df = dFrame2[dFrame2["RESPONSIBILITY_DEPARTMENT_NAME"].str.contains(r"[^(安全监察大队)]")]
	for i,row in station_problem_df.iterrows():
		station_problem.append(_serializeProblemCheck(row))
	station_problem_gp_list = []
	for station, count in station_problem_df.groupby("RESPONSIBILITY_DEPARTMENT_NAME").size().iteritems():
		station_problem_gp_list.append({
			"name": station,
			"count": count,
		})
	return ({
			"evaluate_count": len(anjian_resumption_info),
			"list": anjian_resumption_info,
			"evaluated": anjian_all_analyse_evaluate,
			"problem_list": anjian_problem,
			"problem_gp_info": anjian_problem_gp_list,
			},
			{
			"evaluate_count": len(station_resumption_info),
			"list": station_resumption_info,
			"evaluated": station_all_analyse_evaluate,
			"problem_list": station_problem,
			"problem_gp_info": station_problem_gp_list,
			})


def get_yearCombineMonth_list(start_time, end_time):
	stime = datetime.strptime(start_time, '%Y-%m-%d')
	etime = datetime.strptime(end_time, "%Y-%m-%d")
	months = (etime.year - stime.year)*12 + etime.month - stime.month
	month_range = [ '%s-%s'%(stime.year + mon//12, mon%12+1) for mon in range(stime.month-1,stime.month + months)]
	return month_range

EVALUATE_WAY = {
	0: "自动评价",
	1: "逐条评价",
	2: "定期评价",
	3: "阶段评价"
}
PROBLEM_CHECK_WAY = {
	1: "现场检查",
	2: "填乘检查",
	3: "监控调阅检查",
	4: "复查调阅",
	5: "监控检测转录",
	6: "职工检查"
}


def _serializeProblemCheck(item):
	return {
		"system": item["major"],
		"responsible_department": item["RESPONSIBILITY_DEPARTMENT_NAME"],
		"check_way": PROBLEM_CHECK_WAY[item["CHECK_WAY"]],
		"level": item["LEVEL"],
		"content": item["DESCRIPTION"]
	}


def _serializeProblemZG(item):
	ym = "{0}/{1}".format(item.YEAR, item.MONTH)
	return {
		"responsible_department": item["responsible_department"],
		"person_name": item["person_name"],
		"gradation": item["GRADATION"],
		"belong_ym": ym,
		"evaluate_date": item["CREATE_TIME"],
		"evaluate_way": EVALUATE_WAY[item["EVALUATE_WAY"]],
		"code_addition": item["CODE_ADDITION"],
		"content": item["EVALUATE_CONTENT"],
		"score": item["SCORE"]
	}


def _get_warning_notification_task(start_date, end_date):
	"""
	获取验收并撤销安全警告预警，验收并延期安全警告预警
	Returns:

	"""
	data = pd_query(WARNING_NOTIFICATION_AND_TASK_PROCESS_SQL.format(start_date, end_date))
	# 各种通知书id
	warnning_ids = set(data['PK_ID'])
	nums = [0, 0]
	for ids in warnning_ids:
		tmp_df = data[
			data['PK_ID'] == ids
		]
		max_task_id = max(tmp_df['TASK_PROCESS_ID'])
		tmp_df = tmp_df[
			tmp_df['TASK_PROCESS_ID'] == max_task_id
		]
		task_time = tmp_df['CREATE_TIME'].values[0]
		task_name = tmp_df['TASK_NAME'].values[0]
		task_result = tmp_df['RESULT'].values[0]
		# 如果操作类型为领导核实，操作结果为通过，且创建在当前月份，该条记为撤销
		# 如果操作类型为领导核实，操作结果为延长期限，且创建在当前月份，该条记为延期
		if end_date[:7] == str(task_time)[:7] and task_name == '领导核实':
			if task_result == '通过':
				nums[0] +=1
			elif task_result == '延长期限':
				nums[1] +=1
	return nums
