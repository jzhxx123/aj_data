import random
from app.report.util import save_picture_route
import matplotlib.pyplot as plt
from matplotlib.font_manager import FontManager, FontProperties
from app.data.font import FONT_STYLE_FILE
import numpy as np

plt.rcParams['font.sans-serif'] = ['SimHei']


def random_color():
    """
    生成随机16进制颜色
    :return:
    """
    color_arr = ['1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F']
    color = ''
    for i in range(6):
        color += color_arr[random.randint(0, 14)]
    return '#' + color


def line_picture(x_data, y_data, x_label, title, report_name, major, interval):
    """
    折线图
    :param x_data: x轴对应数据
    :param y_data: y轴对应数据 （列表）
    :param x_label: 标签 （列表）
    :param title: 标题
    :param report_name：文件名
    :return: 图片所在路径
    """
    fig = plt.figure(figsize=(8, 7), edgecolor='black', frameon=True)
    ax = fig.add_subplot(111)
    for i in range(len(y_data)):
        ax.plot(range(len(x_data)), y_data[i], marker='o', color=random_color(), label=x_label[i])
        for a, b in zip(range(len(x_data)), y_data[i]):
            ax.text(a, b + 0.05, b, ha='center', va='bottom', fontsize=12)
    plt.xticks(range(len(x_data)), x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(loc=1, prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def line_and_bar_picture(x_data, line_data, bar_data, title, report_name, major, interval):
    """
    折现/柱状混合图
    :param x_data: x轴数据
    :param line_data: y轴折线图数据
    :param bar_data: y轴柱状图数据
    :param title: 标题
    :param report_name: 文件名
    :return: 返回文件路径
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    for i in line_data:
        ax.plot(range(len(x_data)), i, marker='o', color=random_color())
        for a, b in zip(range(len(x_data)), i):
            ax.text(a, b + 0.05, b, ha='center', va='bottom', fontsize=12)
    for i in bar_data:
        ax.bar(range(len(x_data)), i, alpha=0.2, color=random_color())
    plt.xticks(range(len(x_data)), x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def barh_picture(y_data, x_data, label_name, title, report_name, major, interval):
    """
    横向柱状图
    :param y_data: y轴数据
    :param x_data: x轴数据
    :param title: 标题
    :param label_name: 标签名
    :param report_name: 文件名
    :return: 返回文件路径
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    y_pos = np.arange(len(y_data))
    for i in range(len(x_data)):
        ax.barh(y_pos, x_data[i], color=random_color(), ecolor='black', height=0.3, label=label_name[i])
        y_pos = y_pos + 0.3
    plt.yticks(range(len(y_data)), y_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(loc=1, prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def hollow_pie_picture(y_data, x_data, title, report_name, major, interval):
    """
    空心饼图
    :param y_data: y轴数据
    :param x_data: x轴数据
    :param title: 标题
    :param report_name: 文件名
    :return: 返回文件路径
    """
    x_0 = [1, 0, 0, 0, 0]  # 用于显示空心
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    color = []
    for i in range(len(y_data)):
        color.append(random_color())
    ax.pie(x_data, radius=1.0, pctdistance=0.8, labels=y_data, colors=color, startangle=90, autopct='%1.1f%%')
    ax.pie(x_0, radius=0.6, colors='w')
    plt.legend(loc=1, fontsize=10, prop=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def bar_picture(y_data, x_data, label_name, title, report_name, major, interval):
    """
    垂直柱状图
    :param y_data:
    :param x_data:
    :param label_name:
    :param title:
    :param report_name:
    :param major:
    :param interval:
    :return:
    """
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    y_pos = np.arange(len(x_data))
    for i in range(len(y_data)):
        if i != 0:
            ax.bar(y_pos, y_data[i], color=random_color(), ecolor='black', width=0.1, label=label_name[i])
        else:
            ax.bar(y_pos, y_data[i], color=random_color(), ecolor='black', width=0.1)
        for a, b in zip(y_pos, y_data[i]):
            ax.text(a, b + 0.05, b, ha='center', va='bottom', fontsize=12)
        y_pos = y_pos + 0.3
    plt.xticks(range(len(x_data)), x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(loc=1, prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def bar_pictures(y_data, x_data, label_name, title, report_name, major, interval):
    """
    并列柱状图
    :param y_data:
    :param x_data:
    :param label_name:
    :param title:
    :param report_name:
    :param major:
    :param interval:
    :return:
    """
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(111)
    y_pos = np.arange(len(x_data))
    ax.bar(y_pos, y_data[0], color=random_color(), ecolor='black', width=0.1, label=label_name[0])
    ax.bar(y_pos+0.1, y_data[1], color=random_color(), ecolor='black', width=0.1, label=label_name[1])
    for a, b in zip(y_pos, y_data[0]):
        ax.text(a, b + 0.05, b, ha='center', va='bottom', fontsize=12)
    plt.xticks(range(len(x_data)), x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(loc=1, prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def bars_picture(y_data, x_data, label_name, title, report_name, major, interval):
    """
    垂直柱状图
    :param y_data:
    :param x_data:
    :param label_name:
    :param title:
    :param report_name:
    :param major:
    :param interval:
    :return:
    """
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    y_pos = np.arange(len(x_data))
    for i in range(len(y_data)):
        ax.bar(y_pos, y_data[i], color=random_color(), ecolor='black', width=0.1, label=label_name[i])
        for a, b in zip(y_pos, y_data[i]):
            ax.text(a, b + 0.05, b, ha='center', va='bottom', fontsize=12)
        y_pos = y_pos + 0.3
    plt.xticks(range(len(x_data)), x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(loc=1, prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def bar_picture1(y_data, x_data, label_name, title, report_name, major, interval):
    """
    垂直柱状图/叠加
    :param y_data:
    :param x_data:
    :param label_name:
    :param title:
    :param report_name:
    :param major:
    :param interval:
    :return:
    """
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    y_pos = np.arange(len(x_data))
    for i in range(len(y_data)):
        ax.bar(y_pos, y_data[i], 0.15, color=random_color(), ecolor='black', label=label_name[i])
        for a, b in zip(y_pos, y_data[i]):
            ax.text(a, b + 0.15, b, ha='center', va='bottom', fontsize=12)
        y_pos = y_pos + 0.15
    plt.xticks(np.arange(len(x_data)) + 0.15, x_data, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    plt.legend(prop=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def pie_picture(data, labels, title, report_name, major, interval):
    """
    饼图
    :param data:
    :param labels:
    :param title:
    :param report_name:
    :param major:
    :param interval:
    :return:
    """
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    ax.pie(data, labels=labels, autopct='%1.1f%%', shadow=False, startangle=150)
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route


def hentail_pie_picture(data1, data2, name1, name2, title, report_name, major, interval):
    x_0 = [1, 0, 0, 0, 0]  # 用于显示空心
    fig = plt.figure(figsize=(8, 8))
    ax = fig.add_subplot(111)
    color1 = []
    color2 = []
    for i in range(len(name1)):
        color1.append(random_color())
    for i in range(len(name2)):
        color2.append(random_color())
    ax.pie(data1, radius=1.0, pctdistance=0.8, labels=name1, colors=color1, startangle=90, autopct='%1.1f%%')
    ax.pie(x_0, radius=0.6, colors='w')
    ax.pie(data2, radius=0.5, pctdistance=0.8, labels=name2, colors=color2, startangle=90, autopct='%1.1f%%')
    plt.legend(loc=1, fontsize=10, prop=FontProperties(fname=FONT_STYLE_FILE))
    plt.title(title, fontproperties=FontProperties(fname=FONT_STYLE_FILE))
    route = save_picture_route(report_name + title + '.png', major, interval)
    plt.savefig(route)
    return route
