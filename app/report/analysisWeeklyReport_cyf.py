#! /usr/bin/env python3
# -*- coding: utf-8 -*-
from app import mongo
import pandas as pd
from datetime import datetime
from flask import jsonify, current_app
from app.data.util import pd_query

DUTY_LEVEL = {
    1: "全部责任",
    2: "主要责任",
    3: "同等主要责任",
    4: "重要责任",
    5: "同等重要责任",
    6: "次要责任",
    7: "同等次要责任",
    8: "追究主要责任",
    9: "追究重要责任",
    10: "追究次要责任",
    11: "非责任"
}


# 安全基本情况
def get_safety_basic_situation(start_date, end_date):
    dFrame = pd_query("""
                    SELECT
                        a.PROFESSION
                        ,a.CATEGORY
                        ,a.MAIN_TYPE
                        ,a.DETAIL_TYPE
                        ,a.OVERVIEW
                        ,a.STATUS
                        ,a.ROAD_OUT_DEAD
                        ,a.ROAD_OUT_SERIOUS_INJURY
                        ,a.CODE
                        ,b.DEPARTMENT_NAME
                        ,b.RESPONSIBILITY_IDENTIFIED
                        ,c.NAME
                    FROM
                        t_safety_produce_info AS a
                        INNER JOIN t_safety_produce_info_responsibility_unit as b
                        ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
                        INNER JOIN t_safety_produce_info_type_config as c
                        ON a.CATEGORY = c.CATEGORY AND a.CODE = c.CODE
                    WHERE
                        a.OCCURRENCE_TIME BETWEEN Date( "{0}" )
                        AND Date( "{1}" )
    """.format(start_date, end_date))

    # 行车安全
    xc_aq_dFrame = dFrame[dFrame["DETAIL_TYPE"] == 1]
    xc_aq_dFrame["PROFESSION"].fillna('待定', inplace=True)
    xc_aq_gp = xc_aq_dFrame.groupby("PROFESSION").size()
    xc_detail = []
    for major, count in xc_aq_gp.iteritems():
        info = list(xc_aq_dFrame[xc_aq_dFrame["PROFESSION"] == major]["OVERVIEW"])
        if major is None:
            major = "待定"
        xc_detail.append({
            "major": major,
            "count": int(count),
            "info": info
        })

    # 路外安全
    lw_aq_dFrame = dFrame[dFrame["DETAIL_TYPE"] == 3]
    # 内部防控考核
    lw_aq_gp_dFrame = lw_aq_dFrame.groupby(["DEPARTMENT_NAME", "RESPONSIBILITY_IDENTIFIED"]).size().reset_index()
    fk_kh_info = []
    for i,row in lw_aq_gp_dFrame.iterrows():
        fk_kh_info.append({
            "station": row["DEPARTMENT_NAME"],
            "duty_level": DUTY_LEVEL[row["RESPONSIBILITY_IDENTIFIED"]],
            "count": int(row[0])
        })
    # 设备故障统计信息
    sb_gz_dFrame = dFrame[dFrame["MAIN_TYPE"] == 2]
    sb_gz_gp = sb_gz_dFrame.groupby("NAME").size()
    sb_gz_info = []
    for category_name,count in sb_gz_gp.iteritems():
        sb_gz_info.append({
            "cagetory_name": category_name,
            "count": int(count)
        })
    return {
        "happen" : {
            "shigu": dFrame[dFrame["MAIN_TYPE"] == 1].shape[0],
            "guzhang": dFrame[dFrame["MAIN_TYPE"] == 2].shape[0],
        },
        "xingche_anquan": {
            "count": xc_aq_dFrame.shape[0],
            "detail": xc_detail
        },
        "renshen_anquan": {
            "count":  dFrame[dFrame["DETAIL_TYPE"] == 2].shape[0],
        },
        "luwai_anquan": {
            "shigu": lw_aq_dFrame.shape[0],
            "siwang": int(lw_aq_dFrame["ROAD_OUT_DEAD"].sum()),
            "zhongshang": int(lw_aq_dFrame["ROAD_OUT_SERIOUS_INJURY"].sum()),
            "fkkh": fk_kh_info
        },
        "shebei_guzhang": {
            "count": sb_gz_dFrame.shape[0],
            "gp_info": sb_gz_info
        }
    }