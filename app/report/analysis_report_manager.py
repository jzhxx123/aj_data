"""
安全分析报表的管理中心模块
--- 功能
1. 负责报表任务的调度
2. 提供安全报表相关的生成配置

Author: 卢炯林
Date： 2019-08-20
"""

# 报告生成这的登记册
import openpyxl
from dateutil.relativedelta import relativedelta
from flask import current_app
import os
from datetime import datetime
from app.utils.common_func import get_department_name_by_dpid
from abc import ABC, abstractmethod
from app import mongo
from docxtpl import DocxTemplate

reporter_book = {

}

# 报表（时间）类型，年报、月报、周报、日报
REPORT_INTERVAL_TYPES = {'annual', 'monthly', 'weekly', 'daily', 'quarterly', 'semiannual'}

# 报表的包含的专业
REPORT_MAJORS = {"车务", '供电', "机务", "车辆", "工务", "电务", "客运", "安监", "集团", "货运"}

# 报表的层级类型，专业级/站段级
REPORT_HIERARCHY_TYPES = {'MAJOR', 'STATION', 'GROUP'}

# 不同专业的目录的名称, 用于将中文转为字母，以便目录路径避免中文
MAJOR_PATH_DICT = {
    '车务': 'chewu',
    '供电': 'gongdian',
    '机务': 'jiwu',
    '车辆': 'cheliang',
    '工务': 'gongwu',
    '电务': 'dianwu',
    '客运': 'keyun',
    '工电': 'gongdian',
    '安监': 'anjian',
    '集团': 'group',
    '货运': 'huoyun'
}


def get_report_path(interval_type, major=None):
    """
    返回报告存放的路径，以便统一管理
    :param interval_type:
    :param major: 如果Major为空，默认为：集团
    :return:
    """
    interval_type = interval_type.lower()
    if major is None:
        major = '集团'
    if interval_type not in REPORT_INTERVAL_TYPES \
            or major not in REPORT_MAJORS:
        raise Exception('参数有误')
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path, current_app.config.get('DOWNLOAD_FILE_ANALYSIS'),
                            MAJOR_PATH_DICT.get(major), interval_type)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    return dir_path


def restore_numpy_value(data):
    """
    将数据中包含numpy的float，int的值，还原为python原生的float，int
    :param data:
    :return:
    """
    if isinstance(data, dict):
        for key, value in data.items():
            if isinstance(value, dict) or isinstance(value, list):
                restore_numpy_value(value)
            elif str(type(value)).find('numpy.float') > 0:
                data[key] = float(value)
            elif str(type(value)).find('numpy.int') > 0:
                data[key] = int(value)
    elif isinstance(data, list):
        for index, value in enumerate(data):
            if isinstance(value, dict) or isinstance(value, list):
                restore_numpy_value(value)
            elif str(type(value)).find('numpy.float') > 0:
                data[index] = float(value)
            elif str(type(value)).find('numpy.int') > 0:
                data[index] = int(value)


class AnalysisReport(ABC):
    hierarchy_type = None
    interval_type = None

    def __init__(self, hierarchy_type, interval_type, major=None, station_id=None):
        """
        初始化
        :param hierarchy_type: 层次类型
        :param interval_type: 时间区间类型，周 WEEKLY, 月 MONTHLY, 年 ANNUAL等。
        :param major: 专业（中文）
        :param station_id: 站段的ID
        """
        self.hierarchy_type = hierarchy_type
        self.interval_type = interval_type
        self.major = major
        self.station_id = station_id

    def get_report_paths(self):
        """
        返回报告存放的路径，及图片的存储路径。以便统一管理。
        :return {tuple} 返回两个参数：报告的路径, 报告对应图片的路径
        """
        root_path = os.path.commonprefix([__file__, current_app.instance_path])
        dir_path = os.path.join(root_path, current_app.config.get('DOWNLOAD_FILE_ANALYSIS'),
                                MAJOR_PATH_DICT.get(self.major), self.interval_type.lower())
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
        image_path = os.path.join(dir_path, 'images')
        return dir_path, image_path


class WeeklyAnalysisReport(AnalysisReport):
    """
    周报（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'WEEKLY'

    WEEKLY_REPORT_START_DATE = datetime(2018, 1, 1)

    def __init__(self, hierarchy_type, major=None, station_id=None, start_weekday=3):
        # 周报，默认周四 -> 周三。 其中 0 代表周一
        self._start_weekday = start_weekday
        self._end_weekday = (start_weekday-1) % 7
        super(WeeklyAnalysisReport, self).__init__(hierarchy_type, 'WEEKLY', major, station_id)

    @classmethod
    def get_week_intervals(cls, start_date, end_date):
        """
        根据输入的周开始结束时间，返回本周起始时间，环比起始时间
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: {tuple}，((本周开始日期，本周结束日期), (环比周开始日期，环比月度结束日期))
        """
        start_date0 = datetime.strptime(start_date, '%Y-%m-%d')
        end_date0 = datetime.strptime(end_date, '%Y-%m-%d')
        start_date_prv = start_date0 + relativedelta(weeks=-1)
        end_date_prv = end_date0 + relativedelta(weeks=-1)
        start_date_year_prv = start_date0 + relativedelta(years=-1)
        end_date_year_prv = end_date0 + relativedelta(years=-1)
        return (start_date, end_date), (start_date_prv.strftime('%Y-%m-%d'), end_date_prv.strftime('%Y-%m-%d')), \
               (start_date_year_prv.strftime('%Y-%m-%d'), end_date_year_prv.strftime('%Y-%m-%d'))

    def validate_interval(self, start_date, end_date):
        """
        验证周报的起始时间是否合法。
        合法的条件是：
          - start_date 周四
          - end_date 周三
          - start_date - end_date = 6（days）
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        if end_date < self.WEEKLY_REPORT_START_DATE:
            return False
        if start_date.weekday() != self._start_weekday or end_date.weekday() != self._end_weekday:
            # 判断是否周四开始，周三结束
            return False
        if (end_date - start_date).days != 6:
            # 判断是否一周时间
            return False
        return True

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数start_date, end_date
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'WEEKLY'
             topic: 报告的主题，如 工务系统周分析报告 或 成都工务段周分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        if start_date < WeeklyAnalysisReport.WEEKLY_REPORT_START_DATE:
            start_date = WeeklyAnalysisReport.WEEKLY_REPORT_START_DATE
        yesterday = datetime.now() + relativedelta(days=-1)
        if end_date > yesterday:
            end_date = yesterday
        report_start = start_date

        if report_start.weekday() > self._start_weekday:
            day = 7 - (report_start.weekday() - self._start_weekday)
        elif report_start.weekday() < self._start_weekday:
            day = self._start_weekday - report_start.weekday()
        else:
            day = 0
        report_start = report_start + relativedelta(days=day)
        reports = []
        report_end = report_start + relativedelta(weeks=1) - relativedelta(days=1)
        while report_end <= end_date:
            report_title = '{start}至{end}'.format(start=str(report_start)[:10], end=str(report_end)[:10])
            reports.append({
                "item_name": report_title,
                'start_date': report_start.strftime('%Y-%m-%d'),
                'end_date': report_end.strftime('%Y-%m-%d')
            })
            report_start = report_start + relativedelta(weeks=1)
            report_end = report_end + relativedelta(weeks=1)

        topic = f'{self.major}系统周分析报告'
        if self.hierarchy_type == 'STATION':
            station_name = get_department_name_by_dpid(self.station_id)
            topic = f'{station_name}周分析报告'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    @abstractmethod
    def generate_report_data(self, start_date, end_date):
        pass

    def load_report_data(self, start_date, end_date):
        condition = {
            "start_date": start_date,
            "end_date": end_date,
            "hierarchy": self.hierarchy_type,
            'major': self.major
        }
        if self.hierarchy_type == 'STATION':
            condition["station_id"] = self.station_id

        data = mongo.db['safety_analysis_weekly_report'].find_one(
            condition, {'_id': 0})
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        if self.major in ['电务', '工务']:
            if self.major == '电务' and self.hierarchy_type == 'MAJOR':
                if data is None:
                    data = self.generate_report_data(start_date, end_date)
                else:
                    data = data
            else:
                if data is None or os.path.exists(dir_path + '\\images') is not True:
                    data = self.generate_report_data(start_date, end_date)
                elif os.path.exists(dir_path + '\\images'):
                    files = os.listdir(dir_path + '\\images')
                    if len(files) == 0:
                        data = self.generate_report_data(start_date, end_date)
        else:
            if data is None:
                data = self.generate_report_data(start_date, end_date)
        return data

    def generate_report(self, start_date, end_date):
        """
        生成报告文件
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = self.load_report_data(start_date, end_date)
        # 通过模板渲染报表文件
        major_str = MAJOR_PATH_DICT.get(self.major)
        tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_weekly_report.docx'
        tpl = DocxTemplate(tpl_path)
        tpl.render(data)
        # 报告保存
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']

    def get_report_file(self, start_date, end_date):
        """
        获取报告文件
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        # 查询报表的名称（文件名）
        data = self.load_report_data(start_date, end_date)

        file_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(file_path, data['file_name'])
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return file_path, data['file_name']
        else:
            return self.generate_report(start_date, end_date)


class MonthlyAnalysisReport(AnalysisReport):
    """
    月报（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'MONTHLY'

    MONTHLY_REPORT_START_DATE = datetime(2018, 1, 1)

    def __init__(self, hierarchy_type, major=None, station_id=None):
        super(MonthlyAnalysisReport, self).__init__(hierarchy_type, 'MONTHLY', major, station_id)

    def validate_month(self, year, month):
        test_date = datetime(int(year), int(month), 1)
        if test_date < self.MONTHLY_REPORT_START_DATE:
            return False
        return True

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, month
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'MONTHLy'
             topic: 报告的主题，如 工务系统周分析报告 或 成都工务段周分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')
        if start_date < MonthlyAnalysisReport.MONTHLY_REPORT_START_DATE:
            start_date = MonthlyAnalysisReport.MONTHLY_REPORT_START_DATE
        yesterday = datetime.now() + relativedelta(days=-1)
        if end_date > yesterday:
            end_date = yesterday
        # 计算保证报告开始的时间，大于start_date
        report_start = datetime(start_date.year, start_date.month, 25)
        if report_start <= start_date:
            report_start = start_date + relativedelta(months=1)

        report_end = report_start + relativedelta(days=-1)
        reports = []

        # 可用的报告的结束日期，小于区间末端end_date
        while report_end <= end_date:
            year, month = report_end.year, report_end.month
            report_title = '{}年{}月'.format(year, month)
            reports.append({
                "item_name": report_title,
                'year': year,
                'month': month
            })
            report_start = report_start + relativedelta(months=1)
            report_end = report_start + relativedelta(days=-1)

        topic = f'{self.major}系统月分析报告'
        if self.hierarchy_type == 'STATION':
            station_name = get_department_name_by_dpid(self.station_id)
            topic = f'{station_name}月分析报告'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    @classmethod
    def get_month_interval(cls, year, month):
        end_date = datetime(year, month, 24)
        start_date = end_date + relativedelta(months=-1, days=1)
        return start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')

    @classmethod
    def get_month_intervals(cls, year, month):
        """
        根据输入月度参数，返回月度开始时间，月度环比开始时间，月度同比开始时间
        :param year:
        :param month:
        :return: {tuple}，(本月度开始就，本月度结束时间，环比月度开始时间，环比月度结束时间，同比月度开始时间，同比月度借时间)
        """
        end_date = datetime(year, month, 24)
        start_date = end_date + relativedelta(months=-1, days=1)
        end_date_prv = end_date + relativedelta(months=-1)
        start_date_prv = start_date + relativedelta(months=-1)
        end_date_year = end_date + relativedelta(years=-1)
        start_date_year = start_date + relativedelta(years=-1)
        return ((start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')),
                (start_date_prv.strftime('%Y-%m-%d'), end_date_prv.strftime('%Y-%m-%d')),
                (start_date_year.strftime('%Y-%m-%d'), end_date_year.strftime('%Y-%m-%d')))

    @abstractmethod
    def generate_report_data(self, year, month):
        pass

    def load_report_data(self, year, month):
        condition = {
            "year": year,
            "month": month,
            "hierarchy": self.hierarchy_type,
            'major': self.major
        }
        if self.hierarchy_type == 'STATION':
            condition["station_id"] = self.station_id

        data = mongo.db['safety_analysis_monthly_report'].find_one(
            condition, {'_id': 0})
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        if self.major in ['电务', '工务', '车务']:
            if self.major == '车务' and self.hierarchy_type == 'MAJOR':
                if data is None:
                    data = self.generate_report_data(year, month)
                    return data
                else:
                    data = data
                    return data
            else:
                if data is None or os.path.exists(dir_path + '\\images') is not True:
                    data = self.generate_report_data(year, month)
                elif os.path.exists(dir_path + '\\images'):
                    files = os.listdir(dir_path + '\\images')
                    if len(files) == 0:
                        data = self.generate_report_data(year, month)
        else:
            if data is None:
                data = self.generate_report_data(year, month)
        return data

    def generate_report(self, year, month):
        """
        生成报告文件
        :param year: {int} 年份
        :param month: {int} 月份
        :return:
        """
        data = self.load_report_data(year, month)
        # 通过模板渲染报表文件
        major_str = MAJOR_PATH_DICT.get(self.major)
        tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_monthly_report.docx'
        tpl = DocxTemplate(tpl_path)
        tpl.render(data)
        # 报告保存
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']

    def get_report_file(self, year, month):
        """
        获取报告文件
        :return:
        """
        # 查询报表的名称（文件名）
        data = self.load_report_data(year, month)

        dir_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(dir_path, data['file_name'])
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return dir_path, data['file_name']
        else:
            return self.generate_report(year, month)


class QuarterlyAnalysisReport(AnalysisReport):
    """
    季度报告（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'QUARTERLY'

    QUARTERLY_REPORT_START = (2018, 1)
    QUARTERLY_REPORT_START_DATE = datetime(2017, 12, 25)

    def __init__(self, hierarchy_type, major=None, station_id=None):
        super(QuarterlyAnalysisReport, self).__init__(hierarchy_type, 'QUARTERLY', major, station_id)

    def validate_quarter(self, year, quarter):
        """
        验证指定的季度是否有效
        :param year: {int} 年份
        :param quarter: {int} 季度 1-4
        :return:
        """
        if (year * 10 + quarter) < (self.QUARTERLY_REPORT_START[0] * 10 + self.QUARTERLY_REPORT_START[1]):
            # 年份如果小于所有可以用报告开始的季度
            return False
        report_end = datetime(year, 3 * quarter, 24)
        if report_end > datetime.now():
            # 如果请求的报告的结束时间比当前时间还大
            return False
        return True

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, quarter
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'QUARTERLY'
             topic: 报告的主题，如 工务系统周分析报告 或 成都工务段周分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if start_date < QuarterlyAnalysisReport.QUARTERLY_REPORT_START_DATE:
            start_date = QuarterlyAnalysisReport.QUARTERLY_REPORT_START_DATE
        if end_date > datetime.now():
            end_date = datetime.now()

        # 计算保证报告开始的时间，大于start_date
        report_start = datetime(start_date.year, start_date.month, 25)
        if report_start < start_date:
            report_start = start_date + relativedelta(months=1)
        while report_start.month % 3 != 0:
            # 季度报告开始的时间，是12, 3, 6, 9月的25日
            report_start = report_start + relativedelta(months=1)

        report_end = report_start + relativedelta(months=3, days=-1)
        reports = []
        quarter_ch = ('一', '二', '三', '四')
        while report_end < end_date:
            year, quarter = report_end.year, report_end.month // 3
            report_title = '{}年{}季度'.format(year, quarter_ch[quarter-1])
            reports.append({
                "item_name": report_title,
                'year': year,
                'quarter': quarter
            })
            report_end = report_end + relativedelta(months=3)

        topic = f'{self.major}系统季度分析报告'
        if self.hierarchy_type == 'STATION':
            station_name = get_department_name_by_dpid(self.station_id)
            topic = f'{station_name}季度分析报告'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    @classmethod
    def get_quarter_interval(cls, year, quarter):
        end_date = datetime(year, quarter * 3, 24)
        start_date = end_date + relativedelta(months=-3, days=1)
        return start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')

    @classmethod
    def get_quarter_intervals(cls, year, quarter):
        """
        根据输入季度参数，返回季度开始时间，季度环比开始时间，季度同比开始时间
        :param year:
        :param quarter:
        :return: {tuple}，(本季度开始就，本季度结束时间，环比季度开始时间，环比季度结束时间，同比季度开始时间，同比季度借时间)
        """
        end_date = datetime(year, quarter * 3, 24)
        start_date = end_date + relativedelta(months=-3, days=1)
        end_date_prv = end_date + relativedelta(months=-3)
        start_date_prv = start_date + relativedelta(months=-3)
        end_date_year = end_date + relativedelta(years=-1)
        start_date_year = start_date + relativedelta(years=-1)
        return ((start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')),
                (start_date_prv.strftime('%Y-%m-%d'), end_date_prv.strftime('%Y-%m-%d')),
                (start_date_year.strftime('%Y-%m-%d'), end_date_year.strftime('%Y-%m-%d')))

    @abstractmethod
    def generate_report_data(self, year, quarter):
        pass

    def load_report_data(self, year, quarter):
        condition = {
            "year": year,
            "quarter": quarter,
            "hierarchy": self.hierarchy_type,
            'major': self.major
        }
        if self.hierarchy_type == 'STATION':
            condition["station_id"] = self.station_id

        data = mongo.db['safety_analysis_quarterly_report'].find_one(
            condition, {'_id': 0})
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        if self.major in ['电务', '工务']:
            if data is None or os.path.exists(dir_path + '\\images') is not True:
                data = self.generate_report_data(year, quarter)
            elif os.path.exists(dir_path + '\\images'):
                files = os.listdir(dir_path + '\\images')
                if len(files) == 0:
                    data = self.generate_report_data(year, quarter)
        else:
            if data is None:
                data = self.generate_report_data(year, quarter)
        return data

    def generate_report(self, year, quarter):
        """
        生成报告文件
        :param year: {int} 年份
        :param quarter: {int} 季度
        :return:
        """
        data = self.load_report_data(year, quarter)
        # 通过模板渲染报表文件
        major_str = MAJOR_PATH_DICT.get(self.major)
        tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_quarterly_report.docx'
        tpl = DocxTemplate(tpl_path)
        tpl.render(data)
        # 报告保存
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']

    def get_report_file(self, year, quarter):
        """
        获取报告文件
        :return:
        """
        # 查询报表的名称（文件名）
        data = self.load_report_data(year, quarter)

        dir_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(dir_path, data['file_name'])
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return dir_path, data['file_name']
        else:
            return self.generate_report(year, quarter)


class SemiannualAnalysisReport(AnalysisReport):
    """
    半年报告（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'SEMIANNUAL'

    SEMIANNUAL_REPORT_START = (2018, 1)
    SEMIANNUAL_REPORT_START_DATE = datetime(2017, 12, 25)

    def __init__(self, hierarchy_type, major=None, station_id=None):
        super(SemiannualAnalysisReport, self).__init__(hierarchy_type, 'SEMIANNUAL', major, station_id)

    def validate_semiannual(self, year, half):
        """
        验证指定的半年是否有效
        :param year: {int} 年份
        :param half: {int} 半年 1 -上半年; 2 - 下半年
        :return:
        """
        if (year * 10 + half) < (self.SEMIANNUAL_REPORT_START[0] * 10 + self.SEMIANNUAL_REPORT_START[1]):
            # 如果小于报告开始的半年
            return False
        report_end = datetime(year, 6 * half, 24)
        if report_end > datetime.now():
            # 如果请求的报告的结束时间比当前时间还大
            return False
        return True

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, quarter
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'SEMIANNUAL'
             topic: 报告的主题，如 工务系统半年分析报告 或 成都工务段半年分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if start_date < SemiannualAnalysisReport.SEMIANNUAL_REPORT_START_DATE:
            start_date = SemiannualAnalysisReport.SEMIANNUAL_REPORT_START_DATE
        if end_date > datetime.now():
            end_date = datetime.now()

        beyond_limit = False
        year = SemiannualAnalysisReport.SEMIANNUAL_REPORT_START[0]
        reports = []
        interval_ch = ('上半年', '下半年')
        while not beyond_limit:
            for i in (1, 2):
                report_end = datetime(year, 6 * i, 24)
                report_start = report_end + relativedelta(months=-6, days=1)
                if report_end < end_date and report_start >= start_date:
                    report_title = '{}年{}度'.format(year, interval_ch[i - 1])
                    reports.append({
                        "item_name": report_title,
                        'year': year,
                        'half': i
                    })
                if report_end >= end_date:
                    beyond_limit = True
                    break
            year = year + 1

        topic = f'{self.major}系统半年度分析报告'
        if self.hierarchy_type == 'STATION':
            station_name = get_department_name_by_dpid(self.station_id)
            topic = f'{station_name}半年度分析报告'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    @classmethod
    def get_semiannual_interval(cls, year, half):
        """
        根据输入半年参数，返回半年开始时间、结束时间
        :param year: {int} 年份
        :param half: {int} 半年 1 -上半年; 2 - 下半年
        :return:
        """
        end_date = datetime(year, half * 6, 24)
        start_date = end_date + relativedelta(months=-6, days=1)
        return start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')

    @classmethod
    def get_semiannual_intervals(cls, year, half):
        """
        根据输入半年度参数，返回半年起始时间，半年度环比起始时间，半年度同比起始时间
        :param year: {int} 年份
        :param half: {int} 半年 1 - 上半年; 2 - 下半年
        :return: {tuple}，二维元组
                ((本半年开始就，本半年结束时间)，(环比半年开始时间，环比半年结束时间)，(同比半年开始时间，同比半年借宿时间))
        """
        end_date = datetime(year, half * 6, 24)
        start_date = end_date + relativedelta(months=-6, days=1)
        end_date_prv = end_date + relativedelta(months=-6)
        start_date_prv = start_date + relativedelta(months=-6)
        end_date_year = end_date + relativedelta(years=-1)
        start_date_year = start_date + relativedelta(years=-1)
        return ((start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')),
                (start_date_prv.strftime('%Y-%m-%d'), end_date_prv.strftime('%Y-%m-%d')),
                (start_date_year.strftime('%Y-%m-%d'), end_date_year.strftime('%Y-%m-%d')))

    @abstractmethod
    def generate_report_data(self, year, half):
        pass

    def load_report_data(self, year, half):
        condition = {
            "year": year,
            "half": half,
            "hierarchy": self.hierarchy_type,
            "major": self.major
        }
        if self.hierarchy_type == 'STATION':
            condition["station_id"] = self.station_id

        data = mongo.db['safety_analysis_semiannual_report'].find_one(
            condition, {'_id': 0})
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        if self.major in ['电务', '工务']:
            if data is None or os.path.exists(dir_path + '\\images') is not True:
                data = self.generate_report_data(year, half)
            elif os.path.exists(dir_path + '\\images'):
                files = os.listdir(dir_path + '\\images')
                if len(files) == 0:
                    data = self.generate_report_data(year, half)
        else:
            if data is None:
                data = self.generate_report_data(year, half)
        return data

    def generate_report(self, year, half):
        """
        生成报告文件
        :param year: {int} 年份
        :param half: 半年 1 - 上半年; 2 - 下半年
        :return:
        """
        data = self.load_report_data(year, half)
        # 通过模板渲染报表文件
        major_str = MAJOR_PATH_DICT.get(self.major)
        tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_semiannual_report.docx'
        tpl = DocxTemplate(tpl_path)
        tpl.render(data)
        # 报告保存
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']

    def get_report_file(self, year, half):
        """
        获取报告文件
        :param year: {int} 年份
        :param half: 半年 1 - 上半年; 2 - 下半年
        :return:
        """
        # 查询报表的名称（文件名）
        data = self.load_report_data(year, half)

        dir_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(dir_path, data['file_name'])
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return dir_path, data['file_name']
        else:
            return self.generate_report(year, half)


class AnnualAnalysisReport(AnalysisReport):
    """
    年报告（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'ANNUAL'

    ANNUAL_REPORT_START = 2018
    ANNUAL_REPORT_START_DATE = datetime(2017, 12, 25)

    def __init__(self, hierarchy_type, major=None, station_id=None):
        super(AnnualAnalysisReport, self).__init__(hierarchy_type, 'ANNUAL', major, station_id)

    def validate_annual(self, year):
        """
        验证指定的年是否有效
        :param year: {int} 年份
        :return:
        """
        if year < self.ANNUAL_REPORT_START:
            # 如果小于报告开始的年
            return False
        report_end = datetime(year, 12, 24)
        if report_end > datetime.now():
            # 如果请求的报告的结束时间比当前时间还大
            return False
        return True

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, quarter
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'SEMIANNUAL'
             topic: 报告的主题，如 工务系统半年分析报告 或 成都工务段半年分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = datetime.strptime(start_date, '%Y-%m-%d')
        end_date = datetime.strptime(end_date, '%Y-%m-%d')

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if start_date < AnnualAnalysisReport.ANNUAL_REPORT_START_DATE:
            start_date = AnnualAnalysisReport.ANNUAL_REPORT_START_DATE
        if end_date > datetime.now():
            end_date = datetime.now()

        beyond_limit = False
        year = AnnualAnalysisReport.ANNUAL_REPORT_START
        reports = []
        while not beyond_limit:
            report_end = datetime(year, 12, 24)
            report_start = report_end + relativedelta(years=-1, days=1)
            report_title = '{}年'.format(year)
            if report_end < end_date and report_start >= start_date:
                reports.append({
                    "item_name": report_title,
                    'year': year
                })
            if report_end >= end_date:
                beyond_limit = True
            year = year + 1

        topic = f'{self.major}系统年度分析报告'
        if self.hierarchy_type == 'STATION':
            station_name = get_department_name_by_dpid(self.station_id)
            topic = f'{station_name}年度分析报告'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    @classmethod
    def get_annual_interval(cls, year):
        """
        根据输入半年参数，返回半年开始时间、结束时间
        :param year: {int} 年份
        :return:
        """
        end_date = datetime(year, 12, 24)
        start_date = end_date + relativedelta(years=-1, days=1)
        return start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')

    @classmethod
    def get_annual_intervals(cls, year):
        """
        根据输入年度参数，返回年起始时间，年度同比起始时间
        :param year: {int} 年份
        :return: {tuple}，二维元组
                ((本年开始就，本年结束时间)，(同比年开始时间，同比年结束时间))
        """
        end_date = datetime(year, 12, 24)
        start_date = end_date + relativedelta(years=-1, days=1)
        end_date_year = end_date + relativedelta(years=-1)
        start_date_year = start_date + relativedelta(years=-1)
        return ((start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d')),
                (start_date_year.strftime('%Y-%m-%d'), end_date_year.strftime('%Y-%m-%d')))

    @abstractmethod
    def generate_report_data(self, year):
        pass

    def load_report_data(self, year):
        condition = {
            "year": year,
            "hierarchy": self.hierarchy_type,
            "major": self.major
        }
        if self.hierarchy_type == 'STATION':
            condition["station_id"] = self.station_id

        data = mongo.db['safety_analysis_annual_report'].find_one(
            condition, {'_id': 0})
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        if self.major in ['电务', '工务']:
            if data is None or os.path.exists(dir_path + '\\images') is not True:
                data = self.generate_report_data(year)
            elif os.path.exists(dir_path + '\\images'):
                files = os.listdir(dir_path + '\\images')
                if len(files) == 0:
                    data = self.generate_report_data(year)
        else:
            if data is None:
                data = self.generate_report_data(year)
        return data

    def generate_report(self, year):
        """
        生成报告文件
        :param year: {int} 年份
        :return:
        """
        data = self.load_report_data(year)
        # 通过模板渲染报表文件
        major_str = MAJOR_PATH_DICT.get(self.major)
        tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_annual_report.docx'
        tpl = DocxTemplate(tpl_path)
        tpl.render(data)
        # 报告保存
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']

    def get_report_file(self, year):
        """
        获取报告文件
        :param year: {int} 年份
        :return:
        """
        # 查询报表的名称（文件名）
        data = self.load_report_data(year)

        dir_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(dir_path, data['file_name'])
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return dir_path, data['file_name']
        else:
            return self.generate_report(year)


class DailyAnalysisReport(AnalysisReport):
    """
    日报（抽象类）。报表，主要抽象以下的行为：
    1. 生成数据 generate_report_data (抽象方法，需要具体的报表实现)
    2. 通过数据生成报表 generate_report
    3. 获取报表文件 get_report_file
    """
    # 类变量
    interval_type = 'DAILY'

    def __init__(self, hierarchy_type, major=None, station_id=None):
        super(DailyAnalysisReport, self).__init__(hierarchy_type, 'DAILY', major, station_id)

    @abstractmethod
    def generate_report_data(self, report_date):
        """
        生成报告所需要的数据，并存入MongoDB
        :param report_date: {str} 报告的日期, 格式 %Y-%m-%d, 2019-01-01
        :return: {dict} 报告的数据的字典
        """
        pass

    @abstractmethod
    def fill_data(self, workbook, data):
        """
        使用指定的数据data，填充报告（excel）
        :param workbook: {openyxl.workbook.Workbook} excel文件
        :param data:
        :return:
        """
        pass

    @abstractmethod
    def generate_report_name(self, report_date):
        """
        生成报告的（唯一）名称，它可以用于区分报告。例如：
        1. 用作报告标题
        2. 用作文件名
        :param report_date: {str} 报告的日期, 格式 %Y-%m-%d, 2019-01-01
        :return: {str} 报告的名称，例如: '集团安全日报'
        """
        pass

    @classmethod
    def get_daily_intervals(cls, report_date):
        """
        三个元组包括：
        1. 日报的开始时间为昨天18:00:00 至 当天 18:00:00
        2. 日报所在日期的当周的开始结束时间
        3. 日报所在日期的当月的开始结束时间
        :param report_date: {str} 日期字符串， 格式 %Y-%m-%d, 2019-01-01
        :return: {tuple}，二维元组 日期时间格式 '2019-12-0 =1 18:00:00'
                ((日报开始时间，本年结束时间)，(同比年开始时间，同比年结束时间))
        """
        refer_date = datetime.strptime(report_date, '%Y-%m-%d')
        # 当前的起始
        end0 = refer_date.strftime('%Y-%m-%d 23:59:59')
        start0 = (refer_date + relativedelta(days=-1)).strftime('%Y-%m-%d 23:59:59')
        # 一周时间的起始
        start1 = (refer_date + relativedelta(days=-6)).strftime('%Y-%m-%d 23:59:59')
        end1 = end0
        # 一个月时间的起始
        start2 = datetime(refer_date.year, refer_date.month, 1).strftime('%Y-%m-%d 00:00:00')
        end2 = end0
        return (start0, end0), (start1, end1), (start2, end2)

    def load_report_data(self, report_date):
        """
        生成报告文件
        :param report_date: {str} 报告的日期, 格式 %Y-%m-%d, 2019-01-01
        """
        report_date = datetime.strptime(report_date, '%Y-%m-%d')
        date_idx = report_date.year * 10000 + report_date.month * 100 + report_date.day
        condition = {
            "date": date_idx,
            "hierarchy": self.hierarchy_type,
            'major': self.major,
            'station_id': self.station_id
        }

        data = mongo.db['safety_analysis_daily_report'].find_one(condition, {'_id': 0})
        return data

    def generate_report(self, report_date):
        """
        生成报告文件
        :param report_date: {str} 报告的日期, 格式 %Y-%m-%d, 2019-01-01
        :return: {tuple}, 第一个：存放报告的目录； 第二个：报告文件的名称
        """
        data = self.load_report_data(report_date)
        if not data:
            data = self.generate_report_data(report_date)

        # 通过模板渲染报表文件
        if self.hierarchy_type == 'GROUP':
            tpl_path = f'app/report/template/group_daily_report.xlsx'
        elif self.hierarchy_type == 'AJ':
            tpl_path = f'app/report/template/safety_analysis_center_daily_report.xlsx'
        else:
            major_str = MAJOR_PATH_DICT.get(self.major)
            tpl_path = f'app/report/template/{major_str}_{self.hierarchy_type.lower()}_daily_report.xlsx'
        # 报告的存放目录
        dir_path = get_report_path(self.interval_type.lower(), self.major)
        file_name = '{}.xlsx'.format(self.generate_report_name(report_date))
        file_path = os.path.join(dir_path, file_name)
        workbook = openpyxl.load_workbook(tpl_path)
        self.fill_data(workbook, data)
        workbook.save(file_path)
        return dir_path, file_name

    def get_report_file(self, report_date):
        """
        获取报告文件
        :param report_date: {str} 报告的日期, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        # 查询报表的名称（文件名）
        file_name = '{}.xlsx'.format(self.generate_report_name(report_date))
        file_path = get_report_path(self.interval_type.lower(), self.major)
        report_file = os.path.join(file_path, file_name)
        # 如果文件不存在，则生成报告
        if os.path.exists(report_file):
            return file_path, file_name
        else:
            return self.generate_report(report_date)

