"""
计算工务站段级的半年分析报告
Created: 2019-10-21
@author: Lu Jionglin
"""
import os
from docxtpl import DocxTemplate
from . import station_semiannual_report_data as report_data
from app import mongo
import app.report.analysis_report_manager as manager


class GongwuStationSemiannualAnalysisReport(manager.SemiannualAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(GongwuStationSemiannualAnalysisReport, self).__init__(hierarchy_type='STATION', major='工务',
                                                                    station_id=station_id)

    def generate_report_data(self, year, half):
        """
        执行生成指定半年、站段的数据，并件数据持久化
        :param year: {int} 年
        :param half: {int} 半年 1 - 上半年，2 - 下半年
        :return:
        """
        data = report_data.get_data(year, half, self.station_id)
        mongo.db['safety_analysis_semiannual_report'].delete_one(
            {
                "year": year,
                "half": half,
                "hierarchy": "STATION",
                "station_id": self.station_id
            })
        mongo.db['safety_analysis_semiannual_report'].insert_one(data)
        return data

    def generate_report(self, year, half):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param half: {int} 半年 1 - 上半年，2 - 下半年
        :return:
        """
        data = self.load_report_data(year, half)
        tpl = DocxTemplate('app/report/template/gongwu_station_semiannual_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']
