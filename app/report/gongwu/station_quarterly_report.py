"""
计算工务站段级的季度分析报告
Created: 2019-10-20
@author: Lu Jionglin
"""
import os

from docxtpl import DocxTemplate
from . import station_quarterly_report_data as report_data
from app import mongo
import app.report.analysis_report_manager as manager


class GongwuStationQuarterlyAnalysisReport(manager.QuarterlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(GongwuStationQuarterlyAnalysisReport, self).__init__(hierarchy_type='STATION', major='工务',
                                                                   station_id=station_id)

    def generate_report_data(self, year, quarter):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param quarter: int 季度 1 - 4
        :return:
        """
        data = report_data.get_data(year, quarter, self.station_id)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "quarter": quarter,
                "hierarchy": "STATION",
                "station_id": self.station_id
            })
        mongo.db['safety_analysis_quarterly_report'].insert_one(data)
        return data

    def generate_report(self, year, quarter):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param quarter: {int} 月份
        :return:
        """
        data = self.load_report_data(year, quarter)
        tpl = DocxTemplate('app/report/template/gongwu_station_quarterly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']
