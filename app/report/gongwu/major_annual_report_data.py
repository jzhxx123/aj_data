"""
计算工务专业的年度分析报告的具体数据
Created: 2019-10-21
@author: Lu Jionglin
"""
from datetime import datetime
import json
import threading
import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import MultipleLocator
from matplotlib.figure import Figure
from app.data.util import pd_query
from app.report import util as report_util
from .common_sql import PRODUCE_INFO_ACCIDENT_SQL, CHECK_EVALUATE_INFO_SQL, CHECK_EVALUATE_REVIEW_SQL, \
    STATION_CADRE_COUNT_SQL, LEADER_CHECK_INFO_SQL, LEADER_CHECK_PROBLEM_SQL, CHECK_INFO_DATA_SQL, \
    CHECK_PROBLEM_DATA_SQL, MAJOR_DEPARTMENT_DATA_SQL, \
    VIOLATION_PERSON_LIST_SQL3, VIOLATION_PERSON_CONFIRM_LIST_SQL3
from app import mongo
from app.safety_index.health_index import get_index_title
from docxtpl import InlineImage
from docx.shared import Mm
import app.report.analysis_report_manager as manager
from .common import extract_accident_code_type, calculate_comparison_statistic, \
    evaluate_deduct_level, calc_ratio, extract_section_from_all_name, QUARTER_MONTHS_TUPLE, EVALUATE_DEDUCT_LEVELS, \
    HEALTH_INDEX_MAIN_TYPES, EVALUATE_DEDUCT_LEVEL_NAMES, EVALUATE_PROBLEM_TYPE_NAMES, CHECK_PROBLEM_HIGH_LEVEL, \
    VIOLATION_PERSON_DISPOSE_METHODS, get_first_accident_type_analysis, get_evaluate_report_data, \
    get_first_general_safety_type_analysis, get_accident_resp_analysis, get_first_general, get_percent_text
from app.data.safety_produce_info import resp_type_divide

local_data = threading.local()


def get_data(year):
    """
    该月份的获取报表数据
    :param year:
    :return:
    """
    # 先加载公用的数据
    date_params = manager.AnnualAnalysisReport.get_annual_intervals(year)
    local_data.date_params = date_params
    local_data.year = year
    _load_general_data()

    major = '工务'
    common = {
        'last_year': local_data.end_date_year.year,
        'period_text': f'{year}年',
        'start_month': local_data.start_date.month,
        'end_month': local_data.end_date.month
    }

    # 计算各个章节的数据
    first = _get_first_general()
    second = _get_second_major_evaluate_analysis()
    third = _get_third_level_evaluate_analysis()
    fourth = _get_forth_sys_check_and_problem_analysis()
    fifth = _get_fifth_station_health_index_radar()
    sixth = _get_sixth_violation_person_analysis()

    result = {
        "year": year,
        "hierarchy": "MAJOR",
        "major": major,
        "file_name": f'{year}年工务系统安全情况分析报告.docx',
        "created_at": datetime.now(),
        "common": common,
        "first": first,
        "second": second,
        "third": third,
        "fourth": fourth,
        "fifth": fifth,
        "sixth": sixth
    }
    return result


def _load_general_data(stage=99):
    """
    加载在整个计算过程中使用的通用的数据
    :return:
    """
    date_params = local_data.date_params
    start_date = datetime.strptime(date_params[0][0], '%Y-%m-%d')
    end_date = datetime.strptime(date_params[0][1], '%Y-%m-%d')
    start_date_prv = datetime.strptime(date_params[1][0], '%Y-%m-%d')
    end_date_prv = datetime.strptime(date_params[1][1], '%Y-%m-%d')
    start_date_year = start_date_prv
    end_date_year = end_date_prv

    local_data.major = '工务'
    local_data.start_date = start_date
    local_data.end_date = end_date
    local_data.start_date_prv = start_date_prv
    local_data.end_date_prv = end_date_prv
    local_data.start_date_year = start_date_year
    local_data.end_date_year = end_date_year

    # 专业及其下属部门的清单
    df_dept = pd_query(MAJOR_DEPARTMENT_DATA_SQL)
    local_data.dept_data = df_dept

    # TODO for testing
    if stage == 1:
        return None

    # （本期）检查信息
    local_data.check_info_data = _filter_dept_data(
        CHECK_INFO_DATA_SQL.format(start_date, end_date), 'CHECK_DEPT_NAME', 1)
    # （上期环比)检查信息
    local_data.check_info_data_mom = _filter_dept_data(
        CHECK_INFO_DATA_SQL.format(local_data.start_date_prv, local_data.end_date_prv), 'CHECK_DEPT_NAME', 1)
    # （上年同比）检查信息
    local_data.check_info_data_yoy = local_data.check_info_data_mom.copy()

    # （本期）检查的问题
    local_data.check_problem_data = _filter_dept_data(
        CHECK_PROBLEM_DATA_SQL.format(start_date, end_date), 'CHECK_DEPT_NAME', 1)
    # （上期）检查的问题
    local_data.check_problem_data_mom = _filter_dept_data(
        CHECK_PROBLEM_DATA_SQL.format(local_data.start_date_prv, local_data.end_date_prv), 'CHECK_DEPT_NAME', 1)
    # （上年同比）检查问题
    local_data.check_problem_data_yoy = local_data.check_problem_data_mom.copy()

    # 本期的履职评价
    evaluate_info = pd_query(CHECK_EVALUATE_INFO_SQL.format(start_date, end_date))
    local_data.evaluate_info = evaluate_info

    # 上期履职评价
    last_month_evaluate_info = pd_query(
        CHECK_EVALUATE_INFO_SQL.format(start_date_prv, end_date_prv))
    local_data.last_month_evaluate_info = last_month_evaluate_info

    # 上年同期
    local_data.last_year_evaluate_info = last_month_evaluate_info.copy()
    # 今年全部履职评价
    evaluate_year_info = pd_query(
        CHECK_EVALUATE_INFO_SQL.format(end_date.strftime('%Y-01-01'), end_date.strftime('%Y-%m-%d')))
    local_data.evaluate_year_info = evaluate_year_info


def _filter_dept_data(data_sql, dept_rel_field, dept_rel_type):
    """
    将指定SQL加载的数据，以本线程暂存的dept_data(它是本报表使用的部门清单)进行过滤
    :param data_sql:
    :param dept_rel_field: str，SQL中，用于与dept_data的部门名称/ID管理的字段
    :param dept_rel_type: int， 关联的字段数据类型，0，代表部门ID； 1，代表部门名称(ALL_NAME)
    :return: 过滤后的dataframe
    """
    raw_data = pd_query(data_sql)
    if dept_rel_type == 1:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='ALL_NAME')
    else:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='DEPARTMENT_ID')
    return df


# -------第一章 ------------------------------------- Start
def _get_first_general():
    # 加载数据
    end_date = local_data.end_date
    start_date = local_data.start_date
    prod_data0 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(start_date, end_date))
    # 同比数据
    prod_data1 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(local_data.start_date_year, local_data.end_date_year))
    if prod_data1.empty:
        prod_data1 = pd.DataFrame(columns=prod_data0.columns)
    # 环比数据
    prod_data2 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(local_data.start_date_prv, local_data.end_date_prv))
    if prod_data2.empty:
        prod_data2 = pd.DataFrame(columns=prod_data0.columns)

    general = get_first_general(prod_data0, prod_data1, prod_data2)

    # 1. 行车安全， 2. 劳安安全， 3. 路外安全 三个小节
    gen_sub_stat = get_first_general_safety_type_analysis(prod_data0, prod_data1, prod_data2)

    dept_data = local_data.dept_data[['DEPARTMENT_ID', 'ALL_NAME']]
    resp_data = [prod_data0, prod_data1, prod_data2]
    resp_data = [
        pd.merge(data, dept_data, left_on='RESP_UNIT_ID', right_on='DEPARTMENT_ID')
        for data in resp_data
    ]

    result = {
        'general': general,
        'detail_stat_list': gen_sub_stat['detail_stat_list'],
        'detail_stat_resp_d3': gen_sub_stat['detail_stat_resp_d3'],
        'acc_detail_type_static_list': get_first_accident_type_analysis(prod_data0, prod_data1, prod_data2),
        'responsibility_data': get_accident_resp_analysis(resp_data[0], resp_data[1], resp_data[2])
    }

    return result
# -------第一章 ------------------------------------- END


def calculate_yoy_and_mom_statistic(now_count, year_count, month_count):
    year_ratio = now_count - year_count
    if year_count == 0:
        year_percent = year_ratio * 100
    else:
        year_percent = round(year_ratio / year_count, 3) * 100
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 3) * 100
    result = {
        'year_diff': year_ratio,
        'year_percent': year_percent,
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# -------------第二章--------------------
def _get_second_major_evaluate_analysis():
    """获取履职情况简要统计分析数据
    """
    start_date, end_date = local_data.start_date, local_data.end_date
    evaluate_info = local_data.evaluate_info

    if evaluate_info.empty:
        return {}

    # 第一块 干部评价总体情况分析
    first_paragraph = _get_evaluate_whole_situation()
    evaluate_problem_type_table = _get_evaluate_problem_type_table()

    # 第二块 工务系统干部履职评价情况分析
    major_evaluate_situation = _get_major_evaluate_situation()
    # 第三块 局管领导人员履职评价分析
    leader_evaluate_analysis = _get_leader_evaluate_situation()
    # 第四块 正科职干部履职评价简要分析
    chief_evaluate_analysis = _get_branch_level_cadre_situation('正科级')
    # 第五块 副科职及以下干部履职简要分析
    assistant_evaluate_analysis = _get_branch_level_cadre_situation('副科级')

    result = {
        'evaluate_total_analysis': {
            "total_evaluate": first_paragraph,
            "evaluate_problem_type_table": evaluate_problem_type_table
        },
        'cadre_evaluate_analysis': {
            "major_evaluate_situation": major_evaluate_situation
        },
        'leader_evaluate_analysis': leader_evaluate_analysis,
        'chief_evaluate_analysis': chief_evaluate_analysis,
        'assistant_evaluate_analysis': assistant_evaluate_analysis
    }
    return result


def _get_evaluate_whole_situation():
    start_date, end_date = local_data.start_date, local_data.end_date
    evaluate_info = local_data.evaluate_info
    evaluate_review = pd_query(
        CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date))
    total_ring = calc_ratio(evaluate_info.shape[0], local_data.last_month_evaluate_info.shape[0])
    # 定期评价人次
    regular_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 2].shape[0]
    # 逐条评价人次
    one_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 1].shape[0]
    # 履职问题个数
    evaluate_count = evaluate_info.shape[0]
    # 履职问题人数
    evaluate_person = len(set(evaluate_info['RESPONSIBE_ID_CARD']))
    # 工人履职数据
    work_data = evaluate_info[evaluate_info['LEVEL'] == "工人"]
    # 工人条数和人数
    work_count, work_person = work_data.shape[0], len(
        set(work_data['RESPONSIBE_ID_CARD']))
    # 安全谈心数据
    talk_data = evaluate_info[evaluate_info['ITEM_NAME'] == '安全谈心']
    talk_count, talk_person = talk_data.shape[0], len(
        set(talk_data['RESPONSIBE_ID_CARD']))
    # 共计评价得分
    total_score = sum(list(evaluate_info['SCORE']))
    # 最高评价计分
    score_data = evaluate_info.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    most_score = round(score_data.at[0, 'SCORE'], 1)
    # 今年得分数据
    score_year_data = local_data.evaluate_year_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_total_count = score_year_data.shape[0]
    month_percent = round(evaluate_count / year_total_count * 100, 1)
    most_year_score = round(score_year_data.at[0, 'SCORE'], 1)
    punish_year_count = score_year_data[score_year_data['SCORE'] >= 2].shape[0]
    punish_count = score_data[score_data['SCORE'] >= 2].shape[0]
    score_data['level'] = score_data['SCORE'].apply(evaluate_deduct_level)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        evaluate_deduct_level)
    p_level_rst = []
    for level in EVALUATE_DEDUCT_LEVELS:
        p_level_rst.append({
            "level": EVALUATE_DEDUCT_LEVEL_NAMES.get(level),
            "total_count": score_year_data[score_year_data['level'] == level].shape[0],
            "month_count": score_data[score_data['level'] == level].shape[0]
        })
    score_rst = {
        "year_total_count": year_total_count,
        "month_count": evaluate_count,
        "month_percent": month_percent,
        "most_year_score": most_year_score,
        "punish_year_count": punish_year_count,
        "punish_count": punish_count,
        "p_level_value": p_level_rst,
        "t_level_value": p_level_rst[::-1]
    }
    rst = {
        'regular_count': regular_count,
        'one_count': one_count,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'work_count': work_count,
        'work_person': work_person,
        'talk_count': talk_count,
        'talk_person': talk_person,
        'total_score': total_score,
        'most_score': most_score,
        'total_ring': total_ring,
        'score_level_value': score_rst
    }
    return rst


def _get_evaluate_problem_type_table():
    evaluate_info = local_data.evaluate_info
    last_month_evaluate_info = local_data.last_month_evaluate_info
    total_count = evaluate_info[evaluate_info.ITEM_NAME.isin(EVALUATE_PROBLEM_TYPE_NAMES)].shape[0]
    counts = []
    percents = []
    rings = []
    detail_item = []
    majors_rst = []
    problem_inc_text = []
    problem_ded_text = []
    for item in EVALUATE_PROBLEM_TYPE_NAMES:
        major_item_rst = []
        if total_count == 0:
            count, percent, ring = 0, 0, 0
        else:
            item_info = evaluate_info[evaluate_info['ITEM_NAME'] == item]
            last_item_info = last_month_evaluate_info[
                last_month_evaluate_info['ITEM_NAME'] == item]
            count, last_count = len(item_info), len(last_item_info)
            percent = round(count / total_count * 100, 1)
            ring = calc_ratio(count, last_count)
            if ring > 0:
                problem_inc_text.append(item)
            else:
                problem_ded_text.append(item)
            # 各专业系统分类
            if item_info.empty is False:
                # major_item_rst = _get_major_evaluate_table(item_info, last_item_info)
                # 具体问题分类数据
                item_info = item_info.groupby(['CODE', 'SITUATION'])
                for name, group in item_info:
                    score_count = len(group)
                    score_person = int(group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
                    detail_item.append({
                        'detail_name': name[1],
                        'code': name[0],
                        'name': item,
                        'score_count': score_count,
                        'score_person': score_person
                    })
        counts.append(count)
        percents.append(percent)
        rings.append(ring)
    problem_inc_text = '、'.join(problem_inc_text)
    problem_ded_text = '、'.join(problem_ded_text)
    detail_data = pd.DataFrame(detail_item)
    problem_score_table_data = detail_data.iloc[detail_data.groupby(
        ['name']).apply(lambda x: x['score_count'].idxmax())].sort_values(
        by='score_count', ascending=False).head(6)
    limit = [5, 4, 5, 3, 4, 5, 3, 5, 5, 10000]
    problem_rst = []
    for index in problem_score_table_data.index:
        item_name = problem_score_table_data.at[index, 'name']
        detail_item_name = problem_score_table_data.at[index, 'detail_name']
        detail_item_count = int(
            problem_score_table_data.at[index, 'score_count'])
        item_data = detail_data[detail_data['name'] == item_name].sort_values(
            by='score_count',
            ascending=False).head(limit[EVALUATE_PROBLEM_TYPE_NAMES.index(item_name)])
        item_count = sum(list(item_data['score_count']))
        item_person = sum(list(item_data['score_person']))
        item_rst = []
        for idx in item_data.index:
            detail_name = item_data.at[idx, 'detail_name']
            detail_count = int(item_data.at[idx, 'score_count'])
            detail_person = int(item_data.at[idx, 'score_person'])
            item_rst.append({
                'detail_name': detail_name,
                'detail_count': detail_count,
                'detail_person': detail_person
            })
        problem_rst.append({
            "name":
                item_name,
            "item_count":
                item_count,
            "item_person":
                item_person,
            "detail_name":
                detail_item_name,
            "detail_count":
                detail_item_count,
            "detail_percent":
                round(detail_item_count / item_count * 100, 1),
            "item_problem":
                item_rst
        })
    result = {
        'names': EVALUATE_PROBLEM_TYPE_NAMES,
        'counts': counts,
        'percents': percents,
        'rings': rings,
        'major_value': majors_rst,
        'problem_value': problem_rst,
        'problem_inc_text': problem_inc_text,
        'problem_ded_text': problem_ded_text,
        'problem_inc_count': len(problem_inc_text.split('、'))
    }
    return result


def _get_major_evaluate_situation():
    """各系统干部问题分析

    Returns:
        dict  -- 结果
    """
    all_year_info = local_data.evaluate_year_info.groupby(
        ['MAJOR', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    all_year_info['level'] = all_year_info['SCORE'].apply(evaluate_deduct_level)
    major_info = local_data.evaluate_info
    totals = [
        all_year_info[all_year_info['level'] == i].shape[0]
        for i in range(1, 8)
    ]
    levels = [EVALUATE_DEDUCT_LEVEL_NAMES.get(i) for i in range(1, 8)]

    detail_data = _get_evaluate_major_detail_item_data(major_info, '工务')
    major_count = major_info.shape[0]
    group_check_count = major_info[major_info['CHECK_TYPE'] == 1].shape[0]
    station_check_count = major_info[major_info['CHECK_TYPE'] == 2].shape[0]
    major_info = major_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    if major_info.empty:
        major_most_score = 0
    else:
        major_most_score = major_info.at[0, 'SCORE']
    # major_year_info = all_year_info[all_year_info['MAJOR'] == major]
    # 年度内达到处理标准的人员数量
    major_year_count = all_year_info[all_year_info['SCORE'] >= 2].shape[0]
    # 本月份达到处理标准的人员数量
    major_punish = major_info[major_info['SCORE'] >= 2].shape[0]

    # 各个站段人员聚合
    station_info = local_data.evaluate_info
    station_gb = station_info.groupby(['STATION', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    station_gb = station_gb[station_gb['SCORE'] >= 2]
    station_gb = station_gb.groupby('STATION').count().sort_values(by='SCORE', ascending=False)
    station_list = [
        {
            'station_name': station_name,
            'station_punish_count': int(station_gb.loc[station_name, 'SCORE'])
        } for station_name in station_gb.index
    ]

    result = {
        "major_count": major_count,
        "group_check_count": group_check_count,
        "station_check_count": station_check_count,
        "major_most_score": major_most_score,
        "major_year_punish": major_year_count,
        "major_punish": major_punish,
        "level_headers": levels,
        "level_total_list": totals,
        "detail_data": detail_data,
        "station_data": station_list,

    }

    return result


def _get_evaluate_major_detail_item_data(major_info, major):
    """履职信息各专业突出问题数据

    Arguments:
        major_info {dataframe} -- 各专业原始数据
        major {str} -- 专业名称

    Returns:
        dict -- 结果
    """
    major_limit = {"车务": 10, "机务": 6, "工务": 8, "电务": 6, "供电": 6, "车辆": 6}
    major_data = major_info.groupby(['CODE', 'SITUATION'])
    detail_item = []
    for name, group in major_data:
        score_count = group.shape[0]
        score_person = int(group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
        detail_item.append({
            'detail_name': name[1].replace("\n", ""),
            'code': name[0],
            'score_count': score_count,
            'score_person': score_person
        })
    detail_data = pd.DataFrame(detail_item)
    if detail_data.empty:
        return []
    detail_data = detail_data.sort_values(
        by='score_count', ascending=False).head(major_limit.get(major))
    return [
        json.loads(detail_data.loc[index].T.to_json())
        for index in detail_data.index
    ]


# -------第二章，第三小节： 局领导履职情况分析
def _get_leader_evaluate_situation():
    """
    第二章，第三小节： 局领导履职情况分析
    Returns:
        dict  -- 结果
    """
    evaluate_info = local_data.evaluate_info
    last_month_evaluate_info = local_data.last_month_evaluate_info
    evaluate_year_info = local_data.evaluate_year_info

    leader_eval_info = evaluate_info[evaluate_info['GRADATION'] == '局管领导人员']
    total = leader_eval_info.shape[0]
    total_person = len(set(leader_eval_info['RESPONSIBE_ID_CARD']))
    month_leader_eval_info = last_month_evaluate_info[
        last_month_evaluate_info['GRADATION'] == '局管领导人员']
    major_counts = []

    for major in ['工务']:
        major_counts.append({
            'name':
                major,
            'count':
                leader_eval_info[leader_eval_info['MAJOR'] == major].shape[0]
        })
    # 履职问题分布表
    item_rst = []
    for item in EVALUATE_PROBLEM_TYPE_NAMES:
        major_rst = []
        item_data = leader_eval_info[leader_eval_info['ITEM_NAME'] == item]
        item_month_data = month_leader_eval_info[month_leader_eval_info['ITEM_NAME'] == item]
        if item_data.empty is False:
            major_rst = _get_major_evaluate_table(item_data, item_month_data)
        else:
            for major in ['工务']:
                major_rst.append({'name': major, 'count': 0, 'ring': 0})
            major_rst.append({'name': '合计', 'count': 0, 'ring': 0})
        item_rst.append(major_rst)
    # 问题统计
    niubi_info = leader_eval_info.groupby(['SITUATION'])
    pro_rst = []
    for name, group in niubi_info:
        pro_count = group.shape[0]
        pro_person = len(set(group['RESPONSIBE_ID_CARD']))
        pro_rst.append({
            "name": name,
            "count": pro_count,
            "person": pro_person
        })
    # 一分以上人员信息
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    leader_eval_info = leader_eval_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    punish_data = leader_eval_info[leader_eval_info['SCORE'] >= 1]
    punish_rst = [{
        "name": punish_data.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": punish_data.at[index, 'STATION'],
        "job": punish_data.at[index, 'JOB'],
        "score": round(punish_data.at[index, 'SCORE'], 1)
    } for index in punish_data.index]
    # 全年干部表格表格
    year_info = evaluate_year_info[evaluate_year_info['GRADATION'] == '局管领导人员']
    year_data = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_cadre_total_score_table(year_data, leader_eval_info)
    # 累计处理人次
    year_punish = _get_punish_content(year_data, leader_eval_info)
    rst = {
        "names": EVALUATE_PROBLEM_TYPE_NAMES,
        "total": total,
        "total_person": total_person,
        "table": item_rst,
        "problem_total": pro_rst,
        "major_total": major_counts,
        "punish_p": punish_rst,
        "year_table": year_table,
        "year_punish": year_punish
    }
    return rst


def _get_major_evaluate_table(item_info, last_item_info):
    """专业分类数据

    Arguments:
        item_info {dataframe} -- 当前月份数据
        last_item_info {dataframe} -- 上月数据

    Returns:
        dict -- 结果
    """
    item_info = item_info[item_info['MAJOR'].isin(['工务'])]
    last_item_info = last_item_info[last_item_info['MAJOR'].isin(['工务'])]
    major_item_rst = []
    for major in ['工务']:
        major_info = item_info[item_info['MAJOR'] == major]
        major_month_info = last_item_info[last_item_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_last_count = major_month_info.shape[0]
        major_ring = calc_ratio(major_count, major_last_count)
        major_item_rst.append({
            'name': major,
            'count': major_count,
            'ring': major_ring
        })
    total = item_info.shape[0]
    last_total = last_item_info.shape[0]
    total_ring = calc_ratio(total, last_total)
    major_item_rst.append({'name': '合计', 'count': total, 'ring': total_ring})
    return major_item_rst


def _get_cadre_total_score_table(year_data, now_data):
    """获取干部总体得分表格数据

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict  -- jieguo
    """
    year_data['level'] = year_data['SCORE'].apply(evaluate_deduct_level)
    now_data['level'] = now_data['SCORE'].apply(evaluate_deduct_level)
    levels = []
    year_counts = []
    last_counts = []
    for level in range(1, 8):
        levels.append(EVALUATE_DEDUCT_LEVEL_NAMES.get(level))
        year_counts.append(year_data[year_data['level'] == level].shape[0])
        last_counts.append(now_data[now_data['level'] == level].shape[0])
    year_table = {
        'total': year_data.shape[0],
        'levels': levels,
        'counts': year_counts,
        'last_counts': last_counts
    }
    return year_table


def _get_punish_content(year_data, now_data):
    """获取处理结果信息

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict -- 结果
    """
    year_punish = {
        'year_count': year_data[year_data['SCORE'] >= 2].shape[0],
        'month_count': now_data[now_data['SCORE'] >= 2].shape[0],
        'most_score': year_data.at[0, 'SCORE'],
        'most_person': year_data.at[0, 'RESPONSIBE_PERSON_NAME'],
        'most_station': year_data.at[0, 'STATION'],
        'most_job': year_data.at[0, 'JOB'],
    }
    return year_punish


# -------第二章，第三小节： 局领导履职情况分析 END

# -------第二章，第四/五小节： 正科职干部履职评价简要分析 Start
def _get_branch_level_cadre_situation(level):
    """正副科职履职信息数据

    Arguments:
        level {str} -- 正副科

    Returns:
        dict -- 结果
    """
    evaluate_info = local_data.evaluate_info
    evaluate_year_info = local_data.evaluate_year_info
    majors = ['工务']
    if level == '副科级':
        levels = ['正处级', '副处级', '正科级']
        level_info = evaluate_info[(~evaluate_info['LEVEL'].isin(levels))
                                   & (evaluate_info['MAJOR'].isin(majors))]
    else:
        level_info = evaluate_info[(evaluate_info['LEVEL'] == level)
                                   & (evaluate_info['MAJOR'].isin(majors))]
    level_count = level_info.shape[0]
    level_person = len(set(level_info['RESPONSIBE_ID_CARD']))
    problem_info = level_info.groupby(
        ['CODE', 'SITUATION']).size().reset_index().rename(columns={
        0: 'count'
    }).sort_values(
        by='count', ascending=False).head(8)
    problem_rst = [
        str(problem_info.at[index, 'SITUATION']).strip() for index in problem_info.index
    ]
    major_rst = []
    for major in majors:
        major_info = level_info[level_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_person = len(set(major_info['RESPONSIBE_ID_CARD']))
        major_rst.append({
            "major_name": major,
            "major_count": major_count,
            "major_person": major_person
        })
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    level_info = level_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    high_score_rst = [{
        "name": level_info.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": level_info.at[index, 'STATION'],
        "job": level_info.at[index, 'JOB'],
        "score": level_info.at[index, 'SCORE']
    } for index in level_info.head(5).index]
    year_info = evaluate_year_info[(evaluate_year_info['LEVEL'] == level) & (evaluate_year_info['MAJOR'].isin(majors))]
    year_info = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_cadre_total_score_table(year_info, level_info)
    year_punish = _get_punish_content(year_info, level_info)
    result = {
        "total_count": level_count,
        "total_person": level_person,
        "problem_situation": problem_rst,
        "major_situation": major_rst,
        "high_score_situation": high_score_rst,
        "level_table": year_table,
        "punish_situation": year_punish
    }
    return result


# -------第二章，第四/五小节： 正科职干部履职评价简要分析 End

# -------第三章 ------------------------------------- Start
def _get_third_level_evaluate_analysis():
    start_date, end_date = local_data.start_date, local_data.end_date

    produce_columns = ['ACCIDENT_IDENTIFIED_NUMBER', 'BELONG', 'CATEGORY', 'CLOCK', 'CODE',
                       'DATE', 'DETAIL_CLASS', 'DETAIL_TYPE', 'DIRECT_REASON', 'IS_MATERIALS',
                       'LINE_NAME', 'MAIN_CLASS', 'MAIN_TYPE', 'MAJOR', 'MON', 'NAME',
                       'OVERVIEW', 'PK_ID', 'PONDERANCE_NUMBER', 'PROFESSION', 'RANK',
                       'REASON', 'RESPONSIBILITY_IDENTIFIED', 'RESPONSIBILITY_IDENTIFIED_NAME',
                       'RESPONSIBILITY_NAME', 'RESPONSIBILITY_UNIT', 'RISK_NAME', 'STATUS',
                       'TYPE3', 'TYPE4', 'TYPE5', 'STATION', 'type']
    # 事故故障情况
    accident_datas = _get_data_from_mongo(start_date.strftime('%Y-%m-%d'), end_date.strftime('%Y-%m-%d'),
                                          report_util.get_safety_produce_data, produce_columns)

    check_info_columns = ['DATE', 'MAJOR', 'STATION', 'MON', 'COUNT']
    # 检查信息数据
    info_datas = _get_data_from_mongo(start_date, end_date, report_util.get_check_info_data, check_info_columns)

    problem_columns = ['DATE', 'MAJOR', 'STATION', 'TIMES', 'SERIOUS_COUNT', 'SCORE']
    # 检查问题数据
    pro_datas = _get_data_from_mongo(start_date, end_date,
                                     report_util.get_check_problem_data, problem_columns)
    local_data.accident_datas = accident_datas
    local_data.info_datas = info_datas
    local_data.pro_datas = pro_datas
    major_quality_analysis = _get_major_quality_analysis()
    station_quality_analysis = _get_station_quality_analysis()
    station_leader_analysis = _get_station_leader_analysis(start_date, end_date)
    result = {
        "total_evaluate_analysis": {
            "major_quality_analysis": major_quality_analysis,
            "station_quality_analysis": station_quality_analysis,
            "station_leader_analysis": station_leader_analysis
        }
    }
    return result


# ---- 第三章 1小节检查履职质量分析 整体履职质量分析 业务部履职质量分析
def _get_major_quality_analysis():
    try:
        evaluate_info = local_data.evaluate_info
        leader_evaluate = evaluate_info[evaluate_info.STATION.str.startswith("工务处")]
        major_evaluate_table = _get_evaluate_table(leader_evaluate)

        acc_datas = [
            data[(data['PROFESSION'] == '工务') & (data['MAIN_TYPE'] == 1) & (data['STATION'] == '工务处')]
            for data in local_data.accident_datas
        ]
        err_datas = [
            data[(data['PROFESSION'] == '工务') & (data['MAIN_TYPE'] == 2) & (data['STATION'] == '工务处')]
            for data in local_data.accident_datas
        ]
        problem_datas = [data[data['STATION'] == '工务处'] for data in local_data.pro_datas]
        check_datas = [data[data['STATION'] == '工务处'] for data in local_data.info_datas]
        eva_data = evaluate_info[evaluate_info['STATION'] == '工务处']
        value = _get_acc_err_check_eva_info(
            acc_datas, err_datas, problem_datas, check_datas, eva_data)

        result = {
            "major_evaluate_table": major_evaluate_table,
            "yunshu_accident_situation": value
        }
        return result
    except Exception as ex:
        print(ex)
        return {}


def _get_evaluate_table(evaluate):
    evaluate_way = {
        0: "自动评价",
        1: "逐条评价",
        2: "定期评价",
        3: "阶段评价",
    }
    evaluate = evaluate.sort_values(by='SCORE', ascending=False).head(10)
    major_evaluate_table = []
    for index in evaluate.index:
        value = evaluate.loc[index]
        major_evaluate_table.append({
            "all_name":
                value['ALL_NAME'],
            "name":
                value['RESPONSIBE_PERSON_NAME'],
            "job":
                value['GRADATION'],
            "y_m":
                f"{value['YEAR']}/{value['MONTH']:0>2}",
            "time":
                value['CREATE_TIME'],
            "way":
                evaluate_way[value['EVALUATE_WAY']],
            "content":
                value['EVALUATE_CONTENT'],
            "score":
                value['SCORE']
        })
    return major_evaluate_table


def _get_acc_err_check_eva_info(acc_datas, err_datas, pro_datas, info_datas,
                                eva_data):
    # 事故比较情况
    acc_counts = [data.shape[0] for data in acc_datas]
    acc_total = acc_counts[0]
    acc_ratio = calculate_yoy_and_mom_statistic(acc_counts[0], acc_counts[1], acc_counts[2])
    # 故障比较情况
    err_counts = [data.shape[0] for data in err_datas]
    err_total = err_counts[0]
    err_ratio = calculate_yoy_and_mom_statistic(err_counts[0], err_counts[1], err_counts[2])
    # 检查信息比较情况
    info_counts = [sum(list(data['COUNT'])) for data in info_datas]
    info_total = info_counts[0]
    info_ratio = calculate_yoy_and_mom_statistic(info_counts[0], info_counts[1], info_counts[2])
    # 检查问题比较情况
    pro_counts = [sum(list(data['TIMES'])) for data in pro_datas]
    pro_total = pro_counts[0]
    pro_ratio = calculate_yoy_and_mom_statistic(pro_counts[0], pro_counts[1], pro_counts[2])
    # 严重问题比较情况
    pro_ser_counts = [sum(list(data['SERIOUS_COUNT'])) for data in pro_datas]
    pro_ser_total = pro_ser_counts[0]
    pro_ser_ratio = calculate_yoy_and_mom_statistic(pro_ser_counts[0], pro_ser_counts[1], pro_ser_counts[2])
    # 检查问题质量分
    pro_score_counts = [sum(list(data['SCORE'])) for data in pro_datas]
    pro_score_total = pro_score_counts[0]
    pro_score_ratio = calculate_yoy_and_mom_statistic(pro_score_counts[0], pro_score_counts[1], pro_score_counts[2])
    # 履职评价信息
    total = eva_data.shape[0]
    values = [
        eva_data[eva_data['ITEM_NAME'] == item].shape[0] for item in EVALUATE_PROBLEM_TYPE_NAMES
    ]
    value = {
        "acc_total": acc_total,
        "acc_ratio": acc_ratio,
        "err_total": err_total,
        "err_ratio": err_ratio,
        "info_total": info_total,
        "info_ratio": info_ratio,
        "pro_total": pro_total,
        "pro_ratio": pro_ratio,
        "pro_ser_total": pro_ser_total,
        "pro_ser_ratio": pro_ser_ratio,
        "pro_score_total": pro_score_total,
        "pro_score_ratio": pro_score_ratio,
        "evaluate": {
            "total":
                total,
            "value": [{
                "name": name,
                "count": values[idx]
            } for idx, name in enumerate(EVALUATE_PROBLEM_TYPE_NAMES)]
        }
    }
    return value


# ---第三章 2小节 站段履职质量分析
def _get_station_quality_analysis():
    evaluate_info = local_data.evaluate_info
    res_acc_datas = [
        data[(data['MAIN_TYPE'] == 1) & (data['RESPONSIBILITY_NAME'] == 1) & (data['STATION'] != '工务处')]
        for data in local_data.accident_datas
    ]
    res_err_datas = [data[(data['MAIN_TYPE'] == 2) & (data['STATION'] != '工务处')] for data in local_data.accident_datas]
    major_rst = []
    # for major in ['工务']:
    # major_acc_datas = [
    #     data[data['STATION'] != '工务处'] for data in res_acc_datas
    # ]
    # major_err_datas = [
    #     data[data['STATION'] != '工务处'] for data in res_err_datas
    # ]
    major_acc_datas = res_acc_datas
    major_err_datas = res_err_datas
    sta_acc = list(set(major_acc_datas[0]['STATION']))
    # limit = 3
    # if major == '工务':
    #     limit = 5
    # sta_err = major_err_datas[0]['STATION'].value_counts().head(limit)
    sta_err = major_err_datas[0]['STATION'].value_counts().head(5)
    stations = list(set(sta_acc + list(sta_err.index)))
    station_list = []
    for station in stations:
        sta_info = [
            data[data['STATION'] == station] for data in local_data.info_datas
        ]
        sta_pro = [data[data['STATION'] == station] for data in local_data.pro_datas]
        sta_acc_datas = [
            data[(data['STATION'] == station)] for data in major_acc_datas
        ]
        sta_err_datas = [
            data[data['STATION'] == station] for data in major_err_datas
        ]
        sta_evaluate = evaluate_info[evaluate_info['STATION'] == station]
        value = _get_acc_err_check_eva_info(
            sta_acc_datas, sta_err_datas, sta_pro, sta_info, sta_evaluate)
        station_list.append({"station": station, "value": value})

    return {'major': '工务', 'station_list': station_list}


def _get_health_index(month):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


# 第三章 3小节 站段领导班子整体检查质量分析
def _get_station_leader_analysis(start_date, end_date):
    evaluate_info = local_data.evaluate_info
    juguan_evaluate = evaluate_info[evaluate_info['BUSINESS_CLASSIFY'] == '领导']
    table, description = _get_cadre_quality_analysis(start_date, end_date)
    juguan_leader_evaluate_table = _get_evaluate_table(juguan_evaluate)
    result = {
        "station_cadre_quality_analysis": table,
        "station_cadre_quality_analysis_desc": description,
        "juguan_leader_evaluate_table": juguan_leader_evaluate_table
    }
    return result


def _get_cadre_quality_analysis(start_date, end_date):
    evaluate_info = local_data.evaluate_info
    # 领导班子情况
    station_carde_count = pd_query(STATION_CADRE_COUNT_SQL)
    # 检查信息情况
    check_info = pd_query(LEADER_CHECK_INFO_SQL.format(start_date, end_date))
    check_info = check_info[check_info['ALL_NAME'].str.contains('领导')]
    check_info = check_info.fillna(0)
    set_keys = ['CHECK_WAY', 'IS_YECHA', 'PROBLEM_NUMBER', 'COST_TIME']
    check_info = check_info.set_index(set_keys)['ALL_NAME'].str.split(
        ',', expand=True).stack().reset_index().rename(columns={0: 'ALL_NAME'})
    check_info['WAY'] = check_info['CHECK_WAY'].apply(
        lambda x: 1 if x == 1 else 0)
    check_info['STATION'] = check_info['ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del check_info['CHECK_WAY'], check_info['ALL_NAME']
    check_info['CHECK_COUNT'] = 1
    check_data = check_info.groupby('STATION').sum().reset_index()
    data = pd.merge(station_carde_count, check_data, how='left', on='STATION')
    data = data[(~data['WAY'].isnull()) & (~data.STATION.str.contains('处')) & (~data.STATION.str.contains('所'))]
    # 当前月份
    date = end_date
    if date.day >= current_app.config.get('UPDATE_DAY'):
        date = date + relativedelta(months=1)
    month = date.year * 100 + date.month
    # 履职报告得分
    eva_report_data = get_evaluate_report_data(month)
    if eva_report_data.empty is False:
        eva_report_data = eva_report_data.fillna(value={'BUSINESS_CLASSIFY': 'a'})
        eva_report_data = eva_report_data.drop_duplicates('ID_CARD')
        del eva_report_data['ID_CARD']
        eva_report_data = eva_report_data[
            ~(eva_report_data['BUSINESS_CLASSIFY'].isnull())
            & (eva_report_data.BUSINESS_CLASSIFY.str.contains('领导'))]
        evaluate_score = eva_report_data.groupby('STATION').sum().reset_index()
        data = pd.merge(data, evaluate_score, how='left', on='STATION')
        data['score_aver'] = data.apply(
            lambda x: round(x['score'] / x['TOTAL'], 1), axis=1)
        data['score_rank'] = data.groupby('MAJOR')['score_aver'].rank()
    else:
        data['score_aver'] = 0
        data['score'] = 0
        data['score_rank'] = 0
    # 安全综合指数得分
    health_data = _get_health_index(month)
    if health_data.empty is False:
        health_data = health_data.rename(
            columns={
                'RANK': 'HEALTH_RANK',
                'DEPARTMENT_NAME': 'STATION',
                'SCORE': 'HEALTH_SCORE'
            })
        data = pd.merge(data, health_data, how='left', on='STATION')
    else:
        data['HEALTH_RANK'] = 0
        data['HEALTH_SCORE'] = 0
    # 检查问题情况
    problem_data = pd_query(LEADER_CHECK_PROBLEM_SQL.format(start_date, end_date))
    problem_data = problem_data.set_index(
        ['LEVEL', 'PROBLEM_SCORE'])['ALL_NAME'].str.split(
        ',',
        expand=True).stack().reset_index().rename(columns={0: 'ALL_NAME'})
    problem_data['HIGH_PROBLEM'] = problem_data['LEVEL'].apply(
        lambda x: 1 if x in 'ABCE1E2E3' else 0)
    problem_data['STATION'] = problem_data['ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del problem_data['ALL_NAME']
    problem_data = problem_data.groupby('STATION').sum().reset_index()
    data = pd.merge(data, problem_data, how='left', on='STATION')
    # 履职评价详情
    evaluate_data = evaluate_info[
        (~evaluate_info['BUSINESS_CLASSIFY'].isnull())
        & (evaluate_info.BUSINESS_CLASSIFY.str.contains('领导'))]
    # result = []
    major_rst = []
    major_desc_rst = []
    data = data.fillna(0)
    for major in ['工务']:
        desc_rst = []
        major_data = data[data['MAJOR'] == major].copy()
        # if str(set(major_data['HEALTH_SCORE'])):
        major_data = major_data.sort_values(by='HEALTH_SCORE')
        major_total = sum(list(data['TOTAL']))
        major_check_aver = int(sum(list(data['CHECK_COUNT'])) / major_total)
        major_dy_aver = round(sum(list(data['COST_TIME'])) / major_total, 1)
        major_problem_aver = round(
            sum(list(data['PROBLEM_NUMBER'])) / major_total, 1)
        major_yecha_aver = int(sum(list(data['IS_YECHA'])) / major_total)
        major_high_aver = int(sum(list(data['HIGH_PROBLEM'])) / major_total)
        major_proscore_aver = round(
            sum(list(data['PROBLEM_SCORE'])) / major_total, 1)
        del major_data['level_4'], major_data['level_2']
        a = 1
        for index in major_data.index:
            value = major_data.loc[index].copy()
            station = value['STATION']
            value['CHECK_AVER'] = int(value['CHECK_COUNT'] / value['TOTAL'])
            value['CHECK_DIFF'] = value['CHECK_AVER'] - major_check_aver
            value['DY_AVER'] = int(value['COST_TIME'] / value['TOTAL'])
            value['DY_DIFF'] = value['DY_AVER'] - major_dy_aver
            value['PROBLEM_AVER'] = int(
                value['PROBLEM_NUMBER'] / value['TOTAL'])
            value['PROBLEM_DIFF'] = value['PROBLEM_AVER'] - major_problem_aver
            value['YECHA_AVER'] = int(value['IS_YECHA'] / value['TOTAL'])
            value['YECHA_DIFF'] = value['YECHA_AVER'] - major_yecha_aver
            value['HIGH_PRO_AVER'] = int(
                value['HIGH_PROBLEM'] / value['TOTAL'])
            value['HIGH_PRO_DIFF'] = value['HIGH_PRO_AVER'] - major_high_aver
            value['PRO_SCORE_AVER'] = int(
                value['PROBLEM_SCORE'] / value['TOTAL'])
            value['PRO_SCORE_DIFF'] = value[
                                          'PRO_SCORE_AVER'] - major_proscore_aver
            major_rst.append({
                "station": station,
                "value": json.loads(value.T.to_json())
            })
            # 履职情况
            sta_eva = evaluate_data[evaluate_data['STATION'] == station]
            if sta_eva.empty:
                eva_total = 0
                eva_score_total = 0
                luju = 0
                luju_eva_rst = []
                ziping = 0
                ziping_eva_rst = []
            else:
                eva_total = sta_eva.shape[0]
                eva_score_total = sum(list(sta_eva['SCORE']))
                luju_eva = sta_eva[sta_eva['CHECK_TYPE'] == 1]
                if luju_eva.empty:
                    luju_eva_rst = []
                    luju = 0
                else:
                    luju = luju_eva.shape[0]
                    luju_eva = luju_eva.groupby('CODE_ADDITION').size(
                    ).rename(columns={0: 'count'})
                    luju_eva_rst = [{
                        "CODE": index,
                        "COUNT": int(luju_eva.at[index])
                    } for index in luju_eva.index]
                ziping_eva = sta_eva[sta_eva['CHECK_TYPE'] == 2]
                if ziping_eva.empty:
                    ziping_eva_rst = []
                    ziping = 0
                else:
                    ziping = ziping_eva.shape[0]
                    ziping_eva = ziping_eva.groupby('CODE_ADDITION').size(
                    ).rename(columns={0: 'count'})
                    ziping_eva_rst = [{
                        "CODE": index,
                        "COUNT": int(ziping_eva.at[index])
                    } for index in ziping_eva.index]
            desc_rst.append({
                "station": station,
                "value": json.loads(value.T.to_json()),
                "eva_value": {
                    "total": eva_total,
                    "score_total": eva_score_total,
                    "luju": luju,
                    "luju_value": luju_eva_rst,
                    "ziping": ziping,
                    "ziping_value": ziping_eva_rst
                }
            })

        major_desc_rst.append({"major": major, "value": desc_rst})
    return major_rst, major_desc_rst


# -------第三章 ------------------------------------- END


# -------第四章 ----------------------------------- START
def _get_forth_sys_check_and_problem_analysis():
    major_analysis = _get_sys_analysis_of_major()
    admin_dept_analysis = _get_sys_analysis_of_admin_dept()
    result = {
        'major_analysis': major_analysis,
        'admin_dept_analysis': admin_dept_analysis
    }
    return result


# 第一小节 1.工务系统6月份开展
def _get_sys_analysis_of_major():
    check_data = local_data.check_info_data
    # 检查X次，环比Y
    major_check_count = check_data.shape[0]
    major_check_ratio_month = calc_ratio(major_check_count, local_data.check_info_data_mom.shape[0])

    # 查处问题X个，环比上升X%
    major_problem_count = int(check_data['PROBLEM_NUMBER'].sum())
    major_problem_ratio_month = calc_ratio(major_problem_count,
                                           local_data.check_info_data_mom['PROBLEM_NUMBER'].sum())

    # 查处性质严重问题X，环比
    problem_data = local_data.check_problem_data
    problem_data_mon = local_data.check_problem_data_mom
    ser_pro_data = problem_data[problem_data['RISK_LEVEL'].isin([1, 2])]
    serious_problem_count = ser_pro_data.shape[0]
    serious_problem_ratio_month = calc_ratio(serious_problem_count,
                                             problem_data_mon[problem_data_mon['RISK_LEVEL'].isin([1, 2])].shape[0])

    # 其中作业问题 ....
    cly_gb = problem_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PK_ID', ascending=False)
    classify_list = [
        {
            'classify_name': row.Index,
            'count': row.PK_ID
        } for row in cly_gb.itertuples()
    ]

    # 高质量问题查处较高 LEVEL
    high_lv_data = problem_data[problem_data['LEVEL'].isin(CHECK_PROBLEM_HIGH_LEVEL)]
    hl_gb = high_lv_data.groupby('STATION').count().sort_values(by='PK_ID', ascending=False)
    high_level_head_list = [
        {
            'station': row.Index,
            'count': row.PK_ID
        } for row in hl_gb.head(3).itertuples()
    ]

    hl_gb.sort_values(by='PK_ID', inplace=True)
    high_level_tail_list = [
        {
            'station': row.Index,
            'count': row.PK_ID
        } for row in hl_gb.head(3).itertuples()
    ]

    result = {
        'major_check_count': major_check_count,
        'major_check_ratio_month': major_check_ratio_month,
        'major_check_ratio_month_text': get_percent_text(major_check_ratio_month),
        'major_problem_count': major_problem_count,
        'major_problem_ratio_month': major_problem_ratio_month,
        'major_problem_ratio_month_text': get_percent_text(major_problem_ratio_month),
        'serious_problem_count': serious_problem_count,
        'serious_problem_ratio_month': serious_problem_ratio_month,
        'serious_problem_ratio_month_text': get_percent_text(serious_problem_ratio_month),
        'classify_list': classify_list,
        'high_level_head_list': high_level_head_list,
        'high_level_tail_list': high_level_tail_list
    }
    return result


# 第二小节 2.工务部X月份开展检查
def _get_sys_analysis_of_admin_dept():
    """
    工务部的分析段落
    :return:
    """
    check_data = local_data.check_info_data
    check_data = check_data[check_data['STATION'] == '工务处'].copy()
    check_info_data_mom = local_data.check_info_data_mom
    check_info_data_mom = check_info_data_mom[check_info_data_mom['STATION'] == '工务处']
    # 检查X次，环比Y
    major_check_count = check_data.shape[0]
    major_check_ratio_month = calc_ratio(major_check_count, check_info_data_mom.shape[0])

    # 查处问题X个，环比上升X%
    major_problem_count = int(check_data['PROBLEM_NUMBER'].sum())
    major_problem_ratio_month = calc_ratio(major_problem_count,
                                           local_data.check_info_data_mom['PROBLEM_NUMBER'].sum())

    # 查处性质严重问题X，环比
    problem_data = local_data.check_problem_data
    problem_data = problem_data[problem_data['STATION'] == '工务处'].copy()
    problem_data_mon = local_data.check_problem_data_mom
    problem_data_mon = problem_data_mon[problem_data_mon['STATION'] == '工务处']
    ser_pro_data = problem_data[problem_data['RISK_LEVEL'].isin([1, 2])]
    serious_problem_count = ser_pro_data.shape[0]
    serious_problem_ratio_month = calc_ratio(serious_problem_count,
                                             problem_data_mon[problem_data_mon['RISK_LEVEL'].isin([1, 2])].shape[0])

    # 其中作业问题 ....
    cly_gb = problem_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PK_ID', ascending=False)
    classify_list = [
        {
            'classify_name': row.Index,
            'count': row.PK_ID
        } for row in cly_gb.itertuples()
    ]

    # 关键数据统计表
    problem_levels = ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'G1', 'G2', 'G3',
                      'K1', 'K2', 'K3', 'K4']
    check_data['SECTION_NAME'] = check_data['CHECK_DEPT_NAME'].apply(extract_section_from_all_name)
    problem_data['SECTION_NAME'] = problem_data['CHECK_DEPT_NAME'].apply(extract_section_from_all_name)

    # 工务处所有科室
    dp = local_data.dept_data
    dp = dp[(dp['DEPT_TYPE'] == 6) & (dp['STATION'] == '工务处')].copy()
    dp['SECTION_SHORT'] = dp['ALL_NAME'].apply(lambda name: name.split('-')[-1])
    key_stat_rows = []
    for row in dp.itertuples():
        sub_pro_data = problem_data[problem_data['SECTION_NAME'] == row.ALL_NAME]
        pro_level_list = [
            len(sub_pro_data[sub_pro_data['LEVEL'] == level]) for level in problem_levels
        ]
        item = {
            'section': row.SECTION_SHORT,
            'check_count': check_data[check_data['SECTION_NAME'] == row.ALL_NAME].shape[0],
            'pro_level_list': pro_level_list,
            'pro_total': sum(pro_level_list)
        }
        key_stat_rows.append(item)
    pro_level_list = [
        len(problem_data[problem_data['LEVEL'] == level]) for level in problem_levels
    ]
    key_stat_rows.append(
        {
            'section': '合计',
            'check_count': check_data.shape[0],
            'pro_level_list': pro_level_list,
            'pro_total': len(problem_data)
        }
    )

    result = {
        'major_check_count': major_check_count,
        'major_check_ratio_month': major_check_ratio_month,
        'major_check_ratio_month_text': get_percent_text(major_check_ratio_month),
        'major_problem_count': major_problem_count,
        'major_problem_ratio_month': major_problem_ratio_month,
        'major_problem_ratio_month_text': get_percent_text(major_problem_ratio_month),
        'serious_problem_count': serious_problem_count,
        'serious_problem_ratio_month': serious_problem_ratio_month,
        'serious_problem_ratio_month_text': get_percent_text(serious_problem_ratio_month),
        'classify_list': classify_list,
        'key_stat_headers': problem_levels,
        'key_stat_rows': key_stat_rows
    }
    return result


# -------第四章 ------------------------------------- END

# -------第五章 ----------------------------------- START
def _get_fifth_station_health_index_radar():
    end_date = local_data.end_date
    mon = end_date.year * 100 + end_date.month
    index_list = list(mongo.db['monthly_detail_health_index'].find({'MAJOR': '工务', 'MON': mon,
                                                                    'DETAIL_TYPE': 0, 'HIERARCHY': 3}))
    health_index = pd.DataFrame(index_list)
    gongwu_list = _get_station_health_index_radar_content(health_index)

    index_list = list(mongo.db['monthly_detail_health_index'].find({'MAJOR': '工电', 'MON': mon,
                                                                    'DETAIL_TYPE': 0, 'HIERARCHY': 3}))
    health_index = pd.DataFrame(index_list)
    gongdian_list = _get_station_health_index_radar_content(health_index)

    result = {
        'gongwu_list': gongwu_list,
        'gongdian_list': gongdian_list
    }
    return result


def _get_station_health_index_radar_content(health_index):
    if health_index is None or health_index.empty:
        return []
    station_data = health_index['DEPARTMENT_NAME'].unique().tolist()

    # sub_tags 数据的标题，一共三组数据
    sub_tags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
    main_types = HEALTH_INDEX_MAIN_TYPES

    # labels，就是各个维度的名称，如检查力度指数，检查均衡度，评价力度。。。
    labels = [get_index_title(type_item) for type_item in main_types]
    best_list, avg_list = [], []
    for type_item in main_types:
        avg_list.append(round(health_index[health_index['MAIN_TYPE'] == type_item]['SCORE'].mean(), 2))
        best_list.append(round(health_index[health_index['MAIN_TYPE'] == type_item]['SCORE'].max(), 2))

    station_list = []
    # 迭代站段清单
    for station in station_data:
        better_list = []
        worse_list = []
        station_df = health_index[health_index['DEPARTMENT_NAME'] == station]
        station_score = []
        idx = 0
        # 站段的子指数分数大于平均值为，较好，否则为不足
        for type_item in main_types:
            score = station_df[station_df['MAIN_TYPE'] == type_item]['SCORE'].max()
            station_score.append(score)
            if score >= avg_list[idx]:
                better_list.append(get_index_title(type_item))
            else:
                worse_list.append(get_index_title(type_item))
            idx = idx + 1

        station_data = [station_score, best_list, avg_list]
        title = f'{station}安全管理综合指数雷达图'
        time_str = local_data.end_date.strftime("%Y%m%d")
        file_name = f'{title}_{time_str}.png'

        radar_data = {
            'labels': labels,
            'station_data': station_data,
            'title': title,
            'sub_tags': sub_tags,
            'file_name': file_name
        }
        radar_src = _save_radar_pic(labels, station_data, title, sub_tags, file_name)

        content = {
            'station_name': station,
            'better_list': better_list,
            'worse_list': worse_list,
            'radar_data': radar_data,
            # 'radar_pic': InlineImage(local_data.template, radar_src, width=Mm(120))
        }
        station_list.append(content)

    return station_list


def _create_fig(labels, data, title, sub_tags):
    from app.data.font import FONT_STYLE_FILE
    ymajorLocator = MultipleLocator(0.1)  # 将y轴主刻度标签设置为0.5的倍数
    n = len(labels)

    angles = np.linspace(0, 2 * np.pi, n, endpoint=False)  # 旋转90度，从正上方开始！
    angles = np.concatenate((angles, [angles[0]]))  # 闭合

    fig = Figure(figsize=(12, 9))
    ax = fig.add_subplot(111, polar=True)  # 参数polar，表示极坐标！！
    ax.yaxis.set_major_locator(ymajorLocator)
    # 自己画grid线（5条环形线）
    for i in [30, 60, 90, 100]:
        ax.plot(angles, [i] * (n + 1), '-',
                c="#BDC3C7", lw=0.5)  # 之所以 n +1，是因为要闭合！

    # 填充底色
    ax.fill(angles, [100] * (n + 1), facecolor='w', alpha=0.5)

    # 自己画grid线（6条半径线）
    for i in range(n):
        ax.plot([angles[i], angles[i]], [0, 100], '-', c="#BDC3C7", lw=0.5)
    colors = ['#f0a626', '#1d97fc', '#2fc75c', 'yellow', 'black']
    # 画线
    for pos, item in enumerate(data):
        item = np.array(item)
        item = np.concatenate((item, [item[0]]))  # 闭合
        if pos == 0:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{sub_tags[pos]}')
            ax.fill(angles, item, c=colors[pos], alpha=0.2)
        else:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{sub_tags[pos]}')
    # 填充
    ax.tick_params(axis='both', direction='out', pad=27)  # 刻度跟标签的距离
    ax.legend(loc='best', prop=FontProperties(fname=FONT_STYLE_FILE),
              fontsize=16, bbox_to_anchor=(1.05, 1.0), borderaxespad=0.)
    ax.set_thetagrids(angles * 180 / np.pi, labels, fontproperties=FontProperties(fname=FONT_STYLE_FILE),
                      fontsize=16)
    ax.set_title("{0}指数雷达图".format(title), va='bottom',
                 fontproperties=FontProperties(fname=FONT_STYLE_FILE), fontsize=24)
    ax.set_rlim(0, 100)
    # 下两行去掉所有默认的grid线
    ax.spines['polar'].set_visible(False)  # 去掉最外围的黑圈
    ax.grid(False)  # 去掉中间的黑圈
    # 关闭数值刻度
    ax.set_yticks([])
    return fig


# 雷达图
def _save_radar_pic(labels, data, title, sub_tags, file_name):
    import os
    # 获取项目根目录路径
    dir_path = os.path.join(manager.get_report_path('quarterly', '工务'), 'images')
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    pic_path = os.path.join(dir_path, file_name)
    if os.path.exists(pic_path):
        os.remove(pic_path)
    labels = np.array(labels)
    fig = _create_fig(labels, data, title, sub_tags)
    fig.savefig(pic_path)
    return pic_path


# -------第五章 ------------------------------------- END

# -------第六章 ----------------------------------- START
def _get_sixth_violation_person_analysis():
    result = {
        'violation_distribution': _get_violation_person_distribution(),
        'violation_rectification': _get_violation_person_rectification()
    }
    return result


# X月“违章大王”分布情况
def _get_violation_person_distribution():
    year = local_data.end_date.year
    # 加载违章人员清单
    person_data = pd_query(VIOLATION_PERSON_LIST_SQL3.format(year))
    person_data['STATION'] = person_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])
    # 计算每个站段的人数
    person_gb = person_data.groupby(['STATION']).count()
    # 按站段分布输出
    advice_list = [
        {
            'station_name': row.Index,
            'count': row.ID_CARD
        } for row in person_gb.itertuples()
    ]

    # 加载确认的人员清单
    confirm_data = pd_query(VIOLATION_PERSON_CONFIRM_LIST_SQL3.format(year))
    confirm_data['STATION'] = confirm_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])
    confirm_data.drop_duplicates(['STATION', 'ID_CARD'], inplace=True)
    confirm_gb = confirm_data.groupby(['STATION']).count()
    confirm_list = [
        {
            'station_name': row.Index,
            'count': row.ID_CARD
        } for row in confirm_gb.itertuples()
    ]
    content = {
        'advice_total_count': person_data.shape[0],
        'advice_list': advice_list,
        'confirm_total_count': confirm_data.shape[0],
        'confirm_list': confirm_list
    }

    return content


# X月“违章大王”整改情况
def _get_violation_person_rectification():
    year = local_data.end_date.year
    ref_data = pd_query(VIOLATION_PERSON_CONFIRM_LIST_SQL3.format(year))
    ref_data['STATION'] = ref_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])

    # 整改方法，是以ID的清单存在FK_DISPOSE_METHOD_IDS字段里面，将它，变成dataframe的列，以便统计
    for key, val in VIOLATION_PERSON_DISPOSE_METHODS.items():
        field_name = f'METHOD_{key}'
        ref_data[field_name] = 0
    for index in ref_data.index:
        methods = ref_data.loc[index, 'FK_DISPOSE_METHOD_IDS']
        if not methods:
            continue

        for mth_id in methods.split(','):
            field_name = f'METHOD_{mth_id}'
            ref_data.at[index, field_name] = 1

    # 遍历站段, 求每个站段的不同整改方法的人数
    ref_gb = ref_data.groupby(['STATION']).count()
    rectification_list = []
    for row in ref_gb.itertuples():
        method_list = []
        for key, val in VIOLATION_PERSON_DISPOSE_METHODS.items():
            field_name = f'METHOD_{key}'
            if getattr(row, field_name) > 0:
                method_list.append({
                    'method': val,
                    'count': getattr(row, field_name)
                })
        phase = {
            'station_name': row.Index,
            'total': row.ID_CARD,
            'method_list': method_list
        }
        rectification_list.append(phase)
    content = {
        'rectification_list': rectification_list
    }
    return content


# -------第六章 ------------------------------------- END

# START-------------------------------------------- 构建图片对象 -------------------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第五张的雷达图
    fifth = data.get('fifth')
    if fifth:
        station_list = fifth.get('gongwu_list')
        station_list.extend(fifth.get('gongdian_list'))
        for item in station_list:
            radar_data = item['radar_data']
            pic_path = _save_radar_pic(radar_data['labels'], radar_data['station_data'], radar_data['title'],
                                       radar_data['sub_tags'], radar_data['file_name'])
            item['radar_pic'] = InlineImage(template, pic_path, width=Mm(120))


# END---------------------------------------------- 构建图片对象 -------------------------------------------------------

def _get_data_from_mongo(start_date, end_date, func, columns=[]):
    # 当月数据
    data = func(local_data.start_date.strftime('%Y-%m-%d'), local_data.end_date.strftime('%Y-%m-%d'))
    if data.empty:
        data = pd.DataFrame(columns=columns)
    data = data[data['MAJOR'] == '工务']
    # 上月数据
    month_data = func(local_data.start_date_prv.strftime('%Y-%m-%d'), local_data.end_date_prv.strftime('%Y-%m-%d'))
    if month_data.empty:
        month_data = pd.DataFrame(columns=data.columns)
    else:
        month_data = month_data[month_data['MAJOR'] == '工务']

    # 获取去年本月数据
    year_data = func(local_data.start_date_year.strftime('%Y-%m-%d'), local_data.end_date_year.strftime('%Y-%m-%d'))
    if year_data.empty:
        year_data = pd.DataFrame(columns=data.columns)
    else:
        year_data = year_data[year_data['MAJOR'] == '工务']
    return [data, month_data, year_data]
