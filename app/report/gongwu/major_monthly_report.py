"""
计算工务专业的月度分析报告
Created: 2019-08-05
@author: Lu Jionglin
"""
import logging
import os
from flask import current_app

from docxtpl import DocxTemplate
from . import major_monthly_report_data as report_data
from app import mongo
import app.report.analysis_report_manager as manager


def generate_report_data(year, month):
    """
    执行生成指定月份的月报的数据，并件数据持久化
    :param year: int
    :param month: int
    :return:
    """
    data = report_data.get_data(year, month)
    mongo.db['safety_analysis_monthly_report'].insert_one(data)
    return data


def export_report(year, month):
    """
    导出指定月份的报表，并返回报表的存储路径
    :param year: {int}年份
    :param month: {int}月份
    :return: {str} 报表的路径字符串
    """
    # 1. 检查年/月，是否有效
    # 加载报表数据，如果数据没生成，调用生成数据
    data = mongo.db['safety_analysis_monthly_report'].find_one(
        {'year': year, 'month': month, 'report_class': 'MAJOR'}, {'_id': 0})
    if data:
        pass
    else:
        data = generate_report_data(year, month)
    # 通过模板渲染报表文件
    tpl = DocxTemplate('app/report/template/gongwu_major_monthly_report.docx')
    report_data.insert_images(data, tpl)
    tpl.render(data)
    # 报告保存
    file_path = os.path.join(manager.get_report_path('monthly', '工务'), data['file_name'])
    tpl.save(file_path)
    return file_path


def execute(update_month):
    """计算全履职得分调用函数
    """
    month = update_month.year_month % 100
    year = update_month.year_month // 100
    try:
        export_report(year, month)
        current_app.logger.debug(f'({month}) to mongo is done!!!')
        return 'OK'
    except Exception as ex:
        logging.exception('Analysis Report Error:')
        logging.exception(str(ex))
        current_app.logger.debug(f'analysis_report {month} is wrong')
        return f'{update_month.year_month} ERROR'


class GongwuMajorMonthlyAnalysisReport(manager.MonthlyAnalysisReport):
    """
    工务的专业级的月分析报告类。
    """

    def __init__(self):
        super(GongwuMajorMonthlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='工务')

    def generate_report_data(self, year, month):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param month: int 月
        :return:
        """
        data = report_data.get_data(year, month)
        manager.restore_numpy_value(data)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "month": month,
                "hierarchy": "MAJOR",
                'major': '工务'
            })
        mongo.db['safety_analysis_monthly_report'].insert_one(data)
        return data

    def generate_report(self, year, month):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param month: {int} 月份
        :return: {tuple} 文件目录, 文件名
        """
        data = self.load_report_data(year, month)
        tpl = DocxTemplate('app/report/template/gongwu_major_monthly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']


