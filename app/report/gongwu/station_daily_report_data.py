import pandas as pd
import numpy as np
from datetime import datetime as dt
from flask import current_app
from app.data.util import pd_query
from app.report.gongwu.station_daily_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find


def get_data(times, work_sheet, dp_id):
    start_date = times[0]
    end_date = times[1]
    month = end_date.split('-')[1]
    day = end_date.split('-')[2]
    station_name = pd_query("select NAME from t_department where department_id = '{0}'".format(dp_id))['NAME'][0]
    work_sheet['A1'] = '中国铁路成都局集团有限公司{0}安全生产日报表'.format(station_name)
    first = get_first(times, month, day, work_sheet, dp_id)
    second = get_second(times, work_sheet, dp_id)
    third = get_third(times, work_sheet, dp_id)
    four = get_four(times, work_sheet, dp_id)
    return {
        'start_date': start_date,
        'end_date': end_date,
        'title': '中国铁路成都局集团有限公司{0}安全生产日报表'.format(station_name),
        'first': first,
        'second': second,
        'third': third,
        'four': four
    }


def get_first(times, month, day, worksheet, dp_id):
    # 安全情况
    first_one = get_first_one(times, month, day, worksheet, dp_id)
    # 事故故障情况
    first_two = get_first_two(times, month, day, worksheet, dp_id)
    return {
        'first_one': first_one,
        'first_two': first_two
    }


def get_first_one(times, month, day, worksheet, dp_id):
    """
    安全生产情况
    :param times:
    :param month:
    :param day:
    :param worksheet:
    :return:
    """
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format(times[1], dp_id))
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
        if len(accident) == 0:
            dic['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    content = '截至{0}月{1}日：全段实现无责任一般A类事故{2}天;无责任一般B类事故{3}天;无责任一般C类事故{4}天;' \
              '无责任一般D类事故{5}天。全段实现安全生产{6}天。'.format(month, day, dic['A'], dic['B'], dic['C'], dic['D'], dic['A'])
    worksheet['B5'] = content
    return content


def get_first_two(times, month, day, worksheet, dp_id):
    infos = []
    start_date = int(''.join(times[0].split('-')))
    end_date = int(''.join(times[1].split('-')))
    # 获取安全责任数据
    documents = calc_safety_produce_data(start_date, end_date, dp_id)
    if len(documents) == 0:
        content = "{0}月{1}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障0件。其中信号故障0件、机车故障0件、动车组故障0件。" \
                  "\n5.综合信息：发生综合信息0件，其中涉及工务0件。" \
                  "\n6.工务安全生产情况：监测报警0次，其中正常报警0次，误报0次。线路动态质量报警Ⅲ0处、Ⅳ0处。".format(month, day)
        info = {
            'acc': '无',
            'fault': '无'
        }
        infos.append(info)
    else:
        data = pd.DataFrame(documents)
        data = data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
        acc = len(data[data['MAIN_CLASS'] == '事故'])
        fault = len(data[data['MAIN_CLASS'] == '故障'])
        zonghe = len(data[data['MAIN_CLASS'] == '综合'])
        gongwu = len(data[(data['MAIN_CLASS'] == '综合') & (data['MAJOR'] == '工务')])
        # 行车、劳动、路外
        safety_list = []
        for i in ['行车', '劳动', '路外']:
            new_data = data[data['RISK_NAME'].str.contains(i)]
            dic = {
                '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
            }
            safety_list.append(dic)
        # 设备故障
        new_data = data[data['MAIN_CLASS'] == '故障']
        safety_list.append({
            '设备故障': fault,
            '信号故障': len(new_data[new_data['NAME'] == '信号设备故障']),
            '机车故障': len(new_data[new_data['NAME'] == '机车故障']),
            '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
        })
        content = "{0}月{1}日，发生事故{2}件、故障{3}件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障{4}件。其中信号故障{5}件、机车故障{6}件、动车组故障{7}件。" \
                  "\n5.综合信息：发生综合信息{8}件，其中涉及工务{9}件。" \
                  "\n6.工务安全生产情况：监测报警0次，其中正常报警0次，误报0次。线路动态质量报警Ⅲ0处、Ⅳ0处。". \
            format(month, day, acc, fault, safety_list[-1]['设备故障'], safety_list[-1]['信号故障'],
                   safety_list[-1]['机车故障'], safety_list[-1]['动车组故障'], zonghe, gongwu)
    worksheet['B6'] = content
    return content


def calc_safety_produce_data(now_date, end_date, dp_id):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param now_date:
    :return:
    """
    keys = {
        "match": {
            "DATE": {
                '$lte': end_date,
                '$gte': now_date
            },
            "TYPE3": dp_id
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    return documents


def get_second(times, worksheet, dp_id):
    """
    监督检查基本情况
    :param times:
    :param worksheet:
    :return:
    """
    # 检查信息
    check_info = pd_query(check_info_sql.format(times[0], times[1], dp_id))
    # 检查问题
    check_problem = pd_query(check_problem_sql.format(times[0], times[1], dp_id))
    # 检查风险
    check_risk = pd_query(check_risk_sql.format(times[0], times[1], dp_id))
    # 分析中心检查情况
    analysis_center = pd_query(analysis_center_sql.format(times[0], times[1]))
    # 本月检查风险
    month_risk = pd_query(check_risk_sql.format(times[0][:-2] + '01', times[1], dp_id))
    # 所有风险
    risk_data = pd_query(risk_sql)
    # 全局总体检查情况
    second_one = get_second_one(check_info, check_problem, check_risk, month_risk, risk_data, worksheet)
    # 安全分析中心工作基本情况
    second_three = get_second_three(analysis_center, worksheet)
    return {
        'second_one': second_one,
        'second_three': second_three
    }


def get_second_one(check_info, check_problem, check_risk, month_risk, risk_data, worksheet):
    check_problem = check_problem.dropna(subset=['PROBLEM_CLASSITY_NAME'])
    dic = {}
    # 检查基本情况
    xc = len(check_info[check_info['CHECK_WAY'] == 1])
    find_problem = int(check_info['PROBLEM_NUMBER'].sum())
    main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    work_point = []
    # 问题三项
    for i in ['作业', '设备', '管理']:
        data = check_problem[check_problem['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        work_point.append(len(data))
    dic['one'] = '全段下现场检查{0}人次，发现问题{1}个。性质严重问题{2}个，其中作业项{3}个、设备设施项{4}个、管理项{5}个。'. \
        format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2])
    # 重点风险查处情况
    risk = check_risk.dropna(subset=['NAME'])['NAME'].unique().tolist()
    months_risk = month_risk.dropna(subset=['NAME'])['NAME'].unique().tolist()
    risk_names = risk_data.dropna(subset=['NAME'])['NAME'].unique().tolist()
    not_risks = [i for i in risk_names if i not in months_risk]
    number = len(risk)
    if number == 0:
        dic['three'] = '今日检查风险{0}大类。本月还未检查的风险有:{1}'.format(number, ','.join(risk_names))
        dic['two'] = '无'
        dic['four'] = '无数据'
    else:
        dic['three'] = '今日检查风险{0}大类，分别是：{1},本月还未检查的风险有：{2}。'.format(number, ','.join(risk), ','.join(not_risks))
        dic['two'] = '查处典型突出问题：分别是：{0}等主要风险数及严重问题数。'.format(','.join(risk))
        dic['four'] = '无数据'
    worksheet['C9'] = dic['one']
    worksheet['C10'] = dic['two']
    worksheet['C11'] = dic['three']
    worksheet['C12'] = dic['four']
    return dic


def get_second_three(analysis_center, worksheet):
    count = []
    dic = {}
    # 工作量情况
    for i in range(2, 8):
        data = analysis_center[analysis_center['FLAG'] == i]
        count.append(len(data))
    # 问题查处情况
    mv_re = int(analysis_center[analysis_center['FLAG'] == 2]['DAILY_PROBLEM_NUMBER'].sum())
    eva_re = int(analysis_center[analysis_center['FLAG'].isin([5, 6])]['DAILY_PROBLEM_NUMBER'].sum())
    dic['one'] = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，' \
                 '干部履职评价{1}条，干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，' \
                 '阶段评价{5}人次。 问题查处情况：音视频调阅（复查）发现问题{6}个，' \
                 '干部履职评价（复查）发现{7}个问题。'.format(count[0], count[1], count[2], count[3],
                                              count[4], count[5], mv_re, eva_re)
    # 安全问题查处
    problem_data = analysis_center.replace('', np.nan).replace('无', np.nan).dropna(subset=['RISK_WARNING'])
    problem_count = len(problem_data['RISK_WARNING'].unique())
    situation = []
    for i in range(problem_count):
        situation.append(str(i + 1) + '.' + problem_data['RISK_WARNING'].unique().tolist()[i])
    situation = '\n'.join(situation)
    if problem_count == 0:
        dic['two'] = '安全问题查处{0}个,无重大风险问题'.format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()))
    else:
        dic['two'] = "安全问题查处{0}个，其中典型问题有{1}个：". \
                         format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()), problem_count) + situation
    dic['three'] = '无数据'
    worksheet['C13'] = dic['one']
    worksheet['C14'] = dic['two']
    worksheet['C15'] = dic['three']
    return dic


def get_third(times, work_sheet, dp_id):
    """
    重点安全信息追踪
    :param times:
    :param work_sheet:
    :param dp_id:
    :return:
    """
    safety_info = pd_query(main_info_sql.format(times[0], times[1], dp_id))
    # 上级追踪
    info = safety_info[safety_info['HIERARCHY'] == 3]
    infos = info['CONTENT'].head(3).tolist()
    if len(infos) == 0:
        infos = ['无', '无', '无']
    # 站段追踪
    road_info = safety_info[safety_info['HIERARCHY'] == 4]
    road_infos = road_info['CONTENT'].head(1).tolist()
    if len(road_infos) == 0:
        road_infos = ['无']
    index = 17
    index1 = 20
    for i in range(len(infos)):
        infos[i] = str(i + 1) + '.' + infos[i]
        work_sheet['B{0}'.format(index)] = infos[i]
        index += 1
    for i in range(len(road_infos)):
        road_infos[i] = str(i + 1) + '.' + road_infos[i]
        work_sheet['B{0}'.format(index1)] = road_infos[i]
    return {
        'infos': infos,
        'road_infos': road_infos
    }


def get_four(times, work_sheet, dp_id):
    """
    风险预警
    :param times:
    :param work_sheet:
    :return:
    """
    rank_map = {
        1: "I级安全事故警告预警",
        2: "II级差异化精准警告预警",
        3: "III级劳动安全专项警告预警",
        4: "IV其他警告预警"
    }
    warning_data = pd_query(warning_notification_sql.format(times[0], times[1], dp_id))
    # 专业
    road_data = warning_data[warning_data['HIERARCHY'] == 2].head(3)
    company_warn_list = []
    if len(road_data) == 0:
        company_warn_list = ['无']
    else:
        for index in range(0, len(road_data)):
            company_warn_list.append("""{0}.{1}日对{2}开展{3}""".format(
                index + 1,
                times[0],
                road_data["duty_department_name"][index],
                rank_map.get(road_data["RANK"][index])
            ))
    work_sheet['B22'] = '\n'.join(company_warn_list)
    # 站段
    safety_data = warning_data[warning_data['HIERARCHY'] == 3].head(3)
    safety_warn_list = []
    if len(safety_data) == 0:
        safety_warn_list = ['无']
    else:
        for index in range(0, len(safety_data)):
            safety_warn_list.append("""{0}.{1}日对{2}开展{3}""".format(
                index + 1,
                times[0],
                safety_data["duty_department_name"][index],
                rank_map.get(safety_data["RANK"][index])
            ))
    work_sheet['B23'] = '\n'.join(safety_warn_list)
    return {
        'safety_warn_list': safety_warn_list,
        'company_warn_list': company_warn_list
    }



