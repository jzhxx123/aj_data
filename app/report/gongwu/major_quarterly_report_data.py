"""
计算工务专业的月度分析报告的具体数据
Created: 2019-08-05
@author: Lu Jionglin
"""
import re
from datetime import datetime, date
import json
import threading
import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app
import numpy as np
from matplotlib.font_manager import FontProperties
from matplotlib.ticker import MultipleLocator
from matplotlib.figure import Figure
from app.data.util import pd_query
from app.report import util as report_util
from .common_sql import PRODUCE_INFO_ACCIDENT_SQL, CHECK_EVALUATE_INFO_SQL, \
    STATION_CADRE_COUNT_SQL, LEADER_CHECK_INFO_SQL, LEADER_CHECK_PROBLEM_SQL, \
    CHECK_PROBLEM_DATA_SQL, MAJOR_DEPARTMENT_DATA_SQL, check_info_sql
from app import mongo
from app.safety_index.health_index import get_index_title
from docxtpl import InlineImage
from docx.shared import Mm
import app.report.analysis_report_manager as manager
from .common import get_accident_resp_analysis, get_first_accident_type_analysis, \
    get_first_general_safety_type_analysis, get_first_general, get_violation_person_rectification, \
    get_violation_person_distribution, get_top_check_problem, \
    HEALTH_INDEX_MAIN_TYPES, get_evaluate_problem_count_description, \
    get_cadre_evaluate_state_analysis_of_major, get_sys_analysis_of_admin_dept, \
    get_check_info_data, get_major_quality_analysis, get_evaluate_table, \
    get_station_quality_analysis, get_evaluate_gen_state, get_sys_analysis_of_major, \
    get_branch_level_cadre_situation, get_leader_evaluate_situation, calculate_comparison_statistic

local_data = threading.local()


def get_data(year, quarter):
    """
    该月份的获取报表数据
    :param year:
    :param month:
    :return:
    """
    date_params = manager.QuarterlyAnalysisReport.get_quarter_intervals(year, quarter)
    local_data.date_params = date_params
    local_data.year = year
    local_data.quarter = quarter
    local_data.month_time = int(date_params[1][1][:4]) * 100 + int(date_params[1][1][5:7])
    local_data.year_time = int(date_params[2][1][:4]) * 100 + int(date_params[2][1][5:7])
    end_date = date_params[0][1]
    local_data.time = year * 100 + int(end_date[5:7])
    start_date = date_params[0][0]
    start_month, start_day = start_date[5:7], start_date[8:10]
    end_month, end_day = end_date[5:7], end_date[8:10]
    major = '工务'

    common = {
        'period_text': f'第{quarter}季度',
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day
    }

    # 先加载公用的数据
    _load_general_data()

    # 计算各个章节的数据
    first = _get_first_general()
    second = _get_second_major_evaluate_analysis()
    third = _get_third_level_evaluate_analysis()
    fourth = _get_forth_sys_check_and_problem_analysis()
    fifth = _get_fifth_station_health_index_radar()
    sixth = _get_sixth_violation_person_analysis()
    seventh = _get_seventh_classic_problem_analysis()

    result = {
        "year": year,
        "quarter": quarter,
        "hierarchy": "MAJOR",
        "major": major,
        "file_name": f'{year}年第{quarter}季度工务系统安全情况分析报告.docx',
        "created_at": datetime.now(),
        "common": common,
        "first": first,
        "second": second,
        "third": third,
        "fourth": fourth,
        "fifth": fifth,
        "sixth": sixth,
        "seventh": seventh
    }
    return result


def _load_general_data(stage=99):
    """
    加载在整个计算过程中使用的通用的数据
    :return:
    """
    date_params = local_data.date_params
    start_date = datetime.strptime(date_params[0][0], '%Y-%m-%d')
    end_date = datetime.strptime(date_params[0][1], '%Y-%m-%d')
    start_date_prv = datetime.strptime(date_params[1][0], '%Y-%m-%d')
    end_date_prv = datetime.strptime(date_params[1][1], '%Y-%m-%d')
    start_date_year = datetime.strptime(date_params[2][0], '%Y-%m-%d')
    end_date_year = datetime.strptime(date_params[2][1], '%Y-%m-%d')

    local_data.major = '工务'
    local_data.start_date = start_date
    local_data.end_date = end_date
    local_data.start_date_prv = start_date_prv
    local_data.end_date_prv = end_date_prv
    local_data.start_date_year = start_date_year
    local_data.end_date_year = end_date_year
    # year_month = local_data.year * 100 + local_data.month

    # 专业及其下属部门的清单
    df_dept = pd_query(MAJOR_DEPARTMENT_DATA_SQL)
    local_data.dept_data = df_dept

    # -------------- 履职评价信息
    evaluate_info0 = pd_query(CHECK_EVALUATE_INFO_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.evaluate_info_this = evaluate_info0

    # 上月履职评价
    evaluate_info1 = pd_query(
        CHECK_EVALUATE_INFO_SQL.format(date_params[1][0], date_params[1][1]))
    local_data.evaluate_info_prev = evaluate_info1
    # 去年当月履职评价
    evaluate_info2 = pd_query(
        CHECK_EVALUATE_INFO_SQL.format(date_params[2][0], date_params[2][1]))
    local_data.evaluate_info_yoy = evaluate_info2
    # 今年全部履职评价
    year_evaluate_info = pd_query(
        CHECK_EVALUATE_INFO_SQL.format(end_date.strftime('%Y-01-01'), end_date.strftime('%Y-%m-%d')))
    local_data.year_evaluate_info = year_evaluate_info

    if stage == 1:
        return

    # （本月）检查信息
    check_info = pd_query(check_info_sql.format(date_params[0][0], date_params[0][1]))
    local_data.check_info_this = check_info
    # （上月环比)检查信息
    data = pd_query(check_info_sql.format(date_params[1][0], date_params[1][1]))
    local_data.check_info_prev = data
    # （上年同比）检查信息
    data = pd_query(check_info_sql.format(date_params[2][0], date_params[2][1]))

    local_data.check_info_yoy = data

    # （本月）检查的问题
    data = pd_query(CHECK_PROBLEM_DATA_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.check_problem_this = _append_station_name(data, 'STATION_ID')

    # （上月）检查的问题
    data = pd_query(CHECK_PROBLEM_DATA_SQL.format(date_params[1][0], date_params[1][1]))
    local_data.check_problem_prev = _append_station_name(data, 'STATION_ID')
    # （上年同比）检查问题
    data = pd_query(CHECK_PROBLEM_DATA_SQL.format(date_params[2][0], date_params[2][1]))
    local_data.check_problem_yoy = _append_station_name(data, 'STATION_ID')


def _filter_dept_data(data_sql, dept_rel_field, dept_rel_type):
    """
    将指定SQL加载的数据，以本线程暂存的dept_data(它是本报表使用的部门清单)进行过滤
    :param data_sql:
    :param dept_rel_field: str，SQL中，用于与dept_data的部门名称/ID管理的字段
    :param dept_rel_type: int， 关联的字段数据类型，0，代表部门ID； 1，代表部门名称(ALL_NAME)
    :return: 过滤后的dataframe
    """
    raw_data = pd_query(data_sql)
    if dept_rel_type == 1:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='ALL_NAME')
    else:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='DEPARTMENT_ID')
    return df


def _filter_dept_dataframe(raw_data, dept_rel_field, dept_rel_type):
    """
    将指定SQL加载的数据，以本线程暂存的dept_data(它是本报表使用的部门清单)进行过滤
    :param dept_rel_field: str，SQL中，用于与dept_data的部门名称/ID管理的字段
    :param dept_rel_type: int， 关联的字段数据类型，0，代表部门ID； 1，代表部门名称(ALL_NAME)
    :return: 过滤后的dataframe
    """
    if dept_rel_type == 1:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='ALL_NAME')
    else:
        df = pd.merge(raw_data, local_data.dept_data, left_on=dept_rel_field, right_on='DEPARTMENT_ID')
    return df


def _append_station_name(raw_data, dept_rel_field):
    dept_data = local_data.dept_data[['DEPARTMENT_ID', 'STATION']]
    df = pd.merge(raw_data, dept_data, left_on=dept_rel_field, right_on='DEPARTMENT_ID')
    return df


# --- 第一章
def _get_first_general():
    # 加载数据
    end_date = local_data.end_date
    start_date = local_data.start_date
    prod_data0 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(start_date, end_date))
    # 同比数据
    prod_data1 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(local_data.start_date_year, local_data.end_date_year))
    if prod_data1.empty:
        prod_data1 = pd.DataFrame(columns=prod_data0.columns)
    # 环比数据
    prod_data2 = pd_query(PRODUCE_INFO_ACCIDENT_SQL.format(local_data.start_date_prv, local_data.end_date_prv))
    if prod_data2.empty:
        prod_data2 = pd.DataFrame(columns=prod_data0.columns)

    general = get_first_general(prod_data0, prod_data1, prod_data2)

    # 1. 行车安全， 2. 劳安安全， 3. 路外安全 三个小节
    gen_sub_stat = get_first_general_safety_type_analysis(prod_data0, prod_data1, prod_data2)

    dept_data = local_data.dept_data[['DEPARTMENT_ID', 'ALL_NAME']]
    resp_data = [prod_data0, prod_data1, prod_data2]
    resp_data = [
        pd.merge(data, dept_data, left_on='RESP_UNIT_ID', right_on='DEPARTMENT_ID')
        for data in resp_data
    ]

    result = {
        'general': general,
        'detail_stat_list': gen_sub_stat['detail_stat_list'],
        'detail_stat_resp_d3': gen_sub_stat['detail_stat_resp_d3'],
        'acc_detail_type_static_list': get_first_accident_type_analysis(prod_data0, prod_data1, prod_data2),
        'responsibility_data': get_accident_resp_analysis(resp_data[0], resp_data[1], resp_data[2])
    }

    return result


# -------------第二章--------------------
def _get_second_major_evaluate_analysis():
    """获取履职情况简要统计分析数据
    """
    eval_this = local_data.evaluate_info_this
    eval_prev = local_data.evaluate_info_prev
    eval_this_year = local_data.year_evaluate_info
    # 第一块 干部评价总体情况分析
    first_paragraph = get_evaluate_gen_state(local_data.start_date, local_data.end_date,
                                             eval_this, eval_prev, eval_this_year)
    evaluate_problem_desc = get_evaluate_problem_count_description(eval_this, eval_prev)

    # 第二块 工务系统干部履职评价情况分析
    major_evaluate_situation = get_cadre_evaluate_state_analysis_of_major(eval_this, eval_this_year)
    # 第三块 局管领导人员履职评价分析
    leader_evaluate_analysis = get_leader_evaluate_situation(local_data.evaluate_info_this,
                                                             local_data.evaluate_info_prev,
                                                             local_data.year_evaluate_info)
    # 第四块 正科职干部履职评价简要分析
    chief_evaluate_analysis = get_branch_level_cadre_situation('正科级', local_data.evaluate_info_this,
                                                               local_data.year_evaluate_info)
    # 第五块 副科职及以下干部履职简要分析
    assistant_evaluate_analysis = get_branch_level_cadre_situation('副科级', local_data.evaluate_info_this,
                                                                   local_data.year_evaluate_info)

    result = {
        'evaluate_total_analysis': {
            "total_evaluate": first_paragraph,
            'evaluate_problem_desc': evaluate_problem_desc
        },
        'cadre_evaluate_analysis': {
            "major_evaluate_situation": major_evaluate_situation
        },
        'leader_evaluate_analysis': leader_evaluate_analysis,
        'chief_evaluate_analysis': chief_evaluate_analysis,
        'assistant_evaluate_analysis': assistant_evaluate_analysis
    }
    return result


# -------第二章 ------------------------------------- End


# -------第三章 ------------------------------------- Start
def _get_third_level_evaluate_analysis():
    start_date, end_date = local_data.start_date, local_data.end_date
    # 事故故障情况
    accident_datas = _get_data_from_mongo(report_util.get_safety_produce_data)
    # 检查信息数据
    info_datas = [local_data.check_info_this, local_data.check_info_prev, local_data.check_info_yoy]
    # 检查问题数据
    pro_datas = [local_data.check_problem_this, local_data.check_problem_prev, local_data.check_problem_yoy]
    local_data.accident_datas = accident_datas
    local_data.info_datas = info_datas
    local_data.pro_datas = pro_datas
    eval_data = local_data.evaluate_info_this
    major_quality_analysis = get_major_quality_analysis(eval_data, accident_datas, info_datas, pro_datas)
    station_quality_analysis = get_station_quality_analysis(eval_data, accident_datas, info_datas, pro_datas,
                                                            local_data.dept_data)
    station_leader_analysis = _get_station_leader_analysis(start_date, end_date)

    result = {
        "total_evaluate_analysis": {
            "major_quality_analysis": major_quality_analysis,
            "station_quality_analysis": station_quality_analysis,
            "station_leader_analysis": station_leader_analysis
        }
    }
    return result


def _get_health_index(month):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


# 第三章 3小节 站段领导班子整体检查质量分析
def _get_station_leader_analysis(start_date, end_date):
    evaluate_info = local_data.evaluate_info_this
    juguan_evaluate = evaluate_info[evaluate_info['BUSINESS_CLASSIFY'] == '领导']
    table, description = _get_cadre_quality_analysis(start_date, end_date)
    juguan_leader_evaluate_table = get_evaluate_table(juguan_evaluate)
    result = {
        "station_cadre_quality_analysis": table,
        "station_cadre_quality_analysis_desc": description,
        "juguan_leader_evaluate_table": juguan_leader_evaluate_table
    }
    return result


def _get_cadre_quality_analysis(start_date, end_date):
    evaluate_info = local_data.evaluate_info_this
    # 领导班子情况
    station_carde_count = pd_query(STATION_CADRE_COUNT_SQL)
    # 检查信息情况
    check_info = pd_query(LEADER_CHECK_INFO_SQL.format(start_date, end_date))
    check_info = check_info[check_info['ALL_NAME'].str.contains('领导')]
    check_info = check_info.fillna(0)
    set_keys = ['CHECK_WAY', 'IS_YECHA', 'PROBLEM_NUMBER', 'COST_TIME']
    check_info = check_info.set_index(set_keys)['ALL_NAME'].str.split(
        ',', expand=True).stack().reset_index().rename(columns={0: 'ALL_NAME'})
    check_info['WAY'] = check_info['CHECK_WAY'].apply(
        lambda x: 1 if x == 1 else 0)
    check_info['STATION'] = check_info['ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del check_info['CHECK_WAY'], check_info['ALL_NAME']
    check_info['CHECK_COUNT'] = 1
    check_data = check_info.groupby('STATION').sum().reset_index()
    data = pd.merge(station_carde_count, check_data, how='left', on='STATION')
    data = data[(~data['WAY'].isnull()) & (~data.STATION.str.endswith('部'))
                & (~data.STATION.str.contains('所')) & (~data.STATION.str.endswith('处'))]
    # 当前月份
    date = end_date
    if date.day >= current_app.config.get('UPDATE_DAY'):
        date = date + relativedelta(months=1)
    month = date.year * 100 + date.month
    # 履职报告得分
    eva_report_data = _get_evaluate_report_data(month)
    if eva_report_data.empty is False:
        eva_report_data = eva_report_data.fillna(value={'BUSINESS_CLASSIFY': 'a'})
        eva_report_data = eva_report_data.drop_duplicates('ID_CARD')
        del eva_report_data['ID_CARD']
        eva_report_data = eva_report_data[
            ~(eva_report_data['BUSINESS_CLASSIFY'].isnull())
            & (eva_report_data.BUSINESS_CLASSIFY.str.contains('领导'))]
        evaluate_score = eva_report_data.groupby('STATION').sum().reset_index()
        data = pd.merge(data, evaluate_score, how='left', on='STATION')
        data['score_aver'] = data.apply(
            lambda x: round(x['score'] / x['TOTAL'], 1), axis=1)
        data['score_rank'] = data.groupby('MAJOR')['score_aver'].rank()
    else:
        data['score_aver'] = 0
        data['score'] = 0
        data['score_rank'] = 0
    # 安全综合指数得分
    health_data = _get_health_index(month)
    if health_data.empty is False:
        health_data = health_data.rename(
            columns={
                'RANK': 'HEALTH_RANK',
                'DEPARTMENT_NAME': 'STATION',
                'SCORE': 'HEALTH_SCORE'
            })
        data = pd.merge(data, health_data, how='left', on='STATION')
    else:
        data['HEALTH_RANK'] = 0
        data['HEALTH_SCORE'] = 0
    # 检查问题情况
    problem_data = pd_query(LEADER_CHECK_PROBLEM_SQL.format(start_date, end_date))
    problem_data = problem_data.set_index(
        ['LEVEL', 'PROBLEM_SCORE'])['ALL_NAME'].str.split(
        ',',
        expand=True).stack().reset_index().rename(columns={0: 'ALL_NAME'})
    problem_data['HIGH_PROBLEM'] = problem_data['LEVEL'].apply(
        lambda x: 1 if x in 'ABCE1E2E3' else 0)
    problem_data['STATION'] = problem_data['ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del problem_data['ALL_NAME']
    problem_data = problem_data.groupby('STATION').sum().reset_index()
    data = pd.merge(data, problem_data, how='left', on='STATION')
    # 履职评价详情
    evaluate_data = evaluate_info[
        (~evaluate_info['BUSINESS_CLASSIFY'].isnull())
        & (evaluate_info.BUSINESS_CLASSIFY.str.contains('领导'))]
    # result = []
    major_rst = []
    major_desc_rst = []
    data = data.fillna(0)
    for major in ['工务']:
        desc_rst = []
        major_data = data[data['MAJOR'] == major].copy()
        major_data = major_data.sort_values(by='HEALTH_SCORE', ascending=False)
        major_total = sum(list(data['TOTAL']))
        major_check_aver = int(sum(list(data['CHECK_COUNT'])) / major_total)
        major_dy_aver = round(sum(list(data['COST_TIME'])) / major_total, 2)
        major_problem_aver = round(
            sum(list(data['PROBLEM_NUMBER'])) / major_total, 2)
        major_yecha_aver = int(sum(list(data['IS_YECHA'])) / major_total)
        major_high_aver = int(sum(list(data['HIGH_PROBLEM'])) / major_total)
        major_proscore_aver = round(
            sum(list(data['PROBLEM_SCORE'])) / major_total, 2)
        del major_data['level_4'], major_data['level_2']
        a = 1
        for index in major_data.index:
            value = major_data.loc[index].copy()
            station = value['STATION']
            value['CHECK_AVER'] = int(value['CHECK_COUNT'] / value['TOTAL'])
            value['CHECK_DIFF'] = value['CHECK_AVER'] - major_check_aver
            value['DY_AVER'] = int(value['COST_TIME'] / value['TOTAL'])
            value['DY_DIFF'] = value['DY_AVER'] - major_dy_aver
            value['PROBLEM_AVER'] = int(
                value['PROBLEM_NUMBER'] / value['TOTAL'])
            value['PROBLEM_DIFF'] = value['PROBLEM_AVER'] - major_problem_aver
            value['YECHA_AVER'] = int(value['IS_YECHA'] / value['TOTAL'])
            value['YECHA_DIFF'] = value['YECHA_AVER'] - major_yecha_aver
            value['HIGH_PRO_AVER'] = int(
                value['HIGH_PROBLEM'] / value['TOTAL'])
            value['HIGH_PRO_DIFF'] = value['HIGH_PRO_AVER'] - major_high_aver
            value['PRO_SCORE_AVER'] = int(
                value['PROBLEM_SCORE'] / value['TOTAL'])
            value['PRO_SCORE_DIFF'] = value[
                                          'PRO_SCORE_AVER'] - major_proscore_aver
            major_rst.append({
                "station": station,
                "value": json.loads(value.T.to_json())
            })
            limit = 100
            if a <= limit:  # 文字描述车务工务显示后五名，其余显示后三名
                # 履职情况
                sta_eva = evaluate_data[evaluate_data['STATION'] == station]
                if sta_eva.empty:
                    eva_total = 0
                    eva_score_total = 0
                    luju = 0
                    luju_eva_rst = []
                    ziping = 0
                    ziping_eva_rst = []
                else:
                    eva_total = sta_eva.shape[0]
                    eva_score_total = sum(list(sta_eva['SCORE']))
                    luju_eva = sta_eva[sta_eva['CHECK_TYPE'] == 1]
                    if luju_eva.empty:
                        luju_eva_rst = []
                        luju = 0
                    else:
                        luju = luju_eva.shape[0]
                        luju_eva = luju_eva.groupby('CODE_ADDITION').size(
                        ).rename(columns={0: 'count'})
                        luju_eva_rst = [{
                            "CODE": index,
                            "COUNT": int(luju_eva.at[index])
                        } for index in luju_eva.index]
                    ziping_eva = sta_eva[sta_eva['CHECK_TYPE'] == 2]
                    if ziping_eva.empty:
                        ziping_eva_rst = []
                        ziping = 0
                    else:
                        ziping = ziping_eva.shape[0]
                        ziping_eva = ziping_eva.groupby('CODE_ADDITION').size(
                        ).rename(columns={0: 'count'})
                        ziping_eva_rst = [{
                            "CODE": index,
                            "COUNT": int(ziping_eva.at[index])
                        } for index in ziping_eva.index]
                desc_rst.append({
                    "station": station,
                    "value": json.loads(value.T.to_json()),
                    "eva_value": {
                        "total": eva_total,
                        "score_total": eva_score_total,
                        "luju": luju,
                        "luju_value": luju_eva_rst,
                        "ziping": ziping,
                        "ziping_value": ziping_eva_rst
                    }
                })
            a += 1
        major_desc_rst.append({"major": major, "value": desc_rst})
    return major_rst, major_desc_rst


def _get_evaluate_report_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'detail_evaluate_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "BUSINESS_CLASSIFY": 1,
            "ID_CARD": 1,
            "STATION": 1,
            "score": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


# -------第三章 ------------------------------------- END


# -------第四章 ----------------------------------- START
def _get_forth_sys_check_and_problem_analysis():
    major_analysis = get_sys_analysis_of_major(local_data.check_info_this, local_data.check_info_prev,
                                               local_data.check_problem_this, local_data.check_problem_prev)
    admin_dept_analysis = get_sys_analysis_of_admin_dept(local_data.dept_data,
                                                         local_data.check_info_this,
                                                         local_data.check_info_prev,
                                                         local_data.check_problem_this,
                                                         local_data.check_problem_prev)
    result = {
        'major_analysis': major_analysis,
        'admin_dept_analysis': admin_dept_analysis
    }
    return result


# -------第四章 ------------------------------------- END


# -------第五章 ----------------------------------- START
def _get_fifth_station_health_index_radar():
    end_date = local_data.end_date
    mon = end_date.year * 100 + end_date.month
    index_list = list(mongo.db['monthly_detail_health_index'].find({'MAJOR': '工务', 'MON': mon,
                                                                    'DETAIL_TYPE': 0, 'HIERARCHY': 3}))
    if len(index_list) == 0:
        index_list = [{
            'DEPARTMENT_NAME': '',
            'MAIN_TYPE': -1,
            'SCORE': 0
        }]
    health_index = pd.DataFrame(index_list)
    gongwu_list = _get_station_health_index_radar_content(health_index)

    index_list = list(mongo.db['monthly_detail_health_index'].find({'MAJOR': '工电', 'MON': mon,
                                                                    'DETAIL_TYPE': 0, 'HIERARCHY': 3}))
    if len(index_list) == 0:
        index_list = [{
            'DEPARTMENT_NAME': '',
            'MAIN_TYPE': -1,
            'SCORE': 0
        }]
    health_index = pd.DataFrame(index_list)
    gongdian_list = _get_station_health_index_radar_content(health_index)

    result = {
        'gongwu_list': gongwu_list,
        'gongdian_list': gongdian_list
    }
    return result


def _get_station_health_index_radar_content(health_index):
    station_data = health_index['DEPARTMENT_NAME'].unique().tolist()

    # sub_tags 数据的标题，一共三组数据
    sub_tags = ['本段各项指数', '本专业各指数最高分', '本专业各指数平均分']
    main_types = HEALTH_INDEX_MAIN_TYPES

    # labels，就是各个维度的名称，如检查力度指数，检查均衡度，评价力度。。。
    labels = [get_index_title(type_item) for type_item in main_types]
    best_list, avg_list = [], []
    for type_item in main_types:
        avg_list.append(round(health_index[health_index['MAIN_TYPE'] == type_item]['SCORE'].mean(), 2))
        best_list.append(round(health_index[health_index['MAIN_TYPE'] == type_item]['SCORE'].max(), 2))

    station_list = []
    # 迭代站段清单
    for station in station_data:
        better_list = []
        worse_list = []
        station_df = health_index[health_index['DEPARTMENT_NAME'] == station]
        station_score = []
        idx = 0
        # 站段的子指数分数大于平均值为，较好，否则为不足
        for type_item in main_types:
            score = station_df[station_df['MAIN_TYPE'] == type_item]['SCORE'].max()
            station_score.append(score)
            if score >= avg_list[idx]:
                better_list.append(get_index_title(type_item))
            else:
                worse_list.append(get_index_title(type_item))
            idx = idx + 1

        station_data = [station_score, best_list, avg_list]
        title = f'{station}安全管理综合指数雷达图'
        time_str = local_data.end_date.strftime("%Y%m%d")
        file_name = f'{title}_{time_str}.png'

        radar_data = {
            'labels': labels,
            'station_data': station_data,
            'title': title,
            'sub_tags': sub_tags,
            'file_name': file_name
        }
        radar_src = _save_radar_pic(labels, station_data, title, sub_tags, file_name)

        content = {
            'station_name': station,
            'better_list': better_list,
            'worse_list': worse_list,
            'radar_data': radar_data,
            # 'radar_pic': InlineImage(local_data.template, radar_src, width=Mm(120))
        }
        station_list.append(content)

    return station_list


def _create_fig(labels, data, title, sub_tags):
    from app.data.font import FONT_STYLE_FILE
    ymajorLocator = MultipleLocator(0.1)  # 将y轴主刻度标签设置为0.5的倍数
    n = len(labels)

    angles = np.linspace(0, 2 * np.pi, n, endpoint=False)  # 旋转90度，从正上方开始！
    angles = np.concatenate((angles, [angles[0]]))  # 闭合

    fig = Figure(figsize=(12, 9))
    ax = fig.add_subplot(111, polar=True)  # 参数polar，表示极坐标！！
    ax.yaxis.set_major_locator(ymajorLocator)
    # 自己画grid线（5条环形线）
    for i in [30, 60, 90, 100]:
        ax.plot(angles, [i] * (n + 1), '-',
                c="#BDC3C7", lw=0.5)  # 之所以 n +1，是因为要闭合！

    # 填充底色
    ax.fill(angles, [100] * (n + 1), facecolor='w', alpha=0.5)

    # 自己画grid线（6条半径线）
    for i in range(n):
        ax.plot([angles[i], angles[i]], [0, 100], '-', c="#BDC3C7", lw=0.5)
    colors = ['#f0a626', '#1d97fc', '#2fc75c', 'yellow', 'black']
    # 画线
    for pos, item in enumerate(data):
        item = np.array(item)
        item = np.concatenate((item, [item[0]]))  # 闭合
        if pos == 0:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{sub_tags[pos]}')
            ax.fill(angles, item, c=colors[pos], alpha=0.2)
        else:
            ax.plot(angles, item, 'o-', linewidth=1,
                    c=colors[pos], label=f'{sub_tags[pos]}')
    # 填充
    ax.tick_params(axis='both', direction='out', pad=27)  # 刻度跟标签的距离
    ax.legend(loc='best', prop=FontProperties(fname=FONT_STYLE_FILE),
              fontsize=16, bbox_to_anchor=(1.05, 1.0), borderaxespad=0.)
    ax.set_thetagrids(angles * 180 / np.pi, labels, fontproperties=FontProperties(fname=FONT_STYLE_FILE),
                      fontsize=16)
    ax.set_title("{0}指数雷达图".format(title), va='bottom',
                 fontproperties=FontProperties(fname=FONT_STYLE_FILE), fontsize=24)
    ax.set_rlim(0, 100)
    # 下两行去掉所有默认的grid线
    ax.spines['polar'].set_visible(False)  # 去掉最外围的黑圈
    ax.grid(False)  # 去掉中间的黑圈
    # 关闭数值刻度
    ax.set_yticks([])
    return fig


# 雷达图
def _save_radar_pic(labels, data, title, sub_tags, file_name):
    import os
    # 获取项目根目录路径
    dir_path = os.path.join(manager.get_report_path('monthly', '工务'), 'images')
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    pic_path = os.path.join(dir_path, file_name)
    if os.path.exists(pic_path):
        os.remove(pic_path)
    labels = np.array(labels)
    fig = _create_fig(labels, data, title, sub_tags)
    fig.savefig(pic_path)
    return pic_path


# -------第五章 ------------------------------------- END

# -------第六章 ----------------------------------- START
def _get_sixth_violation_person_analysis():
    year = local_data.end_date.year
    month = local_data.end_date.month
    # 整改情况，上一个月
    year_prev = local_data.start_date.year
    month_prev = local_data.start_date.month
    result = {
        'violation_distribution': get_violation_person_distribution(year, month),
        'violation_rectification': get_violation_person_rectification(year_prev, month_prev)
    }
    return result


# -------第六章 ------------------------------------- END


# -------第七章 ------------------------------------ Start
def _get_seventh_classic_problem_analysis():
    start_date, end_date = local_data.start_date, local_data.end_date
    problem_list = get_top_check_problem(start_date, end_date)
    # 各段典型或突出安全风险
    spec_risk = get_spec_risk()
    # 自轮设备安全风险管控
    zlsb_risk = get_zlsb_risk()
    # “三大”段施工安全风险管控情况
    sgaq_risk = get_sgaq_risk()
    content = {
        'major_analysis': {
            'problem_list': problem_list
        },
        'spec_risk': spec_risk,
        'zlsb_risk': zlsb_risk,
        'sgaq_risk': sgaq_risk
    }
    return content


def get_spec_risk():
    all_station = []
    dept = local_data.dept_data
    time = local_data.time
    # 获取数据
    health_data = _get_health_index_basic_data(time)
    health_data = health_data[(health_data['MAIN_TYPE'] == 6) & (health_data['DETAIL_TYPE'] == 3)].fillna('')
    # 工务站段+部门
    dept = dept[dept['DEPT_TYPE'] == 4]
    for j, k in dept.iterrows():
        data = health_data[health_data['DEPARTMENT_ID'] == k['STATION_ID']]
        if data.empty is not True:
            dic = {
                'name': k['STATION'],
                'content': re.sub(r'<.*?>', '', data['CONTENT'].tolist()[0])
            }
        else:
            dic = {
                'name': k['STATION'],
                'content': '无数据'
            }
        all_station.append(dic)
    return {
        'all_station': all_station
    }


def _get_health_index_basic_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index_basic_data'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "DEPARTMENT_ID": 1,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            "CONTENT": 1
        }))
        if doc:
            break
        else:
            doc = [{
                "DEPARTMENT_ID": '1',
                "MAIN_TYPE": -1,
                "DETAIL_TYPE": -1,
                "CONTENT": '1'
            }]
    data = pd.DataFrame(doc)
    return data


def get_zlsb_risk():
    dept = local_data.dept_data
    dept = dept[dept['DEPT_TYPE'] == 4]
    time = local_data.time
    month = local_data.month_time
    year = local_data.year_time
    now_data = _get_major_index_basic_data(time)
    month_data = _get_major_index_basic_data(month)
    year_data = _get_major_index_basic_data(year)
    all_times = []
    for i in [now_data, month_data, year_data]:
        all_station = []
        for j, k in dept.iterrows():
            data = i[(i['DEPARTMENT_ID'] == k['STATION_ID']) & (i['MAIN_TYPE'] == 1) & (i['INDEX_TYPE'] == 5)]
            dic = {
                'name': k['STATION'],
                # 检查次数
                'count': int(data[data['DETAIL_TYPE'] == 2]['NUMERATOR'].sum()),
                # 发现问题数
                'pro': int(data[data['DETAIL_TYPE'] == 4]['NUMERATOR'].sum()),
                # 质量分
                'score': float('%.2f' % data[data['DETAIL_TYPE'] == 5]['NUMERATOR'].sum())
            }
            all_station.append(dic)
        all_times.append(all_station)
    # 环比同比
    for i in range(len(all_times[0])):
        all_times[0][i]['ratio1'] = calculate_comparison_statistic(all_times[0][i]['count'], all_times[2][i]['count'],
                                                                   all_times[1][i]['count'])
        all_times[0][i]['ratio2'] = calculate_comparison_statistic(all_times[0][i]['pro'], all_times[2][i]['pro'],
                                                                   all_times[1][i]['pro'])
        all_times[0][i]['ratio3'] = calculate_comparison_statistic(all_times[0][i]['score'], all_times[2][i]['score'],
                                                                   all_times[1][i]['score'])
    return {
        'all_times': all_times[0]
    }


def get_sgaq_risk():
    dept = local_data.dept_data
    dept = dept[dept['DEPT_TYPE'] == 4]
    time = local_data.time
    month = local_data.month_time
    year = local_data.year_time
    now_data = _get_major_index_basic_data(time)
    month_data = _get_major_index_basic_data(month)
    year_data = _get_major_index_basic_data(year)
    all_times = []
    for i in [now_data, month_data, year_data]:
        all_station = []
        for j in ['成都工务大修段', '成都工务大型养路机械运用检修段', '成都桥路大修段']:
            data = i[(i['DEPARTMENT_NAME'] == j) & (i['MAIN_TYPE'] == 1) & (i['INDEX_TYPE'] == 6)]
            dic = {
                'name': j,
                # 检查次数
                'count': int(data[data['DETAIL_TYPE'] == 2]['NUMERATOR'].sum()),
                # 发现问题数
                'pro': int(data[data['DETAIL_TYPE'] == 4]['NUMERATOR'].sum()),
                # 质量分
                'score': float('%.2f' % data[data['DETAIL_TYPE'] == 5]['NUMERATOR'].sum())
            }
            all_station.append(dic)
        all_times.append(all_station)
    # 环比同比
    for i in range(len(all_times[0])):
        all_times[0][i]['ratio1'] = calculate_comparison_statistic(all_times[0][i]['count'], all_times[2][i]['count'],
                                                                   all_times[1][i]['count'])
        all_times[0][i]['ratio2'] = calculate_comparison_statistic(all_times[0][i]['pro'], all_times[2][i]['pro'],
                                                                   all_times[1][i]['pro'])
        all_times[0][i]['ratio3'] = calculate_comparison_statistic(all_times[0][i]['score'], all_times[2][i]['score'],
                                                                   all_times[1][i]['score'])
    return {
        'all_times': all_times[0]
    }


def _get_major_index_basic_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_']
    for prefix in prefix_list:
        coll_name = prefix + 'major_index_cardinal_number_basic_data'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
        }, {
            "_id": 0,
            "NUMERATOR": 1,
            "DEPARTMENT_ID": 1,
            "MAIN_TYPE": 1,
            "DETAIL_TYPE": 1,
            'DEPARTMENT_NAME': 1,
            'INDEX_TYPE': 1
        }))
        if doc:
            break
        else:
            doc = [{
                "NUMERATOR": 1,
                "DEPARTMENT_ID": '1',
                "MAIN_TYPE": 1,
                "DETAIL_TYPE": 1,
                'DEPARTMENT_NAME': '1',
                'INDEX_TYPE': 1
            }]
    data = pd.DataFrame(doc)
    return data


# -------第七章 ------------------------------------- END

# START-------------------------------------------- 构建图片对象 -------------------------------------------------------
def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    # 插入第五张的雷达图
    fifth = data.get('fifth')
    if fifth:
        station_list = fifth.get('gongwu_list')
        station_list.extend(fifth.get('gongdian_list'))
        for item in station_list:
            radar_data = item['radar_data']
            pic_path = _save_radar_pic(radar_data['labels'], radar_data['station_data'], radar_data['title'],
                                       radar_data['sub_tags'], radar_data['file_name'])
            item['radar_pic'] = InlineImage(template, pic_path, width=Mm(120))


# END---------------------------------------------- 构建图片对象 -------------------------------------------------------

def _get_data_from_mongo(func):
    # 当月数据
    data = func(local_data.start_date.strftime('%Y-%m-%d'), local_data.end_date.strftime('%Y-%m-%d'), major='工务')
    # 上月数据
    month_data = func(local_data.start_date_prv.strftime('%Y-%m-%d'), local_data.end_date_prv.strftime('%Y-%m-%d'),
                      major='工务')
    if month_data.empty:
        month_data = pd.DataFrame(columns=data.columns)

    # 获取去年本月数据
    year_data = func(local_data.start_date_year.strftime('%Y-%m-%d'), local_data.end_date_year.strftime('%Y-%m-%d'),
                     major='工务')
    if year_data.empty:
        year_data = pd.DataFrame(columns=data.columns)
    return [data, month_data, year_data]
