"""
计算工务专业的月/季/半年/年度分析报告的公用发放

Created: 2019-10-18
@author: Lu Jionglin
"""
import json
import pandas as pd
from app.data.safety_produce_info import detail_type_divide, resp_type_divide
from app import mongo
from app.data.util import pd_query
from app.report.gongwu.common_sql import VIOLATION_PERSON_CONFIRM_LIST_SQL, VIOLATION_PERSON_LIST_SQL, \
    TOP_CHECK_PROBLEM_POINT_SQL, CHECK_EVALUATE_REVIEW_SQL
from app.utils.common_func import choose_collection_prefix
from app.utils.safety_index_common_func import get_child_calc_formula

# 专业的主管部门（X部/X处）
MAJOR_AMDIN_DEPT_ID = '19B8C3534E4F5665E0539106C00A58FD'

# 各个季度包含的月份
QUARTER_MONTHS_TUPLE = ((1, 2, 3), (4, 5, 6), (7, 8, 9), (10, 11, 12))

# 事故的分类，A -》 D
ACCIDENT_CODE_TYPE_SET = {'A', 'B', 'C', 'D'}

# 综合安全指数的Main_type的清单
HEALTH_INDEX_MAIN_TYPES = [1, 2, 3, 4, 5, 6]

EVALUATE_DEDUCT_LEVELS = [7, 6, 5, 4, 3, 2, 1]
EVALUATE_DEDUCT_LEVEL_NAMES = {
    1: '∑＜2',
    2: '2≤∑＜4',
    3: '4≤∑＜6',
    4: '6≤∑＜8',
    5: '8≤∑＜10',
    6: '10≤∑＜12',
    7: '∑≥12'
}

EVALUATE_PROBLEM_TYPE_NAMES = [
    '量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
    '履职评价管理', '事故故障追溯', '弄虚作假'
]

# 检查，高质量问题
CHECK_PROBLEM_HIGH_LEVEL = ['A', 'B', 'E1', 'E2']

# 违章人员处理方式
VIOLATION_PERSON_DISPOSE_METHODS = {
    '1': '培训',
    '2': '离岗',
    '3': '待岗',
    '4': '转岗',
    '5': '前期已处置',  # 数据库中为前期已处理, 报表中的文字为批评教育
    '6': '帮促'
}


def extract_accident_code_type(accident_code):
    """
    用于划分提取事故类型，取首字母。
    对应t_safety_produce_info.code字段。
    :param accident_code:
    :return:
    """
    accident_code = str(accident_code).strip()
    if len(accident_code) > 0:
        return accident_code[0]
    else:
        return accident_code


def get_inc_text(number, unit='件'):
    return f'增长{number}{unit}' if number > 0 else (f'减少{-number}{unit}' if number < 0 else '持平')


def get_percent_text(number):
    return f'上升{number}%' if number > 0 else (f'下降{-number}%' if number < 0 else '持平')


def calculate_comparison_statistic(now_count, year_count, prev_count):
    """
    根据给定的当前数额，同比数额，环比数额，计算同比差额，同比比例，环比差额，环比比例
    :param now_count:
    :param year_count:
    :param prev_count:
    :return:
    """
    year_diff = now_count - year_count
    if year_count == 0:
        year_percent = year_diff * 100
    else:
        year_percent = round(year_diff / year_count * 100, 2)
    prev_diff = now_count - prev_count
    if prev_count == 0:
        month_percent = prev_diff * 100
    else:
        month_percent = round(prev_diff / prev_count * 100, 2)
    result = {
        'year_diff': year_diff,
        'year_percent': year_percent,
        'prev_diff': prev_diff,
        'prev_percent': month_percent
    }
    return result


def evaluate_deduct_level(score):
    """对履职扣分进行分档
    Arguments:
        score {number} -- 扣分数值

    Returns:
        int -- 所属档次
    """
    if score < 2:
        return 1
    elif score < 4:
        return 2
    elif score < 6:
        return 3
    elif score < 8:
        return 4
    elif score < 10:
        return 5
    elif score < 12:
        return 6
    else:
        return 7


def calc_ratio(new_val, old_val):
    if old_val == 0:
        ratio = new_val * 100
    else:
        ratio = round((new_val - old_val) / old_val * 100, 1)
    return ratio


def extract_section_from_all_name(all_name):
    """
    从部门全面中获取科室
    :param all_name:
    :return:
    """
    if all_name:
        names = all_name.split('-')
        if len(names) > 1:
            return f'{names[0]}-{names[1]}'
    return all_name


def get_evaluate_report_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        month {int} -- 月份，如 201901

    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'detail_evaluate_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "BUSINESS_CLASSIFY": 1,
            "ID_CARD": 1,
            "STATION": 1,
            "score": 1
        }))
        if doc:
            break
    if len(doc) == 0:
        data = pd.DataFrame(columns=['BUSINESS_CLASSIFY', 'ID_CARD', 'STATION', 'score'])
    else:
        data = pd.DataFrame(doc)

    return data


def get_check_info_data(start_year_month, end_year_month, major=None):
    match = {
        'MON': {'$gte': start_year_month, '$lte': end_year_month},
        'MAJOR': major
    }
    project = {
        "_id": 0,
        "PK_ID": 1,
        "CHECK_WAY": 1,
        "IS_YECHA": 1,
        "LEVEL": 1,
        "RISK_NAME": 1,
        "IS_RED_LINE": 1,
        "PROBLEM_NUMBER": 1,
        "INSPECT_DPID": 1,
        'TYPE3': 1
    }
    collection_list = ['daily_detail_check_info', 'monthly_detail_check_info', 'history_detail_check_info']
    data = []
    for info_coll in collection_list:
        info_data = list(mongo.db[info_coll].find(match, project))
        data.extend(info_data)
    if len(data) == 0:
        data = [{
            "PK_ID": 1,
            "CHECK_WAY": 1,
            "IS_YECHA": 1,
            "PROBLEM_NUMBER": 1,
            "INSPECT_DPID": '1',
            'TYPE3': '1',
        }]
    else:
        data = data
    df = pd.DataFrame(data)
    df.fillna('')
    return df


def get_check_problem_data(start_year_month, end_year_month, major=None):
    match = {
        'MON': {'$gte': start_year_month, '$lte': end_year_month},
        'MAJOR': major
    }
    project = {
        "_id": 0,
        "PK_ID": 1,
        "CHECK_WAY": 1,
        "IS_YECHA": 1,
        "LEVEL": 1,
        "RISK_NAME": 1,
        "IS_RED_LINE": 1,
        "PROBLEM_NUMBER": 1,
        "INSPECT_DPID": 1
    }
    collection_list = ['daily_detail_check_problem', 'monthly_detail_check_problem', 'history_detail_check_problem']
    data = []
    for info_coll in collection_list:
        info_data = list(mongo.db[info_coll].find(match, project))
        data.extend(info_data)
    df = pd.DataFrame(data)
    df.fillna('')
    return df
# ------------------------------计算文档中需要的数据


# ------------第一章
def get_first_general(prod_data_this, prod_data_year, prod_date_prev):
    """
    分析头部的概况里面
    :param prod_data_this:
    :param prod_data_year:
    :param prod_date_prev:
    :return
    """
    # list：[当前，同比，环比]
    data_arr = [prod_data_this, prod_data_year, prod_date_prev]
    for df in data_arr:
        df.drop_duplicates('PK_ID', inplace=True)
        df['ACC_TYPE'] = df['INFO_CODE'].apply(extract_accident_code_type)

    gen_stat = calculate_comparison_statistic(data_arr[0].shape[0], data_arr[1].shape[0], data_arr[2].shape[0])

    acc_type_list = ['A', 'B', 'C', 'D']
    detail_type_list = [1, 2, 3]  # 行车、劳安、路外
    detail_type_list_text = ('行车', '劳安', '路外')
    acc_stat_list = []
    test_type = []
    test_start = True
    # 求文档中：其中，一般B类及以上事故15件，同比增加3件，环比增加2件
    # 一般X类以上，包含了X类之前的汇总，例如一般B类，包含A\B类
    for acc_type in acc_type_list:
        if test_start:
            # 寻找开始，不是为0的事故数据
            test_type.append(acc_type)
            count0 = prod_data_this[prod_data_this['ACC_TYPE'].isin(test_type)].shape[0]
            if count0 == 0:
                # 如果没找到，将跳出本次遍历并继续循环
                continue
            else:
                test_start = False
            acc_type_text = f'一般及{acc_type}类以上'
        else:
            test_type = [acc_type]
            count0 = prod_data_this[prod_data_this['ACC_TYPE'].isin(test_type)].shape[0]
            acc_type_text = f'{acc_type}类'
        #
        count1 = prod_data_year[prod_data_year['ACC_TYPE'].isin(test_type)].shape[0]
        count2 = prod_date_prev[prod_date_prev['ACC_TYPE'].isin(test_type)].shape[0]
        stat = calculate_comparison_statistic(count0, count1, count2)

        # 求(劳安1件，路外14件)
        idx = 0
        detail_list = []
        for detail_type in detail_type_list:
            sub_item = {
                'detail_text': detail_type_list_text[idx],
                'count': prod_data_this[(prod_data_this['ACC_TYPE'].isin(test_type)) &
                                        (prod_data_this['DETAIL_TYPE'] == detail_type)].shape[0]
            }
            idx = idx + 1
            detail_list.append(sub_item)

        content = {
            'acc_type_text': acc_type_text,
            'count': count0,
            'year_diff': stat['year_diff'],
            'year_diff_text': get_inc_text(stat['year_diff']),
            'prev_diff': stat['prev_diff'],
            'prev_diff_text': get_inc_text(stat['prev_diff']),
            'detail_list': detail_list
        }
        acc_stat_list.append(content)

    # 计算概况描述
    general = {
        'total_count': prod_data_this.shape[0],
        'total_year_diff': gen_stat['year_diff'],
        'total_year_diff_text': get_inc_text(gen_stat['year_diff']),
        'total_prev_diff': gen_stat['prev_diff'],
        'total_prev_diff_text': get_inc_text(gen_stat['prev_diff']),
        'acc_stat_list': acc_stat_list
    }
    return general


def get_first_general_safety_type_analysis(prod_data_this, prod_data_year, prod_date_prev):
    """
    分析头部的概况里面
    1. 行车安全， 2. 劳安安全， 3. 路外安全 三个小节
    :param prod_data_this:
    :param prod_data_year:
    :param prod_date_prev:
    :return:
    """
    if prod_date_prev is None:
        prod_date_prev = pd.DataFrame(columns=prod_data_this.columns)

    detail_type_list = [1, 2, 3]  # 行车、劳安、路外
    detail_type_list_text = ('行车', '劳安', '路外')
    data_arr = [prod_data_this, prod_data_year, prod_date_prev]

    # 1. 行车安全
    detail_stat_list = []
    idx = 0
    for detail_type in detail_type_list:
        data_arr_dt = [data[data['DETAIL_TYPE'] == detail_type] for data in data_arr]
        counts = [data.shape[0] for data in data_arr_dt]
        stat = calculate_comparison_statistic(counts[0], counts[1], counts[2])
        phase = {
            'detail_text': detail_type_list_text[idx],
            'detail_type': detail_type,
            'count': counts[0],
            'year_diff': stat['year_diff'],
            'year_diff_text': get_inc_text(stat['year_diff']),
            'prev_diff': stat['prev_diff'],
            'prev_diff_text': get_inc_text(stat['prev_diff']),
            'sub_acc_list': _get_first_general_safety_type_analysis_sub(data_arr_dt),
            'overview_list': data_arr_dt[0]['OVERVIEW'].tolist()
        }
        detail_stat_list.append(phase)
        idx = idx + 1

    # 求3. 路外安全【防控责任】。防控需要增加额外判断条件
    data_arr_resp_d3 = [
        data[(data['RESP_TYPE'] == 2) & (data['DETAIL_TYPE'] == 3)] for data in data_arr
    ]
    counts2 = [data.shape[0] for data in data_arr_resp_d3]

    stat = calculate_comparison_statistic(counts2[0], counts2[1], counts2[2])
    resp_d3 = {
        'detail_text': '路外安全防控责任',
        'detail_type': detail_type,
        'count': counts2[0],
        'year_diff': stat['year_diff'],
        'year_diff_text': get_inc_text(stat['year_diff']),
        'prev_diff': stat['prev_diff'],
        'prev_diff_text': get_inc_text(stat['prev_diff']),
        'sub_acc_list': _get_first_general_safety_type_analysis_sub(data_arr_resp_d3)
    }
    result = {
        'detail_stat_list': detail_stat_list,
        'detail_stat_resp_d3': resp_d3
    }
    return result


def _get_first_general_safety_type_analysis_sub(data_arr):
    """ get_first_general_safety_type_analysis的子函数
    计算: 其中，一般D类及以上事故11件，同比增加6件，环比增加5件。
    :param data_arr: [当期，去年当期(同比), 前一期(环比)]
    :return:
    """
    sub_acc_list = []
    test_start = True
    test_type = []
    acc_type_list = ['A', 'B', 'C', 'D']
    # 遍历 A/B/C/D 类型的事故
    # 此处需要求解 一般X类及以上，可能是一般B/C/D类及以上
    for acc_type in acc_type_list:
        if test_start:
            # 寻找开始，不是为0的事故数据
            test_type.append(acc_type)
            counts1 = [
                data[data['ACC_TYPE'].isin(test_type)].shape[0]
                for data in data_arr
            ]
            if counts1[0] == 0:
                # 如果没找到，将跳出本次遍历并继续循环
                continue
            else:
                test_start = False
            acc_type_text = f'一般及{acc_type}类以上'
        else:
            test_type = [acc_type]
            counts1 = [
                data[data['ACC_TYPE'].isin(test_type)].shape[0]
                for data in data_arr
            ]
            acc_type_text = f'{acc_type}类'

        stat = calculate_comparison_statistic(counts1[0], counts1[1], counts1[2])
        sub_acc_list.append({
            'acc_type_text': acc_type_text,
            'count': counts1[0],
            'year_diff': stat['year_diff'],
            'year_diff_text': get_inc_text(stat['year_diff']),
            'prev_diff': stat['prev_diff'],
            'prev_diff_text': get_inc_text(stat['prev_diff']),
        })

    return sub_acc_list


def get_first_accident_type_analysis(prod_data_this, prod_data_year, prod_date_prev):
    """
    计算 第一章（一）事故类别分析数据。
    :param prod_data_this: 本期数据
    :param prod_data_year: 上年同期数据
    :param prod_date_prev: 上一期数据
    :return:
    """
    data0, data1, data2 = prod_data_this, prod_data_year, prod_date_prev
    if data2 is None:
        data2 = pd.DataFrame(columns=data0.columns)
    # 采用groupby，聚合。 static_df 按 行车|劳安|路外，及 A/B/C/D类 两个字段聚合
    static_df = data0.groupby(['ACC_TYPE', 'DETAIL_TYPE']).count().reset_index()
    acc_detail_type_static_list = []
    for idx, row in static_df.iterrows():
        phase = {
            'type_name': detail_type_divide(row['DETAIL_TYPE']) + row['ACC_TYPE'] + '类',
            'this_count': row['PK_ID']
        }

        # D9调车脱轨事故1件，同比增加1件，上升100%，环比持平
        sub_df = data0[(data0['ACC_TYPE'] == row['ACC_TYPE']) & (data0['DETAIL_TYPE'] == row['DETAIL_TYPE'])]
        sub_df = sub_df.groupby(['INFO_CODE', 'INFO_NAME']).count().reset_index()
        detail_list = []
        for sub_idx, sub_row in sub_df.iterrows():
            sub_phase = {
                'type_name': sub_row['INFO_NAME'],
                'this_count': sub_row['PK_ID']
            }
            year_count = 0 if data1.empty \
                else len(data1[(data1['DETAIL_TYPE'] == row['DETAIL_TYPE']) &
                               (data1['INFO_NAME'] == row['INFO_NAME'])])
            prev_count = 0 if data2.empty \
                else len(data2[(data2['DETAIL_TYPE'] == row['DETAIL_TYPE']) &
                               (data2['INFO_NAME'] == row['INFO_NAME'])])

            sub_static = calculate_comparison_statistic(sub_phase['this_count'], year_count, prev_count)
            sub_static['year_diff_text'] = get_inc_text(sub_static['year_diff'])
            sub_static['prev_diff_text'] = get_inc_text(sub_static['prev_diff'])
            sub_static['year_percent_text'] = get_percent_text(sub_static['year_percent'])
            sub_static['prev_percent_text'] = get_percent_text(sub_static['prev_percent'])

            sub_phase.update(sub_static)
            detail_list.append(sub_phase)
        phase['detail_list'] = detail_list

        acc_detail_type_static_list.append(phase)
    return acc_detail_type_static_list


def get_accident_resp_analysis(prod_data_this, prod_data_year, prod_date_prev):
    """
    计算（三）事故类别分析数据。
    :param prod_data_this: 本期数据
    :param prod_data_year: 上年同期数据
    :param prod_date_prev: 上一期数据
    :return:
    """
    result = {
        'detail_list': [],
        'acc_type_list': [],
        'responsible_list': []
    }
    data_arr = [prod_data_this, prod_data_year, prod_date_prev]

    resp_data = [data[data['RESPONSIBILITY_IDENTIFIED'].isin([1, 2, 3])].copy() for data in data_arr]

    for data in resp_data:
        data['RESPONSIBILITY_IDENTIFIED_NAME'] = data['RESPONSIBILITY_IDENTIFIED'].apply(resp_type_divide)

    major_detail_values = []
    # 按'行车', '路外', '劳安'划分，须计算同比
    detail_type_list = [1, 2, 3]  # 行车、劳安、路外
    detail_type_list_text = ('行车', '劳安', '路外')
    idx = 0
    for detail_type in detail_type_list:
        detail_datas = [
            data[data['DETAIL_TYPE'] == detail_type].copy().drop_duplicates('PK_ID')
            if data.empty is False else pd.DataFrame(columns=resp_data[0].columns) for data in resp_data
        ]
        # 事故总数
        major_counts = [data.shape[0] for data in detail_datas]
        this_count = major_counts[0]  # 发生责任事故总数
        # 专业发生责任事故同比环比
        total_comparison = calculate_comparison_statistic(major_counts[0], major_counts[1], major_counts[2])
        total_comparison['year_diff_text'] = get_inc_text(total_comparison.get('year_diff'))
        total_comparison['prev_diff_text'] = get_inc_text(total_comparison.get('prev_diff'))
        total_comparison['year_percent_text'] = get_percent_text(total_comparison.get('year_percent'))
        total_comparison['prev_percent_text'] = get_percent_text(total_comparison.get('prev_percent'))

        major_detail_values.append({
            "type_name": detail_type_list_text[idx],
            "this_count": this_count,
            "comparison": total_comparison
        })
        idx = idx + 1

    result['detail_list'] = major_detail_values

    # 计算按事故分类，仅计算数量
    type_data0 = resp_data[0].drop_duplicates('PK_ID')
    type_data0 = type_data0.groupby(['ACC_TYPE', 'INFO_NAME']).size().reset_index().rename(columns={0: 'count'})
    type_rst = [
        {  # 按事故分类数据
            "type_name": type_data0.at[index, 'INFO_NAME'],
            "type": type_data0.at[index, 'ACC_TYPE'],
            "this_count": int(type_data0.at[index, 'count'])
        } for index in type_data0.index
    ]
    # 计算按责任单位分类
    resp_major_data = resp_data[0].groupby([
        'ALL_NAME', 'RESPONSIBILITY_IDENTIFIED_NAME'
    ]).size().reset_index().rename(columns={0: 'count'})

    responsible_list = []
    for index, row in resp_major_data.iterrows():
        sub_content = {
            'unit_name': row['ALL_NAME'],
            "responsible_name": row['RESPONSIBILITY_IDENTIFIED_NAME'],
            "this_count": int(row['count'])
        }
        detail_list = []
        # 路外事故0件，同比持平，环比持平
        idx = 0
        for detail_type in detail_type_list:
            detail_datas = [
                data[(data['DETAIL_TYPE'] == detail_type) &
                     (data['RESPONSIBILITY_IDENTIFIED_NAME'] == row['RESPONSIBILITY_IDENTIFIED_NAME']) &
                     (data['ALL_NAME'] == row['ALL_NAME'])]
                if data.empty is False else pd.DataFrame(columns=resp_data[0].columns) for data in resp_data
            ]
            count_arr = [data.shape[0] for data in detail_datas]
            sub_static = calculate_comparison_statistic(count_arr[0], count_arr[1], count_arr[2])
            detail_content = {
                'type_name': detail_type_list_text[idx],
                'this_count': count_arr[0],
                'year_diff_text': get_inc_text(sub_static.get('year_diff')),
                'prev_diff_text': get_inc_text(sub_static.get('prev_diff'))
            }
            detail_list.append(detail_content)
            idx = idx + 1
        sub_content['detail_list'] = detail_list
        responsible_list.append(sub_content)

    result['acc_type_list'] = type_rst
    result['responsible_list'] = responsible_list
    return result
# --------------------------------- 第一章 End


# -------------------------------- 第二章 Start
def get_evaluate_gen_state(start_date, end_date, evaluate_info_this, evaluate_info_prev, year_evaluate_info):
    """
    干部履职存在问题数量的分析描述。对应
    (一)干部评价总体情况分析的前半截，概况的描述以及干部履职问题类型统计表。
    :param start_date: {date} 开始日期
    :param end_date: {date} 终止日期
    :param evaluate_info_this: 本期的履职评价数据
    :param evaluate_info_prev: 上一期的履职评价数据
    :param year_evaluate_info: 本年度的累计履职评价数据
    :return:
    """
    evaluate_info = evaluate_info_this.copy()
    evaluate_review = pd_query(
        CHECK_EVALUATE_REVIEW_SQL.format(start_date.strftime('%Y-01-01'), end_date.strftime('%Y-%m-%d')))
    total_ring = calc_ratio(evaluate_info.shape[0], evaluate_info_prev.shape[0])
    # 定期评价人次
    regular_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 2].shape[0]
    # 逐条评价人次
    one_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 1].shape[0]
    # 履职问题个数
    evaluate_count = evaluate_info.shape[0]
    # 履职问题人数
    evaluate_person = len(set(evaluate_info['RESPONSIBE_ID_CARD']))
    # 工人履职数据
    work_data = evaluate_info[evaluate_info['LEVEL'] == "工人"]
    # 工人条数和人数
    work_count, work_person = work_data.shape[0], len(
        set(work_data['RESPONSIBE_ID_CARD']))
    # 安全谈心数据
    talk_data = evaluate_info[evaluate_info['ITEM_NAME'] == '安全谈心']
    talk_count, talk_person = talk_data.shape[0], len(
        set(talk_data['RESPONSIBE_ID_CARD']))
    # 共计评价得分
    total_score = sum(list(evaluate_info['SCORE']))
    # 最高评价计分
    score_data = evaluate_info.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    most_score = round(score_data.at[0, 'SCORE'], 1)
    # 今年得分数据
    score_year_data = year_evaluate_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_total_count = score_year_data.shape[0]
    month_percent = round(evaluate_count / year_total_count * 100, 1)
    most_year_score = round(score_year_data.at[0, 'SCORE'], 1)
    punish_year_count = score_year_data[score_year_data['SCORE'] >= 2].shape[0]
    punish_count = score_data[score_data['SCORE'] >= 2].shape[0]
    score_data['level'] = score_data['SCORE'].apply(evaluate_deduct_level)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        evaluate_deduct_level)
    p_level_rst = []
    for level in EVALUATE_DEDUCT_LEVELS:
        p_level_rst.append({
            "level": EVALUATE_DEDUCT_LEVEL_NAMES.get(level),
            "total_count": score_year_data[score_year_data['level'] == level].shape[0],
            "month_count": score_data[score_data['level'] == level].shape[0]
        })
    score_rst = {
        "year_total_count": year_total_count,
        "month_count": evaluate_count,
        "month_percent": month_percent,
        "most_year_score": most_year_score,
        "punish_year_count": punish_year_count,
        "punish_count": punish_count,
        "p_level_value": p_level_rst,
        "t_level_value": p_level_rst[::-1]
    }
    rst = {
        'regular_count': regular_count,
        'one_count': one_count,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'work_count': work_count,
        'work_person': work_person,
        'talk_count': talk_count,
        'talk_person': talk_person,
        'total_score': total_score,
        'most_score': most_score,
        'total_ring': total_ring,
        'score_level_value': score_rst
    }
    return rst


def get_evaluate_problem_count_description(evaluate_info_this, evaluate_info_prev):
    """
    干部履职存在问题数量的分析描述。对应
    (一)干部评价总体情况分析【的中间部分】
    六大干部履职存在问题数量统计以及问题的描述
    :param evaluate_info_this: 本月的情况
    :param evaluate_info_prev: 上一个月的情况
    :return:
    """
    data0 = evaluate_info_this[evaluate_info_this['ITEM_NAME'].isin(EVALUATE_PROBLEM_TYPE_NAMES)].copy()
    data1 = evaluate_info_prev[evaluate_info_prev['ITEM_NAME'].isin(EVALUATE_PROBLEM_TYPE_NAMES)]
    # 计算干部履职问题类型统计表
    pro_type_table = []
    pro_total_count = data0.shape[0]
    problem_ratio = calc_ratio(len(data0), len(data1))
    # 计算、增加数量的问题描述
    problem_inc_list = []
    problem_ded_list = []
    for item in EVALUATE_PROBLEM_TYPE_NAMES:
        count0 = len(data0[data0['ITEM_NAME'].str.contains(item)])
        count1 = len(data1[data1['ITEM_NAME'].str.contains(item)])
        share_percent = round(count0 / pro_total_count * 100, 1)
        prev_percent = calc_ratio(count0, count1)
        # 问题名称/数量/占比/环比
        pro_type_table.append({
            'pro_name': item,
            'count': count0,
            'share_percent': share_percent,
            'prev_percent': prev_percent
        })

        # 增减问题的描述
        if count0 > count1:
            problem_inc_list.append(item)
        else:
            problem_ded_list.append(item)
    problem_inc_text = '、'.join(problem_inc_list)
    problem_ded_text = '、'.join(problem_ded_list)

    # 求出问题做多的六个问题
    data0['ST'] = data0['SITUATION'].apply(lambda s: s.replace('\n', '').replace('。', ''))
    pro_gb = data0[['ITEM_NAME', 'SITUATION', 'PK_ID', 'ST']].groupby(['ITEM_NAME', 'SITUATION', 'ST']).count().reset_index()
    sub_pro = pro_gb.iloc[pro_gb.groupby(['ITEM_NAME']).apply(lambda x:x['PK_ID'].idxmax())]
    sub_pro = sub_pro.sort_values('PK_ID', ascending=False).head(6)

    # 干部履职突出问题记分类型统计表的六列
    hl_pro_table = []

    # static_gb聚合数据，方便每个问题的统计
    static_gb = data0[['ITEM_NAME', 'SITUATION', 'RESPONSIBE_ID_CARD', 'PK_ID', 'ST']].\
        groupby(['ITEM_NAME', 'SITUATION', 'RESPONSIBE_ID_CARD', 'ST']).count().reset_index()

    problem_desc_list = []
    for row in sub_pro.itertuples():
        # 问题的总数量/人数
        sub_static = static_gb[static_gb['ITEM_NAME'] == row.ITEM_NAME]
        pro_total_count = int(sub_static['PK_ID'].sum())
        pro_person_count = len(sub_static)
        classic_pro_count = int(sub_static[sub_static['SITUATION'].str.contains(row.ST)]['PK_ID'].sum())
        classic_ratio = round(100 * classic_pro_count / pro_total_count, 1)
        table_col = {
            'detail_name': row.ST,
            'detail_count': classic_pro_count,
            'detail_ratio': classic_ratio
        }
        hl_pro_table.append(table_col)

        # 子问题内部，也进行一次排序
        detail_problem_list = []
        st_gb = sub_static.groupby('ST').count().reset_index().sort_values('PK_ID', ascending=False)
        situation_list = st_gb['ST'].unique().tolist()

        # 子问题的数量/人数
        for sit in situation_list:
            pro_count = int(sub_static[sub_static['ST'] == sit]['PK_ID'].sum())
            person_count = len(sub_static[sub_static['ST'] == sit])
            detail_problem_list.append({
                'detail_name': sit,
                'pro_count': pro_count,
                'person_count': person_count
            })

        problem_desc_list.append({
            'detail_name': row.ST,
            'problem_name': row.ITEM_NAME,
            'pro_total_count': pro_total_count,
            'pro_person_count': pro_person_count,
            'detail_problem_list': detail_problem_list
        })

    result = {
        'pro_type_table': pro_type_table,  # 干部履职问题类型统计表
        'hl_pro_table': hl_pro_table,  # 突出问题统计表
        'problem_ratio': problem_ratio,
        'problem_inc_text': problem_inc_text,
        'problem_ded_text': problem_ded_text,
        'problem_inc_count': len(problem_inc_text.split('、')),
        'problem_desc_list': problem_desc_list
    }
    return result


def get_cadre_evaluate_state_analysis_of_major(evaluate_info_this, year_evaluate_info):
    """
    专业系统的干部履职评价情况分析
    :param evaluate_info_this: 当期的履职评价数据
    :param year_evaluate_info: 当年的累计评价数据
    :return:
    """
    all_year_info = year_evaluate_info.groupby(
        ['MAJOR', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    all_year_info['level'] = all_year_info['SCORE'].apply(evaluate_deduct_level)
    eval_info = evaluate_info_this
    totals = [
        all_year_info[all_year_info['level'] == i].shape[0]
        for i in range(1, 8)
    ]
    levels = [EVALUATE_DEDUCT_LEVEL_NAMES.get(i) for i in range(1, 8)]

    detail_data = _get_evaluate_major_detail_item_data(eval_info)
    major_count = eval_info.shape[0]
    group_check_count = eval_info[eval_info['CHECK_TYPE'] == 1].shape[0]
    station_check_count = eval_info[eval_info['CHECK_TYPE'] == 2].shape[0]
    eval_info = eval_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    if eval_info.empty:
        major_most_score = 0
    else:
        major_most_score = eval_info.at[0, 'SCORE']
    # major_year_info = all_year_info[all_year_info['MAJOR'] == major]
    # 年度内达到处理标准的人员数量
    major_year_count = all_year_info[all_year_info['SCORE'] >= 2].shape[0]
    # 本月份达到处理标准的人员数量
    major_punish = eval_info[eval_info['SCORE'] >= 2].shape[0]

    # 各个站段人员聚合
    station_info = evaluate_info_this
    station_gb = station_info.groupby(['STATION', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    station_gb = station_gb[station_gb['SCORE'] >= 2]
    station_gb = station_gb.groupby('STATION').count().sort_values(by='SCORE', ascending=False)
    station_list = [
        {
            'station_name': station_name,
            'station_punish_count': int(station_gb.loc[station_name, 'SCORE'])
        } for station_name in station_gb.index
    ]

    result = {
        "major_count": major_count,
        "group_check_count": group_check_count,
        "station_check_count": station_check_count,
        "major_most_score": major_most_score,
        "major_year_punish": major_year_count,
        "major_punish": major_punish,
        "level_headers": levels,
        "level_total_list": totals,
        "detail_data": detail_data,
        "station_data": station_list
    }
    return result


def _get_evaluate_major_detail_item_data(eval_info):
    """履职信息各专业突出问题数据
    Arguments:
        eval_info: {dataframe} -- 各专业原始数据
    Returns:
        dict -- 结果
    """
    major_data = eval_info.groupby(['CODE', 'SITUATION'])
    detail_item = []
    for name, group in major_data:
        score_count = group.shape[0]
        score_person = int(group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
        detail_item.append({
            'detail_name': name[1].replace("\n", ""),
            'code': name[0],
            'score_count': score_count,
            'score_person': score_person
        })
    detail_data = pd.DataFrame(detail_item)
    if detail_data.empty:
        return []
    detail_data = detail_data.sort_values(by='score_count', ascending=False).head(8)
    return [
        json.loads(detail_data.loc[index].T.to_json())
        for index in detail_data.index
    ]


# -------第二章，第三小节： 局领导履职情况分析
def get_leader_evaluate_situation(evaluate_info_this, evaluate_info_prev, year_evaluate_info):
    """
    第二章，第三小节： 局领导履职情况分析
    Returns:
        dict  -- 结果
    """
    evaluate_info = evaluate_info_this
    last_month_evaluate_info = evaluate_info_prev
    evaluate_year_info = year_evaluate_info

    leader_eval_info = evaluate_info[evaluate_info['GRADATION'] == '局管领导人员']
    total = leader_eval_info.shape[0]
    total_person = len(set(leader_eval_info['RESPONSIBE_ID_CARD']))
    month_leader_eval_info = last_month_evaluate_info[
        last_month_evaluate_info['GRADATION'] == '局管领导人员']
    # major_counts = []
    #
    # for major in ['工务']:
    #     major_counts.append({
    #         'name':
    #             major,
    #         'count':
    #             leader_eval_info[leader_eval_info['MAJOR'] == major].shape[0]
    #     })
    # 履职问题分布表
    '''
    item_rst = []
    for item in EVALUATE_PROBLEM_TYPE_NAMES:
        major_rst = []
        item_data = leader_eval_info[leader_eval_info['ITEM_NAME'] == item]
        item_month_data = month_leader_eval_info[month_leader_eval_info['ITEM_NAME'] == item]
        if item_data.empty is False:
            major_rst = _get_major_evaluate_table(item_data, item_month_data)
        else:
            major_rst.append({'name': '工务', 'count': 0, 'ring': 0})

            major_rst.append({'name': '合计', 'count': 0, 'ring': 0})
        item_rst.append(major_rst)
    '''

    pro_type_table = _get_eval_problem_type_table(leader_eval_info, month_leader_eval_info)
    # 问题统计
    niubi_info = leader_eval_info.groupby(['SITUATION'])
    pro_rst = []
    for name, group in niubi_info:
        pro_count = group.shape[0]
        pro_person = len(set(group['RESPONSIBE_ID_CARD']))
        pro_rst.append({
            "name": name,
            "count": pro_count,
            "person": pro_person
        })
    # 一分以上人员信息
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    leader_eval_info = leader_eval_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    punish_data = leader_eval_info[leader_eval_info['SCORE'] >= 1]
    punish_rst = [{
        "name": punish_data.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": punish_data.at[index, 'STATION'],
        "job": punish_data.at[index, 'JOB'],
        "score": round(punish_data.at[index, 'SCORE'], 1)
    } for index in punish_data.index]
    punish_data1 = leader_eval_info[leader_eval_info['SCORE'] < 1]
    punish_rst1 = [{
        "name": punish_data1.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": punish_data1.at[index, 'STATION'],
        "job": punish_data1.at[index, 'JOB'],
        "score": round(punish_data1.at[index, 'SCORE'], 1)
    } for index in punish_data1.index]
    # 全年干部表格表格
    year_info = evaluate_year_info[evaluate_year_info['GRADATION'] == '局管领导人员']
    year_data = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_cadre_total_score_table(year_data, leader_eval_info)
    # 累计处理人次
    year_punish = _get_punish_content(year_data, leader_eval_info)
    rst = {
        "names": EVALUATE_PROBLEM_TYPE_NAMES,
        "total": total,
        "total_person": total_person,
        # "table": item_rst,
        'pro_type_table': pro_type_table,
        "problem_total": pro_rst,
        # "major_total": major_counts,
        "punish_p": punish_rst,
        "punish_p1": punish_rst1,
        "year_table": year_table,
        "year_punish": year_punish
    }
    return rst


def _get_eval_problem_type_table(eval_info_this, eval_info_prev):
    """
    计算干部履职问题分布表
    :param eval_info_this: 本期的履职
    :param eval_info_prev: 上一期的履职
    :return:
    """
    pro_type_table = []
    data0 = eval_info_this
    data1 = eval_info_prev
    pro_total_count = data0.shape[0]
    for item in EVALUATE_PROBLEM_TYPE_NAMES:
        count0 = len(data0[data0['ITEM_NAME'] == item])
        count1 = len(data1[data1['ITEM_NAME'] == item])

        share_percent = round(count0 / (pro_total_count if pro_total_count!= 0 else 1) * 100, 1)
        prev_percent = calc_ratio(count0, count1)
        # 问题名称/数量/占比/环比
        pro_type_table.append({
            'pro_name': item,
            'count': count0,
            'share_percent': share_percent,
            'prev_percent': prev_percent
        })
    return pro_type_table


def _get_cadre_total_score_table(year_data, now_data):
    """获取干部总体得分表格数据
    Arguments:
        year_data {dataframe} -- 年累计数据
        now_data {dataframe} -- 当前一期数据
    Returns:
        dict
    """
    year_data['level'] = year_data['SCORE'].apply(evaluate_deduct_level)
    now_data['level'] = now_data['SCORE'].apply(evaluate_deduct_level)
    levels = []
    year_counts = []
    last_counts = []
    for level in range(1, 8):
        levels.append(EVALUATE_DEDUCT_LEVEL_NAMES.get(level))
        year_counts.append(year_data[year_data['level'] == level].shape[0])
        last_counts.append(now_data[now_data['level'] == level].shape[0])
    year_table = {
        'total': year_data.shape[0],
        'levels': levels,
        'counts': year_counts,
        'last_counts': last_counts
    }
    return year_table


def _get_punish_content(year_data, now_data):
    """获取处理结果信息

    Arguments:
        year_data {dataframe} -- 年累计数据
        now_data {dataframe} -- 当前一期数据

    Returns:
        dict -- 结果
    """
    year_punish = {
        'year_count': year_data[year_data['SCORE'] >= 2].shape[0],
        'month_count': now_data[now_data['SCORE'] >= 2].shape[0],
        'most_score': year_data.at[0, 'SCORE'],
        'most_person': year_data.at[0, 'RESPONSIBE_PERSON_NAME'],
        'most_station': year_data.at[0, 'STATION'],
        'most_job': year_data.at[0, 'JOB'],
    }
    return year_punish


def get_branch_level_cadre_situation(level, evaluate_info_this, year_evaluate_info):
    """正副科职履职信息数据

    Arguments:
        level {str} -- 正副科
        evaluate_info_this {dataframe} - 本期的评价数据
        year_evaluate_info {dataframe} - 本年累计评价数据
    Returns:
        dict -- 结果
    """
    evaluate_info = evaluate_info_this
    evaluate_year_info = year_evaluate_info
    majors = ['工务']
    if level == '副科级':
        levels = ['正处级', '副处级', '正科级']
        level_info = evaluate_info[(~evaluate_info['LEVEL'].isin(levels))]
    else:
        level_info = evaluate_info[(evaluate_info['LEVEL'] == level)]
    level_count = level_info.shape[0]
    level_person = len(set(level_info['RESPONSIBE_ID_CARD']))
    problem_info = level_info.groupby(
        ['CODE', 'SITUATION']).size().reset_index().rename(columns={0: 'count'}).sort_values(
        by='count', ascending=False).head(8)
    problem_rst = [
        str(problem_info.at[index, 'SITUATION']).strip() for index in problem_info.index
    ]
    major_rst = []
    for major in majors:
        major_info = level_info[level_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_person = len(set(major_info['RESPONSIBE_ID_CARD']))
        major_rst.append({
            "major_name": major,
            "major_count": major_count,
            "major_person": major_person
        })
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    level_info = level_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    high_score_rst = [{
        "name": level_info.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": level_info.at[index, 'STATION'],
        "job": level_info.at[index, 'JOB'],
        "score": level_info.at[index, 'SCORE']
    } for index in level_info.head(5).index]
    year_info = evaluate_year_info[(evaluate_year_info['LEVEL'] == level) & (evaluate_year_info['MAJOR'].isin(majors))]
    year_info = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_cadre_total_score_table(year_info, level_info)
    year_punish = _get_punish_content(year_info, level_info)
    result = {
        "total_count": level_count,
        "total_person": level_person,
        "problem_situation": problem_rst,
        "major_situation": major_rst,
        "high_score_situation": high_score_rst,
        "level_table": year_table,
        "punish_situation": year_punish
    }
    return result

# -------------------------------- 第二章 End


# --------------------------------------
# ---- 第三章 1小节检查履职质量分析 整体履职质量分析 业务部履职质量分析
def get_major_quality_analysis(evaluate_info_this, accident_data_arr, check_info_arr, check_problem_arr):
    try:
        evaluate_info = evaluate_info_this.copy()
        major_evaluate_table = get_evaluate_table(evaluate_info)

        acc_datas = [
            data[(data['PROFESSION'] == '工务') & (data['MAIN_TYPE'] == 1)]
            for data in accident_data_arr
        ]
        err_datas = [
            data[(data['PROFESSION'] == '工务') & (data['MAIN_TYPE'] == 2)]
            for data in accident_data_arr
        ]
        problem_datas = check_problem_arr
        check_datas = check_info_arr
        eva_data = evaluate_info[evaluate_info['STATION_ID'] == MAJOR_AMDIN_DEPT_ID]
        value = get_acc_err_check_eva_info(
            acc_datas, err_datas, problem_datas, check_datas, eva_data)

        result = {
            "major_evaluate_table": major_evaluate_table,
            "yunshu_accident_situation": value
        }
        return result
    except Exception as ex:
        print(ex)
        return {}


# ---第三章 2小节 站段履职质量分析
def get_station_quality_analysis(evaluate_info_this, accident_data_arr, check_info_arr, check_problem_arr, dept_data):
    evaluate_info = evaluate_info_this
    res_acc_datas = [
        data[(data['MAIN_TYPE'] == 1) & (data['RESPONSIBILITY_NAME'] == 1)]
        for data in accident_data_arr
    ]
    res_err_datas = [data[(data['MAIN_TYPE'] == 2)] for data in accident_data_arr]
    major_acc_datas = res_acc_datas
    major_err_datas = res_err_datas
    stations = dept_data[dept_data['DEPT_TYPE'] == 4]['ALL_NAME'].tolist()
    station_list = []
    for station in stations:
        sta_info = [
            data[data['STATION'] == station] for data in check_info_arr
        ]
        sta_pro = [data[data['STATION'] == station] for data in check_problem_arr]
        sta_acc_datas = [
            data[(data['STATION'] == station)] for data in major_acc_datas
        ]
        sta_err_datas = [
            data[data['STATION'] == station] for data in major_err_datas
        ]
        sta_evaluate = evaluate_info[evaluate_info['STATION'] == station]
        value = get_acc_err_check_eva_info(
            sta_acc_datas, sta_err_datas, sta_pro, sta_info, sta_evaluate)
        station_list.append({"station": station, "value": value})

    return {'major': '工务', 'station_list': station_list}


def get_evaluate_table(evaluate):
    evaluate_way = {
        0: "自动评价",
        1: "逐条评价",
        2: "定期评价",
        3: "阶段评价",
    }
    evaluate = evaluate.sort_values(by='SCORE', ascending=False).head(10)
    major_evaluate_table = []
    for index in evaluate.index:
        value = evaluate.loc[index]
        major_evaluate_table.append({
            "all_name":
                value['ALL_NAME'],
            "name":
                value['RESPONSIBE_PERSON_NAME'],
            "job":
                value['GRADATION'],
            "y_m":
                f"{value['YEAR']}/{value['MONTH']:0>2}",
            "time":
                value['CREATE_TIME'],
            "way":
                evaluate_way[value['EVALUATE_WAY']],
            "content":
                value['EVALUATE_CONTENT'],
            "score":
                value['SCORE']
        })
    return major_evaluate_table


def get_acc_err_check_eva_info(acc_datas, err_datas, pro_datas, info_datas, eva_data):
    # 事故比较情况
    acc_counts = [data.drop_duplicates('PK_ID').shape[0] for data in acc_datas]
    acc_total = acc_counts[0]
    acc_ratio = calculate_comparison_statistic(acc_counts[0], acc_counts[1], acc_counts[2])
    # 故障比较情况
    err_counts = [data.drop_duplicates('PK_ID').shape[0] for data in err_datas]
    err_total = err_counts[0]
    err_ratio = calculate_comparison_statistic(err_counts[0], err_counts[1], err_counts[2])
    # 检查信息比较情况
    info_counts = [data.drop_duplicates('PK_ID').shape[0] for data in info_datas]
    info_total = info_counts[0]
    info_ratio = calculate_comparison_statistic(info_counts[0], info_counts[1], info_counts[2])
    # 检查问题比较情况
    pro_counts = [data.drop_duplicates('PK_ID').shape[0] for data in pro_datas]
    pro_total = pro_counts[0]
    pro_ratio = calculate_comparison_statistic(pro_counts[0], pro_counts[1], pro_counts[2])
    # 严重问题比较情况
    pro_ser_counts = [data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])].shape[0] for data in pro_datas]
    pro_ser_total = pro_ser_counts[0]
    pro_ser_ratio = calculate_comparison_statistic(pro_ser_counts[0], pro_ser_counts[1], pro_ser_counts[2])
    # 检查问题质量分
    pro_score_counts = [round(sum(list(data['CHECK_SCORE'])), 2) for data in pro_datas]
    pro_score_total = pro_score_counts[0]
    pro_score_ratio = calculate_comparison_statistic(pro_score_counts[0], pro_score_counts[1], pro_score_counts[2])
    # 履职评价信息
    total = eva_data.shape[0]
    values = [
        eva_data[eva_data['ITEM_NAME'] == item].shape[0] for item in EVALUATE_PROBLEM_TYPE_NAMES
    ]
    value = {
        "acc_total": acc_total,
        "acc_ratio": acc_ratio,
        "err_total": err_total,
        "err_ratio": err_ratio,
        "info_total": info_total,
        "info_ratio": info_ratio,
        "pro_total": pro_total,
        "pro_ratio": pro_ratio,
        "pro_ser_total": pro_ser_total,
        "pro_ser_ratio": pro_ser_ratio,
        "pro_score_total": pro_score_total,
        "pro_score_ratio": pro_score_ratio,
        "evaluate": {
            "total":
                total,
            "value": [{
                "name": name,
                "count": values[idx]
            } for idx, name in enumerate(EVALUATE_PROBLEM_TYPE_NAMES)]
        }
    }
    return value
# -------------------------------- 第三章 End


# -------------------------------- 第四章 Start
# 第一小节 1.工务系统6月份开展
def get_sys_analysis_of_major(check_info_this, check_info_prev, check_problem_this, check_problem_prev):
    check_data = check_info_this
    check_data = check_data.drop_duplicates('PK_ID')
    # 检查X次，环比Y
    major_check_count = check_data.shape[0]
    check_data_prev = check_info_prev.drop_duplicates('PK_ID')
    major_check_ratio_month = calc_ratio(major_check_count, check_data_prev.shape[0])

    # 查处问题X个，环比上升X%
    major_problem_count = int(check_data['PROBLEM_NUMBER'].sum())
    major_problem_ratio_month = calc_ratio(major_problem_count,
                                           check_data_prev['PROBLEM_NUMBER'].sum())

    # 查处性质严重问题X，环比
    problem_data = check_problem_this.drop_duplicates('PK_ID')
    problem_data_mon = check_problem_prev.drop_duplicates('PK_ID')
    ser_pro_data = problem_data[problem_data['RISK_LEVEL'].isin([1, 2])]
    serious_problem_count = ser_pro_data.shape[0]
    serious_problem_ratio_month = calc_ratio(serious_problem_count,
                                             problem_data_mon[problem_data_mon['RISK_LEVEL'].isin([1, 2])].shape[0])

    # 其中作业问题 ....
    cly_gb = problem_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PK_ID', ascending=False)
    classify_list = [
        {
            'classify_name': row.Index,
            'count': row.PK_ID
        } for row in cly_gb.itertuples()
    ]

    # 高质量问题查处较高 LEVEL
    high_lv_data = problem_data[problem_data['LEVEL'].isin(CHECK_PROBLEM_HIGH_LEVEL)]
    hl_gb = high_lv_data.groupby('STATION').count().sort_values(by='PK_ID', ascending=False)
    high_level_head_list = [
        {
            'station': row.Index,
            'count': row.PK_ID
        } for row in hl_gb.head(3).itertuples()
    ]

    hl_gb.sort_values(by='PK_ID', inplace=True)
    high_level_tail_list = [
        {
            'station': row.Index,
            'count': row.PK_ID
        } for row in hl_gb.head(3).itertuples()
    ]

    result = {
        'major_check_count': major_check_count,
        'major_check_ratio_month': major_check_ratio_month,
        'major_check_ratio_month_text': get_percent_text(major_check_ratio_month),
        'major_problem_count': major_problem_count,
        'major_problem_ratio_month': major_problem_ratio_month,
        'major_problem_ratio_month_text': get_percent_text(major_problem_ratio_month),
        'serious_problem_count': serious_problem_count,
        'serious_problem_ratio_month': serious_problem_ratio_month,
        'serious_problem_ratio_month_text': get_percent_text(serious_problem_ratio_month),
        'classify_list': classify_list,
        'high_level_head_list': high_level_head_list,
        'high_level_tail_list': high_level_tail_list
    }
    return result


def get_sys_analysis_of_admin_dept(dept_data, check_info_this, check_info_prev,
                                   check_problem_this, check_problem_prev):
    """
    四、安全管理信息系统检查信息及问题分析
    工务部的分析
    :param dept_data: 工务的部门数据
    :param check_info_this: 本期检查数据
    :param check_info_prev: 上一期检查数据
    :param check_problem_this: 本期检查问题数据
    :param check_problem_prev: 上一期检查问题数据
    :return:
    """
    check_data = check_info_this[check_info_this['TYPE3'] == MAJOR_AMDIN_DEPT_ID].copy()
    check_data = check_data.drop_duplicates('PK_ID')
    check_info_data_mom = check_info_prev[check_info_prev['TYPE3'] == MAJOR_AMDIN_DEPT_ID]
    check_info_data_mom = check_info_data_mom.drop_duplicates('PK_ID')
    # 检查X次，环比Y
    major_check_count = check_data.shape[0]
    major_check_ratio_month = calc_ratio(major_check_count, check_info_data_mom.shape[0])

    # 查处问题X个，环比上升X%
    major_problem_count = int(check_data['PROBLEM_NUMBER'].sum())
    major_problem_ratio_month = calc_ratio(major_problem_count,
                                           check_info_data_mom['PROBLEM_NUMBER'].sum())

    # 查处性质严重问题X，环比
    problem_data = check_problem_this
    problem_data = problem_data[problem_data['STATION_ID'] == MAJOR_AMDIN_DEPT_ID].copy().drop_duplicates('PK_ID')
    problem_data_mon = check_problem_prev
    problem_data_mon = problem_data_mon[problem_data_mon['STATION_ID'] == MAJOR_AMDIN_DEPT_ID].drop_duplicates('PK_ID')
    ser_pro_data = problem_data[problem_data['RISK_LEVEL'].isin([1, 2])]
    serious_problem_count = ser_pro_data.shape[0]
    serious_problem_ratio_month = calc_ratio(serious_problem_count,
                                             problem_data_mon[problem_data_mon['RISK_LEVEL'].isin([1, 2])].shape[0])

    # 其中作业问题 ....
    cly_gb = problem_data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='PK_ID', ascending=False)
    classify_list = [
        {
            'classify_name': row.Index,
            'count': row.PK_ID
        } for row in cly_gb.itertuples()
    ]

    # 关键数据统计表
    problem_levels = ['A', 'B', 'C', 'D', 'E1', 'E2', 'E3', 'E4', 'F1', 'F2', 'F3', 'F4', 'G1', 'G2', 'G3',
                      'K1', 'K2', 'K3', 'K4']
    main_levels = ['A', 'B', 'F1', 'F2', 'E1', 'E2']
    check_data['SECTION_NAME'] = check_data['ALL_NAME'].apply(extract_section_from_all_name)
    problem_data['SECTION_NAME'] = problem_data['CHECK_DEPT_NAME'].apply(extract_section_from_all_name)

    # 工务处所有科室
    dp = dept_data[~((dept_data['ALL_NAME'].str.contains('二线人员')) | (dept_data['ALL_NAME'].str.contains('领导')))]
    dp = dp[(dp['DEPT_TYPE'] == 6) & (dp['STATION_ID'] == MAJOR_AMDIN_DEPT_ID)].copy()
    dp['SECTION_SHORT'] = dp['ALL_NAME'].apply(lambda name: name.split('-')[-1])
    key_stat_rows = []
    for row in dp.itertuples():
        sub_pro_data = problem_data[problem_data['SECTION_NAME'] == row.ALL_NAME]
        pro_level_list = [
            len(sub_pro_data[sub_pro_data['LEVEL'] == level]) for level in problem_levels
        ]
        item = {
            'section': row.SECTION_SHORT,
            'check_count': check_data[check_data['SECTION_NAME'] == row.ALL_NAME].shape[0],
            'pro_level_list': pro_level_list,
            'pro_total': sum(pro_level_list),
            'main_problem': len(sub_pro_data[sub_pro_data['LEVEL'].isin(main_levels)])
        }
        key_stat_rows.append(item)
    pro_level_list = [
        len(problem_data[problem_data['LEVEL'] == level]) for level in problem_levels
    ]
    key_stat_rows.append(
        {
            'section': '合计',
            'check_count': check_data.shape[0],
            'pro_level_list': pro_level_list,
            'pro_total': len(problem_data),
            'main_problem': len(problem_data[problem_data['LEVEL'].isin(main_levels)])
        }
    )

    result = {
        'major_check_count': major_check_count,
        'major_check_ratio_month': major_check_ratio_month,
        'major_check_ratio_month_text': get_percent_text(major_check_ratio_month),
        'major_problem_count': major_problem_count,
        'major_problem_ratio_month': major_problem_ratio_month,
        'major_problem_ratio_month_text': get_percent_text(major_problem_ratio_month),
        'serious_problem_count': serious_problem_count,
        'serious_problem_ratio_month': serious_problem_ratio_month,
        'serious_problem_ratio_month_text': get_percent_text(serious_problem_ratio_month),
        'classify_list': classify_list,
        'key_stat_headers': problem_levels,
        'key_stat_rows': key_stat_rows
    }
    return result
# -------------------------------- 第四章 End


# ----------------第六章 start
def get_violation_person_distribution(year, month):
    """
    根据给定的年月，计算整改情况
    :param year: {int} 年
    :param month: {int} 月
    :return:
    """
    # 加载违章人员清单
    person_data = pd_query(VIOLATION_PERSON_LIST_SQL.format(year, month))
    person_data['STATION'] = person_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])
    # 计算每个站段的人数
    person_gb = person_data.groupby(['STATION']).count()
    # 按站段分布输出
    advice_list = [
        {
            'station_name': row.Index,
            'count': row.ID_CARD
        } for row in person_gb.itertuples()
    ]

    # 加载确认的人员清单
    confirm_data = pd_query(VIOLATION_PERSON_CONFIRM_LIST_SQL.format(year, month))
    confirm_data['STATION'] = confirm_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])
    confirm_data.drop_duplicates(['STATION', 'ID_CARD'], inplace=True)
    confirm_gb = confirm_data.groupby(['STATION']).count()
    confirm_list = [
        {
            'station_name': row.Index,
            'count': row.ID_CARD
        } for row in confirm_gb.itertuples()
    ]
    content = {
        'advice_total_count': person_data.shape[0],
        'advice_list': advice_list,
        'confirm_total_count': confirm_data.shape[0],
        'confirm_list': confirm_list
    }

    return content


# X月“违章大王”整改情况
def get_violation_person_rectification(year, month):
    """
    根据给定的年月，计算整改情况
    :param year: {int} 年
    :param month: {int} 月
    :return:
    """
    ref_data = pd_query(VIOLATION_PERSON_CONFIRM_LIST_SQL.format(year, month))
    # 前期已处置不出现在月报里
    ref_data = ref_data[ref_data['FK_DISPOSE_METHOD_IDS'] != '5']
    ref_data['STATION'] = ref_data['DEPT_ALL_NAME'].apply(lambda name: name.split('-')[0])
    ref_data['METHOD_NAME'] = ''

    # 整改方法，是以ID的清单存在FK_DISPOSE_METHOD_IDS字段里面，将它，变成dataframe的列，以便统计
    for key, val in VIOLATION_PERSON_DISPOSE_METHODS.items():
        field_name = f'METHOD_{key}'
        ref_data[field_name] = 0
    for index in ref_data.index:
        methods = ref_data.loc[index, 'FK_DISPOSE_METHOD_IDS']
        if not methods:
            continue

        method_name = ''
        for mth_id in methods.split(','):
            method_name = method_name + VIOLATION_PERSON_DISPOSE_METHODS.get(mth_id) + ','
            field_name = f'METHOD_{mth_id}'
            ref_data.at[index, field_name] = 1
        method_name = method_name[:-1]
        ref_data.at[index, 'METHOD_NAME'] = method_name

    # 遍历站段, 求每个站段的不同整改方法的人数
    ref_gb = ref_data.groupby(['STATION']).count()
    rectification_list = []
    for row in ref_gb.itertuples():
        method_list = []
        for key, val in VIOLATION_PERSON_DISPOSE_METHODS.items():
            field_name = f'METHOD_{key}'
            if getattr(row, field_name) > 0:
                method_list.append({
                    'method': val,
                    'count': getattr(row, field_name)
                })
        phase = {
            'station_name': row.Index,
            'total': row.ID_CARD,
            'method_list': method_list
        }
        rectification_list.append(phase)

    table_gb = ref_data.groupby(['STATION', 'METHOD_NAME']).count().reset_index()
    table_station_list = []
    for row in table_gb.itertuples():
        item = {
            'station_name': row.STATION,
            'method_name': row.METHOD_NAME,
            'count': row.ID_CARD
        }
        table_station_list.append(item)

    # total_count = ref_data[ref_data['FK_DISPOSE_METHOD_IDS'] != '5'].shape[0]
    content = {
        'period_text': f'{year}年{month}月',
        'total_count': ref_data.shape[0],
        'rectification_list': rectification_list,
        'table_station_list': table_station_list
    }
    return content

# ----------------第六章 End


# ----------------第七章 Start
def get_top_check_problem(start_date, end_date):
    """
    起止日期之内的检查问题的清单
    :param start_date: 开始日期
    :param end_date: 终止日期
    :return: {list}, 元素 {'problem_point': 'xxxx', 'count': 888}
    """

    data = pd_query(TOP_CHECK_PROBLEM_POINT_SQL.format(start_date.strftime('%Y-%m-%d'),
                                                       end_date.strftime('%Y-%m-%d')))
    data = data.sort_values('PRO_CT', ascending=False).iloc[0:20]
    problem_list = [
        {
            'problem_point': row.PROBLEM_POINT,
            'count': int(row.PRO_CT)
        } for row in data.itertuples()
    ]
    return problem_list


def get_health_index_doc63(year, month, station_id):
    """
    重点安全指数文档的6.3中的内容
    此处抄写的是safety_index_common_func.py中的export_health_index_word
    :param year:
    :param month:
    :param station_id:
    :return:
    """
    # station_id = '19B8C3534E135665E0539106C00A58FD'
    mon = year * 100 + month
    coll_name_prefix = 'health'
    main_type = 6
    weight_data = list(mongo.db['monthly_base_detail_index_weight'].find(
        {
            'INDEX_TYPE': 0,
            'MAIN_TYPE': main_type,
            'MON': mon,
            'DETAIL_TYPE': 3
        }, {
            '_id': 0,
            'WEIGHT': 1,
            'DETAIL_TYPE': 1
        }))
    if len(weight_data) == 0:
        weight_data = list(mongo.db['base_detail_index_weight'].find(
            {
                'INDEX_TYPE': 0,
                'MAIN_TYPE': main_type,
                'DETAIL_TYPE': 3
            }, {
                '_id': 0,
                'WEIGHT': 1,
                'DETAIL_TYPE': 1
            }))
    prefix = choose_collection_prefix(mon)
    basic_data = list(mongo.db[f'{prefix}{coll_name_prefix}_index_basic_data'].find({
        'DEPARTMENT_ID':
            station_id,
        'MON':
            mon,
        'MAIN_TYPE':
            main_type,
    }))
    if len(basic_data) < 1:
        return "NO DATA"
    basic_data_dict = {item['DETAIL_TYPE']: item for item in basic_data}
    # weight_data 只有一条数据
    for idx, item in enumerate(weight_data):
        if item['DETAIL_TYPE'] not in basic_data_dict:
            continue
        row = basic_data_dict.get(item['DETAIL_TYPE'])
        formula_str = get_child_calc_formula(row['DETAIL_TYPE'], main_type)
        if row['TYPE'] == 1:
            # 处理2月份之前版本指数，没有专业平均分
            if 'AVG_QUOTIENT' not in row:
                avg_quotient = row['AVG_SCORE']
                avg_score = '暂无'
            else:
                avg_quotient = row['AVG_QUOTIENT']
                avg_score = row['AVG_SCORE']
            index_paragraph = formula_str.format(
                row['SCORE'], int(
                    row['RANK']), avg_quotient, row['QUOTIENT'],
                row['NUMERATOR'], row['DENOMINATOR'], avg_score)
        else:
            cnt = row['CONTENT']
            if pd.isnull(cnt):
                cnt = '暂无数据'
            index_paragraph = formula_str.format(cnt)
            index_paragraph.replace('<p>', '').replace('</p>', '\n').replace('<br/>', '\n')
        return index_paragraph

# ----------------第七章 End
