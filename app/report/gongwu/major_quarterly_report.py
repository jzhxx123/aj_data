"""
计算工务专业的月度分析报告
Created: 2019-08-05
@author: Lu Jionglin
"""
import logging
import os
from flask import current_app

from docxtpl import DocxTemplate
from . import major_quarterly_report_data as report_data
from app import mongo
import app.report.analysis_report_manager as manager


def execute(update_month):
    """计算全履职得分调用函数
    """
    month = update_month.year_month % 100
    year = update_month.year_month // 100
    try:
        report = GongwuMajorQuarterlyAnalysisReport()
        report.generate_report(year, month//3 + 1)
        current_app.logger.debug(f'({month}) to mongo is done!!!')
        return 'OK'
    except Exception as ex:
        logging.exception('Analysis Report Error:')
        logging.exception(str(ex))
        current_app.logger.debug(f'analysis_report {month} is wrong')
        return f'{update_month.year_month} ERROR'


class GongwuMajorQuarterlyAnalysisReport(manager.QuarterlyAnalysisReport):
    """
    工务的专业级的季度分析报告类。
    """

    def __init__(self):
        super(GongwuMajorQuarterlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='工务')

    def generate_report_data(self, year, quarter):
        """
        执行生成指定季度的专业级报表的数据，并将数据持久化
        :param year: int 年
        :param quarter: int 季度 1 - 4
        :return:
        """
        data = report_data.get_data(year, quarter)
        mongo.db['safety_analysis_quarterly_report'].delete_one(
            {
                "year": year,
                "quarter": quarter,
                "hierarchy": "MAJOR"
            })
        mongo.db['safety_analysis_quarterly_report'].insert_one(data)
        return data

    def generate_report(self, year, quarter):
        """
        生成报告文件。因为需要插入图片，需要重写父类方法
        :param year: {int} 年份
        :param quarter: {int} 月份
        :return: {tuple} 文件目录, 文件名
        """
        data = self.load_report_data(year, quarter)
        tpl = DocxTemplate('app/report/template/gongwu_major_quarterly_report.docx')
        report_data.insert_images(data, tpl)
        tpl.render(data)
        # 报告保存
        dir_path = manager.get_report_path(self.interval_type.lower(), self.major)
        file_path = os.path.join(dir_path, data['file_name'])
        tpl.save(file_path)
        return dir_path, data['file_name']


