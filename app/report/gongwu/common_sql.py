from app.utils.common_func import ALL_MAJOR_DEPARTMENT_ID_DICT

# 工务专业的事故信息SQL
PRODUCE_INFO_ACCIDENT_SQL = """
    SELECT DISTINCT
        a.PK_ID,
        a.DETAIL_TYPE,
        a.CATEGORY,
        a.CODE AS INFO_CODE,
        a.NAME AS INFO_NAME,
        b.TYPE AS RESP_TYPE,
        b.RESPONSIBILITY_IDENTIFIED,
        c.DEPARTMENT_ID AS RESP_UNIT_ID,
        c.TYPE3 AS RESP_STATION_ID,
        a.OVERVIEW
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE DATE(a.OCCURRENCE_TIME) BETWEEN date('{0}') AND date('{1}')
            AND c.TYPE2 = '%s' AND a.MAIN_TYPE = 1
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务的履职评价信息查询语句
CHECK_EVALUATE_INFO_SQL = """SELECT
        a.PK_ID, a.RESPONSIBE_ID_CARD, a.LEVEL, a.SCORE, a.CODE,
        a.CHECK_TYPE, a.GRADATION, a.RESPONSIBE_PERSON_NAME,
        a.YEAR, a.MONTH, a.CREATE_TIME, a.EVALUATE_WAY, a.EVALUATE_CONTENT,
        a.CODE_ADDITION,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        '工务' AS MAJOR,
        e.JOB,
        f.NAME AS STATION,
        f.DEPARTMENT_ID AS STATION_ID
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        CREATE_TIME BETWEEN '{} 00:00:00' AND '{} 23:59:59'
        AND c.TYPE2 = '%s'
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务的履职评价信息查询语句
CHECK_EVALUATE_INFO_EX_SQL = """SELECT
        a.PK_ID, a.RESPONSIBE_ID_CARD, a.LEVEL, a.SCORE, a.CODE,
        a.CHECK_TYPE, a.GRADATION, a.RESPONSIBE_PERSON_NAME,
        a.YEAR, a.MONTH, a.CREATE_TIME, a.EVALUATE_WAY, a.EVALUATE_CONTENT,
        a.CODE_ADDITION,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY, c.ALL_NAME, c.TYPE AS DEPT_TYPE, 
        c.TYPE3 AS STATION_ID, c.TYPE4,
        '工务' AS MAJOR,
        e.JOB
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
    WHERE
        a.CREATE_TIME BETWEEN date('{}') AND date('{}')
        AND c.TYPE2 = '%s'
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务的履职评价信息查询语句
CHECK_EVALUATE_INFO_STATION_SQL = """SELECT
        a.PK_ID, a.RESPONSIBE_ID_CARD, a.LEVEL, a.SCORE, a.CODE,
        a.CHECK_TYPE, a.GRADATION, a.RESPONSIBE_PERSON_NAME,
        a.YEAR, a.MONTH, a.CREATE_TIME, a.EVALUATE_WAY, a.EVALUATE_CONTENT,
        a.CODE_ADDITION,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY, c.ALL_NAME, c.TYPE AS DEPT_TYPE, 
        c.TYPE3 AS STATION_ID, c.TYPE4,
        '工务' AS MAJOR,
        e.JOB
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
    WHERE
        a.CREATE_TIME BETWEEN date('{}') AND date('{}')
        AND c.TYPE3 = '{2}'
"""

# 履职复查信息查询语句
CHECK_EVALUATE_REVIEW_SQL = """SELECT
        a.PK_ID, a.EVALUATE_WAY, c.DEPARTMENT_ID, c.ALL_NAME, c.TYPE3
    FROM
        t_check_evaluate_review_person a,
        t_person b,
        t_department c
    WHERE
        a.RESPONSIBE_ID_CARD = b.id_card AND b.fk_department_id = c.department_id
        AND a.CREATE_TIME BETWEEN date('{}') AND date('{}')
        AND c.TYPE2 = '%s'
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

STATION_CADRE_COUNT_SQL = """SELECT
        sum( 1 ) AS TOTAL,
        c.NAME AS MAJOR,
        d.NAME AS STATION
    FROM
        t_person AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
        LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = b.TYPE3
    WHERE
        b.BUSINESS_CLASSIFY = '领导'
        AND b.TYPE2 = '%s'
        AND d.TYPE=4
    GROUP BY
        c.NAME,
        d.NAME
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

LEADER_CHECK_INFO_SQL = """SELECT
        a.PK_ID,
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS ALL_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER,
        b.COST_TIME
    FROM
        (select * from t_check_info where SUBMIT_TIME >= date('{}') AND SUBMIT_TIME <= date('{}'))  AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
"""

LEADER_CHECK_INFO_STATION_SQL = """SELECT
        a.PK_ID,
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS ALL_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER,
        b.COST_TIME
    FROM
        (select * from t_check_info where SUBMIT_TIME >= date('{}') AND SUBMIT_TIME <= date('{}'))  AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
    WHERE a.DEPARTMENT_ALL_NAME LIKE '%%{}%%'
"""

"""SELECT
        a.PK_ID,
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS ALL_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER,
        b.COST_TIME
    FROM
        (select * from t_check_info where SUBMIT_TIME >= date('{}') AND SUBMIT_TIME <= date('{}'))  AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        LEFT JOIN t_check_info_and_person AS c ON a.PK_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN t_department AS d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
    WHERE d.TYPE2='%s'
"""


LEADER_CHECK_PROBLEM_SQL = """SELECT DISTINCT
        a.LEVEL,
        a.PROBLEM_SCORE,
        b.DEPARTMENT_ALL_NAME AS ALL_NAME
    FROM
        t_check_problem AS a
        LEFT JOIN t_check_info AS b ON b.PK_ID = a.FK_CHECK_INFO_ID
    WHERE
        b.DEPARTMENT_ALL_NAME LIKE "%%领导%%"
        AND b.SUBMIT_TIME between date('{}') and date('{}')    
"""

LEADER_CHECK_PROBLEM_STATION_SQL = """SELECT DISTINCT
        a.LEVEL,
        a.PROBLEM_SCORE,
        b.DEPARTMENT_ALL_NAME AS ALL_NAME
    FROM
        t_check_problem AS a
        LEFT JOIN t_check_info AS b ON b.PK_ID = a.FK_CHECK_INFO_ID
    WHERE
        b.DEPARTMENT_ALL_NAME LIKE "%%领导%%"
        AND b.DEPARTMENT_ALL_NAME LIKE "%%{2}%%"
        AND b.SUBMIT_TIME between date('{0}') AND date('{1}')
"""

# 检查实体信息清单
CHECK_INFO_DATA_SQL = """SELECT
        a.PK_ID,
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS CHECK_DEPT_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER
    FROM
        t_check_info AS a        
    WHERE a.SUBMIT_TIME BETWEEN date('{}') AND date('{}')  
"""

# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME AS STATION,c.RISK_NAME,e.IDENTITY,d.DEPARTMENT_ID AS TYPE3,
a.ALL_NAME
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '%s'""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']


# 本月的检查项目所关联的问题
CHECK_PROBLEM_DATA_SQL = """SELECT
    a.PK_ID,
    a.PROBLEM_CLASSITY_NAME,
    a.LEVEL,
    a.RESPONSIBILITY_DEPARTMENT_NAME AS RESP_DEPT_NAME,
    a.CHECK_ITEM_NAME,
    a.SERIOUS_VALUE,
    a.PROBLEM_SCORE,
    a.RISK_LEVEL,
    b.PERSON_NAME,
    b.ALL_NAME AS CHECK_DEPT_NAME,
    c.TYPE3 AS STATION_ID,
    d.CHECK_SCORE,
    a.EXECUTE_DEPARTMENT_ID
    FROM 
        t_check_problem a
        LEFT JOIN t_check_info_and_person b ON a.FK_CHECK_INFO_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN t_department c ON b.fk_department_id = c.department_id 
        LEFT JOIN t_department e on e.department_id = c.TYPE3
        LEFT JOIN t_problem_base d on d.PK_ID=a.FK_PROBLEM_BASE_ID
    WHERE a.SUBMIT_TIME BETWEEN date('{}') AND date('{}')
    AND c.TYPE2 = '%s'
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务系统的所有部门的清单数据
MAJOR_DEPARTMENT_DATA_SQL = """select
    a.DEPARTMENT_ID,
    a.ALL_NAME,
    a.TYPE AS DEPT_TYPE,
    b.NAME AS STATION,
    b.DEPARTMENT_ID AS STATION_ID
    FROM t_department a, t_department b 
    WHERE a.TYPE3 = b.DEPARTMENT_ID 
    AND a.TYPE2 = '%s' AND a.IS_DELETE = 0
    AND LENGTH(b.SHORT_NAME) > 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务系统的违章大王
VIOLATION_PERSON_LIST_SQL = """SELECT DISTINCT
        a.ID_CARD,
        b.ALL_NAME AS DEPT_ALL_NAME
    FROM
        t_warning_key_person_library a
        LEFT JOIN t_department b ON a.FK_UNIT_ID = b.department_id 
    WHERE
        a.year = {} AND a.month = {}
        AND b.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

VIOLATION_PERSON_LIST_SQL2 = """SELECT DISTINCT
        a.ID_CARD,
        b.ALL_NAME AS DEPT_ALL_NAME
    FROM
        t_warning_key_person_library a
        LEFT JOIN t_department b ON a.FK_UNIT_ID = b.department_id 
    WHERE
        a.year = {} AND a.month in {}
        AND b.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 整个年度的违章人员
VIOLATION_PERSON_LIST_SQL3 = """SELECT DISTINCT
        a.ID_CARD,
        b.ALL_NAME AS DEPT_ALL_NAME
    FROM
        t_warning_key_person_library a
        LEFT JOIN t_department b ON a.FK_UNIT_ID = b.department_id 
    WHERE
        a.year = {}
        AND b.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务系统的确认的违章大王（单月）
VIOLATION_PERSON_CONFIRM_LIST_SQL = """SELECT DISTINCT
        a.ID_CARD,
        a.FK_DISPOSE_METHOD_IDS,
        c.ALL_NAME AS DEPT_ALL_NAME
    FROM t_warning_key_person_library_official a
        LEFT JOIN t_person b ON a.ID_CARD = b.ID_CARD
        LEFT JOIN t_department c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE a.year = {} AND a.month = {} AND a.status = 1
        AND c.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务系统的确认的违章大王（多个月）
VIOLATION_PERSON_CONFIRM_LIST_SQL2 = """SELECT DISTINCT
        a.ID_CARD,
        a.FK_DISPOSE_METHOD_IDS,
        c.ALL_NAME AS DEPT_ALL_NAME
    FROM t_warning_key_person_library_official a
        LEFT JOIN t_person b ON a.ID_CARD = b.ID_CARD
        LEFT JOIN t_department c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE a.year = {} AND a.month in {} AND a.status = 1
        AND c.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']

# 工务系统的确认的违章大王(年)
VIOLATION_PERSON_CONFIRM_LIST_SQL3 = """SELECT DISTINCT
        a.ID_CARD,
        a.FK_DISPOSE_METHOD_IDS,
        c.ALL_NAME AS DEPT_ALL_NAME
    FROM t_warning_key_person_library_official a
        LEFT JOIN t_person b ON a.ID_CARD = b.ID_CARD
        LEFT JOIN t_department c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE a.year = {} AND a.status = 1
        AND c.TYPE2 = '%s' AND b.IS_DELETE = 0
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']


# 检查人次清单
CHECK_INFO_PERSON_DATA_ST_SQL = """SELECT
        a.PK_ID,
        a.CHECK_WAY,
        b.ID_CARD, b.FK_DEPARTMENT_ID, b.ALL_NAME
    FROM
        t_check_info AS a
          LEFT JOIN
        t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
          LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.department_id    
    WHERE SUBMIT_TIME >= date('{0}') AND SUBMIT_TIME <= date('{1}')
        AND c.TYPE3 = '{2}'
"""


# 典型突出问题 -- 检查问题前X名
TOP_CHECK_PROBLEM_POINT_SQL = """SELECT 
        PROBLEM_POINT, COUNT(1) AS PRO_CT 
    FROM t_check_problem a, t_department b
    WHERE a.EXECUTE_DEPARTMENT_ID = b.DEPARTMENT_ID
        AND b.TYPE2 = '%s'
        AND a.SUBMIT_TIME BETWEEN date('{}') AND date('{}')
    GROUP BY PROBLEM_POINT
    ORDER BY PRO_CT DESC
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']


# 获取所有工务段清单
STATION_LIST_SQL = """SELECT
        DEPARTMENT_ID, ALL_NAME 
    FROM t_department
    WHERE TYPE = 4
        AND SHORT_NAME IS NOT NULL
        AND TRIM(SHORT_NAME) != ''
        AND TYPE2 = '%s'
""" % ALL_MAJOR_DEPARTMENT_ID_DICT['工务']
