# 安全生产信息
without_accident_sql = """SELECT
            a.*,
            d.NAME AS STATION
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
        WHERE a.OCCURRENCE_TIME between '{0}' and '{1}'
            AND c.TYPE2='1ACE7D1C80AF4456E0539106C00A2E70KSC'
        """

# 检查信息
check_info_sql = """SELECT a.*,f.TYPE3 AS ST
FROM `t_check_info` a 
LEFT JOIN t_check_info_and_address b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_check_info_and_person e on e.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID = e.FK_DEPARTMENT_ID
WHERE SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND c.TYPE2='1ACE7D1C80AF4456E0539106C00A2E70KSC'"""

# 问题信息
check_problem_sql = """SELECT p.PK_ID,i.CHECK_WAY,p.`LEVEL`,f.TYPE3,p.PROBLEM_POINT,
p.RISK_LEVEL,e.`NAME`,p.PROBLEM_SCORE,p.TYPE,b.STATUS,p.DESCRIPTION
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_check_info_and_person d on d.FK_CHECK_INFO_ID=i.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID = d.FK_DEPARTMENT_ID
where i.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and c.TYPE2='1ACE7D1C80AF4456E0539106C00A2E70KSC'
"""

# 问题详细信息


# 分析中心日报
analysis_center_daily = """SELECT a.PK_ID,a.CENTER_PROBLEM,a.MONITOR_PROBLEM_DESCRIPTION,b.FLAG,b.DAILY_COMPLETE,
b.DAILY_PROBLEM_NUMBER 
from t_analysis_center_daily a
LEFT JOIN t_analysis_center_daily_details b on b.FK_ANALYSIS_CENTER_DAILY_ID=a.PK_ID
where a.PROFESSION_ID='1ACE7D1C80AF4456E0539106C00A2E70KSC'
AND a.OPERAT_TIME BETWEEN '{0}' AND '{1}'
"""

# 追踪信息
main_info_sql = """select a.*,b.ALL_NAME FROM t_key_information_tracking a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_VEST_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
WHERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND b.TYPE2='1ACE7D1C80AF4456E0539106C00A2E70KSC'
"""

# 安全预警通知书
warning_notification_sql = """SELECT
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						a.CONTENT,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.BELONG_PROFESSION_NAME
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')
					and b.TYPE2 = '1ACE7D1C80AF4456E0539106C00A2E70KSC'"""


# 风险大类
RISK_MAIN_TYPE_SQL = """SELECT DISTINCT b.`NAME` from t_risk a
LEFT JOIN t_risk b on a.PARENT_ID=b.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE c.TYPE2='1ACE7D1C80AF4456E0539106C00A2E70KSC'"""

ANALYSIS_CENTER_WARNING_SQL = """
    SELECT
        RISK_WARNING
    FROM
        t_analysis_center_daily
    WHERE
        DATE = '{0}' 
        AND 
        PROFESSION_ID = '{1}'
"""
