import pandas as pd
from app.report.analysisWeeklyReport_sql import *
from app import mongo
from app.data.util import pd_query
from app.utils.common_func import ALL_MAJOR_DEPARTMENT_ID_DICT

majors = ['车务', '电务', '工务', '机务', '车辆', '供电']
STRING_PUNCATIONS = """！？。＂，＃＄％＆＇（）＊＋，－／：；＜＝＞＠［＼］＾＿｀｛｜｝～｟｠｢｣､、〃》「」『』【】〔〕〖〗〘〙〚〛〜〝〞〟〰〾〿–—‘’‛“”„‟…‧﹏."""

def _get_map_check_info_data(start_date, end_date):
    # prefixs = ['daily_', 'monthly_', 'history_']
    # keys = {
    #     'DATE': {
    #         "$gte": int(start_date.replace('-', '')),
    #         "$lte": int(end_date.replace('-', ''))
    #     },
    #     "MAJOR": {
    #         "$in": majors
    #     }
    # }
    # documents = []
    # for prefix in prefixs:
    #     coll_name = f'{prefix}detail_map_check_info'
    #     doc = list(mongo.db[coll_name].find(keys, {"_id": 0}))
    #     documents = documents + doc
    # data = pd.DataFrame(documents)
    data = pd_query(CHECK_INFO_SQL.format(start_date, end_date))
    data.drop_duplicates(subset=['FK_CHECK_INFO_ID', 'ID_CARD'], keep='first', inplace=True)
    return data


def _get_weekly_diff(now, last):
    if last == 0:
        return 0
    else:
        return round((now - last) / last * 100, 1)


def _second_base_count(info_data, last_info, problem_data, last_pro):
    # 检查次数，发现问题，严重性问题
    info_total = info_data.shape[0]
    info_diff = _get_weekly_diff(info_total, last_info.shape[0])
    _problem_data = problem_data.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'], keep='first')
    _last_pro = last_pro.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'], keep='first')
    problem_total = _problem_data.shape[0]
    problem_diff = _get_weekly_diff(problem_total, _last_pro.shape[0])
    serious_total = _problem_data[_problem_data['LEVEL'].isin(
        ['A', 'B', 'F1', 'F2', 'E1', 'E2'])].shape[0]
    serious_diff = _get_weekly_diff(serious_total,
                                    _last_pro[_last_pro['LEVEL'].isin(
                                        ['A', 'B', 'F1', 'F2', 'E1', 'E2'])].shape[0])
    # 所有作业项
    zuoye_total = _problem_data[_problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0]
    # 所有设备项
    shebei_total = _problem_data[_problem_data.LEVEL.isin(['E1', 'E2', 'E3', 'E4'])].shape[0]
    # 所有管理项
    guanli_total = _problem_data[_problem_data.LEVEL.isin(['F1', 'F2', 'F3', 'F4'])].shape[0]
    # 所有外部环境
    waibu_total = _problem_data[_problem_data.LEVEL.isin(['G1', 'G2', 'G3'])].shape[0]
    # 所有反恐防暴项
    fankong_total = _problem_data[_problem_data.LEVEL.isin(['K1', 'K2', 'K3', 'K4'])].shape[0]
    return {
        "info_total": info_total,
        "info_diff": info_diff,
        "problem_total": problem_total,
        "problem_diff": problem_diff,
        "serious_total": serious_total,
        "serious_diff": serious_diff,
        "zuoye": zuoye_total,
        "shebei": shebei_total,
        "guanli": guanli_total,
        "waibu": waibu_total,
        "fankong": fankong_total
    }


def _red_value(problem_data):
    _problem_data = problem_data.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'], keep='first')
    red_data = _problem_data[
        (_problem_data['IS_RED_LINE'].isin([1, 2])) &
        (_problem_data['IS_EXTERNAL'] == 0)
    ]
    if red_data.empty:
        red_total = 0
        red_value = []
    else:
        red_total = red_data.shape[0]
        # red_data = red_data.groupby('PROBLEM_POINT').size()
        data = red_data['FK_PROBLEM_BASE_ID'].value_counts()
        red_value = [{
            "name": _get_propblem_point(idx, red_data),
            "count": int(data[idx])
        } for idx in data.index]
    return {"count": red_total, "value": red_value}


def _get_propblem_point(id, data):
    tmp = data[data['FK_PROBLEM_BASE_ID'] == id]
    return tmp['PROBLEM_POINT'].values[0]


def _filter_data(content, colunm, row, reseve=False):
    # reseve 表示反选
    # 缺失值 直接返回
    content_list = row[colunm].split('-')
    flag = not reseve
    if not pd.notnull(row[colunm]):
        return not flag
    content_list = [item.strip() for item in content_list]
    if content_list[2] == content:
        return flag
    else:
        return not flag


def _filter_data_bystr(row, str_col, str_contents, reseve=False):
    """
    Args:
        row:
        str_col:
        str_contents:
        reseve: 表示反选

    Returns:

    """
    flag = not reseve
    if not pd.notnull(row[str_col]):
        return False
    for item in str_contents:
        if item in row[str_col]:
            return flag
    return not flag


def _risk_value(problem_data, risk_data):
    # 一个fk_check_problem_id 对应多个FK_RISK_ID
    # 先过滤三级目录中为'其他'的RISK_ID
    risk_data = risk_data[risk_data.apply(lambda row: _filter_data('其他', 'RISK_NAME', row, reseve=True), axis=1)]
    # 检查出的risk_id 与 静态配置的三级risk_id取交集
    fit_risk_ids = set(problem_data['FK_RISK_ID'].values.tolist()) & set(risk_data['RISK_ID'].values.tolist())
    pro_data = problem_data[problem_data['FK_RISK_ID'].isin(list(fit_risk_ids))]
    up_5_data = pro_data['FK_RISK_ID'].value_counts()[:5]
    risk_value = [{
        # 风险名去掉前面3个字符
        "name": risk_data[risk_data['RISK_ID'] == index]['RISK_NAME'].values[0][3:],
        "count": int(up_5_data[index])
    } for index in up_5_data.index]
    return risk_value


def _problem_point_value(problem_data):
    """
    问题项最多的前五个
    Args:
        problem_data:

    Returns:

    """
    pro_data = problem_data[
        problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])
    ]
    up_5_data = pro_data['FK_PROBLEM_BASE_ID'].value_counts()[:5]
    risk_value = []
    for index in up_5_data.index:
        name = _get_propblem_point(index, pro_data)
        # 判断末尾是否为标点符号
        if name[-1] in STRING_PUNCATIONS:
            name = name[:-1]
        # 句号结尾
        risk_value.append({
            'name': name + '。',
            "count": int(up_5_data[index])
        })
    # risk_value = [{
    #     "name": _get_propblem_point(index, pro_data),
    #     "count": int(up_5_data[index])
    # } for index in up_5_data.index]
    return risk_value


def get_second_safe_big_data(start_date, end_date, last_week_start,
                             last_week_end):
    global main_major_data
    # info_data = pd_query(check_info_sql.format(start_date, end_date))
    # 本周数据
    info_data = _get_map_check_info_data(start_date, end_date)
    main_major_data = pd_query(MAIN_MAJOR_INFO_SQL)
    info_data = pd.merge(
        info_data,
        main_major_data,
        on='FK_DEPARTMENT_ID',
        how='inner'
    )
    problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date))
    problem_data = pd.merge(
        problem_data,
        main_major_data,
        on='FK_DEPARTMENT_ID',
        how='inner'
    )
    risk_data = pd_query(RISK_INFO_SQL)
    if info_data.empty:
        return {}
    # 上周数据
    last_info = _get_map_check_info_data(last_week_start, last_week_end)
    last_info = pd.merge(
        last_info,
        main_major_data,
        on='FK_DEPARTMENT_ID',
        how='inner'
    )
    last_pro = pd_query(
        CHECK_PROBLEM_SQL.format(last_week_start, last_week_end))
    last_pro = pd.merge(
        last_pro,
        main_major_data,
        on='FK_DEPARTMENT_ID',
        how='inner'
    )
    # 严重问题
    # problem_data['SERIOUS_COUNT'] = problem_data['RISK_LEVEL'].apply(
    #     lambda x: 1 if x in [1, 2] else 0)
    # last_pro['SERIOUS_COUNT'] = last_pro['RISK_LEVEL'].apply(
    #     lambda x: 1 if x in [1, 2] else 0)
    # 基础查询统计信息
    base_value = _second_base_count(info_data, last_info, problem_data,
                                    last_pro)
    # 各专业数据分析
    major_value = []
    for major in majors:
        major_info = info_data[info_data['MAJOR_NAME'] == major]
        major_problem = problem_data[problem_data['MAJOR_NAME'] == major]
        last_major_info = last_info[last_info['MAJOR_NAME'] == major]
        last_major_pro = last_pro[last_pro.MAJOR_NAME == major]
        # 专业基本统计
        major_base = _second_base_count(major_info, last_major_info,
                                        major_problem, last_major_pro)
        # 红线问题
        red_value = _red_value(major_problem)
        # 风险类型最多的前五
        # risk_value = _risk_value(major_problem, risk_data)
        # 问题项点最多的前五个
        problem_point = _problem_point_value(major_problem)
        major_value.append({
            "name": major,
            "major_base": major_base,
            "red_value": red_value,
            # "risk_value": risk_value,
            "problem_point": problem_point
        })
    return {"base_value": base_value, "major_value": major_value}


def _get_fourth_first_util(pro_data, key):
    data = pro_data.copy()
    data['count'] = 1
    data = data.groupby(key).sum().sort_values(
        by='SERIOUS_VALUE', ascending=False).reset_index().head(8)
    result = [{
        "name": data.at[index, key[0]],
        "count": int(data.at[index, 'count'])
    } for index in data.index]
    return result


def _get_head_problem_by_order(pro_data, order_dict=dict(), limit_num=8, desc=None):
    """
    按照order_list筛选前limit_num个问题desc,默认降序
    Args:
        pro_data:
        order_list:[[key1,value1], [key2,value2]
        limit_num:

    Returns:
    list --> [问题desc]， len-->8
    """
    __attr__ = {
                'LEVEL': ['A', 'B', 'C', 'D'],
                'SERIOUS_VALUE': True,# 表示降序
                'IS_EXTERNAL': [0, 1],
                'ORDER': ['LEVEL', 'IS_EXTERNAL', 'SERIOUS_VALUE']
            }

    if set(order_dict.keys()) - set(list(__attr__.keys())):
        raise Exception("order_dict's keys ERROR,"
                        "not in('LEVEL','IS_EXTERNAL','SERIOUS_VALUE','ORDER')")
    if not desc:
        desc = 'DESCRIPTION'
    if not order_dict:
        order_dict = __attr__
    else:
        for key in set(__attr__.keys()) - set(order_dict.keys()):
            order_dict[key] = __attr__[key]
    order_list = order_dict['ORDER']
    record_data = [pro_data]
    for i in range(0, len(order_list)):
        key = order_list[i]
        if key != 'SERIOUS_VALUE':
            pro_data = pro_data[
                pro_data[key].isin(order_dict[key])
            ]
            record_data.append(pro_data)
            # 记录零界点（大于零届值的数据）
            if (i < len(order_list) - 1 and len(pro_data) > limit_num) or i == 0:
                continue
            elif i < len(order_list) - 1 and len(pro_data) <= limit_num and i > 0:
                break
            else:
                break
    pro_data = record_data[i]
    if order_dict['SERIOUS_VALUE']:
        pro_data.sort_values(by="SERIOUS_VALUE", ascending=False, inplace=True)
    else:
        pro_data.sort_values(by="SERIOUS_VALUE", ascending=True, inplace=True)
    rst = ''
    for i in list(pro_data[desc][:limit_num]):
        # 判断末尾是否为标点符号
        if i[-1] in STRING_PUNCATIONS:
            i = i[:-1]
        rst += i + '。'
    return rst


def _fourth_first_jiefa(pro_data):
    # pro_data = pro_data.set_index(
    #     ['SERIOUS_VALUE', 'PROBLEM_DIVIDE_NAMES'])['RISK_NAMES'].str.split(
    #         ',', expand=True).stack().reset_index().rename(columns={0: 'risk'})
    pro_data = pro_data[(pro_data.RISK_NAMES.str.contains('接发列车'))
                        & (~pro_data.RISK_NAMES.str.contains('劳动安全'))]
    result = _get_head_problem_by_order(pro_data)
    return result


def _fourth_first_diaoche(pro_data):
    pro_data = pro_data[
        ((pro_data['RESP_MAJOR_ID'] == ALL_MAJOR_DEPARTMENT_ID_DICT['车务']) & (pro_data['RISK_NAMES'].str.contains('调车')))
         |
        (pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['工务'], ALL_MAJOR_DEPARTMENT_ID_DICT['供电']]) & (pro_data['CHECK_ITEM_NAME'].str.contains('调车')))
        ]
    pro_data = pro_data[
            pro_data.apply(
        lambda row: _filter_data_bystr(row, 'RISK_NAMES', ['劳动安全', '工务-人身伤害风险']), axis=1)]
    result = _get_head_problem_by_order(pro_data)
    return result


def _fourth_first_laodong(pro_data):
    pro_data = pro_data[
        (
            (
                (
                    pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['车务'], ALL_MAJOR_DEPARTMENT_ID_DICT['车务']]) &
                    pro_data['CHECK_ITEM_NAME'].str.contains('作业纪律')
             )
        |
        (
            (
                pro_data['RESP_MAJOR_ID'].isin([
                    ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['供电'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['车辆'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['电务']]) &
                (
                    pro_data['PROBLEM_POINT'].str.contains('睡觉') |
                    pro_data['PROBLEM_POINT'].str.contains('手机') |
                    pro_data['PROBLEM_POINT'].str.contains('工作无关') |
                    pro_data['PROBLEM_POINT'].str.contains('闲聊')
                )
             )
        )
            )
    )
        ]

    pro_data = pro_data[
            pro_data.apply(
        lambda row: _filter_data_bystr(row, 'RISK_NAMES', ['劳动安全', '工务-人身伤害风险']), axis=1)]
    result = _get_head_problem_by_order(pro_data, order_dict={'ORDER': ['SERIOUS_VALUE']})
    return result


def _fourth_first_chewu(pro_data):
    pro_data = pro_data[(pro_data.RESP_MAJOR_ID.isin([ALL_MAJOR_DEPARTMENT_ID_DICT['机务']])
                           & pro_data.CHECK_ITEM_NAME.str.contains('一次乘务作业'))]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(row, 'RISK_NAMES', ['劳动安全']), axis=1)]
    result = _get_head_problem_by_order(pro_data)
    return result


def _fourth_first_keyun(pro_data):
    pro_data = pro_data[(~pro_data.PROBLEM_DIVIDE_NAMES.str.contains('劳安'))
                        & (pro_data.RESP_MAJOR_ID.isin([ALL_MAJOR_DEPARTMENT_ID_DICT['车务']])
                           & pro_data.CHECK_ITEM_NAME.str.contains('客运生产作业'))]
    result = _get_fourth_first_util(pro_data, ['CHECK_ITEM_NAME'])
    return result


def _fourth_first_zilun(pro_data):
    pro_data = pro_data[
            (pro_data.RESP_MAJOR_ID.isin([ALL_MAJOR_DEPARTMENT_ID_DICT['工务']]) &
             pro_data.CHECK_ITEM_NAME.str.contains('工务-自轮设备（工务）-运用')) |
            (pro_data.RESP_MAJOR_ID.isin([ALL_MAJOR_DEPARTMENT_ID_DICT['供电']]) &
            pro_data.CHECK_ITEM_NAME.str.contains('供电-自轮设备-运用'))
        ]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(row, 'RISK_NAMES', ['劳动安全', '工务-人身安全风险']), axis=1)]
    result = _get_head_problem_by_order(pro_data, order_dict={'ORDER': ['SERIOUS_VALUE']})
    return result


def _fourth_first_work_standard(all_data, pro_data):
    _pro_data = pro_data.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'], keep='first')
    _pro_data = _pro_data[
        _pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    total = len(set(all_data[all_data['LEVEL'].isin(['A', 'B', 'C', 'D'])]['FK_CHECK_PROBLEM_ID']))
    titles = [
        "接发列车作业标准执行不严。",
        "调车标准执行不力。",
        "劳动纪律松弛（含手机管理不到位、睡觉等）",
        "乘务标准执行不严。",
        # "客运标准不落实。",
        "自轮设备作业标准执行不到位。"
    ]
    values = [
        _fourth_first_jiefa(_pro_data.copy()),
        _fourth_first_diaoche(_pro_data.copy()),
        _fourth_first_laodong(_pro_data.copy()),
        _fourth_first_chewu(_pro_data.copy()),
        # _fourth_first_keyun(_pro_data.copy()),
        _fourth_first_zilun(_pro_data.copy()),
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"total": total, "value": result}


def _fourth_second_xianchang(pro_data):
    pro_data = pro_data[
        (
            (~pro_data.CHECK_ITEM_NAME.str.contains('防护')) &
            (~pro_data.CHECK_ITEM_NAME.str.contains('监管')) &
            (~pro_data.CHECK_ITEM_NAME.str.contains('料具'))
        ) &
        pro_data['IS_EXTERNAL'].isin([0]) &
        (
            (
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['工务']]) &
                (
                    pro_data['CHECK_ITEM_NAME'].str.contains('工务-点外修') |
                    pro_data['CHECK_ITEM_NAME'].str.contains('工务-施工') |
                    pro_data['CHECK_ITEM_NAME'].str.contains('工务-天窗修')
                )
            ) |
            (
                (~pro_data.RISK_NAMES.str.contains('劳动安全')) &
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['电务']]) &
                (
                        pro_data['CHECK_ITEM_NAME'].str.contains('电务-施工-主体施工') |
                        pro_data['CHECK_ITEM_NAME'].str.contains('电务-维修-集中修') |
                        pro_data['CHECK_ITEM_NAME'].str.contains('电务-维修-日常修') |
                        pro_data['CHECK_ITEM_NAME'].str.contains('电务-维修-状态修') |
                        pro_data['CHECK_ITEM_NAME'].str.contains('电务-维修-作业标准')
                )
            ) |
            (
                (~pro_data.RISK_NAMES.str.contains('劳动安全')) &
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['供电']]) &
                (
                        pro_data['CHECK_ITEM_NAME'].str.contains('供电-施工')
                )
            )
        )
    ]
    result = _get_head_problem_by_order(pro_data, order_dict={'IS_EXTERNAL': [0]})
    return result


def _fourth_second_jiju(pro_data):
    pro_data = pro_data[
        pro_data['IS_EXTERNAL'].isin([0]) &
        (
            pro_data.RESP_MAJOR_ID.isin([
                ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
            ]) &
            (
                pro_data.CHECK_ITEM_NAME.str.contains('料具') |
                pro_data.CHECK_ITEM_NAME.str.contains('料具-机具')
            )
        )]
    return _get_head_problem_by_order(pro_data,
                                      order_dict={
                                        'IS_EXTERNAL': [0],
                                        'ORDER': ['IS_EXTERNAL', 'SERIOUS_VALUE']}
                                      )


def _fourth_second_fanghu(pro_data):
    pro_data = pro_data[
        pro_data.RESP_MAJOR_ID.isin([
            ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
        ]) &
        pro_data.CHECK_ITEM_NAME.str.contains('防护')
        ]
    return _get_head_problem_by_order(pro_data, order_dict={
                                            'IS_EXTERNAL': [0],
                                            'ORDER': ['IS_EXTERNAL', 'SERIOUS_VALUE']}
                                  )


def _fourth_second_jianguan(pro_data):
    pro_data = pro_data[
        pro_data.RESP_MAJOR_ID.isin([
            ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
        ]) &
        pro_data.PROBLEM_POINT.str.contains('监管')
        ]
    return _get_head_problem_by_order(pro_data, order_dict={
                                            'IS_EXTERNAL': [0],
                                            'ORDER': ['IS_EXTERNAL', 'SERIOUS_VALUE']})


def _fourth_second_luwai(pro_data):
    pro_data = pro_data[
        pro_data['RESP_MAJOR_ID'].isin([
            ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
        ]) &
        (
            (
                pro_data.PROBLEM_POINT.str.contains('无计划') &
                pro_data.PROBLEM_POINT.str.contains('施工')
            ) |
            (
                pro_data.PROBLEM_POINT.str.contains('未签订') &
                pro_data.PROBLEM_POINT.str.contains('安全协议')
            )
        )
        ]
    result = _get_head_problem_by_order(pro_data,
                                        order_dict={
                                            'ORDER': ['SERIOUS_VALUE']})
    return result


def _fourth_second_shigong_safe(all_data, pro_data):
    # total = pro_data[
    #     (pro_data.CHECK_ITEM_NAME.str.contains('工务')
    #      & (pro_data.CHECK_ITEM_NAME.str.contains('工务-点外修')
    #         | pro_data.CHECK_ITEM_NAME.str.contains('工务-施工')
    #         | pro_data.CHECK_ITEM_NAME.str.contains('工务-天窗修')))
    #     | (pro_data.CHECK_ITEM_NAME.str.contains('电务')
    #        & (pro_data.CHECK_ITEM_NAME.str.contains('电务-施工')
    #           | pro_data.CHECK_ITEM_NAME.str.contains('电务-维修')))
    #     | (pro_data.CHECK_ITEM_NAME.str.contains('供电')
    #        & (pro_data.CHECK_ITEM_NAME.str.contains('供电-施工')))].shape[0]
    total = all_data[
        all_data['CHECK_ITEM_NAME'].str.contains('施工')
    ].shape[0]
    titles = ["现场施工管控不力。", "机具材料管理不善。", "安全防护不到位。", "施工监管不到位。", "路外单位违法施工。"]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    values = [
        _fourth_second_xianchang(pro_data.copy()),
        _fourth_second_jiju(pro_data.copy()),
        _fourth_second_fanghu(pro_data.copy()),
        _fourth_second_jianguan(pro_data.copy()),
        _fourth_second_luwai(pro_data.copy()),
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"total": total, "value": result}


def _fourth_third_wenti(safety_data):

    return '、'.join([row['OVERVIEW']+f"({row['REASON']})" for _, row in safety_data.iterrows()])


def _fourth_third_jianxiu(pro_data):
    pro_data = pro_data[
        pro_data['IS_EXTERNAL'].isin([0]) &
        (
            (
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['工务']]) &
                (
                    pro_data.CHECK_ITEM_NAME.str.contains('工务-点外修') |
                    pro_data.CHECK_ITEM_NAME.str.contains('工务-施工') |
                    pro_data.CHECK_ITEM_NAME.str.contains('工务-天窗修')
                )
            ) |
            (
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['电务']]) &
                (
                    pro_data.CHECK_ITEM_NAME.str.contains('电务-施工-主体施工') |
                    pro_data.CHECK_ITEM_NAME.str.contains('电务-维修-集中修') |
                    pro_data.CHECK_ITEM_NAME.str.contains('电务-维修-日常修') |
                    pro_data.CHECK_ITEM_NAME.str.contains('电务-维修-状态修') |
                    pro_data.CHECK_ITEM_NAME.str.contains('电务-维修-作业标准')
                ) &
                (~pro_data.PROBLEM_DIVIDE_NAMES.str.contains('劳动安全'))
            ) |
            (
                pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['供电']]) &
                (
                    pro_data.CHECK_ITEM_NAME.str.contains('供电-施工')
                ) &
                (~pro_data.PROBLEM_DIVIDE_NAMES.str.contains('劳动安全'))
            )
        ) &
        (
            (~pro_data.CHECK_ITEM_NAME.str.contains('防护')) |
            (~pro_data.CHECK_ITEM_NAME.str.contains('监管')) |
            (~pro_data.CHECK_ITEM_NAME.str.contains('料具'))
        )
        ]
    pro_data = pro_data[pro_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])]
    result = _get_head_problem_by_order(pro_data, order_dict={
        'ORDER': ['SERIOUS_VALUE']
    })
    return result


def _fourth_third_luojian(pro_data):
    pro_data = pro_data[
        (~pro_data.PROBLEM_DIVIDE_NAMES.str.contains('劳动安全')) &
        pro_data['RESP_MAJOR_ID'].isin([
            ALL_MAJOR_DEPARTMENT_ID_DICT['机务'],
            ALL_MAJOR_DEPARTMENT_ID_DICT['车辆']
        ]) &
        (
            pro_data.PROBLEM_POINT.str.contains('漏检') |
            pro_data.PROBLEM_POINT.str.contains('漏修')
        )
    ]
    return _get_head_problem_by_order(pro_data, order_dict={
        'ORDER': ['SERIOUS_VALUE']
    })


def _fourth_third_shebei_yinhuan(all_data, pro_data, safety_data):
    total = all_data[all_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])].shape[0]
    titles = ["设备问题突出。", "设备检修养护不力。", "漏检漏修问题突出。"]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    values = [
        _fourth_third_wenti(safety_data),
        _fourth_third_jianxiu(pro_data),
        _fourth_third_luojian(pro_data)
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"total": total, "value": result}


def _fourth_fifth_laodong_anquan(all_data, pro_data):
    pro_data = pro_data[
        (pro_data.RISK_NAMES.str.contains('劳动安全') |
        pro_data.RISK_NAMES.str.contains('人身安全风险')) &
        (
            (pro_data['RESP_MAJOR_ID'].isin([
                ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
                ])) &
            (~pro_data.CHECK_ITEM_NAME.str.contains('防护'))
        )
    ]
    total = all_data[
        (all_data.RISK_NAMES.str.contains('劳动安全') |
        all_data.RISK_NAMES.str.contains('人身安全风险')) &
        (
            (all_data['RESP_MAJOR_ID'].isin([
                ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
                ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
            ])) &
            (~all_data.CHECK_ITEM_NAME.str.contains('防护'))
        )
    ].shape[0]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    result = _get_head_problem_by_order(pro_data, order_dict={'IS_EXTERNAL': [0]})
    return {"total": total, "value": result}


def _fourth_sixth_yinshipin_yunyong(pro_data):
    titles = ["不按规定拍摄作业过程", "音视频设备维护不力"]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    values = [
        _fourth_sixth_yinshipin_paishe(pro_data),
        _fourth_sixth_yinshipin_weihu(pro_data),
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"value": result}



def _fourth_sixth_yinshipin_paishe(pro_data):
    pro_data = pro_data[
        (
                pro_data.RESP_MAJOR_ID.isin([
                    ALL_MAJOR_DEPARTMENT_ID_DICT['车务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['车辆']
                ]) &
                pro_data.CHECK_ITEM_NAME.str.contains('音视频')
        ) |
        (
                pro_data.RESP_MAJOR_ID.isin([
                    ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['供电']
                ]) &
                pro_data.PROBLEM_POINT.str.contains('音视频')
        )
        ]
    pro_data = pro_data[
        ~pro_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])
    ]
    result = _get_head_problem_by_order(pro_data, order_dict={'ORDER': ['SERIOUS_VALUE']})
    return result


def _fourth_sixth_yinshipin_weihu(pro_data):
    pro_data = pro_data[
        (
                pro_data.RESP_MAJOR_ID.isin([
                    ALL_MAJOR_DEPARTMENT_ID_DICT['车务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['工务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['车辆']]) &
                pro_data.CHECK_ITEM_NAME.str.contains('音视频')
        ) |
        (
                pro_data.RESP_MAJOR_ID.isin([
                    ALL_MAJOR_DEPARTMENT_ID_DICT['电务'],
                    ALL_MAJOR_DEPARTMENT_ID_DICT['供电']]) &
                pro_data.PROBLEM_POINT.str.contains('音视频')
        )
        ]
    pro_data = pro_data[
        pro_data['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])
    ]
    result = _get_head_problem_by_order(pro_data, order_dict={'ORDER': ['SERIOUS_VALUE']})
    return result


def _fourth_seventh_luwai_anquan(safety_info, pro_data):
    titles = ["安全生产信息-B1B2", "路外安全问题"]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    values = [
        '、'.join(list(
            safety_info[
            safety_info['CODE'].isin(['B1', 'B2'])
            ]['OVERVIEW'])
        ),
        _get_head_problem_by_order(
            pro_data[
                pro_data.RISK_NAMES.str.contains('路外安全')
            ],
            order_dict={'ORDER': ['SERIOUS_VALUE']}
        ),
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"value": result}


def _get_evaluate_util(evaluate_info: pd.DataFrame, codes: list) -> list:
    evaluate_data = evaluate_info[evaluate_info['CODE'].isin(codes)].copy()
    if evaluate_data.shape[0] <= len(codes):
        return [ evaluate_data.at[index, 'EVALUATE_CONTENT'] for index in evaluate_data.index]
    result = []
    standby = []
    for code in codes:
        data = evaluate_data[evaluate_data['CODE'] == code]
        if data.empty:
            continue
        data = data.sort_values(
            by='SCORE_STANDARD', ascending=False).head(2).reset_index()
        result.append(data.at[0, 'EVALUATE_CONTENT'])
        if len(data) > 1:
            standby.append(data.at[1, 'EVALUATE_CONTENT'])
    lenth = len(result)
    i = 0
    while lenth < len(codes):
        if len(standby) > i:
            result.append(standby[i])
        i += 1
        lenth += 1
    return result


def _fourth_eighth_carde_evaluate(start_date, end_date, all_analysis_center_dpids):
    evaluate_info = pd_query(EVALUATE_INFO_SQL.format(start_date, end_date))
    evaluate_info = evaluate_info.drop_duplicates('PK_ID', keep='first')
    total = evaluate_info.shape[0]
    chuji = evaluate_info[evaluate_info['GRADATION'] == "局管领导人员"].shape[0]
    zhengke = evaluate_info[evaluate_info['GRADATION'] == "正科职管理人员"].shape[0]
    fuke = evaluate_info[evaluate_info['GRADATION'] == "副科职管理人员"].shape[0]
    yiban = evaluate_info[evaluate_info['GRADATION'] == "一般管理和专业技术人员"].shape[0]
    evaluate_data = evaluate_info[evaluate_info['EVALUATE_DEPARTMENT_ID'].isin(all_analysis_center_dpids)]
    titles = [
        '检查质量低下', '重点工作落实不力', '安全检查不认真', '音视频运用管理不到位', '弄虚作假突出', '问题闭环管理不到位'
    ]
    values = [
        _get_evaluate_util(evaluate_data,
                           ["ZL-1", "ZL-2", "ZL-3", "ZL-6", "ZL-7", "LH-6"]),
        _get_evaluate_util(evaluate_data, [
            "ZD-1", "ZD-2", "ZD-3", "ZD-4", "ZD-5", "ZD-6", "ZD-7", "ZD-8",
            "ZD-9"
        ]),
        _get_evaluate_util(evaluate_data, ["ZL-4", "ZL-5"]),
        _get_evaluate_util(evaluate_data, ["YY-1", "YY-2", "YY-3", "YY-4"]),
        _get_evaluate_util(evaluate_data, ["ZJ-1", "ZJ-2", "ZJ-3"]),
        _get_evaluate_util(evaluate_data,
                           ["ZG-1", "ZG-2", "ZG-3", "ZG-4", "ZG-4"]),
    ]
    rst = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    result = {
        "total": total,
        "chuji": chuji,
        "zhengke": zhengke,
        "fuke": fuke,
        "yiban": yiban,
        "value": rst
    }
    return result


departments = [
    "车辆处-车辆分析室", "电务处-综合科-电务分析室", "工务处-工务分析室（灾害监测系统监测中心）", "供电处-供电分析室",
    "货运处-货运监控分析中心", "机务处-机务分析室", "客运处-客运分析室", "运输处-车务分析室"
]


def get_four_typical_problem_analysis(start_date, end_date):
    problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date))
    problem_data = pd.merge(
        problem_data,
        main_major_data,
        on='FK_DEPARTMENT_ID',
        how='inner'
    )
    safety_info = pd_query(
        SAFETY_PRODUCE_INFO_SQL.format(start_date, end_date)
    )
    if problem_data.empty:
        return {}
    analysis_center_data = pd_query(ANALYSIS_CENTER_DEPARTMENT_SQL)
    departments_data = pd_query(DEPARTMENT_SQL)
    all_analysis_center_dpids = get_all_sub_dpids(analysis_center_data['DEPARTMENT_ID'].values.tolist(), departments_data)
    # pro_data = problem_data[problem_data['FK_DEPARTMENT_ID'].isin(all_analysis_center_dpids)]
    pro_data = problem_data[
        problem_data['FK_DEPARTMENT_ID'].isin(all_analysis_center_dpids)
    ]
    result = {
        "first_work_standard":
        _fourth_first_work_standard(problem_data, pro_data.copy()),
        "second_shigong_anquan":
        _fourth_second_shigong_safe(problem_data, pro_data.copy()),
        "third_shebei_yinhuan":
        _fourth_third_shebei_yinhuan(problem_data, pro_data.copy(),
                                     safety_info[safety_info['MAIN_TYPE'] == 2]),
        "fourth_fourth_keyun_anquan": _fourth_ninth_keyun_pro(
            problem_data, pro_data.copy()),
        "fifth_laodong_anquan":
        _fourth_fifth_laodong_anquan(problem_data, pro_data.copy()),
        "sixth_yinshipin_yunyong":
        _fourth_sixth_yinshipin_yunyong(pro_data.copy()),
        "seventh_luwai_anquan":
        _fourth_seventh_luwai_anquan(safety_info, pro_data.copy()),
        "eighth_carde_evaluate":
        _fourth_eighth_carde_evaluate(start_date, end_date, all_analysis_center_dpids),
        "tenth_huozhuang_safety":
        _fourth_tenth_huozhuang_safety(problem_data, pro_data.copy()),
        "eleventh_xiaofang_anquan":
        _fourth_eleventh_xiaofang_safety(problem_data, pro_data.copy()),
    }
    return result


def get_all_sub_dpids(dpids, dpid_data):
    """[summary]
    求该部门的所有子部门
    返回dpid+所有子部门id
    Arguments:
        dpids {[summary]} -- [list]} -- [description]
    """
    ret = dpids.copy()
    parent_ids = dpids.copy()
    while len(parent_ids) > 0:
        df = dpid_data[dpid_data['FK_PARENT_ID'].isin(parent_ids)]
        if len(df) > 0:
            ids_list = df['DEPARTMENT_ID'].tolist()
            parent_ids = [str(val) for val in ids_list]
            ret.extend(parent_ids)
        else:
            break
    # 去除重复的PK_ID
    total_set = set(ret)
    return list(total_set)


def _fourth_ninth_keyun_pro(all_data, pro_data):
    total = all_data[all_data['RISK_NAMES'].str.contains('客运')].shape[0]
    titles = ["客运标准不落实", "安检查危标准执行不严"]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    values = [
        _fourth_fourth_keyun_standard(pro_data),
        _fourth_fourth_keyun_ajcw(pro_data),
    ]
    result = [{
        "title": titles[idx],
        "value": value
    } for idx, value in enumerate(values) if value]
    return {"total": total, "value": result}


def _fourth_fourth_keyun_standard(pro_data):
    pro_data = pro_data[
        pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['车务']]) &
        (
                pro_data.CHECK_ITEM_NAME.str.contains('客运生产作业')
        )
        ]
    return _get_head_problem_by_order(pro_data)


def _fourth_fourth_keyun_ajcw(pro_data):
    pro_data = pro_data[
        pro_data['RESP_MAJOR_ID'].isin([ALL_MAJOR_DEPARTMENT_ID_DICT['车务']]) &
        (
                pro_data.CHECK_ITEM_NAME.str.contains('安检查危')
        )
        ]
    return _get_head_problem_by_order(pro_data, order_dict={
        'ORDER': ['SERIOUS_VALUE']
    })


def _fourth_tenth_huozhuang_safety(all_data, pro_data):
    pro_data = pro_data[
        pro_data.RISK_NAMES.str.contains('货装安全风险')
    ]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['货装劳动安全', '货装消防'], reseve=True),
            axis=1
        )
    ]
    total = all_data[
        all_data.RISK_NAMES.str.contains('货装安全风险')
    ].shape[0]
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾'], reseve=True), axis=1)
    ]
    result = _get_head_problem_by_order(pro_data)
    return {"total": total, "value": result}


def _fourth_eleventh_xiaofang_safety(all_data, pro_data):
    pro_data = pro_data[
        pro_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾']), axis=1)
    ]
    total = all_data[
        all_data.apply(
            lambda row: _filter_data_bystr(
                row, 'RISK_NAMES', ['消防', '防火', '火灾']), axis=1)
    ].shape[0]
    result = _get_head_problem_by_order(pro_data, order_dict={
        'ORDER': ['SERIOUS_VALUE']
    }, limit_num=12)
    return {"total": total, "value": result}
