import openpyxl
from app.report.cheliang.major_daily_report_data import get_data

from datetime import datetime as dt, date

from dateutil.relativedelta import relativedelta
from app.report.analysis_report_manager import DailyAnalysisReport
from app import mongo


def cheliang_daily_report_execute(months):
    report_date = dt.strftime(months.today, '%Y-%m-%d')
    a = CheliangMajorDailyAnalysisReport()
    a.get_report_file(report_date)


class CheliangMajorDailyAnalysisReport(DailyAnalysisReport):

    def __init__(self):
        super(CheliangMajorDailyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='车辆')

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, quarter
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'DAILY'
             topic: 报告的主题，如 工务系统周分析报告 或 成都工务段周分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = dt.strptime(start_date, '%Y-%m-%d').date()
        end_date = dt.strptime(end_date, '%Y-%m-%d').date()

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if end_date >= date.today():
            end_date = date.today() + relativedelta(days=-1)

        max_count = 7
        report_date = end_date
        count = 0
        reports = []
        while (report_date > start_date) and (count < max_count):
            report_title = report_date.strftime('%Y-%m-%d')
            reports.append({
                "item_name": report_title,
                'report_date': report_date.strftime('%Y-%m-%d'),
            })
            report_date = report_date + relativedelta(days=-1)
            count = count + 1

        topic = '车辆系统安全生产日报表'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    # 适配原有页面的api
    @classmethod
    def get_list(cls, start_date, end_date):
        start_date = dt.strptime(start_date, '%Y-%m-%d').date()
        end_date = dt.strptime(end_date, '%Y-%m-%d').date()

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if end_date >= date.today():
            end_date = date.today() + relativedelta(days=-1)

        max_count = 7
        report_date = end_date
        count = 0
        reports = []
        while (report_date > start_date) and (count < max_count):
            report_title = report_date.strftime('%Y-%m-%d')
            reports.append({
                'TITLE': report_title,
                'report_date': report_date.strftime('%Y-%m-%d'),
                'TYPE': 'daily_analysis'
            })
            report_date = report_date + relativedelta(days=-1)
            count = count + 1

        return reports

    def generate_report_data(self, report_date):
        """
        生成报告所需要的数据
        :param report_date: 报告的日期
        :return: {dict} 报告的数据的字典
        """
        report_date = dt.strptime(report_date, '%Y-%m-%d')
        date_idx = report_date.year * 10000 + report_date.month * 100 + report_date.day
        data = get_data(report_date.year, report_date.month, report_date.day)
        mongo.db['safety_analysis_daily_report'].delete_one(
            {
                "date": date_idx,
                "hierarchy": self.hierarchy_type,
                "major": self.major
            })
        mongo.db['safety_analysis_daily_report'].insert_one(data)
        return data

    def fill_data(self, workbook, data):
        """
        使用指定的数据data，填充报告（excel）
        :param workbook: {openyxl.workbook.Workbook} excel文件
        :param data:
        :return:
        """
        worksheet = workbook.worksheets[0]
        # 标题
        worksheet['A1'] = f"中国铁路成都局集团有限公司运输安全生产日报表（{str(data['date'])[4:-2]}月{str(data['date'])[-2:]}日）(车辆部)"
        # 安全生产情况
        worksheet['B5'] = data['first']['first_one']

        # 车辆系统总体检查情况
        ## 检查基本情况
        worksheet['C6'] = data['first']['first_two']['base_check_situation']
        ## 非车辆系统检查发现问题
        worksheet['C7'] = data['first']['first_two']['no_cheliang_check_problem']
        ## 车辆部检查发现问题
        worksheet['C8'] = data['first']['first_two']['cheliangbu_check_problem']
        ## 站段查处典型突出问题
        worksheet['C9'] = data['first']['first_two']['zhanduan_check_seriouse_problem']
        ## 车辆分析室检查情况
        ### 1
        worksheet['C10'] = data['first']['first_two']['analysis_center_check_situation']['one']
        ### 2
        worksheet['C11'] = data['first']['first_two']['analysis_center_check_situation']['two']
        ### 3
        worksheet['C12'] = data['first']['first_two']['analysis_center_check_situation']['three']

        # 车辆系统监督检查基本情况
        dp_count = 0
        for key, value in data['first']['first_three'].items():
            dp_count +=2
            worksheet[f'B{11 + dp_count}'] = key
            worksheet[f'D{11 + dp_count}'] = value['base_check_situation']
            worksheet[f'D{12 + dp_count}'] = value['important_risk_check_situation']

        # 站段评价情况
        worksheet['C29'] = data['first']['first_four']['zhanduan_evaluate_content']

        # 安全预警
        worksheet['B31'] = data['second']['luju']
        worksheet['B32'] = data['second']['major']
        worksheet['B33'] = data['second']['zhuanduan']

        # 安全问题通知书
        worksheet['A35'] = data['third']['supervise_command']
        worksheet['A36'] = data['third']['supervise_notification']

    def generate_report_name(self, report_date):
        """
        生成报告的（唯一）名称，它可以用于区分报告。例如：
        1. 用作报告标题
        2. 用作文件名
        :param report_date: 报告的日期
        :return: {str} 报告的名称，例如: '集团安全日报'
        """
        return '中国铁路成都局集团有限公司运输安全生产日报表（{}）(车辆部)'.format(report_date)
