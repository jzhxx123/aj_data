#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import re
import threading
from app.data.util import pd_query
from app.report.cheliang.major_weekly_report_sql import *
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta

LOCAL_DATA = threading.local()


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_day = start_date[8:10]
    end_day = end_date[8:10]
    year = int(end_date[:4])
    start_month = int(start_date[5:7])
    end_month = int(end_date[5:7])
    if dt.strptime(end_date, '%Y-%m-%d') >= dt.strptime(end_date[:4]+'-12-25', '%Y-%m-%d'):
        LOCAL_DATA.yearly_start_date = end_date[:4]+'1225'
    else:
        LOCAL_DATA.yearly_start_date = str(int(end_date.split('-')[0]) - 1) + '-12-25'
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查问题
    problem_data = pd_query(check_problem_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查风险情况
    risk_data = pd_query(check_risk_sql.format(start_date, end_date))
    # 履职信息
    eva_data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 考核信息(以月为单位，当前只能获取上个月的考核信息)
    assess_data = pd_query(analysis_center_assess_sql.format(year, end_month - 1 if end_month - 1 > 0 else 12 ))
    # 预警通知书
    warning_data = pd_query(warning_notification_sql.format(LOCAL_DATA.yearly_start_date, end_date))
    # 系统通知
    # sys_data = pd_query(sys_notification_sql.format(start_date, end_date))
    first_data = get_first(info_data, last_info, problem_data, last_problem, risk_data)
    second_data = get_second_data(eva_data)
    three_data = get_three_data(assess_data, warning_data, problem_data)
    file_name = f'{start_date}至{end_date}车辆系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        'start_month': start_month,
        'start_day': start_day,
        'end_month': end_month,
        'end_day': end_day,
        'year': year,
        "major": '车辆',
        "hierarchy": "MAJOR",
        "created_at": dt.now(),
        'file_name': file_name,
        "first_data": first_data,
        "second_data": second_data,
        "three_data": three_data
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calc_ratio_or_diff(now_data, last_data):
    if last_data == 0:
        return {
            'diff': now_data,
            'percent': round(now_data * 100, 2)
        }
    else:
        return {
            'diff': now_data - last_data,
            'percent': round(ded_zero(now_data-last_data, last_data), 2)
        }


def get_first(info_data, last_info, problem_data, last_problem, risk_datas):
    # 检查次数
    check_count = len(info_data)
    ratio1 = calc_ratio_or_diff(check_count, len(last_info))
    # 问题个数
    pro_count = len(problem_data)
    ratio2 = calc_ratio_or_diff(pro_count, len(last_problem))
    # 严重问题
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    ratio3 = calc_ratio_or_diff(main_pro, len(last_problem[last_problem['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]))
    # 作业、管理、设备问题
    pros = []
    level_dict = {
        '作业': ['A', 'B', 'C', 'D'],
        '管理': ['F1', 'F2', 'F3', 'F4'],
        '设备': ['E1', 'E2', 'E3', 'E4']
    }
    for i in ['作业', '管理', '设备']:
        pros.append(len(problem_data[problem_data['LEVEL'].isin(level_dict[i])]))
    # 红线问题
    red_pro = len(problem_data[problem_data['IS_RED_LINE'].isin([1, 2])])
    # 5T问题
    problem_data = problem_data.dropna(subset=['PROBLEM_POINT'])
    five_t = len(problem_data[problem_data['PROBLEM_POINT'].str.contains('5T')])
    # 风险问题
    risk_count = []
    risk_datas = risk_datas.dropna(subset=['RISK_NAME'])
    for i in ['劳动安全', '车辆-行车设备质量-制动供风', '车辆-行车设备质量-供电', '车辆-行车设备质量-动客车上部服务设施质量']:
        new_data = risk_datas[risk_datas['RISK_NAME'].str.contains(i)]
        risk_count.append(len(new_data))
    return {
        'check_count': check_count,
        'ratio1': ratio1,
        'pro_count': pro_count,
        'ratio2': ratio2,
        'main_pro': main_pro,
        'ratio3': ratio3,
        'pros': pros,
        'red_pro': red_pro,
        'risk_count': risk_count,
        'five_t': five_t
    }


def get_second_data(eva_data):
    """
    干部履职情况分析
    :param eva_data:
    :return:
    """
    # 扣分人数
    score_count = len(eva_data)
    # 重点工作落实不到位
    zdgz = len(eva_data[eva_data['CODE'].isin(['ZD-4', 'ZD-5', 'ZD-6', 'ZD-7', 'ZD-8'])])
    ratio1 = round(ded_zero(zdgz, score_count) * 100, 2)
    # 监督检查质量问题
    jdjc = len(eva_data[eva_data['CODE'].isin(['ZL-1', 'ZL-2', 'ZL-3', 'ZL-4', 'ZL-5', 'ZL-6', 'ZL-7'])])
    ratio2 = round(ded_zero(jdjc, score_count) * 100, 2)
    # 检查信息录入质量问题
    jcxx = len(eva_data[eva_data['CODE'].isin(['JL-2'])])
    ratio3 = round(ded_zero(jcxx, score_count) * 100, 2)
    # 安全谈心
    aqtx = len(eva_data[eva_data['CODE'].isin(['TX-1', 'TX-4'])])
    ratio4 = round(ded_zero(aqtx, score_count) * 100, 2)
    # 问题闭环管理问题
    wtbh = len(eva_data[eva_data['CODE'].isin(['ZG-5'])])
    ratio5 = round(ded_zero(wtbh, score_count) * 100, 2)
    # 表格
    all_count = []
    for i in [1, 2]:
        count = []
        new_data = eva_data[eva_data['CHECK_TYPE'] == i]
        for j in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
            new_datas = new_data[new_data['GRADATION'] == j]
            count.append(int(new_datas['SCORE'].sum()))
        count.append(sum(count))
        all_count.append(count)
    return {
        'score_count': score_count,
        'zdgz': zdgz,
        'jdjc': jdjc,
        'jcxx': jcxx,
        'aqtx': aqtx,
        'wtbh': wtbh,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'all_count': all_count
    }


def _get_level_detail_info(pro_data, kinds):
    kinds_dict = dict()
    kinds_list = []
    for k in kinds:
        kinds_dict['name'] = k
        kinds_dict['count'] = pro_data[pro_data['LEVEL'] == k].shape[0]
        kinds_dict['value'] = [
            '发现人:' + row['PERSON_NAME'] + '\n' + '问题描述:' + row['DESCRIPTION']
            for _, row in pro_data[pro_data['LEVEL'] == k].iterrows() if not row.empty
        ]
        kinds_list.append(kinds_dict)
    return kinds_list


def get_three_data(assess_data, warning_data, problem_data):
    """
    安全生产管控
    :param assess_data:
    :param warning_data:
    :return:
    """
    warning_data = warning_data[
        warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7])
    ]
    all_list = []
    pro_list = []
    problem_data = problem_data.dropna(subset=['PK_ID'])
    # 各段得分情况
    for i in ['成都动车段', '成都车辆段', '重庆车辆段', '贵阳车辆段', '成都北车辆段', '重庆西车辆段', '贵阳南车辆段']:
        data = assess_data[assess_data['NAME'] == i]
        # TYPE 3,站段对站段检查, 筛选A, F1, E1类
        pro_data = problem_data[
            (problem_data['NAME'] == i) &
            (problem_data['TYPE'] == 3) &
            (problem_data['LEVEL'].isin(['A', 'F1', 'E1']))
            ]
        # 加分
        sum_score = data[data['GRADES_TYPE'] == 1]['ACTUAL_SCORE'].sum()
        # 扣分
        ded_score = data[data['GRADES_TYPE'] == 0]['ACTUAL_SCORE'].sum()
        # 总分
        all_score = sum_score - ded_score + 80
        all_list.append([i, all_score])
        kinds = set(pro_data['LEVEL'])
        kinds_list = _get_level_detail_info(pro_data, kinds)
        pro_list.append(
            {
                'name': i,
                'count': len(pro_data),
                'value': kinds_list,
                'kinds': 'A/F1/E1'
            }
        )
    # 添加车辆部的发现问题信息
    pro_list.append(
        {
            'name': '车辆部',
            'count': len(problem_data[(problem_data['NAME'] == '车辆部') &
                                  (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))]),
            'value': _get_level_detail_info(problem_data[(problem_data['NAME'] == '车辆部') &
                                  (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))],
                                            set(problem_data[(problem_data['NAME'] == '车辆部') &
                                  (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))]['LEVEL'])),
            'kinds': 'A/B/F1/F2'
        }
    )
    # 添加集团公司的发现问题信息，检查类型为路局对站段检查,type =1
    pro_list.append(
        {
            'name': '集团公司',
            'count': len(problem_data[
                         (problem_data['TYPE'] == 1) &
                         (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))]),
            'value': _get_level_detail_info(problem_data[
                         (problem_data['TYPE'] == 1) &
                         (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))],
                                             set(problem_data[
                         (problem_data['TYPE'] == 1) &
                         (problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2']))]['LEVEL'])),
            'kinds': 'A/B/F1/F2'
        }
    )
    # 预警通知书信息
    warning_list1 = []
    warning_data = warning_data.drop_duplicates(subset=['CONTENT'])
    # 站段路局分类
    for i, j in warning_data[warning_data['HIERARCHY'] == 1].iterrows():
        dic = {
            'name': j['STATION'],
            'time': j['CREATE_TIME'],
            'content': j['CONTENT']
        }
        warning_list1.append(dic)
    # 站段下发
    warning_list2 = []
    for i, j in warning_data[warning_data['HIERARCHY'] == 3].iterrows():
        dic = {
            'name': j['STATION'],
            'time': j['CREATE_TIME'],
            'content': j['CONTENT']
        }
        warning_list2.append(dic)
    return {
        'all_list': all_list,
        'warning_list1': warning_list1,
        'warning_list2': warning_list2,
        'pro_list': pro_list
    }
