# 安全生产信息
CHECK_SAFETY_INFO_SQL = """SELECT
        a.*,
        c.DEPARTMENT_ID,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED,
        g.TYPE,
        c.`NAME` as STATION
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID  
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1} 23:59:59')
            and c.TYPE3='{2}'"""

# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY,b.ID_CARD,
c.CHECK_ITEM_NAMES
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'
"""


# 问题信息
check_problem_sql = """SELECT p.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,p.PROBLEM_CLASSITY_NAME,p.`LEVEL`,
d.CHECK_SCORE,i.IS_YECHA,p.RISK_LEVEL,e.`NAME`,p.CHECK_ITEM_NAME,p.PROBLEM_SCORE,i.IS_GENBAN,p.TYPE,b.STATUS,p.PROBLEM_POINT,
i.CHECK_TYPE,c.ALL_NAME,b.RESPONSIBILITY_PERSON_NAMES,p.DESCRIPTION,p.IS_RED_LINE
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_problem_base d on p.FK_PROBLEM_BASE_ID = d.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE4
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}'"""


# 检查问题风险信息
check_risk_sql = """SELECT c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,f.GRADATION,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.IDENTITY,i.`STATUS`,p.PROBLEM_POINT,h.RISK_NAME
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_person_level e on e.LEVEL = d.LEVEL
LEFT JOIN t_person_gradation_ratio f on f.pk_id=e.FK_PERSON_GRADATION_RATIO_ID
LEFT JOIN t_check_problem_and_risk h on h.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_check_problem_and_responsible_department i on i.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on i.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'"""


# 履职查询
CHECK_EVALUATE_SQL = """SELECT
	a.*,
	b.SITUATION,
	e.NAME 
FROM
	t_check_evaluate_info AS a
	LEFT JOIN t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
	LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department d ON d.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department e ON e.DEPARTMENT_ID = d.TYPE3 
WHERE
	a.CREATE_TIME >= '{0}' 
	AND a.CREATE_TIME <= '{1} 23:59:59' 
	AND d.TYPE3 = '{2}'"""


# 车间信息
SHOP_SQL = """SELECT DISTINCT d.NAME,d.DEPARTMENT_ID
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0 AND d.`NAME` like '%%车间'
"""

# 检查通知书
check_notification_sql = """SELECT a.* from t_check_notification a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.ISSUE_TIME BETWEEN '{0}' and '{1} 23:59:59'
AND b.TYPE3='{2}'"""


# 安全表扬通知书
praise_notice_sql = """
SELECT a.* from t_safety_praise_notice a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.PRAISE_DEPARTMENT_ID
WHERE a.ISSUE_TIME BETWEEN '{0}' and '{1} 23:59:59'
AND b.TYPE3='{2}'"""


# 站段信息
ZHANDUAN_PERSON_SQL = """SELECT count(b.`NAME`) as COUNT,b.`NAME`
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0 AND b.`NAME` like '%%车间'
GROUP BY b.`NAME`
"""