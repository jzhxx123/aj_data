# 检查信息
check_info_sql = """SELECT 
c.PK_ID,
c.CHECK_WAY,
c.PROBLEM_NUMBER,
b.ID_CARD,
d.NAME,
c.RISK_NAME,
e.IDENTITY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 问题信息
check_problem_sql = """SELECT 
p.PK_ID,c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,p.PROBLEM_POINT,p.IS_RED_LINE, p.TYPE,
tp.PERSON_NAME, p.DESCRIPTION
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_info_and_person d on c.PK_ID=d.fk_check_info_id
LEFT JOIN t_person as tp on d.ID_CARD = tp.ID_CARD
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 检查问题风险信息
check_risk_sql = """SELECT c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,f.GRADATION,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.IDENTITY,i.`STATUS`,p.PROBLEM_POINT,h.RISK_NAME
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_person_level e on e.LEVEL = d.LEVEL
LEFT JOIN t_person_gradation_ratio f on f.pk_id=e.FK_PERSON_GRADATION_RATIO_ID
LEFT JOIN t_check_problem_and_risk h on h.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_check_problem_and_responsible_department i on i.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on i.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 履职查询
CHECK_EVALUATE_SQL = """SELECT
	a.*,
	b.SITUATION,
	e.NAME 
FROM
	t_check_evaluate_info AS a
	LEFT JOIN t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
	LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department d ON d.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
	LEFT JOIN t_department e ON e.DEPARTMENT_ID = d.TYPE3 
WHERE
	a.CREATE_TIME >= '{0}' 
	AND a.CREATE_TIME <= '{1} 23:59:59' 
	AND d.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""

# 分析中心考核分数
analysis_center_assess_sql = """SELECT b.GRADES_TYPE,b.ACTUAL_SCORE,c.`NAME` 
from t_analysis_center_assess a
LEFT JOIN t_analysis_center_assess_detail b on a.PK_ID=b.FK_ANALYSIS_CENTER_ASSESS_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
WHERE a.`YEAR` = {0} and a.`MONTH` = {1}
AND c.TYPE2='1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 预警通知书信息
warning_notification_sql = """SELECT a.*,c.`NAME` as STATION FROM t_warning_notification a
LEFT JOIN t_department b on b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
where a.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
AND b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 系统通知
sys_notification_sql = """select a.*,c.`NAME` as STATION from t_system_notify a
LEFT JOIN t_system_notify_and_department b on b.fk_system_notify_id=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
where a.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
AND c.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""