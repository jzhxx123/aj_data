import app.report.analysis_report_manager as manager
import datetime
from app.data.util import pd_query
from app.report.cheliang.station_semiannual_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find
import pandas as pd


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


levels = [7, 6, 5, 4, 3, 2, 1]
majors = ['车辆']
all_items = [
    '量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
    '事故故障追责', '安全谈心'
]
level_name = {
    1: '∑＜2',
    2: '2≤∑＜4',
    3: '4≤∑＜6',
    4: '6≤∑＜8',
    5: '8≤∑＜10',
    6: '10≤∑＜12',
    7: '∑≥12'
}


def get_data(year, station_id):
    date = manager.AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 11
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    # 月履职
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date, station_id))
    # 年履职
    evaluate_year_info = pd_query(
        check_evaluate_sql.format(f'{end_date[:4]}-01-01', end_date, station_id))
    evaluate_review = pd_query(check_evaluate_review_sql.format(start_date, end_date, station_id))
    # 检查信息
    info = pd_query(check_info_sql.format(start_date, end_date, station_id))
    last_info = pd_query(check_info_sql.format(last_month_start, last_month_end, station_id))
    # 检查问题
    check_info = pd_query(check_problem_info_sql.format(start_date, end_date, station_id))
    last_check_info = pd_query(check_problem_info_sql.format(last_month_start, last_month_end, station_id))
    # 检查职工信息
    worker_info = pd_query(worker_check_info_sql.format(start_date, end_date, station_id))
    first = get_first(info, last_info, check_info, last_check_info, worker_info, start_date, end_date, station_id)
    second_one = get_second_data_anquan(info, check_info, last_check_info)
    second_two = get_second_data_lvzhi(evaluate_info, evaluate_year_info, evaluate_review)
    third = get_third(info, check_info)
    table_two_seven = t_two_seven(start_date, end_date, station_id)
    file_name = f'{start_date}至{end_date}{station_name}安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        'last_year': last_month_start[:4],
        "major": '车辆',
        "station_id": station_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second_one": second_one,
        "second_two": second_two,
        'third': third,
        "biao7": table_two_seven,
    }
    return result


# 第一部分
def get_first(info, last_info, check_info, last_check_info, worker_info, start_date, end_date, station_id):
    table1 = get_table1(info, last_info, check_info, last_check_info)
    table2 = get_table2(info, check_info)
    table3 = get_table3(worker_info)
    table4 = get_table4(info)
    data = pd.DataFrame(calc_safety_produce_data(start_date, end_date, station_id)).dropna(
        subset=['RESPONSIBILITY_IDENTIFIED_NAME'])
    if data.empty is True:
        duty = 0
        accs = 0
    else:
        data = data.dropna(subset=['RESPONSIBILITY_IDENTIFIED_NAME'])
        # 责任事故
        duty_data = data[(data['MAIN_CLASS'] == '事故') & (data['RESPONSIBILITY_IDENTIFIED_NAME'] != '非责任')]
        duty = len(duty_data)
        # 行车设备故障
        acc = data[data['NAME'].str.contains('行车设备故障')]
        accs = len(acc)
    return {
        'table1': table1,
        'table2': table2,
        'table3': table3,
        'table4': table4,
        'duty': duty,
        'acc': accs
    }


def calc_safety_produce_data(start_date, end_date, station_id):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param start_date:
    :param end_date:
    :return:
    """
    now_time = int(''.join(start_date.split('-')))
    end_time = int(''.join(end_date.split('-')))
    keys = {
        "match": {
            "DATE": {
                '$lte': end_time,
                '$gte': now_time
            },
            'TYPE3': station_id
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
            'RESPONSIBILITY_IDENTIFIED_NAME': 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    return documents


def get_table1(info, last_info, check_info, last_check_info):
    problem_list = []
    for i in [check_info, last_check_info]:
        i = i.dropna(subset=['PROBLEM_CLASSITY_NAME', 'LEVEL'])
        count = [len(i)]
        for j in ['作业', '设备', '管理', '反恐', '路外']:
            data = i[i['PROBLEM_CLASSITY_NAME'].str.contains(j)]
            count.append(len(data))
        high = len(i[i['LEVEL'].isin(['A', 'B', 'F1'])])
        count.append(high)
        count.append(round(ded_zero(len(i[i['IS_ASSESS'] == 1]), len(i)) * 100, 1))
        problem_list.append(count)
    index = 0
    for i in [info, last_info]:
        i = i.dropna(subset=['IS_YECHA'])
        yecha = len(i[i['IS_YECHA'] == 1])
        yecha_ratio = round(ded_zero(yecha, len(i)) * 100, 1)
        problem_list[index].append(yecha)
        problem_list[index].append(yecha_ratio)
        index += 1
    ratio = []
    for i in range(len(problem_list[0])):
        ratio.append(get_ring_ratio(problem_list[0][i], problem_list[1][i]))
    return {
        'problem_list': problem_list,
        'ratio': ratio
    }


def get_table2(info, check_info):
    all_list = []
    for i in info['STATION'].value_counts().index:
        data = info[info['STATION'] == i]
        problem = check_info[check_info['STATION'] == i]
        find_ratio = round(ded_zero(int(data['PROBLEM_NUMBER'].sum()), len(data)) * 100, 1)
        avg_check = round(ded_zero(len(data), len(data['CHECK_PERSON_NAMES'].value_counts())), 1)
        yecha = round(ded_zero(len(data[data['IS_YECHA'] == 1]), len(data)) * 100, 1)
        problem_num = len(problem[problem['LEVEL'].isin(['A', 'B', 'F1', 'C', 'E3'])])
        problem_ratio = round(ded_zero(problem_num, int(data['PROBLEM_NUMBER'].sum())) * 100, 1)
        all_list.append([i, find_ratio, avg_check, yecha, problem_ratio])
    return {
        'all_list': all_list
    }


def get_table3(worker_info):
    all_list = []
    for i in ['安全科', '技术科', '调度科', '检修车间', '设备维修车间', '运用车间', '乘务车间']:
        data = worker_info[worker_info['NAME'].str.contains(i)]
        lis = [i]
        for j in ['质检员', '5T维修工',	'高级修机械师',	'地勤机械师', '随车机械师',	'探伤工', '调度员',	'调车作业']:
            new_data = data[data['POSITION'].str.contains(j)]
            lis.append([int(new_data['COST_TIME'].sum()), int(new_data['PROBLEM_NUMBER'].sum())])
        all_list.append(lis)
    return{
        'all_list': all_list
    }


def get_table4(info):
    all_list = []
    for i in ['安全科', '技术科', '乘务车间', '运用车间', '检修车间']:
        info = info[info['ALL_NAME'].str.contains(i)]
        lis = [i]
        for j in ['配件脱落', '火灾爆炸', '高压牵引', '制动供风', '劳动安全', '防溜']:
            info = info.dropna(subset=['RISK_NAME'])
            data1 = info[info['RISK_NAME'].str.contains(j)]
            check_count = len(data1)
            problem_count = int(data1['PROBLEM_NUMBER'].sum())
            person = len(info['CHECK_PERSON_NAMES'].value_counts())
            if person == 0:
                person = 1
            lis.append([check_count, problem_count, int(round(ded_zero(check_count, person), 1)),
                        int(round(ded_zero(problem_count, person), 1))])
        all_list.append(lis)
    return {
        'all_list': all_list
    }


# 第三部分统计内容
def get_third(info, check_info):
    check_info = check_info[check_info['PROBLEM_NUMBER'] != 0]
    # 发现问题数
    find_problem = len(check_info)
    # 严重性质问题
    serious = len(check_info[check_info['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    sta_list = []
    for i in check_info['STATION'].value_counts().index:
        data = check_info[check_info['STATION'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        sta_list.append(dic)
    max_sta = sorted(sta_list, key=lambda x: x['count'])[0]
    # 典型问题
    data = check_info[check_info['LEVEL'].isin(['A', 'F1'])]
    pro_list = []
    for i, k in data.iterrows():
        pro_list.append(k['STATION'] + k['CHECK_PERSON_NAMES'] + k['DESCRIPTION'])
    check_info = check_info.dropna(subset=['NAME'])
    info = info.dropna(subset=['RISK_NAME'])
    dongkeche_fanghuo = dongke_fanghuo(info, check_info)
    dongkeche_sbtuoluo = shebei_tuoluo(info, check_info)
    dongche_gaoyaqy = dongche_gaoya(info, check_info)
    keche_zdgf = keche_zhidong(info, check_info)
    huoche_fivet = five_t(info, check_info)
    laodong_anquan = laoan(info, check_info)
    return {
        'find_problem': find_problem,
        'serious': serious,
        'max_sta': max_sta,
        'pro_list': pro_list,
        'dongkeche_fanghuo': dongkeche_fanghuo,
        'dongkeche_peijiantuo': dongkeche_sbtuoluo,
        'dongche_gaoyaqy': dongche_gaoyaqy,
        'keche_zdgf': keche_zdgf,
        'huoche_fivet': huoche_fivet,
        'laoan': laodong_anquan,
    }


# 第二部分的第一节安全内容
def get_second_data_anquan(info, check_info, last_check_info):
    # 检查信息
    check_info = check_info.dropna(subset=['PROBLEM_CLASSITY_NAME', 'LEVEL'])
    check_info = check_info[check_info['PROBLEM_NUMBER'] != 0]
    last_check_info = last_check_info[last_check_info['PROBLEM_NUMBER'] != 0]
    # 检查人数
    person = len(info['CHECK_PERSON_NAMES'].value_counts())
    # 检查次数
    check_count = len(info)
    # 问题个数
    problem_count = len(check_info)
    # 作业项问题数
    work_problem = len(check_info[check_info['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    # 严重性质问题
    serious = len(check_info[check_info['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    last_serious = len(last_check_info[last_check_info['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    ratio = get_ring_ratio(serious, last_serious)
    # 添乘、设备监控调阅、职工、转录检查
    tc = int(info[info['CHECK_WAY'] == 2]['PROBLEM_NUMBER'].sum())
    mv = int(info[info['CHECK_WAY'].isin([3, 4])]['PROBLEM_NUMBER'].sum())
    zg = int(info[info['CHECK_WAY'] == 6]['PROBLEM_NUMBER'].sum())
    zl = int(info[info['CHECK_WAY'] == 5]['PROBLEM_NUMBER'].sum())
    return {
        'person': person,
        'check_count': check_count,
        'problem_count': problem_count,
        'work_problem': work_problem,
        'serious': serious,
        'tc': tc,
        'mv': mv,
        'zg': zg,
        'zl': zl,
        'ratio': ratio
    }


# 第二部分的第二节履职内容
def get_second_data_lvzhi(info, year_info, revie):

    total = info[info.ITEM_NAME.isin(all_items)].shape[0]
    counts = []
    percents = []
    for item in all_items:
        if total == 0:
            count, percent = 0, 0
        else:
            item_info = info[info['ITEM_NAME'] == item]
            count = len(item_info)
            percent = round(ded_zero(count, total) * 100, 1)
        counts.append(count)
        percents.append(percent)
    review = revie.drop_duplicates(['PK_ID'], keep='last')
    # 定期评价人次
    regular_count = review[review['EVALUATE_WAY'] == 2].shape[0]
    # 逐条评价人次
    one_count = int(review[review['EVALUATE_WAY'] == 1].shape[0])
    # 履职问题个数
    evaluate_count = info.shape[0]
    # 履职问题人数
    evaluate_person = len(set(info['RESPONSIBE_ID_CARD']))
    # 最高评价计分
    score_data = info.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()

    pingjia_data = info.drop_duplicates(['RESPONSIBE_ID_CARD'], keep='last')
    pingjia = {
        'renshu': pingjia_data.shape[0],
        'keji': pingjia_data[pingjia_data['GRADATION'].isin(['正科职管理人员', '副科职管理人员'])].shape[0],
        'yiban': pingjia_data[pingjia_data['GRADATION'] == '一般管理和专业技术人员'].shape[0],
        'feigb': pingjia_data[pingjia_data['GRADATION'] == '非管理和专业技术人员'].shape[0],
        'chuji': pingjia_data[pingjia_data['GRADATION'] == '局管领导人员'].shape[0],
    }

    id_card = score_data.at[0, 'RESPONSIBE_ID_CARD']
    koufeng_most = {
        'fengshu': round(score_data.at[0, 'SCORE'], 1),
        'person_name': (info['RESPONSIBE_PERSON_NAME'][info.RESPONSIBE_ID_CARD == id_card]).iloc[0],
        'all_name': (info['ALL_NAME'][info.RESPONSIBE_ID_CARD == id_card]).iloc[0],
        'job': (info['JOB'][info.RESPONSIBE_ID_CARD == id_card]).iloc[0]
    }

    # 今年得分数据
    score_year_data = year_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()

    z_level_rst = zhanduan_fun(info, year_info)

    year_total_count = score_year_data.shape[0]
    month_percent = round(ded_zero(evaluate_count, year_total_count) * 100, 1)
    score_data['level'] = score_data['SCORE'].apply(singe_score_section)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        singe_score_section)

    p_level_rst = []
    for level in levels:
        p_level_rst.append({
            "level":
                level_name.get(level),
            "total_count":
                score_year_data[score_year_data['level'] == level].shape[0],
            "month_count":
                score_data[score_data['level'] == level].shape[0]
        })
    score_rst = {
        "year_total_count": year_total_count,
        "month_count": evaluate_count,
        "month_percent": month_percent,
        "p_level_value": p_level_rst,
        "z_level_value": z_level_rst,
    }

    result = {
        'names': all_items,
        'counts': counts,
        'percents': percents,
        'regular_count': regular_count,
        'one_count': one_count,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'koufeng_most': koufeng_most,
        'pingjia': pingjia,
        'score_level_value': score_rst
    }
    return result


def t_two_one(infos, pros):
    w_pro = []
    count = []
    sheb = []
    guanli = []
    fangk = []
    out = []
    yc_count = []
    yc_percent = []

    lists = [w_pro, count, sheb, guanli, fangk, out, yc_count, yc_percent]

    for pro in pros:
        prr = pro.drop_duplicates(['PK_ID'], keep='last')
        w_pro.append(prr[prr['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0])
        count.append(prr.shape[0])
        sheb.append(prr[prr['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])].shape[0])
        guanli.append(prr[prr['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])].shape[0])
        fangk.append(prr[prr['LEVEL'].isin(['K1', 'K2', 'K3', 'K4'])].shape[0])
        out.append(prr[prr['LEVEL'].isin(['G1', 'G2', 'G3', 'G4'])].shape[0])
    for info in infos:
        inff = info.drop_duplicates(['FK_CHECK_INFO_ID'], keep='last')
        yc_count.append(inff[inff['IS_YECHA'] == 1].shape[0])
        yc_percent.append(ded_zero(inff[inff['IS_YECHA'] == 1].shape[0], info.shape[0]) * 100)
    for li in lists:
        li.append(round(get_ring_ratio(li[0], li[1]), 2))

    return {
        'work': w_pro,
        'count': count,
        'sb': sheb,
        'guanli': guanli,
        'fangk': fangk,
        'out': out,
        'yc_c': yc_count,
        'yc_p': yc_percent
    }


# 表7和表8 违章大王和重点班组
def t_two_seven(start, end, station_id):
    table7, table8 = [], []
    dtf1 = pd_query(weizhang_king.format(start, end))
    dtf2 = dtf1[dtf1['TYPE3'] == station_id]
    dtf3 = dtf2.groupby('ID_CARD').sum().sort_values(
        by='ORIGINAL_VIOLATION_NUMBER', ascending=False).reset_index()
    dtf33 = dtf3[dtf3['ORIGINAL_VIOLATION_NUMBER'] >= 2]
    if not dtf33.empty:
        id_list1 = dtf33['ID_CARD'].tolist()
        for idd in id_list1:
            table7.append(
                {
                    'k_allname': str((dtf2['ALL_NAME'][dtf2.ID_CARD == idd]).iloc[0]),
                    'k_name': str((dtf2['PERSON_NAME'][dtf2.ID_CARD == idd]).iloc[0]),
                    'k_job': str((dtf2['POSITION'][dtf2.ID_CARD == idd]).iloc[0]),
                    'k_number': int(dtf33['ORIGINAL_VIOLATION_NUMBER'][dtf33.ID_CARD == idd]),
                    'k_score': int(dtf33['DEDUCT_SCORE'][dtf33.ID_CARD == idd]),
                }
            )

    dtf4 = dtf2.groupby('TYPE5').sum().sort_values(
        by='ORIGINAL_VIOLATION_NUMBER', ascending=False).reset_index()
    dtf44 = dtf4[dtf4['ORIGINAL_VIOLATION_NUMBER'] >= 7]
    if not dtf44.empty:
        id_list2 = dtf44['TYPE5'].tolist()
        for ids in id_list2:
            table8.append({
                'd_name': str((dtf2['ALL_NAME'][dtf2.TYPE5 == ids]).iloc[0]),
                'd_number': int(dtf44['ORIGINAL_VIOLATION_NUMBER'][dtf44.TYPE5 == ids]),
            })

    rest = {
        'table7': table7,
        'table8': table8
    }
    return rest


def get_evaluate_problem_type_table(start_date, end_date):
    """履职问题分类数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    # 当月履职评价
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date))
    total = evaluate_info[evaluate_info.ITEM_NAME.isin(all_items)].shape[0]
    counts = []
    percents = []
    for item in all_items:
        if total == 0:
            count, percent = 0, 0
        else:
            item_info = evaluate_info[evaluate_info['ITEM_NAME'] == item]
            count = len(item_info)
            percent = round(ded_zero(count, total) * 100, 1)
        counts.append(count)
        percents.append(percent)

    result = {
        'names': all_items,
        'counts': counts,
        'percents': percents,
    }
    return result


def get_ring_ratio(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        rst = count * 100
    else:
        rst = round(ded_zero((count - last_count), last_count) * 100, 1)
    return rst


# 动客车防火
def dongke_fanghuo(info, check_info):
    """
    :param info: dataframe 检查信息
    :param pro: dataframe  问题信息
    :return:总防火检查数   t
    客车防火问题数 k
    动车防火问题数 d
    典型问题详情   content
    """
    # 总防火检查次数
    fh_info = info[(info['RISK_NAME'].str.contains('供电')) | (info['RISK_NAME'].str.contains('火宅爆炸'))]
    t = len(fh_info)

    # 客车防火问题数
    check_info['is_fh'] = check_info['NAME'].apply(
        IS_FH)
    kc_pro = check_info[(check_info['IS_DONGCHE'] == 0) & (check_info['IS_GAOTIE'] == 0)]
    k = int(kc_pro['is_fh'].sum())

    # 动车防火问题数
    dc_pro = check_info[check_info['IS_DONGCHE'] == 1]
    d = int(dc_pro['is_fh'].sum())

    # 问题详情
    detail_pro = check_info[check_info['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]
    detail_pr = (detail_pro.DESCRIPTION[detail_pro['is_fh'] == 1]).tolist()
    if detail_pr:
        return {
            'totle_fangh_cou': t,
            'kc_fangh_pro': k,
            'dc_fangh_pro': d,
            'content': detail_pr
        }
    else:
        return {
            'totle_fangh_cou': t,
            'kc_fangh_pro': k,
            'dc_fangh_pro': d
        }


# 动客车防设备脱落
def shebei_tuoluo(info, pro):
    """

    :param info:检查信息 dataframe
    :param pro:问题信息 dataframe
    :return:总检查次数 t
    客车脱落问题数 k
    动车脱落问题数 d
    """
    # 配件脱落总检查次数
    gy_info = info[info['RISK_NAME'].str.contains('配件脱落')]
    gy_c = len(gy_info)

    # 高压牵引问题数
    # 严重问题详情
    gy_pro = pro[pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]
    gy_pro['is_dc'] = gy_pro['NAME'].apply(
        IS_TL)
    kc_pro = gy_pro[(gy_pro['IS_DONGCHE'] == 0) & (gy_pro['IS_GAOTIE'] == 0)]
    gy_k = int(kc_pro['is_fh'].sum())
    # 动车防火问题数
    dc_pro = gy_pro[gy_pro['IS_DONGCHE'] == 1]
    gy_d = int(dc_pro['is_fh'].sum())
    gy_pr = (gy_pro.DESCRIPTION[gy_pro['is_dc'].isin([1])]).tolist()
    if gy_pr:
        return {
            'gy_k': gy_k,
            'gy_d': gy_d,
            'gy_c': gy_c,
            'content': gy_pr
        }
    else:
        return {
            'gy_k': gy_k,
            'gy_d': gy_d,
            'gy_c': gy_c,
        }


# 动车高压牵引
def dongche_gaoya(info, pro):
    """
    info check-itms-names 车辆-辅助作业-动车组辅助作业
    pro  risk-names       车辆高压牵引
    :param info: 检查信息
    :param pro:  问题信息
    :return:gy_c gaoya check count
            gy_p gaoya problem count
    """
    # 高压牵引总检查次数
    gy_info = info[info['RISK_NAME'].str.contains('动车组高压牵引')]
    gy_c = len(gy_info)
    # 高压牵引问题数
    pro['is_dc'] = pro['NAME'].apply(
        IS_GY)
    gy_p = int(pro['is_dc'].sum())
    # 严重问题详情
    gy_pro = pro[pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]
    gy_pr = (gy_pro.DESCRIPTION[gy_pro['is_dc'] == 1]).tolist()
    if gy_pr:
        return {
            'gy_p': gy_p,
            'gy_c': gy_c,
            'content': gy_pr
        }
    else:
        return {
            'gy_p': gy_p,
            'gy_c': gy_c,
        }


def IS_FH(strrr):
    if strrr.count('火灾爆炸') or strrr.count('供电'):
        return 1
    else:
        return 0


def IS_TL(strrr):
    if strrr.count('配件脱落'):
        return 1
    else:
        return 0


def IS_GY(strrr):
    if strrr.count('动车组高压牵引'):
        return 1
    else:
        return 0


def IS_ZD(strrr):
    if strrr.count('制动供风'):
        return 1
    else:
        return 0


def deal_with(strr):
    return strr.split('-')[0]


def IS_DT(strrr):
    if strrr.count('货车动态检测作业'):
        return 1
    else:
        return 0


def IS_LAOAN(strrr):
    if strrr.count('劳动安全'):
        return 1
    else:
        return 0


def is_diaoche(strrr):
    if strrr.count('调车') or strrr.count('防溜'):
        return 1
    else:
        return 0


def singe_score_section(score):
    """对履职扣分进行分档

    Arguments:
        score {number} -- 扣分数值

    Returns:
        int -- 所属档次
    """
    if score < 2:
        return 1
    elif score < 4:
        return 2
    elif score < 6:
        return 3
    elif score < 8:
        return 4
    elif score < 10:
        return 5
    elif score < 12:
        return 6
    else:
        return 7


# 客车防制动
def keche_zhidong(info, pro):
    """

    :param  info:检查信息
    :param  pro:问题信息
    :return:zd_c 客车制动 count
            zd_p 客车制动 problem
            行车设备质量-制动供风
    """
    # 制动供风检查总次数
    zd_info = info[info['RISK_NAME'].str.contains('制动供风')]
    zd_c = len(zd_info)
    # 制动供风问题数
    pro['is_zd'] = pro['NAME'].apply(
        IS_ZD)
    zd_p = int(pro['is_zd'].sum())
    # 严重问题详情
    zd_pro = pro[pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]
    zd_pr = (zd_pro.DESCRIPTION[zd_pro['is_zd'] == 1]).tolist()
    if zd_pr:
        return {
            'zd_p': zd_p,
            'zd_c': zd_c,
            'content': zd_pr
        }
    else:
        return {
            'zd_p': zd_p,
            'zd_c': zd_c,
        }


# 货车5t
def five_t(info, pro):
    """

    :param  info:检查信息
    :param  pro:问题信息
    :return:hc_c 检查次数
            hc_p 问题数
            货车动态检测
    """

    # 货车检查总次数
    dt_info = info[info['RISK_NAME'].str.contains('货车动态检测作业')]
    dt_c = len(dt_info)
    # 动态问题数
    pro['is_dt'] = pro['NAME'].apply(
        IS_DT)
    dt_p = int(pro['is_dt'].sum())

    # 严重问题详情
    dt_pro = pro[pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]
    dt_pr = (dt_pro.DESCRIPTION[dt_pro['is_dt'] == 1]).tolist()
    if dt_pr:
        return {
            'dt_p': dt_p,
            'dt_c': dt_c,
            'content': dt_pr
        }
    else:
        return {
            'dt_p': dt_p,
            'dt_c': dt_c,
        }


# 防劳安
def laoan(info, pro):
    # 劳安检查总次数
    laoan_info = info[info['RISK_NAME'].str.contains('劳动安全')]
    laoan_c = len(laoan_info)
    # 劳安问题数
    pro['is_laoan'] = pro['NAME'].apply(IS_LAOAN)
    laoan_p = int(pro['is_laoan'].sum())
    # 严重问题详情
    laoan_pro = pro[(pro['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])) & (pro['is_laoan'] == 1)]
    laoan_pr = (laoan_pro.DESCRIPTION[laoan_pro['is_laoan'] == 1]).tolist()
    if laoan_pr:
        return {
            'laoan_p': laoan_p,
            'laoan_c': laoan_c,
            'content': laoan_pr
        }
    else:
        return {
            'laoan_p': laoan_p,
            'laoan_c': laoan_c,
        }


# 站段履职统计
def zhanduan_fun(eva, eva_year):
    eva['NEW_NAME'] = eva['ALL_NAME'].apply(lambda x: x.split('-')[1])
    eva_year['NEW_NAME'] = eva_year['ALL_NAME'].apply(lambda x: x.split('-')[1])
    rest = []
    for tt in eva['NEW_NAME'].value_counts().index:
        rst = []
        month_dat = eva[eva['NEW_NAME'] == tt]
        month_data = month_dat.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
        month_data['level'] = month_data['SCORE'].apply(singe_score_section)
        year_dat = eva_year[eva_year['NEW_NAME'] == tt]
        year_data = year_dat.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
        year_data['level'] = year_data['SCORE'].apply(singe_score_section)
        for lv in levels:
            rst.append(
                {
                    'level': level_name.get(lv),
                    'month': month_data[month_data['level'] == lv].shape[0],
                    'year': year_data[year_data['level'] == lv].shape[0],
                }
            )
        rest.append({
            'name': tt,
            'data': rst
        })
    return rest
