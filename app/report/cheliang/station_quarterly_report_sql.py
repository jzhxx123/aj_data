# 职工信息检查
worker_check_info_sql = """SELECT a.CHECK_PERSON_NAMES,a.PROBLEM_NUMBER,c.POSITION,e.NAME,b.COST_TIME
FROM t_check_info a
LEFT JOIN t_check_info_and_media b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_person c on a.ID_CARD=c.ID_CARD
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
LEFT JOIN t_department e on d.TYPE4=e.DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and d.TYPE3='{2}'"""


# 检查信息
check_info_sql = """SELECT a.CHECK_PERSON_NAMES,d.ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,e.ALL_NAME as STATION,a.RISK_NAME,
a.IS_YECHA
FROM t_check_info a
LEFT JOIN t_check_info_and_address c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
LEFT JOIN t_department e on d.TYPE4 = e.DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and d.TYPE3='{2}'
"""


# 检查发现问题信息
check_problem_info_sql = """
SELECT a.CHECK_PERSON_NAMES,d.ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,b.`LEVEL`,b.PROBLEM_CLASSITY_NAME,b.DESCRIPTION,
e.ALL_NAME as STATION,f.PK_ID,a.IS_DONGCHE,a.IS_GAOTIE,f.NAME,b.IS_ASSESS
FROM t_check_info a
LEFT JOIN t_check_problem b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_check_item f on b.FK_CHECK_ITEM_ID=f.PK_ID
LEFT JOIN t_check_info_and_address c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
LEFT JOIN t_department e on d.TYPE4 = e.DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and d.TYPE3='{2}'"""

# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        c.TYPE4,
        d.NAME AS MAJOR,
        e.JOB,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND  c.TYPE3 = '{2}'
"""

# 履职复查信息查询语句
check_evaluate_review_sql = """SELECT
        t.*,d.TYPE3
    FROM
        t_check_evaluate_review_person t
        left join t_department d on d.TYPE4=t.FK_RESPONSIBE_DEPARTMENT_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND d.TYPE3 = '{2}'
"""
laoan_item_name = '''
1078,1079,1080,1081,1082,1083,1089,1090,1093,1094,1095,1096,1098,1099,1100,1102,1103,1113,7156,1120,1121,1122,1123,1127
'''

#违章大王 没过滤部门
#重点班组就在这里 group by type5 sum
weizhang_king = '''
SELECT d.ALL_NAME,p.PERSON_NAME,p.ID_CARD,tp.JOB,tp.POSITION,p.ORIGINAL_VIOLATION_NUMBER,p.DEDUCT_SCORE,d.TYPE3,d.TYPE4,d.TYPE5 
FROM t_warning_key_person p LEFT JOIN t_person tp ON p.ID_CARD=tp.ID_CARD
                            LEFT JOIN t_department d ON tp.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
WHERE p.WARN_START_DATE BETWEEN '{}' AND '{}';
'''