from dateutil.relativedelta import relativedelta
import datetime
from app.data.util import pd_query
from app.report.cheliang.station_monthly_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find
import pandas as pd

_PROBLEM_TYPE = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4'],
    '外部环境': ['G1', 'G2', 'G3', 'G4'],
    '反恐防暴': ['K1', 'K2', 'K3', 'K4'],
    '高质量': ['A', 'B', 'F1', 'F2', 'E1', 'E2']
}


def calc_ring_and_diff(now_number, last_number):
    """
    环比计算
    :param now_number:
    :param last_number:
    :return:
    """
    if last_number == 0:
        return {
            'diff': now_number,
            'percent': now_number * 100
        }
    else:
        return {
            'diff': now_number - last_number,
            'percent': round((now_number - last_number) / last_number * 100, 2)
        }


def ded_zero(num1, num2):
    """
    防止除数为0
    :param num1:
    :param num2:
    :return:
    """
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, month, station_id):
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = \
        pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
            'all_name']
    # 月履职
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date, station_id))
    # 年履职
    evaluate_year_info = pd_query(
        check_evaluate_sql.format(f'{end_date[:4]}-01-01', end_date, station_id))
    evaluate_review = pd_query(check_evaluate_review_sql.format(start_date, end_date, station_id))
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_month_start, last_month_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 检查问题
    problem_data = pd_query(check_problem_info_sql.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_problem = pd_query(
        check_problem_info_sql.format(last_month_start, last_month_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 量化信息
    quantaze_data = pd_query(t_quanttization_sql.format(year, month, station_id))
    # 音视频
    mv_data = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 车间信息
    shop_data = pd_query(SHOP_PERSON_SQL.format(station_id))
    # 问题责任人
    duty_data = pd_query(DUTY_PERSON_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()

    first = get_first(start_date, end_date, station_id)
    second_one = get_second_data_anquan(info_data, problem_data, last_problem, quantaze_data)
    second_two = get_second_data_lvzhi(evaluate_info, evaluate_year_info, evaluate_review)
    second_table = get_second_table(info_data, last_info, problem_data, last_problem, quantaze_data, mv_data, shop_data,
                                    duty_data)
    third = get_third(info_data, problem_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        'last_month': str(last_month_start[5:7]) if str(last_month_start[5]) != 0 else str(last_month_start[6]),
        "major": '车辆',
        "station_id": station_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second_one": second_one,
        "second_two": second_two,
        'second_table': second_table,
        'third': third,
    }
    return result


# 第一部分
def get_first(start_date, end_date, station_id):
    data = pd.DataFrame(calc_safety_produce_data(start_date, end_date, station_id))
    data = data.dropna(subset=['RESPONSIBILITY_IDENTIFIED_NAME'])
    # 责任事故
    duty_data = data[(data['MAIN_CLASS'] == '事故') & (data['RESPONSIBILITY_IDENTIFIED_NAME'] != '非责任')]
    # 行车设备故障
    acc = data[data['NAME'].str.contains('行车设备故障')]
    return {
        'duty': len(duty_data),
        'acc': len(acc)
    }


def calc_safety_produce_data(start_date, end_date, station_id):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param start_date:
    :param end_date:
    :param station_id:
    :return:
    """
    now_time = int(''.join(start_date.split('-')))
    end_time = int(''.join(end_date.split('-')))
    keys = {
        "match": {
            "DATE": {
                '$lte': end_time,
                '$gte': now_time
            },
            'TYPE3': station_id
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
            'RESPONSIBILITY_IDENTIFIED_NAME': 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        documents = [{"NAME": '1',
                      'REASON': '1',
                      'RISK_NAME': '1',
                      "MAIN_CLASS": '1',
                      'RESPONSIBILITY_IDENTIFIED_NAME': '1'}]
    return documents


# 第二部分的第一节安全内容
def get_second_data_anquan(info_data, problem_data, last_problem, quantaze_data):
    # 检查人员数
    check_person = len(quantaze_data)
    # 检查次数+兑现率
    check_count = int(quantaze_data['REALITY_NUMBER'].sum())
    ratio1 = round(ded_zero(check_count, quantaze_data['CHECK_TIMES_TOTAL'].sum()) * 100, 2)
    # 发现问题个数+兑现率
    find_pro = int(quantaze_data['REALITY_PROBLEM_NUMBER'].sum())
    ratio2 = round(ded_zero(find_pro, quantaze_data['PROBLEM_NUMBER_TOTAL'].sum()) * 100, 2)
    # 作业项问题+兑现率
    work_pro = int(quantaze_data['REALITY_WORK_ITEM_PROBLEM_NUMBER'].sum())
    ratio3 = round(ded_zero(work_pro, quantaze_data['WORK_PROBLEM_NUMBER_TOTAL'].sum()) * 100, 2)
    # 检查发现各问题数
    all_pros = []
    for i in [2, 3, 4, 6, 5]:
        data = info_data[info_data['CHECK_WAY'] == i]
        all_pros.append(int(data['PROBLEM_NUMBER'].sum()))
    # 严重问题
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    last_pro = len(last_problem[last_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    # 环比
    ratio4 = calc_ring_and_diff(main_pro, last_pro)
    return {
        'check_person': check_person,
        'check_count': check_count,
        'ratio1': ratio1,
        'find_pro': find_pro,
        'ratio2': ratio2,
        'work_pro': work_pro,
        'ratio3': ratio3,
        'all_pros': all_pros,
        'main_pro': main_pro,
        'ratio4': ratio4
    }


# 第二部分的第二节履职内容
def get_second_data_lvzhi(evaluate_info, evaluate_year_info, evaluate_review):
    # 履职评价
    eva_situation = get_eva_situation(evaluate_info, evaluate_review)
    # 干部履职问题类型统计表
    table1 = get_eva_problem_count(evaluate_info)
    # 计分统计表
    table2 = get_score_count(evaluate_info, evaluate_year_info)
    return {
        'eva_situation': eva_situation,
        'table1': table1,
        'table2': table2
    }


def get_eva_situation(evaluate_info, evaluate_review):
    """
    履职评价总体情况
    :param evaluate_info:
    :param evaluate_review:
    :return:
    """
    evaluate_info = evaluate_info[evaluate_info['CHECK_TYPE'] == 2]
    evaluate_review = evaluate_review[evaluate_review['CHECK_TYPE'] == 2]
    # 定期评价
    dq = len(evaluate_info[evaluate_info['EVALUATE_WAY'] == 2])
    # 定期复查
    dq_re = len(evaluate_review[evaluate_review['EVALUATE_WAY'] == 2])
    # 逐条评价
    zt = len(evaluate_info[evaluate_info['EVALUATE_WAY'] == 1])
    # 复查信息和问题
    info_and_pro = len(evaluate_review[evaluate_review['EVALUATE_TYPE'].isin([1, 2])])
    # 干部履职
    all_count = []
    for i in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        data = evaluate_info[evaluate_info['GRADATION'].str.contains(i)]
        all_count.append(len(data))
    return {
        'dq': dq,
        'dq_re': dq_re,
        'zt': zt,
        'info_and_pro': info_and_pro,
        'gb': sum(all_count),
        'cj': all_count[0],
        'kj': sum(all_count[1:3]),
        'yb': all_count[3],
        'fg': all_count[4]
    }


def get_eva_problem_count(evaluate_info):
    count = []
    for i in ['问题闭环管理', '考核责任落实', '监督检查质量', '检查信息录入', '重点工作落实', '量化指标完成', '安全谈心', '事故故障追责', '音视频运用管理']:
        count.append(len(evaluate_info[evaluate_info['ITEM_NAME'] == i]))
    ratio = []
    for i in count:
        ratio.append(round(ded_zero(i, sum(count)) * 100, 2))
    # 扣分最高人员
    max_data = evaluate_info.sort_values(by='SCORE', ascending=False).reset_index()
    dic = {}
    for i, k in max_data.iterrows():
        dic = {
            'name': k['RESPONSIBE_PERSON_NAME'],
            'dp': k['ALL_NAME'],
            'job': k['JOB'],
            'score': round(float(k['SCORE']), 4)
        }
        break
    return {
        'count': count,
        'ratio': ratio,
        'dic': dic
    }


def get_score_count(evaluate_info, evaluate_year_info):
    all_list = []
    for i in evaluate_info['SHOP'].unique().tolist():
        eva_info = evaluate_info[evaluate_info['SHOP'] == i].groupby('RESPONSIBE_ID_CARD').sum().reset_index()
        eva_year_info = evaluate_year_info[evaluate_year_info['SHOP'] == i].groupby(
            'RESPONSIBE_ID_CARD').sum().reset_index()
        # 月扣分累计
        month_number = calc_eva_score(eva_info)
        # 年扣分累计
        year_number = calc_eva_score(eva_year_info)
        dic = {
            'name': i,
            'month': month_number,
            'year': year_number
        }
        all_list.append(dic)
    # 系统月扣分累计
    month_count = calc_sum_score(all_list, 'month')
    # 系统年扣分累计
    year_count = calc_sum_score(all_list, 'year')
    # 年扣分总人数
    year = sum(year_count)
    return {
        'all_list': all_list,
        'month_count': month_count,
        'year_count': year_count,
        'year': year
    }


def calc_eva_score(data):
    """
    计算履职扣分各阶段人数
    :param data:
    :return:
    """
    count = [0, 0, 0, 0, 0, 0, 0]
    for j, k in data.iterrows():
        if k['SCORE'] < 2:
            count[0] += 1
        elif k['SCORE'] < 4:
            count[1] += 1
        elif k['SCORE'] < 6:
            count[2] += 1
        elif k['SCORE'] < 8:
            count[3] += 1
        elif k['SCORE'] < 10:
            count[4] += 1
        elif k['SCORE'] < 12:
            count[5] += 1
        else:
            count[6] += 1
    return count


def calc_sum_score(data, time_str):
    """
    扣分累计
    :param data:
    :param time_str: 计算月还是年数据
    :return:
    """
    count = [0, 0, 0, 0, 0, 0, 0]
    for i in data:
        for j in range(len(i[time_str])):
            count[j] += i[time_str][j]
    return count


def get_second_table(info_data, last_info, problem_data, last_problem, quantaze_data, mv_data, shop_data, duty_data):
    # 月度检查数据环比
    table1 = get_second_table1(info_data, last_info, problem_data, last_problem)
    # 各部门关键指标对比表
    table2 = get_second_table2(info_data, problem_data, quantaze_data)
    # 音视频调阅情况
    table3 = get_second_table3(mv_data)
    # 段主要风险检查管控情况
    table4 = get_second_table4(info_data, shop_data)
    # 违章大王情况分析
    table5 = get_second_table5(duty_data)
    return {
        'table1': table1,
        'table2': table2,
        'table3': table3,
        'table4': table4,
        'table5': table5
    }


def get_second_table1(info_data, last_info, problem_data, last_problem):
    """
    月度检查数据环比
    :param info_data:
    :param last_info:
    :param problem_data:
    :param last_problem:
    :return:
    """
    # 本月
    month_data = calc_two_month_data(info_data, problem_data)
    # 上月
    last_month = calc_two_month_data(last_info, last_problem)
    # 环比
    ratio = []
    for i in range(len(month_data)):
        ratio.append(calc_ring_and_diff(month_data[i], last_month[i])['percent'])
    return {
        'month_data': month_data,
        'last_month': last_month,
        'ratio': ratio
    }


def calc_two_month_data(info_data, problem_data):
    """
    计算单个月的检查信息、问题数据
    :param info_data:
    :param problem_data:
    :return:
    """
    # 问题数
    pro = len(problem_data)
    pro_type = []
    for i in ['作业', '设备', '管理', '反恐防暴', '外部环境', '高质量']:
        data = problem_data[problem_data['LEVEL'].isin(_PROBLEM_TYPE[i])]
        pro_type.append(len(data))
    # 考核率
    kh = len(problem_data[problem_data['IS_ASSESS'] == 1])
    kh_ratio = round(ded_zero(kh, pro) * 100, 2)
    # 夜查次数
    yc = len(info_data[info_data['IS_YECHA'] == 1])
    # 夜查率
    yc_ratio = round(ded_zero(yc, len(info_data)) * 100, 2)
    return [pro, pro_type[0], pro_type[1], pro_type[2], pro_type[3], pro_type[4], pro_type[5], kh_ratio, yc, yc_ratio]


def get_second_table2(info_data, problem_data, quantaze_data):
    all_shop = []
    for i in quantaze_data['SHOP'].unique().tolist():
        info = info_data[info_data['NAME'] == i]
        pro = problem_data[problem_data['NAME'] == i]
        qt = quantaze_data[quantaze_data['SHOP'] == i]
        # 检查完成率
        check_ratio = round(float(ded_zero(qt['REALITY_NUMBER'].sum(), qt['CHECK_TIMES_TOTAL'].sum())) * 100, 2)
        # 问题完成率
        pro_ratio = round(float(ded_zero(qt['REALITY_PROBLEM_NUMBER'].sum(), qt['PROBLEM_NUMBER_TOTAL'].sum())) * 100,
                          2)
        # 问题发现率
        find_ratio = round(float(ded_zero(qt['REALITY_PROBLEM_NUMBER'].sum(), qt['REALITY_NUMBER'].sum())) * 100, 2)
        # 人均检查次数
        check_avg = round(float(ded_zero(qt['REALITY_NUMBER'].sum(), len(qt))), 1)
        # 夜查率
        yc_ratio = round(float(ded_zero(len(info[info['IS_YECHA'] == 1]), len(info))) * 100, 2)
        # 中高质量问题占比
        main_ratio = round(
            float(ded_zero(len(pro[pro['LEVEL'].isin(['A', 'B', 'C', 'F1', 'F2', 'F3', 'E1', 'E2', 'E3'])]),
                           len(pro))) * 100, 2)
        all_shop.append({
            'name': i,
            'check_ratio': check_ratio,
            'pro_ratio': pro_ratio,
            'find_ratio': find_ratio,
            'check_avg': check_avg,
            'yc_ratio': yc_ratio,
            'main_ratio': main_ratio
        })
    return {
        'all_shop': all_shop
    }


def get_second_table3(mv_data):
    shops = []
    for i in mv_data[mv_data['NAME'].str.contains('车间')]['NAME'].unique().tolist():
        data = mv_data[mv_data['NAME'] == i]
        position = []
        for j in ['质检员', '5T维修工', '高级修机械师', '地勤机械师', '随车机械师', '探伤工', '调度员', '调车作业']:
            new_data = data[data['MONITOR_POST_NAMES'].str.contains(j)]
            # 调阅时长
            cost_time = float(new_data['COST_TIME'].sum())
            # 发现问题数
            find_pro = int(new_data['PROBLEM_NUMBER'].sum())
            position.append([cost_time, find_pro])
        shops.append({
            'name': i,
            'position': position
        })
    return {
        'shops': shops
    }


def get_second_table4(info_data, shop_data):
    all_shops = []
    for i in shop_data['NAME'].tolist():
        # 车间人数
        zd_person = int(shop_data[shop_data['NAME'] == i]['COUNT'])
        info = info_data[info_data['NAME'] == i]
        all_risk = []
        for j in ['配件脱落', '火灾爆炸', '高压牵引', '制动供风', '劳动安全']:
            info = info.dropna(subset=['RISK_NAME'])
            data = info[info['RISK_NAME'].str.contains(j)]
            dic = {
                'count': len(data),
                'pro': int(data['PROBLEM_NUMBER'].sum(0)),
                'avg_count': round(ded_zero(len(data), zd_person), 2),
                'avg_pro': round(ded_zero(int(data['PROBLEM_NUMBER'].sum(0)), zd_person), 2)
            }
            all_risk.append(dic)
        # 调车防溜
        dc_data = info[info['CHECK_ITEM_NAMES'].str.contains('调车防溜')]
        all_risk.append({
            'count': len(dc_data),
            'pro': int(dc_data['PROBLEM_NUMBER'].sum()),
            'avg_count': round(ded_zero(len(dc_data), zd_person), 2),
            'avg_pro': round(ded_zero(int(dc_data['PROBLEM_NUMBER'].sum(0)), zd_person), 2)
        })
        all_shops.append({
            'name': i,
            'content': all_risk
        })
    return {
        'all_shops': all_shops
    }


def get_second_table5(duty_data):
    # 违章人员情况
    person = duty_data['ID_CARD'].value_counts()
    # 获取身份信息
    id_card = list(person[person > 0].index)
    # 获取违章次数
    count = list(person[person > 0].values)
    all_person = []
    for i in id_card:
        data = duty_data[duty_data['ID_CARD'] == i]
        if int(count[id_card.index(i)]) >= 2:
            dic = {
                'dp': list(data['ALL_NAME'].unique())[0],
                'name': list(data['PERSON_NAME'].unique())[0],
                'position': list(data['POSITION'].unique())[0],
                'score': float(data['RESPONSIBILITY_SCORE'].sum()),
                'count': int(count[id_card.index(i)])
            }
            all_person.append(dic)
        else:
            continue
    # 重点违章班组
    all_shops = []
    for j, k in duty_data[duty_data['TYPE'].isin([9, 10])].groupby('NAME').count().reset_index().iterrows():
        if k['POSITION'] >= 7:
            dic = {
                'dp': k['NAME'],
                'count': int(k['POSITION'])
            }
            all_shops.append(dic)
        else:
            continue
    return {
        'all_person': sorted(all_person, key=lambda x: x['count'], reverse=True),
        'all_shops': sorted(all_shops, key=lambda x: x['count'], reverse=True)
    }


def get_third(info_data, problem_data):
    """
    重点风险管控情况分析
    :param info_data:
    :param problem_data:
    :return:
    """
    # 集团公司检查发现问题
    third_one = get_third_one(problem_data)
    # 车辆系统主要风险检查管控情况
    third_two = get_third_two(info_data, problem_data)
    return {
        'third_one': third_one,
        'third_two': third_two
    }


def get_third_one(problem_data):
    data = problem_data[problem_data['TYPE'].isin([1, 2])]
    # 发现问题个数
    pro = len(data)
    # 严重问题
    main_pro = len(data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    # 问题集中段
    new_data = data.groupby('NAME').count().sort_values(by='LEVEL', ascending=False).reset_index()
    max_station = new_data['NAME'][0]
    max_count = int(new_data['LEVEL'][0])
    ratio = round(ded_zero(max_count, pro) * 100, 2)
    # 典型问题
    all_content = calc_special_problem(data, ['A', 'B'])
    return {
        'pro': pro,
        'main_pro': main_pro,
        'max_station': max_station,
        'max_count': max_count,
        'ratio': ratio,
        'all_content': all_content
    }


def calc_special_problem(data, list1):
    # 问题类型
    data = data[data['LEVEL'].isin(list1)]
    # 根据问题分类
    special_pro = data.groupby(['NAME', 'LEVEL', 'CHECK_PERSON_NAME', 'DESCRIPTION']).count().reset_index()
    all_content = []
    for i, k in special_pro.iterrows():
        dic = {
            # 站段
            'station': k['NAME'],
            # 等级
            'level': k['LEVEL'],
            # 问题检查人
            'person': k['CHECK_PERSON_NAME'],
            # 内容
            'content': k['DESCRIPTION']
        }
        all_content.append(dic)
    return all_content


def get_third_two(infos_data, problems_data):
    info_data = infos_data.dropna(subset=['RISK_NAME'])
    problem_data = problems_data.dropna(subset=['RISK_NAMES'])
    # 动客车防火
    fh_info = info_data[(info_data['RISK_NAME'].str.contains('供电')) | (info_data['RISK_NAME'].str.contains('火灾爆炸'))]
    fh_data = problem_data[
        (problem_data['RISK_NAMES'].str.contains('供电')) | (problem_data['RISK_NAMES'].str.contains('火灾爆炸'))]
    fh = calc_risk_type_data(fh_info, fh_data, [3, 2])
    # 动、客车防脱风险
    ft_info = info_data[info_data['RISK_NAME'].str.contains('配件脱落')]
    ft_data = problem_data[problem_data['RISK_NAMES'].str.contains('配件脱落')]
    ft = calc_risk_type_data(ft_info, ft_data, [3, 2])
    # 动车高压牵引风险
    gyqy_info = info_data[info_data['RISK_NAME'].str.contains('动车组高压牵引')]
    gyqy_data = problem_data[problem_data['RISK_NAMES'].str.contains('动车组高压牵引')]
    gyqy = calc_risk_type_data(gyqy_info, gyqy_data, [1, 2])
    # 客车防制动风险
    zdgf_info = info_data[info_data['RISK_NAME'].str.contains('制动供风')]
    zdgf_data = problem_data[problem_data['RISK_NAMES'].str.contains('制动供风')]
    zdgf = calc_risk_type_data(zdgf_info, zdgf_data, [3])
    # 防劳动安全风险
    ldaq_info = info_data[info_data['RISK_NAME'].str.contains('劳动安全')]
    ldaq_data = problem_data[problem_data['RISK_NAMES'].str.contains('劳动安全')]
    ldaq = calc_risk_type_data(ldaq_info, ldaq_data, [])
    # 货车防5T作业风险
    info_datas = infos_data.dropna(subset=['CHECK_ITEM_NAMES'])
    problem_datas = problems_data.dropna(subset=['CHECK_ITEM_NAME'])
    five_t_data = problem_datas[problem_datas['CHECK_ITEM_NAME'].str.contains('货车动态检测作业')]
    five_t_count = len(info_datas[info_datas['CHECK_ITEM_NAMES'].str.contains('货车动态检测作业')])
    five_t_pro = int(info_datas[info_datas['CHECK_ITEM_NAMES'].str.contains('货车动态检测作业')]['PROBLEM_NUMBER'].sum())
    five_t_content = calc_special_problem(five_t_data, ['A', 'B', 'F1', 'F2', 'E1', 'E2'])
    return {
        'five_t_count': five_t_count,
        'five_t_pro': five_t_pro,
        'five_t_content': five_t_content,
        'fh': fh,
        'ft': ft,
        'gyqy': gyqy,
        'zdgf': zdgf,
        'ldaq': ldaq
    }


def calc_risk_type_data(info, pro, list1):
    """
    计算风险信息和问题
    :param info: 信息data
    :param pro: 问题data
    :param list1: 高动客问题 1：高铁 2：动车 3：客车
    :return:
    """
    count = len(info)
    all_content = []
    pro_count = []
    if len(list1) == 0:
        data = pro[pro['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
        special_pro = data.groupby(['NAME', 'LEVEL', 'CHECK_PERSON_NAME', 'DESCRIPTION']).count().reset_index()
        pro_count.append(len(data))
        for j, k in special_pro.iterrows():
            dic = {
                'station': k['NAME'],
                'level': k['LEVEL'],
                'person': k['CHECK_PERSON_NAME'],
                'content': k['DESCRIPTION']
            }
            all_content.append(dic)
    else:
        for i in list1:
            data = pro[pro['PROBLEM_DIVIDE_IDS'] == i]
            data = data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
            special_pro = data.groupby(['NAME', 'LEVEL', 'CHECK_PERSON_NAME', 'DESCRIPTION']).count().reset_index()
            pro_count.append(len(data))
            for j, k in special_pro.iterrows():
                dic = {
                    'station': k['NAME'],
                    'level': k['LEVEL'],
                    'person': k['CHECK_PERSON_NAME'],
                    'content': k['DESCRIPTION']
                }
                all_content.append(dic)
    return {
        'count': count,
        'all_content': all_content,
        'pro_count': pro_count
    }
