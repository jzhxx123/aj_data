from dateutil.relativedelta import relativedelta
import datetime
from app.data.util import pd_query
from app.report.cheliang.major_monthly_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find
import pandas as pd


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calc_ring_and_diff(now_number, last_number):
    if last_number == 0:
        return {
            'diff': now_number,
            'percent': now_number * 100
        }
    else:
        return {
            'diff': now_number - last_number,
            'percent': round((now_number - last_number) / last_number * 100, 2)
        }


def get_data(year, month):
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    global last_month_start, last_month_end, last_year_start, last_year_end
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    last_year_start = str(start_date - relativedelta(months=12))[:10]
    last_year_end = str(end_date - relativedelta(months=12))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查问题信息
    problem_data = pd_query(check_problem_info_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_info_sql.format(last_month_start, last_month_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职评价信息
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    evaluate_year_info = pd_query(check_evaluate_sql.format(end_date[:4] + '-01-01', end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    evaluate_review = pd_query(check_evaluate_review_sql.format(start_date, end_date))
    eva_re = pd_query(eva_review_sql.format(start_date, end_date))
    # 量化指标
    quantaze_data = pd_query(t_quanttization_sql.format(int(year), int(month)))

    # 文档数据
    first = get_first(start_date, end_date)
    second_one = get_second_one_data(info_data, problem_data, last_problem, quantaze_data)
    second_two = get_second_two_data(evaluate_info, evaluate_year_info, evaluate_review, quantaze_data, eva_re)
    third = get_third(info_data, problem_data)
    file_name = f'{start_date}至{end_date}车辆系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '车辆',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'first': first,
        "second_one": second_one,
        "second_two": second_two,
        'third': third
    }
    return result


def get_first(start_date, end_date):
    data = pd.DataFrame(calc_safety_produce_data(start_date, end_date))
    data = data.dropna(subset=['RESPONSIBILITY_IDENTIFIED_NAME'])
    # 责任事故
    duty_data = data[(data['MAIN_CLASS'] == '事故') & (data['RESPONSIBILITY_IDENTIFIED_NAME'] != '非责任')]
    # 行车设备故障
    acc = data[data['NAME'].str.contains('行车设备故障')]
    # 事故故障内容
    content = list(set(duty_data['OVERVIEW'].tolist() + acc['OVERVIEW'].tolist()))
    return {
        'duty': len(duty_data),
        'acc': len(acc),
        'content': content
    }


def calc_safety_produce_data(start_date, end_date):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param start_date:
    :param end_date:
    :return:
    """
    now_time = int(''.join(start_date.split('-')))
    end_time = int(''.join(end_date.split('-')))
    keys = {
        "match": {
            "DATE": {
                '$lte': end_time,
                '$gte': now_time
            },
            'MAJOR': '车辆'
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
            'OVERVIEW': 1,
            'RESPONSIBILITY_IDENTIFIED_NAME': 1
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if documents == []:
        documents = [{"NAME": '1',
            'REASON': '1',
            'RISK_NAME': '1',
            "MAIN_CLASS": '1',
            'OVERVIEW': '1',
            'RESPONSIBILITY_IDENTIFIED_NAME': '1'}]
    return documents


# 第二部分的第一节安全内容
def get_second_one_data(info_data, problem_data, last_problem, quantaze_data):
    # 检查人员数
    check_person = len(quantaze_data)
    # 检查次数+兑现率
    check_count = int(quantaze_data['REALITY_NUMBER'].sum())
    ratio1 = round(ded_zero(check_count, quantaze_data['CHECK_TIMES_TOTAL'].sum()) * 100, 2)
    # 发现问题个数+兑现率
    find_pro = int(quantaze_data['REALITY_PROBLEM_NUMBER'].sum())
    ratio2 = round(ded_zero(find_pro, quantaze_data['PROBLEM_NUMBER_TOTAL'].sum()) * 100, 2)
    # 作业项问题+兑现率
    work_pro = int(quantaze_data['REALITY_WORK_ITEM_PROBLEM_NUMBER'].sum())
    ratio3 = round(ded_zero(work_pro, quantaze_data['WORK_PROBLEM_NUMBER_TOTAL'].sum()) * 100, 2)
    # 检查发现各问题数
    all_pros = []
    for i in [2, 3, 4, 6, 5]:
        data = info_data[info_data['CHECK_WAY'] == i]
        all_pros.append(int(data['PROBLEM_NUMBER'].sum()))
    # 严重问题
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    last_pro = len(last_problem[last_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    # 环比
    ratio4 = calc_ring_and_diff(main_pro, last_pro)
    return {
        'check_person': check_person,
        'check_count': check_count,
        'ratio1': ratio1,
        'find_pro': find_pro,
        'ratio2': ratio2,
        'work_pro': work_pro,
        'ratio3': ratio3,
        'all_pros': all_pros,
        'main_pro': main_pro,
        'ratio4': ratio4
    }


# 第二部分的第二节履职内容
def get_second_two_data(evaluate_info, evaluate_year_info, evaluate_review, quantaze_data, eva_re):
    # 履职评价
    eva_situation = get_eva_situation(evaluate_info, evaluate_review, quantaze_data, eva_re)
    # 干部履职问题类型统计表
    table1 = get_eva_problem_count(evaluate_info)
    # 计分统计表
    table2 = get_score_count(evaluate_info, evaluate_year_info)
    return {
        'eva_situation': eva_situation,
        'table1': table1,
        'table2': table2
    }


def get_eva_situation(evaluate_info, evaluate_review, quantaze_data, eva_re):
    """
    履职评价总体情况
    :param evaluate_info:
    :param evaluate_review:
    :return:
    """
    # 定期评价
    dq = len(evaluate_review[evaluate_review['EVALUATE_WAY'] == 2])
    # 定期复查
    dq_re = len(eva_re[eva_re['EVALUATE_WAY'] == 2])
    # 逐条评价
    zt = len(evaluate_review[evaluate_review['EVALUATE_WAY'] == 1])
    # 复查信息和问题
    info_and_pro = len(eva_re[eva_re['EVALUATE_TYPE'].isin([1, 2])])
    # 干部履职
    all_count = []
    for i in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员']:
        data = evaluate_info[evaluate_info['GRADATION'].str.contains(i)]
        all_count.append(len(data))
    #量化干部人数
    lh_gb = len(quantaze_data['ID_CARD'].unique())
    # 占比
    lh_ratio = round(ded_zero(sum(all_count), lh_gb) * 100, 2)
    return {
        'dq': dq,
        'dq_re': dq_re,
        'zt': zt,
        'info_and_pro': info_and_pro,
        'gb': sum(all_count[:4]),
        'cj': all_count[0],
        'kj': sum(all_count[1:3]),
        'yb': all_count[3],
        'lh_ratio': lh_ratio
    }


def get_eva_problem_count(evaluate_info):
    count = []
    for i in ['问题闭环管理', '考核责任落实', '监督检查质量', '检查信息录入', '重点工作落实', '量化指标完成', '安全谈心', '事故故障追责', '音视频运用管理']:
        count.append(len(evaluate_info[evaluate_info['ITEM_NAME'].str.contains(i)]))
    ratio = []
    for i in count:
        ratio.append(round(ded_zero(i, sum(count)) * 100, 2))
    # 累计扣分最高人员
    max_data = evaluate_info.groupby(['RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'ALL_NAME', 'JOB']).sum().sort_values(by='SCORE', ascending=False).reset_index()
    dic = {}
    for i, k in max_data.iterrows():
        dic = {
            'name': k['RESPONSIBE_PERSON_NAME'],
            'dp': k['ALL_NAME'],
            'job': k['JOB'],
            'score': round(float(k['SCORE']), 4)
        }
        break
    return {
        'count': count,
        'ratio': ratio,
        'dic': dic
    }


def get_score_count(evaluate_info, evaluate_year_info):
    all_list = []
    for i in ['车辆部', '成都动车段', '成都车辆段', '成都北车辆段', '重庆车辆段', '重庆西车辆段', '贵阳车辆段', '贵阳南车辆段']:
        eva_info = evaluate_info[evaluate_info['STATION'] == i].groupby('RESPONSIBE_ID_CARD').sum().reset_index()
        eva_year_info = evaluate_year_info[evaluate_year_info['STATION'] == i].groupby('RESPONSIBE_ID_CARD').sum().reset_index()
        # 月扣分累计
        month_number = calc_eva_score(eva_info)
        # 年扣分累计
        year_number = calc_eva_score(eva_year_info)
        dic = {
            'name': i,
            'month': month_number,
            'year': year_number
        }
        all_list.append(dic)
    # 系统月扣分累计
    month_count = calc_sum_score(all_list, 'month')
    # 系统年扣分累计
    year_count = calc_sum_score(all_list, 'year')
    # 年扣分总人数
    year = sum(year_count)
    all_year_count = calc_eva_score(evaluate_year_info.groupby('RESPONSIBE_ID_CARD').sum().reset_index())
    return {
        'all_list': all_list,
        'month_count': month_count,
        'year_count': year_count,
        'year': year,
        'all_year_count': all_year_count
    }


def calc_eva_score(data):
    """
    计算履职扣分各阶段人数
    :param data:
    :return:
    """
    count = [0, 0, 0, 0, 0, 0, 0]
    for j, k in data.iterrows():
        if k['SCORE'] < 2:
            count[0] += 1
        elif k['SCORE'] < 4:
            count[1] += 1
        elif k['SCORE'] < 6:
            count[2] += 1
        elif k['SCORE'] < 8:
            count[3] += 1
        elif k['SCORE'] < 10:
            count[4] += 1
        elif k['SCORE'] < 12:
            count[5] += 1
        else:
            count[6] += 1
    return count


def calc_sum_score(data, time_str):
    """
    扣分累计
    :param data:
    :param time_str: 计算月还是年数据
    :return:
    """
    count = [0, 0, 0, 0, 0, 0, 0]
    for i in data:
        for j in range(len(i[time_str])):
            count[j] += i[time_str][j]
    return count


def get_third(info_data, problem_data):
    # 集团公司检查发现问题
    third_one = get_third_one(problem_data)
    # 车辆系统主要风险检查管控情况
    third_two = get_third_two(info_data, problem_data)
    return {
        'third_one': third_one,
        'third_two': third_two
    }


def get_third_one(problem_data):
    data = problem_data[problem_data['DEPARTMENT_ALL_NAME'].str.contains('安全监察大队') |
                        problem_data['DEPARTMENT_ALL_NAME'].str.contains('车辆部')]
    data = data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2'])]
    # 发现问题个数
    pro = len(data)
    # 安监室
    aj = len(data[data['DEPARTMENT_ALL_NAME'].str.contains('安全监察大队')])
    # 车辆部
    cl_dept = len(data[data['DEPARTMENT_ALL_NAME'].str.contains('车辆部')])
    # 严重问题
    main_pro = len(data[data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    # 问题集中段
    new_data = data.groupby('NAME').count().sort_values(by='LEVEL', ascending=False).reset_index()
    max_station = new_data['NAME'][0]
    max_count = int(new_data['LEVEL'][0])
    ratio = round(ded_zero(max_count, pro) * 100, 2)
    # 典型问题
    all_content = calc_special_problem(data, ['A', 'F1'])
    return {
        'pro': pro,
        'aj': aj,
        'cl_dept': cl_dept,
        'main_pro': main_pro,
        'max_station': max_station,
        'max_count': max_count,
        'ratio': ratio,
        'all_content': all_content
    }


def calc_special_problem(data, list1):
    # 问题类型
    data = data[data['LEVEL'].isin(list1)]
    # 根据问题分类
    special_pro = data.groupby(['NAME', 'LEVEL', 'CHECK_PERSON_NAME', 'DESCRIPTION']).count().reset_index()
    all_content = []
    for i, k in special_pro.iterrows():
        dic = {
            # 站段
            'station': k['NAME'],
            # 等级
            'level': k['LEVEL'],
            # 问题检查人
            'person': k['CHECK_PERSON_NAME'],
            # 内容
            'content': k['DESCRIPTION']
        }
        all_content.append(dic)
    return all_content


def get_third_two(infos_data, problems_data):
    info_data = infos_data.dropna(subset=['RISK_NAME'])
    problem_data = problems_data.dropna(subset=['RISK_NAMES'])
    problem_data['PROBLEM_DIVIDE_IDS'] = problem_data['PROBLEM_DIVIDE_IDS'].apply(lambda x: ','.join([str(int(i)) for i in x.split(',')]))
    # 动客车防火
    fh_info = info_data[info_data['RISK_NAME'].str.contains('车辆-火灾爆炸')]
    fh_data = problem_data[problem_data['RISK_NAMES'].str.contains('车辆-火灾爆炸')]
    fh = calc_risk_type_data(fh_info, fh_data, [3, 2])
    # 动、客车防脱风险
    ft_info = info_data[info_data['RISK_NAME'].str.contains('车辆-行车设备质量-配件脱落')]
    ft_data = problem_data[problem_data['RISK_NAMES'].str.contains('车辆-行车设备质量-配件脱落')]
    ft = calc_risk_type_data(ft_info, ft_data, [3, 2])
    # 动车高压牵引风险
    gyqy_info = info_data[info_data['RISK_NAME'].str.contains('车辆-行车设备质量-动车组高压牵引')]
    gyqy_data = problem_data[problem_data['RISK_NAMES'].str.contains('车辆-行车设备质量-动车组高压牵引')]
    gyqy = calc_risk_type_data(gyqy_info, gyqy_data, [])
    # 客车防制动风险
    zdgf_info = info_data[info_data['RISK_NAME'].str.contains('车辆-行车设备质量-制动供风')]
    zdgf_data = problem_data[problem_data['RISK_NAMES'].str.contains('车辆-行车设备质量-制动供风')]
    zdgf = calc_risk_type_data(zdgf_info, zdgf_data, [3])
    # 防劳动安全风险
    ldaq_info = info_data[info_data['RISK_NAME'].str.contains('劳动安全')]
    ldaq_data = problem_data[problem_data['RISK_NAMES'].str.contains('劳动安全')]
    ldaq = calc_risk_type_data(ldaq_info, ldaq_data, ['劳安'])
    # 货车防5T作业风险
    info_datas = infos_data.dropna(subset=['CHECK_ITEM_NAMES'])
    problem_datas = problems_data.dropna(subset=['CHECK_ITEM_NAME'])
    five_t_data = problem_datas[problem_datas['CHECK_ITEM_NAME'].str.contains('车辆-货车运用作业-货车动态检测作业')]
    five_t_count = len(info_datas[info_datas['CHECK_ITEM_NAMES'].str.contains('车辆-货车运用作业-货车动态检测作业')])
    five_t_pro = int(info_datas[info_datas['CHECK_ITEM_NAMES'].str.contains('车辆-货车运用作业-货车动态检测作业')]['PROBLEM_NUMBER'].sum())
    five_t_content = calc_special_problem(five_t_data, ['A', 'B', 'F1', 'F2', 'E1', 'E2'])
    return {
        'five_t_count': five_t_count,
        'five_t_pro': five_t_pro,
        'five_t_content': five_t_content,
        'fh': fh,
        'ft': ft,
        'gyqy': gyqy,
        'zdgf': zdgf,
        'ldaq': ldaq
    }


def calc_risk_type_data(info, pro, list1):
    """
    计算风险信息和问题
    :param info: 信息data
    :param pro: 问题data
    :param list1: 高动客问题 1：高铁 2：动车 3：客车
    :return:
    """
    # 检查次数
    count = len(info)
    all_content = []
    pro_count = []
    if list1 == []:
        data = pro[pro['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
        special_pro = data.drop_duplicates(subset=['DESCRIPTION'])
        pro_count.append(len(pro))
        for j, k in special_pro.iterrows():
            dic = {
                'station': k['NAME'],
                'level': k['LEVEL'],
                'person': k['CHECK_PERSON_NAME'],
                'content': k['DESCRIPTION']
            }
            all_content.append(dic)
    elif list1[0] == ['劳安']:
        data = pro[pro['LEVEL'].isin(['A', 'B', 'F1', 'F2'])]
        special_pro = data.drop_duplicates(subset=['DESCRIPTION'])
        pro_count.append(len(pro))
        for j, k in special_pro.iterrows():
            dic = {
                'station': k['NAME'],
                'level': k['LEVEL'],
                'person': k['CHECK_PERSON_NAME'],
                'content': k['DESCRIPTION']
            }
            all_content.append(dic)
    else:
        for i in list1:
            datas = pro[pro['PROBLEM_DIVIDE_IDS'].str.contains(str(i))]
            data = datas[datas['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
            special_pro = data.drop_duplicates(subset=['DESCRIPTION'])
            pro_count.append(len(datas))
            for j, k in special_pro.iterrows():
                dic = {
                    'station': k['NAME'],
                    'level': k['LEVEL'],
                    'person': k['CHECK_PERSON_NAME'],
                    'content': k['DESCRIPTION']
                }
                all_content.append(dic)
    return {
        'count': count,
        'all_content': all_content,
        'pro_count': pro_count
    }
