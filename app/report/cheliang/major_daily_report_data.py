import threading

import pandas as pd
import numpy as np
from datetime import datetime as dt
from app.data.util import pd_query
from app.report.cheliang.major_daily_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find
from app.report.analysis_report_manager import DailyAnalysisReport


LOCAL_DATA = threading.local()
MAJOR_DPID = '1ACE7D1C80B44456E0539106C00A2E70KSC'
CHELIANGBU_DPID = '19B8C3534E3F5665E0539106C00A58FD'

# 作业
ZUOYE_LEVEL = ['A', 'B', 'C', 'D']
# 设备
SHEBEI_LEVEL = ['E1', 'E2', 'E3', 'E4']
# 管理
GUANLI_LEVEL = ['F1', 'F2', 'F3', 'F4']


def get_data(year, month, day):
    report_date = dt(year, month, day)
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    times = (params[0][0], params[0][1])
    LOCAL_DATA.times = times
    if dt.strptime(times[1][:10], '%Y-%m-%d') >= dt.strptime(times[1][:4]+'-12-25', '%Y-%m-%d'):
        LOCAL_DATA.yearly_start_date = times[1][:4]+'1225'
    else:
        LOCAL_DATA.yearly_start_date = str(int(times[1].split('-')[0]) - 1) + '-12-25'
    _get_local_data()
    first = get_first(times, month, day)
    second = get_second()
    third = get_third()
    return {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "MAJOR",
        'major': '车辆',
        "file_name": '{0}车辆系统安全分析日报.xlsx',
        "created_at": dt.now(),
        'first': first,
        'second': second,
        'third': third
    }


def _get_local_data():
    check_problem = pd_query(check_problem_sql.format(
        LOCAL_DATA.times[0], LOCAL_DATA.times[1]))
    check_problem = check_problem[
        check_problem['RESP_TYPE2'] == MAJOR_DPID
    ]
    LOCAL_DATA.check_problem = check_problem
    LOCAL_DATA.zhanduan_data = pd_query(ZHANDUAN_DPID_SQL)


def get_first(times, month, day):
    # 安全情况
    first_one = get_first_one(times, month, day)
    first_two = get_first_two()
    first_three = get_first_three()
    first_four = get_first_four()
    return {
        'first_one': first_one,
        'first_two': first_two,
        'first_three': first_three,
        'first_four': first_four
    }


def get_first_one(times, month, day):
    """
    获取安全情况内容
    """
    infos = []
    start_date = int(times[0].split(' ')[0].replace('-', ''))
    end_date = int(times[1].split(' ')[0].replace('-', ''))
    # 获取安全责任数据
    documents = calc_safety_produce_data(start_date, end_date)
    # 获取截止本日的安全责任数据(qunia
    last_start_date = int(LOCAL_DATA.yearly_start_date.split(' ')[0].replace('-', ''))
    last_documents = calc_safety_produce_data(int(last_start_date), end_date)
    last_data = pd.DataFrame(last_documents)
    last_data = last_data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
    last_acc = len(last_data[last_data['MAIN_CLASS'] == '事故'])
    last_fault = len(last_data[last_data['MAIN_CLASS'] == '故障'])
    # 行车、劳动、路外
    last_safety_list = []
    for i in ['行车', '劳动', '路外']:
        new_data = last_data[last_data['RISK_NAME'].str.contains(i)]
        dic = {
            '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
        }
        last_safety_list.append(dic)
    # 设备故障
    new_data = last_data[last_data['MAIN_CLASS'] == '故障']
    last_safety_list.append({
        '设备故障': last_fault,
        '货车故障': len(new_data[new_data['NAME'].str.contains('货车故障')]),
        '客车故障': len(new_data[new_data['NAME'].str.contains('客车故障')]),
        '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
    })
    if len(documents) == 0:
        content = f"{month}月{day}日，当日发生定责事故0件、定责故障0件。(截止今日发生定责事故{last_acc}件、定责故障{last_fault}件)" + \
                  f"\n1.行车安全：当日未发生事故。(截止今日发生{last_safety_list[-4]['行车']}件)" +\
                  f"\n2.劳动安全：当日未发生事故。(截止今日发生{last_safety_list[-3]['劳动']}件)" +\
                  f"\n3.路外安全：当日未发生相撞事故。(截止今日发生{last_safety_list[-2]['路外']}件)" +\
                  f"\n4.当日设备故障0件。其中货车故障0件、客车故障0件、动车组故障0件。(" + \
                  f"截止今日设备故障{last_safety_list[-1]['设备故障']}件。其中货车故障{last_safety_list[-1]['货车故障']}件、" +\
                  f"客车故障{last_safety_list[-1]['客车故障']}件、动车组故障{last_safety_list[-1]['动车组故障']}件)"
        info = {
            'acc': '无',
            'fault': '无'
        }
        infos.append(info)
    else:
        data = pd.DataFrame(documents)
        data = data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
        acc = len(data[data['MAIN_CLASS'] == '事故'])
        fault = len(data[data['MAIN_CLASS'] == '故障'])
        # 行车、劳动、路外
        safety_list = []
        for i in ['行车', '劳动', '路外']:
            new_data = data[data['RISK_NAME'].str.contains(i)]
            dic = {
                '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
            }
            safety_list.append(dic)
        # 设备故障
        new_data = data[data['MAIN_CLASS'] == '故障']
        safety_list.append({
            '设备故障': fault,
            '客车故障': len(new_data[new_data['NAME'].str.contains('客车故障')]),
            '货车故障': len(new_data[new_data['NAME'].str.contains('货车故障')]),
            '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
        })
        content = f"{month}月{day}日，发生事故{acc}件、故障{fault}件。(截止今日发生定责事故{last_acc}件、定责故障{last_fault}件)" +\
                  f"\n1.行车安全：发生事故{safety_list[-4]['行车']}件。" +\
                  f"\n2.劳动安全：发生事故{safety_list[-3]['劳动']}件。" +\
                  f"\n3.路外安全：发生相撞事故{safety_list[-2]['路外']}件。" +\
                  f"\n4.设备故障{safety_list[-1]['设备故障']}件。其中货车故障{safety_list[-1]['货车故障']}件、" +\
                  f"客车故障{safety_list[-1]['客车故障']}件、动车组故障{safety_list[-1]['动车组故障']}件。(" + \
                  f"截止今日设备故障{last_safety_list[-1]['设备故障']}件。其中货车故障{last_safety_list[-1]['货车故障']}件、" +\
                  f"客车故障{last_safety_list[-1]['客车故障']}件、动车组故障{last_safety_list[-1]['动车组故障']}件)"
    return content


def calc_safety_produce_data(now_date, end_date):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param now_date:
    :param end_date:
    :return:
    """
    keys = {
        "match": {
            "DATE": {
                '$lte': end_date,
                '$gte': now_date
            },
            "PROFESSION": '车辆'
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    if len(documents) == 0:
        documents = [{
            "NAME": '',
            'REASON': '',
            'RISK_NAME': '',
            "MAIN_CLASS": '',
        }]
    return documents


def get_first_two():
    """
    总体检查基本情况
    :return:
    """
    # 检查基本情况
    check_info = LOCAL_DATA.check_problem.drop_duplicates(subset=['FK_CHECK_INFO_ID','ID_CARD'])
    check_problem = LOCAL_DATA.check_problem.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'])
    zhanduan_dic = LOCAL_DATA.zhanduan_data[['DEPARTMENT_ID', 'NAME']]
    zhanduan_dic = dict(zip(zhanduan_dic['DEPARTMENT_ID'], zhanduan_dic['NAME']))
    zhanduan_dic.update({CHELIANGBU_DPID: "车辆部"})
    # 站段发现问题排行
    type3_problem = check_problem[check_problem['TYPE3'].isin(list(zhanduan_dic.keys()))]
    type3_problem = type3_problem['TYPE3'].value_counts()
    _serious_problem = check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F2', 'F1', 'E1'])]
    base_check_situation = f"车辆系统下现场检查{check_info[check_info['TYPE2'] == MAJOR_DPID ].shape[0]}人次，" \
                           f"发现问题{check_problem[check_problem['TYPE2'] == MAJOR_DPID].shape[0]}个。" \
                           f"其中检查问题最多的是{zhanduan_dic[str(type3_problem.index[0])]}, {str(type3_problem.values[0])}个、" \
                           f"检查问题最少的是{zhanduan_dic[str(type3_problem.index[-1])]}, {str(type3_problem.values[-1])}个。\n" \
                           f"性质严重问题{_serious_problem[(_serious_problem['TYPE2'] == MAJOR_DPID)].shape[0]}个，" \
                           f"其中作业项{_serious_problem[(_serious_problem['TYPE2'] == MAJOR_DPID)& (_serious_problem['LEVEL'].isin(['A', 'B']))].shape[0]}个、" \
                           f"设备设施项{_serious_problem[(_serious_problem['TYPE2'] == MAJOR_DPID)& (_serious_problem['LEVEL'].isin(['E1']))].shape[0]}个、" \
                           f"管理项{_serious_problem[(_serious_problem['TYPE2'] == MAJOR_DPID)& (_serious_problem['LEVEL'].isin(['F2', 'F1']))].shape[0]}个。"

    # 非车辆系统检查发现问题
    no_cheliang_check_problem = check_problem[
        (check_problem['TYPE2'] != MAJOR_DPID) &
        ('车辆分析室'not in check_problem['DEPARTMENT_ALL_NAME']) &
        (check_problem['RESP_TYPE2'] == MAJOR_DPID)
    ]
    if not no_cheliang_check_problem.empty:
        no_cheliang_check_problem = [
            f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
            for _, row in no_cheliang_check_problem.iterrows()
        ]
        no_cheliang_check_problem = '\n'.join(no_cheliang_check_problem)
    else:
        no_cheliang_check_problem = '无'

    # 车辆部发现问题
    # cheliangbu_check_problem = [
    #     f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
    #     if row['TYPE3'] == CHELIANGBU_DPID
    #     else ''
    #     for _, row in check_problem.iterrows()
    # ]
    # cheliangbu_check_problem = '\n'.join(cheliangbu_check_problem)

    cheliangbu_check_problem = check_problem[
        check_problem['TYPE3'] == CHELIANGBU_DPID
        ]
    if not cheliangbu_check_problem.empty:
        cheliangbu_check_problem = [
            f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
            for _, row in cheliangbu_check_problem.iterrows()
        ]
        cheliangbu_check_problem = '\n'.join(cheliangbu_check_problem)
    else:
        cheliangbu_check_problem = '无'

    # 站段查处典型问题
    # zhanduan_check_seriouse_problem = [
    #     f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
    #     if row['TYPE3'] in list(zhanduan_dic.keys())
    #        and row['LEVEL'] in ['A', 'B', 'F2', 'F1', 'E1']
    #     else ''
    #     for _, row in check_problem.iterrows()
    # ]
    # zhanduan_check_seriouse_problem = '\n'.join(zhanduan_check_seriouse_problem)
    zhanduan_check_seriouse_problem = check_problem[
        (check_problem['TYPE3'].isin(list(zhanduan_dic.keys()))) &
        (check_problem['TYPE3'] != CHELIANGBU_DPID) &
        (check_problem['LEVEL'].isin(['A', 'B', 'F2', 'F1', 'E1']))
        ]
    if not zhanduan_check_seriouse_problem.empty:
        zhanduan_check_seriouse_problem = [
            f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
            for _, row in zhanduan_check_seriouse_problem.iterrows()
        ]
        zhanduan_check_seriouse_problem = '\n'.join(zhanduan_check_seriouse_problem)
    else:
        zhanduan_check_seriouse_problem = '无'

    # 车辆分析室检查情况
    analysis_center_data = pd_query(analysis_center_sql.format(LOCAL_DATA.times[0], LOCAL_DATA.times[1]))
    analysis_center_count = []
    for i in range(2, 8):
        data = analysis_center_data[analysis_center_data['FLAG'] == i]
        analysis_center_count.append(len(data))
    # 问题查处情况
    analysis_center_count.append(
        int(analysis_center_data[analysis_center_data['FLAG'] == 2]['DAILY_PROBLEM_NUMBER'].sum()))
    analysis_center_count.append(
        int(analysis_center_data[analysis_center_data['FLAG'].isin([5, 6])]['DAILY_PROBLEM_NUMBER'].sum()))
    # 查出典型突出问题
    # analysis_center_check_seriouse_problem = [
    #     f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
    #     if '车辆分析室' in row['DEPARTMENT_ALL_NAME'] and row['TYPE2'] == MAJOR_DPID
    #        and row['LEVEL'] in ['A', 'B', 'F2', 'F1', 'E1']
    #     else ''
    #     for _, row in check_problem.iterrows()
    # ]
    # analysis_center_check_seriouse_problem = '\n'.join(analysis_center_check_seriouse_problem)
    analysis_center_check_seriouse_problem = check_problem[
        (check_problem['TYPE2'] == MAJOR_DPID) &
        (check_problem['DEPARTMENT_ALL_NAME'].str.contains('车辆分析室')) &
        (check_problem['LEVEL'].isin(['A', 'B', 'F2', 'F1', 'E1']))
        ]
    if not analysis_center_check_seriouse_problem.empty:
        analysis_center_check_seriouse_problem = [
            f"部门: {row['DEPARTMENT_ALL_NAME']} 发现问题: {row['DESCRIPTION']}"
            for _, row in analysis_center_check_seriouse_problem.iterrows()
        ]
        analysis_center_check_seriouse_problem = '\n'.join(analysis_center_check_seriouse_problem)
    else:
        analysis_center_check_seriouse_problem = '无'

    # 履职评价
    analysis_center_evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(LOCAL_DATA.times[0], LOCAL_DATA.times[1]))
    # analysis_center_evaluate_content = [
    #     f"部门: {row['ALL_NAME']} 履职评价内容: {row['EVALUATE_CONTENT']}"
    #     if '车辆分析室' in row['ALL_NAME']
    #     else ''
    #     for _, row in analysis_center_evaluate_data.iterrows()
    # ]
    # analysis_center_evaluate_content = '\n'.join(analysis_center_evaluate_content)
    analysis_center_evaluate_data = analysis_center_evaluate_data[
        analysis_center_evaluate_data['ALL_NAME'].str.contains('车辆分析室')
        ]
    if not analysis_center_evaluate_data.empty:
        analysis_center_evaluate_data = [
            f"部门: {row['ALL_NAME']} 履职评价内容: {row['EVALUATE_CONTENT']}"
            for _, row in analysis_center_evaluate_data.iterrows()
        ]
        analysis_center_evaluate_data = '\n'.join(analysis_center_evaluate_data)
    else:
        analysis_center_evaluate_data = '无'

    analysis_center_check_situation = {
        'one': '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，'
               '干部履职评价{1}条，干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，'
               '阶段评价{5}人次。 问题查处情况：音视频调阅（复查）发现问题{6}个，'
               '干部履职评价（复查）发现{7}个问题。'.format(analysis_center_count[0],
                                            analysis_center_count[1], analysis_center_count[2],
                                            analysis_center_count[3], analysis_center_count[4],
                                            analysis_center_count[5], analysis_center_count[6],
                                            analysis_center_count[7]),
        'two': analysis_center_check_seriouse_problem,
        'three': analysis_center_evaluate_data,
    }

    return {
        'base_check_situation': base_check_situation,
        'no_cheliang_check_problem': no_cheliang_check_problem,
        'cheliangbu_check_problem': cheliangbu_check_problem,
        'zhanduan_check_seriouse_problem': zhanduan_check_seriouse_problem,
        'analysis_center_check_situation': analysis_center_check_situation
    }


def get_first_three():
    """
    车辆系统监督检查基本情况
    """
    zhanduan_data = LOCAL_DATA.zhanduan_data[['DEPARTMENT_ID', 'NAME']]
    check_info = LOCAL_DATA.check_problem.drop_duplicates(subset=['FK_CHECK_INFO_ID', 'ID_CARD'])
    check_problem = LOCAL_DATA.check_problem.drop_duplicates(subset=['FK_CHECK_PROBLEM_ID'])
    _serious_problem = check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F2', 'F1', 'E1'])]
    zhanduan_data = zhanduan_data.append([{"DEPARTMENT_ID": CHELIANGBU_DPID, "NAME": "车辆部"}], ignore_index=True)
    risk_data = pd_query(risk_sql)
    cheliang_system_check_base_situation = dict()
    for _, row in zhanduan_data.iterrows():
        base_check_situation = f"车辆系统下现场检查{check_info[check_info['TYPE3'] == row['DEPARTMENT_ID']].shape[0]}人次，" \
                               f"发现问题{check_problem[check_problem['TYPE3'] == row['DEPARTMENT_ID']].shape[0]}个。" \
                               f"性质严重问题{_serious_problem[(_serious_problem['TYPE3'] == row['DEPARTMENT_ID'])].shape[0]}个，" \
                               f"其中作业项{_serious_problem[(_serious_problem['TYPE3'] == row['DEPARTMENT_ID']) & (_serious_problem['LEVEL'].isin(['A', 'B']))].shape[0]}个、" \
                               f"设备设施项{_serious_problem[(_serious_problem['TYPE3'] == row['DEPARTMENT_ID']) & (_serious_problem['LEVEL'].isin(['E1']))].shape[0]}个、" \
                               f"管理项{_serious_problem[(_serious_problem['TYPE3'] == row['DEPARTMENT_ID']) & (_serious_problem['LEVEL'].isin(['F2', 'F1']))].shape[0]}个。"
        # 重点风险管控
        total_risk = risk_data['NAME'].values.tolist()
        total_check_risk = check_info[check_info['TYPE3'] == row['DEPARTMENT_ID']]['RISK_NAME'].values.tolist()
        check_risk = []
        # 过滤大类风险
        for _risk in total_check_risk:
            if not _risk:
                continue
            _risk_list = _risk.split(',') if ',' in _risk else [_risk]
            for item in _risk_list:
                if item:
                    check_risk.append(item.split('-')[1])
        check_risk = set(check_risk)
        no_check_risk = set(total_risk) - check_risk
        important_risk_check_situation = f"今日检查风险{len(check_risk)}大类，" \
                                         f"本月还未检查的风险有:{','.join(list(no_check_risk))[:-1]}。"
        cheliang_system_check_base_situation[row['NAME']] = {
            'base_check_situation': base_check_situation,
            'important_risk_check_situation': important_risk_check_situation
        }
    return cheliang_system_check_base_situation


def get_first_four():
    """
    站段评价情况
    """
    # 履职评价
    analysis_center_evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(LOCAL_DATA.times[0], LOCAL_DATA.times[1]))
    zhanduan_data = LOCAL_DATA.zhanduan_data[['DEPARTMENT_ID', 'NAME']]
    zhanduan_dpids = zhanduan_data['DEPARTMENT_ID'].values.tolist()
    # analysis_center_evaluate_content = [
    #     f"部门: {row['ALL_NAME']} 履职评价内容: {row['EVALUATE_CONTENT']}"
    #     if row['TYPE3'] in zhanduan_dpids
    #     else ''
    #     for _, row in analysis_center_evaluate_data.iterrows()
    # ]
    # analysis_center_evaluate_content = '\n'.join(analysis_center_evaluate_content)
    analysis_center_evaluate_data = analysis_center_evaluate_data[
        analysis_center_evaluate_data['TYPE3'].isin(zhanduan_dpids)
    ]
    if not analysis_center_evaluate_data.empty:
        analysis_center_evaluate_data = [
            f"部门: {row['ALL_NAME']} 履职评价内容: {row['EVALUATE_CONTENT']}"
            for _, row in analysis_center_evaluate_data.iterrows()
        ]
        analysis_center_evaluate_data = '\n'.join(analysis_center_evaluate_data)
    else:
        analysis_center_evaluate_data = '无'
    return {
        'zhanduan_evaluate_content': analysis_center_evaluate_data
    }


def get_second():
    """
    安全预警
    """
    last_start_time = LOCAL_DATA.yearly_start_date
    warning_data = pd_query(warning_notification_sql.format(last_start_time,
                                                            LOCAL_DATA.times[1]))
    warning_data = warning_data[
        warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7])
    ]
    return {
        'luju': '\n'.join(warning_data[warning_data['HIERARCHY'] == 1]['CONTENT'].values.tolist()),
        'major': '\n'.join(warning_data[warning_data['HIERARCHY'] == 2]['CONTENT'].values.tolist()),
        'zhuanduan': '\n'.join(warning_data[warning_data['HIERARCHY'] == 3]['CONTENT'].values.tolist()),
    }


def get_third():
    """
    安全问题通知书
    """
    last_start_time = LOCAL_DATA.yearly_start_date
    supervise_command_data = pd_query(SUPERVISE_COMMAND_SQL.format(last_start_time, LOCAL_DATA.times[1]))
    supervise_command_data = supervise_command_data[
        supervise_command_data['STATUS'].isin([2, 3, 4, 5, 6, 7])
    ].reset_index()
    supervise_notification_data = pd_query(SUPERVISE_NOTIFICATION_SQL.format(last_start_time, LOCAL_DATA.times[1]))
    supervise_notification_data = supervise_notification_data[
        supervise_notification_data['STATUS'].isin([2, 3, 4, 5, 6, 7])
    ].reset_index()
    return {
        # 安全监察指令书
        'supervise_command': '\n'.join([
            f"{idx+1}.问题描述: {row['PROBLEM_DESCRIPTION']} 整改意见: {row['RECTIFY_OPINIONS']}"
            if row
            else ''
            for idx, row in supervise_command_data.iterrows()
        ]),
        'supervise_notification': '\n'.join([
            f"{idx+1}.问题描述: {row['PROBLEM_DESCRIPTION']} 整改意见: {row['RECTIFY_OPINIONS']}"
            if row
            else ''
            for idx, row in supervise_notification_data.iterrows()
        ]),
    }

#
# def get_second_one(check_info, check_problem, worksheet):
#     check_problem = check_problem.dropna(subset=['PROBLEM_CLASSITY_NAME'])
#     dic = {}
#     # 检查基本情况
#     xc = len(check_info[check_info['CHECK_WAY'] == 1])
#     find_problem = int(check_info['PROBLEM_NUMBER'].sum())
#     main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
#     work_point = []
#     # 问题三项
#     for i in ['作业', '设备', '管理']:
#         data = check_problem[check_problem['PROBLEM_CLASSITY_NAME'].str.contains(i)]
#         work_point.append(len(data))
#     # 红线问题
#     luju_red = len(check_problem[check_problem['IS_RED_LINE'] == 1])
#     station_red = len(check_problem[check_problem['IS_RED_LINE'] == 2])
#     dic['one'] = '车辆系统下现场检查{0}人次，发现问题{1}个。性质严重问题{2}个，其中作业项{3}个、设备设施项{4}个、管理项{5}个。'. \
#         format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2])
#     # 路局查出红线问题
#     dic['two'] = '路局查处红线问题{0}个、站段查处红线问题{1}个。'.format(luju_red, station_red)
#     dic['three'] = '无数据'
#     worksheet['C6'] = dic['one']
#     worksheet['C7'] = dic['two']
#     worksheet['C8'] = dic['three']
#     return dic
#
#
# def get_second_two(safety_info, safety_problem, mv_data, check_risk, month_risk, risk_data, worksheet):
#     """
#     安监室监督检查基本情况
#     :param safety_info:
#     :param safety_problem:
#     :param mv_data:
#     :param check_risk:
#     :param month_risk:
#     :param risk_data:
#     :param worksheet:
#     :return:
#     """
#     contents = []
#     index = 9
#     for i in safety_info['STATION'].unique():
#         check_problem = safety_problem[safety_problem['STATION'].str.contains(i)]
#         check_info = safety_info[safety_info['STATION'].str.contains(i)]
#         check_problem = check_problem.dropna(subset=['PROBLEM_CLASSITY_NAME'])
#         check_risk = check_risk[check_risk['STATION'].str.contains(i)]
#         month_check_risk = month_risk[month_risk['STATION'].str.contains(i)]
#         risk_datas = risk_data[risk_data['ALL_NAME'].str.contains(i)]
#         check_mv = mv_data[mv_data['STATION'].str.contains(i)]
#         content = {}
#         # 检查基本情况
#         xc = len(check_info[check_info['CHECK_WAY'] == 1])
#         find_problem = int(check_info['PROBLEM_NUMBER'].sum())
#         main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
#         # 音视频调阅问题
#         mv = check_mv[check_mv['TYPE'] == 0]
#         mv_count = len(mv)
#         cost_time = int(mv['COST_TIME'].sum())
#         mv_pro = int(mv['PROBLEM_NUMBER'].sum())
#         # 问题三项
#         work_point = []
#         for j in ['作业', '设备', '管理']:
#             data = check_problem[check_problem['PROBLEM_CLASSITY_NAME'].str.contains(j)]
#             work_point.append(len(data))
#         content['name'] = i
#         content['one'] = '下现场检查{0}人次，发现问题{1}个；音视频调阅{6}人次，时长{7}小时，发现问题{8}个。' \
#                          '性质严重问题{2}个，其中作业项{3}个、设备设施项{4}个、管理项{5}个。'. \
#             format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2], mv_count, cost_time,
#                    mv_pro)
#         # 重点风险查处情况
#         risk = check_risk.dropna(subset=['NAME'])['NAME'].unique().tolist()
#         months_risk = month_check_risk.dropna(subset=['NAME'])['NAME'].unique().tolist()
#         risk_names = risk_datas.dropna(subset=['NAME'])['NAME'].unique().tolist()
#         not_risks = [j for j in risk_names if j not in months_risk]
#         number = len(risk)
#         if number == 0:
#             content['two'] = '今日检查风险{0}大类。本月还未检查的风险有:{1}'.format(number, ','.join(risk_names))
#         else:
#             content['two'] = '今日检查风险{0}大类，分别是：{1},本月还未检查的风险有：{2}。'.format(number, ','.join(risk), ','.join(not_risks))
#         worksheet['B{0}'.format(index)] = content['name']
#         worksheet['D{0}'.format(index)] = content['one']
#         worksheet['D{0}'.format(index + 1)] = content['two']
#         contents.append(content)
#         index += 2
#     return contents
#
#
# def get_second_three(analysis_center, worksheet):
#     count = []
#     dic = {}
#     # 工作量情况
#     for i in range(2, 8):
#         data = analysis_center[analysis_center['FLAG'] == i]
#         count.append(len(data))
#     # 问题查处情况
#     mv_re = int(analysis_center[analysis_center['FLAG'] == 2]['DAILY_PROBLEM_NUMBER'].sum())
#     eva_re = int(analysis_center[analysis_center['FLAG'].isin([5, 6])]['DAILY_PROBLEM_NUMBER'].sum())
#     dic['one'] = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，' \
#                  '干部履职评价{1}条，干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，' \
#                  '阶段评价{5}人次。 问题查处情况：音视频调阅（复查）发现问题{6}个，' \
#                  '干部履职评价（复查）发现{7}个问题。'.format(count[0], count[1], count[2], count[3],
#                                               count[4], count[5], mv_re, eva_re)
#     # 安全问题查处
#     problem_data = analysis_center.replace('', np.nan).replace('无', np.nan).dropna(subset=['RISK_WARNING'])
#     problem_count = len(problem_data['RISK_WARNING'].unique())
#     situation = []
#     for i in range(problem_count):
#         situation.append(str(i + 1) + '.' + problem_data['RISK_WARNING'].unique().tolist()[i])
#     situation = '\n'.join(situation)
#     if problem_count == 0:
#         dic['two'] = '安全问题查处{0}个,无重大风险问题'.format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()))
#     else:
#         dic['two'] = "安全问题查处{0}个，其中典型问题有{1}个：". \
#                          format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()), problem_count) + situation
#     dic['three'] = '无数据'
#     worksheet['C25'] = dic['one']
#     worksheet['C26'] = dic['two']
#     worksheet['C27'] = dic['three']
#     return dic
#
#
# def get_four(times, work_sheet):
#     """
#     风险预警
#     :param times:
#     :param work_sheet:
#     :return:
#     """
#     rank_map = {
#         1: "I级安全事故警告预警",
#         2: "II级差异化精准警告预警",
#         3: "III级劳动安全专项警告预警",
#         4: "IV其他警告预警"
#     }
#     warning_data = pd_query(warning_notification_sql.format(times[0], times[1]))
#     # 总公司
#     # 路局
#     road_data = warning_data[warning_data['HIERARCHY'] == 1].reset_index().head(3)
#     company_warn_list = []
#     if len(road_data) == 0:
#         company_warn_list = ['无']
#     else:
#         for index in range(0, len(road_data)):
#             company_warn_list.append("""{0}.{1}日对{2}开展{3}""".format(
#                 index + 1,
#                 times[0],
#                 road_data["duty_department_name"][index],
#                 rank_map.get(road_data["RANK"][index])
#             ))
#     work_sheet['B29'] = '\n'.join(company_warn_list)
#     # 专业
#     safety_data = warning_data[warning_data['HIERARCHY'] == 2].reset_index().head(3)
#     safety_warn_list = []
#     if len(safety_data) == 0:
#         safety_warn_list = ['无']
#     else:
#         for index in range(0, len(safety_data)):
#             safety_warn_list.append("""{0}.{1}日对{2}开展{3}""".format(
#                 index + 1,
#                 times[0],
#                 safety_data["duty_department_name"][index],
#                 rank_map.get(safety_data["RANK"][index])
#             ))
#     work_sheet['B30'] = '\n'.join(safety_warn_list)
#     # 路局
#     station_data = warning_data[warning_data['HIERARCHY'] == 3].reset_index().head(3)
#     station_warn_list = []
#     if len(safety_data) == 0:
#         station_warn_list = ['无']
#     else:
#         for index in range(0, len(safety_data)):
#             station_warn_list.append("""{0}.{1}日对{2}开展{3}""".format(
#                 index + 1,
#                 times[0],
#                 safety_data["duty_department_name"][index],
#                 rank_map.get(station_data["RANK"][index])
#             ))
#     work_sheet['B31'] = '\n'.join(station_warn_list)
#     return {
#         'station_warn_list': '\n'.join(station_warn_list),
#         'safety_warn_list': '\n'.join(safety_warn_list),
#         'company_warn_list': '\n'.join(company_warn_list)
#     }
