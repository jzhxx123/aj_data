#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   analysisWeeklyReport_cheliang
Description:
Author:    yuchuang
date:         2019-07-10
-------------------------------------------------
Change Activity:2019-07-10 14:54
-------------------------------------------------
"""

from app.report.cheliang.major_weekly_report_data import get_data
from app.report.analysis_report_manager import WeeklyAnalysisReport, restore_numpy_value
from app import mongo





class CheliangMajorWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    电务的专业级的周分析报告类。
    """

    def __init__(self):
        super(CheliangMajorWeeklyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='车辆')

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date)
        restore_numpy_value(data)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data
