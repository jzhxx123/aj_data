from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta

from app.data.util import pd_query
from app.report.cheliang.station_weekly_report_sql import *


def get_data(start_date, end_date, dp_id):
    start_date = dt.strptime(start_date, '%Y-%m-%d')
    end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = pd_query("""select ALL_NAME from t_department where DEPARTMENT_ID = '{0}'""".format(dp_id)).iloc[0][
        'ALL_NAME']
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date, dp_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_info = pd_query(check_info_sql.format(last_week_start, last_week_end, dp_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 检查问题
    problem_data = pd_query(check_problem_sql.format(start_date, end_date, dp_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_week_start, last_week_end, dp_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 检查风险情况
    risk_data = pd_query(check_risk_sql.format(start_date, end_date, dp_id))
    # 履职信息
    eva_data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date, dp_id))
    # 车间信息
    shop_data = pd_query(SHOP_SQL.format(dp_id))
    # 安全生产信息
    safety_produce = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date, dp_id))
    # 检查通知书
    check_notification = pd_query(check_notification_sql.format(start_date, end_date, dp_id))
    # 安全表扬通知书
    praise = pd_query(praise_notice_sql.format(start_date, end_date, dp_id))
    # 站段人数
    zd_data = pd_query(ZHANDUAN_PERSON_SQL.format(dp_id))

    first_data = get_first(info_data, last_info, problem_data, last_problem, risk_data)
    first_two = get_first_two(info_data, problem_data, shop_data)
    second_data = get_second(eva_data)
    third_data = get_third(safety_produce, check_notification, praise)
    third_two = get_third_two(zd_data, info_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "hierarchy": "STATION",
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        'major': '车辆',
        'station_id': dp_id,
        'station_name': station_name,
        'created_at': dt.now(),
        'file_name': file_name,
        "first": first_data,
        "first_two": first_two,
        "second": second_data,
        "third": third_data,
        'third_two': third_two
    }
    return result


def calc_ratio_or_diff(now_data, last_data):
    if last_data == 0:
        return {
            'diff': now_data,
            'percent': round(now_data * 100, 2)
        }
    else:
        return {
            'diff': now_data - last_data,
            'percent': round(ded_zero(now_data - last_data, last_data))
        }


def get_first(info_data, last_info, problem_data, last_problem, risk_datas):
    """
    基本情况
    :param info_data:
    :param last_info:
    :param problem_data:
    :param last_problem:
    :param risk_datas:
    :return:
    """
    # 检查次数
    check_count = len(info_data)
    ratio1 = calc_ratio_or_diff(check_count, len(last_info))
    # 问题个数
    pro_count = len(problem_data)
    ratio2 = calc_ratio_or_diff(pro_count, len(last_problem))
    # 严重问题
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
    ratio3 = calc_ratio_or_diff(main_pro,
                                len(last_problem[last_problem['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]))
    # 作业、管理、设备问题
    pros = []
    for i in ['作业', '管理', '设备']:
        pros.append(len(problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]))
    # 红线问题
    red_pro = len(problem_data[problem_data['IS_RED_LINE'].isin([1, 2])])
    # 5T问题
    problem_data = problem_data.dropna(subset=['PROBLEM_POINT'])
    five_t = len(problem_data[problem_data['PROBLEM_POINT'].str.contains('5T')])
    # 风险问题
    risk_count = []
    risk_datas = risk_datas.dropna(subset=['RISK_NAME'])
    for i in ['劳动安全', '车辆-行车设备质量-制动供风', '车辆-行车设备质量-供电', '车辆-行车设备质量-动客车上部服务设施质量']:
        new_data = risk_datas[risk_datas['RISK_NAME'].str.contains(i)]
        risk_count.append(len(new_data))
    return {
        'check_count': check_count,
        'ratio1': ratio1,
        'pro_count': pro_count,
        'ratio2': ratio2,
        'main_pro': main_pro,
        'ratio3': ratio3,
        'pros': pros,
        'red_pro': red_pro,
        'risk_count': risk_count,
        'five_t': five_t
    }


def get_first_two(info_data, problem_data, shop_data):
    """
    部门检查情况
    :param info_data:
    :param problem_data:
    :param shop_data:
    :return:
    """
    shop_name = shop_data['NAME'].tolist()
    all_shop = []
    for i in shop_name:
        info = info_data[info_data['NAME'] == i]
        pro = problem_data[problem_data['NAME'] == i]
        dic = {
            'name': i,
            # 检查次数
            'check_count': len(info),
            # 发现问题数
            'find_pro': len(pro),
            # 质量分
            'score': float(pro['CHECK_SCORE'].sum())
        }
        all_shop.append(dic)
    return {
        'all_shop': all_shop
    }


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_second(eva_data):
    # 扣分人数
    score_count = len(eva_data)
    # 重点工作落实不到位
    zdgz = len(eva_data[eva_data['CODE'].isin(['ZD-4', 'ZD-5', 'ZD-6', 'ZD-7', 'ZD-8'])])
    ratio1 = round(ded_zero(zdgz, score_count) * 100, 2)
    # 监督检查质量问题
    jdjc = len(eva_data[eva_data['CODE'].isin(['ZL-1', 'ZL-2', 'ZL-3', 'ZL-4', 'ZL-5', 'ZL-6', 'ZL-7'])])
    ratio2 = round(ded_zero(jdjc, score_count) * 100, 2)
    # 检查信息录入质量问题
    jcxx = len(eva_data[eva_data['CODE'].isin(['JL-2'])])
    ratio3 = round(ded_zero(jcxx, score_count) * 100, 2)
    # 安全谈心
    aqtx = len(eva_data[eva_data['CODE'].isin(['TX-1', 'TX-4'])])
    ratio4 = round(ded_zero(aqtx, score_count) * 100, 2)
    # 问题闭环管理问题
    wtbh = len(eva_data[eva_data['CODE'].isin(['ZG-5'])])
    ratio5 = round(ded_zero(wtbh, score_count) * 100, 2)
    # 表格
    all_count = []
    # 路局检查和站段检查
    for i in [1, 2]:
        count = []
        new_data = eva_data[eva_data['CHECK_TYPE'] == i]
        for j in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
            new_datas = new_data[new_data['GRADATION'] == j]
            count.append(int(new_datas['SCORE'].sum()))
        count.append(sum(count))
        all_count.append(count)
    return {
        'score_count': score_count,
        'zdgz': zdgz,
        'jdjc': jdjc,
        'jcxx': jcxx,
        'aqtx': aqtx,
        'wtbh': wtbh,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
        'ratio5': ratio5,
        'all_count': all_count
    }


def get_third(safety_produce, check_notification, praise):
    # 安全生产信息
    description = safety_produce['OVERVIEW'].unique().tolist()
    # 安全检查通知书
    notifications = []
    for i, j in check_notification.iterrows():
        dic = {
            'name': j['CHECK_PERSON_NAMES'],
            'dp': j['ALL_NAME'],
            'content': j['PROBLEM_DESCRIPTION']
        }
        notifications.append(dic)
    # 安全表扬通知书
    praises = []
    for i, j in praise.iterrows():
        dic = {
            'unit': j['PRAISE_DEPARTMENT_NAME'],
            'content': j['PRAISE_DEPARTMENT_CONTENT']
        }
        praises.append(dic)
    return {
        'description': description,
        'notifications': notifications,
        'praises': praises
    }


def get_third_two(zd_data, info_data):
    all_shops = []
    for i in zd_data['NAME'].tolist():
        # 车间人数
        zd_person = int(zd_data[zd_data['NAME'] == i]['COUNT'])
        info = info_data[info_data['NAME'] == i]
        all_risk = []
        for j in ['配件脱落', '火灾爆炸', '高压牵引', '制动供风', '劳动安全']:
            info = info.dropna(subset=['RISK_NAME'])
            data = info[info['RISK_NAME'].str.contains(j)]
            dic = {
                'count': len(data),
                'pro': int(data['PROBLEM_NUMBER'].sum(0)),
                'avg_count': round(ded_zero(len(data), zd_person), 2),
                'avg_pro': round(ded_zero(int(data['PROBLEM_NUMBER'].sum(0)), zd_person), 2)
            }
            all_risk.append(dic)
        # 调车防溜
        dc_data = info[info['CHECK_ITEM_NAMES'].str.contains('调车防溜')]
        all_risk.append({
            'count': len(dc_data),
            'pro': int(dc_data['PROBLEM_NUMBER'].sum()),
            'avg_count': round(ded_zero(len(dc_data), zd_person), 2),
            'avg_pro': round(ded_zero(int(dc_data['PROBLEM_NUMBER'].sum(0)), zd_person), 2)
        })
        all_shops.append({
            'name': i,
            'content': all_risk
        })
    return {
        'all_shops': all_shops
    }
