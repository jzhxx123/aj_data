
check_info_sql = """
SELECT a.CHECK_WAY, a.CHECK_TYPE,a.PROBLEM_NUMBER,d.NAME,e.`NAME` as STATION
FROM `t_check_info` a
LEFT JOIN t_check_info_and_person c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE3
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE2='1ACE7D1C80B44456E0539106C00A2E70KSC'
"""

check_problem_sql = """
SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    a.CHECK_ADDRESS_NAMES,
    a.CHECK_TYPE,
    a.CHECK_WAY,
    a.DEPARTMENT_ALL_NAME,
    a.RISK_NAME,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    b.IS_RED_LINE,
    b.DESCRIPTION,
    b.PROBLEM_CLASSITY_NAME,
    b.`LEVEL`,
    b.SERIOUS_VALUE,
    b.CHECK_ITEM_NAME,
    c.ID_CARD,
    d.NAME,
    d.DEPARTMENT_ID,
    d.TYPE2,
    d.TYPE3,
    e.`NAME` AS STATION,
    f.FK_DEPARTMENT_ID AS RESP_DEPARTMENT_ID,
    g.TYPE2 AS RESP_TYPE2
FROM
    `t_check_info` a
        LEFT JOIN
    t_check_problem b ON a.PK_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN
    t_check_info_and_person c ON c.FK_CHECK_INFO_ID = a.PK_ID
        LEFT JOIN
    t_department d ON d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
        LEFT JOIN
    t_department e ON e.DEPARTMENT_ID = d.TYPE3
        LEFT JOIN 
    t_check_problem_and_responsible_department as f on f.FK_CHECK_PROBLEM_ID = b.PK_ID
        LEFT JOIN 
    t_department as g on g.DEPARTMENT_ID = f.FK_DEPARTMENT_ID
WHERE
    a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
"""

check_risk_sql = """SELECT a.CHECK_WAY, a.CHECK_TYPE,a.PROBLEM_NUMBER,d.`NAME`,f.NAME as ALL_NAME,g.`NAME` as STATION
FROM `t_check_info` a
LEFT JOIN t_check_info_and_item b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_risk c on b.FK_CHECK_ITEM_ID=c.PK_ID
LEFT JOIN t_risk d on d.PK_ID = c.PARENT_ID
LEFT JOIN t_check_info_and_person e on e.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID=e.FK_DEPARTMENT_ID
LEFT JOIN t_department g on g.DEPARTMENT_ID = f.TYPE3
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and f.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""

analysis_center_sql = """SELECT a.*,b.FLAG,b.DAILY_PROBLEM_NUMBER FROM t_analysis_center_daily a
LEFT JOIN t_analysis_center_daily_details b on a.PK_ID=b.FK_ANALYSIS_CENTER_DAILY_ID
where a.DATE between '{0}' AND '{1}'
and a.PROFESSION_ID = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 音视频检查
mv_check_sql = """SELECT b.TYPE,b.COST_TIME,b.MONITOR_POST_NAMES,e.`NAME` as STATION,a.PROBLEM_NUMBER
from t_check_info a
LEFT JOIN t_check_info_and_media b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_info_and_monitor_device_info c on c.PK_ID=b.MONITOR_POST_IDS
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND d.TYPE2='1ACE7D1C80B44456E0539106C00A2E70KSC'"""


risk_sql = """
select a.NAME, b.NAME as ALL_NAME, a.FK_DEPARTMENT_ID
from t_risk a
left join t_department b on b.department_id=a.fk_department_id
where b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
and a.is_delete = 0
and a.PARENT_ID = 0
"""


warning_notification_sql = """SELECT
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						a.CONTENT,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.BELONG_PROFESSION_NAME
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')
					and b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
					"""

# 站段
ZHANDUAN_DPID_SQL = """SELECT
        a.DEPARTMENT_ID, a.NAME, b.NAME AS MAJOR
    FROM
        t_department AS a
            LEFT JOIN
        t_department AS b ON a.FK_PARENT_ID = b.DEPARTMENT_ID
    WHERE
        a.TYPE = 4
            AND a.IS_DELETE = 0
            AND b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
            AND a.SHORT_NAME != "";
"""

CHECK_EVALUATE_SQL = """
SELECT 
    DISTINCT a.EVALUATE_CONTENT, b.ALL_NAME, a.PK_ID, b.TYPE3
FROM
    t_check_evaluate_info AS a
        INNER JOIN
    t_department AS b ON a.CHECK_PERSON_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')
    AND b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""

# 安全监察指令书
SUPERVISE_COMMAND_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID, a.PROBLEM_DESCRIPTION, a.RECTIFY_OPINIONS, a.STATUS
FROM
    t_supervise_command as a
    inner join t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    SUBMIT_TIME BETWEEN Date('{0}') AND Date('{1}')
    and b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""

# 安全监察通知书
SUPERVISE_NOTIFICATION_SQL = """
SELECT 
    a.FK_DEPARTMENT_ID, a.PROBLEM_DESCRIPTION, a.RECTIFY_OPINIONS, a.STATUS
FROM
    t_supervise_notification as a
    inner join t_department as b on a.FK_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE
    SUBMIT_TIME BETWEEN Date('{0}') AND Date('{1}')
    and b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""