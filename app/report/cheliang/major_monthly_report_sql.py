# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY,c.CHECK_ITEM_NAMES
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 检查发现问题信息
check_problem_info_sql = """SELECT p.PK_ID,c.DEPARTMENT_ALL_NAME,c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,p.CHECK_PERSON_NAME,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.`STATUS`,p.PROBLEM_POINT,p.IS_RED_LINE,p.DESCRIPTION,p.RISK_NAMES,p.CHECK_ITEM_NAME,
p.PROBLEM_DIVIDE_IDS,p.TYPE
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department d on d.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where p.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""

# 履职工作量查询语句
check_evaluate_review_sql = """SELECT
a.*,c.`NAME`,d.ALL_NAME AS CHECK_DEPT_NAME,c.TYPE AS CHECK_DEPT_TYPE
FROM
t_check_evaluate_check_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.FK_RESPONSIBE_DEPARTMENT_ID
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
"""


# 履职复查
eva_review_sql = """SELECT
b.*
FROM
t_check_evaluate_review_person b
LEFT JOIN t_check_evaluate_check_person a on b.FK_CHECK_EVALUATE_CHECK_PERSON_ID=a.pk_id
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_REVIEW_DEPARTMENT_ID
WHERE
b.CREATE_TIME >= '{0}'
AND b.CREATE_TIME <= '{1} 23:59:59'
AND c.TYPE2='1ACE7D1C80B44456E0539106C00A2E70KSC'"""


# 量化信息完成情况
t_quanttization_sql = """SELECT
        a.ID_CARD, a.PERSON_NAME, dp.ALL_NAME, 
        a.CHECK_TIMES_TOTAL,
        a.CHECK_NOTIFICATION_TIMES_TOTAL,
        a.MONITOR_NUMBER_TOTAL,
        a.PROBLEM_NUMBER_TOTAL,
        a.WORK_PROBLEM_NUMBER_TOTAL,
        a.MONITOR_PROBLEM_NUMBER_TOTAL,
        a.HIDDEN_DANGER_RECHECK_TIMES_TOTAL,
        a.RISK_RECHECK_TIMES_TOTAL,
        a.IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL,
        a.MIN_QUALITY_GRADES_TOTAL,
        a.MANAGE_PROBLEM_NUMBER_TOTAL,
        a.DEVICE_PROBLEM_NUMBER_TOTAL,
        a.ENVIRONMENT_PROBLEM_NUMBER_TOTAL,
        a.OUT_WORK_PROBLEM_NUMBER_TOTAL,
        b.REALITY_NUMBER,
        b.CHECK_NOTIFICATION_REALITY_NUMBER,
        b.MEDIA_REALITY_TIME,
        b.REALITY_PROBLEM_NUMBER,
        b.REALITY_WORK_ITEM_PROBLEM_NUMBER,
        b.MEDIA_REALITY_PROBLEM_NUMBER,
        b.HIDDEN_DANGER_RECHECK_REALITY_NUMBER,
        b.RISK_RECHECK_REALITY_NUMBER,
        b.IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER,
        b.REALITY_MIN_QUALITY_GRADES,
        b.REALITY_MANAGE_PROBLEM_NUMBER,
        b.REALITY_DEVICE_PROBLEM_NUMBER,
        b.REALITY_ENVIRONMENT_PROBLEM_NUMBER,
        b.REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER,
        dp.TYPE3 AS STATION_ID, dp.TYPE4 AS SECTION_ID
    FROM t_quantization_base_quota a
        LEFT JOIN t_quantify_assess_real_time b ON a.ID_CARD = b.ID_CARD AND a.MONTH = b.MONTH AND a.YEAR = b.YEAR
        LEFT JOIN t_department dp ON a.FK_DEPARTMENT_ID = dp.DEPARTMENT_ID
    WHERE dp.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'
    AND a.YEAR = {0} AND a.MONTH = {1}"""
