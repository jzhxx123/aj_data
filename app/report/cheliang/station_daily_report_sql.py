without_accident_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
            WHERE a.OCCURRENCE_TIME <= '{0}'
            AND c.TYPE3='{1}'
        """


check_info_sql = """
SELECT a.CHECK_WAY, a.CHECK_TYPE,a.PROBLEM_NUMBER,d.NAME,e.`NAME` as WORKSHOP
FROM `t_check_info` a
LEFT JOIN t_check_info_and_person c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE4
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE3='{2}'"""

check_problem_sql = """SELECT b.IS_RED_LINE,b.PROBLEM_CLASSITY_NAME,b.`LEVEL`,b.SERIOUS_VALUE,
b.CHECK_ITEM_NAME,a.CHECK_ADDRESS_NAMES,a.CHECK_TYPE,d.NAME,e.`NAME` as WORKSHOP
FROM `t_check_info` a
LEFT JOIN t_check_problem b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_check_info_and_person c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = d.TYPE4
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND d.TYPE3 = '{2}'
"""

check_risk_sql = """SELECT a.CHECK_WAY, a.CHECK_TYPE,a.PROBLEM_NUMBER,d.`NAME`,f.NAME as ALL_NAME,g.`NAME` as WORKSHOP
FROM `t_check_info` a
LEFT JOIN t_check_info_and_item b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_risk c on b.FK_CHECK_ITEM_ID=c.PK_ID
LEFT JOIN t_risk d on d.PK_ID = c.PARENT_ID
LEFT JOIN t_check_info_and_person e on e.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department f on f.DEPARTMENT_ID=e.FK_DEPARTMENT_ID
LEFT JOIN t_department g on g.DEPARTMENT_ID = f.TYPE4
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and f.TYPE3 = '{2}'
"""

analysis_center_sql = """SELECT a.*,b.FLAG,b.DAILY_PROBLEM_NUMBER FROM t_analysis_center_daily a
LEFT JOIN t_analysis_center_daily_details b on a.PK_ID=b.FK_ANALYSIS_CENTER_DAILY_ID
where a.DATE between '{0}' AND '{1}'"""

safety_info_sql = """SELECT * FROM `t_safety_produce_info`
where CREATE_TIME BETWEEN '{0}' and '{1}' """

# 总岗位
all_position_sql = """SELECT a.POSITION,c.`NAME`
from t_person a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE4
WHERE b.TYPE3='{0}'"""

# 音视频检查
mv_check_sql = """SELECT b.TYPE,b.COST_TIME,b.MONITOR_POST_NAMES,e.`NAME` as WORKSHOP,a.PROBLEM_NUMBER
from t_check_info a
LEFT JOIN t_check_info_and_media b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_info_and_monitor_device_info c on c.PK_ID=b.MONITOR_POST_IDS
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE4
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND d.TYPE3='{2}'"""


main_info_sql = """select a.*,b.ALL_NAME FROM t_key_information_tracking a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_VEST_DEPARTMENT_ID
WHERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND b.TYPE3='{2}'"""


warning_notification_sql = """SELECT
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.BELONG_PROFESSION_NAME
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')
					and b.TYPE3 = '{2}'"""


risk_sql = """select a.NAME,b.NAME as ALL_NAME from t_risk a
left join t_department b on b.department_id=a.fk_department_id
where b.TYPE2 = '1ACE7D1C80B44456E0539106C00A2E70KSC'"""