# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY,c.CHECK_ITEM_NAMES,
c.IS_YECHA
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'"""


# 检查发现问题信息
check_problem_info_sql = """SELECT p.PK_ID,c.CHECK_WAY,p.PROBLEM_CLASSITY_NAME,b.NAME,p.CHECK_PERSON_NAME,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,d.`STATUS`,p.PROBLEM_POINT,p.IS_RED_LINE,p.DESCRIPTION,p.RISK_NAMES,p.CHECK_ITEM_NAME,
p.PROBLEM_DIVIDE_IDS,p.TYPE,p.IS_ASSESS
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_check_problem_and_responsible_department d on d.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE4
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE3 = '{2}'"""


# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS SHOP
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE4
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE3 = '{2}'
"""

# 履职复查信息查询语句
check_evaluate_review_sql = """SELECT
a.*,c.`NAME`
FROM
t_check_evaluate_review_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE4
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE3 = '{2}'
"""

# 量化信息完成情况
t_quanttization_sql = """SELECT
        a.ID_CARD, a.PERSON_NAME, dp.ALL_NAME, 
        a.CHECK_TIMES_TOTAL,
        a.CHECK_NOTIFICATION_TIMES_TOTAL,
        a.MONITOR_NUMBER_TOTAL,
        a.PROBLEM_NUMBER_TOTAL,
        a.WORK_PROBLEM_NUMBER_TOTAL,
        a.MONITOR_PROBLEM_NUMBER_TOTAL,
        a.HIDDEN_DANGER_RECHECK_TIMES_TOTAL,
        a.RISK_RECHECK_TIMES_TOTAL,
        a.IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL,
        a.MIN_QUALITY_GRADES_TOTAL,
        a.MANAGE_PROBLEM_NUMBER_TOTAL,
        a.DEVICE_PROBLEM_NUMBER_TOTAL,
        a.ENVIRONMENT_PROBLEM_NUMBER_TOTAL,
        a.OUT_WORK_PROBLEM_NUMBER_TOTAL,
        b.REALITY_NUMBER,
        b.CHECK_NOTIFICATION_REALITY_NUMBER,
        b.MEDIA_REALITY_TIME,
        b.REALITY_PROBLEM_NUMBER,
        b.REALITY_WORK_ITEM_PROBLEM_NUMBER,
        b.MEDIA_REALITY_PROBLEM_NUMBER,
        b.HIDDEN_DANGER_RECHECK_REALITY_NUMBER,
        b.RISK_RECHECK_REALITY_NUMBER,
        b.IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER,
        b.REALITY_MIN_QUALITY_GRADES,
        b.REALITY_MANAGE_PROBLEM_NUMBER,
        b.REALITY_DEVICE_PROBLEM_NUMBER,
        b.REALITY_ENVIRONMENT_PROBLEM_NUMBER,
        b.REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER,
        dp.TYPE3 AS STATION_ID, dp.TYPE4 AS SECTION_ID,
        sp.`NAME` AS SHOP
    FROM t_quantization_base_quota a
        LEFT JOIN t_quantify_assess_real_time b ON a.ID_CARD = b.ID_CARD AND a.MONTH = b.MONTH AND a.YEAR = b.YEAR
        LEFT JOIN t_department dp ON a.FK_DEPARTMENT_ID = dp.DEPARTMENT_ID
        LEFT JOIN t_department sp ON sp.DEPARTMENT_ID=dp.TYPE4
    WHERE dp.TYPE3 = '{2}'
    AND a.YEAR = {0} AND a.MONTH = {1}"""


# 音视频检查信息
CHECK_MV_COST_TIME_SQL = """SELECT 
        a.PK_ID,
        b.CHECK_PERSON_NAMES,
        b.PROBLEM_NUMBER,
        b.DEPARTMENT_ALL_NAME,
        a.COST_TIME,
        b.CHECK_ITEM_NAMES,
        b.CHECK_WAY,
        d.ALL_NAME,
        e.`NAME`,
        a.MONITOR_POST_NAMES
FROM
    t_check_info_and_media AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        LEFT JOIN
    t_check_info_and_person c on c.FK_CHECK_INFO_ID=b.PK_ID
        LEFT JOIN
    t_department d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
        LEFT JOIN 
    t_department e on e.DEPARTMENT_ID=d.TYPE4
WHERE
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
    AND d.TYPE3 = '{2}'
"""

# 车间人数信息
SHOP_PERSON_SQL = """SELECT count(b.`NAME`) as COUNT,b.`NAME`
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0
GROUP BY b.`NAME`"""


# 问题责任信息
DUTY_PERSON_SQL = """SELECT a.PK_ID,a.ID_CARD,a.PERSON_NAME,a.RESPONSIBILITY_SCORE,c.POSITION,d.ALL_NAME,d.TYPE,d.NAME
from t_check_problem_and_responsibility_person a
LEFT JOIN t_check_problem b on b.PK_ID=a.FK_CHECK_PROBLEM_ID
LEFT JOIN t_person c on c.ID_CARD=a.ID_CARD
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
WHERE d.type3='{2}' 
AND b.submit_time BETWEEN '{0}' AND '{1} 23:59:59'"""