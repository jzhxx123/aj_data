#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   analysisReport_wwj
Description:
Author:    wangwenjun
date:         2019-03-13
-------------------------------------------------
Change Activity:2019-03-13 14:54
-------------------------------------------------
"""
import json
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta

from app.data.util import pd_query
from app.report.analysisMonthlyReport_sql import *

MAJORS = ('车务', '电务', '工务', '机务', '车辆', '供电')


def get_warning_key_person_library(start_date, end_date):
    """
    获取重点人员库表信息
    :return:
    当月：now_month
    """
    # 从重点人员库表里获取预警配置id，以及身份证号
    year, month = int(end_date[:4]), int(end_date[5:7])
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')

    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    last_year, last_month = int(last_month_end[:4]), int(last_month_end[5:7])
    department_data = pd_query(DEPARTMENT_INFO_SQL)
    violation_total, violation_distrb = _get_system_violation_distribution(
        SYSTEM_VIOLATION_DISTRIBUTION_SQL.format(month, year), department_data)
    lastmonth_violation_slove = _get_lastmonth_violation_results(
        pd_query(MONTH_VIOLATION_RESULTS_SQL.format(last_month, last_year)),
        pd_query(SYSTEM_VIOLATION_DISTRIBUTION_SQL.format(last_month, last_year)),
        department_data
    )

    fourthresults = {
        'year': year,
        'month': month,
        'last_year': last_year,
        'last_month': last_month,
        'violation_total': violation_total,
        'main_objects': violation_distrb,
        'lastmonth_violation_solve': lastmonth_violation_slove
    }

    return fourthresults


def _get_type_counts(data, main_colunm_name, sub_colunm_name):
    """
    获取datafram某一列的种类对应的数量,返回字典类型
    :param data:
    :param colunm_name:
    :return:
    {
        jiwu:{total:19,duan:1}
    }
    """
    main_colunm = {}
    main_objects = []
    violation_total = data.shape[0]
    main_c = data[main_colunm_name].value_counts().to_dict()
    sub_c = data[sub_colunm_name].value_counts().to_dict()
    main_colunm = {k: [] for k in main_c.keys()}
    for k, v in sub_c.items():
        # 根据列值寻找索引
        tmp = {}
        tmp['name'] = k
        tmp['total'] = v
        pos = data.loc[data[sub_colunm_name] == k].head(1).index
        cl = [v for v in data.loc[pos][main_colunm_name].to_dict().values()]
        main_colunm[cl[0]].append(tmp)
    for k, v in main_colunm.items():
        tmp = {}
        tmp['name'] = k
        tmp['total'] = main_c[k]
        tmp['sub_objects'] = v
        main_objects.append(tmp)
    return violation_total, main_objects


def _get_system_violation_distribution(sql, department_data):

    raw_data = pd_query(sql)
    department_data = department_data[department_data['TYPE'] == 4]
    department_data = department_data[['DEPARTMENT_ID', 'NAME']]
    raw_data.drop_duplicates(subset=['ID_CARD'], inplace=True)
    all_major_dpids = [_get_major_dpid(item) for item in MAJORS]
    all_major_datas = raw_data[raw_data['TYPE2'].isin(all_major_dpids)]
    violation_total = all_major_datas.shape[0]
    main_objects = []
    for major in MAJORS:
        sub_objects = []
        major_data = all_major_datas[all_major_datas['TYPE2'].isin([_get_major_dpid(major)])]
        zhanduan_data = major_data['TYPE3'].value_counts()
        for zhanduan_dpid in zhanduan_data.index:
            sub_objects.append(
                {
                'name': str(department_data[
                    department_data['DEPARTMENT_ID'] == zhanduan_dpid]['NAME'].values[0]),
                'total': int(zhanduan_data[zhanduan_dpid])
                }
            )
        main_objects.append(
            {
                'name': major,
                'total': int(major_data.shape[0]),
                'sub_objects': sub_objects
            }
        )
    # # 子类型的数量统计，对应于type3
    # type2_set = set(raw_data["TYPE2"])
    # type2_name = _get_real_name(type2_set)
    # type3_set = set(raw_data['TYPE3'])
    # type3_name = _get_real_name(type3_set)
    # # type2与type3对应关系
    # type2_to_type3 = _translate_to_dict(raw_data, type2_name, type3_name)
    # violation_total, main_colunm = _get_type_counts(
    #     type2_to_type3, 'TYPE2', 'TYPE3')

    return int(violation_total), main_objects


def _translate_to_dict(data, main_dict, sub_dict):
    # data
    # type2与type3对应关系
    data['TYPE2'] = data['TYPE2'].map(main_dict)
    data['TYPE3'] = data['TYPE3'].map(sub_dict)
    return data


def _get_lastmonth_violation_results(offical_data, primary_data, department_data):
    """

    Args:
        offical_data: 重点人员库
        primary_data: 重点人员预选库
    处置方式id(1:培训,2:离岗,3:待岗,4:转岗, 5: 前期已处置, 6: 帮促)
    Returns:
    FK_DISP0SE_METHOD_IDS
    """
    offical_data.drop_duplicates(subset=['PK_ID'], inplace=True)
    slove_dict = {}
    slove_dict['total'] = len(set(offical_data['ID_CARD'].values.tolist()))
    static_contents = {
        'peixun': 1,
        'ligang': 2,
        'daigang': 3,
        'zhuangang': 4,
        'qianqiyichuzhi': 5,
        'bangcu': 6
    }
    for key, value in static_contents.items():
        tmp_df = offical_data[
            offical_data['STATUS'] == 1
        ]
        tmp_df = tmp_df[
            tmp_df['FK_DISPOSE_METHOD_IDS'].str.contains(str(value))
        ]
        slove_dict[key] = len(set(tmp_df['ID_CARD'].values.tolist()))
    data = primary_data[
                primary_data['STATUS'] == 0
        ]
    slove_dict['daiqueren'] = dict()
    slove_dict['daiqueren']['count'] = len(set(data['ID_CARD']))
    if not data.empty:
        rst = data['TYPE3'].value_counts()
        slove_dict['daiqueren']['value'] = '、'.join([
            f"{department_data[department_data['DEPARTMENT_ID'] == index]['NAME'].values[0]}{rst[index]}"
            for index in rst.index
        ])

    # 待处置、
    data = offical_data[
                offical_data['STATUS'] == 0
            ]
    slove_dict['daichuzhi'] = dict()
    slove_dict['daichuzhi']['count'] = len(
        set(data['ID_CARD'])
    )
    if not data.empty:
        rst = data['TYPE3'].value_counts()
        slove_dict['daichuzhi']['value'] = '、'.join([
            f"{department_data[department_data['DEPARTMENT_ID'] == index]['NAME'].values[0]}{rst[index]}"
            for index in rst.index
        ])
    return slove_dict


def _get_major_dpid(major):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(major, None)
