#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
import json
import re
from datetime import datetime as dt

import pandas as pd
from dateutil.relativedelta import relativedelta
from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.report.analysisReport_cyf import (
    get_cadre_check_quality_analyse, get_check_evaluate_info, get_safety_tip)
from app.report.analysisReport_wwj import get_warning_key_person_library
from app.report.util import (devide_type, get_check_info_data,
                             get_check_problem_data,
                             get_ring_ratio,
                             singe_score_section)
from app.report.analysisMonthlyReport_sql import *

MAJORS = ('车务', '机务', '工务', '电务', '供电', '车辆')


def calculate_year_and_ring_ratio(counts):
    now_count, year_count, month_count = counts
    year_ratio = now_count - year_count
    if year_count == 0:
        year_percent = year_ratio * 100
    else:
        year_percent = round(year_ratio / year_count, 3) * 100
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 3) * 100
    result = {
        'year_diff': year_ratio,
        'year_percent': year_percent,
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


_OFFICE_ANALYSIS_CENTER_DICT = {
    # todo 将局分析中心的检查组静态编码，暂时无法通过sql确定
    '车务': ['80A7FF67374A754FE0534720C00A4A19',
           '80A7FD6259C567B0E0534820C00A62AE',
           '80A7FB683B077555E0534720C00AEC28'],  # 分为客运部，货运部，运输部
    '机务': ['999900020014000315DC0004'],
    '工务': ['999900020014000315DC0005'],
    '电务': ['999900020014000315DC0006'],
    '供电': ['999900020014000315DC0007'],
    '车辆': ['999900020014000315DC0008'],
    '综合': ['999900020014000315DC0001', '999900020014000315DC0002'],  # 综合分析室+安全分析信息化室
}
item_data_dict = {'LH': '量化指标完成', 'JL': '检查信息录入', 'ZL': '监督检查质量', 'KH': '考核责任落实', 'ZG': '问题闭环管理', 'ZD': '重点工作落实',
                  'YY': '音视频运用管理', 'SZ': '事故故障追溯', 'ZJ': '弄虚作假'}

all_items = [
    '量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
    '履职评价管理', '事故故障追溯', '弄虚作假'
]

items_code = ['LH', 'JL', 'ZL', 'KH', 'ZG', 'ZD', 'YY', 'LZ', 'SZ', 'ZJ']

level_name = {
    1: '∑＜2',
    2: '2≤∑＜4',
    3: '4≤∑＜6',
    4: '6≤∑＜8',
    5: '8≤∑＜10',
    6: '10≤∑＜12',
    7: '∑≥12'
}


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    global LAST_MONTH_START, LAST_MONTH_END, LAST_YEAR_START, LAST_YEAR_END
    LAST_MONTH_START = str(start_date - relativedelta(months=1))[:10]
    LAST_MONTH_END = str(end_date - relativedelta(months=1))[:10]
    LAST_YEAR_START = str(start_date - relativedelta(months=12))[:10]
    LAST_YEAR_END = str(end_date - relativedelta(months=12))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    first = get_safe_produce_base_doc(start_date, end_date)
    second = get_evaluate_situation_base_doc(start_date, end_date, year, month)
    third_fifth = get_analysis_center_work_content(start_date, end_date)
    third_2 = get_cadre_check_quality_analyse(start_date, end_date)
    third_3 = get_check_evaluate_info(start_date, end_date)
    third_third = get_level_carde_evaluate(start_date, end_date)
    fourth = get_warning_key_person_library(start_date, end_date)
    fifth = get_safety_tip(start_date, end_date)
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "first_accident_analysis": first,
        "second_evaluate_analysis": second,
        "third_analysis_center": third_fifth,
        "third_carde_evaluate_analysis": third_third,
        "fourth_violation_analysis": fourth,
        "third_2": third_2,
        "third_3": third_3,
        "fifth_safety_warning": fifth
    }
    return result


# def get_analysis_data():
def get_analysis_data(start_date, end_date):
    """获取分析报告数据入口

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- 当月报告所有数据
    """
    year, month = int(end_date[:4]), int(end_date[5:7])
    start_month, start_day = int(start_date[5:7]), int(start_date[8:10])
    end_month, end_day = int(end_date[5:7]), int(end_date[8:10])
    if start_day != 25 or end_day != 24 or start_month == end_month:
        result = get_data(start_date, end_date)
    else:
        result = list(
            mongo.db.detail_analysis_report.find({
                'year': year,
                'month': month
            }))
        if result:
            result = result[0]
        else:
            result = get_data(start_date, end_date)
            mongo.db.detail_analysis_report.insert_one(result)
    return result


def calc_different_type(data):
    """
    计算各类问题数量
    :param data:
    :return:
    """
    # 一般ABCD类事故数
    number = []
    for i in ['A', 'B', 'C', 'D']:
        new_data = data[data['MAIN_TYPE'] == 1]
        number.append(len(new_data[new_data['CODE'].str.contains(i)]))
    # 设备故障数量
    number.append(len(data[(data['NAME'].str.contains('设备故障')) & (data['MAIN_TYPE'] == 2)]))
    return number


def get_safe_produce_base_doc(start_date, end_date):
    """一。集团公司安全生产基本情况分析

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- 第一部分结果
    """
    # 安全生产信息
    accident_data = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_month_data = pd_query(CHECK_SAFETY_INFO_SQL.format(LAST_MONTH_START, LAST_MONTH_END)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_year_data = pd_query(CHECK_SAFETY_INFO_SQL.format(LAST_YEAR_START, LAST_YEAR_END)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # TOP信息
    top = get_top(accident_data, last_month_data, last_year_data)
    # 行车、路外、劳安
    all_acc = get_all_acc(accident_data, last_month_data, last_year_data)
    # 事故类别分析
    acc_type = get_acc_type(accident_data, last_month_data, last_year_data)
    # 事故责任归属分析
    acc_duty = get_acc_duty(accident_data, last_month_data, last_year_data)
    # 设备故障统计分析
    eq_failed = get_eq_failed(accident_data, last_month_data, last_year_data)
    return {
        'top': top,
        'all_acc': all_acc,
        'acc_type': acc_type,
        'acc_duty': acc_duty,
        'eq_failed': eq_failed,
    }


def get_top(accident_data, last_month_data, last_year_data):
    """
    TOP信息计算
    :param accident_data: 本月数据
    :param last_month_data: 上月数据
    :param last_year_data: 去年数据
    :return:
    """
    data_list = [accident_data, last_month_data, last_year_data]
    # 表格信息
    result = []
    for i in data_list:
        data = i[i['RANK'].isin([1, 2, 3])]
        result.append(calc_different_type(data))
    # 计算环比、同比
    ratio = []
    for i in range(5):
        ratio.append(calculate_year_and_ring_ratio([result[0][i], result[2][i], result[1][i]]))
    # 一般事故
    general_acc = accident_data[accident_data['MAIN_TYPE'] == 1]
    last_month_acc = last_month_data[last_month_data['MAIN_TYPE'] == 1]
    last_year_acc = last_year_data[last_year_data['MAIN_TYPE'] == 1]
    # 环比同比
    all_ratio = calculate_year_and_ring_ratio([len(general_acc), len(last_month_acc), len(last_year_acc)])
    return {
        'result': result,
        'ratio': ratio,
        'general_acc': len(general_acc),
        'all_ratio': all_ratio
    }


def get_all_acc(accident_data, last_month_data, last_year_data):
    """
    行车、路外、劳安
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 行车
    xc = calc_all_acc(accident_data, last_month_data, last_year_data, 1)
    # 路外
    lw = calc_all_acc(accident_data, last_month_data, last_year_data, 3)
    # 劳安
    la = calc_all_acc(accident_data, last_month_data, last_year_data, 2)
    return {
        'xc': xc,
        'lw': lw,
        'la': la

    }


def calc_all_acc(accident_data, last_month_data, last_year_data, detail_type):
    """
    计算行车/路外/劳安问题
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :param detail_type: 问题分类
    :param l1:
    :return:
    """
    # 事故类别
    accident_data = accident_data[(accident_data['DETAIL_TYPE'] == detail_type) & (accident_data['MAIN_TYPE'] == 1)]
    last_month_data = last_month_data[
        (last_month_data['DETAIL_TYPE'] == detail_type) & (last_month_data['MAIN_TYPE'] == 1)]
    last_year_data = last_year_data[(last_year_data['DETAIL_TYPE'] == detail_type) & (last_year_data['MAIN_TYPE'] == 1)]

    data_list = [accident_data, last_year_data, last_month_data]
    acc_list = []
    type_list = []
    # 事故发生件数
    for i in data_list:
        acc_list.append(len(i))
    # 环比同比
    ratio1 = calculate_year_and_ring_ratio(acc_list)
    # 发生ABCD类事故件数
    for i in ['A', 'B', 'C', 'D']:
        l2 = []
        for j in data_list:
            l2.append(len(j[j['CODE'].str.contains(i)]))
        type_list.append(l2)
    # 环比同比
    ratio = []
    for i in type_list:
        ratio.append(calculate_year_and_ring_ratio(i))
    return {
        'acc_list': acc_list[0],
        'ratio1': ratio1,
        'type_list': type_list,
        'ratio': ratio
    }


def get_acc_type(accident_data, last_month_data, last_year_data):
    """
    事故类别分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    accident_data = accident_data[accident_data['MAIN_TYPE'] == 1]
    last_month_data = last_month_data[last_month_data['MAIN_TYPE'] == 1]
    last_year_data = last_year_data[last_year_data['MAIN_TYPE'] == 1]
    # 行车C类
    type_c = calc_accident_code_type(accident_data, last_month_data, last_year_data, 'C', 1)
    # 行车D类
    type_d = calc_accident_code_type(accident_data, last_month_data, last_year_data, 'D', 1)
    # 劳安事故
    labor = calc_labor(accident_data, last_month_data, last_year_data)
    return {
        'type_c': type_c,
        'type_d': type_d,
        'labor': labor
    }


def calc_accident_code_type(accident_data, last_month_data, last_year_data, str1, detail_type):
    """
    计算事故类别
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :param str1:
    :return:
    """
    # 各个类别事故数量
    data = accident_data[(accident_data['CODE'].str.contains(str1)) & (accident_data['DETAIL_TYPE'] == detail_type)]
    last_month_data = last_month_data[
        (last_month_data['CODE'].str.contains(str1)) & (last_month_data['DETAIL_TYPE'] == detail_type)]
    last_year_data = last_year_data[
        (last_year_data['CODE'].str.contains(str1)) & (last_year_data['DETAIL_TYPE'] == detail_type)]
    new_data = data.groupby('NAME').count().reset_index()
    # 事故名称
    name_list = new_data['NAME'].tolist()
    # 事故数量
    number = new_data['CODE'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, [], []]
    for i in name_list:
        l1[1].append(len(last_year_data[last_year_data['NAME'] == i]))
        l1[2].append(len(last_month_data[last_month_data['NAME'] == i]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i], l1[2][i]]))
    return {
        'count': len(data),
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def calc_labor(accident_data, last_month_data, last_year_data):
    """
    劳安问题
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 劳安事故分类
    data = accident_data[accident_data['MAIN_TYPE'] == 2]
    last_month_data = last_month_data[last_month_data['MAIN_TYPE'] == 2]
    last_year_data = last_year_data[last_year_data['MAIN_TYPE'] == 2]
    new_data = data.groupby('NAME').count().reset_index()
    # 事故名称
    name_list = new_data['NAME'].tolist()
    # 事故数量
    number = new_data['PROBLEM_POINT'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, [], []]
    for i in name_list:
        l1[1].append(len(last_year_data[last_year_data['NAME'] == i]))
        l1[2].append(len(last_month_data[last_month_data['NAME'] == i]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i], l1[2][i]]))
    return {
        'count': len(data),
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def get_acc_duty(accident_data, last_month_data, last_year_data):
    """
    事故责任分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 系统责任
    sys_duty = get_sys_duty(accident_data, last_year_data, last_month_data)
    # 站段责任
    station_duty = get_station_duty(accident_data, last_year_data, last_month_data)
    return {
        'sys_duty': sys_duty,
        'station_duty': station_duty,
    }


def get_sys_duty(accident_data, last_year_data, last_month_data):
    """
    系统责任
    :param accident_data:
    :param last_year_data:
    :param last_month_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    xc = []
    laoan = []
    for i in data_list:
        # 行车责任事故
        xc.append(len(i[(i['DETAIL_TYPE'] == 1) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
        # 劳安责任事故
        laoan.append(
            len(i[(i['DETAIL_TYPE'] == 2) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
    ratio1 = calculate_year_and_ring_ratio(xc)
    ratio2 = calculate_year_and_ring_ratio(laoan)
    return {
        'xc': xc[0],
        'laoan': laoan[0],
        'ratio1': ratio1,
        'ratio2': ratio2
    }


def get_station_duty(accident_data, last_year_data, last_month_data):
    """
    站段责任
    :param accident_data:
    :param last_year_data:
    :param last_month_data:
    :return:
    """
    all_list = []
    for name in MAJORS:
        data_list = [accident_data[accident_data['MAJOR'] == name], last_year_data[last_year_data['MAJOR'] == name],
                     last_month_data[last_month_data['MAJOR'] == name]]
        xc = []
        for i in data_list:
            # 行车责任事故
            xc.append(
                len(i[(i['DETAIL_TYPE'] == 1) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
        ratio1 = calculate_year_and_ring_ratio(xc)
        # 按事故类别分
        new_data = data_list[0].groupby(['NAME', 'CODE']).count().reset_index()
        acc_content = []
        for i, j in new_data.iterrows():
            dic = {
                'name': j['NAME'],
                'count': int(j['REASON']),
                'type': j['CODE']
            }
            acc_content.append(dic)
        # 按站段责任分
        station_number = []
        for st in data_list[0]['STATION'].unique():
            duty_number = []
            data = data_list[0][data_list[0]['STATION'] == st]
            for i in [1, 4, 2, 6]:
                duty_number.append(len(data[(data['RESPONSIBILITY_IDENTIFIED'] == i) & (accident_data['TYPE'] == 1)]))
            dic = {
                'name': st,
                'duty_number': duty_number
            }
            station_number.append(dic)
        dic = {
            'name': name,
            'xc': xc[0],
            'ratio1': ratio1,
            'acc_content': acc_content,
            'station_number': station_number,
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_eq_failed(accident_data, last_month_data, last_year_data):
    """
    事故故障分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 供电系统发生设备故障
    shebei = calc_eq(accident_data, last_month_data, last_year_data)
    # 设备故障构成事故
    shebei_acc = calc_eq_acc(accident_data, last_month_data, last_year_data)
    # 未构成事故的设备故障
    not_acc = calc_eq_not_acc(accident_data, last_month_data, last_year_data)
    return {
        'shebei': shebei,
        'shebei_acc': shebei_acc,
        'not_acc': not_acc
    }


def calc_eq(accident_data, last_month_data, last_year_data):
    """
    计算供电系统发生设备故障
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        i = i.dropna(subset=['NAME'])
        count.append(len(i[i['NAME'].str.contains('设备故障')]))
    d21 = len(accident_data[(accident_data['NAME'].str.contains('设备故障')) & (accident_data['CODE'] == 'D21')])
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'd21': d21,
        'ratio': ratio
    }


def calc_eq_acc(accident_data, last_month_data, last_year_data):
    """
    计算故障事故
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        count.append(len(i[(i['NAME'].str.contains('设备故障')) & (i['MAIN_TYPE'] == 1)]))
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'ratio': ratio
    }


def calc_eq_not_acc(accident_data, last_month_data, last_year_data):
    """
    计算故障事故
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        count.append(len(i[(i['NAME'].str.contains('设备故障')) & (i['MAIN_TYPE'] != 1)]))
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'ratio': ratio
    }


def get_evaluate_situation_base_doc(start_date, end_date, year, month):
    """获取履职情况简要统计分析数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    # 公用数据
    global EVALUATE_INFO, LAST_MONTH_EVALUATE_INFO, EVALUATE_INFOS, LAST_MONTH_EVALUATE_INFOS, LAST_YEAR_EVALUATE_INFO, \
        EVALUATE_YEAR_INFO, EVALUATE_YEAR_MONTH_INFO
    # 当月履职评价, 去重
    EVALUATE_INFO = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date, int(year))).drop_duplicates(
        subset=['PK_ID']).reset_index()
    EVALUATE_INFOS = pd_query(CHECK_EVALUATE_SQLS.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_year = year
    if month == 1:
        last_year = int(year) - 1
    # 上月履职评价
    LAST_MONTH_EVALUATE_INFO = pd_query(
        CHECK_EVALUATE_SQL.format(LAST_MONTH_START, LAST_MONTH_END, int(last_year))).drop_duplicates(
        subset=['PK_ID']).reset_index()
    LAST_MONTH_EVALUATE_INFOS = pd_query(
        CHECK_EVALUATE_SQLS.format(LAST_MONTH_START, LAST_MONTH_END)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 去年当月履职评价
    LAST_YEAR_EVALUATE_INFO = pd_query(
        CHECK_EVALUATE_SQL.format(LAST_YEAR_START, LAST_YEAR_END, int(year) - 1)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 今年全部履职评价
    EVALUATE_YEAR_INFO = pd_query(
        CHECK_EVALUATE_SQL.format(f'{str(int(end_date[:4]) - 1)}-12-25', end_date, int(year))).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 今年上月数据
    EVALUATE_YEAR_MONTH_INFO = pd_query(
        CHECK_EVALUATE_SQL.format(f'{str(int(end_date[:4]) - 1)}-12-25', LAST_MONTH_END, int(year))).drop_duplicates(
        subset=['PK_ID']).reset_index()
    if EVALUATE_YEAR_MONTH_INFO.empty:
        EVALUATE_YEAR_MONTH_INFO['SCORE'] = 0
    if EVALUATE_INFO.empty:
        return {}
    # 第一块 干部评价总体分析
    first_paragraph = get_evaluate_whole_situation(start_date, end_date)
    evaluate_problem_type_table = get_evaluate_problem_type_table(
        start_date, end_date)
    # 第二块 各系统干部问题分析
    major_evaluate_situation = get_major_evaluate_situation()
    # 第三块 局管领导人员履职评价分析
    major_niubi_evaluate_situation = get_niubi_evaluate_situation()
    # 第四块 正科职干部履职评价简要分析
    zheng_level_evaluate_analysis = get_branch_level_carbe_situation('正科级')
    # 第五块 副科职及以下干部履职简要分析
    fu_level_evaluate_analysis = get_branch_level_carbe_situation('副科级')
    result = {
        'evaluate_total_analysis': {
            "total_evaluate": first_paragraph,
            "evaluate_problem_type_table": evaluate_problem_type_table
        },
        'carde_evaluate_analysis': {
            "major_evaluate_situation": major_evaluate_situation
        },
        'niubi_evaluate_analysis': major_niubi_evaluate_situation,
        'zheng_level_evaluate_analysis': zheng_level_evaluate_analysis,
        'fu_level_evaluate_analysis': fu_level_evaluate_analysis
    }
    return result


def get_evaluate_whole_situation(start_date, end_date):
    """干部评价总体情况分析

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    # 筛选干部履职评价(复查)
    eva_re = pd_query(eva_review_sql.format(start_date, end_date))
    evaluate_review = pd_query(NEW_CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date))
    total_ring = get_ring_ratio(EVALUATE_INFO.shape[0], LAST_MONTH_EVALUATE_INFO.shape[0])
    # 定期评价人次
    regular_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 2].shape[0] + \
                    eva_re[eva_re['EVALUATE_WAY'] == 2].shape[0]
    # 逐条评价人次
    one_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 1].shape[0] + \
                eva_re[eva_re['EVALUATE_WAY'] == 1].shape[0]
    # 筛选干部履职评价信息
    _evaluate_info = EVALUATE_INFOS
    # 履职问题个数
    evaluate_count = len(set(_evaluate_info['PK_ID']))
    # 履职问题人数
    evaluate_person = len(set(_evaluate_info['RESPONSIBE_ID_CARD']))
    # 工人履职数据
    work_data = _evaluate_info[_evaluate_info['GRADATION'] == "非管理和专业技术人员"]
    # 工人条数和人数
    work_count, work_person = len(set(work_data['PK_ID'])), len(
        set(work_data['RESPONSIBE_ID_CARD']))
    # 安全谈心数据
    talk_data = _evaluate_info[_evaluate_info['ITEM_NAME'] == '安全谈心']
    talk_count, talk_person = len(set(talk_data['PK_ID'])), len(
        set(talk_data['RESPONSIBE_ID_CARD']))
    # 上述不去重原因是一个履职评价信息可能对应多个评价内容（安全谈心，弄虚作假等）
    # 共计评价得分,去重
    _evaluate_info.drop_duplicates(subset=['PK_ID'], inplace=True)
    EVALUATE_YEAR_MONTH_INFO.drop_duplicates(subset=['PK_ID'], inplace=True)
    total_score = sum(list(_evaluate_info['SCORE']))
    # 最高评价计分
    score_data = _evaluate_info.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    EVALUATE_YEAR_INFOs = EVALUATE_YEAR_INFO.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    eva_month_year_data = EVALUATE_YEAR_MONTH_INFO.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    most_score = round(float(score_data.at[0, 'SCORE']), 1)
    # 今年得分数据
    score_year_data = EVALUATE_YEAR_INFO[EVALUATE_YEAR_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])] \
        .groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    score_month_year_data = EVALUATE_YEAR_MONTH_INFO[
        EVALUATE_YEAR_MONTH_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])] \
        .groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    # 本月干部累计记分人数
    month_total_count = len(set(_evaluate_info['RESPONSIBE_ID_CARD']))
    # 截止目前干部累计记分人数
    year_total_count = len(set(score_year_data['RESPONSIBE_ID_CARD']))
    # 本月新增人数
    new_people_added = _calc_new_add_count(EVALUATE_YEAR_MONTH_INFO, EVALUATE_YEAR_INFOs, 'RESPONSIBE_ID_CARD')
    month_percent = round(new_people_added / year_total_count * 100, 1)
    most_year_score = round(score_year_data.at[0, 'SCORE'], 1)
    # todo 暂时累计扣分超过2分列为被处理
    punish_year_count = score_year_data[score_year_data['SCORE'] >= 2].shape[0]
    # punish_count = score_data[score_data['SCORE'] >= 2].shape[0]
    punish_count = _calc_new_add_count(score_month_year_data[score_month_year_data['SCORE'] >= 2],
                                       score_year_data[score_year_data['SCORE'] >= 2],
                                       'RESPONSIBE_ID_CARD')
    score_data['level'] = score_data['SCORE'].apply(singe_score_section)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        singe_score_section)
    score_month_year_data['level'] = score_month_year_data['SCORE'].apply(singe_score_section)
    levels = [7, 6, 5, 4, 3, 2, 1]
    p_level_rst = []
    for level in levels:
        p_level_rst.append({
            "level":
                level_name.get(level),
            "total_count":
            # score_year_data[score_year_data['level'] == level].shape[0],
                len(set(score_year_data[score_year_data['level'] == level]['RESPONSIBE_ID_CARD'])),
            "month_count": _calc_new_add_count(score_month_year_data[score_month_year_data['level'] == level],
                                               score_year_data[score_year_data['level'] == level],
                                               'RESPONSIBE_ID_CARD')
        })
    score_rst = {
        "year_total_count": year_total_count,
        # "month_count": evaluate_count,
        "month_count": new_people_added,
        "month_percent": month_percent,
        "most_year_score": most_year_score,
        "punish_year_count": punish_year_count,
        "punish_count": punish_count,
        "p_level_value": p_level_rst,
        "t_level_value": p_level_rst[::-1]
    }
    rst = {
        'regular_count': regular_count,
        'one_count': one_count,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'work_count': work_count,
        'work_person': work_person,
        'talk_count': talk_count,
        'talk_person': talk_person,
        'total_score': total_score,
        'most_score': most_score,
        'total_ring': total_ring,
        'score_level_value': score_rst
    }
    return rst


def get_evaluate_problem_type_table(start_date, end_date):
    """履职问题分类数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    total = EVALUATE_INFO[EVALUATE_INFO.ITEM_NAME.isin(all_items)].shape[0]
    # 去重
    _evaluate_info = EVALUATE_INFOS.drop_duplicates(subset=['PK_ID'], keep='first').dropna(subset=['ITEM_NAME'])
    _last_info = LAST_MONTH_EVALUATE_INFOS.drop_duplicates(subset=['PK_ID'], keep='first').dropna(subset=['ITEM_NAME'])
    # 履职存在问题
    eva_pro = len(set(_evaluate_info['PK_ID']))
    last_pro = len(set(_last_info['PK_ID']))
    if last_pro == 0:
        pro_ratio = 0
    else:
        pro_ratio = round((eva_pro - last_pro) / last_pro * 100, 2)
    counts = []
    percents = []
    rings = []
    detail_item = []
    majors_rst = []
    for item in all_items:
        major_item_rst = []
        if total == 0:
            count, percent, ring = 0, 0, 0
            for major in MAJORS:
                major_item_rst.append({'name': major, 'count': [], 'ring': []})
        else:
            item_info = _evaluate_info[_evaluate_info['ITEM_NAME'].str.contains(item)]
            last_item_info = LAST_MONTH_EVALUATE_INFO[LAST_MONTH_EVALUATE_INFO['ITEM_NAME'].str.contains(item)]
            # 去重, 筛选干部履职评价
            last_item_info.drop_duplicates(subset=['PK_ID'], inplace=True)
            count, last_count = len(item_info), len(last_item_info)
            percent = round(count / total * 100, 1)
            ring = get_ring_ratio(count, last_count)
            # 各专业系统分类
            if item_info.empty is False:
                major_item_rst = _get_major_evaluate_table(
                    item_info, last_item_info)
                # 具体问题分类数据
                item_info = item_info.groupby(['CODE', 'SITUATION'])
                for name, group in item_info:
                    score_count = len(group)
                    score_person = int(
                        group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
                    detail_item.append({
                        'detail_name': name[1],
                        'code': name[0],
                        'name': item,
                        'score_count': score_count,
                        'score_person': score_person
                    })
            else:
                for major in MAJORS:
                    major_item_rst.append({
                        'name': major,
                        'count': 0,
                        'ring': 0
                    })
                major_item_rst.append({'name': '合计', 'count': 0, 'ring': 0})
            majors_rst.append(major_item_rst)
        counts.append(count)
        percents.append(percent)
        rings.append(ring)
    detail_data = pd.DataFrame(detail_item)
    limit = [9, 9, 9, 9, 9, 9, 9, 9, 9, 10000]
    problem_rst = []
    sit_data = _evaluate_info.groupby('CODE').count().sort_values(by='PK_ID', ascending=False).reset_index()
    for index, values in sit_data.head(6).iterrows():
        detail_item_name = list(_evaluate_info[_evaluate_info['CODE'] == values['CODE']]['SITUATION'])[0]
        detail_item_count = int(values['SITUATION'])
        problem_rst.append({
            "detail_name":
                detail_item_name,
            "detail_count":
                detail_item_count,
            "detail_percent":
                round(detail_item_count / len(_evaluate_info) * 100, 1)
        })
    problem_value = []
    index = 0
    cs = ['部分管理人员对上级布置重点工作不认真思考、不主动抓落实、不检查督促，重点工作推进迟缓、贯彻落实不力。',
          '下现场检查力度、深度不够，安全检查不认真、查处问题质量不高的问题突出。',
          '部分管理人员系统运用不重视，安全问题、信息录入明显错误，录入与安全无关的问题，拆分检查信息录入的问题突出。',
          '部分管理人员对问题整改不重视，问题整改敷衍了事、整改督促不力、销号把关不严，问题整改不彻底、安全问题反复发生的问题突出。',
          '部分管理干部安全意识松懈、安全管理不严、专业管理不力，安全过程控制不到位，现场作业失管失控。',
          '部分管理干部日常安全考核不严格、存有好人主义，查处安全问题该考核不考核、降低标准考核问题仍然较多。',
          '部分管理人员对音视频运用管理不重视，音视频设备管理不到位、调阅分析开展不力的问题突出。',
          '']
    for i in ['ZD', 'ZL', 'JL', 'ZG', 'SZ', 'KH', 'YY', 'ZJ']:
        item_data = _evaluate_info[_evaluate_info['CODE'].str.contains(i)]
        situation = []
        for j, k in item_data.groupby('CODE').count().sort_values(by='SITUATION', ascending=False).reset_index().head(
                5).iterrows():
            sits = item_data[item_data['CODE'] == k['CODE']]
            dics = {
                'name': sits['SITUATION'].values[0],
                'count': len(sits),
                'person': len(sits['RESPONSIBE_ID_CARD'].unique())
            }
            situation.append(dics)
        if i == 'ZJ':
            zj_content = []
            for x, y in item_data.iterrows():
                zj_content.append(
                    y['ALL_NAME'] + ('自查' if y['CHECK_TYPE'] == 2 else '被路局检查') + y['POSITION'] +
                    y['RESPONSIBE_PERSON_NAME'] + y['SITUATION'] + y['EVALUATE_CONTENT'])
            dic = {
                'name': item_data_dict[i],
                'count': len(item_data),
                'person': len(item_data['RESPONSIBE_ID_CARD'].unique()),
                'situation': situation,
                'zj_content': zj_content,
                'content': cs[index]
            }
        else:
            dic = {
                'name': item_data_dict[i],
                'count': len(item_data),
                'person': len(item_data['RESPONSIBE_ID_CARD'].unique()),
                'situation': situation,
                'content': cs[index]
            }
        index += 1
        problem_value.append(dic)
    result = {
        'names': all_items,
        'counts': counts,
        'pro_ratio': pro_ratio,
        'percents': percents,
        'rings': rings,
        'major_value': majors_rst,
        'problem_value': problem_rst,
        'problem_values': problem_value
    }
    return result


def _get_major_evaluate_table(item_info, last_item_info):
    """专业分类数据

    Arguments:
        item_info {dataframe} -- 当前月份数据
        last_item_info {dataframe} -- 上月数据

    Returns:
        dict -- 结果
    """
    major_item_rst = []
    for major in MAJORS:
        major_info = item_info[item_info['MAJOR'] == major]
        major_month_info = last_item_info[last_item_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_last_count = major_month_info.shape[0]
        major_ring = get_ring_ratio(major_count, major_last_count)
        major_item_rst.append({
            'name': major,
            'count': major_count,
            'ring': major_ring
        })
    total = item_info.shape[0]
    last_total = last_item_info.shape[0]
    total_ring = get_ring_ratio(total, last_total)
    major_item_rst.append({'name': '合计', 'count': total, 'ring': total_ring})
    return major_item_rst


def get_major_evaluate_situation():
    """各系统干部问题分析

    Returns:
        dict  -- 结果
    """
    # 去重
    all_year_info = EVALUATE_YEAR_INFO.drop_duplicates(subset=['PK_ID'])
    all_year_info = all_year_info[all_year_info['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
    all_year_info = all_year_info.groupby(
        ['MAJOR', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    all_year_info['level'] = all_year_info['SCORE'].apply(singe_score_section)

    all_month_info = EVALUATE_YEAR_MONTH_INFO.drop_duplicates(subset=['PK_ID'])

    all_month_info = all_month_info[all_month_info['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
    all_month_info = all_month_info.groupby(
        ['MAJOR', 'RESPONSIBE_ID_CARD']).sum().reset_index()
    # if all_month_info.empty is False:
    all_month_info['level'] = all_month_info['SCORE'].apply(singe_score_section)
    # else:
    #     all_month_info['level'] = 0
    rst = []
    totals = ['合计'] + [
        all_year_info[all_year_info['level'] == i].shape[0]
        for i in range(1, 8)
    ]
    levels = [level_name.get(i) for i in range(1, 8)]
    for major in MAJORS:
        major_info = EVALUATE_INFO[
            (EVALUATE_INFO['MAJOR'] == major) & (EVALUATE_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4]))]
        year_info = EVALUATE_YEAR_INFO[(EVALUATE_YEAR_INFO['MAJOR'] == major) & (
            EVALUATE_YEAR_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4]))]
        month_info = EVALUATE_YEAR_MONTH_INFO[(EVALUATE_YEAR_MONTH_INFO['MAJOR'] == major) & (
            EVALUATE_YEAR_MONTH_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4]))]
        # 去重
        major_info.drop_duplicates(subset=['PK_ID'], inplace=True)
        detail_data = _get_evaluate_major_detail_item_data(major_info, major)
        major_count = major_info.shape[0]
        major_info = major_info.groupby(
            'RESPONSIBE_ID_CARD').sum().sort_values(
            by='SCORE', ascending=False).reset_index()
        major_year_infos = year_info.groupby(
            'RESPONSIBE_ID_CARD').sum().sort_values(
            by='SCORE', ascending=False).reset_index()
        major_month_infos = month_info.groupby(
            'RESPONSIBE_ID_CARD').sum().sort_values(
            by='SCORE', ascending=False).reset_index()
        if major_info.empty:
            major_most_score = 0
        else:
            major_most_score = float(major_info.at[0, 'SCORE'])
        major_year_info = all_year_info[all_year_info['MAJOR'] == major]
        major_year_count = major_year_info.shape[0]
        # todo 处理标准为累计记分大于2分，标准暂不清楚
        major_punish = major_year_infos[major_year_infos['SCORE'] >= 2].shape[0] - \
                       major_month_infos[major_month_infos['SCORE'] >= 2].shape[0]

        values = [major] + [
            major_year_info[major_year_info['level'] == i].shape[0]
            for i in range(1, 8)
        ]
        rst.append({
            "major": major,
            "major_count": major_count,
            "major_most_score": major_most_score,
            "major_year_punish": major_year_count,
            "major_punish": major_punish,
            "levels": levels,
            "totals": totals,
            "table": values,
            "value": detail_data
        })
    return rst


def _get_evaluate_major_detail_item_data(major_info, major):
    """履职信息各专业突出问题数据

    Arguments:
        major_info {dataframe} -- 各专业原始数据
        major {str} -- 专业名称

    Returns:
        dict -- 结果
    """
    major_limit = {"车务": 10, "机务": 6, "工务": 8, "电务": 6, "供电": 6, "车辆": 6}
    major_data = major_info.groupby(['CODE', 'SITUATION'])
    detail_item = []
    for name, group in major_data:
        score_count = group.shape[0]
        score_person = int(group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
        detail_item.append({
            'detail_name': name[1],
            'code': name[0],
            'major': major,
            'score_count': score_count,
            'score_person': score_person
        })
    detail_data = pd.DataFrame(detail_item)
    if detail_data.empty:
        return []
    detail_data = detail_data.sort_values(
        by='score_count', ascending=False).head(major_limit.get(major))
    return [
        json.loads(detail_data.loc[index].T.to_json())
        for index in detail_data.index
    ]


def get_niubi_evaluate_situation():
    """局领导履职情况分析

    Returns:
        dict  -- 结果
    """
    # 评价信息表里，gradation字段表示被评价人的职位层级，
    # 对应id为FK_PERSON_GRADATION_RATIO_ID字段。
    # 局管领导人员(GRADATION) -- 1(FK_PERSON_GRADATION_RATIO_ID)
    niubi_data = EVALUATE_INFO[EVALUATE_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1])]
    # 去重
    niubi_data.drop_duplicates(subset=['PK_ID'], inplace=True)
    total = niubi_data.shape[0]
    total_person = len(set(niubi_data['RESPONSIBE_ID_CARD']))
    month_niubi_data = LAST_MONTH_EVALUATE_INFO[
        LAST_MONTH_EVALUATE_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1])]
    major_counts = []
    for major in MAJORS:
        major_counts.append({
            'name':
                major,
            'count':
                niubi_data[niubi_data['MAJOR'] == major].shape[0]
        })
    # 专业表格
    item_rst = []
    for item in items_code:
        major_rst = []
        for major in MAJORS:
            major_rst.append({'name': major, 'count': 0, 'ring': 0})
        major_rst.append({'name': '合计', 'count': 0, 'ring': 0})
        item_data = niubi_data[niubi_data['CODE'].str.contains(item)]
        item_month_data = month_niubi_data[month_niubi_data['CODE'].str.contains(item)]
        if item_data.empty is False:
            major_rst = _get_major_evaluate_table(item_data, item_month_data)
        item_rst.append(major_rst)
    # 问题统计
    niubi_info = niubi_data.groupby(['SITUATION'])
    pro_rst = []
    for name, group in niubi_info:
        pro_count = group.shape[0]
        pro_person = len(set(group['RESPONSIBE_ID_CARD']))
        pro_rst.append({
            "name": name,
            "count": pro_count,
            "person": pro_person
        })
    # 一分以上人员信息
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME', 'SHOPS']
    niubi_data = niubi_data.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    punish_data = niubi_data[niubi_data['SCORE'] >= 1]
    punish_rst = [{
        "name": punish_data.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": punish_data.at[index, 'SHOPS'],
        "job": punish_data.at[index, 'JOB'],
        "score": round(float(punish_data.at[index, 'SCORE']), 1)
    } for index in punish_data.index]

    # 全年干部表格表格
    year_info = EVALUATE_YEAR_INFO[EVALUATE_YEAR_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1])]
    month_info = EVALUATE_YEAR_MONTH_INFO[EVALUATE_YEAR_MONTH_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin([1])]

    # 去重
    year_info.drop_duplicates(subset=['PK_ID'], inplace=True)
    year_data = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    month_data = month_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_carde_total_score_table(year_data, niubi_data, month_data)
    # 累计处理人次
    year_punish = _get_punish_content(year_data, niubi_data, month_data)
    rst = {
        "names": all_items,
        "total": total,
        "total_person": total_person,
        "table": item_rst,
        "problem_total": pro_rst,
        "major_total": major_counts,
        "punish_p": punish_rst,
        "year_table": year_table,
        "year_punish": year_punish
    }
    return rst


def get_branch_level_carbe_situation(level):
    """正副科职履职信息数据

    Arguments:
        level {str} -- 正副科

    Returns:
        dict -- 结果
    """
    # 副科级(GRADATION) -- 3(FK_PERSON_GRADATION_RATIO_ID)
    # 正科级(GRADATION) -- 2(FK_PERSON_GRADATION_RATIO_ID)
    if level == '副科级':
        levels = ['正处级', '副处级', '正科级']
        fk_person_gradation_ratio_id = [3, 4]
    else:
        fk_person_gradation_ratio_id = [2]
    level_info = EVALUATE_INFO[EVALUATE_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin(
        fk_person_gradation_ratio_id)]
    # 去重
    level_info.drop_duplicates(subset=['PK_ID'], inplace=True)
    level_count = level_info.shape[0]
    level_person = len(set(level_info['RESPONSIBE_ID_CARD']))
    problem_info = level_info.groupby(['CODE', 'SITUATION']).size().reset_index().rename(columns={0: 'count'}).\
        sort_values(by='count', ascending=False).head(8)
    problem_rst = [
        problem_info.at[index, 'SITUATION'] for index in problem_info.index
    ]
    major_rst = []
    for major in MAJORS:
        major_info = level_info[level_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_person = len(set(major_info['RESPONSIBE_ID_CARD']))
        major_rst.append({
            "major_name": major,
            "major_count": major_count,
            "major_person": major_person
        })
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    level_info = level_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    high_score_rst = [{
        "name": level_info.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": level_info.at[index, 'STATION'],
        "job": level_info.at[index, 'JOB'],
        "score": float(level_info.at[index, 'SCORE'])
    } for index in level_info.head(5).index]
    # 计算年评价
    year_info = EVALUATE_YEAR_INFO[
        (EVALUATE_YEAR_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin(
            fk_person_gradation_ratio_id))]
    year_info.drop_duplicates(subset=['PK_ID'], inplace=True)
    year_info = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()

    month_info = EVALUATE_YEAR_MONTH_INFO[
        (EVALUATE_YEAR_MONTH_INFO['FK_PERSON_GRADATION_RATIO_ID'].isin(
            fk_person_gradation_ratio_id))]

    month_info.drop_duplicates(subset=['PK_ID'], inplace=True)
    month_info = month_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()

    year_table = _get_carde_total_score_table(year_info, level_info, month_info)
    year_punish = _get_punish_content(year_info, level_info, month_info)
    result = {
        "total_count": level_count,
        "total_person": level_person,
        "problem_situation": problem_rst,
        "major_situation": major_rst,
        "high_score_situation": high_score_rst,
        "level_table": year_table,
        "punish_situation": year_punish
    }
    return result


def _get_carde_total_score_table(year_data, now_data, month_info):
    """获取干部总体得分表格数据

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict  -- jieguo
    """
    year_data['level'] = year_data['SCORE'].apply(singe_score_section)
    now_data['level'] = now_data['SCORE'].apply(singe_score_section)
    month_info['level'] = month_info['SCORE'].apply(singe_score_section)
    levels = []
    year_counts = []
    last_counts = []
    for level in range(1, 8):
        levels.append(level_name.get(level))
        year_counts.append(year_data[year_data['level'] == level].shape[0])
        last_counts.append(
            year_data[year_data['level'] == level].shape[0] - month_info[month_info['level'] == level].shape[0])
    year_table = {
        'total': year_data.shape[0],
        'levels': levels,
        'counts': year_counts,
        'last_counts': last_counts
    }
    return year_table


def _get_punish_content(year_data, now_data, month_data):
    """获取处理结果信息

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict -- 结果
    """
    year_punish = {
        'year_count': year_data[year_data['SCORE'] >= 2].shape[0],
        'month_count': year_data[year_data['SCORE'] >= 2].shape[0] - month_data[month_data['SCORE'] >= 2].shape[0],
        'most_score': float(year_data.at[0, 'SCORE']),
        'status': 1 if len(
            now_data[now_data['RESPONSIBE_PERSON_NAME'] == year_data.at[0, 'RESPONSIBE_PERSON_NAME']]) > 0 else 0,
        'most_person': year_data.at[0, 'RESPONSIBE_PERSON_NAME'],
        'most_station': year_data.at[0, 'STATION'],
        'most_job': year_data.at[0, 'JOB'],
    }
    return year_punish


# 分析日报详情信息查询语句
# a.MONITOR_PROBLEM_DESCRIPTION,
# a.EVALUATION_PROBLEM_DESCRIPTION,
# a.RISK_WARNING,
# a.CENTER_PROBLEM,
# analysis_daily_report_sql = """SELECT
#         a.CHECK_POSTION,
#         b.TYPE,
#         b.FLAG,
#         b.MONTHLY_COMPLETE,
#         b.MONTHLY_PROBLEM_NUMBER,
#         c.NAME AS MAJOR
#     FROM
#         t_analysis_center_daily AS a
#         LEFT JOIN t_analysis_center_daily_details AS b
#             ON b.FK_ANALYSIS_CENTER_DAILY_ID = a.PK_ID
#         LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.PROFESSION_ID
#     WHERE
#         a.DATE = '{}'
# """


def get_analysis_center_work_content(start_date, end_date):
    belong_profession_dic = {
        '客运': 898,
        '货运': 899,
        '运输': 897,
    }
    analysis_ctr_review_data = pd_query(NEW_CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID'])
    check_info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date))
    analysis_daily = pd_query(analysis_daily_sql.format(start_date, end_date))
    check_media_data = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date))
    main_post = pd_query(main_post_sql)
    department_data = pd_query(DEPARTMENT_INFO_SQL)
    zhanduan_list = department_data[
        (department_data['SHORT_NAME'] != '') &
        (department_data['SHORT_NAME'].notnull()) &
        (department_data['TYPE3'] == '999900020014000315DC')
        ]['NAME'].values.tolist()
    rst = []
    postion_rst = []
    _all_dpids = [j for i in _OFFICE_ANALYSIS_CENTER_DICT.values() for j in i]
    _all_sub_dpids = _get_all_sub_ids(_all_dpids, department_data,
                                      high_colunm='FK_PARENT_ID', sub_colunm='DEPARTMENT_ID')
    _all_check_dpnames = department_data[department_data['DEPARTMENT_ID'].isin(_all_sub_dpids)][
        'ALL_NAME'].values.tolist()
    _all_check_media_data = check_media_data[
        check_media_data['DEPARTMENT_ALL_NAME'].isin(_all_check_dpnames)]
    for major, major_dpids in _OFFICE_ANALYSIS_CENTER_DICT.items():
        value = []
        # 获取对应分析中心的检查信息等
        # 先获取该分析中心下所有部门
        _sub_dpids = _get_all_sub_ids(major_dpids, department_data,
                                      high_colunm='FK_PARENT_ID', sub_colunm='DEPARTMENT_ID')
        _all_names = department_data[department_data['DEPARTMENT_ID'].isin(_sub_dpids)]['ALL_NAME'].values.tolist()
        # 复查信息
        _review_data = analysis_ctr_review_data[
            analysis_ctr_review_data['FK_REVIEW_DEPARTMENT_ID'].isin(_sub_dpids)
        ]
        # 检查信息
        _check_info_data = check_info_data[
            check_info_data['DEPARTMENT_ALL_NAME'].isin(
                _all_names
            )
        ]
        # 检查媒体信息
        _check_media_data = check_media_data[
            check_media_data['DEPARTMENT_ALL_NAME'].isin(
                _all_names
            )
        ]
        # 履职评价信息
        _evaluate_info_data = EVALUATE_INFO[
            EVALUATE_INFO['CHECK_PERSON_DEPARTMENT_ID'].isin(
                _sub_dpids
            )
        ]
        # 分析中心日报
        _analysis_daily_data = analysis_daily[analysis_daily['NAME'].isin([major])]
        # 去重，干部履职
        _check_media_data.drop_duplicates(subset=['PK_ID'], inplace=True)
        _check_info_data.drop_duplicates(subset=['PK_ID'], inplace=True)
        _review_data.drop_duplicates(subset=['PK_ID'], inplace=True)
        _review_data = _review_data[_review_data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
        _evaluate_info_data.drop_duplicates(subset=['PK_ID'], inplace=True)
        _evaluate_info_data = _evaluate_info_data[
            _evaluate_info_data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
        # major_data = center_data[center_data['MAJOR'] == major]
        if _review_data.empty & _check_info_data.empty & _check_media_data.empty:
            value = [0 for i in range(7)]
            if major == '综合组':
                continue
            postion_rst.append(0)
        else:
            # 音视频调阅（小时）
            value.append(round(
                sum(_check_media_data['COST_TIME'].values.tolist())
                , 1))
            # 音视频复查（条）
            value.append(
                int(_analysis_daily_data[_analysis_daily_data['FLAG'] == 2]['MONTHLY_COMPLETE'].sum())
            )
            # 逐条评价复查（条）
            value.append(
                int(_analysis_daily_data[_analysis_daily_data['FLAG'] == 5]['MONTHLY_COMPLETE'].sum())
            )
            # 定期评价复查（人）
            value.append(
                int(_analysis_daily_data[_analysis_daily_data['FLAG'] == 6]['MONTHLY_COMPLETE'].sum())
            )
            # 发现履职问题（个）
            value.append(
                int(_analysis_daily_data[_analysis_daily_data['FLAG'].isin([3, 4, 7])]['MONTHLY_PROBLEM_NUMBER'].sum())
            )
            # 发现现场问题（个）
            value.append(
                len(
                    set(_check_info_data[_check_info_data['CHECK_WAY'].isin([1, 2, 3, 4])][
                            'FK_CHECK_PROBLEM_ID'].values.tolist()))
            )
            # 音视频调阅不认真
            value.append(
                _evaluate_info_data[_evaluate_info_data['ITEM_NAME'].str.contains('音视频运用管理')].shape[0]
            )
            # 检查站段数（个）
            value.append(
                len(_check_info_data['ALL_NAME'].unique())
            )
            if major == '车务':
                # 主要是获取相应的站段
                _station = main_post[main_post['ALL_NAME'] == major]
                postion_rst.append(_calc_media_check_position(_all_check_media_data, _station))
            elif major == '综合':
                pass
            else:
                _station = main_post[main_post['ALL_NAME'] == major]
                postion_rst.append(_calc_media_check_position(_all_check_media_data, _station))
        # todo 检查重点岗位渲染顺序应该为：运输，货运，客运, 机务部, 工务部, 电务部, 供电部, 车辆部
        rst.append({'major': major, 'value': value})
    result = rst.copy()
    names = [
        '音视频调阅（小时）', '音视频复查（条）', '逐条评价复查（条）', '定期评价复查（人次）', '发现履职问题（个）',
        '发现现场问题（个）', '音视频调阅不认真', '检查站段数（个）'
    ]
    result.append({
        'major': '合计',
        'value': [
            sum([major_rst['value'][i] for major_rst in rst])
            for i in range(0, 8)
        ],
        'postion': postion_rst,
        'wanc_position': sum([i['count'] for i in postion_rst]),
        'all_position': sum([i['all_count'] for i in postion_rst])
    })
    return {"name": names, "value": result}


def get_center_daily_detail_total(data, Flag, Type, column):
    total = sum([
        data.at[idx, column] for idx in data[(data['FLAG'].isin(Flag))
                                             & (data['TYPE'].isin(Type))].index
    ])
    return int(total)


def get_data_from_mongo(start_date, end_date, func):
    # 当月数据
    data = func(start_date, end_date)
    # 上月数据
    month_data = func(LAST_MONTH_START, LAST_MONTH_END)
    if month_data.empty:
        month_data = pd.DataFrame(columns=data.columns)
    # 获取去年本月数据
    year_data = func(LAST_YEAR_START, LAST_YEAR_END)
    if year_data.empty:
        year_data = pd.DataFrame(columns=data.columns)
    return [data, month_data, year_data]


def get_level_carde_evaluate(start_date, end_date):
    global ACCIDENT_DATAS, INFO_DATAS, PRO_DATAS  # accident_datas, info_datas, pro_datas
    # 事故故障情况
    ACCIDENT_DATAS = get_data_from_mongo(start_date, end_date,
                                         _get_safety_produce_data)
    # 检查信息数据
    INFO_DATAS = get_data_from_mongo(start_date, end_date, get_check_info_data)
    # 检查问题数据
    PRO_DATAS = get_data_from_mongo(start_date, end_date, get_check_problem_data)
    major_quality_analysis = get_major_quality_analysis(start_date, end_date)
    station_quality_analysis = get_station_quality_analysis(start_date, end_date)
    station_leader_analysis = get_station_leader_analysis(start_date, end_date)
    result = {
        "total_evaluate_analysis": {
            "major_quality_analysis": major_quality_analysis,
            "station_quality_analysis": station_quality_analysis,
            "station_leader_analysis": station_leader_analysis
        }
    }
    return result


def _get_acc_err_check_eva_info(acc_datas, err_datas, pro_datas, info_datas,
                                eva_data):
    # 事故比较情况
    acc_counts = [data.shape[0] for data in acc_datas]
    acc_total = acc_counts[0]
    acc_ratio = calculate_year_and_ring_ratio(acc_counts)
    # 故障比较情况
    err_counts = [data.shape[0] for data in err_datas]
    err_total = err_counts[0]
    err_ratio = calculate_year_and_ring_ratio(err_counts)
    # 检查信息比较情况
    info_counts = [sum(list(data['COUNT'])) for data in info_datas]
    info_total = info_counts[0]
    info_ratio = calculate_year_and_ring_ratio(info_counts)
    # 检查问题比较情况
    pro_counts = [sum(list(data['TIMES'])) for data in pro_datas]
    pro_total = pro_counts[0]
    pro_ratio = calculate_year_and_ring_ratio(pro_counts)
    # 严重问题比较情况
    pro_ser_counts = [sum(list(data['SERIOUS_COUNT'])) for data in pro_datas]
    pro_ser_total = pro_ser_counts[0]
    pro_ser_ratio = calculate_year_and_ring_ratio(pro_ser_counts)
    # 检查问题质量分
    pro_score_counts = [sum(list(data['SCORE'])) for data in pro_datas]
    pro_score_total = pro_score_counts[0]
    pro_score_ratio = calculate_year_and_ring_ratio(pro_score_counts)
    # 履职评价信息
    total = eva_data.shape[0]
    values = [
        eva_data[eva_data['ITEM_NAME'] == item].shape[0] for item in all_items
    ]
    value = {
        "acc_total": acc_total,
        "acc_ratio": acc_ratio,
        "err_total": err_total,
        "err_ratio": err_ratio,
        "info_total": info_total,
        "info_ratio": info_ratio,
        "pro_total": pro_total,
        "pro_ratio": pro_ratio,
        "pro_ser_total": pro_ser_total,
        "pro_ser_ratio": pro_ser_ratio,
        "pro_score_total": pro_score_total,
        "pro_score_ratio": pro_score_ratio,
        "evaluate": {
            "total":
                total,
            "value": [{
                "name": name,
                "count": values[idx]
            } for idx, name in enumerate(all_items)]
        }
    }
    return value


def _get_evaluate_table(evaluate):
    evaluate_way = {
        0: "自动评价",
        1: "逐条评价",
        2: "定期评价",
        3: "阶段评价",
    }
    major_evaluate_table = []
    for index in evaluate.index:
        value = evaluate.loc[index]
        major_evaluate_table.append({
            "all_name":
                value['ALL_NAME'],
            "name":
                value['RESPONSIBE_PERSON_NAME'],
            "job":
                value['GRADATION'],
            "y_m":
                f"{value['YEAR']}/{value['MONTH']:0>2}",
            "time":
                value['CREATE_TIME'],
            "way":
                evaluate_way[value['EVALUATE_WAY']],
            "content":
                value['EVALUATE_CONTENT'],
            "score":
                round(float(value['SCORE']), 1)
        })
    return major_evaluate_table


def get_major_quality_analysis(start_date, end_date):
    try:
        major_evaluate = EVALUATE_INFO[
            (EVALUATE_INFO.STATION.str.contains("处"))
            & (EVALUATE_INFO['MAJOR'].isin(MAJORS))]
        major_evaluate.drop_duplicates(subset=['PK_ID'], inplace=True)
        major_evaluate_table = _get_evaluate_table(major_evaluate)
        acc_datas = [
            data[(data['PROFESSION'] == '运输') & (data['MAIN_TYPE'] == 1)]
            for data in ACCIDENT_DATAS
        ]
        err_datas = [
            data[(data['PROFESSION'] == '运输') & (data['MAIN_TYPE'] == 2)]
            for data in ACCIDENT_DATAS
        ]
        problem_datas = [data[data['MAJOR'] == '运输'].drop_duplicates(subset=['PK_ID']) for data in PRO_DATAS]
        check_datas = [data[data['MAJOR'] == '运输'] for data in INFO_DATAS]
        eva_data = EVALUATE_INFO[EVALUATE_INFO['STATION'] == '运输处']
        eva_data.drop_duplicates(subset=['PK_ID'], inplace=True)
        value = _get_acc_err_check_eva_info(
            acc_datas, err_datas, problem_datas, check_datas, eva_data)
        result = {
            "major_evaluate_table": major_evaluate_table,
            "yunshu_accident_situation": value
        }
        return result
    except Exception:
        return {}


def get_station_quality_analysis(start_date, end_date):
    res_acc_datas = [
        data[(data['MAIN_TYPE'] == 1) & (data['RESPONSIBILITY_NAME'] == 1)]
        for data in ACCIDENT_DATAS
    ]
    res_err_datas = [data[data['MAIN_TYPE'] == 2].drop_duplicates(subset=['PK_ID']) for data in ACCIDENT_DATAS]
    major_rst = []
    for major in MAJORS:
        major_acc_datas = [
            data[data['MAJOR'] == major].drop_duplicates(subset=['PK_ID']) for data in res_acc_datas
        ]
        major_err_datas = [
            data[data['MAJOR'] == major] for data in res_err_datas
        ]
        sta_acc = list(set(major_acc_datas[0]['STATION']))
        limit = 3
        if major == '工务':
            limit = 5
        sta_err = major_err_datas[0]['STATION'].value_counts().head(limit)
        stations = list(set(sta_acc + list(sta_err.index)))
        rst = []
        for station in stations:
            sta_info = [
                data[data['STATION'] == station] for data in INFO_DATAS
            ]
            sta_pro = [data[data['STATION'] == station] for data in PRO_DATAS]
            sta_acc_datas = [
                data[(data['STATION'] == station)] for data in major_acc_datas
            ]
            sta_err_datas = [
                data[data['STATION'] == station] for data in major_err_datas
            ]
            sta_evaluate = EVALUATE_INFO[EVALUATE_INFO['STATION'] == station]
            value = _get_acc_err_check_eva_info(
                sta_acc_datas, sta_err_datas, sta_pro, sta_info, sta_evaluate)
            rst.append({"station": station, "value": value})
        major_rst.append({"major": major, "value": rst})
    return major_rst


# check_info_sql = """SELECT
#         a.CHECK_WAY,
#         a.DEPARTMENT_ALL_NAME AS ALL_NAME,
#         a.IS_YECHA,
#         a.PROBLEM_NUMBER,
#         b.COST_TIME
#     FROM
#         t_check_info AS a
#         LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
#     WHERE
#         a.DEPARTMENT_ALL_NAME LIKE "%%领导%%"
#         AND a.END_CHECK_TIME BETWEEN '{}' AND  '{} :23:59:59'
#         """

# check_problem_sql = """SELECT
#         a.LEVEL,
#         a.PROBLEM_SCORE,
#         b.DEPARTMENT_ALL_NAME AS ALL_NAME
#     FROM
#         t_check_problem AS a
#         LEFT JOIN t_check_info AS b ON b.PK_ID = a.FK_CHECK_INFO_ID
#     WHERE
#         b.DEPARTMENT_ALL_NAME LIKE "%%领导%%"
#         AND b.END_CHECK_TIME >= '{}'
#         AND b.END_CHECK_TIME <= '{}'
#     """


def _get_evaluate_report_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_', 'history_']
    doc = dict()
    for prefix in prefix_list:
        coll_name = prefix + 'detail_evaluate_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "BUSINESS_CLASSIFY": 1,
            "ID_CARD": 1,
            "STATION": 1,
            "score": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def _get_health_index(month):
    prefix_list = ['monthly_', 'history_']
    doc = []
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def get_carde_quality_analysis(start_date, end_date):
    # 领导班子情况
    station_carde_count = pd_query(STATION_CARDE_COUNT_SQL)
    # 检查信息情况
    check_info = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date))
    check_info = check_info[check_info['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    check_info = check_info.fillna(0)
    set_keys = ['CHECK_WAY', 'IS_YECHA', 'PROBLEM_NUMBER', 'COST_TIME']
    check_info = check_info.set_index(set_keys)['DEPARTMENT_ALL_NAME'].str.split(
        ',', expand=True).stack().reset_index().rename(columns={0: 'DEPARTMENT_ALL_NAME'})
    check_info['WAY'] = check_info['CHECK_WAY'].apply(
        lambda x: 1 if x == 1 else 0)
    check_info['STATION'] = check_info['DEPARTMENT_ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del check_info['CHECK_WAY'], check_info['DEPARTMENT_ALL_NAME']
    check_info['CHECK_COUNT'] = 1
    check_data = check_info.groupby('STATION').sum().reset_index()
    data = pd.merge(station_carde_count, check_data, how='left', on='STATION')
    data = data[(~data['WAY'].isnull()) & (~data.STATION.str.contains('处')) & (~data.STATION.str.contains('所'))]
    # 当前月份
    date = dt.strptime(end_date, "%Y-%m-%d")
    if date.day >= current_app.config.get('UPDATE_DAY'):
        date = date + relativedelta(months=1)
    month = date.year * 100 + date.month
    # 履职报告得分
    eva_report_data = _get_evaluate_report_data(month)
    if eva_report_data.empty is False:
        eva_report_data = eva_report_data.fillna(value={'BUSINESS_CLASSIFY': 'a'})
        eva_report_data = eva_report_data.drop_duplicates('ID_CARD')
        del eva_report_data['ID_CARD']
        eva_report_data = eva_report_data[
            ~(eva_report_data['BUSINESS_CLASSIFY'].isnull())
            & (eva_report_data.BUSINESS_CLASSIFY.str.contains('领导'))]
        evaluate_score = eva_report_data.groupby('STATION').sum().reset_index()
        data = pd.merge(data, evaluate_score, how='left', on='STATION')
        data['score_aver'] = data.apply(
            lambda x: round(x['score'] / x['TOTAL'], 1), axis=1)
        data['score_rank'] = data.groupby('MAJOR')['score_aver'].rank()
    else:
        data['score_aver'] = 0
        data['score'] = 0
        data['score_rank'] = 0
    # 安全综合指数得分
    health_data = _get_health_index(month)
    if health_data.empty is False:
        health_data = health_data.rename(
            columns={
                'RANK': 'HEALTH_RANK',
                'DEPARTMENT_NAME': 'STATION',
                'SCORE': 'HEALTH_SCORE'
            })
        data = pd.merge(data, health_data, how='left', on='STATION')
    else:
        data['HEALTH_RANK'] = 0
        data['HEALTH_SCORE'] = 0
    # 检查问题情况
    problem_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date))
    problem_data = problem_data[problem_data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    problem_data = problem_data.set_index(
        ['LEVEL', 'PROBLEM_SCORE'])['DEPARTMENT_ALL_NAME'].str.split(
        ',',
        expand=True).stack().reset_index().rename(columns={0: 'DEPARTMENT_ALL_NAME'})
    problem_data['HIGH_PROBLEM'] = problem_data['LEVEL'].apply(
        lambda x: 1 if x in 'ABCE1E2E3' else 0)
    problem_data['STATION'] = problem_data['DEPARTMENT_ALL_NAME'].apply(
        lambda x: x.split('-')[0])
    del problem_data['DEPARTMENT_ALL_NAME']
    problem_data = problem_data.groupby('STATION').sum().reset_index()
    data = pd.merge(data, problem_data, how='left', on='STATION')
    # 履职评价详情
    evaluate_data = EVALUATE_INFO[
        (~EVALUATE_INFO['BUSINESS_CLASSIFY'].isnull())
        & (EVALUATE_INFO.BUSINESS_CLASSIFY.str.contains('领导'))]
    # result = []
    major_rst = []
    major_desc_rst = []
    data = data.fillna(0)
    for major in MAJORS:
        desc_rst = []
        major_data = data[data['MAJOR'] == major].copy()
        # if str(set(major_data['HEALTH_SCORE'])):
        major_data = major_data.sort_values(by='HEALTH_SCORE')
        major_total = sum(list(data['TOTAL']))
        major_check_aver = int(sum(list(data['CHECK_COUNT'])) / major_total)
        major_dy_aver = round(sum(list(data['COST_TIME'])) / major_total, 1)
        major_problem_aver = round(
            sum(list(data['PROBLEM_NUMBER'])) / major_total, 1)
        major_yecha_aver = int(sum(list(data['IS_YECHA'])) / major_total)
        major_high_aver = int(sum(list(data['HIGH_PROBLEM'])) / major_total)
        major_proscore_aver = round(
            sum(list(data['PROBLEM_SCORE'])) / major_total, 1)
        del major_data['level_4'], major_data['level_2']
        a = 1
        for index in major_data.index:
            value = major_data.loc[index].copy()
            station = value['STATION']
            value['CHECK_AVER'] = int(value['CHECK_COUNT'] / value['TOTAL'])
            value['CHECK_DIFF'] = value['CHECK_AVER'] - major_check_aver
            value['DY_AVER'] = int(value['COST_TIME'] / value['TOTAL'])
            value['DY_DIFF'] = value['DY_AVER'] - major_dy_aver
            value['PROBLEM_AVER'] = int(
                value['PROBLEM_NUMBER'] / value['TOTAL'])
            value['PROBLEM_DIFF'] = value['PROBLEM_AVER'] - major_problem_aver
            value['YECHA_AVER'] = int(value['IS_YECHA'] / value['TOTAL'])
            value['YECHA_DIFF'] = value['YECHA_AVER'] - major_yecha_aver
            value['HIGH_PRO_AVER'] = int(
                value['HIGH_PROBLEM'] / value['TOTAL'])
            value['HIGH_PRO_DIFF'] = value['HIGH_PRO_AVER'] - major_high_aver
            value['PRO_SCORE_AVER'] = int(
                value['PROBLEM_SCORE'] / value['TOTAL'])
            value['PRO_SCORE_DIFF'] = value[
                                          'PRO_SCORE_AVER'] - major_proscore_aver
            major_rst.append({
                "station": station,
                "value": json.loads(value.T.to_json())
            })
            limit = 3
            if major in ['车务', '工务']:
                limit = 5
            if a <= limit:  # 文字描述车务工务显示后五名，其余显示后三名
                # 履职情况
                sta_eva = evaluate_data[evaluate_data['STATION'] == station]
                if sta_eva.empty:
                    eva_total = 0
                    eva_score_total = 0
                    luju = 0
                    luju_eva_rst = []
                    ziping = 0
                    ziping_eva_rst = []
                else:
                    eva_total = sta_eva.shape[0]
                    eva_score_total = sum(list(sta_eva['SCORE']))
                    luju_eva = sta_eva[sta_eva['CHECK_TYPE'] == 1]
                    if luju_eva.empty:
                        luju_eva_rst = []
                        luju = 0
                    else:
                        luju = luju_eva.shape[0]
                        luju_eva = luju_eva.groupby('CODE_ADDITION').size(
                        ).rename(columns={0: 'count'})
                        luju_eva_rst = [{
                            "CODE": index,
                            "COUNT": int(luju_eva.at[index])
                        } for index in luju_eva.index]
                    ziping_eva = sta_eva[sta_eva['CHECK_TYPE'] == 2]
                    if ziping_eva.empty:
                        ziping_eva_rst = []
                        ziping = 0
                    else:
                        ziping = ziping_eva.shape[0]
                        ziping_eva = ziping_eva.groupby('CODE_ADDITION').size(
                        ).rename(columns={0: 'count'})
                        ziping_eva_rst = [{
                            "CODE": index,
                            "COUNT": int(ziping_eva.at[index])
                        } for index in ziping_eva.index]
                desc_rst.append({
                    "station": station,
                    "value": json.loads(value.T.to_json()),
                    "eva_value": {
                        "total": eva_total,
                        "score_total": eva_score_total,
                        "luju": luju,
                        "luju_value": luju_eva_rst,
                        "ziping": ziping,
                        "ziping_value": ziping_eva_rst
                    }
                })
            a += 1
        major_desc_rst.append({"major": major, "value": desc_rst})
    return major_rst, major_desc_rst


def get_station_leader_analysis(start_date, end_date):
    juguan_evaluate = EVALUATE_INFO[EVALUATE_INFO['BUSINESS_CLASSIFY'] == '领导']
    table, description = get_carde_quality_analysis(start_date, end_date)
    juguan_leader_evaluate_table = _get_evaluate_table(juguan_evaluate)
    result = {
        "station_carde_quality_analysis": table,
        "station_carde_quality_analysis_desc": description,
        "juguan_leader_evaluate_table": juguan_leader_evaluate_table
    }
    return result


def _get_safety_produce_data(start_date, end_date, major=None):
    """从mongo中获取安全生产信息

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 安全生产信息
    """
    start_date = int(start_date.replace("-", ''))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    result = []
    match = {
        'DATE': {
            '$gte': start_date,
            "$lte": end_date
        }
    }
    if major is not None:
        match['MAJOR'] = major
    for prefix in prefix_list:
        coll_name = prefix + 'detail_safety_produce_info'
        doc = list(mongo.db[coll_name].find(match, {"_id": 0}))
        if len(doc) == 0:
            doc = [{
                'PK_ID': 1,
                'MAIN_TYPE': -1,
                'DETAIL_TYPE': -1,
                'CATEGORY': 0.0,
                'OVERVIEW': '',
                'STATUS': -1,
                'REASON': '',
                'PROFESSION': '',
                'RESPONSIBILITY_UNIT': '',
                'PONDERANCE_NUMBER': 0.0,
                'RANK': 0,
                'CODE': '',
                'NAME': '',
                'ACCIDENT_IDENTIFIED_NUMBER': -1,
                'TYPE3': '',
                'RISK_NAME': '',
                'DIRECT_REASON': ' ',
                'RESPONSIBILITY_IDENTIFIED': 0.0,
                'RESPONSIBILITY_NAME': '',
                'RESPONSIBILITY_IDENTIFIED_NAME': '',
                'MAIN_CLASS': '',
                'DETAIL_CLASS': -1,
                'MAJOR': ''
            }]
        else:
            doc = doc
        result = result + doc
    data = pd.DataFrame(result)
    if data.empty is False:
        dep_data = pd_query(DEPARTMENT_INFO_SQL)
        dep_data = dep_data[['DEPARTMENT_ID', 'NAME']]
        dep_data.rename(columns={"NAME": 'STATION'}, inplace=True)
        # data = data.drop_duplicates('PK_ID')
        # 暂时不去重，因为一个生产信息可以关联多个责任部门
        data['TYPE3'] = data['TYPE3'].apply(lambda x: str(x))
        data = pd.merge(
            data,
            dep_data,
            how='left',
            left_on='TYPE3',
            right_on='DEPARTMENT_ID')
        del data['DEPARTMENT_ID']
        data['type'] = data['CODE'].apply(devide_type)
    # else:

    return data


def _calc_new_add_count(now_data, till_this_moment_data, colunm):
    """
    计算当前数据，与截止数据关于某个类目的新增数目
    :param now_data:
    :param till_this_moment_data:
    :param colunm:
    :return:
    """
    add = set(till_this_moment_data[colunm]) - set(now_data[colunm])
    return len(add)


def _get_all_sub_ids(sub_ids, all_data, high_colunm, sub_colunm):
    """
    获取id以及子集的ids
    """
    ret = [i for i in sub_ids]
    parent_ids = [i for i in sub_ids]
    while True:
        df = all_data[all_data[high_colunm].isin(parent_ids)]
        if len(df) > 0:
            parent_ids = df[sub_colunm].values.tolist()
            ret.extend(parent_ids)
        else:
            break
    return ret


def _calc_check_things_set(multiple_list):
    """
    计算多重列表中，去重后的个数
    例如：['a, b', 'b']
    默认字符间连接是--- ','
    return ['a', 'b']
    """
    rst = []
    for item in multiple_list:
        _list = item.split(',')
        for _it in _list:
            if _it in rst:
                continue
            rst.append(_it)
    return rst


def _calc_media_check_position(data, stations):
    """
    统计重点检查岗位数量，限定检查地点
    :param data: 监控调阅岗位总数
    :param stations: 各专业严重岗位
    :return:
    """
    count = 0
    monitor = []
    for i in data['MONITOR_POST_IDS'].dropna():
        monitor = monitor + str(i).strip().split(',')
    s = [i for i in set(monitor)]
    for i in list(stations['PK_ID']):
        if str(i) in s:
            count += 1
    return {
        'count': count,
        'all_count': len(stations)
    }


def _get_major_dpid(major):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(major, None)
