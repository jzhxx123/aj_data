without_accident_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.FK_RESPONSIBILITY_DIVISION_ID,
            a.PROFESSION,
            a.NAME,
            d.NAME AS STATION
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
        WHERE a.OCCURRENCE_TIME <= '{0}'
        """


check_info_sql = """SELECT CHECK_WAY, CHECK_TYPE,PROBLEM_NUMBER
FROM `t_check_info`
WHERE SUBMIT_TIME BETWEEN '{0}' and '{1}'"""

check_problem_sql = """SELECT
b.IS_RED_LINE,b.PROBLEM_CLASSITY_NAME,b.`LEVEL`,b.SERIOUS_VALUE,
b.CHECK_ITEM_NAME,a.CHECK_ADDRESS_NAMES,a.CHECK_TYPE,
b.
FROM t_check_info a, t_check_problem b 
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND a.PK_ID=b.FK_CHECK_INFO_ID
"""

check_risk_sql = """SELECT a.CHECK_WAY, a.CHECK_TYPE,a.PROBLEM_NUMBER,c.ALL_NAME,d.`NAME`
FROM `t_check_info` a
LEFT JOIN t_check_info_and_item b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_risk c on b.FK_CHECK_ITEM_ID=c.PK_ID
LEFT JOIN t_risk d on d.PK_ID = c.PARENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
"""

analysis_center_sql = """SELECT a.*,b.FLAG,b.DAILY_PROBLEM_NUMBER FROM t_analysis_center_daily a
LEFT JOIN t_analysis_center_daily_details b on a.PK_ID=b.FK_ANALYSIS_CENTER_DAILY_ID
where a.DATE between '{0}' AND '{1}'"""


main_info_sql = """select * FROM t_key_information_tracking
WHERE CREATE_TIME BETWEEN '{0}' and '{1}'"""


warning_notification_sql = """SELECT
						a.STATUS,
						a.CREATE_TIME,
						a.HIERARCHY,
						a.CONTENT,
						a.RANK,
						a.APPLY_UUID,
						a.TYPE AS warning_type,
						b.NAME AS duty_department_name,
						b.TYPE AS duty_department_type,
						c.TYPE AS department_type,
						a.DEPARTMENT_NAME AS department_name,
						c.BELONG_PROFESSION_NAME
					FROM
						t_warning_notification AS a
						INNER JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
						INNER JOIN t_department AS c ON b.TYPE3 = c.DEPARTMENT_ID
					WHERE a.CREATE_TIME BETWEEN Date('{0}') AND Date('{1}')"""
