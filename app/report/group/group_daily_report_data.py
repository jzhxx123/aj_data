import threading
import pandas as pd
import numpy as np
from datetime import timedelta, date
from datetime import datetime as dt

from dateutil.relativedelta import relativedelta
from flask import current_app
from app.data.util import pd_query
from app.report.analysis_report_manager import DailyAnalysisReport
from app.report.group.common import TOP10_RISK
from app.report.group.common_sql import CHECK_PROBLEM_DATA_SQL, CHECK_INFO_AND_PERSON_SQL, CHECK_INFO_DATA_SQL
from app.report.group.group_daily_report_sql import *
from app.new_big.util import get_data_from_mongo_by_find
from app.report.util import get_week_interval

local_data = threading.local()

# 每周的其实的天，周一 1， 周日7
ISO_WEEK_START_DAY = 4
ISO_WEEK_END_DAY = 3


def get_data(year, month, day):
    """
    获取报表数据
    :param day:
    :param month:
    :param year:
    :return: {Dict} 报告的数据
    """
    report_date = dt(year, month, day)
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    local_data.date_params = params

    local_data.prob_data_this = pd_query(CHECK_PROBLEM_DATA_SQL.format(params[0][0], params[0][1])).drop_duplicates(subset=['PK_ID']).reset_index()
    local_data.check_person_this = pd_query(CHECK_INFO_AND_PERSON_SQL.format(params[0][0], params[0][1]))
    local_data.check_info_this = pd_query(CHECK_INFO_DATA_SQL.format(params[0][0], params[0][1])).drop_duplicates(subset=['PK_ID']).reset_index()

    first = get_first(report_date)
    second = get_second(report_date)
    third = get_third(report_date)
    fourth = get_fourth(report_date)

    result = {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "GROUP",
        "file_name": '{0}集团安全分析日报.xlsx',
        "created_at": dt.now(),
        "first": first,
        "second": second,
        "third": third,
        "fourth": fourth
    }
    return result


def get_first(report_date):
    # 安全情况
    first_one = get_first_one(report_date)
    # 事故故障情况
    first_two = get_first_two(report_date)
    # 本周
    first_two_week = get_first_two_week(report_date)
    # 本月
    first_two_month = get_first_two_month(report_date)
    return {
        'first_one': first_one,
        'first_two': first_two,
        'first_two_week': first_two_week,
        'first_two_month': first_two_month
    }


def get_first_one(report_date):
    """
    安全生产情况
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format(report_date.strftime('%Y-%m-%d')))
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
        if len(accident) == 0:
            dic['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    content = '截至{0}月{1}日：全局实现无责任一般A类事故{2}天。全局实现安全生产{3}天。' \
        .format(report_date.month, report_date.day, dic['A'], dic['A'])
    # TODO
    # worksheet['B5'] = content
    return content


def get_first_two(report_date):
    """
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    start_date = report_date + timedelta(weeks=-1, days=1)
    start_date_num = start_date.year * 10000 + start_date.month * 100 + start_date.day
    end_date_num = report_date.year * 10000 + report_date.month * 100 + report_date.day

    month, day = report_date.month, report_date.day
    # 获取安全责任数据
    documents = calc_safety_produce_data(start_date_num, end_date_num)
    if len(documents) == 0:
        content = "{0}月{1}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障0件。其中信号故障0件、机车故障0件、动车组故障0件。".format(month, day)
        info = {
            'acc': '无',
            'fault': '无',
            'zh': '无'
        }
    else:
        data = pd.DataFrame(documents)
        data = data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
        acc = len(data[data['MAIN_CLASS'] == '事故'])
        fault = len(data[data['MAIN_CLASS'] == '故障'])
        # 行车、劳动、路外
        safety_list = []
        for i in ['行车', '劳动', '路外']:
            new_data = data[data['RISK_NAME'].str.contains(i)]
            dic = {
                '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
            }
            safety_list.append(dic)
        # 设备故障
        new_data = data[data['MAIN_CLASS'] == '故障']
        safety_list.append({
            '设备故障': fault,
            '信号故障': len(new_data[new_data['NAME'] == '信号设备故障']),
            '机车故障': len(new_data[new_data['NAME'] == '机车故障']),
            '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
        })
        # 事故信息、故障信息
        acc_data = data[data['MAIN_CLASS'] == '事故'].dropna(subset=['REASON'])['REASON'].unique()
        for i in range(len(acc_data)):
            acc_data[i] = '{0}.'.format(i + 1) + acc_data[i]
        fault_data = data[data['MAIN_CLASS'] == '故障'].dropna(subset=['REASON'])['REASON'].unique()
        for i in range(len(fault_data)):
            fault_data[i] = '{0}.'.format(i + 1) + fault_data[i]
        zh_data = data[data['MAIN_CLASS'] == '综合'].dropna(subset=['REASON'])['REASON'].unique()
        for i in range(len(zh_data)):
            zh_data[i] = '{0}.'.format(i + 1) + zh_data[i]
        info = {
            'acc': acc_data.tolist(),
            'fault': fault_data.tolist(),
            'zh': zh_data.tolist()
        }
        # 数据渲染
        content = "{0}月{1}日，发生事故{2}件、故障{3}件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障{4}件。其中信号故障{5}件、机车故障{6}件、动车组故障{7}件。". \
            format(month, day, acc, fault, safety_list[-1]['设备故障'], safety_list[-1]['信号故障'],
                   safety_list[-1]['机车故障'], safety_list[-1]['动车组故障'])
    result = {
        'content': content,
        'info': info
    }
    # TODO
    # worksheet['B6'] = content
    # worksheet['B7'] = '\n'.join(info['acc'])
    # worksheet['B8'] = '\n'.join(info['fault'])
    # worksheet['B9'] = '\n'.join(info['zh'])
    return result


def get_first_two_week(report_date):
    """
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    start_date = report_date + timedelta(weeks=-1, days=1)
    start_date_num = start_date.year * 10000 + start_date.month * 100 + start_date.day
    end_date_num = report_date.year * 10000 + report_date.month * 100 + report_date.day
    # 获取安全责任数据
    documents = calc_safety_produce_data(start_date_num, end_date_num)
    if len(documents) == 0:
        content = "{0}月{1}日-{2}月{3}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障0件。其中信号故障0件、机车故障0件、动车组故障0件。"
        content = content.format(start_date.month, start_date.day, report_date.month, report_date.day)
    else:
        data = pd.DataFrame(documents)
        data = data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
        acc = len(data[data['MAIN_CLASS'] == '事故'])
        fault = len(data[data['MAIN_CLASS'] == '故障'])
        # 行车、劳动、路外
        safety_list = []
        for i in ['行车', '劳动', '路外']:
            new_data = data[data['RISK_NAME'].str.contains(i)]
            dic = {
                '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
            }
            safety_list.append(dic)
        # 设备故障
        new_data = data[data['MAIN_CLASS'] == '故障']
        safety_list.append({
            '设备故障': fault,
            '信号故障': len(new_data[new_data['NAME'] == '信号设备故障']),
            '机车故障': len(new_data[new_data['NAME'] == '机车故障']),
            '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
        })
        content = "{0}月{1}日-{2}月{3}日，发生事故{4}件、故障{5}件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障{6}件。其中信号故障{7}件、机车故障{8}件、动车组故障{9}件。". \
            format(start_date.month, start_date.day, report_date.month, report_date.day,
                   acc, fault, safety_list[-1]['设备故障'], safety_list[-1]['信号故障'],
                   safety_list[-1]['机车故障'], safety_list[-1]['动车组故障'])
    # worksheet['K6'] = content
    return content


def get_first_two_month(report_date):
    """
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    month = report_date.month
    # 求当月的开始、结束日期
    start_date = date(report_date.year, report_date.month, 1)
    end_date = report_date + relativedelta(months=1, days=-1)
    start_date_num = start_date.year * 10000 + start_date.month * 100 + start_date.day
    end_date_num = end_date.year * 10000 + end_date.month * 100 + end_date.day
    # 获取安全责任数据
    documents = calc_safety_produce_data(start_date_num, end_date_num)
    if len(documents) == 0:
        content = "{0}月份，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障0件。其中信号故障0件、机车故障0件、动车组故障0件。".format(month)
    else:
        data = pd.DataFrame(documents)
        data = data.replace(0, np.nan).dropna(subset=['RISK_NAME'])
        acc = len(data[data['MAIN_CLASS'] == '事故'])
        fault = len(data[data['MAIN_CLASS'] == '故障'])
        # 行车、劳动、路外
        safety_list = []
        for i in ['行车', '劳动', '路外']:
            new_data = data[data['RISK_NAME'].str.contains(i)]
            dic = {
                '{0}'.format(i): len(new_data[new_data['MAIN_CLASS'] == '事故'])
            }
            safety_list.append(dic)
        # 设备故障
        new_data = data[data['MAIN_CLASS'] == '故障']
        safety_list.append({
            '设备故障': fault,
            '信号故障': len(new_data[new_data['NAME'] == '信号设备故障']),
            '机车故障': len(new_data[new_data['NAME'] == '机车故障']),
            '动车组故障': len(new_data[new_data['NAME'] == '动车组故障'])
        })
        content = "{0}月份，发生事故{1}件、故障{2}件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" \
                  "\n3.路外安全：未发生相撞事故。" \
                  "\n4.设备故障{3}件。其中信号故障{4}件、机车故障{5}件、动车组故障{6}件。"\
            .format(month, acc, fault, safety_list[-1]['设备故障'], safety_list[-1]['信号故障'],
                    safety_list[-1]['机车故障'], safety_list[-1]['动车组故障'])
    # TODO
    # worksheet['T6'] = content
    return content


def calc_safety_produce_data(start_date, end_date):
    """
    获取今年安全生产信息和责任安全生产信息数据
    :param start_date: {int} 开始日期 格式如：20191230
    :param end_date: {int} 结束日期 格式如：20191230
    :return:
    """
    keys = {
        "match": {
            "DATE": {
                '$lte': start_date,
                '$gte': end_date
            },
        },
        "project": {
            "_id": 0,
            "NAME": 1,
            'REASON': 1,
            'RISK_NAME': 1,
            "MAIN_CLASS": 1,
        }
    }
    coll = 'detail_safety_produce_info'
    documents = get_data_from_mongo_by_find(coll, keys)
    return documents


def get_second(report_date):
    """
    监督检查基本情况
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    start = local_data.date_params[0][0]
    end = local_data.date_params[0][1]
    # 检查信息
    check_info = local_data.check_info_this
    check_person = local_data.check_person_this
    # 检查问题
    check_problem = local_data.prob_data_this

    # 分析中心检查情况
    analysis_center = pd_query(analysis_center_sql.format(start, end))
    # 上级检查情况
    second_one = get_second_one(check_problem)
    # 全局总体检查情况
    second_two = get_second_two(check_person, check_problem, check_info)
    # 安全分析中心工作基本情况
    second_three = get_second_three(analysis_center)
    return {
        'second_one': second_one,
        'second_two': second_two,
        'second_three': second_three
    }


def get_second_one(check_problem):
    dic = {}
    content = []
    # 上级检查
    data = check_problem[check_problem['CHECK_TYPE'].isin([401, 402, 403, 404])].sort_values(by='SERIOUS_VALUE',
                                                                                             ascending=False).head(5)
    data = data.copy()
    data['CHECK_ITEM_NAME'] = data['CHECK_ITEM_NAME'].apply(lambda x: x.split(',')[0])
    if data.empty is not True:
        for i, k in data.iterrows():
            desc = '{}检查地点({})，项目({})，问题：{}'.format(i+1, k['CHECK_ADDRESS_NAMES'],
                                                      k['CHECK_ITEM_NAME'], k['DESCRIPTION'])
            content.append(desc)
    else:
        content = ['本日无问题']
    dic['one'] = '\n'.join(content)
    # TODO
    # worksheet['C11'] = dic['one']
    return dic


def get_second_two(check_person, check_problem, check_info):
    dic = {}
    # 检查基本情况 -----------------------------
    chk_count = len(check_person[check_person['CHECK_WAY'].isin([1, 2])])
    find_problem = int(check_problem.shape[0])
    main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    ser_count1 = len(check_problem[check_problem['LEVEL'].isin(['A', 'B'])])
    ser_count2 = len(check_problem[check_problem['LEVEL'].isin(['E1', 'E2'])])
    ser_count3 = len(check_problem[check_problem['LEVEL'].isin(['F1', 'F2'])])

    # 红线问题
    luju_red = len(check_problem[check_problem['IS_RED_LINE'] == 1])
    station_red = len(check_problem[check_problem['IS_RED_LINE'] == 2])
    dic['check'] = '上周集团公司下现场检查{0}人次，发现问题{1}个。性质严重问题{2}个，其中作业项{3}个、' \
                   '设备设施项{4}个、管理项{5}个。路局查处红线问题{6}个、站段查处红线问题{7}个。'. \
        format(chk_count, find_problem, main_problem, ser_count1, ser_count2, ser_count3, luju_red, station_red)
    # 路局查出红线问题 ----------------------
    red_prob = check_problem[check_problem['IS_RED_LINE'] == 1]
    red_content = []
    if red_prob.empty is not True:
        for i, k in red_prob.iterrows():
            desc = '{}.{}'.format(i+1, k['DESCRIPTION'])
            red_content.append(desc)
    else:
        red_content = ['本日无问题']
    dic['red_pro'] = '\n'.join(red_content)

    # 重点风险查处情况 ------------------------
    checked = []
    uncheck = []
    rk_data = check_info.dropna(subset=['RISK_NAME'])
    for rk in TOP10_RISK:
        if rk_data[rk_data['RISK_NAME'].str.contains(rk)].shape[0] > 0:
            checked.append(rk)
        else:
            uncheck.append(rk)
    checked_con = '，'.join(checked)
    uncheck_con = '，'.join(uncheck)
    line31 = '今日检查风险{}大类，分别是：{}\n'.format(len(checked), checked_con)
    line32 = '今日未检查风险{}大类，分别是：{}'.format(len(uncheck), uncheck_con)
    dic['risk'] = line31 + line32
    return dic


def get_second_three(analysis_center):
    count = []
    dic = {}
    # 工作量情况
    for i in range(2, 8):
        data = analysis_center[analysis_center['FLAG'] == i]
        count.append(len(data))
    # 问题查处情况
    mv_re = int(analysis_center[analysis_center['FLAG'] == 2]['DAILY_PROBLEM_NUMBER'].sum())
    eva_re = int(analysis_center[analysis_center['FLAG'].isin([5, 6])]['DAILY_PROBLEM_NUMBER'].sum())
    dic['one'] = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，' \
                 '干部履职评价{1}条，干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，' \
                 '阶段评价{5}人次。 问题查处情况：音视频调阅（复查）发现问题{6}个，' \
                 '干部履职评价（复查）发现{7}个问题。'.format(count[0], count[1], count[2], count[3],
                                              count[4], count[5], mv_re, eva_re)
    # 安全问题查处
    problem_data = analysis_center.replace('', np.nan).replace('无', np.nan).dropna(subset=['RISK_WARNING'])
    problem_count = len(problem_data['RISK_WARNING'].unique())
    situation = []
    for i in range(problem_count):
        situation.append(str(i + 1) + '.' + problem_data['RISK_WARNING'].unique().tolist()[i])
    situation = '\n'.join(situation)
    if problem_count == 0:
        dic['two'] = '安全问题查处{0}个,无重大风险问题'.format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()))
    else:
        dic['two'] = "安全问题查处{0}个，其中典型问题有{1}个：". \
                         format(int(analysis_center['DAILY_PROBLEM_NUMBER'].sum()), problem_count) + situation
    dic['three'] = '没有数据'
    # TODO
    # worksheet['C15'] = dic['one']
    # worksheet['C16'] = dic['two']
    return dic


def get_third(report_date):
    """
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    start_date, end_date = get_week_interval(report_date, ISO_WEEK_END_DAY)
    start = start_date.strftime('%Y-%m-%d')
    end = end_date.strftime('%Y-%m-%d')
    safety_info = pd_query(main_info_sql.format(start, end))
    # 总公司追踪
    info = safety_info[safety_info['HIERARCHY'] == 1]
    infos = info['CONTENT'].head(3).tolist()
    if len(infos) == 0:
        infos = ['无', '无', '无']
    # 路局追踪
    road_info = safety_info[safety_info['HIERARCHY'] == 2]
    road_infos = road_info['CONTENT'].head(3).tolist()
    if len(road_infos) == 0:
        road_infos = ['无', '无', '无']
    index = 19
    index1 = 22
    for i in range(3):
        if len(infos) < i + 1:
            infos.append(str(i + 1) + '.无')
        else:
            infos[i] = str(i + 1) + '.' + infos[i]
        # TODO
        # work_sheet['B{0}'.format(index)] = infos[i]
        index += 1
    for i in range(len(road_infos)):
        if len(road_infos) < i + 1:
            road_infos.append(str(i + 1) + '.无')
        else:
            road_infos[i] = str(i + 1) + '.' + road_infos[i]
        # TODO
        # work_sheet['B{0}'.format(index1)] = road_infos[i]
        index1 += 1
    return {
        'infos': infos,
        'road_infos': road_infos
    }


def get_fourth(report_date):
    """
    风险预警
    :param report_date: {datetime.date} 日报的日期
    :return:
    """
    start = report_date.strftime('%Y-%m-%d')
    end = report_date.strftime('%Y-%m-%d')
    warning_data = pd_query(warning_notification_sql.format(start, end))
    # 总公司
    gs = ['没数据']
    # 路局
    road_data = warning_data[(warning_data['HIERARCHY'] == 1) & (warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7]))].reset_index().head(3)
    company_warn_list = []
    if len(road_data) == 0:
        company_warn_list = ['无']
    else:
        for index in range(0, len(road_data)):
            company_warn_list.append("""{}.{}""".format(
                index + 1,
                road_data['CONTENT'][index])
            )
    # 安全分析室
    safety_data = warning_data[(warning_data['HIERARCHY'] == 2) & (warning_data['STATUS'].isin([2, 3, 4, 5, 6, 7]))].reset_index().head(3)
    safety_warn_list = []
    if len(safety_data) == 0:
        safety_warn_list = ['无']
    else:
        for index in range(0, len(safety_data)):
            safety_warn_list.append("""{}.{}""".format(
                index + 1,
                safety_data['CONTENT'][index])
            )
    return {
        'gs': '\n'.join(gs),
        'safety_warn_list': '\n'.join(company_warn_list),
        'company_warn_list': '\n'.join(safety_warn_list)
    }
