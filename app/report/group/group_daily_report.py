from datetime import datetime as dt, date

from dateutil.relativedelta import relativedelta

from app.report.group.group_daily_report_data import get_data
from app.report.analysis_report_manager import DailyAnalysisReport
from app import mongo


def group_daily_report_execute(months):
    report_date = dt.strftime(months.today, '%Y-%m-%d')
    a = GroupDailyAnalysisReport()
    a.get_report_file(report_date)


class GroupDailyAnalysisReport(DailyAnalysisReport):

    def __init__(self):
        super(GroupDailyAnalysisReport, self).__init__(hierarchy_type='GROUP')

    def get_available_report_interval_list(self, start_date, end_date):
        """
        获取在给定的开始、结束时间区间的报告清单。
        清单的每个item，包含了报告的关键参数year, quarter
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return: 查询的结果实体，包含字段
             interval_type： 'DAILY'
             topic: 报告的主题，如 工务系统周分析报告 或 成都工务段周分析报告
             available_list：清单的每个item，包含了报告的关键参数start_date, end_date
        """
        start_date = dt.strptime(start_date, '%Y-%m-%d').date()
        end_date = dt.strptime(end_date, '%Y-%m-%d').date()

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if end_date >= date.today():
            end_date = date.today() + relativedelta(days=-1)

        max_count = 7
        report_date = end_date
        count = 0
        reports = []
        while (report_date > start_date) and (count < max_count):
            report_title = report_date.strftime('%Y-%m-%d')
            reports.append({
                "item_name": report_title,
                'report_date': report_date.strftime('%Y-%m-%d'),
            })
            report_date = report_date + relativedelta(days=-1)
            count = count + 1

        topic = '集团安全生产日报表'

        result = {
            'interval_type': self.interval_type,
            'topic': topic,
            'available_list': reports
        }
        return result

    # 适配原有页面的api
    @classmethod
    def get_list(cls, start_date, end_date):
        start_date = dt.strptime(start_date, '%Y-%m-%d').date()
        end_date = dt.strptime(end_date, '%Y-%m-%d').date()

        # 计算start_date, end_date是否超出数据运行边界并加以调整
        if end_date >= date.today():
            end_date = date.today() + relativedelta(days=-1)

        max_count = 7
        report_date = end_date
        count = 0
        reports = []
        while (report_date > start_date) and (count < max_count):
            report_title = report_date.strftime('%Y-%m-%d')
            reports.append({
                'TITLE': report_title,
                'report_date': report_date.strftime('%Y-%m-%d'),
                'TYPE': 'daily_analysis'
            })
            report_date = report_date + relativedelta(days=-1)
            count = count + 1

        return reports

    def generate_report_data(self, report_date):
        """
        生成报告所需要的数据
        :param report_date: 报告的日期
        :return: {dict} 报告的数据的字典
        """
        report_date = dt.strptime(report_date, '%Y-%m-%d')
        date_idx = report_date.year * 10000 + report_date.month * 100 + report_date.day
        data = get_data(report_date.year, report_date.month, report_date.day)
        mongo.db['safety_analysis_daily_report'].delete_one(
            {
                "date": date_idx,
                "hierarchy": self.hierarchy_type,
            })
        mongo.db['safety_analysis_daily_report'].insert_one(data)
        return data

    def fill_data(self, workbook, data):
        """
        使用指定的数据data，填充报告（excel）
        :param workbook: {openyxl.workbook.Workbook} excel文件
        :param data:
        :return:
        """
        worksheet = workbook.worksheets[0]
        worksheet['B5'] = data['first']['first_one']
        worksheet['B6'] = data['first']['first_two']['content']
        worksheet['B7'] = '\n'.join(data['first']['first_two']['info']['acc'])
        worksheet['B8'] = '\n'.join(data['first']['first_two']['info']['fault'])
        worksheet['B9'] = '\n'.join(data['first']['first_two']['info']['zh'])
        worksheet['K6'] = data['first']['first_two_week']
        worksheet['T6'] = data['first']['first_two_month']
        worksheet['C11'] = data['second']['second_one']['one']
        # 全局总体检查情况
        worksheet['C12'] = data['second']['second_two']['check']
        worksheet['C13'] = data['second']['second_two']['red_pro']
        worksheet['C14'] = data['second']['second_two']['risk']
        # 安全分析中心工作基本情况
        worksheet['C15'] = data['second']['second_three']['one']
        worksheet['C16'] = data['second']['second_three']['two']
        worksheet['C17'] = data['second']['second_three']['three']
        # 安全生产信息追踪
        worksheet['B19'] = data['third']['infos'][0]
        worksheet['B20'] = data['third']['infos'][1]
        worksheet['B21'] = data['third']['infos'][2]
        worksheet['B22'] = data['third']['road_infos'][0]
        worksheet['B23'] = data['third']['road_infos'][1]
        worksheet['B24'] = data['third']['road_infos'][2]
        # 安全预警
        worksheet['B22'] = data['fourth']['gs']
        worksheet['B23'] = data['fourth']['safety_warn_list']
        worksheet['B24'] = data['fourth']['company_warn_list']

    def generate_report_name(self, report_date):
        """
        生成报告的（唯一）名称，它可以用于区分报告。例如：
        1. 用作报告标题
        2. 用作文件名
        :param report_date: 报告的日期
        :return: {str} 报告的名称，例如: '集团安全日报'
        """
        return '中国铁路成都局集团有限公司运输安全生产日报表（{}）'.format(report_date)



