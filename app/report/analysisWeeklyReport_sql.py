#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   analysisWeeklyReport_sql
Description:
Author:    
date:         2019/11/26
-------------------------------------------------
Change Activity:2019/11/26 9:50 上午
-------------------------------------------------
"""
CHECK_INFO_SQL = """
SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    b.ID_CARD,
    b.FK_DEPARTMENT_ID,
    a.CHECK_TYPE,
    a.CHECK_WAY
FROM
    t_check_info AS a
        INNER JOIN
    t_check_info_and_person AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
WHERE
    DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59',
            '%%Y-%%m-%%d %%H:%%i:%%s')
"""

CHECK_PROBLEM_SQL = """
SELECT 
    a.PK_ID AS FK_CHECK_PROBLEM_ID,
    a.LEVEL,
    a.RISK_NAMES,
    a.IS_RED_LINE,
    a.PROBLEM_POINT,
    a.CHECK_ITEM_NAME,
    a.SERIOUS_VALUE,
    a.PROBLEM_DIVIDE_NAMES,
    a.RISK_LEVEL,
    a.IS_EXTERNAL,
    a.FK_PROBLEM_BASE_ID,
    a.DESCRIPTION,
    b.DEPARTMENT_ALL_NAME AS ALL_NAME,
    a.CHECK_PERSON_ID_CARD as ID_CARD,
    c.FK_DEPARTMENT_ID,
    c.ALL_NAME as CHECK_DEPARTMENT_NAME,
    c.ID_CARD,
    f.FK_RISK_ID,
    d.TYPE2 as RESP_MAJOR_ID
FROM
    t_check_problem AS a
        INNER JOIN
    t_check_info AS b ON b.PK_ID = a.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_info_and_person AS c ON a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
        INNER JOIN
    t_check_problem_and_risk as f on a.PK_ID = f.FK_CHECK_PROBLEM_ID
        LEFT JOIN
    t_check_problem_AND_RESPONSIBLE_DEPARTMENT as e on a.PK_ID = e.FK_CHECK_PROBLEM_ID
        LEFT JOIN
    t_department as d on e.FK_DEPARTMENT_ID = d.DEPARTMENT_ID 
WHERE
     DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.SUBMIT_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59', '%%Y-%%m-%%d %%H:%%i:%%s')
"""

RISK_INFO_SQL = """
SELECT 
    PK_ID AS RISK_ID, ALL_NAME AS RISK_NAME
FROM
    t_risk
WHERE
    IS_DELETE = 0 AND HIERARCHY = 2
"""

ANALYSIS_CENTER_DEPARTMENT_SQL = """
SELECT 
    NAME, ALL_NAME, DEPARTMENT_ID
FROM
    t_department
WHERE
    TYPE2 = '1ACE7D1C12345456E0539106C00A2E70KSC'
        AND IS_DELETE = 0
"""

DEPARTMENT_SQL = """
SELECT 
    a.DEPARTMENT_ID,
    a.TYPE3,
    a.TYPE4,
    a.TYPE5,
    a.NAME,
    a.TYPE,
    a.HIERARCHY,
    a.FK_PARENT_ID
FROM
    t_department AS a
WHERE
    a.IS_DELETE = 0
"""

EVALUATE_INFO_SQL = """ 
SELECT 
    a.PK_ID,
    a.GRADATION,
    a.`LEVEL`,
    a.`CODE`,
    a.SCORE_STANDARD,
    a.EVALUATE_CONTENT,
    tp.FK_DEPARTMENT_ID AS EVALUATE_DEPARTMENT_ID
FROM
    t_check_evaluate_info AS a
        INNER JOIN
    t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
        INNER JOIN
    t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
        INNER JOIN
    t_person AS tp ON tp.ID_CARD = a.CHECK_PERSON_ID_CARD
WHERE
    DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d') >= DATE_FORMAT('{0}', '%%Y-%%m-%%d')
        AND DATE_FORMAT(a.CREATE_TIME, '%%Y-%%m-%%d %%H:%%i:%%s') <= DATE_FORMAT('{1} 23:59:59', '%%Y-%%m-%%d %%H:%%i:%%s')
        AND tp.IS_DELETE = 0
        AND c.IS_DELETE = 0
"""

MAIN_MAJOR_INFO_SQL = """
SELECT 
    a.DEPARTMENT_ID as FK_DEPARTMENT_ID,
    b.DEPARTMENT_ID AS MAJOR_DPID,
    b.name as MAJOR_NAME
FROM
    t_department AS a
        INNER JOIN
    t_department AS b ON a.type2 = b.DEPARTMENT_ID
WHERE
    a.IS_DELETE = 0
"""

SAFETY_PRODUCE_INFO_SQL = """
select OVERVIEW, REASON, MAIN_TYPE, CODE from t_safety_produce_info where CREATE_TIME between '{0}' and '{1} 23:59:59'
"""