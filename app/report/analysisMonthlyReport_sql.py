#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   analysisMonthlyReport_sql
Description:
Author:    
date:         2019/12/4
-------------------------------------------------
Change Activity:2019/12/4 4:59 下午
-------------------------------------------------
"""

# 分析日报详情信息查询语句
# a.MONITOR_PROBLEM_DESCRIPTION,
# a.EVALUATION_PROBLEM_DESCRIPTION,
# a.RISK_WARNING,
# a.CENTER_PROBLEM,
ANALYSIS_DAILY_REPORT_SQL = """SELECT
        a.CHECK_POSTION,
        b.TYPE,
        b.FLAG,
        b.MONTHLY_COMPLETE,
        b.MONTHLY_PROBLEM_NUMBER,
        c.NAME AS MAJOR
    FROM
        t_analysis_center_daily AS a
        LEFT JOIN t_analysis_center_daily_details AS b
            ON b.FK_ANALYSIS_CENTER_DAILY_ID = a.PK_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.PROFESSION_ID
    WHERE
        a.DATE = '{}'
"""


# 履职工作量
NEW_CHECK_EVALUATE_REVIEW_SQL = """SELECT a.*,b.FK_REVIEW_DEPARTMENT_ID 
FROM t_check_evaluate_check_person a
LEFT JOIN t_check_evaluate_review_person b on b.FK_CHECK_EVALUATE_CHECK_PERSON_ID=a.pk_id
WHERE a.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
"""

# 履职复查
eva_review_sql = """SELECT
b.*
FROM
t_check_evaluate_review_person b
LEFT JOIN t_check_evaluate_check_person a on b.FK_CHECK_EVALUATE_CHECK_PERSON_ID=a.pk_id
WHERE
b.CREATE_TIME >= '{0}'
AND b.CREATE_TIME <= '{1} 23:59:59'"""


# 重要检查岗位
main_post_sql = """SELECT a.PK_ID,a.`NAME`,a.FK_DEPARTMENT_ID,b.ALL_NAME FROM t_profession_dictionary a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.`CODE`='monitor_post' AND a.IS_DELETE=0"""


CHECK_MV_COST_TIME_SQL = """
SELECT 
	a.PK_ID,
    b.CHECK_PERSON_NAMES,
    b.DEPARTMENT_ALL_NAME,
    b.CHECK_ADDRESS_NAMES,
    a.COST_TIME,
    b.IS_YECHA,
    b.PROBLEM_NUMBER,
    b.CHECK_ITEM_NAMES,
    b.CHECK_WAY,
    d.ALL_NAME,
	e.`NAME`,
	a.MONITOR_POST_NAMES,
	a.MONITOR_POST_IDS,
	f.`NAME` AS MAJOR
FROM
    t_check_info_and_media AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        LEFT JOIN
    t_check_info_and_person c on c.FK_CHECK_INFO_ID=b.PK_ID
		LEFT JOIN
    t_department d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
		LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
		LEFT JOIN t_department f on f.DEPARTMENT_ID=d.TYPE2
WHERE
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
"""

DEPARTMENT_INFO_SQL = """
SELECT 
    DEPARTMENT_ID, NAME, ALL_NAME, TYPE, TYPE3, 
    TYPE4, FK_PARENT_ID, SHORT_NAME, BELONG_PROFESSION_ID, 
    TYPE2
FROM
    t_department
WHERE
    IS_DELETE = 0
"""

CHECK_INFO_SQL = """
SELECT 
    a.DEPARTMENT_ALL_NAME,
    a.PK_ID,
    a.CHECK_TYPE,
    a.CHECK_WAY,
    a.CHECK_ITEM_NAMES,
    a.CHECK_ADDRESS_NAMES,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    b.LEVEL,
    b.PROBLEM_SCORE,
	e.ALL_NAME
FROM
    t_check_info AS a
        INNER JOIN
    t_check_problem AS b ON a.PK_ID = b.FK_CHECK_INFO_ID
		LEFT JOIN t_check_problem_and_responsible_department c on c.FK_CHECK_PROBLEM_ID=b.pk_id
		LEFT JOIN t_department d on d.DEPARTMENT_ID=c.fk_department_id
		LEFT JOIN t_department e on e.department_id=d.type3
WHERE
    a.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
"""

# 岗位数
position_sql = """SELECT a.*,b.ALL_NAME AS MAJOR from t_profession_dictionary a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.`CODE`='monitor_post'
AND a.IS_DELETE=0"""


# 履职评价信息查询语句
CHECK_EVALUATE_SQL = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        d.NAME AS MAJOR,
        e.JOB,
        e.POSITION,
        e.IDENTITY,
        f.NAME AS STATION,
        f.BELONG_PROFESSION_ID,
        g.ALL_NAME AS SHOPS
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
            LEFT JOIN
        t_department AS g ON g.DEPARTMENT_ID = c.TYPE4
    WHERE
        CREATE_TIME >= '{}'
            AND CREATE_TIME <= '{} 23:59:59'
            and a.YEAR = {}
"""

# 履职评价信息查询语句
CHECK_EVALUATE_SQLS = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        d.NAME AS MAJOR,
        e.JOB,
        e.POSITION,
        e.IDENTITY,
        f.NAME AS STATION,
        f.BELONG_PROFESSION_ID,
        g.ALL_NAME AS SHOPS
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
            LEFT JOIN
        t_department AS g ON g.DEPARTMENT_ID = c.TYPE4
    WHERE
        CREATE_TIME >= '{}'
            AND CREATE_TIME <= '{} 23:59:59'
"""

# 分析中心日报
analysis_daily_sql = """SELECT a.*,b.*,c.NAME FROM t_analysis_center_daily a
LEFT JOIN t_analysis_center_daily_details b on a.PK_ID=b.FK_ANALYSIS_CENTER_DAILY_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=a.PROFESSION_ID
WHERE a.START_TIME BETWEEN '{0}' AND '{1}'"""


STATION_CARDE_COUNT_SQL = """SELECT
        sum( 1 ) AS TOTAL,
        c.`NAME` AS MAJOR,
        d.`NAME` AS STATION
    FROM
        t_person AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = b.TYPE2
        LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = b.TYPE3
    WHERE
        b.BUSINESS_CLASSIFY = '领导'
        AND c.`NAME` in ('车务', '电务', '工务', '机务', '车辆', '供电')
    GROUP BY
        c.`NAME`,
        d.`NAME`
"""

# 分析日报详情信息查询语句

ANALYSIS_DAILY_REPORT = """SELECT
        a.PK_ID,
        a.STATUS,
        a.CHECK_POSTION,
        a.MONITOR_PROBLEM_DESCRIPTION,
        a.EVALUATION_PROBLEM_DESCRIPTION,
        a.RISK_WARNING,
        a.CENTER_PROBLEM,
        b.TYPE,
        b.FLAG,
        b.MONTHLY_COMPLETE,
        b.MONTHLY_PROBLEM_NUMBER,
        c.NAME
    FROM
        t_analysis_center_daily AS a
        LEFT JOIN t_analysis_center_daily_details AS b
            ON b.FK_ANALYSIS_CENTER_DAILY_ID = a.PK_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.PROFESSION_ID
"""
# 对表安监管理系统的重点人员预选库
SYSTEM_VIOLATION_DISTRIBUTION_SQL = """SELECT
        wkpl.PK_ID,
        wkpl.PERSON_NAME,
        wkpl.ID_CARD,
        wkpl.STATUS,
        p.FK_DEPARTMENT_ID,
        d.TYPE3,
        d.TYPE2,
        d.TYPE,
        d.ALL_NAME AS allname,
        d.HIERARCHY AS cengji
    FROM
        t_warning_key_person_library AS wkpl
        LEFT JOIN t_person AS p ON wkpl.ID_CARD = p.ID_CARD
        LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = p.FK_DEPARTMENT_ID
    WHERE
        wkpl.`YEAR` ={1}
        AND wkpl.`MONTH` ={0}
"""

LASTMONTH_VIOLATION_RESULTS_SQL = """SELECT
        COUNT( * ) AS total,
        COUNT( FIND_IN_SET( 1, FK_DISPOSE_METHOD_IDS ) OR NULL ) AS peixun,
        COUNT( FIND_IN_SET( 2, FK_DISPOSE_METHOD_IDS ) OR NULL ) AS ligang,
        COUNT( FIND_IN_SET( 3, FK_DISPOSE_METHOD_IDS ) OR NULL ) AS daigang,
        COUNT( FIND_IN_SET( 4, FK_DISPOSE_METHOD_IDS ) OR NULL ) AS zhuangang,
        COUNT( FIND_IN_SET( 5, FK_DISPOSE_METHOD_IDS ) OR NULL )
                    AS qianqiyichuzhi,
        COUNT( FIND_IN_SET( 6, FK_DISPOSE_METHOD_IDS ) OR NULL ) AS bangcu
    FROM
        t_warning_key_person_library_official
    WHERE
        `STATUS` = 1
        AND CREATE_TIME between '{0}' and '{1} :23:59:59'
"""

# 对表安监管理系统的重点人员库
MONTH_VIOLATION_RESULTS_SQL = """
    SELECT
        a.PK_ID,
        a.ID_CARD,
        a.PERSON_NAME,
        a.STATUS,
        a.FK_DISPOSE_METHOD_IDS,
        b.FK_DEPARTMENT_ID,
        c.TYPE2,
        c.TYPE3,
        c.HIERARCHY
    FROM
        t_warning_key_person_library_official as a 
        inner join
        t_person as b on a.ID_CARD = b.ID_CARD
        inner join
        t_department as c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
    WHERE
         a.MONTH = '{0}' 
         AND a.YEAR = '{1}'
"""

# 安全生产信息
CHECK_SAFETY_INFO_SQL = """SELECT
        a.*,
        c.DEPARTMENT_ID,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED,
        g.TYPE,
        c.`NAME` as STATION,
        c.DEPARTMENT_ID AS TYPE3,
        d.`NAME` AS MAJOR,
        c.BELONG_PROFESSION_ID
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID  
            LEFT JOIN
        t_department d on d.DEPARTMENT_ID=c.TYPE2
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1} 23:59:59')"""


WARNING_NOTIFICATION_AND_TASK_PROCESS_SQL = """
SELECT 
    a.PK_ID, b.PK_ID AS TASK_PROCESS_ID, b.TASK_NAME, b.CREATE_TIME, b.RESULT
FROM
    t_warning_notification AS a
        INNER JOIN
    t_process_variable AS b ON a.FK_PROCESS_INFO_ID = b.FK_PROCESS_INFO_ID
WHERE
    a.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
    """