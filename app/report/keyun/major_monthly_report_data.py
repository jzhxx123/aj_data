from datetime import datetime
import threading
from app import mongo
from app.data.util import pd_query
from app.report.keyun.common import get_evaluate_score_and_punish_table, get_evaluate_rank_data, MID_HIGH_PROBLEM_LEVEL
from app.report.keyun.major_common_sql import QUANTIFY_ASSESS_DATA_SQL, QUANTIFY_DETAIL_ASSESS_UNQ_PERSON_SQL, \
    MAJOR_PERSON_DATA_SQL, CHECK_EVALUATE_INFO_SQL, STATION_LIST_SQL, CHECK_INFO_AND_PROBLEM_SQL, \
    TRANSPORT_ADMIN_DEPT_ID, CHAOQI_PROBLEM_SQL, ASSESS_MONEY_SQL, RETURN_MONEY_SQL, STATION_SAFETY_KESHI_SQL, \
    analysis_qt_sql, EVALATE_WORKER_SQL, eva_review_sql, CADRE_CUMULATIVE_SQL
import pandas as pd
import app.report.analysis_report_manager as manager

local_data = threading.local()


def calc_ratio(num1, num2):
    if num2 == 0:
        return {
            'diff': num1,
            'percent': num1 * 100
        }
    else:
        return {
            'diff': num1 - num2,
            'percent': round((num1 - num2) / num2 * 100, 1)
        }


def calc_rate(num1, num2):
    if num2 == 0:
        return num1
    else:
        return round(float(num1 / num2), 2)


def _get_health_index(month, station_name):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '客运',
            'DEPARTMENT_NAME': station_name
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def get_data(year, month):
    date_params = manager.MonthlyAnalysisReport.get_month_intervals(year, month)
    local_data.date_params = date_params
    local_data.year = year
    local_data.month = month
    # 加载所有SQL数据
    _load_general_data()

    start_date = local_data.start_date
    end_date = local_data.end_date
    start_month = start_date.month
    end_month = end_date.month
    start_day = start_date.day
    end_day = end_date.day

    first = get_first()
    second = _get_second()
    third = get_third()

    file_name = f'{year}年{month}月客运系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '客运',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.now(),
        'first': first,
        'second': second,
        'third': third
    }
    return result


def _load_general_data(stage=99):
    """
    加载在整个计算过程中使用的通用的数据
    :return:
    """
    date_params = local_data.date_params
    start_date = datetime.strptime(date_params[0][0], '%Y-%m-%d')
    end_date = datetime.strptime(date_params[0][1], '%Y-%m-%d')
    start_date_prv = datetime.strptime(date_params[1][0], '%Y-%m-%d')
    end_date_prv = datetime.strptime(date_params[1][1], '%Y-%m-%d')
    start_date_year = datetime.strptime(date_params[2][0], '%Y-%m-%d')
    end_date_year = datetime.strptime(date_params[2][1], '%Y-%m-%d')

    local_data.major = '车务'
    local_data.start_date = start_date
    local_data.end_date = end_date
    local_data.start_date_prv = start_date_prv
    local_data.end_date_prv = end_date_prv
    local_data.start_date_year = start_date_year
    local_data.end_date_year = end_date_year
    year_month = local_data.year * 100 + local_data.month

    station_list = pd_query(STATION_LIST_SQL)
    local_data.station_list = station_list

    # 站段人员
    person = pd_query(MAJOR_PERSON_DATA_SQL)
    local_data.person = person

    # 站段分析中心ID
    center_data = pd_query(STATION_SAFETY_KESHI_SQL)
    local_data.center = center_data

    # 分析中心量化质量
    qt0 = pd_query(analysis_qt_sql.format(local_data.year, local_data.month))
    local_data.qt_center = qt0

    # 计算量化人数, 增加COMPLETION 字段以便判断是否完成
    qt_data = pd_query(QUANTIFY_ASSESS_DATA_SQL.format(local_data.year, local_data.month))
    qt_data.fillna(0, inplace=True)
    # 量化的完成情况
    qt_data['COMPLETION'] = 1
    unq_data = pd_query(QUANTIFY_DETAIL_ASSESS_UNQ_PERSON_SQL.format(local_data.year, local_data.month))
    uq_set = set(unq_data['ID_CARD'].tolist())
    qt_data['COMPLETION'] = qt_data.apply(lambda row: mark_complete(row, uq_set), axis=1)
    local_data.qt_data = qt_data

    # 上一个月的量化完成情况
    qt_data1 = pd_query(QUANTIFY_ASSESS_DATA_SQL.format(end_date_prv.year, end_date_prv.month))
    qt_data1.fillna(0, inplace=True)
    local_data.qt_data_prev = qt_data1

    # -------------- 履职评价信息
    eval_info0 = pd_query(CHECK_EVALUATE_INFO_SQL.format(date_params[0][0], date_params[0][1]))
    eval_info0['SITUATION'] = eval_info0['SITUATION'].str.replace('\n', '').str.replace('。', '')
    local_data.evaluate_info_this = eval_info0

    year_start_str = f'{local_data.year}-01-01'
    eval_info4 = pd_query(CHECK_EVALUATE_INFO_SQL.format(year_start_str, date_params[0][1]))
    eval_info4['SITUATION'] = eval_info4['SITUATION'].str.replace('\n', '').str.replace('。', '')
    local_data.evaluate_info_year_total = eval_info4

    eval_rank0 = get_evaluate_rank_data({'MON': year_month, 'MAJOR': local_data.major})
    local_data.eval_rank_this = eval_rank0

    # 履职工作量查询
    eva_worker = pd_query(EVALATE_WORKER_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.eva_worker = eva_worker

    # 履职复查
    eva_re = pd_query(eva_review_sql.format(date_params[0][0], date_params[0][1]))
    local_data.eva_re = eva_re

    # 检查与问题信息数据(PK_ID_CK, PK_ID_CP)按需要除重复
    cb_data0 = pd_query(CHECK_INFO_AND_PROBLEM_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.check_problem_data_this = cb_data0
    cb_data1 = pd_query(CHECK_INFO_AND_PROBLEM_SQL.format(date_params[1][0], date_params[1][1]))
    local_data.check_problem_data_prev = cb_data1

    # 超期整改
    cq_data0 = pd_query(CHAOQI_PROBLEM_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.chaoqi_data = cq_data0

    # 考核金额
    kh_data0 = pd_query(ASSESS_MONEY_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.assess_money = kh_data0

    # 返奖金额
    return0 = pd_query(RETURN_MONEY_SQL.format(local_data.year, local_data.month))
    local_data.return_money = return0

    # 干部累计记分
    cadre_cumulative_data = pd_query(CADRE_CUMULATIVE_SQL.format(local_data.start_date.date(), local_data.end_date.date()))
    local_data.cadre_cumulative_data = cadre_cumulative_data


# ------------------------------------ 第一章
def get_first():
    # （一）评价整体情况分析
    entire_state = _get_evaluate_entire_state()
    # （三）各级干部履职分析
    cadre_evaluate = _cadre_evaluate_analysis()

    return {
        'entire_state': entire_state,
        'cadre_evaluate': cadre_evaluate,
    }


def _get_evaluate_entire_state():
    # 量化任务描述
    qt_state = _get_quantify_access_total(local_data.qt_data)

    eval_info = local_data.evaluate_info_this
    worker = local_data.eva_worker
    eva_re = local_data.eva_re

    # 两级分析中心评价信息
    center_state = _center_evaluate_state(eval_info, worker, eva_re)
    # 履职存在问题
    problem_desc = _evaluate_problem_type_desc(eval_info)

    eval_person_year = local_data.evaluate_info_year_total
    evaluate_score = _evaluate_person_score(eval_person_year)
    content = {
        'qt_state': qt_state,
        'center_state': center_state,
        'problem_desc': problem_desc,
        'evaluate_score': evaluate_score
    }
    return content


# 量化人员的完成情况及描述 对应 一、（一）评价整体情况分析
def _get_quantify_access_total(qt_data):
    count = qt_data.shape[0]
    cpl_count = qt_data[qt_data['COMPLETION'] == 1].shape[0]
    cpl_rate = round(100 * cpl_count/count, 2)
    unc_data = qt_data[qt_data['COMPLETION'] == 0]
    un_persons = [
        {
            'station': str(row.ALL_NAME).split('-')[0],
            'job': row.JOB,
            'person_name': row.PERSON_NAME
        } for row in unc_data.itertuples()
    ]

    result = {
        'count': count,
        'cpl_count': cpl_count,
        'cpl_rate': cpl_rate,
        'un_persons': un_persons
    }
    return result


def mark_complete(row, unq_id_set):
    if row['CHECK_TIMES_TOTAL'] > row['REALITY_NUMBER']:
        return 0
    if row['CHECK_NOTIFICATION_TIMES_TOTAL'] > row['CHECK_NOTIFICATION_REALITY_NUMBER']:
        return 0
    if row['MONITOR_NUMBER_TOTAL'] > row['MEDIA_REALITY_TIME']:
        return 0
    if row['PROBLEM_NUMBER_TOTAL'] > row['REALITY_PROBLEM_NUMBER']:
        return 0
    if row['WORK_PROBLEM_NUMBER_TOTAL'] > row['REALITY_WORK_ITEM_PROBLEM_NUMBER']:
        return 0
    if row['MONITOR_PROBLEM_NUMBER_TOTAL'] > row['MEDIA_REALITY_PROBLEM_NUMBER']:
        return 0
    if row['HIDDEN_DANGER_RECHECK_TIMES_TOTAL'] > row['HIDDEN_DANGER_RECHECK_REALITY_NUMBER']:
        return 0
    if row['RISK_RECHECK_TIMES_TOTAL'] > row['RISK_RECHECK_REALITY_NUMBER']:
        return 0
    if row['IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL'] > row['IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER']:
        return 0
    if row['MIN_QUALITY_GRADES_TOTAL'] > row['REALITY_MIN_QUALITY_GRADES']:
        return 0
    if row['MANAGE_PROBLEM_NUMBER_TOTAL'] > row['REALITY_MANAGE_PROBLEM_NUMBER']:
        return 0
    if row['DEVICE_PROBLEM_NUMBER_TOTAL'] > row['REALITY_DEVICE_PROBLEM_NUMBER']:
        return 0
    if row['ENVIRONMENT_PROBLEM_NUMBER_TOTAL'] > row['REALITY_ENVIRONMENT_PROBLEM_NUMBER']:
        return 0
    if row['OUT_WORK_PROBLEM_NUMBER_TOTAL'] > row['REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER']:
        return 0
    if row['ID_CARD'] in unq_id_set:
        return 0
    return 1


def _center_evaluate_state(evaluate_info, worker, eva_re):
    """
    两级分析中心的评价情况
    :param evaluate_info:
    :return:
    """
    # 逐条评价
    zt_data = worker[worker['EVALUATE_WAY'] == 1]
    zt = len(zt_data) + len(eva_re[eva_re['EVALUATE_WAY'] == 1])
    # 管理人员的评价
    ad_data = zt_data[zt_data['GRADATION'] != '非管理和专业技术人员']

    # 路局检查发现的问题
    grp_data = evaluate_info[evaluate_info['CHECK_TYPE'] == 1]

    # 路局评价的问题
    grp_eva = evaluate_info[evaluate_info['CHECK_TYPE'] == 1]

    dic = {
        'eval_count': zt,
        'ad_problem_count': len(ad_data),  # 行车管理人员履职问题数
        'ad_person_count': len(set(ad_data['RESPONSIBE_ID_CARD'])),  # 行车管理人员履职人数
        'grp_check_count': grp_data.shape[0],  # 集团检查发现问题个数
        'st_check_count': len(ad_data[ad_data['CHECK_TYPE'] == 2]),  # 站段检查发现问题个数
        'manager_count': len(evaluate_info[evaluate_info['IDENTITY'] == '干部']),  # 涉及干部x人
        'total_score': int(evaluate_info['SCORE'].sum()),  # 合计记分
        'grp_eval_count': len(set(grp_eva['RESPONSIBE_ID_CARD'])),  # 路局评价总人数
        'grp_eval_score': int(grp_eva['SCORE'].sum()),  # 路局评价总扣分
    }
    return dic


def _evaluate_person_score(eva_year):
    # 总人数
    eva_month = eva_year[eva_year['MONTH'] == local_data.month]
    count1 = len(set(eva_year['RESPONSIBE_ID_CARD']))
    last_month = local_data.month - 1
    # 计算本月新增多少人
    if last_month == 0:
        add_count = count1
    else:
        last_count = len(eva_year[eva_year['MONTH'] <= last_month]['RESPONSIBE_ID_CARD'].unique().tolist())
        add_count = count1 - last_count
    # 计算计分最高(当月次)
    max_data = eva_month.sort_values(by='SCORE', ascending=False).reset_index().iloc[0]
    max_score = float(max_data['SCORE'])
    name1 = max_data['RESP_STATION'] + max_data['JOB'] + max_data['RESPONSIBE_PERSON_NAME']

    # 累计最高分
    eva_gb = eva_year.groupby('RESPONSIBE_ID_CARD').sum().reset_index().sort_values(by='SCORE', ascending=False)
    total_max = eva_gb.iloc[0]
    total_score = float(total_max['SCORE'])
    max_idcard = total_max['RESPONSIBE_ID_CARD']

    name_data = eva_year[eva_year['RESPONSIBE_ID_CARD'] == max_idcard].iloc[0]
    name2 = name_data['RESP_STATION'] + name_data['JOB'] + name_data['RESPONSIBE_PERSON_NAME']
    return {
        'count1': count1,
        'add_count': add_count,
        'max_score': max_score,
        'name1': name1,
        'total_score': total_score,
        'name2': name2,
        'punish_table': get_evaluate_score_and_punish_table(eva_month, eva_year)
    }


def _evaluate_problem_type_desc(evaluate_info):
    all_list = []
    # 各个履职问题数据
    index = 0
    titles = ['一是重点工作落实不到位', '二是监督检查质量低下', '三是检查信息录入质量不高', '四是问题整改督促不力',
              '五是安全过程控制不到位', '六是查处问题考核不严', '七是音视频运用管理问题突出', '八是弄虚作假时有发生']
    descriptions = [
        '部分管理人员对上级布置重点工作不认真思考、不主动抓落实、不检查督促，重点工作推进迟缓、贯彻落实不力',
        '下现场检查力度、深度不够，安全检查不认真、查处问题质量不高的问题突出',
        '部分管理人员系统运用不重视，安全问题、信息录入明显错误，录入与安全无关的问题，拆分检查信息录入的问题突出',
        '部分管理人员对问题整改不重视，问题整改敷衍了事、整改督促不力、销号把关不严，问题整改不彻底、安全问题反复发生的问题突出',
        '部分管理干部安全意识松懈、安全管理不严、专业管理不力，安全过程控制不到位，现场作业失管失控',
        '部分管理干部日常安全考核不严格、存有好人主义，查处安全问题该考核不考核、降低标准考核问题仍然较多',
        '部分管理人员对音视频运用管理不重视，音视频设备管理不到位、调阅分析开展不力的问题突出',
        ''
    ]
    reasons = [
        '重点工作落实',
        '检查质量不高',
        '检查信息录入质量不高',
        '问题闭环管理',
        '因事故、故障信息追溯管理人员负直接、间接管理责任',
        '不严格考核',
        '音视频设备管理',
        '本月查处弄虚作假'
    ]
    for i in ['ZD', 'ZL', 'JL', 'ZG', 'SZ', 'KH', 'YY', 'ZJ']:
        data = evaluate_info[evaluate_info['CODE'].str.contains(i)]
        if data.shape[0] == 0:
            continue
        detail = []
        # 每个问题的SITUATION分类
        for j in data['CODE'].value_counts().index:
            new_data = data[data['CODE'] == j]
            dic = {
                'name': list(new_data['SITUATION'])[0].strip(),
                'code': new_data['CODE'].tolist()[0],
                'count': len(new_data),
                'person': len(new_data['RESPONSIBE_ID_CARD'].value_counts())
            }
            detail.append(dic)
        count = data.shape[0]
        person_count = data['RESPONSIBE_ID_CARD'].value_counts().shape[0]
        # 人员描述的数据
        data = data.sort_values(by='SCORE', ascending=False)
        person_list = [
            {
                'station': it.RESP_STATION,
                'resp_dept': it.RESP_DEPT_NAME,
                'job': it.JOB,
                'person_name': it.RESPONSIBE_PERSON_NAME,
                'content': it.EVALUATE_CONTENT
            } for it in data[data['CHECK_TYPE'] == 1].itertuples()
        ]

        dic = {
            'title': titles[index],
            'desc': descriptions[index],
            'reason': reasons[index],
            'pro_count': count,
            'per_count': person_count,
            'detail': sorted(detail, key=lambda x: x['count'], reverse=True),
            'person_list': person_list
        }
        all_list.append(dic)

        index += 1
    all_list = sorted(all_list, key=lambda x: x['pro_count'], reverse=True)
    return {
        'problem_list': all_list
    }


# （三）各级干部履职分析
def _cadre_evaluate_analysis():
    eval_info = local_data.evaluate_info_this
    eval_rank = local_data.eval_rank_this

    eval_info_total = local_data.evaluate_info_year_total
    # 1. 局管领导
    head = _group_admin_evaluate()
    # 正科职干部履职
    zk = _get_zhengke_evaluate(eval_info, eval_info_total, eval_rank)
    # 副科职及以下干部履职
    fk = _get_fuke_evaluate(eval_info, eval_info_total)
    return {
        'admin': head,
        'zk': zk,
        'fk': fk
    }


def _group_admin_evaluate():
    eval_info = local_data.evaluate_info_this
    eval_rank = local_data.eval_rank_this
    chk_prob_data = local_data.check_problem_data_this
    qt_data = local_data.qt_data
    station_list = local_data.station_list
    person = local_data.person
    # 局管干部履职
    admin_gen = _get_group_admin_eval(eval_info)
    # 领导班子履职表
    evaluate_table = get_evaluate_table(eval_rank, qt_data, chk_prob_data, station_list,
                                        local_data.year, local_data.month, person)
    admin_detail = _get_group_admin_diff(eval_rank, qt_data, chk_prob_data, eval_info, station_list,
                                         local_data.year, local_data.month)
    return {
        'admin_gen': admin_gen,
        'evaluate_table': evaluate_table['table_list'],
        'evaluate_tables': evaluate_table,
        'admin_detail': admin_detail
    }


def _get_group_admin_eval(evaluate_info):
    # 局管干部计分情况
    ju_data = evaluate_info[evaluate_info['GRADATION'] == '局管领导人员']
    person = len(ju_data)
    content = []
    for i, k in ju_data.iterrows():
        dic = {
            'station': k['RESP_STATION'],
            'job': k['JOB'],
            'name': k['RESPONSIBE_PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    return {
        'person': person,
        'content': content
    }


def _get_group_admin_diff(eval_rank, qt_data, check_problem_data_this, eval_info, station_list, year, month):
    person = local_data.person
    # 局管领导的差异
    eval_rank = eval_rank[(eval_rank['LEVEL'].isin(['正处级', '副处级'])) & (eval_rank['BUSINESS_CLASSIFY'] == '领导')]
    qt_data = qt_data[qt_data['ALL_NAME'].str.contains('领导')]
    check_problem_data_this = check_problem_data_this[check_problem_data_this['CHECK_DEPT_NAME'].str.contains('领导')]
    eval_info = eval_info[(eval_info['LEVEL'].isin(['正处级', '副处级'])) &
                          (eval_info['RESP_BUSINESS_CLASSIFY'] == '领导')]
    diff_list = get_evaluate_table(eval_rank, qt_data, check_problem_data_this, station_list, year, month, person)
    desc_list = get_evaluate_desc(eval_info, station_list)
    return {
        'diff_list': diff_list['table_list'],
        'desc_list': desc_list
    }


def get_evaluate_table(eval_rank, qt_data, check_problem_data_this, station_list, year, month, person):
    """
    计算履职分析表格
    :param eval_rank: 履职评价排名数据
    :param qt_data:  量化数据
    :param check_problem_data_this: {dataframe} 检查信息与问题数据
    :param station_list: {list} 站段列表
    :param year: 年份 综合指数的年份
    :param month: 月份 综合指数的月份
    :return:
    """
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.NAME
        } for it in pd.DataFrame({
            'DEPARTMENT_ID': ['19B8C3534E125665E0539106C00A58FD', '19B8C3534E1E5665E0539106C00A58FD',
                              '19B8C3534E205665E0539106C00A58FD', '19B8C3534E055665E0539106C00A58FD',
                              '19B8C3534E045665E0539106C00A58FD', '19B8C3534E3B5665E0539106C00A58FD'],
            'NAME': ['成都车站', '重庆车站', '贵阳车站', '成都客运段', '重庆客运段', '贵阳客运段']
        }).itertuples()
    ]
    table_list = []
    # 系统的安全履职评价评价分
    sys_eval_avg = round(float(eval_rank['evaluate_score'].mean()), 4)
    sys_check_avg = round(float(qt_data['REALITY_NUMBER'].mean()), 4)
    sys_media_avg = round(float(qt_data['MEDIA_REALITY_TIME'].mean()), 4)
    sys_problem_avg = round(float(qt_data['REALITY_PROBLEM_NUMBER'].mean()), 4)
    sys_qt_count = qt_data.shape[0]

    check_data = check_problem_data_this.copy()
    check_data = check_data.drop_duplicates('PK_ID_CK')
    sys_yecha_count = check_data[(check_data['IS_YECHA'] == 1) & (check_data['CHECK_WAY'].isin([1, 2]))].shape[0]
    sys_yecha_avg = round(float(calc_rate(sys_yecha_count, sys_qt_count)), 2)

    # 中高质量问题，问题质量分
    pro_data = check_problem_data_this[~check_problem_data_this['PK_ID_CP'].isna()].copy()
    pro_data = pro_data.drop_duplicates('PK_ID_CP')
    sys_mh_count = pro_data[pro_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
    sys_mh_avg = round(float(calc_rate(sys_mh_count, sys_qt_count)), 2)

    # 计划质量分
    plan_pro_score = round(float(qt_data['MIN_QUALITY_GRADES_TOTAL'].sum()), 2)
    # 实际质量分
    real_pro_score = round(float(qt_data['REALITY_MIN_QUALITY_GRADES'].sum()), 2)

    sys_chk_score_total = round(float(pro_data['CHECK_SCORE'].sum()), 2)
    sys_chk_score_avg = round(calc_rate(sys_chk_score_total, sys_qt_count), 2)

    # 完成率
    finish_diff = round(calc_rate(real_pro_score, plan_pro_score) * 100, 2)

    sys_ke = get_sys_shop_mean_data(qt_data, check_problem_data_this, '安全科')
    sys_ji = get_sys_shop_mean_data(qt_data, check_problem_data_this, '技术信息科')
    sys_cw = get_sys_shop_mean_data(qt_data, check_problem_data_this, '乘务科')

    for st in st_list:
        sid, name = st['station_id'], st['station_name']
        erd = eval_rank[eval_rank['STATION'] == name]
        # 安全履职总分 / 评价分
        eval_total = round(float(erd['evaluate_score'].sum()), 2)
        eval_avg = round(float(erd['evaluate_score'].mean()), 2)
        eval_avg_diff = eval_avg - sys_eval_avg

        year_month = year * 100 + month
        health_index = _get_health_index(year_month, name)
        if health_index.empty is True:
            health_score = ''
            health_rank = ''
        else:
            health_score = float(health_index['SCORE'])
            health_rank = int(health_index['RANK'])

        # 站段人数
        std = person[person['STATION_ID'] == sid]
        std_count = std.shape[0]

        # 量化人数/检查次数
        qtd = qt_data[qt_data['STATION_ID'] == sid]
        qt_count = qtd.shape[0]
        check_count = int(qtd['REALITY_NUMBER'].sum())
        check_avg = round(float(calc_rate(check_count, qt_count)))
        check_avg_diff = round(check_avg - sys_check_avg, 2)

        # 完成率
        cpl_count = qtd[qtd['COMPLETION'] == 1].shape[0]
        qt_cpl_rate = round(100 * (calc_rate(cpl_count, qt_count)), 2)

        # 调阅时长
        media_len = round(float(qtd['MEDIA_REALITY_TIME'].sum()), 4)
        media_avg = round(float(calc_rate(media_len, qt_count)), 4)
        media_avg_diff = round(media_avg - sys_media_avg, 4)

        # 发现问题数
        problem_count = round(int(qtd['REALITY_PROBLEM_NUMBER'].sum()), 2)
        problem_avg = round(float(calc_rate(problem_count, qt_count)), 2)
        problem_avg_diff = round(problem_avg - sys_problem_avg, 2)

        # 夜查次数，中高质量， 检查质量分
        cb_data = check_data[check_data['CHECK_STATION_ID'] == sid]
        yecha_count = cb_data[cb_data['IS_YECHA'] == 1].shape[0]
        yecha_avg = round(float(calc_rate(yecha_count, qt_count)), 2)
        yecha_avg_diff = round(yecha_avg - sys_yecha_avg, 2)
        yecha_avg_percent = round(calc_rate(yecha_count, check_count) * 100, 2)

        # 中高质量
        p_data = pro_data[pro_data['CHECK_STATION_ID'] == sid]
        mh_count = p_data[p_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
        mh_avg = round(float(calc_rate(mh_count, qt_count)), 2)
        mh_avg_diff = round(sys_mh_avg - mh_avg, 2)

        # 检查质量分
        chk_score_total = round(float(p_data['CHECK_SCORE'].sum()), 2)
        chk_score_avg = round(calc_rate(chk_score_total, qt_count), 2)
        chk_score_avg_diff = round(chk_score_avg - sys_chk_score_avg, 2)
        chk_score_number_avg = round(calc_rate(chk_score_total, problem_count), 2)
        chk_score_std_avg = round(calc_rate(chk_score_total, std_count), 2)

        # 安全科 / 客运科 / 乘务科
        safety_dic = [
            {
                'station_id': it.DEPARTMENT_ID,
                'station_name': it.NAME
            } for it in
            station_list[(station_list['FK_PARENT_ID'] == sid) & (station_list['NAME'] == '安全科')].itertuples()
        ]
        tec_dic = [
            {
                'station_id': it.DEPARTMENT_ID,
                'station_name': it.NAME
            } for it in
            station_list[(station_list['FK_PARENT_ID'] == sid) & (station_list['NAME'] == '客运科')].itertuples()
        ]
        cw_dic = [
            {
                'station_id': it.DEPARTMENT_ID,
                'station_name': it.NAME
            } for it in
            station_list[(station_list['FK_PARENT_ID'] == sid) & (station_list['NAME'] == '乘务科')].itertuples()
        ]
        safety_ke = get_cal_shop_data(safety_dic, qt_data, check_problem_data_this, person, sys_ke, pro_data)
        tec_ke = get_cal_shop_data(tec_dic, qt_data, check_problem_data_this, person, sys_ji, pro_data)
        cw_ke = get_cal_shop_data(cw_dic, qt_data, check_problem_data_this, person, sys_cw, pro_data)
        row = {
            'station': name,
            'safety_ke': safety_ke,
            'tec_ke': tec_ke,
            'cw_ke': cw_ke,
            'eval_total': eval_total,
            'eval_avg': eval_avg,  # 人均安全履职得分
            'eval_avg_diff': eval_avg_diff,
            'sys_eval_avg': sys_eval_avg,
            'health_score': health_score,
            'health_rank': health_rank,  #
            'qt_count': qt_count,
            'check_count': check_count,  # 检查次数
            'check_avg': check_avg,
            'sys_check_avg': sys_check_avg,
            'check_avg_diff': check_avg_diff,
            'qt_cpl_rate': qt_cpl_rate,  # 完成率
            'media_len': media_len,  # 调阅时长
            'media_avg': media_avg,
            'media_avg_diff': media_avg_diff,
            'problem_count': problem_count,  # 发现问题数
            'problem_avg': problem_avg,
            'problem_avg_diff': problem_avg_diff,
            'yecha_count': yecha_count,  # 夜查次数
            'yecha_avg': yecha_avg,
            'yecha_avg_diff': yecha_avg_diff,
            'yecha_avg_percent': yecha_avg_percent,
            'mh_count': mh_count,  # 中高质量问题
            'mh_avg': mh_avg,
            'mh_avg_diff': mh_avg_diff,
            'chk_score_total': chk_score_total,  # 检查质量分
            'chk_score_avg': chk_score_avg,  # 量化人员平均质量分
            'chk_score_avg_diff': chk_score_avg_diff,
            'chk_score_number_avg': chk_score_number_avg,  # 平均问题质量分
            'chk_score_std_avg': chk_score_std_avg  # 单位人均质量分
        }
        table_list.append(row)
    yecha_list = sorted(table_list, key=lambda x: x['yecha_avg_percent'], reverse=True)
    score_list = sorted(table_list, key=lambda x: x['chk_score_std_avg'], reverse=True)
    return {
        'table_list': table_list,
        'plan_pro_score': plan_pro_score,
        'real_pro_score': real_pro_score,
        'finish_diff': finish_diff,
        'yecha_list': yecha_list,
        'score_list': score_list
    }


def get_sys_shop_mean_data(qt, check_problem_data, str1):
    qt_data = qt[qt['ALL_NAME'].str.contains(str1)]
    check_problem_data_this = check_problem_data[check_problem_data['CHECK_DEPT_NAME'].str.contains(str1)]
    # 系统的安全履职评价评价分
    sys_check_avg = round(float(qt_data['REALITY_NUMBER'].mean()), 4)
    sys_media_avg = round(float(qt_data['MEDIA_REALITY_TIME'].mean()), 4)
    sys_problem_avg = round(float(qt_data['REALITY_PROBLEM_NUMBER'].mean()), 4)
    sys_qt_count = qt_data.shape[0]

    check_data = check_problem_data_this.copy()
    check_data = check_data.drop_duplicates('PK_ID_CK')
    sys_yecha_count = check_data[(check_data['IS_YECHA'] == 1) & (check_data['CHECK_WAY'].isin([1, 2]))].shape[0]
    sys_yecha_avg = round(float(calc_rate(sys_yecha_count, sys_qt_count)), 2)

    # 中高质量问题，问题质量分
    pro_data = check_problem_data_this[~check_problem_data_this['PK_ID_CP'].isna()].copy()
    pro_data = pro_data.drop_duplicates('PK_ID_CP')
    sys_mh_count = pro_data[pro_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
    sys_mh_avg = round(float(calc_rate(sys_mh_count, sys_qt_count)), 2)

    sys_chk_score_total = round(float(pro_data['CHECK_SCORE'].sum()), 2)
    sys_chk_score_avg = round(calc_rate(sys_chk_score_total, sys_qt_count), 2)
    return {
        'sys_check_avg': sys_check_avg,
        'sys_media_avg': sys_media_avg,
        'sys_problem_avg': sys_problem_avg,
        'sys_yecha_avg': sys_yecha_avg,
        'sys_mh_avg': sys_mh_avg,
        'sys_chk_score_avg': sys_chk_score_avg
    }


def get_cal_shop_data(st, qt, check_problem_data, person, sys_mean, pro_data):
    if len(st) != 0:
        sid, name = st[0]['station_id'], st[0]['station_name']
        qt_data = qt[qt['SECTION_ID'] == sid]
        check_data = check_problem_data[check_problem_data['CHECK_DEPT_ID'] == sid]
        # 车间人数
        std = person[person['DEPARTMENT_ID'] == sid]
        std_count = std.shape[0]

        # 量化人数/检查次数
        qtd = qt_data
        qt_count = qtd.shape[0]
        check_count = int(qtd['REALITY_NUMBER'].sum())
        check_avg = round(float(calc_rate(qtd['REALITY_NUMBER'].sum(), qt_count)), 2)
        check_avg_diff = round(check_avg - sys_mean['sys_check_avg'], 2)

        # 完成率
        cpl_count = qtd[qtd['COMPLETION'] == 1].shape[0]
        qt_cpl_rate = round(100 * calc_rate(cpl_count, qt_count), 2)

        # 调阅时长
        media_len = round(float(qtd['MEDIA_REALITY_TIME'].sum()), 4)
        media_avg = round(calc_rate(media_len, qt_count), 4)
        media_avg_diff = round(media_avg - sys_mean['sys_media_avg'], 4)

        # 发现问题数
        problem_count = round(int(qtd['REALITY_PROBLEM_NUMBER'].sum()), 2)
        problem_avg = round(float(calc_rate(problem_count, qt_count)), 2)
        problem_avg_diff = round(problem_avg - sys_mean['sys_problem_avg'], 2)

        # 夜查次数，中高质量， 检查质量分
        cb_data = check_data
        xc_count = len(cb_data[cb_data['CHECK_WAY'].isin([1, 2])])
        yecha_count = cb_data[(cb_data['IS_YECHA'] == 1) & (cb_data['CHECK_WAY'].isin([1, 2]))].shape[0]
        yecha_avg = round(float(calc_rate(yecha_count, qt_count)), 2)
        yecha_avg_diff = round(yecha_avg - sys_mean['sys_yecha_avg'], 2)
        yecha_avg_percent = round(calc_rate(yecha_count, check_count) * 100, 2)

        # 中高质量
        # p_data = pro_data[pro_data['CHECK_STATION_ID'] == sid]
        p_data = pro_data[pro_data['TYPE4'] == sid]
        mh_count = p_data[p_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
        mh_avg = round(float(calc_rate(mh_count, qt_count)), 2)
        mh_avg_diff = round(sys_mean['sys_mh_avg'] - mh_avg, 2)

        # 检查质量分
        chk_score_total = round(float(p_data['CHECK_SCORE'].sum()), 2)
        chk_score_avg = round(calc_rate(chk_score_total, qt_count), 2)
        chk_score_avg_diff = round(chk_score_avg - sys_mean['sys_chk_score_avg'], 2)
        chk_score_number_avg = round(calc_rate(chk_score_total, problem_count), 2)
        chk_score_std_avg = round(calc_rate(chk_score_total, std_count), 2)

        return {
            'station': name,
            'qt_count': qt_count,
            'check_count': check_count,  # 检查次数
            'check_avg': check_avg,
            'check_avg_diff': check_avg_diff,
            'qt_cpl_rate': qt_cpl_rate,  # 完成率
            'media_len': media_len,  # 调阅时长
            'media_avg': media_avg,
            'media_avg_diff': media_avg_diff,
            'problem_count': problem_count,  # 发现问题数
            'problem_avg': problem_avg,
            'problem_avg_diff': problem_avg_diff,
            'yecha_count': yecha_count,  # 夜查次数
            'yecha_avg': yecha_avg,
            'yecha_avg_diff': yecha_avg_diff,
            'yecha_avg_percent': yecha_avg_percent,
            'mh_count': mh_count,  # 中高质量问题
            'mh_avg': mh_avg,
            'xc_count': xc_count,
            'mh_avg_diff': mh_avg_diff,
            'chk_score_total': chk_score_total,  # 检查质量分
            'chk_score_avg': chk_score_avg,  # 量化人员平均质量分
            'chk_score_avg_diff': chk_score_avg_diff,
            'chk_score_number_avg': chk_score_number_avg,  # 平均问题质量分
            'chk_score_std_avg': chk_score_std_avg,  # 单位人均质量分
            'sys_mean': sys_mean,
            'status': 1
        }
    else:
        return {
            'station': '',
            'qt_count': '',
            'check_count': '',  # 检查次数
            'check_avg': '',
            'check_avg_diff': '',
            'qt_cpl_rate': '',  # 完成率
            'media_len': '',  # 调阅时长
            'media_avg': '',
            'media_avg_diff': '',
            'problem_count': '',  # 发现问题数
            'problem_avg': '',
            'problem_avg_diff': '',
            'yecha_count': '',  # 夜查次数
            'yecha_avg': '',
            'yecha_avg_diff': '',
            'yecha_avg_percent': '',
            'mh_count': '',  # 中高质量问题
            'mh_avg': '',
            'xc_count': '',
            'mh_avg_diff': '',
            'chk_score_total': '',  # 检查质量分
            'chk_score_avg': '',  # 量化人员平均质量分
            'chk_score_avg_diff': '',
            'chk_score_number_avg': '',  # 平均问题质量分
            'chk_score_std_avg': '',  # 单位人均质量分
            'sys_mean': '',
            'status': 0
        }


def get_evaluate_desc(eval_info, station_list):
    """
    计算履职评价信息中，按不同站段
    :param eval_info: 检查评价
    :param station_list: {list} 站段列表
    :return:
    """
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.NAME
        } for it in station_list[(station_list['TYPE'] == 4) & (station_list['SHORT_NAME'] != '')].itertuples()
    ]
    unit_list = []

    for st in st_list:
        sid, name = st['station_id'], st['station_name']
        cv = eval_info[eval_info['RESP_STATION_ID'] == sid]
        chk_eval_count = cv.shape[0]
        # 被路局评价
        cv0 = cv[cv['CHECK_TYPE'].isin([1])]
        chk_eval_count_grp = cv0.shape[0]
        if chk_eval_count_grp > 0:
            chk_eval_grp_list = [{
                'code': it.CODE,
                'count': it.PK_ID
                } for it in cv0.groupby('CODE').count().reset_index().
                sort_values('PK_ID', ascending=False).head(3).itertuples()
            ]
        else:
            chk_eval_grp_list = []

        # 非路局部门评价的
        cv1 = cv[~cv['CHECK_TYPE'].isin([2])]
        chk_eval_count_st = cv1.shape[0]
        if chk_eval_count_st > 0:
            chk_eval_st_list = [{
                'code': it.CODE,
                'count': it.PK_ID
                } for it in cv1.groupby('CODE').count().reset_index().
                sort_values('PK_ID', ascending=False).head(3).itertuples()
            ]
        else:
            chk_eval_st_list = []

        phase = {
            'station': name,
            'chk_eval_count': chk_eval_count,  # 被评价总条数
            'chk_eval_count_grp': chk_eval_count_grp,  # 路局评价
            'chk_eval_grp_list': chk_eval_grp_list,  # 站段评价问题清单
            'chk_eval_count_st': chk_eval_count_st,  # 站段自评
            'chk_eval_st_list': chk_eval_st_list  # 站段评价的问题清单
        }
        unit_list.append(phase)
    return unit_list


# (三)1.正科干部履职情况
def _get_zhengke_evaluate(eval_info, eval_info_total, eval_rank):
    eva0 = eval_info[eval_info['GRADATION'].str.contains('正科')]
    eva1 = eval_info_total[eval_info_total['GRADATION'].str.contains('正科')]
    eval_arr = [eva0, eva1]
    person_count = len(eva0['RESPONSIBE_ID_CARD'].unique().tolist())
    content = []
    # 局评价的
    grp_eval = eva0[eva0['CHECK_TYPE'] == 1].sort_values(by='SCORE', ascending=False)
    for i, k in grp_eval.iterrows():
        dic = {
            'station': k['RESP_STATION'],
            'job': k['JOB'],
            'name': k['RESPONSIBE_PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    # 累计计分
    gb_arr = [
        d.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum().
            reset_index().sort_values('SCORE', ascending=False)
        for d in eval_arr
    ]

    # 本月扣分的前五民
    this_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[0].head(5).itertuples()
    ]

    # 本年扣分的前五民
    total_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[1].head(5).itertuples()
    ]

    eval_rank_desc = _get_eval_rank_of_position(eval_rank, ['安全科', '技术科', '职教科'], ['正科级'], 3)

    # 累计记分处理情况表
    eva_now = eva0.groupby(['RESPONSIBE_ID_CARD']).sum().reset_index()
    eva_year = eva1.groupby(['RESPONSIBE_ID_CARD']).sum().reset_index()
    # 记分人数
    now_person_count = len(eva_now)
    year_person_count = len(eva_year)
    now = [now_person_count, 0, 0, 0, 0, 0, 0, 0]
    year = [year_person_count, 0, 0, 0, 0, 0, 0, 0]
    for i in eva_now['SCORE']:
        if i < 2:
            now[1] += 1
        elif i < 4:
            now[2] += 1
        elif i < 6:
            now[3] += 1
        elif i < 8:
            now[4] += 1
        elif i < 10:
            now[5] += 1
        elif i < 12:
            now[6] += 1
        else:
            now[7] += 1

    for i in eva_year['SCORE']:
        if i < 2:
            year[1] += 1
        elif i < 4:
            year[2] += 1
        elif i < 6:
            year[3] += 1
        elif i < 8:
            year[4] += 1
        elif i < 10:
            year[5] += 1
        elif i < 12:
            year[6] += 1
        else:
            year[7] += 1
    return {
        'person_count': person_count,
        'problem_desc': content,
        'this_score_list': this_score_list,
        'total_score_list': total_score_list,
        'eval_rank': eval_rank_desc,
        'now': now,
        'year': year
    }


# (三)2.副科及干部履职情况
def _get_fuke_evaluate(eval_info, eval_info_total):
    # XX月份，副科职及以下干部XX人被记分，另有XX名非干部管理人员被评价记分。
    eva0 = eval_info[eval_info['LEVEL'].isin(['副科级', '一般管理'])]
    eva1 = eval_info_total[eval_info_total['LEVEL'].isin(['副科级', '一般管理'])]
    eval_arr = [eva0, eva1]
    person_count = len(set(eva0['RESPONSIBE_ID_CARD']))
    person_count_x = len(set(eva0[eva0['LEVEL'] == '一般管理']['RESPONSIBE_ID_CARD']))

    # 累计计分
    gb_arr = [
        d.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum().
            reset_index().sort_values('SCORE', ascending=False)
        for d in eval_arr
    ]

    # 本月扣分的前五民
    this_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[0].head(5).itertuples()
    ]

    # 本年扣分的前五民
    total_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[1].head(5).itertuples()
    ]

    # 集团检查评价 + 从高到低前20名履职评价人员履职问题及记分
    eva_c1 = eva0[eva0['CHECK_TYPE'] == 1]
    gc1 = eva_c1.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum()
    gc1 = gc1.reset_index().sort_values('SCORE', ascending=False)
    person_list = []
    for it in gc1.head(20).itertuples():
        ev = eva_c1[eva_c1['RESPONSIBE_ID_CARD'] == it.RESPONSIBE_ID_CARD].sort_values('SCORE', ascending=False)
        detail = [
            {
                'score': round(float(i.SCORE), 2),
                'content': i.EVALUATE_CONTENT
            } for i in ev.itertuples()
        ]
        person_list.append({
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'total_score': round(float(it.SCORE), 2),
            'detail': detail
        })

    return {
        'person_count': person_count,
        'person_count_x': person_count_x,  # 非干部人数
        'this_score_list': this_score_list,
        'total_score_list': total_score_list,
        'person_list': person_list  # 20名履职评价人员履职问题及记分
    }


def _get_eval_rank_of_position(eval_rank, sections, levels, count):
    head_list = []
    tail_list = []
    try:
        ev = eval_rank[eval_rank['DEPARTMENT'].isin(sections)]
        ev = ev[ev['LEVEL'].isin(levels)]
        rk = ev.sort_values('evaluate_score', ascending=False)
        head_list = [
            {
                'dept': str(it.ALL_NAME).replace('-', ''),
                'job': it.JOB,
                'name': it.NAME,
                'score': it.evaluate_score
            } for it in rk.head(count).itertuples()
        ]

        tail_list = [
            {
                'dept': str(it.ALL_NAME).replace('-', ''),
                'job': it.JOB,
                'name': it.NAME,
                'score': it.evaluate_score
            } for it in rk.tail(count).itertuples()
        ]
    except BaseException as ex:
        pass
    return {
        'head_list': head_list,
        'tail_list': tail_list
    }


# --------------------------------------- 第一章End
def _get_second():
    check_quality = _check_quality_analysis()
    return {
        'check_quality': check_quality
    }


def _check_quality_analysis():
    # （一）监督检查质量分析
    qt0 = local_data.qt_data
    qt1 = local_data.qt_data_prev
    cpd0 = local_data.check_problem_data_this
    cpd1 = local_data.check_problem_data_prev
    general = _check_quality_total(qt0, qt1, cpd0, cpd1)
    # 车间检查质量情况分析
    person = local_data.person
    chk = local_data.check_problem_data_this
    station = local_data.station_list
    shop_freq = _get_shop_freq(person, chk, station)
    # 整改质量分析
    cq_data = local_data.chaoqi_data
    zg_freq = _get_zg_freq(cq_data)
    # 考核质量分析
    assess_data = local_data.assess_money
    return_data = local_data.return_money
    kh_freq = _get_kh_freq(person, chk, station, assess_data, return_data)
    # 音视频管理使用情况分析
    mv_analysis = _get_mv_analysis(chk)
    # 站段分析中心工作质量分析
    center = local_data.center
    qt_center = local_data.qt_center
    evaluate_this = local_data.evaluate_info_this
    analysis_center_freq = _get_analysis_center_freq(person, center, chk, qt_center, evaluate_this)
    # 违章大王情况
    iiiledge = _get_iiiledge(assess_data)
    return {
        'general': general,
        'shop_freq': shop_freq,
        'zg_freq': zg_freq,
        'kh_freq': kh_freq,
        'mv_analysis': mv_analysis,
        'analysis_center_freq': analysis_center_freq,
        'iiiledge': iiiledge
    }


def _get_iiiledge(assess_data):
    # 违章大王人数
    wz_data = assess_data[assess_data['IS_OUT_SIDE_PERSON'] == 0].groupby('ID_CARD').sum().reset_index()
    wz = wz_data[wz_data['RESPONSIBILITY_SCORE'] >= 8]
    wz_count = len(wz)
    wz_id = list(wz['ID_CARD'])

    main_stations = []
    gener_stations = []
    for i in wz_id:
        # 严重违章大王
        data1 = assess_data[(assess_data['ID_CARD'] == i) & (assess_data['LEVEL'].isin(['A', 'B']))]
        if len(data1) >= 3:
            main_stations.append({
                'name': data1['PERSON_NAME'].iloc[0],
                'dp': data1['NAME'].iloc[0]
            })
        # 一般违章大王
        data2 = assess_data[(assess_data['ID_CARD'] == i) & (assess_data['LEVEL'].isin(['A', 'B', 'C']))]
        if len(data2) >= 3:
            gener_stations.append({
                'name': data2['PERSON_NAME'].iloc[0],
                'dp': data2['NAME'].iloc[0]
            })
    # 按站段分布
    sta_data = assess_data[assess_data['ID_CARD'].isin(wz_id)].groupby('NAME').count().reset_index()
    all_station = []
    for i, j in sta_data.iterrows():
        dic = {
            'name': j['NAME'],
            'count': int(j['LEVEL'])
        }
        all_station.append(dic)
    return {
        'wz_count': wz_count,
        'main_count': len(main_stations),
        'gener_count': len(gener_stations),
        'main_stations': main_stations,
        'gener_stations': gener_stations,
        'all_station': all_station
    }


def _get_analysis_center_freq(person, center, chk, qt_center, evaluate_this):
    # 站段分析中心ID
    st_list = [
        {
            'station_id': it.STATION_ID,
            'station_name': it.NAME,
            'shop_id': it.DEPARTMENT_ID
        } for it in center.itertuples()
    ]
    all_center = []
    for i in st_list:
        info = chk[chk['CHECK_DEPT_ID'] == i['shop_id']].drop_duplicates(subset=['PK_ID_CK']).reset_index()
        problem = chk[chk['CHECK_DEPT_ID'] == i['shop_id']].drop_duplicates(subset=['PK_ID_CP']).reset_index()
        qt = qt_center[qt_center['FK_DEPARTMENT_ID'] == i['station_id']]
        eva = evaluate_this[evaluate_this['CHECK_PERSON_DEPARTMENT_ID'] == i['shop_id']]
        evaed = evaluate_this[evaluate_this['RESPONSIBE_DEPARTMENT_ID'] == i['shop_id']]
        # 人数
        person_count = len(person[person['DEPARTMENT_ID'] == i['shop_id']])
        # 分析中心得分
        center_score = round(float(sum(list(qt['MONITOR_PROBLEM_SCORE']))), 2)
        # 现场检查次数
        xc = len(info[info['CHECK_WAY'].isin([1, 2])])
        # 调阅时长
        time = round(float(sum(list(qt['MONITOR_TIME']))), 4)
        # 人均检查次数
        check_avg = round(calc_rate(len(info), person_count), 2)
        # 人均调阅时长
        time_avg = round(calc_rate(time, person_count), 4)
        # 发现问题数
        pro = len(problem)
        # 人均发现问题数
        pro_avg = round(calc_rate(pro, person_count))
        # 中高质量问题
        middle = len(problem[problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        # 人均中高质量问题
        middle_avg = round(calc_rate(middle, person_count), 2)
        # 检查问题质量分
        check_score = round(float(problem['CHECK_SCORE'].sum()), 2)
        # 人均检查问题质量分
        check_score_avg = round(calc_rate(check_score, person_count), 2)
        # 履职评价条数
        eva_count = len(eva)
        # 主动评价条数
        zd_count = len(eva[eva['EVALUATE_WAY'].isin([1, 2, 3])])
        # 中心人均评价分
        center_score_avg = round(calc_rate(float(eva['SCORE'].sum()), person_count), 4)
        # 上级检查中心问题数
        top_check = int(sum(qt.dropna(subset=['CHECK_ANALYSIS_CENTER_PROBLEM'])['CHECK_ANALYSIS_CENTER_PROBLEM'].tolist()))
        # 上级评价中心问题数
        top_eva = len(evaed[evaed['CHECK_TYPE'] == 1])
        # 中心人均被记分
        top_score_avg = round(calc_rate(float(sum(list(qt.dropna(subset=['CHECK_ANALYSIS_CENTER_SCORE'])['CHECK_ANALYSIS_CENTER_SCORE']))), person_count), 2)
        dic = {
            'name': i['station_name'],
            'center_score': center_score,
            'person_count': person_count,
            'xc': xc,
            'time': time,
            'check_avg': check_avg,
            'time_avg': time_avg,
            'pro': pro,
            'pro_avg': pro_avg,
            'middle': middle,
            'middle_avg': middle_avg,
            'check_score': check_score,
            'check_score_avg': check_score_avg,
            'eva_count': eva_count,
            'zd_count': zd_count,
            'center_score_avg': center_score_avg,
            'top_check': top_check,
            'top_eva': top_eva,
            'top_score_avg': top_score_avg
        }
        all_center.append(dic)
    return {
        'all_center': all_center
    }


def _get_mv_analysis(chk):
    chk = chk.drop_duplicates(subset=['PK_ID_CK']).reset_index()
    check_data = chk[chk['CHECK_WAY'].isin([3, 4])]
    # 音视频设备管理不到位
    mv_bdw = check_data[check_data['CHECK_ITEM_NAME'] == '车务-行车自管设备及备品-音视频监控设备']
    all_mv_bdw = []
    bdw_sum = 0
    for i in mv_bdw['NAME'].unique():
        data = mv_bdw[mv_bdw['NAME'] == i]
        e1 = len(data[data['LEVEL'] == 'E1'])
        e2 = len(data[(data['LEVEL'].isin(['E2', 'E3'])) & (data['TYPE'].isin([1, 2]))])
        bdw_sum += e1+e2
        dic = {
            'name': i,
            'count': e1+e2
        }
        all_mv_bdw.append(dic)
    # 音视频系统运用情况
    dy_sum = 0
    all_dy = []
    for i in check_data['NAME'].unique():
        data = check_data[(check_data['NAME'] == i) & (check_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
        dic = {
            'name': i,
            'count': len(data)
        }
        dy_sum += len(data)
        all_dy.append(dic)
    # 调车移动记录仪运用情况
    ydjl_data = check_data[check_data['CHECK_ITEM_NAME'] == '车务-调车-移动记录仪运用']
    ydjl_sum = 0
    all_ydjl = []
    for i in ydjl_data['NAME'].unique():
        data = ydjl_data[(ydjl_data['NAME'] == i) & (ydjl_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
        dic = {
            'name': i,
            'count': len(data)
        }
        ydjl_sum += len(data)
        all_ydjl.append(dic)
    return {
        'all_mv_bdw': all_mv_bdw,
        'bdw_sum': bdw_sum,
        'dy_sum': dy_sum,
        'all_dy': all_dy,
        'ydjl_sum': ydjl_sum,
        'all_ydjl': all_ydjl,
        'ydjl_count': len(ydjl_data)
    }


def _get_kh_freq(person, chk, station_list, assess_data, return_data):
    # 获取所有站段
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.ALL_NAME
        } for it in station_list[(station_list['TYPE'] == 4) & (station_list['SHORT_NAME'] != '')].itertuples()
    ]
    all_stations = []
    for i in st_list:
        person_data = person[person['DEPARTMENT_ID'] == i['station_id']]
        chk_data = chk[chk['CHECK_DEPT_ID'] == i['station_id']]
        pro_data = chk_data.drop_duplicates(subset=['PK_ID_CP']).reset_index()
        assess = assess_data[assess_data['DEPARTMENT_ID'] == i['station_id']]
        re_money = return_data[return_data['DEPARTMENT_ID'] == i['station_id']]
        # 管理人员
        xc_person = len(person_data)
        # 考核问题数
        kh_count = len(pro_data[pro_data['IS_ASSESS'] == 1])
        # 人均考核
        kh_avg = round(calc_rate(kh_count, xc_person), 2)
        # 考核金额
        assess_money = float(assess['ASSESS_MONEY'].sum())
        # 返奖金额
        re_moneys = float(re_money['ACTUAL_MONEY'].sum())
        # 返奖率
        re_diff = round(calc_rate(re_moneys, assess_money) * 100, 2)
        dic = {
            'name': i['station_name'],
            'manager': xc_person,
            'kh_count': kh_count,
            'kh_avg': kh_avg,
            'assess_money': assess_money,
            're_moneys': re_moneys,
            're_diff': re_diff
        }
        all_stations.append(dic)
    # 根据条件排序
    kh_sort = sorted(all_stations, key=lambda x: x['kh_avg'], reverse=True)
    re_sort = sorted(all_stations, key=lambda x: x['re_diff'], reverse=True)
    return {
        'kh_sort': kh_sort,
        're_sort': re_sort
    }


def _get_zg_freq(cq_data):
    """
    整改质量分析
    :param cq_data:
    :return:
    """
    # 超期整改问题个数
    cq_zg = cq_data[cq_data['TYPE'] == 2]
    all_station = []
    for i, j in cq_zg.groupby('NAME').count().reset_index().iterrows():
        dic = {
            'name': j['NAME'],
            'count': int(j['TYPE'])
        }
        all_station.append(dic)
    return {
        'cq_count': len(cq_zg),
        'all_station': all_station
    }


def _get_shop_freq(person, chk, station_list):
    """
    车间检查质量情况分析
    :param person:
    :param chk:
    :param station_list:
    :return:
    """
    # 获取所有车间
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.ALL_NAME
        } for it in station_list[station_list['TYPE'] == 7].itertuples()
    ]
    all_shops = []
    for i in st_list:
        person_data = person[person['DEPARTMENT_ID'] == i['station_id']]
        chk_data = chk[chk['CHECK_DEPT_ID'] == i['station_id']]
        ck_data = chk_data.drop_duplicates(subset=['PK_ID_CK']).reset_index()
        pro_data = chk_data.drop_duplicates(subset=['PK_ID_CP']).reset_index()
        # 管理人员
        xc_person = len(person_data[person_data['IDENTITY'] == '干部'])
        # 现场检查
        xc_count = len(ck_data[ck_data['CHECK_WAY'].isin([1, 2])])
        # 人均现场检查
        xc_avg = round(calc_rate(xc_count, xc_person), 2)
        # 夜查
        yc_count = len(ck_data[ck_data['IS_YECHA'].isin([1])])
        # 人均夜查
        yc_avg = round(calc_rate(yc_count, xc_person), 2)
        # 高质量
        high_count = len(pro_data[pro_data['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])])
        # 人均高质量
        high_avg = round(calc_rate(high_count, xc_person), 2)
        # 发现问题质量分
        score = float(pro_data['CHECK_SCORE'].sum())
        # 人均质量分
        score_avg = round(calc_rate(score, xc_person), 2)
        dic = {
            'name': i['station_name'],
            'manager': xc_person,
            'xc_count': xc_count,
            'xc_avg': xc_avg,
            'yc_count': yc_count,
            'yc_avg': yc_avg,
            'high_count': high_count,
            'high_avg': high_avg,
            'score': score,
            'score_avg': score_avg
        }
        all_shops.append(dic)
    # 根据条件排序
    xc_sort = sorted(all_shops, key=lambda x: x['xc_avg'], reverse=True)
    yc_sort = sorted(all_shops, key=lambda x: x['yc_avg'], reverse=True)
    high_sort = sorted(all_shops, key=lambda x: x['high_avg'], reverse=True)
    score_sort = sorted(all_shops, key=lambda x: x['score_avg'], reverse=True)
    return {
        'xc_sort': xc_sort,
        'yc_sort': yc_sort,
        'high_sort': high_sort,
        'score_sort': score_sort
    }


def _check_quality_total(qt_this, qt_prev, check_pro_data, check_pro_data_prev):
    """
    计算监督检查质量分析情况
    :param qt_this: 本月量化数据
    :param qt_prev: 上月的量化数据
    :param check_pro_data: 检查数据与问题（本月）
    :param check_pro_data_prev: 检查信息与问题（上月）
    :return:
    """
    major = _check_quality_unit_situation(qt_this, qt_prev, check_pro_data, check_pro_data_prev)
    # 运输处的数据
    ts_qt_this = qt_this[qt_this['STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_qt_prev = qt_prev[qt_prev['STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_check_pro_data = check_pro_data[check_pro_data['CHECK_STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_check_pro_data_prev = check_pro_data_prev[check_pro_data_prev['CHECK_STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    trans_admin = _check_quality_unit_situation(ts_qt_this, ts_qt_prev, ts_check_pro_data, ts_check_pro_data_prev)
    return {
        'major': major,
        'trans_admin': trans_admin
    }


def _check_quality_unit_situation(qt_this, qt_prev, check_pro_data, check_pro_data_prev):
    # 车务专业的量化考核干部清单, 用于筛选检查信息清单
    # 车务专业的量化考核干部清单, 用于筛选检查信息清单
    cadre0 = qt_this[['ID_CARD', 'PERSON_NAME', 'STATION_ID', 'REALITY_NUMBER']]
    cadre0.rename(columns={"ID_CARD": "QT_ID_CARD", "PERSON_NAME": "QT_PERSON_NAME",
                           "STATION_ID": "QT_STATION_ID"}, inplace=True)
    cadre1 = qt_prev[['ID_CARD', 'PERSON_NAME', 'STATION_ID']]
    cadre1.rename(columns={"ID_CARD": "QT_ID_CARD", "PERSON_NAME": "QT_PERSON_NAME",
                           "STATION_ID": "QT_STATION_ID"}, inplace=True)

    cpd0 = check_pro_data.copy()
    cpd1 = check_pro_data_prev.copy()
    cpd0 = pd.merge(left=cpd0, right=cadre0, left_on='CHECK_ID_CARD', right_on='QT_ID_CARD')
    cpd1 = pd.merge(left=cpd1, right=cadre1, left_on='CHECK_ID_CARD', right_on='QT_ID_CARD')

    # 删除重复的记录，保证每个人员一条检查信息（数据中，包含了检查人以及问题，所以一个检查信息出现多次）
    ck0 = cpd0.drop_duplicates(['PK_ID_CK', 'CHECK_ID_CARD'])
    ck1 = cpd1.drop_duplicates(['PK_ID_CK', 'CHECK_ID_CARD'])

    ck_arr = [ck0, ck1]
    # 计算夜查次数/夜查率/环比
    # 本段计算出来的检查次数，是检查人次，大于检查次数。
    # 检查次数兑现率
    xc_cpl_rate = calc_rate(int(qt_this['REALITY_NUMBER'].sum()), int(qt_this['CHECK_TIMES_TOTAL'].sum()))
    yc_counts = [
        len(d[(d['IS_YECHA'] == 1) & (d['CHECK_WAY'].isin([1, 2]))]) for d in ck_arr
    ]
    xc_counts = [
        len(d[d['CHECK_WAY'].isin([1, 2])]) for d in ck_arr
    ]
    xc_count_ring = calc_ratio(xc_counts[0], xc_counts[1])
    yc_count_ring = calc_ratio(yc_counts[0], yc_counts[1])
    # 夜查率
    yc_rate0 = calc_rate(yc_counts[0], xc_counts[0])
    yc_rate1 = calc_rate(yc_counts[1], xc_counts[1])
    # 夜查率环比
    yc_rate_ring = calc_ratio(yc_rate0, yc_rate1)

    # 发现问题个数/兑现率
    pro_cpl_rate = calc_rate(int(qt_this['REALITY_PROBLEM_NUMBER'].sum()), int(qt_this['PROBLEM_NUMBER_TOTAL'].sum()))
    pb0 = cpd0.drop_duplicates(['PK_ID_CP'])
    pb1 = cpd1.drop_duplicates(['PK_ID_CP'])
    # 现场检查发现的问题
    pb_arr = [pb0, pb1]
    xc_pro_counts = [len(d[d['CHECK_WAY'].isin([1, 2])]) for d in pb_arr]
    xc_pro_ring = calc_ratio(xc_pro_counts[0], xc_pro_counts[1])
    # 其中，现场作业类问题
    xczy_pro_counts = [len(d[(d['CHECK_WAY'].isin([1, 2])) & (d['LEVEL'].isin(['A', 'B', 'C', 'D']))]) for d in pb_arr]
    # 高质量问题/环比
    xcgzl_pro_counts = [len(d[(d['CHECK_WAY'].isin([1, 2])) & (d['LEVEL'].isin(['A', 'B', 'F1']))]) for d in pb_arr]
    xcgzl_pro_ring = calc_ratio(xcgzl_pro_counts[0], xcgzl_pro_counts[1])

    # 音视频调阅
    md_cpl_rate = round(float(calc_rate(qt_this['MEDIA_REALITY_TIME'].sum(), qt_this['MONITOR_NUMBER_TOTAL'].sum())), 4)
    md_time = round(float(qt_this['MEDIA_REALITY_TIME'].sum()), 2)

    # 调阅发现问题数量
    md_pro_counts = [len(d[(d['CHECK_WAY'].isin([3])) & (d['LEVEL'].isin(['A', 'B', 'C', 'D']))]) for d in pb_arr]
    md_pro_ring = calc_ratio(md_pro_counts[0], md_pro_counts[1])
    # 调阅发现的作业类高质量问题
    mdgzl_pro_counts = [len(d[(d['CHECK_WAY'].isin([3])) & (d['LEVEL'].isin(['A', 'B']))]) for d in pb_arr]
    mdgzl_pro_ring = calc_ratio(mdgzl_pro_counts[0], mdgzl_pro_counts[1])

    content = {
        'xc_count': xc_counts[0],  # 现场检查次数
        'xc_count_ring': xc_count_ring,  # 现场检查次数环比
        'xc_cpl_rate': xc_cpl_rate,  # 现场检查兑现率（量化任务对比）
        'yc_count': yc_counts[0],  # 夜查数
        'yc_count_ring': yc_count_ring,  # 夜查环比
        'yc_rate': yc_rate0,  # 夜查率
        'yc_rate_ring': yc_rate_ring,  # 夜查率环比
        'xc_pro_count': xc_pro_counts[0],  # 现场检查发现问题数
        'pro_cpl_rate': pro_cpl_rate,  # 现场检查发现问题数兑现率
        'xc_pro_ring': xc_pro_ring, # 先查发现问题环比
        'xczy_pro_count': xczy_pro_counts[0],  # 发现的现场作业问题数量
        'xcgzl_pro_count': xcgzl_pro_counts[0],  # 发现的现场高质量问题数量
        'xcgzl_pro_ring': xcgzl_pro_ring,  # 发现的现场高质量问题数量环比
        'md_time': md_time,  # 调阅时长
        'md_cpl_rate': md_cpl_rate,  # 调阅时长兑现率
        'md_pro_count': md_pro_counts[0],  # 监控调阅发现行车作业问题数量
        'md_pro_ring': md_pro_ring,  # 监控调阅发现行车作业问题环比
        'mdgzl_pro_count': mdgzl_pro_counts[0],  # 调阅发现作业类高质量问题
        'mdgzl_pro_ring': mdgzl_pro_ring,  # 调阅发现作业类高质量问题 环比
    }
    return content


def get_third():
    # 干部累计记分及处理标准表
    cadre_cumulative = get_cadre_cumulative(local_data.cadre_cumulative_data)
    return {'cadre_cumulative': cadre_cumulative}


def get_cadre_cumulative(cadre_cumulative_data):
    all_station = []
    index = 0
    for i in cadre_cumulative_data['ALL_NAME'].unique():
        index += 1
        data = cadre_cumulative_data[cadre_cumulative_data['ALL_NAME'] == i]
        level_list = ['局管干部', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']
        level_jg = data.loc[data.GRADATION == level_list[0]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
        level_zk = data.loc[data.GRADATION == level_list[1]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
        level_fkjyx = data.loc[(data.GRADATION == level_list[2]) | (data.GRADATION == level_list[3])].drop_duplicates(subset='RESPONSIBE_ID_CARD')
        level_fg = data.loc[data.GRADATION == level_list[4]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
        # 局管干部数据
        jg_people_count = len(level_jg)
        jg_level_1_count = len(level_jg.loc[level_jg['SCORE'] < 2])
        jg_level_2_count = len(level_jg.loc[(level_jg['SCORE'] >= 2) & (level_jg['SCORE'] < 4)])
        jg_level_3_count = len(level_jg.loc[(level_jg['SCORE'] >= 4) & (level_jg['SCORE'] < 6)])
        jg_level_4_count = len(level_jg.loc[(level_jg['SCORE'] >= 6) & (level_jg['SCORE'] < 8)])
        jg_level_5_count = len(level_jg.loc[(level_jg['SCORE'] >= 8) & (level_jg['SCORE'] < 10)])
        jg_level_6_count = len(level_jg.loc[(level_jg['SCORE'] >= 10) & (level_jg['SCORE'] < 12)])
        jg_level_7_count = len(level_jg.loc[level_jg['SCORE'] >= 12])
        jg = {
            'people_count': jg_people_count,
            'level_1_count': jg_level_1_count,
            'level_2_count': jg_level_2_count,
            'level_3_count': jg_level_3_count,
            'level_4_count': jg_level_4_count,
            'level_5_count': jg_level_5_count,
            'level_6_count': jg_level_6_count,
            'level_7_count': jg_level_7_count,
        }
        # 正科数据
        zk_people_count = len(level_zk)
        zk_level_1_count = len(level_zk.loc[level_zk['SCORE'] < 2])
        zk_level_2_count = len(level_zk.loc[(level_zk['SCORE'] >= 2) & (level_zk['SCORE'] < 4)])
        zk_level_3_count = len(level_zk.loc[(level_zk['SCORE'] >= 4) & (level_zk['SCORE'] < 6)])
        zk_level_4_count = len(level_zk.loc[(level_zk['SCORE'] >= 6) & (level_zk['SCORE'] < 8)])
        zk_level_5_count = len(level_zk.loc[(level_zk['SCORE'] >= 8) & (level_zk['SCORE'] < 10)])
        zk_level_6_count = len(level_zk.loc[(level_zk['SCORE'] >= 10) & (level_zk['SCORE'] < 12)])
        zk_level_7_count = len(level_zk.loc[level_zk['SCORE'] >= 12])
        zk = {
            'people_count': zk_people_count,
            'level_1_count': zk_level_1_count,
            'level_2_count': zk_level_2_count,
            'level_3_count': zk_level_3_count,
            'level_4_count': zk_level_4_count,
            'level_5_count': zk_level_5_count,
            'level_6_count': zk_level_6_count,
            'level_7_count': zk_level_7_count,
        }
        # 副科及以下数据
        fkjyx_people_count = len(level_fkjyx)
        fkjyx_level_1_count = len(level_fkjyx.loc[level_fkjyx['SCORE'] < 2])
        fkjyx_level_2_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 2) & (level_fkjyx['SCORE'] < 4)])
        fkjyx_level_3_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 4) & (level_fkjyx['SCORE'] < 6)])
        fkjyx_level_4_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 6) & (level_fkjyx['SCORE'] < 8)])
        fkjyx_level_5_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 8) & (level_fkjyx['SCORE'] < 10)])
        fkjyx_level_6_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 10) & (level_fkjyx['SCORE'] < 12)])
        fkjyx_level_7_count = len(level_fkjyx.loc[level_fkjyx['SCORE'] >= 12])
        fkjyx = {
            'people_count': fkjyx_people_count,
            'level_1_count': fkjyx_level_1_count,
            'level_2_count': fkjyx_level_2_count,
            'level_3_count': fkjyx_level_3_count,
            'level_4_count': fkjyx_level_4_count,
            'level_5_count': fkjyx_level_5_count,
            'level_6_count': fkjyx_level_6_count,
            'level_7_count': fkjyx_level_7_count,
        }
        # 非管数据
        fg_people_count = len(level_fg)
        fg_level_1_count = len(level_fg.loc[level_fg['SCORE'] < 2])
        fg_level_2_count = len(level_fg.loc[(level_fg['SCORE'] >= 2) & (level_fg['SCORE'] < 4)])
        fg_level_3_count = len(level_fg.loc[(level_fg['SCORE'] >= 4) & (level_fg['SCORE'] < 6)])
        fg_level_4_count = len(level_fg.loc[(level_fg['SCORE'] >= 6) & (level_fg['SCORE'] < 8)])
        fg_level_5_count = len(level_fg.loc[(level_fg['SCORE'] >= 8) & (level_fg['SCORE'] < 10)])
        fg_level_6_count = len(level_fg.loc[(level_fg['SCORE'] >= 10) & (level_fg['SCORE'] < 12)])
        fg_level_7_count = len(level_fg.loc[level_fg['SCORE'] >= 12])
        fg = {
            'people_count': fg_people_count,
            'level_1_count': fg_level_1_count,
            'level_2_count': fg_level_2_count,
            'level_3_count': fg_level_3_count,
            'level_4_count': fg_level_4_count,
            'level_5_count': fg_level_5_count,
            'level_6_count': fg_level_6_count,
            'level_7_count': fg_level_7_count,
        }

        station = {
            'index': index,
            'name': i,
            'jg': jg,
            'zk': zk,
            'fkjyx': fkjyx,
            'fg': fg
        }

        all_station.append(station)

    res = {
        'all_station': all_station
    }

    return res