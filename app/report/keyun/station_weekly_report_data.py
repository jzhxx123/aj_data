import pandas as pd
from dateutil.relativedelta import relativedelta
from datetime import datetime as dt

from app.data.util import pd_query
from app.report.keyun.station_weekly_report_sql import *


def get_data(start_date, end_date, station_id):
    start_date = dt.strptime(start_date, '%Y-%m-%d')
    end_date = dt.strptime(end_date, '%Y-%m-%d')
    global last_week_start, last_week_end
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = \
    pd_query("""select all_name from t_department where department_id = '{0}'""".format(station_id)).iloc[0][
        'all_name']
    # 检查信息
    # info_data = pd_query(check_info_sql.format(start_date, end_date))
    # 检查问题
    problem_data = pd_query(check_problem_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem = pd_query(check_problem_sql.format(last_week_start, last_week_end, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职评价
    eva_info = pd_query(check_evaluate_sql.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职复查
    # eva_re_info = pd_query(check_evaluate_review_sql.format(start_date, end_date, station_id))
    top = get_top(problem_data)
    first = get_first(problem_data)
    second = get_second(eva_info)
    third = get_third(problem_data, last_problem)

    file_name = f'{start_date}至{end_date}{station_name}安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "hierarchy": "STATION",
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        'station_name': station_name,
        'station_id': station_id,
        "end_day": end_day,
        'major': '客运',
        'created_at': dt.now(),
        'file_name': file_name,
        'top': top,
        "first": first,
        "second": second,
        'third': third
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_ring_ratio(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        return {
            'diff': count,
            'percent': count * 100
        }
    else:
        return {
            'diff': count - last_count,
            'percent': round(ded_zero(count - last_count, last_count) * 100, 2)
        }


def get_top(problem_data):
    # 典型问题
    all_list = []
    data = problem_data[problem_data['LEVEL'].isin(['A', 'B'])]
    for i, k in data.groupby('PROBLEM_CLASSITY_NAME').count().sort_values(by='LEVEL', ascending=False).reset_index().iterrows():
        dic = {
            'name': k['PROBLEM_CLASSITY_NAME'],
            'count': int(k['LEVEL'])
        }
        all_list.append(dic)
    # 路局红线问题
    red_pro = len(data[data['IS_RED_LINE'].isin([1])])
    return {
        'all_list': all_list,
        'red_pro': red_pro
    }


def get_first(problem_data):
    # A类问题数
    a_type = len(problem_data[problem_data['LEVEL'] == 'A'])
    # A类红线数
    a_red_data = problem_data[problem_data['IS_RED_LINE'].isin(['A', 'B'])]
    # A类红线问题内容
    content = a_red_data['DESCRIPTION'].unique().tolist()
    return {
        'a_type': a_type,
        'a_red': len(a_red_data),
        'content': content
    }


def get_second(eva_info):
    # 检查信息逐条评价
    info_zt_data = eva_info[(eva_info['EVALUATE_WAY'] == 1) & (eva_info['EVALUATE_TYPE'] == 1)]
    # 检查问题逐条评价
    pro_zt_data = eva_info[(eva_info['EVALUATE_WAY'] == 1) & (eva_info['EVALUATE_TYPE'] == 2)]
    # 局管、段管干部条数
    all_count = []
    for i in [info_zt_data, pro_zt_data]:
        data = i[i['IDENTITY'] == '干部']
        # 局管
        ju = len(data[data['GRADATION'].isin(['局管领导人员'])])
        # 段管
        duan = len(data[data['GRADATION'].isin(['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员'])])
        all_count.append([ju, duan])
    # 定期评价及复查局管领导干部
    dq = len(eva_info[(eva_info['EVALUATE_WAY'] == 2) & (eva_info['IDENTITY'] == '干部') & (eva_info['GRADATION'] == '局管领导人')])
    # 客运分析组评价
    keyunzu = eva_info[eva_info['CHECK_TYPE'] == 1]
    kyz_count = []
    for i in ['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        kyz_count.append(len(keyunzu[keyunzu['GRADATION'] == i]))
    # 履职评价存在问题
    content = []
    pro_data = eva_info.groupby('SITUATION').count().sort_values(by='GRADATION', ascending=False).reset_index()
    for i, j in pro_data.iterrows():
        dic = {
            'name': j['SITUATION'],
            'count': int(j['GRADATION']),
        }
        content.append(dic)
    return {
        'zt_info': len(info_zt_data),
        'zt_pro': len(pro_zt_data),
        'all_count': all_count,
        'kyz': len(keyunzu),
        'kyz_count': kyz_count,
        'dq': dq,
        'content': content
    }


def get_third(problem_data, last_problem):
    """
    上周存在主要问题
    :param problem_data:
    :param last_problem:
    :return:
    """
    all_list = []
    for i in ['食品安全', '机动车管理', '客运作业纪律', '劳动安全', '客车防火']:
        data = problem_data[problem_data['CHECK_ITEM_NAME'].str.contains(i)]
        last_data = last_problem[last_problem['CHECK_ITEM_NAME'].str.contains(i)]
        # 典型问题
        new_data = data[(data['LEVEL'] == 'A') | (data['IS_RED_LINE'].isin([1, 2]))]
        # 路局检查
        luju_list = []
        luju_data = new_data[new_data['TYPE'].isin([1, 2])].groupby('PROBLEM_POINT').count().sort_values('LEVEL', ascending=False).reset_index()
        for j, k in luju_data.iterrows():
            dic = {
                'name': k['PROBLEM_POINT'],
                'count': int(k['LEVEL'])
            }
            luju_list.append(dic)
        # 站段检查
        zd_list = []
        zd_data = new_data[new_data['TYPE'].isin([3])].groupby('PROBLEM_POINT').count().sort_values('LEVEL', ascending=False).reset_index()
        for j, k in zd_data.iterrows():
            dic = {
                'name': k['PROBLEM_POINT'],
                'count': int(k['LEVEL'])
            }
            zd_list.append(dic)
        dic = {
            'name': i,
            'count': len(data),
            'ratio': get_ring_ratio(len(data), len(last_data)),
            'luju': luju_list,
            'zd': zd_list
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }