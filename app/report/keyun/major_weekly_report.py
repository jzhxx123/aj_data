
from app import mongo
from app.report.keyun.major_weekly_report_data import get_data
from app.report.analysis_report_manager import WeeklyAnalysisReport


class KeyunMajorWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    机务的站段级的周分析报告类。
    """

    def __init__(self):
        super(KeyunMajorWeeklyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='客运')

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "MAJOR",
                "major": self.major
            })

        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data