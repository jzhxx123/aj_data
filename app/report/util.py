#!/usr/bin/python3
# -*- coding: utf-8 -*-
'''
    安全中心日报分析公共函数
    Author: WeiBo
    Date: 2018/11/29
    Desc: daily report common function
'''

from datetime import datetime as dt
from datetime import timedelta
import app.report.analysis_report_manager as manager
import pandas as pd
from dateutil.relativedelta import relativedelta
from docx import Document
from flask import current_app

from app import mongo
from app.data.util import pd_query
from app.utils.common_func import get_today


def get_report_list(start_time, end_time, title, TYPE):
    """获取报告列表
    Arguments:
        start_time {str} -- 起始时间
        end_time {str} -- 结束时间
        title {str} -- 标题
        TYPE {str} -- 报告类型（analysis, daily, evaluate）
    """
    start_date = dt.strptime('2017-10-01', "%Y-%m-%d")
    start_time = dt.strptime(start_time, "%Y-%m-%d")
    end_time = dt.strptime(end_time, "%Y-%m-%d")
    if start_time.day > 25:
        start_time = start_time + relativedelta(months=1)
    start_time = dt(start_time.year, start_time.month, 25)
    if end_time.day < 25:
        end_time = end_time - relativedelta(months=1)
    end_time = dt(end_time.year, end_time.month, 25)
    reports = []
    if start_time < start_date:
        start_time = start_date + relativedelta(days=1)
    while start_time > start_date and start_time <= end_time:
        year = start_time.year
        month = start_time.month
        report_title = title.format(year=str(year), month=str(month))
        date = f'{year}/{month:0>2}/25'
        reports.append({"TITLE": report_title, 'DATE': date, 'TYPE': TYPE})
        start_time = start_time + relativedelta(months=1)
    return reports


def get_weekly_report_list(start_time, end_time, title, TYPE):
    """获取报告列表
    Arguments:
        start_time {str} -- 起始时间
        end_time {str} -- 结束时间
        title {str} -- 标题
        TYPE {str} -- 报告类型（analysis, daily, evaluate, weekly_report）
    """
    start_date = dt.strptime('2017-10-01', "%Y-%m-%d")
    start_time = dt.strptime(start_time, "%Y-%m-%d")
    end_time = dt.strptime(end_time, "%Y-%m-%d")
    if start_time < start_date:
        start_time = start_date + relativedelta(days=1)
    if start_time.weekday() > 3:
        day = 7 - (start_time.weekday() - 3)
    elif start_time.weekday() < 3:
        day = 3 - start_time.weekday()
    else:
        day = 0
    start_date = start_time + relativedelta(days=day)
    reports = []
    end_date = start_date + relativedelta(weeks=1) - relativedelta(days=1)
    while start_date >= start_time and end_date <= end_time:
        report_title = title.format(start=str(start_date)[:10], end=str(end_date)[:10])
        reports.append({
            "TITLE": report_title,
            'start_date': str(start_date)[:10],
            'end_date': str(end_date)[:10],
            'TYPE': TYPE
        })
        start_date = start_date + relativedelta(weeks=1)
        end_date = end_date + relativedelta(weeks=1)
    return reports


def get_station_weekly_report_list(start_time, end_time, title, TYPE, station_name):
    """获取站段报告列表
    Arguments:
        start_time {str} -- 起始时间
        end_time {str} -- 结束时间
        title {str} -- 标题
        TYPE {str} -- 报告类型（analysis, daily, evaluate, weekly_report）
    """
    start_date = dt.strptime('2017-10-01', "%Y-%m-%d")
    start_time = dt.strptime(start_time, "%Y-%m-%d")
    end_time = dt.strptime(end_time, "%Y-%m-%d")
    if start_time < start_date:
        start_time = start_date + relativedelta(days=1)
    if start_time.weekday() > 3:
        day = 7 - (start_time.weekday() - 3)
    elif start_time.weekday() < 3:
        day = 3 - start_time.weekday()
    else:
        day = 0
    start_date = start_time + relativedelta(days=day)
    reports = []
    end_date = start_date + relativedelta(weeks=1) - relativedelta(days=1)
    while start_date > start_time and end_date <= end_time:
        report_title = title.format(start=str(start_date)[:10], end=str(end_date)[:10], station_name=station_name)
        reports.append({
            "TITLE": report_title,
            'start_date': str(start_date)[:10],
            'end_date': str(end_date)[:10],
            'TYPE': TYPE
        })
        start_date = start_date + relativedelta(weeks=1)
        end_date = end_date + relativedelta(weeks=1)
    return reports


def get_department_structrue(major=None, station=None):
    '''获取部门结构'''
    if major is not None and station is None:
        match = {"MAJOR": major}
        majors = [major]
    elif major is not None and station is not None:
        match = {"ALL_NAME": {"$regex": station}}
        majors = [major]
    else:
        match = {}
    majors = ['安监', '供电', '车辆', '工务', '机务', '电务', '车务']
    match['HIERARCHY'] = {"$in": [2, 3, 4, 5]}
    project = {"_id": 0, "NAME": 1, "PARENT_NAME": 1}
    department = pd.DataFrame(
        list(mongo.db['base_department'].find(match, project)))
    result = {}
    for major in majors:
        result[major] = {}
        major_depart = department[department['PARENT_NAME'] == major]
        # levels = list(set(
        #     major_depart['PARENT_HIERARCHY'])).sort(reverse=False)
        # for level in leves:
        if station is not None:
            stations = [station]
        else:
            stations = list(set(major_depart['NAME']))
        for sta in stations:
            station_depart = department[department['PARENT_NAME'] == sta]
            result[major][sta] = {}
            chejians = list(set(station_depart['NAME']))
            if len(chejians) != 0:
                for chejian in chejians:
                    che_depart = department[department['PARENT_NAME'] == chejian]
                    if len(che_depart) != 0:
                        result[major][sta][chejian] = list(
                            set(che_depart['NAME']))
                    else:
                        result[major][sta][chejian] = []
    return result


def _get_info_data(department=None, period=None):
    '''获取检查信息数据'''
    today = get_today()
    if today.day == current_app.config.get('UPDATE_DAY'):
        prefix = 'monthly_'
    else:
        prefix = 'daily_'
    if period is None:
        period = 1
    last_day = today - relativedelta(days=period)
    last_day = int(f'{last_day.year}{last_day.month:0>2}{last_day.day:0>2}')
    info_coll = f'{prefix}detail_check_info'
    match = {"DATE": last_day}
    if department is not None:
        match['DEPARTMENT_ALL_NAME'] = {"$regex": department}
    project = {
        "_id": 0,
        "PK_ID": 1,
        "CHECK_WAY": 1,
        "LEVEL": 1,
        "RISK_NAME": 1,
        "IS_RED_LINE": 1,
        "PROBLEM_NUMBER": 1
    }
    info_data = pd.DataFrame(list(mongo.db[info_coll].find(match, project)))
    info_data = info_data.drop_duplicates('PK_ID')
    info_data = info_data.fillna('')
    return info_data


def _get_problem_data(department=None, period=None):
    '''获取检查问题数据'''
    today = get_today()
    if today.day == current_app.config.get('UPDATE_DAY'):
        prefix = 'monthly_'
    else:
        prefix = 'daily_'
    if period is None:
        period = 1
    last_day = today - relativedelta(days=period)
    last_day = int(f'{last_day.year}{last_day.month:0>2}{last_day.day:0>2}')
    pro_coll = f'{prefix}detail_check_problem'
    match = {"DATE": last_day}
    if department is not None:
        match['DEPARTMENT_ALL_NAME'] = {"$regex": department}
    project = {
        "_id": 0,
        "PK_ID": 1,
        "CHECK_WAY": 1,
        "LEVEL": 1,
        "IS_RED_LINE": 1,
        "PROBLEM_NUMBER": 1
    }
    pro_data = pd.DataFrame(list(mongo.db[pro_coll].find(match, project)))
    pro_data = pro_data.drop_duplicates('PK_ID')
    pro_data = pro_data.fillna('')
    return pro_data


def _get_serious_issue_data(data):
    '''获取性质严重问题数据'''
    data = data[data.LEVEL.str.contains('A', regex=False)
                | data.LEVEL.str.contains('B', regex=False)
                | data.LEVEL.str.contains('C', regex=False)
                | data.LEVEL.str.contains('D', regex=False)
                | data.LEVEL.str.contains('E', regex=False)
                | data.LEVEL.str.contains('F', regex=False)]
    return data


def _get_work_data(data):
    '''获取作业项数据'''
    data = data[data.LEVEL.str.contains('A', regex=False)
                | data.LEVEL.str.contains('B', regex=False)
                | data.LEVEL.str.contains('C', regex=False)
                | data.LEVEL.str.contains('D', regex=False)]
    return data


def _get_equipment_data(data):
    '''获取设备项数据'''
    data = data[data.LEVEL.str.contains('E', regex=False)]
    return data


def _get_manage_data(data):
    '''获取管理项数据'''
    data = data[data.LEVEL.str.contains('F', regex=False)]
    return data


def get_base_check_situation(info_data, pro_data):
    '''检查基本情况'''
    xian_info = len(info_data[info_data['CHECK_WAY'] == 1])
    xian_pro = sum(
        list(info_data[info_data['CHECK_WAY'] == 1]['PROBLEM_NUMBER']))
    yin_info = len(info_data[(info_data['CHECK_WAY'] == 4) | (info_data['CHECK_WAY'] == 5)])
    yin_pro = sum(list(info_data[(info_data['CHECK_WAY'] == 4) | (info_data['CHECK_WAY'] == 5)]['PROBLEM_NUMBER']))
    xing_pro = len(_get_serious_issue_data(pro_data))
    work_count = len(_get_work_data(pro_data))
    equipment_count = len(_get_equipment_data(pro_data))
    manage_count = len(_get_manage_data(pro_data))
    result = {
        "xian": xian_info,  # 现场检查次数
        "xian_pro": xian_pro,  # 现场检查发现问题个数
        "yin": yin_info,  # 音视频调阅次数
        "yin_pro": yin_pro,  # 音视频调阅发现问题个数
        "xing": xing_pro,  # 性质严重问题个数
        "work": work_count,  # 作业项问题个数
        "equipment": equipment_count,  # 设备项问题个数
        "manage": manage_count  # 管理项问题个数
    }
    return result


def get_risk_content(risk_data):
    '''排查风险信息'''
    risks = list(risk_data['RISK_NAME'])
    risks = [risk for names in risks for risk in names.split(',')]
    return risks


def get_daily_report_content(department, date=None):
    info_data = _get_info_data(department)
    # pro_data = _get_problem_data(department)
    # result = get_base_check_situation(info_data, pro_data)
    result = get_risk_content(info_data)
    return result


def combine_word_documents(documents):
    """拼接多个Document对象

    Arguments:
        documents {list} -- Document列表

    Returns:
        Docuemnt -- 拼接后的Document对象
    """
    combined_document = Document()
    for docu in documents:
        for element in docu.element.body:
            combined_document.element.body.append(element)
    return combined_document


def get_safety_produce_data(start_date, end_date, major=None):
    """从mongo中获取安全生产信息

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 安全生产信息
    """
    start_date = int(start_date.replace("-", ''))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    result = []
    match = {
        'DATE': {
            '$gte': start_date,
            "$lte": end_date
        }
    }
    if major is not None:
        match['MAJOR'] = major
    for prefix in prefix_list:
        coll_name = prefix + 'detail_safety_produce_info'
        doc = list(mongo.db[coll_name].find(match, {"_id": 0}))
        result = result + doc
    if len(result) == 0:
        result = [{
            'PK_ID': 1,
            'MAIN_TYPE': 1,
            'DETAIL_TYPE': 1,
            'CATEGORY': 1,
            'OVERVIEW': '1',
            'STATUS': 1,
            'REASON': '1',
            'PROFESSION': '1',
            'RESPONSIBILITY_UNIT': '1',
            'PONDERANCE_NUMBER': 1,
            'IS_MATERIALS': 1,
            'RANK': 1,
            'CODE': '1',
            'NAME': '1',
            'ACCIDENT_IDENTIFIED_NUMBER': '',
            'LINE_NAME': '',
            'TYPE3': '',
            'TYPE4': '',
            'TYPE5': '',
            'RISK_NAME': '',
            'DIRECT_REASON': ' ',
            'RESPONSIBILITY_IDENTIFIED': 1,
            'DATE': 1,
            'MON': 1,
            'CLOCK': 1,
            'RESPONSIBILITY_NAME': 1,
            'RESPONSIBILITY_IDENTIFIED_NAME': '',
            'BELONG': '',
            'MAIN_CLASS': '',
            'DETAIL_CLASS': 1,
            'MAJOR': ''
        }]
    else:
        result = result
    data = pd.DataFrame(result)
    dep_data = pd_query(
        "SELECT DEPARTMENT_ID, NAME AS STATION FROM t_department")
    data = data.drop_duplicates('PK_ID')
    data['TYPE3'] = data['TYPE3'].apply(lambda x: str(x))
    data = pd.merge(
        data,
        dep_data,
        how='left',
        left_on='TYPE3',
        right_on='DEPARTMENT_ID')
    del data['DEPARTMENT_ID']
    data['type'] = data['CODE'].apply(devide_type)
    return data


def devide_type(ONEtype):
    ONEtype = str(ONEtype)
    if 'A' in ONEtype:
        return 'A'
    elif 'B' in ONEtype:
        return 'B'
    elif 'C' in ONEtype:
        return 'C'
    elif 'D' in ONEtype:
        return 'D'
    else:
        return 0


def get_data_group_size(datas, keys):
    result = []
    for data in datas:
        data = data.groupby(keys).size().reset_index().rename(
            columns={0: 'count'})
        data = data.astype({'type': 'str'})
        result.append(data)
    return result


def get_ring_ratio(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        rst = count * 100
    else:
        rst = round((count - last_count) / last_count * 100, 1)
    return rst


def singe_score_section(score):
    """对履职扣分进行分档

    Arguments:
        score {number} -- 扣分数值

    Returns:
        int -- 所属档次
    """
    if score < 2:
        return 1
    elif score < 4:
        return 2
    elif score < 6:
        return 3
    elif score < 8:
        return 4
    elif score < 10:
        return 5
    elif score < 12:
        return 6
    else:
        return 7


def get_check_info_data(start_date, end_date):
    """从mongo中获取检查信息数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """
    start_date = int(start_date.replace("-", ""))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    result = []
    for prefix in prefix_list:
        coll_name = prefix + 'detail_map_check_info'
        doc = list(mongo.db[coll_name].find({
            'DATE': {
                '$gte': start_date,
                "$lte": end_date
            }
        }, {"_id": 0}))
        result = result + doc
    data = pd.DataFrame(result)
    data = data.drop_duplicates(['DATE', 'MAJOR', 'STATION'])
    return data


def get_check_problem_data(start_date, end_date, major=None):
    """从mongo中获取检查信息数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """
    start_date = int(start_date.replace("-", ""))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    result = []
    match = {
        'DATE': {
            '$gte': start_date,
            "$lte": end_date
        }
    }
    if major is not None:
        match['MAJOR'] = major
    for prefix in prefix_list:
        coll_name = prefix + 'map_check_problem'
        doc = list(mongo.db[coll_name].find(match, {"_id": 0}))
        if len(doc) == 0:
            doc = [{
                'DATE': 0,
                'MAJOR':'',
                'STATION': '',
                'TIMES': -1,
                'SERIOUS_COUNT': -1,
                'SCORE': -1
            }]
        else:
            doc = doc
        result = result + doc
    data = pd.DataFrame(result)
    if data.empty is not True:
        data = data.drop_duplicates(['DATE', 'MAJOR', 'STATION'])
    else:
        data = data
    return data

#
# niubi_cards = [
#     '510106197908081417', '510125197409290010', '510802198209070513',
#     '51012519840611581X', '510125198112131216', '511303198403160010',
#     '513401197008231616', '513001197803090857', '530111198203024454',
#     '520111198312165414', '510129198801173548', '511026197108194713',
#     '51100219740501153X', '51102719761025487X', '622826198610041178',
#     '510802198108253513', '510622197305220315', '510106198310261418',
#     '511137197610211413', '510283198008120399', '620202198912260223',
#     '51100219760618121X', '510781198801271816', '420105197302122036',
#     '362502198110296851', '510781197503118739', '510106198611111819',
#     '510625197612110019', '510125198403242311', '511025198407258818',
#     '511024198611225216', '510681198312120317', '513825198711020615',
#     '511024198011173133', '511026197303046417', '513401196310271614',
#     '510106198409031014', '51102619740921022X', '513401197605041618',
#     '320107197502013423', '510106197607211417'
# ]


niubi_cards = [
    "513401196905131610", "510106197503052132",
    "432524198203044537", "510106198409031014",
    "51102619740921022X", "320107197502013423",
    "510106197607211417", "513401196310271614",
    "511026197303046417", "510625197612110019",
]


def get_person_role(idcard):
    role_info = {}
    if idcard in niubi_cards:
        role_info['role'] = 0
        role_info['major'] = ''
        role_info['station'] = ''
    else:
        person_info = pd_query(f'''SELECT
                    a.*,
                    b.`NAME`,
                    b.BUSINESS_CLASSIFY,
                    b.VIEW_TYPE,
                    c.`NAME` AS MAJOR,
                    d.`NAME` AS STATION
                FROM
                    t_person AS a
                    LEFT JOIN t_department AS b
                        ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                    LEFT JOIN t_department as c on c.DEPARTMENT_ID = b.TYPE2
                    LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = b.TYPE3
                    WHERE a.ID_CARD = "{idcard}"''').loc[0]
        if person_info['NAME'] == '局领导' or (
                person_info['VIEW_TYPE'] == 2
                and person_info['BUSINESS_CLASSIFY'] == '领导'):
            role_info['role'] = 0
            role_info['major'] = person_info['MAJOR']
            role_info['station'] = ''
        elif person_info['VIEW_TYPE'] == 3 and \
                person_info['BUSINESS_CLASSIFY'] == '领导':
            role_info['role'] = 1
            role_info['major'] = person_info['MAJOR']
            role_info['station'] = person_info['STATION']
        else:
            role_info['role'] = 2
            role_info['major'] = person_info['MAJOR']
            role_info['station'] = person_info['STATION']
    return role_info


def save_picture_route(file_name, major, interval):
    import os
    # 获取项目根目录路径
    dir_path = os.path.join(manager.get_report_path(interval, major), 'images')
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    pic_path = os.path.join(dir_path, file_name)
    if os.path.exists(pic_path):
        os.remove(pic_path)
    return pic_path


def get_week_interval(current_date, iso_week_end_day):
    """
    获取输入日期的当周的起始日期
    :param current_date: {datetime.date} 当前日期
    :param iso_week_end_day: {int} 周结束日期为该周的第几天。星期一 1 -> 星期天 7。
                             例如路局的周，为上周四至本周三，则 iso_week_end_day = 3
    :return: (date, date) 分别为start_date, end_date
    """
    weekday = current_date.isoweekday()
    diff = 0
    while weekday != iso_week_end_day:
        diff = diff + 1
        weekday = (weekday + 1) % 7
    end_date = current_date + timedelta(days=-diff)
    start_date = end_date + timedelta(days=-6)
    return start_date, end_date


def get_detail_check_problem_data(start_date, end_date, major):
    """从mongo中获取检查问题明细数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """
    start_date = int(start_date.replace("-", ""))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    result = []
    for prefix in prefix_list:
        coll_name = prefix + 'detail_check_problem'
        doc = list(mongo.db[coll_name].find({
            'DATE': {
                '$gte': start_date,
                "$lte": end_date
            },
            'MAJOR': major
        }, {"_id": 0}))
        result = result + doc
    data = pd.DataFrame(result)
    data = data.drop_duplicates(['PK_ID', 'PROBLEM_CLASSIFY', 'RESPONSIBILITY_ID_CARD'])
    return data


def get_check_info_data_ym(start_year_month, end_year_month, major=None, inspect_dpid=None):
    match = {
        'MON': {'$gte': start_year_month, '$lte': end_year_month}
    }
    if major is not None:
        match['MAJOR'] = major
    if inspect_dpid is not None:
        match['INSPECT_DPID'] = inspect_dpid
    project = {
        "_id": 0,
        "PK_ID": 1,
        "CHECK_WAY": 1,
        "IS_YECHA": 1,
        "LEVEL": 1,
        "RISK_NAME": 1,
        "IS_RED_LINE": 1,
        "PROBLEM_NUMBER": 1,
        "INSPECT_DPID": 1
    }
    collection_list = ['daily_detail_check_info', 'monthly_detail_check_info', 'history_detail_check_info']
    data = []
    for info_coll in collection_list:
        info_data = list(mongo.db[info_coll].find(match, project))
        data.extend(info_data)
    df = pd.DataFrame(data)
    df.fillna('')
    return df


def get_check_info_data(start_date, end_date, major=None):
    """从mongo中获取检查信息数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        DataFrame -- 检查问题信息
    """
    start_date = int(start_date.replace("-", ""))
    end_date = int(end_date.replace("-", ""))
    prefix_list = ['daily_', 'monthly_', 'history_']
    match = {
        'DATE': {'$gte': start_date, '$lte': end_date}
    }
    if major is not None:
        match['MAJOR'] = major
    result = []
    for prefix in prefix_list:
        coll_name = prefix + 'detail_map_check_info'
        doc = list(mongo.db[coll_name].find(match, {"_id": 0}))
        if len(doc) == 0:
            doc = [{
                'DATE': 0,
                'MAJOR':'',
                'STATION': '',
                'COUNT': 0
            }]
        else:
            doc = doc
        result = result + doc
    data = pd.DataFrame(result)
    return data