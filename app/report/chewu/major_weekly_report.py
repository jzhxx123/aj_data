#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   month_report_cheliang
Description:
Author:    yuchuang
date:         2019-07-23
-------------------------------------------------
Change Activity:2019-07-23 14:54
-------------------------------------------------
"""
from dateutil.relativedelta import relativedelta

from app.report.chewu.major_weekly_report_data import get_data
from app.report.analysis_report_manager import WeeklyAnalysisReport, restore_numpy_value
from app import mongo
from datetime import datetime

from app.safety_index.workshop_health_index import get_department_name_by_dpid


class ChewuMajorWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    电务的专业级的周分析报告类。
    """

    def __init__(self):
        # 车务周报，周五 -> 周四
        super(ChewuMajorWeeklyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='车务', start_weekday=4)

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date)
        restore_numpy_value(data)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data