from app.data.util import pd_query
from app.report.chewu.station_daily_report_sql import *



def get_data(times, work_sheet, dp_id):
    global date_str
    start_date = times[0]
    end_date = times[1]
    month = end_date.split('-')[1]
    day = end_date.split('-')[2]
    station_name = pd_query("select NAME from t_department where department_id = '{0}'".format(dp_id))['NAME'][0]
    work_sheet['A1'] = '{0}安全分析中心日报表'.format(station_name)
    date_str = '{0}年{1}月{2}日'.format(start_date[:4], month, day)
    work_sheet['A2'] = date_str
    # 履职评价信息
    eva_data = pd_query(check_evaluate_info_sql.format(start_date, end_date, dp_id))
    # 履职复查
    eva_re_data = pd_query(check_evaluate_re_sql.format(start_date, end_date, dp_id))
    # 检查信息
    check_info_data = pd_query(check_info_sql.format(start_date, end_date, dp_id))
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date, dp_id))
    # 超期问题
    zg_data = pd_query(zg_sql.format(start_date, end_date, dp_id))
    # 标记扣分
    mark_data = pd_query(mark_sql.format(start_date, end_date, dp_id))
    # 错误标注扣分
    mistake_data = pd_query(mistake_sql.format(start_date, end_date, dp_id))
    # 监控设备调阅检查
    shebei_data = pd_query(shebei_sql.format(start_date, end_date, dp_id))
    item_data = pd_query(item_sql.format(start_date, end_date, dp_id))
    # 重点信息追踪
    main_data = pd_query(main_info_tracking_sql.format(start_date, end_date, dp_id))
    # 问题风险分布
    risk_data = pd_query(pro_risk_sql.format(start_date, end_date, dp_id))
    # 风险预警
    risk_warning = pd_query(risk_warning_sql.format(start_date, end_date, dp_id))
    first = get_first(eva_data, eva_re_data, work_sheet)
    second = get_second(check_info_data, problem_data, work_sheet)
    third = get_third(zg_data, work_sheet)
    four = get_four(eva_data, mark_data, mistake_data, work_sheet)
    five = get_five(shebei_data, item_data, work_sheet)
    six = get_six(work_sheet)
    seven = get_seven(problem_data, work_sheet)
    eight = get_eight(work_sheet)
    night = get_night(main_data, work_sheet)
    ten = get_ten(risk_data, work_sheet)
    ele = get_ele(risk_warning, work_sheet)
    tw = get_tw(work_sheet)
    return {
        'start_date': start_date,
        'end_date': end_date,
        'times': '{0}年{1}月{2}日'.format(start_date[:4], month, day),
        'title': '{0}安全分析中心日报表'.format(station_name),
        'first': first,
        'second': second,
        'third': third,
        'four': four,
        'five': five,
        'six': six,
        'seven': seven,
        'eight': eight,
        'night': night,
        'ten': ten,
        'ele': ele,
        'tw': tw
    }


def get_first(eva_data, eva_re_data, work_sheet):
    """
        履职评价工作量
    :param eva_data:
    :param eva_re_data:
    :param work_sheet:
    :return:
    """
    all_list = []
    index = 4
    for i in [eva_data, eva_re_data]:
        infos = []
        for j in range(3):
            data = i[i['EVALUATE_TYPE'] == j]
            # 站长
            zz = len(data[data['PERSON_JOB_TYPE'].str.contains('站长')])
            # 安全科长
            aqkz = len(data[data['PERSON_JOB_TYPE'].str.contains('安全科长')])
            # 分析主任
            fxzr = len(data[data['PERSON_JOB_TYPE'].str.contains('分析室主任')])
            # 分析员
            fxy = len(data[data['PERSON_JOB_TYPE'].str.contains('分析员')])
            # 总问题
            pro = len(data.dropna(subset=['LEVEL']))
            # 红线问题
            red_pro = len(data[data['IS_RED_LINE'].isin([1, 2])])
            info = [zz, aqkz, fxzr, fxy, pro, red_pro]
            # 问题类型
            new_data = data.dropna(subset=['LEVEL'])
            for k in ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'K']:
                info.append(len(new_data[new_data['LEVEL'] == k]))
            index1 = 0
            for k in ['E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S']:
                work_sheet['{0}{1}'.format(k, index)] = info[index1]
                index1 += 1
            index += 1
            infos.append(info)
        all_list.append(infos)
    return all_list


def get_second(check_info_data, problem_data, work_sheet):
    # 录入信息条数
    infos = len(check_info_data)
    # 领导录入信息条数
    leaders = len(check_info_data[check_info_data['ALL_NAME'].str.contains('领导')])
    # 音视频回放条数
    # 防洪信息条数
    # 检查发现问题
    find_pro = len(problem_data)
    # 路局检查发现问题
    luju_pro = len(problem_data[problem_data['TYPE'].isin([1, 2])])
    # 典型问题统计(A,B,F1)
    main_data = problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1'])]
    pro_list = main_data['PROBLEM_POINT'].unique().tolist()
    content = '二、系统录入情况：{0}共计录入信息{1}条，其中站领导录入{2}条，音视频回放{3}条，防洪信息{4}条；检查发现问题{5}个，' \
              '路局检查发现问题{6}个。典型问题（典型的A、B两违及F1问题，此处只列问题及类型）：{7}'.format(date_str, infos, leaders,
                                                                      0, 0, find_pro, luju_pro, ' '.join(pro_list))
    work_sheet['A10'] = content
    return content


def get_third(zg_data, work_sheet):
    all_list = []
    for i in range(1, 5):
        count = zg_data[zg_data['TYPE'] == i]
        all_list.append(len(count))
    content = '三、系统录入整改、销号（指有无整改到期、过期和销号到期、过期信息，并进行提醒）' \
              '\n1：超期未整改{0}条' \
              '\n2：超期整改{1}条' \
              '\n3：超期未销号{2}条' \
              '\n4：超期销号{3}条'.format(all_list[0], all_list[1], all_list[2], all_list[3])
    work_sheet['A13'] = content
    return content


def get_four(eva_data, mark_data, mistake_data, work_sheet):
    # 履职扣分
    eva_sit = eva_data[eva_data['SCORE'] != 0]['EVALUATE_CONTENT'].tolist()
    # 标记扣分
    mark_sit = mark_data['DESCRIPTION'].tolist()
    # 错误标注
    mistake_sit = mistake_data['DESCRIPTION'].tolist()
    content = "四、履职评价扣分、标记或错误标注情况及原因：" \
              "\n1.履职评价扣分{0}人,原因为: {1}" \
              "\n2.标记{2}人, 原因为: {3}" \
              "\n3.错误标注{4}人, 原因为: {5}".format(len(eva_sit), ' '.join(eva_sit), len(mark_sit), ' '.join(mark_sit),
                                              len(mistake_sit), ' '.join(mistake_sit))
    work_sheet['A14'] = content
    return content


def get_five(shebei_data, item_data, work_sheet):
    """
    监控设备调阅情况
    :param shebei_data:
    :param item_data:
    :param work_sheet:
    :return:
    """
    all_count = []
    for i in ['调车作业', '接发列车', '客运组织', '装卸作业', '施工作业']:
        new_data = item_data[item_data['ALL_NAME'].str.contains(i)]
        all_count.append(len(new_data))
    for i in ['作业记录仪', 'TDCS回放', '数调电话', '无线列调电话']:
        new_data = shebei_data[(shebei_data['CHECK_WAY'] == 3) & (shebei_data['RETRIVAL_TYPE_NAME'].str.contains(i))]
        all_count.append(int(new_data['COST_TIME'].sum()))
    all_count.append(int(shebei_data['PROBLEM_NUMBER'].sum()))
    content = "五、监控设备调阅：盯控调车作业{0}批次、接发列车{1}站次、客运组织{2}站次、装卸作业{3}站次、施工作业{4}站次。" \
              "调车作业记录仪回放{5}小时、TDCS回放{6}小时、数调电话、无线列调电话回放{7}小时等等，发现问题{8}个。".format(all_count[0],
                                                                                 all_count[1], all_count[2], all_count[3],
                                                                                 all_count[4], all_count[5], all_count[6],
                                                                                 all_count[7], all_count[8])
    work_sheet['A15'] = content
    return content


def get_six(work_sheet):
    pass


def get_seven(problem_data, work_sheet):
    """
    站段典型问题情况
    :param problem_data:
    :param work_sheet:
    :return:
    """
    main_pro = problem_data.groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index()
    all_content = []
    for i, k in main_pro.iterrows():
        data = problem_data[problem_data['PROBLEM_POINT'] == k['PROBLEM_POINT']]['DESCRIPTION'].tolist()
        content = '{0}.{1}{2}个, 原因为: {3}'.format(i+1, k['PROBLEM_POINT'], k['LEVEL'], '\n'.join(data))
        all_content.append(content)
    work_sheet['A17'] = '七、本站段典型问题分析' \
                        '{0}'.format('\n'.join(all_content))
    return '\n'.join(all_content)


def get_eight(work_sheet):
    pass


def get_night(main_data, work_sheet):
    """
    重点信息追踪
    :param main_data:
    :param work_sheet:
    :return:
    """
    # 信息概况
    info_sit = main_data['CONTENT'].tolist() if len(main_data['CONTENT'].tolist()) != 0 else ['无']
    # 追踪部门
    track_dp = main_data['TRACKING_DEPARTMENT_NAMES'].tolist() if len(main_data['TRACKING_DEPARTMENT_NAMES']) != 0 else ['无']
    # 追踪事项和要求
    track_result = main_data['TRACKING_REQUEST'].tolist() if len(main_data['TRACKING_REQUEST']) != 0 else ['无']
    # 提交问题
    # 信息概况
    submit_data = main_data[main_data['STATUS'] == 1]
    submit_sit = submit_data['CONTENT'].tolist() if len(submit_data['CONTENT']) != 0 else ['无']
    # 追踪分析结果
    submit_result = submit_data['RESULT'].tolist() if len(submit_data['RESULT']) != 0 else ['无']
    content = '九、本站段日重点信息追踪'\
        '\n1.今日追踪（含信息概况、追踪部门和追踪事项和要求3项内容）：'\
        '\n信息概况：{0}'\
        '\n追踪部门：{1}'\
        '\n追踪事项和要求：{2}'\
    '\n2.今日提交问题追踪结果（含信息概况和追踪分析结果）：'\
        '\n信息概况：{3}'\
        '\n追踪分析结果：{4}'.format(';  '.join(info_sit), ';  '.join(track_dp), ';  '.join(track_result), ';  '.join(submit_sit),
                          ';  '.join(submit_result))
    work_sheet['A19'] = content
    return content


def get_ten(risk_data, work_sheet):
    """
    安全风险问题分布情况
    :param risk_data:
    :param work_sheet:
    :return:
    """
    count = len(risk_data)
    if count == 0:
        count = 1
    risk_data['NEW_NAME'] = risk_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    data = risk_data.groupby('NEW_NAME').count().reset_index()
    all_list = []
    for i, k in data.iterrows():
        content = '{0}问题{1}个占比{2}%'.format(k['NEW_NAME'], k['ALL_NAME'], round(k['ALL_NAME'] / count, 1))
        all_list.append(content)
    work_sheet['A20'] = '十、安全问题风险分布情况:{0}'.format(';  '.join(all_list))
    return '十、安全问题风险分布情况:{0}'.format(';'.join(all_list))


def get_ele(risk_warning, work_sheet):
    """
    安全风险预警
    :param risk_warning:
    :param work_sheet:
    :return:
    """
    warnings = risk_warning['CONTENT'].unique().tolist()
    for i in range(len(warnings)):
        warnings[i] = '{0}'.format(i + 1) + warnings[i]
    content = '\n'.join(warnings)
    work_sheet['A21'] = '十一、安全风险预警' \
                        '{0}'.format(content)
    return content


def get_tw(work_sheet):
    pass