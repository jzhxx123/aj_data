#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   month_report_cheliang
Description:
Author:    yuchuang
date:         2019-07-23
-------------------------------------------------
Change Activity:2019-07-23 14:54
-------------------------------------------------
"""

from app import mongo
from app.report.chewu.station_weekly_report_data import get_data
from app.report.analysis_report_manager import WeeklyAnalysisReport


class ChewuStationWeeklyAnalysisReport(WeeklyAnalysisReport):
    """
    机务的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(ChewuStationWeeklyAnalysisReport, self).__init__(hierarchy_type='STATION', major='车务',
                                                               station_id=station_id)

    def generate_report_data(self, start_date, end_date):
        """
        根据给定的报告起始时间，提取数据
        :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
        :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
        :return:
        """
        data = get_data(start_date, end_date, self.station_id)
        mongo.db['safety_analysis_weekly_report'].delete_one(
            {
                "start_date": start_date,
                "end_date": end_date,
                "hierarchy": "STATION",
                "station_id": self.station_id,
                "major": self.major
            })
        mongo.db['safety_analysis_weekly_report'].insert_one(data)
        return data