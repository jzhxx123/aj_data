import threading
from datetime import datetime as dt

import pandas as pd

from app.data.util import pd_query
from app.report.analysis_report_manager import WeeklyAnalysisReport
from app.report.chewu.common import get_analysis_center_daily_list, \
    get_detail_check_problem_by_ac_raw, HIGH_PROBLEM_LEVEL, PROBLEM_CATEGORY_LIST, MID_HIGH_PROBLEM_LEVEL, \
    calc_ratio_ring, get_detail_check_problem_raw
from app.report.chewu.common_sql import CHECK_EVALUATE_INFO_SQL, MAJOR_DEPARTMENT_DATA_SQL, EVALATE_WORKER_SQL
from app.report.gongwu.common import get_inc_text, get_percent_text

local_data = threading.local()


def get_data(start_date, end_date):
    """
    获周报数据
    :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
    :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
    :return:
    """
    date_params = WeeklyAnalysisReport.get_week_intervals(start_date, end_date)
    local_data.date_params = date_params
    start_day = start_date[8:10]
    end_day = end_date[8:10]
    year = end_date[:4]
    start_month = start_date[5:7]
    end_month = end_date[5:7]
    # 专业及其下属部门的清单
    df_dept = pd_query(MAJOR_DEPARTMENT_DATA_SQL)
    local_data.dept_data = df_dept
    # 检查问题信息
    prob_data_this = get_detail_check_problem_raw(date_params[0][0], date_params[0][1])
    prob_data_prev = get_detail_check_problem_raw(date_params[1][0], date_params[1][1])
    common = {
        'year': year,
        'start_month': start_month,
        'start_day': start_day,
        'end_month': end_month,
        'end_day': end_day
    }

    # 履职评价信息
    eval_data = pd_query(CHECK_EVALUATE_INFO_SQL.format(start_date, end_date))
    eval_data['SITUATION'] = eval_data['SITUATION'].str.replace('\n', '').str.replace('。', '')
    # 履职工作量
    eva_worker = pd_query(EVALATE_WORKER_SQL.format(start_date, end_date))
    # 内容CONTENT
    top = get_top(prob_data_this, prob_data_prev)
    first = get_first(prob_data_this, prob_data_prev)
    second = get_second(eval_data, eva_worker)
    third = get_third(prob_data_this, prob_data_prev)
    fourth = get_fourth()
    fifth = get_fifth()
    file_name = f'{start_date}至{end_date}车务系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "major": '车务',
        "hierarchy": "MAJOR",
        'common': common,
        "created_at": dt.now(),
        'file_name': file_name,
        'top': top,
        'first': first,
        'second': second,
        'third': third,
        'fourth': fourth,
        'fifth': fifth
    }
    return result


def get_top(problem_data_this, problem_data_prev):
    data_arr = [problem_data_this, problem_data_prev]
    data_arr = [
        d[['PK_ID', 'LEVEL', 'IS_RED_LINE', 'CHECK_ITEM_NAME',
           'RESP_DEPT_NAME', 'DESCRIPTION', 'RESP_STATION_ID', 'IS_EXTERNAL']].drop_duplicates('PK_ID')
        for d in data_arr
    ]
    data0, data1 = data_arr[0], data_arr[1]
    counts = [
        d.shape[0] for d in data_arr
    ]
    ratio = calc_ratio_ring(counts[0], counts[1])
    ratio['diff_text'] = get_inc_text(ratio['diff'], '个')
    ratio['ratio_text'] = get_percent_text(ratio['percent'])

    # 性质严重问题
    ser_counts = [
        d[d['LEVEL'].isin(['A', 'B', 'F1'])].shape[0] for d in data_arr
    ]
    ser_ratio = calc_ratio_ring(ser_counts[0], ser_counts[1])
    ser_ratio['diff_text'] = get_inc_text(ser_ratio['diff'], '个')
    ser_ratio['ratio_text'] = get_percent_text(ser_ratio['percent'])

    # 四大类问题数量统计
    cate_list = []
    for it in PROBLEM_CATEGORY_LIST:
        count = data0[data0['LEVEL'].isin(it['level_list'])].shape[0]
        cate_list.append({
            'category': it['category'],
            'count': count
        })

    # 问题类型
    check_type_list = ['车务-接发列车', '车务-调车', '车务-防溜', '劳']
    check_type_names = ['接发列车', '调车', '防溜', '劳安']
    idx = 0
    check_list = []
    for i in check_type_list:
        cts = [
            d[d['CHECK_ITEM_NAME'].str.contains(i)].shape[0] for d in data_arr
        ]
        ratio = calc_ratio_ring(cts[0], cts[1])
        ratio['diff_text'] = get_inc_text(ratio['diff'], '个')
        ratio['ratio_text'] = get_percent_text(ratio['percent'])
        dic = {
            'check_name': check_type_names[idx],
            'count': cts[0],
            'ratio': ratio
        }
        check_list.append(dic)
        idx = idx + 1
    # 红线问题
    red_counts = [
        d[(d['IS_RED_LINE'] == 1) & (d['IS_EXTERNAL'] == 0)].shape[0] for d in data_arr
    ]
    red_ratio = calc_ratio_ring(red_counts[0], red_counts[1])
    red_ratio['diff_text'] = get_inc_text(red_ratio['diff'], '个')
    red_ratio['ratio_text'] = get_percent_text(red_ratio['percent'])

    red_problem = data0[(data0['IS_RED_LINE'] == 1) & (data0['IS_EXTERNAL'] == 0)]
    red_list = red_problem['DESCRIPTION'].tolist()

    gen = {
        'count': counts[0],
        'ratio': ratio,
        'ser_count': ser_counts[0],
        'ser_ratio': ser_ratio,
        'red_count': red_counts[0],
        'red_ratio': red_ratio,
        'cate_list': cate_list,
        'check_list': check_list,
        'red_list': red_list
    }

    mh_problem = data0[data0['LEVEL'].isin(['A', 'B'])]

    # 需要匹配站段
    df = pd.merge(mh_problem, local_data.dept_data, left_on='RESP_STATION_ID', right_on='DEPARTMENT_ID')
    mh_gb = df.groupby('STATION').count().reset_index().sort_values('PK_ID', ascending=False)

    mid_desc = {
        'count': mh_problem.shape[0],
        'station_list': [
            {
                'station': row.STATION,
                'count': row.PK_ID
            } for row in mh_gb.itertuples()
        ]
    }
    return {
        'general': gen,
        'mid_desc': mid_desc
    }


def get_first(problem_data_this, problem_data_prev):
    data_arr = [problem_data_this, problem_data_prev]
    data_arr = [
        d[['PK_ID', 'LEVEL', 'IS_RED_LINE', 'CHECK_ITEM_NAME', 'PROBLEM_POINT', 'TYPE',
           'RESP_DEPT_NAME', 'DESCRIPTION', 'RESP_STATION_ID']].drop_duplicates('PK_ID')
        for d in data_arr
    ]

    check_type_list = ['车务-接发列车', '车务-调车', '车务-防溜', '劳']
    check_type_names = ['接发列车', '调车', '防溜', '劳安']
    idx = 0
    check_list = []
    for i in check_type_list:
        arr = [
            d[d['CHECK_ITEM_NAME'].str.contains(i)] for d in data_arr
        ]
        ratio = calc_ratio_ring(arr[0].shape[0], arr[1].shape[0])
        ratio['diff_text'] = get_inc_text(ratio['diff'], '个')
        ratio['ratio_text'] = get_percent_text(ratio['percent'])
        # TYPE 1, 2 路局检查， 3 站段自查
        d1 = arr[0]
        d1 = d1.groupby(['PROBLEM_POINT']).count().reset_index().sort_values('PK_ID', ascending=False)
        points = d1['PROBLEM_POINT'].head(10).tolist()
        point_desc = []
        for p in points:
            d2 = arr[0]
            dp = d2[d2['PROBLEM_POINT'] == p]
            point_desc.append({
                'point': p,
                'count': dp.shape[0],
                'grp_count': dp[dp['TYPE'].isin([1, 2])].shape[0],
                'st_count': dp[dp['TYPE'] == 3].shape[0]
            })

        dic = {
            'check_name': check_type_names[idx],
            'count': arr[0].shape[0],
            'ratio': ratio,
            'point_desc': point_desc
        }
        check_list.append(dic)
        idx = idx + 1

    return {
        'check_list': check_list
    }


def get_second(evaluate_data, eva_worker):
    # 干部类型(局管， 段管)
    leader_lv = ['正处级', '副处级', '正局级', '副局级']
    admin_grd = ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']
    evaluate_data1 = eva_worker.copy()
    total_count = len(eva_worker['PK_ID'].unique())
    # 逐条评价
    zt_list = []
    for i in [1, 2]:
        data = evaluate_data1[(evaluate_data1['EVALUATE_WAY'] == 1) & (evaluate_data1['EVALUATE_TYPE'] == i)]
        count = len(data)
        # 局领导
        ju_idt = len(data[(data['CHECK_DEPT_TYPE'] == 6) & (data['LEVEL'].isin(leader_lv))
                          & (data['CHECK_DEPT_NAME'].str.endswith('行政领导')
                             | data['CHECK_DEPT_NAME'].str.endswith('党群领导'))])
        # 段领导
        duan_idt = len(data[(data['CHECK_DEPT_TYPE'] == 7) & (data['LEVEL'].isin(leader_lv))
                            & (data['CHECK_DEPT_NAME'].str.endswith('行政领导')
                               | data['CHECK_DEPT_NAME'].str.endswith('党群领导'))])
        zt_list.append([count, ju_idt, duan_idt])
    # 定期评价
    data = evaluate_data1[evaluate_data1['EVALUATE_WAY'] == 2]
    dq_ju = data[(data['CHECK_DEPT_TYPE'] == 6) & (data['LEVEL'].isin(leader_lv))
                 & (data['CHECK_DEPT_NAME'].str.endswith('行政领导')
                    | data['CHECK_DEPT_NAME'].str.endswith('党群领导'))]
    dq_ju = len(dq_ju) + len(dq_ju[dq_ju['IS_REVIEW'] == 1])
    # 车务分析室评价
    evaluate_data = evaluate_data.copy()
    analysis_room = evaluate_data[evaluate_data['CHECK_DEPT_NAME'].str.contains('安全监察大队-车务分析室-运输组')]
    # 管理人员
    ad_count = len(analysis_room[analysis_room['GRADATION'].isin(admin_grd)])
    manager_level = []
    for i in admin_grd:
        data = analysis_room[analysis_room['GRADATION'] == i]
        dic = {
            'name': i,
            'count': len(data)
        }
        manager_level.append(dic)
    center_phase = {
        'admin_count': ad_count,
        'manager_level': manager_level
    }

    # 履职评价问题
    eval_gb = analysis_room.groupby(['SITUATION', 'GRADATION']).count().reset_index()
    st_list = eval_gb.groupby('SITUATION').sum().reset_index().sort_values('PK_ID', ascending=False)['SITUATION'].tolist()
    problem_list = []
    for st in st_list:
        ed = eval_gb[eval_gb['SITUATION'] == st]
        grades = [
            {
                'grade': row.GRADATION,
                'count': row.PK_ID
            } for row in ed.itertuples()
        ]
        desc = {
            'situation': st,
            'count': int(ed['PK_ID'].sum()),
            'grade_list': grades
        }
        problem_list.append(desc)

    # 分析中心作用发挥的问题
    center_problem = get_analysis_center_daily_list(local_data.date_params[0][0], local_data.date_params[0][1])

    return {
        'total_count': total_count,
        'zt_list': zt_list,
        'dq_ju': dq_ju,
        'center_eval': center_phase,
        'problem_list': problem_list,
        'analysis_center': center_problem
    }


def get_third(problem_data, last_problem_data):
    all_list = []
    data0 = problem_data.drop_duplicates('PK_ID')
    data1 = last_problem_data.drop_duplicates('PK_ID')
    problem_title = ['接发列车标准执行不严', '调车安全不容忽视', '防溜问题仍需引起重视', '劳动安全风险隐患突出',
                     '施工安全风险隐患较大', '应急（通）-行车应急', '接发列车（普）-非正常作业']
    check_item_list = ['车务-接发列车', '车务-调车', '车务-防溜', '劳',
                       '车务-施工', '应急（通）-行车应急', '接发列车（普）-非正常作业']
    idx = 0
    for i in check_item_list:
        data = data0[data0['CHECK_ITEM_NAME'].str.contains(i)]
        last_data = data1[data1['CHECK_ITEM_NAME'].str.contains(i)]
        # 典型问题（红线以及A类问题)
        problem = data[(data['IS_RED_LINE'] != 3) | (data['LEVEL'] == 'A')]
        problem_list = []
        pro_gb = problem.groupby('PROBLEM_POINT').count().reset_index().sort_values('PK_ID', ascending=False)

        for point in pro_gb['PROBLEM_POINT'].tolist():
            gp_prob = problem[(problem['PROBLEM_POINT'] == point) & (problem['TYPE'].isin([1, 2]))]
            gp_desc_list = gp_prob['DESCRIPTION'].tolist()
            st_prob = problem[(problem['PROBLEM_POINT'] == point) & (problem['TYPE'] == 3)]
            st_desc_list = st_prob['DESCRIPTION'].tolist()

            desc = {
                'problem_point': point,
                'gp_desc_list': gp_desc_list,
                'gp_prob_count': gp_prob.shape[0],
                'st_desc_list': st_desc_list,
                'st_prob_count': st_prob.shape[0],
            }
            problem_list.append(desc)

        dic = {
            'title': problem_title[idx],
            'check_item': i,
            'count': len(data),
            'ratio': calc_ratio_ring(len(data), len(last_data)),
            'problem_list': problem_list
        }
        all_list.append(dic)
        idx = idx + 1

    return {
        'all_list': all_list
    }


def get_fourth():
    warn_list = get_analysis_center_daily_list(local_data.date_params[0][0],
                                               local_data.date_params[0][1])
    result = {
        'warn_list': warn_list
    }
    return result


# --------------------- 第五章 start
def get_fifth():
    dps = local_data.date_params
    data0 = get_detail_check_problem_by_ac_raw(dps[0][0], dps[0][1])
    data1 = get_detail_check_problem_by_ac_raw(dps[1][0], dps[1][1])
    data_arr = [data0, data1]

    counts = [
        data.shape[0] for data in data_arr
    ]
    ratio = calc_ratio_ring(counts[0], counts[1])
    ratio['diff_text'] = get_inc_text(ratio['diff'], '个')
    ratio['ratio_text'] = get_percent_text(ratio['percent'])

    # 严重类问题
    problem_level = {'A', 'B', 'F1'}
    level_desc = []
    for level in problem_level:
        counts_lv = [
            data[data['LEVEL'] == level].shape[0] for data in data_arr
        ]
        ratio = calc_ratio_ring(counts_lv[0], counts_lv[1])
        ratio['diff_text'] = get_inc_text(ratio['diff'], '个')
        ratio['ratio_text'] = get_percent_text(ratio['percent'])
        level_desc.append({
            'level': level,
            'count': counts_lv[0],
            'ratio': ratio
        })

    content = {
        'total_count': counts[0],
        'ratio': ratio,
        'level_desc': level_desc
    }
    return content

# --------------------- 第五章 End
