import pandas as pd

from app.data.util import pd_query
from app.report.chewu.common_sql import ANALYSIS_CENTER_DAILY_SQL, CHECK_PROBLEM_DATA_SQL, CHECK_PROBLEM_DATA_AC_SQL
from app.report.util import get_detail_check_problem_data
from app import mongo

HIGH_PROBLEM_LEVEL = ['A', 'B', 'E1', 'E2', 'F1', 'F2']

MID_HIGH_PROBLEM_LEVEL = ['A', 'B', 'C', 'E1', 'E2', 'E3', 'F1', 'F2', 'F3']

PROBLEM_CATEGORY_LIST = [
    {'category': '作业类', 'level_list': ['A', 'B', 'C', 'D']},
    {'category': '管理类', 'level_list': ['F1', 'F2', 'F3', 'F4']},
    {'category': '设备类', 'level_list': ['E1', 'E2', 'E3', 'E4']},
    {'category': '外部环境类', 'level_list': ['G1', 'G2', 'G3']}
]


def calc_ratio_ring(sum1, sum2):
    if sum2 == 0:
        diff = sum1
        percent = round(sum1 * 100, 1)
        return {
            'diff': diff,
            'percent': percent
        }
    else:
        diff = sum1 - sum2
        percent = round(diff / sum2 * 100, 2)
        return {
            'diff': diff,
            'percent': percent
        }


def get_detail_check_problem(start_date_this, end_date_this,
                             start_date_prev=None, end_date_prev=None,
                             start_date_year=None, end_date_year=None):
    """
    从mongodb获取检查问题明细数据。PK_ID有重复
    :param start_date_this: {str} 本期开始日期，格式：'2019-12-30'
    :param end_date_this: {str} 本期结束日期，格式：'2019-12-30'
    :param start_date_prev: {str} 上一期开始日期，格式：'2019-12-30'
    :param end_date_prev: {str} 上一期结束日期，格式：'2019-12-30'
    :param start_date_year: {str} 上年同期期开始日期，格式：'2019-12-30'
    :param end_date_year: {str} 上年同期结束日期，格式：'2019-12-30'
    :return:
    """
    # 当期数据/上一期数据
    data0 = get_detail_check_problem_data(start_date_this, end_date_this, '车务')
    data1 = None
    if (start_date_prev is not None) and (end_date_prev is not None):
        data1 = get_detail_check_problem_data(start_date_prev, end_date_prev, '车务')

    data2 = None
    if (start_date_year is not None) and (end_date_year is not None):
        data2 = get_detail_check_problem_data(start_date_year, end_date_year, '车务')

    if data1 is None or data1.empty:
        data1 = pd.DataFrame(columns=data0.columns)
    if data2 is None or data2.empty:
        data2 = pd.DataFrame(columns=data0.columns)
    return [data0, data1, data2]


def get_analysis_center_daily_list(start_date, end_date):
    """
    读取安全分析中心日报
    :param start_date: {str} 本期开始日期，格式：'2019-12-30'
    :param end_date: {str} 本期结束日期，格式：'2019-12-30'
    :return:
    """
    data = pd_query(ANALYSIS_CENTER_DAILY_SQL.format(start_date, end_date))
    warn_list = []
    for it in data.itertuples():
        if len(str(it.RISK_WARNING)) > 0:
            warn_list.append({
                    'date': str(it.DATE),
                    'center_problem': it.CENTER_PROBLEM,
                    'risk_warning': it.RISK_WARNING
            })
    return warn_list


def get_detail_check_problem_raw(start_date, end_date):
    """
    从数据库获取检查问题明细数据
    :param start_date: {str} 本期开始日期，格式：'2019-12-30'
    :param end_date: {str} 本期结束日期，格式：'2019-12-30'
    :return: {dataframe}
    """
    data = pd_query(CHECK_PROBLEM_DATA_SQL.format(start_date, end_date))
    return data


def get_detail_check_problem_by_ac_raw(start_date, end_date):
    """
    从数据库获取检查问题明细数据
    :param start_date: {str} 本期开始日期，格式：'2019-12-30'
    :param end_date: {str} 本期结束日期，格式：'2019-12-30'
    :return: {dataframe}
    """
    data = pd_query(CHECK_PROBLEM_DATA_AC_SQL.format(start_date, end_date))
    return data


def get_evaluate_score_and_punish_table(eval_this, eval_year):
    """
    计算干部累计记分及处理表的数据
    :param eval_this: 本期的数据
    :param eval_year: 本年的累计数据
    :return:
    """
    return {
        'this_list': get_punish_list(eval_this),
        'year_list': get_punish_list(eval_year)
    }


def get_punish_list(eval_data):
    """
    处罚的人员清单
    :param eval_data:
    :return:
    """
    data = eval_data.groupby('RESPONSIBE_ID_CARD').sum().reset_index().sort_values('SCORE', ascending=False)
    count_list = [int(data['RESPONSIBE_ID_CARD'].shape[0])]
    down = 0
    up = 2
    while down < 12:
        ct = data[(data['SCORE'] >= down) & (data['SCORE'] < up)].shape[0]
        count_list.append(int(ct))
        down = up
        up = up + 2
    ct = data[data['SCORE'] >= 12].shape[0]
    count_list.append(int(ct))
    return count_list


def get_evaluate_rank_data(key, dup_subset=['ID_CARD']):
    """
    根据指定的检索key，检索记录。并根据dup_subset自定的列，进行数据除重操作
    :param key: {dict} 检索的key
    :param dup_subset: {list} 默认是['ID_CARD']
    :return:
    """
    prefixs = ['daily_', 'monthly_', 'history_']
    for prefix in prefixs:
        coll_name = f'{prefix}detail_evaluate_index'
        doc = list(mongo.db[coll_name].find(key, {"_id": 0}))
        if doc:
            break
    data = pd.DataFrame(doc)
    if data.empty:
        return pd.DataFrame()
    else:
        data = data.drop_duplicates(dup_subset)
        short_name_data = pd_query('''SELECT
                a.ID_CARD,
                c.SHORT_NAME
            FROM
                t_person AS a
                LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                LEFT JOIN t_department as c on c.DEPARTMENT_ID = b.TYPE3
            WHERE
                c.SHORT_NAME IS NOT NULL
                AND c.SHORT_NAME != ''
        ''')
        data = pd.merge(data, short_name_data, how='left', on='ID_CARD')
        data = data[~data['SHORT_NAME'].isnull()]
    return data