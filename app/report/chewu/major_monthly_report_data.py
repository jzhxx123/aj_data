from datetime import datetime
import threading
from app import mongo
from app.data.util import pd_query
from app.report.chewu.common import get_evaluate_score_and_punish_table, get_evaluate_rank_data, MID_HIGH_PROBLEM_LEVEL
from app.report.chewu.common_sql import (QUANTIFY_ASSESS_DATA_SQL, QUANTIFY_DETAIL_ASSESS_UNQ_PERSON_SQL,
    MAJOR_PERSON_DATA_SQL, CHECK_EVALUATE_INFO_SQL, STATION_LIST_SQL, CHECK_INFO_AND_PROBLEM_SQL,
    TRANSPORT_ADMIN_DEPT_ID, CHAOQI_PROBLEM_SQL, ASSESS_MONEY_SQL, RETURN_MONEY_SQL, STATION_SAFETY_KESHI_SQL,
    analysis_qt_sql, EVALATE_WORKER_SQL, CHECK_INFO_MEDIA_SQL, OVERDUE_PROBLEM_NUMBER_SQL,
    ASSESS_RESPONSIBLE_SQL, SYSTEM_VIOLATION_DISTRIBUTION_SQL, CADRE_CUMULATIVE_SQL, TRAIN_ANALYSIS_SQL, CHECK_INFO_SQL,
                                         CHECK_PROBLEM_SQL)
import pandas as pd
from flask import current_app
from dateutil.relativedelta import relativedelta
import app.report.analysis_report_manager as manager
from app.report.chewu.major_monthly_report_sql import *


local_data = threading.local()
NORIDE_JOB_PERSON_DPNAME = [
    '客', '货', '装', '公司',
    '营销', '客运段', '行包', '售票',
    '收入', '基金', '乘务车间'
]


def calc_ratio(num1, num2):
    if num2 == 0:
        return {
            'diff': num1,
            'percent': num1 * 100
        }
    else:
        return {
            'diff': num1 - num2,
            'percent': round((num1 - num2) / num2 * 100, 1)
        }


def calc_rate(num1, num2):
    if num2 == 0:
        return num1
    else:
        return round(float(num1 / num2), 2)


def calc_score(list1):
    count = [0, 0, 0, 0, 0, 0, 0]
    for i in list1:
        if 2 < i:
            count[0] += 1
        elif i < 4:
            count[1] += 1
        elif i < 6:
            count[2] += 1
        elif i < 8:
            count[3] += 1
        elif i < 10:
            count[4] += 1
        elif i < 12:
            count[5] += 1
        else:
            count[6] += 1
    return count


def _get_evaluate_report_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间
    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'detail_evaluate_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '车务'
        }, {
            "_id": 0,
            "BUSINESS_CLASSIFY": 1,
            "ID_CARD": 1,
            "STATION": 1,
            "score": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def _get_health_index(month, station_name):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '车务',
            'DEPARTMENT_NAME': station_name
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def get_data(year, month):
    date_params = manager.MonthlyAnalysisReport.get_month_intervals(year, month)
    local_data.date_params = date_params
    local_data.year = year
    local_data.month = month
    # 加载所有SQL数据
    _load_general_data()

    start_date = local_data.start_date
    end_date = local_data.end_date
    start_month = start_date.month
    end_month = end_date.month
    start_day = start_date.day
    end_day = end_date.day

    first = get_first()
    second = _get_second()
    third = get_third()
    four = get_four()
    five = get_five()

    file_name = f'{year}年{month}月车务系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '车务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.now(),
        'first': first,
        'second': second,
        'third': third,
        'four': four,
        'five': five
    }
    return result


def _load_general_data(stage=99):
    """
    加载在整个计算过程中使用的通用的数据
    :return:
    """
    date_params = local_data.date_params
    start_date = datetime.strptime(date_params[0][0], '%Y-%m-%d')
    end_date = datetime.strptime(date_params[0][1], '%Y-%m-%d')
    start_date_prv = datetime.strptime(date_params[1][0], '%Y-%m-%d')
    end_date_prv = datetime.strptime(date_params[1][1], '%Y-%m-%d')
    start_date_year = datetime.strptime(date_params[2][0], '%Y-%m-%d')
    end_date_year = datetime.strptime(date_params[2][1], '%Y-%m-%d')
    # 3个月以前的年月
    if int(local_data.month) > 3:
        three_month_ago = local_data.month - 3
        three_month_ago_year = local_data.year
    else:
        three_month_ago_year = local_data.year - 1
        three_month_ago = local_data.month - 3 + 12
    local_data.three_month_ago_year, local_data.three_month_ago = three_month_ago_year, three_month_ago
    local_data.major = '车务'
    local_data.start_date = start_date
    local_data.end_date = end_date
    local_data.start_date_prv = start_date_prv
    local_data.end_date_prv = end_date_prv
    local_data.start_date_year = start_date_year
    local_data.end_date_year = end_date_year
    year_month = local_data.year * 100 + local_data.month
    if datetime.strptime(date_params[0][1], '%Y-%m-%d') >= datetime.strptime(date_params[0][1][:4] + '-12-25', '%Y-%m-%d'):
        local_data.year_start_str = end_date[:4] + '1225'
    else:
        local_data.year_start_str = str(int(date_params[0][1].split('-')[0]) - 1) + '-12-25'
    local_data.year_start_str = datetime.strptime(local_data.year_start_str, '%Y-%m-%d')
    # df_dept = pd_query(MAJOR_DEPARTMENT_DATA_SQL)
    # local_data.dept_data = df_dept
    station_list = pd_query(STATION_LIST_SQL)
    local_data.station_list = station_list

    # 站段人员
    person = pd_query(MAJOR_PERSON_DATA_SQL)
    local_data.person = person

    # 检查信息
    local_data.check_info_data = pd_query(CHECK_INFO_SQL.format(local_data.start_date.date(), local_data.end_date.date())).drop_duplicates(subset='PK_ID')
    local_data.check_info_last_month_data = pd_query(CHECK_INFO_SQL.format(local_data.start_date_prv.date(), local_data.end_date_prv.date())).drop_duplicates(subset='PK_ID')

    # 检查问题
    local_data.check_problem = pd_query(CHECK_PROBLEM_SQL.format(local_data.start_date.date(),local_data.end_date.date())).drop_duplicates(subset='PK_ID')

    # 干部累计记分
    local_data.cadre_cumulative_data = pd_query(CADRE_CUMULATIVE_SQL.format(local_data.start_date.date(), local_data.end_date.date()))
    local_data.year_cadre_cumulative_data = pd_query(CADRE_CUMULATIVE_SQL.format(local_data.year_start_str.date(), local_data.end_date.date()))

    # 接发/调车信息
    local_data.train_analysis_data = pd_query(TRAIN_ANALYSIS_SQL.format(local_data.start_date, local_data.end_date))
    local_data.train_analysis_last_month_data = pd_query(
        TRAIN_ANALYSIS_SQL.format(local_data.start_date_prv, local_data.end_date_prv))

    # 站段分析中心ID
    center_data = pd_query(STATION_SAFETY_KESHI_SQL)
    local_data.center = center_data

    # 分析中心量化质量
    qt0 = pd_query(analysis_qt_sql.format(local_data.year, local_data.month))
    local_data.qt_center = qt0

    # 计算量化人数, 增加COMPLETION 字段以便判断是否完成
    qt_data = pd_query(QUANTIFY_ASSESS_DATA_SQL.format(local_data.year, local_data.month))
    qt_data.fillna(0, inplace=True)
    # 量化的完成情况
    qt_data['COMPLETION'] = 1
    unq_data = pd_query(QUANTIFY_DETAIL_ASSESS_UNQ_PERSON_SQL.format(local_data.year, local_data.month))
    uq_set = set(unq_data['ID_CARD'].tolist())
    qt_data['COMPLETION'] = qt_data.apply(lambda row: mark_complete(row, uq_set), axis=1)
    local_data.qt_data = qt_data

    # 上一个月的量化完成情况
    qt_data1 = pd_query(QUANTIFY_ASSESS_DATA_SQL.format(end_date_prv.year, end_date_prv.month))
    qt_data1.fillna(0, inplace=True)
    local_data.qt_data_prev = qt_data1

    # -------------- 履职评价信息
    eval_info0 = pd_query(CHECK_EVALUATE_INFO_SQL.format(date_params[0][0], date_params[0][1]))
    eval_info0['SITUATION'] = eval_info0['SITUATION'].str.replace('\n', '').str.replace('。', '')
    local_data.evaluate_info_this = eval_info0


    # year_start_str = f'{local_data.year}-01-01'
    # 年度数据
    eval_info4 = pd_query(CHECK_EVALUATE_INFO_SQL.format(str(local_data.year_start_str)[:10], date_params[0][1]))
    eval_info4['SITUATION'] = eval_info4['SITUATION'].str.replace('\n', '').str.replace('。', '')
    local_data.evaluate_info_year_total = eval_info4

    eval_rank0 = get_evaluate_rank_data({'MON': year_month, 'MAJOR': local_data.major})
    local_data.eval_rank_this = eval_rank0

    local_data.evaluate_workload = pd_query(EVALATE_WORKER_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.evaluate_workload = local_data.evaluate_workload.drop_duplicates(subset=['PK_ID']).reset_index()

    # 检查与问题信息数据(PK_ID_CK, PK_ID_CP)按需要除重复
    cb_data0 = pd_query(CHECK_INFO_AND_PROBLEM_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.check_problem_data_this = cb_data0
    cb_data1 = pd_query(CHECK_INFO_AND_PROBLEM_SQL.format(date_params[1][0], date_params[1][1]))
    local_data.check_problem_data_prev = cb_data1

    # 超期整改(年，月）
    cq_data0 = pd_query(OVERDUE_PROBLEM_NUMBER_SQL.format(local_data.year, local_data.month))
    local_data.chaoqi_data = cq_data0

    # 考核金额
    kh_data0 = pd_query(ASSESS_RESPONSIBLE_SQL.format(local_data.year, local_data.month))
    local_data.assess_money = kh_data0.drop_duplicates(subset=['PK_ID']).reset_index()
    # 3个月前的考核
    local_data.assess_money_third_prv = pd_query(ASSESS_RESPONSIBLE_SQL.format(three_month_ago_year, three_month_ago))

    # 返奖金额（当前月返的是3个月前的金额，例如12，返的是9月）
    return0 = pd_query(RETURN_MONEY_SQL.format(local_data.year, local_data.month))
    local_data.return_money = return0

    # 检查媒体信息
    local_data.check_media_data = pd_query(CHECK_INFO_MEDIA_SQL.format(date_params[0][0], date_params[0][1]))
    local_data.check_media_data = local_data.check_media_data.drop_duplicates(subset=['PK_ID', 'ID_CARD']).reset_index()

    # 重点人员库
    local_data.key_person_data = pd_query(SYSTEM_VIOLATION_DISTRIBUTION_SQL.format(local_data.year, local_data.month))
    local_data.key_person_data = local_data.key_person_data.drop_duplicates(subset=['ID_CARD']).reset_index()


# ------------------------------------ 第一章
def get_first():
    # （一）评价整体情况分析
    entire_state = _get_evaluate_entire_state()
    # (二） 站段履职评价分析
    station_state = _get_evaluate_station_state()
    # （三）各级干部履职分析
    cadre_evaluate = _cadre_evaluate_analysis()
    cadre_evaluate_score_table = get_cadre_evaluate_score_table()

    return {
        'entire_state': entire_state,
        'cadre_evaluate': cadre_evaluate,
        'station_evaluate': station_state,
        'cadre_evaluate_score_table': cadre_evaluate_score_table
    }


def get_cadre_evaluate_score_table():
    cadre_evaluate_score_data = local_data.cadre_cumulative_data
    year_cadre_cumulative_data = local_data.year_cadre_cumulative_data
    current_zhengke_level_table_data = cadre_evaluate_score_data[cadre_evaluate_score_data['GRADATION'].str.contains('正科职管理人员')].drop_duplicates(subset='RESPONSIBE_ID_CARD')
    current_count = len(current_zhengke_level_table_data)
    current_lt_2 = len(current_zhengke_level_table_data[current_zhengke_level_table_data['SCORE'] < 2])
    current_gtn_2_lt_4 = len(current_zhengke_level_table_data[(current_zhengke_level_table_data['SCORE'] >= 2)
                                                      & (current_zhengke_level_table_data['SCORE'] < 4)])
    current_gtn_4_lt_6 = len(current_zhengke_level_table_data[(current_zhengke_level_table_data['SCORE'] >= 4)
                                                      & (current_zhengke_level_table_data['SCORE'] < 6)])
    current_gtn_6_lt_8 = len(current_zhengke_level_table_data[(current_zhengke_level_table_data['SCORE'] >= 6)
                                                      & (current_zhengke_level_table_data['SCORE'] < 8)])
    current_gtn_8_lt_10 = len(current_zhengke_level_table_data[(current_zhengke_level_table_data['SCORE'] >= 8)
                                                       & (current_zhengke_level_table_data['SCORE'] < 10)])
    current_gtn_10_lt_12 = len(current_zhengke_level_table_data[(current_zhengke_level_table_data['SCORE'] >= 10)
                                                        & (current_zhengke_level_table_data['SCORE'] < 12)])
    current_gtn_12 = len(current_zhengke_level_table_data[current_zhengke_level_table_data['SCORE'] >= 12])

    current = {
        'count': current_count,
        'lt_2': current_lt_2,
        'gtn_2_lt_4': current_gtn_2_lt_4,
        'gtn_4_lt_6': current_gtn_4_lt_6,
        'gtn_6_lt_8': current_gtn_6_lt_8,
        'gtn_8_lt_10': current_gtn_8_lt_10,
        'gtn_10_lt_12': current_gtn_10_lt_12,
        'gtn_12': current_gtn_12
    }

    year_zhengke_level_table_data = year_cadre_cumulative_data[
        year_cadre_cumulative_data['GRADATION'].str.contains('正科职管理人员')].drop_duplicates(subset='RESPONSIBE_ID_CARD')
    year_count = len(year_zhengke_level_table_data)
    year_lt_2 = len(year_zhengke_level_table_data[year_zhengke_level_table_data['SCORE'] < 2])
    year_gtn_2_lt_4 = len(year_zhengke_level_table_data[(year_zhengke_level_table_data['SCORE'] >= 2)
                                                              & (year_zhengke_level_table_data['SCORE'] < 4)])
    year_gtn_4_lt_6 = len(year_zhengke_level_table_data[(year_zhengke_level_table_data['SCORE'] >= 4)
                                                              & (year_zhengke_level_table_data['SCORE'] < 6)])
    year_gtn_6_lt_8 = len(year_zhengke_level_table_data[(year_zhengke_level_table_data['SCORE'] >= 6)
                                                              & (year_zhengke_level_table_data['SCORE'] < 8)])
    year_gtn_8_lt_10 = len(year_zhengke_level_table_data[(year_zhengke_level_table_data['SCORE'] >= 8)
                                                               & (year_zhengke_level_table_data['SCORE'] < 10)])
    year_gtn_10_lt_12 = len(year_zhengke_level_table_data[(year_zhengke_level_table_data['SCORE'] >= 10)
                                                                & (current_zhengke_level_table_data['SCORE'] < 12)])
    year_gtn_12 = len(year_zhengke_level_table_data[year_zhengke_level_table_data['SCORE'] >= 12])

    year = {
        'count': year_count,
        'lt_2': year_lt_2,
        'gtn_2_lt_4': year_gtn_2_lt_4,
        'gtn_4_lt_6': year_gtn_4_lt_6,
        'gtn_6_lt_8': year_gtn_6_lt_8,
        'gtn_8_lt_10': year_gtn_8_lt_10,
        'gtn_10_lt_12': year_gtn_10_lt_12,
        'gtn_12': year_gtn_12
    }

    res = {
        'current': current,
        'year': year
    }
    return res


def _get_evaluate_entire_state():
    # 量化任务描述
    qt_state = _get_quantify_access_total(local_data.qt_data)

    eval_info = local_data.evaluate_info_this.drop_duplicates(subset=['PK_ID'], keep='first').reset_index()

    # 两级分析中心评价信息
    # ct_eval_info = eval_info[eval_info['BUSINESS_CLASSIFY'].str.contains('分析中心')]
    ct_eval_info = local_data.evaluate_workload
    center_state = _center_evaluate_state(ct_eval_info, eval_info)
    # 履职存在问题
    problem_desc = _evaluate_problem_type_desc(eval_info)

    eval_person_year = local_data.evaluate_info_year_total.drop_duplicates(subset=['PK_ID'], keep='first').reset_index()
    evaluate_score = _evaluate_person_score(eval_person_year, eval_info, local_data.person)
    content = {
        'qt_state': qt_state,
        'center_state': center_state,
        'problem_desc': problem_desc,
        'evaluate_score': evaluate_score
    }
    return content


def _get_evaluate_station_state():
    from app.report.chewu.common import get_punish_list
    # 站段履职评价分析
    eval_person_year = local_data.evaluate_info_year_total.drop_duplicates(subset=['PK_ID'], keep='first').reset_index()
    eval_person_year = eval_person_year[
        eval_person_year.apply(lambda row: _filter_data_bystr(
            row, 'RESP_DEPT_NAME', NORIDE_JOB_PERSON_DPNAME, reseve=True), axis=1)
    ]
    eval_person_year = eval_person_year[
        eval_person_year['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])
    ]
    # 当月
    eval_person_month = eval_person_year[
        eval_person_year.apply(lambda row: _filter_data_bytime(
            row, [local_data.start_date, local_data.end_date], 'CREATE_TIME'), axis=1)
    ]
    # 前一个月
    eval_person_prv = eval_person_year[
        eval_person_year.apply(lambda row: _filter_data_bytime(
            row, [local_data.start_date_prv, local_data.end_date_prv], 'CREATE_TIME'), axis=1)
    ]
    # 各站段
    station_dp_names = local_data.person[['STATION', 'STATION_ID']].drop_duplicates(subset=['STATION_ID'])
    desc_list = ['总数', '0-2分', '2-4分', '4-6分', '6-8分', '8-10分', '10-12分', '12分以上']
    rst = []
    for _, row in station_dp_names.iterrows():
        # 获取每个站段前后两个月的数据 RESP_STATION_ID
        # 返回list前后顺序（总数、0-2，2-4，4-6，6-8，8-10，10-12，12以上
        this_list = get_punish_list(
            eval_person_month[
                eval_person_month['RESP_STATION_ID'] == row['STATION_ID']
            ]
        )
        prv_list = get_punish_list(
            eval_person_prv[
                eval_person_prv['RESP_STATION_ID'] == row['STATION_ID']
            ]
        )
        rst_list = [this_list[i] - prv_list[i] for i in range(len(prv_list))]
        rst_list = [i for i in range(len(prv_list)) if rst_list[i] > 0 and i !=1]
        desc = f"上月{','.join([desc_list[i] + str(prv_list[i])+'人' for i in rst_list])},"
        desc += f"本月{','.join([desc_list[i] + str(this_list[i])+'人' for i in rst_list])}。"
        rst.append(
            {
                'station_name': row['STATION'],
                'value': desc+'\n'
            }
        )
    return rst

# 量化人员的完成情况及描述 对应 一、（一）评价整体情况分析
def _get_quantify_access_total(qt_data):
    count = qt_data.shape[0]
    cpl_count = qt_data[qt_data['COMPLETION'] == 1].shape[0]
    cpl_rate = round(100 * cpl_count / count, 2)
    unc_data = qt_data[qt_data['COMPLETION'] == 0]
    un_persons = [
        {
            'station': str(row.ALL_NAME).split('-')[0],
            'job': row.JOB,
            'person_name': row.PERSON_NAME
        } for row in unc_data.itertuples()
    ]

    result = {
        'count': count,
        'cpl_count': cpl_count,
        'cpl_rate': cpl_rate,
        'un_persons': un_persons
    }
    return result


def mark_complete(row, unq_id_set):
    if row['CHECK_TIMES_TOTAL'] > row['REALITY_NUMBER']:
        return 0
    if row['CHECK_NOTIFICATION_TIMES_TOTAL'] > row['CHECK_NOTIFICATION_REALITY_NUMBER']:
        return 0
    if row['MONITOR_NUMBER_TOTAL'] > row['MEDIA_REALITY_TIME']:
        return 0
    if row['PROBLEM_NUMBER_TOTAL'] > row['REALITY_PROBLEM_NUMBER']:
        return 0
    if row['WORK_PROBLEM_NUMBER_TOTAL'] > row['REALITY_WORK_ITEM_PROBLEM_NUMBER']:
        return 0
    if row['MONITOR_PROBLEM_NUMBER_TOTAL'] > row['MEDIA_REALITY_PROBLEM_NUMBER']:
        return 0
    if row['HIDDEN_DANGER_RECHECK_TIMES_TOTAL'] > row['HIDDEN_DANGER_RECHECK_REALITY_NUMBER']:
        return 0
    if row['RISK_RECHECK_TIMES_TOTAL'] > row['RISK_RECHECK_REALITY_NUMBER']:
        return 0
    if row['IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL'] > row['IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER']:
        return 0
    if row['MIN_QUALITY_GRADES_TOTAL'] > row['REALITY_MIN_QUALITY_GRADES']:
        return 0
    if row['MANAGE_PROBLEM_NUMBER_TOTAL'] > row['REALITY_MANAGE_PROBLEM_NUMBER']:
        return 0
    if row['DEVICE_PROBLEM_NUMBER_TOTAL'] > row['REALITY_DEVICE_PROBLEM_NUMBER']:
        return 0
    if row['ENVIRONMENT_PROBLEM_NUMBER_TOTAL'] > row['REALITY_ENVIRONMENT_PROBLEM_NUMBER']:
        return 0
    if row['OUT_WORK_PROBLEM_NUMBER_TOTAL'] > row['REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER']:
        return 0
    if row['ID_CARD'] in unq_id_set:
        return 0
    return 1


def _center_evaluate_state(ct_eval_info, evaluate_info):
    """
    两级分析中心的评价情况
    :param evaluate_info:
    :return:
    """
    # 逐条评价(检查信息跟问题)
    # zt_data = evaluate_info[evaluate_info['EVALUATE_WAY'] == 1]
    zt_data = ct_eval_info[
              (ct_eval_info['EVALUATE_TYPE'].isin([1, 2])) &
              (ct_eval_info['EVALUATE_WAY'] == 1)
    ]
    # 逐条评价(检查信息跟问题)（复查）
    fc_zt_data = ct_eval_info[
              (ct_eval_info['EVALUATE_TYPE'].isin([1, 2])) &
              (ct_eval_info['EVALUATE_WAY'] == 1) &
              (ct_eval_info['IS_REVIEW'] == 1)
    ]
    all_zt_data = zt_data.append(fc_zt_data).reset_index()
    # 行车管理人员的评价
    ad_data = all_zt_data[
              all_zt_data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])
    ]
    ad_data = ad_data[
        ad_data.apply(lambda row: _filter_data_bystr(
            row, 'CHECK_DEPT_NAME', NORIDE_JOB_PERSON_DPNAME), axis=1)]
    # 统计分（检查信息）
    ad_check_info_ids = ad_data[
        ad_data['EVALUATE_TYPE'].isin([1])
    ]['FK_CHECK_OR_PROBLEM_ID'].values.tolist()
    # 统计分（检查问题）
    ad_check_problem_ids = ad_data[
        ad_data['EVALUATE_TYPE'].isin([2])
    ]['FK_CHECK_OR_PROBLEM_ID'].values.tolist()
    # 上述问题的实例评价信息
    ad_evaluate_info = evaluate_info[
                            (
                                (evaluate_info['EVALUATE_TYPE'] == 1) &
                                (evaluate_info['FK_CHECK_OR_PROBLEM_ID'].isin(ad_check_info_ids))
                            ) |
                            (
                                (evaluate_info['EVALUATE_TYPE'] == 2) &
                                (evaluate_info['FK_CHECK_OR_PROBLEM_ID'].isin(ad_check_problem_ids))
                            )
    ]
    ad_evaluate_info = ad_evaluate_info[
        ad_evaluate_info['EVALUATE_WAY'] == 1
    ]
    # 路局检查发现的问题
    grp_data = ad_data[ad_data['CHECK_TYPE'] == 1]

    # 路局评价的问题
    # grp_eva = zt_data[zt_data['CHECK_DEPT_TYPE'] == 6]

    dic = {
        'eval_count': len(zt_data) + len(fc_zt_data),
        'ad_problem_count': len(ad_data),  # 行车管理人员履职问题数
        'ad_person_count': len(set(ad_data['RESPONSIBE_ID_CARD'])),  # 行车管理人员履职人数
        'grp_check_count': grp_data.shape[0],  # 集团检查发现问题个数
        'st_check_count': len(ad_data[ad_data['CHECK_TYPE'] == 2]),  # 站段检查发现问题个数
        # 'manager_count': len(zt_data[zt_data['IDENTITY'] == '干部']),  # 涉及干部x人
        'manager_count': len(set(ad_data['RESPONSIBE_ID_CARD'])),
        'total_score': int(ad_evaluate_info['SCORE'].sum()),  # 合计记分
        # 'grp_eval_count': len(set(grp_eva['RESPONSIBE_ID_CARD'])),  # 路局评价总人数
        'grp_eval_count': len(set(grp_data['RESPONSIBE_ID_CARD'])),
        # 'grp_eval_score': int(grp_eva['SCORE'].sum()),  # 路局评价总扣分
        'grp_eval_score': int(ad_evaluate_info[
                                  ad_evaluate_info['CHECK_TYPE'] ==1]['SCORE'].sum())
        # 'count': count
    }
    return dic


def _filter_data_bytime(row, times, time_col):
    # 缺失值 直接返回
    # times[0] -- start -- type:datetime
    # time[1] -- end

    if not pd.notnull(row[time_col]):
        return False
    time_date = str(row[time_col])[:10]
    if times[0] <= datetime.strptime(time_date,'%Y-%m-%d') <= times[1]:
        return True
    return False


def _filter_data_bystr(row, str_col, str_contents, reseve=False):
    """
    Args:
        row:
        str_col:
        str_contents:
        reseve: 表示反选

    Returns:

    """
    flag = not reseve
    if not pd.notnull(row[str_col]):
        return False
    for item in str_contents:
        if item in row[str_col]:
            return flag
    return not flag


def _evaluate_person_score(eva_year, eva_month, person_data):
    # 当月干部
    ride_person_gb = person_data[
        person_data['IDENTITY'] == '干部'
    ]['ID_CARD']
    # 行车干部评价记分信息
    eva_year_info = eva_year[
        (eva_year['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])) &
        (eva_year['RESPONSIBE_ID_CARD'].isin(list(ride_person_gb)))
    ]
    eva_year_info = eva_year_info[
        eva_year_info.apply(lambda row: _filter_data_bystr(
            row, 'RESP_DEPT_NAME', NORIDE_JOB_PERSON_DPNAME, reseve=True), axis=1)]

    eva_month_info = eva_month[
        (eva_month['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])) &
        (eva_month['RESPONSIBE_ID_CARD'].isin(list(ride_person_gb)))
        ]
    eva_month_info = eva_month_info[
        eva_month_info.apply(lambda row: _filter_data_bystr(
            row, 'RESP_DEPT_NAME', NORIDE_JOB_PERSON_DPNAME, reseve=True), axis=1)]

    # 总人数
    count1 = len(set(eva_year_info['RESPONSIBE_ID_CARD']))
    this_month_resp_idcards = set(eva_month_info['RESPONSIBE_ID_CARD'])
    # 起始月到上个月的数据
    last_month_resp_idcards = eva_year_info[eva_year_info.apply(
        lambda row: _filter_data_bytime(
            row, [local_data.year_start_str, local_data.end_date_prv],
            'CREATE_TIME'), axis=1)]
    last_month_resp_idcards = set(last_month_resp_idcards['RESPONSIBE_ID_CARD'])
    if int(local_data.month) == 1:
        add_count = len(this_month_resp_idcards)
    else:
        add_count = count1 - len(last_month_resp_idcards)
    # last_month = local_data.month - 1
    # 计算计分最高(当月次)
    max_data = eva_month_info.groupby(['RESPONSIBE_ID_CARD'])['SCORE'].sum().reset_index()
    max_data = max_data[
        max_data['SCORE'] == max(max_data['SCORE'])
    ]
    # max_data = eva_month.sort_values(by='SCORE', ascending=False).reset_index().iloc[0]
    max_score = float(max(max_data['SCORE']))
    # name1 = max_data['RESP_STATION'] + max_data['JOB'] + max_data['RESPONSIBE_PERSON_NAME']
    name1 = [
        row['RESP_STATION'] + row['JOB'] + row['RESPONSIBE_PERSON_NAME']
        for _,row in eva_month_info[
            eva_month_info['RESPONSIBE_ID_CARD'].isin(
                list(max_data['RESPONSIBE_ID_CARD']))].iterrows()
    ]
    name1 = '、'.join(list(set(name1)))
    # 累计最高分(判断该干部当月是否为干部)
    max_data = eva_year.groupby(['RESPONSIBE_ID_CARD'])['SCORE'].sum().reset_index()
    max_data = max_data[
        max_data['SCORE'] == max(max_data['SCORE'])
        ]
    # eva_gb = eva_year.groupby('RESPONSIBE_ID_CARD').sum().reset_index().sort_values(by='SCORE', ascending=False)
    # total_max = eva_gb.iloc[0]
    total_score = float(max(max_data['SCORE']))
    # max_idcard = total_max['RESPONSIBE_ID_CARD']

    # name_data = eva_year[eva_year['RESPONSIBE_ID_CARD'] == max_idcard].iloc[0]
    # name2 = name_data['RESP_STATION'] + name_data['JOB'] + name_data['RESPONSIBE_PERSON_NAME']
    name2 = [
        row['RESP_STATION'] + row['JOB'] + row['RESPONSIBE_PERSON_NAME']
        for _, row in eva_year_info[
            eva_year_info['RESPONSIBE_ID_CARD'].isin(
                list(max_data['RESPONSIBE_ID_CARD']))].iterrows()
    ]
    name2 = '、'.join(list(set(name2)))

    return {
        'count1': count1,
        'add_count': add_count,
        'max_score': max_score,
        'name1': name1,
        'total_score': total_score,
        'name2': name2,
        'punish_table': get_evaluate_score_and_punish_table(eva_month_info, eva_year_info)
    }


def _evaluate_problem_type_desc(evaluate_info):
    all_list = []
    # 各个履职问题数据
    index = 0
    person_limit = {
        'ZD': 5,
        'ZL': 5,
        'JL': 4,
        'ZG': 3,
        'ZS': 3,
        'KH': 3,
        'YY': 3,
        'ZJ': 3
    }

    titles = ['一是重点工作落实不到位', '二是监督检查质量低下', '三是检查信息录入质量不高', '四是问题整改督促不力',
              '五是安全过程控制不到位', '六是查处问题考核不严', '七是音视频运用管理问题突出', '八是弄虚作假时有发生']
    descriptions = [
        '部分管理人员对上级布置重点工作不认真思考、不主动抓落实、不检查督促，重点工作推进迟缓、贯彻落实不力',
        '下现场检查力度、深度不够，安全检查不认真、查处问题质量不高的问题突出',
        '部分管理人员系统运用不重视，安全问题、信息录入明显错误，录入与安全无关的问题，拆分检查信息录入的问题突出',
        '部分管理人员对问题整改不重视，问题整改敷衍了事、整改督促不力、销号把关不严，问题整改不彻底、安全问题反复发生的问题突出',
        '部分管理干部安全意识松懈、安全管理不严、专业管理不力，安全过程控制不到位，现场作业失管失控',
        '部分管理干部日常安全考核不严格、存有好人主义，查处安全问题该考核不考核、降低标准考核问题仍然较多',
        '部分管理人员对音视频运用管理不重视，音视频设备管理不到位、调阅分析开展不力的问题突出',
        ''
    ]
    reasons = [
        '重点工作落实',
        '检查质量不高',
        '检查信息录入质量不高',
        '问题闭环管理',
        '因事故、故障信息追溯管理人员负直接、间接管理责任',
        '不严格考核',
        '音视频设备管理',
        '本月查处弄虚作假'
    ]
    for i in ['重点工作落实', '监督检查质量', '检查信息录入', '问题闭环管理', '事故故障追溯',
              '考核责任落实', '音视频运用管理', '弄虚作假']:
        data = evaluate_info[evaluate_info['ITEM_NAME'] == i]
        if data.shape[0] == 0:
            continue
        detail = []
        # 每个问题的SITUATION分类
        for j in data['SITUATION'].value_counts().index:
            new_data = data[data['SITUATION'] == j]
            dic = {
                'name': j.strip(),
                'code': new_data['CODE'].tolist()[0],
                'count': len(new_data),
                'person': len(new_data['RESPONSIBE_ID_CARD'].value_counts())
            }
            detail.append(dic)
        count = data.shape[0]
        person_count = data['RESPONSIBE_ID_CARD'].value_counts().shape[0]
        # 人员描述的数据
        data = data.sort_values(by='SCORE', ascending=False)
        code_pref = data['CODE'].tolist()[0].split('-')[0]
        limit = 0 if person_limit.get(code_pref) is None else person_limit.get(code_pref)
        person_list = [
            {
                'station': it.RESP_STATION,
                'check_type': '自查' if it.CHECK_TYPE == 2 else '被路局抽查',
                'resp_dept': it.RESP_DEPT_NAME,
                'job': it.JOB,
                'person_name': it.RESPONSIBE_PERSON_NAME,
                'content': it.EVALUATE_CONTENT
            } for it in data.head(limit).itertuples()
        ]

        dic = {
            'title': titles[index],
            'desc': descriptions[index],
            'reason': reasons[index],
            'pro_count': count,
            'per_count': person_count,
            'detail': sorted(detail, key=lambda x: x['count'], reverse=True),
            'person_list': person_list
        }
        all_list.append(dic)

        index += 1
    return {
        'problem_list': all_list
    }


# （三）各级干部履职分析
def _cadre_evaluate_analysis():
    eval_info = local_data.evaluate_info_this
    eval_rank = local_data.eval_rank_this

    eval_info_total = local_data.evaluate_info_year_total
    person = local_data.person
    # 1. 局管领导
    head = _group_admin_evaluate()
    # 正科职干部履职
    zk = _get_zhengke_evaluate(eval_info, eval_info_total, eval_rank, person)
    # 副科职及以下干部履职
    fk = _get_fuke_evaluate(eval_info, eval_info_total, person)
    return {
        'admin': head,
        'zk': zk,
        'fk': fk
    }


def _group_admin_evaluate():
    eval_info = local_data.evaluate_info_this.copy()
    eval_rank = local_data.eval_rank_this.copy()
    chk_prob_data = local_data.check_problem_data_this.copy()
    qt_data = local_data.qt_data.copy()
    station_list = local_data.station_list.copy()
    person = local_data.person.copy()
    check_media_data = local_data.check_media_data.copy()
    # 局管干部履职
    admin_gen = _get_group_admin_eval(eval_info)
    # 领导班子履职表
    evaluate_table = get_evaluate_table(eval_rank, qt_data, chk_prob_data, station_list,
                                        local_data.year, local_data.month, person, check_media_data)
    admin_detail = _get_group_admin_diff(eval_rank, qt_data, chk_prob_data, eval_info, station_list,
                                         person, check_media_data,
                                         local_data.year, local_data.month)
    return {
        'admin_gen': admin_gen,
        'evaluate_table': evaluate_table['table_list'],
        'evaluate_tables': evaluate_table,
        'admin_detail': admin_detail
    }


def _get_group_admin_eval(evaluate_info):
    # 局管干部计分情况
    ju_data = evaluate_info[evaluate_info['GRADATION'] == '局管领导人员']
    person = len(ju_data)
    content = []
    for i, k in ju_data.iterrows():
        dic = {
            'station': k['RESP_STATION'],
            'job': k['JOB'],
            'name': k['RESPONSIBE_PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    return {
        'person': person,
        'content': content
    }


def _get_group_admin_diff(eval_rank_this, qt_data, check_problem_data_this, eval_info, station_list,
                          person, check_media_data, year, month):
    # 局管领导的差异 党群领导，行政领导（站段领导班子）
    # eval_rank = eval_rank[(eval_rank['LEVEL'].isin(['正处级', '副处级'])) & (eval_rank['BUSINESS_CLASSIFY'] == '领导')]
    eval_rank = eval_rank_this[
        eval_rank_this.apply(
            lambda row: _filter_data_bystr(
                row, 'ALL_NAME', ['党群领导', '行政领导']), axis=1)]
    qt_data = qt_data[
        qt_data.apply(
            lambda row: _filter_data_bystr(
                row, 'ALL_NAME', ['党群领导', '行政领导']), axis=1)]
    # check_problem_data_this = check_problem_data_this[check_problem_data_this['LEVEL'].isin(['正处级', '副处级'])]
    check_problem_data_this = check_problem_data_this[
        check_problem_data_this.apply(
            lambda row: _filter_data_bystr(
                row, 'CHECK_DEPT_NAME', ['党群领导', '行政领导']), axis=1)]
    eval_info = eval_info[eval_info['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
    check_media_data = check_media_data[
        check_media_data.apply(
            lambda row: _filter_data_bystr(
                row, 'ALL_NAME', ['党群领导', '行政领导']), axis=1)
    ]
    diff_list = get_evaluate_table(eval_rank, qt_data, check_problem_data_this,
                                   station_list, year, month, person, check_media_data)
    desc_list = get_evaluate_desc(eval_info, station_list)
    return {
        'diff_list': diff_list['table_list'],
        'desc_list': desc_list
    }


def get_evaluate_table(eval_rank, qt_data, check_problem_data_this,
                       station_list, year, month, person,
                       check_media_data):
    """
    计算履职分析表格
    :param eval_rank: 履职评价排名数据
    :param qt_data:  量化数据
    :param check_problem_data_this: {dataframe} 检查信息与问题数据
    :param station_list: {list} 站段列表
    :param year: 年份 综合指数的年份
    :param month: 月份 综合指数的月份
    :param person: 人员表
    :param check_media_data: 检查媒体信息
    :return:
    """
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.NAME
        } for it in station_list[station_list['TYPE'] == 4].itertuples()
    ]
    table_list = []
    # 系统的安全履职评价评价分
    sys_eval_avg = round(float(eval_rank['evaluate_score'].mean()), 4)
    # sys_check_avg = round(float(qt_data['REALITY_NUMBER'].mean()), 4)
    # sys_media_avg = round(float(qt_data['MEDIA_REALITY_TIME'].mean()), 4)
    sys_media_avg = round(sum(check_media_data['COST_TIME']) / len(set(check_media_data['ID_CARD'])), 1)
    # sys_problem_avg = round(float(qt_data['REALITY_PROBLEM_NUMBER'].mean()), 4)
    sys_qt_count = qt_data.shape[0]

    check_data = check_problem_data_this.copy()
    # 检查与具体人员绑定
    check_data = check_data.drop_duplicates(subset=['PK_ID_CK', 'CHECK_ID_CARD']).reset_index()
    sys_yecha_count = check_data[(check_data['IS_YECHA'] == 1) & (check_data['CHECK_WAY'].isin([1, 2]))].shape[0]
    sys_yecha_person = len(set(check_data[(check_data['IS_YECHA'] == 1) & (check_data['CHECK_WAY'].isin([1, 2]))]['CHECK_ID_CARD']))
    sys_check_avg = check_data[check_data['CHECK_WAY'].isin([1, 2])].shape[0] / len(
        set(check_data[check_data['CHECK_WAY'].isin([1, 2])]['CHECK_ID_CARD'])
    )
    sys_check_avg = round(sys_check_avg, 2)
    sys_yecha_avg = round(float(calc_rate(sys_yecha_count, sys_yecha_person)), 2)

    # 中高质量问题，问题质量分
    pro_data = check_problem_data_this[~check_problem_data_this['PK_ID_CP'].isna()].copy()
    pro_data = pro_data.drop_duplicates('PK_ID_CP')
    sys_problem_avg = round(
    pro_data.shape[0] / len(set(pro_data['CHECK_ID_CARD'])), 4)
    sys_mh_count = pro_data[pro_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
    sys_mh_avg = round(float(
        calc_rate(sys_mh_count,
                  len(set(
                      pro_data[pro_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)]['CHECK_ID_CARD'])
                  )
                  )
    ), 2)

    # 计划质量分
    plan_pro_score = round(float(
        qt_data['MIN_QUALITY_GRADES_TOTAL'].sum()),
        2)
    # 实际质量分
    real_pro_score = round(float(qt_data['REALITY_MIN_QUALITY_GRADES'].sum()), 2)

    sys_chk_score_total = round(float(pro_data['CHECK_SCORE'].sum()), 2)
    sys_chk_score_avg = round(calc_rate(sys_chk_score_total,
                                        len(set(pro_data['CHECK_ID_CARD']))), 2)

    # 完成率
    finish_diff = round(calc_rate(real_pro_score, plan_pro_score) * 100, 2)

    sys_ke = get_sys_shop_mean_data(qt_data, check_problem_data_this, '安全科')
    sys_ji = get_sys_shop_mean_data(qt_data, check_problem_data_this, '技术信息科')

    for st in st_list:
        sid, name = st['station_id'], st['station_name']
        erd = eval_rank[eval_rank['STATION'] == name]
        # 安全履职总分 / 评价分
        eval_total = round(float(erd['evaluate_score'].sum()), 2)
        eval_avg = round(float(erd['evaluate_score'].mean()), 2)
        eval_avg_diff = eval_avg - sys_eval_avg

        year_month = year * 100 + month
        health_index = _get_health_index(year_month, name)
        if health_index.empty is True:
            health_score = ''
            health_rank = ''
        else:
            health_score = float(health_index['SCORE'])
            health_rank = int(health_index['RANK'])

        # 站段人数
        std = person[person['STATION_ID'] == sid]
        std_count = std.shape[0]

        # 量化人数/检查次数
        qtd = qt_data[qt_data['STATION_ID'] == sid]
        qt_count = len(set(qtd['ID_CARD']))
        # check_count = int(qtd['REALITY_NUMBER'].sum())
        # check_avg = round(float(calc_rate(check_count, qt_count)))
        # check_avg_diff = round(check_avg - sys_check_avg, 2)

        # 完成率
        cpl_count = qtd[qtd['COMPLETION'] == 1].shape[0]
        qt_cpl_rate = round(100 * (calc_rate(cpl_count, qt_count)), 2)

        # 调阅时长
        media_len = round(float(qtd['MEDIA_REALITY_TIME'].sum()), 4)
        media_avg = round(float(calc_rate(media_len, qt_count)), 4)
        media_avg_diff = round(media_avg - sys_media_avg, 4)

        # 发现问题数
        # problem_count = round(int(qtd['REALITY_PROBLEM_NUMBER'].sum()), 2)
        # problem_avg = round(float(calc_rate(problem_count, qt_count)), 2)
        # problem_avg_diff = round(problem_avg - sys_problem_avg, 2)

        # 夜查次数，中高质量， 检查质量分
        cb_data = check_data[check_data['CHECK_STATION_ID'] == sid]
        # 现在检查次数
        check_count = cb_data[cb_data['CHECK_WAY'].isin([1, 2])].shape[0]
        check_avg = round(float(calc_rate(check_count,
                                          len(set(cb_data[cb_data['CHECK_WAY'].isin([1, 2])]['CHECK_ID_CARD'])
                                              ))))
        check_avg_diff = round(check_avg - sys_check_avg, 2)
        yecha_count = cb_data[(cb_data['IS_YECHA'] == 1) & (cb_data['CHECK_WAY'].isin([1, 2]))].shape[0]
        yecha_avg = round(float(calc_rate(yecha_count,
                                          len(set(
                                              cb_data[(cb_data['IS_YECHA'] == 1) &
                                                      (cb_data['CHECK_WAY'].isin([1, 2]))]['CHECK_ID_CARD']))
                                          )), 2)
        yecha_avg_diff = round(yecha_avg - sys_yecha_avg, 2)
        yecha_avg_percent = round(calc_rate(yecha_count, check_count) * 100, 2)

        # 中高质量
        p_data = pro_data[pro_data['CHECK_STATION_ID'] == sid]
        mh_count = p_data[p_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
        mh_avg = round(float(calc_rate(mh_count, len(
            set(p_data[p_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)]['CHECK_ID_CARD'])
        ))), 2)
        mh_avg_diff = round(sys_mh_avg - mh_avg, 2)

        # 发现问题数
        problem_count = p_data.shape[0]
        problem_avg = round(float(calc_rate(problem_count, len(set(p_data['CHECK_ID_CARD'])))), 2)
        problem_avg_diff = round(problem_avg - sys_problem_avg, 2)

        # 检查质量分
        chk_score_total = round(float(p_data['CHECK_SCORE'].sum()), 2)
        chk_score_avg = round(calc_rate(chk_score_total,
                                        len(set(
                                            p_data['CHECK_ID_CARD']
                                        ))), 2)
        chk_score_avg_diff = round(chk_score_avg - sys_chk_score_avg, 2)
        chk_score_number_avg = round(calc_rate(chk_score_total, problem_count), 2)
        chk_score_std_avg = round(calc_rate(chk_score_total, std_count), 2)

        # 安全科 / 技术科
        safety_dic = [
            {
                'station_id': it.DEPARTMENT_ID,
                'station_name': it.NAME
            } for it in
            station_list[(station_list['FK_PARENT_ID'] == sid) & (station_list['NAME'] == '安全科')].itertuples()
        ]
        tec_dic = [
            {
                'station_id': it.DEPARTMENT_ID,
                'station_name': it.NAME
            } for it in
            station_list[(station_list['FK_PARENT_ID'] == sid) & (station_list['NAME'] == '技术信息科')].itertuples()
        ]
        safety_ke = get_cal_shop_data(safety_dic, qt_data, check_problem_data_this, person, sys_ke, pro_data)
        tec_ke = get_cal_shop_data(tec_dic, qt_data, check_problem_data_this, person, sys_ji, pro_data)
        row = {
            'station': name,
            'safety_ke': safety_ke,
            'tec_ke': tec_ke,
            'eval_total': eval_total,
            'eval_avg': eval_avg,  # 人均安全履职得分
            'eval_avg_diff': eval_avg_diff,
            'sys_eval_avg': sys_eval_avg,
            'health_score': health_score,
            'health_rank': health_rank,  #
            'qt_count': qt_count,
            'check_count': check_count,  # 检查次数
            'check_avg': check_avg,
            'sys_check_avg': sys_check_avg,
            'check_avg_diff': check_avg_diff,
            'qt_cpl_rate': qt_cpl_rate,  # 完成率
            'media_len': media_len,  # 调阅时长
            'media_avg': media_avg,
            'media_avg_diff': media_avg_diff,
            'problem_count': problem_count,  # 发现问题数
            'problem_avg': problem_avg,
            'problem_avg_diff': problem_avg_diff,
            'yecha_count': yecha_count,  # 夜查次数
            'yecha_avg': yecha_avg,
            'yecha_avg_diff': yecha_avg_diff,
            'yecha_avg_percent': yecha_avg_percent,
            'mh_count': mh_count,  # 中高质量问题
            'mh_avg': mh_avg,
            'mh_avg_diff': mh_avg_diff,
            'chk_score_total': chk_score_total,  # 检查质量分
            'chk_score_avg': chk_score_avg,  # 量化人员平均质量分
            'chk_score_avg_diff': chk_score_avg_diff,
            'chk_score_number_avg': chk_score_number_avg,  # 平均问题质量分
            'chk_score_std_avg': chk_score_std_avg  # 单位人均质量分
        }
        table_list.append(row)
    yecha_list = sorted(table_list, key=lambda x: x['yecha_avg_percent'], reverse=True)
    score_list = sorted(table_list, key=lambda x: x['chk_score_std_avg'], reverse=True)
    return {
        'table_list': table_list,
        'plan_pro_score': plan_pro_score,
        'real_pro_score': real_pro_score,
        'finish_diff': finish_diff,
        'yecha_list': yecha_list,
        'score_list': score_list
    }


def get_sys_shop_mean_data(qt, check_problem_data, str1):
    qt_data = qt[qt['ALL_NAME'].str.contains(str1)]
    check_problem_data_this = check_problem_data[check_problem_data['CHECK_DEPT_NAME'].str.contains(str1)]
    # 系统的安全履职评价评价分
    sys_check_avg = round(float(qt_data['REALITY_NUMBER'].mean()), 4)
    sys_media_avg = round(float(qt_data['MEDIA_REALITY_TIME'].mean()), 4)
    sys_problem_avg = round(float(qt_data['REALITY_PROBLEM_NUMBER'].mean()), 4)
    sys_qt_count = qt_data.shape[0]

    check_data = check_problem_data_this.copy()
    check_data = check_data.drop_duplicates('PK_ID_CK')
    sys_yecha_count = check_data[(check_data['IS_YECHA'] == 1) & (check_data['CHECK_WAY'].isin([1, 2]))].shape[0]
    sys_yecha_avg = round(float(calc_rate(sys_yecha_count, sys_qt_count)), 2)

    # 中高质量问题，问题质量分
    pro_data = check_problem_data_this[~check_problem_data_this['PK_ID_CP'].isna()].copy()
    pro_data = pro_data.drop_duplicates('PK_ID_CP')
    sys_mh_count = pro_data[pro_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
    sys_mh_avg = round(float(calc_rate(sys_mh_count, sys_qt_count)), 2)

    sys_chk_score_total = round(float(pro_data['CHECK_SCORE'].sum()), 2)
    sys_chk_score_avg = round(calc_rate(sys_chk_score_total, sys_qt_count), 2)
    return {
        'sys_check_avg': sys_check_avg,
        'sys_media_avg': sys_media_avg,
        'sys_problem_avg': sys_problem_avg,
        'sys_yecha_avg': sys_yecha_avg,
        'sys_mh_avg': sys_mh_avg,
        'sys_chk_score_avg': sys_chk_score_avg
    }


def get_cal_shop_data(st, qt, check_problem_data, person, sys_mean, pro_data):
    if len(st) != 0:
        sid, name = st[0]['station_id'], st[0]['station_name']
        qt_data = qt[qt['SECTION_ID'] == sid]
        check_data = check_problem_data[check_problem_data['CHECK_DEPT_ID'] == sid]
        # 车间人数
        std = person[person['DEPARTMENT_ID'] == sid]
        std_count = std.shape[0]

        # 量化人数/检查次数
        qtd = qt_data
        qt_count = qtd.shape[0]
        check_count = int(qtd['REALITY_NUMBER'].sum())
        check_avg = round(float(calc_rate(qtd['REALITY_NUMBER'].sum(), qt_count)), 2)
        check_avg_diff = round(check_avg - sys_mean['sys_check_avg'], 2)

        # 完成率
        cpl_count = qtd[qtd['COMPLETION'] == 1].shape[0]
        qt_cpl_rate = round(100 * calc_rate(cpl_count, qt_count), 2)

        # 调阅时长
        media_len = round(float(qtd['MEDIA_REALITY_TIME'].sum()), 4)
        media_avg = round(calc_rate(media_len, qt_count), 4)
        media_avg_diff = round(media_avg - sys_mean['sys_media_avg'], 4)

        # 发现问题数
        problem_count = round(int(qtd['REALITY_PROBLEM_NUMBER'].sum()), 2)
        problem_avg = round(float(calc_rate(problem_count, qt_count)), 2)
        problem_avg_diff = round(problem_avg - sys_mean['sys_problem_avg'], 2)

        # 夜查次数，中高质量， 检查质量分
        cb_data = check_data
        xc_count = len(cb_data[cb_data['CHECK_WAY'].isin([1, 2])])
        yecha_count = cb_data[(cb_data['IS_YECHA'] == 1) & (cb_data['CHECK_WAY'].isin([1, 2]))].shape[0]
        yecha_avg = round(float(calc_rate(yecha_count, qt_count)), 2)
        yecha_avg_diff = round(yecha_avg - sys_mean['sys_yecha_avg'], 2)
        yecha_avg_percent = round(calc_rate(yecha_count, check_count) * 100, 2)

        # 中高质量
        p_data = pro_data[pro_data['TYPE4'] == sid]
        mh_count = p_data[p_data['LEVEL'].isin(MID_HIGH_PROBLEM_LEVEL)].shape[0]
        mh_avg = round(float(calc_rate(mh_count, qt_count)), 2)
        mh_avg_diff = round(sys_mean['sys_mh_avg'] - mh_avg, 2)

        # 检查质量分
        chk_score_total = round(float(p_data['CHECK_SCORE'].sum()), 2)
        chk_score_avg = round(calc_rate(chk_score_total, qt_count), 2)
        chk_score_avg_diff = round(chk_score_avg - sys_mean['sys_chk_score_avg'], 2)
        chk_score_number_avg = round(calc_rate(chk_score_total, problem_count), 2)
        chk_score_std_avg = round(calc_rate(chk_score_total, std_count), 2)

        return {
            'station': name,
            'qt_count': qt_count,
            'check_count': check_count,  # 检查次数
            'check_avg': check_avg,
            'check_avg_diff': check_avg_diff,
            'qt_cpl_rate': qt_cpl_rate,  # 完成率
            'media_len': media_len,  # 调阅时长
            'media_avg': media_avg,
            'media_avg_diff': media_avg_diff,
            'problem_count': problem_count,  # 发现问题数
            'problem_avg': problem_avg,
            'problem_avg_diff': problem_avg_diff,
            'yecha_count': yecha_count,  # 夜查次数
            'yecha_avg': yecha_avg,
            'yecha_avg_diff': yecha_avg_diff,
            'yecha_avg_percent': yecha_avg_percent,
            'mh_count': mh_count,  # 中高质量问题
            'mh_avg': mh_avg,
            'xc_count': xc_count,
            'mh_avg_diff': mh_avg_diff,
            'chk_score_total': chk_score_total,  # 检查质量分
            'chk_score_avg': chk_score_avg,  # 量化人员平均质量分
            'chk_score_avg_diff': chk_score_avg_diff,
            'chk_score_number_avg': chk_score_number_avg,  # 平均问题质量分
            'chk_score_std_avg': chk_score_std_avg,  # 单位人均质量分
            'sys_mean': sys_mean,
            'status': 1
        }
    else:
        return {
            'station': '',
            'qt_count': '',
            'check_count': '',  # 检查次数
            'check_avg': '',
            'check_avg_diff': '',
            'qt_cpl_rate': '',  # 完成率
            'media_len': '',  # 调阅时长
            'media_avg': '',
            'media_avg_diff': '',
            'problem_count': '',  # 发现问题数
            'problem_avg': '',
            'problem_avg_diff': '',
            'yecha_count': '',  # 夜查次数
            'yecha_avg': '',
            'yecha_avg_diff': '',
            'yecha_avg_percent': '',
            'mh_count': '',  # 中高质量问题
            'mh_avg': '',
            'xc_count': '',
            'mh_avg_diff': '',
            'chk_score_total': '',  # 检查质量分
            'chk_score_avg': '',  # 量化人员平均质量分
            'chk_score_avg_diff': '',
            'chk_score_number_avg': '',  # 平均问题质量分
            'chk_score_std_avg': '',  # 单位人均质量分
            'sys_mean': '',
            'status': 0
        }


def get_evaluate_desc(eval_info, station_list):
    """
    计算履职评价信息中，按不同站段
    :param eval_info: 检查评价
    :param station_list: {list} 站段列表
    :return:
    """
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.NAME
        } for it in station_list[station_list['TYPE'] == 4].itertuples()
    ]
    unit_list = []

    for st in st_list:
        sid, name = st['station_id'], st['station_name']
        cv = eval_info[eval_info['RESP_STATION_ID'] == sid]
        chk_eval_count = cv.shape[0]
        # 被路局评价
        cv0 = cv[cv['CHECK_DEPT_TYPE'].isin([5, 6])]
        chk_eval_count_grp = cv0.shape[0]
        if chk_eval_count_grp > 0:
            chk_eval_grp_list = [{
                'code': it.CODE,
                'count': it.PK_ID
            } for it in cv0.groupby('CODE').count().reset_index().
                sort_values('PK_ID', ascending=False).head(3).itertuples()
            ]
        else:
            chk_eval_grp_list = []

        # 非路局部门评价的
        cv1 = cv[~cv['CHECK_DEPT_TYPE'].isin([5, 6])]
        chk_eval_count_st = cv1.shape[0]
        if chk_eval_count_st > 0:
            chk_eval_st_list = [{
                'code': it.CODE,
                'count': it.PK_ID
            } for it in cv1.groupby('CODE').count().reset_index().
                sort_values('PK_ID', ascending=False).head(3).itertuples()
            ]
        else:
            chk_eval_st_list = []

        phase = {
            'station': name,
            'chk_eval_count': chk_eval_count,  # 被评价总条数
            'chk_eval_count_grp': chk_eval_count_grp,  # 路局评价
            'chk_eval_grp_list': chk_eval_grp_list,  # 站段评价问题清单
            'chk_eval_count_st': chk_eval_count_st,  # 站段自评
            'chk_eval_st_list': chk_eval_st_list  # 站段评价的问题清单
        }
        unit_list.append(phase)
    return unit_list


# (三)1.正科干部履职情况
def _get_zhengke_evaluate(eval_info, eval_info_total, eval_rank, person):
    gb_person = person[
        person['IDENTITY'] == '干部'
    ]['ID_CARD'].values.tolist()
    # eva0 = eval_info[eval_info['GRADATION'].str.contains('正科')]
    eva0 = eval_info[
        (eval_info['FK_PERSON_GRADATION_RATIO_ID'].isin([2])) &
        (eval_info['RESPONSIBE_ID_CARD'].isin(gb_person))
        ]
    # eva1 = eval_info_total[eval_info_total['GRADATION'].str.contains('正科')]
    eva1 = eval_info_total[
        (eval_info_total['FK_PERSON_GRADATION_RATIO_ID'].isin([2])) &
        (eval_info_total['RESPONSIBE_ID_CARD'].isin(gb_person))
    ]
    eval_arr = [eva0, eva1]
    person_count = len(eva0['RESPONSIBE_ID_CARD'].unique().tolist())
    content = []
    # 局评价的
    grp_eval = eva0[eva0['CHECK_TYPE'] == 1].sort_values(by='SCORE', ascending=False)
    for i, k in grp_eval.iterrows():
        dic = {
            'station': k['RESP_STATION'],
            'job': k['JOB'],
            'name': k['RESPONSIBE_PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    # 累计计分
    gb_arr = [
        d.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum().
            reset_index().sort_values('SCORE', ascending=False)
        for d in eval_arr
    ]

    # 本月扣分的前五民
    this_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[0].head(5).itertuples()
    ]

    # 本年扣分的前五民
    total_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[1].head(5).itertuples()
    ]

    eval_rank_desc = _get_eval_rank_of_position(eval_rank, ['安全科', '技术科', '职教科'], ['正科级','正科'], 3)

    return {
        'person_count': person_count,
        'problem_desc': content,
        'this_score_list': this_score_list,
        'total_score_list': total_score_list,
        'eval_rank': eval_rank_desc
    }


# (三)2.副科及干部履职情况
def _get_fuke_evaluate(eval_info, eval_info_total, person):
    # XX月份，副科职及以下干部XX人被记分，另有XX名非干部管理人员被评价记分。
    # eva0 = eval_info[eval_info['LEVEL'].isin(['副科级', '一般管理'])]
    gb_person = person[
        person['IDENTITY'] == '干部'
        ]['ID_CARD'].values.tolist()
    eva0 = eval_info[
        (eval_info['FK_PERSON_GRADATION_RATIO_ID'].isin([3, 4])) &
        (eval_info['RESPONSIBE_ID_CARD'].isin(gb_person))
        ]
    # eva1 = eval_info_total[eval_info_total['LEVEL'].isin(['副科级', '一般管理'])]
    eva1 = eval_info_total[
        (eval_info_total['FK_PERSON_GRADATION_RATIO_ID'].isin([3, 4])) &
        (eval_info_total['RESPONSIBE_ID_CARD'].isin(gb_person))]
    eval_arr = [eva0, eva1]
    person_count = len(set(eva0['RESPONSIBE_ID_CARD']))
    # person_count_x = len(set(eva0[eva0['LEVEL'] == '一般管理']['RESPONSIBE_ID_CARD']))
    person_count_x = len(set(eva0[eva0['FK_PERSON_GRADATION_RATIO_ID'].isin([4])]['RESPONSIBE_ID_CARD']))
    # 累计计分
    gb_arr = [
        d.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum().
            reset_index().sort_values('SCORE', ascending=False)
        for d in eval_arr
    ]

    # 本月扣分的前五民
    this_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[0].head(5).itertuples()
    ]

    # 本年扣分的前五民
    total_score_list = [
        {
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'score': round(float(it.SCORE), 2),
        } for it in gb_arr[1].head(5).itertuples()
    ]

    # 集团检查评价 + 从高到低前20名履职评价人员履职问题及记分
    eva_c1 = eva0[eva0['CHECK_TYPE'] == 1]
    gc1 = eva_c1.groupby(['RESP_DEPT_NAME', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'JOB'])['SCORE'].sum()
    gc1 = gc1.reset_index().sort_values('SCORE', ascending=False)
    person_list = []
    for it in gc1.head(20).itertuples():
        ev = eva_c1[eva_c1['RESPONSIBE_ID_CARD'] == it.RESPONSIBE_ID_CARD].sort_values('SCORE', ascending=False)
        detail = [
            {
                'score': round(float(i.SCORE), 2),
                'content': i.EVALUATE_CONTENT
            } for i in ev.itertuples()
        ]
        person_list.append({
            'dept': str(it.RESP_DEPT_NAME).replace('-', ''),
            'job': it.JOB,
            'name': it.RESPONSIBE_PERSON_NAME,
            'total_score': round(float(it.SCORE), 2),
            'detail': detail
        })

    return {
        'person_count': person_count,
        'person_count_x': person_count_x,  # 非干部人数
        'this_score_list': this_score_list,
        'total_score_list': total_score_list,
        'person_list': person_list  # 20名履职评价人员履职问题及记分
    }


def _get_eval_rank_of_position(eval_rank, sections, levels, count):
    head_list = []
    tail_list = []
    try:
        ev = eval_rank[eval_rank['DEPARTMENT'].isin(sections)]
        ev = ev[ev['LEVEL'].isin(levels)]
        rk = ev.sort_values('evaluate_score', ascending=False)
        head_list = [
            {
                'dept': str(it.ALL_NAME).replace('-', ''),
                'job': it.JOB,
                'name': it.NAME,
                'score': it.evaluate_score
            } for it in rk.head(count).itertuples()
        ]

        tail_list = [
            {
                'dept': str(it.ALL_NAME).replace('-', ''),
                'job': it.JOB,
                'name': it.NAME,
                'score': it.evaluate_score
            } for it in rk.tail(count).itertuples()
        ]
    except BaseException as ex:
        pass
    return {
        'head_list': head_list,
        'tail_list': tail_list
    }


# --------------------------------------- 第一章End
def _get_second():
    check_quality = _check_quality_analysis()
    return {
        'check_quality': check_quality
    }


def _check_quality_analysis():
    # （一）监督检查质量分析
    qt0 = local_data.qt_data
    qt1 = local_data.qt_data_prev
    cpd0 = local_data.check_problem_data_this
    cpd1 = local_data.check_problem_data_prev
    general = _check_quality_total(qt0, qt1, cpd0, cpd1)
    # 车间检查质量情况分析
    person = local_data.person
    chk = local_data.check_problem_data_this
    station = local_data.station_list
    shop_freq = _get_shop_freq(person, chk, station)
    # 整改质量分析
    cq_data = local_data.chaoqi_data
    zg_freq = _get_zg_freq(cq_data)
    # 考核质量分析
    assess_data = local_data.assess_money
    return_data = local_data.return_money
    assess_data_third_prv = local_data.assess_money_third_prv
    kh_freq = _get_kh_freq(person, chk, station, assess_data, return_data, assess_data_third_prv)
    # 音视频管理使用情况分析
    eva_info_this = local_data.evaluate_info_this
    mv_analysis = _get_mv_analysis(chk, eva_info_this)
    # 站段分析中心工作质量分析
    center = local_data.center
    qt_center = local_data.qt_center
    evaluate_this = local_data.evaluate_info_this
    check_media_data = local_data.check_media_data
    analysis_center_freq = _get_analysis_center_freq(person, center, chk, qt_center, evaluate_this,
                                                     check_media_data)
    # 违章大王情况
    key_person_data = local_data.key_person_data
    # iiiledge = _get_iiiledge(assess_data)
    iiiledge = _get_violation(key_person_data)
    return {
        'general': general,
        'shop_freq': shop_freq,
        'zg_freq': zg_freq,
        'kh_freq': kh_freq,
        'mv_analysis': mv_analysis,
        'analysis_center_freq': analysis_center_freq,
        'iiiledge': iiiledge
    }


def _get_violation(key_person):
    station_list = list(set(key_person['STATION_NAME']))
    all_station = []
    for station in station_list:
        tmp = key_person[
            key_person['STATION_NAME'] == station
        ]
        all_station.append(
            {
                'name': station,
                'count': len(tmp),
                'person_names': '、'.join(list(tmp['PERSON_NAME']))
            }
        )
    return {
        'wz_count': len(key_person),
        'all_station': all_station
    }

def _get_iiiledge(assess_data):
    # 违章大王人数
    wz_data = assess_data[assess_data['IS_OUT_SIDE_PERSON'] == 0].groupby('ID_CARD').sum().reset_index()
    wz = wz_data[wz_data['RESPONSIBILITY_SCORE'] >= 8]
    wz_count = len(wz)
    wz_id = list(wz['ID_CARD'])

    main_stations = []
    gener_stations = []
    for i in wz_id:
        # 严重违章大王
        data1 = assess_data[(assess_data['ID_CARD'] == i) & (assess_data['LEVEL'].isin(['A', 'B']))]
        if len(data1) >= 3:
            main_stations.append({
                'name': data1['PERSON_NAME'].iloc[0],
                'dp': data1['NAME'].iloc[0]
            })
        # 一般违章大王
        data2 = assess_data[(assess_data['ID_CARD'] == i) & (assess_data['LEVEL'].isin(['A', 'B', 'C']))]
        if len(data2) >= 3:
            gener_stations.append({
                'name': data2['PERSON_NAME'].iloc[0],
                'dp': data2['NAME'].iloc[0]
            })
    # 按站段分布
    sta_data = assess_data[assess_data['ID_CARD'].isin(wz_id)].groupby('NAME').count().reset_index()
    all_station = []
    for i, j in sta_data.iterrows():
        dic = {
            'name': j['NAME'],
            'count': int(j['LEVEL'])
        }
        all_station.append(dic)
    return {
        'wz_count': wz_count,
        'main_count': len(main_stations),
        'gener_count': len(gener_stations),
        'main_stations': main_stations,
        'gener_stations': gener_stations,
        'all_station': all_station
    }


def _get_analysis_center_freq(person, center, chk,
                              qt_center, evaluate_this,
                              check_media_data):
    # 站段分析中心ID
    st_list = [
        {
            'station_id': it.STATION_ID,
            'station_name': it.NAME,
            'shop_id': it.DEPARTMENT_ID
        } for it in center.itertuples()
    ]
    all_center = []
    for i in st_list:
        info = chk[chk['CHECK_DEPT_ID'] == i['shop_id']].drop_duplicates(subset=['PK_ID_CK']).reset_index()
        problem = chk[chk['CHECK_DEPT_ID'] == i['shop_id']].drop_duplicates(subset=['PK_ID_CP']).reset_index()
        check_mv = check_media_data[
            check_media_data['STATION_ID'] ==i['shop_id']].drop_duplicates(subset=['PK_ID']).reset_index()
        qt = qt_center[qt_center['FK_DEPARTMENT_ID'] == i['station_id']]
        eva = evaluate_this[evaluate_this['CHECK_PERSON_DEPARTMENT_ID'] == i['shop_id']]
        evaed = evaluate_this[evaluate_this['RESPONSIBE_DEPARTMENT_ID'] == i['shop_id']]
        # 现场检查人数
        # person_count = len(person[person['DEPARTMENT_ID'] == i['shop_id']])
        person_count = len(set(info['CHECK_ID_CARD']))
        # 分析中心得分
        center_score = round(float(sum(list(qt['MONITOR_PROBLEM_SCORE']))), 2)
        # 现场检查次数
        xc = len(info[info['CHECK_WAY'].isin([1, 2])])
        # 调阅时长
        # time = round(float(sum(list(qt['MONITOR_TIME']))), 4)
        time = round(float(sum(list(check_mv['COST_TIME']))), 4)
        # 人均检查次数
        check_avg = round(calc_rate(len(info), person_count), 2)
        # 人均调阅时长
        time_avg = round(calc_rate(time,
                                   len(set(
                                       check_mv['ID_CARD']
                                   ))), 4)
        # 发现问题数
        pro = len(problem)
        # 人均发现问题数
        pro_avg = round(calc_rate(pro, len(set(problem['CHECK_ID_CARD']))), 2)
        # 中高质量问题
        middle = len(problem[problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
        # 人均中高质量问题
        middle_avg = round(calc_rate(middle, len(set(
            problem[problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]['CHECK_ID_CARD']
        ))), 2)
        # 检查问题质量分
        check_score = round(float(problem['CHECK_SCORE'].sum()), 2)
        # 人均检查问题质量分
        check_score_avg = round(calc_rate(check_score, len(set(problem['CHECK_ID_CARD']))), 2)
        # 履职评价条数
        eva_count = len(eva)
        # 主动评价条数
        zd_count = len(eva[eva['EVALUATE_WAY'].isin([1, 2, 3])])
        # 中心人均评价分
        center_score_avg = round(calc_rate(float(eva['SCORE'].sum()), len(
            set(eva['RESPONSIBE_ID_CARD'])
        )), 4)
        # 上级检查中心问题数
        top_check = int(
            sum(qt.dropna(subset=['CHECK_ANALYSIS_CENTER_PROBLEM'])['CHECK_ANALYSIS_CENTER_PROBLEM'].tolist()))
        # 上级评价中心问题数
        top_eva = len(evaed[evaed['CHECK_TYPE'] == 1])
        # 中心人均被记分
        top_score_avg = round(calc_rate(
            float(sum(list(qt.dropna(subset=['CHECK_ANALYSIS_CENTER_SCORE'])['CHECK_ANALYSIS_CENTER_SCORE']))),
            person_count), 2)
        dic = {
            'name': i['station_name'],
            'center_score': center_score,
            'person_count': person_count,
            'xc': xc,
            'time': time,
            'check_avg': check_avg,
            'time_avg': time_avg,
            'pro': pro,
            'pro_avg': pro_avg,
            'middle': middle,
            'middle_avg': middle_avg,
            'check_score': check_score,
            'check_score_avg': check_score_avg,
            'eva_count': eva_count,
            'zd_count': zd_count,
            'center_score_avg': center_score_avg,
            'top_check': top_check,
            'top_eva': top_eva,
            'top_score_avg': top_score_avg
        }
        all_center.append(dic)
    return {
        'all_center': all_center
    }


def _get_mv_analysis(chk, eva_info_this):
    chk = chk.drop_duplicates(subset=['PK_ID_CK']).reset_index()
    check_data = chk[chk['CHECK_WAY'].isin([3, 4])]
    # 音视频设备管理不到位(评价信息中附标YY的)
    # mv_bdw = check_data[check_data['CHECK_ITEM_NAME'] == '车务-行车自管设备及备品-音视频监控设备']
    mv_bdw = eva_info_this[
        eva_info_this['CODE'].str.contains('YY')
    ]
    mv_bdw = mv_bdw.drop_duplicates(subset=['PK_ID']).reset_index()
    all_mv_bdw = []
    bdw_sum = 0
    for i in mv_bdw['RESP_STATION'].unique():
        data = mv_bdw[mv_bdw['RESP_STATION'] == i]
        # e1 = len(data[data['LEVEL'] == 'E1'])
        # e2 = len(data[(data['LEVEL'].isin(['E2', 'E3'])) & (data['TYPE'].isin([1, 2]))])
        # bdw_sum += e1 + e2
        dic = {
            'name': i,
            # 'count': e1 + e2
            'count': data.shape[0]
        }
        all_mv_bdw.append(dic)
    # 音视频系统运用情况
    dy_sum = 0
    all_dy = []
    for i in check_data['NAME'].unique():
        data = check_data[(check_data['NAME'] == i) & (check_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
        dic = {
            'name': i,
            'count': len(data)
        }
        dy_sum += len(data)
        all_dy.append(dic)
    # 调车移动记录仪运用情况
    ydjl_data = check_data[check_data['CHECK_ITEM_NAME'].str.contains('车务-现场作业音视频')]
    ydjl_sum = 0
    all_ydjl = []
    for i in ydjl_data['NAME'].unique():
        data = ydjl_data[(ydjl_data['NAME'] == i) & (ydjl_data['LEVEL'].isin(['A', 'B', 'C', 'D']))]
        dic = {
            'name': i,
            'count': len(data)
        }
        ydjl_sum += len(data)
        all_ydjl.append(dic)
    return {
        'all_mv_bdw': all_mv_bdw,
        'bdw_sum': bdw_sum,
        'dy_sum': dy_sum,
        'all_dy': all_dy,
        'ydjl_sum': ydjl_sum,
        'all_ydjl': all_ydjl,
        'ydjl_count': len(ydjl_data)
    }


def _get_kh_freq(person, chk, station_list, assess_data, return_data, assess_data_third_prv):
    # 获取所有站段
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.ALL_NAME
        } for it in station_list[station_list['TYPE'] == 4].itertuples()
    ]
    all_stations = []
    for i in st_list:
        person_data = person[
            person['STATION_ID'] == i['station_id']]
        chk_data = chk[chk['CHECK_STATION_ID'] == i['station_id']]
        pro_data = chk_data.drop_duplicates(subset=['PK_ID_CP']).reset_index()
        assess = assess_data[assess_data['DEPARTMENT_ID'] == i['station_id']]
        assess_third_prv = assess_data_third_prv[assess_data_third_prv['DEPARTMENT_ID'] == i['station_id']]
        re_money = return_data[return_data['DEPARTMENT_ID'] == i['station_id']]
        # 正式职工数
        xc_person = len(
            set(person_data['ID_CARD'])
        )
        # 考核问题数
        kh_count = len(pro_data[
                           (pro_data['IS_ASSESS'] == 1) &
                           (pro_data['IS_EXTERNAL'] == 0)])
        # 人均考核
        kh_avg = round(calc_rate(kh_count, xc_person), 2)
        # 考核金额
        assess_money = float(assess['ACTUAL_MONEY'].sum())
        # 三个月前考核金额
        assess_money_third_prv = float(assess_third_prv['ACTUAL_MONEY'].sum())
        # 返奖金额
        re_moneys = float(re_money['ACTUAL_MONEY'].sum())
        # 返奖率
        re_diff = round(calc_rate(re_moneys, assess_money_third_prv) * 100, 2)
        dic = {
            'name': i['station_name'],
            'manager': xc_person,
            'kh_count': kh_count,
            'kh_avg': kh_avg,
            'assess_money': assess_money,
            'assess_money_third_prv': assess_money_third_prv,
            're_moneys': re_moneys,
            're_diff': re_diff,
            'three_month_ago': f'{local_data.three_month_ago_year}年{local_data.three_month_ago}月'
        }
        all_stations.append(dic)
    # 根据条件排序
    kh_sort = sorted(all_stations, key=lambda x: x['kh_avg'], reverse=True)
    re_sort = sorted(all_stations, key=lambda x: x['re_diff'], reverse=True)
    return {
        'kh_sort': kh_sort,
        're_sort': re_sort
    }


def _get_zg_freq(cq_data):
    """
    整改质量分析
    :param cq_data:
    :return:
    """
    # 超期整改问题个数
    # cq_zg = cq_data[cq_data['TYPE'] == 2]
    cq_zg = cq_data.drop_duplicates(subset=['PK_ID']).reset_index()
    all_station = []
    for i, j in cq_zg.groupby('NAME').count().reset_index().iterrows():
        dic = {
            'name': j['NAME'],
            # 'count': int(j['TYPE'])
            'count': int(j['COUNT'])
        }
        all_station.append(dic)
    return {
        'cq_count': len(cq_zg),
        'all_station': all_station
    }


def _retrieve_all_dp_configid(top_ids):
    """
    获取所有部门配置id
    检修及整备车间(包含车间班组分类配置-机务-检整车间-检修车间、整备车间、大部件检修车间(parent id = 6)，
    以及车间班组分类配置-机务-解体组装车间(parent id = 281)
    """
    # 查询所属的子check_item的PK_ID
    load_child_check_item_sql = "SELECT PK_ID FROM t_department_classify_config WHERE PARENT_ID IN ({0});"
    ret = top_ids
    parent_ids = top_ids
    while len(parent_ids) > 0:
        df = pd_query(load_child_check_item_sql.format(parent_ids))
        if len(df) > 0:
            ids_list = df['PK_ID'].tolist()
            str_list = [str(val) for val in ids_list]
            parent_ids = ','.join(str_list)
            ret = ret + ',' + parent_ids
        else:
            break

    # 去除重复的PK_ID
    total_set = set(ret.split(','))
    return list(total_set)


def _get_shop_freq(person, chk, station_list):
    """
    车间检查质量情况分析
    :param person:
    :param chk:
    :param station_list:
    :return:
    """
    # 获取所有车间(运输车间（FK_DEPARTMENT_CLASSIFY_CONFIG_ID=224）)
    all_shopconfig_ids = _retrieve_all_dp_configid('224')
    st_list = [
        {
            'station_id': it.DEPARTMENT_ID,
            'station_name': it.ALL_NAME
        } for it in station_list[
            (station_list['TYPE'] == 8) &
            (station_list['FK_DEPARTMENT_CLASSIFY_CONFIG_ID'].isin(all_shopconfig_ids))
            ].itertuples()
    ]
    all_shops = []
    for i in st_list:
        # person_data = person[person['DEPARTMENT_ID'] == i['station_id']]
        person_data = person[person['WORKSHOP_ID'] == i['station_id']]
        chk_data = chk[chk['CHECK_DEPT_ID'] == i['station_id']]
        ck_data = chk_data.drop_duplicates(subset=['PK_ID_CK', 'CHECK_ID_CARD']).reset_index()
        pro_data = chk_data.drop_duplicates(subset=['PK_ID_CP']).reset_index()
        # 管理人员(现场检查)
        xc_person = len(
            set(person_data[person_data['IDENTITY'] == '干部']['ID_CARD']) &
            set(ck_data[ck_data['CHECK_WAY'].isin([1, 2])]['CHECK_ID_CARD'])
        )
        # 现场检查
        xc_count = len(ck_data[ck_data['CHECK_WAY'].isin([1, 2])])
        # 人均现场检查
        xc_avg = round(calc_rate(xc_count,
                                 len(set(ck_data[ck_data['CHECK_WAY'].isin([1, 2])]['CHECK_ID_CARD']))),
                       2)
        # 夜查
        yc_count = len(ck_data[
                           (ck_data['IS_YECHA'].isin([1])) &
                           (ck_data['CHECK_WAY'].isin([1, 2]))
                       ])
        # 夜查干部
        yc_manager = len(
            set(person_data[person_data['IDENTITY'] == '干部']['ID_CARD']) &
            set(ck_data[(ck_data['IS_YECHA'].isin([1])) &
                           (ck_data['CHECK_WAY'].isin([1, 2]))]['CHECK_ID_CARD'])
        )
        # 人均夜查
        yc_avg = round(calc_rate(yc_count,
                                 len(set(ck_data[
                           (ck_data['IS_YECHA'].isin([1])) &
                           (ck_data['CHECK_WAY'].isin([1, 2]))
                       ]['CHECK_ID_CARD']))), 2)
        # 高质量
        high_count = len(pro_data[pro_data['LEVEL'].isin(['A', 'B', 'F1'])])
        # 高质量干部
        high_manager = len(
            set(person_data[person_data['IDENTITY'] == '干部']['ID_CARD']) &
            set(pro_data[pro_data['LEVEL'].isin(['A', 'B', 'F1'])]['CHECK_ID_CARD'])
        )
        # 人均高质量
        high_avg = round(calc_rate(high_count,
                                   len(set(
                                       pro_data[pro_data['LEVEL'].isin(
                                           ['A', 'B', 'F1'])]['CHECK_ID_CARD']
                                   ))), 2)
        # 发现问题质量分
        score = float(pro_data['CHECK_SCORE'].sum())
        score_manager = len(
            set(person_data[person_data['IDENTITY'] == '干部']['ID_CARD']) &
            set(pro_data['CHECK_ID_CARD'])
        )
        # 人均质量分
        score_avg = round(calc_rate(score, len(set(
                                       pro_data['CHECK_ID_CARD']
                                   ))), 2)
        dic = {
            'name': i['station_name'],
            'manager': xc_person,
            'xc_count': xc_count,
            'xc_avg': xc_avg,
            'yc_count': yc_count,
            'yc_avg': yc_avg,
            'high_count': high_count,
            'high_avg': high_avg,
            'score': score,
            'score_avg': score_avg,
            'yc_manager': yc_manager,
            'high_manager': high_manager,
            'score_manager': score_manager
        }
        all_shops.append(dic)
    # 根据条件排序
    xc_sort = sorted(all_shops, key=lambda x: x['xc_avg'], reverse=True)
    yc_sort = sorted(all_shops, key=lambda x: x['yc_avg'], reverse=True)
    high_sort = sorted(all_shops, key=lambda x: x['high_avg'], reverse=True)
    score_sort = sorted(all_shops, key=lambda x: x['score_avg'], reverse=True)
    return {
        'xc_sort': xc_sort,
        'yc_sort': yc_sort,
        'high_sort': high_sort,
        'score_sort': score_sort
    }


def _check_quality_total(qt_this, qt_prev, check_pro_data, check_pro_data_prev):
    """
    计算监督检查质量分析情况
    :param qt_this: 本月量化数据
    :param qt_prev: 上月的量化数据
    :param check_pro_data: 检查数据与问题（本月）
    :param check_pro_data_prev: 检查信息与问题（上月）
    :return:
    """
    # major = _check_quality_unit_situation(qt_this, qt_prev, check_pro_data, check_pro_data_prev)
    # 运输处的数据
    ts_qt_this = qt_this[qt_this['STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_qt_prev = qt_prev[qt_prev['STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_check_pro_data = check_pro_data[check_pro_data['CHECK_STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    ts_check_pro_data_prev = check_pro_data_prev[check_pro_data_prev['CHECK_STATION_ID'] == TRANSPORT_ADMIN_DEPT_ID]
    trans_admin = _check_quality_unit_situation(ts_qt_this, ts_qt_prev, ts_check_pro_data, ts_check_pro_data_prev)
    return {
        # 'major': major,
        'trans_admin': trans_admin
    }


def _check_quality_unit_situation(qt_this, qt_prev, check_pro_data, check_pro_data_prev):
    # 车务专业的量化考核干部清单, 用于筛选检查信息清单
    cadre0 = qt_this[['ID_CARD', 'PERSON_NAME', 'STATION_ID', 'REALITY_NUMBER']]
    cadre0.rename(columns={"ID_CARD": "QT_ID_CARD", "PERSON_NAME": "QT_PERSON_NAME",
                           "STATION_ID": "QT_STATION_ID"}, inplace=True)
    cadre1 = qt_prev[['ID_CARD', 'PERSON_NAME', 'STATION_ID']]
    cadre1.rename(columns={"ID_CARD": "QT_ID_CARD", "PERSON_NAME": "QT_PERSON_NAME",
                           "STATION_ID": "QT_STATION_ID"}, inplace=True)

    cpd0 = check_pro_data.copy()
    cpd1 = check_pro_data_prev.copy()
    cpd0 = pd.merge(left=cpd0, right=cadre0, left_on='CHECK_ID_CARD', right_on='QT_ID_CARD')
    cpd1 = pd.merge(left=cpd1, right=cadre1, left_on='CHECK_ID_CARD', right_on='QT_ID_CARD')

    # 删除重复的记录，保证每个人员一条检查信息（数据中，包含了检查人以及问题，所以一个检查信息出现多次）
    ck0 = cpd0.drop_duplicates(['PK_ID_CK', 'CHECK_ID_CARD'])
    ck1 = cpd1.drop_duplicates(['PK_ID_CK', 'CHECK_ID_CARD'])

    ck_arr = [ck0, ck1]
    # 计算夜查次数/夜查率/环比
    # 本段计算出来的检查次数，是检查人次，大于检查次数。
    # 检查次数兑现率
    xc_cpl_rate = calc_rate(qt_this['REALITY_NUMBER'].sum(), qt_this['CHECK_TIMES_TOTAL'].sum()) * 100
    yc_counts = [
        len(d[(d['IS_YECHA'] == 1) & (d['CHECK_WAY'].isin([1, 2]))]) for d in ck_arr
    ]
    xc_counts = [
        len(d[d['CHECK_WAY'].isin([1, 2])]) for d in ck_arr
    ]
    xc_count_ring = calc_ratio(xc_counts[0], xc_counts[1])
    yc_count_ring = calc_ratio(yc_counts[0], yc_counts[1])
    # 夜查率
    yc_rate0 = calc_rate(yc_counts[0], xc_counts[0])
    yc_rate1 = calc_rate(yc_counts[1], xc_counts[1])
    # 夜查率环比
    yc_rate_ring = calc_ratio(yc_rate0, yc_rate1)

    # 发现问题个数/兑现率
    pro_cpl_rate = calc_rate(qt_this['REALITY_PROBLEM_NUMBER'].sum(), qt_this['PROBLEM_NUMBER_TOTAL'].sum()) * 100
    pb0 = cpd0.drop_duplicates(['PK_ID_CP'])
    pb1 = cpd1.drop_duplicates(['PK_ID_CP'])
    # 现场检查发现的问题
    pb_arr = [pb0, pb1]
    xc_pro_counts = [len(d[d['CHECK_WAY'].isin([1, 2])]) for d in pb_arr]
    xc_pro_ring = calc_ratio(xc_pro_counts[0], xc_pro_counts[1])
    # 其中，现场作业类问题
    xczy_pro_counts = [len(d[(d['CHECK_WAY'].isin([1, 2])) & (d['LEVEL'].isin(['A', 'B', 'C', 'D']))]) for d in pb_arr]
    # 高质量问题/环比
    xcgzl_pro_counts = [len(d[(d['CHECK_WAY'].isin([1, 2])) & (d['LEVEL'].isin(['A', 'B', 'F1']))]) for d in pb_arr]
    xcgzl_pro_ring = calc_ratio(xcgzl_pro_counts[0], xcgzl_pro_counts[1])

    # 音视频调阅
    md_cpl_rate = calc_rate(qt_this['MEDIA_REALITY_TIME'].sum(), qt_this['MONITOR_NUMBER_TOTAL'].sum())
    md_time = round(float(qt_this['MEDIA_REALITY_TIME'].sum()), 2)

    # 调阅发现问题数量
    md_pro_counts = [len(d[(d['CHECK_WAY'].isin([3])) & (d['LEVEL'].isin(['A', 'B', 'C', 'D']))]) for d in pb_arr]
    md_pro_ring = calc_ratio(md_pro_counts[0], md_pro_counts[1])
    # 调阅发现的作业类高质量问题
    mdgzl_pro_counts = [len(d[(d['CHECK_WAY'].isin([3])) & (d['LEVEL'].isin(['A', 'B']))]) for d in pb_arr]
    mdgzl_pro_ring = calc_ratio(mdgzl_pro_counts[0], mdgzl_pro_counts[1])

    content = {
        'xc_count': xc_counts[0],  # 现场检查次数
        'xc_count_ring': xc_count_ring,  # 现场检查次数环比
        'xc_cpl_rate': xc_cpl_rate,  # 现场检查兑现率（量化任务对比）
        'yc_count': yc_counts[0],  # 夜查数
        'yc_count_ring': yc_count_ring,  # 夜查环比
        'yc_rate': yc_rate0,  # 夜查率
        'yc_rate_ring': yc_rate_ring,  # 夜查率环比
        'xc_pro_count': xc_pro_counts[0],  # 现场检查发现问题数
        'pro_cpl_rate': pro_cpl_rate,  # 现场检查发现问题数兑现率
        'xc_pro_ring': xc_pro_ring,  # 先查发现问题环比
        'xczy_pro_count': xczy_pro_counts[0],  # 发现的现场作业问题数量
        'xcgzl_pro_count': xcgzl_pro_counts[0],  # 发现的现场高质量问题数量
        'xcgzl_pro_ring': xcgzl_pro_ring,  # 发现的现场高质量问题数量环比
        'md_time': md_time,  # 调阅时长
        'md_cpl_rate': md_cpl_rate,  # 调阅时长兑现率
        'md_pro_count': md_pro_counts[0],  # 监控调阅发现行车作业问题数量
        'md_pro_ring': md_pro_ring,  # 监控调阅发现行车作业问题环比
        'mdgzl_pro_count': mdgzl_pro_counts[0],  # 调阅发现作业类高质量问题
        'mdgzl_pro_ring': mdgzl_pro_ring,  # 调阅发现作业类高质量问题 环比
    }
    return content


def get_third():
    cadre_cumulative = get_cadre_cumulative(local_data.cadre_cumulative_data, local_data.year_cadre_cumulative_data)

    return {'cadre_cumulative': cadre_cumulative}


def get_cadre_cumulative(cadre_cumulative_data, year_cadre_cumulative_data):
    all_station = []
    index = 0
    for name in cadre_cumulative_data['ALL_NAME'].unique():
        index += 1
        station_data = []
        month_data = cadre_cumulative_data[cadre_cumulative_data['ALL_NAME'] == name]
        year_data = year_cadre_cumulative_data[year_cadre_cumulative_data['ALL_NAME'] == name]
        for j in [month_data, year_data]:
            station_data.append(cal_level_score_count(j))

        station = {
            'index': index,
            'name': name,
            'station_data': station_data
        }

        all_station.append(station)

    res = {
        'all_station': all_station
    }

    return res


def cal_level_score_count(data):
    level_list = ['局管干部', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']
    level_jg = data.loc[data.GRADATION == level_list[0]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
    level_zk = data.loc[data.GRADATION == level_list[1]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
    level_fkjyx = data.loc[(data.GRADATION == level_list[2]) | (data.GRADATION == level_list[3])].drop_duplicates(
        subset='RESPONSIBE_ID_CARD')
    level_fg = data.loc[data.GRADATION == level_list[4]].drop_duplicates(subset='RESPONSIBE_ID_CARD')
    # 局管干部数据
    jg_people_count = len(level_jg)
    jg_level_1_count = len(level_jg.loc[level_jg['SCORE'] < 2])
    jg_level_2_count = len(level_jg.loc[(level_jg['SCORE'] >= 2) & (level_jg['SCORE'] < 4)])
    jg_level_3_count = len(level_jg.loc[(level_jg['SCORE'] >= 4) & (level_jg['SCORE'] < 6)])
    jg_level_4_count = len(level_jg.loc[(level_jg['SCORE'] >= 6) & (level_jg['SCORE'] < 8)])
    jg_level_5_count = len(level_jg.loc[(level_jg['SCORE'] >= 8) & (level_jg['SCORE'] < 10)])
    jg_level_6_count = len(level_jg.loc[(level_jg['SCORE'] >= 10) & (level_jg['SCORE'] < 12)])
    jg_level_7_count = len(level_jg.loc[level_jg['SCORE'] >= 12])
    jg = {
        'people_count': jg_people_count,
        'level_1_count': jg_level_1_count,
        'level_2_count': jg_level_2_count,
        'level_3_count': jg_level_3_count,
        'level_4_count': jg_level_4_count,
        'level_5_count': jg_level_5_count,
        'level_6_count': jg_level_6_count,
        'level_7_count': jg_level_7_count,
    }
    # 正科数据
    zk_people_count = len(level_zk)
    zk_level_1_count = len(level_zk.loc[level_zk['SCORE'] < 2])
    zk_level_2_count = len(level_zk.loc[(level_zk['SCORE'] >= 2) & (level_zk['SCORE'] < 4)])
    zk_level_3_count = len(level_zk.loc[(level_zk['SCORE'] >= 4) & (level_zk['SCORE'] < 6)])
    zk_level_4_count = len(level_zk.loc[(level_zk['SCORE'] >= 6) & (level_zk['SCORE'] < 8)])
    zk_level_5_count = len(level_zk.loc[(level_zk['SCORE'] >= 8) & (level_zk['SCORE'] < 10)])
    zk_level_6_count = len(level_zk.loc[(level_zk['SCORE'] >= 10) & (level_zk['SCORE'] < 12)])
    zk_level_7_count = len(level_zk.loc[level_zk['SCORE'] >= 12])
    zk = {
        'people_count': zk_people_count,
        'level_1_count': zk_level_1_count,
        'level_2_count': zk_level_2_count,
        'level_3_count': zk_level_3_count,
        'level_4_count': zk_level_4_count,
        'level_5_count': zk_level_5_count,
        'level_6_count': zk_level_6_count,
        'level_7_count': zk_level_7_count,
    }
    # 副科及以下数据
    fkjyx_people_count = len(level_fkjyx)
    fkjyx_level_1_count = len(level_fkjyx.loc[level_fkjyx['SCORE'] < 2])
    fkjyx_level_2_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 2) & (level_fkjyx['SCORE'] < 4)])
    fkjyx_level_3_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 4) & (level_fkjyx['SCORE'] < 6)])
    fkjyx_level_4_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 6) & (level_fkjyx['SCORE'] < 8)])
    fkjyx_level_5_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 8) & (level_fkjyx['SCORE'] < 10)])
    fkjyx_level_6_count = len(level_fkjyx.loc[(level_fkjyx['SCORE'] >= 10) & (level_fkjyx['SCORE'] < 12)])
    fkjyx_level_7_count = len(level_fkjyx.loc[level_fkjyx['SCORE'] >= 12])
    fkjyx = {
        'people_count': fkjyx_people_count,
        'level_1_count': fkjyx_level_1_count,
        'level_2_count': fkjyx_level_2_count,
        'level_3_count': fkjyx_level_3_count,
        'level_4_count': fkjyx_level_4_count,
        'level_5_count': fkjyx_level_5_count,
        'level_6_count': fkjyx_level_6_count,
        'level_7_count': fkjyx_level_7_count,
    }
    # 非管数据
    fg_people_count = len(level_fg)
    fg_level_1_count = len(level_fg.loc[level_fg['SCORE'] < 2])
    fg_level_2_count = len(level_fg.loc[(level_fg['SCORE'] >= 2) & (level_fg['SCORE'] < 4)])
    fg_level_3_count = len(level_fg.loc[(level_fg['SCORE'] >= 4) & (level_fg['SCORE'] < 6)])
    fg_level_4_count = len(level_fg.loc[(level_fg['SCORE'] >= 6) & (level_fg['SCORE'] < 8)])
    fg_level_5_count = len(level_fg.loc[(level_fg['SCORE'] >= 8) & (level_fg['SCORE'] < 10)])
    fg_level_6_count = len(level_fg.loc[(level_fg['SCORE'] >= 10) & (level_fg['SCORE'] < 12)])
    fg_level_7_count = len(level_fg.loc[level_fg['SCORE'] >= 12])
    fg = {
        'people_count': fg_people_count,
        'level_1_count': fg_level_1_count,
        'level_2_count': fg_level_2_count,
        'level_3_count': fg_level_3_count,
        'level_4_count': fg_level_4_count,
        'level_5_count': fg_level_5_count,
        'level_6_count': fg_level_6_count,
        'level_7_count': fg_level_7_count,
    }

    result = {
        'jg': jg,
        'zk': zk,
        'fkjyx': fkjyx,
        'fg': fg
    }

    return result


# 接发列车
def get_four():
    four_1 = get_four_1(local_data.check_info_data, local_data.check_info_last_month_data)
    four_2 = get_four_2(local_data.check_problem)
    four_3 = get_four_3(local_data.check_problem)
    four_4 = get_four_4(local_data.check_problem)
    four_5 = get_four_5(local_data.check_problem)

    return {
        'four_1': four_1,
        'four_2': four_2,
        'four_3': four_3,
        'four_4': four_4,
        'four_5': four_5,
    }


def get_four_1(check_info_data, check_info_last_month_data):
    if check_info_data['COST_TIME'].isnull().any():
        check_info_data['COST_TIME'].fillna(0, inplace=True)

    all_station = []
    for zhanduan in check_info_data['NAME'].unique():
        # 上月
        zhanduan_last_month_data = check_info_last_month_data[check_info_last_month_data['NAME'] == zhanduan]
        zhanduan_last_month_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        zhanduan_last_month_data = zhanduan_last_month_data[zhanduan_last_month_data['CHECK_ITEM_NAME'].str.contains('接发列车')]
        last_month_total_check_time = zhanduan_last_month_data.shape[0]
        # 当月
        zhanduan_data = check_info_data[check_info_data['NAME'] == zhanduan]
        zhanduan_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        zhanduan_data = zhanduan_data[zhanduan_data['CHECK_ITEM_NAME'].str.contains('接发列车')]
        total_check_time = zhanduan_data.shape[0]
        # 环比
        sequential = calc_ratio(total_check_time, last_month_total_check_time)['percent']
        # 现场检查次数
        xc_check_time = zhanduan_data[zhanduan_data['CHECK_WAY'] == 1].shape[0]
        # 添乘检查
        tc_check_time = zhanduan_data[zhanduan_data['CHECK_WAY'] == 2].shape[0]
        # 调阅检查
        dy_check_time = zhanduan_data[(zhanduan_data['CHECK_WAY'] == 3) | (zhanduan_data['CHECK_WAY'] == 4)].shape[0]
        # 调阅时长
        cost_time = round(zhanduan_data['COST_TIME'].sum(), 2)
        # 夜查次数
        yc_check_time = zhanduan_data[zhanduan_data['IS_YECHA'] == 1].shape[0]
        # 夜查率
        if yc_check_time == 0:
            yc_ratio = 0
        else:
            yc_ratio = round((yc_check_time / total_check_time) * 100, 1)

        station = {
            'station_name': zhanduan,
            'total_check_time': total_check_time,
            'sequential': sequential,
            'xc_check_time': xc_check_time,
            'tc_check_time': tc_check_time,
            'dy_check_time': dy_check_time,
            'cost_time': cost_time,
            'yc_check_time': yc_check_time,
            'yc_ratio': yc_ratio,
        }

        all_station.append(station)
    all_station.sort(key=lambda x: x['yc_ratio'], reverse=True)
    res = {
        'all_station': all_station
    }
    return res


def get_four_2(check_problem_data):
    all_station = []
    LEVEL = ['A', 'B', 'F1']
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('接发列车')]

        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        # 问题总数
        self_check_problem_count = int(self_check_data['PROBLEM_NUMBER'].sum())
        # 高质量问题数
        self_check_high_level_problem_count = int(self_check_data[self_check_data['LEVEL'].isin(LEVEL)]['PROBLEM_NUMBER'].sum())
        # 现场检查
        self_check_xc_check_time = int(self_check_data[self_check_data['CHECK_WAY'] == 1].shape[0])
        # 高质量问题占比
        if self_check_high_level_problem_count == 0:
            self_check_high_level_problem_ratio = 0
        else:
            self_check_high_level_problem_ratio = round((self_check_high_level_problem_count / self_check_problem_count) * 100, 1)

        self_check = {
            'problem_count': self_check_problem_count,
            'high_level_problem_count': self_check_high_level_problem_count,
            'xc_check_time': self_check_xc_check_time,
            'high_level_problem_ratio': self_check_high_level_problem_ratio,
        }
        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        # 问题总数
        top_check_problem_count = int(top_check_data['PROBLEM_NUMBER'].sum())
        # 高质量问题
        top_check_high_level_problem_count = int(top_check_data[top_check_data['LEVEL'].isin(LEVEL)]['PROBLEM_NUMBER'].sum())
        # 现场检查
        top_check_xc_check_time = int(top_check_data[top_check_data['CHECK_WAY'] == 1].shape[0])
        # 高质量问题占比
        if top_check_high_level_problem_count == 0:
            top_check_high_level_problem_ratio = 0
        else:
            top_check_high_level_problem_ratio = round((top_check_high_level_problem_count / top_check_problem_count) * 100, 1)

        top_check = {
            'problem_count': top_check_problem_count,
            'high_level_problem_count': top_check_high_level_problem_count,
            'xc_check_time': top_check_xc_check_time,
            'high_level_problem_ratio': top_check_high_level_problem_ratio
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check,
        }
        all_station.append(station)

    res = {'all_station': all_station}
    return res


def get_four_3(check_problem_data):
    CHECK_ITEM_DICT = {
        '设备操作': '车务-接发列车（高）-设备操作',
        '作业两纪': '车务-接发列车（高）-作业两纪',
        '安全卡控': '车务-接发列车（高）-安全卡控',
        '簿册登记': '车务-接发列车（高）-簿册填记',
        '劳动安全': '车务-接发列车（高）-劳动安全',
        '规章管理': '车务-行车专业管理（高）-规章管理',
    }

    CHECK_ITEM_LIST = [
        '车务-接发列车（高）-设备操作',
        '车务-接发列车（高）-作业两纪',
        '车务-接发列车（高）-安全卡控',
        '车务-接发列车（高）-簿册填记',
        '车务-接发列车（高）-劳动安全',
        '车务-行车专业管理（高）-规章管理',
    ]

    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('接发列车')]

        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_data = self_check_data[self_check_data['CHECK_ITEM_NAME'].isin(CHECK_ITEM_LIST)]
        self_check_problem_count = int(self_check_data['PROBLEM_NUMBER'].sum())
        # 设备操作
        self_check_sbcz_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['设备操作']]['PROBLEM_NUMBER'].sum())
        # 作业两纪
        self_check_zylj_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业两纪']]['PROBLEM_NUMBER'].sum())
        # 安全卡控
        self_check_aqkk_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['安全卡控']]['PROBLEM_NUMBER'].sum())
        # 簿册填记
        self_check_bcdj_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['簿册登记']]['PROBLEM_NUMBER'].sum())
        # 劳动安全
        self_check_ldaq_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['劳动安全']]['PROBLEM_NUMBER'].sum())
        # 规章管理
        self_check_gzgl_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['规章管理']]['PROBLEM_NUMBER'].sum())

        self_check = {
            'problem_count': self_check_problem_count,
            'sbcz_count': self_check_sbcz_count,
            'zylj_count': self_check_zylj_count,
            'aqkk_count': self_check_aqkk_count,
            'bcdj_count': self_check_bcdj_count,
            'ldaq_count': self_check_ldaq_count,
            'gzgl_count': self_check_gzgl_count
        }
        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_data = top_check_data[top_check_data['CHECK_ITEM_NAME'].isin(CHECK_ITEM_LIST)]
        if top_check_data.shape[0] == 0:
            top_check_data['PROBLEM'] = 0
        top_check_problem_count = int(top_check_data['PROBLEM_NUMBER'].sum())
        top_check_sbcz_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['设备操作']]['PROBLEM_NUMBER'].sum())
        top_check_zylj_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业两纪']]['PROBLEM_NUMBER'].sum())
        top_check_aqkk_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['安全卡控']]['PROBLEM_NUMBER'].sum())
        top_check_bcdj_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['簿册登记']]['PROBLEM_NUMBER'].sum())
        top_check_ldaq_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['劳动安全']]['PROBLEM_NUMBER'].sum())
        top_check_gzgl_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['规章管理']]['PROBLEM_NUMBER'].sum())

        top_check = {
            'problem_count': top_check_problem_count,
            'sbcz_count': top_check_sbcz_count,
            'zylj_count': top_check_zylj_count,
            'aqkk_count': top_check_aqkk_count,
            'bcdj_count': top_check_bcdj_count,
            'ldaq_count': top_check_ldaq_count,
            'gzgl_count': top_check_gzgl_count
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)

    res = {'all_station': all_station}
    return res


def get_four_4(check_problem_data):
    CHECK_ITEM_DICT = {
        '出务接发车': '车务-接发列车（普）-出务接发车',
        '作业互控': '车务-接发列车（普）-作业互控',
        '行车凭证': '车务-接发列车（普）-行车凭证',
        '调度命令': '车务-接发列车（普）-调度命令',
        '非正常作业': '车务-接发列车（普）-非正常作业',
        '防错办': '车务-接发列车（普）-防错办',
        '车机联控': '车务-接发列车（普）-车机联控',
        '设备操作': '车务-接发列车（普）-设备操作',
        '发车条件确认': '车务-接发列车（普）-发车条件确认',
        '作业标准': '车务-接发列车（普）-作业标准',
        '规章管理': '车务-行车专业管理（普）-规章管理',
    }

    CHECK_ITEM_LIST = [
        '车务-接发列车（普）-出务接发车',
        '车务-接发列车（普）-作业互控',
        '车务-接发列车（普）-行车凭证',
        '车务-接发列车（普）-调度命令',
        '车务-接发列车（普）-非正常作业',
        '车务-接发列车（普）-防错办',
        '车务-接发列车（普）-车机联控',
        '车务-接发列车（普）-设备操作',
        '车务-接发列车（普）-发车条件确认',
        '车务-接发列车（普）-作业标准',
        '车务-行车专业管理（普）-规章管理',
    ]

    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('接发列车')]

        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_data = self_check_data[self_check_data['CHECK_ITEM_NAME'].isin(CHECK_ITEM_LIST)]
        self_check_problem_count = int(self_check_data['PROBLEM_NUMBER'].sum())
        self_check_cwjfc_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['出务接发车']]['PROBLEM_NUMBER'].sum())
        self_check_zyhk_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业互控']]['PROBLEM_NUMBER'].sum())
        self_check_xcpz_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['行车凭证']]['PROBLEM_NUMBER'].sum())
        self_check_ddml_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['调度命令']]['PROBLEM_NUMBER'].sum())
        self_check_fzczy_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['非正常作业']]['PROBLEM_NUMBER'].sum())
        self_check_fcb_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['防错办']]['PROBLEM_NUMBER'].sum())
        self_check_cjlk_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['车机联控']]['PROBLEM_NUMBER'].sum())
        self_check_sbcz_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['设备操作']]['PROBLEM_NUMBER'].sum())
        self_check_fctjqr_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['发车条件确认']]['PROBLEM_NUMBER'].sum())
        self_check_zybz_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业标准']]['PROBLEM_NUMBER'].sum())
        self_check_gzgl_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['规章管理']]['PROBLEM_NUMBER'].sum())

        self_check = {
            'problem_count': self_check_problem_count,
            'cwjfc_count': self_check_cwjfc_count,
            'zyhk_count': self_check_zyhk_count,
            'xcpz_count': self_check_xcpz_count,
            'ddml_count': self_check_ddml_count,
            'fzczy_count': self_check_fzczy_count,
            'fcb_count': self_check_fcb_count,
            'cjlk_count': self_check_cjlk_count,
            'sbcz_count': self_check_sbcz_count,
            'fctjqr_count': self_check_fctjqr_count,
            'zybz_count': self_check_zybz_count,
            'gzgl_count': self_check_gzgl_count
        }

        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_data = top_check_data[top_check_data['CHECK_ITEM_NAME'].isin(CHECK_ITEM_LIST)]
        top_check_problem_count = int(top_check_data['PROBLEM_NUMBER'].sum())
        top_check_cwjfc_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['出务接发车']]['PROBLEM_NUMBER'].sum())
        top_check_zyhk_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业互控']]['PROBLEM_NUMBER'].sum())
        top_check_xcpz_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['行车凭证']]['PROBLEM_NUMBER'].sum())
        top_check_ddml_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['调度命令']]['PROBLEM_NUMBER'].sum())
        top_check_fzczy_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['非正常作业']]['PROBLEM_NUMBER'].sum())
        top_check_fcb_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['防错办']]['PROBLEM_NUMBER'].sum())
        top_check_cjlk_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['车机联控']]['PROBLEM_NUMBER'].sum())
        top_check_sbcz_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['设备操作']]['PROBLEM_NUMBER'].sum())
        top_check_fctjqr_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['发车条件确认']]['PROBLEM_NUMBER'].sum())
        top_check_zybz_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['作业标准']]['PROBLEM_NUMBER'].sum())
        top_check_gzgl_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['规章管理']]['PROBLEM_NUMBER'].sum())

        top_check = {
            'problem_count': top_check_problem_count,
            'cwjfc_count': top_check_cwjfc_count,
            'zyhk_count': top_check_zyhk_count,
            'xcpz_count': top_check_xcpz_count,
            'ddml_count': top_check_ddml_count,
            'fzczy_count': top_check_fzczy_count,
            'fcb_count': top_check_fcb_count,
            'cjlk_count': top_check_cjlk_count,
            'sbcz_count': top_check_sbcz_count,
            'fctjqr_count': top_check_fctjqr_count,
            'zybz_count': top_check_zybz_count,
            'gzgl_count': top_check_gzgl_count
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }
        all_station.append(station)

    res = {'all_station': all_station}
    return res


def get_four_5(check_problem_data):
    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('接发列车')]

        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_level_a = int(self_check_data[self_check_data['LEVEL'] == 'A']['PROBLEM_NUMBER'].sum())
        self_check_level_b = int(self_check_data[self_check_data['LEVEL'] == 'B']['PROBLEM_NUMBER'].sum())
        self_check_level_c = int(self_check_data[self_check_data['LEVEL'] == 'C']['PROBLEM_NUMBER'].sum())
        self_check_level_d = int(self_check_data[self_check_data['LEVEL'] == 'D']['PROBLEM_NUMBER'].sum())
        self_check_level_f1 = int(self_check_data[self_check_data['LEVEL'] == 'F1']['PROBLEM_NUMBER'].sum())
        self_check_level_f2 = int(self_check_data[self_check_data['LEVEL'] == 'F2']['PROBLEM_NUMBER'].sum())
        self_check_level_f3 = int(self_check_data[self_check_data['LEVEL'] == 'F3']['PROBLEM_NUMBER'].sum())
        self_check_level_f4 = int(self_check_data[self_check_data['LEVEL'] == 'F4']['PROBLEM_NUMBER'].sum())
        self_check_level_e1 = int(self_check_data[self_check_data['LEVEL'] == 'E1']['PROBLEM_NUMBER'].sum())
        self_check_level_e2 = int(self_check_data[self_check_data['LEVEL'] == 'E2']['PROBLEM_NUMBER'].sum())
        self_check_level_e3 = int(self_check_data[self_check_data['LEVEL'] == 'E3']['PROBLEM_NUMBER'].sum())
        self_check_level_e4 = int(self_check_data[self_check_data['LEVEL'] == 'E4']['PROBLEM_NUMBER'].sum())

        self_check = {
            'level_a': self_check_level_a,
            'level_b': self_check_level_b,
            'level_c': self_check_level_c,
            'level_d': self_check_level_d,
            'level_f1': self_check_level_f1,
            'level_f2': self_check_level_f2,
            'level_f3': self_check_level_f3,
            'level_f4': self_check_level_f4,
            'level_e1': self_check_level_e1,
            'level_e2': self_check_level_e2,
            'level_e3': self_check_level_e3,
            'level_e4': self_check_level_e4
        }

        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_level_a = int(top_check_data[top_check_data['LEVEL'] == 'A']['PROBLEM_NUMBER'].sum())
        top_check_level_b = int(top_check_data[top_check_data['LEVEL'] == 'B']['PROBLEM_NUMBER'].sum())
        top_check_level_c = int(top_check_data[top_check_data['LEVEL'] == 'C']['PROBLEM_NUMBER'].sum())
        top_check_level_d = int(top_check_data[top_check_data['LEVEL'] == 'D']['PROBLEM_NUMBER'].sum())
        top_check_level_f1 = int(top_check_data[top_check_data['LEVEL'] == 'F1']['PROBLEM_NUMBER'].sum())
        top_check_level_f2 = int(top_check_data[top_check_data['LEVEL'] == 'F2']['PROBLEM_NUMBER'].sum())
        top_check_level_f3 = int(top_check_data[top_check_data['LEVEL'] == 'F3']['PROBLEM_NUMBER'].sum())
        top_check_level_f4 = int(top_check_data[top_check_data['LEVEL'] == 'F4']['PROBLEM_NUMBER'].sum())
        top_check_level_e1 = int(top_check_data[top_check_data['LEVEL'] == 'E1']['PROBLEM_NUMBER'].sum())
        top_check_level_e2 = int(top_check_data[top_check_data['LEVEL'] == 'E2']['PROBLEM_NUMBER'].sum())
        top_check_level_e3 = int(top_check_data[top_check_data['LEVEL'] == 'E3']['PROBLEM_NUMBER'].sum())
        top_check_level_e4 = int(top_check_data[top_check_data['LEVEL'] == 'E4']['PROBLEM_NUMBER'].sum())

        top_check = {
            'level_a': top_check_level_a,
            'level_b': top_check_level_b,
            'level_c': top_check_level_c,
            'level_d': top_check_level_d,
            'level_f1': top_check_level_f1,
            'level_f2': top_check_level_f2,
            'level_f3': top_check_level_f3,
            'level_f4': top_check_level_f4,
            'level_e1': top_check_level_e1,
            'level_e2': top_check_level_e2,
            'level_e3': top_check_level_e3,
            'level_e4': top_check_level_e4
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)
    res = {'all_station': all_station}
    return res


# 调车
def get_five():
    five_1 = get_five_1(local_data.check_info_data, local_data.check_info_last_month_data)
    five_2 = get_five_2(local_data.check_problem)
    five_3 = get_five_3(local_data.check_problem)
    five_4 = get_five_4(local_data.check_problem)
    five_5 = get_five_5(local_data.check_problem)

    return {
        'five_1': five_1,
        'five_2': five_2,
        'five_3': five_3,
        'five_4': five_4,
        'five_5': five_5,

    }


def get_five_1(check_info_data, check_info_last_month_data):
    if check_info_data['COST_TIME'].isnull().any():
        check_info_data['COST_TIME'].fillna(0, inplace=True)

    all_station = []
    for zhanduan in check_info_data['NAME'].unique():
        # 上月
        zhanduan_last_month_data = check_info_last_month_data[check_info_last_month_data['NAME'] == zhanduan]
        zhanduan_last_month_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        zhanduan_last_month_data = zhanduan_last_month_data[zhanduan_last_month_data['CHECK_ITEM_NAME'].str.contains('调车')]
        last_month_total_check_time = zhanduan_last_month_data.shape[0]
        # 当月
        zhanduan_data = check_info_data[check_info_data['NAME'] == zhanduan]
        zhanduan_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        zhanduan_data = zhanduan_data[zhanduan_data['CHECK_ITEM_NAME'].str.contains('调车')]
        # 检查次数
        total_check_time = zhanduan_data.shape[0]
        # 环比
        sequential = calc_ratio(total_check_time, last_month_total_check_time)['percent']
        # 现场检查
        xc_check_time = zhanduan_data[zhanduan_data['CHECK_WAY'] == 1].shape[0]
        # 添乘检查
        tc_check_time = zhanduan_data[zhanduan_data['CHECK_WAY'] == 2].shape[0]
        # 调阅检查
        dy_check_time = zhanduan_data[(zhanduan_data['CHECK_WAY'] == 3) | (zhanduan_data['CHECK_WAY'] == 4)].shape[0]
        # 调阅时长
        cost_time = round(zhanduan_data['COST_TIME'].sum(), 2)
        # 夜查
        yc_check_time = zhanduan_data[zhanduan_data['IS_YECHA'] == 1].shape[0]
        # 夜查率
        if yc_check_time == 0:
            yc_ratio = 0
        else:
            yc_ratio = round((yc_check_time / total_check_time) * 100, 1)

        station = {
            'station_name': zhanduan,
            'total_check_time': total_check_time,
            'sequential': sequential,
            'xc_check_time': xc_check_time,
            'tc_check_time': tc_check_time,
            'dy_check_time': dy_check_time,
            'cost_time': cost_time,
            'yc_check_time': yc_check_time,
            'yc_ratio': yc_ratio
        }

        all_station.append(station)
    all_station.sort(key=lambda x: x['yc_ratio'], reverse=True)
    res = {
        'all_station': all_station
    }
    return res


def get_five_2(check_problem_data):
    LEVEL = ['A', 'B', 'F1']
    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('调车')]
        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_problem_count = int(self_check_data['PROBLEM_NUMBER'].sum())
        self_check_high_level_problem_count = int(self_check_data[self_check_data['LEVEL'].isin(LEVEL)]['PROBLEM_NUMBER'].sum())
        if self_check_high_level_problem_count == 0:
            self_check_high_level_problem_ratio = 0
        else:
            self_check_high_level_problem_ratio = round((self_check_high_level_problem_count / self_check_problem_count) * 100, 1)
        self_check_ldaq_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('劳动安全')]['PROBLEM_NUMBER'].sum())
        self_check_tfzy_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('驼峰作业')]['PROBLEM_NUMBER'].sum())
        self_check_tsqddc_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('特殊区段调车')]['PROBLEM_NUMBER'].sum())
        self_check_dcjh_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('调车计划')]['PROBLEM_NUMBER'].sum())
        self_check_zybz_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('作业标准')]['PROBLEM_NUMBER'].sum())
        self_check_dcaqgl_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('调车安全管理')]['PROBLEM_NUMBER'].sum())
        self_check_dclk_count = int(self_check_data[self_check_data['CHECK_ITEM_NAME'].str.contains('调车联控')]['PROBLEM_NUMBER'].sum())

        self_check = {
            'problem_count': self_check_problem_count,
            'high_level_problem_count': self_check_high_level_problem_count,
            'high_level_problem_ratio': self_check_high_level_problem_ratio,
            'ldaq_count': self_check_ldaq_count,
            'tfzy_count': self_check_tfzy_count,
            'tsqddc_count': self_check_tsqddc_count,
            'dcjh_count': self_check_dcjh_count,
            'zybz_count': self_check_zybz_count,
            'dcaqgl_count': self_check_dcaqgl_count,
            'dclk_count': self_check_dclk_count
        }

        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_problem_count = int(top_check_data['PROBLEM_NUMBER'].sum())
        top_check_high_level_problem_count = int(top_check_data[top_check_data['LEVEL'].isin(LEVEL)][
                                                                   'PROBLEM_NUMBER'].sum())
        # top_check_xc_check_time = int(top_check_data[top_check_data['CHECK_WAY'] == 1].shape[0])
        if top_check_high_level_problem_count == 0:
            top_check_high_level_problem_ratio = 0
        else:
            top_check_high_level_problem_ratio = round((top_check_high_level_problem_count / top_check_problem_count) * 100, 1)
        top_check_ldaq_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('劳动安全')]['PROBLEM_NUMBER'].sum())
        top_check_tfzy_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('驼峰作业')]['PROBLEM_NUMBER'].sum())
        top_check_tsqddc_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('特殊区段调车')]['PROBLEM_NUMBER'].sum())
        top_check_dcjh_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('调车计划')]['PROBLEM_NUMBER'].sum())
        top_check_zybz_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('作业标准')]['PROBLEM_NUMBER'].sum())
        top_check_dcaqgl_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('调车安全管理')]['PROBLEM_NUMBER'].sum())
        top_check_dclk_count = int(top_check_data[top_check_data['CHECK_ITEM_NAME'].str.contains('调车联控')]['PROBLEM_NUMBER'].sum())

        top_check = {
            'problem_count': top_check_problem_count,
            'high_level_problem_count': top_check_high_level_problem_count,
            'high_level_problem_ratio': top_check_high_level_problem_ratio,
            'ldaq_count': top_check_ldaq_count,
            'tfzy_count': top_check_tfzy_count,
            'tsqddc_count': top_check_tsqddc_count,
            'dcjh_count': top_check_dcjh_count,
            'zybz_count': top_check_zybz_count,
            'dcaqgl_count': top_check_dcaqgl_count,
            'dclk_count': top_check_dclk_count
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)

    res = {'all_station': all_station}
    return res


def get_five_3(check_problem_data):
    CHECK_ITEM_DICT = {
        'gk': '车务-防溜-隔开设备使用及管理',
        'cs': '车务-防溜-防溜措施执行',
        'xs': '车务-防溜-巡视检查',
        'qj': '车务-防溜-防溜器具',
        'cx': '车务-防溜-岔线防溜',
        'kk': '车务-防溜-空客车底保留',
        'bl': '车务-防溜-保留车防溜'
    }
    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        #自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_gk = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['gk']]['PROBLEM_NUMBER'].sum())
        self_check_cs = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cs']]['PROBLEM_NUMBER'].sum())
        self_check_xs = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['xs']]['PROBLEM_NUMBER'].sum())
        self_check_qj = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['qj']]['PROBLEM_NUMBER'].sum())
        self_check_cx = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cx']]['PROBLEM_NUMBER'].sum())
        self_check_kk = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['kk']]['PROBLEM_NUMBER'].sum())
        self_check_bl = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['bl']]['PROBLEM_NUMBER'].sum())

        self_check = {
            'gk': self_check_gk,
            'cs': self_check_cs,
            'xs': self_check_xs,
            'qj': self_check_qj,
            'cx': self_check_cx,
            'kk': self_check_kk,
            'bl': self_check_bl
        }

        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_gk = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['gk']]['PROBLEM_NUMBER'].sum())
        top_check_cs = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cs']]['PROBLEM_NUMBER'].sum())
        top_check_xs = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['xs']]['PROBLEM_NUMBER'].sum())
        top_check_qj = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['qj']]['PROBLEM_NUMBER'].sum())
        top_check_cx = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cx']]['PROBLEM_NUMBER'].sum())
        top_check_kk = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['kk']]['PROBLEM_NUMBER'].sum())
        top_check_bl = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['bl']]['PROBLEM_NUMBER'].sum())

        top_check = {
            'gk': top_check_gk,
            'cs': top_check_cs,
            'xs': top_check_xs,
            'qj': top_check_qj,
            'cx': top_check_cx,
            'kk': top_check_kk,
            'bl': top_check_bl
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)
    res = {'all_station': all_station}
    return res


def get_five_4(check_problem_data):
    CHECK_ITEM_DICT = {
        'cw': '车务-调乘一体化-乘务标准',
        'ct': '车务-调乘一体化-出退勤',
        'zl': '车务-调乘一体化-机车质量',
        'yy': '车务-调乘一体化-机车运用',
        'dc': '车务-调乘一体化-待乘',
        'zb': '车务-调乘一体化-行安装备',
        'sb': '车务-调乘一体化-设备管理',
        'dg': '车务-调乘一体化-调乘管理',
        'dk': '车务-调乘一体化-调乘互控',
    }

    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        #自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_cw = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cw']]['PROBLEM_NUMBER'].sum())
        self_check_ct = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['ct']]['PROBLEM_NUMBER'].sum())
        self_check_zl = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['zl']]['PROBLEM_NUMBER'].sum())
        self_check_yy = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['yy']]['PROBLEM_NUMBER'].sum())
        self_check_dc = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dc']]['PROBLEM_NUMBER'].sum())
        self_check_zb = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['zb']]['PROBLEM_NUMBER'].sum())
        self_check_sb = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['sb']]['PROBLEM_NUMBER'].sum())
        self_check_dg = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dg']]['PROBLEM_NUMBER'].sum())
        self_check_dk = int(self_check_data[self_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dk']]['PROBLEM_NUMBER'].sum())

        self_check = {
            'cw': self_check_cw,
            'ct': self_check_ct,
            'zl': self_check_zl,
            'yy': self_check_yy,
            'dc': self_check_dc,
            'zb': self_check_zb,
            'sb': self_check_sb,
            'dg': self_check_dg,
            'dk': self_check_dk
        }

        #上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_cw = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['cw']]['PROBLEM_NUMBER'].sum())
        top_check_ct = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['ct']]['PROBLEM_NUMBER'].sum())
        top_check_zl = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['zl']]['PROBLEM_NUMBER'].sum())
        top_check_yy = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['yy']]['PROBLEM_NUMBER'].sum())
        top_check_dc = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dc']]['PROBLEM_NUMBER'].sum())
        top_check_zb = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['zb']]['PROBLEM_NUMBER'].sum())
        top_check_sb = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['sb']]['PROBLEM_NUMBER'].sum())
        top_check_dg = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dg']]['PROBLEM_NUMBER'].sum())
        top_check_dk = int(top_check_data[top_check_data['CHECK_ITEM_NAME'] == CHECK_ITEM_DICT['dk']]['PROBLEM_NUMBER'].sum())

        top_check = {
            'cw': top_check_cw,
            'ct': top_check_ct,
            'zl': top_check_zl,
            'yy': top_check_yy,
            'dc': top_check_dc,
            'zb': top_check_zb,
            'sb': top_check_sb,
            'dg': top_check_dg,
            'dk': top_check_dk
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)
    res = {'all_station': all_station}
    return res


def get_five_5(check_problem_data):
    all_station = []
    for station_name in check_problem_data['NAME'].unique():
        station_data = check_problem_data[check_problem_data['NAME'] == station_name]
        station_data['CHECK_ITEM_NAME'].fillna(' ', inplace=True)
        station_data = station_data[station_data['CHECK_ITEM_NAME'].str.contains('调车')]
        # 自查
        self_check_data = station_data[station_data['TYPE'].isin([2, 3])]
        self_check_level_a = int(self_check_data[self_check_data['LEVEL'] == 'A']['PROBLEM_NUMBER'].sum())
        self_check_level_b = int(self_check_data[self_check_data['LEVEL'] == 'B']['PROBLEM_NUMBER'].sum())
        self_check_level_c = int(self_check_data[self_check_data['LEVEL'] == 'C']['PROBLEM_NUMBER'].sum())
        self_check_level_d = int(self_check_data[self_check_data['LEVEL'] == 'D']['PROBLEM_NUMBER'].sum())
        self_check_level_f1 = int(self_check_data[self_check_data['LEVEL'] == 'F1']['PROBLEM_NUMBER'].sum())
        self_check_level_f2 = int(self_check_data[self_check_data['LEVEL'] == 'F2']['PROBLEM_NUMBER'].sum())
        self_check_level_f3 = int(self_check_data[self_check_data['LEVEL'] == 'F3']['PROBLEM_NUMBER'].sum())
        self_check_level_f4 = int(self_check_data[self_check_data['LEVEL'] == 'F4']['PROBLEM_NUMBER'].sum())
        self_check_level_e1 = int(self_check_data[self_check_data['LEVEL'] == 'E1']['PROBLEM_NUMBER'].sum())
        self_check_level_e2 = int(self_check_data[self_check_data['LEVEL'] == 'E2']['PROBLEM_NUMBER'].sum())
        self_check_level_e3 = int(self_check_data[self_check_data['LEVEL'] == 'E3']['PROBLEM_NUMBER'].sum())
        self_check_level_e4 = int(self_check_data[self_check_data['LEVEL'] == 'E4']['PROBLEM_NUMBER'].sum())

        self_check = {
            'level_a': self_check_level_a,
            'level_b': self_check_level_b,
            'level_c': self_check_level_c,
            'level_d': self_check_level_d,
            'level_f1': self_check_level_f1,
            'level_f2': self_check_level_f2,
            'level_f3': self_check_level_f3,
            'level_f4': self_check_level_f4,
            'level_e1': self_check_level_e1,
            'level_e2': self_check_level_e2,
            'level_e3': self_check_level_e3,
            'level_e4': self_check_level_e4,
        }

        # 上级检查
        top_check_data = station_data[station_data['TYPE'].isin([1, 4])]
        top_check_level_a = int(top_check_data[top_check_data['LEVEL'] == 'A']['PROBLEM_NUMBER'].sum())
        top_check_level_b = int(top_check_data[top_check_data['LEVEL'] == 'B']['PROBLEM_NUMBER'].sum())
        top_check_level_c = int(top_check_data[top_check_data['LEVEL'] == 'C']['PROBLEM_NUMBER'].sum())
        top_check_level_d = int(top_check_data[top_check_data['LEVEL'] == 'D']['PROBLEM_NUMBER'].sum())
        top_check_level_f1 = int(top_check_data[top_check_data['LEVEL'] == 'F1']['PROBLEM_NUMBER'].sum())
        top_check_level_f2 = int(top_check_data[top_check_data['LEVEL'] == 'F2']['PROBLEM_NUMBER'].sum())
        top_check_level_f3 = int(top_check_data[top_check_data['LEVEL'] == 'F3']['PROBLEM_NUMBER'].sum())
        top_check_level_f4 = int(top_check_data[top_check_data['LEVEL'] == 'F4']['PROBLEM_NUMBER'].sum())
        top_check_level_e1 = int(top_check_data[top_check_data['LEVEL'] == 'E1']['PROBLEM_NUMBER'].sum())
        top_check_level_e2 = int(top_check_data[top_check_data['LEVEL'] == 'E2']['PROBLEM_NUMBER'].sum())
        top_check_level_e3 = int(top_check_data[top_check_data['LEVEL'] == 'E3']['PROBLEM_NUMBER'].sum())
        top_check_level_e4 = int(top_check_data[top_check_data['LEVEL'] == 'E4']['PROBLEM_NUMBER'].sum())

        top_check = {
            'level_a': top_check_level_a,
            'level_b': top_check_level_b,
            'level_c': top_check_level_c,
            'level_d': top_check_level_d,
            'level_f1': top_check_level_f1,
            'level_f2': top_check_level_f2,
            'level_f3': top_check_level_f3,
            'level_f4': top_check_level_f4,
            'level_e1': top_check_level_e1,
            'level_e2': top_check_level_e2,
            'level_e3': top_check_level_e3,
            'level_e4': top_check_level_e4,
        }

        station = {
            'name': station_name,
            'self_check': self_check,
            'top_check': top_check
        }

        all_station.append(station)
    res = {'all_station': all_station}
    return res
