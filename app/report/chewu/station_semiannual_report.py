#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   month_report_cheliang
Description:
Author:    yuchuang
date:         2019-07-23
-------------------------------------------------
Change Activity:2019-07-23 14:54
-------------------------------------------------
"""

from app import mongo
import app.report.analysis_report_manager as manager
from app.report.chewu.station_semiannual_report_data import get_data


class ChewuStationSemiannualAnalysisReport(manager.SemiannualAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self, station_id=None):
        super(ChewuStationSemiannualAnalysisReport, self).__init__(hierarchy_type='STATION', major='车务',
                                                                   station_id=station_id)

    def generate_report_data(self, year, half):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param half: int 月
        :return:
        """
        data = get_data(year, half, self.station_id)
        mongo.db['safety_analysis_semiannual_report'].delete_one(
            {
                "year": year,
                "half": half,
                "hierarchy": "STATION",
                "station_id": self.station_id,
                "major": self.major
            })
        mongo.db['safety_analysis_semiannual_report'].insert_one(data)
        return data
