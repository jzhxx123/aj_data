# 履职评价信息查询语句
check_evaluate_sql = """SELECT a.*,b.ITEM_NAME,b.SITUATION,c.BUSINESS_CLASSIFY,c.NAME,c.ALL_NAME,
e.JOB,e.PERSON_NAME,e.IDENTITY,f.NAME AS STATION,g.ALL_NAME AS NAMES,h.PROBLEM_NUMBER
FROM t_check_evaluate_info AS a
LEFT JOIN t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
LEFT JOIN t_check_info h on a.FK_CHECK_OR_PROBLEM_ID = h.PK_ID
LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
LEFT JOIN t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
LEFT JOIN t_department g on a.CHECK_PERSON_DEPARTMENT_ID=g.DEPARTMENT_ID
WHERE CREATE_TIME >= '{0}' AND CREATE_TIME <= '{1}'
AND  c.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
"""

station_carde_count_sql = """SELECT
        b.ALL_NAME,
        c.BELONG_PROFESSION_NAME AS MAJOR,
        c.`NAME` AS STATION
    FROM
        t_person AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = b.TYPE3
        LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = b.TYPE4
    WHERE
         b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC' 
"""

new_check_info_sql = """SELECT
        a.ID_CARD,
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS ALL_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER,
        b.COST_TIME,
        e.`NAME`
    FROM
        t_check_info AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        LEFT JOIN t_check_info_and_person c on b.FK_CHECK_INFO_ID=c.FK_CHECK_INFO_ID
        LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
        LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
    WHERE
        a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
        AND d.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


new_check_problem_sql = """SELECT
        b.CHECK_WAY,
        a.PROBLEM_CLASSITY_NAME,
        a.LEVEL,
        a.PROBLEM_SCORE,
        b.DEPARTMENT_ALL_NAME AS ALL_NAME,
        d.`NAME`,
        a.ID_CARD
    FROM
        t_check_problem AS a
        LEFT JOIN t_check_info AS b ON b.PK_ID = a.FK_CHECK_INFO_ID
        LEFT JOIN t_department c on a.EXECUTE_DEPARTMENT_ID=c.DEPARTMENT_ID
        LEFT JOIN t_department d on d.DEPARTMENT_ID = c.TYPE3
    WHERE
        b.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND c.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
"""

check_manager_info_sql = """SELECT a.CHECK_WAY,f.ALL_NAME,e.`LEVEL`,a.IS_YECHA,a.ID_CARD
FROM t_check_info a
LEFT JOIN t_check_info_and_person b on a.PK_ID=b.FK_CHECK_INFO_ID
LEFT JOIN t_person e on b.ID_CARD=e.ID_CARD
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID=c.DEPARTMENT_ID
LEFT JOIN t_department f on c.TYPE4=f.DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND c.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


check_mamager_problem_sql = """SELECT b.PROBLEM_SCORE,d.ALL_NAME,b.`LEVEL`,b.IS_ASSESS,e.NAME
FROM t_check_info a
LEFT JOIN t_check_problem b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department c on b.EXECUTE_DEPARTMENT_ID=c.DEPARTMENT_ID
LEFT JOIN t_department d on c.TYPE4=d.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3 
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND c.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


# 问题整改+风险
zg_sql = """SELECT a.RISK_LEVEL,e.RECTIFY_DATE,b.`STATUS`,b.RECTIFY_TIME,d.`NAME`,a.PROBLEM_POINT
FROM t_problem_base a
LEFT JOIN t_check_problem e on a.PK_ID=e.FK_PROBLEM_BASE_ID
LEFT JOIN t_check_problem_and_responsible_department b on e.PK_ID=b.FK_CHECK_PROBLEM_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
WHERE e.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND c.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""

# 返奖金额
return_money_sql = """SELECT c.MONEY,e.`NAME`
FROM t_check_problem a
LEFT JOIN t_safety_award_responsible_return_detail c on a.PK_ID=c.FK_CHECK_PROBLEM_ID
LEFT JOIN t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
where a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
and d.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""

# 考核金额
assess_money_sql = """SELECT c.ASSESS_MONEY,e.`NAME`,a.LEVEL,c.RESPONSIBILITY_LEVEL,c.PERSON_NAME
FROM t_check_problem a
LEFT JOIN t_check_problem_and_responsibility_person c on a.PK_ID=c.FK_CHECK_PROBLEM_ID
LEFT JOIN t_check_problem_and_responsible_department b on c.FK_RESPONSIBLE_DEPARTMENT_ID=b.PK_ID
LEFT JOIN t_department d on b.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
where a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
and d.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


# 职工人数
person_sql = """SELECT count(1) AS NUMBER,c.`NAME` FROM t_person a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID=b.DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
WHERE b.TYPE3 in (SELECT DEPARTMENT_ID FROM t_department WHERE BELONG_PROFESSION_ID=898)
GROUP BY c.`NAME`"""

# 音视频检查
mv_check_sql = """SELECT a.CHECK_WAY,c.TYPE,a.PROBLEM_NUMBER,d.NAME,c.COST_TIME,e.ALL_NAME
FROM t_check_info a
RIGHT JOIN t_check_info_and_media c on a.PK_ID=c.FK_CHECK_INFO_ID
LEFT JOIN t_person b on b.ID_CARD=a.ID_CARD
LEFT JOIN t_department e on e.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=e.TYPE3
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND e.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


# 安全科人数
safety_person_sql = """SELECT count(1) AS NUMBER,b.ALL_NAME 
FROM t_person a
LEFT JOIN t_department b on a.FK_DEPARTMENT_ID=b.DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
WHERE b.TYPE3 in (SELECT DEPARTMENT_ID FROM t_department WHERE BELONG_PROFESSION_ID=898)
AND b.ALL_NAME like '%%安全科'
GROUP BY b.ALL_NAME """