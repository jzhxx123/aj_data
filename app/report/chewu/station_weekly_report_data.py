import threading
from dateutil.relativedelta import relativedelta
from datetime import datetime as dt
import datetime
import calendar
from app.data.util import pd_query
from app.report.analysis_report_manager import WeeklyAnalysisReport
from app.report.chewu.station_weekly_report_sql import *

local_data = threading.local()


def calc_ratio_ring(num1, num2):
    if num2 == 0:
        return {
            'diff': num1,
            'percent': round(num1 * 100, 1)
        }
    else:
        return {
            'diff': num1 - num2,
            'percent': round((num1 - num2) / num2 * 100, 1)
        }


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(start_date, end_date, dp_id):
    """
    获周报数据
    :param dp_id: 站段的ID
    :param start_date: {str} 开始时间, 格式 %Y-%m-%d, 2019-01-01
    :param end_date: {str} 结束时间, 格式 %Y-%m-%d, 2019-01-01
    :return:
    """
    global DEPARTMENT_DATA
    DEPARTMENT_DATA = pd_query(DEPARTMENT_SQL)
    DEPARTMENT_DATA = DEPARTMENT_DATA[DEPARTMENT_DATA['TYPE2'] == _get_major_dpid('车务')]
    date_params = WeeklyAnalysisReport.get_week_intervals(start_date, end_date)
    local_data.date_params = date_params
    last_week_start = local_data.date_params[1][0]
    last_week_end = local_data.date_params[1][1]
    year, month = str(dt.strptime(end_date, "%Y-%m-%d").year), str(dt.strptime(end_date, "%Y-%m-%d").month)
    start_month, start_day =  str(dt.strptime(start_date, "%Y-%m-%d").month), str(dt.strptime(start_date, "%Y-%m-%d").day)
    end_month, end_day = month, str(dt.strptime(end_date, "%Y-%m-%d").day)
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = pd_query("""select ALL_NAME from t_department where DEPARTMENT_ID = '{0}'""".format(dp_id)).iloc[0][
        'ALL_NAME']
    # 无责任事故信息
    without_accident = pd_query(without_accident_sql.format(dp_id))
    # 检查问题信息
    check_data = pd_query(CHECK_INFP_AND_PROBLEM_SQL.format(start_date, end_date, dp_id))
    last_check_data = pd_query(CHECK_INFP_AND_PROBLEM_SQL.format(last_week_start, last_week_end, dp_id))
    # 履职评价信息
    evaluate_data = pd_query(EVALUATE_INFO_SQL.format(start_date, end_date, dp_id))
    # 履职评价复查信息
    evaluate_review_data = pd_query(EVALUATE_REVIEW_SQL.format(start_date, end_date, dp_id))
    # 内容CONTENT
    first = get_first(without_accident)
    fourth = get_fourth(evaluate_data, evaluate_review_data)
    third = get_third(start_date, end_date, dp_id)
    fourth_table = get_fourth_table(evaluate_data, evaluate_review_data)
    five = get_five(check_data, last_check_data)
    five_one = get_five_one(check_data, last_check_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        "hierarchy": "STATION",
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        'major': '车务',
        'station_id': dp_id,
        'station_name': station_name,
        'created_at': dt.now(),
        'file_name': file_name,
        'first': first,
        'third': third,
        'fourth': fourth,
        'fourth_table': fourth_table,
        'five': five,
        'five_one': five_one
    }
    return result


def get_first(without_accident):
    """
    头部信息
    :param without_accident:
    :return:
    """
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    all_list = []
    # ABCD类无责任事故
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
        if len(accident) == 0:
            acc_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            acc_time = \
                str(dt.now().date() - accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
        all_list.append(acc_time)
    return {
        'all_list': all_list
    }


def get_third(start_date, end_date, dp_id):
    """
    综合指数情况
    """
    import pandas as pd
    # 获取截止日期
    end_year,end_month, end_day = dt.strptime(end_date, '%Y-%m-%d').year,\
                                  dt.strptime(end_date, '%Y-%m-%d').month, \
                                  dt.strptime(end_date, '%Y-%m-%d').day
    # 该周（以截止日期为准）所处月份
    real_start_month = dt.strptime(end_date, '%Y-%m-%d')
    # 总天数
    total_days = 0
    # 目前进行天数
    days_hasgone = 0
    if int(end_day) >= 25:
        days_hasgone = int(end_day) - 24
        total_days = int(
            calendar.monthrange(
                dt.strptime(end_date, '%Y-%m-%d').year, end_month
                )[1])
    else:
        real_start_month = dt.strptime(end_date, '%Y-%m-%d') - datetime.timedelta(
                           days=calendar.monthrange(
            dt.strptime(end_date, '%Y-%m-%d').year, end_month
        )[1])
        days_hasgone = int(calendar.monthrange(real_start_month.year, real_start_month.month)[1]) - 24 + int(end_day)
        total_days = int(calendar.monthrange(real_start_month.year, real_start_month.month)[1])
    real_start_month = str(real_start_month.replace(day=25))[:10]
    # 由于综合指数时间区间为前闭后开（上个月25（包括25号）到这个月的25号（不包括25号）
    real_end_date = dt.strptime(end_date, '%Y-%m-%d') + datetime.timedelta(
                           days=1)
    real_end_date = str(real_end_date)[:10]
    # 综合指数
    result = {
        "end_month": str(end_month),
        "end_day": str(end_day),
        "total_days": total_days,
        "days_hasgone": days_hasgone,
        # 检查力度指数
        "check_count_per_person": _get_fraction_values(
            pd_query(CHECK_COUNT_SQL.format(real_start_month, real_end_date)),
            pd.concat(
                [pd_query(WORK_LOAD_SQL),
                 pd_query(EXTERNAL_PERSON_SQL.format(int(end_month))).rename(
                     columns={'TYPE3': "FK_DEPARTMENT_ID"})
                 ], axis=0),
            dp_id),
        "check_score_per_problem": _get_fraction_values(
            pd_query(PROBLEM_CHECK_SCORE_SQL.format(real_start_month, real_end_date)),
            pd_query(CHECK_PROBLEM_SQL.format(real_start_month,real_end_date)),
            dp_id),
        "check_score_per_person": _get_fraction_values(
            pd_query(PROBLEM_CHECK_SCORE_SQL.format(real_start_month, real_end_date)),
            pd.concat(
                [pd_query(WORK_LOAD_SQL),
                 pd_query(EXTERNAL_PERSON_SQL.format(int(end_month))).rename(
                     columns={'TYPE3': "FK_DEPARTMENT_ID"})
                 ], axis=0),
            dp_id),
        "media_monitor_intensity": _get_media_monitor_intensity_info(
            pd_query(MEDIA_COST_TIME_SQL.format(real_start_month, real_end_date)),
            pd_query(MEDIA_PROBLEM_NUMBER_SQL.format(real_start_month, real_end_date)),
            dp_id),
        "risk_problem_per_person": _get_fraction_values(
            pd_query(JIAODA_RISK_LEVEL_PROBLEM_SQL.format(real_start_month, real_end_date)),
            pd.concat(
                [pd_query(WORK_LOAD_SQL),
                 pd_query(EXTERNAL_PERSON_SQL.format(int(end_month))).rename(
                     columns={'TYPE3': "FK_DEPARTMENT_ID"})
                 ], axis=0),
            dp_id),
        "check_addr_ratio": _get_fraction_values(
            pd_query(REAL_CHECK_BANZU_SQL.format(real_start_month, real_end_date)).append(
                pd_query(REAL_CHECK_POINT_SQL.format(real_start_month, real_end_date))
            ),
            pd_query(BANZU_POINT_SQL.format(real_start_month, real_end_date)).append(
                pd_query(CHECK_POINT_SQL.format(real_start_month, real_end_date))
            ),
            dp_id),
        "yecha_per_check": _get_fraction_values(
            pd_query(YECHA_CHECK_SQL.format(real_start_month, real_end_date)),
            pd_query(CHECK_COUNT_SQL.format(real_start_month, real_end_date)),
            dp_id),
        # 评价力度指数
        "evaluate_count_per_total": _get_fraction_values(
            pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            pd_query(EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            dp_id),
        "evaluate_score_per_cadre": _get_fraction_values(
            pd_query(ACTIVE_EVALUATE_SCORE_SQL.format(real_start_month, real_end_date)),
            pd_query(CADRE_COUNT_SQL),
            dp_id),
        "station_evaluate_count_per_total": _get_fraction_values(
            pd_query(DUAN_CADRE_COUNT_SQL.format(real_start_month, real_end_date)),
            pd_query(EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            dp_id),
        "evaluate_count_per_cadre": _get_fraction_values(
            pd_query(ACTIVE_EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            pd_query(CADRE_COUNT_SQL),
            dp_id),
        "gradation_ratio": _get_fraction_values(
            pd_query(ACTIVE_KEZHI_EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            pd_query(EVALUATE_COUNT_SQL.format(real_start_month, real_end_date)),
            dp_id),
        # 考核力度指数
        "assess_money_per_person": _get_fraction_values(
            pd.concat(
                [
                    pd_query(each_sql.format(int(end_year), int(end_month))) for each_sql in [
                    ASSESS_REVISE_SQL, ASSESS_RROBLEM_SQL, ASSESS_QUANTIFY_SQL,
                    ASSESS_RESPONSIBLE_SQL
                ]
                ],
                axis=0),
            pd_query(WORK_LOAD_SQL),
            dp_id),
        "check_problem_assess_radio": _get_fraction_values(
            pd_query(KAOHE_PROBLEM_SQL.format(real_start_month, real_end_date)),
            pd_query(WORK_LOAD_SQL),
            dp_id),
        # 检查均衡度指数
        "problem_point_evenness": _get_fraction_values(
            pd_query(GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL.format(real_start_month, real_end_date)),
            pd_query(GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL),
            dp_id),
        # 问题整改效果指数
        "rectification_review": _get_fraction_values(
            pd_query(IMPORTANT_PROBLEM_RECHECK_COUNT_SQL.format(int(end_year), int(end_month))),
            pd.concat(
                [pd_query(WORK_LOAD_SQL),
                 pd_query(EXTERNAL_PERSON_SQL.format(int(end_month))).rename(
                     columns={'TYPE3': "FK_DEPARTMENT_ID"})
                 ], axis=0),
            dp_id),
    }
    return result


def get_fourth(evaluate_data, evaluate_review_data):
    # 顶部文字
    top = []
    count = len(evaluate_data)
    top.append(count)
    # 检查信息问题逐条评价
    for i in [1, 2]:
        data = evaluate_data[(evaluate_data['EVALUATE_WAY'] == 1) & (evaluate_data['EVALUATE_TYPE'] == i)]
        top.append(len(data))
    # 监控调阅评价
    top.append(len(evaluate_data[(evaluate_data['EVALUATE_WAY'] == 1) & (evaluate_data['CHECK_WAY'] == 3)]))
    # 定期
    top.append(len(evaluate_data[evaluate_data['EVALUATE_WAY'] == 2]))
    # 复查检查信息问题
    for i in [1, 2]:
        data = evaluate_review_data[
            (evaluate_review_data['EVALUATE_WAY'] == 1) & (evaluate_review_data['EVALUATE_TYPE'] == i)]
        top.append(len(data))
    # 复查监控调阅
    top.append(len(
        evaluate_review_data[(evaluate_review_data['EVALUATE_WAY'] == 1) & (evaluate_review_data['CHECK_WAY'] == 3)]))
    # 复查定期评价
    top.append(len(evaluate_review_data[evaluate_review_data['EVALUATE_WAY'] == 2]))
    # 管理人员检查次数
    person_data = evaluate_data[evaluate_data['GRADATION'].str.contains('管理')]
    person_count = len(person_data)
    person_num = len(person_data['RESPONSIBE_ID_CARD'].value_counts())
    top.append(person_count)
    top.append(person_num)
    return {
        'count': count,
        'top': top
    }


def get_fourth_table(evaluate_data, evaluate_review_data):
    table1 = get_table1(evaluate_data, evaluate_review_data)
    table2 = get_table2(evaluate_data)
    table3 = get_table3(evaluate_data)
    return {
        'table1': table1,
        'table2': table2,
        'table3': table3
    }


def get_table1(data, review_data):
    all_list = []
    for i in ['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        new_data = data[data['GRADATION'] == i]
        re = review_data[review_data['GRADATION'] == i]
        one = []
        # 检查问题逐条评价（条）
        zt_data = new_data[(new_data['EVALUATE_WAY'] == 1) & (new_data['EVALUATE_TYPE'].isin([1, 2]))]
        # 定期评价（人）
        dq_data = new_data[new_data['CHECK_WAY'] == 2]
        one.append(len(zt_data))
        one.append(len(dq_data))
        # 复查检查信息、问题评价（条）
        for j in [1, 2]:
            news_data = re[(re['EVALUATE_WAY'] == 1) & (re['EVALUATE_TYPE'] == j)]
            one.append(len(news_data))
        # 复查定期评价（人）
        dq_review = len(re[re['EVALUATE_WAY'] == 2])
        one.append(dq_review)
        all_list.append(one)
    return {
        'all_list': all_list
    }


def get_table2(evaluate_data):
    """
    评价人员分析
    :param evaluate_data:
    :return:
    """
    data = evaluate_data.groupby('RESPONSIBE_PERSON_NAME').sum().sort_values(by='SCORE').reset_index()
    # 被评价人数
    person_num = len(data)
    # 被评价人信息
    person_list = []
    for i, k in data.iterrows():
        dic = {
            'name': k['RESPONSIBE_PERSON_NAME'],
            'score': float('%.4f' % k['SCORE'])
        }
        person_list.append(dic)
    return {
        'person_num': person_num,
        'person_list': person_list
    }


def get_table3(evaluate_data):
    """
    评价项目分析
    :param evaluate_data:
    :return:
    """
    # 项目个数
    project = len(evaluate_data['ITEM_NAME'].unique())
    # 评价次数
    count = len(evaluate_data)
    # 项目名称
    project_name = list(evaluate_data['ITEM_NAME'].unique())
    project_list = []
    for i in project_name:
        data = evaluate_data[evaluate_data['ITEM_NAME'] == i]
        content = [i.strip().replace('。', ',') for i in data['SITUATION'].tolist()]
        dic = {
            'name': i,
            'content': ''.join(content),
            'score': int(data['SCORE'].sum()),
            'ratio': round(ded_zero(len(data), count) * 100, 1)
        }
        project_list.append(dic)
    return {
        'project': project,
        'count': count,
        'project_name': project_name,
        'project_list': project_list
    }


def get_five(check_data, last_check_data):
    """
    监督检查情况
    :param check_data:
    :param last_check_data:
    :return:
    """
    all_list = []
    # 检查信息数
    info = len(check_data)
    last_info = len(last_check_data)
    info_ratio = calc_ratio_ring(info, last_info)
    all_list.append([info, last_info, info_ratio])
    # 监控设备调阅及现场检查
    for i in [1, 3]:
        shebei = len(check_data[check_data['CHECK_WAY'] == i])
        last_shebei = len(check_data[check_data['CHECK_WAY'] == i])
        shebei_ratio = calc_ratio_ring(shebei, last_shebei)
        all_list.append([shebei, last_shebei, shebei_ratio])
    # 发现问题数
    find_problem = int(check_data['PROBLEM_NUMBER'].sum())
    last_find_problem = int(check_data['PROBLEM_NUMBER'].sum())
    find_ratio = calc_ratio_ring(find_problem, last_find_problem)
    all_list.append([find_problem, last_find_problem, find_ratio])
    # 作业项问题数
    work = int(check_data[check_data['PROBLEM_CLASSITY_NAME'].str.contains('作业')]['PROBLEM_NUMBER'].sum())
    last_work = int(
        last_check_data[last_check_data['PROBLEM_CLASSITY_NAME'].str.contains('作业')]['PROBLEM_NUMBER'].sum())
    work_ratio = calc_ratio_ring(work, last_work)
    all_list.append([work, last_work, work_ratio])
    # 较大及以上风险问题数
    risk = int(check_data[check_data['RISK_LEVEL'].isin([1, 2])]['PROBLEM_NUMBER'].sum())
    last_risk = int(last_check_data[last_check_data['RISK_LEVEL'].isin([1, 2])]['PROBLEM_NUMBER'].sum())
    risk_ratio = calc_ratio_ring(risk, last_risk)
    all_list.append([risk, last_risk, risk_ratio])
    return {
        'all_list': all_list
    }


def get_five_one(check_data, last_check_data):
    """
    安全问题风险分析
    :param check_data:
    :param last_check_data:
    :return:
    """
    # 安全问题
    problem_num = int(check_data['PROBLEM_NUMBER'].sum())
    problem_list = []
    sum = 0
    last_sum = 0
    for i in ['作业', '管理', '反恐防暴', '路外', '设备']:
        data = check_data[check_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        last_data = last_check_data[last_check_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        dic = {
            'name': i,
            'count': len(data),
            'last_count': len(last_data),
            'ratio': round(ded_zero(len(data), problem_num) * 100, 1),
            'ring': calc_ratio_ring(len(data), len(last_data))
        }
        sum += len(data)
        last_sum += len(last_data)
        problem_list.append(dic)
    sum_ratio = calc_ratio_ring(sum, last_sum)
    # 主要风险问题
    main_list = []
    for i in ['行车', '调车', '接发列车', '防溜', '施工', '货装', '货装作业', '货装劳安', '货装设备', '货装专业管理',
              '客运', '客运生产', '客运专业管理', '其他']:
        count = len(data[data['ALL_NAME'].str.contains(i)])
        ratio = round(ded_zero(count, problem_num) * 100, 1)
        main_list.append([count, ratio])
    return {
        'problem_num': problem_num,
        'problem_list': problem_list,
        'main_list': main_list,
        'sum': sum,
        'last_sum': last_sum,
        'sum_ratio': sum_ratio
    }

__FRACTION_USED_COUNT = 0


def _get_fraction_values(numerator, denominator, dpid):
    """
    获取分子分母型，表达式各种值
    """
    global __FRACTION_USED_COUNT
    __FRACTION_USED_COUNT += 1
    print("第%d次调用" % __FRACTION_USED_COUNT)
    import pandas as pd
    numerator = pd.merge(
        numerator,
        DEPARTMENT_DATA,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    denominator = pd.merge(
        denominator,
        DEPARTMENT_DATA,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    numerator = numerator[numerator['TYPE3'] == dpid]
    denominator = denominator[denominator['TYPE3'] == dpid]
    return {
        'numerator': round(float(numerator['COUNT'].sum()), 3),
        'denominator': round(float(denominator['COUNT'].sum()), 3),
        'value': _divide_nozero(
            round(float(numerator['COUNT'].sum()), 3),
            round(float(denominator['COUNT'].sum()), 3)
        )
    }


def _get_major_dpid(major):
    major_dpid = {
        "供电": "1ACE7D1C80B04456E0539106C00A2E70KSC",
        "车辆": "1ACE7D1C80B44456E0539106C00A2E70KSC",
        "机务": "1ACE7D1C80B14456E0539106C00A2E70KSC",
        "车务": "1ACE7D1C80B24456E0539106C00A2E70KSC",
        "工务": "1ACE7D1C80AF4456E0539106C00A2E70KSC",
        "电务": "1ACE7D1C80B34456E0539106C00A2E70KSC",
    }
    return major_dpid.get(major, None)


def _divide_nozero(a, b):
    """
    除法，分母为0，默认给0
    """
    return 0 if b == 0 else round(a/b, 3)


def _get_media_monitor_intensity_info(media_cost_time, media_check_problem, dpid):
    """
    获取监控调阅力度相关统计信息
    """
    import pandas as pd
    # 调阅时间
    station_cost_time = pd.merge(
        media_cost_time,
        DEPARTMENT_DATA,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    stations_number = len(set(station_cost_time['TYPE3'].values.tolist()))
    major_cost_time = round(float(station_cost_time['TIME'].sum()), 3)
    station_cost_time = station_cost_time[
        station_cost_time['TYPE3'] == dpid
    ]
    station_cost_time = round(float(station_cost_time['TIME'].sum()), 3)
    cost_time_proportion = abs(
        (station_cost_time - (major_cost_time/stations_number)) / (major_cost_time/stations_number)
    )*100

    # 调阅问题
    station_media_problem = pd.merge(
        media_check_problem,
        DEPARTMENT_DATA,
        left_on='FK_DEPARTMENT_ID',
        right_on='DEPARTMENT_ID',
        how='inner'
    )
    stations_number = len(set(station_media_problem['TYPE3'].values.tolist()))
    major_media_problem = round(float(station_media_problem['NUMBER'].sum()), 3)
    station_media_problem = station_media_problem[
        station_media_problem['TYPE3'] == dpid
        ]
    station_media_problem = round(float(station_media_problem['NUMBER'].sum()), 3)
    media_problem_proportion = abs(
        (station_media_problem - (major_media_problem / stations_number)) / (major_media_problem / stations_number)
    )*100
    return {
        'station_cost_time': station_cost_time,
        'station_media_problem': station_media_problem,
        'cost_time_proportion': cost_time_proportion,
        'media_problem_proportion': media_problem_proportion
    }
