# 履职评价信息
check_evaluate_info_sql = """SELECT
        a.EVALUATE_WAY,
        a.EVALUATE_TYPE,
        e.PERSON_JOB_TYPE,
        d.`LEVEL`,
        d.IS_RED_LINE,
        a.SCORE,
        a.EVALUATE_CONTENT
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.CHECK_PERSON_ID_CARD
            LEFT JOIN t_check_problem d on d.pk_id = a.FK_CHECK_OR_PROBLEM_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND  c.TYPE3 = '{2}'"""

# 履职评价复查
check_evaluate_re_sql = """SELECT
        a.EVALUATE_WAY,
        a.EVALUATE_TYPE,
        b.PERSON_JOB_TYPE,
        c.IS_RED_LINE,
        c.`LEVEL`,
        d.ALL_NAME
    FROM
        t_check_evaluate_review_person a
        LEFT JOIN t_person b on b.id_card = a.REVIEW_PERSON_ID_CARD
        LEFT JOIN t_check_problem c on c.pk_id = a.FK_CHECK_OR_PROBLEM_ID
        left join t_department d on d.DEPARTMENT_ID=a.FK_RESPONSIBE_DEPARTMENT_ID
    WHERE
        a.CREATE_TIME >= '{0}'
            AND a.CREATE_TIME <= '{1}'
            AND d.TYPE3 = '{2}'"""


# 检查信息
check_info_sql = """SELECT DISTINCT(a.PK_ID),f.ALL_NAME,d.ALL_NAME as ITEM_NAMES FROM t_check_info a
LEFT JOIN t_person b on b.ID_CARD = a.ID_CARD
LEFT JOIN t_department f on f.department_id = b.FK_DEPARTMENT_ID
LEFT JOIN t_check_info_and_item c on c.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_item d on c.FK_CHECK_ITEM_ID=d.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID= d.FK_DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND e.TYPE3='{2}'"""


# 检查问题信息
check_problem_sql = """SELECT b.TYPE,b.`LEVEL`,b.PROBLEM_POINT,b.DESCRIPTION FROM t_check_info a
LEFT JOIN t_check_problem b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=b.EXECUTE_DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND e.TYPE3='{2}'"""


# 超期问题
zg_sql = """SELECT b.TYPE,d.POSITION,e.`NAME` from t_check_problem a
LEFT JOIN t_safety_assess_month_problem_detail b on b.FK_CHECK_PROBLEM_ID=a.PK_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID = c.TYPE4
LEFT JOIN t_person d on d.ID_CARD=b.ID_CARD
WHERE a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
AND c.TYPE3='{2}'"""


# 标记扣分信息
mark_sql = """SELECT a.DESCRIPTION,c.ALL_NAME FROM t_check_evaluate_mark a
LEFT JOIN t_check_problem b ON a.FK_CHECK_OR_PROBLEM_ID=b.PK_ID 
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.EXECUTE_DEPARTMENT_ID
WHERE a.MARK_TIME BETWEEN '{0}' and '{1}'
AND c.TYPE3='{2}'"""


# 错误标记信息
mistake_sql = """SELECT a.DESCRIPTION,c.ALL_NAME FROM t_check_evaluate_mistake_label a
LEFT JOIN t_check_problem b ON a.FK_CHECK_OR_PROBLEM_ID=b.PK_ID 
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.EXECUTE_DEPARTMENT_ID
WHERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND c.TYPE3='{2}'"""


# 监控调阅设备检查
shebei_sql = """SELECT a.CHECK_WAY,b.RETRIVAL_TYPE_NAME,b.COST_TIME,a.PROBLEM_NUMBER FROM t_check_info a
LEFT JOIN t_check_info_and_media b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_info_and_address c on c.FK_CHECK_INFO_ID=a.PK_ID 
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND d.TYPE3='{2}'
"""


# 监控调阅项目检查
item_sql = """SELECT a.CHECK_WAY,c.ALL_NAME FROM t_check_info a
LEFT JOIN t_check_info_and_item b on b.FK_CHECK_INFO_ID=a.PK_ID
LEFT JOIN t_check_item c on c.PK_ID=b.FK_CHECK_ITEM_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
WHERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND d.TYPE3='{2}'"""


# 重点信息追踪
main_info_tracking_sql = """select a.*,b.ALL_NAME FROM t_key_information_tracking a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_VEST_DEPARTMENT_ID
WHERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND b.TYPE3='{2}'"""


# 问题风险分布信息
pro_risk_sql = """select c.ALL_NAME FROM t_check_problem a
LEFT JOIN t_check_problem_and_risk b on b.FK_CHECK_PROBLEM_ID = a.PK_ID
LEFT JOIN t_risk c on c.PK_ID=b.FK_RISK_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.EXECUTE_DEPARTMENT_ID
WhERE a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
AND d.TYPE3='{2}'"""


# 安全风险预警
risk_warning_sql = """select a.CONTENT,b.ALL_NAME FROM t_warning_notification a
LEFT JOIN t_department b on b.DEPARTMENT_ID = a.FK_DUTY_DEPARTMENT_ID
WhERE a.CREATE_TIME BETWEEN '{0}' and '{1}'
AND b.TYPE3='{2}'"""