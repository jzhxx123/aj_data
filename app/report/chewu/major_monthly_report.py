#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   month_report_cheliang
Description:
Author:    yuchuang
date:         2019-07-23
-------------------------------------------------
Change Activity:2019-07-23 14:54
-------------------------------------------------
"""
import app.report.analysis_report_manager as manager
from docxtpl import DocxTemplate
from app import mongo
from app.report.chewu.major_monthly_report_data import get_data


class ChewuMajorMonthlyAnalysisReport(manager.MonthlyAnalysisReport):
    """
    车务的站段级的周分析报告类。
    """

    def __init__(self):
        super(ChewuMajorMonthlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='车务',)

    def generate_report_data(self, year, month):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param month: int 月
        :return:
        """
        data = get_data(year, month)
        mongo.db['safety_analysis_monthly_report'].delete_one(
            {
                "year": year,
                "month": month,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_monthly_report'].insert_one(data)
        return data
