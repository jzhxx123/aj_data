lh_sql = """SELECT b.* from t_safety_assess_month a
LEFT JOIN t_safety_assess_month_quantify_detail b on a.PK_ID=b.FK_SAFETY_ASSESS_MONTH_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and c.TYPE3='{2}'
"""

without_accident_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR,
            e.CHEWU_OWN AS TYPE
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
                LEFT JOIN
            t_check_problem AS e ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            where c.TYPE3 = '{}'
        """



SECOND_ONE_ANQUAN_SQL = '''SELECT 
                                 a.ID_CARD,a.PROBLEM_NUMBER,a.CHECK_WAY,a.CHECK_ITEM_NAMES,a.IS_GAOTIE,a.IS_DONGCHE,
                                 b.FK_CHECK_INFO_ID,b.FK_CHECK_ITEM_ID,d.TYPE2,d.TYPE3,d.TYPE4,a.IS_YECHA,d.TYPE5
                           FROM 
                                 t_check_info a RIGHT JOIN t_check_info_and_item b ON a.PK_ID=b.FK_CHECK_INFO_ID 
                                                 LEFT JOIN t_check_item c ON b.FK_CHECK_ITEM_ID=c.PK_ID
                                                 LEFT JOIN t_check_problem p ON p.FK_CHECK_INFO_ID=a.PK_ID
                                                 LEFT JOIN t_department d ON d.TYPE4 = p.EXECUTE_DEPARTMENT_ID

                           WHERE 
                                 d.TYPE3 ='{2}'
                                 AND DATE(a.SUBMIT_TIME) BETWEEN '{0}' AND '{1}';
'''

THIRD_ONE_FENGXIAN_SQL = '''
                       SELECT 
                            i.IS_GAOTIE,i.IS_DONGCHE,p.PERSON_NAME,p.SUBMIT_TIME,p.EXECUTE_DEPARTMENT_NAME,
                            p.RISK_NAMES,p.`LEVEL`,p.PROBLEM_POINT,p.DESCRIPTION,p.IS_RED_LINE,p.TYPE,
                            d.TYPE2,d.TYPE3,d.TYPE4,p.PK_ID,d.TYPE5
                       FROM 
                            t_check_problem p LEFT JOIN t_check_info i ON p.FK_CHECK_INFO_ID=i.PK_ID
                                              LEFT JOIN t_check_item t ON p.FK_CHECK_ITEM_ID=t.PK_ID
                                              LEFT JOIN t_department d ON d.TYPE4 = p.EXECUTE_DEPARTMENT_ID
                       WHERE 
                            p.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
                            AND d.TYPE3 ='{2}';
'''

# 履职评价信息查询语句
check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        c.TYPE3,
        c.TYPE4,
        d.NAME AS MAJOR,
        e.JOB,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE4
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND  c.TYPE3 = '{2}'
"""

# 履职复查信息查询语句
check_evaluate_review_sql = """SELECT
        t.*,d.TYPE3
    FROM
        t_check_evaluate_review_person t
        left join t_department d on d.TYPE4=t.FK_RESPONSIBE_DEPARTMENT_ID
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1}'
            AND d.TYPE3 = '{2}'
"""

# 违章大王 没过滤部门
# 重点班组就在这里 group by type5 sum
weizhang_king = '''
SELECT 
     p.PERSON_NAME,p.ID_CARD,tp.JOB,tp.POSITION,p.ORIGINAL_VIOLATION_NUMBER,p.DEDUCT_SCORE,
     d.TYPE3,d.TYPE4,d.TYPE5,d.ALL_NAME
FROM 
     t_warning_key_person p 
     LEFT JOIN t_person tp ON p.ID_CARD=tp.ID_CARD
     LEFT JOIN t_department d ON tp.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
WHERE 
     p.WARN_START_DATE BETWEEN '{}' AND '{}';
'''

# 责任安全信息 没限制
RESPONSIBE_SAFETY_PRODUCE_INFO_SQL = '''
SELECT 
     i.FK_DEPARTMENT_ID,i.OVERVIEW,i.MAIN_TYPE,i.DETAIL_TYPE,i.RESPONSIBILITY_DIVISION_NAME,
     d.TYPE3,d.TYPE4,d.TYPE5 
FROM 
     t_safety_produce_info i
     LEFT JOIN t_safety_produce_info_responsibility_unit u ON u.FK_SAFETY_PRODUCE_INFO_ID=i.PK_ID 
     LEFT JOIN t_department d ON u.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
WHERE 
     i.OCCURRENCE_TIME BETWEEN '{0}' AND '{1}'
     AND u.FK_DEPARTMENT_ID IS NOT NULL;
'''


#安全中心统计信息 没限制部门
analysis_center = '''
SELECT * FROM `t_analysis_center_quantization_quota`
WHERE `YEAR`='{}' and
`MONTH`='{}';
'''

check_info_sql = """SELECT
        a.CHECK_WAY,
        d.ALL_NAME,
        a.PROBLEM_NUMBER,
        b.COST_TIME,
        d.`NAME`
    FROM
        t_check_info AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        left join t_person c on a.ID_CARD=c.ID_CARD
        left join t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
    WHERE
        a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
         AND d.TYPE3 ='{2}'
        """