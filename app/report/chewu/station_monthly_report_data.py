from dateutil.relativedelta import relativedelta
from datetime import datetime as dt
import datetime
from docxtpl import InlineImage
from docx.shared import Mm
import threading
from app.utils.common_func import get_department_name_by_dpid
from app.data.util import pd_query
from app.report.chewu.station_monthly_report_sql import *
from app.report.draw_picture import bar_pictures

local_data = threading.local()

_PROBLRM_TYPE_DICT = {
    # 问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
    "作业": ['A', 'B', 'C', 'D'],
    "设备": ['E1', 'E2', 'E3', 'E4'],
    "管理": ['F1', 'F2', 'F3', 'F4'],
    "路外": ['G1', 'G2', 'G3', 'G4'],
    "反恐防暴": ['K1', 'K2', 'K3', 'K4'],
}


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, month, station_id):
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    station_name = get_department_name_by_dpid(station_id)
    local_data.station_name = station_name
    local_data.month = month
    # 分析中心DPID
    center_id = pd_query(analysis_center_id.format(station_id))['DEPARTMENT_ID'].tolist()
    local_data.center_id = center_id
    # 安全生产信息
    year_produce = pd_query(CHECK_SAFETY_INFO_SQL.format('2017-10-01', end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 检查信息
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 问题数据
    problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_problem = pd_query(CHECK_PROBLEM_SQL.format(last_month_start, last_month_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 量化数据
    qt_data = pd_query(t_quanttization_sql.format(year, month, station_id))
    # 音视频调阅
    mv_data = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 车间信息
    shop_data = pd_query(ZHANDUAN_PERSON_SQL.format(station_id))
    local_data.shop_data = shop_data
    # 风险信息
    risk_data = pd_query(RISK_SQL.format(start_date, end_date, station_id))
    risk_data['RISK_TYPE'] = risk_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    last_risk = pd_query(RISK_SQL.format(last_month_start, last_month_end, station_id))
    last_risk['RISK_TYPE'] = last_risk['ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 履职信息
    eva_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_eva = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(last_month_start, last_month_end, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    year_eva = pd_query(
        CHECK_EVALUATE_SITUATION_SQL.format(start_date[:4] + '-01-01', end_date, station_id)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 履职复查
    eva_re = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date, station_id))
    # 违章人员信息
    wz_data = pd_query(CHECK_PROBLEM_WZ_SQL.format(start_date, end_date, station_id)).drop_duplicates(subset=['PK_ID']).\
        reset_index()

    first = _get_first(year_produce)
    second = _get_second(qt_data, problem_data, last_problem, shop_data, mv_data, risk_data, last_risk)
    third = _get_third(eva_data, last_eva, year_eva)
    # four = _get_four(problem_data)
    six = _get_six(wz_data)
    seven = _get_seven(eva_data, eva_re, info_data, mv_data)
    file_name = f'{start_date}至{end_date}{station_name}安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '车务',
        "station_id": station_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        'second': second,
        'third': third,
        'six': six,
        'seven': seven
    }
    return result


def get_ring_ratio(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        rst = count * 100
    else:
        rst = (count - last_count) / last_count * 100
    return rst


def _get_first(year_produce):
    # 无责任
    data = year_produce[(year_produce['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (year_produce['MAIN_TYPE'] == 1)]
    dics = {}
    # 无责任ABCD类事故天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[data['CODE'].str.contains(i)]
        if len(accident) == 0:
            dics['{0}'.format(i)] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dics['{0}'.format(i)] = \
                str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                    'OCCURRENCE_TIME'].date()).split(' ')[0]
    # 死亡、重伤无责任事故天数
    times = []
    for i in [['ROAD_IN_DEAD', 'ROAD_OUT_DEAD'], ['ROAD_IN_SERIOUS_INJURY', 'ROAD_OUT_SERIOUS_INJURY']]:
        data1 = data[data[i[0]] == 1]
        data2 = data[data[i[1]] == 1]
        if len(data1) == 0:
            time1 = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            time1 = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                'OCCURRENCE_TIME'].date()).split(' ')[0]
        if len(data2) == 0:
            time2 = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            time2 = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).iloc[0][
                'OCCURRENCE_TIME'].date()).split(' ')[0]
        if int(time1) <= int(time2):
            times.append(time1)
        else:
            times.append(time2)
    # 行车、劳动、路外事故情况
    all_acc_type = []
    for i in [1, 2, 3]:
        new_data = year_produce[year_produce['MAIN_TYPE'] == 1]
        acc_data = new_data[new_data['DETAIL_TYPE'] == i]
        dic = {
            'name': '行车' if i == 1 else ('劳动' if i == 2 else '路外'),
            'count': len(acc_data),
            'content': list(acc_data['OVERVIEW'].unique())
        }
        all_acc_type.append(dic)
    return {
        'dics': dics,
        'times': times,
        'all_acc_type': all_acc_type
    }


def _get_second(qt_data, problem_data, last_problem, shop_data, mv_data, risk_data, last_risk):
    """
    检查基本情况统计分析
    :param qt_data:
    :param problem_data:
    :return:
    """
    # 检查基本情况
    check_base = get_check_base(qt_data, problem_data, shop_data, mv_data)

    # 问题统计分析
    problem_count = get_problem_count(risk_data, last_risk, problem_data, last_problem)
    return {
        'check_base': check_base,
        'problem_count': problem_count
    }


def get_check_base(qt_data, problem_data, shop_data, mv_data):
    """
    检查基本情况
    :param qt_data:
    :param problem_data:
    :param shop_data:
    :return:
    """
    # 站段检查
    all_info = []
    for i in [['REALITY_NUMBER', 'CHECK_TIMES_TOTAL'], ['REALITY_PROBLEM_NUMBER', 'PROBLEM_NUMBER_TOTAL'],
              ['REALITY_WORK_ITEM_PROBLEM_NUMBER', 'WORK_PROBLEM_NUMBER_TOTAL'],
              ['MEDIA_REALITY_TIME', 'MONITOR_NUMBER_TOTAL'],
              ['REALITY_MIN_QUALITY_GRADES', 'MIN_QUALITY_GRADES_TOTAL']]:
        # 实际数据
        data1 = float(qt_data[i[0]].sum())
        # 计划数据
        data2 = float(qt_data[i[1]].sum())
        # 兑现率
        ratio = round(ded_zero(data1, data2) * 100, 2)
        all_info.append([data1, ratio, data2])
    # 监控调阅发现问题
    all_info.append(int(qt_data['MEDIA_REALITY_PROBLEM_NUMBER'].sum()))
    # 红线问题
    red = problem_data[problem_data['IS_RED_LINE'].isin([1, 2])]
    zd_red = len(red[red['TYPE'] == 3])
    sp_red = len(red[red['TYPE'] == 4])

    # 站段调阅检查
    # 调阅总次数
    dy_count = len(mv_data)
    # 发现问题数
    dy_pro = int(qt_data['MEDIA_REALITY_PROBLEM_NUMBER'].sum())
    # 发现问题率
    dy_ratio = round(ded_zero(dy_pro, int(qt_data['REALITY_NUMBER'].sum())) * 100, 2)
    # 调阅类型
    dy_type = []
    for i, j in mv_data.groupby('NAME').count().reset_index().iterrows():
        dic = {
            'name': j['NAME'],
            'count': int(j['SHOP'])
        }
        dy_type.append(dic)

    # 车间检查
    shops = []
    for i in shop_data['NAME'].unique():
        data = qt_data[qt_data['SHOP'] == i]
        shop_info = []
        for j in [['REALITY_NUMBER', 'CHECK_TIMES_TOTAL'], ['REALITY_PROBLEM_NUMBER', 'PROBLEM_NUMBER_TOTAL'],
                  ['REALITY_WORK_ITEM_PROBLEM_NUMBER', 'WORK_PROBLEM_NUMBER_TOTAL'],
                  ['REALITY_MIN_QUALITY_GRADES', 'MIN_QUALITY_GRADES_TOTAL']]:
            # 实际数据
            data1 = float(data[j[0]].sum())
            # 计划数据
            data2 = float(data[j[1]].sum())
            # 兑现率
            ratio = round(ded_zero(data1, data2) * 100, 2)
            shop_info.append([data1, ratio, data2])
        shops.append({
            'name': i,
            'content': shop_info
        })

    # 监控设备回放完成情况
    # 车间检查
    shops_mv = []
    for i in shop_data['NAME'].unique():
        data = qt_data[qt_data['SHOP'] == i]
        mv = mv_data[mv_data['SHOP'] == i]
        # 发现问题数
        pro_count = int(data['MEDIA_REALITY_PROBLEM_NUMBER'].sum())
        # 问题发现率
        pro_ratio = round(ded_zero(pro_count, int(data['REALITY_NUMBER'].sum())) * 100, 2)
        # 调阅次数
        dy = len(mv)
        shop_mv = []
        for j in [['MEDIA_REALITY_TIME', 'MONITOR_NUMBER_TOTAL']]:
            # 实际数据
            data1 = float(data[j[0]].sum())
            # 计划数据
            data2 = float(data[j[1]].sum())
            # 兑现率
            ratio = round(ded_zero(data1, data2) * 100, 2)
            shop_mv.append([data1, ratio, data2])
        shops_mv.append({
            'name': i,
            'content': shop_mv,
            'pro_count': pro_count,
            'pro_ratio': pro_ratio,
            'dy': dy,
        })

    return {
        'all_info': all_info,
        'red_count': len(red),
        'zd_red': zd_red,
        'sp_red': sp_red,
        'shops': shops,
        'shops_mv': shops_mv,
        'dy_count': dy_count,
        'dy_ratio': dy_ratio,
        'dy_type': dy_type,
        'dy_pro': dy_pro
    }


def get_problem_count(risk_data, last_risk, problem_data, last_problem):
    # 按照风险类型分类
    # 总风险数
    all_risk = []
    nows = []
    lasts = []
    name = []
    risk_count = len(risk_data)
    for i in risk_data['RISK_TYPE'].unique():
        data = risk_data[risk_data['RISK_TYPE'] == i]
        last = last_risk[last_risk['RISK_TYPE'] == i]
        dic = {
            'name': i,
            'now': len(data),
            'last': len(last),
            # 占比
            'ring': round(ded_zero(len(data), risk_count) * 100, 2),
            # 环比
            'ratio': get_ring_ratio(len(data), len(last))
        }
        all_risk.append(dic)
        nows.append(len(data))
        lasts.append(len(lasts))
        name.append(i)
    # 绘图1
    images1 = bar_pictures([nows, lasts], name, ['本月', '上月'], '风险对比',
                           local_data.station_name + str(local_data.month) + '月份', '车务', 'monthly')

    # 按照问题分项分类
    # 总问题数
    main_type = []
    pro_count = len(problem_data)
    for i in ['作业', '设备', '管理', '路外']:
        data = problem_data[problem_data['LEVEL'].isin(_PROBLRM_TYPE_DICT[i])]
        last = last_problem[last_problem['LEVEL'].isin(_PROBLRM_TYPE_DICT[i])]
        # 小分类
        detail_type = []
        for j in _PROBLRM_TYPE_DICT[i]:
            new_data = data[data['LEVEL'] == j]
            dic = {
                'name': j,
                'count': len(new_data),
                'ring': round(ded_zero(len(new_data), pro_count) * 100, 2)
            }
            detail_type.append(dic)
        dic = {
            'name': i,
            'now': len(data),
            'last': len(last),
            # 环比
            'ratio': get_ring_ratio(len(data), len(last)),
            'ring': round(ded_zero(len(data), pro_count) * 100, 2),
            'detail_type': detail_type
        }
        main_type.append(dic)
    return {
        'all_risk': all_risk,
        'images1': images1,
        'main_type': main_type
    }


def _get_third(eva_data, last_eva, year_eva):
    # 干部评价总体情况分析
    carde_sit = get_carde_sit(eva_data, last_eva, year_eva)
    # 履职评价工作情况分析
    shop_sit = get_shop_sit(eva_data, last_eva, year_eva)
    # 干部履职评价简要分析
    gb_eva_analysis = get_gb_eva_analysis(eva_data, year_eva)
    # 履职评价存在主要问题
    eva_problem = get_eva_problem(eva_data)
    return {
        'carde_sit': carde_sit,
        'shop_sit': shop_sit,
        'gb_eva_analysis': gb_eva_analysis,
        'eva_problem': eva_problem
    }


def get_carde_sit(eva_data, last_eva, year_eva):
    # 总问题数
    count = len(eva_data)
    # 干部履职问题类型统计表
    table1 = []
    for i in ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实',
              '音视频运用管理', '履职评价管理', '事故故障追溯', '弄虚作假']:
        data = eva_data[eva_data['ITEM_NAME'] == i]
        last = last_eva[last_eva['ITEM_NAME'] == i]
        dic = {
            'name': i,
            'count': len(data),
            'ring': round(ded_zero(len(data), count) * 100, 2),
            'ratio': get_ring_ratio(len(data), len(last))
        }
        table1.append(dic)

    # 干部履职突出问题记分类型统计表
    table2 = []
    for i in ['专业管理重点落实不到位', '日常重点安全工作落实不到位', '倒查重点问题检查质量低下', '监控调阅检查不认真', '问题整改督促不力']:
        data = eva_data[eva_data['SITUATION'].str.contains(i)]
        dic = {
            'name': i,
            'count': len(data),
            'ring': round(ded_zero(len(data), count) * 100, 2)
        }
        table2.append(dic)

    # 累计记分表
    number = calc_score_rank(year_eva)
    return {
        'table1': table1,
        'table2': table2,
        'number': number
    }


def get_shop_sit(eva_data, last_eva, year_eva):
    shop = local_data.shop_data
    all_station = []
    all_score_count = []
    for i in shop['NAME'].unique():
        # 车间履职评价情况分析
        data = eva_data[eva_data['SHOP'] == i]
        last_data = last_eva[last_eva['SHOP'] == i]
        all_item = []
        for j in ['量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实',
              '音视频运用管理', '履职评价管理', '事故故障追溯', '弄虚作假']:
            new_data = data[data['ITEM_NAME'] == j]
            new_last = last_data[last_data['ITEM_NAME'] == j]
            dic = {
                'count': len(new_data),
                'ratio': round(get_ring_ratio(len(new_data), len(new_last)), 2)
            }
            all_item.append(dic)
        all_station.append({
            'all_item': all_item,
            'name': i
        })
        # 车间干部累计记分情况
        year_data = year_eva[year_eva['SHOP'] == i]
        score_count = calc_score_rank(year_data)
        all_score_count.append({'score_count': score_count, 'name': i})
    # 站段总人数
    count = len(eva_data['RESPONSIBE_ID_CARD'].unique())
    # 个人被记分最高
    max_one = {}
    for j, k in eva_data.sort_values('SCORE', ascending=False).reset_index().iterrows():
        max_one = {
            'name': k['RESPONSIBE_PERSON_NAME'],
            'score': float('%.2f' % k['SCORE']),
            'dp': k['SHOP']
        }
        break
    # 累计最高
    max_two = {}
    for j, k in eva_data.groupby(['SCORE', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'SHOP']).sum().\
            sort_values('SCORE', ascending=False).reset_index().iterrows():
        max_two = {
            'name': k['RESPONSIBE_PERSON_NAME'],
            'score': float('%.2f' % k['SCORE']),
            'dp': k['SHOP']
        }
        break
    return {
        'all_station': all_station,
        'all_score_count': all_score_count,
        'count': count,
        'max_one': max_one,
        'max_two': max_two
    }


def get_gb_eva_analysis(eva_data, year_eva):
    gradations = []
    for i in ['正科职管理人员', '副科职管理人员', '一般管理和专业技术人员']:
        eva = eva_data[eva_data['GRADATION'] == i]
        year = year_eva[year_eva['GRADATION'] == i]
        # 本月记分数
        now_score = calc_score_rank(eva)
        # 全年记分数
        year_score = calc_score_rank(year)
        # 本月记分人数
        person = len(eva['RESPONSIBE_ID_CARD'].unique())
        # 个人记分最高
        max_one = {}
        for key, item in eva.sort_values('SCORE', ascending=False).reset_index().iterrows():
            max_one = {
                'name': item['RESPONSIBE_PERSON_NAME'],
                'score': float('%.2f' % item['SCORE']),
                'dp': item['SHOP']
            }
            break
        # 累计积分最高
        max_two = {}
        for j, k in eva.groupby(['SCORE', 'RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'SHOP']).sum(). \
                sort_values('SCORE', ascending=False).reset_index().iterrows():
            max_two = {
                'name': k['RESPONSIBE_PERSON_NAME'],
                'score': float('%.2f' % k['SCORE']),
                'dp': k['SHOP']
            }
            break
        gradations.append({
            'year_score': year_score,
            'now_score': now_score,
            'person': person,
            'max_one': max_one,
            'max_two': max_two
        })
    return {
        'gradations': gradations
    }


def calc_score_rank(data):
    """
    计算履职评价记分等级
    :param data:
    :return:
    """
    score_count = [0, 0, 0, 0, 0, 0, 0]
    for j, k in data.groupby('RESPONSIBE_ID_CARD').sum().sort_values('SCORE', ascending=False).reset_index().iterrows():
        if k['SCORE'] < 2:
            score_count[0] += 1
        elif k['SCORE'] < 4:
            score_count[1] += 1
        elif k['SCORE'] < 6:
            score_count[2] += 1
        elif k['SCORE'] < 8:
            score_count[3] += 1
        elif k['SCORE'] < 10:
            score_count[4] += 1
        elif k['SCORE'] < 12:
            score_count[5] += 1
        else:
            score_count[6] += 1
    return score_count


def get_eva_problem(eva_data):
    all_gra = []
    for i in [['局管领导人员'], ['副科职管理人员', '正科职管理人员'], ['一般管理和专业技术人员']]:
        data = eva_data[eva_data['GRADATION'].isin(i)]
        situations = []
        for j, k in data.groupby('SITUATION').count().sort_values('GRADATION', ascending=False).reset_index().iterrows():
            dic = {
                'name': k['SITUATION'],
                'count': int(k['GRADATION'])
            }
            situations.append(dic)
        all_gra.append(situations)
    return {
        'all_gra': all_gra
    }


def _get_six(wz_data):
    # 违章大王
    wz_id = []
    for i, j in wz_data.groupby('ID_CARD').sum().reset_index().iterrows():
        if j['RESPONSIBILITY_SCORE'] >= 8:
            wz_id.append(j['ID_CARD'])
        else:
            continue

    # 严重违章大王
    main_wz = []
    for i in wz_id:
        data = wz_data[(wz_data['ID_CARD'] == i) & (wz_data['LEVEL'].isin(['A', 'B']))].reset_index()
        # AB类问题数大于等于3
        if len(data) >= 3:
            # 计算各类问题数
            level_data = data[data['ID_CARD'] == i]
            level = []
            for j in ['A', 'B', 'C']:
                level.append(len(level_data[level_data['LEVEL'] == j]))
            dic = {
                'name': data['PERSON_NAME'][0],
                'dp': data['NAME'][0],
                'job': data['JOB'][0],
                'score': float('%.2f' % data['RESPONSIBILITY_SCORE'].sum()),
                'level': level
            }
            main_wz.append(dic)
        else:
            continue

    # 一般违章大王
    gener_wz = []
    for i in wz_id:
        data = wz_data[(wz_data['ID_CARD'] == i) & (wz_data['LEVEL'].isin(['A', 'B', 'C']))].reset_index()
        # AB类问题数大于等于3
        if len(data) >= 3:
            # 计算各类问题数
            level_data = data[data['ID_CARD'] == i]
            level = []
            for j in ['A', 'B', 'C']:
                level.append(len(level_data[level_data['LEVEL'] == j]))
            dic = {
                'name': data['PERSON_NAME'][0],
                'dp': data['NAME'][0],
                'job': data['JOB'][0],
                'score': float('%.2f' % data['RESPONSIBILITY_SCORE'].sum()),
                'level': level
            }
            gener_wz.append(dic)
        else:
            continue

    wz_person = wz_data[wz_data['ID_CARD'].isin(wz_id)]
    # 按车间分
    shop_data = []
    for i in wz_person['NAME'].unique():
        data = wz_person[wz_person['NAME'] == i]
        dic = {
            'name': i,
            'count': len(data['ID_CARD'].unique())
        }
        shop_data.append(dic)

    # 按职务分类
    job_data = []
    for i in wz_person['JOB'].unique():
        data = wz_person[wz_person['JOB'] == i]
        dic = {
            'name': i,
            'count': len(data['ID_CARD'].unique())
        }
        job_data.append(dic)
    return {
        'wz_count': len(wz_id),
        'main_wz': main_wz,
        'gener_wz': gener_wz,
        'shop_data': shop_data,
        'job_data': job_data
    }


def _get_seven(eva_data, eva_re, info_data, mv_data):
    center_id = local_data.center_id
    eva = eva_data[eva_data['SHOP_ID'].isin(center_id)]
    re = eva_re[eva_re['SHOP_ID'].isin(center_id)]
    info = info_data[info_data['SHOP_ID'].isin(center_id)]
    mv = mv_data[mv_data['SHOP_ID'].isin(center_id)]
    # 音视频调阅时长
    cost_time = float('%.4f' % mv['COST_TIME'].sum())
    # 音视频复查条数
    mv_re_count = len(info[info['CHECK_WAY'] == 4])
    # 逐条评价复查
    zt = len(re[re['EVALUATE_WAY'] == 1])
    # 定期复查人数
    dq = len(re[re['EVALUATE_WAY'] == 2])
    # 履职发现问题
    find_pro = int(eva['PROBLEM_NUMBER'].sum())
    # 调阅发现问题
    dy_pro = int(info[info['CHECK_WAY'].isin([3, 4])]['PROBLEM_NUMBER'].sum())
    # 阶段评价人次
    jd = len(eva[eva['EVALUATE_WAY'] == 3])
    return {
        'cost_time': cost_time,
        'mv_re_count': mv_re_count,
        'zt': zt,
        'dq': dq,
        'find_pro': find_pro,
        'dy_pro': dy_pro,
        'jd': jd
    }


def insert_images(data, template):
    """
    在Docx生成图片，对应的是data中的InlineImage对象。本函数实现在data内部，将图片的对象InlineImage嵌入。
    :param data: dict类型，报表数据
    :param template: 报表渲染用的模板
    :return:
    """
    data['second']['picture'] = InlineImage(template, data['second']['problem_count'][
        'images1'], width=Mm(150), height=Mm(120))
