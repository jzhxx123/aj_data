#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""
File Name:   month_report_cheliang
Description:
Author:    yuchuang
date:         2019-07-23
-------------------------------------------------
Change Activity:2019-07-23 14:54
-------------------------------------------------
"""
import app.report.analysis_report_manager as manager
from app import mongo
from app.report.chewu.major_annual_report_data import get_data


class ChewuMajorAnnualAnalysisReport(manager.AnnualAnalysisReport):
    """
    车务的站段级的周分析报告类。
    """

    def __init__(self):
        super(ChewuMajorAnnualAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='车务',)

    def generate_report_data(self, year):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :return:
        """
        data = get_data(year)
        mongo.db['safety_analysis_annual_report'].delete_one(
            {
                "year": year,
                "hierarchy": "MAJOR",
                "major": self.major
            })
        mongo.db['safety_analysis_annual_report'].insert_one(data)
        return data
