import datetime
from app import mongo
from app.data.util import pd_query
import app.report.analysis_report_manager as manager
from app.report.chewu.major_annual_report_sql import *
import pandas as pd
from flask import current_app
from dateutil.relativedelta import relativedelta


def calc_ratio(num1, num2):
    if num2 == 0:
        return {
            'diff': num1,
            'percent': num1 * 100
        }
    else:
        return {
            'diff': num1 - num2,
            'percent': round((num1 - num2) / num2 * 100, 1)
        }


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return round(num1 / num2, 1)


def calc_score(list1):
    count = [0, 0, 0, 0, 0, 0, 0]
    for i in list1:
        if 2 < i:
            count[0] += 1
        elif i < 4:
            count[1] += 1
        elif i < 6:
            count[2] += 1
        elif i < 8:
            count[3] += 1
        elif i < 10:
            count[4] += 1
        elif i < 12:
            count[5] += 1
        else:
            count[6] += 1
    return count


def _get_evaluate_report_data(month):
    """从mongo中获取检查信息数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间
    Returns:
        DataFrame -- 检查问题信息
    """

    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'detail_evaluate_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '车务'
        }, {
            "_id": 0,
            "BUSINESS_CLASSIFY": 1,
            "ID_CARD": 1,
            "STATION": 1,
            "score": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def _get_health_index(month, station_name):
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '车务',
            'DEPARTMENT_NAME': station_name
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def get_data(year):
    date = manager.AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 11
    # 履职信息
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date))
    year_evaluate_info = pd_query(check_evaluate_sql.format(str(start_date[:4]) + '-01-01', end_date))
    # 检查信息
    check_info = pd_query(check_manager_info_sql.format(start_date, end_date))
    check_problem = pd_query(check_mamager_problem_sql.format(start_date, end_date))
    # 整改+风险问题信息
    zg_data = pd_query(zg_sql.format(start_date, end_date))
    # 返奖金额
    return_money = pd_query(return_money_sql.format(start_date, end_date))
    # 考核金额
    assess_money = pd_query(assess_money_sql.format(start_date, end_date))
    # 职工人数
    person_data = pd_query(person_sql)
    # 音视频检查
    mv_data = pd_query(mv_check_sql.format(start_date, end_date))
    # 安全科人数
    safety_data = pd_query(safety_person_sql)
    # 领导班子情况
    station_carde_count = pd_query(station_carde_count_sql)
    # 检查信息情况
    info = pd_query(new_check_info_sql.format(start_date, end_date))
    last_info = pd_query(new_check_info_sql.format(last_month_start, last_month_end))
    # 检查问题情况
    problem = pd_query(new_check_problem_sql.format(start_date, end_date))
    last_problem = pd_query(new_check_problem_sql.format(last_month_start, last_month_end))
    first = get_first(evaluate_info, year_evaluate_info, end_date, station_carde_count, info, problem)
    second = get_second(info, last_info, problem, last_problem)
    second_one = get_second_one(end_date, station_carde_count, info, problem, evaluate_info)
    second_two = get_second_two(check_info, check_problem)
    second_three = get_second_three(zg_data)
    second_four = get_second_four(check_problem, return_money, assess_money, person_data)
    second_five = get_second_five(mv_data)
    third = get_third(evaluate_info, year_evaluate_info)
    last = get_last(safety_data, mv_data, check_problem, evaluate_info)
    file_name = f'{start_date}至{end_date}车务系统安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '车务',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        'first': first,
        'second': second,
        'second_one': second_one,
        'second_two': second_two,
        'second_three': second_three,
        'second_four': second_four,
        'second_five': second_five,
        'third': third,
        'last': last
    }
    return result


def get_first(evaluate_info, year_evaluate_info, end_date, station_carde_count, check_info, problem_data):
    # 评价整体情况分析
    first_one = get_first_one(evaluate_info)
    # 评价信息
    first_two = get_first_two(evaluate_info)
    # 扣分情况
    first_three = get_first_three(year_evaluate_info)
    # 履职存在问题
    first_four = get_first_four(evaluate_info)
    # 各级干部履职分析
    first_five = get_first_five(evaluate_info, year_evaluate_info, end_date, station_carde_count, check_info,
                                problem_data)
    return {
        'first_one': first_one,
        'first_two': first_two,
        'first_three': first_three,
        'first_four': first_four,
        'first_five': first_five,
    }


def get_first_one(info):
    # 总人数
    person = info['RESPONSIBE_ID_CARD'].unique().shape[0]
    # 量化人数
    qtz = info[info['FK_CHECK_OR_PROBLEM_ID'] == 0].shape[0]
    # 量化率
    qtz_ratio = round((qtz / person * 100), 2)
    # 未量化人员信息
    not_qtz = info[~info['FK_CHECK_OR_PROBLEM_ID'].isin([0])]
    names = []
    all_name = []
    job = []
    for i, k in not_qtz.iterrows():
        names.append(k['RESPONSIBE_PERSON_NAME'])
        all_name.append(k['NAME'])
        job.append(k['JOB'])
    return {
        'person': person,
        'qtz': qtz,
        'qtz_ratio': qtz_ratio,
        'names': names,
        'all_name': all_name,
        'job': job,
    }


def get_first_two(evaluate_info):
    """
    干部评价总体情况分析
    :param evaluate_info:
    :return:
    """
    # 逐条评价
    zt_data = evaluate_info[evaluate_info['EVALUATE_WAY'] == 1]
    # 阶段评价
    jt_data = zt_data[zt_data['CHECK_TYPE'] == 1]
    data = evaluate_info.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE').reset_index()
    # 总扣分情况
    count = calc_score(data['SCORE'].tolist())
    dic = {
        'one_count': len(zt_data),
        'evaluate_count': len(zt_data),
        'evaluate_person': len(set(zt_data['RESPONSIBE_ID_CARD'])),
        'jt': len(jt_data),
        'zd': len(zt_data[zt_data['CHECK_TYPE'] == 2]),
        'identity': len(zt_data[zt_data['IDENTITY'] == '干部']),
        'total_score': int(zt_data['SCORE'].sum()),
        'jt_person': len(set(jt_data['RESPONSIBE_ID_CARD'])),
        'jt_score': int(jt_data['SCORE'].sum()),
        'count': count
    }
    return dic


def get_first_three(eva_year):
    # 总人数
    count1 = len(set(eva_year['RESPONSIBE_ID_CARD']))
    # 计算计分最高
    max_data = eva_year.sort_values(by='SCORE', ascending=False).reset_index().iloc[0]
    max_add = \
        eva_year.groupby('RESPONSIBE_PERSON_NAME').sum().sort_values(by='SCORE', ascending=False).reset_index().iloc[0]
    max_score = int(max_data['SCORE'])
    name1 = max_data['STATION'] + max_data['JOB'] + max_data['RESPONSIBE_PERSON_NAME']
    add_score = int(max_add['SCORE'])
    name_data = eva_year[eva_year['RESPONSIBE_PERSON_NAME'] == max_add['RESPONSIBE_PERSON_NAME']].iloc[0]
    name2 = name_data['STATION'] + name_data['JOB'] + name_data['RESPONSIBE_PERSON_NAME']
    data = eva_year.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    count = calc_score(data['SCORE'].tolist())
    return {
        'count1': count1,
        'max_score': max_score,
        'name1': name1,
        'add_score': add_score,
        'name2': name2,
        'count': count
    }


def get_first_four(evaluate_info):
    all_list = []
    # 各个履职问题数据
    index = 0
    for i in ['重点工作落实', '监督检查质量', '检查信息录入', '问题闭环管理', '事故故障追溯', '考核责任落实',
              '音视频运用管理', '弄虚作假']:
        data = evaluate_info[evaluate_info['ITEM_NAME'] == i]
        detail = []
        # 每个问题的SITUATION分类
        for j in data['SITUATION'].value_counts().index:
            new_data = data[data['SITUATION'] == j]
            dic = {
                'name': j.strip(),
                'code': new_data['CODE'].tolist()[0],
                'count': len(new_data),
                'person': len(new_data['RESPONSIBE_ID_CARD'].value_counts())
            }
            detail.append(dic)
        dic = {
            'count': len(data),
            'person': len(data['RESPONSIBE_ID_CARD'].value_counts()),
            'detail': sorted(detail, key=lambda x: x['count'], reverse=True)[:5]
        }
        all_list.append(dic)
        # 弄虚作假人员检查内容
        if index == 7:
            content = []
            for a, b in data.iterrows():
                dic = {
                    'station': b['STATION'],
                    'check_type': '自查' if b['CHECK_TYPE'] == 2 else '被路局抽查',
                    'department': b['NAMES'],
                    'job': b['JOB'],
                    'name': b['RESPONSIBE_PERSON_NAME'],
                    'content': b['EVALUATE_CONTENT']
                }
                content.append(dic)
            all_list.append(content)
        index += 1
    return {
        'all_list': all_list
    }


def get_first_five(evaluate_info, year_evaluate_info, end_date, station_carde_count, check_info, problem_data):
    # 局管干部履职
    juguan = get_juguan(evaluate_info)
    # 领导班子履职表
    evaluate_table = get_evaluate_table(evaluate_info, end_date, station_carde_count, check_info, problem_data, '领导')
    # 正科职干部履职
    zk = get_zk(evaluate_info, year_evaluate_info)
    # 副科职及以下干部履职
    fk_bottom = get_fk_bottom(evaluate_info, year_evaluate_info)
    return {
        'juguan': juguan,
        'evaluate_table': evaluate_table,
        'zk': zk,
        'fk_bottom': fk_bottom
    }


def get_juguan(evaluate_info):
    # 局管干部计分情况
    ju_data = evaluate_info[evaluate_info['GRADATION'] == '局管领导人员']
    person = len(ju_data)
    content = []
    for i, k in ju_data.iterrows():
        dic = {
            'station': k['STATION'],
            'job': k['JOB'],
            'name': k['PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    return {
        'person': person,
        'content': content
    }


def get_evaluate_table(evaluate_info, end_date, station_carde_count, check_info, problem_data, str1):
    # 系统平均
    person = station_carde_count[station_carde_count['ALL_NAME'].str.contains(str1)]
    info = check_info[check_info['ALL_NAME'].str.contains(str1)]
    pro = problem_data[problem_data['ALL_NAME'].str.contains(str1)]
    eva = evaluate_info[evaluate_info['NAMES'].str.contains(str1)]
    sys_result = calc_table_data(info, len(person), pro, eva)
    # 站段平均
    station_list = []
    for i in ['内江车务段', '成都客运段', '重庆车站', '遵义车务段', '重庆车务段', '六盘水车务段', '遂宁车务段',
              '贵阳车站', '绵阳车务段', '贵阳车务段', '涪陵车务段', '重庆客运段', '凯里车务段', '兴隆场车站',
              '重庆北车务段', '成都北车站', '成都车务段', '成都车站', '广元车务段', '贵阳南车站', '贵阳客运段',
              '峨眉车务段', '西昌车务段', '达州车务段', '宜宾车务段']:
        sta_info = info[info['NAME'] == i]
        sta_person = person[person['STATION'] == i]
        sta_pro = pro[pro['NAME'] == i]
        sta_eva = eva[eva['STATION'] == i]
        station_result = calc_table_data(sta_info, len(sta_person), sta_pro, sta_eva)
        # 安全综合指数
        date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        if date.day >= current_app.config.get('UPDATE_DAY'):
            date = date + relativedelta(months=1)
        month = date.year * 100 + date.month
        health_index = _get_health_index(month, i)
        if health_index.empty is True:
            health_score = ''
            health_rank = ''
        else:
            health_score = float(health_index['SCORE'])
            health_rank = int(health_index['RANK'])
        # 被评价计分
        eva_data = eva[eva['ALL_NAME'].str.contains('领导')]
        luju = eva_data[eva_data['CHECK_TYPE'] == 1]
        luju_content = luju.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
        luju_list = []
        for j, k in luju_content.iterrows():
            luju_list.append({
                'code': k['CODE'],
                'count': int(k['CODE_ADDITION'])
            })
        zd = eva_data[eva_data['CHECK_TYPE'] == 2]
        zd_content = zd.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
        zd_list = []
        for j, k in zd_content.iterrows():
            zd_list.append({
                'code': k['CODE'],
                'count': int(k['CODE_ADDITION'])
            })
        station_list.append({
            'name': i,
            'station_result': station_result,
            'health_score': health_score,
            'health_rank': health_rank,
            'eva_count': len(eva_data),
            'luju': len(luju),
            'luju_list': luju_list,
            'zd': len(zd),
            'zd_list': zd_list,
        })
    return {
        'sys_result': sys_result,
        'station_list': station_list
    }


def get_evaluate_table1(end_date, station_carde_count, check_info, problem_data, evaluate_info):
    # 系统平均
    sys_result = calc_table_data(check_info, len(station_carde_count), problem_data, evaluate_info)
    # 站段平均
    station_list = []
    for i in ['内江车务段', '成都客运段', '重庆车站', '遵义车务段', '重庆车务段', '六盘水车务段', '遂宁车务段',
              '贵阳车站', '绵阳车务段', '贵阳车务段', '涪陵车务段', '重庆客运段', '凯里车务段', '兴隆场车站',
              '重庆北车务段', '成都北车站', '成都车务段', '成都车站', '广元车务段', '贵阳南车站', '贵阳客运段',
              '峨眉车务段', '西昌车务段', '达州车务段', '宜宾车务段']:
        sta_info = check_info[check_info['NAME'] == i]
        sta_person = station_carde_count[station_carde_count['STATION'] == i]
        sta_pro = problem_data[problem_data['NAME'] == i]
        sta_eva = evaluate_info[evaluate_info['STATION'] == i]
        all_list = []
        for j in ['安全科', '客运科', '技术信息科']:
            station_result = calc_table_data(sta_info[sta_info['ALL_NAME'].str.contains(j)],
                                             len(sta_person[sta_person['ALL_NAME'].str.contains(j)]),
                                             sta_pro[sta_pro['ALL_NAME'].str.contains(j)],
                                             sta_eva[sta_eva['ALL_NAME'].str.contains(j)])
            dic = {
                'name': j,
                'result': station_result
            }
            all_list.append(dic)
        # 安全综合指数
        date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        if date.day >= current_app.config.get('UPDATE_DAY'):
            date = date + relativedelta(months=1)
        month = date.year * 100 + date.month
        health_index = _get_health_index(month, i)
        if health_index.empty is True:
            health_score = ''
            health_rank = ''
        else:
            health_score = float(health_index['SCORE'])
            health_rank = int(health_index['RANK'])
        # 被评价计分
        eva_data = evaluate_info[evaluate_info['ALL_NAME'].str.contains('领导')]
        luju = eva_data[eva_data['CHECK_TYPE'] == 1]
        luju_content = luju.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
        luju_list = []
        # 路局检查信息
        for j, k in luju_content.iterrows():
            luju_list.append({
                'code': k['CODE'],
                'count': int(k['CODE_ADDITION'])
            })
        zd = eva_data[eva_data['CHECK_TYPE'] == 2]
        zd_content = zd.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
        zd_list = []
        # 站段自查信息
        for j, k in zd_content.iterrows():
            zd_list.append({
                'code': k['CODE'],
                'count': int(k['CODE_ADDITION'])
            })
        station_list.append({
            'name': i,
            'all_list': all_list,
            'health_score': health_score,
            'health_rank': health_rank,
            'eva_count': len(eva_data),
            'luju': len(luju),
            'luju_list': luju_list,
            'zd': len(zd),
            'zd_list': zd_list,
        })
    return {
        'sys_result': sys_result,
        'station_list': station_list
    }


def calc_table_data(info, person, pro, evaluate_info):
    """
    计算表格中需要的数据
    :return:
    """
    # 人均现场检查次数
    xc_count = len(info[info['CHECK_WAY'] == 1])
    xc_avg = round(ded_zero(xc_count, person), 1)
    # 人均夜查次数
    yc_count = len(info[info['IS_YECHA'] == 1])
    yc_avg = round(ded_zero(yc_count, person), 1)
    # 人均高质量问题数
    high_count = len(pro[pro['LEVEL'].isin(['A', 'B', 'F1', 'C', 'E3'])])
    high_avg = round(ded_zero(high_count, person), 1)
    # 人均发现问题质量分
    score = int(pro['PROBLEM_SCORE'].sum())
    score_avg = round(ded_zero(score, person), 1)
    # 人均发现问题数
    find_pro = int(info['PROBLEM_NUMBER'].sum())
    find_avg = round(ded_zero(find_pro, person), 1)
    # 人均调阅时长
    cost_time = int(info['COST_TIME'].sum())
    cost_avg = round(ded_zero(cost_time, person))
    # 人均安全履职得分
    safety_score = int(evaluate_info['SCORE'].sum())
    safety_avg = round(ded_zero(safety_score, person), 1)
    return {
        'xc_count': xc_count,
        'xc_avg': xc_avg,
        'yc_count': yc_count,
        'yc_avg': yc_avg,
        'high_count': high_count,
        'high_avg': high_avg,
        'score': score,
        'score_avg': score_avg,
        'find_pro': find_pro,
        'find_avg': find_avg,
        'cost_time': cost_time,
        'cost_avg': cost_avg,
        'safety_score': safety_score,
        'safety_avg': safety_avg
    }


def get_zk(evaluate_info, year_evaluate_info):
    # 局管干部计分情况
    zk_data = evaluate_info[evaluate_info['GRADATION'].str.contains('正科')]
    zk_year_data = year_evaluate_info[year_evaluate_info['GRADATION'].str.contains('正科')]
    person = len(zk_data)
    content = []
    for i, k in zk_data.iterrows():
        dic = {
            'station': k['STATION'],
            'job': k['JOB'],
            'name': k['PERSON_NAME'],
            'reason': k['SITUATION'],
            'score': k['SCORE']
        }
        content.append(dic)
    # 累计计分
    all_list = [[], []]
    for i in zk_data['PERSON_NAME'].value_counts().index:
        year_data = zk_year_data[zk_year_data['PERSON_NAME'] == i]
        data = zk_data[zk_data['PERSON_NAME'] == i]
        dic1 = {
            'name': i,
            'station': data.iloc[0]['STATION'],
            'department': data.iloc[0]['NAMES'],
            'job': data.iloc[0]['JOB'],
            'score': int(data['SCORE'].sum())
        }
        dic2 = {
            'name': i,
            'station': year_data.iloc[0]['STATION'],
            'department': data.iloc[0]['NAMES'],
            'job': year_data.iloc[0]['JOB'],
            'score': int(year_data['SCORE'].sum())
        }
        all_list[0].append(dic1)
        all_list[1].append(dic2)
    # 计算本月和今年的累计计分情况
    all_score = []
    for i in all_list:
        count = [0, 0, 0, 0, 0, 0, 0]
        for j in i:
            if 2 < j['score']:
                count[0] += 1
            elif j['score'] < 4:
                count[1] += 1
            elif j['score'] < 6:
                count[2] += 1
            elif j['score'] < 8:
                count[3] += 1
            elif j['score'] < 10:
                count[4] += 1
            elif j['score'] < 12:
                count[5] += 1
            else:
                count[6] += 1
        all_score.append(count)
    return {
        'person': person,
        'content': content,
        'all_list': all_list,
        'all_score': all_score
    }


def get_fk_bottom(evaluate_info, year_evaluate_info):
    # 局管干部计分情况
    fk_data = evaluate_info[evaluate_info['GRADATION'].isin(['副科职管理人员', '一般管理和专业技术人员'])]
    fk_year_data = year_evaluate_info[year_evaluate_info['GRADATION'].isin(['副科职管理人员', '一般管理和专业技术人员'])]
    general_data = evaluate_info[evaluate_info['GRADATION'].isin(['非管理和专业技术人员'])]
    fk_count = len(fk_data)
    general_count = len(general_data)
    # 累计计分
    all_list = [[], []]
    for i in fk_data['PERSON_NAME'].value_counts().index:
        year_data = fk_year_data[fk_year_data['PERSON_NAME'] == i]
        data = fk_data[fk_data['PERSON_NAME'] == i]
        dic1 = {
            'name': i,
            'station': data.iloc[0]['STATION'],
            'department': data.iloc[0]['NAMES'],
            'job': data.iloc[0]['JOB'],
            'score': int(data['SCORE'].sum())
        }
        dic2 = {
            'name': i,
            'station': year_data.iloc[0]['STATION'],
            'department': data.iloc[0]['NAMES'],
            'job': year_data.iloc[0]['JOB'],
            'score': int(year_data['SCORE'].sum())
        }
        all_list[0].append(dic1)
        all_list[1].append(dic2)
    # 计算本月和今年的累计计分情况
    all_score = []
    for i in all_list:
        count = [0, 0, 0, 0, 0, 0, 0]
        for j in i:
            if 2 < j['score']:
                count[0] += 1
            elif j['score'] < 4:
                count[1] += 1
            elif j['score'] < 6:
                count[2] += 1
            elif j['score'] < 8:
                count[3] += 1
            elif j['score'] < 10:
                count[4] += 1
            elif j['score'] < 12:
                count[5] += 1
            else:
                count[6] += 1
        all_score.append(count)
    return {
        'fk_count': fk_count,
        'general_count': general_count,
        'all_list': all_list,
        'all_score': all_score
    }


def get_second(info, last_info, problem, last_problem):
    # 检查力度
    check_intensity = get_check_intensity(info, problem)
    # 检查信息
    info_list = []
    for i in [info, last_info]:
        xc = len(i[i['CHECK_WAY'] == 1])
        yecha = len(i[i['IS_YECHA'] == 1])
        yecha_ratio = round(ded_zero(yecha, xc) * 100, 1)
        pro_count = int(i['PROBLEM_NUMBER'].sum())
        cost_time = int(i['COST_TIME'].sum())
        info_list.append([xc, yecha, yecha_ratio, pro_count, cost_time])
    # 问题信息
    pro_list = []
    for i in [problem, last_problem]:
        work = len(i[i['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
        high = len(i[i['LEVEL'].isin(['A', 'B', 'F1'])])
        jk_data = i[i['CHECK_WAY'] == 3]
        jk_work = len(jk_data[jk_data['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
        jk_high = len(jk_data[jk_data['LEVEL'].isin(['A', 'B'])])
        pro_list.append([work, high, jk_work, jk_high])
    # 计算环比
    ratio1 = []
    ratio2 = []
    for i in range(len(info_list[0])):
        ratio1.append(calc_ratio(info_list[0][i], info_list[1][i]))
    for i in range(len(pro_list[0])):
        ratio2.append(calc_ratio(pro_list[0][i], pro_list[1][i]))
    # 客运部信息
    ky_info = info[info['ALL_NAME'].str.contains('客运部')]
    ky_pro = problem[problem['ALL_NAME'].str.contains('客运部')]
    ky_xc = len(ky_info[ky_info['CHECK_WAY'] == 1])
    ky_pro_count = int(ky_info['PROBLEM_NUMBER'].sum())
    ky_work = len(ky_pro[ky_pro['PROBLEM_CLASSITY_NAME'].str.contains('作业')])
    # 站段检查质量情况分析
    analysis_freq = get_analysis_freq(info, problem)
    return {
        'check_intensity': check_intensity,
        'info_list': info_list,
        'pro_list': pro_list,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ky_xc': ky_xc,
        'ky_pro_count': ky_pro_count,
        'ky_work': ky_work,
        'analysis_freq': analysis_freq
    }


def get_analysis_freq(info, problem):
    all_list = []
    for i in info['NAME'].value_counts().index:
        info_data = info[info['NAME'] == i]
        pro_data = problem[problem['NAME'] == i]
        person = len(info_data['ID_CARD'].value_counts())
        find_problem = int(info_data['PROBLEM_NUMBER'].sum())
        score = int(pro_data['PROBLEM_SCORE'].sum())
        dic = {
            'name': i,
            'person': person,
            'check_count': len(info_data),
            'find_problem': find_problem,
            'score': score,
            'score_avg': round(ded_zero(score, find_problem), 1),
            'score_person_avg': round(ded_zero(score, person), 1)
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_check_intensity(info, problem):
    # 计算夜查，人均质量分排名
    all_list = []
    for i in info['NAME'].value_counts().index:
        data = info[info['NAME'] == i]
        pro_data = problem[problem['NAME'] == i]
        person = len(problem['ID_CARD'].value_counts())
        dic = {
            'name': i,
            'yecha': int(round(ded_zero(len(data[data['IS_YECHA'] == 1]), len(data)) * 100, 1)),
            'score': int(round(ded_zero(int(pro_data['PROBLEM_SCORE'].sum()), person), 1))
        }
        all_list.append(dic)
    a_l1 = sorted(all_list, key=lambda x: x['yecha'], reverse=True)
    a_l2 = sorted(all_list, key=lambda x: x['score'], reverse=True)
    return {
        'a_l1': a_l1,
        'a_l2': a_l2
    }


def get_second_one(end_date, station_carde_count, check_info1, problem_data, evaluate_info):
    table = get_evaluate_table1(end_date, station_carde_count, check_info1, problem_data, evaluate_info)
    result = {
        "table": table,
    }
    return result


def get_second_two(check_info, check_problem):
    all_list = []
    for i in check_info['ALL_NAME'].value_counts().index:
        info = check_info[check_info['ALL_NAME'] == i]
        pro = check_problem[check_problem['ALL_NAME'] == i]
        # 人均现场检查次数
        xc_count = len(info[info['CHECK_WAY'] == 1])
        manager = len(info[(info['LEVEL'].str.contains('正科')) | (info['LEVEL'].str.contains('副科'))
                           | (info['LEVEL'].str.contains('一般管理'))])
        xc_avg = round(ded_zero(xc_count, manager), 1)
        # 人均夜查次数
        yc_count = len(info[info['IS_YECHA'] == 1])
        yc_avg = round(ded_zero(yc_count, manager), 1)
        # 人均高质量问题数
        high_count = len(pro[pro['LEVEL'].isin(['A', 'B', 'F1'])])
        high_avg = round(ded_zero(high_count, manager), 1)
        # 人均发现问题质量分
        score = int(pro['PROBLEM_SCORE'].sum())
        score_avg = round(ded_zero(score, manager), 1)
        dic = {
            'name': i,
            'manager': manager,
            'xc_count': xc_count,
            'xc_avg': xc_avg,
            'yc_count': yc_count,
            'yc_avg': yc_avg,
            'high_count': high_count,
            'high_avg': high_avg,
            'score': score,
            'score_avg': score_avg
        }
        all_list.append(dic)
    one = sorted(all_list, key=lambda x: x['xc_count'], reverse=True)
    two = sorted(all_list, key=lambda x: x['yc_count'], reverse=True)
    three = sorted(all_list, key=lambda x: x['high_count'], reverse=True)
    four = sorted(all_list, key=lambda x: x['score'], reverse=True)
    return {
        'one': one,
        'two': two,
        'three': three,
        'four': four
    }


def get_second_three(zg_data):
    zg_data = zg_data.dropna(subset=['RECTIFY_TIME'])
    zg_data['NEW_TIME'] = zg_data['RECTIFY_TIME'].apply(lambda x: datetime.datetime.strptime(str(x), '%Y-%m-%d %H:%M:%S').date())
    # 站段名
    station_name = [i for i in zg_data['NAME'].value_counts().index]
    count = []
    risk = []
    risk_name = ['严重风险问题', '较大风险问题', '一般风险问题']
    # 超期整改条数
    for i in station_name:
        data = zg_data[zg_data['NAME'] == i]
        count.append(len(data[(data['STATUS'] == 2) & (data['NEW_TIME'] < data['RECTIFY_DATE'])]))
        # 风险问题
        risk_level = []
        for j in [1, 2, 3]:
            risk_level.append(len(data[data['RISK_LEVEL'] == j]))
        risk.append(risk_level)
    return {
        'station_name': station_name,
        'count': count,
        'risk': risk,
        'risk_name': risk_name
    }


def get_second_four(check_problem, return_money, assess_money, person_data):
    all_list = []
    for i in person_data['NAME'].tolist():
        # 职工人数
        person_number = int(person_data[person_data['NAME'] == i]['NUMBER'])
        # 换算单位考核问题数
        pro_data = check_problem[check_problem['NAME'] == i]
        assess_count = len(pro_data[pro_data['IS_ASSESS'] == 1])
        assess_ring = round(ded_zero(assess_count, person_number), 1)
        # 返奖率
        return_data = return_money[return_money['NAME'] == i]
        assess_data = assess_money[assess_money['NAME'] == i]
        as_money = int(assess_data['ASSESS_MONEY'].sum())
        re_money = int(return_data['MONEY'].sum())
        re_ring = round(ded_zero(re_money, as_money) * 100, 1)
        dic = {
            'name': i,
            'person_number': person_number,
            'assess_count': assess_count,
            'assess_ring': assess_ring,
            'as_money': as_money,
            're_money': re_money,
            're_ring': re_ring
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_second_five(mv_data):
    check_data = mv_data[(mv_data['TYPE'] == 4) & (mv_data['CHECK_WAY'] == 3)]
    count = len(check_data)
    pro_number = int(check_data['PROBLEM_NUMBER'].sum())
    counts = []
    for i in mv_data['NAME'].value_counts().index:
        data = mv_data[mv_data['NAME'] == i]
        counts.append({
            'count': int(data['PROBLEM_NUMBER'].sum()),
            'name': i
        })
    return {
        'count': count,
        'pro_number': pro_number,
        'counts': counts
    }


def get_third(eva, eva_year):
    d_list = [eva, eva_year]
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'GRADATION']
    month_list, year_list = [], []
    for n in range(2):
        eva = d_list[n]
        level_info = eva.groupby(keys).sum().sort_values(by='SCORE', ascending=False).reset_index()
        level_info.drop(['FK_CHECK_EVALUATE_CONFIG_ID', 'FK_ACCIDENT_ID', 'CHEWU_TYPE', 'EVALUATE_TYPE', 'EVALUATE_WAY',
                         'FK_CHECK_OR_PROBLEM_ID', 'CHECK_TYPE', 'YEAR', 'MONTH', 'IS_GAOTIE', 'IS_DONGCHE',
                         'IS_KECHE', 'FK_CHECK_EVALUATE_CONFIG_ID'], axis=1)
        stas = list(level_info.STATION.value_counts().to_dict().keys())
        for sta in stas:
            ju_d = level_info[(level_info['STATION'] == sta) & (level_info['GRADATION'] == '局管领导人员')]
            if ju_d.shape[0] != 0:
                ju = {
                    'sta': sta,
                    'ju_0': ju_d.shape[0],
                    'ju_1': ju_d[ju_d['SCORE'] < 2].shape[0],
                    'ju_2': ju_d[(ju_d['SCORE'] >= 2) & (ju_d['SCORE'] < 4)].shape[0],
                    'ju_3': ju_d[(ju_d['SCORE'] >= 4) & (ju_d['SCORE'] < 6)].shape[0],
                    'ju_4': ju_d[(ju_d['SCORE'] >= 6) & (ju_d['SCORE'] < 8)].shape[0],
                    'ju_5': ju_d[(ju_d['SCORE'] >= 8) & (ju_d['SCORE'] < 10)].shape[0],
                    'ju_6': ju_d[(ju_d['SCORE'] >= 10) & (ju_d['SCORE'] < 12)].shape[0],
                    'ju_7': ju_d[ju_d['SCORE'] >= 12].shape[0],
                }
            else:
                ju = {
                    'sta': sta,
                    'ju_0': 0,
                    'ju_1': 0,
                    'ju_2': 0,
                    'ju_3': 0,
                    'ju_4': 0,
                    'ju_5': 0,
                    'ju_6': 0,
                    'ju_7': 0,
                }
            zk_d = level_info[(level_info['STATION'] == sta) & (level_info['GRADATION'] == '正科职管理人员')]
            if zk_d.shape[0] != 0:
                zk = {
                    'sta': sta,
                    'zk_0': zk_d.shape[0],
                    'zk_1': zk_d[zk_d['SCORE'] < 2].shape[0],
                    'zk_2': zk_d[(zk_d['SCORE'] >= 2) & (zk_d['SCORE'] < 4)].shape[0],
                    'zk_3': zk_d[(zk_d['SCORE'] >= 4) & (zk_d['SCORE'] < 6)].shape[0],
                    'zk_4': zk_d[(zk_d['SCORE'] >= 6) & (zk_d['SCORE'] < 8)].shape[0],
                    'zk_5': zk_d[(zk_d['SCORE'] >= 8) & (zk_d['SCORE'] < 10)].shape[0],
                    'zk_6': zk_d[(zk_d['SCORE'] >= 10) & (zk_d['SCORE'] < 12)].shape[0],
                    'zk_7': zk_d[zk_d['SCORE'] >= 12].shape[0],
                }
            else:
                zk = {
                    'sta': sta,
                    'zk_0': 0,
                    'zk_1': 0,
                    'zk_2': 0,
                    'zk_3': 0,
                    'zk_4': 0,
                    'zk_5': 0,
                    'zk_6': 0,
                    'zk_7': 0,
                }
            fk_d = level_info[(level_info['STATION'] == sta)
                              & (level_info['GRADATION'].isin(['副科职管理人员', '一般管理和专业技术人员']))]
            if fk_d.shape[0] != 0:
                fk = {
                    'sta': sta,
                    'fk_0': fk_d.shape[0],
                    'fk_1': fk_d[fk_d['SCORE'] < 2].shape[0],
                    'fk_2': fk_d[(fk_d['SCORE'] >= 2) & (fk_d['SCORE'] < 4)].shape[0],
                    'fk_3': fk_d[(fk_d['SCORE'] >= 4) & (fk_d['SCORE'] < 6)].shape[0],
                    'fk_4': fk_d[(fk_d['SCORE'] >= 6) & (fk_d['SCORE'] < 8)].shape[0],
                    'fk_5': fk_d[(fk_d['SCORE'] >= 8) & (fk_d['SCORE'] < 10)].shape[0],
                    'fk_6': fk_d[(fk_d['SCORE'] >= 10) & (fk_d['SCORE'] < 12)].shape[0],
                    'fk_7': fk_d[fk_d['SCORE'] >= 12].shape[0],
                }
            else:
                fk = {
                    'sta': sta,
                    'fk_0': 0,
                    'fk_1': 0,
                    'fk_2': 0,
                    'fk_3': 0,
                    'fk_4': 0,
                    'fk_5': 0,
                    'fk_6': 0,
                    'fk_7': 0,
                }
            fg_d = level_info[(level_info['STATION'] == sta) & (level_info['GRADATION'] == '非管理和专业技术人员')]
            if fg_d.shape[0] != 0:
                fg = {
                    'sta': sta,
                    'fg_0': fg_d.shape[0],
                    'fg_1': fg_d[fg_d['SCORE'] < 2].shape[0],
                    'fg_2': fg_d[(fg_d['SCORE'] >= 2) & (fg_d['SCORE'] < 4)].shape[0],
                    'fg_3': fg_d[(fg_d['SCORE'] >= 4) & (fg_d['SCORE'] < 6)].shape[0],
                    'fg_4': fg_d[(fg_d['SCORE'] >= 6) & (fg_d['SCORE'] < 8)].shape[0],
                    'fg_5': fg_d[(fg_d['SCORE'] >= 8) & (fg_d['SCORE'] < 10)].shape[0],
                    'fg_6': fg_d[(fg_d['SCORE'] >= 10) & (fg_d['SCORE'] < 12)].shape[0],
                    'fg_7': fg_d[fg_d['SCORE'] >= 12].shape[0],
                }
            else:
                fg = {
                    'sta': sta,
                    'fg_0': 0,
                    'fg_1': 0,
                    'fg_2': 0,
                    'fg_3': 0,
                    'fg_4': 0,
                    'fg_5': 0,
                    'fg_6': 0,
                    'fg_7': 0,
                }
            total_d = level_info[level_info['STATION'] == sta]
            if total_d.shape[0] != 0:
                total = {
                    'sta': sta,
                    'total_0': total_d.shape[0],
                    'total_1': total_d[total_d['SCORE'] < 2].shape[0],
                    'total_2': total_d[(total_d['SCORE'] >= 2) & (total_d['SCORE'] < 4)].shape[0],
                    'total_3': total_d[(total_d['SCORE'] >= 4) & (total_d['SCORE'] < 6)].shape[0],
                    'total_4': total_d[(total_d['SCORE'] >= 6) & (total_d['SCORE'] < 8)].shape[0],
                    'total_5': total_d[(total_d['SCORE'] >= 8) & (total_d['SCORE'] < 10)].shape[0],
                    'total_6': total_d[(total_d['SCORE'] >= 10) & (total_d['SCORE'] < 12)].shape[0],
                    'total_7': total_d[total_d['SCORE'] >= 12].shape[0],
                }
            else:
                total = {
                    'sta': sta,
                    'total_0': 0,
                    'total_1': 0,
                    'total_2': 0,
                    'total_3': 0,
                    'total_4': 0,
                    'total_5': 0,
                    'total_6': 0,
                    'total_7': 0,
                }
            if n == 0:
                month_list.append(
                    {
                        'ju': ju,
                        'zk': zk,
                        'fk': fk,
                        'fg': fg,
                        'total': total,
                    }
                )
            if n == 1:
                year_list.append(
                    {
                        'ju': ju,
                        'zk': zk,
                        'fk': fk,
                        'fg': fg,
                        'total': total,
                    }
                )
    return {
        'month_eva': month_list,
        'year_eva': year_list,
    }


def get_last(safety_data, mv_data, check_problem, evaluate_info):
    all_list = []
    for i in safety_data['ALL_NAME'].value_counts().index:
        # 分析中心人数
        person = int(safety_data[safety_data['ALL_NAME'] == i]['NUMBER'])
        # 检查信息
        mv = mv_data[mv_data['ALL_NAME'] == i]
        cost_time = int(mv[mv['CHECK_WAY'] == 3]['COST_TIME'].sum())
        # 检查问题信息
        pro = check_problem[check_problem['ALL_NAME'] == i]
        high = len(pro[pro['LEVEL'].isin(['A', 'B', 'C', 'E3', 'F1'])])
        pro_score = int(pro['PROBLEM_SCORE'].sum())
        # 履职检查信息
        eva = evaluate_info[evaluate_info['NAMES'] == i]
        evaed = evaluate_info[evaluate_info['ALL_NAME'] == i]
        dic = {
            # 履职部分
            'name': i,
            'person': person,
            'eva_count': len(eva),
            'zd_count': len(eva[eva['EVALUATE_WAY'].isin([1, 2, 3])]),
            'avg_score': round(ded_zero(int(eva['SCORE'].sum()), person), 1),
            'top_pro': int(evaed[evaed['CHECK_TYPE'] == 1]['PROBLEM_NUMBER'].sum()),
            'top_eva': len(evaed[evaed['CHECK_TYPE'] == 1]),
            'evaed_score': round(ded_zero(int(evaed['SCORE'].sum()), person), 1),
            # 检查信息部分
            'xc': len(mv[mv['CHECK_WAY'] == 1]),
            'cost_time': cost_time,
            'avg_time': round(ded_zero(cost_time, person), 1),
            'avg_check': round(ded_zero(len(mv), person), 1),
            'find_pro': int(mv['PROBLEM_NUMBER'].sum()),
            'avg_find': round(ded_zero(int(mv['PROBLEM_NUMBER'].sum()), person), 1),
            # 检查问题部分
            'high': high,
            'avg_high': round(ded_zero(high, person), 1),
            'pro_score': pro_score,
            'avg_pro_score': round(ded_zero(pro_score, person), 1)
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_ext_table1(safe_check, l_safe_check):
    safe_check['NEW_NAME'] = safe_check['ALL_NAME'].apply(lambda x: x.split('-')[0])
    l_safe_check['NEW_NAME'] = l_safe_check['ALL_NAME'].apply(lambda x: x.split('-')[0])
    all_list = []
    name_list = []
    for i in safe_check['NEW_NAME'].value_counts().index:
        data = safe_check[safe_check['NEW_NAME'] == i]
        ldata = l_safe_check[l_safe_check['NEW_NAME'] == i]
        name_list.append(i)
        le = []
        for j in ['自查', '上级检查']:
            if j == '自查':
                data = data[data['CHECK_ADDRESS_NAMES'] == data['DEPARTMENT_ALL_NAME']]
                ldata = ldata[ldata['CHECK_ADDRESS_NAMES'] == ldata['DEPARTMENT_ALL_NAME']]
            else:
                data = data[data['CHECK_TYPE'].isin([401, 402, 403, 404])]
                ldata = ldata[ldata['CHECK_TYPE'].isin([401, 402, 403, 404])]
            check_zt = j
            # 检查项
            check_count = len(data)
            diff = calc_ratio(check_count, len(ldata))['percent']
            xc = len(data[data['CHECK_WAY'] == 1])
            tc = len(data[data['CHECK_WAY'] == 2])
            dy = len(data[data['CHECK_WAY'] == 3])
            time = int(data[data['CHECK_WAY'] == 3]['COST_TIME'].sum())
            yecha = len(data[data['IS_YECHA'] == 1])
            yecha_ratio = round(ded_zero(yecha, check_count) * 100, 1)
            # 问题项
            problem_count = int(data['PROBLEM_NUMBER'].sum())
            high_proble = int(data[data['LEVEL'].isin(['A', 'B', 'F1'])]['PROBLEM_NUMBER'].sum())
            high_ratio = round(ded_zero(high_proble, problem_count) * 100, 1)
            # 高铁
            gt_data = data[data['IS_GAOTIE'] == 1]
            gt_problem_count = int(gt_data['PROBLEM_NUMBER'].sum())
            gt_problem_point = []
            for k in ['设备操作', '作业两纪', '安全卡控', '簿册填记', '劳动安全', '规章管理']:
                gt_problem_point.append(int(gt_data[gt_data['NAME'].str.contains(k)]['PROBLEM_NUMBER'].sum()))
            # 普速
            ps_data = data[~data['IS_GAOTIE'] == 1]
            ps_problem_count = int(ps_data['PROBLEM_NUMBER'].sum())
            ps_problem_point = []
            for k in ['出务接发车', '作业互控', '行车凭证', '调度命令', '非正常作业', '防错办', '车机联控', '设备操作',
                      '发车条件确认', '作业标准', '规章管理']:
                ps_problem_point.append(int(ps_data[ps_data['NAME'].str.contains(k)]['PROBLEM_NUMBER'].sum()))
            # 作业、设备、管理
            work = [int(data[data['LEVEL'] == k]['PROBLEM_NUMBER'].sum()) for k in ['A', 'B', 'C', 'D']]
            equipment = [int(data[data['LEVEL'] == k]['PROBLEM_NUMBER'].sum()) for k in ['F1', 'F2', 'F3', 'F4']]
            manage = [int(data[data['LEVEL'] == k]['PROBLEM_NUMBER'].sum()) for k in ['E1', 'E2', 'E3', 'E4']]
            le.append([check_zt, check_count, diff, xc, tc, dy, time, yecha, yecha_ratio, problem_count, high_proble,
                       high_ratio, gt_problem_count, gt_problem_point, ps_problem_count, ps_problem_point, work,
                       equipment, manage])
        all_list.append(le)
    return {
        'all_list': all_list[:15],
        'name_list': name_list[:15],
    }
