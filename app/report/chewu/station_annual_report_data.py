import json
import app.report.analysis_report_manager as manager
import pandas as pd
from datetime import datetime as dt
import datetime
from app.utils.common_func import get_department_name_by_dpid
from app.data.util import pd_query
from app.report.chewu.station_annual_report_sql import *
from app.report.util import singe_score_section

main_risk = ['调车', '反恐防暴', '防溜', '房建设备病害', '管理', '货运交接检查', '货装安全设备', '技术规章', '接发列车',
             '客运组织', '劳动安全', '路外', '其他', '设备质量', '施工', '危险货物运输', '消防', '行车设备故障', '行车组织',
             '装卸质量', '装卸作业', ]
two_point_two = {
    0: '作业类',
    1: '设施设备类',
    2: '管理类',
    3: '外部环境类',
    4: '合计',
}
majors = ['车务']
levels = [7, 6, 5, 4, 3, 2, 1]
all_items = [
    '量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
    '履职评价管理', '事故故障追溯', '弄虚作假'
]
level_name = {
    1: '∑＜2',
    2: '2≤∑＜4',
    3: '4≤∑＜6',
    4: '6≤∑＜8',
    5: '8≤∑＜10',
    6: '10≤∑＜12',
    7: '∑≥12'
}


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, dp_id):
    date = manager.AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 11
    station_name = get_department_name_by_dpid(dp_id)
    # 公用数据
    global evaluate_info2, last_month_evaluate_info, evaluate_year_info, evaluate_review
    # 量化作业信息
    lh_data = pd_query(lh_sql.format(start_date, end_date, dp_id))
    lh_data['NEW_NAME'] = lh_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    # 无责任事故信息
    without_accident = pd_query(without_accident_sql.format(dp_id))
    # 本月检查信息 和上月检查信息
    safe_check = pd_query(SECOND_ONE_ANQUAN_SQL.format(start_date, end_date, dp_id))
    l_safe_check = pd_query(SECOND_ONE_ANQUAN_SQL.format(last_month_start, last_month_end, dp_id))
    # 检查问题 和上月检查问题
    key_point_risk = pd_query(THIRD_ONE_FENGXIAN_SQL.format(start_date, end_date, dp_id))
    l_key_point_risk = pd_query(THIRD_ONE_FENGXIAN_SQL.format(last_month_start, last_month_end, dp_id))
    # 本月履职 和上月履职
    evaluate_info2 = pd_query(check_evaluate_sql.format(start_date, end_date, dp_id))
    last_month_evaluate_info = pd_query(
        check_evaluate_sql.format(last_month_start, last_month_end, dp_id))
    # 今年履职
    evaluate_year_info = pd_query(
        check_evaluate_sql.format(f'{end_date[:4]}-01-01', end_date, dp_id))
    # 履职复查
    evaluate_review = pd_query(check_evaluate_review_sql.format(start_date, end_date, dp_id))
    # 违章信息 本月和上月
    this_k = pd_query(weizhang_king.format(start_date, end_date))
    last_k = pd_query(weizhang_king.format(last_month_start, last_month_end))
    # 安全中心信息
    analysis_inf = pd_query(analysis_center.format(year, month))
    # 检查信息
    check_info_data = pd_query(check_info_sql.format(start_date, end_date, dp_id))
    first = get_first(start_date, end_date, dp_id, without_accident)
    second_one = get_second_one(safe_check, l_safe_check, key_point_risk, l_key_point_risk, lh_data, check_info_data)
    second_two = get_second_two(key_point_risk, l_key_point_risk)
    third = get_evaluate_situation_base_doc(start_date, end_date, dp_id)
    fourth = get_fourth(analysis_inf, dp_id)
    sixth = get_sixth(this_k, last_k, dp_id)
    seventh = get_seventh(analysis_inf, evaluate_info2, dp_id)
    file_name = f'{start_date}至{end_date}{station_name}安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '车务',
        "station_id": dp_id,
        "station_name": station_name,
        "hierarchy": "STATION",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second_one": second_one,
        "second_two": second_two,
        "third": third,
        'fourth': fourth,
        'sixth': sixth,
        'seventh': seventh,
    }
    return result


def get_ring_ratio(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        rst = count * 100
    else:
        rst = round((count - last_count) / last_count * 100, 1)
    return rst


def get_percent(count, last_count):
    """获取环比或同比数据
    Arguments:
        count {int} -- 当前数值
        last_count {int} -- 过去数值
    Returns:
        float -- 一位浮点数
    """
    if last_count == 0:
        rst = count * 100
    else:
        rst = round(count / last_count * 100, 1)
    return rst


def get_first(start_date, end_date, dp_id, without_accident):
    """
    头部信息
    :param start_date:
    :param end_date:
    :param dp_id:
    :param without_accident:
    :return:
    """
    without_info = get_without_info(without_accident)
    shigu_dtf = pd_query(RESPONSIBE_SAFETY_PRODUCE_INFO_SQL.format(start_date, end_date))
    zd_dtf = shigu_dtf[
        (shigu_dtf['TYPE3'] == dp_id)
        & (shigu_dtf['MAIN_TYPE'] == 1)
        ]  # 成都车务段
    if not zd_dtf.empty:
        xinche = zd_dtf[zd_dtf['DETAIL_TYPE'] == 1].shape[0]
        laonn = zd_dtf[zd_dtf['DETAIL_TYPE'] == 2].shape[0]
        luwai = zd_dtf[zd_dtf['DETAIL_TYPE'] == 3].shape[0]
        content = zd_dtf['OVERVIEW'].tolist()
        rst = {
            'xinche': xinche,
            'laoan': laonn,
            'luwai': luwai,
            'con': content,
            'without_info': without_info
        }
    else:
        rst = {
            'xinche': 0,
            'laoan': 0,
            'luwai': 0,
            'without_info': without_info
        }
    return rst


def get_without_info(without_accident):
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    b_accident = data[(data['CODE'].str.contains('B')) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
    c_accident = data[(data['CODE'].str.contains('C')) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
    d_accident = data[(data['CODE'].str.contains('D')) & (data['RESPONSIBILITY_UNIT'].str.contains('全部责任'))]
    hurt = data[(data['NAME'].str.contains('重伤')) | (data['NAME'].str.contains('死亡'))]
    if len(b_accident) == 0:
        b_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
    else:
        b_time = \
            str(dt.now().date() - b_accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    if len(c_accident) == 0:
        c_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
    else:
        c_time = \
            str(dt.now().date() - c_accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    if len(d_accident) == 0:
        d_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
    else:
        d_time = \
            str(dt.now().date() - d_accident.sort_values(by='OT', ascending=False).iloc[0]['OT'].date()).split(' ')[0]
    if len(hurt) == 0:
        hurt_time = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
    else:
        hurt_time = \
            str(dt.now().date() - hurt.sort_values(by='OT', ascending=False)['OT'].tolist()[0].date()).split(' ')[0]
    return {
        'b_time': b_time,
        'c_time': c_time,
        'd_time': d_time,
        'hurt_time': hurt_time
    }


def get_second_one(infoo, l_infoo, pro, l_pro, lh_data, check_info_data):
    """
    得到第2部分数据
    :return:
    """
    pro.drop_duplicates(['PK_ID'], keep='last')
    l_pro.drop_duplicates(['PK_ID'], keep='last')
    find_problem_dtf = infoo.drop_duplicates(['FK_CHECK_INFO_ID'], keep='last')
    l_find_problem_dtf = l_infoo.drop_duplicates(['FK_CHECK_INFO_ID'], keep='last')

    xc_check = find_problem_dtf[find_problem_dtf['CHECK_WAY'] == 1].shape[0]
    l_xc_check = l_find_problem_dtf[l_find_problem_dtf['CHECK_WAY'] == 1].shape[0]
    dtf22 = pro[pro['LEVEL'].isin(['A', 'B', 'C', 'D'])]
    work_problem = int(dtf22['PK_ID'].nunique())
    find_problem_number = int(pro['PK_ID'].nunique())
    l_find_problem_number = int(l_pro['PK_ID'].nunique())
    # 红线问题
    red_line_data = pro[pro['IS_RED_LINE'].isin([1, 2])]
    chejian = red_line_data[red_line_data['TYPE'] == 3].shape[0]
    qt = red_line_data[red_line_data['TYPE'] == 4].shape[0]
    red_line = red_line_data.shape[0]
    red_line_c = pro[pro['IS_RED_LINE'].isin([1, 2])].DESCRIPTION.tolist()
    # 表格
    check_table1 = get_check_table1(lh_data)
    check_table2 = get_check_table2(lh_data)
    check_table2.insert(0, check_table1)
    table3 = get_table3(check_info_data)
    return {
        'xianchang': xc_check,
        'xc_ring': get_ring_ratio(xc_check, l_xc_check),
        'work': work_problem,
        'find_pro_num': find_problem_number,
        'find_pro_ring': get_ring_ratio(find_problem_number, l_find_problem_number),
        'red_line': red_line,
        'red_line_c': red_line_c,
        'chejian': chejian,
        'qt': qt,
        'table': check_table2,
        'table3': table3
    }


def get_table3(check_info_data):
    """
    监控设备回放信息表格
    :param check_info_data:
    :return:
    """
    # 总数据
    data = check_info_data
    dic = {
        'count': len(data[data['CHECK_WAY'] == 3]),
        'cost_time': int(data['COST_TIME'].sum()),
        'pro_count': int(data['PROBLEM_NUMBER'].sum()),
        'find_ratio': round(ded_zero(int(data['PROBLEM_NUMBER'].sum()), len(data)) * 100, 1)
    }
    chejian = check_info_data[check_info_data['NAME'].str.contains('车间')]['NAME'].value_counts().index
    all_list = []
    # 领导及安全科数据
    for i in ['领导', '安全科']:
        data = check_info_data[check_info_data['NAME'].str.contains(i)]
        count = len(data[data['CHECK_WAY'] == 3])
        cost_time = int(data['COST_TIME'].sum())
        pro_count = int(data['PROBLEM_NUMBER'].sum())
        find_ratio = round(ded_zero(pro_count, len(data)) * 100, 1)
        all_list.append([count, cost_time, pro_count, find_ratio])
    # 各车间数据
    for i in chejian:
        data = check_info_data[check_info_data['NAME'] == i]
        # 监控调阅次数
        count = len(data[data['CHECK_WAY'] == 3])
        # 调阅时长
        cost_time = int(data['COST_TIME'].sum())
        # 发现问题数
        pro_count = int(data['PROBLEM_NUMBER'].sum())
        # 发现问题率
        find_ratio = round(ded_zero(pro_count, len(data)))
        all_list.append([i, count, cost_time, pro_count, find_ratio])
    return {
        'dic': dic,
        'all_list': all_list
    }


def get_check_table1(lh_data):
    """
    量化信息
    :param lh_data:
    :return:
    """
    leader = lh_data[lh_data['ALL_NAME'].str.contains('领导')]
    check_plan = int(leader['CHECK_NUMBER'].sum())
    check_real = int(leader['CHECK_COUNT'].sum())
    check_ratio = round(ded_zero(check_real, check_plan) * 100, 1)
    problem_plan = int(leader['PROBLEM_QUANTIFY_NUMBER'].sum())
    problem_real = int(leader['PROBLEM_QUANTIFY_COUNT'].sum())
    problem_ratio = round(ded_zero(problem_real, problem_plan) * 100, 1)
    work_plan = int(leader['WORK_ITEM_PROBLEM_NUMBER'].sum())
    work_real = int(leader['WORK_ITEM_PROBLEM_COUNT'].sum())
    score = int(leader['WORK_ITEM_PROBLEM_COUNT'].sum())
    avg_score = round(ded_zero(score, len(leader)), 1)
    data = ['段领导', check_plan, check_real, check_ratio, problem_plan, problem_real, problem_ratio, work_plan, work_real,
            score, avg_score]
    return data


def get_check_table2(lh_data):
    all_list = []
    lh_data = lh_data[~lh_data['ALL_NAME'].str.contains('领导')]
    for i in lh_data['NEW_NAME'].value_counts().index:
        data = lh_data[lh_data['NEW_NAME'] == i]
        name = i
        check_plan = int(data['CHECK_NUMBER'].sum())
        check_real = int(data['CHECK_COUNT'].sum())
        check_ratio = round(ded_zero(check_real, check_plan) * 100, 1)
        problem_plan = int(data['PROBLEM_QUANTIFY_NUMBER'].sum())
        problem_real = int(data['PROBLEM_QUANTIFY_COUNT'].sum())
        problem_ratio = round(ded_zero(problem_real, problem_plan) * 100, 1)
        work_plan = int(data['WORK_ITEM_PROBLEM_NUMBER'].sum())
        work_real = int(data['WORK_ITEM_PROBLEM_COUNT'].sum())
        score = int(data['WORK_ITEM_PROBLEM_COUNT'].sum())
        avg_score = round(ded_zero(score, len(data)), 1)
        data_list = [name, check_plan, check_real, check_ratio, problem_plan, problem_real, problem_ratio, work_plan,
                     work_real, score, avg_score]
        all_list.append(data_list)
    return all_list


def get_second_two(thi, las):
    """
    得到第2部分数据
    传这个月的pro和上个月的pro
    :return:
    """
    rst, res, ret = [], [], []
    this = thi.drop_duplicates(['PK_ID'], keep='last')
    total = this.shape[0]
    last = las.drop_duplicates(['PK_ID'], keep='last')
    for risk in main_risk:
        rst.append(
            {
                "name": risk,
                "lastmonth": len(last[last['RISK_NAMES'].str.contains(risk)]),
                "thismonth": len(this[this['RISK_NAMES'].str.contains(risk)]),
                "ring": get_ring_ratio(len(this[this['RISK_NAMES'].str.contains(risk)]),
                                       len(last[last['RISK_NAMES'].str.contains(risk)])),
                "percent": get_percent(len(this[this['RISK_NAMES'].str.contains(risk)]), total),
            }
        )
    for n in range(0, 5):
        if n == 0:
            res.append({
                'weizhi': two_point_two.get(n),
                'last': last[last['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0],
                'this': this[this['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0],
                'ring': get_ring_ratio(this[this['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0],
                                       last[last['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0]),
                'percent': get_percent(this[this['LEVEL'].isin(['A', 'B', 'C', 'D'])].shape[0],
                                       total)
            })
        if n == 1:
            res.append({
                'weizhi': two_point_two.get(n),
                'last': len(last[last['LEVEL'].str.contains('E')]),
                'this': len(this[this['LEVEL'].str.contains('E')]),
                'ring': get_ring_ratio(len(this[this['LEVEL'].str.contains('E')]),
                                       len(last[last['LEVEL'].str.contains('E')])),
                'percent': get_percent(len(this[this['LEVEL'].str.contains('E')]),
                                       total)
            })
        if n == 2:
            res.append({
                'weizhi': two_point_two.get(n),
                'last': len(last[last['LEVEL'].str.contains('F')]),
                'this': len(this[this['LEVEL'].str.contains('F')]),
                'ring': get_ring_ratio(len(this[this['LEVEL'].str.contains('F')]),
                                       len(last[last['LEVEL'].str.contains('F')])),
                'percent': get_percent(len(this[this['LEVEL'].str.contains('F')]),
                                       total)
            })
        if n == 3:
            res.append({
                'weizhi': two_point_two.get(n),
                'last': len(last[last['LEVEL'].str.contains('G')]),
                'this': len(this[this['LEVEL'].str.contains('G')]),
                'ring': get_ring_ratio(len(this[this['LEVEL'].str.contains('G')]),
                                       len(last[last['LEVEL'].str.contains('G')])),
                'percent': get_percent(len(this[this['LEVEL'].str.contains('G')]),
                                       total)
            })
        if n == 4:
            res.append({
                'weizhi': two_point_two.get(n),
                'last': last.shape[0],
                'this': total,
                'ring': get_ring_ratio(total,
                                       last.shape[0])
            })
    key_d = dict(this['LEVEL'].value_counts())
    key_lis = key_d.keys()
    for key in key_lis:
        ret.append(
            {
                'name': key,
                'number': int(key_d[key]),
                'percent': float(get_percent(key_d[key], total))
            }
        )
    return {
        'fir_table': rst,
        'sec_table': res,
        'sec_text': ret,
    }


def get_fourth(anquan, dp_id):
    """
    得到第4部分数据
    :return:
    """
    analysis_center_info = anquan[anquan['FK_DEPARTMENT_ID'] == dp_id]

    yinpin_time = int(analysis_center_info['MONITOR_TIME'].iloc[0])
    work_pro = int(analysis_center_info['MONITOR_WORK_ITEM_PROBLEM'].iloc[0])
    find_pro = int(analysis_center_info['MONITOR_PROBLEM'].iloc[0])
    return {
        'yinpin_time': yinpin_time,
        'find_pro': find_pro,
        'work_pro': work_pro,
    }
    pass


def get_sixth(kingsss, l_kingsss, dp_id):
    """
    得到第6部分数据 传这个月 和上个月的违章信息
    :return:
    """
    kingss = kingsss[kingsss['TYPE3'] == dp_id]
    l_kingss = l_kingsss[l_kingsss['TYPE3'] == dp_id]
    l_num = int(l_kingss['ID_CARD'].nunique())
    kings = kingss.drop_duplicates(['ID_CARD'], keep='last')

    dtf3 = kingss.groupby('ID_CARD').sum().sort_values(
        by='DEDUCT_SCORE', ascending=False).reset_index()
    dtf33 = dtf3[(dtf3['DEDUCT_SCORE'] >= 12) & (dtf3['DEDUCT_SCORE'] < 16)]
    dtf44 = dtf3[dtf3['DEDUCT_SCORE'] >= 16]
    ser_k, yiban_k, ret, allname = [], [], [], []
    if not dtf33.empty:
        id_list1 = dtf33['ID_CARD'].tolist()
        for idd in id_list1:
            yiban_k.append(
                {
                    'k_allname': str((kingss['ALL_NAME'][kingss.ID_CARD == idd]).iloc[0]),
                    'k_name': str((kingss['PERSON_NAME'][kingss.ID_CARD == idd]).iloc[0]),
                    'k_job': str((kingss['POSITION'][kingss.ID_CARD == idd]).iloc[0]),
                    'k_number': int(dtf33['ORIGINAL_VIOLATION_NUMBER'][dtf33.ID_CARD == idd]),
                    'k_score': int(dtf33['DEDUCT_SCORE'][dtf33.ID_CARD == idd]),
                }
            )
    if not dtf44.empty:
        id_list2 = dtf44['ID_CARD'].tolist()
        for idt in id_list2:
            ser_k.append(
                {
                    'k_allname': str((kingss['ALL_NAME'][kingss.ID_CARD == idt]).iloc[0]),
                    'k_name': str((kingss['PERSON_NAME'][kingss.ID_CARD == idt]).iloc[0]),
                    'k_job': str((kingss['POSITION'][kingss.ID_CARD == idt]).iloc[0]),
                    'k_number': int(dtf44['ORIGINAL_VIOLATION_NUMBER'][dtf44.ID_CARD == idt]),
                    'k_score': int(dtf44['DEDUCT_SCORE'][dtf44.ID_CARD == idt]),
                }
            )
    key_d = dict(kings['POSITION'].value_counts())
    key_lis = key_d.keys()
    for key in key_lis:
        ret.append(
            {
                'name': key,
                'number': int(key_d[key]),
            }
        )
    name_d = dict(kings['ALL_NAME'].value_counts())
    name_lis = name_d.keys()
    for names in name_lis:
        allname.append(
            {
                'name': names,
                'number': int(name_d[names]),
            }
        )
    return {
        'ser': ser_k,
        'yiban': yiban_k,
        'job': ret,
        'allname': allname,
        'ring': dtf3.shape[0] - l_num,
    }


def get_seventh(analysis_info, evaluae_info, dp_id):
    """
    得到第7部分数据
    传安全中心统计信息 履职信息 履职复查信息
    :return:
    """

    analysis_center_info = analysis_info[analysis_info['FK_DEPARTMENT_ID'] == dp_id]

    evaluate_count = evaluae_info.shape[0]
    yinpin_count = evaluate_review[evaluate_review['EVALUATE_TYPE'] == 3].shape[0]
    yinpin_time = int(analysis_center_info['MONITOR_TIME'].iloc[0])
    zhutiao = int(analysis_center_info['STEP_REVIEW_NUMBER'].iloc[0])
    dinqi = int(analysis_center_info['REGULAR_REVIEW_PERSON'].iloc[0])
    find_pro = int(analysis_center_info['MONITOR_PROBLEM'].iloc[0])
    jieduan = int(analysis_center_info['MONITOR_REVIEW_PERSON'].iloc[0])
    return {
        'yinpin_time': yinpin_time,
        'yinpin_count': yinpin_count,
        'zhutiao': zhutiao,
        'dinqi': dinqi,
        'lvzhi': evaluate_count,
        'find_pro': find_pro,
        'jieduan': jieduan,
    }


# --------------------------------------------------------------------------------------- #


def get_evaluate_situation_base_doc(start_date, end_date, dp_id):
    """获取履职情况简要统计分析数据
    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    # 第一块 干部评价总体分析
    first_paragraph = get_evaluate_whole_situation(start_date, end_date, dp_id)
    evaluate_problem_type_table = get_evaluate_problem_type_table(
        start_date, end_date)
    # 第二块 各系统干部问题分析
    major_evaluate_situation = get_major_evaluate_situation(evaluate_info2, evaluate_year_info)
    # 第三块 局管领导人员履职评价分析
    # major_niubi_evaluate_situation = get_niubi_evaluate_situation()
    # 第四块 正科职干部履职评价简要分析
    zheng_level_evaluate_analysis = get_branch_level_carbe_situation('正科级')
    # 第五块 副科职干部履职简要分析
    fu_level_evaluate_analysis = get_branch_level_carbe_situation('副科级')
    # 第六块 一般干部履职简要分析
    yi_level_evaluate_analysis = get_branch_level_carbe_situation('一般管理')
    result = {
        'evaluate_total_analysis': {
            "total_evaluate": first_paragraph,
            "evaluate_problem_type_table": evaluate_problem_type_table
        },
        'carde_evaluate_analysis': {
            "major_evaluate_situation": major_evaluate_situation
        },
        'zheng_level_evaluate_analysis': zheng_level_evaluate_analysis,
        'fu_level_evaluate_analysis': fu_level_evaluate_analysis,
        'yi_level_evaluate_analysis': yi_level_evaluate_analysis
    }
    return result


def get_evaluate_whole_situation(start_date, end_date, dp_id):
    """干部评价总体情况分析

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    evaluate_review = pd_query(
        check_evaluate_review_sql.format(start_date, end_date, dp_id))
    total_ring = get_ring_ratio(evaluate_info2.shape[0],
                                last_month_evaluate_info.shape[0])
    # 定期评价人次
    regular_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 2].shape[0]
    # 逐条评价人次
    one_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 1].shape[0]
    # 履职问题个数
    evaluate_count = evaluate_info2.shape[0]
    # 履职问题人数
    evaluate_person = len(set(evaluate_info2['RESPONSIBE_ID_CARD']))
    # 工人履职数据
    work_data = evaluate_info2[evaluate_info2['LEVEL'] == "工人"]
    # 工人条数和人数
    work_count, work_person = work_data.shape[0], len(
        set(work_data['RESPONSIBE_ID_CARD']))
    # 安全谈心数据
    talk_data = evaluate_info2[evaluate_info2['ITEM_NAME'] == '安全谈心']
    talk_count, talk_person = talk_data.shape[0], len(
        set(talk_data['RESPONSIBE_ID_CARD']))
    # 共计评价得分
    total_score = sum(list(evaluate_info2['SCORE']))
    # 最高评价计分
    score_data = evaluate_info2.groupby('RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    if score_data.empty is True:
        most_score = 0
    else:
        most_score = score_data['SCORE'].tolist()[0]
    # 今年得分数据
    score_year_data = evaluate_year_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_total_count = score_year_data.shape[0]
    month_percent = round(ded_zero(evaluate_count, year_total_count) * 100, 1)
    if score_year_data.empty is True:
        most_year_score = 0
    else:
        most_year_score = round(score_year_data.at[0, 'SCORE'], 1)
    punish_year_count = score_year_data[score_year_data['SCORE'] >= 2].shape[0]
    punish_count = score_data[score_data['SCORE'] >= 2].shape[0]
    score_data['level'] = score_data['SCORE'].apply(singe_score_section)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        singe_score_section)
    levels = [7, 6, 5, 4, 3, 2, 1]
    p_level_rst = []
    for level in levels:
        p_level_rst.append({
            "level":
                level_name.get(level),
            "total_count":
                score_year_data[score_year_data['level'] == level].shape[0],
            "month_count":
                score_data[score_data['level'] == level].shape[0]
        })
    score_rst = {
        "year_total_count": year_total_count,
        "month_count": evaluate_count,
        "month_percent": month_percent,
        "most_year_score": float(most_year_score),
        "punish_year_count": punish_year_count,
        "punish_count": punish_count,
        "p_level_value": p_level_rst,
        "t_level_value": p_level_rst[::-1]
    }
    rst = {
        'regular_count': regular_count,
        'one_count': one_count,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'work_count': work_count,
        'work_person': work_person,
        'talk_count': talk_count,
        'talk_person': talk_person,
        'total_score': total_score,
        'most_score': most_score,
        'total_ring': total_ring,
        'score_level_value': score_rst
    }
    return rst


def get_evaluate_problem_type_table(start_date, end_date):
    """履职问题分类数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    data = evaluate_info2
    last_data = last_month_evaluate_info
    all_list = []
    counts = []
    diffs = []
    rings = []
    # 干部履职问题类型统计表
    for j in all_items:
        new_data = data[data['ITEM_NAME'] == j]
        last_new_data = last_data[last_data['ITEM_NAME'] == j]
        counts.append(len(new_data))
        rings.append(get_ring_ratio(len(new_data), len(last_new_data)))
    for i in range(len(counts)):
        diffs.append(round(ded_zero(counts[i], sum(counts)), 1))
    problem_value = []
    # 干部履职突出问题记分类型统计表
    for i in ['专业管理重点落实不到位。', '日常重点安全工作落实不到位。', '倒查重点问题，检查质量低下。', '监控调阅检查不认真。', '问题整改督促不力。']:
        new_data = data[data['SITUATION'] == i]
        count = len(new_data)
        problem_value.append(count)
    ratio = []
    for i in problem_value:
        ratio.append(round(ded_zero(i, sum(problem_value)) * 100, 1))
    # 履职评价工作情况分析
    for i in data['STATION'].value_counts().index:
        new_data = data[data['STATION'] == i]
        last_new_data = last_data[last_data['STATION'] == i]
        num = []
        rings = []
        for j in all_items:
            news_data = new_data[new_data['ITEM_NAME'] == j]
            lasts_data = last_new_data[last_new_data['ITEM_NAME'] == j]
            rings.append(get_ring_ratio(len(news_data), len(lasts_data)))
            num.append(len(news_data))
        dic = {
            'name': i,
            'count': num,
            'rings': rings
        }
        all_list.append(dic)
    return {
        'counts': counts,
        'diffs': diffs,
        'rings': rings,
        'names': all_items,
        'all_list': all_list,
        'problem_value': problem_value,
        'ratio': ratio
    }


def _get_major_evaluate_table(item_info, last_item_info):
    """专业分类数据

    Arguments:
        item_info {dataframe} -- 当前月份数据
        last_item_info {dataframe} -- 上月数据

    Returns:
        dict -- 结果
    """
    item_info = item_info[item_info['MAJOR'].isin(majors)]
    last_item_info = last_item_info[last_item_info['MAJOR'].isin(majors)]
    major_item_rst = []
    for major in majors:
        major_info = item_info[item_info['MAJOR'] == major]
        major_month_info = last_item_info[last_item_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_last_count = major_month_info.shape[0]
        major_ring = get_ring_ratio(major_count, major_last_count)
        major_item_rst.append({
            'name': major,
            'count': major_count,
            'ring': major_ring
        })
    total = item_info.shape[0]
    last_total = last_item_info.shape[0]
    total_ring = get_ring_ratio(total, last_total)
    major_item_rst.append({'name': '合计', 'count': total, 'ring': total_ring})
    return major_item_rst


def get_major_evaluate_situation(data, year_data):
    year_data['NEW_NAME'] = year_data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    data['NEW_NAME'] = data['ALL_NAME'].apply(lambda x: x.split('-')[1])
    all_list = []
    for i in year_data['NEW_NAME'].value_counts().index:
        new_data = data[data['NEW_NAME'] == i]
        new_year = year_data[year_data['NEW_NAME'] == i]
        month_data = len(new_data[new_data['SCORE'] != 0])
        score_list = calc_score(new_year)
        score_list.append(month_data)
        score_list.insert(0, i)
        all_list.append(score_list)
    sum_list = [[], [], [], [], [], [], [], []]
    # 合计
    for i in all_list:
        sum_list[0].append(i[1])
        sum_list[1].append(i[2])
        sum_list[2].append(i[3])
        sum_list[3].append(i[4])
        sum_list[4].append(i[5])
        sum_list[5].append(i[6])
        sum_list[6].append(i[7])
        sum_list[7].append(i[8])
    add_data = data[data['SCORE'] != 0]
    score_list = calc_score(add_data)
    # bottom info
    bottom_info = get_bottom_info(add_data)
    # 问题类型分类
    problem_type = get_problem_type(data)
    return {
        'all_list': all_list,
        'sum_list': sum_list,
        'score_list': score_list,
        'bottom_info': bottom_info,
        'problem_type': problem_type
    }


def get_problem_type(data):
    num = len(data)
    new_data = data.groupby('ITEM_NAME').count().reset_index()
    all_list = []
    for i in new_data['ITEM_NAME'].tolist():
        s = new_data[new_data['ITEM_NAME'] == i]
        dic = {
            'name': i,
            'count': int(s['RESPONSIBE_ID_CARD']),
            'ratio': round(ded_zero(int(s['RESPONSIBE_ID_CARD']), num) * 100, 1)
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_bottom_info(add_data):
    num = len(add_data['RESPONSIBE_ID_CARD'].value_counts())
    max_data = add_data.sort_values(by='SCORE', ascending=False).reset_index().iloc[0]
    max_name = max_data['RESPONSIBE_PERSON_NAME']
    max_score = int(max_data['SCORE'])
    max_station = max_data['NEW_NAME']
    # 累计最高
    data = add_data.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    id = data.iloc[0]['RESPONSIBE_ID_CARD']
    score = int(data.iloc[0]['SCORE'])
    name = add_data[add_data['RESPONSIBE_ID_CARD'] == id]['RESPONSIBE_PERSON_NAME'].tolist()[0]
    station = add_data[add_data['RESPONSIBE_ID_CARD'] == id]['NEW_NAME'].tolist()[0]
    return {
        'num': num,
        'max_name': max_name,
        'max_score': max_score,
        'max_station': max_station,
        'score': score,
        'name': name,
        'station': station
    }


def calc_score(data):
    new_data = data.groupby('RESPONSIBE_ID_CARD').sum().sort_values(by='SCORE', ascending=False).reset_index()
    person = [0, 0, 0, 0, 0, 0, 0]
    for i in new_data['SCORE']:
        if i < 2:
            person[0] += 1
        elif i < 4:
            person[1] += 1
        elif i < 6:
            person[2] += 1
        elif i < 8:
            person[3] += 1
        elif i < 10:
            person[4] += 1
        elif i < 12:
            person[5] += 1
        else:
            person[6] += 1
    return person


def _get_evaluate_major_detail_item_data(major_info, major):
    """履职信息各专业突出问题数据

    Arguments:
        major_info {dataframe} -- 各专业原始数据
        major {str} -- 专业名称

    Returns:
        dict -- 结果
    """
    major_limit = {"车务": 10, "机务": 6, "工务": 8, "电务": 6, "供电": 6, "车辆": 6}
    major_data = major_info.groupby(['CODE', 'SITUATION'])
    detail_item = []
    for name, group in major_data:
        score_count = group.shape[0]
        score_person = int(group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
        detail_item.append({
            'detail_name': name[1],
            'code': name[0],
            'major': major,
            'score_count': score_count,
            'score_person': score_person
        })
    detail_data = pd.DataFrame(detail_item)
    if detail_data.empty:
        return []
    detail_data = detail_data.sort_values(
        by='score_count', ascending=False).head(major_limit.get(major))
    return [
        json.loads(detail_data.loc[index].T.to_json())
        for index in detail_data.index
    ]


def get_branch_level_carbe_situation(level):
    """正副科职履职信息数据

    Arguments:
        level {str} -- 正副科

    Returns:
        dict -- 结果
    """
    if level == '一般管理':
        levels = ['正处级', '副处级', '正科级', '副科级']
        level_info = evaluate_info2[(~evaluate_info2['LEVEL'].isin(levels))
                                    & (evaluate_info2['MAJOR'].isin(majors))]
    elif level == '副科级':
        levels = ['正处级', '副处级', '正科级', '一般管理']
        level_info = evaluate_info2[(~evaluate_info2['LEVEL'].isin(levels))
                                    & (evaluate_info2['MAJOR'].isin(majors))]
    else:
        level_info = evaluate_info2[(evaluate_info2['LEVEL'] == level)
                                    & (evaluate_info2['MAJOR'].isin(majors))]
    level_count = level_info.shape[0]
    level_person = len(set(level_info['RESPONSIBE_ID_CARD']))
    problem_info = level_info.groupby(
        ['CODE', 'SITUATION']).size().reset_index().rename(columns={
        0: 'count'
    }).sort_values(
        by='count', ascending=False).head(8)
    problem_rst = [
        problem_info.at[index, 'SITUATION'] for index in problem_info.index
    ]
    major_rst = []
    for major in majors:
        major_info = level_info[level_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_person = len(set(major_info['RESPONSIBE_ID_CARD']))
        major_rst.append({
            "major_name": major,
            "major_count": major_count,
            "major_person": major_person
        })
    keys = ['RESPONSIBE_ID_CARD', 'STATION', 'JOB', 'RESPONSIBE_PERSON_NAME']
    level_info = level_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    high_score_rst = [{
        "name": level_info.at[index, 'RESPONSIBE_PERSON_NAME'],
        "station": level_info.at[index, 'STATION'],
        "job": level_info.at[index, 'JOB'],
        "score": int(level_info.at[index, 'SCORE'])
    } for index in level_info.head(5).index]
    year_info = evaluate_year_info[(evaluate_year_info['LEVEL'] == level) & (evaluate_year_info['MAJOR'].isin(majors))]
    year_info = year_info.groupby(keys).sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_table = _get_carde_total_score_table(year_info, level_info)
    year_punish = _get_punish_content(year_info, level_info)
    result = {
        "total_count": level_count,
        "total_person": level_person,
        "problem_situation": problem_rst,
        "major_situation": major_rst,
        "high_score_situation": high_score_rst,
        "level_table": year_table,
        "punish_situation": year_punish
    }
    return result


def _get_carde_total_score_table(year_data, now_data):
    """获取干部总体得分表格数据

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict  -- jieguo
    """
    year_data['level'] = year_data['SCORE'].apply(singe_score_section)
    now_data['level'] = now_data['SCORE'].apply(singe_score_section)
    levels = []
    year_counts = []
    last_counts = []
    for level in range(1, 8):
        levels.append(level_name.get(level))
        year_counts.append(year_data[year_data['level'] == level].shape[0])
        last_counts.append(now_data[now_data['level'] == level].shape[0])
    year_table = {
        'total': year_data.shape[0],
        'levels': levels,
        'counts': year_counts,
        'last_counts': last_counts
    }
    return year_table


def _get_punish_content(year_data, now_data):
    """获取处理结果信息

    Arguments:
        year_data {dataframe} -- 过去数据
        now_data {dataframe} -- 当前数据

    Returns:
        dict -- 结果
    """
    year_punish = {
        'year_count': year_data[year_data['SCORE'] >= 2].shape[0],
        'month_count': now_data[now_data['SCORE'] >= 2].shape[0],
        # 'most_score': year_data.at[0, 'SCORE'],
        # 'most_person': year_data.at[0, 'RESPONSIBE_PERSON_NAME'],
        # 'most_station': year_data.at[0, 'STATION'],
        # 'most_job': year_data.at[0, 'JOB'],
    }
    return year_punish
