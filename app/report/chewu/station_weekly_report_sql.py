from app.data.health_index.common_sql import (
    CHECK_COUNT_SQL, WORK_LOAD_SQL, PROBLEM_CHECK_SCORE_SQL, CHECK_PROBLEM_SQL,
    DEPARTMENT_SQL, JIAODA_RISK_LEVEL_PROBLEM_SQL, CADRE_COUNT_SQL, EXTERNAL_PERSON_SQL
)
from app.data.health_index.check_intensity_sql import (
    MEDIA_PROBLEM_NUMBER_SQL, MEDIA_COST_TIME_SQL, REAL_CHECK_POINT_SQL,
    REAL_CHECK_BANZU_SQL, CHECK_POINT_SQL, BANZU_POINT_SQL, YECHA_CHECK_SQL,
    KAOHE_PROBLEM_SQL
)
from app.data.health_index.evaluate_intensity_sql import (
    ACTIVE_EVALUATE_COUNT_SQL, EVALUATE_COUNT_SQL,
    ACTIVE_EVALUATE_SCORE_SQL,
    DUAN_CADRE_COUNT_SQL,
    ACTIVE_KEZHI_EVALUATE_COUNT_SQL
)
from app.data.health_index.assess_intensity_sql import (
    ASSESS_REVISE_SQL, ASSESS_RROBLEM_SQL, ASSESS_QUANTIFY_SQL,
    ASSESS_RESPONSIBLE_SQL,
)
from app.data.health_index.check_evenness_sql import (
    GENERALLY_ABOVE_PROBLEM_POINT_COUNT_SQL,
    GENERALLY_ABOVE_PROBLEM_POINT_IN_PROBLEM_BASE_COUNT_SQL
)
from app.data.health_index.problem_rectification_sql import (
    IMPORTANT_PROBLEM_RECHECK_COUNT_SQL
)
# 无责任事故信息
without_accident_sql = """SELECT
            a.PK_ID,
            a.OCCURRENCE_TIME AS OT,
            a.CODE,
            a.RESPONSIBILITY_UNIT,
            a.PROFESSION,
            a.NAME,
            d.NAME AS MAJOR
        FROM
            t_safety_produce_info AS a
                LEFT JOIN
            t_safety_produce_info_refer_department AS b
                ON b.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
                LEFT JOIN
            t_department AS c ON c.DEPARTMENT_ID = b.FK_DEPARTMENT_ID
                LEFT JOIN
            t_department AS d ON d.DEPARTMENT_ID = c.TYPE3
            where c.TYPE3 = '{}'
        """


# 检查信息问题
CHECK_INFP_AND_PROBLEM_SQL = """
SELECT 
    a.PK_ID AS FK_CHECK_INFO_ID,
    a.CHECK_WAY,
    b.PROBLEM_CLASSITY_NAME,
    b.RISK_LEVEL,
    b.PK_ID AS FK_CHECK_PROBLEM_ID,
    d.ALL_NAME,
    d.`NAME`,
    a.PROBLEM_NUMBER
FROM
    t_check_info a
        LEFT JOIN
    t_check_problem b ON a.PK_ID = b.FK_CHECK_INFO_ID
        LEFT JOIN
    t_check_item d ON b.FK_CHECK_ITEM_ID = d.PK_ID
        LEFT JOIN
    t_department c ON b.EXECUTE_DEPARTMENT_ID = c.DEPARTMENT_ID
WHERE
    a.SUBMIT_TIME BETWEEN '{0}' AND '{1}'
        AND c.TYPE3 = '{2}'
"""

# 履职评价信息
EVALUATE_INFO_SQL = """
SELECT 
    a.RESPONSIBE_ID_CARD,
    a.EVALUATE_WAY,
    a.RESPONSIBE_PERSON_NAME,
    a.SCORE,
    a.EVALUATE_TYPE,
    a.`LEVEL`,
    a.GRADATION,
    a.CHECK_TYPE,
    b.ITEM_NAME,
    b.SITUATION,
    b.EVALUATE_POINT,
    c.ALL_NAME,
    d.ALL_NAME AS NAME,
    e.CHECK_WAY
FROM
    t_check_evaluate_info a
        LEFT JOIN
    t_check_evaluate_config b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
        LEFT JOIN
    t_check_info e ON a.FK_CHECK_OR_PROBLEM_ID = e.PK_ID
        LEFT JOIN
    t_department c ON a.RESPONSIBE_DEPARTMENT_ID = c.DEPARTMENT_ID
        LEFT JOIN
    t_department d ON a.CHECK_PERSON_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE
    a.CREATE_TIME BETWEEN '{0}' AND '{1}'
        AND c.TYPE3 = '{2}'
"""

# 履职复查信息
EVALUATE_REVIEW_SQL = """
SELECT 
    t.EVALUATE_TYPE,
    t.EVALUATE_WAY,
    t.GRADATION,
    a.CHECK_WAY,
    t.RESPONSIBE_ID_CARD
FROM
    t_check_evaluate_review_person t
        LEFT JOIN
    t_check_info a ON a.PK_ID = t.FK_CHECK_OR_PROBLEM_ID
        LEFT JOIN
    t_department d ON d.DEPARTMENT_ID = t.FK_RESPONSIBE_DEPARTMENT_ID
WHERE
    CREATE_TIME >= '{0}'
        AND CREATE_TIME <= '{1}'
        AND d.TYPE3 = '{2}'
"""

# 部门信息
# 部门
DEPARTMENT_SQL = """SELECT
        a.DEPARTMENT_ID, 
        a.TYPE3, 
        a.TYPE4, 
        a.TYPE5, 
        a.NAME, 
        a.TYPE, 
        a.HIERARCHY,
        a.TYPE2
    FROM
        t_department AS a
            INNER JOIN
        t_department AS b ON a.TYPE3 = b.DEPARTMENT_ID
    WHERE
        b.TYPE = 4 AND b.IS_DELETE = 0
            AND b.SHORT_NAME != ''
            AND b.DEPARTMENT_ID NOT IN ('19B8C3534DE85665E0539106C00A58FD' , 
                                        '19B8C3534E2C5665E0539106C00A58FD',
                                        '19B8C3534E33512345539106C00A58FD',
                                        '19B8C3534E3A566512345106C00A58FD')
"""