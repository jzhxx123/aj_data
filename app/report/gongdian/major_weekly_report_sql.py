# 履职评价信息
CHECK_EVALUATE_SITUATION_SQL = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME AS DEPARTMENT_ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE2 = '1ACE7D1C80B04456E0539106C00A2E70KSC'"""


# 检查信息
CHECK_INFO_SQL = """SELECT i.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,i.IS_YECHA,e.`NAME`,i.CHECK_ITEM_NAMES,
i.ID_CARD
from t_check_info i
left join t_check_info_and_person b on b.fk_check_info_id = i.pk_id
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'"""


# 问题信息
CHECK_PROBLEM_SQL = """SELECT p.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,p.PROBLEM_CLASSITY_NAME,p.`LEVEL`,
d.CHECK_SCORE,i.IS_YECHA,p.RISK_LEVEL,e.`NAME`,p.CHECK_ITEM_NAME,p.RISK_NAMES
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_problem_base d on p.FK_PROBLEM_BASE_ID = d.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'"""


# 风险
risk_sql = """
SELECT p.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,p.PROBLEM_CLASSITY_NAME,p.`LEVEL`,
i.IS_YECHA,p.RISK_LEVEL,e.`NAME`,p.CHECK_ITEM_NAME,d.ALL_NAME
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_check_problem_and_risk a on a.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_risk d on d.PK_ID=a.FK_RISK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'"""


CHECK_MV_COST_TIME_SQL = """SELECT 
	a.PK_ID,
    b.CHECK_PERSON_NAMES,
    b.DEPARTMENT_ALL_NAME,
    a.COST_TIME,
    c.ID_CARD,
    b.CHECK_ITEM_NAMES,
    b.CHECK_WAY,
    d.ALL_NAME,
	e.`NAME`,
	a.MONITOR_POST_NAMES
FROM
    t_check_info_and_media AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        LEFT JOIN
    t_check_info_and_person c on c.FK_CHECK_INFO_ID=b.PK_ID
				LEFT JOIN
    t_department d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
		LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
WHERE
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
        AND d.TYPE2 = '1ACE7D1C80B04456E0539106C00A2E70KSC'
"""


# 履职工作量
CHECK_EVALUATE_REVIEW_SQL = """SELECT c.ALL_NAME,b.*,d.NAME
FROM t_check_evaluate_check_person b 
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
WHERE b.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
AND c.TYPE2 = '1ACE7D1C80B04456E0539106C00A2E70KSC'
"""

# 履职复查
eva_review_sql = """SELECT
b.*
FROM
t_check_evaluate_review_person b
LEFT JOIN t_check_evaluate_check_person a on b.FK_CHECK_EVALUATE_CHECK_PERSON_ID=a.pk_id
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_REVIEW_DEPARTMENT_ID
WHERE
b.CREATE_TIME >= '{0}'
AND b.CREATE_TIME <= '{1} 23:59:59'
AND c.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'"""


# 站段信息
ZHANDUAN_PERSON_SQL = """SELECT count(b.`NAME`) as COUNT,b.`NAME`
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
WHERE a.IS_DELETE = 0 AND a.TYPE2 = '1ACE7D1C80B04456E0539106C00A2E70KSC' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0
GROUP BY b.`NAME`
"""

# 重要检查岗位
main_post_sql = """SELECT a.`NAME`,a.FK_DEPARTMENT_ID,b.ALL_NAME FROM t_profession_dictionary a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.`CODE`='monitor_post' AND a.IS_DELETE=0
and b.DEPARTMENT_ID = '1ACE7D1C80B04456E0539106C00A2E70KSC'"""