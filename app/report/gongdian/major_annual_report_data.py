from app.data.util import pd_query
from app import mongo
import pandas as pd
from dateutil.relativedelta import relativedelta
from app.report.analysis_report_manager import AnnualAnalysisReport
from flask import current_app
from app.report.gongdian.major_annual_report_sql import *
from app.report.util import (get_ring_ratio, singe_score_section)
import datetime


def get_data(year):
    date = AnnualAnalysisReport.get_annual_intervals(year)
    start_date, end_date = date[0]
    last_month_start, last_month_end = date[1]
    month = int(end_date[5:7])
    end_month = int(end_date[5:7])
    start_month = end_month - 11
    first = get_first(start_date, end_date, last_month_start, last_month_end)
    second = get_second(start_date, end_date, last_month_start, last_month_end)
    third = get_third(start_date, end_date, last_month_start, last_month_end)
    fifth = get_fifth(start_date, end_date)
    iiilege = get_iiilege(str(year) + '-01-01', end_date)
    fly = get_finally(start_date, end_date)
    file_name = f'{start_date}至{end_date}供电系统安全管理年度分析.docx'
    result = {
        "year": year,
        "start_month": start_month,
        "end_month": end_month,
        "major": '供电',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        "second": second,
        'third': third,
        'fifth': fifth,
        'iiilege': iiilege,
        'fly': fly
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_year_and_ring_ratio(counts):
    """
    计算环比和同比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def calc_different_type(data):
    """
    计算各类问题数量
    :param data:
    :return:
    """
    types = ['A', 'B', 'C', 'D']
    number = []
    for i in types:
        number.append(len(data[data['CODE'].str.contains(i)]))
    number.append(len(data[data['NAME'].str.contains('设备故障')]))
    return number


# -------------------------------------------------------------第一部分--------------------------------------------------------------------------
def get_first(start_date, end_date, last_month_start, last_month_end):
    """
    段安全生产基本情况分析
    :param start_date: 开始日期
    :param end_date: 结束日期
    :param last_month_start: 上月开始日期
    :param last_month_end: 上月结束日期
    :return:
    """
    accident_data = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date))
    last_month_data = pd_query(CHECK_SAFETY_INFO_SQL.format(last_month_start, last_month_end))
    accident_data1 = pd_query(CHECK_SAFETY_INFO_SQL1.format(start_date, end_date))
    last_month_data1 = pd_query(CHECK_SAFETY_INFO_SQL1.format(last_month_start, last_month_end))
    # TOP信息
    top = get_top(accident_data, last_month_data)
    # 行车、路外、劳安
    all_acc = get_all_acc(accident_data, last_month_data)
    # 事故类别分析
    acc_type = get_acc_type(accident_data, last_month_data)
    # 事故责任归属分析
    acc_duty = get_acc_duty(accident_data, last_month_data, accident_data1, last_month_data1)
    # 设备故障统计分析
    eq_failed = get_eq_failed(accident_data, last_month_data)
    return {
        'top': top,
        'all_acc': all_acc,
        'acc_type': acc_type,
        'acc_duty': acc_duty,
        'eq_failed': eq_failed,
    }


def get_top(accident_data, last_month_data):
    """
    TOP信息计算
    :param accident_data:
    :param last_month_data:
    :return:
    """
    data_list = [accident_data, last_month_data]
    result = []
    for i in data_list:
        data = i[i['RANK'].isin([3])]
        result.append(calc_different_type(data))
    ratio = []
    for i in range(5):
        ratio.append(calculate_year_and_ring_ratio([result[0][i], result[1][i]]))
    general_acc = sum(result[0])
    all_ratio = calculate_year_and_ring_ratio([general_acc, sum(result[1])])
    return {
        'result': result,
        'ratio': ratio,
        'general_acc': general_acc,
        'all_ratio': all_ratio
    }


def get_all_acc(accident_data, last_month_data):
    """
    行车、路外、劳安
    :param accident_data:
    :param last_month_data:
    :return:
    """
    # 行车
    xc = calc_all_acc(accident_data, last_month_data, '行车', ['C', 'D'])
    # 路外
    lw = calc_all_acc(accident_data, last_month_data, '路外', ['B', 'C', 'D'])
    # 劳安
    la = calc_all_acc(accident_data, last_month_data, '劳动安全', ['A', 'B', 'D'])
    return {
        'xc': xc,
        'lw': lw,
        'la': la

    }


def calc_all_acc(accident_data, last_month_data, str1, l1):
    """
    计算行车/路外/劳安问题
    :param accident_data:
    :param last_month_data:
    :param str1:
    :param l1:
    :return:
    """
    # 计算风险同比环比
    accident_data = accident_data[(accident_data['RISK_NAME'].str.contains(str1)) & (accident_data['MAIN_TYPE'] == 1)]
    last_month_data = last_month_data[
        (last_month_data['RISK_NAME'].str.contains(str1)) & (last_month_data['MAIN_TYPE'] == 1)]
    data_list = [accident_data, last_month_data]
    acc_list = []
    type_list = []
    for i in data_list:
        acc_list.append(len(i))
    ratio1 = calculate_year_and_ring_ratio(acc_list)
    for i in range(len(l1)):
        l2 = []
        for j in data_list:
            l2.append(len(j[j['CODE'].str.contains(l1[i])]))
        type_list.append(l2)
    ratio = []
    for i in type_list:
        ratio.append(calculate_year_and_ring_ratio(i))
    return {
        'acc_list': acc_list,
        'ratio1': ratio1,
        'type_list': type_list,
        'ratio': ratio
    }


def get_acc_type(accident_data, last_month_data):
    """
    事故类别分析
    :param accident_data:
    :param last_month_data:
    :return:
    """
    # 行车C类
    type_c = calc_accident_code_type(accident_data, last_month_data, 'C')
    # 行车D类
    type_d = calc_accident_code_type(accident_data, last_month_data, 'D')
    # 劳安事故
    labor = calc_labor(accident_data, last_month_data, '劳动安全')
    return {
        'type_c': type_c,
        'type_d': type_d,
        'labor': labor
    }


def calc_accident_code_type(accident_data, last_month_data, str1):
    """
    计算事故类别
    :param accident_data:
    :param last_month_data:
    :param str1:
    :return:
    """
    # 各个类别事故数量
    count = len(accident_data[accident_data['CODE'].str.contains(str1)])
    new_data = accident_data[accident_data['CODE'].str.contains(str1)].groupby('NAME').count().reset_index()
    name_list = new_data['NAME'].tolist()
    number = new_data['CODE'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, []]
    for i in name_list:
        l1[1].append(
            len(last_month_data[(last_month_data['CODE'].str.contains(str1)) & (last_month_data['NAME'] == i)]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i]]))
    return {
        'count': count,
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def calc_labor(accident_data, last_month_data, str1):
    """
    劳安问题
    :param accident_data:
    :param last_month_data:
    :param str1:
    :return:
    """
    accident_data = accident_data.dropna(subset=['RISK_NAME'])
    count = len(accident_data[accident_data['RISK_NAME'].str.contains(str1)])
    new_data = accident_data[accident_data['RISK_NAME'].str.contains(str1)].groupby('NAME').count().reset_index()
    name_list = new_data['NAME'].tolist()
    number = new_data['RISK_NAME'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, [], []]
    for i in name_list:
        l1[1].append(
            len(last_month_data[(last_month_data['RISK_NAME'].str.contains(str1)) & (last_month_data['NAME'] == i)]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i]]))
    return {
        'count': count,
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def get_acc_duty(accident_data, last_month_data, accident_data1, last_month_data1):
    """
    事故责任分析
    :param accident_data:
    :param last_month_data:
    :return:
    """
    # 系统责任
    sys_duty = get_sys_duty(accident_data1, last_month_data1)
    # 站段责任
    station_duty = get_station_duty(accident_data, last_month_data)
    return {
        'sys_duty': sys_duty,
        'station_duty': station_duty,
    }


def get_sys_duty(accident_data1, last_month_data1):
    """
    系统责任
    :param accident_data1:
    :param last_month_data1:
    :return:
    """
    data_list = [accident_data1, last_month_data1]
    xc = []
    laoan = []
    for i in data_list:
        xc.append(len(i[(i['RISK_NAME'].str.contains('行车')) & (i['RESPONSIBILITY_IDENTIFIED'] == 2)]))
        laoan.append(len(i[(i['RISK_NAME'].str.contains('劳动安全')) & (i['RESPONSIBILITY_IDENTIFIED'] == 2)]))
    ratio1 = calculate_year_and_ring_ratio(xc)
    ratio2 = calculate_year_and_ring_ratio(laoan)
    return {
        'xc': xc[0],
        'laoan': laoan[0],
        'ratio1': ratio1,
        'ratio2': ratio2
    }


def get_station_duty(accident_data, last_month_data):
    """
    站段责任
    :param accident_data:
    :param last_month_data:
    :return:
    """
    data_list = [accident_data, last_month_data]
    xc = []
    for i in data_list:
        xc.append(len(i[(i['RISK_NAME'].str.contains('行车')) & (i['RESPONSIBILITY_IDENTIFIED'] == 2)]))
    ratio1 = calculate_year_and_ring_ratio(xc)
    # 按事故类别分
    new_data = accident_data.groupby('NAME').count().reset_index()
    acc_name = []
    acc_number = []
    for i in range(len(new_data)):
        acc_name.append(new_data.iloc[i]['NAME'])
        acc_number.append(int(new_data.iloc[i]['RISK_NAME']))
    type_list = []
    for i in acc_name:
        type_list.append(accident_data[accident_data['NAME'] == i]['CODE'].tolist()[0])
    # 按站段责任分
    duty = [1, 4, 2, 6]
    duty_name = ['全部责任', '重要责任', '主要责任', '次要责任']
    duty_number = []
    for i in duty:
        duty_number.append(len(accident_data[accident_data['RESPONSIBILITY_IDENTIFIED'] == i]))
    return {
        'xc': xc[0],
        'ratio1': ratio1,
        'acc_name': acc_name,
        'acc_number': acc_number,
        'type_list': type_list,
        'duty_number': duty_number,
        'duty_name': duty_name
    }


def get_eq_failed(accident_data, last_month_data):
    """
    事故故障分析
    :param accident_data:
    :param last_month_data:
    :return:
    """
    # 供电系统发生设备故障
    shebei = calc_eq(accident_data, last_month_data)
    # 设备故障构成事故
    shebei_acc = calc_eq_acc(accident_data, last_month_data, [1])
    # 未构成事故的设备故障
    not_acc = calc_eq_acc(accident_data, last_month_data, [2, 3])
    return {
        'shebei': shebei,
        'shebei_acc': shebei_acc,
        'not_acc': not_acc
    }


def calc_eq(accident_data, last_month_data):
    """
    计算供电系统发生设备故障
    :param accident_data:
    :param last_month_data:
    :return:
    """
    data_list = [accident_data, last_month_data]
    count = []
    for i in data_list:
        i = i.dropna(subset=['RISK_NAME'])
        count.append(len(i[i['RISK_NAME'].str.contains('设备')]))
    d21 = len(accident_data[(accident_data['RISK_NAME'].str.contains('设备')) & (accident_data['CODE'] == 'D21')])
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'd21': d21,
        'ratio': ratio
    }


def calc_eq_acc(accident_data, last_month_data, l1):
    """
    计算故障事故
    :param accident_data:
    :param last_month_data:
    :param l1:
    :return:
    """
    data_list = [accident_data, last_month_data]
    count = []
    for i in data_list:
        count.append(len(i[(i['RISK_NAME'].str.contains('设备')) & (i['MAIN_TYPE'].isin(l1))]))
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'ratio': ratio
    }


# ------------------------------------------------------------------第二部分--------------------------------------------
def get_second(start_date, end_date, last_month_start, last_month_end):
    """
    检查基本情况统计分析
    :param start_date:
    :param end_date:
    :param last_month_start:
    :param last_month_end:
    :return:
    """
    lianghua_data = pd_query(CHECK_LIANGHUA_SQL.format(start_date, end_date))
    problem_data = pd_query(CHECK_PROBLEM_ANALYSIS_SQL.format(start_date, end_date))
    problem_data['NEW_NAME'] = problem_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    last_month_data = pd_query(CHECK_PROBLEM_ANALYSIS_SQL.format(last_month_start, last_month_end))
    last_month_data['NEW_NAME'] = last_month_data['DEPARTMENT_ALL_NAME'].apply(lambda x: x.split('-')[0])
    # 检查基本情况
    check_base_situation = get_check_base_situation(lianghua_data)
    # 检查发现问题
    check_find_problem = get_check_find_problem(problem_data)
    # 问题统计分析
    problem_count = get_problem_count(problem_data)
    # 问题三性分析
    problem_three = get_problem_three_analysis(problem_data, last_month_data)
    return {
        'check_base_situation': check_base_situation,
        'check_find_problem': check_find_problem,
        'problem_count': problem_count,
        'problem_three': problem_three
    }


def get_check_base_situation(lianghua_data):
    """
    检查基本情况
    :param lianghua_data:
    :return:
    """
    count = len(lianghua_data)
    data = lianghua_data[lianghua_data['EVALUATE_WAY'] == 0]
    new_data = data[data['CODE'].isin(['LH-1', 'LH-2', 'LH-3', 'LH-6'])]
    if len(new_data) != 0:
        name = new_data['RESPONSIBE_PERSON_NAME'].tolist()
        dep = new_data['ALL_NAME'].tolist()
        job = new_data['POSITION'].tolist()
        content = new_data['EVALUATE_CONTENT'].tolist()
        return {
            's': 1,
            'count': count,
            'name': name,
            'dep': dep,
            'job': job,
            'content': content
        }
    else:
        return {
            'count': count,
            's': 0
        }


def get_check_find_problem(problem_data):
    """
    检查发现问题
    :param problem_data:
    :return:
    """
    # 作业、设备、管理、路外、反恐防暴
    find_problem_count = int(problem_data['PROBLEM_NUMBER'].sum())
    all_list = []
    for i in ['作业', '设备', '管理', '路外', '反恐防暴']:
        data = problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        count = int(data['PROBLEM_NUMBER'].sum())
        ratio = round(ded_zero(count, find_problem_count) * 100, 1)
        all_list.append({
            'count': count,
            'ratio': ratio
        })
    return {
        'find_problem_count': find_problem_count,
        'all_list': all_list
    }


def get_problem_count(problem_data):
    """
    问题统计分析
    :param problem_data:
    :return:
    """
    # table
    table = calc_table(problem_data)
    # 作业、设备、管理、环境 高/中高质量问题
    check_problem = calc_check_problem(problem_data)
    return {
        'table': table,
        'check_problem': check_problem
    }


def calc_table(problem_data):
    """
    计算表格数据
    :param problem_data:
    :return:
    """
    new_data = problem_data.groupby('NEW_NAME').count().reset_index()
    name_list = new_data['NEW_NAME'].tolist()
    all = []
    for i in range(len(name_list)):
        data = problem_data[problem_data['NEW_NAME'] == name_list[i]]
        work = calc_type_score(data, '作业')
        shebei = calc_type_score(data, '设备')
        manager = calc_type_score(data, '管理')
        env = len(data[(data['PROBLEM_CLASSITY_NAME'].str.contains('路外')) & (data['LEVEL'].str.contains('G'))])
        cs = len(data[(data['PROBLEM_CLASSITY_NAME'].str.contains('反恐防暴')) & (data['LEVEL'].str.contains('K'))])
        total = sum(work[:4]) + sum(shebei[:4]) + sum(manager[:4]) + env + cs
        total_score = work[4] + shebei[4] + manager[4]
        qt = [env, cs, total, total_score]
        all.append(work + shebei + manager + qt)
    return {
        'name_list': name_list[:11],
        'all': all[:11]
    }


def calc_type_score(data, str1):
    """
    计算各类问题分数
    :param data:
    :param str1:
    :return:
    """
    # 计算ABCD类问题数量及平均分
    work_data = data[data['PROBLEM_CLASSITY_NAME'].str.contains(str1)]
    data_list = []
    for i in ['A', 'B', 'C', 'D']:
        data_list.append(len(work_data[work_data['LEVEL'].str.contains(i)]))
    person_number = len(work_data.groupby('PERSON_NAME'))
    score = int(work_data['PROBLEM_SCORE'].sum())
    if person_number != 0:
        avg_score = round(ded_zero(score, person_number), 1)
    else:
        avg_score = score
    data_list.append(avg_score)
    return data_list


def calc_check_problem(problem_data):
    """
    计算检查问题信息
    :param problem_data:
    :return:
    """
    # 发现问题数
    problem_count = int(problem_data['PROBLEM_NUMBER'].sum())
    # 跟班发现问题数
    genban = int(problem_data[problem_data['IS_GENBAN'] == 1]['PROBLEM_NUMBER'].sum())
    # 发现设备监控问题数
    shebei = int(problem_data[problem_data['CHECK_WAY'] == 3]['PROBLEM_NUMBER'].sum())
    # 高质量问题
    high_problem = int(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1'])]['PROBLEM_NUMBER'].sum())
    middle_problem = int(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'C', 'E3'])]['PROBLEM_NUMBER'].sum())
    # 问题占比
    ratio1 = round(ded_zero(high_problem, problem_count) * 100, 1)
    ratio2 = round(ded_zero(middle_problem, problem_count) * 100, 1)
    problem_list = []
    # 作业、设备、管理、路外问题平均分
    for i in ['作业', '设备', '管理', '路外']:
        data = problem_data[problem_data['PROBLEM_CLASSITY_NAME'].str.contains(i)]
        station = []
        for j in data['NEW_NAME'].value_counts().index:
            new_data = data[data['NEW_NAME'] == j]
            dics = {
                'name': j,
                'avg_score': round(ded_zero(int(new_data['PROBLEM_SCORE'].sum()),
                                            len(new_data['PERSON_NAME'].value_counts())), 1)
            }
            station.append(dics)
        dic = {
            'name': i,
            'count': int(data['PROBLEM_NUMBER'].sum()),
            'ratio': round(ded_zero(int(data['PROBLEM_NUMBER'].sum()), problem_count) * 100, 1),
            'high': int(data[data['LEVEL'].isin(['A', 'B', 'F1'])]['PROBLEM_NUMBER'].sum()),
            'middle': int(data[data['LEVEL'].isin(['A', 'B', 'F1', 'C', 'E3'])]['PROBLEM_NUMBER'].sum()),
            'avg_score': round(ded_zero(int(data['PROBLEM_SCORE'].sum()), len(data['PERSON_NAME'].value_counts())), 1),
            'station': station
        }
        problem_list.append(dic)
    # 供电系统人均质量分
    score = int(problem_data['PROBLEM_SCORE'].sum())
    avg_score = round(ded_zero(score, len(problem_data['PERSON_NAME'].value_counts())), 1)
    sta_info = []
    for i in problem_data['NEW_NAME'].value_counts().index:
        data = problem_data[problem_data['NEW_NAME'] == i]
        sta_score = int(data['PROBLEM_SCORE'].sum())
        dic = {
            'name': i,
            'sta_avg': round(ded_zero(sta_score, len(data['PERSON_NAME'].value_counts())), 1)
        }
        sta_info.append(dic)
    return {
        'genban': genban,
        'shebei': shebei,
        'high_problem': high_problem,
        'middle_problem': middle_problem,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'problem_list': problem_list,
        'avg_score': avg_score,
        'sta_info': sta_info,
    }


def get_problem_three_analysis(data, month_data):
    """
    问题三性分析
    :param data:
    :param month_data:
    :return:
    """
    # 作业类问题分析
    work = get_calc_work(data, month_data)
    # 管理类问题分析
    manager = get_calc_manager(data)
    # 设备类问题分析
    shebei = get_calc_shebei(data, month_data)
    # 外部环境项分析
    env = get_clac_env(data, month_data)
    return {
        'work': work,
        'manager': manager,
        'shebei': shebei,
        'env': env
    }


def get_calc_work(data, month_data):
    """
    计算作业类问题
    :param data:
    :param month_data:
    :return:
    """
    data_list = [data, month_data]
    for i in range(2):
        data_list[i] = data_list[i][data_list[i]['PROBLEM_CLASSITY_NAME'].str.contains('作业')]
    result = []
    # 问题分项统计
    main_problem_list = ['接触网', '电力', '变配电', '轨道车', '给水']
    for j in main_problem_list:
        data_more = []
        for i in data_list:
            data_more.append(i[i['CHECK_ITEM_NAME'].str.contains(j)])
        result.append(calc_major_problem(data_more))
    return {
        'result': result,
        'main_problem_list': main_problem_list
    }


def get_calc_shebei(data, month_data):
    """
    设备问题分析
    :param data:
    :param month_data:

    :return:
    """
    data_list = [data, month_data]
    for i in range(2):
        data_list[i] = data_list[i][data_list[i]['PROBLEM_CLASSITY_NAME'].str.contains('设备')]
    result = []
    main_problem_list = ['接触网', '电力', '变配电', '轨道车']
    for j in main_problem_list:
        data_more = []
        for i in data_list:
            data_more.append(i[i['CHECK_ITEM_NAME'].str.contains(j)])
        result.append(calc_major_problem_manager(data_more))
    return {
        'result': result,
        'main_problem_list': main_problem_list
    }


def calc_major_problem(data_more):
    """
    计算主要专业问题
    :param data_more:
    :return:
    """
    data = data_more[0]
    all_number = int(data['PROBLEM_NUMBER'].sum())
    problem_3 = []
    ratio_3 = []
    new_data = data.groupby('PROBLEM_POINT').sum().sort_values(by='PROBLEM_NUMBER', ascending=False).reset_index()
    if new_data.empty is not True:
        for i in range(3 if len(new_data) > 3 else len(new_data)):
            problem_3.append(new_data.iloc[i]['PROBLEM_POINT'])
            ratio_3.append(round(ded_zero(int(new_data.iloc[i]['PROBLEM_NUMBER']), all_number), 1))
        station_name = []
        for i in problem_3:
            station_name.append(data[data['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].tolist())
        min_problem = new_data.iloc[-1]['PROBLEM_POINT']
        min_count = int(new_data.iloc[-1]['PROBLEM_NUMBER'])
        min_ratio = round(ded_zero(min_count, all_number), 1)
        problem_list = list(set(data['CHECK_ITEM_NAME'].tolist()))
        all_problem_list = []
        zicha_count = []
        for i in problem_list:
            p_data = data[data['CHECK_ITEM_NAME'] == i]
            all_problem_list.append(len(p_data))
            zicha_count.append(
                int(p_data[p_data['EXECUTE_DEPARTMENT_NAME'] == p_data['RESPONSIBILITY_DEPARTMENT_NAME']][
                        'PROBLEM_NUMBER'].sum()))
        # 自查问题
        index = zicha_count.index(min(zicha_count))
        zicha_name = problem_list[index]
        up = int(data[(data['CHECK_ITEM_NAME'] == zicha_name) & (data['TYPE'].isin([1, 2]))]['PROBLEM_NUMBER'].sum())
        zj = int(data[(data['CHECK_ITEM_NAME'] == zicha_name) & (data['TYPE'].isin([3]))]['PROBLEM_NUMBER'].sum())
        zc_count = zicha_count[index]
        zc_ratio = round(ded_zero(zc_count, all_number) * 100, 1)
        # 问题变化率
        problem_change = calc_problem_change(data_more)
        # 调阅与跟班查处作业类问题数量差距
        problem_grop = calc_problem_grop(data_more[0])
        return {
            'status': 1,
            'problem_3': problem_3,
            'ratio_3': ratio_3,
            'station_name': station_name,
            'min_problem': min_problem,
            'min_count': min_count,
            'min_ratio': min_ratio,
            'zicha_name': zicha_name,
            'up': up,
            'zj': zj,
            'zc_ratio': zc_ratio,
            'all_problem_list': all_problem_list,
            'problem_list': problem_list,
            'problem_change': problem_change,
            'problem_grop': problem_grop
        }
    else:
        return {
            'status': 0
        }


def calc_problem_change(data_more):
    """
    问题变化率
    :param data_more:
    :return:
    """
    now_data = data_more[0].groupby('CHECK_ITEM_NAME').sum().sort_values(by='PROBLEM_NUMBER',
                                                                         ascending=False).reset_index()
    now_name = now_data['CHECK_ITEM_NAME'].tolist()
    now_count = now_data['PROBLEM_NUMBER'].tolist()
    last_month_count = []
    for i in now_name:
        last_month_count.append(int(data_more[1][data_more[1]['CHECK_ITEM_NAME'] == i]['PROBLEM_NUMBER'].sum()))
    ratio = []
    for i in range(len(now_count)):
        ratio.append(calculate_year_and_ring_ratio([now_count[i], last_month_count[i]]))
    max = sorted(ratio, key=lambda x: x['month_diff'], reverse=True)[0]
    index = ratio.index(max)
    max_name = now_name[index]
    station_data = data_more[0][data_more[0]['CHECK_ITEM_NAME'] == max_name]
    station_name = station_data['NEW_NAME'].tolist()[:2]
    return {
        'max_name': max_name,
        'max': max,
        'station_name': station_name
    }


def calc_problem_grop(data):
    """
    调阅与跟班查处作业类问题数量差距
    :param data:
    :return:
    """
    number = []
    station_name = list(set(data['DEPARTMENT_ALL_NAME'].tolist()))
    shebei = []
    genban = []
    for i in station_name:
        new_data = data[data['DEPARTMENT_ALL_NAME'] == i]
        # 设备监控调阅
        shebei.append(int(new_data[new_data['CHECK_WAY'] == 3]['PROBLEM_NUMBER'].sum()))
        # 跟班检查
        genban.append(int(new_data[new_data['IS_GENBAN'] == 1]['PROBLEM_NUMBER'].sum()))
        number.append(int(new_data[new_data['CHECK_WAY'] == 3]['PROBLEM_NUMBER'].sum() - new_data[
            new_data['IS_GENBAN'] == 1]['PROBLEM_NUMBER'].sum()))
    index = number.index(max(number))
    station_name = station_name[index]
    shebei = shebei[index]
    genban = genban[index]
    return {
        'station_name': station_name,
        'shebei': shebei,
        'genban': genban
    }


def get_calc_manager(data):
    """
    管理类问题分析
    :param data:
    :return:
    """
    data = data[data['PROBLEM_CLASSITY_NAME'].str.contains('管理')]
    all_number = int(data['PROBLEM_NUMBER'].sum())
    problem_3 = []
    ratio_3 = []
    new_data = data.groupby('PROBLEM_POINT').sum().sort_values(by='PROBLEM_NUMBER', ascending=False).reset_index()
    if new_data.empty is not True:
        for i in range(3 if len(new_data) > 3 else len(new_data)):
            # 问题项点
            problem_3.append(new_data.iloc[i]['PROBLEM_POINT'])
            ratio_3.append(round(ded_zero(int(new_data.iloc[i]['PROBLEM_NUMBER']), all_number), 1))
        station_name = []
        for i in problem_3:
            # 站段名
            station_name.append(data[data['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].tolist())
        return {
            'status': 1,
            'problem_3': problem_3,
            'ratio_3': ratio_3,
            'station_name': station_name,
        }
    else:
        return {
            'status': 0
        }


def calc_major_problem_manager(data_more):
    """
    计算管理类主要专业问题
    :param data_more:
    :return:
    """
    all_number = int(data_more[0]['PROBLEM_NUMBER'].sum())
    problem_3 = []
    ratio_3 = []
    new_data = data_more[0].groupby('PROBLEM_POINT').sum().sort_values(by='PROBLEM_NUMBER', ascending=False).reset_index()
    if new_data.empty is not True:
        for i in range(3 if len(new_data) > 3 else len(new_data)):
            problem_3.append(new_data.iloc[i]['PROBLEM_POINT'])
            ratio_3.append(round(ded_zero(int(new_data.iloc[i]['PROBLEM_NUMBER']), all_number), 1))
        station_name = []
        lm_count = []
        now_count = []
        for i in problem_3:
            station_name.append(data_more[0][data_more[0]['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].tolist())
            now_count.append(int(data_more[0][data_more[0]['PROBLEM_POINT'] == i]['PROBLEM_NUMBER'].sum()))
            lm_count.append(int(data_more[1][data_more[1]['PROBLEM_POINT'] == i]['PROBLEM_NUMBER'].sum()))
        ratio_list = []
        for i in range(3):
            ratio_list.append(calculate_year_and_ring_ratio([now_count[i], lm_count[i]]))
        shebei_data = data_more[0].groupby('DEPARTMENT_ALL_NAME').sum().sort_values(by='PROBLEM_NUMBER',
                                                                                    ascending=False).reset_index()
        # 设备类问题
        max_name = shebei_data.iloc[0]['DEPARTMENT_ALL_NAME']
        max_number = int(shebei_data.iloc[0]['PROBLEM_NUMBER'])
        # 整改
        zg = len(shebei_data[shebei_data['STATUS'] == 2])
        zg_ratio = round(ded_zero(zg, all_number) * 100, 1)
        # 严重设备质量问题
        serious_problem = []
        for i in data_more:
            serious_problem.append(int(i[i['LEVEL'].isin(['E1', 'E2'])]['PROBLEM_NUMBER'].sum()))
        serious_ratio = calculate_year_and_ring_ratio([serious_problem[0], serious_problem[1]])
        return {
            'status': 1,
            'problem_3': problem_3,
            'ratio_3': ratio_3,
            'station_name': station_name,
            'ratio_list': ratio_list,
            'max_name': max_name,
            'max_number': max_number,
            'serious_problem': serious_problem[0],
            'serious_ratio': serious_ratio,
            'zg': zg,
            'zg_ratio': zg_ratio
        }
    else:
        return {
            'status': 0
        }


def get_clac_env(data, month_data):
    """
    外部环境问题分析
    :param data:
    :param month_data:
    :return:
    """
    data_list = [data, month_data]
    for i in range(2):
        data_list[i] = data_list[i][data_list[i]['PROBLEM_CLASSITY_NAME'].str.contains('路外')]
    problem_count = []
    for i in data_list:
        problem_count.append(int(i['PROBLEM_NUMBER'].sum()))
    ratio = calculate_year_and_ring_ratio(problem_count)
    new_data = data_list[0].groupby('PROBLEM_POINT').sum().sort_values(by='PROBLEM_NUMBER',
                                                                       ascending=False).reset_index()
    point_name = new_data.iloc[0]['PROBLEM_POINT']
    main_problem_list = ['接触网', '电力']
    result = []
    for j in main_problem_list:
        result.append(calc_major_problem_env(data_list[0], j))
    return {
        'problem_count': problem_count[0],
        'ratio': ratio,
        'point_name': point_name,
        'result': result,
        'main_problem_list': main_problem_list
    }


def calc_major_problem_env(data, str1):
    """
    计算管理类主要专业问题
    :param data_more:
    :return:
    """
    data = data[data['CHECK_ITEM_NAME'].str.contains(str1)]
    problem_count = int(data['PROBLEM_NUMBER'].sum())
    new_data = data.groupby('DEPARTMENT_ALL_NAME').sum().sort_values(by='PROBLEM_NUMBER', ascending=False).reset_index()
    zg = len(data[data['STATUS'] == 2])
    zg_ratio = round(ded_zero(zg, problem_count) * 100, 1)
    station_name = []
    for i in range(2 if len(new_data) >= 2 else len(new_data)):
        station_name.append(new_data.iloc[i]['DEPARTMENT_ALL_NAME'])
    return {
        'problem_count': problem_count,
        'station_name': station_name,
        'zg': zg,
        'zg_ratio': zg_ratio
    }


# --------------------------------------------------------------第三部分-----------------------------------------------------------------
def get_third(start_date, end_date, last_month_start, last_month_end):
    duty_data = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date))
    last_month_duty = pd_query(CHECK_SAFETY_INFO_SQL.format(last_month_start, last_month_end))
    check_problem_data = pd_query(check_problem_sql.format(start_date, end_date))
    month_check_problem_data = pd_query(check_problem_sql.format(last_month_start, last_month_end))
    check_evaluate_data = pd_query(CHECK_EVALUATE_SCORE_SQL.format(start_date, end_date))
    # 整体检查履职质量分析
    third_one = get_third_one(duty_data, last_month_duty, check_problem_data, month_check_problem_data,
                              check_evaluate_data, start_date, end_date)
    # 干部履职评价分析
    third_two = get_evaluate_situation_base_doc(start_date, end_date, last_month_start, last_month_end)
    return {
        'third_one': third_one,
        'third_two': third_two,
    }


def get_third_one(duty_data, last_month_duty, check_problem_data, month_check_problem_data,
                  check_evaluate_data, start_date, end_date):
    """
    整体检查履职质量分析
    :param duty_data:
    :param last_month_duty:
    :param check_problem_data:
    :param month_check_problem_data:
    :param check_evaluate_data:
    :param start_date:
    :param end_date:
    :return:
    """
    evaluate_info1 = pd_query(check_evaluate_sql.format(start_date, end_date))
    station_carde_count = pd_query(cards)
    check_info = pd_query(check_info_sql.format(start_date, end_date))
    # 整体履职质量分析
    station_evaluate_analysis = get_station_evaluate_analysis(duty_data)
    # 事故故障情况
    acc_situation = get_acc_situation(duty_data, last_month_duty)
    # 监督检查情况
    check_situation = get_check_situation(check_problem_data, month_check_problem_data)
    # 履职计分
    evaluate_score = get_evaluate_score(check_evaluate_data)
    # 站段领导班子检查质量分析
    table = get_carde_quality_analysis(evaluate_info1, end_date, station_carde_count, check_info, check_problem_data)
    juguan_evaluate = evaluate_info1[evaluate_info1['BUSINESS_CLASSIFY'] == '领导']
    juguan_leader_evaluate_table = _get_evaluate_table(juguan_evaluate)
    return {
        'station_evaluate_analysis': station_evaluate_analysis,
        'acc_situation': acc_situation,
        'check_situation': check_situation,
        'evaluate_score': evaluate_score,
        "table": table,
        "juguan_leader_evaluate_table": juguan_leader_evaluate_table
    }


def get_station_evaluate_analysis(data):
    """
    整体履职质量分析
    :param data:
    :return:
    """
    data = data[data['MAIN_TYPE'].isin([1, 2])]
    pass


def get_acc_situation(duty_data, last_month_duty):
    """
    事故故障情况
    :param duty_data:
    :param last_month_duty:
    :return:
    """
    data_list = [duty_data, last_month_duty]
    # 行车责任事故
    car_duty = []
    shebei = []
    d21 = []
    for i in data_list:
        car_duty.append(len(
            i[(i['RISK_NAME'].str.contains('行车')) & (i['MAIN_TYPE'] == 1) & (i['RESPONSIBILITY_IDENTIFIED'] != 11)]))
        shebei_data = i[(i['RISK_NAME'].str.contains('设备')) & (i['MAIN_TYPE'] == 1)]
        shebei.append(len(shebei_data))
        d21.append(len(shebei_data[shebei_data['CODE'].isin(['D21'])]))
    car_ratio = calculate_year_and_ring_ratio(car_duty)
    shebei_ratio = calculate_year_and_ring_ratio(shebei)
    return {
        'car_duty': car_duty[0],
        'car_ratio': car_ratio,
        'shebei': shebei[0],
        'd21': d21[0],
        'shebei_ratio': shebei_ratio
    }


def get_check_situation(check_problem_data, month_check_problem_data):
    """
    监督检查情况
    :param check_problem_data:
    :param month_check_problem_data:
    :return:
    """
    data_list = [check_problem_data, month_check_problem_data]
    check_count = []
    find_problem = []
    main_problem = []
    score = []
    # 发现问题数， 严重风险问题数， 问题得分
    for i in data_list:
        check_count.append(len(i))
        find_problem.append(int(i['PROBLEM_NUMBER'].sum()))
        main_problem.append(int(i[i['RISK_LEVEL'].isin([1, 2])]['PROBLEM_NUMBER'].sum()))
        score.append(int(i['PROBLEM_SCORE'].sum()))
    # 计算环比同比
    ratio1 = calculate_year_and_ring_ratio(check_count)
    ratio2 = calculate_year_and_ring_ratio(find_problem)
    ratio3 = calculate_year_and_ring_ratio(main_problem)
    ratio4 = calculate_year_and_ring_ratio(score)
    return {
        'check_count': check_count[0],
        'find_problem': find_problem[0],
        'main_problem': main_problem[0],
        'score': score[0],
        'ratio1': ratio1,
        'ratio2': ratio2,
        'ratio3': ratio3,
        'ratio4': ratio4,
    }


def get_evaluate_score(data):
    """
    履职评价计分
    :param data:
    :return:
    """
    # 评价人次
    count = len(data)
    # 量化指标完成
    # 检查信息录入、监督质量检查、考核责任落实、问题闭环管理、重点工作落实
    problem_list = ['量化指标完成', '检查信息录入', '监督质量检查', '考核责任落实', '问题闭环管理', '重点工作落实']
    problem_dic = {}
    for i in problem_list:
        problem_dic[i] = len(data[data['ITEM_NAME'] == i])
    return {
        'count': count,
        'problem_dic': problem_dic,
    }


def get_carde_quality_analysis(evaluate_info1, end_date, station_carde_count, check_info, problem_data):
    # 系统平均
    person = station_carde_count[station_carde_count['ALL_NAME'].str.contains('领导')]
    info = check_info[check_info['ALL_NAME'].str.contains('领导')]
    pro = problem_data[problem_data['ALL_NAME'].str.contains('领导')]
    eva = evaluate_info1[evaluate_info1['ALL_NAME'].str.contains('领导')]
    sys_result = calc_table_data(info, len(person), pro, eva)
    # 站段平均
    station_list = []
    for i in ['贵阳北供电段', '贵阳供电段', '成都供电段', '达州供电段', '西昌供电段', '重庆供电段']:
        sta_info = info[info['NAME'] == i]
        sta_person = person[person['STATION'] == i]
        sta_pro = pro[pro['NAME'] == i]
        sta_eva = eva[eva['STATION'] == i]
        station_result = calc_table_data(sta_info, len(sta_person), sta_pro, sta_eva)
        # 安全综合指数
        date = datetime.datetime.strptime(end_date, "%Y-%m-%d")
        if date.day >= current_app.config.get('UPDATE_DAY'):
            date = date + relativedelta(months=1)
        month = date.year * 100 + date.month
        health_index = _get_health_index(month, i)
        if health_index.empty is True:
            health_score = ''
            health_rank = ''
        else:
            health_score = float(health_index['SCORE'])
            health_rank = int(health_index['RANK'])
        station_list.append({
            'name': i,
            'station_result': station_result,
            'health_score': health_score,
            'health_rank': health_rank,
        })
    return {
        'sys_result': sys_result,
        'station_list': station_list
    }


def calc_table_data(info, person, pro, eva):
    """
    计算表格中需要的数据
    :return:
    """
    # 人均现场检查次数
    xc_count = len(info[info['CHECK_WAY'] == 1])
    xc_avg = round(ded_zero(xc_count, person), 1)
    # 人均夜查次数
    yc_count = len(info[info['IS_YECHA'] == 1])
    yc_avg = round(ded_zero(yc_count, person), 1)
    # 人均高质量问题数
    high_count = len(pro[pro['LEVEL'].isin(['A', 'B', 'F1', 'C', 'E3'])])
    high_avg = round(ded_zero(high_count, person), 1)
    # 人均发现问题质量分
    score = int(pro['PROBLEM_SCORE'].sum())
    score_avg = round(ded_zero(score, person), 1)
    # 人均发现问题数
    find_pro = int(info['PROBLEM_NUMBER'].sum())
    find_avg = round(ded_zero(find_pro, person), 1)
    # 人均调阅时长
    cost_time = int(info['COST_TIME'].sum())
    cost_avg = round(ded_zero(cost_time, person))
    # 人均安全履职得分
    safety_score = int(eva['SCORE'].sum())
    safety_avg = round(ded_zero(safety_score, person), 1)
    # 被评价计分
    eva_data = eva
    luju = eva_data[eva_data['CHECK_TYPE'] == 1]
    luju_content = luju.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
    luju_list = []
    for j, k in luju_content.iterrows():
        luju_list.append({
            'code': k['CODE'],
            'count': int(k['CODE_ADDITION'])
        })
    zd = eva_data[eva_data['CHECK_TYPE'] == 2]
    zd_content = zd.groupby('CODE').count().sort_values(by='CODE_ADDITION').reset_index()
    zd_list = []
    for j, k in zd_content.iterrows():
        zd_list.append({
            'code': k['CODE'],
            'count': int(k['CODE_ADDITION'])
        })
    return {
        'xc_count': xc_count,
        'xc_avg': xc_avg,
        'yc_count': yc_count,
        'yc_avg': yc_avg,
        'high_count': high_count,
        'high_avg': high_avg,
        'score': score,
        'score_avg': score_avg,
        'find_pro': find_pro,
        'find_avg': find_avg,
        'cost_time': cost_time,
        'cost_avg': cost_avg,
        'safety_score': safety_score,
        'safety_avg': safety_avg,
        'eva_count': len(eva_data),
        'luju': len(luju),
        'luju_list': luju_list,
        'zd': len(zd),
        'zd_list': zd_list
    }


def _get_health_index(month, station_name):
    """
    安全综合指数得分及排名
    :param month:
    :return:
    """
    prefix_list = ['monthly_', 'history_']
    for prefix in prefix_list:
        coll_name = prefix + 'health_index'
        doc = list(mongo.db[coll_name].find({
            'MON': month,
            'MAJOR': '供电',
            'DEPARTMENT_NAME': station_name
        }, {
            "_id": 0,
            "DEPARTMENT_NAME": 1,
            "SCORE": 1,
            "RANK": 1
        }))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def _get_evaluate_table(evaluate):
    evaluate_way = {
        0: "自动评价",
        1: "逐条评价",
        2: "定期评价",
        3: "阶段评价",
    }
    major_evaluate_table = []
    for index in evaluate.index:
        value = evaluate.loc[index]
        major_evaluate_table.append({
            "all_name":
                value['ALL_NAME'],
            "name":
                value['RESPONSIBE_PERSON_NAME'],
            "job":
                value['GRADATION'],
            "y_m":
                f"{value['YEAR']}/{value['MONTH']:0>2}",
            "time":
                value['CREATE_TIME'],
            "way":
                evaluate_way[value['EVALUATE_WAY']],
            "content":
                value['EVALUATE_CONTENT'],
            "score":
                int(value['SCORE'])
        })
    return major_evaluate_table


def get_evaluate_situation_base_doc(start_date, end_date, last_month_start, last_month_end):
    """获取履职情况简要统计分析数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    # 公用数据
    global evaluate_info, last_month_evaluate_info
    # 当月履职评价
    evaluate_info = pd_query(check_evaluate_sql.format(start_date, end_date))
    evaluate_info['NEW_NAME'] = evaluate_info['ALL_NAME'].apply(lambda x: x.split('-')[0])
    # 上月履职评价
    last_month_evaluate_info = pd_query(check_evaluate_sql.format(last_month_start, last_month_end))
    last_month_evaluate_info['NEW_NAME'] = last_month_evaluate_info['ALL_NAME'].apply(lambda x: x.split('-')[0])
    global evaluate_year_info
    # 今年全部履职评价
    evaluate_year_info = pd_query(check_evaluate_sql.format(f'{end_date[:4]}-01-01', end_date))
    evaluate_year_info['NEW_NAME'] = evaluate_year_info['ALL_NAME'].apply(lambda x: x.split('-')[0])
    if evaluate_info.empty:
        return {}
    # 第一块 干部评价总体分析
    first_paragraph = get_evaluate_whole_situation(start_date, end_date)
    evaluate_problem_type_table = get_evaluate_problem_type_table(
        start_date, end_date)
    # 第二块 各科室、车间干部履职评价情况分析
    major_evaluate_situation = get_major_evaluate_situation(evaluate_info, last_month_evaluate_info)
    # 第三块 各级干部检查质量分析
    cadres_check_quality = get_cadres_check_quality(evaluate_info, start_date, end_date)
    result = {
        'evaluate_total_analysis': {
            "total_evaluate": first_paragraph,
            "evaluate_problem_type_table": evaluate_problem_type_table
        },
        'major_evaluate_situation': major_evaluate_situation,
        'cadres_check_quality': cadres_check_quality
    }
    return result


level_name = {
    1: '∑＜2',
    2: '2≤∑＜4',
    3: '4≤∑＜6',
    4: '6≤∑＜8',
    5: '8≤∑＜10',
    6: '10≤∑＜12',
    7: '∑≥12'
}

all_items = [
    '量化指标完成', '检查信息录入', '监督检查质量', '考核责任落实', '问题闭环管理', '重点工作落实', '音视频运用管理',
    '履职评价管理', '事故故障追溯', '弄虚作假'
]


def get_evaluate_whole_situation(start_date, end_date):
    """干部评价总体情况分析

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    evaluate_review = pd_query(check_evaluate_review_sql.format(start_date, end_date))
    total_ring = get_ring_ratio(evaluate_info.shape[0], last_month_evaluate_info.shape[0])
    # 定期评价人次
    regular_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 2].shape[0]
    regular_luju = evaluate_review[(evaluate_review['CHECK_TYPE'] == 1) & (evaluate_review['EVALUATE_WAY'] == 2)].shape[
        0]
    regular_duan = evaluate_review[(evaluate_review['CHECK_TYPE'] == 2) & (evaluate_review['EVALUATE_WAY'] == 2)].shape[
        0]
    # 逐条评价人次
    one_count = evaluate_review[evaluate_review['EVALUATE_WAY'] == 1].shape[0]
    one_luju = evaluate_review[(evaluate_review['CHECK_TYPE'] == 1) & (evaluate_review['EVALUATE_WAY'] == 1)].shape[0]
    one_duan = evaluate_review[(evaluate_review['CHECK_TYPE'] == 2) & (evaluate_review['EVALUATE_WAY'] == 1)].shape[0]
    # 履职问题个数
    evaluate_count = evaluate_info.shape[0]
    # 履职问题人数
    evaluate_person = len(set(evaluate_info['RESPONSIBE_ID_CARD']))
    # 工人履职数据
    work_data = evaluate_info[evaluate_info['LEVEL'] == "工人"]
    # 工人条数和人数
    work_count, work_person = work_data.shape[0], len(
        set(work_data['RESPONSIBE_ID_CARD']))
    # 安全谈心数据
    talk_data = evaluate_info[evaluate_info['ITEM_NAME'] == '安全谈心']
    talk_count, talk_person = talk_data.shape[0], len(
        set(talk_data['RESPONSIBE_ID_CARD']))
    # 共计评价得分
    total_score = sum(list(evaluate_info['SCORE']))
    # 最高评价计分
    score_data = evaluate_info.groupby('ALL_NAME').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    most_score = int(score_data.at[0, 'SCORE'])
    most_name = score_data.iloc[0]['ALL_NAME']
    more_score = int(max(evaluate_info['SCORE'].tolist()))
    # 今年得分数据
    score_year_data = evaluate_year_info.groupby(
        'RESPONSIBE_ID_CARD').sum().sort_values(
        by='SCORE', ascending=False).reset_index()
    year_total_count = score_year_data.shape[0]
    month_percent = round(ded_zero(evaluate_count, year_total_count) * 100, 1)
    most_year_score = int(score_year_data.at[0, 'SCORE'])
    punish_year_count = score_year_data[score_year_data['SCORE'] >= 2].shape[0]
    punish_count = score_data[score_data['SCORE'] >= 2].shape[0]
    score_data['level'] = score_data['SCORE'].apply(singe_score_section)
    score_year_data['level'] = score_year_data['SCORE'].apply(
        singe_score_section)
    levels = [7, 6, 5, 4, 3, 2, 1]
    p_level_rst = []
    for level in levels:
        p_level_rst.append({
            "level":
                level_name.get(level),
            "total_count":
                score_year_data[score_year_data['level'] == level].shape[0],
            "month_count":
                score_data[score_data['level'] == level].shape[0]
        })
    score_rst = {
        "year_total_count": year_total_count,
        "month_count": evaluate_count,
        "month_percent": month_percent,
        "most_year_score": most_year_score,
        "punish_year_count": punish_year_count,
        "punish_count": punish_count,
        "p_level_value": p_level_rst,
        "t_level_value": p_level_rst[::-1]
    }
    rst = {
        'regular_count': regular_count,
        'regular_luju': regular_luju,
        'regular_duan': regular_duan,
        'one_count': one_count,
        'one_luju': one_luju,
        'one_duan': one_duan,
        'evaluate_count': evaluate_count,
        'evaluate_person': evaluate_person,
        'work_count': work_count,
        'work_person': work_person,
        'talk_count': talk_count,
        'talk_person': talk_person,
        'total_score': total_score,
        'most_score': most_score,
        'most_name': most_name,
        'more_score': more_score,
        'total_ring': total_ring,
        'score_level_value': score_rst
    }
    return rst


majors = ['供电']


def get_evaluate_problem_type_table(start_date, end_date):
    """履职问题分类数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    total = evaluate_info[evaluate_info['ITEM_NAME'].isin(all_items)].shape[0]
    counts = []
    percents = []
    rings = []
    detail_item = []
    majors_rst = []
    for item in all_items:
        major_item_rst = []
        if total == 0:
            count, percent, ring = 0, 0, 0
            for major in majors:
                major_item_rst.append({'name': major, 'count': [], 'ring': []})
        else:
            item_info = evaluate_info[evaluate_info['ITEM_NAME'] == item]
            last_item_info = last_month_evaluate_info[
                last_month_evaluate_info['ITEM_NAME'] == item]
            count, last_count = len(item_info), len(last_item_info)
            percent = round(ded_zero(count, total) * 100, 1)
            ring = get_ring_ratio(count, last_count)
            # 各专业系统分类
            if item_info.empty is False:
                major_item_rst = _get_major_evaluate_table(
                    item_info, last_item_info)
                # 具体问题分类数据
                item_info = item_info.groupby(['CODE', 'SITUATION'])
                for name, group in item_info:
                    score_count = len(group)
                    score_person = int(
                        group['RESPONSIBE_ID_CARD'].value_counts().shape[0])
                    detail_item.append({
                        'detail_name': name[1],
                        'code': name[0],
                        'name': item,
                        'score_count': score_count,
                        'score_person': score_person
                    })
            else:
                for major in majors:
                    major_item_rst.append({
                        'name': major,
                        'count': 0,
                        'ring': 0
                    })
                major_item_rst.append({'name': '合计', 'count': 0, 'ring': 0})
            majors_rst.append(major_item_rst)
        counts.append(count)
        percents.append(percent)
        rings.append(ring)
    detail_data = pd.DataFrame(detail_item)
    problem_score_table_data = detail_data.iloc[detail_data.groupby(
        ['name']).apply(lambda x: x['score_count'].idxmax())].sort_values(
        by='score_count', ascending=False).head(6)
    limit = [5, 4, 5, 3, 4, 5, 3, 5, 5, 10000]
    problem_rst = []
    for index in problem_score_table_data.index:
        item_name = problem_score_table_data.at[index, 'name']
        detail_item_name = problem_score_table_data.at[index, 'detail_name']
        detail_item_count = int(
            problem_score_table_data.at[index, 'score_count'])
        item_data = detail_data[detail_data['name'] == item_name].sort_values(
            by='score_count',
            ascending=False).head(limit[all_items.index(item_name)])
        item_count = sum(list(item_data['score_count']))
        item_person = sum(list(item_data['score_person']))
        item_rst = []
        for idx in item_data.index:
            detail_name = item_data.at[idx, 'detail_name']
            detail_count = int(item_data.at[idx, 'score_count'])
            detail_person = int(item_data.at[idx, 'score_person'])
            item_rst.append({
                'detail_name': detail_name,
                'detail_count': detail_count,
                'detail_person': detail_person
            })
        problem_rst.append({
            "name":
                item_name,
            "item_count":
                item_count,
            "item_person":
                item_person,
            "detail_name":
                detail_item_name,
            "detail_count":
                detail_item_count,
            "detail_percent":
                round(ded_zero(detail_item_count, item_count) * 100, 1),
            "item_problem":
                item_rst
        })
    result = {
        'names': all_items,
        'counts': counts,
        'percents': percents,
        'rings': rings,
        'major_value': majors_rst,
        'problem_value': problem_rst
    }
    return result


def _get_major_evaluate_table(item_info, last_item_info):
    """专业分类数据

    Arguments:
        item_info {dataframe} -- 当前月份数据
        last_item_info {dataframe} -- 上月数据

    Returns:
        dict -- 结果
    """
    item_info = item_info[item_info['MAJOR'].isin(majors)]
    last_item_info = last_item_info[last_item_info['MAJOR'].isin(majors)]
    major_item_rst = []
    for major in majors:
        major_info = item_info[item_info['MAJOR'] == major]
        major_month_info = last_item_info[last_item_info['MAJOR'] == major]
        major_count = major_info.shape[0]
        major_last_count = major_month_info.shape[0]
        major_ring = get_ring_ratio(major_count, major_last_count)
        major_item_rst.append({
            'name': major,
            'count': major_count,
            'ring': major_ring
        })
    total = item_info.shape[0]
    last_total = last_item_info.shape[0]
    total_ring = get_ring_ratio(total, last_total)
    major_item_rst.append({'name': '合计', 'count': total, 'ring': total_ring})
    return major_item_rst


def get_major_evaluate_situation(evaluate_info, last_month_evaluate_info):
    """
    各科室、车间干部履职评价情况分析
    :param evaluate_info:
    :return:
    """

    last_month_evaluate_info['NEW_NAME'] = last_month_evaluate_info['ALL_NAME'].apply(lambda x: x.split('-')[0])
    # 各科室、车间干部履职问题分析
    problem_analysis = get_problem_analysis(evaluate_info)
    # 各科室、车间干部履职记分分析
    score_analysis = get_score_analysis(evaluate_info, last_month_evaluate_info)
    # 正科职干部履职评价简要分析
    zk_evaluate_analysis = get_zk_evaluate_analysis(evaluate_info, last_month_evaluate_info, '正科')
    # 副科职干部履职评价简要分析
    fk_evaluate_analysis = get_zk_evaluate_analysis(evaluate_info, last_month_evaluate_info, '副科')
    # 一般管理干部履职评价简要分析
    general_evaluate_analysis = get_zk_evaluate_analysis(evaluate_info, last_month_evaluate_info, '一般管理')
    return {
        'problem_analysis': problem_analysis,
        'score_analysis': score_analysis,
        'zk_evaluate_analysis': zk_evaluate_analysis,
        'fk_evaluate_analysis': fk_evaluate_analysis,
        'g_evaluate_analysis': general_evaluate_analysis
    }


def get_problem_analysis(evaluate_info):
    """
    各科室、车间干部履职问题分析
    :param evaluate_info:
    :return:
    """
    station_name = list(set(evaluate_info['NEW_NAME'].tolist()))
    station_data = []
    # 各级干部得分
    for i in station_name:
        l1 = []
        l1.append(i)
        l1.append(len(evaluate_info[(evaluate_info['NEW_NAME'] == i) & (evaluate_info['LEVEL'].str.contains('处'))]))
        l1.append(len(evaluate_info[(evaluate_info['NEW_NAME'] == i) & (evaluate_info['LEVEL'].str.contains('科'))]))
        l1.append(len(evaluate_info[(evaluate_info['NEW_NAME'] == i) & (evaluate_info['LEVEL'].str.contains('一般管理'))]))
        l1.append(l1[1] + l1[2] + l1[3])
        l1.append(int(evaluate_info[evaluate_info['NEW_NAME'] == i]['SCORE'].sum()))
        station_data.append(l1)
    count = [0, 0, 0, 0, 0]
    # 总分数
    for i in range(len(station_data)):
        count[0] = count[0] + station_data[i][1]
        count[1] = count[1] + station_data[i][2]
        count[2] = count[2] + station_data[i][3]
        count[3] = count[3] + station_data[i][4]
        count[4] = count[4] + station_data[i][5]
    return {
        'station_data': station_data,
        'count': count
    }


def get_score_analysis(evaluate_info, last_month_evaluate_info):
    """
    各科室、车间干部履职记分分析
    :param evaluate_info:
    :return:
    """
    data = evaluate_info[evaluate_info['ALL_NAME'].str.contains('车间')]
    station_name = list(set(data['NEW_NAME'].tolist()))
    person_number = []
    max_score = []
    for i in station_name:
        new_data = data[data['NEW_NAME'] == i]
        person_number.append(len(new_data))
        max_score.append(int(max(new_data['SCORE'].tolist())))
    # 部门干部累计记分统计表
    data_list = [evaluate_info, last_month_evaluate_info]
    new_name = list(set(evaluate_info['NEW_NAME'].tolist()))
    station_data_list = [[], []]
    x = 0
    for i in data_list:
        for j in new_name:
            l1 = [j]
            score_data = i[i['NEW_NAME'] == j].groupby('RESPONSIBE_PERSON_NAME').sum().sort_values(by='SCORE',
                                                                                                   ascending=False).reset_index()
            l1.append(len(score_data[score_data['SCORE'] < 2]))
            l1.append(len(score_data[(score_data['SCORE'] >= 2) & (score_data['SCORE'] < 4)]))
            l1.append(len(score_data[(score_data['SCORE'] >= 4) & (score_data['SCORE'] < 6)]))
            l1.append(len(score_data[(score_data['SCORE'] >= 6) & (score_data['SCORE'] < 8)]))
            l1.append(len(score_data[(score_data['SCORE'] >= 8) & (score_data['SCORE'] < 10)]))
            l1.append(len(score_data[(score_data['SCORE'] >= 10) & (score_data['SCORE'] < 12)]))
            l1.append(len(score_data[score_data['SCORE'] >= 12]))
            l1.append(sum(l1[1:]))
            station_data_list[x].append(l1)
        x = x + 1
    all_data = [0, 0, 0, 0, 0, 0, 0, 0, 0]
    # 各分段累计计分
    for i in range(len(station_data_list[0])):
        all_data[0] = all_data[0] + station_data_list[0][i][1]
        all_data[1] = all_data[1] + station_data_list[0][i][2]
        all_data[2] = all_data[2] + station_data_list[0][i][3]
        all_data[3] = all_data[3] + station_data_list[0][i][4]
        all_data[4] = all_data[4] + station_data_list[0][i][5]
        all_data[5] = all_data[5] + station_data_list[0][i][6]
        all_data[6] = all_data[6] + station_data_list[0][i][7]
        all_data[7] = all_data[7] + station_data_list[0][i][8]
        all_data[8] = all_data[8] + station_data_list[1][i][8]

    return {
        'station_name': station_name,
        'person_number': person_number,
        'max_score': max_score,
        'station_data_list': station_data_list,
        'all_data': all_data
    }


def get_zk_evaluate_analysis(evaluate_info, last_month_evaluate_info, str1):
    """
    干部履职评价分析
    :param evaluate_info:
    :return:
    """
    # 问题等级
    data = evaluate_info[evaluate_info['LEVEL'].str.contains(str1)]
    last_data = last_month_evaluate_info[last_month_evaluate_info['LEVEL'].str.contains(str1)]
    # 问题得分
    score_count = len(data[data['SCORE'] != 0])
    score_person = len(data.groupby('RESPONSIBE_PERSON_NAME'))
    # 问题描述
    main_problem = list(set(data['SITUATION'].tolist()))[:5]
    station_name = list(set(data['NEW_NAME'].tolist()))
    problem_count = []
    problem_number = []
    for i in station_name:
        station_data = data[data['NEW_NAME'] == i]
        problem_count.append(len(station_data))
        problem_number.append(len(list(set(station_data['RESPONSIBE_PERSON_NAME'].tolist()))))
    up_5 = [[], [], [], [], []]
    up_5_data = data.groupby(['RESPONSIBE_PERSON_NAME', 'NEW_NAME'], as_index=False).sum().sort_values(by='SCORE',
                                                                                                       ascending=False).reset_index()
    for i in range(5):
        up_5[i].append(up_5_data.iloc[i]['NEW_NAME'])
        up_5[i].append(up_5_data.iloc[i]['RESPONSIBE_PERSON_NAME'])
        up_5[i].append(int(up_5_data.iloc[i]['SCORE']))
    # 存在主要问题
    # 累计记分统计表
    data_list = [data, last_data]
    station_data_list = []
    x = 0
    for i in data_list:
        l1 = []
        score_data = i.groupby(['RESPONSIBE_PERSON_NAME', 'NEW_NAME']).sum().sort_values(by='SCORE',
                                                                                         ascending=False).reset_index()
        l1.append(len(score_data[score_data['SCORE'] < 2]))
        l1.append(len(score_data[(score_data['SCORE'] >= 2) & (score_data['SCORE'] < 4)]))
        l1.append(len(score_data[(score_data['SCORE'] >= 4) & (score_data['SCORE'] < 6)]))
        l1.append(len(score_data[(score_data['SCORE'] >= 6) & (score_data['SCORE'] < 8)]))
        l1.append(len(score_data[(score_data['SCORE'] >= 8) & (score_data['SCORE'] < 10)]))
        l1.append(len(score_data[(score_data['SCORE'] >= 10) & (score_data['SCORE'] < 12)]))
        l1.append(len(score_data[score_data['SCORE'] >= 12]))
        station_data_list.append(l1)
        x = x + 1
    all_data = [0, 0, 0, 0, 0, 0, 0]
    all_data[0] = station_data_list[0][0] - station_data_list[1][0]
    all_data[1] = station_data_list[0][1] - station_data_list[1][1]
    all_data[2] = station_data_list[0][2] - station_data_list[1][2]
    all_data[3] = station_data_list[0][3] - station_data_list[1][3]
    all_data[4] = station_data_list[0][4] - station_data_list[1][4]
    all_data[5] = station_data_list[0][5] - station_data_list[1][5]
    all_data[6] = station_data_list[0][6] - station_data_list[1][6]
    return {
        'score_count': score_count,
        'score_person': score_person,
        'main_problem': main_problem,
        'station_name': station_name,
        'problem_count': problem_count,
        'problem_number': problem_number,
        'up_5': up_5,
        'station_data_list': station_data_list,
        'all_data': all_data
    }


def get_cadres_check_quality(evaluate_info, start_date, end_date):
    """
    各级干部检查质量分析
    :param evaluate_info:
    :return:
    """
    evaluate_info = evaluate_info[evaluate_info['CHECK_TYPE'] == 1]
    # 职责履行有差距
    check1 = get_gw_safety_job(evaluate_info, ['ZD-5-3', 'ZL-6-4'])
    # 风险防控有差距
    check2 = get_gw_safety_job(evaluate_info, ['ZL-6-4', 'ZD-8-3', 'ZL-1-1'])
    # 设备管控有差距
    check3 = get_gw_safety_job(evaluate_info, ['ZL-3-2', 'ZL-1-1'])
    # 针对性检查有差距
    check4 = get_gw_safety_job(evaluate_info, ['ZL-1-1', 'ZL-7-1'])
    # 关键查处有差距
    check5 = get_gw_safety_job(evaluate_info, ['ZD-5-3', 'ZL-7-1'])
    # 作业现场管控有差距
    check6 = get_gw_safety_job(evaluate_info, ['ZL-4-1', 'ZL-4-2'])
    # 专业管理能力欠缺
    check7 = get_gw_safety_job(evaluate_info, ['ZD-5-1', 'ZD-5-2'])
    # 专业管理松懈
    check8 = get_gw_safety_job(evaluate_info, ['ZD-5-1', 'ZD-5-2', 'ZD-5-3'])
    # 专业检查指导不力
    check9 = get_gw_safety_job(evaluate_info, ['ZD-5-1', 'ZD-5-2', 'ZD-5-3'])
    # 音视频调阅缺乏针对性、有效性
    check10 = get_gw_safety_job(evaluate_info, ['ZL-5-2'])
    # 音视频调阅分析不认真
    check11 = get_gw_safety_job(evaluate_info, ['ZL-5-1'])
    # 音视频调阅工作开展不力
    check12, check13 = get_fxs_check_result(start_date, end_date)
    # 车间音视频管理薄弱
    # check13 =
    # 重点工作贯彻落实不力
    check14 = get_gw_safety_job(evaluate_info, ['ZD-5-1', 'ZD-5-2', 'ZD-8-1', 'ZD-8-2'])
    # 重点工作检查督促不够
    check15 = get_gw_safety_job(evaluate_info, ['ZD-5-3', 'ZD-8-3'])
    # 问题整改不彻底
    check16 = get_gw_safety_job1(evaluate_info, ['ZG-1', 'ZG-2', 'ZG-3'])
    # 问题整改不认真
    check17 = get_gw_safety_job1(evaluate_info, ['ZG-1', 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5'])
    # 问题整改不把关
    check18 = get_gw_safety_job1(evaluate_info, ['ZG-1', 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5'])
    # 考核质量分析
    check19 = get_gw_safety_job1(evaluate_info, ['KH-1', 'KH-2'])
    return {
        'gw_safety': check1,
        'risk_def': check2,
        'shebei': check3,
        'check4': check4,
        'check5': check5,
        'check6': check6,
        'check7': check7,
        'check8': check8,
        'check9': check9,
        'check10': check10,
        'check11': check11,
        'check12': check12[:10],
        'check13': check13[:10],
        'check14': check14,
        'check15': check15,
        'check16': check16,
        'check17': check17,
        'check18': check18,
        'check19': check19
    }


def get_gw_safety_job(evaluate_info, ls1):
    """
    职责数据计算
    :param evaluate_info:
    :return:
    """
    data = evaluate_info[evaluate_info['CODE_ADDITION'].isin(ls1)]
    detail_count = []
    detail_person = []
    for i in ls1:
        detail_count.append(len(data[data['CODE_ADDITION'] == i]))
        detail_person.append(len(data[data['CODE_ADDITION'] == i].groupby(['RESPONSIBE_PERSON_NAME', 'NEW_NAME'])))
    count = sum(detail_count)
    person = sum(detail_person)
    # table
    table = []
    for i in list(set(data['RESPONSIBE_PERSON_NAME'].tolist())):
        l1 = []
        n_data = data[data['RESPONSIBE_PERSON_NAME'] == i]
        # 被评价人部门
        l1.append(n_data['NEW_NAME'].tolist()[0])
        # 姓名
        l1.append(i)
        # 职务层次
        l1.append(n_data['GRADATION'].tolist()[0])
        # 所属年月
        l1.append(str(n_data['YEAR'].tolist()[0]) + '-' + str(n_data['MONTH'].tolist()[0]))
        # 评价时间
        l1.append(str(n_data['CREATE_TIME'].tolist()[0]))
        # 评价方式
        l1.append(calc_evaluate_way(n_data['EVALUATE_WAY'].tolist()[0]))
        # 评价条款及内容
        l1.append(','.join(n_data['CODE'].tolist()))
        # 记分分值
        l1.append(int(n_data['SCORE'].sum()))
        table.append(l1)
    return {
        'ls1': ls1,
        'count': count,
        'person': person,
        'detail_count': detail_count,
        'detail_person': detail_person,
        'table': table
    }


def get_gw_safety_job1(evaluate_info, ls1):
    """
    职责数据计算
    :param evaluate_info:
    :param ls1:
    :return:
    """
    data = evaluate_info[evaluate_info['CODE'].isin(ls1)]
    detail_count = []
    detail_person = []
    for i in ls1:
        detail_count.append(len(data[data['CODE'] == i]))
        detail_person.append(len(data[data['CODE'] == i].groupby(['RESPONSIBE_PERSON_NAME', 'NEW_NAME'])))
    count = sum(detail_count)
    person = sum(detail_person)
    # table
    table = []
    for i in list(set(data['RESPONSIBE_PERSON_NAME'].tolist())):
        l1 = []
        n_data = data[data['RESPONSIBE_PERSON_NAME'] == i]
        # 被评价人部门
        l1.append(n_data['NEW_NAME'].tolist()[0])
        # 姓名
        l1.append(i)
        # 职务层次
        l1.append(n_data['GRADATION'].tolist()[0])
        # 所属年月
        l1.append(str(n_data['YEAR'].tolist()[0]) + '-' + str(n_data['MONTH'].tolist()[0]))
        # 评价时间
        l1.append(str(n_data['CREATE_TIME'].tolist()[0]))
        # 评价方式
        l1.append(calc_evaluate_way(n_data['EVALUATE_WAY'].tolist()[0]))
        # 评价条款及内容
        l1.append(','.join(n_data['CODE'].tolist()))
        # 记分分值
        l1.append(int(n_data['SCORE'].sum()))
        table.append(l1)
    return {
        'ls1': ls1,
        'count': count,
        'person': person,
        'detail_count': detail_count,
        'detail_person': detail_person,
        'table': table
    }


def calc_evaluate_way(num):
    if num == 0:
        return '自动评价'
    elif num == 1:
        return '逐条评价'
    elif num == 2:
        return '定期评价'
    else:
        return '阶段评价'


def get_fxs_check_result(start_date, end_date):
    dFrame = pd_query("""
    SELECT
    a.PK_ID,
    a.FK_CHECK_INFO_ID,
    LEFT(a.RISK_NAMES, 2) as system,
    f.NAME as responsible_station,
    e.NAME as responsible_department,
    d.RESPONSIBILITY_PERSON_NAMES,
    a.LEVEL,
    a.DESCRIPTION,
    e.TYPE as department_type
    FROM
    t_check_problem AS a
    INNER JOIN t_check_info as b ON a.FK_CHECK_INFO_ID = b.PK_ID
    INNER JOIN t_check_info_and_person AS c ON a.FK_CHECK_INFO_ID = c.FK_CHECK_INFO_ID
    INNER JOIN t_check_problem_and_responsible_department as d ON d.FK_CHECK_PROBLEM_ID = a.PK_ID
    INNER JOIN t_department as e ON d.FK_DEPARTMENT_ID = e.DEPARTMENT_ID
    INNER JOIN t_department as f ON e.TYPE3 = f.DEPARTMENT_ID
    WHERE
    a.SUBMIT_TIME BETWEEN DATE( '{0}' )
    AND DATE( '{1}' )
    AND e.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'
    """.format(start_date, end_date))
    if dFrame.empty:
        dFrame = dFrame.drop_duplicates(["FK_CHECK_INFO_ID", "PK_ID"])
    office_info = []
    # 查处的责任部门为职能科室一级
    for i, row in dFrame[dFrame["department_type"] == 7].iterrows():
        office_info.append({
            "system": row["system"],
            "responsible_station": row["responsible_station"],
            "responsible_department": row["responsible_department"],
            "responsible_names": row["RESPONSIBILITY_PERSON_NAMES"],
            "problem_type": row["LEVEL"],
            "content": row["DESCRIPTION"]
        })
    workshop_info = []
    # 查处的责任部门为职能车间一级
    for i, row in dFrame[dFrame["department_type"] == 8].iterrows():
        workshop_info.append({
            "system": row["system"],
            "responsible_station": row["responsible_station"],
            "responsible_department": row["responsible_department"],
            "responsible_names": row["RESPONSIBILITY_PERSON_NAMES"],
            "problem_type": row["LEVEL"],
            "content": row["DESCRIPTION"]
        })
    return office_info, workshop_info


# -----------------------------------------------------第五部分------------------------------------------------------------
def get_fifth(start_date, end_date):
    """
    安全重点工作落实质量分析
    :return:
    """
    global mv_cost_time_data
    mv_cost_time_data = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date))
    mv_cost_time_data = mv_cost_time_data[mv_cost_time_data['TYPE'] == 4]
    mv_cost_time_data['NEW_NAME'] = mv_cost_time_data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    data = mv_cost_time_data.groupby('NEW_NAME').sum().reset_index()
    station_name = data['NEW_NAME'].tolist()
    all = []
    for i in station_name:
        s = calc_cost_time(mv_cost_time_data[mv_cost_time_data['NEW_NAME'] == i])
        all.append(s)
    a = [0, 0, 0, 0, 0, 0]
    for i in range(len(all)):
        for j in range(len(all[i])):
            a[j] = a[j] + all[i][j]
    return {
        'station_name': station_name,
        'all': all,
        'a': a
    }


def calc_cost_time(data):
    # 音视频调阅（小时）
    mv_time = int(data['COST_TIME'].sum())
    # 人均调阅时长（小时）
    all_person = len(list(set(data['CHECK_PERSON_NAMES'].tolist())))
    avg_time = round(ded_zero(mv_time, all_person), 1)
    # 发现总问题数（个）
    problem_count = int(data['PROBLEM_NUMBER'].sum())
    # 平均问题质量分
    problem_score = int(data['PROBLEM_SCORE'].sum())
    avg_score = round(ded_zero(problem_score, problem_count), 1)
    # 段调阅评价人次（次）
    number = len(data)
    # 段调阅评价合计分值
    # 分析室调阅评价人次
    # 分析室调阅评价合计分值
    l1 = [mv_time, avg_time, problem_count, avg_score, number, problem_score]
    return l1


# -----------------------------------------------违章大王-------------------------------------------------------------
def get_iiilege(start_date, end_date):
    iiilege_data = pd_query(check_problem_sql.format(start_date, end_date))
    all_list = []
    for i in ['成都', '重庆', '贵阳', '西昌', '达州', '贵阳北']:
        data = iiilege_data[iiilege_data['DEPARTMENT_ALL_NAME'].str.contains(i)]
        new_data = data.groupby('PERSON_NAME').sum().sort_values(by='PROBLEM_SCORE').reset_index()
        person = len(new_data)
        count = [0, 0, 0, 0, 0]
        for j in new_data['PROBLEM_SCORE'].tolist():
            if 4 <= j < 8:
                count[0] += 1
            elif j < 12:
                count[1] += 1
            elif j < 16:
                count[2] += 1
            elif j < 20:
                count[3] += 1
            else:
                count[4] += 1
        dic = {
            'name': i,
            'count': count,
            'ratio': [round(ded_zero(k, person) * 100, 1) for k in count],
            'big': count[3] + count[4]
        }
        all_list.append(dic)
    all_count = 0
    for i in all_list:
        all_count += i['big']
    return {
        'all_list': all_list,
        'all_count': all_count
    }


# -----------------------------------------------------最后一点--------------------------------------------------------------
def get_finally(start_date, end_date):
    mv_data = pd_query(CHECK_MV_SQL.format(start_date, end_date))
    mv_data['NEW_NAME'] = mv_data['ALL_NAME'].apply(lambda x: x.split('-')[0])
    # table
    table = get_table(mv_data)
    # 详细信息
    detail_info = get_detail_info(mv_data)
    return {
        'table': table,
        'detail_info': detail_info,
    }


def get_table(mv_data):
    station_name = list(set(mv_data['NEW_NAME']))
    all_list = []
    for i in station_name:
        data = mv_data[mv_data['NEW_NAME'] == i]
        # 音视频调阅（小时）
        # 音视频复查（条）
        mv_review = len(data[(data['EVALUATE_TYPE'] == 3) & (data['IS_REVIEW'] == 1)])
        # 逐条评价复查（条）
        zt_review = len(data[(data['EVALUATE_WAY'] == 1) & (data['IS_REVIEW'] == 1)])
        # 定期评价复查（人次）
        dq_review = len(data[(data['EVALUATE_WAY'] == 2) & (data['IS_REVIEW'] == 1)])
        # 发现履职问题（个）
        all_problem = int(data['PROBLEM_NUMBER'].sum())
        # 发现现场问题（个）
        xc_problem = int(data[data['CHECK_WAY'] == 1]['PROBLEM_NUMBER'].sum())
        # 检查部门数（个）
        check_dep = len(data.groupby('ALL_NAME'))
        all_list.append([mv_review, zt_review, dq_review, all_problem, xc_problem, check_dep])
    s = [0, 0, 0, 0, 0, 0]
    for i in range(len(all_list)):
        for j in range(len(all_list[i])):
            s[j] = s[j] + all_list[i][j]
    return {
        'station_name': station_name,
        'all_list': all_list,
        's': s
    }


def get_detail_info(data):
    """
    详细信息
    :param mv_data:
    :return:
    """
    count = len(data)
    # 逐条评价复查（条）
    zt_review = len(data[(data['EVALUATE_WAY'] == 1) & (data['IS_REVIEW'] == 1)])
    # 定期评价复查（人次）
    dq_review = len(data[(data['EVALUATE_WAY'] == 2) & (data['IS_REVIEW'] == 1)])
    # 发现履职问题（个）
    all_problem = int(data['PROBLEM_NUMBER'].sum())
    ratio = round(ded_zero(all_problem, count) * 100, 1)
    # 音视频复查（条）
    mv_review = len(data[(data['EVALUATE_TYPE'] == 3) & (data['IS_REVIEW'] == 1)])
    # 调阅问题
    dy_problem = int(data[data['CHECK_WAY'] == 3]['PROBLEM_NUMBER'].sum())
    # 发现现场问题（个）
    xc_problem = int(data[data['CHECK_WAY'] == 1]['PROBLEM_NUMBER'].sum())
    return {
        'zt_review': zt_review,
        'dq_review': dq_review,
        'all_problem': all_problem,
        'ratio': ratio,
        'mv_review': mv_review,
        'dy_problem': dy_problem,
        'xc_problem': xc_problem,
    }