from app.report.analysis_report_manager import QuarterlyAnalysisReport
from app import mongo
from app.report.gongdian.major_quarterly_report_data import get_data


class GongdianMajorQuarterlyAnalysisReport(QuarterlyAnalysisReport):
    """
    客运的站段级的周分析报告类。
    """

    def __init__(self):
        super(GongdianMajorQuarterlyAnalysisReport, self).__init__(hierarchy_type='MAJOR', major='供电')

    def generate_report_data(self, year, quarter):
        """
        执行生成指定月份的，指定站段月报的数据，并件数据持久化
        :param year: int 年
        :param quarter: int 月
        :return:
        """
        data = get_data(year, quarter)
        mongo.db['safety_analysis_quarterly_report'].delete_one(
            {
                "year": year,
                "quarter": quarter,
                "major": self.major,
                "hierarchy": "MAJOR",
            })
        mongo.db['safety_analysis_quarterly_report'].insert_one(data)
        return data

