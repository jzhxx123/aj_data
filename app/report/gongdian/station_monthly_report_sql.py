# 安全生产信息
CHECK_SAFETY_INFO_SQL = """SELECT
        a.*,
        c.DEPARTMENT_ID,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED,
        g.TYPE,
        c.`NAME` as STATION
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID  
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1} 23:59:59')
            and c.TYPE3='{2}'"""


# 量化信息完成情况
t_quanttization_sql = """SELECT
        a.ID_CARD, a.PERSON_NAME, dp.ALL_NAME, 
        a.CHECK_TIMES_TOTAL,
        a.CHECK_NOTIFICATION_TIMES_TOTAL,
        a.MONITOR_NUMBER_TOTAL,
        a.PROBLEM_NUMBER_TOTAL,
        a.WORK_PROBLEM_NUMBER_TOTAL,
        a.MONITOR_PROBLEM_NUMBER_TOTAL,
        a.HIDDEN_DANGER_RECHECK_TIMES_TOTAL,
        a.RISK_RECHECK_TIMES_TOTAL,
        a.IMPORTANT_PROBLEM_RECHECK_TIMES_TOTAL,
        a.MIN_QUALITY_GRADES_TOTAL,
        a.MANAGE_PROBLEM_NUMBER_TOTAL,
        a.DEVICE_PROBLEM_NUMBER_TOTAL,
        a.ENVIRONMENT_PROBLEM_NUMBER_TOTAL,
        a.OUT_WORK_PROBLEM_NUMBER_TOTAL,
        b.REALITY_NUMBER,
        b.CHECK_NOTIFICATION_REALITY_NUMBER,
        b.MEDIA_REALITY_TIME,
        b.REALITY_PROBLEM_NUMBER,
        b.REALITY_WORK_ITEM_PROBLEM_NUMBER,
        b.MEDIA_REALITY_PROBLEM_NUMBER,
        b.HIDDEN_DANGER_RECHECK_REALITY_NUMBER,
        b.RISK_RECHECK_REALITY_NUMBER,
        b.IMPORTANT_PROBLEM_RECHECK_REALITY_NUMBER,
        b.REALITY_MIN_QUALITY_GRADES,
        b.REALITY_MANAGE_PROBLEM_NUMBER,
        b.REALITY_DEVICE_PROBLEM_NUMBER,
        b.REALITY_ENVIRONMENT_PROBLEM_NUMBER,
        b.REALITY_OUT_WORK_ITEM_PROBLEM_NUMBER,
        dp.TYPE3 AS STATION_ID, dp.TYPE4 AS SECTION_ID,
        st.`NAME`
    FROM t_quantization_base_quota a
        LEFT JOIN t_quantify_assess_real_time b ON a.ID_CARD = b.ID_CARD AND a.MONTH = b.MONTH AND a.YEAR = b.YEAR
        LEFT JOIN t_department dp ON a.FK_DEPARTMENT_ID = dp.DEPARTMENT_ID
        LEFT JOIN t_department st on st.DEPARTMENT_ID=dp.TYPE3
    WHERE dp.TYPE3 = '{2}'
    AND a.YEAR = {0} AND a.MONTH = {1}"""

# 问题信息
CHECK_PROBLEM_SQL = """SELECT p.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,p.PROBLEM_CLASSITY_NAME,p.`LEVEL`,
d.CHECK_SCORE,i.IS_YECHA,p.RISK_LEVEL,e.`NAME`,p.CHECK_ITEM_NAME,p.PROBLEM_SCORE,i.IS_GENBAN,p.TYPE,b.STATUS,p.PROBLEM_POINT,
i.CHECK_TYPE,c.ALL_NAME,b.RESPONSIBILITY_PERSON_NAMES,p.DESCRIPTION,f.NAME as SHOP
from t_check_info i
LEFT JOIN t_check_problem p on i.PK_ID = p.FK_CHECK_INFO_ID
LEFT JOIN t_check_problem_and_responsible_department b on b.FK_CHECK_PROBLEM_ID=p.PK_ID
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_problem_base d on p.FK_PROBLEM_BASE_ID = d.PK_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_department f on f.DEPARTMENT_ID=c.TYPE4
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}'
"""

# 检查信息
CHECK_INFO_SQL = """SELECT i.PK_ID,i.CHECK_WAY,i.DEPARTMENT_ALL_NAME,i.PROBLEM_NUMBER,i.IS_YECHA,e.`NAME`,d.NAME AS SHOP,
i.CHECK_ADDRESS_NAMES
from t_check_info i
left join t_check_info_and_person b on b.fk_check_info_id = i.pk_id
LEFT JOIN t_department c on b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE4
where i.SUBMIT_TIME BETWEEN '{0} 00:00:00' and '{1} 23:59:59'
and c.TYPE3='{2}'"""

# 履职评价
CHECK_EVALUATE_SITUATION_SQL = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME AS DEPARTMENT_ALL_NAME,
        c.TYPE3,
        d.NAME AS MAJOR,
        e.JOB,
        e.IDENTITY,
        f.NAME AS STATION,
        g.NAME AS SHOP
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
            LEFT JOIN
        t_department AS g ON g.DEPARTMENT_ID = c.TYPE4
    WHERE
        CREATE_TIME >= '{0}'
            AND CREATE_TIME <= '{1} 23:59:59'
            AND  c.TYPE3 = '{2}'"""

# 履职复查
CHECK_EVALUATE_REVIEW_SQL = """SELECT c.ALL_NAME,b.*,d.NAME,e.NAME AS SHOP
FROM t_check_evaluate_review_person b 
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.FK_REVIEW_DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=c.TYPE3
LEFT JOIN t_department e on e.DEPARTMENT_ID=c.TYPE4
WHERE b.CREATE_TIME BETWEEN '{0}' AND '{1} 23:59:59'
AND c.TYPE3 = '{2}'
"""

# 音视频检查信息
CHECK_MV_COST_TIME_SQL = """SELECT 
        a.PK_ID,
        b.CHECK_PERSON_NAMES,
        b.DEPARTMENT_ALL_NAME,
        a.COST_TIME,
        b.CHECK_ITEM_NAMES,
        b.CHECK_WAY,
        d.ALL_NAME,
        e.`NAME`,
        a.MONITOR_POST_NAMES,
        f.NAME AS SHOP
FROM
    t_check_info_and_media AS a
        INNER JOIN
    t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
        LEFT JOIN
    t_check_info_and_person c on c.FK_CHECK_INFO_ID=b.PK_ID
        LEFT JOIN
    t_department d ON c.FK_DEPARTMENT_ID = d.DEPARTMENT_ID
        LEFT JOIN 
    t_department e on e.DEPARTMENT_ID=d.TYPE3
        LEFT JOIN 
    t_department f on f.DEPARTMENT_ID=d.TYPE4
WHERE
    b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
    AND d.TYPE3 = '{2}'
"""


# 车间名称和人数
SHOP_PERSON_SQL = """
SELECT count(b.`NAME`) as COUNT,b.`NAME`
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND LENGTH(b.SHORT_NAME) > 0 AND a.TYPE in (7, 8)
GROUP BY b.`NAME`"""


# 站段信息
ZHANDUAN_PERSON_SQL = """SELECT count(b.`NAME`) as COUNT,b.`NAME`
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND LENGTH(b.SHORT_NAME) > 0 
GROUP BY b.`NAME`
"""

# 领导班子信息
LEADER_INFO = """
SELECT COUNT(d.`NAME`) as COUNT,b.`NAME`,d.`NAME` AS SHOP
FROM t_person c
LEFT JOIN t_department AS a on a.DEPARTMENT_ID=c.FK_DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE4
WHERE a.IS_DELETE = 0 AND a.TYPE3 = '{0}' 
AND a.IS_DELETE=0 AND LENGTH(b.SHORT_NAME) > 0
GROUP BY d.`NAME`,b.`NAME`"""


# 量化指标
center_qt_sql = """SELECT b.ALL_NAME,a.* from t_analysis_center_quantization_quota a
LEFT JOIN t_department b on b.DEPARTMENT_id=a.FK_DEPARTMENT_ID
WHERE a.`YEAR`={0} and a.`MONTH`={1}
AND b.TYPE3='{2}'"""

# 违章人员
wz_sql = """SELECT a.ID_CARD,a.ACTUAL_DEDUCT_SCORE,b.ALL_NAME,a.`MONTH` from t_warning_key_person_library a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_UNIT_ID
WHERE b.TYPE3='{2}'
and a.`YEAR` = {0} and a.`MONTH` BETWEEN 1 and {1}"""
