from app.data.util import pd_query
from app import mongo
import pandas as pd
import re
from dateutil.relativedelta import relativedelta
from app.report.gongdian.major_monthly_report_sql import *
import datetime

_PROBLRM_TYPE_DICT = {
    # 问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
    "作业": ['LEVEL', ['A', 'B', 'C', 'D']],
    "设备": ['LEVEL', ['E1', 'E2', 'E3', 'E4']],
    "管理": ['LEVEL', ['F1', 'F2', 'F3', 'F4']],
    "路外": ['LEVEL', ['G1', 'G2', 'G3', 'G4']],
    "反恐防暴": ['LEVEL', ['K1', 'K2', 'K3', 'K4']],
}


def get_data(year, month):
    global start_date, end_date, last_year
    end_date = datetime.date(year, month, 24)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = str(start_date - relativedelta(months=1))[:10]
    last_month_end = str(end_date - relativedelta(months=1))[:10]
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    last_year_start = str(start_date - relativedelta(months=12))[:10]
    last_year_end = str(end_date - relativedelta(months=12))[:10]
    last_year_month_start = str(start_date - relativedelta(months=13))[:10]
    last_year_month_end = str(end_date - relativedelta(months=13))[:10]
    last_year = last_year_end[:4]
    last_year_month = last_year_end[5:7]
    last_month = last_month_end[5:7]
    last_year_last_month = last_year_month_end[5:7]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    # 检查信息
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_month_info = pd_query(CHECK_INFO_SQL.format(last_month_start, last_month_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_year_info = pd_query(CHECK_INFO_SQL.format(last_year_start, last_year_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 安全生产信息
    accident_data = pd_query(CHECK_SAFETY_INFO_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_month_data = pd_query(CHECK_SAFETY_INFO_SQL.format(last_month_start, last_month_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_year_data = pd_query(CHECK_SAFETY_INFO_SQL.format(last_year_start, last_year_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_year_month_data = pd_query(
        CHECK_SAFETY_INFO_SQL.format(last_year_month_start, last_year_month_end)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 量化任务人员
    qutazan_data = pd_query(t_quanttization_sql.format(year, month))
    # 问题信息
    problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date)).dropna(subset=['PK_ID']).drop_duplicates(
        subset=['PK_ID']).reset_index()
    last_month_pro_data = pd_query(CHECK_PROBLEM_SQL.format(last_month_start, last_month_end)).dropna(
        subset=['PK_ID']).drop_duplicates(subset=['PK_ID']).reset_index()
    last_year_pro_data = pd_query(CHECK_PROBLEM_SQL.format(last_year_start, last_year_end)).dropna(
        subset=['PK_ID']).drop_duplicates(subset=['PK_ID']).reset_index()
    # 站段人数
    zhanduan_data = pd_query(ZHANDUAN_PERSON_SQL)
    # 履职评价
    check_evaluate_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date))
    # 履职复查
    eva_re_data = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 调阅时长
    mv = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 量化指标
    center_qt_data = pd_query(center_qt_sql.format(year, month))
    # 违章人员
    wz_data = pd_query(wz_sql.format(year, month))

    first = get_first(accident_data, last_month_data, last_year_data, last_year_month_data)
    second = get_second(qutazan_data, problem_data, last_month_pro_data, last_year_pro_data, zhanduan_data)
    third = get_third(problem_data, last_month_pro_data, last_year_pro_data, accident_data, last_month_data,
                      last_year_data,
                      check_evaluate_data, info_data, last_month_info, last_year_info, qutazan_data, mv, eva_re_data)
    five = get_five(problem_data, check_evaluate_data, mv, zhanduan_data)
    six = get_six(check_evaluate_data, eva_re_data, info_data, mv)
    six_one = get_six_one(check_evaluate_data, eva_re_data, info_data, mv, center_qt_data)
    eight = get_eight(wz_data, month)
    file_name = f'{start_date}至{end_date}供电系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '供电',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        'last_year': last_year,
        'last_year_month': last_year_month,
        'last_month': last_month,
        'last_year_last_month': last_year_last_month,
        "first": first,
        "second": second,
        'third': third,
        'five': five,
        'six': six,
        'six_one': six_one,
        'eight': eight
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_year_and_ring_ratio(counts):
    """
    计算环比和同比
    :param counts:
    :return:
    """
    now_count, year_count, month_count = counts
    year_ratio = now_count - year_count
    if year_count == 0:
        year_percent = year_ratio * 100
    else:
        year_percent = round(year_ratio / year_count, 1) * 100
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 1) * 100
    result = {
        'year_diff': round(year_ratio, 3),
        'year_percent': round(year_percent, 3),
        'month_diff': round(month_ratio, 3),
        'month_percent': round(month_percent, 3)
    }
    return result


def calc_different_type(data):
    """
    计算各类问题数量
    :param data:
    :return:
    """
    # 一般ABCD类事故数
    number = []
    for i in ['A', 'B', 'C', 'D']:
        new_data = data[data['MAIN_TYPE'] == 1]
        number.append(len(new_data[new_data['CODE'].str.contains(i)]))
    # 设备故障数量
    number.append(len(data[(data['NAME'].str.contains('设备故障')) & (data['MAIN_TYPE'] == 2)]))
    return number


# -------------------------------------------------------------第一部分--------------------------------------------------------------------------
def get_first(accident_data, last_month_data, last_year_data, last_year_month_data):
    """
    段安全生产基本情况分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :param last_year_month_data:
    :return:
    """
    # TOP信息
    top = get_top(accident_data, last_month_data, last_year_data, last_year_month_data)
    # 行车、路外、劳安
    all_acc = get_all_acc(accident_data, last_month_data, last_year_data)
    # 事故类别分析
    acc_type = get_acc_type(accident_data, last_month_data, last_year_data)
    # 事故责任归属分析
    acc_duty = get_acc_duty(accident_data, last_month_data, last_year_data)
    # 设备故障统计分析
    eq_failed = get_eq_failed(accident_data, last_month_data, last_year_data)
    return {
        'top': top,
        'all_acc': all_acc,
        'acc_type': acc_type,
        'acc_duty': acc_duty,
        'eq_failed': eq_failed,
    }


def get_top(accident_data, last_month_data, last_year_data, last_year_month_data):
    """
    TOP信息计算
    :param accident_data: 本月数据
    :param last_month_data: 上月数据
    :param last_year_data: 去年数据
    :param last_year_month_data: 去年上月数据
    :return:
    """
    data_list = [accident_data, last_month_data, last_year_data, last_year_month_data]
    # 表格信息
    result = []
    for i in data_list:
        data = i[i['RANK'] == 3]
        result.append(calc_different_type(data))
    # 计算环比、同比
    ratio = []
    for i in range(5):
        ratio.append(calculate_year_and_ring_ratio([result[0][i], result[2][i], result[1][i]]))
    # 一般事故
    general_acc = accident_data[(accident_data['MAIN_TYPE'] == 1) & (accident_data['RANK'] == 3)]
    last_month_acc = last_month_data[(last_month_data['MAIN_TYPE'] == 1) & (accident_data['RANK'] == 3)]
    last_year_acc = last_year_data[(last_year_data['MAIN_TYPE'] == 1) & (accident_data['RANK'] == 3)]
    # 环比同比
    all_ratio = calculate_year_and_ring_ratio([len(general_acc), len(last_month_acc), len(last_year_acc)])
    return {
        'result': result,
        'ratio': ratio,
        'general_acc': len(general_acc),
        'all_ratio': all_ratio
    }


def get_all_acc(accident_data, last_month_data, last_year_data):
    """
    行车、路外、劳安
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 行车
    xc = calc_all_acc(accident_data, last_month_data, last_year_data, 1)
    # 路外
    lw = calc_all_acc(accident_data, last_month_data, last_year_data, 3)
    # 劳安
    la = calc_all_acc(accident_data, last_month_data, last_year_data, 2)
    return {
        'xc': xc,
        'lw': lw,
        'la': la

    }


def calc_all_acc(accident_data, last_month_data, last_year_data, detail_type):
    """
    计算行车/路外/劳安问题
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :param detail_type: 问题分类
    :param l1:
    :return:
    """
    # 事故类别
    accident_data = accident_data[(accident_data['DETAIL_TYPE'] == detail_type) & (accident_data['MAIN_TYPE'] == 1)]
    last_month_data = last_month_data[
        (last_month_data['DETAIL_TYPE'] == detail_type) & (last_month_data['MAIN_TYPE'] == 1)]
    last_year_data = last_year_data[(last_year_data['DETAIL_TYPE'] == detail_type) & (last_year_data['MAIN_TYPE'] == 1)]

    data_list = [accident_data, last_year_data, last_month_data]
    acc_list = []
    type_list = []
    # 事故发生件数
    for i in data_list:
        acc_list.append(len(i))
    # 环比同比
    ratio1 = calculate_year_and_ring_ratio(acc_list)
    # 发生ABCD类事故件数
    for i in ['A', 'B', 'C', 'D']:
        l2 = []
        for j in data_list:
            l2.append(len(j[j['CODE'].str.contains(i)]))
        type_list.append(l2)
    # 环比同比
    ratio = []
    for i in type_list:
        ratio.append(calculate_year_and_ring_ratio(i))
    return {
        'acc_list': acc_list,
        'ratio1': ratio1,
        'type_list': type_list,
        'ratio': ratio
    }


def get_acc_type(accident_data, last_month_data, last_year_data):
    """
    事故类别分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    accident_data = accident_data[accident_data['MAIN_TYPE'] == 1]
    last_month_data = last_month_data[last_month_data['MAIN_TYPE'] == 1]
    last_year_data = last_year_data[last_year_data['MAIN_TYPE'] == 1]
    # 行车C类
    type_c = calc_accident_code_type(accident_data, last_month_data, last_year_data, 'C', 1)
    # 行车D类
    type_d = calc_accident_code_type(accident_data, last_month_data, last_year_data, 'D', 1)
    # 劳安事故
    labor = calc_labor(accident_data, last_month_data, last_year_data)
    return {
        'type_c': type_c,
        'type_d': type_d,
        'labor': labor
    }


def calc_accident_code_type(accident_data, last_month_data, last_year_data, str1, detail_type):
    """
    计算事故类别
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :param str1:
    :return:
    """
    # 各个类别事故数量
    data = accident_data[(accident_data['CODE'].str.contains(str1)) & (accident_data['DETAIL_TYPE'] == detail_type)]
    last_month_data = last_month_data[
        (last_month_data['CODE'].str.contains(str1)) & (last_month_data['DETAIL_TYPE'] == detail_type)]
    last_year_data = last_year_data[
        (last_year_data['CODE'].str.contains(str1)) & (last_year_data['DETAIL_TYPE'] == detail_type)]
    new_data = data.groupby('NAME').count().reset_index()
    # 事故名称
    name_list = new_data['NAME'].tolist()
    # 事故数量
    number = new_data['CODE'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, [], []]
    for i in name_list:
        l1[1].append(len(last_year_data[last_year_data['NAME'] == i]))
        l1[2].append(len(last_month_data[last_month_data['NAME'] == i]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i], l1[2][i]]))
    return {
        'count': len(data),
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def calc_labor(accident_data, last_month_data, last_year_data):
    """
    劳安问题
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 劳安事故分类
    data = accident_data[accident_data['MAIN_TYPE'] == 2]
    last_month_data = last_month_data[last_month_data['MAIN_TYPE'] == 2]
    last_year_data = last_year_data[last_year_data['MAIN_TYPE'] == 2]
    new_data = data.groupby('NAME').count().reset_index()
    # 事故名称
    name_list = new_data['NAME'].tolist()
    # 事故数量
    number = new_data['PROBLEM_POINT'].tolist()
    for i in range(len(number)):
        number[i] = int(number[i])
    l1 = [number, [], []]
    for i in name_list:
        l1[1].append(len(last_year_data[last_year_data['NAME'] == i]))
        l1[2].append(len(last_month_data[last_month_data['NAME'] == i]))
    ratio = []
    for i in range(len(name_list)):
        ratio.append(calculate_year_and_ring_ratio([l1[0][i], l1[1][i], l1[2][i]]))
    return {
        'count': len(data),
        'name_list': name_list,
        'number': number,
        'ratio': ratio
    }


def get_acc_duty(accident_data, last_month_data, last_year_data):
    """
    事故责任分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 系统责任
    sys_duty = get_sys_duty(accident_data, last_year_data, last_month_data)
    # 站段责任
    station_duty = get_station_duty(accident_data, last_year_data, last_month_data)
    return {
        'sys_duty': sys_duty,
        'station_duty': station_duty,
    }


def get_sys_duty(accident_data, last_year_data, last_month_data):
    """
    系统责任
    :param accident_data:
    :param last_year_data:
    :param last_month_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    xc = []
    laoan = []
    for i in data_list:
        # 行车责任事故
        xc.append(len(i[(i['DETAIL_TYPE'] == 1) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
        # 劳安责任事故
        laoan.append(
            len(i[(i['DETAIL_TYPE'] == 2) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
    ratio1 = calculate_year_and_ring_ratio(xc)
    ratio2 = calculate_year_and_ring_ratio(laoan)
    return {
        'xc': xc[0],
        'laoan': laoan[0],
        'ratio1': ratio1,
        'ratio2': ratio2
    }


def get_station_duty(accident_data, last_year_data, last_month_data):
    """
    站段责任
    :param accident_data:
    :param last_year_data:
    :param last_month_data:
    :return:
    """
    all_list = []
    for name in ['贵阳供电段', '成都供电段', '达州供电段', '重庆供电段']:
        data_list = [accident_data[accident_data['STATION'] == name], last_year_data[last_year_data['STATION'] == name],
                     last_month_data[last_month_data['STATION'] == name]]
        xc = []
        for i in data_list:
            # 行车责任事故
            xc.append(
                len(i[(i['DETAIL_TYPE'] == 1) & (i['FK_RESPONSIBILITY_DIVISION_ID'] == 667) & (i['MAIN_TYPE'] == 1)]))
        ratio1 = calculate_year_and_ring_ratio(xc)
        # 按事故类别分
        new_data = accident_data.groupby(['NAME', 'CODE']).count().reset_index()
        acc_content = []
        for i, j in new_data.iterrows():
            dic = {
                'name': j['NAME'],
                'count': int(j['REASON']),
                'type': j['CODE']
            }
            acc_content.append(dic)
        # 按站段责任分
        duty_number = []
        for i in [1, 4, 2, 6]:
            duty_number.append(
                len(accident_data[(accident_data['RESPONSIBILITY_IDENTIFIED'] == i) & (accident_data['TYPE'] == 1)]))
        dic = {
            'name': name,
            'xc': xc[0],
            'ratio1': ratio1,
            'acc_content': acc_content,
            'duty_number': duty_number,
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_eq_failed(accident_data, last_month_data, last_year_data):
    """
    事故故障分析
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    # 供电系统发生设备故障
    shebei = calc_eq(accident_data, last_month_data, last_year_data)
    # 设备故障构成事故
    shebei_acc = calc_eq_acc(accident_data, last_month_data, last_year_data)
    # 未构成事故的设备故障
    not_acc = calc_eq_not_acc(accident_data, last_month_data, last_year_data)
    return {
        'shebei': shebei,
        'shebei_acc': shebei_acc,
        'not_acc': not_acc
    }


def calc_eq(accident_data, last_month_data, last_year_data):
    """
    计算供电系统发生设备故障
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        i = i.dropna(subset=['NAME'])
        count.append(len(i[i['NAME'].str.contains('设备故障')]))
    d21 = len(accident_data[(accident_data['NAME'].str.contains('设备故障')) & (accident_data['CODE'] == 'D21')])
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'd21': d21,
        'ratio': ratio
    }


def calc_eq_acc(accident_data, last_month_data, last_year_data):
    """
    计算故障事故
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        count.append(len(i[(i['NAME'].str.contains('设备故障')) & (i['MAIN_TYPE'] == 1)]))
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'ratio': ratio
    }


def calc_eq_not_acc(accident_data, last_month_data, last_year_data):
    """
    计算故障事故
    :param accident_data:
    :param last_month_data:
    :param last_year_data:
    :return:
    """
    data_list = [accident_data, last_year_data, last_month_data]
    count = []
    for i in data_list:
        count.append(len(i[(i['NAME'].str.contains('设备故障')) & (i['MAIN_TYPE'] != 1)]))
    ratio = calculate_year_and_ring_ratio(count)
    return {
        'count': count[0],
        'ratio': ratio
    }


# ------------------------------------------------------------------第二部分--------------------------------------------
def get_second(qutazan_data, problem_data, last_month_pro_data, last_year_pro_data, zhanduan_data):
    """
    检查基本情况统计分析
    :param qutazan_data:
    :param problem_data:
    :param last_month_pro_data:
    :param last_year_pro_data:
    :param zhanduan_data:
    :return:
    """
    # 检查基本情况
    check_base_situation = get_check_base_situation(qutazan_data, problem_data)
    # 安全综合指数分析
    health_index = get_health_index()
    # 问题统计分析
    problem_count = get_problem_count(problem_data, zhanduan_data)
    # 问题三性分析
    problem_three = get_problem_three_analysis(problem_data, last_month_pro_data, last_year_pro_data)
    return {
        'check_base_situation': check_base_situation,
        'health_index': health_index,
        'problem_count': problem_count,
        'problem_three': problem_three
    }


def get_check_base_situation(qutazan_data, problem_data):
    """
    检查基本情况
    :param qutazan_data:
    :param problem_data:
    :return:
    """
    count = len(qutazan_data)
    # 检查次数
    real_check = int(qutazan_data['REALITY_NUMBER'].sum())
    plan_check = int(qutazan_data['CHECK_TIMES_TOTAL'].sum())
    ratio1 = round(ded_zero(real_check, plan_check) * 100, 2)
    # 发现问题数
    real_pro = int(qutazan_data['REALITY_PROBLEM_NUMBER'].sum())
    plan_pro = int(qutazan_data['PROBLEM_NUMBER_TOTAL'].sum())
    ratio2 = round(ded_zero(real_pro, plan_pro) * 100, 2)
    # 检查发现问题
    check_pro = len(problem_data)
    # 作业问题
    work = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    ratio3 = round(ded_zero(work, check_pro) * 100, 2)
    # 设备问题
    shebei = len(problem_data[problem_data['LEVEL'].str.contains('E')])
    ratio4 = round(ded_zero(shebei, check_pro) * 100, 2)
    # 管理问题
    manager = len(problem_data[problem_data['LEVEL'].str.contains('F')])
    ratio5 = round(ded_zero(manager, check_pro) * 100, 2)
    # 外部环境
    env = len(problem_data[problem_data['LEVEL'].str.contains('G')])
    ratio6 = round(ded_zero(env, check_pro) * 100, 2)
    # 反恐防暴
    cs = len(problem_data[problem_data['LEVEL'].str.contains('K')])
    ratio7 = round(ded_zero(cs, check_pro) * 100, 2)
    return {
        'count': count,
        'real_check': real_check,
        'plan_check': plan_check,
        'ratio1': ratio1,
        'real_pro': real_pro,
        'plan_pro': plan_pro,
        'ratio2': ratio2,
        'work': work,
        'ratio3': ratio3,
        'shebei': shebei,
        'ratio4': ratio4,
        'manager': manager,
        'ratio5': ratio5,
        'env': env,
        'ratio6': ratio6,
        'cs': cs,
        'ratio7': ratio7,
        'check_pro': check_pro,
    }


def get_health_index():
    """
    安全综合指数分析
    :param problem_data:
    :return:
    """
    pass


def get_problem_count(problem_data, zhanduan_data):
    """
    问题统计分析
    :param problem_data:
    :return:
    """
    # table
    table = calc_table(problem_data, zhanduan_data)
    # 作业、设备、管理、环境 高/中高质量问题
    check_problem = calc_check_problem(problem_data, zhanduan_data)
    return {
        'table': table,
        'check_problem': check_problem
    }


def calc_table(problem_data, zhanduan_data):
    """
    计算表格数据
    :param problem_data:
    :return:
    """
    all_stations = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        pro_data = problem_data[problem_data['NAME'] == i]
        zd_data = zhanduan_data[zhanduan_data['NAME'] == i]
        # 站段人数
        person = int(zd_data['COUNT'])
        all_pro = []
        all_score = []
        # 作业类、设备类、管理类
        for j in [['A', 'B', 'C', 'D'], ['E1', 'E2', 'E3', 'E4'], ['F1', 'F2', 'F3', 'F4']]:
            # 问题个数
            class_type = []
            # 问题质量分
            pro_score = []
            for k in j:
                data = pro_data[pro_data['LEVEL'] == k]
                class_type.append(len(data))
                pro_score.append(round(float(data['PROBLEM_SCORE'].sum()), 2))
            # 人均质量分
            avg_score = round(ded_zero(sum(pro_score), person), 2)
            all_score.append(avg_score)
            all_pro.append(class_type)
        # 外部环境、反恐防暴问题
        for j in ['G', 'K']:
            data = pro_data[pro_data['LEVEL'].str.contains(j)]
            all_pro.append([len(data)])
        # 总人数
        all_person = sum([sum(i) for i in all_pro])
        # 人均质量分
        all_avg_score = round(ded_zero(float(pro_data['PROBLEM_SCORE'].sum()), person), 2)
        dic = {
            'name': i,
            'all_pro': all_pro,
            'all_score': all_score,
            'all_person': all_person,
            'all_avg_score': all_avg_score
        }
        all_stations.append(dic)
    return {
        'all_stations': all_stations
    }


def calc_type_score(data, str1):
    """
    计算各类问题分数
    :param data:
    :param str1:
    :return:
    """
    _problem_type_dict = {
        # 问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
        "作业": ['LEVEL', ['A', 'B', 'C', 'D']],
        "设备": ['LEVEL', ['E1', 'E2', 'E3', 'E4']],
        "管理": ['LEVEL', ['F1', 'F2', 'F3', 'F4']],
        "路外": ['LEVEL', ['G1', 'G2', 'G3', 'G4']],
        "反恐防暴": ['LEVEL', ['K1', 'K2', 'K3', 'K4']],
    }
    # 计算ABCD类问题数量及平均分
    _column = _problem_type_dict[str1][0]
    _content = _problem_type_dict[str1][1]
    work_data = data[data[_column].isin(_content)]
    data_list = []
    for item in _content:
        data_list.append(len(work_data[work_data['LEVEL'] == item]))
    # 这里的人均质量分（检查问题的所有人数（不去重）/ 总问题的分数）
    # person_number = len(work_data.groupby('PERSON_NAME'))
    # score = int(work_data['PROBLEM_SCORE'].sum())
    person_number = work_data.shape[0]
    score = float(work_data['CHECK_SCORE'].sum())
    if person_number != 0:
        avg_score = round(ded_zero(score, person_number), 3)
    else:
        avg_score = score
    data_list.append(avg_score)
    return data_list


def calc_check_problem(problem_data, zhanduan_data):
    """
    计算检查问题信息
    :param problem_data:
    :return:
    """
    # 人数
    person = int(zhanduan_data['COUNT'].sum())
    # 发现问题数
    problem_count = len(problem_data)
    # 跟班发现问题数
    genban = len(problem_data[problem_data['IS_GENBAN'] == 1])
    # 发现设备监控问题数
    shebei = len(problem_data[problem_data['CHECK_WAY'].isin([3, 4])])
    # 高质量问题
    high_problem = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    ratio1 = round(ded_zero(high_problem, problem_count) * 100, 2)
    # 中高质量问题
    middle_problem = len(
        problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2', 'F3', 'E3', 'G1', 'C'])])
    ratio2 = round(ded_zero(middle_problem, problem_count) * 100, 2)
    problem_list = []
    # 作业、设备、管理、路外问题平均分
    for key in [['A', 'B', 'C', 'D'], ['E1', 'E2', 'E3', 'E4'], ['F1', 'F2', 'F3', 'F4'], ['G1', 'G2', 'G3', 'G4']]:
        data = problem_data[problem_data['LEVEL'].isin(key)]
        # 占系统比例
        ratios = round(ded_zero(len(data), problem_count) * 100, 2)
        # 高质量问题
        high = data[data['LEVEL'].isin(key[:2])]
        # 中高质量问题
        middle = data[data['LEVEL'].isin(key[:3])]
        # 问题质量分
        pro_avg_score = round(ded_zero(int(data['PROBLEM_SCORE'].sum()), person), 2)
        # 站段人均质量分
        station_all_score = []
        for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
            new_data = data[data['NAME'] == i]
            person_data = int(zhanduan_data[zhanduan_data['NAME'] == i]['COUNT'])
            scores = round(ded_zero(float(new_data['PROBLEM_SCORE'].sum()), person_data), 2)
            station_all_score.append(scores)
        problem_list.append([len(data), ratios, len(high), len(middle), pro_avg_score, station_all_score])
    # 系统总质量分
    sys_score = round(ded_zero(float(problem_data['PROBLEM_SCORE'].sum()), person), 2)
    # 站段人均质量分
    stations_all_score = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        data = problem_data[problem_data['NAME'] == i]
        person_data = int(zhanduan_data[zhanduan_data['NAME'] == i]['COUNT'])
        stations_all_score.append(round(ded_zero(float(data['PROBLEM_SCORE'].sum()), person_data), 2))
    return {
        'genban': genban,
        'shebei': shebei,
        'high_problem': high_problem,
        'middle_problem': middle_problem,
        'ratio1': ratio1,
        'ratio2': ratio2,
        'problem_list': problem_list,
        'sys_score': sys_score,
        'stations_all_score': stations_all_score
    }


def get_problem_three_analysis(data, month_data, year_data):
    """
    问题三性分析
    :param data:
    :param month_data:
    :param year_data:
    :return:
    """
    # 作业类问题分析
    work = get_calc_work(data, month_data, year_data)
    # 管理类问题分析
    manager = get_calc_manager(data)
    # 设备类问题分析
    shebei = get_calc_shebei(data, month_data, year_data)
    # 外部环境项分析
    env = get_clac_env(data, month_data, year_data)
    return {
        'work': work,
        'manager': manager,
        'shebei': shebei,
        'env': env
    }


def get_calc_work(data, month_data, year_data):
    """
    计算作业类问题
    :param data:
    :param month_data:
    :param year_data:
    :return:
    """
    # 作业类问题
    data_list = []
    for i in [data, month_data, year_data]:
        data_list.append(i[i['LEVEL'].isin(['A', 'B', 'C', 'D'])])
    result = []
    # 问题分项统计
    main_problem_list = ['接触网', '电力', '变配电', '自轮设备', '给水']
    for j in ['接触网', '电力', '变配电', '自轮设备', '给水']:
        # 专业分类
        data_more = []
        for i in data_list:
            data_more.append(i[i['CHECK_ITEM_NAME'].str.contains(j)])
        # 计算各专业数据
        result.append(calc_major_problem(data_more))
    return {
        'result': result,
        'main_problem_list': main_problem_list
    }


def calc_major_problem(data_more):
    """
    计算主要专业问题
    :param data_more:
    :return:
    """
    # 总问题数
    data = data_more[0]
    all_number = len(data)
    problem_3 = []
    ratio_3 = []
    # 按项点分类取前三
    new_data = data.groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index()
    if new_data.empty is not True:
        for i in range(3 if len(new_data) > 3 else len(new_data)):
            # 项点名称
            problem_3.append(new_data.iloc[i]['PROBLEM_POINT'])
            # 占比
            ratio_3.append(round(ded_zero(int(new_data.iloc[i]['LEVEL']), all_number) * 100, 2))
        # 项点集中站段车间
        station_name = []
        for i in problem_3:
            station_name.append(data[data['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].unique().tolist())
        # 薄弱问题
        min_problem = new_data.iloc[-1]['PROBLEM_POINT']
        min_count = int(new_data.iloc[-1]['LEVEL'])
        min_ratio = round(ded_zero(min_count, all_number), 3)
        problem_list = data['CHECK_ITEM_NAME'].unique().tolist()
        all_problem_list = []
        # 自查问题
        zicha_count = []
        for i in problem_list:
            p_data = data[data['CHECK_ITEM_NAME'] == i]
            # 总问题数
            all_problem_list.append(len(p_data))
            # 自查问题数
            zicha_count.append(len(p_data[p_data['TYPE'] == 3]))
        # 自查问题
        index = zicha_count.index(min(zicha_count))
        zicha_name = problem_list[index]
        up = len(data[(data['CHECK_ITEM_NAME'] == zicha_name) & (data['CHECK_TYPE'].isin([401, 402, 403, 404]))])
        zj = len(data[(data['CHECK_ITEM_NAME'] == zicha_name) & (data['TYPE'].isin([3]))])
        zc_count = zicha_count[index]
        zc_ratio = round(ded_zero(zc_count, all_number) * 100, 3)
        # 问题变化率
        problem_change = calc_problem_change(data_more)
        # 调阅与跟班查处作业类问题数量差距
        problem_grop = calc_problem_grop(data_more[0])
        return {
            'status': 1,
            'problem_3': problem_3,
            'ratio_3': ratio_3,
            'station_name': station_name,
            'min_problem': min_problem,
            'min_count': min_count,
            'min_ratio': min_ratio,
            'zicha_name': zicha_name,
            'up': up,
            'zj': zj,
            'zc_ratio': zc_ratio,
            'all_problem_list': all_problem_list,
            'problem_list': problem_list,
            'problem_change': problem_change,
            'problem_grop': problem_grop
        }
    else:
        return {
            'status': 0
        }


def calc_problem_change(data_more):
    """
    问题变化率
    :param data_more:
    :return:
    """
    now_data = data_more[0].groupby('CHECK_ITEM_NAME').count().sort_values(by='LEVEL', ascending=False).reset_index()
    now_name = now_data['CHECK_ITEM_NAME'].tolist()
    now_count = [int(i) for i in now_data['LEVEL'].tolist()]
    last_month_count = []
    last_year_count = []
    for i in now_name:
        last_month_count.append(len(data_more[1][data_more[1]['CHECK_ITEM_NAME'] == i]))
        last_year_count.append(len(data_more[2][data_more[2]['CHECK_ITEM_NAME'] == i]))
    ratio = []
    for i in range(len(now_count)):
        ratio.append(calculate_year_and_ring_ratio([now_count[i], last_year_count[i], last_month_count[i]]))
    max = sorted(ratio, key=lambda x: x['month_diff'], reverse=True)[0]
    index = ratio.index(max)
    max_name = now_name[index]
    station_data = data_more[0][data_more[0]['CHECK_ITEM_NAME'] == max_name]
    station_name = station_data['NAME'].tolist()[:2]
    return {
        'max_name': max_name,
        'max': max,
        'station_name': station_name
    }


def calc_problem_grop(data):
    """
    调阅与跟班查处作业类问题数量差距
    :param data:
    :return:
    """
    number = []
    station_name = data['DEPARTMENT_ALL_NAME'].unique().tolist()
    shebei = []
    genban = []
    for i in station_name:
        new_data = data[data['DEPARTMENT_ALL_NAME'] == i]
        # 设备监控调阅
        shebei.append(len(new_data[new_data['CHECK_WAY'] == 3]))
        # 跟班检查
        genban.append(len(new_data[new_data['IS_GENBAN'] == 1]))
        number.append(len(new_data[new_data['CHECK_WAY'] == 3]) - len(new_data[new_data['IS_GENBAN'] == 1]))
    index = number.index(max(number))
    station_name = station_name[index]
    shebei = shebei[index]
    genban = genban[index]
    return {
        'station_name': station_name,
        'shebei': shebei,
        'genban': genban
    }


def get_calc_manager(data):
    """
    管理类问题分析
    :param data:
    :return:
    """
    data = data[data['LEVEL'].isin(['F1', 'F2', 'F3', 'F4'])]
    all_number = data.shape[0]
    problem_3 = []
    ratio_3 = []
    # 按项点分类取前三
    new_data = data.groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index()
    for i, k in new_data.iterrows():
        if i == 3:
            break
        else:
            # 项点名称
            problem_3.append(k['PROBLEM_POINT'])
            # 占比
            ratio_3.append(round(ded_zero(int(k['LEVEL']), all_number) * 100, ))
    # 项点集中站段车间
    station_name = []
    for i in problem_3:
        station_name.append(data[data['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].unique().tolist())
    return {
        'problem_3': problem_3,
        'ratio_3': ratio_3,
        'station_name': station_name,
    }


def get_calc_shebei(data, month_data, year_data):
    """
    设备问题分析
    :param data:
    :param month_data:
    :param year_data:
    :return:
    """
    data_list = []
    for i in [data, month_data, year_data]:
        data_list.append(i[i['LEVEL'].isin(['E1', 'E2', 'E3', 'E4'])])
    result = []
    main_problem_list = ['接触网', '电力', '变配电', '轨道车']
    for j in main_problem_list:
        data_more = []
        for i in data_list:
            data_more.append(i[i['CHECK_ITEM_NAME'].str.contains(j)])
        result.append(calc_major_problem_manager(data_more))
    return {
        'result': result,
        'main_problem_list': main_problem_list
    }


def calc_major_problem_manager(data_more):
    """
    计算设备类主要专业问题
    :param data_more:
    :return:
    """
    all_number = len(data_more[0])
    problem_3 = []
    ratio_3 = []
    new_data = data_more[0].groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index()
    if new_data.empty is not True:
        for i in range(3 if len(new_data) > 3 else len(new_data)):
            problem_3.append(new_data.iloc[i]['PROBLEM_POINT'])
            ratio_3.append(round(ded_zero(int(new_data.iloc[i]['LEVEL']), all_number) * 100, ))
        station_name = []
        lm_count = []
        ly_count = []
        now_count = []
        for i in problem_3:
            station_name.append(
                data_more[0][data_more[0]['PROBLEM_POINT'] == i]['DEPARTMENT_ALL_NAME'].unique().tolist())
            now_count.append(len(data_more[0][data_more[0]['PROBLEM_POINT'] == i]))
            lm_count.append(len(data_more[1][data_more[1]['PROBLEM_POINT'] == i]))
            ly_count.append(len(data_more[2][data_more[2]['PROBLEM_POINT'] == i]))
        ratio_list = []
        for i in range(3):
            ratio_list.append(calculate_year_and_ring_ratio([now_count[i], ly_count[i], lm_count[i]]))
        shebei_data = data_more[0].groupby('DEPARTMENT_ALL_NAME').count().sort_values(by='LEVEL',
                                                                                      ascending=False).reset_index()
        # 设备类问题
        max_name = shebei_data.iloc[0]['DEPARTMENT_ALL_NAME']
        max_number = int(shebei_data.iloc[0]['LEVEL'])
        # 整改
        zg = len(shebei_data[shebei_data['STATUS'] == 2])
        zg_ratio = round(ded_zero(zg, all_number) * 100, 3)
        # 严重设备质量问题
        serious_problem = []
        for i in data_more:
            serious_problem.append(len(i[i['LEVEL'].isin(['E1', 'E2'])]))
        serious_ratio = calculate_year_and_ring_ratio([serious_problem[0], serious_problem[2], serious_problem[1]])
        return {
            'status': 1,
            'problem_3': problem_3,
            'ratio_3': ratio_3,
            'station_name': station_name,
            'ratio_list': ratio_list,
            'max_name': max_name,
            'max_number': max_number,
            'serious_problem': serious_problem[0],
            'serious_ratio': serious_ratio,
            'zg': zg,
            'zg_ratio': zg_ratio
        }
    else:
        return {
            'status': 0
        }


def get_clac_env(data, month_data, year_data):
    """
    外部环境问题分析
    :param data:
    :param month_data:
    :param year_data:
    :return:
    """
    data_list = []
    for i in [data, year_data, month_data]:
        data_list.append(i[i['LEVEL'].isin(['G1', 'G2', 'G3', 'G4'])])
    problem_count = []
    for i in data_list:
        problem_count.append(i.shape[0])
    ratio = calculate_year_and_ring_ratio(problem_count)
    data_cp = data_list[0].groupby('PROBLEM_POINT').count().sort_values(by='LEVEL', ascending=False).reset_index()
    if data_cp.empty is not True:
        point_name = data_cp['PROBLEM_POINT'].tolist()[0]
    else:
        point_name = '无'
    main_problem_list = ['接触网', '电力']
    result = []
    for j in main_problem_list:
        result.append(calc_major_problem_env(data_list[0], j))
    return {
        'problem_count': problem_count[0],
        'ratio': ratio,
        'point_name': point_name,
        'result': result,
        'main_problem_list': main_problem_list
    }


def calc_major_problem_env(data, str1):
    """
    计算管理类主要专业问题
    :param data_more:
    :return:
    """
    data = data[data['CHECK_ITEM_NAME'].str.contains(str1)]
    problem_count = data.shape[0]
    # 问题数前二的责任部门
    data_cp = data.groupby(['PROBLEM_POINT', 'DEPARTMENT_ALL_NAME']).count().sort_values(by='LEVEL',
                                                                                         ascending=False).reset_index()
    zg = len(data[data['STATUS'] == 2])
    zg_ratio = round(ded_zero(zg, problem_count) * 100, 2)
    station_name = []
    for i in range(2 if len(data_cp) >= 2 else len(data_cp)):
        station_name.append(data_cp.iloc[i]['DEPARTMENT_ALL_NAME'])
    return {
        'problem_count': problem_count,
        'station_name': station_name,
        'zg': zg,
        'zg_ratio': zg_ratio
    }


# --------------------------------------------------------------第三部分-----------------------------------------------------------------
def get_third(problem_data, last_month_pro_data, last_year_pro_data, accident_data, last_month_data, last_year_data,
              check_evaluate_data, info_data, last_month_info, last_year_info, qutazan_data, mv, eva_re_data):
    # 整体检查履职质量分析
    third_one = get_third_one(accident_data, last_month_data, last_year_data, problem_data, last_month_pro_data,
                              last_year_pro_data, check_evaluate_data, info_data, last_month_info,
                              last_year_info, qutazan_data, mv)
    # 干部履职评价分析
    third_two = get_evaluate_situation_base_doc(check_evaluate_data, eva_re_data)
    # 各级干部检查质量分析
    third_three = get_gb_check_analysis(check_evaluate_data, problem_data)
    return {
        'third_one': third_one,
        'third_two': third_two,
        'third_three': third_three
    }


def get_third_one(duty_data, last_month_duty, last_year_duty, check_problem_data, month_check_problem_data,
                  year_check_problem_data, check_evaluate_data, info_data, last_month_info,
                  last_year_info, qutazan_data, mv):
    """
    整体检查履职质量分析
    :param duty_data:
    :param last_month_duty:
    :param last_year_duty:
    :param check_problem_data:
    :param month_check_problem_data:
    :param year_check_problem_data:
    :param check_evaluate_data:
    :param start_date:
    :param end_date:
    :return:
    """
    # 整体履职质量分析
    station_evaluate_analysis = get_station_evaluate_analysis(duty_data, last_month_duty, last_year_duty)
    # 监督检查情况
    check_situation = get_check_situation(info_data, last_month_info, last_year_info,
                                          check_problem_data, month_check_problem_data, year_check_problem_data)
    # 履职计分
    evaluate_score = get_evaluate_score(check_evaluate_data)
    # 站段领导班子检查质量分析
    table = get_carde_quality_analysis(info_data, check_problem_data, check_evaluate_data, qutazan_data, mv)
    leader_table = get_leader_table(check_evaluate_data)
    return {
        'station_evaluate_analysis': station_evaluate_analysis,
        'check_situation': check_situation,
        'evaluate_score': evaluate_score,
        "table": table,
        "leader_table": leader_table
    }


def get_station_evaluate_analysis(duty_data, last_month_duty, last_year_duty):
    """
    站段履职质量分析
    :param duty_data:
    :param last_month_duty:
    :param last_year_duty:
    :return:
    """
    all_station = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        data = duty_data[duty_data['STATION'] == i]
        last_month_data = last_month_duty[last_month_duty['STATION'] == i]
        last_year_data = last_year_duty[last_year_duty['STATION'] == i]
        xcs = []
        shebeis = []
        d21s = []
        for j in [data, last_year_data, last_month_data]:
            # 行车责任
            xc = j[(j['MAIN_TYPE'] == 1) & (j['DETAIL_TYPE'] == 1) & (j['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
            # 设备故障
            shebei = j[(j['MAIN_TYPE'] == 2) & (j['NAME'].str.contains('设备故障'))]
            # D21
            d21 = shebei[shebei['CODE'] == 'D21']
            xcs.append(len(xc))
            shebeis.append(len(shebei))
            d21s.append(len(d21))
        ratio1 = calculate_year_and_ring_ratio(xcs)
        ratio2 = calculate_year_and_ring_ratio(shebeis)
        all_station.append({
            'name': i,
            'xc': xcs[0],
            'shebei': shebeis[0],
            'd21': d21s[0],
            'ratio1': ratio1,
            'ratio2': ratio2
        })
    return {
        'all_station': all_station
    }


def get_check_situation(info_data, last_month_info, last_year_info, check_problem_data, month_check_problem_data,
                        year_check_problem_data):
    """
    监督检查情况
    :param info_data:
    :param last_month_info:
    :param last_year_info:
    :param check_problem_data:
    :param month_check_problem_data:
    :param year_check_problem_data:
    :return:
    """
    all_station = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        infos = []
        # 检查信息
        for j in [info_data, last_year_info, last_month_info]:
            data = j[j['NAME'] == i]
            infos.append(len(data))
        # 问题信息
        find_pro = []
        main_pro = []
        pro_score = []
        for j in [check_problem_data, year_check_problem_data, month_check_problem_data]:
            data = j[j['NAME'] == i]
            # 发现问题数
            find_pro.append(len(data))
            # 严重问题数
            main_pro.append(len(data[data['LEVEL'].isin(['A', 'B', 'E1', 'E2', 'F1', 'F2'])]))
            # 问题质量分
            pro_score.append(float(data['CHECK_SCORE'].sum()))
        ratio1 = calculate_year_and_ring_ratio(infos)
        ratio2 = calculate_year_and_ring_ratio(find_pro)
        ratio3 = calculate_year_and_ring_ratio(main_pro)
        ratio4 = calculate_year_and_ring_ratio(pro_score)
        all_station.append({
            'name': i,
            'info': infos[0],
            'find_pro': find_pro[0],
            'main_pro': main_pro[0],
            'pro_score': pro_score[0],
            'ratio1': ratio1,
            'ratio2': ratio2,
            'ratio3': ratio3,
            'ratio4': ratio4
        })
    return {
        'all_station': all_station
    }


def get_evaluate_score(data):
    """
    履职评价计分
    :param data:
    :return:
    """
    all_station = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        new_data = data[data['STATION'] == i]
        # 评价人次
        count = len(new_data)
        # 量化指标完成、检查信息录入、监督质量检查、考核责任落实、问题闭环管理、重点工作落实
        problem_dic = []
        for j in ['量化指标完成', '检查信息录入', '监督质量检查', '考核责任落实', '问题闭环管理', '重点工作落实']:
            dic = {
                'name': j,
                'count': len(new_data[new_data['ITEM_NAME'] == j])
            }
            problem_dic.append(dic)
        dic = {
            'name': i,
            'count': count,
            'problem_dic': problem_dic
        }
        all_station.append(dic)
    return {
        'all_station': all_station
    }


def get_carde_quality_analysis(info_data, check_problem_data, check_evaluate_data, qutazan_data, mv):
    # 领导班子
    leader = pd_query(LEADER_INFO)
    # 安全综合指数
    health_index = _get_health_index1(start_date, end_date)
    all_station = []
    # 系统总体检查
    mv_data = mv[mv['ALL_NAME'].str.contains('领导')]
    info = info_data[info_data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    problem = check_problem_data[check_problem_data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    eva_data = check_evaluate_data[check_evaluate_data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    leader_data = leader[leader['SHOP'].str.contains('领导')]['COUNT'].sum()
    dics = {
        # 人均安全履职得分
        'eva_score': round(float(ded_zero(eva_data['SCORE'].sum(), leader_data)), 4),
        # 现场检查次数
        'xc': len(info[info['CHECK_WAY'].isin([1, 2])]),
        # 人均现场检查
        'avg_xc': round(ded_zero(len(info[info['CHECK_WAY'].isin([1, 2])]), int(leader_data)), 1),
        # 人均调阅时长
        'avg_time': round(ded_zero(round(float(mv_data['COST_TIME'].sum()), 4), int(leader_data)), 4),
        # 人均夜查
        'avg_yc': round(ded_zero(len(info[info['IS_YECHA'] == 1]), int(leader_data)), 1),
        # 人均中高质量
        'avg_main': round(
            ded_zero(len(problem[problem['LEVEL'].isin(['A', 'B', 'C', 'E1', 'E2', 'E3', 'F1', 'F2', 'F3'])]),
                     int(leader_data)), 1),
        # 人均质量分
        'avg_score': round(ded_zero(int(problem['CHECK_SCORE'].sum()), int(leader_data)), 2)
    }
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        mv_data = mv[(mv['NAME'] == i) & (mv['ALL_NAME'].str.contains('领导'))]
        qutazan = qutazan_data[(qutazan_data['NAME'] == i) & (qutazan_data['ALL_NAME'].str.contains('领导'))]
        health = health_index[health_index['DEPARTMENT_NAME'] == i]
        info = info_data[(info_data['NAME'] == i) & (info_data['DEPARTMENT_ALL_NAME'].str.contains('领导'))]
        problem = check_problem_data[
            (check_problem_data['NAME'] == i) & (check_problem_data['DEPARTMENT_ALL_NAME'].str.contains('领导'))]
        eva_data = check_evaluate_data[
            (check_evaluate_data['STATION'] == i) & (check_evaluate_data['DEPARTMENT_ALL_NAME'].str.contains('领导'))]
        leader_data = leader[(leader['NAME'] == i) & (leader['SHOP'].str.contains('领导'))]['COUNT'].sum()
        dic = {
            'name': i,
            # 安全综合指数分
            'health_score': float(health['SCORE'].sum()),
            # 综合指数排名
            'health_rank': int(health['RANK']) if int(health['RANK'].sum()) != 0 else '无',
            # 安全履职得分
            'eva': float(eva_data['SCORE'].sum()),
            # 人均安全履职得分
            'eva_score': round(float(ded_zero(eva_data['SCORE'].sum(), leader_data)), 4),
            # 量化人数
            'qt': len(qutazan),
            # 现场检查次数
            'xc': len(info[info['CHECK_WAY'].isin([1, 2])]),
            # 人均现场检查
            'avg_xc': round(ded_zero(len(info[info['CHECK_WAY'].isin([1, 2])]), int(leader_data)), 1),
            # 调阅时长
            'cost_time': round(float(mv_data['COST_TIME'].sum()), 4),
            # 人均调阅时长
            'avg_time': round(ded_zero(round(float(mv_data['COST_TIME'].sum()), 4), int(leader_data)), 4),
            # 发现问题数
            'find_pro': len(problem),
            # 人均发现问题书
            'avg_pro': round(ded_zero(len(problem), int(leader_data)), 1),
            # 夜查
            'yc': len(info[info['IS_YECHA'] == 1]),
            # 人均夜查
            'avg_yc': round(ded_zero(len(info[info['IS_YECHA'] == 1]), int(leader_data)), 1),
            # 中高质量
            'main_pro': len(problem[problem['LEVEL'].isin(['A', 'B', 'C', 'E1', 'E2', 'E3', 'F1', 'F2', 'F3'])]),
            # 人均中高质量
            'avg_main': round(
                ded_zero(len(problem[problem['LEVEL'].isin(['A', 'B', 'C', 'E1', 'E2', 'E3', 'F1', 'F2', 'F3'])]),
                         int(leader_data)), 1),
            # 检查问题质量分
            'pro_score': int(problem['CHECK_SCORE'].sum()),
            # 人均质量分
            'avg_score': round(ded_zero(int(problem['CHECK_SCORE'].sum()), int(leader_data)), 2)
        }
        all_station.append(dic)
    return {
        'all_station': all_station,
        'dics': dics
    }


def get_leader_table(check_evaluate_data):
    data = check_evaluate_data[check_evaluate_data['DEPARTMENT_ALL_NAME'].str.contains('领导')]
    all_content = []
    for i, j in data.iterrows():
        dic = {
            'name': j['RESPONSIBE_PERSON_NAME'],
            'dp': j['DEPARTMENT_ALL_NAME'],
            'gradation': j['GRADATION'],
            'time': j['CREATE_TIME'],
            'eva_way': pjfs[j['EVALUATE_WAY']],
            'content': j['EVALUATE_CONTENT'],
            'score': float(j['SCORE'])
        }
        all_content.append(dic)
    return {
        'all_content': all_content
    }


pjfs = {
    0: '自动评价',
    1: '逐条评价',
    2: '定期评价',
    3: '阶段评价'
}


def _get_health_index1(start_date, end_date):
    """
    安全综合指数得分及排名
    :param start_date:
    :param end_date:
    :return:
    """
    prefix_list = 'monthly_'
    coll_name = prefix_list + 'health_index'
    doc = list(mongo.db[coll_name].find({
        'MAJOR': '供电',
        'MON': {
            "$gt": int(start_date.split('-')[0] + start_date.split('-')[1]),
            "$lte": int(end_date.split('-')[0] + end_date.split('-')[1])
        }
    }, {
        "_id": 0,
        'DEPARTMENT_NAME': 1,
        "SCORE": 1.0,
        'RANK': 1
    }))
    if len(doc) == 0:
        doc = [{
            'DEPARTMENT_NAME': '无',
            "SCORE": 0.0,
            'RANK': 0
        }]
    data = pd.DataFrame(doc)
    return data


def get_evaluate_situation_base_doc(check_evaluate_data, eva_re_data):
    """获取履职情况简要统计分析数据

    Arguments:
        start_date {str} -- 开始时间
        end_date {str} -- 结束时间

    Returns:
        dict -- jieguo
    """
    year_eva_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(last_year + '-12-25', end_date))
    year_re_data = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(last_year + '-12-25', end_date)).drop_duplicates(
        subset=['PK_ID']).reset_index()
    # 干部评价总体情况分析
    situation = get_third_two_situation(check_evaluate_data, year_eva_data, year_re_data, eva_re_data)
    # 干部履职突出问题记分项点统计
    gb_eva_pro = get_gb_eva_pro(check_evaluate_data, year_eva_data)
    # 供电系统干部履职评价情况分析
    gb_score_table = get_gb_score_table(check_evaluate_data)
    # 干部履职评价分析
    gb_eva_analysis = get_gb_eva_analysis(check_evaluate_data)
    return {
        'situation': situation,
        'gb_eva_pro': gb_eva_pro,
        'gb_score_table': gb_score_table,
        'gb_eva_analysis': gb_eva_analysis
    }


def get_third_two_situation(check_evaluate_data, year_eva_data, year_re_data, eva_re_data):
    # 定期评价
    dq_data = year_re_data[year_re_data['EVALUATE_WAY'] == 2]
    dq = len(dq_data) + len(dq_data[dq_data['IS_REVIEW'] == 1])
    luju = len(dq_data[dq_data['CHECK_TYPE'] == 1]) + \
           len(dq_data[(dq_data['IS_REVIEW'] == 1) & (dq_data['CHECK_TYPE'] == 1)])
    zd = len(dq_data[dq_data['CHECK_TYPE'] == 2]) + \
         len(dq_data[(dq_data['IS_REVIEW'] == 1) & (dq_data['CHECK_TYPE'] == 2)])
    # 逐条评价
    zt_data = year_re_data[year_re_data['EVALUATE_WAY'] == 1]
    zt = len(zt_data) + len(zt_data[zt_data['IS_REVIEW'] == 1])
    luju_zt = len(zt_data[zt_data['CHECK_TYPE'] == 1]) + \
              len(zt_data[(zt_data['CHECK_TYPE'] == 1) & (zt_data['IS_REVIEW'] == 1)])
    zd_zt = len(zt_data[zt_data['CHECK_TYPE'] == 2]) + \
            len(zt_data[(zt_data['CHECK_TYPE'] == 2) & (zt_data['IS_REVIEW'] == 1)])
    # 发现干部检查履职问题
    pro_count = len(year_eva_data)
    # 人数
    eva_person = len(year_eva_data['RESPONSIBE_ID_CARD'].unique())
    # 本月记分
    score = float(check_evaluate_data['SCORE'].sum())
    # 最高分
    max_score = float(check_evaluate_data.sort_values('SCORE', ascending=False).reset_index()['SCORE'][0])
    # 累计最高
    sum_score = \
        check_evaluate_data.groupby('RESPONSIBE_ID_CARD').sum().sort_values('SCORE', ascending=False).reset_index()[
            'SCORE'][0]
    return {
        'dq': dq,
        'luju': luju,
        'zd': zd,
        'zt': zt,
        'luju_zt': luju_zt,
        'zd_zt': zd_zt,
        'pro_count': pro_count,
        'eva_person': eva_person,
        'score': score,
        'max_score': max_score,
        'sum_score': sum_score
    }


def get_gb_eva_pro(check_evaluate_data, year_eva_data):
    count = len(check_evaluate_data)
    # 前5突出问题
    all_pro = []
    for i, k in check_evaluate_data.groupby('ITEM_NAME').count().sort_values('CODE').reset_index().iterrows():
        if i == 5:
            break
        dic = {
            'name': k['ITEM_NAME'],
            'count': int(k['CODE']),
            'ratio': round(ded_zero(int(k['CODE']), count) * 100, 2),
            'person': len(
                check_evaluate_data[check_evaluate_data['ITEM_NAME'] == k['ITEM_NAME']]['RESPONSIBE_ID_CARD'].unique())
        }
        all_pro.append(dic)
    # 累计记分
    all_person = len(year_eva_data['RESPONSIBE_ID_CARD'].unique())
    year_data = year_eva_data.groupby('RESPONSIBE_ID_CARD').sum().reset_index()
    scores = [0, 0, 0, 0, 0, 0, 0]
    for i, k in year_data.iterrows():
        if k['SCORE'] < 2:
            scores[0] += 1
        elif k['SCORE'] < 4:
            scores[1] += 1
        elif k['SCORE'] < 6:
            scores[2] += 1
        elif k['SCORE'] < 8:
            scores[3] += 1
        elif k['SCORE'] < 10:
            scores[4] += 1
        elif k['SCORE'] < 12:
            scores[5] += 1
        else:
            scores[6] += 1
    return {
        'all_pro': all_pro,
        'all_person': all_person,
        'scores': scores
    }


def get_gb_score_table(check_evaluate_data):
    all_station = []
    for i in ['供电部', '成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        data = check_evaluate_data[check_evaluate_data['STATION'] == i]
        data = data[data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
        # 职务级别
        gradations = []
        for j in [[1], [2, 3], [4]]:
            gradations.append(len(data[data['FK_PERSON_GRADATION_RATIO_ID'].isin(j)]['RESPONSIBE_ID_CARD'].unique()))
        # 考核人数
        person = len(data['RESPONSIBE_ID_CARD'].unique())
        # 扣分
        score = float(data['SCORE'].sum())
        # 最高记分
        if len(data) != 0:
            max_score = float(data.sort_values('SCORE', ascending=False).reset_index()['SCORE'][0])
        else:
            max_score = 0
        # 干部记分情况
        count = [0, 0, 0, 0, 0, 0, 0]
        for j, k in data.groupby('RESPONSIBE_ID_CARD').sum().reset_index().iterrows():
            if k['SCORE'] < 2:
                count[0] += 1
            elif k['SCORE'] < 4:
                count[1] += 1
            elif k['SCORE'] < 6:
                count[2] += 1
            elif k['SCORE'] < 8:
                count[3] += 1
            elif k['SCORE'] < 10:
                count[4] += 1
            elif k['SCORE'] < 12:
                count[5] += 1
            else:
                count[6] += 1
        all_station.append({
            'name': i,
            'gradations': gradations,
            'person': person,
            'score': score,
            'max_score': max_score,
            'count': count
        })
    return {
        'all_station': all_station
    }


def get_gb_eva_analysis(check_evaluate_data):
    """
    干部履职
    :param check_evaluate_data:
    :return:
    """
    gradations = []
    eva_data = check_evaluate_data[check_evaluate_data['FK_PERSON_GRADATION_RATIO_ID'].isin([1, 2, 3, 4])]
    for i in [2, 3, 4]:
        data = eva_data[eva_data['FK_PERSON_GRADATION_RATIO_ID'] == i]
        station = []
        # 按段分类
        for j in ['供电部', '成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
            dic = {
                'name': j,
                # 履职次数
                'count': len(data[data['STATION'] == j]),
                # 履职人数
                'person': len(data[data['STATION'] == j]['RESPONSIBE_ID_CARD'].unique())
            }
            station.append(dic)
        # 记分前5人员
        people = []
        for j, k in data.groupby(['RESPONSIBE_ID_CARD', 'RESPONSIBE_PERSON_NAME', 'STATION']).sum(). \
                sort_values('SCORE', ascending=False).reset_index().iterrows():
            if j >= 5:
                if float(k['SCORE']) == people[-1]['score']:
                    dic = {
                        'station': k['STATION'],
                        'name': k['RESPONSIBE_PERSON_NAME'],
                        'score': float(k['SCORE'])
                    }
                else:
                    break
            else:
                dic = {
                    'station': k['STATION'],
                    'name': k['RESPONSIBE_PERSON_NAME'],
                    'score': float(k['SCORE'])
                }
                people.append(dic)
        # 累计记分情况
        score_count = [0, 0, 0, 0, 0, 0, 0]
        for j, k in data.groupby('RESPONSIBE_ID_CARD').sum().reset_index().iterrows():
            if k['SCORE'] < 2:
                score_count[0] += 1
            elif k['SCORE'] < 4:
                score_count[1] += 1
            elif k['SCORE'] < 6:
                score_count[2] += 1
            elif k['SCORE'] < 8:
                score_count[3] += 1
            elif k['SCORE'] < 10:
                score_count[4] += 1
            elif k['SCORE'] < 12:
                score_count[5] += 1
            else:
                score_count[6] += 1
        dic = {
            # 履职次数
            'count': len(data),
            # 履职人数
            'person': len(data['RESPONSIBE_ID_CARD'].unique()),
            # 重点履职问题
            'main_pro': list(data['ITEM_NAME'].unique())[:5],
            # 各站段信息
            'station': station,
            'people': people,
            'score_count': score_count
        }
        gradations.append(dic)
    return {
        'gradations': gradations
    }


def get_gb_check_analysis(check_evaluate_data, problem_data):
    # 岗位安全职责履行质量方面
    position_safety = get_position_safety(check_evaluate_data,
                                          [['ZD-5-3', 'ZL-6-4'], ['ZL-6-4', 'ZD-8-3', 'ZL-1-1'], ['ZL-3-2', 'ZL-1-1']])
    # 重点关键监督检查质量方面
    main_skill = get_position_safety(check_evaluate_data,
                                     [['ZL-1-1', 'ZL-7-1'], ['ZD-5-3', 'ZL-7-1'], ['ZL-4-1', 'ZL-4-2']])
    # 专业技术管理工作质量方面
    professional_technology = get_position_safety(check_evaluate_data, [['ZD-5-1', 'ZD-5-2', 'ZD-5-3'],
                                                                        ['ZD-5-1', 'ZD-5-2', 'ZD-5-3'],
                                                                        ['ZD-5-1', 'ZD-5-2', 'ZD-5-3']])
    # 音频视频调阅运用质量方面
    mv = get_mv_yy(check_evaluate_data, problem_data)
    # 干部重点工作落实质量分析
    main_work = get_work_zg_kh(check_evaluate_data, [['ZD-5-1', 'ZD-5-2', 'ZD-8-1', 'ZD-8-2'], ['ZD-5-3', 'ZD-8-3']])
    # 整改质量分析
    zg_zl = get_work_zg_khs(check_evaluate_data, [['ZG-1', 'ZG-2', 'ZG-3'], ['ZG-1', 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5'],
                                                  ['ZG-1', 'ZG-2', 'ZG-3', 'ZG-4', 'ZG-5']])
    # 考核质量分析
    kh_zl = get_work_zg_khs(check_evaluate_data, [['KH-1', 'KH-2']])
    return {
        'position_safety': position_safety,
        'main_skill': main_skill,
        'professional_technology': professional_technology,
        'mv': mv,
        'main_work': main_work,
        'zg_zl': zg_zl,
        'kh_zl': kh_zl
    }


def get_position_safety(check_evaluate_data, lists):
    check_evaluate_data = check_evaluate_data[check_evaluate_data['CHECK_TYPE'] == 1]
    all_list = []
    # 职责履行有差距
    for i in lists:
        data = check_evaluate_data[check_evaluate_data['CODE_ADDITION'].isin(i)]
        # 计算单个附表个数
        codes = []
        for j in i:
            new_data = data[data['CODE_ADDITION'] == j]
            codes.append(len(new_data))
        # 被评价人信息
        person_info = []
        for j, k in data.iterrows():
            dic = {
                # 部门
                'dp': k['DEPARTMENT_ALL_NAME'],
                # 名称
                'name': k['RESPONSIBE_PERSON_NAME'],
                # 职位
                'gradation': k['GRADATION'],
                # 日期
                'time': str(k['YEAR']) + str(k['MONTH']),
                # 评价日期
                'eva_time': k['CREATE_TIME'],
                # 评价方式
                'eva_way': pjfs[k['EVALUATE_WAY']],
                # 编号
                'code': k['CODE'],
                # 履职分
                'score': float(k['SCORE'])
            }
            person_info.append(dic)
        dic = {
            # 评价次数
            'count': len(data),
            # 人数
            'person': len(data['RESPONSIBE_ID_CARD'].unique()),
            # 单个附表次数
            'codes': codes,
            # 表格信息
            'person_info': person_info
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_mv_yy(check_evaluate_data, problem_data):
    problem_data = problem_data[problem_data['TYPE'].isin([1, 2])]
    check_evaluate_data = check_evaluate_data[check_evaluate_data['CHECK_TYPE'] == 1]
    all_list = []
    # 职责履行有差距
    for i in [['ZL-5-2'], ['ZL-5-1']]:
        data = check_evaluate_data[check_evaluate_data['CODE_ADDITION'].isin(i)]
        # 被评价人信息
        person_info = []
        for j, k in data.iterrows():
            dic = {
                # 部门
                'dp': k['DEPARTMENT_ALL_NAME'],
                # 名称
                'name': k['RESPONSIBE_PERSON_NAME'],
                # 职位
                'gradation': k['GRADATION'],
                # 日期
                'time': str(k['YEAR']) + str(k['MONTH']),
                # 评价日期
                'eva_time': k['CREATE_TIME'],
                # 评价方式
                'eva_way': pjfs[k['EVALUATE_WAY']],
                # 编号
                'code': k['CODE'],
                # 履职分
                'score': float(k['SCORE'])
            }
            person_info.append(dic)
        dic = {
            # 评价次数
            'count': len(data),
            # 人数
            'person': len(data['RESPONSIBE_ID_CARD'].unique()),
            # 表格信息
            'person_info': person_info
        }
        all_list.append(dic)
    # 音视频问题
    pro_data = problem_data[(problem_data['CHECK_WAY'].isin([3, 4])) &
                            ((problem_data['LEVEL'].str.contains('E')) | (problem_data['LEVEL'].str.contains('F')))]
    shop_list = []
    for shop in ['科', '车间']:
        mv_list = []
        data = pro_data[pro_data['ALL_NAME'].str.contains(shop)].dropna(subset=['RESPONSIBILITY_PERSON_NAMES'])
        for i, k in data.iterrows():
            dic = {
                'dp': k['NAME'],
                'unit': k['ALL_NAME'],
                'name': ','.join(
                    [re.search(r'(\w+)\(?', i).group()[:-1] for i in k['RESPONSIBILITY_PERSON_NAMES'].split(',')]),
                'type': k['LEVEL'],
                'content': k['DESCRIPTION']
            }
            mv_list.append(dic)
        shop_list.append({
            'mv_list': mv_list,
            'count': len(data)
        })
    return {
        'all_list': all_list,
        'shop_list': shop_list
    }


def get_work_zg_kh(check_evaluate_data, lists):
    check_evaluate_data = check_evaluate_data[check_evaluate_data['CHECK_TYPE'] == 1]
    all_list = []
    # 职责履行有差距
    for i in lists:
        data = check_evaluate_data[check_evaluate_data['CODE_ADDITION'].isin(i)]
        # 被评价人信息
        person_info = []
        for j, k in data.iterrows():
            dic = {
                # 部门
                'dp': k['DEPARTMENT_ALL_NAME'],
                # 名称
                'name': k['RESPONSIBE_PERSON_NAME'],
                # 职位
                'gradation': k['GRADATION'],
                # 日期
                'time': str(k['YEAR']) + str(k['MONTH']),
                # 评价日期
                'eva_time': k['CREATE_TIME'],
                # 评价方式
                'eva_way': pjfs[k['EVALUATE_WAY']],
                # 编号
                'code': k['CODE'],
                # 履职分
                'score': float(k['SCORE'])
            }
            person_info.append(dic)
        dic = {
            # 评价次数
            'count': len(data),
            # 人数
            'person': len(data['RESPONSIBE_ID_CARD'].unique()),
            # 表格信息
            'person_info': person_info
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_work_zg_khs(check_evaluate_data, lists):
    check_evaluate_data = check_evaluate_data[check_evaluate_data['CHECK_TYPE'] == 1]
    all_list = []
    # 职责履行有差距
    for i in lists:
        data = check_evaluate_data[check_evaluate_data['CODE'].isin(i)]
        # 被评价人信息
        person_info = []
        for j, k in data.iterrows():
            dic = {
                # 部门
                'dp': k['DEPARTMENT_ALL_NAME'],
                # 名称
                'name': k['RESPONSIBE_PERSON_NAME'],
                # 职位
                'gradation': k['GRADATION'],
                # 日期
                'time': str(k['YEAR']) + str(k['MONTH']),
                # 评价日期
                'eva_time': k['CREATE_TIME'],
                # 评价方式
                'eva_way': pjfs[k['EVALUATE_WAY']],
                # 编号
                'code': k['CODE'],
                # 履职分
                'score': float(k['SCORE'])
            }
            person_info.append(dic)
        dic = {
            # 评价次数
            'count': len(data),
            # 人数
            'person': len(data['RESPONSIBE_ID_CARD'].unique()),
            # 表格信息
            'person_info': person_info
        }
        all_list.append(dic)
    return {
        'all_list': all_list
    }


def get_five(problem_data, check_evaluate_data, mv, zhanduan_data):
    all_station = []
    for i in ['供电部', '成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        pro = problem_data[problem_data['NAME'] == i]
        eva = check_evaluate_data[check_evaluate_data['STATION'] == i]
        mv_data = mv[mv['NAME'] == i]
        zd_data = zhanduan_data[zhanduan_data['NAME'] == i]
        # 调阅时长
        cost_time = float(mv_data['COST_TIME'].sum())
        # 总人数
        person = int(zd_data['COUNT'].sum())
        # 人均调阅时长
        avg_time = round(ded_zero(cost_time, person), 4)
        # 发现问题数
        find_pro = len(pro)
        # 平均问题质量分
        avg_score = round(ded_zero(float(pro['PROBLEM_SCORE'].sum()), find_pro), 2)
        eva_data = eva[eva['CODE_ADDITION'].isin(['ZL-5-1', 'ZL-5-2'])]
        # 调阅评价人次
        dy = len(eva_data)
        # 分值
        dy_score = float(eva_data['SCORE'].sum())
        # 分析室
        fxs = len(eva_data[eva_data['CHECK_TYPE'] == 1])
        fxs_score = float(eva_data[eva_data['CHECK_TYPE'] == 2]['SCORE'].sum())
        all_station.append({
            'name': i,
            'cost_time': cost_time,
            'avg_time': avg_time,
            'find_pro': find_pro,
            'avg_score': avg_score,
            'dy': dy,
            'dy_score': dy_score,
            'fxs': fxs,
            'fxs_score': fxs_score
        })
    return {
        'all_station': all_station
    }


def get_six(check_evaluate_data, eva_re_data, info_data, mv):
    all_station = []
    for i in ['供电部', '成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        eva_data = check_evaluate_data[check_evaluate_data['STATION'] == i]
        eva_re = eva_re_data[eva_re_data['NAME'] == i]
        info = info_data[info_data['NAME'] == i]
        mv_data = mv[mv['NAME'] == i]
        all_station.append({
            'name': i,
            # 音视频调阅时长
            'cost_time': float(mv_data[mv_data['CHECK_WAY'] == 3]['COST_TIME'].sum()),
            # 音视频复查
            'mv_re': len(info[info['CHECK_WAY'] == 4]),
            # 逐条复查
            'zt_re': len(eva_re[eva_re['EVALUATE_WAY'] == 1]),
            # 定期复查
            'dq_re': len(eva_re[eva_re['EVALUATE_WAY'] == 2]),
            # 履职问题
            'eva_pro': len(eva_data),
            # 现场问题
            'xc': int(info[info['CHECK_WAY'].isin([1, 2])]['PROBLEM_NUMBER'].sum()),
            # 检查部门数
            'check_dp': len(info['SHOP'].unique()),
        })
    return {
        'all_station': all_station
    }


def get_six_one(check_evaluate_data, eva_re_data, info_data, mv, center_qt_data):
    eva_data = check_evaluate_data
    eva_re = eva_re_data
    info = info_data
    mv_data = mv
    # 强化监控岗位
    gw = []
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        data = center_qt_data[center_qt_data['ALL_NAME'] == i]
        gw.append(int(data['MONITOR_COVER_POSITION']))
    dic = {
        # 音视频调阅时长
        'cost_time': float(mv_data[mv_data['CHECK_WAY'] == 3]['COST_TIME'].sum()),
        # 音视频复查
        'mv_re': len(info[info['CHECK_WAY'] == 4]),
        # 逐条复查
        'zt_re': len(eva_re[eva_re['EVALUATE_WAY'] == 1]),
        # 定期复查
        'dq_re': len(eva_re[eva_re['EVALUATE_WAY'] == 2]),
        # 履职问题
        'eva_pro': len(eva_data[eva_data['IDENTITY'] == '干部']),
        # 占比
        'ratio1': round(ded_zero(len(eva_data[eva_data['IDENTITY'] == '干部']), len(eva_data)) * 100, 2),
        # 现场问题
        'xc': int(info[info['CHECK_WAY'].isin([1, 2])]['PROBLEM_NUMBER'].sum()),
        # 调阅问题
        'dy': int(info[info['CHECK_WAY'].isin([3, 4])]['PROBLEM_NUMBER'].sum()),
        # 调阅音视频不认真
        'dybrz': int(center_qt_data['EVALUATE_NOT_SERIOUS_NUMBER'].sum()),
        # 检查部门数
        'check_dp': len(info['SHOP'].unique()),
        # 强化监控岗位
        'jkgw': gw
    }
    return dic


def get_eight(wz_data, month):
    all_station = []
    # 总人数
    person = len(wz_data['ID_CARD'].unique())
    for i in ['成都供电段', '贵阳供电段', '达州供电段', '重庆供电段']:
        data = wz_data[wz_data['ALL_NAME'] == i]
        # 各分段人数
        count = [0, 0, 0, 0, 0]
        # 个分段人数占比
        ratio = []
        # 全年累计
        all_person = len(data['ID_CARD'].unique())
        for k, j in data.groupby('ID_CARD').sum().reset_index().iterrows():
            if 4 <= j['ACTUAL_DEDUCT_SCORE'] < 8:
                count[0] += 1
            elif 8 <= j['ACTUAL_DEDUCT_SCORE'] < 12:
                count[1] += 1
            elif 12 <= j['ACTUAL_DEDUCT_SCORE'] < 16:
                count[2] += 1
            elif 16 <= j['ACTUAL_DEDUCT_SCORE'] < 20:
                count[3] += 1
            elif 20 <= j['ACTUAL_DEDUCT_SCORE'] < 24:
                count[4] += 1
        for j in count:
            ratio.append(round(ded_zero(j, all_person) * 100, 2))
        # 单月8分
        person_number = 0
        for j in data['ID_CARD'].unique().tolist():
            person_data = data[data['ID_CARD'] == j].sort_values('MONTH').reset_index()
            for x, y in person_data.iterrows():
                if y['ACTUAL_DEDUCT_SCORE'] >= 8:
                    person_number += 1
                    break

        all_station.append({
            'name': i,
            'count': count,
            'ratio': ratio,
            'person_number': person_number,
            'ratio1': round(ded_zero(person_number, all_person) * 100, 2)
        })
    return {
        'all_station': all_station,
        'person': person
    }
