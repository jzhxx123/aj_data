from app.data.util import pd_query
from app.report.gongdian.major_weekly_report_sql import *
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta
import pandas as pd


_PROBLRM_TYPE_DICT = {
        # 问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
        "作业": ['LEVEL', ['A', 'B', 'C', 'D']],
        "设备": ['LEVEL', ['E1', 'E2', 'E3', 'E4']],
        "管理": ['LEVEL', ['F1', 'F2', 'F3', 'F4']],
        "路外": ['LEVEL', ['G1', 'G2', 'G3', 'G4']],
        "反恐防暴": ['LEVEL', ['K1', 'K2', 'K3', 'K4']],
    }


_MIDDLE_PROBLRM_TYPE_DICT = {
        # 中高质量问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
        "作业": ['A', 'B', 'C'],
        "设备": ['E1', 'E2', 'E3'],
        "管理": ['F1', 'F2', 'F3'],
        "路外": ['G1', 'G2', 'G3'],
        "反恐防暴": ['K1', 'K2', 'K3'],
    }


_MAIN_PROBLRM_TYPE_DICT = {
        # 严重问题分类检索，列表第一项表示在哪个字段查，第二项列表表示查的内容
        "作业": ['A', 'B'],
        "设备": ['E1', 'E2'],
        "管理": ['F1', 'F2'],
        "路外": ['G1', 'G2'],
        "反恐防暴": ['K1', 'K2'],
    }


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')  # 2019-04-27
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    last_week_start = str(start_date - relativedelta(weeks=1))[:10]
    last_week_end = str(end_date - relativedelta(weeks=1))[:10]
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_month = start_date[5:7]
    start_day = start_date[8:]
    end_month = end_date[5:7]
    end_day = end_date[8:]
    year = start_date[:4]
    # 检查信息
    info_data = pd_query(CHECK_INFO_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_info = pd_query(CHECK_INFO_SQL.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查问题
    problem_data = pd_query(CHECK_PROBLEM_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    last_problem_data = pd_query(CHECK_PROBLEM_SQL.format(last_week_start, last_week_end)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 风险
    risk_data = pd_query(risk_sql.format(start_date, end_date)).drop_duplicates().drop_duplicates(subset=['PK_ID']).reset_index()
    # 履职问题
    eva_data = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(start_date, end_date))
    last_eva = pd_query(CHECK_EVALUATE_SITUATION_SQL.format(last_week_start, last_week_end))
    # 履职工作量
    eva_re = pd_query(CHECK_EVALUATE_REVIEW_SQL.format(start_date, end_date))
    # 设备监控调阅
    mv_data = pd_query(CHECK_MV_COST_TIME_SQL.format(start_date, end_date)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 站段人员信息
    zd_data = pd_query(ZHANDUAN_PERSON_SQL)
    # 履职复查
    eva_re_data = pd_query(eva_review_sql.format(start_date, end_date))
    # 重要检查岗位
    main_position = pd_query(main_post_sql.format(start_date, end_date))

    first = get_first(info_data, last_info, problem_data, last_problem_data, risk_data)
    second = get_second(eva_data, last_eva)
    third = get_third(eva_data, info_data, last_info, problem_data, mv_data, eva_re, eva_re_data, main_position)
    fourth = get_fourth(problem_data, zd_data)
    file_name = f'{start_date}至{end_date}供电系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        'start_month': start_month,
        'start_day': start_day,
        'end_month': end_month,
        'end_day': end_day,
        'year': year,
        "major": '供电',
        "hierarchy": "MAJOR",
        "created_at": dt.now(),
        'file_name': file_name,
        'first': first,
        'second': second,
        'third': third,
        'four': fourth
    }
    return result


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count * 100, 2)
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


# ----------------------------------------------------------第一部分----------------------------------------------------
def get_first(info_data, last_info, problem_data, last_problem_data, risk_data):
    """
    检查信息
    :param start_date:
    :param end_date:
    :return:
    """
    # 1.1
    first_one = get_first_one()
    # 1.2
    first_two = get_first_two(info_data, last_info, problem_data, last_problem_data)
    # 1.3
    first_three = get_first_three(problem_data, last_problem_data)
    # 1.4
    first_four = get_first_four(problem_data, last_problem_data)
    # 1.5
    first_five = get_first_five(problem_data, risk_data)
    return {
        'first_one': first_one,
        'first_two': first_two,
        'first_three': first_three,
        'first_four': first_four,
        'first_five': first_five,
    }


def get_first_one():
    pass


def get_first_two(info_data, last_info, problem_data, last_problem_data):
    """
    1.2
    :return:
    """
    # 检查发现问题
    check_pro = int(info_data['PROBLEM_NUMBER'].sum())
    last_check = int(last_info['PROBLEM_NUMBER'].sum())
    ratio1 = calculate_month_and_ring_ratio([check_pro, last_check])
    # 供电责任问题
    duty_pro = len(problem_data)
    # 占比
    diff1 = round(ded_zero(check_pro, duty_pro) * 100, 2)
    # 其他类问题
    all_pro = []
    for i in ['作业', '设备', '管理', '反恐防暴', '路外']:
        data = problem_data[problem_data['LEVEL'].isin(_PROBLRM_TYPE_DICT[i][1])]
        last_data = last_problem_data[last_problem_data['LEVEL'].isin(_PROBLRM_TYPE_DICT[i][1])]
        dic = {
            'name': i,
            'count': len(data),
            'diff': round(ded_zero(len(data), duty_pro) * 100, 2),
            'ratio': calculate_month_and_ring_ratio([len(data), len(last_data)])
        }
        all_pro.append(dic)
    return {
        'check_pro': check_pro,
        'ratio1': ratio1,
        'diff1': diff1,
        'all_pro': all_pro
    }


def get_first_three(problem_data, last_problem_data):
    """
    1.3
    :param risk_data:
    :param last_risk_data:
    :return:
    """
    # 严重问题数
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    # 问题分类
    all_pro = []
    for i in ['作业', '设备', '管理']:
        data = problem_data[problem_data['LEVEL'].isin(_MAIN_PROBLRM_TYPE_DICT[i])]
        last_data = last_problem_data[last_problem_data['LEVEL'].isin(_MAIN_PROBLRM_TYPE_DICT[i])]
        dic = {
            'name': i,
            'count': len(data),
            'diff': round(ded_zero(len(data), main_pro) * 100, 2),
            'ratio': calculate_month_and_ring_ratio([len(data), len(last_data)])
        }
        all_pro.append(dic)
    return {
        'all_pro': all_pro
    }


def get_first_four(problem_data, last_problem_data):
    """
    1.4
    :param risk_data:
    :param last_risk_data:
    :return:
    """
    # 中高质量问题数
    main_pro = len(problem_data[problem_data['LEVEL'].isin(['A', 'B', 'C', 'E1', 'E2', 'E3', 'F1', 'F2', 'F3'])])
    # 问题分类
    all_pro = []
    for i in ['作业', '设备', '管理']:
        data = problem_data[problem_data['LEVEL'].isin(_MIDDLE_PROBLRM_TYPE_DICT[i])]
        last_data = last_problem_data[last_problem_data['LEVEL'].isin(_MIDDLE_PROBLRM_TYPE_DICT[i])]
        dic = {
            'name': i,
            'count': len(data),
            'diff': round(ded_zero(len(data), main_pro) * 100, 2),
            'ratio': calculate_month_and_ring_ratio([len(data), len(last_data)])
        }
        all_pro.append(dic)
    return {
        'all_pro': all_pro
    }


def get_first_five(problem_data, risk_data):
    """
    1.5
    :param risk_data:
    :param last_risk_data:
    :return:
    """
    # 检查发现问题
    pro = len(problem_data)
    # 风险前5
    all_risk = []
    for i in ['设备质量', '专业管理', '劳动安全', '新线管理', '自轮设备']:
        k = problem_data[problem_data['RISK_NAMES'].str.contains(i)]
        # 风险集中
        station = []
        for j, k in k.groupby('NAME').count().reset_index().iterrows():
            dic = {
                'name': k['NAME'],
                'count': k['LEVEL']
            }
            station.append(dic)
        dic = {
            'name': i,
            'count': len(k),
            'diff': round(ded_zero(len(k), pro) * 100, 2),
            'station': station
        }
        all_risk.append(dic)
    return {
        'pro': pro,
        'all_risk': all_risk
    }


# ------------------------------------------------------------------第二部分--------------------------------------------
def get_second(eva_data, last_eva):
    """
    干部履职评价
    :param start_date:
    :param end_date:
    :return:
    """
    # 2.1
    second_one = get_evaluate(eva_data)
    # 2.2
    second_two = get_second_two(eva_data, last_eva)
    second_three =get_second_three(eva_data)
    return {
        'second_one': second_one,
        'second_two': second_two,
        'second_three': second_three
    }


def get_evaluate(data):
    """
    2.1
    :param data:
    :return:
    """
    # 总人数
    person = len(data['RESPONSIBE_ID_CARD'].unique())
    new_data = data[data['FK_PERSON_GRADATION_RATIO_ID'] != 5]
    # 干部被记分人次
    gb = len(new_data['RESPONSIBE_ID_CARD'].unique())
    ratio1 = round(ded_zero(gb, person) * 100, 2)
    # 职务分
    gra_person = []
    gra_ratio = []
    for i in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员']:
        gra_data = new_data[new_data['GRADATION'].str.contains(i)]
        gra_person.append(len(gra_data['RESPONSIBE_ID_CARD'].unique()))
        gra_ratio.append(round(ded_zero(len(gra_data), person) * 100, 2))
    # 扣分等级
    count = [0, 0, 0, 0, 0, 0]
    max_score = 0
    max_dp = []
    for i, k in new_data.groupby(['RESPONSIBE_ID_CARD', 'DEPARTMENT_ALL_NAME']).\
        sum().sort_values('SCORE', ascending=False).reset_index().iterrows():
        if k['SCORE'] < 2:
            count[0] += 1
        elif k['SCORE'] < 4:
            count[1] += 1
        elif k['SCORE'] < 6:
            count[2] += 1
        elif k['SCORE'] < 8:
            count[3] += 1
        elif k['SCORE'] < 10:
            count[4] += 1
        elif k['SCORE'] < 12:
            count[5] += 1
        else:
            count[6] += 1
        if i == 0:
            max_score = float('%.4f' % k['SCORE'])
            max_dp.append(k['DEPARTMENT_ALL_NAME'])
        if float('%.4f' % k['SCORE']) == max_score:
            max_dp.append(k['DEPARTMENT_ALL_NAME'])
    dict = pd.value_counts(max_dp)
    max_dps = {}
    for x in range(len(dict)):
        max_dps[dict.index[x]] = dict.values[x]
    return {
        'gb_count': len(new_data),
        'gb': gb,
        'ratio1': ratio1,
        'count': count,
        'max_score': max_score,
        'max_dp': max_dps,
        'gra_person': gra_person,
        'gra_ratio': gra_ratio
    }


def get_second_two(data, last_data):
    """
    2.2
    :param data:
    :param last_data:
    :return:
    """
    # 定期检查
    dq = len(data[data['EVALUATE_WAY'] == 2])
    # 人数
    person = len(data[data['EVALUATE_WAY'] == 2]['RESPONSIBE_ID_CARD'].unique())
    ratio1 = calculate_month_and_ring_ratio([dq, len(last_data[last_data['EVALUATE_WAY'] == 2])])
    # 干部履职问题
    data = data[data['IDENTITY'] == '干部']
    last_data = last_data[last_data['IDENTITY'] == '干部']
    # 问题数
    pro_count = len(data)
    # 人数
    gb_person = len(data['RESPONSIBE_ID_CARD'].unique())
    # 记分
    score = float(data['SCORE'].sum())
    ratio2 = calculate_month_and_ring_ratio([score, float(last_data['SCORE'].sum())])
    # 最高记分
    max_score = 0
    for j, k in data.groupby('RESPONSIBE_ID_CARD').sum().sort_values('SCORE', ascending=False).reset_index().iterrows():
        if j == 0:
            max_score = float(k['SCORE'])
            break
    return {
        'dq': dq,
        'person': person,
        'ratio1': ratio1,
        'pro_count': pro_count,
        'gb_person': gb_person,
        'score': score,
        'ratio2': ratio2,
        'max_score': max_score
    }


def get_second_three(eva_data):
    # 按评价记分
    eva_score = []
    for i in [1, 2]:
        data = eva_data[eva_data['CHECK_TYPE'] == i]
        dic = {
            'count': len(data),
            'person': len(data['RESPONSIBE_ID_CARD'].unique())
        }
        eva_score.append(dic)
    # 按评价方式
    eva_type = []
    for i in range(4):
        data = eva_data[eva_data['EVALUATE_WAY'] == i]
        dic = {
            'count': len(data),
            'person': len(data['RESPONSIBE_ID_CARD'].unique())
        }
        eva_type.append(dic)
    # 按职务分
    all_gra = []
    for i in ['局管领导人员', '正科职管理人员', '副科职管理人员', '一般管理和专业技术人员', '非管理和专业技术人员']:
        data = eva_data[eva_data['GRADATION'] == i]
        stations = []
        # 评价最多站段
        for j in ['成都供电段', '重庆供电段', '达州供电段', '贵阳供电段']:
            new_data = data[data['STATION'] == j]
            dic = {
                'name': j,
                'count': len(new_data['RESPONSIBE_ID_CARD'].unique())
            }
            stations.append(dic)
        if len(stations) != 0:
            station = sorted(stations, key=lambda x: x['count'], reverse=True)
            max_station = []
            max_count = station[0]['count']
            for x in station:
                if x['count'] == max_count:
                    dic = {
                        'name': x['name'],
                        'count': max_count
                    }
                    max_station.append(dic)
            dic = {
                'name': i,
                'count': len(data),
                'person': len(data['RESPONSIBE_ID_CARD'].unique()),
                'station': max_station
            }
        else:
            dic = {
                'name': i,
                'count': len(data),
                'person': len(data['RESPONSIBE_ID_CARD'].unique()),
                'station': [{
                    'name': '无',
                    'count': 0
                }]
            }
        all_gra.append(dic)
    return {
        'eva_score': eva_score,
        'eva_type': eva_type,
        'all_gra': all_gra
    }


# ----------------------------------------------------------------------第三部分----------------------------------------
def get_third(eva_data, info_data, last_info, problem_data, mv_data, eva_re, eva_re_data, main_position):
    # 3.1
    third_one = get_third_one(eva_data, eva_re, eva_re_data)
    # 3.2
    third_two = get_third_two(info_data, last_info, mv_data, main_position)
    # 3.3
    third_three = get_third_three(info_data, mv_data, problem_data)
    # 3.4
    third_four = get_third_four(info_data, mv_data, problem_data)
    return {
        'third_one': third_one,
        'third_two': third_two,
        'third_three': third_three,
        'third_four': third_four
    }


def get_third_one(eva_data, eva_re, eva_re_data):
    all_count = []
    for i in [1, 2]:
        eva = eva_data[eva_data['CHECK_TYPE'] == i]
        re = eva_re[eva_re['CHECK_TYPE'] == i]
        eva_data = eva_re_data[eva_re_data['CHECK_TYPE'] == i]
        # 逐条
        zt = len(re[re['EVALUATE_WAY'] == 1]) + len(eva_data[eva_data['EVALUATE_WAY'] == 1])
        # 定期
        dq = len(re[re['EVALUATE_WAY'] == 2]) + len(eva_data[eva_data['EVALUATE_WAY'] == 2])
        # 音视频
        mv = len(eva_data[eva_data['EVALUATE_TYPE'] == 3])
        all_count.append([zt, dq, mv])
    major = []
    for i in range(len(all_count[0])):
        major.append(all_count[0][i] + all_count[1][i])
    return {
        'all_count': all_count,
        'major': major
    }


def get_third_two(info_data, last_info, mv_data, main_position):

    # 设备监控调阅
    shebei = info_data[info_data['CHECK_WAY'].isin([3, 4])]
    last_shebei = last_info[last_info['CHECK_WAY'].isin([3, 4])]
    ratio1 = calculate_month_and_ring_ratio([len(shebei), len(last_shebei)])
    # 发现问题
    find_pro = int(shebei['PROBLEM_NUMBER'].sum())
    last_pro = int(last_shebei['PROBLEM_NUMBER'].sum())
    ratio2 = calculate_month_and_ring_ratio([find_pro, last_pro])
    # 调阅覆盖岗位
    position = _calc_media_check_position(mv_data, main_position)
    return {
        'shebei': len(shebei),
        'ratio1': ratio1,
        'find_pro': find_pro,
        'ratio2': ratio2,
        'position': position
    }


def _calc_media_check_position(data, stations):
    """
    统计重点检查岗位数量，限定检查地点
    """
    count = 0
    _addr_list = []
    for _idx, row in data.iterrows():
        _addr_list += row['MONITOR_POST_NAMES'].split(',')
    for addr in list(set(_addr_list)):
        if len(stations[stations['NAME'].str.contains(addr)]) > 0:
            count += 1
    return {
        'count': count,
        'all_count': len(stations)
    }


def get_third_three(info_data, mv_data, problem_data):
    all_major = []
    all_pro = 0
    for i in ['接触网', '电力', '变配电', '自轮设备']:
        info = info_data[info_data['CHECK_ITEM_NAMES'].str.contains(i)]
        mv = mv_data[mv_data['CHECK_ITEM_NAMES'].str.contains(i)]
        pro = problem_data[problem_data['CHECK_ITEM_NAME'].str.contains(i)]
        # 调阅检查
        dy = len(info[info['CHECK_WAY'] == 3])
        dy_person = len(info[info['CHECK_WAY'] == 3]['ID_CARD'].unique())
        dy_time = float('%.4f' % mv[mv['CHECK_WAY'] == 3]['COST_TIME'].sum())
        # 复查调阅
        re = len(info[info['CHECK_WAY'] == 4])
        re_person = len(info[info['CHECK_WAY'] == 4]['ID_CARD'].unique())
        re_time = float('%.4f' % mv[mv['CHECK_WAY'] == 4]['COST_TIME'].sum())
        # 发现问题
        find_pro = len(pro[pro['CHECK_WAY'].isin([3, 4])])
        all_pro += find_pro
        dic = {
            'name': i,
            'dy': dy,
            'dy_person': dy_person,
            'dy_time': dy_time,
            're': re,
            're_person': re_person,
            're_time': re_time,
            'find_pro': find_pro
        }
        all_major.append(dic)
    # 其他问题数
    qt_pro = len(problem_data) - all_pro
    qt_data = problem_data
    for i in ['接触网', '电力', '变配电', '自轮设备']:
        qt_data = qt_data[~qt_data['CHECK_ITEM_NAME'].str.contains(i)]
    qt_data['PRO_NAME'] = qt_data['CHECK_ITEM_NAME'].apply(lambda x: x.split('-')[1])
    qt_name = list(qt_data['PRO_NAME'].unique())
    return {
        'all_major': all_major,
        'qt_pro': qt_pro,
        'qt_name': qt_name
    }


def get_third_four(info_data, mv_data, problem_data):
    all_major = []
    all_pro = 0
    for i in ['接触网', '电力', '变配电', '自轮设备']:
        info = info_data[(info_data['CHECK_ITEM_NAMES'].str.contains(i)) & (info_data['DEPARTMENT_ALL_NAME'].str.contains('分析中心'))]
        mv = mv_data[(mv_data['CHECK_ITEM_NAMES'].str.contains(i)) & (mv_data['DEPARTMENT_ALL_NAME'].str.contains('分析中心'))]
        pro = problem_data[(problem_data['CHECK_ITEM_NAME'].str.contains(i)) & (problem_data['DEPARTMENT_ALL_NAME'].str.contains('分析中心'))]
        # 调阅检查
        dy = len(info[info['CHECK_WAY'] == 3])
        dy_person = len(info[info['CHECK_WAY'] == 3]['ID_CARD'].unique())
        dy_time = float(mv[mv['CHECK_WAY'] == 3]['COST_TIME'].sum())
        # 复查调阅
        re = len(info[info['CHECK_WAY'] == 4])
        re_person = len(info[info['CHECK_WAY'] == 4]['ID_CARD'].unique())
        re_time = float(mv[mv['CHECK_WAY'] == 4]['COST_TIME'].sum())
        # 发现问题
        find_pro = len(pro[pro['CHECK_WAY'].isin([3, 4])])
        all_pro += find_pro
        dic = {
            'name': i,
            'dy': dy,
            'dy_person': dy_person,
            'dy_time': dy_time,
            're': re,
            're_person': re_person,
            're_time': re_time,
            'find_pro': find_pro
        }
        all_major.append(dic)
    # 其他问题数
    qt_pro = len(problem_data) - all_pro
    qt_data = problem_data
    for i in ['接触网', '电力', '变配电', '自轮设备']:
        qt_data = qt_data[~qt_data['CHECK_ITEM_NAME'].str.contains(i)]
    qt_data['PRO_NAME'] = qt_data['CHECK_ITEM_NAME'].apply(lambda x: x.split('-')[1])
    qt_name = list(qt_data['PRO_NAME'].unique())
    return {
        'all_major': all_major,
        'qt_pro': qt_pro,
        'qt_name': qt_name
    }


# -------------------------------------------------------------问题质量分-----------------------------------------------
def get_fourth(problem_data, zd_data):
    # 总质量分
    score = float(problem_data['CHECK_SCORE'].sum())
    # 人均质量分
    avg_score = round(ded_zero(score, int(zd_data['COUNT'].sum())), 2)
    # 问题平均分
    avg_pro = round(ded_zero(score, len(problem_data)), 2)
    all_station = []
    for i in ['成都供电段', '重庆供电段', '达州供电段', '贵阳供电段']:
        data = problem_data[problem_data['NAME'] == i]
        zd = int(zd_data[zd_data['NAME'] == i]['COUNT'])
        dic = {
            'name': i,
            # 人均质量分
            'avg_score': round(ded_zero(float(data['CHECK_SCORE'].sum()), zd), 2),
            # 问题平均质量分
            'avg_pro': round(ded_zero(float(data['CHECK_SCORE'].sum()), len(data)), 2)
        }
        all_station.append(dic)
    return {
        'avg_score': avg_score,
        'avg_pro': avg_pro,
        'all_station': all_station
    }

