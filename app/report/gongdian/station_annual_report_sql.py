CHECK_SAFETY_INFO_SQL = """SELECT
        a.OCCURRENCE_TIME,
        a.MAIN_TYPE,
        a.DETAIL_TYPE,
        a.CATEGORY,
        a.PROFESSION,
        a.RESPONSIBILITY_DIVISION_NAME,
        a.RESPONSIBILITY_UNIT,
        a.RANK,
        a.CODE,
        a.NAME,
        c.DEPARTMENT_ID,
        d.RISK_NAME,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS d
                    ON d.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1}')
            and c.TYPE3='{2}'"""

CHECK_SAFETY_INFO_SQL1 = """SELECT
        a.OCCURRENCE_TIME,
        a.MAIN_TYPE,
        a.DETAIL_TYPE,
        a.CATEGORY,
        a.PROFESSION,
        a.RESPONSIBILITY_DIVISION_NAME,
        a.RESPONSIBILITY_UNIT,
        a.RANK,
        a.CODE,
        a.NAME,
        c.DEPARTMENT_ID,
        d.RISK_NAME,
        f.PROBLEM_POINT,
        g.RESPONSIBILITY_IDENTIFIED
    FROM
        t_safety_produce_info AS a
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS b
                    ON a.PK_ID = b.FK_SAFETY_PRODUCE_INFO_ID
            LEFT JOIN
        t_department AS c ON b.FK_DEPARTMENT_ID = c.DEPARTMENT_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit_and_risk AS d
                    ON d.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_safety_produce_info_problem_base AS e
                    ON e.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            LEFT JOIN
        t_problem_base AS f ON f.PK_ID = e.FK_PROBLEM_BASE_ID
            LEFT JOIN
        t_safety_produce_info_responsibility_unit AS g
                    ON g.FK_SAFETY_PRODUCE_INFO_ID = a.PK_ID
            WHERE a.OCCURRENCE_TIME >= date('{0}') AND a.OCCURRENCE_TIME < date('{1}')
            and c.TYPE2='1ACE7D1C80B04456E0539106C00A2E70KSC'"""

CHECK_LIANGHUA_SQL = """SELECT
        a.RESPONSIBE_PERSON_NAME,
        a.EVALUATE_CONTENT,
        a.`CODE`,
        a.EVALUATE_WAY,
        c.ALL_NAME,
        b.POSITION
FROM t_check_evaluate_info a
LEFT JOIN t_department c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
LEFT JOIN t_person b on a.RESPONSIBE_ID_CARD = b.ID_CARD
WHERE a.CREATE_TIME >= '{0}' AND a.CREATE_TIME <= '{1}'
and c.TYPE3 = '{2}'"""

CHECK_PROBLEM_ANALYSIS_SQL = """select a.DEPARTMENT_ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,b.`LEVEL`,b.PROBLEM_SCORE,
b.PERSON_NAME,b.PROBLEM_CLASSITY_NAME,b.RISK_NAMES,b.PROBLEM_POINT,b.CHECK_ITEM_NAME,a.IS_GENBAN,b.TYPE,
b.EXECUTE_DEPARTMENT_NAME,b.RESPONSIBILITY_DEPARTMENT_NAME,c.`STATUS`
from t_check_info a,t_check_problem b
LEFT JOIN t_check_problem_and_responsible_department c on b.PK_ID=c.FK_CHECK_PROBLEM_ID
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and a.PK_ID=b.FK_CHECK_INFO_ID and EXISTS
(SELECT c.DEPARTMENT_ID from t_department c where c.type3 = '{2}' and b.EXECUTE_DEPARTMENT_ID=c.department_id)"""

CHECK_ALL_PROBLEM_ANALYSIS_SQL = """
select a.DEPARTMENT_ALL_NAME,a.PROBLEM_NUMBER,a.CHECK_WAY,b.`LEVEL`,b.PROBLEM_SCORE,b.PERSON_NAME,
b.PROBLEM_CLASSITY_NAME,b.RISK_NAMES,b.PROBLEM_POINT,b.CHECK_ITEM_NAME,a.IS_GENBAN
from t_check_info a,t_check_problem b
where a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and a.PK_ID=b.FK_CHECK_INFO_ID and EXISTS
(SELECT c.DEPARTMENT_ID from t_department c where c.type2 = '1ACE7D1C80B04456E0539106C00A2E70KSC' and b.EXECUTE_DEPARTMENT_ID=c.department_id)"""

check_problem_sql = """SELECT c.CHECK_WAY,a.ALL_NAME,p.PROBLEM_CLASSITY_NAME,p.PROBLEM_SCORE,c.DEPARTMENT_ALL_NAME,
p.`LEVEL`,p.RISK_LEVEL,c.PROBLEM_NUMBER,e.IDENTITY,p.IS_RED_LINE ,p.PERSON_NAME,b.`NAME`
from t_check_info c
LEFT JOIN t_check_problem p on c.pk_id=p.fk_check_info_id
LEFT JOIN t_department a on p.EXECUTE_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.TYPE3
LEFT JOIN t_person e on e.ID_CARD = c.ID_CARD
where c.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and a.TYPE3 = '{2}'"""

CHECK_EVALUATE_SCORE_SQL = """SELECT a.EVALUATE_TYPE,a.EVALUATE_WAY,a.CHECK_TYPE,a.SCORE,a.GRADATION,a.`CODE`,c.SITUATION,
a.CODE_ADDITION,a.RESPONSIBE_PERSON_NAME,b.ALL_NAME,c.ITEM_NAME
from t_check_evaluate_info a
LEFT JOIN t_check_evaluate_config c on a.FK_CHECK_EVALUATE_CONFIG_ID = c.PK_ID
LEFT JOIN t_department b on a.CHECK_PERSON_DEPARTMENT_ID = b.DEPARTMENT_ID
where a.CREATE_TIME BETWEEN '{0}' and '{1}'
and b.TYPE3='{2}'
"""

check_info_sql = """SELECT
        a.CHECK_WAY,
        a.DEPARTMENT_ALL_NAME AS ALL_NAME,
        a.IS_YECHA,
        a.PROBLEM_NUMBER,
        b.COST_TIME,
        e.`NAME`
    FROM
        t_check_info AS a
        LEFT JOIN t_check_info_and_media AS b ON b.FK_CHECK_INFO_ID = a.PK_ID
        left join t_person c on a.ID_CARD=c.ID_CARD
        left join t_department d on c.FK_DEPARTMENT_ID=d.DEPARTMENT_ID
        LEFT JOIN t_department e on e.DEPARTMENT_ID=d.TYPE3
    WHERE
        a.SUBMIT_TIME BETWEEN '{0}' and '{1}'
        AND d.TYPE3 = '{2}'
        """

check_evaluate_sql = """SELECT
        a.*,
        b.ITEM_NAME,
        b.SITUATION,
        c.BUSINESS_CLASSIFY,
        c.ALL_NAME,
        d.NAME AS MAJOR,
        e.JOB,
        f.NAME AS STATION
    FROM
        t_check_evaluate_info AS a
            LEFT JOIN
        t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
            LEFT JOIN
        t_department AS c ON c.DEPARTMENT_ID = a.RESPONSIBE_DEPARTMENT_ID
            LEFT JOIN
        t_department AS d ON d.DEPARTMENT_ID = c.TYPE2
            LEFT JOIN
        t_person AS e ON e.ID_CARD = a.RESPONSIBE_ID_CARD
            LEFT JOIN
        t_department AS f ON f.DEPARTMENT_ID = c.TYPE3
    WHERE
        a.CREATE_TIME >= '{0}'
            AND a.CREATE_TIME <= '{1}'
            and c.TYPE3 = '{2}'
"""

check_evaluate_review_sql = """SELECT * FROM t_check_evaluate_review_person a
LEFT JOIN t_department b on a.FK_REVIEW_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE a.CREATE_TIME >= '{0}' AND a.CREATE_TIME <= '{1}'
and b.TYPE3 = '{2}'
"""

CHECK_MV_COST_TIME_SQL = """SELECT b.CHECK_PERSON_NAMES,a.COST_TIME,b.PROBLEM_NUMBER,b.CHECK_WAY,d.ALL_NAME,c.PROBLEM_SCORE
FROM t_check_info_and_media AS a
INNER JOIN t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
LEFT JOIN t_check_problem c on c.FK_CHECK_INFO_ID = b.PK_ID
INNER JOIN t_department d on c.EXECUTE_DEPARTMENT_ID = d.DEPARTMENT_ID
WHERE b.SUBMIT_TIME BETWEEN '{0}' and '{1}'
and d.TYPE3 = '{2}'"""

CHECK_MV_SQL = """SELECT a.CHECK_TYPE,a.EVALUATE_WAY,a.EVALUATE_TYPE,a.IS_REVIEW,c.CHECK_WAY,c.IS_EVALUATE,
c.PROBLEM_NUMBER,c.CHECK_ITEM_NAMES,b.ALL_NAME from t_check_info c,t_check_evaluate_check_person a
LEFT JOIN t_department b on a.FK_RESPONSIBE_DEPARTMENT_ID = b.DEPARTMENT_ID
WHERE a.FK_CHECK_OR_PROBLEM_ID = c.PK_ID
and a.CREATE_TIME BETWEEN '{0}' and '{1}'
and b.TYPE3='{2}'
"""

cards = """SELECT
        b.ALL_NAME,
        c.BELONG_PROFESSION_NAME AS MAJOR,
        c.`NAME` AS STATION
    FROM
        t_person AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
        LEFT JOIN t_department AS c ON c.DEPARTMENT_ID = b.TYPE3
        LEFT JOIN t_department AS d ON d.DEPARTMENT_ID = b.TYPE4
    WHERE
         b.TYPE3 = '{0}'
"""