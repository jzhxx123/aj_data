import threading
from datetime import datetime as dt
from app.report.analysis_report_manager import DailyAnalysisReport
from app.data.util import pd_query
from app.utils.common_func import get_department_name_by_dpid
from app.report.gongdian.station_daily_report_sql import *

locals_data = threading.local()

_MAIN_PROBLEM = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4']
}


def get_data(year, month, day, station_id):
    report_date = dt(year, month, day)
    locals_data.station_id = station_id
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    station_name = get_department_name_by_dpid(station_id)
    times = (params[0][0], params[0][1])
    first = get_first(times, month, day)
    second = get_second(times)
    four = get_four(times)
    return {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "STATION",
        'major': '供电',
        'station_id': station_id,
        'station_name': station_name,
        "file_name": '{0}安全分析日报.xlsx'.format(station_name),
        "created_at": dt.now(),
        'first': first,
        'second': second,
        'four': four
    }


def get_first(times, month, day):
    # 事故故障情况
    first_two = get_first_two(times, month, day)
    return {
        'first_two': first_two
    }


def get_first_two(times, month, day):
    infos = []
    without_accident = pd_query(without_accident_sql.format(times[0], times[1], locals_data.station_id))
    if len(without_accident) == 0:
        content = "{0}月{1}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" .format(month, day)
        info = {
            'acc': '无',
            'fault': '无'
        }
        infos.append(info)
    else:
        data = without_accident
        acc = len(data[data['MAIN_TYPE'] == 1])
        fault = len(data[data['MAIN_TYPE'] == 2])
        # 行车、劳动、路外
        safety_list = []
        for i in [1, 2]:
            new_data = data[data['DETAIL_TYPE'] == i]
            safety_list.append(len(new_data[new_data['MAIN_TYPE'] == 1]))
        content = "{0}月{1}日，发生事故{2}件、故障{3}件。" \
                  "\n1.行车安全：发生{4}件事故。" \
                  "\n2.劳动安全：发生{5}件事故。" .\
            format(month, day, acc, fault, safety_list[0], safety_list[1])
    return content


def get_second(times):
    """
    监督检查基本情况
    :param times:
    :param worksheet:
    :return:
    """
    # 检查信息
    check_info = pd_query(check_info_sql.format(times[0], times[1], locals_data.station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查问题
    check_problem = pd_query(check_problem_sql.format(times[0], times[1], locals_data.station_id)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 全局总体检查情况
    second_one = get_second_one(check_info, check_problem)
    return {
        'second_one': second_one,
    }


def get_second_one(check_info, check_problem):
    dic = {}
    # 检查基本情况
    xc = len(check_info[check_info['CHECK_WAY'].isin([1, 2])])
    mv = len(check_info[check_info['CHECK_WAY'].isin([3, 4])])
    tc = len(check_info[check_info['CHECK_WAY'].isin([2])])
    # 发现问题个数
    find_problem = int(check_info['PROBLEM_NUMBER'].sum())
    # 严重问题个数
    main_problem = check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])]
    # 路局发现严重问题
    luju_main = main_problem[main_problem['TYPE'].isin([1, 2])]
    # 问题三项
    dic['one'] = '全局下现场检查{0}人次，音视频调阅{3}人次，添乘检查{4}人次，发现问题{1}个。性质严重问题{2}个，' \
                 '路局层面发现的严重问题{5}个' .format(xc, find_problem, len(main_problem), mv, tc, len(luju_main))
    # 昨日安全严重问题
    content = []
    index = 1
    for i in luju_main['DESCRIPTION'].unique():
        content.append(str(index) + '.' + i)
        index += 1
    dic['two'] = '\n'.join(list(content)) if len(content) != 0 else '无'
    return dic


def get_four(times):
    """
    风险预警
    :param times:
    :return:
    """
    warning_data = pd_query(warning_notification_sql.format(times[0], times[1], locals_data.station_id))
    # 路局
    road_data = warning_data[warning_data['HIERARCHY'] == 1].head(3)
    company_warn_list = []
    if len(road_data) == 0:
        company_warn_list = ['无']
    else:
        for index in range(0, len(road_data)):
            company_warn_list.append("""{0}.{1}""".format(
                index + 1,
                road_data['CONTENT'][index])
            )
    return {
        'company_warn_list': '\n'.join(company_warn_list)
    }
