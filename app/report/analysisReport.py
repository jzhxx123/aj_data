from dateutil.relativedelta import relativedelta
from datetime import datetime as dt

from app.report.analysisReport_wb import get_analysis_data
from app.report.util import get_report_list
from docxtpl import DocxTemplate


def get_analysis_report_list(start_time, end_time):
    title = '{year}年{month}月份集团公司安全情况分析报告'
    TYPE = 'analysis'
    reports = get_report_list(start_time, end_time, title, TYPE)
    today = dt.strptime(end_time, "%Y-%m-%d")
    if today.day != 25:
        date = today
        if today.day > 25:
            date = today + relativedelta(months=1)
        year, month = date.year, date.month
        date = end_time.replace('-', '/')
        reports.append({"TITLE": title.format(year=year, month=month), 'DATE': date, 'TYPE': TYPE})
    return reports


def save_analysis_report(start_date, end_date, file_path):
    """集团公司安全情况月度分析报告
    Arguments:
        month {int} -- 年份月份信息
    Returns:
        str -- 是否保存成功
    """
    tpl = DocxTemplate('app/report/template/analysis_report_template_limit.docx')
    # 第一部分
    result = get_analysis_data(start_date, end_date)
    tpl.render(result)
    tpl.save(file_path)
    return True
