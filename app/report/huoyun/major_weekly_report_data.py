from app.data.util import pd_query
from datetime import datetime as dt
from app.report.huoyun.major_weekly_report_sql import *


def get_data(start_date, end_date):
    if isinstance(start_date, str):
        start_date = dt.strptime(start_date, '%Y-%m-%d')
        end_date = dt.strptime(end_date, '%Y-%m-%d')
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    start_month = start_date[5:7]
    start_day = start_date[8:]
    end_month = end_date[5:7]
    end_day = end_date[8:]
    year = start_date[:4]
    # 检查信息
    info_data = pd_query(check_info_sql.format(start_date, end_date))
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date))
    # 监控设备调阅
    mv_data = pd_query(CHECK_MV_TIME_OR_PROBLEM_SQL.format(start_date, end_date))

    first = get_fitst(info_data, problem_data, mv_data)
    second = get_second(problem_data)
    file_name = f'{start_date}至{end_date}货运系统安全管理周分析.docx'
    result = {
        "start_date": start_date,
        "end_date": end_date,
        'start_month': start_month,
        'start_day': start_day,
        'end_month': end_month,
        'end_day': end_day,
        'year': year,
        "major": '货运',
        "hierarchy": "MAJOR",
        "created_at": dt.now(),
        'file_name': file_name,
        "first": first,
        'second': second
    }
    return result


MAIN_PROBLEM_TYPE = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4'],
    '性质严重': ['A', 'B', 'F1', 'F2', 'E1', 'E2']
}


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 2) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def get_fitst(info_data, problem_data, mv_data):
    # 下现场及视频监控
    count = len(info_data[info_data['CHECK_WAY'].isin([1, 2, 3, 4])])
    # 发现问题数
    pro_count = len(problem_data)
    code_type = []
    for i in ['作业', '设备', '管理']:
        data = problem_data[problem_data['LEVEL'].isin(MAIN_PROBLEM_TYPE[i])]
        dic = {
            'name': i,
            'count': len(data),
            'ratio': round(ded_zero(len(data), pro_count) * 100, 2)
        }
        code_type.append(dic)
    # 监控调阅
    jk_count = len(info_data[info_data['CHECK_WAY'] == 3])
    # 调阅时长
    cost_time = float('%.4f' % mv_data[mv_data['CHECK_WAY'] == 3]['COST_TIME'].sum())
    return {
        'count': count,
        'pro_count': pro_count,
        'code_type': code_type,
        'jk_count': jk_count,
        'cost_time': cost_time
    }


def get_second(problem_data):
    # 带脱轨器防护信号
    tgxh = problem_data[problem_data['PROBLEM_POINT'].str.contains('带脱轨器防护信号')]
    # 手机
    phone = problem_data[problem_data['PROBLEM_POINT'].str.contains('手机')]
    # 固定径路、道心、枕木头
    wzsx = problem_data[(problem_data['PROBLEM_POINT'].str.contains('固定路径')) |
                        (problem_data['PROBLEM_POINT'].str.contains('道心')) |
                        (problem_data['PROBLEM_POINT'].str.contains('枕木头'))]
    # 防护人员、警示标志
    fhry = problem_data[(problem_data['PROBLEM_POINT'].str.contains('防护人员')) |
                        (problem_data['PROBLEM_POINT'].str.contains('警示标志'))]
    # 三检、交接检查、漏检
    sj = problem_data[(problem_data['PROBLEM_POINT'].str.contains('三检')) |
                      (problem_data['PROBLEM_POINT'].str.contains('交接检查')) |
                      (problem_data['PROBLEM_POINT'].str.contains('漏检'))]
    # 货场管理不到位
    hcgl = problem_data[(problem_data['CHECK_ITEM_NAME'].str.contains('货场管理')) |
                        (problem_data['CHECK_ITEM_NAME'].str.contains('货场管理制度执行'))]
    # 货装设备管理
    hzsb = problem_data[problem_data['CHECK_ITEM_NAME'].str.contains('货装设备')]
    # 货场消防
    hcxf = problem_data[problem_data['CHECK_ITEM_NAME'].str.contains('货装消防')]
    result = []
    for i in [tgxh, phone, wzsx, fhry, sj, hcgl, hzsb, hcxf]:
        res = calc_issue_problem(i)
        result.append(res)
    return {
        'result': result
    }


def calc_issue_problem(data):
    all_point = []
    for i in data['PROBLEM_POINT'].unique().tolist():
        data1 = data[data['PROBLEM_POINT'] == i]
        dic = {
            'point': i.replace('。', ''),
            'count': len(data1),
            'desc': list(data1['DESCRIPTION'].unique())
        }
        all_point.append(dic)
    return {
        'all_point': all_point
    }