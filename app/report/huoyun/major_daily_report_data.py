import numpy as np
from datetime import datetime as dt
from app.report.analysis_report_manager import DailyAnalysisReport
from app.data.util import pd_query
from app.report.common_sql_one import *

_MAIN_PROBLEM = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4']
}

TOP10_RISK = ['劳动安全', '施工', '调车', '接发列车', '防溜', '安全管理', '防洪']


def get_data(year, month, day):
    report_date = dt(year, month, day)
    params = DailyAnalysisReport.get_daily_intervals(f'{year}-{month}-{day}')
    times = (params[0][0], params[0][1])
    first = get_first(times, month, day)
    second = get_second(times)
    third = get_third(times)
    four = get_four(times)
    return {
        "date": year * 10000 + month * 100 + day,
        "hierarchy": "MAJOR",
        'major': '货运',
        "file_name": '{0}货运系统安全分析日报.xlsx',
        "created_at": dt.now(),
        'first': first,
        'second': second,
        'third': third,
        'four': four
    }


def get_first(times, month, day):
    # 安全情况
    first_one = get_first_one(times, month, day)
    # 事故故障情况
    first_two = get_first_two(times, month, day)
    return {
        'first_one': first_one,
        'first_two': first_two
    }


def get_first_one(times, month, day):
    """
    安全生产情况
    :param times:
    :param month:
    :param day:
    :param worksheet:
    :return:
    """
    # 无责任事故及安全生产天数
    without_accident = pd_query(without_accident_sql.format('2017-10-01', times[1], 899))
    data = without_accident.dropna(subset=['RESPONSIBILITY_UNIT'])
    dic = {}
    # 计算ABCD 4类问题无责任天数
    for i in ['A', 'B', 'C', 'D']:
        accident = data[(data['CODE'].str.contains(i)) & (data['FK_RESPONSIBILITY_DIVISION_ID'] == 667)]
        if len(accident) == 0:
            dic[i] = str(dt.now().date() - dt.strptime('2017-10-01', "%Y-%m-%d").date()).split(' ')[0]
        else:
            dic[i] = str(dt.now().date() - accident.sort_values(by='OCCURRENCE_TIME', ascending=False).
                                iloc[0]['OCCURRENCE_TIME'].date()).split(' ')[0]
    content = '截至{0}月{1}日：全段实现无责任一般A类事故{2}天;无责任一般B类事故{3}天;无责任一般C类事故{4}天;' \
              '无责任一般D类事故{5}天。全段实现安全生产{6}天。'.format(month, day, dic['A'], dic['B'], dic['C'], dic['D'], dic['A'])
    return content


def get_first_two(times, month, day):
    infos = []
    without_accident = pd_query(without_accident_sql.format(times[0], times[1], 899))
    if len(without_accident) == 0:
        content = "{0}月{1}日，发生事故0件、故障0件。" \
                  "\n1.行车安全：未发生事故。" \
                  "\n2.劳动安全：未发生事故。" .format(month, day)
        info = {
            'acc': '无',
            'fault': '无'
        }
        infos.append(info)
    else:
        data = without_accident
        acc = len(data[data['MAIN_TYPE'] == 1])
        fault = len(data[data['MAIN_TYPE'] == 2])
        # 行车、劳动、路外
        safety_list = []
        for i in [1, 2]:
            new_data = data[data['DETAIL_TYPE'] == i]
            safety_list.append(len(new_data[new_data['MAIN_TYPE'] == 1]))
        content = "{0}月{1}日，发生事故{2}件、故障{3}件。" \
                  "\n1.行车安全：发生{4}件事故。" \
                  "\n2.劳动安全：发生{5}件事故。" .\
            format(month, day, acc, fault, safety_list[0], safety_list[1])
    return content


def get_second(times):
    """
    监督检查基本情况
    :param times:
    :param worksheet:
    :return:
    """
    # 检查信息
    check_info = pd_query(check_info_sql.format(times[0], times[1], 899)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 检查问题
    check_problem = pd_query(check_problem_sql.format(times[0], times[1], 899)).drop_duplicates(subset=['PK_ID']).reset_index()
    # 分析中心日报
    center_daily = pd_query(analysis_center_daily.format(times[0], times[1]))

    # 全局总体检查情况
    second_one = get_second_one(check_info, check_problem)
    # 客运处检查情况
    second_two = get_second_two(check_info, check_problem)
    # 分析中心检查
    second_three = get_second_three(center_daily)
    return {
        'second_one': second_one,
        'second_two': second_two,
        'second_three': second_three
    }


def get_second_one(check_info, check_problem):
    dic = {}
    # 检查基本情况
    xc = len(check_info[check_info['CHECK_WAY'].isin([1, 2])])
    find_problem = int(check_info['PROBLEM_NUMBER'].sum())
    main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    work_point = []
    # 问题三项
    for i in ['作业', '设备', '管理']:
        data = check_problem[check_problem['LEVEL'].isin(_MAIN_PROBLEM[i])]
        work_point.append(len(data))
    dic['one'] = '集团公司下现场检查{0}人次，发现问题{1}个。性质严重问题{2}个，其中作业项{3}个、' \
                 '设备设施项{4}个、管理项{5}个。'. \
        format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2])
    # 典型突出问题
    content = []
    for i, j in check_problem[check_problem['LEVEL'].isin(['A', 'B', 'E1', 'F1'])].iterrows():
        content.append(j['DESCRIPTION'])
    dic['two'] = '\n'.join(list(set(content))) if len(content) != 0 else '无'
    return dic


def get_second_two(check_info, check_problem):
    """
    客运系统检查基本情况
    :param check_info:
    :param check_problem:
    :return:
    """
    content = {}
    check_info = check_info[check_info['ST'] == '19B8C3534E545665E0539106C00A58FD']
    check_problem = check_problem[check_problem['TYPE3'] == '19B8C3534E545665E0539106C00A58FD']
    # 检查基本情况
    xc = len(check_info[check_info['CHECK_WAY'].isin([1, 2])])
    find_problem = int(check_info['PROBLEM_NUMBER'].sum())
    main_problem = len(check_problem[check_problem['LEVEL'].isin(['A', 'B', 'F1', 'F2', 'E1', 'E2'])])
    work_point = []
    # 问题三项
    for j in ['作业', '设备', '管理']:
        data = check_problem[check_problem['LEVEL'].isin(_MAIN_PROBLEM[j])]
        work_point.append(len(data))
    # 音视频调阅
    dy = len(check_info[check_info['CHECK_WAY'].isin([3, 4])])
    dy_pro = int(check_info[check_info['CHECK_WAY'].isin([3, 4])]['PROBLEM_NUMBER'].sum())
    content['one'] = '下现场检查{0}人次，发现问题{1}个；音视频调阅{6}人次，发现问题{7}个。性质严重问题{2}个，其中作业项{3}个、设备设施项{4}个、'\
                    '管理项{5}个。'.\
        format(xc, find_problem, main_problem, work_point[0], work_point[1], work_point[2], dy, dy_pro)
    # 重点风险查处情况
    checked = []
    uncheck = []
    rk_data = check_info.dropna(subset=['RISK_NAME'])
    for rk in TOP10_RISK:
        if rk_data[rk_data['RISK_NAME'].str.contains(rk)].shape[0] > 0:
            checked.append(rk)
        else:
            uncheck.append(rk)
    checked_con = '，'.join(checked)
    uncheck_con = '，'.join(uncheck)
    line31 = '今日检查风险{}大类，分别是：{}\n'.format(len(checked), checked_con)
    line32 = '今日未检查风险{}大类，分别是：{}'.format(len(uncheck), uncheck_con)
    content['two'] = line31 + line32
    return content


def get_second_three(center_daily):
    """
    分析中心检查情况
    :param center_daily:
    :return:
    """
    content = {}
    # 音视频调阅时长
    cost_time = float('%.4f' % center_daily[center_daily['FLAG'] == 1]['DAILY_COMPLETE'].sum())
    # 履职评价条数
    eva_count1 = int(center_daily[center_daily['FLAG'].isin([3])]['DAILY_COMPLETE'].sum())
    eva_count2 = int(center_daily[center_daily['FLAG'].isin([4])]['DAILY_COMPLETE'].sum())
    # 履职复查条数
    re_count1 = int(center_daily[center_daily['FLAG'].isin([5])]['DAILY_COMPLETE'].sum())
    re_count2 = int(center_daily[center_daily['FLAG'].isin([6])]['DAILY_COMPLETE'].sum())
    # 阶段评价
    jd = int(center_daily[center_daily['FLAG'].isin([7])]['DAILY_COMPLETE'].sum())
    # 音视频复查发现问题
    re_pro = int(center_daily[center_daily['FLAG'].isin([2])]['DAILY_PROBLEM_NUMBER'].sum())
    # 履职评价发现问题
    eva_pro = int(center_daily[center_daily['FLAG'].isin([3, 4])]['DAILY_PROBLEM_NUMBER'].sum())
    content['one'] = '音视频调阅（复查）及干部履职评价（复查）方面：今日完成视频调阅复查{0}小时，干部履职评价{1}条，' \
              '干部履职评价{2}人次，复查履职评价{3}条，干部履职复查{4}人次，阶段评价{5}人次。 问题查处情况：音视频调阅' \
              '（复查）发现问题{6}个，干部履职评价（复查）发现{7}个问题。'.format(cost_time, eva_count1, eva_count2,
                                                        re_count1, re_count2, jd, re_pro, eva_pro)
    # 典型突出问题
    content['two'] = '\n'.join(list(center_daily['MONITOR_PROBLEM_DESCRIPTION'].unique())) \
        if len(center_daily['MONITOR_PROBLEM_DESCRIPTION']) != 0 else '无'
    # 评价分析工作存在的主要问题
    content['three'] = '\n'.join(list(center_daily['CENTER_PROBLEM'].unique())) if len(center_daily['CENTER_PROBLEM']) \
                                                                                   != 0 else '无'
    return content


def get_third(times):
    """
    重点安全信息追踪
    :param times:
    :return:
    """
    safety_info = pd_query(main_info_sql.format(times[0], times[1], 899))
    # 总公司追踪
    info = safety_info[safety_info['HIERARCHY'] == 1]
    infos = info['CONTENT'].head(3).tolist()
    if len(infos) == 0:
        infos = ['无', '无', '无']
    # 路局追踪
    road_info = safety_info[safety_info['HIERARCHY'] == 2]
    road_infos = road_info['CONTENT'].head(3).tolist()
    if len(road_infos) == 0:
        road_infos = ['无', '无', '无']
    return {
        'infos': infos,
        'road_infos': road_infos,
    }


def get_four(times):
    """
    风险预警
    :param times:
    :return:
    """
    warning_data = pd_query(warning_notification_sql.format(times[0], times[1], 899))
    # 路局
    road_data = warning_data[warning_data['HIERARCHY'] == 1].reset_index().head(3)
    company_warn_list = []
    if len(road_data) == 0:
        company_warn_list = ['无']
    else:
        for index in range(0, len(road_data)):
            company_warn_list.append("""{0}.{1}""".format(
                index + 1,
                road_data['CONTENT'][index])
            )
    # 专业
    safety_data = warning_data[warning_data['HIERARCHY'] == 2].reset_index().head(3)
    safety_warn_list = []
    if len(safety_data) == 0:
        safety_warn_list = ['无']
    else:
        for index in range(0, len(safety_data)):
            safety_warn_list.append("""{0}.{1}""".format(
                index + 1,
                safety_data['CONTENT'][index])
            )
    return {
        'safety_warn_list': '\n'.join(safety_warn_list),
        'company_warn_list': '\n'.join(company_warn_list)
    }
