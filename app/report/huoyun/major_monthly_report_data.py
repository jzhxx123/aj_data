from app.data.util import pd_query
import pandas as pd
from app import mongo

from app.report.huoyun.major_monthly_report_sql import *
from docxtpl import InlineImage
from docx.shared import Mm
from dateutil.relativedelta import relativedelta
import datetime

MAIN_PROBLEM_TYPE = {
    '作业': ['A', 'B', 'C', 'D'],
    '设备': ['E1', 'E2', 'E3', 'E4'],
    '管理': ['F1', 'F2', 'F3', 'F4'],
    '性质严重': ['A', 'B', 'F1', 'F2', 'E1', 'E2']
}


def calculate_month_and_ring_ratio(counts):
    """
    计算环比
    :param counts:
    :return:
    """
    now_count, month_count = counts
    month_ratio = now_count - month_count
    if month_count == 0:
        month_percent = month_ratio * 100
    else:
        month_percent = round(month_ratio / month_count, 2) * 100
    result = {
        'month_diff': month_ratio,
        'month_percent': month_percent
    }
    return result


def ded_zero(num1, num2):
    if num2 == 0:
        return num1
    else:
        return num1 / num2


def get_data(year, month):
    global end_date
    end_date = datetime.date(year, month, 24)
    last_month_end = end_date - relativedelta(months=1)
    start_date = end_date - relativedelta(months=1, days=-1)
    last_month_start = start_date - relativedelta(months=1)
    year, month = end_date.year, end_date.month
    start_month, start_day = start_date.month, start_date.day
    end_month, end_day = end_date.month, end_date.day
    start_date = str(start_date)[:10]
    end_date = str(end_date)[:10]
    last_month_end = str(last_month_end)[:10]
    last_month_start = str(last_month_start)[:10]
    # 履职评价
    evaluate_data = pd_query(CHECK_EVALUATE_SQL.format(start_date, end_date))
    # 问题信息
    problem_data = pd_query(check_problem_sql.format(start_date, end_date))
    last_pro_data = pd_query(check_problem_sql.format(last_month_start, last_month_end))
    # 监控设备调阅时长
    dy_time_data = pd_query(CHECK_MV_TIME_OR_PROBLEM_SQL.format(start_date, end_date))
    # 分析组工作量查询
    fxz_eva_worker = pd_query(FXZ_EVALUATE_WORKER_SQL.format(start_date, end_date))
    # 分析组检查问题
    fxz_problem_data = pd_query(FXZ_CHECK_PROBLEM_SQL.format(start_date, end_date))
    # 量化完成情况查询
    qt_data = pd_query(qt_check_sql.format(year, month))

    first = get_first(problem_data, last_pro_data)
    second = get_second(dy_time_data, fxz_eva_worker, evaluate_data, fxz_problem_data, qt_data)
    third = get_third(fxz_problem_data, evaluate_data)

    file_name = f'{start_date}至{end_date}电务系统安全管理月分析.docx'
    result = {
        "year": year,
        "month": month,
        "start_month": start_month,
        "end_month": end_month,
        "start_day": start_day,
        "end_day": end_day,
        "major": '货运',
        "hierarchy": "MAJOR",
        "file_name": file_name,
        "created_at": datetime.datetime.now(),
        "year_month": year * 100 + month,
        "first": first,
        'second': second,
        'third': third
    }
    return result


def get_first(problem_data, last_pro_data):
    # 问题数
    count = len(problem_data)
    last_count = len(last_pro_data)
    ratio = calculate_month_and_ring_ratio([count, last_count])
    # 问题分类
    type_count = []
    type_ratio = []
    for i in ['作业', '设备', '管理', '性质严重']:
        data = problem_data[problem_data['LEVEL'].isin(MAIN_PROBLEM_TYPE[i])]
        last_data = last_pro_data[last_pro_data['LEVEL'].isin(MAIN_PROBLEM_TYPE[i])]
        type_count.append(len(data))
        type_ratio.append(calculate_month_and_ring_ratio([len(data), len(last_data)]))
    return {
        'count': count,
        'ratio': ratio,
        'type_count': type_count,
        'type_ratio': type_ratio
    }


def get_second(dy_time_data, fxz_eva_worker, evaluate_data, fxz_problem_data, qt_data):
    fxz_problem_data = fxz_problem_data.dropna(subset=['LEVEL'])
    # 调阅时长
    cost_time = float('%.4f' % dy_time_data['COST_TIME'].sum())
    # 复查调阅时长
    re_time = float('%.4f' % qt_data['MONITOR_REVIEW_TIME'].sum())
    # 分析组检查问题
    pro_count = len(fxz_problem_data)
    # 问题分类
    all_type = []
    for i, j in fxz_problem_data.groupby('LEVEL').count().reset_index().iterrows():
        dic = {
            'code': j['LEVEL'],
            'count': int(j['PK_ID'])
        }
        all_type.append(dic)
    # 逐条评价
    zt = fxz_eva_worker[fxz_eva_worker['EVALUATE_WAY'] == 1]
    zt_count = len(zt[zt['EVALUATE_TYPE'].isin([1, 2])])
    zt_re = len(zt[(zt['EVALUATE_TYPE'].isin([1, 2])) & (zt['IS_REVIEW'] == 1)])
    # 定期
    dq = len(fxz_eva_worker[fxz_eva_worker['EVALUATE_WAY'] == 2])
    dq_re = len(fxz_eva_worker[(fxz_eva_worker['EVALUATE_WAY'] == 2) & (fxz_eva_worker['IS_REVIEW'] == 1)])
    # 阶段
    jd = len(fxz_eva_worker[fxz_eva_worker['EVALUATE_WAY'] == 3])
    # 履职评价人次
    eva_count = len(evaluate_data)
    return {
        'cost_time': cost_time,
        're_time': re_time,
        'pro_count': pro_count,
        'all_type': all_type,
        'zt_count': zt_count,
        'zt_re': zt_re,
        'dq': dq,
        'dq_re': dq_re,
        'jd': jd,
        'eva_count': eva_count
    }


def get_third(fxz_problem_data, evaluate_data):
    # 音视频调阅发现的问题
    fxz_problem_data = fxz_problem_data.dropna(subset=['LEVEL'])
    desc = []
    for i, j in fxz_problem_data[fxz_problem_data['CHECK_WAY'] == 3].iterrows():
        desc.append(j['DESCRIPTION'])
    desc = list(set(desc))
    # 干部履职评价
    eva_content = []
    for i in ['ZD-5-3', 'ZL-1-1', 'SZ-3-3', 'KH-2-1', 'ZL-5-1', 'JL-1-1', 'ZG-5-2']:
        data = evaluate_data[evaluate_data['CODE_ADDITION'].str.contains(i)]
        eva_content.append(list(data['EVALUATE_CONTENT'].unique()))
    return {
        'desc': desc,
        'eva_content': eva_content
    }