from app.utils.common_func import ALL_MAJOR_DEPARTMENT_ID_DICT


# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and FIND_IN_SET(2,c.CHEWU_OWN)
and a.TYPE2 = '%s'""" % ALL_MAJOR_DEPARTMENT_ID_DICT['车务']


# （检查信息和问题）的数据
check_problem_sql = """SELECT p.PK_ID,p.STATUS,c.CHECK_WAY,
p.`LEVEL`,p.RISK_LEVEL,d.IDENTITY,p.PROBLEM_POINT,p.DESCRIPTION
from t_check_problem p 
LEFT JOIN t_check_info c on c.pk_id=p.fk_check_info_id
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and FIND_IN_SET(2,p.CHEWU_OWN)
and a.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""

# 分析组检查问题
FXZ_CHECK_PROBLEM_SQL = """SELECT 
c.PK_ID,c.PROBLEM_NUMBER,d.`LEVEL`,d.DESCRIPTION,c.CHECK_WAY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_check_problem d on d.fk_check_info_id = c.pk_id
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and a.TYPE5 = '80A7FF67374A754FE0534720C00A4A19'
"""


# 分析组履职评价的数据
CHECK_EVALUATE_SQL = """SELECT
	a.EVALUATE_CONTENT,
	b.SITUATION,
    b.`CODE`,
    b.CODE_ADDITION
FROM
	t_check_evaluate_info AS a
	LEFT JOIN t_check_evaluate_config AS b ON b.PK_ID = a.FK_CHECK_EVALUATE_CONFIG_ID
WHERE
	a.CREATE_TIME >= '{0}' 
	AND a.CREATE_TIME <= '{1} 23:59:59' 
	AND a.CHECK_PERSON_DEPARTMENT_ID='80A7FF67374A754FE0534720C00A4A19'
	AND a.FK_PERSON_GRADATION_RATIO_ID != 5
"""


# 履职工作量查询
EVALATE_WORKER_SQL = """SELECT
a.*,c.`NAME`,d.ALL_NAME AS CHECK_DEPT_NAME,c.TYPE AS CHECK_DEPT_TYPE
FROM
t_check_evaluate_check_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
LEFT JOIN t_department c on c.DEPARTMENT_ID=b.TYPE3
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.FK_RESPONSIBE_DEPARTMENT_ID
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'
AND a.CHECK_TYPE = 1
AND c.BELONG_PROFESSION_ID=899"""


# 分析组监控调阅时长
CHECK_MV_TIME_OR_PROBLEM_SQL = """SELECT
        a.COST_TIME
    FROM
        t_check_info_and_media AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
            LEFT JOIN
		t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
		    LEFT JOIN 
		t_department e on e.DEPARTMENT_ID=d.TYPE5
    WHERE
        b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
                AND d.TYPE5= '80A7FF67374A754FE0534720C00A4A19'"""


# 量化完成情况查询
qt_check_sql = """SELECT a.MONITOR_REVIEW_TIME FROM t_analysis_center_quantization_quota a
LEFT JOIN t_department b on b.DEPARTMENT_ID=a.FK_DEPARTMENT_ID
WHERE a.`YEAR`={0} AND a.`MONTH`={1}
AND b.TYPE5='80A7FF67374A754FE0534720C00A4A19'"""


# 分析组履职工作量
FXZ_EVALUATE_WORKER_SQL = """SELECT
a.*
FROM
t_check_evaluate_check_person a
LEFT JOIN t_department b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
WHERE
a.CREATE_TIME >= '{0}'
AND a.CREATE_TIME <= '{1} 23:59:59'
AND b.TYPE5 = '80A7FF67374A754FE0534720C00A4A19'
"""
