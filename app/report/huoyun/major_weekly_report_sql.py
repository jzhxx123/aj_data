from app.utils.common_func import ALL_MAJOR_DEPARTMENT_ID_DICT


# 检查信息
check_info_sql = """SELECT c.PK_ID,c.CHECK_WAY,c.PROBLEM_NUMBER,d.NAME,c.RISK_NAME,e.IDENTITY
from t_check_info c
LEFT JOIN t_check_info_and_person b on b.FK_CHECK_INFO_ID= c.PK_ID
LEFT JOIN t_person e on e.id_card=b.ID_card
LEFT JOIN t_department a on b.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
LEFT JOIN t_department d on d.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and FIND_IN_SET(2,c.CHEWU_OWN)
and a.TYPE2 = '%s'""" % ALL_MAJOR_DEPARTMENT_ID_DICT['车务']


# （检查信息和问题）的数据
check_problem_sql = """SELECT p.PK_ID,p.STATUS,c.CHECK_WAY,p.CHECK_ITEM_NAME,
p.`LEVEL`,p.RISK_LEVEL,d.IDENTITY,p.PROBLEM_POINT,p.DESCRIPTION
from t_check_problem p 
LEFT JOIN t_check_info c on c.pk_id=p.fk_check_info_id
LEFT JOIN t_person d on d.id_card=p.CHECK_PERSON_ID_CARD
LEFT JOIN t_department a on d.FK_DEPARTMENT_ID = a.DEPARTMENT_ID
left join t_department b on b.DEPARTMENT_ID=a.TYPE3
where c.SUBMIT_TIME BETWEEN '{0}' and '{1} 23:59:59'
and FIND_IN_SET(2,p.CHEWU_OWN)
and a.TYPE2 = '1ACE7D1C80B24456E0539106C00A2E70KSC'"""


# 监控调阅时长
CHECK_MV_TIME_OR_PROBLEM_SQL = """SELECT
        a.COST_TIME,b.CHECK_WAY
    FROM
        t_check_info_and_media AS a
            INNER JOIN
        t_check_info AS b ON a.FK_CHECK_INFO_ID = b.PK_ID
            INNER JOIN
        t_check_info_and_person AS c ON b.PK_ID = c.FK_CHECK_INFO_ID
            LEFT JOIN
        t_department d on d.DEPARTMENT_ID = c.FK_DEPARTMENT_ID
            LEFT JOIN 
        t_department e on e.DEPARTMENT_ID=d.TYPE3
    WHERE
        b.SUBMIT_TIME BETWEEN '{0}' AND '{1} 23:59:59'
        AND d.TYPE2= '1ACE7D1C80B24456E0539106C00A2E70KSC'"""
