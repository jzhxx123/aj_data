import re
from datetime import datetime as dt
from dateutil.relativedelta import relativedelta

import pandas as pd
import numpy as np

from app import mongo
from app.data.util import pd_query
from app.report.util import get_report_list

majors = ['车务', '电务', '工务', '机务', '车辆', '供电']


def get_evaluate_rank_list(start_time, end_time):
    title = '{year}年{month}月份个人履职报告统计分析表'
    TYPE = 'evaluate_rank'
    reports = get_report_list(start_time, end_time, title, TYPE)
    return reports


def _get_evaluate_rank_data(key, dup_subset=['ID_CARD']):
    """
    根据指定的检索key，检索记录。并根据dup_subset自定的列，进行数据除重操作
    :param key: {dict} 检索的key
    :param dup_subset: {list} 默认是['ID_CARD']
    :return:
    """
    prefixs = ['daily_', 'monthly_', 'history_']
    for prefix in prefixs:
        coll_name = f'{prefix}detail_evaluate_index'
        doc = list(mongo.db[coll_name].find(key, {"_id": 0}))
        if doc:
            break
    data = pd.DataFrame(doc)
    if data.empty:
        return pd.DataFrame()
    else:
        data = data.drop_duplicates(dup_subset)
        short_name_data = pd_query('''SELECT
                a.ID_CARD,
                c.SHORT_NAME
            FROM
                t_person AS a
                LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_DEPARTMENT_ID
                LEFT JOIN t_department as c on c.DEPARTMENT_ID = b.TYPE3
            WHERE
                c.SHORT_NAME IS NOT NULL
                AND c.SHORT_NAME != ''
        ''')
        data = pd.merge(data, short_name_data, how='left', on='ID_CARD')
        data = data[~data['SHORT_NAME'].isnull()]
    return data


def _get_rank_grade(data):
    data = data.sort_values(by='score', ascending=False).reset_index()
    data['all_rank'] = data.index + 1
    total = data.shape[0]
    if total % 3 == 2:
        first = total // 3 + 1
    else:
        first = total // 3
    second = total - total // 3 + 1
    del data['index']
    return data, first, second


def _get_evaluate_percent(data, first, second):
    if data.empty:
        return [0, 0, 0]
    else:
        total = data.shape[0]
        first_count = data[data['all_rank'] <= first].shape[0]
        third_count = data[data['all_rank'] >= second].shape[0]
        second_count = total - first_count - third_count
        first_per = round(first_count / total * 100, 1)
        second_per = round(second_count / total * 100, 1)
        third_per = round(third_count / total * 100, 1)
        return [first_per, second_per, third_per]


department_sql = """SELECT
        a.`NAME` AS station,
        b.`NAME` AS major ,a.SHORT_NAME
    FROM
        t_department AS a
        LEFT JOIN t_department AS b ON b.DEPARTMENT_ID = a.FK_PARENT_ID
    WHERE
        b.`NAME` IN ( '车务', '电务', '工务', '机务', '车辆', '供电' )
        AND a.SHORT_NAME IS NOT NULL
        AND a.SHORT_NAME != ''
        AND a.`NAME` NOT LIKE '%%处%%'
        AND a.NAME != '调度所'
    """


def get_first_major_carde_statistics(role_info, date, **args):
    """干部履职总体分析"""
    major = args['department'].split('-')[0]
    if major == "":
        if role_info['role'] == 0:
            major_filter = majors
            if role_info['major'] not in majors:
                return {"data": [], "filter": majors}
            else:
                major = role_info['major']
        else:
            major_filter = []
            if role_info['major'] in majors:
                major = role_info['major']
            else:
                return {"data": [], "filter": []}
    else:
        if role_info['role'] == 0:
            major_filter = majors
        else:
            major_filter = []
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month}
    data = _get_evaluate_rank_data(key)
    leader_data = data[(data.BUSINESS_CLASSIFY.str.contains('领导'))
                       & (data['MAJOR'] == major)].copy()
    leader_data, leader_first, leader_second = _get_rank_grade(leader_data)
    one_data = data[(data['GRADE'] == 1) & (data['MAJOR'] == major)].copy()
    one_data, one_first, one_second = _get_rank_grade(one_data)
    all_data = data[data['MAJOR'] == major].copy()
    all_data, all_first, all_second = _get_rank_grade(all_data)
    firsts = [leader_first, one_first, all_first]
    seconds = [leader_second, one_second, all_second]
    stations = list(set(all_data['STATION']))
    result = []
    for index, station in enumerate(stations):
        sta_leader = leader_data[leader_data['STATION'] == station]
        sta_one = one_data[one_data['STATION'] == station]
        sta_all = all_data[all_data['STATION'] == station]
        names = ['领导班子', '1档干部', '全部干部']
        for idx, sta in enumerate([sta_leader, sta_one, sta_all]):
            first = firsts[idx]
            second = seconds[idx]
            per = _get_evaluate_percent(sta, first, second)
            score_value = _get_score_statistic(sta, names[idx])
            result.append({
                "index": index + 1,
                "name": station,
                "class": names[idx],
                "number": sta.shape[0],
                "pre_percent": f'{per[0]}%',
                "percent": f'{per[1]}%',
                "aft_percent": f'{per[2]}%',
                "score_value": score_value
            })
    rst = {"data": result, "filter": major_filter}
    return rst


def _get_department_structure(role_info):
    role = role_info['role']
    station = role_info['station']
    majors = ['车务', '电务', '工务', '机务', '车辆', '供电']
    depart_data = pd_query(department_sql)
    # if role != 2 and role_info['major'] != '':
    if role != 2 and role_info['major'] != '' and role_info['major'] != '局领导':
        majors = [role_info['major']]
    result = {}
    if station == '':
        for major in majors:
            result[major] = []
            major_data = depart_data[depart_data['major'] == major]
            result[major] = [
                major_data.at[index, 'station'] for index in major_data.index
            ]
    return result


def _get_second_carde_rank(data, station):
    data = data[data['STATION'] == station]
    result = []
    zc_data = data[data['LEVEL'] == '正处级']
    zc_data, zc_first, zc_second = _get_rank_grade(zc_data)
    zc_per = _get_evaluate_percent(zc_data, zc_first, zc_second)
    result.append({
        "index": 1,
        "station": station,
        "level": '正处级',
        "number": zc_data.shape[0],
        "pre_percent": f'{zc_per[0]}%',
        "percent": f'{zc_per[1]}%',
        "aft_percent": f'{zc_per[2]}%'
    })
    levels = ['副处级', '正科级', '副科级', '一般管理']
    idx = 2
    for level in levels:
        for i in range(1, 4):
            rst_major_data = data[(data['LEVEL'] == level)
                                  & (data['GRADE'] == i)].copy()
            rst_major_data, rst_first, rst_second = _get_rank_grade(
                rst_major_data)
            if rst_major_data.empty is False:
                name = f'{level}{i}档'
                rst_per = _get_evaluate_percent(
                    rst_major_data, rst_first, rst_second)
                result.append({
                    "index": idx,
                    "station": station,
                    "level": name,
                    "number": rst_major_data.shape[0],
                    "pre_percent": f'{rst_per[0]}%',
                    "percent": f'{rst_per[1]}%',
                    "aft_percent": f'{rst_per[2]}%'
                })
                idx += 1
        grade = 0
        rst_major_data_0 = data[(data['LEVEL'] == level)
                                & (data['GRADE'] == grade)].copy()
        rst_major_data_0, rst_first_0, rst_second_0 = _get_rank_grade(
            rst_major_data_0)
        if rst_major_data_0.empty is False:
            name = f'{level}?档'
            rst_name = [{"index": idx * 10 + index + 1, "level": rst_major_data_0.at[index, 'NAME']}
                        for index in rst_major_data_0.index]
            rst_per = _get_evaluate_percent(
                rst_major_data_0, rst_first_0, rst_second_0)
            result.append({
                "index": idx,
                "station": station,
                "level": name,
                "number": rst_major_data_0.shape[0],
                "pre_percent": f'{rst_per[0]}%',
                "percent": f'{rst_per[1]}%',
                "aft_percent": f'{rst_per[2]}%',
                "children": rst_name
            })
            idx += 1
    return result


def get_second_station_evaluate_rank(role_info, date, **args):
    """各级干部履职分析"""
    department = args['department']
    depart_structure = _get_department_structure(role_info)
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month}
    data = _get_evaluate_rank_data(key)
    role = role_info['role']
    if department == '':  # 未传入部门
        if role == 0:  # 局和处领导
            result = []
        elif role == 1:  # 站段领导
            station = role_info['station']
            result = _get_second_carde_rank(data, station)
        else:  # 普通员工
            if role_info['major'] in majors:
                station = role_info['station']
                result = _get_second_carde_rank(data, station)
            else:
                result = []
    else:
        station = department.split('-')[1]
        result = _get_second_carde_rank(data, station)
    rst = {'data': result, 'filter': depart_structure}
    return rst


def _get_third_carde_detail(data, month_data, year_data, major, station,
                            level):
    if level != '':
        if '?' in level:
            grade = 0
            level = level.split('?')[0]
        else:
            grade = int(re.findall('\\d', level)[0])
            level = level.split(str(grade))[0]
        data = data[(data['LEVEL'] == level) & (data['GRADE'] == grade)]
        month_data = month_data[(month_data['LEVEL'] == level)
                                & (month_data['GRADE'] == grade)]
        year_data = year_data[(year_data['LEVEL'] == level)
                              & (year_data['GRADE'] == grade)]
    level_filter = [
        "正处级1档", "正处级2档", "正处级3档", "正处级?档", "副处级1档", "副处级2档", "副处级3档", "副处级?档",
        "正科级1档", "正科级2档", "正科级3档", "正科级?档", "副科级1档", "副科级2档", "副科级3档", "副科级?档",
        "一般管理1档", "一般管理2档", "一般管理3档", "一般管理?档"
    ]
    major_data = data[data['MAJOR'] == major].copy()
    if major_data.empty:
        return [], level_filter
    major_data = major_data.drop_duplicates('ID_CARD')
    major_data['major_rank'] = major_data.groupby(['CLASS'])['score'].rank(
        ascending=0, method='min')
    major_count = major_data.groupby('CLASS').size().reset_index().rename(
        columns={0: 'class_count'})
    major_data = pd.merge(major_data, major_count, how='left', on='CLASS')
    sta_data = major_data[major_data['STATION'] == station].copy()
    sta_data['sta_rank'] = sta_data['score'].rank(ascending=0, method='min')
    # 计算上月排名
    month_data = month_data[(month_data['MAJOR'] == major)
                            & (month_data['STATION'] == station)]
    month_data = month_data.drop_duplicates('ID_CARD')
    month_data['last_rank'] = month_data['score'].rank(
        ascending=0, method='min')
    month_data = month_data[['ID_CARD', 'last_rank']]
    sta_data = pd.merge(sta_data, month_data, how='left', on='ID_CARD')
    # 计算全年得分排名
    year_data = year_data[(year_data['MAJOR'] == major)]
    # Fix - 20191012 - 年度数据中人员可能有多个CLASS(职位)，需要以最新职位统计，否则一个人在group by后存在两天记录
    # 解决方法：1. 时间最后的职位CLASS， 2. 用merge改变原有数据CLASS字段
    year_class_data = year_data[['ID_CARD', 'MON', 'CLASS']]
    year_class_data = year_class_data.sort_values(by='MON', ascending=False)
    year_class_data.drop_duplicates('ID_CARD', inplace=True)
    year_class_data.drop(['MON'], axis=1, inplace=True)
    year_data.drop(['CLASS'], axis=1, inplace=True)
    year_data = pd.merge(year_data, year_class_data, how='left', on='ID_CARD')
    # END OF Fix - 20191012
    year_data = year_data.drop_duplicates(['ID_CARD', 'MON'])
    year_data = year_data.groupby(['ID_CARD', 'CLASS']).sum().reset_index()
    year_data['year_rank'] = year_data.groupby('CLASS')['score'].rank(ascending=0, method='min')
    year_data = year_data.rename(columns={'score': 'year_score'})
    year_data = year_data[['ID_CARD', 'year_score', 'year_rank']]
    sta_data = pd.merge(sta_data, year_data, how='left', on='ID_CARD')
    sta_data['rank_diff'] = sta_data['sta_rank'] - sta_data['last_rank']
    sta_data = sta_data.reset_index()
    sta_data = sta_data.fillna(0)
    total = sta_data.shape[0]
    result = []
    for index in sta_data.index:
        value = sta_data.loc[index].copy()
        if value['GRADE'] == 0:
            name = f'''{value['LEVEL']}?档'''
        else:
            name = f'''{value['LEVEL']}{int(value['GRADE'])}档'''
        if np.isnan(value['year_rank']):
            year_rank = '?'
        else:
            year_rank = int(value['year_rank'])
        result.append({
            "index": index + 1,
            "level": name,
            "name": value['NAME'],
            "department": value['DEPARTMENT'],
            "check_score": round(value['check_score'], 1),
            "problem_score": round(value['problem_score'], 1),
            "evaluate_score": round(value['evaluate_score'], 1),
            "score": round(value['score'], 1),
            "major_rank": (f'''{int(value['major_rank'])}/{int(value['class_count'])}'''),
            "station_rank": str(int(value['sta_rank'])) + '/' + str(total),
            "last_rank": value['last_rank'],
            "rank_diff": value['rank_diff'],
            "year_score": round(value['year_score'], 1),
            "year_rank": (f'''{year_rank}/{int(value['class_count'])}'''),
        })
    return result, level_filter


def get_third_station_evaluate_rank_detail(role_info, date, **args):
    """段内干部履职明细"""
    department = args['department']
    level = args['level']
    role = role_info['role']
    if role == 2:
        return {}
    depart_structure = _get_department_structure(role_info)
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month}
    data = _get_evaluate_rank_data(key)
    last_date = date - relativedelta(months=1)
    last_key = {'MON': last_date.year * 100 + last_date.month}
    last_data = _get_evaluate_rank_data(last_key)
    # FIX 20191012 - 修正年度累计分数为首月的分数问题
    year_key = {'MON': {'$gt': date.year * 100, '$lt': (date.year + 1) * 100}}
    year_data = _get_evaluate_rank_data(year_key, ['ID_CARD', 'MON'])
    # FIX 20191012  END
    if role == 1:
        major = role_info['major']
        station = role_info['station']
        result, level_filter = _get_third_carde_detail(
            data, last_data, year_data, major, station, level)
    elif role == 0 and department != '':
        major = department.split('-')[0]
        station = department.split('-')[1]
        result, level_filter = _get_third_carde_detail(
            data, last_data, year_data, major, station, level)
    elif role == 0 and department == '':
        result = []
        level_filter = []
    rst = {
        'data': result,
        'depart_filter': depart_structure,
        'level_filter': level_filter
    }
    return rst


def _get_fourth_carde_rank(data, year_data, job, page):
    if job != '':
        data = data[data['JOB'] == job]
        data = data.drop_duplicates('ID_CARD')
        year_data = year_data[year_data['JOB'] == job]
    if data.empty:
        return []
    total = data.shape[0]
    data['jobRank'] = data.groupby('JOB')['score'].rank(
        ascending=0, method='min')
    job_count = data.groupby('JOB').size().reset_index().rename(
        columns={0: 'jobCount'})
    data = pd.merge(data, job_count, how='left', on='JOB')
    # 计算全年得分排名
    year_data = pd.merge(
        data[['ID_CARD']], year_data, how='left', on='ID_CARD')
    # Fix - 20191012 - 年度数据中人员可能有多个JOB(职位)，需要以最新职位统计，否则一个人在group by后存在两天记录
    # 解决方法：1. 时间最后的职位JOB， 2. 用merge改变原有数据JOB字段
    year_job_data = year_data[['ID_CARD', 'MON', 'JOB']]
    year_job_data = year_job_data.sort_values(by='MON', ascending=False)
    year_job_data.drop_duplicates('ID_CARD', inplace=True)
    year_job_data.drop(['MON'], axis=1, inplace=True)
    year_data.drop(['JOB'], axis=1, inplace=True)
    year_data = pd.merge(year_data, year_job_data, how='left', on='ID_CARD')
    # END OF Fix - 20191012
    year_data = year_data.groupby(['ID_CARD', 'JOB']).sum().reset_index()
    year_data['year_rank'] = year_data.groupby('JOB')['score'].rank(
        ascending=0, method='min')
    year_data = year_data.rename(columns={'score': 'year_score'})
    year_data = year_data[['ID_CARD', 'year_score', 'year_rank']]
    data = pd.merge(data, year_data, how='left', on='ID_CARD')
    data = data.iloc[15 * (page - 1):15 * page, :].copy()
    data = data.reset_index()
    result = []
    data = data.fillna(0)
    for index in data.index:
        value = data.loc[index]
        result.append({
            "index": (page - 1) * 15 + index + 1,
            "station":
            value['STATION'],
            "job":
            value['JOB'],
            "name":
            value['NAME'],
            "check_score":
            round(value['check_score'], 1),
            "problem_score":
            round(value['problem_score'], 1),
            "evaluate_score":
            round(value['evaluate_score'], 1),
            "score":
            round(value['score'], 1),
            "month_rank": (f'''{int(value['jobRank'])}/'''
                           f'''{int(value['jobCount'])}'''),
            "year_score":
            round(value['year_score'], 1),
            "year_rank": (f'''{int(value['year_rank'])}/'''
                          f'''{int(value['jobCount'])}'''),
        })
    return result, total


def get_fourth_major_carde_rank(role_info, date, **args):
    """同类干部履职分析"""
    depart_strncture = _get_department_structure(role_info)
    role = role_info['role']
    if role > 1:
        return {}
    if args['department']:
        station = args['department'].split('-')[1]
    else:
        station = ''
    job = args['level']
    page = int(args.get('page', 1))
    if station == '':
        if role_info['station']:
            station = role_info['station']
        else:
            return {"data": [], "depart_filter": depart_strncture, "level_filter": []}
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month, 'STATION': station}
    data = _get_evaluate_rank_data(key)
    # FIX 20191012 - 修正年度累计分数为首月的分数问题
    year_key = {'MON': {'$gt': date.year * 100, '$lt': (date.year + 1) * 100}}
    year_data = _get_evaluate_rank_data(year_key, ['ID_CARD', 'MON'])
    # FIX 20191012 END
    major_data = data[(data['JOB'] != 'a') & (data['JOB'] != '')]
    # major_total = major_data.shape[0]
    # major_data = major_data
    job_filter = list(set(major_data['JOB']))
    result, total = _get_fourth_carde_rank(major_data, year_data, job,
                                           page)
    rst = {
        "data": result,
        "depart_filter": depart_strncture,
        "level_filter": job_filter,
        'total': total
    }
    return rst


def _get_health_index(key):
    prefixs = ['monthly_', 'history_']
    for prefix in prefixs:
        coll_name = f'{prefix}health_index'
        doc = list(mongo.db[coll_name].find(key, {"_id": 0}))
        if doc:
            break
    data = pd.DataFrame(doc)
    return data


def get_fifth_leader_rank(role_info, date, **args):
    """领导班子分析"""
    role = role_info['role']
    if role > 1:
        return {}
    major = args['department'].split('-')[0]
    if major == '':
        if role_info['major'] not in majors:
            return {"data": [], "depart_filter": majors}
        else:
            major_filter = [role_info['major']]
            major = role_info['major']
    else:
        major_filter = majors
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month}
    data = _get_evaluate_rank_data(key)
    # 专业履职得分
    major_data = data[(data.BUSINESS_CLASSIFY.str.contains('领导'))
                      & (data['MAJOR'] == major)]
    if major_data.empty:
        return {"data": [], "depart_filter": major_filter}
    major_data = major_data[['score', 'STATION']].copy()
    major_data['count'] = 1
    major_data = major_data.groupby('STATION').sum().reset_index()
    major_data['aver'] = major_data.apply(
        lambda x: x['score'] / x['count'], axis=1)
    # 综合指数得分
    key['DEPARTMENT_NAME'] = {"$in": list(set(major_data['STATION']))}
    health_index = _get_health_index(key)
    data = pd.merge(major_data, health_index, how='left',
                    left_on='STATION', right_on='DEPARTMENT_NAME')
    data['finally_score'] = data.apply(
        lambda x: x['aver'] * 0.4 + x['SCORE'] * 0.6, axis=1)
    data = data.sort_values(by='finally_score', ascending=False).reset_index()
    result = [{
        "index": index + 1,
        "station": data.at[index, 'STATION'],
        "total": int(data.at[index, 'count']),
        "aver": round(data.at[index, 'aver'], 1),
        "manage": round(data.at[index, 'SCORE'], 1),
        "score": round(data.at[index, 'finally_score'])
    } for index in data.index]
    rst = {"data": result, "depart_filter": major_filter}
    return rst


def get_sixth_office_rank(role_info, date, **args):
    """科室分析"""
    major = role_info['major']
    role = role_info['role']
    page = int(args['page'])

    # 局领导处理
    if major == "局领导":
        major = ''

    if major == '':  # 未传入部门
        if role == 0:  # 局领导
            if args['department'] != '':
                major = args['department']
                depart_filter = majors
            else:
                return {"data": [], "depart_filter": majors, "total": 0}
        else:  # 普通员工
            return {"data": [], "depart_filter": [], "total": 0}
    else:
        depart_filter = []
    # 获取履职得分数据
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    key = {'MON': month, 'MAJOR': major}
    data = _get_evaluate_rank_data(key)
    del data['GRADE']
    # 获取科室部门
    office_data = pd_query('''SELECT
            a.ALL_NAME,
            a.TYPE AS DEPART_TYPE,
            b.TYPE AS GRADE
        FROM
            t_department AS a
            LEFT JOIN t_department_classify_config AS b ON a.FK_DEPARTMENT_CLASSIFY_CONFIG_ID = b.PK_ID
        WHERE
            a.TYPE = 7
    ''')
    data = pd.merge(data, office_data, how='left', on='ALL_NAME')
    data = data[data['DEPART_TYPE'] == 7]
    # 获取量化人员数据
    assess_data = pd_query('''SELECT
            ID_CARD
        FROM
            t_quantization_base_quota
        WHERE
            YEAR = {}
            AND MONTH = {}'''.format(date.year, date.month))
    assess_data['assess_count'] = 1  # 计算科室量化人数
    data = pd.merge(data, assess_data, how='left', on='ID_CARD')
    # 计算量化得分
    data['assess_score'] = data.apply(
        lambda x: x['score'] if x['assess_count'] == 1 else 0, axis=1)
    data['count'] = 1
    data = data[['ALL_NAME', 'DEPARTMENT_CLASS', 'score',
                 'count', 'assess_count', 'GRADE', 'assess_score']]
    # 计算科室平均分
    data = data.groupby(['ALL_NAME', 'DEPARTMENT_CLASS',
                         'GRADE']).sum().reset_index()
    data['aver'] = data.apply(
        lambda x: x['score'] / x['count'], axis=1)  # 总平均分
    data['assess_aver'] = data.apply(
        lambda x: x['assess_score'] / x['assess_count'] if x['assess_count'] != 0 else 0, axis=1)  # 量化平均分
    del data['score'], data['assess_score']
    # 相同科室类别总数
    depart_count = data.groupby('DEPARTMENT_CLASS').size(
    ).reset_index().rename(columns={0: 'class_count'})
    data = pd.merge(data, depart_count, how='left', on='DEPARTMENT_CLASS')
    # 计算排名信息
    data['rank'] = data.groupby('DEPARTMENT_CLASS')[
        'aver'].rank(ascending=0, method='min')
    data['assess_rank'] = data.groupby('DEPARTMENT_CLASS')[
        'assess_aver'].rank(ascending=0, method='min')
    # 根据档次和得分排序
    total = data.shape[0]
    data = data.sort_values(
        by=['GRADE', 'rank']).reset_index().iloc[(page - 1) * 15: page * 15, :]
    result = [{
        "index": index + 1,
        "office": data.at[index, 'ALL_NAME'],
        "class": data.at[index, 'DEPARTMENT_CLASS'],
        "total": int(data.at[index, 'count']),
        "assess_total": int(data.at[index, 'assess_count']),
        "aver": round(data.at[index, 'aver'], 1),
        "rank": f'''{int(data.at[index, 'rank'])}/{int(data.at[index, 'class_count'])}''',
        "assess_aver": round(data.at[index, 'assess_aver'], 1),
        "assess_rank": f'''{int(data.at[index, 'assess_rank'])}/{int(data.at[index, 'class_count'])}'''
    } for index in data.index]
    return {"data": result, "depart_filter": depart_filter, "total": total}


def get_seventh_global_rank(role_info, date, **args):
    '''干部履职统计表'''
    date = dt.strptime(date, "%Y-%m-%d")
    month = int(f'{date.year}{date.month:0>2}')
    match = {"MON": month}
    department = args.get('department', '')
    if department:
        params = department.split("-")
        match['MAJOR'] = params[0]
        if len(params) > 1:
            match['STATION'] = params[1]
    data = _get_evaluate_rank_data(match)
    # 按级别统计
    level_data = data.copy()
    levels = ['正处级', '副处级', '正科级', '副科级', '一般管理']
    l_result = []
    for level in levels:
        l_data = level_data[level_data['LEVEL'] == level].copy()
        l_result.append(_get_score_statistic(l_data, level))
    l_result.append(_get_score_statistic(level_data, '合计'))
    # 按档次统计
    grade_data = data[data['GRADE'].isin([1, 2, 3])].copy()
    grades = [1, 2, 3]
    g_result = []
    for grade in grades:
        g_data = grade_data[grade_data['GRADE'] == grade].copy()
        name = f'{grade}档'
        g_result.append(_get_score_statistic(g_data, name))
    g_result.append(_get_score_statistic(grade_data, '全部'))
    depart_structure = _get_department_structure(role_info)
    return {'level': l_result, 'grade': g_result, "filter": depart_structure}


def _get_score_statistic(data, name):
    '''根据分数统计人数'''
    p_count = data.shape[0]
    rst = {'name': name, 'count': p_count}
    scores = [(90, 100), (80, 90), (60, 80), (0, 60)]
    for idx, score in enumerate(scores):
        name = f'{score[0]}-{score[1]}'
        if idx == 0:
            s_data = data[(data['score'] >= score[0]) & (data['score'] <= score[1])]
        else:
            s_data = data[(data['score'] >= score[0]) & (data['score'] < score[1])]
        rst[name] = s_data.shape[0]
    if p_count == 0:
        aver = 0
    else:
        aver = round(sum(list(data['score'])) / p_count, 1)
    rst['aver'] = aver
    return rst
