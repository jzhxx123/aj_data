#! /usr/bin/env python3
# -*- coding: utf-8 -*-
'''
    日报数据
'''

from app import mongo


def get_safety_report_list(start_time, end_time):
    reports = [{"DATE": "2018/12/25", "TITLE": "SSSS", "TYPE": "safety"}]
    return reports


def get_daily_report_list(start_time, end_time):
    start_time = int(start_time.replace('-', ''))
    end_time = int(end_time.replace('-', ''))
    documents = list(
        mongo.db.report.find({
            "DATE": {
                "$gte": start_time,
                "$lte": end_time
            }
        }, {
            "_id": 0,
            "ID": 1,
            "TITLE": 1,
            "DATE": 1,
            "TYPE": 1,
            "ID_CARD": 1
        }))
    result = []
    for doc in documents:
        _date = str(doc['DATE'])
        doc['DATE'] = '{}/{}/{}'.format(_date[:4], _date[4:6], _date[6:])
        result.append(doc)
    return result


def get_report(id):
    document = mongo.db.report.find_one({"ID": int(id)})
    if document is not None:
        return document['VALUE']
    return "资源不存在"


if __name__ == '__main__':
    pass
