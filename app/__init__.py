from flask import Flask
from flask_admin import Admin
from flask_admin.contrib import rediscli
from flask_caching import Cache
from flask_compress import Compress
from flask_cors import CORS
from flask_dotenv import DotEnv
from flask_pymongo import PyMongo
from flask_redis import Redis
from flask_sqlalchemy import SQLAlchemy as Sa

from app.utils.data_update_models import UpdateTimeController
from app.utils.mylog import create_low_query_logger
from config import config


class SQLAlchemy(Sa):
    def apply_pool_defaults(self, app, options):
        Sa.apply_pool_defaults(self, app, options)
        options['pool_pre_ping'] = True
        options['convert_unicode'] = False  # TODO: suppress warning until fsql=2.4 to delete


db = SQLAlchemy()
mongo = PyMongo()
redis = Redis()
cors = CORS()
admin = Admin()
cache = Cache()
env = DotEnv()
compress = Compress()
utctl = UpdateTimeController()


def create_app(config_name):
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_object(config[config_name])
    # instance/config.py
    app.config.from_pyfile('config.py', silent=True)  # 失败只返回False
    app.logger.info('Mode: {}'.format(config_name))
    config[config_name].init_app(app)

    handler = create_low_query_logger()
    app.logger.addHandler(handler)

    register_extensions(app)
    register_blueprint(app)
    # 挂载初始化函数列表
    add_maps(app)

    return app


def register_extensions(app):
    """ Register extensions """
    db.init_app(app)
    mongo.init_app(app)
    redis.init_app(app)
    cors.init_app(app, supports_credentials=True)
    admin.init_app(app)
    compress.init_app(app)
    cache.init_app(app, config={'CACHE_TYPE': 'redis'})
    utctl.init_app(app)

    # https://stackoverflow.com/a/19438054/4757521
    with app.app_context():
        # 测试时跳过, 防止重复注册
        if app.testing is False:
            admin.add_view(rediscli.RedisCli(redis))


def register_blueprint(app):
    from .api_1_0 import api_bp as api_1_0_blueprint
    from .auth import auth_bp as auth_blueprint
    from .sys import sys_bp as sys_blueprint
    from .new_big import new_big_bp as new_big_blueprint
    from .new_three import new_three_bp as new_three_blueprint
    from .safety_index import safety_index_bp as safety_index_blueprint
    from .big_screen_analysis import big_screen_analysis_bp as big_screen_analysis_blueprint
    from .index_warning import index_warning_bp as index_warning_blueprint
    # app.register_blueprint(api_1_0_blueprint, url_prefix='/api/v1.0')
    app.register_blueprint(api_1_0_blueprint, url_prefix='/api')
    app.register_blueprint(auth_blueprint, url_prefix='/auth')
    app.register_blueprint(sys_blueprint, url_prefix='/sys')
    app.register_blueprint(new_big_blueprint, url_prefix='/new_big')
    app.register_blueprint(new_three_blueprint, url_prefix='/new_three')
    app.register_blueprint(safety_index_blueprint, url_prefix='/safety_index')
    app.register_blueprint(big_screen_analysis_blueprint, url_prefix='/big_screen_analysis')
    app.register_blueprint(index_warning_blueprint, url_prefix='/index_warning')
    # from .admin import admin_bp as admin_blueprint
    # app.register_blueprint(admin_blueprint)


def add_maps(app):
    from app.utils.init_func_maps import UPDATE_FUNC_MAP
    app.maps = UPDATE_FUNC_MAP
