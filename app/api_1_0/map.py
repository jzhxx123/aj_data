import time
from datetime import datetime as dt
from flask import current_app

from dateutil.relativedelta import relativedelta
from flask import g, jsonify, request

from app.api_1_0 import api_bp
from app.utils.common_func import request_record, wrapper_rtn_msg, get_person_major
from app.map import railway_map
from app import mongo

# from flask_sqlalchemy import get_debug_queries


@api_bp.before_request
def before_request():
    g.major = '路局'
    #     TOKEN = request.cookies.get('TOKEN')
    #     major = get_token_major(TOKEN)
    #     if major == None:
    #         rs = {"status": 2, "error_message": "TOKEN无效"}
    #         return jsonify(rs)
    #     g.major = major
    if request.method != 'OPTIONS':
        record = {
            "REQUEST_IP":
            request.remote_addr,
            "REQUEST_PATH":
            request.path,
            "REQUEST_DATA":
            request.data.decode('utf8'),
            "REQUEST_TIME":
            time.strftime("%m/%B/%Y %H:%M:%S", time.localtime(time.time())),
            "METHOD":
            request.method,
        }
        request_record(record)


@api_bp.route('/map_new', methods=['GET'])
def map_new_data():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    param_dict = request.args
    # param_dict = request.get_json(force=True)
    token = request.cookies.get('TOKEN', None)
    if not token:
        return jsonify({"status": '', "error_message": "AUTH_ENABLED ERROR"})
    zhanduan_info = dict()
    g.major = "路局" if int(token.split("#")[-1]) == 1 else get_person_major(token.split('#')[0])
    zhanduan_info = {
        "zhanduan_id": token.split('#')[-3],
        'zhuanduan_name': token.split('#')[-2]
    }
    if (param_dict) and ('START_MONTH' in param_dict) and (
            'END_MONTH' in param_dict):
        results = railway_map.get_map_data(param_dict, g.major, zhanduan_info)
        rs = {'data': results, 'status': 0, 'error_message': ''}

    else:
        if 'TIMESTAMP' in param_dict:
            now = dt.fromtimestamp(int(param_dict['TIMESTAMP']))
            # 如果是每月25号，则加载的3个月数据（不包含本月，因为本月1号数据没有）
            if now.day > current_app.config.get('UPDATE_DAY'):
                today = now + relativedelta(months=1)
                last = now + relativedelta(months=-1)
                today = int('{}{:0>2}'.format(today.year, today.month))
                last = int('{}{:0>2}'.format(last.year, last.month))
            else:
                last = now + relativedelta(months=-2)
                today = int('{}{:0>2}'.format(now.year, now.month))
                last = int('{}{:0>2}'.format(last.year, last.month))

            param_dict = {
                'START_MONTH': last,
                'END_MONTH': today,
            }

            results = railway_map.get_map_data(param_dict, g.major, zhanduan_info)
            if isinstance(results, str):
                rs = wrapper_rtn_msg("", 1, results)
            else:
                rs = wrapper_rtn_msg(results, 0, "")
        else:
            results = ''
            rs = {"status": results, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)


@api_bp.route('/map_global_data', methods=['GET'])
def map_global_data():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    # param_dict = request.get_json(force=True)
    param_dict = request.args
    token = request.cookies.get('TOKEN', None)
    if not token:
        return jsonify({"status": '', "error_message": "AUTH_ENABLED ERROR"})
    g.major = "路局" #if int(token.split("#")[-1]) == 1 else get_person_major(token.split('#')[0])
    print(g.major)
    if param_dict and 'TIMESTAMP' in param_dict:
        TIMESTAMP = param_dict['TIMESTAMP']
        results = railway_map.get_global_data(int(TIMESTAMP), g.major)
        if isinstance(results, str):
            rs = wrapper_rtn_msg("", 1, results)
        else:
            rs = wrapper_rtn_msg(results, 0, "")

    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)


@api_bp.route('/map_rank', methods=['GET'])
def map_rank():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    # param_dict = request.get_json(force=True)
    param_dict = request.args
    TYPE = 1
    if param_dict and 'DPID' in param_dict and 'MONTH' in param_dict:
        DPID = param_dict['DPID']
        MONTH = int(param_dict['MONTH'])
        if 'TYPE' in param_dict:
            TYPE = int(param_dict['TYPE'])
        results = railway_map.get_dpid_rank(DPID, TYPE, MONTH)
        if isinstance(results, str):
            rs = wrapper_rtn_msg("", 1, results)
        else:
            rs = wrapper_rtn_msg(results, 0, "")

    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)


def _query_map_date(param_dict, major, station_param=None):
    """
    按照查询条件param_dict（主要是时间段参数）、专业、站段信息，查询地图所需数据
    :param param_dict: 时间查询条件
    :param major: 专业查询条件
    :param station_param: 站段查询条件
    :return:
    """
    if not station_param:
        station_param = {
            'zhanduan_id': None,
            'zhanduan_name': None
        }
    if param_dict and ('START_MONTH' in param_dict) \
            and ('END_MONTH' in param_dict):
        results = railway_map.get_map_data(param_dict, major, station_param)
    else:
        if 'TIMESTAMP' in param_dict:
            now = dt.fromtimestamp(int(param_dict['TIMESTAMP']))
            # 如果是每月25号，则加载的3个月数据（不包含本月，因为本月1号数据没有）
            if now.day > current_app.config.get('UPDATE_DAY'):
                today = now + relativedelta(months=1)
                last = now + relativedelta(months=-1)
                today = int('{}{:0>2}'.format(today.year, today.month))
                last = int('{}{:0>2}'.format(last.year, last.month))
            else:
                last = now + relativedelta(months=-2)
                today = int('{}{:0>2}'.format(now.year, now.month))
                last = int('{}{:0>2}'.format(last.year, last.month))

            param_dict = {
                'START_MONTH': last,
                'END_MONTH': today,
            }

            results = railway_map.get_map_data(param_dict, major, station_param)
        else:
            results = []
    return results


def _check_map_data_param(param_dict):
    """
    检查API的参数是否完备
    :param param_dict:
    :return:
    """
    if not param_dict:
        return False
    if 'TIMESTAMP' in param_dict:
        return True
    elif 'START_MONTH' in param_dict and 'END_MONTH' in param_dict:
        return True
    return False


@api_bp.route('/map_data_all', methods=['GET'])
def map_data_all():
    """
    获取路局全局的数据
    :return:
    """
    param_dict = request.args
    if not _check_map_data_param(param_dict):
        return jsonify({"status": 9, "error_message": "LACK_PARAM ERROR"})

    data = _query_map_date(param_dict, '路局')
    rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@api_bp.route('/map_data_major', methods=['GET'])
def map_data_major():
    """
    获取用户所属专业的地图数据信息
    :return:
    """
    param_dict = request.args
    if not _check_map_data_param(param_dict):
        return jsonify({"status": 9, "error_message": "LACK_PARAM ERROR"})

    user_info = g.user_info
    if not user_info:
        '''兼容原理的系统功能，使用原理系统的cookie的TOKEN'''
        token = request.cookies.get('TOKEN', None)
        # 使用Token的第一段ID_CARD判断
        id_card = token.split('#')[0]
        if not token or id_card == 'no_user':
            return jsonify({"status": 9, "error_message": "非法用户"})
    else:
        id_card = user_info.get('id_card')
    person = mongo.db['base_person'].find_one({"ID_CARD": id_card}, {"_id": 0})
    g.major = person.get('MAJOR')

    # 专业级别
    data = _query_map_date(param_dict, person.get('MAJOR'))
    rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/map_data_station', methods=['GET'])
def map_data_station():
    """
    获取用户所属站段的地图数据信息
    :return:
    """
    param_dict = request.args
    if not _check_map_data_param(param_dict):
        return jsonify({"status": 9, "error_message": "LACK_PARAM ERROR"})

    user_info = g.user_info
    if not user_info:
        '''兼容原理的系统功能，使用原理系统的cookie的TOKEN'''
        token = request.cookies.get('TOKEN', None)
        # 使用Token的第一段ID_CARD判断
        id_card = token.split('#')[0]
        if not token or id_card == 'no_user':
            return jsonify({"status": 9, "error_message": "非法用户"})
    else:
        id_card = user_info.get('id_card')
    person = mongo.db['base_person'].find_one({"ID_CARD": id_card}, {"_id": 0})
    g.major = person.get('MAJOR')

    # 设置站段查询条件
    station_param = {
        "zhanduan_id": person.get('STATION_ID'),
        'zhuanduan_name': person.get('STATION_NAME')
    }

    # 站段级别
    data = _query_map_date(param_dict, person.get('MAJOR'), station_param)
    rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


# @api_bp.before_app_first_request
# def setup_logging():
#     app = current_app
#     if not app.debug:
#         # In production mode, add log handler to sys.stderr.
#         app.logger.addHandler(logging.StreamHandler())
#         app.logger.setLevel(logging.INFO)
