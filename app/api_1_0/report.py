import os
from flask import (current_app, jsonify, make_response, request,
                   send_from_directory)
from app.api_1_0 import api_bp
from app.report import (analysisReport, dailyReport, evaluateNewReport,
                        evaluateRank, analysisWeeklyReport)
from app.report.evaluateNewReport import validate_id_card, export_evaluate_word, validate_evaluate_id_card, \
    has_safety_mon_perm
from app.report.group.group_daily_report import GroupDailyAnalysisReport
from app.utils.common_func import is_lack_required_args, wrapper_rtn_msg
from app.report.util import get_person_role


@api_bp.route('/report', methods=['GET'])
def get_report_list():
    '''根据选取日期获取可查看报告列表'''
    param_dict = request.args
    if 'start_date' in param_dict and 'end_date' in param_dict:
        start_time = param_dict['start_date']
        end_time = param_dict['end_date']
        safety_reports = dailyReport.get_daily_report_list(
            start_time, end_time)
        # safety_reports = []
        analysis_reports = analysisReport.get_analysis_report_list(
            start_time, end_time)
        evaluate_reports = evaluateNewReport.get_evaluate_report_list(
            start_time, end_time)
        evaluate_rank = evaluateRank.get_evaluate_rank_list(
            start_time, end_time)
        analysis_weekly = analysisWeeklyReport.get_analysis_weekly_list(
            start_time, end_time)
        analysis_daily = GroupDailyAnalysisReport.get_list(start_time, end_time)
        reports = analysis_reports + safety_reports + \
                  evaluate_reports + evaluate_rank + analysis_weekly + analysis_daily
        reports.reverse()
        rs = {'data': reports, 'status': 0, 'error_message': ''}
    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}
    return jsonify(rs)


@api_bp.route('/report/evaluate', methods=['GET'])
def get_other_list_by_page():
    '''根据当前人员权限获取可查看下属或本人报告列表'''
    param_dict = request.args
    token = request.cookies.get('TOKEN')
    if not token:
        return jsonify(wrapper_rtn_msg("", 1, 'NO_TOKEN'))
    id_card = token.split('#')[0]
    permission = validate_id_card(id_card)
    if not permission:
        return jsonify(wrapper_rtn_msg("", 1, 'NO_PERMISSION'))
    # id_card = request.cookies.get('TOKEN').split('#')[0]
    page = int(param_dict.get('page', 1))
    name = param_dict.get('name', None)
    month = int(param_dict.get('date').replace('/', '')[:6])
    station = param_dict.get('station', '')
    number = int(param_dict.get('number', 20))
    result = evaluateNewReport.get_other_list(id_card, station, month, name,
                                              page, number)
    rs = {'data': result, 'status': 0, 'error_message': ''}
    return jsonify(rs)


@api_bp.route('/report/evaluate/other', methods=['GET'])
def get_report():
    '''查看报告内容'''
    param_dict = request.args
    id_card = param_dict.get('id_card')
    date = param_dict.get('date')
    TYPE = param_dict.get('TYPE')
    month = int(date[:7].replace('/', ''))
    results = evaluateNewReport.get_report(id_card, month, TYPE)
    rs = {'data': results, 'status': 0, 'error_message': ''}
    return jsonify(rs)


@api_bp.route('/report/SAFETY')
def get_daily_report():
    '''查看生产日报内容'''
    param_dict = request.args
    report_id = param_dict.get('id')
    result = dailyReport.get_report(report_id)
    rs = {'data': result, 'status': 0, 'error_message': ''}
    return jsonify(rs)


@api_bp.route('/report/analysis')
def get_analysis_report():
    '''查看安全情况月度分析报告'''
    param_dict = request.args
    necessary_param = {'start_date', 'end_date'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    year = end_date[:4]
    month = end_date[5:7]
    if int(start_date[8:]) == 25 and int(end_date[8:]) == 24:
        filename = f'{year}年{month}月份集团公司安全情况分析报告.docx'
    else:
        filename = f'{start_date}至{end_date}集团公司安全情况分析报告.docx'
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_ANALYSIS'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反则重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = analysisReport.save_analysis_report(start_date, end_date,
                                                  file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@api_bp.route('report/evaluate_rank')
def get_detail_evaluate_rank_report():
    """干部履职统计
    干部履职总体分析: first
    同类干部: fourth
    各级干部: second
    段内干部: third
    领导班子: fifth
    科室分析: sixth
    干部履职统计: seventh
    """
    params = request.args
    id_card = params.get('id_card')
    role_info = get_person_role(id_card)
    # 安监监察大队及安监室新政领导的拥有本api数据权限的出来 2019-09-26
    if has_safety_mon_perm(id_card):
        role_info = {'role': 0, 'major': '', 'station': ''}
    # End of 安监监察大队及安监室新政领导的拥有本api数据权限的出来 2019-09-26
    date = params.get('date')
    department = params.get('department', '')
    level = params.get('level', '')
    page = params.get('page', 1)
    table_type = params.get('type')
    permission = validate_evaluate_id_card(table_type)
    if not permission:
        return jsonify(wrapper_rtn_msg("", 1, 'NO_PERMISSION'))
    func_dict = {
        'first': evaluateRank.get_first_major_carde_statistics,
        'second': evaluateRank.get_second_station_evaluate_rank,
        'third': evaluateRank.get_third_station_evaluate_rank_detail,
        'fourth': evaluateRank.get_fourth_major_carde_rank,
        'fifth': evaluateRank.get_fifth_leader_rank,
        'sixth': evaluateRank.get_sixth_office_rank,
        'seventh': evaluateRank.get_seventh_global_rank
    }
    table = func_dict.get(table_type)(
        role_info, date, department=department, level=level, page=page)
    rs = {'data': table, 'status': 0, 'error_message': ''}
    return jsonify(rs)


@api_bp.route('report/weekly_analysis')
def get_weekly_analysis_report():
    param_dict = request.args
    necessary_param = {'start_date', 'end_date'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    filename = f'{start_date}至{end_date}集团公司周安全分析报告.docx'
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(
        root_path, current_app.config.get('DOWNLOAD_FILE_WEEKLY_ANALYSIS'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反则重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = analysisWeeklyReport.save_weekly_report(start_date, end_date,
                                                      file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))

# --------------------履职报告类------------------------- #


@api_bp.route('report/evaluate/download')
def get_evaluate_download():
    '''根据当前人员权限获取可查看下属或本人报告列表'''
    param_dict = request.args
    necessary_param = {'date', 'major', 'station'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    id_card = request.cookies.get('TOKEN').split('#')[0]
    month = int(param_dict.get('date').replace('/', '')[:6])
    major = param_dict.get('major', '')
    station = param_dict.get('station', '')
    if major == '':
        department = '全局'
    else:
        if station == '':
            department = major
        else:
            department = major + '_' + station
    filename = f'个人履职得分_{department}_{month}.xls'
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path, current_app.config.get('DOWNLOAD_FILE_EVALUATE'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反则重新生成
    if os.path.exists(file_path):
        return make_response(send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = evaluateNewReport.save_evaluate(id_card, month, major, station, file_path)
        if rtn is True:
            return make_response(send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@api_bp.route('/report/evaluate/export_file', methods=['GET'])
def evaluate_file():
    """履职报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'date', 'id_card', 'name', "TYPE"}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))

    # 检验下载权限
    # permission_id_card = '510625197612110019'
    token = request.cookies.get('TOKEN')
    if not token:
        return jsonify(wrapper_rtn_msg("", 1, 'NO_TOKEN'))
    permission_id_card = token.split('#')[0]
    permission = validate_id_card(permission_id_card)
    if not permission:
        return jsonify(wrapper_rtn_msg("", 1, 'NO_PERMISSION'))

    # 获取参数
    id_card = param_dict.get("id_card")
    date = param_dict.get("date")
    evaluate_type = param_dict.get("TYPE")
    name = param_dict.get("name")
    month = int(date[:7].replace('/', ''))
    if evaluate_type == "history":
        mon = month // 100
    else:
        mon = month % 100

    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_EVALUATE'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    filename = f'{name}{mon}月安全履职分析报告.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_evaluate_word(id_card, month, mon, name, evaluate_type, file_path)
        if rtn:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))
