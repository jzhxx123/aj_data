from flask import g, jsonify, request

from app.api_1_0 import api_bp
from app.utils.common_func import wrapper_rtn_msg
from app.global_story import health_index, major_index, safety_produce_info


@api_bp.route('/global/safety_produce_info', methods=['GET'])
def ssafety_produce_info():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    param_dict = request.args
    if param_dict and ('START_MONTH' in param_dict) and (
            'END_MONTH' in param_dict):
        results = safety_produce_info.get_data(param_dict, g.major)
        if isinstance(results, str):
            rs = wrapper_rtn_msg("", 1, results)
        else:
            rs = wrapper_rtn_msg(results, 0, "")

    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)


@api_bp.route('/global/health_index', methods=['GET'])
def f_health_index():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    param_dict = request.args
    if param_dict and 'MAJOR' in param_dict and 'MONTH' in param_dict:
        results = health_index.get_data(param_dict)
        if isinstance(results, str):
            rs = wrapper_rtn_msg("", 1, results)
        else:
            rs = wrapper_rtn_msg(results, 0, "")

    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)


@api_bp.route('/global/major_index', methods=['GET'])
def f_major_index():
    # param_dict = request.get_json()
    # 无视application/json, 强制转换为json
    param_dict = request.args
    if param_dict and 'MONTH' in param_dict and 'TYPE' in param_dict:
        results = major_index.get_data(param_dict)
        if isinstance(results, str):
            rs = wrapper_rtn_msg("", 1, results)
        else:
            rs = wrapper_rtn_msg(results, 0, "")

    else:
        rs = {"status": 1, "error_message": "LACK_PARAM ERROR"}

    return jsonify(rs)
