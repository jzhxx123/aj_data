import os
import re
import shutil
from flask import (current_app, jsonify, make_response, request,
                   send_from_directory)

from app.api_1_0 import api_bp
from app.story import (story_check_info_problem, story_evaluate,
                       story_health_index, story_major_index,
                       story_safety_produce_info)
from app.utils.common_func import (get_department_name_by_dpid,
                                   is_lack_required_args, wrapper_rtn_msg)
from app.utils.safety_index_common_func import (
    export_health_index_word, export_major_index_word, get_risk_type_name)


@api_bp.route('/evaluate', methods=['GET'])
def evaluate():
    # param_dict = request.args
    param_dict = request.args
    if ('DPID' in param_dict) and ('START_MONTH' in param_dict) and (
            'END_MONTH' in param_dict):
        data = story_evaluate.get_data(param_dict)
        if isinstance(data, str):
            rs = wrapper_rtn_msg("", 1, data)
        else:
            rs = wrapper_rtn_msg(data, 0, "")
    else:
        rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")

    return jsonify(rs)


@api_bp.route('/evaluate_detail', methods=['GET'])
def evaluate_code():
    param_dict = request.args
    necessary_param = [
        'DPID', 'START_MONTH', 'END_MONTH', 'LEVEL', 'TYPE', 'CODE'
    ]
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)

    data = story_evaluate.get_code_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")
    return jsonify(rs)


@api_bp.route('/safety_produce_info', methods=['GET'])
def safety_produce_info():
    param_dict = request.args
    if ('DPID' in param_dict) and ('START_MONTH' in param_dict) and (
            'END_MONTH' in param_dict):
        data = story_safety_produce_info.get_data(param_dict)
        if isinstance(data, str):
            rs = wrapper_rtn_msg("", 1, data)
        else:
            rs = wrapper_rtn_msg(data, 0, "")
    else:
        rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
    return jsonify(rs)


@api_bp.route('/safety_produce_info_detail', methods=['GET'])
def safety_produce_info_detail():
    param_dict = request.args
    rs = None
    if 'DPID' in param_dict and 'MON' in param_dict:
        data = story_safety_produce_info.get_detail_data(param_dict)
        if isinstance(data, str):
            rs = wrapper_rtn_msg("", 1, data)
        else:
            rs = wrapper_rtn_msg(data, 0, "")
    else:
        rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
    return jsonify(rs)


@api_bp.route('/check_info_problem', methods=['GET'])
def check_info_problem():
    param_dict = request.args
    if ('DPID' in param_dict) and ('START_MONTH' in param_dict) and (
            'END_MONTH' in param_dict):
        data = story_check_info_problem.get_data(param_dict)
        if isinstance(data, str):
            rs = wrapper_rtn_msg("", 1, data)
        else:
            rs = wrapper_rtn_msg(data, 0, "")
    else:
        rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
    return jsonify(rs)


@api_bp.route('/check_problem_detail', methods=['GET'])
def check_problem_detail():
    param_dict = request.args
    necessary_param = ['DPID', 'START_MONTH', 'END_MONTH', 'WEEKDAY', 'HOUR']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = story_check_info_problem.get_detail_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/health_index', methods=['GET'])
def health_index():
    param_dict = request.args
    necessary_param = ['DPID']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = story_health_index.get_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/health_index/tree', methods=['GET'])
def health_index_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = story_health_index.calc_tree(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/major_index', methods=['GET'])
def major_index():
    param_dict = request.args
    necessary_param = ['DPID', 'TYPE']
    for each in necessary_param:
        if each not in param_dict:
            rs = wrapper_rtn_msg("", 1, "LACK_PARAM ERROR")
            return jsonify(rs)
    data = story_major_index.get_data(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/major_index/tree', methods=['GET'])
def major_index_tree():
    """指数的中间计算过程按树结构展示
    """
    param_dict = request.args
    necessary_param = {'DPID', 'TYPE', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    data = story_major_index.calc_tree(param_dict)
    if isinstance(data, str):
        rs = wrapper_rtn_msg("", 1, data)
    else:
        rs = wrapper_rtn_msg(data, 0, "")

    return jsonify(rs)


@api_bp.route('/major_index/export_file', methods=['GET'])
def major_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID', 'MAJOR', 'RISK_TYPE'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    risk_name = get_risk_type_name(param_dict['MAJOR'],
                                   param_dict['RISK_TYPE'])
    risk_name = f'{param_dict["MAJOR"]}_{risk_name}'
    filename = f'{department_name}_{risk_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_major_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@api_bp.route('/health_index/export_file', methods=['GET'])
def health_index_file():
    """指数报告生成并提供下载
    """
    param_dict = request.args
    necessary_param = {'MONTH', 'DPID'}
    validate_rtn = is_lack_required_args(necessary_param, param_dict.keys())
    if validate_rtn:
        return jsonify(wrapper_rtn_msg("", 1, 'LACK_PARAM_ERROR'))
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    dir_path = os.path.join(root_path,
                            current_app.config.get('DOWNLOAD_FILE_HEALTH'))
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    department_name = get_department_name_by_dpid(param_dict['DPID'])
    filename = f'安全管理综合指数_{department_name}_{param_dict["MONTH"]}.docx'
    file_path = os.path.join(dir_path, filename)
    # 已经存在该站段文档，则直接返回，反正重新生成
    if os.path.exists(file_path):
        return make_response(
            send_from_directory(dir_path, filename, as_attachment=True))
    else:
        rtn = export_health_index_word(department_name, param_dict, file_path)
        if rtn is True:
            return make_response(
                send_from_directory(dir_path, filename, as_attachment=True))
        else:
            return jsonify(wrapper_rtn_msg("", 1, rtn))


@api_bp.route('/download/health_index/clear', methods=['GET'])
def download_clear():
    """删除下载文件目录下制定目录下的所有文件，如果没有指定则是health_index下
    """
    param = request.args
    folder_path = 'download/health_index'
    if 'folder_name' in param:
        folder_path = param['folder_path']
    # 获取项目根目录路径
    root_path = os.path.commonprefix([__file__, current_app.instance_path])
    folder_path = os.path.join(root_path, folder_path)

    if os.path.exists(folder_path):
        for remove_path in os.listdir(folder_path):
            remove_path = os.path.join(folder_path, remove_path)
            if os.path.isfile(remove_path):
                os.remove(remove_path)
            if os.path.isdir(remove_path):
                shutil.rmtree(remove_path)
        return jsonify(wrapper_rtn_msg("删除成功", 0, ""))
    else:
        return jsonify(wrapper_rtn_msg("", 1, "目录不存在"))


def _check_dpid_month(dpid, month):
    if dpid.isalnum():
        if re.match('^[0-9]{4}-[0-9]{1,2}', month):
            return ''
        else:
            return {"status": 1, "error_message": 'Month error'}
    else:
        return {"status": 1, "error_message": 'dpid error'}
