import json
from urllib.parse import parse_qs, urlparse

from flask import Response, current_app, g, jsonify, request
# from werkzeug.urls import url_parse

from app.api_1_0 import api_bp
from app.api_1_0.util import get_user_cardId, judg_accessToken
from app.utils.common_func import get_person_info, get_person_major

# from app.utils.common_func import get_token_major

major_map = {
    '供电': 0,
    '车辆': 1,
    '机务': 2,
    '车务': 3,
    '工务': 4,
    '电务': 5,
    '局领导': 6,
    "路局": 6,
    '安监': 6,
    '工电': 7,
    '客运': 6,
}


@api_bp.route('/get_role', methods=['GET'])
def get_role():
    # param_dict = request.get_json()
    user = request.args.get('user', '')
    print(user)
    token = request.cookies.get('TOKEN')
    # 无视application/json, 强制转换为json
    # 获取用户uuid
    auth_enabled = current_app.config.get('AUTH_ENABLED')
    if auth_enabled is True:  # 路局权限判定
        referer = request.headers.get('Referer')
        if referer and 'SSOA' in referer:  # 走蜂窝系统
            user_uid = referer.split("=")[1]
            judg_accessToken()
            userID = get_user_cardId(user_uid)
            if userID == 'no_user':
                content = 'no_user#1#4#0#0###3'
                g.major = None
            else:
                content = get_person_info(userID)
        elif referer and 'IDCARD' in referer:
            userID = parse_qs(urlparse(referer).query).get('IDCARD')[0]
            content = get_person_info(userID)
        elif token:
            content = token
            try:
                content = get_person_info(token.split('#')[0])
            except Exception as e:
                g.major = None
        elif user == 'guanxiaobo':  # 直接访问
            content = get_person_info('510625197612110019')
        else:  # 无访问权限
            content = 'no_user#1#4#0#0###3'
            g.major = None
    else:  # dev权限（官小波权限）
        if len(user) == 18:
            # 指定了特定的用户
            content = get_person_info(user)
        else:
            content = get_person_info('510625197612110019')
        # content = get_person_info('50023619941105713X')
        # f'{idcard}#{job_id}#{dep_id}#{station}#{all_name}#{zhanduan_id}#{zhanduan_name}#{permission_type}'
    permission_type = int(content.split('#')[7])
    major = get_person_major(content.split('#')[0])
    if permission_type == 3 or (not g.major):
        rst = {}
    else:
        rst = {
            'level': 1 if major_map.get(major) == 6 else 2,
            'major': major_map.get(major),
            'department': int(content.split('#')[2]),
            'job': int(content.split('#')[1]),
            'station': content.split('#')[3],
            'all_name': content.split('#')[4],
            'user': content.split('#')[0],
            "zhanduan_id": content.split('#')[5],
            "zhanduan_name": content.split('#')[6]
        }
    rs = {'data': rst, 'status': 0, 'error_message': ''}
    res = Response(json.dumps(rs), mimetype='application/json')
    res.delete_cookie('TOKEN')
    if content is not None:
        expire = 32472115200  # 2999年过期
        res.set_cookie(
            key='TOKEN',
            value=content,
            max_age=None,
            expires=expire,
            httponly=False)
    return res


@api_bp.route('/get_big_role', methods=['GET'])
def get_big_role():
    # param_dict = request.get_json()
    # param_dict = request.args
    # 无视application/json, 强制转换为json
    # 获取用户uuid
    user = request.args.get('user', '')
    auth_enabled = current_app.config.get('AUTH_ENABLED')
    if auth_enabled is True:
        referer = request.headers.get('Referer')
        if referer and 'SSOA' in referer:
            user_uid = referer.split("=")[1]
            judg_accessToken()
            userID = get_user_cardId(user_uid)
            content = get_person_info(userID)
        elif user == 'guanxiaobo':
            content = get_person_info('510625197612110019')
        else:
            content = 'no_user#1#4#0#0###3'
            g.major = None
    else:
        content = get_person_info('510625197612110019')
    permission_type = int(content.split('#')[7])
    major = get_person_major(content.split('#')[0])
    if permission_type == 3 or (not g.major):
        rst = {}
    else:
        rst = {
            'level': 1 if major_map.get(major) == 6 else 2,
            'major': major_map.get(major),
            'department': int(content.split('#')[2]),
            'job': int(content.split('#')[1]),
            'station': content.split('#')[3],
            'all_name': content.split('#')[4],
            'user': content.split('#')[0],
            "zhanduan_id": content.split('#')[5],
            "zhanduan_name": content.split('#')[6]
        }

    rs = {'data': rst, 'status': 0, 'error_message': ''}
    res = Response(json.dumps(rs), mimetype='application/json')
    return res


@api_bp.route('/get_major_index', methods=['GET'])
def get_major_index():
    """提供指数的列表清单。
    它用于【站段排行】顶部筛选的选项中。它返回的数据，主要包含label,major,order三个字段。
    其对应意义：
        label: 指数在页面上显示的选项的名称
        major: 实际上是序号。在前端的代码里面，硬编码了['供电', '车辆', '机务', '车务', '工务', '电务', '客运', '工电']数组。它
               的【下标】对应了major的值。从0开始。
        order: 它实际上是risk_type，将被前端作为RISK_TYPE在调用数据的时候返回。同时也用于前端进行菜单项排序。
    Returns:
        [JSON] -- [指数的清单] 格式为：
        {   
            "data": [{
                "label": "专业管理风险",
                "major": 7,
                "order": 3
            },..],
            "error_message": "",
            "status": 0
        }
    """
    # 格式为： "<风险名称>":"<专业名称>-<RISK_TYPE>"
    risk_names = {
        "设备质量风险": "供电-1",
        # "规章制度风险": "供电-2",
        "专业管理风险": "供电-3",
        # "接触网专业管理风险": "供电-4",
        # "职工素质": "供电-5",
        "劳安风险": "供电-6",
        "施工安全": "供电-7",
        # "自轮设备安全风险": "供电-8",
        "自轮设备运用风险": "供电-9",
        "施工配合监管风险": '供电-10',
        # "电务普铁信号设备质量风险分析": "电务-1",
        # "电务普铁通信设备质量风险分析": "电务-2",
        # "电务高铁通信设备质量风险分析": "电务-3",
        # "电务高铁信号设备质量风险分析": "电务-4",
        "电务劳动安全风险分析": "电务-5",
        "电务施工安全风险分析": "电务-6",
        "电务道岔设备": "电务-7",
        "点外修": "电务-8",
        "电务轨道电路": "电务-9",
        "电务信号机": "电务-10",
        "调车": "车务-1",
        "劳动安全": "车务-2",
        # "车门管理": "车务-3",
        # "客运组织": "车务-4",
        # "多方向接发列车": "车务-5",
        "接发列车": "车务-6",
        "高动客接发列车": "车务-7",
        '货装安全风险': '车务-8',
        # '接发列车错办风险': '车务-9',
        '调乘一体化': '车务-10',
        "间断瞭望": "机务-1",
        "调车风险": "机务-2",
        # "漏检漏修、简化修风险": "机务-3",
        "错误操纵风险": "机务-4",
        # "劳安风险": "机务-5",
        "机车质量风险": "机务-6",
        "库内牵车": "机务-7",
        "火灾爆炸": "车辆-1",
        # "劳动安全": "车辆-2",
        # "客车制动抱闸": "车辆-3",
        # "货车制动抱闸": "车辆-4",
        "高压牵引": "车辆-5",
        # "车辆脱轨": "车辆-6",
        # "列车分离": "车辆-7",
        "配件脱落(货车)": "车辆-8",
        "配件脱落(动车)": "车辆-9",
        "配件脱落(客车)": "车辆-10",
        "调车防溜": "车辆-11",
        "防断风险": "工务-1",
        "劳动安全风险": "工务-2",
        "点外修作业风险": "工务-3",
        "防洪风险": "工务-4",
        "自轮设备风险": "工务-5",
        "施工安全风险": "工务-6",
        "施工安全风险": "工电-2",
        "劳动安全风险": "工电-1",
        '劳动安全风险': "客运-1",
        "客运组织及消防(车务站)": "客运-2",
        "客运组织及消防(客运段)": "客运-3",
        "客运组织及消防(客站)": "客运-4",
        # "施工风险": "工务-7",
        # "天窗修风险": "工务-8",
        "施工监管风险": "工务-9",
        "站专线风险": "工务-10",
        "天窗施工单项": "工务-11",
        "点外修作业风险": "工电-3",
        # "天窗修作业风险": "工电-4",
        # "施工作业风险": "工电-5",
        "施工安全风险(工务版)": "工电-6",
        "劳动安全风险(工务版)": "工电-7",
        "劳动安全风险(电务版)": "工电-8",
        "施工安全风险(电务版)": "工电-9",
        "劳动安全风险(供电版)": "工电-10",
        "施工安全风险(供电版)": "工电-11",
        # "专业管理风险(供电版)": "工电-13",
        # "自轮设备风险(供电版)": "工电-14",
        # "施工配合监管风险(供电版)": "工电-15",
    }
    index_item = [
        # ["安全综合指数", 6, 1],
        ["劳安风险", 2, 5],
        ["劳动风险", 1, 2],
        ["劳动安全风险", 7, 1],
        ["施工安全风险", 4, 6],
        ["劳动安全风险", 4, 2],
        ["点外修作业风险", 4, 3],
        ["天窗施工单项", 7, 12],
    ]
    '''
    major_map实际上是序号。在前端的代码里面，硬编码了['供电', '车辆', '机务', '车务', '工务', '电务', '客运', '工电']数组。
    它的【下标】对应了major的值。从0开始。
    '''
    major_map = {
        '供电': 0,
        '车辆': 1,
        '机务': 2,
        '车务': 3,
        '工务': 4,
        '电务': 5,
        # '路局': 6,
        # '安监': 6,
        '客运': 6,
        '工电': 7,
    }
    for each in risk_names:
        val = risk_names[each]
        index_item.append([each,
                           major_map.get(val.split('-')[0]),
                           int(val.split('-')[1])])
    data = [{
        'label': item[0],
        'major': item[1],
        'order': item[2]
    } for item in index_item]
    rs = {'data': data, 'status': 0, 'error_message': ''}
    return jsonify(rs)
