from flask import (jsonify, make_response, request,
                   send_from_directory)
from app.api_1_0 import api_bp
from app.data.util import pd_query, mongo
from app.report.group.group_daily_report import GroupDailyAnalysisReport
from app.utils.common_func import wrapper_rtn_msg
from app.data.major_risk_control_intensity_index.combine_child_index import execute as major_excute

major_map = {
    '供电': 0,
    '车辆': 1,
    '机务': 2,
    '车务': 3,
    '工务': 4,
    '电务': 5,
    '工电': 7,
    '客运': 8,
}

major_dict = {
    'gongdian': '1ACE7D1C80B04456E0539106C00A2E70KSC',
    'cheliang': '1ACE7D1C80B44456E0539106C00A2E70KSC',
    'jiwu': '1ACE7D1C80B14456E0539106C00A2E70KSC',
    'gongwu': '1ACE7D1C80AF4456E0539106C00A2E70KSC',
    'dianwu': '1ACE7D1C80B34456E0539106C00A2E70KSC',
}


@api_bp.route('/report/param/stations')
def get_station_list():
    res = {}
    data = pd_query(
        """select ALL_NAME,DEPARTMENT_ID,BELONG_PROFESSION_ID,FK_PARENT_ID from t_department where type=4 and LENGTH(SHORT_NAME)>0""")
    for key, value in major_dict.items():
        major_data = data[data['FK_PARENT_ID'] == value]
        all_department = []
        for i, j in major_data.iterrows():
            dic = {
                'dp_id': j['DEPARTMENT_ID'],
                'dp_name': j['ALL_NAME']
            }
            all_department.append(dic)
        res[key] = all_department
    # 客运
    keyun_data = data[data['BELONG_PROFESSION_ID'] == 898]
    keyun = []
    for i, j in keyun_data.iterrows():
        dic = {
            'dp_id': j['DEPARTMENT_ID'],
            'dp_name': j['ALL_NAME']
        }
        keyun.append(dic)
    res['keyun'] = keyun
    # 车务
    chewu_data = data[data['BELONG_PROFESSION_ID'] == 897]
    chewu = []
    for i, j in chewu_data.iterrows():
        dic = {
            'dp_id': j['DEPARTMENT_ID'],
            'dp_name': j['ALL_NAME']
        }
        chewu.append(dic)
    res['chewu'] = chewu
    rs = {'data': res, 'status': 0, 'error_message': ''}
    return jsonify(rs)


# 报表Class的字典
report_class_dict = {}


def _get_report_class(major, hierarchy, interval_type):
    # 动态创建目标的类实例。先查找目标Class
    if hierarchy.lower() == 'group':
        class_name = f'{hierarchy.capitalize()}{interval_type.capitalize()}AnalysisReport'
        major = 'group'
    else:
        class_name = f'{major.capitalize()}{hierarchy.capitalize()}{interval_type.capitalize()}AnalysisReport'
    class_meta = report_class_dict.get(class_name)
    if not class_meta:
        try:
            module_meta = __import__(f'app.report.{major.lower()}.{hierarchy.lower()}_{interval_type.lower()}_report',
                                     globals(), locals(), [class_name])
            class_meta = getattr(module_meta, class_name)
            report_class_dict[class_name] = class_meta
        except (ModuleNotFoundError, AttributeError) as ex:
            return None
    return class_meta


@api_bp.route('report/analysis/<string:major>/station/weekly/doc', methods=['GET'])
def get_analysis_weekly_station_doc(major):
    """
    站段级周报的下载文档的请求处理函数
    :param major: 专业（拼音）
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'WEEKLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta(station_id=station_id)
        if report_obj.validate_interval(start_date, end_date) is True:
            dir_path, file_name = report_obj.get_report_file(start_date, end_date)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或周报起始时间无效'))


@api_bp.route('report/analysis/<string:major>/station/weekly/list')
def get_analysis_weekly_station_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        if major == 'dianwu':
            return jsonify(wrapper_rtn_msg({
                "available_list": [],
                "interval_type": "WEEKLY",
                "topic": "贵阳车辆段周分析报告"
            }, 0, ''))
        else:
            # 动态创建目标的类实例。先查找目标Class
            class_meta = _get_report_class(major, 'STATION', 'WEEKLY')
            if class_meta is None:
                return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

            # 创建报表class的实例
            report_obj = class_meta(station_id=station_id)

            data = report_obj.get_available_report_interval_list(start_date, end_date)
            return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/weekly/doc', methods=['GET'])
def get_analysis_weekly_major_doc(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'WEEKLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()
        if report_obj.validate_interval(start_date, end_date) is True:
            dir_path, file_name = report_obj.get_report_file(start_date, end_date)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或周报起始时间无效'))


@api_bp.route('report/analysis/<string:major>/major/weekly/list')
def get_analysis_weekly_major_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'WEEKLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/monthly/list')
def get_analysis_monthly_major_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'MONTHLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/station/monthly/list')
def get_analysis_monthly_station_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'MONTHLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta(station_id)

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/monthly/doc', methods=['GET'])
def get_analysis_monthly_major_doc(major):
    param_dict = request.args
    year = param_dict.get('year')
    month = param_dict.get('month')

    if 'year' in param_dict and 'month' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'MONTHLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year, month = int(year), int(month)
        report_obj = class_meta()
        if report_obj.validate_month(year, month) is True:
            dir_path, file_name = report_obj.get_report_file(year, month)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或月报年月无效'))


@api_bp.route('report/analysis/<string:major>/station/monthly/doc', methods=['GET'])
def get_analysis_monthly_station_doc(major):
    param_dict = request.args
    year = param_dict.get('year')
    month = param_dict.get('month')
    station_id = param_dict.get('DPID')

    if 'year' in param_dict and 'month' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'MONTHLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year, month = int(year), int(month)
        report_obj = class_meta(station_id)
        if report_obj.validate_month(year, month) is True:
            dir_path, file_name = report_obj.get_report_file(year, month)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或月报年月无效'))


@api_bp.route('report/analysis/<string:major>/major/quarterly/list')
def get_analysis_quarterly_major_list(major):
    """
    获取专业级的季度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'QUARTERLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/quarterly/doc', methods=['GET'])
def get_analysis_quarterly_major_doc(major):
    """
    导出专业级季度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')
    quarter = param_dict.get('quarter')

    if 'year' in param_dict and 'quarter' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'QUARTERLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year, quarter = int(year), int(quarter)
        report_obj = class_meta()
        if report_obj.validate_quarter(year, quarter) is True:
            dir_path, file_name = report_obj.get_report_file(year, quarter)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


@api_bp.route('report/analysis/<string:major>/station/quarterly/list')
def get_analysis_quarterly_station_list(major):
    """
    获取站段级的季度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'QUARTERLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta(station_id)

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/station/quarterly/doc', methods=['GET'])
def get_analysis_quarterly_station_doc(major):
    """
    导出站段季度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')
    quarter = param_dict.get('quarter')
    station_id = param_dict.get('DPID')

    if 'year' in param_dict and 'quarter' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'QUARTERLY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year, quarter = int(year), int(quarter)
        report_obj = class_meta(station_id)
        if report_obj.validate_quarter(year, quarter) is True:
            dir_path, file_name = report_obj.get_report_file(year, quarter)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


@api_bp.route('report/analysis/<string:major>/major/annual/list')
def get_analysis_annual_major_list(major):
    """
    获取专业级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'ANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/annual/doc', methods=['GET'])
def get_analysis_annual_major_doc(major):
    """
    导出专业级年度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')

    if 'year' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'ANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year = int(year)
        report_obj = class_meta()
        if report_obj.validate_annual(year) is True:
            dir_path, file_name = report_obj.get_report_file(year)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或年报参数无效'))


@api_bp.route('report/analysis/<string:major>/station/annual/list')
def get_analysis_annual_station_list(major):
    """
    获取站段级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'ANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta(station_id)

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/station/annual/doc', methods=['GET'])
def get_analysis_annual_station_doc(major):
    """
    导出站段季度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')
    station_id = param_dict.get('DPID')

    if 'year' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'ANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year = int(year)
        report_obj = class_meta(station_id)
        if report_obj.validate_annual(year) is True:
            dir_path, file_name = report_obj.get_report_file(year)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


# ----------- 半年报接口 start
@api_bp.route('report/analysis/<string:major>/major/semiannual/list')
def get_analysis_semiannual_major_list(major):
    """
    获取专业级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'SEMIANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/semiannual/doc', methods=['GET'])
def get_analysis_semiannual_major_doc(major):
    """
    导出专业级年度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')
    half = param_dict.get('half')

    if 'year' in param_dict and 'half' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'SEMIANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year = int(year)
        half = int(half)
        report_obj = class_meta()
        if report_obj.validate_semiannual(year, half) is True:
            dir_path, file_name = report_obj.get_report_file(year, half)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或年报参数无效'))


@api_bp.route('report/analysis/<string:major>/station/semiannual/list')
def get_analysis_semiannual_station_list(major):
    """
    获取站段级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    station_id = param_dict.get('DPID')

    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'SEMIANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        report_obj = class_meta(station_id)

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/station/list')
def get_analysis_station_list():
    """
    获取站段级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    data = list(mongo.db['base_department'].find(
        {'TYPE': 4, 'IS_DELETE': 0, '$and': [{'SHORT_NAME': {'$ne': None}}, {'SHORT_NAME': {'$ne': ''}}]},
        {'_id': 0, 'DEPARTMENT_ID': 1, 'MAJOR': 1, 'NAME': 1}))
    for i in data:
        i['MAJOR'] = major_map.get(i['MAJOR'])
    if data:
        return jsonify({'data': data, 'status': 0})
    else:
        return jsonify({'error_msg': '无数据', 'status': 1})


@api_bp.route('report/analysis/chejian/list')
def get_analysis_chejian_list():
    """
    获取站段级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    data = list(mongo.db['base_department'].find(
        {'TYPE': 8, 'IS_DELETE': 0, '$and': [{'SHORT_NAME': {'$ne': None}}, {'SHORT_NAME': {'$ne': ''}}]},
        {'_id': 0, 'DEPARTMENT_ID': 1, 'MAJOR': 1, 'NAME': 1, 'TYPE3': 1, 'SHORT_NAME': 1}))
    for i in data:
        i['MAJOR'] = major_map.get(i['MAJOR'])
    if data:
        return jsonify({'data': data, 'status': 0})
    else:
        return jsonify({'error_msg': '无数据', 'status': 1})


@api_bp.route('report/analysis/banzu/list')
def get_analysis_banzu_list():
    """
    获取站段级的年度报告的清单
    :param major: 专业名称的拼音
    :return:
    """
    data = list(mongo.db['base_department'].find(
        {'TYPE': 10, 'IS_DELETE': 0, '$and': [{'SHORT_NAME': {'$ne': None}}, {'SHORT_NAME': {'$ne': ''}}]},
        {'_id': 0, 'DEPARTMENT_ID': 1, 'MAJOR': 1, 'NAME': 1, 'TYPE3': 1, 'TYPE4': 1}))
    for i in data:
        i['MAJOR'] = major_map.get(i['MAJOR'])
    if data:
        return jsonify({'data': data, 'status': 0})
    else:
        return jsonify({'error_msg': '无数据', 'status': 1})


@api_bp.route('report/analysis/<string:major>/station/semiannual/doc', methods=['GET'])
def get_analysis_semiannual_station_doc(major):
    """
    导出站段季度报告文档
    :param major: 专业名称的拼音
    :return:
    """
    param_dict = request.args
    year = param_dict.get('year')
    half = param_dict.get('half')
    station_id = param_dict.get('DPID')

    if 'year' in param_dict and 'half' in param_dict and 'DPID' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'SEMIANNUAL')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))

        # 创建报表class的实例
        year = int(year)
        half = int(half)
        report_obj = class_meta(station_id)
        if report_obj.validate_semiannual(year, half) is True:
            dir_path, file_name = report_obj.get_report_file(year, half)
            res = make_response(
                send_from_directory(dir_path, file_name, as_attachment=True))
            return res

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))
# ---------------------------------------- 半年报接口 End ------------------------------------------


# ---------------------------------------- 日报报接口 start ----------------------------------------
@api_bp.route('report/analysis/group/daily/doc', methods=['GET'])
def get_analysis_daily_group_doc():
    """
    集团日报
    :return:
    """
    param_dict = request.args
    report_date = param_dict.get('report_date')
    if 'report_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class('group', 'GROUP', 'DAILY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))
        # 创建报表class的实例
        report_obj = class_meta()
        dir_path, file_name = report_obj.get_report_file(report_date)
        res = make_response(
            send_from_directory(dir_path, file_name, as_attachment=True))
        return res
    else:
        return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


@api_bp.route('report/analysis/group/daily/list')
def get_analysis_daily_group_list():
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')

    if 'start_date' in param_dict and 'end_date' in param_dict:
        report_obj = GroupDailyAnalysisReport()

        data = report_obj.get_available_report_interval_list(start_date, end_date)
        return jsonify(wrapper_rtn_msg(data, 0, ''))

    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/major/daily/doc', methods=['GET'])
def get_analysis_major_daily_doc(major):
    """
    集团日报
    :return:
    """
    param_dict = request.args
    report_date = param_dict.get('report_date')
    if 'report_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'MAJOR', 'DAILY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))
        # 创建报表class的实例
        report_obj = class_meta()
        dir_path, file_name = report_obj.get_report_file(report_date)
        res = make_response(
            send_from_directory(dir_path, file_name, as_attachment=True))
        return res
    else:
        return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


@api_bp.route('report/analysis/<string:major>/major/daily/list')
def get_analysis_major_daily_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    if 'start_date' in param_dict and 'end_date' in param_dict:
        if major == 'chewu':
            return jsonify({'data': {'available_list': []}})
        else:
            class_meta = _get_report_class(major, 'MAJOR', 'DAILY')
            report_obj = class_meta()
            data = report_obj.get_available_report_interval_list(start_date, end_date)
            return jsonify(wrapper_rtn_msg(data, 0, ''))
    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/analysis/<string:major>/station/daily/doc', methods=['GET'])
def get_analysis_station_daily_doc(major):
    """
    集团日报
    :return:
    """
    param_dict = request.args
    report_date = param_dict.get('report_date')
    dpid = param_dict.get('DPID')
    if 'report_date' in param_dict:
        # 动态创建目标的类实例。先查找目标Class
        class_meta = _get_report_class(major, 'STATION', 'DAILY')
        if class_meta is None:
            return jsonify(wrapper_rtn_msg("", 1, '模块开发中'))
        # 创建报表class的实例
        report_obj = class_meta(dpid)
        dir_path, file_name = report_obj.get_report_file(report_date)
        res = make_response(
            send_from_directory(dir_path, file_name, as_attachment=True))
        return res
    else:
        return jsonify(wrapper_rtn_msg("", 1, '缺少参数或季报参数无效'))


@api_bp.route('report/analysis/<string:major>/station/daily/list')
def get_analysis_station_daily_list(major):
    param_dict = request.args
    start_date = param_dict.get('start_date')
    end_date = param_dict.get('end_date')
    dpid = param_dict.get('DPID')
    if 'start_date' in param_dict and 'end_date' in param_dict and 'DPID' in param_dict:
        if major == 'chewu':
            return jsonify({'data': {'available_list': []}})
        else:
            class_meta = _get_report_class(major, 'STATION', 'DAILY')
            report_obj = class_meta()
            data = report_obj.get_available_report_interval_list(start_date, end_date, dpid)
            return jsonify(wrapper_rtn_msg(data, 0, ''))
    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))


@api_bp.route('report/test')
def report_test():
    param_dict = request.args
    month_ago = int(param_dict.get('month_ago'))
    major = param_dict.get('major')
    major_excute(month_ago, major)
    return jsonify(wrapper_rtn_msg("", 1, '缺少参数'))