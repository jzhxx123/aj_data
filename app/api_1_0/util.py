#! /usr/bin/env python3
# -*- encoding:utf-8 -*-
'''
    author: Qiangsheng
    date: 2018/05/04
    desc:  接口路由的一些公用函数
'''

import json
import time
import requests
from flask import current_app, g, request

from app import mongo

# from app.api_1_0 import api_bp


def get_access_token():
    '''
    获取access_token
    params: appId: client_ID
            appSecret: client_Secret
    '''
    appId = current_app.config.get('APPID')
    appSecret = current_app.config.get('APPSECRET')
    baseUrl = 'http://10.192.4.169/SSOauth/accessToken'
    params = [("grant_type", "client_credentials"), ("client_id", appId),
              ("client_secret", appSecret)]
    urlResp = requests.post(baseUrl, data=params)
    urlResp = json.loads(urlResp.text)
    g.accessToken = urlResp['access_token']
    g.leftTime = int(urlResp['expires_in'])
    g.nowTime = time.time()
    mongo.db.base_data.update({
        'TYPE': 'access_token'
    }, {
        "$set": {
            'TYPE': 'access_token',
            'accessToken': g.accessToken,
            'leftTime': g.leftTime,
            'nowTime': g.nowTime
        }
    },
        upsert=True,
        multi=True)


def judg_accessToken():
    '''
    expires_in是access_token的有效时间，超时重新获取
    return: access_token
    '''
    now_time = time.time()
    token_doc = mongo.db.base_data.find_one({'TYPE': 'access_token'})
    if token_doc:
        nowTime = token_doc['nowTime']
        leftTime = token_doc['leftTime']
    else:
        nowTime = 0
        leftTime = 0
    dur_time = now_time - nowTime
    if dur_time >= leftTime - 600:
        get_access_token()
        judg_accessToken()
    else:
        g.accessToken = token_doc['accessToken']


# @api_bp.before_request
# def before_request():
#     g.lock = 0
#     judg_accessToken()
#     # time.sleep(5)
#     print("before_request", g.accessToken)


def get_user_cardId(uid):
    '''
    获取用户身份证号
    params:
            uid: person uuid
    return 用户身份层级
    '''
    baseUrl = 'http://10.192.4.169/SSOauth/openapi/client/getUrlInfo'
    params = {"accessToken": g.accessToken, "urlEncode": uid}
    urlResp = requests.post(baseUrl, data=params)
    if urlResp.json().get('result') == 'False':
        userID = 'no_user'
    rst = urlResp.json().get('data')
    if rst:
        urlResp = json.loads(rst)
        userID = urlResp.get('UserPID')
    else:
        token = request.cookies.get('TOKEN')
        if token:
            userID = token.split('#')[0]
        else:
            userID = 'no_user'
    return userID


niubi_cards = [
    '510106197908081417', '510125197409290010', '510802198209070513',
    '51012519840611581X', '510125198112131216', '511303198403160010',
    '513401197008231616', '513001197803090857', '530111198203024454',
    '520111198312165414', '510129198801173548', '511026197108194713',
    '51100219740501153X', '51102719761025487X', '622826198610041178',
    '510802198108253513', '510622197305220315', '510106198310261418',
    '511137197610211413', '510283198008120399', '620202198912260223',
    '51100219760618121X', '510781198801271816', '420105197302122036',
    '362502198110296851', '510781197503118739', '510106198611111819',
    '510625197612110019', '510125198403242311', '511025198407258818',
    '511024198611225216', '510681198312120317', '513825198711020615',
    '511024198011173133', '511026197303046417', '513401196310271614',
    '510106198409031014', '51102619740921022X', '513401197605041618',
    '320107197502013423', '510106197607211417'
]


def get_person_info(userid):
    '''根据身份证ID获取相应身份信息'''
    person = mongo.db['base_person'].find_one({"ID_CARD": userid}, {"_id": 0})
    g.major = person['MAJOR']
    idcard = person['ID_CARD']
    # 职务代码
    job = str(person['PERSON_JOB_TYPE'])
    if idcard in niubi_cards or person['NAME'] == '局领导' or \
        '安监室' in person['ALL_NAME'] or '党委正职' in job or \
            '处室正职' in job or '行政正职' in job:
        job_id = 0  # 领导
    else:
        job_id = 1  # 职员
    # 部门代码
    if person['TYPE'] in [1]:  # 路局
        dep_id = 0
        station = ''
        all_name = ''
    elif person['TYPE'] in [2, 3]:  # 专业
        dep_id = 1
        station = ''
        all_name = g.major
    elif person['TYPE'] in [4, 5]:  # 站段
        dep_id = 2
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''
    elif person['TYPE'] in [6, 7, 8]:  # 车间
        dep_id = 3
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''
    else:
        dep_id = 4  # 班组
        station = person['ALL_NAME'].split('-')[0]
        all_name = f'''{g.major}-{person['ALL_NAME']}'''
    content = f'{idcard}#{job_id}#{dep_id}#{station}#{all_name}'
    # content = [idcard, job_id, dep_id]
    return content
