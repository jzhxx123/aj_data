from flask import make_response, jsonify

from app.api_1_0 import api_bp


@api_bp.errorhandler(404)
def not_found(error):
    return make_response(
        jsonify({
            'error': 'Not found',
            'error_message': error,
            'status': 1
        }), 404)
