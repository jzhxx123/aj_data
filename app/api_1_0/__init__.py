#! /usr/bin/env python3
# -*- coding: utf-8 -*-

from flask import Blueprint

api_bp = Blueprint('api', __name__)

from . import map, story, report, global_story, role, report_analysis
