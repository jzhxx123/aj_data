import ast
import os

from flask_dotenv import DotEnv

# 当前文件的目录(文件夹名称)的绝对路径
BASEDIR = os.path.abspath(os.path.dirname(__file__))
BASENAME = os.path.basename(BASEDIR)
INSTANCE_DIR = os.path.join(BASEDIR, 'instance')
LOG_DIR = os.path.join(BASEDIR, 'log')


class Config(object):
    PREFIX = ''
    FLASK_APP = os.environ.get('FLASK_APP') or 'manage.py'
    SECRET_KEY = os.environ.get(
        'SECRET_KEY', default=None) or 'hard to guess string'
    # SSL开关
    SSL_DISABLE = False
    SQLALCHEMY_ECHO = False
    # TODO: need research
    # SQLALCHEMY_POOL_SIZE = 100
    # SQLALCHEMY_POOL_RECYCLE = 50

    # True, 每次请求结束, 自动提交数据库中变动
    SQLALCHEMY_COMMIT_ON_TEARDOWN = True
    SQLALCHEMY_RECORD_QUERIES = True
    DATABASE_SLOW_QUERY_TIME = 30
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    SQLALCHEMY_DATABASE_URI_MID = 'sqlite://'
    SQLALCHEMY_BINDS = {
        'db_aj': SQLALCHEMY_DATABASE_URI,
        'db_mid': SQLALCHEMY_DATABASE_URI_MID,
    }
    MONGO_URI = None
    REDIS_URL = None
    CACHE_REDIS_URL = None

    APPID = None
    APPSECRET = None
    FAKE_USERID = None
    AUTH_ENABLED = False

    UPDATE_DAY = 25
    FAKE_TODAY = None
    IS_INIT_INDEX = True
    INIT_MAX_WORKERS = None
    INIT_START_MONTH = '2017-10'

    DOWNLOAD_FILE_HEALTH = 'download/health_index'
    DOWNLOAD_FILE_ANALYSIS = 'download/analysis_report'
    DOWNLOAD_FILE_WEEKLY_ANALYSIS = 'download/weekly_analysis'
    DOWNLOAD_FILE_EVALUATE = 'download/evaluate'
    DOWNLOAD_FILE_WORKSHOP_HEALTH = 'download/workshop_health_index'
    DOWNLOAD_FILE_MONTHLY_ANALYSIS = 'download/monthly_analysis'
    DOWNLOAD_FILE_CONTROL_INTENSITY = 'download/control_intensity_index'

    @classmethod
    def init_app(cls, app):
        # load env by file
        env_file = os.path.join(BASEDIR, '.env')
        if os.path.exists(env_file):
            env = DotEnv()
            env.init_app(app, env_file)

            # TODO: need user must set prefix + var
            # # set prefix + key to key
            # # like DEV_FOO to FOO
            # envs = (
            #     'SQLALCHEMY_DATABASE_URI',
            #     'MONGO_URI',
            # )
            # env.alias(maps={f'{cls.PREFIX}_{env}': env for env in envs})

            env.eval(keys={
                'UPDATE_DAY': int,
            })
        else:
            app.logger.debug('=== Not detect .env file ===')
        # load env by os.environ
        cls.reload_env(app)
        cls.update_bind(app)

    @classmethod
    def reload_env(cls, app):
        for env in dir(cls):
            if f'{cls.PREFIX}_{env}' in os.environ:
                value = os.environ.get(f'{cls.PREFIX}_{env}')
            else:
                value = os.environ.get(env)

            if value and (not value.startswith('#')):
                if value.lower() in ('true', 'false'):
                    value = value.title()
                    value = ast.literal_eval(value)
                elif value.isdigit():
                    value = ast.literal_eval(value)

                app.config[env] = value
                app.logger.debug(f'{env:<24} {value}')

    @classmethod
    def update_bind(cls, app):
        app.config['SQLALCHEMY_BINDS'] = {
            'db_aj': app.config.get('SQLALCHEMY_DATABASE_URI'),
            'db_mid': app.config.get('SQLALCHEMY_DATABASE_URI_MID'),
        }


class DevelopmentConfig(Config):
    DEBUG = True
    PREFIX = 'DEV'

    # SQLALCHEMY_ECHO = True

    @classmethod
    def init_app(cls, app):
        super().init_app(app)
        # from flask_debugtoolbar import DebugToolbarExtension
        # DebugToolbarExtension(app)


class TestingConfig(Config):
    TESTING = True
    PREFIX = 'TEST'
    # 关闭CSRF保护功能, 方便测试
    WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    pass


class RailwayConfig(Config):
    AUTH_ENABLED = True


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'railway': RailwayConfig,
    'default': DevelopmentConfig,
}
