FROM python:3.6
LABEL maintainer='bluethon'

ENV PROJECT_DIR=/app \
    FLASK_APP=manage.py \
    FLASK_ENV="production" \
    APP_PORT=9000 \
    TZ=Asia/Shanghai
# 不能合并, 否则内容不能生效
ENV PYTHONPATH=$PROJECT_DIR

RUN sed -i 's/[[:alpha:]]*.debian.org/mirrors.ustc.edu.cn/g' /etc/apt/sources.list
RUN apt-get update \
    && apt-get install -y --no-install-recommends cron \
    && rm -rf /var/lib/apt/lists/*

WORKDIR $PROJECT_DIR

COPY ./requirements.txt requirements.txt
RUN pip install --no-cache-dir -U pip -r requirements.txt -i https://pypi.douban.com/simple

#COPY ./docker/api/crontab /etc/crontab
COPY ./docker/api/docker-entrypoint.sh /usr/local/bin/
COPY ./docker/api/gunicorn_config.py manage.py config.py ./
COPY ./app app
RUN echo '0  2  *  *  *  root /usr/local/bin/flask daily > /proc/1/fd/1 2>/proc/1/fd/2' > /etc/crontab

ENTRYPOINT ["docker-entrypoint.sh"]

EXPOSE $APP_PORT

CMD ["gunicorn", "-c", "./gunicorn_config.py", "manage:app"]
